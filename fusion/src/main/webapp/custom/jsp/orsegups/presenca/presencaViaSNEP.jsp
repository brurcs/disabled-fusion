<%@page import="com.itextpdf.text.log.SysoLogger"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.UraPresencaUtils"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.EscalaPostoVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.PostoVO"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.EntradaAutorizadaVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.ColaboradorVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page
	import="com.neomind.fusion.custom.orsegups.presenca.vo.HorarioVO"%>
<%@page
	import="com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils"%>
<%@page
	import="com.neomind.fusion.custom.orsegups.utils.QLBioPresencaUtils"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.EscalaVO"%>
<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="com.neomind.fusion.persist.QLWhere"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.io.*"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.TimeZone"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLNotInFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.datawarehouse.ExternalEntityInfo"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="org.apache.commons.logging.Log"%>


<%!
	public static Boolean listaContemValor(List<String> lista, String value)
	{

		Boolean result = false;
		if ((value != null) && (lista != null) && (!lista.isEmpty()))
		{
			for (String str : lista)
			{

				if (str != null && str.trim().equals(value.trim()))
				{
					result = true;
				}
			}
		}

		return result;
	}

	public static void salvarMinhaPresenca(ColaboradorVO colaborador, String acao, String fone)
	{
		try
		{
			/* 
			Desabilitado por Lucas - 06/06/2016
			
			System.out.println("MAT MP: " + colaborador.getNumeroCadastro());
			System.out.println("LOC MP: " + colaborador.getNumeroLocal());
			if (colaborador.getNumeroEmpresa() == 22L)
			{
				URL url = new URL( "http://app.minhapresenca.com.br/MarcacaoService?action=registrarMarcacao&numemp=" + colaborador.getNumeroEmpresa() + "&numcad=" + colaborador.getNumeroCadastro() + "&acao=" + acao + "&fone=" + fone);  
				URLConnection murlc = url.openConnection();
				BufferedReader in = new BufferedReader(new InputStreamReader(murlc.getInputStream()));
				in.close(); 
			}
			*/
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
%>

<%
	//medir tempo execucao
	Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();
	
	final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.presenca.presencaViaSNEP");
	
	//GregorianCalendar dataAtual = new GregorianCalendar();
	
	
	String origemReq = request.getParameter("origem");
	GregorianCalendar dataAtual = null;
	if (origemReq != null){
	    if (origemReq.contains("BIOPRESENCA")){
			String idPonto = origemReq.split(";")[1];
			
			String numEmpPonto = request.getParameter("numEmp");
			String numCadPonto = request.getParameter("numCad");
			Long numCadLongPonto = NeoUtils.safeLong(numCadPonto);
			Long numEmpLongPonto = NeoUtils.safeLong(numEmpPonto);
			Long idLongPonto = NeoUtils.safeLong(idPonto);
			
			dataAtual = QLBioPresencaUtils.getHorarioPontoColaboradorBioPresenca(numCadLongPonto, numEmpLongPonto, idLongPonto);
	    }else if (origemReq.equals("SNEP")){
			dataAtual = new GregorianCalendar();
	    }else{
			dataAtual = new GregorianCalendar();
	    }
	}else{
	    dataAtual = new GregorianCalendar();
	}
	
	//GregorianCalendar dataAtual = new GregorianCalendar( dataAux.get(GregorianCalendar.YEAR), dataAux.get(GregorianCalendar.MONTH), dataAux.get(GregorianCalendar.DAY_OF_MONTH));
	
	//Teste
	//dataAtual.set(Calendar.DATE, 22);
	//dataAtual.set(Calendar.MONTH, 0);
	//dataAtual.set(Calendar.YEAR, 2013);  
	
// 	dataAtual.set(Calendar.HOUR_OF_DAY, 06);  
// 	dataAtual.set(Calendar.MINUTE, 53);  
	//dataAtual.set(Calendar.SECOND, 0); 
	//dataAtual.set(Calendar.MILLISECOND, 0); 
	//System.out.println(NeoUtils.safeDateFormat(dataAtual));
	
	String verboseError = request.getParameter("verboseError");
	boolean isVerboseError = false;
	if (verboseError != null && (verboseError.trim().equals("sim") || verboseError.trim().equals("1") || verboseError.trim().equals("yes") || verboseError.trim().equals("on")))
	{
		isVerboseError = true;
	}

	String username = request.getParameter("username");
	String testMode = request.getParameter("testMode");
	boolean isTest = false;
	if (testMode != null && (testMode.trim().equals("sim") || testMode.trim().equals("1") || testMode.trim().equals("yes") || testMode.trim().equals("on")))
	{
		isTest = true;
	}
	isTest = false;
	String fone = request.getParameter("fone");
	String origem = "";
	if (fone != null && fone.trim().equals("fusion"))
	{
		origem = "FusionAA(" + username + ")";
	}
	else
	{
		origem = "SNEP-" + fone;
	}
	origem = URLEncoder.encode(origem, "ISO8859-1");

	String numEmp = request.getParameter("numEmp");
	if (numEmp == null || numEmp.trim().equals(""))
	{
		out.print("Erro #1 - C�digo da Empresa n�o informado");
		return;
	}
	Long numEmpLong = 0L;
	String numCad = request.getParameter("numCad");
	if (numCad == null || numCad.trim().equals(""))
	{
		out.print("Erro #2 - Matr�cula n�o informada");
		return;
	}
	Long numCadLong = 0L;

	String acao = request.getParameter("acao");
	if (acao == null || (!acao.trim().equals("login") && !acao.trim().equals("logoff")))
	{
		out.print("Erro #3 - A��o inv�lida");
		return;
	}
	
	boolean isDebug = true;

	Connection conn = null;

	/*
		Retornos poss�veis:
		OK - Sem erro
		1 - Empresa inv�lida!
		2 - Matr�cula inv�lida!
		3 - Entrada duplicada!
		4 - Acesso n�o permitido!
		99 - Erro desconhecido. Entre em contato com seu supervisor.
	 */
	try
	{
		String nomeFonteDados = "VETORH";
		conn = PersistEngine.getConnection(nomeFonteDados);
		String direcaoAcesso = "";
		String tipoDirecaoAcesso = "P";
		Boolean entradaSaidaPadrao = true;
		String centroCustoPosto = "";
		List<String> listaCentroCustoTelefone = new ArrayList<String>();
		ColaboradorVO colaborador = new ColaboradorVO();
		EscalaPostoVO escalaPosto = new EscalaPostoVO();
		Long permanenciaPosto = 80L;
		
		/*
		 * Validar Empresa
		 */
		numEmpLong = NeoUtils.safeLong(numEmp);
		if ((numEmpLong != 1L) && (numEmpLong != 2L) && (numEmpLong != 4L) && (numEmpLong != 6L) && (numEmpLong != 7L) && (numEmpLong != 8L) && (numEmpLong != 12L) && (numEmpLong != 15L) && (numEmpLong != 17L) && (numEmpLong != 18L) && (numEmpLong != 19L) && (numEmpLong != 21L) && (numEmpLong != 22L)
			&& (numEmpLong != 23L) && (numEmpLong != 24L) && (numEmpLong != 25L) && (numEmpLong != 26L) && (numEmpLong != 27L) && (numEmpLong != 28L) && (numEmpLong != 29L) && (numEmpLong != 30L))
		{
			if (isVerboseError)
			{
				out.print("1-Empresa inv�lida");
			}
			else
			{
				out.print("{\"id\":\"" + numCad + "\", \"return\":\"1\"}");
			}
			if (isDebug)
			{
				log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> 1-Empresa inv�lida (" + numEmp + ")");
			}
			return;
		}
		
		/*
		 * Validar Matricula
		 */
		numCadLong = NeoUtils.safeLong(numCad);
		Boolean horarioFlexivel = QLPresencaUtils.buscaHorarioFlexivel(numEmpLong, numCadLong);
		colaborador = QLPresencaUtils.getBuscaColaborador(numCadLong, numEmpLong, dataAtual);
		String mensagens = UraPresencaUtils.getBuscaMensagens(numCadLong, numEmpLong, dataAtual, acao);
		log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Colaborador - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");

		if (colaborador == null || colaborador.getNumeroCracha().equals("") || colaborador.getCpf().equals(85113220949L)
			|| colaborador.getCpf().equals(3383424006L) || colaborador.getCpf().equals(8774479938L)
			|| colaborador.getCpf().equals(8062157989L) || colaborador.getCpf().equals(506545946L)
			|| colaborador.getCpf().equals(5307401922L) || colaborador.getCpf().equals(4086517922L)
			|| colaborador.getCpf().equals(8598551988L) || colaborador.getCpf().equals(10946568952L)
			|| colaborador.getCpf().equals(9494750926L) || colaborador.getCpf().equals(9338860981L))
		{
			if (isVerboseError)
			{
				out.print("2-Matr�cula inv�lida");
			}
			else
			{
				out.print("{\"id\":\"" + numCad + "\", \"return\":\"2\"}");
			}
			if (isDebug)
			{
				log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> 2-Matr�cula inv�lida (" + numEmpLong + "/" + numCadLong + ")");
			}
			return;
		}
		
		/*
		 * Validar Telefone
		 */
		String fone2 = fone;
		
		if (fone.length()>10){
		    String subFone = fone.substring(3);
		    
		    if(subFone.startsWith("7") || subFone.startsWith("8") || subFone.startsWith("9")){
			fone2 = fone.substring(0, 2)+subFone;
		    }	    
		}
		  
		QLRawFilter telefoneFilter = new QLRawFilter(" REPLACE(isNull(ddd,'') + numero, '-', '') = '" + fone + "' OR REPLACE(isNull(ddd,'') + numero, '-', '') = '"+fone2+"'  ");
		
		NeoObject noTelefone = PersistEngine.getObject(AdapterUtils.getEntityClass("TELECOMCelulares"), telefoneFilter);
		if (NeoUtils.safeIsNotNull(noTelefone))
		{
			EntityWrapper wrpTelefone = new EntityWrapper(noTelefone);
			Collection<NeoObject> listaNoCentroCusto = wrpTelefone.findField("centroCustoTelefone").getValues();
			Integer countCC = 1;
			for (NeoObject noCentroCusto : listaNoCentroCusto)
			{
				EntityWrapper wrpCentroCusto = new EntityWrapper(noCentroCusto);
				String strCentroCusto = (String) wrpCentroCusto.findValue("centroCusto");
				//log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> CCT: " + strCentroCusto);
				if ((!NeoUtils.safeIsNull(strCentroCusto)) && (!NeoUtils.safeEquals(strCentroCusto, "")))
				{
					listaCentroCustoTelefone.add(strCentroCusto);
					centroCustoPosto = centroCustoPosto + strCentroCusto;
					
					if (listaNoCentroCusto.size() > countCC)
					{
						centroCustoPosto = centroCustoPosto + ",";
					}
					countCC ++;
				}
			}

		}
		if (listaCentroCustoTelefone == null || listaCentroCustoTelefone.isEmpty())
		{
			if (isVerboseError)
			{
				out.print("10-Acesso n�o permitido ramal n�o cadastrado!");
			}
			else
			{
				out.print("{\"id\":\"" + numCad + "\", \"return\":\"10\"}");
			}
			if (isDebug)
			{
				if (acao.equals("login"))
				{
					direcaoAcesso = "E";
				} else {
					direcaoAcesso = "S";
				}
				
				/*
				 * Registrar o problema no acesso - Telefone do Posto n�o Cadastrado
				 */
				if (QLPresencaUtils.isFuncionarioFerias(numEmpLong, colaborador.getTipoColaborador(), numCadLong)){ // se o colaborador est� de f�rias marca como acesso local
					log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o (Chegada ao local) - colaborador de f�rias (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
					tipoDirecaoAcesso = "L";
				}
				QLPresencaUtils.insereRegistroAcesso(colaborador, direcaoAcesso, tipoDirecaoAcesso, dataAtual, 101L, permanenciaPosto, fone);
				salvarMinhaPresenca(colaborador,acao,fone);
				log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> 4-Acesso n�o permitido - Telefone n�o cadastrado (" + numEmpLong + "/" + numCadLong + ")");
			}
			return;
		}

		/*
		 * Verificar se entrada/saida � padrao, ou seja, se o centro de custos do colaboradro existe no telefone informado.
		 */
		entradaSaidaPadrao = listaContemValor(listaCentroCustoTelefone, colaborador.getCentroCusto());
		
		log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> CCC: " + colaborador.getCentroCusto() + " CCP:" + centroCustoPosto + " LC: " + colaborador.getNumeroLocal() + ")");
		
		/*
		 * Entrada
		 */
		if (acao.equals("login"))
		{
			direcaoAcesso = "E";

			/*
			 * Entrada Padrao
			 */
			if (entradaSaidaPadrao)
			{		
				/*
				 * Validar Horario do posto 
				 */
				escalaPosto = QLPresencaUtils.getEscalaPosto(colaborador.getNumeroLocal(), colaborador.getCodigoOrganograma(), dataAtual);
				
				if (escalaPosto != null && escalaPosto.getHorariosHoje() != null && !escalaPosto.getHorariosHoje().isEmpty())
				{
					permanenciaPosto = escalaPosto.getPermanenciaPosto();
					
					if (!horarioFlexivel && !QLPresencaUtils.validaHorarioPosto(escalaPosto, dataAtual, direcaoAcesso))
					{
						if (isVerboseError)
						{
							out.print("4-Acesso n�o permitido!");
						}
						else
						{
						    if (mensagens == null || mensagens.isEmpty()){
								out.print("{\"id\":\"" + numCad + "\", \"return\":\"4\"}");
						    }else{
								out.print("{\"id\":\"" + numCad + "\", \"return\":\"4;"+mensagens+"\"}");
							
						    }
						}
						if (isDebug)
						{
							/*
							 * Registrar o problema no acesso - Acesso Negado pelo Hor�rio do Posto Inv�lido
							 */
							if (QLPresencaUtils.isFuncionarioFerias(numEmpLong, colaborador.getTipoColaborador(), numCadLong)){ // se o colaborador est� de f�rias marca como acesso local
								log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o (Chegada ao local) - colaborador de f�rias (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
								tipoDirecaoAcesso = "L";
							}
							QLPresencaUtils.insereRegistroAcesso(colaborador, direcaoAcesso, tipoDirecaoAcesso, dataAtual, 102L, permanenciaPosto, fone);
							salvarMinhaPresenca(colaborador,acao,fone);
							log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o - Hor�rio do posto inv�lido (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
						}
						return;
					}
				}
				
				/*
				 * Validar Tolerancia e duplicidade
				 */
				Integer retorno = QLPresencaUtils.validaToleranciaEscala(colaborador, escalaPosto, permanenciaPosto, direcaoAcesso, dataAtual, tipoDirecaoAcesso, true);
				
				if(horarioFlexivel)
					retorno = 1;
				
				switch (retorno) {
				
				case 1:
					
					break;
				
				case 3:
					if (isVerboseError)
					{
						out.print("3-Entrada duplicada");
					}
					else
					{
						out.print("{\"id\":\"" + numCad + "\", \"return\":\"3\"}");
					}
					if (isDebug)
					{
						/*
						 * Registrar o problema no acesso - Acesso Duplicado
						 */
						if (QLPresencaUtils.isFuncionarioFerias(numEmpLong, colaborador.getTipoColaborador(), numCadLong)){ // se o colaborador est� de f�rias marca como acesso local
							log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o (Chegada ao local) - colaborador de f�rias (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
							tipoDirecaoAcesso = "L";
						}
						QLPresencaUtils.insereRegistroAcesso(colaborador, direcaoAcesso, tipoDirecaoAcesso, dataAtual, 103L, permanenciaPosto, fone);
						salvarMinhaPresenca(colaborador,acao,fone);
						log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o - Duplicada (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
					}
					return;
				case 4:
					
					/*
					 * Validar Entrada Autorizada no local de Servi�o
					 */
					List<EntradaAutorizadaVO> entradasAutorizadas = new ArrayList<EntradaAutorizadaVO>();
					entradasAutorizadas = QLPresencaUtils.getEntradasAutorizadasColaborador(colaborador, dataAtual, centroCustoPosto);
					
					if (entradasAutorizadas != null && !entradasAutorizadas.isEmpty()) 
					{
						int countEntradas = 0;
						for (EntradaAutorizadaVO entrada : entradasAutorizadas)
						{
							/*
							 * Validar Horario do posto 
							 */
							boolean postoValido = false; 
							escalaPosto = QLPresencaUtils.getEscalaPosto(entrada.getColaborador().getNumeroLocal(), entrada.getColaborador().getCodigoOrganograma(), dataAtual);	
							
							countEntradas++;
							
							if (escalaPosto != null && escalaPosto.getHorariosHoje() != null && !escalaPosto.getHorariosHoje().isEmpty())
							{
								permanenciaPosto = escalaPosto.getPermanenciaPosto();
								
								if (entrada.getDescricaoCobertura().equals("HORA EXTRA - Cobertura Posto com Hora Extra")) 
								{
									if (QLPresencaUtils.validaHorarioPosto(escalaPosto, dataAtual, direcaoAcesso))
									{
										postoValido = true;
									}
								}
								else
								{
									postoValido = true;
								}
							}
							
							if(horarioFlexivel)
								postoValido = true;
							
							if (!postoValido)
							{
								if (isVerboseError)
								{
									out.print("4-Acesso n�o permitido!");
								}
								else
								{
								    if (mensagens == null || mensagens.isEmpty()){
										out.print("{\"id\":\"" + numCad + "\", \"return\":\"4\"}");
								    }else{
										out.print("{\"id\":\"" + numCad + "\", \"return\":\"4;"+mensagens+"\"}");									
								    }
								}
								if (isDebug)
								{
									/*
									 * Registrar o problema no acesso - Acesso Negado pelo Hor�rio do Posto Inv�lido
									 */
									if (entradasAutorizadas.size() == countEntradas) 
									{
										if (QLPresencaUtils.isFuncionarioFerias(numEmpLong, colaborador.getTipoColaborador(), numCadLong)){ // se o colaborador est� de f�rias marca como acesso local
											log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o (Chegada ao local) - colaborador de f�rias (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
											tipoDirecaoAcesso = "L";
										}
										QLPresencaUtils.insereRegistroAcesso(entrada.getColaborador(), direcaoAcesso, tipoDirecaoAcesso, dataAtual, 102L, permanenciaPosto, fone);
										salvarMinhaPresenca(entrada.getColaborador(),acao,fone);
										log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o - Hor�rio do posto inv�lido (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
									}
								}
								return;
							}
							//Verifica �ltima Entrada Padrao nas ultimas 12 horas
							
							String tipoDirecaoAcesso2 = QLPresencaUtils.verificaUltimaEntradaPadrao(entrada.getColaborador().getNumeroCracha(), dataAtual);	
							//tipoDirecaoAcesso = QLPresencaUtils.verificaUltimaEntradaPadrao(entrada.getColaborador().getNumeroCracha(), dataAtual);						
							
							/*
							 * Validar Tolerancia e duplicidade
							 */
							Integer retorno2 = QLPresencaUtils.validaToleranciaEscala(entrada.getColaborador(), escalaPosto, permanenciaPosto, direcaoAcesso, dataAtual, tipoDirecaoAcesso2, true);
							
							if(horarioFlexivel)
								retorno2 = 1;
							
							switch (retorno2) {
							
							case 1:
								if (!QLPresencaUtils.possuiCoberuraHEColaborador(colaborador, entrada, direcaoAcesso))
								{
									tipoDirecaoAcesso = tipoDirecaoAcesso2;
								}
								colaborador = entrada.getColaborador();
								
								break;
							case 3:
								if (isVerboseError)
								{
									out.print("3-Entrada duplicada");
								}
								else
								{
									out.print("{\"id\":\"" + numCad + "\", \"return\":\"3\"}");
								}
								if (isDebug)
								{
									/*
									 * Registrar o problema no acesso - Acesso Duplicado
									 */
									if (QLPresencaUtils.isFuncionarioFerias(numEmpLong, colaborador.getTipoColaborador(), numCadLong)){ // se o colaborador est� de f�rias marca como acesso local
										log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o (Chegada ao local) - colaborador de f�rias (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
										tipoDirecaoAcesso = "L";
									}
									QLPresencaUtils.insereRegistroAcesso(colaborador, direcaoAcesso, tipoDirecaoAcesso2, dataAtual, 103L, permanenciaPosto, fone);
									salvarMinhaPresenca(colaborador,acao,fone);
									log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o - Entrada autorizada duplicada  (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
								}
								return;
							case 4:
								//log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o - Entrada autorizada hor�rio inv�lido (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
								
								if (isVerboseError)
								{
									out.print("4-Acesso n�o permitido!");
								}
								else
								{
								    if (mensagens == null || mensagens.isEmpty()){
										out.print("{\"id\":\"" + numCad + "\", \"return\":\"4\"}");
								    }else{
										out.print("{\"id\":\"" + numCad + "\", \"return\":\"4;"+mensagens+"\"}");									
								    }
								}
								if (isDebug)
								{
									/*
									 * Registrar o problema no acesso - Acesso Negado pelo Hor�rio Inv�lido
									 */
									if (QLPresencaUtils.isFuncionarioFerias(numEmpLong, colaborador.getTipoColaborador(), numCadLong)){ // se o colaborador est� de f�rias marca como acesso local
										log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o (Chegada ao local) - colaborador de f�rias (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
										tipoDirecaoAcesso = "L";
									}
									QLPresencaUtils.insereRegistroAcesso(colaborador, direcaoAcesso, tipoDirecaoAcesso2, dataAtual, 104L, permanenciaPosto, fone);
									salvarMinhaPresenca(colaborador,acao,fone);
									log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o - Entrada autorizada hor�rio inv�lido (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
								}
								return;
							}
						}
					}
					else
					{
						//log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o - Hor�rio Autorizado inv�lido (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
						
						if (isVerboseError)
						{
							out.print("4-Acesso n�o permitido!");
						}
						else
						{
						    if (mensagens == null || mensagens.isEmpty()){
								out.print("{\"id\":\"" + numCad + "\", \"return\":\"4\"}");
						    }else{
								out.print("{\"id\":\"" + numCad + "\", \"return\":\"4;"+mensagens+"\"}");							
						    }
						}
						if (isDebug)
						{
							/*
							 * Registrar o problema no acesso - Acesso Negado pelo Hor�rio Inv�lido
							 */
							if (QLPresencaUtils.isFuncionarioFerias(numEmpLong, colaborador.getTipoColaborador(), numCadLong)){ // se o colaborador est� de f�rias marca como acesso local
								log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o (Chegada ao local) - colaborador de f�rias (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
								tipoDirecaoAcesso = "L";
							}
							QLPresencaUtils.insereRegistroAcesso(colaborador, direcaoAcesso, tipoDirecaoAcesso, dataAtual, 104L, permanenciaPosto, fone);
							salvarMinhaPresenca(colaborador,acao,fone);
							log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o - Hor�rio Autorizado inv�lido (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
						}
						return;
					}
				}
			}
			
			/*
			 * Entrada Autorizada
			 */
			else
			{
				/*
				 * Validar Entrada Autorizada
				 */
				List<EntradaAutorizadaVO> entradasAutorizadas = new ArrayList<EntradaAutorizadaVO>();
				entradasAutorizadas = QLPresencaUtils.getEntradasAutorizadasColaborador(colaborador, dataAtual, centroCustoPosto);
				
				if (entradasAutorizadas != null && !entradasAutorizadas.isEmpty()) 
				{
					int countEntradas = 0;
					for (EntradaAutorizadaVO entrada : entradasAutorizadas)
					{
						/*
						 * Validar Horario do posto 
						 */
						
						boolean postoValido = false; 
						escalaPosto = QLPresencaUtils.getEscalaPosto(entrada.getColaborador().getNumeroLocal(), entrada.getColaborador().getCodigoOrganograma(), dataAtual);	
						
						countEntradas++;
						
						if (escalaPosto != null && escalaPosto.getHorariosHoje() != null && !escalaPosto.getHorariosHoje().isEmpty())
						{
							permanenciaPosto = escalaPosto.getPermanenciaPosto();
							
							if (entrada.getDescricaoCobertura().equals("HORA EXTRA - Cobertura Posto com Hora Extra")) 
							{
								if (QLPresencaUtils.validaHorarioPosto(escalaPosto, dataAtual, direcaoAcesso))
								{
									postoValido = true;
								}
							}
							else
							{
								postoValido = true;
							}
						}
						
						if(horarioFlexivel)
							postoValido = true;
						
						if (!postoValido)
						{
							if (isVerboseError)
							{
								out.print("4-Acesso n�o permitido!");
							}
							else
							{
							    if (mensagens == null || mensagens.isEmpty()){
									out.print("{\"id\":\"" + numCad + "\", \"return\":\"4\"}");
							    }else{
									out.print("{\"id\":\"" + numCad + "\", \"return\":\"4;"+mensagens+"\"}");								
							    }
							}
							if (isDebug)
							{
								/*
								 * Registrar o problema no acesso - Acesso Negado pelo Hor�rio do Posto Inv�lido
								 */
								if (entradasAutorizadas.size() == countEntradas) 
								{
									if (QLPresencaUtils.isFuncionarioFerias(numEmpLong, colaborador.getTipoColaborador(), numCadLong)){ // se o colaborador est� de f�rias marca como acesso local
										log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o (Chegada ao local) - colaborador de f�rias (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
										tipoDirecaoAcesso = "L";
									}
									QLPresencaUtils.insereRegistroAcesso(entrada.getColaborador(), direcaoAcesso, tipoDirecaoAcesso, dataAtual, 102L, permanenciaPosto, fone);
									salvarMinhaPresenca(entrada.getColaborador(),acao,fone);
									log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o - Hor�rio do posto inv�lido (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
								}
							}
							return;
						}
						
						//Verifica �ltima Entrada Padrao nas ultimas 12 horas
						tipoDirecaoAcesso = QLPresencaUtils.verificaUltimaEntradaPadrao(entrada.getColaborador().getNumeroCracha(), dataAtual);						
						
						/*
						 * Validar Tolerancia e duplicidade
						 */
						Integer retorno = QLPresencaUtils.validaToleranciaEscala(entrada.getColaborador(), escalaPosto, permanenciaPosto, direcaoAcesso, dataAtual, tipoDirecaoAcesso, true);
						
						if(horarioFlexivel)
							retorno = 1;
						
						switch (retorno) {
						
						case 1:
							colaborador = entrada.getColaborador();
							break;
						case 3:
							if (isVerboseError)
							{
								out.print("3-Entrada duplicada");
							}
							else
							{
								out.print("{\"id\":\"" + numCad + "\", \"return\":\"3\"}");
							}
							if (isDebug)
							{
								if (QLPresencaUtils.isFuncionarioFerias(numEmpLong, colaborador.getTipoColaborador(), numCadLong)){ // se o colaborador est� de f�rias marca como acesso local
									log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o (Chegada ao local) - colaborador de f�rias (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
									tipoDirecaoAcesso = "L";
								}
								QLPresencaUtils.insereRegistroAcesso(entrada.getColaborador(), direcaoAcesso, tipoDirecaoAcesso, dataAtual, 103L, permanenciaPosto, fone);
								salvarMinhaPresenca(entrada.getColaborador(),acao,fone);
								log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Autorizada - Duplicada (" + numEmpLong + "/" + numCadLong + ")");
							}
							return;
						case 4:
							//log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Autorizada - Hor�rio inv�lido (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
							colaborador = entrada.getColaborador();
							
							if (isVerboseError)
							{
								out.print("4-Acesso n�o permitido!");
							}
							else
							{
							    if (mensagens == null || mensagens.isEmpty()){
									out.print("{\"id\":\"" + numCad + "\", \"return\":\"4\"}");
							    }else{
									out.print("{\"id\":\"" + numCad + "\", \"return\":\"4;"+mensagens+"\"}");								
							    }
							}
							if (isDebug)
							{
								if (QLPresencaUtils.isFuncionarioFerias(numEmpLong, colaborador.getTipoColaborador(), numCadLong)){ // se o colaborador est� de f�rias marca como acesso local
									log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o (Chegada ao local) - colaborador de f�rias (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
									tipoDirecaoAcesso = "L";
								}
								QLPresencaUtils.insereRegistroAcesso(entrada.getColaborador(), direcaoAcesso, tipoDirecaoAcesso, dataAtual, 104L, permanenciaPosto, fone);
								salvarMinhaPresenca(entrada.getColaborador(),acao,fone);
								log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Autorizada - Hor�rio inv�lido (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
							}
							return;
						}
					}
				} 
				else 
				{
					if (isVerboseError)
					{
						out.print("10-Acesso n�o permitido ramal inv�lido!");
					}
					else
					{
						out.print("{\"id\":\"" + numCad + "\", \"return\":\"10\"}");
					}
					if (isDebug)
					{
						if (QLPresencaUtils.isFuncionarioFerias(numEmpLong, colaborador.getTipoColaborador(), numCadLong)){ // se o colaborador est� de f�rias marca como acesso local
							log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o (Chegada ao local) - colaborador de f�rias (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
							tipoDirecaoAcesso = "L";
						}
						QLPresencaUtils.insereRegistroAcesso(colaborador, direcaoAcesso, tipoDirecaoAcesso, dataAtual, 101L, permanenciaPosto, fone);
						salvarMinhaPresenca(colaborador,acao,fone);
						log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada n�o permitido ramal inv�lido! (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
					}
					return;	
				}
			}
		}
		else if (acao.equals("logoff"))
		{
			direcaoAcesso = "S";
			
			/*
			 * Saida Padrao
			 */
			if (entradaSaidaPadrao)
			{
				
				/*
				 * Validar Horario do posto 
				 */
				escalaPosto = QLPresencaUtils.getEscalaPosto(colaborador.getNumeroLocal(), colaborador.getCodigoOrganograma(), dataAtual);
				
				if (escalaPosto != null && escalaPosto.getHorariosHoje() != null && !escalaPosto.getHorariosHoje().isEmpty())
				{
					permanenciaPosto = escalaPosto.getPermanenciaPosto();
					
					if (!horarioFlexivel && !QLPresencaUtils.validaHorarioPosto(escalaPosto, dataAtual, direcaoAcesso))
					{
						if (isVerboseError)
						{
							out.print("4-Acesso n�o permitido!");
						}
						else
						{
						    if (mensagens == null || mensagens.isEmpty()){
								out.print("{\"id\":\"" + numCad + "\", \"return\":\"4\"}");
						    }else{
								out.print("{\"id\":\"" + numCad + "\", \"return\":\"4;"+mensagens+"\"}");							
						    }
						}
						if (isDebug)
						{
							/*
							 * Registrar o problema no acesso - Acesso Negado pelo Hor�rio do Posto Inv�lido
							 */
							if (QLPresencaUtils.isFuncionarioFerias(numEmpLong, colaborador.getTipoColaborador(), numCadLong)){ // se o colaborador est� de f�rias marca como acesso local
								log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o (Chegada ao local) - colaborador de f�rias (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
								tipoDirecaoAcesso = "L";
							}
							QLPresencaUtils.insereRegistroAcesso(colaborador, direcaoAcesso, tipoDirecaoAcesso, dataAtual, 102L, permanenciaPosto, fone);
							salvarMinhaPresenca(colaborador,acao,fone);
							log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Sa�da Padr�o - Hor�rio do posto inv�lido (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
						}
						return;
					}
				}
				
				/*
				 * Validar Tolerancia e duplicidade
				 */
				Integer retorno = QLPresencaUtils.validaToleranciaEscala(colaborador, escalaPosto, permanenciaPosto, direcaoAcesso, dataAtual, tipoDirecaoAcesso, true);
				
				if(horarioFlexivel)
					retorno = 1;
				
				switch (retorno) {
				
				case 1:
					
					break;
				
				case 3:
					if (isVerboseError)
					{
						out.print("7-Sa�da duplicada");
					}
					else
					{
					    if (mensagens == null || mensagens.isEmpty()){
							out.print("{\"id\":\"" + numCad + "\", \"return\":\"8\"}");
					    }else{
							out.print("{\"id\":\"" + numCad + "\", \"return\":\"8;"+mensagens+"\"}");						
					    }
					}
					if (isDebug)
					{
						/*
						 * Registrar o problema no acesso - Acesso Duplicado
						 */
						if (QLPresencaUtils.isFuncionarioFerias(numEmpLong, colaborador.getTipoColaborador(), numCadLong)){ // se o colaborador est� de f�rias marca como acesso local
							log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o (Chegada ao local) - colaborador de f�rias (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
							tipoDirecaoAcesso = "L";
						}
						QLPresencaUtils.insereRegistroAcesso(colaborador, direcaoAcesso, tipoDirecaoAcesso, dataAtual, 103L, permanenciaPosto, fone);
						salvarMinhaPresenca(colaborador,acao,fone);
						log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Saida Padr�o - Duplicada (" + numEmpLong + "/" + numCadLong + ")");
					}
					return;
				case 4:
					
					List<EntradaAutorizadaVO> entradasAutorizadas = new ArrayList<EntradaAutorizadaVO>();
					entradasAutorizadas = QLPresencaUtils.getEntradasAutorizadasColaborador(colaborador, dataAtual, centroCustoPosto);
					
					if (entradasAutorizadas != null && !entradasAutorizadas.isEmpty()) 
					{
						int countEntradas = 0;
						for (EntradaAutorizadaVO entrada : entradasAutorizadas)
						{		
							/*
							 * Validar Horario do posto 
							 */
							boolean postoValido = false; 
							escalaPosto = QLPresencaUtils.getEscalaPosto(entrada.getColaborador().getNumeroLocal(), entrada.getColaborador().getCodigoOrganograma(), dataAtual);	
							
							countEntradas++;
							
							if (escalaPosto != null && escalaPosto.getHorariosHoje() != null && !escalaPosto.getHorariosHoje().isEmpty())
							{
								permanenciaPosto = escalaPosto.getPermanenciaPosto();
								
								if (entrada.getDescricaoCobertura().equals("HORA EXTRA - Cobertura Posto com Hora Extra")) 
								{
									if (QLPresencaUtils.validaHorarioPosto(escalaPosto, dataAtual, direcaoAcesso))
									{
										postoValido = true;
									}
								}
								else
								{
									postoValido = true;
								}
							}
							
							if(horarioFlexivel)
								postoValido = true;
							
							if (!postoValido)
							{
								if (isVerboseError)
								{
									out.print("4-Acesso n�o permitido!");
								}
								else
								{
								    if (mensagens == null || mensagens.isEmpty()){
										out.print("{\"id\":\"" + numCad + "\", \"return\":\"4\"}");
								    }else{
										out.print("{\"id\":\"" + numCad + "\", \"return\":\"4;"+mensagens+"\"}");									
								    }
								}
								if (isDebug)
								{
									/*
									 * Registrar o problema no acesso - Acesso Negado pelo Hor�rio do Posto Inv�lido
									 */
									if (entradasAutorizadas.size() == countEntradas) 
									{
										if (QLPresencaUtils.isFuncionarioFerias(numEmpLong, colaborador.getTipoColaborador(), numCadLong)){ // se o colaborador est� de f�rias marca como acesso local
											log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o (Chegada ao local) - colaborador de f�rias (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
											tipoDirecaoAcesso = "L";
										}
										QLPresencaUtils.insereRegistroAcesso(entrada.getColaborador(), direcaoAcesso, tipoDirecaoAcesso, dataAtual, 102L, permanenciaPosto, fone);
										salvarMinhaPresenca(entrada.getColaborador(),acao,fone);
										log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Sa�da Padr�o - Autorizada - Hor�rio do posto inv�lido (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
									}
								}
								return;
							}
							
							//Verifica �ltima Entrada Autorizada
							String tipoDirecaoAcesso2 = QLPresencaUtils.verificaUltimaEntradaAutorizada(entrada.getColaborador().getNumeroCracha(),entrada.getColaborador().getNumeroLocal(), entrada.getColaborador().getCodigoOrganograma(), dataAtual);		
							
							/*
							 * Validar Tolerancia e duplicidade
							 */
							Integer retorno2 = QLPresencaUtils.validaToleranciaEscala(entrada.getColaborador(), escalaPosto, permanenciaPosto, direcaoAcesso, dataAtual, tipoDirecaoAcesso2, true);
							
							if(horarioFlexivel)
								retorno2 = 1;
							
							switch (retorno2) {
							
							case 1:
								if (!QLPresencaUtils.possuiCoberuraHEColaborador(colaborador, entrada, direcaoAcesso))
								{
									tipoDirecaoAcesso = tipoDirecaoAcesso2;
								}
								colaborador = entrada.getColaborador();
								break;
							case 3:
								if (isVerboseError)
								{
									out.print("7-Saida duplicada");
								}
								else
								{
								    if (mensagens == null || mensagens.isEmpty()){
										out.print("{\"id\":\"" + numCad + "\", \"return\":\"8\"}");
								    }else{
										out.print("{\"id\":\"" + numCad + "\", \"return\":\"8;"+mensagens+"\"}");									
								    }
								}
								if (isDebug)
								{
									/*
									 * Registrar o problema no acesso - Acesso Duplicado
									 */
									if (QLPresencaUtils.isFuncionarioFerias(numEmpLong, colaborador.getTipoColaborador(), numCadLong)){ // se o colaborador est� de f�rias marca como acesso local
										log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o (Chegada ao local) - colaborador de f�rias (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
										tipoDirecaoAcesso = "L";
									}
									QLPresencaUtils.insereRegistroAcesso(entrada.getColaborador(), direcaoAcesso, tipoDirecaoAcesso2, dataAtual, 103L, permanenciaPosto, fone);
									salvarMinhaPresenca(entrada.getColaborador(),acao,fone);
									log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Saida Padr�o - Sa�da autorizada duplicada (" + numEmpLong + "/" + numCadLong + ")");
								}
								return;
							case 4:
								//log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Saida Padr�o - Sa�da autorizada hor�rio inv�lido (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
								colaborador = entrada.getColaborador();
								if (isVerboseError)
								{
									out.print("8-Acesso n�o permitido!");
								}
								else
								{								   
								    if (mensagens == null || mensagens.isEmpty()){
										out.print("{\"id\":\"" + numCad + "\", \"return\":\"8\"}");
								    }else{
										out.print("{\"id\":\"" + numCad + "\", \"return\":\"8;"+mensagens+"\"}");
									
								    }
								    
								}
								if (isDebug)
								{
									/*
									 * Registrar o problema no acesso - Acesso Negado pelo Hor�rio Inv�lido
									 */
									if (QLPresencaUtils.isFuncionarioFerias(numEmpLong, colaborador.getTipoColaborador(), numCadLong)){ // se o colaborador est� de f�rias marca como acesso local
										log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o (Chegada ao local) - colaborador de f�rias (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
										tipoDirecaoAcesso = "L";
									}
									QLPresencaUtils.insereRegistroAcesso(entrada.getColaborador(), direcaoAcesso, tipoDirecaoAcesso2, dataAtual, 104L, permanenciaPosto, fone);
									salvarMinhaPresenca(entrada.getColaborador(),acao,fone);
									log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Saida Padr�o - Sa�da autorizada hor�rio inv�lido (" + numEmpLong + "/" + numCadLong + ")");
								}
								return;
							}
						}
					} 
					else 
					{
						//log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Saida Padr�o - Hor�rio Autorizado inv�lido (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
						if (isVerboseError)
						{
							out.print("8-Acesso n�o permitido!");
						}
						else
						{
						    if (mensagens == null || mensagens.isEmpty()){
							out.print("{\"id\":\"" + numCad + "\", \"return\":\"8\"}");
						    }else{
								out.print("{\"id\":\"" + numCad + "\", \"return\":\"8;"+mensagens+"\"}");							
						    }
						}
						if (isDebug)
						{
							/*
							 * Registrar o problema no acesso - Acesso Negado pelo Hor�rio Inv�lido
							 */
							if (QLPresencaUtils.isFuncionarioFerias(numEmpLong, colaborador.getTipoColaborador(), numCadLong)){ // se o colaborador est� de f�rias marca como acesso local
								log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o (Chegada ao local) - colaborador de f�rias (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
								tipoDirecaoAcesso = "L";
							}
							QLPresencaUtils.insereRegistroAcesso(colaborador, direcaoAcesso, tipoDirecaoAcesso, dataAtual, 104L, permanenciaPosto, fone);
							salvarMinhaPresenca(colaborador,acao,fone);
							log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Saida Padr�o - Hor�rio Inv�lido (" + numEmpLong + "/" + numCadLong + ")");
						}
						return;
					}	
				}
			}
			/*
			 * Saida Autorizada
			 */
			else
			{
				/*
				 * Validar Saida Autorizada
				 */
				List<EntradaAutorizadaVO> entradasAutorizadas = new ArrayList<EntradaAutorizadaVO>();
				entradasAutorizadas = QLPresencaUtils.getEntradasAutorizadasColaborador(colaborador, dataAtual, centroCustoPosto);
				
				if (entradasAutorizadas != null && !entradasAutorizadas.isEmpty()) 
				{
					int countEntradas = 0;
					for (EntradaAutorizadaVO entrada : entradasAutorizadas)
					{		
						/*
						 * Validar Horario do posto 
						 */
						boolean postoValido = false; 
						escalaPosto = QLPresencaUtils.getEscalaPosto(entrada.getColaborador().getNumeroLocal(), entrada.getColaborador().getCodigoOrganograma(), dataAtual);	
						
						countEntradas++;
						
						if (escalaPosto != null && escalaPosto.getHorariosHoje() != null && !escalaPosto.getHorariosHoje().isEmpty())
						{
							permanenciaPosto = escalaPosto.getPermanenciaPosto();
							
							if (entrada.getDescricaoCobertura().equals("HORA EXTRA - Cobertura Posto com Hora Extra")) 
							{
								if (QLPresencaUtils.validaHorarioPosto(escalaPosto, dataAtual, direcaoAcesso))
								{
									postoValido = true;
								}
							}
							else
							{
								postoValido = true;
							}
						}
						
						if(horarioFlexivel)
							postoValido = true;
						
						if (!postoValido)
						{
							if (isVerboseError)
							{
								out.print("4-Acesso n�o permitido!");
							}
							else
							{
							    if (mensagens == null || mensagens.isEmpty()){
									out.print("{\"id\":\"" + numCad + "\", \"return\":\"4\"}");
							    }else{
									out.print("{\"id\":\"" + numCad + "\", \"return\":\"4;"+mensagens+"\"}");
								
							    }
							}
							if (isDebug)
							{
								/*
								 * Registrar o problema no acesso - Acesso Negado pelo Hor�rio do Posto Inv�lido
								 */
								if (entradasAutorizadas.size() == countEntradas) 
								{
									if (QLPresencaUtils.isFuncionarioFerias(numEmpLong, colaborador.getTipoColaborador(), numCadLong)){ // se o colaborador est� de f�rias marca como acesso local
										log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o (Chegada ao local) - colaborador de f�rias (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
										tipoDirecaoAcesso = "L";
									}
									QLPresencaUtils.insereRegistroAcesso(entrada.getColaborador(), direcaoAcesso, tipoDirecaoAcesso, dataAtual, 102L, permanenciaPosto, fone);
									salvarMinhaPresenca(entrada.getColaborador(),acao,fone);
									log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Sa�da Autorizada - Hor�rio do posto inv�lido (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
								}
							}
							return;
						}
						
						//Verifica �ltima Entrada Autorizada
						tipoDirecaoAcesso = QLPresencaUtils.verificaUltimaEntradaAutorizada(entrada.getColaborador().getNumeroCracha(),entrada.getColaborador().getNumeroLocal(), entrada.getColaborador().getCodigoOrganograma(), dataAtual);	
						
						/*
						 * Validar Tolerancia e duplicidade
						 */
						Integer retorno = QLPresencaUtils.validaToleranciaEscala(entrada.getColaborador(), escalaPosto, permanenciaPosto, direcaoAcesso, dataAtual, tipoDirecaoAcesso, true);
						
						if(horarioFlexivel)
							retorno = 1;
						
						switch (retorno) {
						
						case 1:
							colaborador = entrada.getColaborador();
							break;
						case 3:
							if (isVerboseError)
							{
								out.print("7-Saida duplicada");
							}
							else
							{
							    if (mensagens == null || mensagens.isEmpty()){
									out.print("{\"id\":\"" + numCad + "\", \"return\":\"8\"}");
							    }else{
									out.print("{\"id\":\"" + numCad + "\", \"return\":\"8;"+mensagens+"\"}");								
							    }
							}
							if (isDebug)
							{
								log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Sa�da Autorizada - Duplicada (" + numEmpLong + "/" + numCadLong + ")");
							}
							return;
						case 4:
							colaborador = entrada.getColaborador();
							
							if (isVerboseError)
							{
								out.print("4-Acesso n�o permitido!");
							}
							else
							{
							    if (mensagens == null || mensagens.isEmpty()){
									out.print("{\"id\":\"" + numCad + "\", \"return\":\"4\"}");
							    }else{
									out.print("{\"id\":\"" + numCad + "\", \"return\":\"4;"+mensagens+"\"}");								
							    }
							}
							if (isDebug)
							{
								if (QLPresencaUtils.isFuncionarioFerias(numEmpLong, colaborador.getTipoColaborador(), numCadLong)){ // se o colaborador est� de f�rias marca como acesso local
									log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o (Chegada ao local) - colaborador de f�rias (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
									tipoDirecaoAcesso = "L";
								}
								QLPresencaUtils.insereRegistroAcesso(entrada.getColaborador(), direcaoAcesso, tipoDirecaoAcesso, dataAtual, 104L, permanenciaPosto, fone);
								salvarMinhaPresenca(entrada.getColaborador(),acao,fone);
								log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Sa�da Autorizada - Hor�rio inv�lido (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
							}
							return;
						}
					}
				} 
				else 
				{
					if (isVerboseError)
					{
						out.print("10-Acesso n�o permitido ramal inv�lido!");
					}
					else
					{
						out.print("{\"id\":\"" + numCad + "\", \"return\":\"10\"}");
					}
					if (isDebug)
					{
						if (QLPresencaUtils.isFuncionarioFerias(numEmpLong, colaborador.getTipoColaborador(), numCadLong)){ // se o colaborador est� de f�rias marca como acesso local
							log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o (Chegada ao local) - colaborador de f�rias (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
							tipoDirecaoAcesso = "L";
						}
						QLPresencaUtils.insereRegistroAcesso(colaborador, direcaoAcesso, tipoDirecaoAcesso, dataAtual, 101L, permanenciaPosto, fone);
						salvarMinhaPresenca(colaborador,acao,fone);
						log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Sa�da n�o permitida ramal inv�lido (" + numEmpLong + "/" + numCadLong + ") - - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
					}
					return;
				}
			}
		}

		if (!isTest)
		{
			// Registrar o acesso
			if (QLPresencaUtils.isFuncionarioFerias(numEmpLong, colaborador.getTipoColaborador(), numCadLong)){ // se o colaborador est� de f�rias marca como acesso local
				log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Entrada Padr�o (Chegada ao local) - colaborador de f�rias (" + numEmpLong + "/" + numCadLong + ") - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
				tipoDirecaoAcesso = "L";
			}
			QLPresencaUtils.insereRegistroAcesso(colaborador, direcaoAcesso, tipoDirecaoAcesso, dataAtual, 100L, permanenciaPosto, fone);
			salvarMinhaPresenca(colaborador,acao,fone);
			
			if (isDebug)
			{
				log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Acesso Registrado com sucesso (" + numEmpLong + "/" + numCadLong + ")");
			}

		}
		// entrada / saida com sucesso
		if (isVerboseError)
		{
			out.print("OK");
		}
		else
		{
		    if (mensagens == null || mensagens.isEmpty()){
				out.print("{\"id\":\"" + numCad + "\", \"return\":\"OK\"}");
		    }else{
			    out.print("{\"id\":\"" + numCad + "\", \"return\":\"OK;"+mensagens+"\"}");	
		    }
		    
		}
		

	}
	catch (Exception e)
	{
		e.printStackTrace();
		
		StringWriter erros = new StringWriter();
		e.printStackTrace(new PrintWriter(erros));
		log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Erro:"+ erros.toString());
		try {
			erros.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		if (isVerboseError)
		{
			out.print("9-Erro desconhecido");
		}
		else
		{
			out.print("{\"id\":\"" + numCad + "\", \"return\":\"9\"}");
		}
		return;
	}
	finally
	{
		try
		{
			log.warn("##### PRESENCA (" + acao + ") [" + numCad + "] via " + origem + "> Total - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
			conn.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			out.print("Erro #6 - Erro ao finalizar conex�o");
			return;
		}
	}
%>
