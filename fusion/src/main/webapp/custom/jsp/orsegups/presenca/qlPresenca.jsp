<%@page import="java.net.URLEncoder"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.EscalaPostoVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.ValeTransporteVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.JustificativaMarcacaoVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.RelatorioActionsEnum"%>
<%@page import="com.neomind.fusion.security.NeoPaper"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.EscalaVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.ColaboradorVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.PostoVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.ClienteVO"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@taglib uri="/WEB-INF/form.tld" prefix="form"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLNotInFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.datawarehouse.ExternalEntityInfo"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.Timestamp"%>
<%@page import="com.neomind.fusion.custom.orsegups.presenca.vo.EntradaAutorizadaVO"%>
<%@page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="org.apache.commons.logging.LogFactory"%>
<%@page import="org.apache.commons.logging.Log"%>

 
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL() %>custom/jsp/orsegups/overlib/overlib.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>

<%
    final Log log = LogFactory.getLog("jsp.custom.jsp.orsegups.presenca.qlPresenca");

	NeoUser user = PortalUtil.getCurrentUser();
	NeoPaper papelOperadorQl = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code","[QLP] Operador do Quadro de Lota��o Presen�a"));
	NeoPaper papelOperadorQlApuracao = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code","[QLP] Operador do Quadro de Lota��o Apura��o Ponto"));
	NeoPaper papelLogQl = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code","[QLP] Log do Quadro de Lota��o Presen�a"));
	NeoPaper papelPresencaSupervisao = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code","presencaSupervisao"));
	NeoPaper papelVisualizarLog = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code","QLPVisualizarLog"));
	
	/*** Declaracoes globais ***/
	Long codigoOrganograma = 203L;
	GregorianCalendar dataAtual = new GregorianCalendar();
	Long timeMillisDataAtual = 0L; // para passar datas via ajax sem se preocupar com formato
	SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	SimpleDateFormat dfData = new SimpleDateFormat("dd/MM/yyyy");
	
	Boolean isOperadorQl = Boolean.FALSE;
	Boolean isOperadorQlApuracao = Boolean.FALSE;
	Boolean isLogQl = Boolean.FALSE;
	Boolean isPresencaSupervisao = Boolean.FALSE;
	Boolean isVisualizarLog = Boolean.FALSE;
	
	if(user != null && user.getPapers() != null && user.getPapers().contains(papelOperadorQl)) {
		isOperadorQl = Boolean.TRUE;
	}
	
	if(user != null && user.getPapers() != null && user.getPapers().contains(papelOperadorQlApuracao)) {
		isOperadorQlApuracao = Boolean.TRUE;
	}
	
	if(user != null && user.getPapers() != null && user.getPapers().contains(papelLogQl)) {
		isLogQl = Boolean.TRUE;
	}
	if(user != null && user.getPapers() != null && user.getPapers().contains(papelPresencaSupervisao)) {
		isPresencaSupervisao = Boolean.TRUE;
	}
	
	if(user != null && user.getPapers() != null && user.getPapers().contains(papelVisualizarLog)) {
		isVisualizarLog = Boolean.TRUE;
	}
			
	String[] controles = request.getParameterValues("controle");
	String controle = null;

	String raizQL = request.getParameter("raizQL");
	Boolean isContratos = Boolean.TRUE;	
	if(raizQL != null && raizQL.trim().equals("adm") )	{
		isContratos = Boolean.FALSE;	
	}
	
	String regional = request.getParameter("regional");
	String empresa = request.getParameter("empresa");
	String dep = request.getParameter("dep");
	String parAsseio = request.getParameter("asseio");
	Boolean isAsseio = Boolean.FALSE;
	if (parAsseio != null && parAsseio.equals("true")){
	    isAsseio = Boolean.TRUE;
	}
	String raizCC = "";
	
	if (dep != null && !dep.equals(""))
	{
		if (dep.equals("financeiro"))
		{
			raizCC = "1.1.01.00002.002%";
		}
		else if (dep.equals("administrativo"))
		{
			raizCC = "1.1.01.00002.006%";
		}
		else if (dep.equals("controladoria"))
		{
			raizCC = "1.1.01.00002.008%";
		}
		else if (dep.equals("cerec"))
		{
			raizCC = "1.1.01.00002.009%";
		}
		else if (dep.equals("marketing"))
		{
			raizCC = "1.1.01.00002.010%";
		}
		else if (dep.equals("assessoriaTecnica"))
		{
			raizCC = "1.1.01.00003.001.03%";
		}
		else if (dep.equals("qualidade"))
		{
			raizCC = "1.1.01.00003.001.05%";
		}
		else if (dep.equals("publico"))
		{
			raizCC = "1.1.01.00003.004%";
		}
		else if (dep.equals("privado"))
		{
			raizCC = "1.1.01.00003.005.01%";
		}
		else if (dep.equals("rh"))
		{
			raizCC = "1.1.01.00003.006%";
		}
		else if (dep.equals("zonaVerde"))
		{
			raizCC = "1.1.01.00003.008%";
		}
		else if (dep.equals("ti"))
		{
			raizCC = "1.1.01.00005%";
		}
		else if (dep.equals("juridico"))
		{
			raizCC = "1.1.01.00001.005%";
		}
		else if (dep.equals("cm"))
		{
			raizCC = "1.1.01.00001.006%";
		}
		else if (dep.equals("operacionalHumana"))
		{
			raizCC = "1.1.01.00002.003.02%";
		}
		else if (dep.equals("operacionalEletronica"))
		{
			raizCC = "1.1.01.00002.003.12%";
		}
		else if (dep.equals("rhEstrategico")){
			raizCC = "1.1.01.00003.027%";
		}
		else if (dep.equals("tiSistemas")){
			raizCC = "1.1.01.00005.001.03.0001%";
		}
		else if (dep.equals("tiInfraestrutura"))
		{
			raizCC = "1.1.01.00005.001.03.0002%";
		}else if (dep.equals("rastreamento"))
		{
		    raizCC = "1.1.01.00003.029%";
		}else if (dep.equals("nexti"))
		{
		    raizCC = "1.1.01.00001.007%";
	    }else if (dep.equals("nexxus"))
	    {
		    raizCC = "1.1.01.00001.008%";
		}else if (dep.equals("winker"))
		{
		    raizCC = "1.1.01.00001.009%";
		}else if (dep.equals("pro")){
		    raizCC = "1.1.01.00001.011%";
		}else if (dep.equals("presenca")){
		    raizCC = "1.1.01.00001.012%";
		}else if (dep.equals("nacinterno")){
		    raizCC = "1.1.01.00002.011%";
		}else if (dep.equals("recuperacaoCredito")){
		    raizCC = "1.1.01.00001.010%";
		}
	}
	
	String tipCli = request.getParameter("tipCli");
	if(tipCli == null || tipCli.trim().equals(""))	{
		tipCli = "0";
	}
	
	String dataString = request.getParameter("dataString");
	if(dataString != null && !dataString.trim().equals(""))	{
		Date parsed = df.parse(dataString);
		dataAtual.setTime(parsed);
	} else {
		dataString = NeoUtils.safeDateFormat(dataAtual);
	}
	
	timeMillisDataAtual = dataAtual.getTimeInMillis();
	
	GregorianCalendar dataAux = new GregorianCalendar();  
	dataAux.add(Calendar.MINUTE, -15);
	
	Boolean diaAtual = true;
	
	//Data diferente da data atual
	if (dataAtual.before(dataAux)) {
		diaAtual = false;
	}
	
	if (controles == null) {
		Cookie cookie = null;
		Cookie aCookies[] = request.getCookies();

		if (aCookies != null) {
			for(int i = 0; i < aCookies.length; i++){
	
				if(aCookies[i].getName().equals("controle")){
					
					cookie = aCookies[i];
					
				}
				
			}
		}
		
		if (cookie != null) {
			controle = cookie.getValue();
		} else {
			controle = "ON/NA/OF/ST";
		}
	} else {
		controle = "";
		for (String str : controles) {
			if (!controle.equals("")) {
				controle = controle + "/";
			}
			controle = controle + str;
		}

	}

	Cookie cookie = new Cookie("controle", controle);
	response.addCookie(cookie);

	Long codigoRegional = NeoUtils.safeLong(regional);
	Long codigoEmpresa = NeoUtils.safeLong(empresa);
	Long codigoTipCli = NeoUtils.safeLong(tipCli);
%>

<head>
	<meta charset="UTF-8">
</head>

<portal:head title="Quadro de Lota��o - Presen�a">

<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui-lang-pt_BR.js"></script>
<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/icon.css"> 
<script type="text/javascript" src="https://www.google.com/jsapi"></script> 
    
    <script type="text/javascript">
	   google.load("visualization","1",{packages:["corechart"]});
	   google.setOnLoadCallback(drawChart);
	   function loadPieChart(numemp,tipcol,numcad,cpt){
	       $.ajax({
	           type:"post",
	           url:"servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet",
	           data:'action=pieChartSituacoes&numemp='+numemp+'&tipcol='+tipcol+'&numcad='+numcad+'&cpt='+cpt,
	           dataType:'text',
	           success: function(result) {
	        	   var result = eval('('+result+')');
	               var data = new google.visualization.arrayToDataTable(result);
	               var options = {title:'Situacao da Apuracao do Ponto',is3D:true,width:600,height:330,tooltip:{text:'percentage'}};
	               var chart = new google.visualization.PieChart(document.getElementById('piechart'));
	               chart.draw(data, options);
	               $('#modalSituacaoColaborador').window('open');
	           }
	       });
	   }
	</script>

<cw:main>
	<cw:header title="Quadro de Lota��o - Presen�a" />
	<cw:body id="area_scroll">
	
	<div id="dlgLoader" title="Carregando..." class="easyui-window" data-options="modal:true,closed:true,resizable:false,collapsible:false,minimizable:false,maximizable:false,closable:false" style="width:180px;height:80px">  
		<div style="width:100%; text-align:center; margin-top: 10px">
			<img src="imagens/custom/loader.gif">
		</div>	
	</div>
	
	<div id="modalSituacaoColaborador" class="easyui-window" title="Situa��o do Colaborador" data-options="modal:true,closed:true,resizable:false,collapsible:false,minimizable:false,maximizable:false" style="width:620px;height:386px">  
		<div id="piechart"></div>
	</div>
	
	<div id="modalEscalaRevezamento" class="easyui-window" title="Revezamento de Escala" data-options="modal:true,closed:true,resizable:false,collapsible:false,minimizable:false,maximizable:false" style="width:850px;height:470px;padding:10px;">  
    
    </div>

    <div id="modalEscalaPosto" class="easyui-window" title="Escala Semanal" data-options="modal:true,closed:true,resizable:false,collapsible:false,minimizable:false,maximizable:false" style="width:400px;height:220px;padding:10px;">  
    
    </div>
    
    <div id="modalErrosAcesso" class="easyui-window" title="Erros de Acesso" data-options="modal:true,closed:true,resizable:true,collapsible:false,minimizable:false,maximizable:false" style="width:605px;height:400px;padding:10px;">  
    
    </div>
    
    <div id="modalFichaColaborador" class="easyui-window" title="Ficha do Colaborador" data-options="modal:true,closed:true,resizable:false,collapsible:false,minimizable:false,maximizable:false" style="width:1024px;height:650px">  
    
    </div>
    
    <div id="modalFichaPosto" class="easyui-window" title="Ficha do Posto" data-options="modal:true,closed:true,resizable:false,collapsible:false,minimizable:false,maximizable:false" style="width:1024px;height:670px">  
    
    </div>
    
    <div id="modalContaSigma" class="easyui-window" title="Conta SIGMA" data-options="modal:true,closed:true,resizable:false,collapsible:false,minimizable:false,maximizable:false" style="width:800px;height:500px">  
    
    </div>
    
    <div id="modalVisita" class="easyui-window" title="Visitas" data-options="modal:true,closed:true,resizable:false,collapsible:false,minimizable:false,maximizable:false" style="width:800px;height:500px">  
    
    </div>
    
    <div id="modalTelefones" class="easyui-window" title="Telefones" data-options="modal:true,closed:true,resizable:false,collapsible:false,minimizable:false,maximizable:false" style="width:300px;height:200px">  
    
    </div>
    
	<div style="width: 100%;height: 30px">
		<form name="formControles" method="POST" onSubmit="return validaData();">
			<input type="hidden" name="regional" id="regional" value="<%=regional%>">
			<input type="hidden" name="raizQL" id="raizQL" value="<%=raizQL%>">
			<input onclick="clickBtn(this.value);" type="checkbox" <%= controle.contains("ON") ? "checked" : "" %> name="controle" value="ON" id="ON"/> <label for="mostrarON" id="mostrarON" style="font-weight: <%= controle.contains("ON") ? "bold" : "normal" %>;"><img src="imagens/custom/online.png">&nbsp;Consistentes</label>
			<input onclick="clickBtn(this.value);" type="checkbox" <%= controle.contains("NA") ? "checked" : "" %> name="controle" value="NA" id="NA"/> <label for="mostrarNA" id="mostrarNA" style="font-weight: <%= controle.contains("NA") ? "bold" : "normal" %>;"><img src="imagens/custom/na.png">&nbsp;Consistentes</label>
			<input onclick="clickBtn(this.value);" type="checkbox" <%= controle.contains("OF") ? "checked" : "" %> name="controle" value="OF" id="OF"/> <label for="mostrarOF" id="mostrarOF" style="font-weight: <%= controle.contains("OF") ? "bold" : "normal" %>;"><img src="imagens/custom/offline.png">&nbsp;Inconsistentes</label>
			<input onclick="clickBtn(this.value);" type="checkbox" <%= controle.contains("VG") ? "checked" : "" %> name="controle" value="VG" id="VG"/> <label for="mostrarVG" id="mostrarVG" style="font-weight: <%= controle.contains("VG") ? "bold" : "normal" %>;"><img src="imagens/custom/offline.png">&nbsp;Efetivo Inconsistente</label>
			<input onclick="clickBtn(this.value);" type="checkbox" <%= controle.contains("ST") ? "checked" : "" %> name="controle" value="ST" id="ST"/> <label for="mostrarST" id="mostrarST" style="font-weight: <%= controle.contains("ST") ? "bold" : "normal" %>;"><img src="imagens/icones_final/sphere_yellow_att_16x16-trans.png">&nbsp;N�o cadastrado</label>&nbsp;&nbsp;&nbsp;
			<% if (isOperadorQlApuracao) { %>
				<input onclick="clickBtn(this.value);" type="checkbox" <%= controle.contains("EA") ? "checked" : "" %> name="controle" value="EA" id="EA"/> <label for="mostrarEA" id="mostrarEA" style="font-weight: <%= controle.contains("EA") ? "bold" : "normal" %>;"><img src="custom/jsp/orsegups/jms/themes/icons/clock_pencil.png">&nbsp;Exce��es no Ponto</label>
			<% } %>
			<input id="dataString" class="input_text" style="height: 20px;width: 120px" name="dataString" value="<%=dataString%>" errorSpan="err_var_dataInicialDe" required="false" onchange="" onfocus="activeDeactiveObj(this)" onblur="activeDeactiveObj(this)"/><span id="err_var_dataInicialDe" style="margin-left:0px;"></span>&nbsp;<img src="imagens/icones_final/date_16x16-trans.png" class="ico" id="btdataInicialDe" name="btdataInicialDe"  onclick="return showNeoCalendar('dataString', '%d/%m/%Y %H:%M','btdataInicialDe', event);">&nbsp;<img src='imagens/icones_final/eraser_16x16-trans.png' class='ico' title="Buscar a data atual" onclick="javascript: limpaData();">&nbsp;&nbsp;&nbsp;
			<span>Utilize o formato DD/MM/AAAA HH:MM</span>
			<input type="submit" class="input_button" value="Aplicar"/>
		</form>
	</div>
	<%
		// medir tempo execucao
		Long timeExecFinal = GregorianCalendar.getInstance().getTimeInMillis();
	
		// medir tempo execucao
		Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();
		List<EscalaVO> escalas = QLPresencaUtils.getEscalasColaboradores(dataAtual,0L,0L,false);
		//List<EscalaVO> escalas = QLPresencaUtils.getEscalasColaboradoresOld(dataAtual,0L,0L); // se a nova versao que reduz custo de processamento falhar, use esta.
		log.warn("getEscalasColaboradores - Reg: " + regional + " - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
		System.out.println("getEscalasColaboradores - Reg: " + regional + " - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
		
		// medir tempo execucao
		timeExec = GregorianCalendar.getInstance().getTimeInMillis();
		List<ColaboradorVO> colaboradores = QLPresencaUtils.getListaColaboradores(codigoRegional, codigoEmpresa, codigoOrganograma, escalas ,dataAtual ,codigoTipCli, isContratos, isAsseio);
		log.warn("getListaColaboradores - Reg: " + regional + " - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");

		// medir tempo execucao
		timeExec = GregorianCalendar.getInstance().getTimeInMillis();
		List<EntradaAutorizadaVO> entradasAutorizadas = QLPresencaUtils.getListaEntradasAutorizadas(codigoRegional, codigoOrganograma, colaboradores, dataAtual ,codigoTipCli,escalas);
		log.warn("getListaEntradasAutorizadas - Reg: " + regional + " - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");

		// medir tempo execucao
		timeExec = GregorianCalendar.getInstance().getTimeInMillis();	
		QLPresencaUtils.atualizaStatusColaboradores(colaboradores, entradasAutorizadas);
		log.warn("atualizaStatusColaboradores - Reg: " + regional + " - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
		
		// medir tempo execucao
		timeExec = GregorianCalendar.getInstance().getTimeInMillis();	
		QLPresencaUtils.atualizaCoberuraHE(colaboradores, entradasAutorizadas);
		log.warn("atualizaCoberuraHE - Reg: " + regional + " - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
		
		//medir tempo execucao
		timeExec = GregorianCalendar.getInstance().getTimeInMillis();	
		QLPresencaUtils.atualizaTrocaDeEscala(colaboradores, entradasAutorizadas);
		log.warn("atualizaTrocaDeEscala - Reg: " + regional + " - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");

		// medir tempo execucao
		timeExec = GregorianCalendar.getInstance().getTimeInMillis();
		List<EscalaPostoVO> escalasPosto = QLPresencaUtils.getEscalasPosto(codigoRegional, codigoEmpresa, codigoOrganograma, dataAtual, isContratos, codigoTipCli, raizCC);
		log.warn("getEscalasPosto - Reg: " + regional + " - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
		
		// medir tempo execucao
		timeExec = GregorianCalendar.getInstance().getTimeInMillis();	
		Collection<ClienteVO> listaClientes = QLPresencaUtils.getClientes(colaboradores, entradasAutorizadas, escalasPosto, codigoRegional, codigoEmpresa, codigoOrganograma, dataAtual, isContratos ,codigoTipCli, raizCC, isAsseio);
		log.warn("getClientes - Reg: " + regional + " - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
		
		// medir tempo execucao
		timeExec = GregorianCalendar.getInstance().getTimeInMillis();	
		QLPresencaUtils.ajustaEscalaColaborador(listaClientes);
		log.warn("ajustaEscalaColaborador - Reg: " + regional + " - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
		
		List<JustificativaMarcacaoVO> justificativas = QLPresencaUtils.listaJustificativaMarcacao(null);
		
		log.warn("Tempo Total - Reg:" + regional + " - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExecFinal) + "ms");
		
		for (ClienteVO cliente : listaClientes)
		{
			Collection<PostoVO> listaPostos = new ArrayList();
			Long countTotal = 0L;
			
			for (PostoVO posto : cliente.getPostos())
			{
				if  (
					(posto.getStatus().isStatusOnline() && posto.getTelefone() != null && controle.contains("ON")) ||
					(posto.getStatus().isStatusNa() && posto.getTelefone() != null && controle.contains("NA")) ||
					(posto.getStatus().isStatusOffline() && posto.getTelefone() != null && controle.contains("OF")) ||
					(posto.getTelefone() == null && controle.contains("ST")) 
					)
				{
					if (controle.contains("EA"))
					{
						Long countPosto = 0L;
						for (ColaboradorVO col : posto.getColaboradores())
						{
							if (col.getQdeExcecao() > 0)
							{
								countPosto = col.getQdeExcecao();
								countTotal = countTotal + countPosto;
							}
						}
						if (countPosto > 0)
						{
							listaPostos.add(posto);
						}
					}else
					{
						listaPostos.add(posto);
					}
				}else if (	/*(posto.getStatus().getDescricao().contains("inconsistente")) &&*/ 
							(controle.equals("VG") && (posto.getVagas()  !=  posto.getColaboradores().size()) )
						 )
				{
					listaPostos.add(posto);
				}
			}
			if (!listaPostos.isEmpty()){
	%>			
				<FIELDSET class="fieldGroup">
				<LEGEND class="legend">&nbsp;<%= cliente.getNomeCliente() %>&nbsp;</LEGEND>
				<div style="min-height: 25px;">
					<input type="button" class="input_button" value="Liberar Presen�a Site" id="btn_<%= cliente.getCodigoCliente().replace(".", "") %>" onclick="javascript:liberacaoPresenca('spn_<%= cliente.getCodigoCliente().replace(".", "") %>','<%= cliente.getCodigoCliente()  %>', '<%= PortalUtil.getCurrentUser().getCode() %>');" >
				</div>
				<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0" width="100%">
				<tbody>						
				<%
				
					for (PostoVO posto : listaPostos)
					{
						
							String btnFichaPosto = "<a title='Ficha do Posto' style='cursor:pointer' onclick='javascript:showFichaPosto(" + posto.getNumeroLocal() + "," + posto.getCodigoOrganograma() + ",\""+ raizQL + "\")'><img src='imagens/custom/home_16x16.png' /></a>";
							String btnEscalaPosto = "<a style='cursor:\"pointer\"' onclick='javascript:showEscalaPosto(" + posto.getNumeroLocal() + ", " + posto.getCodigoOrganograma() + ")'><img src='imagens/icones_final/clock_16x16.png' title='Visualizar Escala' /></a>";
							String bgPosto = "";
							
							//Boolean controlaASO = QLPresencaUtils.contratoControlaASO(posto.getNumeroContrato(), posto.getNumeroPosto());
							
							
							if (posto.getSituacao().equals("Inativo"))
							{
								bgPosto = "style='background-color: #EECCCC;'";
							} 
							else if (posto.getSituacao().equals("Encerrando"))
							{
								bgPosto = "style='background-color: #f3d5ba;'";
							}
							if (posto.getSituacao().equals("Iniciando"))
							{
								bgPosto = "style='background-color: #d9fbdb;'";
							}
					%>				
							<tr id="posto-<%= posto.getCentroCusto() %>" style="cursor: auto">
								<th style="cursor: auto">Nome do Posto</th>
								<th style="cursor: auto">Cidade</th>
								<th style="cursor: auto">Servi�o</th>
								<th style="cursor: auto" width="100px">Centro de Custo</th>
								<th style="cursor: auto" width="140px">Dispositivo</th>
								<th style="cursor: auto" width="140px">Contato</th>
								<th style="cursor: auto" width="190px">Escala de Hoje</th>
								<th style="cursor: auto" width="155px">Situa��o</th>
								<th style="cursor: auto" width="60px">Vagas/Colab.</th>
								<th style="cursor: auto" width="60px">Status</th>
							</tr>
							<tr <%= bgPosto %>>
								<td style="font-weight: bold"><%= btnFichaPosto + "&nbsp"%>
								<% if (!isPresencaSupervisao || isVisualizarLog) {%>
								<a class="easyui-tooltip" 
								<% 		if (isLogQl ) {%>
											href="javascript:saveInputLog('<%=posto.getCodigoLocal() %>','null','posto')" 
								<% 		} %>	
									data-options="
							            content: $('<div></div>'),
							            position: 'right',
							            hideDelay: '400',
							            showDelay: '400',
							            onUpdate: function(cc){
							                cc.panel({
							                    width: 600,
							                    height: 500,
							                    title: 'LOG DO POSTO',
							                    href: '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/presenca/logPosto.jsp?codloc=<%= posto.getCodigoLocal().toString()%>'
							                });
							            },
							            onShow: function(){
						                    var t = $(this);
						                    t.tooltip('tip').unbind().bind('mouseenter', function(){
						                        t.tooltip('show');
						                    }).bind('mouseleave', function(){
						                        t.tooltip('hide');
						                    });
						                },
						                onPosition: function(){
						                    $(this).tooltip('tip').css('top', $(this).offset().top);
						                    $(this).tooltip('arrow').css('top', 20);
						                }
							        "><img src='imagens/icones_final/document_v_16x16-trans.png' style='cursor: pointer;'/></a>
							    <% } %>
							    <%
						    	String colabIncons = "";
						    	colabIncons = QLPresencaUtils.listaEmpresaMatriculaColaborador(posto.getColaboradores());
						    	String numPosto = String.valueOf(posto.getNumeroPosto());
						    	%>
								<%=	posto.getLotacao() + " - " + posto.getNomePosto()%></td>
								<td style="font-weight: bold"><%= posto.getCidade() != null ? posto.getCidade() : "&nbsp" %></td>
								<td style="font-weight: bold"><%= posto.getServico() %></td>
								<td style="font-weight: bold" width="100px"><%= posto.getCentroCusto() %></td>
								<td style="font-weight: bold" width="140px"><%= posto.getTelefone() != null ? QLPresencaUtils.getScriptTelefone(posto.getTelefone()) : "<img src='imagens/icones_final/sphere_yellow_att_16x16-trans.png' alt='N�o cadastrado'/> N�o cadastrado" %></td>
								<td style="font-weight: bold" width="140px"><%= QLPresencaUtils.getScriptContato(posto.getCentroCusto()) %></td>
								<td style="font-weight: bold" width="190px"><%= posto.getEscalaPosto() != null ? btnEscalaPosto + "&nbsp;" + posto.getEscalaPosto() : btnEscalaPosto + "&nbsp; Sem Escala" %> </td>
								<td style="font-weight: bold" width="155px"<%= posto.getSituacao().equals("Inativo") ? " style='font-weight: bold; color: red; text-transform: uppercase'" : " style='font-weight: bold'" %>><%= posto.getSituacao() + (posto.getMotivoEncerramento() == null ? "":" <br> Motivo:") + NeoUtils.safeOutputString( posto.getMotivoEncerramento())  %></td>
								<td style="font-weight: bold" width="60px"><%= posto.getVagas() %> / <%= posto.getColaboradores().size() %></td>
								<td style="font-weight: bold" width="50px"><%= posto.getStatus().getImageTag() %><% if(posto.getStatus() != null && posto.getStatus().getDescricao().contains("inconsistente")){ %>&nbsp;<a title="Enviar e-mail posto inconsistente" style="cursor:pointer" onclick="javascript:postoInconsistente('<%=posto.getNomePosto().replaceAll("'", "") %>','<%=posto.getLotacao().replaceAll("'", "")%>','<%=posto.getCidade().replaceAll("'", "")%>','<%=posto.getServico().replaceAll("'", "")%>','<%=posto.getCentroCusto()%>','<%=posto.getTelefone()%>','<%=posto.getEscalaPosto()%>','<%=posto.getSituacao()%>','<%=posto.getVagas()%>','<%=posto.getStatus().getDescricao()%>','<%=posto.getStatus().getCodigo()%>','<%=regional%>','<%=posto.getCodigoLocal() %>','<%=numPosto %>','<%=colabIncons %>','<%=posto.getColaboradores().size() %>')"><img src="imagens/custom/forward.png"></a><%}%>
								</td> 
							</tr>
							<%
							if (!posto.getColaboradores().isEmpty()) 
							{
							%>
							<tr>
								<td colspan="10" <%= diaAtual ? "style='padding-top: 10px;padding-bottom: 10px;background-color: #dee3f7'" : "style='padding-top: 10px;padding-bottom: 10px;background-color: #d1d1d1'" %>>
									<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0" width="100%">
										<tr style="cursor: auto">
											<th style="cursor: auto">Emp.</th>
											<th style="cursor: auto">Matr�cula</th>
											<th style="cursor: auto">Colaborador</th>
											<th style="cursor: auto">Cargo</th>
											<th style="cursor: auto">Escala</th>
											<th style="cursor: auto" width="242px">Hor�rios</th>
											<th style="cursor: auto" width="140px">Situa��o</th>
											<th style="cursor: auto" width="140px">Entrada</th>
											<th style="cursor: auto" width="140px">Sa�da</th>
											<th style="cursor: auto" width="20px">Alertas</th>
											<th style="cursor: auto" width="90px" title="Opera��es">Op.</th>
											<th style="cursor: auto" width="40px">Status</th>
										</tr>
										<tbody>	
										<%
										for(ColaboradorVO colaborador : posto.getColaboradores()){
											
											String btnEscalaRevezamento = "<a style='cursor:\"pointer\"' onclick='javascript:showEscalaRevezamento(\"" + colaborador.getCodigoEscala() + "\", \"" + dataString + "\")'><img src='imagens/icones_final/clock_16x16.png' title='Visualizar Escala' /></a>";
											String btnEntrada = "<a title='For�ar entrada' class='easyui-tooltip' style='cursor:\"pointer\"' onclick='javascript:showForcarEvento(\"E\", \"P\", this, \"" + colaborador.getNumeroCracha() + "\", \"" + colaborador.getNumeroEmpresa() + "\", \"" + colaborador.getTipoColaborador() + "\", \"" + colaborador.getNumeroCadastro() + "\", \"" + posto.getNumeroLocal() + "\", \"" + posto.getCodigoOrganograma() + "\")'><img src='imagens/custom/arrow_in.png' /></a>";
											String btnSaida = "<a title='For�ar sa�da' class='easyui-tooltip' style='cursor:\"pointer\"' onclick='javascript:showForcarEvento(\"S\", \"P\", this, \"" + colaborador.getNumeroCracha() + "\", \"" + colaborador.getNumeroEmpresa() + "\", \"" + colaborador.getTipoColaborador() + "\", \"" + colaborador.getNumeroCadastro() + "\", \"" + posto.getNumeroLocal() + "\", \"" + posto.getCodigoOrganograma() + "\")'><img src='imagens/custom/arrow_out.png' align='middle'  /></a>";
											String btnAfastamento = "<a title='Inserir Afastamento' class='easyui-tooltip' style='cursor:\"pointer\"' onclick='javascript:showInserirAfastamento(this, \"" + colaborador.getNumeroEmpresa() + "\", \"" + colaborador.getTipoColaborador() + "\", \"" + colaborador.getNumeroCadastro() + "\", \"" + colaborador.getCodigoEscala() + "\", \"" + colaborador.getTurmaEscala() + "\",\"0\")'><img src='imagens/custom/papel_16x16-offline.png' /></a>";
											String btnErroAcesso = "<a title='Visualizar erros de acesso' class='easyui-tooltip' style='cursor:\"pointer\"' onclick='javascript:showErroAcesso(" + colaborador.getNumeroCracha() + ")'><img src='imagens/icones_final/sphere_error_16x16-trans.png' /></a>";
											String icoCm = "<a title='For�ada Manualmente' class='easyui-tooltip'><img src='imagens/custom/monitor_14x14.png' /></a> ";
											String icoPhone = "<a title='Via Telefone' class='easyui-tooltip'><img src='imagens/custom/phone_14x14.png' /></a>";
											String icoAutorizada = "<a title='Acesso Autorizado' class='easyui-tooltip'><img src='imagens/custom/cobertura_14x14.png' /></a>";
											String situacaoColaborador = "";
											String excecoes = "";
											
											if (isOperadorQlApuracao && colaborador.getQdeExcecao() > 0)
											{
												excecoes = colaborador.getQdeExcecao().toString();
											}
											
											String btnFichaColaborador = 				"<div class='iconCol' onclick='javascript:showFichaColaborador(" + colaborador.getNumeroEmpresa() + "," + colaborador.getTipoColaborador() + "," + colaborador.getNumeroCadastro() + ",\"" +dep+"\","+posto.getCentroCusto()+")'>";
											btnFichaColaborador = btnFichaColaborador + "	<div id='txt-" + colaborador.getNumeroEmpresa() + "-" + colaborador.getNumeroCadastro() + "' class='txtCol'>" + excecoes + "</div>";
											btnFichaColaborador = btnFichaColaborador + "</div>";
											
											String btnEvento = "";
											String txtEntrada = "&nbsp;";
											String txtSaida = "&nbsp;";
											String txtAlertas = "&nbsp;";
	
											if (colaborador.getPresenca().getEntrada() != null)
											{
												txtEntrada = NeoUtils.safeDateFormat(colaborador.getPresenca().getEntrada(), "dd/MM/yyyy HH:mm");
												
												if (colaborador.getPresenca().getColetorEntrada() == 2L)
												{
													txtEntrada += icoCm;
												}
												else
												{
													txtEntrada += icoPhone;
												}
												if (colaborador.getPresenca().getTipoDirecao() != null && (colaborador.getPresenca().getTipoDirecao().equals("A") || colaborador.getPresenca().getTipoDirecao().equals("L"))) 
												{
													txtEntrada += icoAutorizada;
												}
											}
											else
											{
												btnEvento = btnEntrada;
											}
											if (colaborador.getPresenca().getSaida() != null)
											{
												btnEvento = btnEntrada;
												txtSaida = NeoUtils.safeDateFormat(colaborador.getPresenca().getSaida(), "dd/MM/yyyy HH:mm");
												
												if (colaborador.getPresenca().getColetorSaida() == 2L)
												{
													txtSaida += icoCm;
												}
												else
												{
													txtSaida += icoPhone;
												}
												if (colaborador.getPresenca().getTipoDirecao() != null && (colaborador.getPresenca().getTipoDirecao().equals("A") || colaborador.getPresenca().getTipoDirecao().equals("L"))) 
												{
													txtSaida += icoAutorizada;
												}
											}
											else if (colaborador.getPresenca().getEntrada() != null)
											{
												btnEvento = btnSaida;
											}
											
											if (colaborador.getDataDemissao() != null)
											{
												situacaoColaborador = "<img src='imagens/custom/papel_16x16-warn.png' title='" + colaborador.getObsSituacao() + "'/>" + colaborador.getSituacao();
											}
	// 										else if (colaborador.getAvosFerias() != null && colaborador.getAvosFerias() >= 22L)
	// 										{
	// 											situacaoColaborador = "<img src='imagens/icones_final/holiday_16x16.png' title='" + colaborador.getAvosFerias() + " avos de f�rias em aberto'/> &nbsp" + colaborador.getSituacao();
	// 										}
											else
											{
												situacaoColaborador = colaborador.getSituacao();
											}
											
											List<ValeTransporteVO> vts = QLPresencaUtils.listaValeTransporte(colaborador.getNumeroEmpresa(), colaborador.getTipoColaborador(), colaborador.getNumeroCadastro());
											String img = "alerta_vt_nada";
											String tooltip = " n�o possui VTs ";
											if (vts != null && vts.size() >= 1){
												switch (vts.size()){
													case 0:	img = "alerta_vt_nada";
															tooltip = " n�o possui VTs ";
															break;
													case 1: img = "alerta_vt_amarelo";
															tooltip = " possui 1 VT ";
															break;
													case 2: img = "alerta_vt_vermelho";
															tooltip = " possui 2 VTs ";
															break;
													default: img = "alerta_vt_vermelho";
															  tooltip = " possui 2 ou mais VTs ";
															break;
												}
												txtAlertas = "<img src='imagens/custom/"+img+".png' title='Colaborador"+tooltip+"' style='width:16px; height:16px;' />";
											}
											vts.clear();
											vts = null;
												
											%>
										<tr id="col-<%= posto.getCentroCusto() %>-<%= colaborador.getNumeroEmpresa() %>-<%= colaborador.getNumeroCadastro() %>" <%= colaborador.getPresenca().getStatus().getCodigo().equals("CVA") ? "style='background-color: #DDDDDD;'" : "" %>>
											<td><%= colaborador.getNumeroEmpresa() %></td>
											<td><%= colaborador.getNumeroCadastro() %></td>
											<td><%= btnFichaColaborador + "&nbsp;" %>
												<% if (!isPresencaSupervisao || isVisualizarLog){ %>
												<a class="easyui-tooltip" 
												<% if (isLogQl) {%>
													href="javascript:saveInputLog('<%=posto.getCodigoLocal() %>','<%=colaborador.getCpf() %>','colaborador')" 
												<% } %>	
													data-options="
											            content: $('<div></div>'),
											            position: 'right',
											            hideDelay: '400',
											            showDelay: '400',
											            onUpdate: function(cc){
											                cc.panel({
											                    width: 600,
											                    height: 500,
											                    title: 'LOG DO COLABORADOR',
											                    href: '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/presenca/logColaborador.jsp?numcpf=<%= colaborador.getCpf().toString()%>'
											                });
											            },
											            onShow: function(){
										                    var t = $(this);
										                    t.tooltip('tip').unbind().bind('mouseenter', function(){
										                        t.tooltip('show');
										                    }).bind('mouseleave', function(){
										                        t.tooltip('hide');
										                    });
										                },
										                onPosition: function(){
										                    $(this).tooltip('tip').css('top', $(this).offset().top);
										                    $(this).tooltip('arrow').css('top', 20);
										                }
											        "><img src='imagens/icones_final/document_v_16x16-trans.png' style='cursor: pointer;'/></a>
											    <% } %>    
												<%=	colaborador.getNomeColaborador() %></td>
											<td><%= colaborador.getCargo() %></td>
											<td><%= btnEscalaRevezamento + "&nbsp" + colaborador.getEscala().getDescricao() %></td>
											<td width="242px"><%= colaborador.getEscala().getDescricaoHorarios() %></td> 
											<td width="140px"><span title="<%= colaborador.getObsSituacao() %>"><%= situacaoColaborador %></span></td>
											<td width="140px"><%= txtEntrada %></td>
											<td width="140px"><%= txtSaida %></td>
											<td width="20px"><%= txtAlertas %></td>
											<td width="90px"><%= isOperadorQl ? btnEvento + "&nbsp" + btnAfastamento + "&nbsp" + btnErroAcesso : "&nbsp" %></td>
											<td width="40px"><%= colaborador.getPresenca().getStatus().getImageTag() %></td>
										</tr>
										<%
										}
										%>
										</tbody>
										</table>
								</td>
							</tr>
							<%
							}
							
							if (!posto.getEntradasAutorizadas().isEmpty()) 
							{
							%>
								<tr>
									<td colspan="9" <%= diaAtual ? "style='padding-top: 10px;padding-bottom: 10px;background-color: #dee3f7'" : "style='padding-top: 10px;padding-bottom: 10px;background-color: #d1d1d1'" %>>
										<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0">
										<tr style="cursor: auto" style="background-color: #F0E68C;">
			   							    <th style="cursor: auto">Cobertura</th>
											<th style="cursor: auto">Emp.</th>
											<th style="cursor: auto">Matr�cula</th>
											<th style="cursor: auto">Colaborador</th>
											<th style="cursor: auto">Cargo</th>
											<th style="cursor: auto">Substituindo</th>
											<th style="cursor: auto">Per�odo</th>
											<th style="cursor: auto">Escala Autorizada</th>
											<th style="cursor: auto">Hor�rios Autorizados</th>
											<th style="cursor: auto" width="150px">Entrada</th>
											<th style="cursor: auto" width="150px">Sa�da</th>
											<th style="cursor: auto" width="90px" title="Opera��es"">Op.</th>
											<th style="cursor: auto" width="40px">Status</th>
										</tr>
										<tbody>	
										<%
										for(EntradaAutorizadaVO entradaAutorizada : posto.getEntradasAutorizadas()){
											
											SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
											String dataInicial = sf.format(entradaAutorizada.getDataInicial().getTime());
											
											String btnEntrada = "<a style='cursor:\"pointer\"' onclick='javscript:showForcarEvento(\"E\", \"A\", this, \"" + entradaAutorizada.getColaborador().getNumeroCracha() + "\", \"" + entradaAutorizada.getColaborador().getNumeroEmpresa() + "\", \"" + entradaAutorizada.getColaborador().getTipoColaborador() + "\", \"" + entradaAutorizada.getColaborador().getNumeroCadastro() + "\", \"" + posto.getNumeroLocal() + "\", \"" + posto.getCodigoOrganograma() + "\")'><img src='imagens/custom/arrow_in.png' title='For�ar entrada' /></a>";
											String btnSaida = "<a style='cursor:\"pointer\"' onclick='javscript:showForcarEvento(\"S\", \"A\", this, \"" + entradaAutorizada.getColaborador().getNumeroCracha() + "\", \"" + entradaAutorizada.getColaborador().getNumeroEmpresa() + "\", \"" + entradaAutorizada.getColaborador().getTipoColaborador() + "\", \"" + entradaAutorizada.getColaborador().getNumeroCadastro() + "\", \"" + posto.getNumeroLocal() + "\", \"" + posto.getCodigoOrganograma() + "\")'><img src='imagens/custom/arrow_out.png' align='middle' title='For�ar sa�da' /></a>";
											String btnAfastamento = "<a style='cursor:\"pointer\"' onclick='javscript:showInserirAfastamento(this, \"" + entradaAutorizada.getColaborador().getNumeroEmpresa() + "\", \"" + entradaAutorizada.getColaborador().getTipoColaborador() + "\", \"" + entradaAutorizada.getColaborador().getNumeroCadastro() + "\", \"0\", \"0\" , \"" + entradaAutorizada.getHoraInicial() + "\")'><img src='imagens/custom/papel_16x16-offline.png' title='Inserir Afastamento' /></a>";
											String icoCm = "<img src='imagens/custom/monitor_14x14.png' title='For�ada Manualmente' />";
											String icoPhone = "<img src='imagens/custom/phone_14x14.png' title='Via SNEP' />";
											String btnEvento = "";
											String txtEntrada = "&nbsp;";
											String txtSaida = "&nbsp;";
											String btnFichaColaborador = "<a title='Ficha do Colaborador' style='cursor:pointer' onclick='javascript:showFichaColaborador(" + entradaAutorizada.getColaborador().getNumeroEmpresa() + "," + entradaAutorizada.getColaborador().getTipoColaborador() + "," + entradaAutorizada.getColaborador().getNumeroCadastro() + ",\"" +dep+"\","+posto.getCentroCusto()+")'><img src='imagens/custom/papel_16x16.png' /></a>";
											String btnFichaColaboradorSub = "";
											String corCobertura = "";
											if (entradaAutorizada.getColaboradorSubstituido() != null) 
											{
												btnFichaColaboradorSub = "<a title='Ficha do Colaborador' style='cursor:pointer' onclick='javascript:showFichaColaborador(" + entradaAutorizada.getColaboradorSubstituido().getNumeroEmpresa() + "," + entradaAutorizada.getColaboradorSubstituido().getTipoColaborador() + "," + entradaAutorizada.getColaboradorSubstituido().getNumeroCadastro() + ",\"" +dep+"\","+posto.getCentroCusto()+")'><img src='imagens/custom/papel_16x16.png' /></a>";
											}
											
											String btnRelatorio = "";
								            if(entradaAutorizada.getDescricaoCobertura().equals("F�rias"))
								            {
								            	btnRelatorio = "<a target='_blank' href='servlet/com.neomind.fusion.custom.orsegups.servlets.RelatoriosServlet?action=" +
								            		RelatorioActionsEnum.COBERTURA_FERIAS.getDescricao()+"&numemp="+entradaAutorizada.getColaborador().getNumeroEmpresa() + "&tipcol=" +
								            		entradaAutorizada.getColaborador().getTipoColaborador() + "&numcad="+entradaAutorizada.getColaborador().getNumeroCadastro() +
								            		"&datalt="+NeoUtils.safeDateFormat(entradaAutorizada.getDataInicial(),"dd/MM/yyyy") +
								            		"'><img src='imagens/custom/report_pdf_16x16.png' align='middle' title='Imprimir comunicado de F�rias' /></a>";
								            }
											
											if (entradaAutorizada.getColaborador().getPresenca().getEntrada() != null)
											{
												txtEntrada = NeoUtils.safeDateFormat(entradaAutorizada.getColaborador().getPresenca().getEntrada(), "dd/MM/yyyy HH:mm");
												
												if (entradaAutorizada.getColaborador().getPresenca().getColetorEntrada() == 2L)
												{
													txtEntrada += icoCm;
												}
												else
												{
													txtEntrada += icoPhone;
												}
											}
											else
											{
												btnEvento = btnEntrada;
											}
											if (entradaAutorizada.getColaborador().getPresenca().getSaida() != null)
											{
												btnEvento = btnEntrada;
												txtSaida = NeoUtils.safeDateFormat(entradaAutorizada.getColaborador().getPresenca().getSaida(), "dd/MM/yyyy HH:mm");
												
												if (entradaAutorizada.getColaborador().getPresenca().getColetorSaida() == 2L)
												{
													txtSaida += icoCm;
												}
												else
												{
													txtSaida += icoPhone;
												}
											}
											else if (entradaAutorizada.getColaborador().getPresenca().getEntrada() != null)
											{
												btnEvento = btnSaida;
											}
											
											if (entradaAutorizada.getDescricaoCobertura().equals("QL - Invers�o de Escala"))
											{
												if (entradaAutorizada.getColaborador().getEscala().getHorarios().size() > 0)
												{
													corCobertura = " style='background-color: #b4ebab;' ";
												}
												else
												{
													corCobertura = " style='background-color: #dbf5d6;' ";
												}
											}
											else
											{
												if (entradaAutorizada.getColaborador().getEscala().getHorarios().size() > 0)
												{
													corCobertura = " style='background-color: #e2e61c;' ";
												}
												else
												{
													corCobertura = " style='background-color: #EEEECC;' ";
												}
											}

										%>
											<tr<%= corCobertura %>>
											    <td><%= isOperadorQl ? entradaAutorizada.getBtnDelete() + "&nbsp" + entradaAutorizada.getDescricaoCobertura() : entradaAutorizada.getDescricaoCobertura()%></td>	
												<td><%= entradaAutorizada.getColaborador().getNumeroEmpresa() %></td>
												<td><%= entradaAutorizada.getColaborador().getNumeroCadastro() %></td>
												<td><%= btnFichaColaborador + "&nbsp"%>
												<% if (!isPresencaSupervisao || isVisualizarLog){ %>
													<a class="easyui-tooltip" 
													<% if (isLogQl) {%>
														href="javascript:saveInputLog('<%=posto.getCodigoLocal() %>','<%=entradaAutorizada.getColaborador().getCpf() %>','colaborador')" 
													<% } %>
														data-options="
											            content: $('<div></div>'),
											            position: 'right',
											            hideDelay: '400',
											            showDelay: '400',
											            onUpdate: function(cc){
											                cc.panel({
											                    width: 600,
											                    height: 500,
											                    title: 'LOG DO COLABORADOR',
											                    href: '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/presenca/logColaborador.jsp?numcpf=<%= entradaAutorizada.getColaborador().getCpf().toString()%>'
											                });
											            },
											            onShow: function(){
										                    var t = $(this);
										                    t.tooltip('tip').unbind().bind('mouseenter', function(){
										                        t.tooltip('show');
										                    }).bind('mouseleave', function(){
										                        t.tooltip('hide');
										                    });
										                },
										                onPosition: function(){
										                    $(this).tooltip('tip').css('top', $(this).offset().top);
										                    $(this).tooltip('arrow').css('top', 20);
										                }
											        "><img src='imagens/icones_final/document_v_16x16-trans.png' style='cursor: pointer;'/></a>
											        <% } %>
													<%= entradaAutorizada.getColaborador().getNomeColaborador() + " - " +entradaAutorizada.getColaborador().getSituacao()%></td>
												<td><%= entradaAutorizada.getColaborador().getCargo() %></td>
												<td><%
													if (entradaAutorizada.getColaboradorSubstituido() != null && entradaAutorizada.getColaboradorSubstituido().getCpf() != null)
													{
													%>
														<%= btnFichaColaboradorSub + "&nbsp" %>
														<% if (!isPresencaSupervisao || isVisualizarLog){ %>
														<a class="easyui-tooltip" 
														<% if (isLogQl) {%>
															href="javascript:saveInputLog('<%=posto.getCodigoLocal() %>','<%=entradaAutorizada.getColaboradorSubstituido().getCpf() %>','colaborador')" 
														<% } %>
															data-options="
												            content: $('<div></div>'),
												            position: 'right',
												            hideDelay: '400',
												            showDelay: '400',
												            onUpdate: function(cc){
												                cc.panel({
												                    width: 600,
												                    height: 500,
												                    title: 'LOG DO COLABORADOR',
												                    href: '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/presenca/logColaborador.jsp?numcpf=<%= entradaAutorizada.getColaboradorSubstituido().getCpf().toString()%>'
												                });
												            },
												            onShow: function(){
											                    var t = $(this);
											                    t.tooltip('tip').unbind().bind('mouseenter', function(){
											                        t.tooltip('show');
											                    }).bind('mouseleave', function(){
											                        t.tooltip('hide');
											                    });
											                },
											                onPosition: function(){
											                    $(this).tooltip('tip').css('top', $(this).offset().top);
											                    $(this).tooltip('arrow').css('top', 20);
											                }
												        "><img src='imagens/icones_final/document_v_16x16-trans.png' style='cursor: pointer;'/></a>
												        <% } %>
												        <%= entradaAutorizada.getColaboradorSubstituido().getNomeColaborador() %>
													<%	
													}
													%>
												</td>
												<td><%= NeoUtils.safeDateFormat(entradaAutorizada.getDataInicial(), "dd/MM/yyyy") %> a <%= NeoUtils.safeDateFormat(entradaAutorizada.getDataFinal(), "dd/MM/yyyy") %></td>
												<td><%= entradaAutorizada.getColaborador().getEscala().getDescricao() %></td>
												<td><%= entradaAutorizada.getColaborador().getEscala().getDescricaoHorarios() %></td>
												<td><%= txtEntrada %></td>
												<td><%= txtSaida %></td>
												<td><%= isOperadorQl ? btnEvento + "&nbsp" + btnAfastamento + "&nbsp" + btnRelatorio : btnRelatorio + "&nbsp" %></td>
												<td><%= entradaAutorizada.getColaborador().getPresenca().getStatus().getImageTag() %> </td>
											</tr>
										<%
										}
										%>
										</tbody>
										</table>
										
									</td>
								</tr>					
						<%
							}
					}
					%>
				</tbody>
				</table>
				</fieldset>
				<br/>
	<%
			}
		}
	%>
		<div class="easyui-window" title="For�ar Evento" closed="true" style="min-width: 300px; min-height: 160px; background-color: #DDDDDD;" id="divForcarEvento">
			<form>
				<br><span id="tituloSaida"><b>&nbsp;FOR�AR SA�DA EM</b></span><span id="tituloEntrada" style="display: none;"><b>&nbsp;FOR�AR ENTRADA EM</b></span><br><br>
				&nbsp;<input class="input_text" onfocus="activeDeactiveObj(this)" value="<%=df.format(dataAtual.getTime())%>" onblur="activeDeactiveObj(this)" name= 'datacc' errorSpan='err_var_dataEvento' id='datacc' required='true' onchange='' style="width: 65px; height: 20px; margin-right: 0px"/>
				<span id='err_var_dataInicialDe' style='margin-left:0px;'></span>&nbsp;
				<select name="horacc" id="horacc" style="width: 40px; height: 20px;">
				<%
					int i = 0;
					while (i < 24) {
				%>
					<option value="<%=i %>"<%= i == dataAtual.get(Calendar.HOUR_OF_DAY) ? " selected='selected'" : "" %>><%=i %></option>
				<%
						i++;
					}
				%>
				</select>&nbsp;:
				<select name="minacc" id="minacc" style="width: 40px; height: 20px;">
				<%
					i = 0;
					while (i < 60) {
				%>
					<option value="<%=i %>"<%= i == dataAtual.get(Calendar.MINUTE) ? " selected='selected'" : "" %>><%=i %></option>
				<%
						i++;
					}
				%>
				</select>
				<br/>
				<div style="padding: 3px">
					<select name="codjma" id="codjma" style="width: 264px; height: 20px;">
						<option value="">Selecione a justificativa</option>
					<%
						for (JustificativaMarcacaoVO jus : justificativas)
						{
					%>
						<option value="<%=jus.getCodJma() %>"><%=jus.getDesJma() %></option>
					<%		
						}
					%>
					</select>
				</div>
				<div style="padding: 3px">
					<textarea name="obsjma" id="obsjma" class="input_text" maxlength="255" type="text" rows="2" style="width: 264px" placeholder="Observa��o" required></textarea>
				</div>	
				<input type="hidden" name="diracc" id="diracc"><input type="hidden" name="tipdir" id="tipdir"><input type="hidden" name="numcra" id="numcra"><input type="hidden" name="numloc" id="numloc"><input type="hidden" name="taborg" id="taborg">
				<input type="button"class="input_button"id="consulta"value="Aplicar" style="width: 55px" onclick="javascript:forcarEvento()">
				<input type="button" class="input_button" id="consulta" value="Fechar" style="margin-right: -10px; width: 55px" onclick="javascript:jQuery('#divForcarEvento').window('close');">
				<br/>
				<span class="fieldHelp" style="margin-left: 0px; margin-right: 0px;padding-left: 3px">Utilize o formato DD/MM/AAAA hh:mm</span><br><br>
			</form>
		</div>
		
		<div class="easyui-window" title="Inserir Afastamento" closed="true" style="width: 260px; height: 145px; background-color: #DDDDDD;" id="divInserirAfastamento">
			<form>
				<br><span id="tituloSaidaAfa"><b>&nbsp;INSERIR AFASTAMENTO EM:</b></span><br><br>
				&nbsp;<input class="input_text" onfocus="activeDeactiveObj(this)" value="<%=dfData.format(dataAtual.getTime())%>" onblur="activeDeactiveObj(this)" name= 'datafa' errorSpan='err_var_dataEvento' id='datafa' required='true' onchange='' style="width: 65px; height: 20px; margin-right: 0px"/>
				<span id='err_var_dataInicialDe' style='margin-left:0px;'></span>&nbsp;
				<input type="hidden" name="numemp" id="numemp"><input type="hidden" name="tipcol" id="tipcol"><input type="hidden" name="numcad" id="numcad"><input type="hidden" name="codesc" id="codesc"><input type="hidden" name="codtma" id="codtma"><input type="hidden" name="horini" id="horini">
				<input type="button"class="input_button"id="aplicar"value="Aplicar" style="width: 55px" onclick="javascript:inserirAfastamento()">
				<input type="button" class="input_button" id="fechar" value="Fechar" style="margin-right: -10px; width: 55px" onclick="javascript:jQuery('#divInserirAfastamento').window('close');"><br>
				<span class="fieldHelp" style="margin-left: 0px;; margin-right: 0px;">Utilize o formato DD/MM/AAAA</span><br><br>
			</form>
		</div>
		
		<div class="easyui-window" title="Controle de ASO" closed="true" style="min-width: 225px; min-height: 110px; background-color: #DDDDDD; padding-left: 10px;" id="divControlaAso">
			<form>
				<input type="hidden" id="hdn_numposto" name="hdn_numposto" />
				<input type="hidden" id="hdn_numctr" name="hdn_numctr" />
				<input type="hidden" id="hdn_numemp" name="hdn_numemp" />
				<input type="hidden" id="hdn_numfil" name="hdn_numfil" />
				<input type="hidden" id="hdn_codser" name="hdn_codser" />
				<br>
				<span id="tituloSaidaCTRASO"><b>&nbsp;Controlar ASO no Contrato <span id="spn_ctr"></span>? </b></span> 
				<br><br>
				<select name="cmb_ctraso" id="cmb_ctraso" style="width: 100px; height: 20px;margin-left: 4px;">
					<option value="0" >N&atilde;o</option>
					<option value="1" selected='selected'>Sim</option>
				</select>
				<br/><br>
				<input type="button" class="input_button" id="aplica_aso" value="Aplicar" style="width: 55px" onclick="javascript:controlarASO();">
				<input type="button" class="input_button" id="fecha_aso" value="Fechar" style="margin-right: -10px; width: 55px" onclick="javascript:jQuery('#divControlaAso').window('close');">
			</form>
		</div>
		
		<div style="width: 100%;height: 500px"></div>
	</cw:body>
</cw:main>
</portal:head>

<script language="JavaScript">

function doAction(acao, empresa, matricula) {

	var response = "";
	
	if (confirm("Deseja realizar a SA�DA MANUAL do colaborador?")) {
		response = callSync('<%= PortalUtil.getBaseURL() %>custom/jsp/orsegups/presenca/presencaViaSNEP.jsp?verboseError=yes&fone=fusion&acao=' + acao + '&numEmp='+ empresa + '&numCad='+ matricula);

		if (response.indexOf("OK") != -1) {
			alert("OK - A��o realizada com sucesso.");
		} else {
			alert("ERRO: " + response);
		}
	}
}


function unavaliable() {
	alert('Discagem autom�tica dispon�vel apenas para a Central de Monitoramento.');
}

function dial(src, dst) {
	var result = callSync('<%= PortalUtil.getBaseURL() %>servlet/com.neomind.fusion.custom.orsegups.servlets.RamalServlet?ramaOrigem='+src+'&ramalDestino='+dst);

	if (result.indexOf("Chamada efetuada") == -1) {
		alert("N�o foi poss�vel discar: " + response);
	}
}

function saveInputLog(idPosto, idColaborador, tipo)
{
	var label = "";
	if(tipo == "posto")
	{
		label = "Informar Log do Posto:";
	} 
	else if(tipo == "colaborador")
	{
		label = "Informar Log do Colaborador:";
	}
	
	texto = prompt(label,"");
	
	if(texto != null && texto != "")
	{
		var	result = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet?action=saveLog&tipo="+tipo+"&idPosto="+idPosto+"&idColaborador="+idColaborador+"&texto="+JSON.stringify(texto));
		
		alert(result);
	}
}

function deleteRegistroCobertura(numEmp, tipCol, numCad, dataInicial, horaInicial, numEmpCob, tipColCob, numCadCob)
{
	var isDelete = confirm("Deseja excluir o registro de cobertura?");

	if(isDelete)
	{
		var	result = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet?action=deleteRegistroCobertura&numEmp="+numEmp+"&tipCol="+tipCol+"&numCad="+numCad+"&dataInicial="+dataInicial+"&horaInicial="+horaInicial+"&numEmpCob="+numEmpCob+"&tipColCob="+tipColCob+"&numCadCob="+numCadCob);
		if(result == 0){
		  alert('N�o foi poss�vel excluir o registro de cobertura.');
		}
		if(result > 0){
		  alert('Registro exclu�do com sucesso.');
		  document.location.reload();
		}
	}
}

function clickBtn(bt)
{
	if (document.getElementById(bt).checked) {
		document.getElementById("mostrar" + bt).style.fontWeight = 'bold';
	} else {
		document.getElementById("mostrar" + bt).style.fontWeight = '';
	}
}

function showEscalaRevezamento(codesc,data) {
	var caminho = '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/presenca/consultaEscalaRevezamento.jsp?codesc='+codesc+'&data='+data;

	$('#modalEscalaRevezamento').window('open');
	$('#modalEscalaRevezamento').window('refresh', caminho);
}

function showEscalaPosto(numloc,taborg) {
	var caminho = '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/presenca/consultaEscalaPosto.jsp?numloc='+numloc+'&taborg='+taborg;

	$('#modalEscalaPosto').window('open');
	$('#modalEscalaPosto').window('refresh', caminho);
}

function showErroAcesso(numcra) {
	
	var caminho = '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/presenca/consultaErrosAcesso.jsp?numcra='+numcra;

	$('#modalErrosAcesso').window('open');
	$('#modalErrosAcesso').window('refresh', caminho);
}

function showFichaColaborador(numemp, tipcol, numcad, dep, codccu) {
	var caminho = '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/presenca/fichaColaborador.jsp?numemp='+numemp+'&tipcol='+tipcol+'&numcad='+numcad+'&dep='+dep+'&codccu='+codccu;

	$('#modalFichaColaborador').window('open');
	$('#modalFichaColaborador').window('refresh', caminho);
}

function showFichaPosto(numloc, taborg, raizQL) {
	var caminho = '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/presenca/fichaPosto.jsp?numloc='+numloc+'&taborg='+taborg+'&timeMillisDataAtual='+ <%= timeMillisDataAtual %>+ '&raizQL='+raizQL;

	$('#modalFichaPosto').window('open');
	$('#modalFichaPosto').window('refresh', caminho);
}

function showTelefones(codccu) {
	var caminho = '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/presenca/consultaTelefones.jsp?codccu='+codccu;

	$('#modalTelefones').window('open');
	$('#modalTelefones').window('refresh', caminho);
}

function showEventosSigma(cdCliente, conta, fantasia) {
	var caminho = '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/presenca/contaSigma.jsp?cdCliente='+cdCliente+'&conta='+conta+'&fantasia='+fantasia;

	$('#modalContaSigma').window('open');
	$('#modalContaSigma').window('refresh', caminho);
}

function showVisita(neoid) {
	var caminho = '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/presenca/visita.jsp?neoid='+neoid;

	$('#modalVisita').window('open');
	$('#modalVisita').window('refresh', caminho);
}

function showForcarEvento(es, tipdir, obj, numcra, numemp, tipcol, numcad, numloc, taborg)
{
	if (es == "E")
	{
		document.getElementById("tituloEntrada").style.display = "";
		document.getElementById("tituloSaida").style.display = "none";
		document.getElementById("diracc").value = "E";
	}
	else
	{
		document.getElementById("tituloEntrada").style.display = "none";
		document.getElementById("tituloSaida").style.display = "";
		document.getElementById("diracc").value = "S";
	}
	document.getElementById("numcra").value = numcra;
	document.getElementById("numemp").value = numemp;
	document.getElementById("tipcol").value = tipcol;
	document.getElementById("numcad").value = numcad;
	document.getElementById("numloc").value = numloc;
	document.getElementById("taborg").value = taborg;
	document.getElementById("tipdir").value = tipdir;	
	document.getElementById("codjma").value = '';	
	document.getElementById("obsjma").value = '';	

	var curtop = 0;
	if (jQuery.browser.msie)
	{
		if(obj.offsetParent)
		{
			while(1)
			{
				curtop += obj.offsetTop;
				if(!obj.offsetParent)
					break;
				obj = obj.offsetParent;
			}
		}
		else if(obj.y)
		{
			curtop += obj.y;
		}
	}
	else
	{
		curtop = event.clientY;
	}
	jQuery("#divForcarEvento").window('open')
	//document.getElementById("divForcarEvento").style.display="";
	//document.getElementById("divForcarEvento").style.left = event.clientX - 300;
	//document.getElementById("divForcarEvento").style.top = curtop - 90;
}

function forcarEvento() {
	
	var obs = document.getElementById('obsjma').value.trim();
	if (obs.length > 0){
		var isDelete = confirm("Deseja incluir o registro?");
	
		if(isDelete)
		{
			var	result = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet?action=insertEventoForcado&numcra="+document.getElementById('numcra').value+"&numemp="+document.getElementById('numemp').value+"&tipcol="+document.getElementById('tipcol').value+"&numcad="+document.getElementById('numcad').value+"&numloc="+document.getElementById('numloc').value+"&taborg="+document.getElementById('taborg').value+"&diracc="+document.getElementById('diracc').value+"&datacc="+document.getElementById('datacc').value+"&horacc="+document.getElementById('horacc').value+"&minacc="+document.getElementById('minacc').value+"&tipdir="+document.getElementById('tipdir').value+"&codjma="+document.getElementById('codjma').value+"&obsjma="+document.getElementById('obsjma').value);
			
			if(result == -1){
			  alert('Hor�rio j� registrado.');
			}
			if(result == 0){
			  alert('N�o foi poss�vel incluir o registro.');
			}
			if(result > 0){
			  alert('Registro inclu�do com sucesso.');
			}
			
			//document.getElementById('divForcarEvento').style.display='none';
			jQuery("#divForcarEvento").window('close');
		}
	}else{
		alert("Informe a observa��o!");
	}
	
}

function showInserirAfastamento(obj, numemp, tipcol, numcad, codesc, codtma, horini)
{
	document.getElementById("numemp").value = numemp;
	document.getElementById("tipcol").value = tipcol;
	document.getElementById("numcad").value = numcad;
	document.getElementById("codesc").value = codesc;
	document.getElementById("codtma").value = codtma;
	document.getElementById("horini").value = horini;

	var curtop = 0;
	if (jQuery.browser.msie)
	{
		if(obj.offsetParent)
		{
			while(1)
			{
				curtop += obj.offsetTop;
				if(!obj.offsetParent)
					break;
				obj = obj.offsetParent;
			}
		}
		else if(obj.y)
		{
			curtop += obj.y;
		}
	}
	else
	{
		curtop = event.clientY;
	}
	/* document.getElementById("divInserirAfastamento").style.display="";
	document.getElementById("divInserirAfastamento").style.left = event.clientX - 300;
	document.getElementById("divInserirAfastamento").style.top = curtop - 90; */
	jQuery('#divInserirAfastamento').window('open');
}

function inserirAfastamento() {
	var isDelete = confirm("Deseja incluir o registro?");

	if(isDelete)
	{
		var	result = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet?action=insertSituacaoIndefinida&numemp="+document.getElementById('numemp').value+"&tipcol="+document.getElementById('tipcol').value+"&numcad="+document.getElementById('numcad').value+"&datafa="+document.getElementById('datafa').value+"&codesc="+document.getElementById('codesc').value+"&codtma="+document.getElementById('codtma').value+"&horini="+document.getElementById('horini').value);
		
		if(result == 0){
		  	alert('N�o foi poss�vel incluir o registro.');
		} 
		else if(result == 100)
		{
			alert('O colaborador j� possui afastamento nesta data.');
		} 
		else if(result == 1000)
		{
			alert('Data inferior a data de Admiss�o.');
		}
		else
		{
			alert('Registro inclu�do com sucesso.');
		}
		
		jQuery('#divInserirAfastamento').window('close');
		
		//document.getElementById('divInserirAfastamento').style.display='none';
	}
}

function limpaData()
{
	var data = new Date();
	var dia = data.getDate();
	var mes = data.getMonth() + 1;
	var ano = data.getFullYear();
	var hora = data.getHours();
	var min = data.getMinutes();
	
	if (data.getDate() <= 9) {
		dia = "0" + dia;
	}
	
	if (data.getMonth() <= 8) {
		mes = "0" + mes;
	}
	
	if (data.getHours() <= 9) {
		hora = "0" + hora;
	}
	
	if (data.getMinutes() <= 9) {
		min = "0" + min;
	}
	
	var dataNova = dia + "/" + mes + "/" + ano + " " + hora + ":" + min;
	document.getElementById('dataString').value = dataNova;
}

function postoInconsistente(nomePosto, lotacao, cidade, servico, centroDeCusto, telefone, escalaPosto, situacao, vagas, status, codigoStatus, reg, codigoLocal, postoIncons, colabIncons, efetivo){
	
	$.messager.prompt('Envia e-mail posto inconsist�nte', 'Informe o e-mail do destinat�rio.', function(r){
		var usuario = r.substring(0, r.indexOf("@")); 
		var dominio = r.substring(r.indexOf("@")+ 1, r.length); 
		if ((usuario.length >=1) && (dominio.length >=3) && 
			(usuario.search("@")==-1) && (dominio.search("@")==-1) && 
			(usuario.search(" ")==-1) && (dominio.search(" ")==-1) && (dominio.search(".")!=-1) && 
			(dominio.indexOf(".") >=1)&& (dominio.lastIndexOf(".") < dominio.length - 1)) {
		if (r){
			var posto = nomePosto +";"+ lotacao +";"+ cidade + ";" + servico + ";" + centroDeCusto + ";" + telefone + ";" + escalaPosto + ";" + situacao + ";" + vagas + ";" +status +" "+codigoStatus+";"+reg+";"+codigoLocal+";"+postoIncons+";"+colabIncons+";"+efetivo;
		 	$.ajax({
		     	   method: 'post',
		     	   url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet?action=enviaEmailPostoInconsistente&posto='+posto+'&emailTo='+r+'&isAsseio='+<%=parAsseio%>,
		           success: function(result){
		              alert('Sucesso ao enviar e-mail.');     
		           },  
		       	   error: function(e){  
		       		  alert('Erro ao enviar e-mail.');  
		      	   }
		           });
	         }
		}else{
			alert("E-mail invalido"); 	
		}
     	});
}


function validacaoEmail(field) { 
	usuario = field.substring(0, field.indexOf("@")); 
	dominio = field.substring(field.indexOf("@")+ 1, field.length); 
	if ((usuario.length >=1) && (dominio.length >=3) && 
		(usuario.search("@")==-1) && (dominio.search("@")==-1) && 
		(usuario.search(" ")==-1) && (dominio.search(" ")==-1) && (dominio.search(".")!=-1) && 
		(dominio.indexOf(".") >=1)&& (dominio.lastIndexOf(".") < dominio.length - 1)) {
		document.getElementById("msgemail").innerHTML="E-mail v�lido"; 
		alert("E-mail valido");
		} else{ 
		alert("E-mail invalido"); 
		} 
}



function validaData()  
{  
	var valid = true;
	var bissexto = 0;
	var data = document.getElementById('dataString').value;
	//var tam = data.length;
	var regex = new RegExp("^([0-9]{2})/([0-9]{2})/([0-9]{4}) ([0-9]{2}):([0-9]{2})$");  
    var mat = regex.exec(data);  

    if (mat == null)  
	{
    	alert("A Data "+data+" � inv�lida!");
		return false;
	}	
	
	var dia = data.substr(0,2);
	var mes = data.substr(3,2);
	var ano = data.substr(6,4);
	var hora = data.substr(11,2);
	var min = data.substr(14,2);

	if ((ano <= 1900)||(ano > 2100))
	{
		alert("A Data "+data+" � inv�lida!");
		return false;
	}
	
	if ((mes < 1) || (mes > 12))
	{
		alert("A Data "+data+" � inv�lida!");
		return false;
	}
	
	switch (mes) 
	{
		case '01':
		case '03':
		case '05':
		case '07':
		case '08':
		case '10':
		case '12':
			if  ((dia < 1) || (dia > 31)) 
			{
				alert("A Data "+data+" � inv�lida!");
				return false;
			}
			break;
		
		case '04':		
		case '06':
		case '09':
		case '11':
			if  ((dia < 1) || (dia > 30)) 
			{
				alert("A Data "+data+" � inv�lida!");
				return false;
			}
			break;
		case '02':
			/* Validando ano Bissexto / fevereiro / dia */ 
			if ((ano % 4 == 0) || (ano % 100 == 0) || (ano % 400 == 0)) 
			{ 
				bissexto = 1; 
			} 
			if ((bissexto == 1) && ((dia < 1) || (dia > 29))) 
			{ 
				alert("A Data "+data+" � inv�lida!");
				return false;			 
			} 
			if ((bissexto != 1) && ((dia < 1) || (dia > 28))) 
			{ 
				alert("A Data "+data+" � inv�lida!");
				return false;
			}			
			break;			
	}
	
	if ((hora < 0) || (hora > 23) || (min < 0) || (min > 59))
	{
		alert("A Data "+data+" � inv�lida!");
		return false;
	}
	
	var dataPreenchida = new Date(ano, mes - 1, dia, hora, min, 0, 0);
	var dataAtual = new Date();
	dataAtual.setMinutes(dataAtual.getMinutes() + 15);
	
	/*
	if (dataPreenchida > dataAtual)
	{
		alert("A Data "+data+ " � superior a data atual!");
		return false;
	}
	*/
	return valid;
}

function showSolicitacao(neoid) {
	var url = '<%=PortalUtil.getBaseURL()%>bpm/workflow_search.jsp?id='+neoid;
	var newModalId = NEO.neoUtils.dialog().addModal(true,null,null,null,null,'');
	var win = window.name; 
	
    NEO.neoUtils.dialog().createModal(url+'&modalId='+newModalId+'&idDiv='+win);
}

function liberacaoPresenca(idButton,codigoCliente, usuario){
	//var	result = callSync('<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet?action=saveLiberacaoSite&codigoCliente='+codigoCliente+'&usuario='+usuario);
	var libera = 'Libera Presen�a Site';
	$.ajax({
        type:"post",
        url:'servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet',
        data:'action=saveLiberacaoSite&codigoCliente='+codigoCliente+'&usuario='+usuario,
        success: function(result) {
        	if (result.liberado == true){
        		alert('Acesso ao Presen�a no site Liberado para este cliente');
        		libera = 'Bloqueia Presen�a Site';
        	}else{
        		alert('Acesso ao Presen�a no site Bloqueado para este cliente');
        		libera = 'Libera Presen�a Site';
        	}
     	   	//alert('liberado?'+result.liberado);
     	   	//$('#'+idButton).html('hellooooo');
        	//alert($('iframe').contents().find('#'+idButton).val());
        }
    });
	
	//alert(result + ' - ' + result.liberado);
	
	
}




</script>
<style type="text/css">
   .iconCol {
  		cursor:pointer;
  		width:16px;
   		height:16px;
   		float: left;
   		background: url('<%=PortalUtil.getBaseURL()%>imagens/custom/papel_16x16.png') no-repeat;
	}
	.txtCol{
	    background:red; 
	    border-width:1px;
	    border-color:black;
	    color:white;
	    font-weight:bold;
	    font-size:8px;
	    margin-top:6px;
	    margin-left:5px;
	    text-align: center;
	    padding-right: 1px;
	}
</style>