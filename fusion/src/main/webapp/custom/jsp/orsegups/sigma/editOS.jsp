<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.security.NeoPaper"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.custom.orsegups.sigma.vo.DBOrdemVO"%>
<%@page import="com.neomind.fusion.custom.orsegups.sigma.SigmaUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%
	String idOrdem = request.getParameter("idOrdem");
	SigmaUtils sigmaUtils = new SigmaUtils();
	DBOrdemVO ordem = new DBOrdemVO();
	ordem = sigmaUtils.buscaOS(Long.parseLong(idOrdem));
	
	NeoUser user = PortalUtil.getCurrentUser();
	NeoPaper papelOSAgendar = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code","[OS] Agendar Ordem de Serviço"));
	Boolean isAgendarOS= Boolean.FALSE;
	
	if(user != null && user.getPapers() != null && user.getPapers().contains(papelOSAgendar)) {
		isAgendarOS = Boolean.TRUE;
	}
%>

<html>
<body>
	<div style="width:100%; padding:5px;height:auto">
		<form id="fmOrdem" method="post">
			<table style="width: 100%;">
				<tbody>
					<tr>
						<td class="flabel" class="flabel">OS:</td>
						<td class="fvalue">
							<span style="font-weight: bold"><%= ordem.getId() %></span>
						</td>
					</tr>
					<tr>
						<td class="flabel" class="flabel">Conta:</td>
						<td class="fvalue">
							<span style="font-weight: bold"><%= ordem.getContaParticao() %></span>
						</td>
					</tr>
					<tr>
						<td class="flabel" class="flabel">Razão Social:</td>
						<td class="fvalue">
							<span style="font-weight: bold"><%= ordem.getRazao() %></span>
						</td>
					</tr>
					<tr>
						<td class="flabel" class="flabel">Fantasia:</td>
						<td class="fvalue">
							<span style="font-weight: bold"><%= ordem.getFantasia() %></span>
						</td>
					</tr>
					<% if (ordem.getTecnicoForcarOS() != null) {%>
					<tr>
						<td class="flabel" class="flabel">Forçar OS para:</td>
						<td class="fvalue">
							<span style="font-weight: bold"><pre><%= ordem.getTecnicoForcarOS() %></pre></span>
						</td>
					</tr>
					<% }%>
					<tr>
						<td class="flabel" class="flabel">Técnico Responsável:</td>
						<td class="fvalue">
							<input id="cbColaborador" style="width: 500px;font-size:11px;text-transform: uppercase;" 
								class="easyui-combobox" data-options="
		        		   		valueField:'id',
		        		   		textField:'nome',
		        		   		panelHeight:'160',
		        		   		multiple:false,
		        		   		url:'servlet/com.neomind.fusion.custom.orsegups.sigma.servlet.SigmaServlet?action=listaColaboradores&idInstalador=<%= ordem.getIdInstalador().toString() %>'">
		        		   		
						</td>
					</tr>
					<tr>
						<td class="flabel" class="flabel">Defeito:</td>
						<td class="fvalue">
							<input id="cbDefeito" style="width: 500px;font-size:11px;"
								class="easyui-combobox" data-options="
		        		   		valueField:'id',
		        		   		textField:'descricao',
		        		   		panelHeight:'160',
		        		   		editable:false,
		        		   		multiple:false,
		        		   		url:'servlet/com.neomind.fusion.custom.orsegups.sigma.servlet.SigmaServlet?action=listaDefeitos&idDefeito=<%= ordem.getIdDefeito() %>'">
							
						</td>
					</tr>
					<tr>
						<td class="flabel" class="flabel">Solicitante:</td>
						<td class="fvalue">
							<input id="cbSolicitante" style="width: 500px;font-size:11px;"
								class="easyui-combobox" data-options="
		        		   		valueField:'id',
		        		   		textField:'descricao',
		        		   		panelHeight:'160',
		        		   		editable:false,
		        		   		multiple:false,
		        		   		url:'servlet/com.neomind.fusion.custom.orsegups.sigma.servlet.SigmaServlet?action=listaSolicitantes&idSolicitante=<%= ordem.getIdSolicitacao() %>'">
						</td>
					</tr>
					<tr>
						<td class="flabel" class="flabel">Descrição:</td>
						<td class="fvalue">
							<textarea id="taDescricao" style="border:solid 1px #95b8e7; width:500px; height: 100px" ><%=ordem.getDescricao()%></textarea>
						</td>
					</tr>
					
					<% if (isAgendarOS) {%>
					
					<tr>
						<td class="flabel" class="flabel">Agendado com Cliente:</td>
						<td class="fvalue" style="padding-top: 3px;">
							<input id="cbAgendado" type="checkbox" style="width: 22px; height: 22px" onclick="disabledAgendamento()">
						</td>
					</tr>
					<tr>
						<td class="flabel" class="flabel">Data:</td>
						<td class="fvalue">
							<input id="dbData" class="easyui-datebox" style="width: 106px"
								data-options="formatter:myformatter,parser:myparser, editable:false, disabled:true" 
								onload=""></input>
						</td>
					</tr>
					<tr>
						<td class="flabel" class="flabel">Hora:</td>
						<td class="fvalue">
							<select id="cbHora"  class="easyui-combobox" data-options="panelHeight:'160', disabled:true" style="width: 50px;font-size:11px;">
								<option value=00>00</option><option value=01>01</option><option value=02>02</option><option value=03>03</option>
								<option value=04>04</option><option value=05>05</option><option value=06>06</option><option value=07>07</option>
								<option value=08>08</option><option value=09>09</option><option value=10>10</option><option value=11>11</option>
								<option value=12>12</option><option value=13>13</option><option value=14>14</option><option value=15>15</option>
								<option value=16>16</option><option value=17>17</option><option value=18>18</option><option value=19>19</option>
								<option value=20>20</option><option value=21>21</option><option value=22>22</option><option value=23>23</option>
							</select>
							<select id="cbMinuto"  class="easyui-combobox" data-options="panelHeight:'90', disabled:true" style="width: 50px;font-size:11px;">
								<option value=00>00</option><option value=15>15</option><option value=30>30</option><option value=45>45</option>
							</select>
						</td>
					</tr>
					<% }%>
				</tbody>
			</table>		
			<div align="center" style="width:100%; padding: 10px">
				<a href="javascript:void(0)" class="easyui-linkbutton" onclick='salvar()'>Salvar</a>
				<a href="javascript:void(0)" class="easyui-linkbutton" onclick="$('#modalEditOS').dialog('close')">Cancelar</a>
			</div>
		</form>	
	</div>	
	
	<script type="text/javascript">
	
	function myformatter(date) {
		var y = date.getFullYear();
		var m = date.getMonth() + 1;
		var d = date.getDate();
		return (d < 10 ? ('0' + d) : d) + '/' + (m < 10 ? ('0' + m) : m) + '/'
				+ y;
	
	}
	function myparser(s) {
		if (!s)
			return new Date();
		var ss = (s.split('-'));
		var y = parseInt(ss[0], 10);
		var m = parseInt(ss[1], 10);
		var d = parseInt(ss[2], 10);
		if (!isNaN(y) && !isNaN(m) && !isNaN(d)) {
			return new Date(d, m - 1, y);
		} else {
			return new Date();
		}
	}
	function disabledAgendamento() {
		
		if (document.getElementById('cbAgendado').checked) {
			
			var data = <%= ordem.getData() %>;
			var hora = <%= ordem.getHora() %>;
			var minuto = <%= ordem.getMinuto() %>;
			
			
			if (data != null && data != ''){
				$('#dbData').datebox('setValue', '<%= ordem.getData() %>');
			}
			if (hora != null && hora != ''){
				$('#cbHora').combobox('setValue', '<%= ordem.getHora() %>');
			}
			if (minuto != null && minuto != ''){
				$('#cbMinuto').combobox('setValue', '<%= ordem.getMinuto() %>');
			}
			
			$('#dbData').datebox({
				disabled:false
			});
			$('#cbHora').combobox({
				disabled:false
			});
			$('#cbMinuto').combobox({
				disabled:false
			});
        } else {
        	$('#dbData').datebox({
				disabled:true
			});
			$('#cbHora').combobox({
				disabled:true
			});
			$('#cbMinuto').combobox({
				disabled:true
			});
        }
	}
	
	function salvar() {
		
		var ordem = <%= ordem.getId()%>;
		var cdCliente = <%= ordem.getCdCliente()%>;
		var instalador = $('#cbColaborador').combobox('getValue');
		var defeito = $('#cbDefeito').combobox('getValue');
	    var solicitante = $('#cbSolicitante').combobox('getValue');	
		var solicitanteAnt = <%= ordem.getIdSolicitacao()%>;
	    var descricao = $('#taDescricao').val();
	    var data;
	    var hora;
	    var minuto = '';
	    if (document.getElementById('cbAgendado').checked) 
	    {
	    	data = $('#dbData').datebox('getValue');
	    	hora = $('#cbHora').combobox('getValue');
	    	minuto = $('#cbMinuto').combobox('getValue');
	    }
	    else
	    {
	    	var dataAux = '<%= ordem.getData()%>';
	    	if (dataAux == 'null')
	    	{
	    		data = '';
	    	}
	    	else
	    	{
	    		data = dataAux;
	    	}
		    hora = '<%= ordem.getHora()%>';
		    minuto = '<%= ordem.getMinuto()%>';
	    }
		
    	$.ajax({
        	method: 'post',
            url: 'servlet/com.neomind.fusion.custom.orsegups.sigma.servlet.SigmaServlet',
            data: 'action=editaOS&idOrdem='+ordem+'&cdCliente='+cdCliente+'&idInstalador='+instalador+'&idDefeito='+defeito+'&idSolicitante='+solicitante+'&idSolicitanteAnt='+solicitanteAnt+'&descricao='+descricao+'&data='+data+'&hora='+hora+'&minuto='+minuto,
            dataType: 'json',
            success: function(result){
            	if (result.msg == 'OK')
            	{
	            	$.messager.show({
	                   	title:'Mensagem',
	                   	msg:'OS alterada com sucesso!',
	                   	timeout:5000,
	                   	showType:'slide'
	            	});
            	}
            	else
            	{
            		$.messager.show({
	                   	title:'Mensagem',
	                   	msg:result.msg,
	                   	timeout:10000,
	                   	showType:'slide'
	            	});
            	}	
            },
            error: function(result){
            	$.messager.show({
                   	title:'Mensagem',
                   	msg:'Erro ao editar a OS!',
                   	timeout:10000,
                   	showType:'slide'
            	});
            }
        });
	}
</script>
</body>
</html>