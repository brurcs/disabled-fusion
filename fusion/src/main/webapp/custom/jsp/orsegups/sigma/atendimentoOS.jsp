<%@page import="com.neomind.fusion.persist.QLRawFilter"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.security.NeoPaper"%>
<%@page import="com.neomind.fusion.custom.orsegups.sigma.SigmaUtils"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
 
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLNotInFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.datawarehouse.ExternalEntityInfo"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.*"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.Timestamp"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.net.URLEncoder"%>


<style type="text/css">
.cor_os_vermelha_aberta {
  background-color: #ffffff;
  color: #ff0000;
}
.cor_os_verde_execucao {
  background-color: #ffffff;
  color: #008000;
}
.cor_os_amarela_pausada{
  background-color: #ffffff;
  color: #ff9a00;
}
.cor_os_cinza_fechada {
  background-color: #ffffff;
  color: #959595;
}

.flabel{
	text-align: right; 
	height:28px; 
	font-weight: bold; 
	background-color: #e0ecff;
	width: 150px;
	padding-right: 2px; 
}
.fvalue{
	padding-left: 8px; 
}

</style>
 <!-- <meta http-equiv="refresh" content="180"/>-->
<%		
	NeoUser user = PortalUtil.getCurrentUser();
	NeoPaper papelOSEditar = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code","[OS] Editar Ordem de Servi�o"));
	Boolean isEditarOS= Boolean.FALSE;
	
	if(user != null && user.getPapers() != null && user.getPapers().contains(papelOSEditar)) {
		isEditarOS = Boolean.TRUE;
	}

	String regional = request.getParameter("regional");
	if(regional == null || regional.trim().equals(""))	{
		regional = "";
 	}
	String cd_tecnico = request.getParameter("cd_tecnico");
	if(cd_tecnico == null || cd_tecnico.trim().equals(""))	{
		cd_tecnico = "";
 	}
	
	String cd_cliente_p = request.getParameter("cd_cliente");
	if(cd_cliente_p == null || cd_cliente_p.trim().equals(""))	{
	    cd_cliente_p = "";
 	}

	String checkedFechadas = "";
	String checkedAbertas = "";
	String checkedSomenteFechadas = "";
	String checkedAlertas = "";
	
	String checkedRastreamento = "";
	
	String checkedPRO = "";

	boolean booleanTerceiro = false;
	String mostrarTerceiro = request.getParameter("terceiro");
	if(mostrarTerceiro != null && mostrarTerceiro.trim().equals("sim"))	{
		booleanTerceiro = true;
 	} 
	
	boolean booleanCorpe = false;
	
	String mostrarCorpe = request.getParameter("corpe");
	
	if(mostrarCorpe != null && mostrarCorpe.trim().equals("sim")){
	    booleanCorpe = true;
	}
	

	boolean booleanMostrarFechadas = false;
	String mostrarFechadas = request.getParameter("controle");
	if(mostrarFechadas != null && mostrarFechadas.trim().equals("on"))	{
		booleanMostrarFechadas = true;
		checkedFechadas = " checked ";
 	} 

	boolean booleanMostrarAbertas = false;
	String mostrarAbertas = request.getParameter("controle");
	if(mostrarAbertas != null && mostrarAbertas.trim().equals("on"))	{
		booleanMostrarAbertas = true;
		checkedAbertas = " checked ";
 	}	

	boolean booleanMostrarAlertas = false;
	String mostrarAlertas = request.getParameter("controle");
	if(mostrarAlertas != null && mostrarAlertas.trim().equals("on"))	{
		booleanMostrarAlertas = true;
		checkedAlertas = " checked ";
 	}
	boolean booleanSomenteFechadas = false;
	String somenteFechadas = request.getParameter("controle");
	if(somenteFechadas != null && somenteFechadas.trim().equals("on"))	{
		booleanSomenteFechadas = true;
		checkedSomenteFechadas = " checked ";
 	}
	
	boolean booleanRastreamento = false;
	String sRastreamento = request.getParameter("check_rastremanento");
	if(sRastreamento != null && sRastreamento.trim().equals("on"))	{
	    booleanRastreamento = true;
		checkedRastreamento = " checked ";
 	}

	boolean booleanPRO = false;
	String sPRO = request.getParameter("check_pro");
	if(sPRO != null && sPRO.trim().equals("on"))	{
		booleanPRO = true;
		checkedPRO = " checked ";
 	}
	
	String controle = request.getParameter("controle");
	//System.out.println(controle);
	if (controle == null)	{
		controle = "P";
 	} 
	if (controle.equals("T"))	{
		booleanMostrarFechadas = true;
		booleanMostrarAbertas = true;
		booleanSomenteFechadas = false;
		booleanMostrarAlertas = true;
		checkedFechadas	= " checked ";	
 	} 
	if (controle.equals("P"))	{
		booleanMostrarFechadas = false;
		booleanMostrarAbertas = true;
		booleanMostrarAlertas = true;
		checkedAbertas	= " checked ";	
 	} 
	if (controle.equals("A"))	{
		booleanMostrarFechadas = false;
		booleanMostrarAbertas = false;
		booleanMostrarAlertas = true;
		checkedAlertas	= " checked ";	
 	} 
	if (controle.equals("F"))	{
		booleanMostrarFechadas = false;
		booleanMostrarAbertas = false;
		booleanMostrarAlertas = false;
		booleanSomenteFechadas = true;
		checkedSomenteFechadas	= " checked ";	
 	} 
	
	StringBuilder txtCol = new StringBuilder();
	String menu = request.getParameter("menu");
	
	List<NeoObject> cols = new ArrayList<NeoObject>();
	
	if (menu != null && !menu.equals(""))
	{
		QLRawFilter filtroOS = new QLRawFilter("menuOs.descricao = '" + menu + "'");
		cols = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("OSSigmaColaboradores"),filtroOS);
	}
	else
	{
		cols = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("OSSigmaColaboradores"));
	}
	
	if (cols != null)
	{
		int cont = 0;
		for (NeoObject col : cols)
		{
			EntityWrapper colWrapper = new EntityWrapper(col);
			String nomCol = (String) colWrapper.findValue("colaborador.nm_colaborador");
			
			txtCol.append("'");
			txtCol.append(nomCol);
			txtCol.append("'");
			if (cont < cols.size()-1)
			{
				txtCol.append(",");	
			}
			cont++;
		}
	}
%>
<portal:head title="Ordens de Servi�o">
<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/icon.css"> 
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui-lang-pt_BR.js"></script>
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/datagrid-detailview.js"></script>	
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL() %>custom/jsp/orsegups/overlib/overlib.js"></script>
	<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL() %>custom/jsp/orsegups/overlib/overlibmws.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>

	

<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>

<cw:main>
	<cw:header title="Ordens de Servi�o" />
	<cw:body id="area_scroll">
<%	
	if (cd_tecnico == "" && cd_cliente_p == "") {
%>	
		<form name="formControles" action="/fusion/custom/jsp/orsegups/sigma/atendimentoOS.jsp?regional=<%=regional%>&terceiro=<%=mostrarTerceiro%>" method="POST">
			<input onClick="document.formControles.submit();" type="button" value="Atualizar"/>
			<input onClick="document.formControles.submit();" type="radio" <%= checkedFechadas %> name="controle" id="mostrarFechadas" value="T"/> <label for="mostrarFechadas">Todas</label>
			<input onClick="document.formControles.submit();" type="radio" <%= checkedSomenteFechadas %> name="controle" id="somenteFechadas" value="F"/> <label for="somenteFechadas">Fechadas</label>
			<input onClick="document.formControles.submit();" type="radio" <%= checkedAbertas %> name="controle" id="mostrarAbertas" value="P"/> <label for="mostrarAbertas">Abertas</label>
			<input onClick="document.formControles.submit();" type="radio" <%= checkedAlertas %> name="controle" id="mostrarAlertas" value="A"/> <label for="mostrarAlertas">Somente Alertas</label>
<%
	if(booleanTerceiro) {
%>			
			<input style="align:left" onClick="document.formControles.submit();" type="checkbox" <%= checkedRastreamento %> name="check_rastremanento" id="check_rastremanento"/>Somente Rastreamento
			<input style="align:left" onClick="document.formControles.submit();" type="checkbox" <%= checkedPRO %> name="check_pro" id="check_pro"/>Somente Portaria Remota
<%
	}
%>			
		</form>
<%	
	} else {
		controle = "F";
		booleanSomenteFechadas = true;
	}
%>	

	<div id="modalEditOS" class="easyui-window" title="Editar OS" data-options="modal:true,closed:true,resizable:false,collapsible:false,minimizable:false,maximizable:false" style="width:700px;height:510px">  
    
    </div>

		<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0">
	<thead style="cursor: auto">
		<tr style="cursor: auto">
			<th style="cursor: auto">OS</th>
			<th style="cursor: auto">Sit.</th>
			<th style="cursor: auto">Abertura</th>
			<th style="cursor: auto"><img src="imagens/icones_final/clock.png" alt="Tempo desde abertura"/></th>
			<th style="cursor: auto">Execu��o/Pausa</th>
			<th style="cursor: auto">.</th>
			<th style="cursor: auto"><img src="imagens/icones_final/clock.png" alt="Tempo de execu��o/pausa"/></th>
			<th style="cursor: auto">Fechamento</th>
			<th style="cursor: auto">Defeito</th>
			<th style="cursor: auto">Prioridade</th>
			<th style="cursor: auto">Agenda</th>
			<th style="cursor: auto">Eqp</th>
			<th style="cursor: auto">Conta [Parti��o] Cliente</th>
			<th style="cursor: auto" class="min">Endere�o</th>
		</tr>
	</thead>
	<tbody>	

<%!
public static String getTimeSpentDays(long timeSpent) {

		// string de retorno
		String str = "";

		// quantidade de dias
		long days = timeSpent / 86400;

		// se houver dias
		if (days > 0) {

			// mostra quantos dias passaram
			str = str + days + "d";
		} else {
			str =  "1d";
		}

		return str;

	}
	
public static String getTimeSpent(long timeSpent) {

		// string de retorno
		String str = "";

		// quantidade de anos
		long years = timeSpent / 31536000;

		// se houver anos
		if (years >= 1) {
			// mostra quantos anos passaram
			str = years + "a";

			// retira do total, o tempo em segundos dos anos passados
			timeSpent = timeSpent - (years * 31536000);
		}

		// quantidade de meses (anos/12 e n�o dias*30)
		long months = timeSpent / 2628000;

		// se houver meses
		if (months >= 1) {
			str = str + months + "m";

			// retira do total, o tempo em segundos dos meses passados
			timeSpent = timeSpent - (months * 2628000);
		}

		// quantidade de dias
		long days = timeSpent / 86400;

		// se houver dias
		if (days >= 1) {

			// mostra quantos dias passaram
			str = str + days + "d";

			// retira do total, o tempo em segundos dos dias passados
			timeSpent = timeSpent - (days * 86400);
		}

		// quantidade de horas
		long hours = timeSpent / 3600;

		// se houver horas
		if (hours >= 1) {

			// mostra quantas horas passaram
			str = str + hours + "h";

			// retira do total, o tempo em segundos das horas passados
			timeSpent = timeSpent - (hours * 3600);
		}

		// quantidade de minutos
		long minutes = timeSpent / 60;

		// se houver minutos
		if (minutes >= 1) {

			// mostra quantos minutos passaram
			str = str + minutes + "m";

			// retira do total, o tempo em segundos dos minutos passados
			timeSpent = timeSpent - (minutes * 60);
		}

		// mostra quantos minutos passaram
		if (timeSpent > 0)
			str = str + timeSpent + "s";

		return str;

	}
	

	public static String encodeHTML(String s) {
		StringBuffer out = new StringBuffer();
		for(int i=0; i<s.length(); i++)
		{
			char c = s.charAt(i);
			if(c > 127 || c=='"' || c=='<' || c=='>')
			{
			   out.append("&#"+(int)c+";");
			}
			else
			{
				out.append(c);
			}
		}
		return out.toString();
	}		
%>	
<%		
	final String none = "&nbsp;";
	Connection conn = null;
	Connection connSapiens = null;
    PreparedStatement st = null;
    PreparedStatement st2 = null;
    PreparedStatement st3 = null;
	ResultSet rs = null;
	ResultSet rs2 = null;
	ResultSet rsEquip = null;
	String erro = "";
    try {
		String nomeFonteDados = "SIGMA90";
		conn = PersistEngine.getConnection(nomeFonteDados);
		connSapiens = PersistEngine.getConnection("SAPIENS");

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH':'mm");
		Timestamp tsAuxAbertura = null;
		Timestamp tsAuxAgendada = null;
		Timestamp tsAuxFec = null;
		Timestamp tsAuxExec = null;
		Timestamp tsAuxPausa = null;
		Timestamp tsAuxUltimo = null;
		Date dateAuxAgora = new Date();
		String auxTimeAbertura = "";
		String auxTimeAgendada = "";
		String auxTimeFec = "";
		String auxTimeExec = "";
		long longTempoMinutos = 0;
		String sTempoAtendimento = "";
		String sTempoExec = "";

		// 0 - Aberta
		// 1 - Fechada
		// 2 - Execu��o
		// 3 - Pausa        
		
		StringBuffer sql1 = new StringBuffer();
		sql1.append(" SELECT col.NM_COLABORADOR , def.DESCRICAODEFEITO, os.ID_ORDEM, os.ABERTURA, os.DEFEITO, os.ID_INSTALADOR,   		");
		sql1.append(" c.ID_EMPRESA, c.ID_CENTRAL, c.PARTICAO , c.CD_CLIENTE, c.FANTASIA, c.RAZAO, c.ENDERECO, bai.NOME AS NOMEBAIRRO, 	");
		sql1.append(" cid.NOME AS NOMECIDADE, os.FECHAMENTO, os.FECHADO, osh.DATAINICIOEXECUCAO, osh.DATAFIMEXECUCAO, os.EXECUTADO, sol.NM_DESCRICAO, CD_COLABORADOR,       				");
		sql1.append(" mp.NM_DESCRICAO AS NOMEMOTIVOPAUSA, p.TX_OBSERVACAO, p.CD_MOTIVO_PAUSA, os.DATAAGENDADA, p.DT_PAUSA			");
		sql1.append(" FROM dbORDEM os WITH (NOLOCK)																									");
		sql1.append(" INNER JOIN COLABORADOR col WITH (NOLOCK) ON col.CD_COLABORADOR = os.ID_INSTALADOR 												");
		sql1.append(" INNER JOIN OS_SOLICITANTE sol WITH (NOLOCK) ON sol.CD_OS_SOLICITANTE = os.CD_OS_SOLICITANTE 												");
		sql1.append(" INNER JOIN OSDEFEITO def WITH (NOLOCK) ON def.IDOSDEFEITO = os.IDOSDEFEITO														");
		sql1.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = os.CD_CLIENTE 														 	");
		sql1.append(" LEFT JOIN dbBAIRRO bai WITH (NOLOCK) ON bai.ID_BAIRRO = c.ID_BAIRRO AND bai.ID_CIDADE = c.ID_CIDADE  							");
		sql1.append(" LEFT JOIN dbCIDADE cid WITH (NOLOCK) ON cid.ID_CIDADE = c.ID_CIDADE  										 					");
		sql1.append(" LEFT JOIN OSHISTORICO osh WITH (NOLOCK) ON osh.ID_ORDEM = os.ID_ORDEM AND osh.IDOSHISTORICO = (SELECT MIN(IDOSHISTORICO) FROM OSHISTORICO WHERE ID_ORDEM = os.ID_ORDEM)	");
		sql1.append(" LEFT JOIN ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p ON p.CD_ORDEM_SERVICO = os.ID_ORDEM AND p.CD_ORDEM_SERVICO_MOTIVO_PAUSA = (SELECT MAX(p2.CD_ORDEM_SERVICO_MOTIVO_PAUSA) FROM ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p2 WHERE p2.CD_ORDEM_SERVICO = p.CD_ORDEM_SERVICO )	");
		sql1.append(" LEFT OUTER JOIN MOTIVO_PAUSA mp ON mp.CD_MOTIVO_PAUSA = p.CD_MOTIVO_PAUSA ");
		if (cd_tecnico == "" && cd_cliente_p == "") {
			sql1.append(" WHERE (col.NM_COLABORADOR LIKE '"+regional+"%' or col.NM_COLABORADOR LIKE 'PUB - "+regional+"%' or col.CD_COLABORADOR = 9999)  AND col.FG_ATIVO_COLABORADOR = 1	AND OS.ID_ORDEM != 1715193 																		");
			//sql1.append(" And col.CD_COLABORADOR not in (105190) ");
			if (menu != null) {
				sql1.append(" AND col.NM_COLABORADOR IN ( " + txtCol + ")															");
			} else {
				sql1.append(" AND col.NM_COLABORADOR NOT IN ( " + txtCol + ")														");
				if (booleanTerceiro) {
				    if (booleanCorpe){
						sql1.append(" AND col.NM_COLABORADOR LIKE '%TERC%CORP%' 																	");
				    }else{
						sql1.append(" AND col.NM_COLABORADOR LIKE '%TERC%' 																	");
					
				    }
				} else {
					sql1.append(" AND col.NM_COLABORADOR NOT LIKE '%TERC%' 																");
				}
			}
			sql1.append(" AND (  					      																					");
			sql1.append("       (os.FECHADO IN (0, 2, 3) )      																				");
			sql1.append("       OR                      		 																			");
			sql1.append("       (os.FECHADO = 1 AND os.FECHAMENTO >= (CAST((STR( YEAR( GETDATE() ) ) + '/' + STR( MONTH( GETDATE() ) ) + '/' + STR( DAY( GETDATE() ) ))AS DATETIME)) ) 																		");
			sql1.append("    )  		 																									");
			
			if (booleanRastreamento && booleanTerceiro) {
			    sql1.append("  AND col.NM_COLABORADOR LIKE '%RASTR%' ");
			}
			if (booleanPRO && booleanTerceiro) {
			    sql1.append("  AND def.DESCRICAODEFEITO LIKE 'OS-PRO%' ");
			}
			
		} else if (cd_tecnico != ""){
			sql1.append(" WHERE col.CD_COLABORADOR = "+cd_tecnico+"  																	");
			sql1.append(" AND (os.FECHADO = 1 AND os.FECHAMENTO >=(GETDATE() - 11) ) 																		");
		}else if (cd_cliente_p != ""){
		    sql1.append(" WHERE c.CD_CLIENTE = "+cd_cliente_p+"  																	");
			sql1.append(" AND (os.FECHADO = 1 AND os.FECHAMENTO >=(GETDATE() - 90) ) 																		");
		}
		sql1.append(" ORDER BY CASE WHEN col.NM_COLABORADOR = 'COLABORADOR_PADR�O' THEN 2 ELSE 1 END, col.NM_COLABORADOR, os.FECHAMENTO DESC, sol.NM_DESCRICAO, os.DATAAGENDADA, os.ABERTURA																			");
		//out.println(sql1.toString());	
		
		st = conn.prepareStatement(sql1.toString());
		rs = st.executeQuery();
				
		String nm_tecnico_anterior = "primeira";
		String icone_tecnico = "<img src='imagens/custom/user-worker.png' alt='T�cnico'/>&nbsp;";
		String linha_rota = "";
		String class_cor_evento = "";
		String nm_status = "";
		String nm_tecnico = ".";
		String nm_tecnico_display = "";
		int qtdAtendimentos = 0;
		int qtdAtendidos = 0;
		String horaLoginAA = "";
		Timestamp timeLoginAA = null;
		String alertExec = "&nbsp;";
		String alertIdle = "&nbsp;";
		boolean isAlertOS = false;
		boolean isAlertCol = false;
		boolean isFechada = false;

		StringBuffer linhaOS = new StringBuffer();		
					
		while(rs.next()) {
			isAlertOS = false;
			isFechada = false;
			
			/* Trocou de TECNICO */
			nm_tecnico = rs.getString("NM_COLABORADOR");
			if (!nm_tecnico_anterior.equals(nm_tecnico)) {
			
				out.print(linhaOS.toString());
			
				if(!nm_tecnico_anterior.equals("primeira")) {
					out.print("</tr class='linha_viatura'>");
					out.print("<td style='font-weight: normal' colspan='2'>&nbsp;</td>");
					if (cd_tecnico == "" && cd_cliente_p == "") {
						out.print("<td style='font-weight: bold' colspan='2'> Em atendimento: "+ qtdAtendimentos +"</td>");					
					} else {
						out.print("<td style='font-weight: bold' colspan='2'>&nbsp;</td>");					
					}
					//TODO REMOVER
					//out.print("<td style='font-weight: bold' colspan='3'> Fechadas: "+ qtdAtendidos + " desde: " + formatter.format(tsAuxUltimo) + "</td>");
					out.print("<td style='font-weight: normal' colspan='6'>&nbsp;</td>");
					out.print("</tr>");
				}
				String id_instalador = rs.getString("ID_INSTALADOR");
				tsAuxUltimo = null;				

				StringBuffer sql2 = new StringBuffer();
				sql2.append(" SELECT MIN(FECHAMENTO) AS FECHAMENTO	");
				sql2.append(" FROM dbORDEM WITH (NOLOCK) 																							");
				if (cd_tecnico == "") {
					sql2.append(" WHERE FECHADO = 1 AND FECHAMENTO >= (CAST((STR( YEAR( GETDATE() ) ) + '/' + STR( MONTH( GETDATE() ) ) + '/' + STR( DAY( GETDATE() ) ))AS DATETIME))  			");
					sql2.append(" AND ID_INSTALADOR = " + id_instalador + " ");
				} else {
					sql2.append(" WHERE ID_INSTALADOR = "+cd_tecnico+" 																	");
					sql2.append(" AND (FECHADO = 1 AND FECHAMENTO >=(GETDATE() - 11) ) 																		");				
				}

				st2 = conn.prepareStatement(sql2.toString());
				rs2 = st2.executeQuery();
// 				while(rs2.next()) {
// 					tsAuxUltimo = rs2.getTimestamp("FECHAMENTO");
// 				}
// 				if (tsAuxUltimo == null) {
// 					tsAuxUltimo = new Timestamp( formatter.parse(Calendar.getInstance().get(Calendar.DAY_OF_YEAR) +"/" + Calendar.getInstance().get(Calendar.MONTH)+1 +"/" +Calendar.getInstance().get(Calendar.YEAR) + " 08:00:00.000").getTime());
// 				}
				
// 				if (dateAuxAgora.getTime() - tsAuxUltimo.getTime() > (1000*60*30)) {
// 					isAlertCol = true;
// 				}
				nm_tecnico_anterior = nm_tecnico;
				out.print("<tr class='linha_viatura'>");				
				out.print("<td style='font-weight: bold' colspan='6'>");				
				if (isAlertCol) {
					if (cd_tecnico == "" && cd_cliente_p == "") {
						out.print("<img src='imagens/custom/01_14_t_e0.gif' alt='ALERTA: T�cnico ocioso h� mais de 30 minutos'/> ");
					}
				}
				out.print(icone_tecnico+ SigmaUtils.getLogColaboradorLink(Long.parseLong(id_instalador)) +"&nbsp;"+ nm_tecnico);				
				out.print("</td>");
				if (cd_tecnico == "" && cd_cliente_p == "") {
					out.print("<td style='font-weight: bold' colspan='2'><a href='javascript:imprimeRoteiro(" + rs.getInt("CD_COLABORADOR") + ");'><img src='imagens/custom/ico_20_dash_imp.gif' alt='Imprimir Roteiro'/> Imprimir Roteiro</a>");
					out.print("<td style='font-weight: bold' colspan='4'><a href='javascript:visualizarHistorico(" + rs.getInt("CD_COLABORADOR") + ");'><img src='imagens/icones_final/audit_event_query_16x16-trans.png' alt='Hist�rico 10 dias'/> Hist�rico 10 dias</a>");

				} else {
					out.print("<td style='font-weight: bold' colspan='6'>&nbsp;</a>");
				}
				out.print("</tr>");
				qtdAtendimentos = 0;
				qtdAtendidos = 0;	
				linhaOS = new StringBuffer();	
				isAlertCol = false;
								

			}	
			
			String cd_empresa = rs.getString("ID_EMPRESA");
			String id_central = rs.getString("ID_CENTRAL");
			String particao = rs.getString("PARTICAO");
			String cd_cliente = rs.getString("CD_CLIENTE");
			
			StringBuffer sqlEquip = new StringBuffer();
			sqlEquip.append(" SELECT cvs.USU_CodSer ");
			sqlEquip.append(" FROM USU_T160SIG sig ");
			sqlEquip.append(" INNER JOIN USU_T160CVS cvs ON cvs.usu_codemp = sig.usu_codemp AND cvs.usu_numctr = sig.usu_numctr AND cvs.usu_numpos = sig.usu_numpos ");
			sqlEquip.append(" WHERE sig.usu_codcli = " + cd_cliente + " ");
			sqlEquip.append(" AND cvs.usu_codser IN ('9002035', '9002011', '9002004', '9002005', '9002014') ");
			sqlEquip.append(" ORDER BY cvs.usu_sitcvs ");
			
			st3 = connSapiens.prepareStatement(sqlEquip.toString());
			rsEquip = st3.executeQuery();
			String flag_equip = "<img src='imagens/icones/sphere_red_16x16-trans.png' alt='Equipamento Cliente'/>";
			if (rsEquip.next()) {
				// Equipamento Orsegups
				flag_equip = "<img src='imagens/icones/sphere_yellow_16x16-trans.png' alt='Equipamento Orsegups'/>";
			}
			
			nm_status = "&nbsp;";
			String def = rs.getString("DESCRICAODEFEITO");
			if (rs.getInt("FECHADO") == 1) {
				// 1 - Fechada
				qtdAtendidos++;
				class_cor_evento = "cor_os_cinza_fechada";
				nm_status = "<img src='imagens/icones_final/check_16x16-trans.png' alt='Fechada - "+def+"'/>";
				isFechada = true;
			
			} else if (rs.getInt("FECHADO") == 2) {
				// 2 - Execu��o
				qtdAtendimentos++;
				class_cor_evento = "cor_os_verde_execucao";			
				nm_status = "<img src='imagens/icones_final/user_green_16x16-trans.png' alt='Em execu��o - "+def+"'/>";				
			} else if (rs.getInt("FECHADO") == 3) {
				// 3 - Pausa   			
				qtdAtendimentos++;
				class_cor_evento = "cor_os_amarela_pausada";			
				nm_status = "<img src='imagens/custom/flag_orange.gif' alt='Pausada - "+def+"'/>";				
			} else if (rs.getInt("FECHADO") == 0) {
				// 0 - Aberta
				qtdAtendimentos++;
				class_cor_evento = "cor_os_vermelha_aberta";			
				nm_status = "<img src='imagens/custom/flag_red.gif' alt='Aberta - "+def+"'/>";				
			}

			tsAuxAbertura = rs.getTimestamp("ABERTURA");
			tsAuxAgendada = rs.getTimestamp("DATAAGENDADA");
			tsAuxFec = rs.getTimestamp("FECHAMENTO");
			tsAuxExec = rs.getTimestamp("DATAINICIOEXECUCAO");
			tsAuxPausa = rs.getTimestamp("DT_PAUSA");
			
			sTempoAtendimento = none;
			sTempoExec = none;
			auxTimeAbertura = none;
			auxTimeAgendada = none;
			auxTimeFec = none;
			auxTimeExec = none;
			alertExec = "&nbsp;";
			alertIdle = "&nbsp;";
	
			if (tsAuxAgendada != null) {
				auxTimeAgendada = formatter.format(tsAuxAgendada);
			}
			if (tsAuxAbertura != null) {
				auxTimeAbertura = formatter.format(tsAuxAbertura);
				sTempoAtendimento = getTimeSpentDays(Math.round((dateAuxAgora.getTime() - tsAuxAbertura.getTime()) / 1000));
			}
			if (tsAuxExec != null) {
				auxTimeExec = formatter.format(tsAuxExec);
				sTempoExec = getTimeSpent(Math.round((dateAuxAgora.getTime() - tsAuxExec.getTime()) / 1000));
				if (rs.getInt("FECHADO") == 2) {
					if (Math.round((dateAuxAgora.getTime() - tsAuxExec.getTime()) / 1000) > 7200) {
						alertExec = " <img src='imagens/custom/ball2.gif' alt='ALERTA: Tempo de execu��o acima de 2h'/> ";
						isAlertOS = true;
					}
				} else if (rs.getInt("FECHADO") == 3) {
					// 3 - Pausa   			
					auxTimeExec = formatter.format(tsAuxPausa);
					sTempoExec = getTimeSpent(Math.round((dateAuxAgora.getTime() - tsAuxPausa.getTime()) / 1000));
			    } if (tsAuxFec != null) {
					sTempoExec = getTimeSpent(Math.round((tsAuxFec.getTime() - tsAuxExec.getTime()) / 1000));
				}
			}
			if (tsAuxFec != null) {
				auxTimeFec = formatter.format(tsAuxFec);
			} else if (rs.getInt("FECHADO") == 3) {
				String nomePausa = rs.getString("NOMEMOTIVOPAUSA");
				if (nomePausa == null) {
					auxTimeFec = "<img src=\"imagens/icones_final/process_unknown.png\" alt='Motivo n�o informado'/> ";
				} else {
					String motivoPausa = rs.getString("TX_OBSERVACAO");
					if (motivoPausa == null) {
						motivoPausa = "Vazio";
					} else {
						motivoPausa = "<li>" + motivoPausa;
						motivoPausa =  encodeHTML(motivoPausa.replace("\n", "<br><li>")); 
					}
					motivoPausa =  encodeHTML(motivoPausa.replace("\n", "<br>")); 
					if (rs.getInt("CD_MOTIVO_PAUSA") == 2) {
						auxTimeFec = "  <a tabIndex=\"-1\" href=\"javascript:void(0);\" onmouseover=\"return overlib('" + motivoPausa + "', CAPTION, 'Motivo: "+nomePausa+"', STICKY, CLOSETEXT, 'Fechar', TEXTSIZE, '10px', WIDTH, 400, CELLPAD, 10, 5, FOLLOWMOUSE, VAUTO);\" onmouseout=\"return nd();\">" + " <img src=\"imagens/icones_final/process_finished.png\"/></a> ";
					} else if (rs.getInt("CD_MOTIVO_PAUSA") == 1) {
						auxTimeFec = "  <a tabIndex=\"-1\" href=\"javascript:void(0);\" onmouseover=\"return overlib('" + motivoPausa + "', CAPTION, 'Motivo: "+nomePausa+"', STICKY, CLOSETEXT, 'Fechar', TEXTSIZE, '10px', WIDTH, 400, CELLPAD, 10, 5, FOLLOWMOUSE, VAUTO);\" onmouseout=\"return nd();\">" + " <img src=\"imagens/icones_final/process_start_16x16-trans.png\"/></a> ";					
					}else if (rs.getInt("CD_MOTIVO_PAUSA") == 43) {
						auxTimeFec = "  <a tabIndex=\"-1\" href=\"javascript:void(0);\" onmouseover=\"return overlib('" + motivoPausa + "', CAPTION, 'Motivo: "+nomePausa+"', STICKY, CLOSETEXT, 'Fechar', TEXTSIZE, '10px', WIDTH, 400, CELLPAD, 10, 5, FOLLOWMOUSE, VAUTO);\" onmouseout=\"return nd();\">" + " <img src=\"imagens/icones_final/pause_15x15.png\"/></a> ";					
					} else {
						auxTimeFec = "  <a tabIndex=\"-1\" href=\"javascript:void(0);\" onmouseover=\"return overlib('" + motivoPausa + "', CAPTION, 'Motivo: "+nomePausa+"', STICKY, CLOSETEXT, 'Fechar', TEXTSIZE, '10px', WIDTH, 400, CELLPAD, 10, 5, FOLLOWMOUSE, VAUTO);\" onmouseout=\"return nd();\">" + " <img src=\"imagens/icones_final/jobs_16x16.png\"/></a> ";					
					}
				}
			}
			if ( (controle.equals("T")) || (controle.equals("F") && (isFechada))  || (controle.equals("P") && (!isFechada)) || (controle.equals("A") && (isAlertOS))) {
				String href = "href=\"javascript:saveInputLog('" + rs.getString("ID_ORDEM") + "', 'os');\"";
				
				String contentString = " <a  tabIndex=\"-1\" " + href + " onmouseover=\"return overlib(getLogOs('"+rs.getString("ID_ORDEM")+"'), CAPTION, 'Log da OS', STICKY, CLOSETEXT, 'Fechar', TEXTSIZE, '10px', WIDTH, 350, CELLPAD, 10, 5, FOLLOWMOUSE);\" onmouseout=\"return nd();\">" + " <img src=\"imagens/icones_final/document_v_16x16-trans.png\"/></a> ";
				String editOs = "";
				
				if (isEditarOS)
				{
					editOs = "<a title='Editar OS' style='cursor:pointer' onclick='javascript:editOS("+rs.getString("ID_ORDEM")+")'><img src='imagens/icones_final/document_edit_16x16-trans.png'></a>";
				}
						
				linhaOS.append("<tr>");
				linhaOS.append("<td class='"+ class_cor_evento + "'> "+ rs.getString("ID_ORDEM") + " " +  contentString + " " + editOs + "</td>");
				linhaOS.append("<td class='"+ class_cor_evento + "'> " +  nm_status + "</td>");
				linhaOS.append("<td align='center' class='"+ class_cor_evento + "'> " + auxTimeAbertura + "</td>");
				linhaOS.append("<td align='center' class='"+ class_cor_evento + "'> " + sTempoAtendimento + "</td>");
				linhaOS.append("<td align='center' class='"+ class_cor_evento + "'> " + auxTimeExec + "</td>");
				linhaOS.append("<td align='center' class='"+ class_cor_evento + "'> " + alertExec + "</td>");
				linhaOS.append("<td align='center' class='"+ class_cor_evento + "'> " + sTempoExec + "</td>");
				linhaOS.append("<td align='center' class='"+ class_cor_evento + "'> " + auxTimeFec + "</td>");
				linhaOS.append("<td align='center' class='"+ class_cor_evento + "'> " + def + "</td>");
				linhaOS.append("<td class='"+ class_cor_evento + "'>");	
				String log = rs.getString("DEFEITO");
				if (log == null) {
					log = "Vazio";
				} else {
				    //log = log.replace("ATEN��O: Conferir se a(s) central(is) deste cliente est�o com a programa��o correta de envio de RESTAURO no ato do fechamento da zona. D�vidas entrar em contato com a Assessoria T�cnica.", "");
				    log = log.replace("\r","");
					log = "<li>" + log;
					log =  encodeHTML(log.replace("\n", "<br><li>")); 
				}
				log =  encodeHTML(log.replace("\n", "<br>")); 
				String txtExecutado = rs.getString("EXECUTADO");
				if (txtExecutado == null) {
					txtExecutado = "Vazio";
				} else {
					txtExecutado = "<li>" + txtExecutado;
					txtExecutado =  encodeHTML(txtExecutado.replace("\n", "<br><li>")); 
				}
				txtExecutado =  encodeHTML(txtExecutado.replace("\n", "<br>")); 
								
				linhaOS.append("  <a tabIndex=\"-1\" href=\"javascript:void(0);\" onmouseover=\"return overlib('" + log + "', CAPTION, 'Texto OS', STICKY, CLOSETEXT, 'Fechar', TEXTSIZE, '10px', WIDTH, 350, CELLPAD, 10, 5, FOLLOWMOUSE, VAUTO);\" onmouseout=\"return nd();\">" + " <img src=\"imagens/icones_final/document_v_16x16-trans.png\"/></a> ");
				linhaOS.append("  <a tabIndex=\"-1\" href=\"javascript:void(0);\" onmouseover=\"return overlib('" + txtExecutado + "', CAPTION, 'Fechamento OS', STICKY, CLOSETEXT, 'Fechar', TEXTSIZE, '10px', WIDTH, 350, CELLPAD, 10, 5, FOLLOWMOUSE, VAUTO);\" onmouseout=\"return nd();\">" + " <img src=\"imagens/custom/icon_accept.gif\"/></a> ");
				//linhaOS.append(rs.getString("DESCRICAODEFEITO"));
				linhaOS.append(rs.getString("NM_DESCRICAO"));			
				linhaOS.append("</td>");			
				linhaOS.append("<td class='"+ class_cor_evento + "'>"+auxTimeAgendada+"</td>");			
				linhaOS.append("<td class='"+ class_cor_evento + "'>"+flag_equip+"</td>");			
				linhaOS.append("<td class='"+ class_cor_evento + "'>"+(cd_cliente_p == "" ? "<a href='javascript:visualizarHistoricoCliente("+cd_cliente+");'><img src='imagens/icones_final/audit_event_query_16x16-trans.png' alt='Hist�rico 60 dias'/></a>" : "") + rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "] "+ rs.getString("FANTASIA") + " ( " + rs.getString("RAZAO") +" )</td>");			
				linhaOS.append("<td class='"+ class_cor_evento + "'>" + rs.getString("ENDERECO") + ", " + rs.getString("NOMEBAIRRO") + ", " + rs.getString("NOMECIDADE") + "</td>");
				linhaOS.append("</tr>");		
			}

		}
		out.print(linhaOS.toString());		
		out.print("<tr class='linha_viatura'>");
		out.print("<td style='font-weight: normal' colspan='2'>&nbsp;</td>");
		if (cd_tecnico == "" && cd_cliente_p == "") {
			out.print("<td style='font-weight: bold' colspan='2'> Em atendimento: "+ qtdAtendimentos +"</td>");					
		} else {
			out.print("<td style='font-weight: bold' colspan='2'>&nbsp;</td>");					
		}
		//out.print("<td style='font-weight: bold' colspan='3'> Fechadas: "+ qtdAtendidos + " desde: " + formatter.format(tsAuxUltimo) + "</td>");
		out.print("<td style='font-weight: normal' colspan='5'>&nbsp;</td>");
		out.print("</tr>");
		
			
	} catch (Exception e) {
		e.printStackTrace();
		erro = e.getMessage();

	}finally {
		try {
		
			OrsegupsUtils.closeConnection(null, st3, rsEquip);
			OrsegupsUtils.closeConnection(connSapiens, st2, rs2);
			OrsegupsUtils.closeConnection(conn, st, rs);

		} catch (Exception e) {
			e.printStackTrace();
			erro = e.getMessage();
		}
		}
%>
	</tbody>
	</table>
<%
	if (cd_tecnico == "" && cd_cliente_p == "") {
%>
	<input onClick="javascript:document.location.reload();" type="button" value="Atualizar"/></a>
<%
	}
%>

<%
	if (erro != "") {
%>				
			
			 <script type="text/javascript"> 
				var erroJavaScript = "<%=erro%>";			 
    			$.messager.alert('Erro', erroJavaScript,'error');
			</script> 
<%
	}
%>
	
	<br>

	
	</cw:body>
</cw:main>
</portal:head>


<script language="JavaScript">

function imprimeRoteiro(cd_tecnico) {
	window.open('http://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/sigma/atendimentoOSRoteiroTecnico.jsp?cd_tecnico=' + cd_tecnico,'','toolbar=yes,scrollbars=yes,width=1000,height=500')
}
function visualizarHistorico(cd_tecnico) {
	window.open('http://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/sigma/atendimentoOS.jsp?cd_tecnico=' + cd_tecnico,'','toolbar=yes,scrollbars=yes,width=1000,height=500')
}
function visualizarHistoricoCliente(cd_cliente) {
	window.open('http://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/sigma/atendimentoOS.jsp?cd_cliente=' + cd_cliente,'','toolbar=yes,scrollbars=yes,width=1000,height=500')
}


function saveInputLog(idEntidade, tipo)
{
	var label = "Informar Log do T�cnico/Terceiro:";
	
	texto = prompt(label,"");
	
	if(texto != null && texto != "")
	{
		var	result = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.sigma.servlet.SigmaServlet?action=saveLog&tipo="+tipo+"&idEntidade="+idEntidade+"&texto="+JSON.stringify(texto));
		
		alert(result);
	}
}


function getLogOs(idOs)
{
	var result = 'Vazio!';
	if(idOs != null && idOs != "")
	{
		
		result = callSync("<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.sigma.servlet.SigmaServlet?action=getLog&idOs="+idOs);
		
		return result;
	}
	
	return result;
}

function editOS(idOrdem) {
	var caminho = '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/sigma/editOS.jsp?idOrdem='+idOrdem;

	$('#modalEditOS').window('open');
	$('#modalEditOS').window('refresh', caminho);
}
</script>