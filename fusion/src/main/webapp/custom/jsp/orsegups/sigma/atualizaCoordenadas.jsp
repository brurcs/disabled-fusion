<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLNotInFilter"%>
<%@page import="com.neomind.fusion.persist.QLOpFilter"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.datawarehouse.ExternalEntityInfo"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.neomind.fusion.portal.PortalKeys"%>
<%@page import="com.neomind.fusion.security.UserSessionControl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="java.util.SortedSet"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.*"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.Timestamp"%>
<%@page import="java.net.*"%>
<%@page import="java.io.*"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="org.xml.sax.SAXException"%>
<%@page import="org.xml.sax.SAXParseException"%> 
<%@page import="org.w3c.dom.Document"%> 
<%@page import="org.w3c.dom.*"%> 
<%		
	String cd_cliente = request.getParameter("cd_cliente");
	if(cd_cliente == null || cd_cliente.trim().equals(""))	{
		cd_cliente = "";
 	}
	String limit = request.getParameter("limit");
	if(limit == null || limit.trim().equals(""))	{
		limit = "0";
 	}
	String start = request.getParameter("start");
	if(start == null || start.trim().equals(""))	{
		start = "0";
 	}

	boolean show = false;
	String stringShow = request.getParameter("show");
	if(stringShow != null && stringShow.trim().equals("sim"))	{
		show = true;
 	}

%>

<%
if (show) {
%>
	<input onClick="javascript:document.location.reload();" type="button" value="Atualizar"/></a>
	
	<table class="gridbox gridboxNoHover" cellpadding="0" cellspacing="0">
	<thead style="cursor: auto">
		<tr style="cursor: auto">
			<th style="cursor: auto">CD_CLIENTE</th>
			<th style="cursor: auto">ENDERECO.</th>
			<th style="cursor: auto">BAIRRO</th>
			<th style="cursor: auto">CIDADE</th>
			<th style="cursor: auto">UF</th>
			<th style="cursor: auto">LAT</th>
			<th style="cursor: auto">LONG</th>
		</tr>
	</thead>
	<tbody>	
<%
} //if show
%>	
<%!

	public final String getString(String tagName, Element element) {
		NodeList list = element.getElementsByTagName(tagName);
		if (list != null && list.getLength() > 0) {
			NodeList subList = list.item(0).getChildNodes();

			if (subList != null && subList.getLength() > 0) {
				return subList.item(0).getNodeValue();
			}
		}
		return null;
	}
%>
<%
	final String none = "&nbsp;";
	int count = 0;

	PreparedStatement st = null;
	try {
		String nomeFonteDados = "SIGMA90";
		Connection conn = PersistEngine.getConnection(nomeFonteDados);
 		
		StringBuffer sql1 = new StringBuffer();
		sql1.append("SELECT c.CD_CLIENTE, c.ENDERECO, bai.NOME AS NOMEBAIRRO, cid.NOME AS NOMECIDADE, uf.NOME AS NOMEUF, cid.ID_ESTADO, c.NU_LATITUDE, c.NU_LONGITUDE ");
		sql1.append("FROM dbCENTRAL c ");
		sql1.append("INNER JOIN dbCIDADE cid ON c.ID_CIDADE = cid.ID_CIDADE ");
		sql1.append("INNER JOIN dbBAIRRO bai ON c.ID_BAIRRO = bai.ID_BAIRRO AND c.ID_CIDADE = bai.ID_CIDADE ");
		sql1.append("INNER JOIN dbESTADO uf ON cid.ID_ESTADO = uf.ID_ESTADO ");
		//sql1.append("WHERE c.ID_EMPRESA = 10052 AND c.CTRL_CENTRAL = 1 AND (c.NU_LATITUDE IS NULL OR c.NU_LONGITUDE IS NULL)           ");
		if (cd_cliente != "") {
			sql1.append("WHERE c.CD_CLIENTE = "+ cd_cliente+" ");
		} else {
			sql1.append("WHERE (c.NU_LATITUDE IS NULL OR c.NU_LONGITUDE IS NULL) ");
			sql1.append("AND c.CTRL_CENTRAL = 1 ");
			sql1.append("AND c.CD_CLIENTE >= "+ start + "  ");
			sql1.append("AND c.CD_CLIENTE <= "+ limit + "   ");
		}
		sql1.append("ORDER BY c.CD_CLIENTE ");
			

		st = conn.prepareStatement(sql1.toString());
		ResultSet rs = st.executeQuery();
			
		while(rs.next()) {
			String lat = null;
			String lng = null;
			
			String end = rs.getString("ENDERECO");
			String nomcid = rs.getString("NOMECIDADE");
			String uf = rs.getString("NOMEUF");
			
			end = end.replaceAll("[�����]", "a")   
	                .replaceAll("[����]", "e")   
	                .replaceAll("[����]", "i")   
	                .replaceAll("[�����]", "o")   
	                .replaceAll("[����]", "u")   
	                .replaceAll("[�����]", "A")   
	                .replaceAll("[����]", "E")   
	                .replaceAll("[����]", "I")   
	                .replaceAll("[�����]", "O")   
	                .replaceAll("[����]", "U")   
	                .replace('�', 'c')   
	                .replace('�', 'C')   
	                .replace('�', 'n')   
	                .replace('�', 'N')
	                .replaceAll("!", "")	                
	                .replaceAll ("\\[\\�\\`\\?!\\@\\#\\$\\%\\�\\*"," ")
	                .replaceAll("\\(\\)\\=\\{\\}\\[\\]\\~\\^\\]"," ")
	                .replaceAll("[\\.\\;\\-\\_\\+\\'\\�\\�\\:\\;\\/]"," ");
			
			nomcid = nomcid.replaceAll("[�����]", "a")   
		                .replaceAll("[����]", "e")   
		                .replaceAll("[����]", "i")   
		                .replaceAll("[�����]", "o")   
		                .replaceAll("[����]", "u")   
		                .replaceAll("[�����]", "A")   
		                .replaceAll("[����]", "E")   
		                .replaceAll("[����]", "I")   
		                .replaceAll("[�����]", "O")   
		                .replaceAll("[����]", "U")   
		                .replace('�', 'c')   
		                .replace('�', 'C')   
		                .replace('�', 'n')   
		                .replace('�', 'N')
		                .replaceAll("!", "")	                
		                .replaceAll ("\\[\\�\\`\\?!\\@\\#\\$\\%\\�\\*"," ")
		                .replaceAll("\\(\\)\\=\\{\\}\\[\\]\\~\\^\\]"," ")
		                .replaceAll("[\\.\\;\\-\\_\\+\\'\\�\\�\\:\\;\\/]"," ");
			
			uf = uf.replaceAll("[�����]", "a")   
		                .replaceAll("[����]", "e")   
		                .replaceAll("[����]", "i")   
		                .replaceAll("[�����]", "o")   
		                .replaceAll("[����]", "u")   
		                .replaceAll("[�����]", "A")   
		                .replaceAll("[����]", "E")   
		                .replaceAll("[����]", "I")   
		                .replaceAll("[�����]", "O")   
		                .replaceAll("[����]", "U")   
		                .replace('�', 'c')   
		                .replace('�', 'C')   
		                .replace('�', 'n')   
		                .replace('�', 'N')
		                .replaceAll("!", "")	                
		                .replaceAll ("\\[\\�\\`\\?!\\@\\#\\$\\%\\�\\*"," ")
		                .replaceAll("\\(\\)\\=\\{\\}\\[\\]\\~\\^\\]"," ")
		                .replaceAll("[\\.\\;\\-\\_\\+\\'\\�\\�\\:\\;\\/]"," ");
			String sURL = end+" - " + nomcid + " - "+ uf;
			String encodedURL = URLEncoder.encode(sURL.toString(),"UTF-8");
			System.out.println(end + "," + nomcid + "," + uf);
			System.out.println(encodedURL);
			URL url = new URL("https://maps.google.com/maps/api/geocode/xml?sensor=false&key=AIzaSyC1rjshuDpxMagmiBBztVi47gEGmLi3YPk&address=" + encodedURL);
			URLConnection uc = url.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
			String inputLine;
			//while ((inputLine = in.readLine()) != null) 
			//	System.out.println(inputLine);
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.parse (uc.getInputStream());			
			in.close();	
				
			Element rootElement = doc.getDocumentElement();
			lat = getString("lat", rootElement);
			lng = getString("lng", rootElement);

			if (show) {
				out.print("<tr>");		
				out.print("<td> " + rs.getString("CD_CLIENTE") + "</td>");
				out.print("<td> " + rs.getString("ENDERECO") + "</td>");
				out.print("<td> " + rs.getString("NOMEBAIRRO") + "</td>");
				out.print("<td> " + rs.getString("NOMECIDADE") + "</td>");
				out.print("<td> " + rs.getString("ID_ESTADO") + "</td>");
				out.print("<td> " + lat + "</td>");
				out.print("<td> " + lng + "</td>");
				out.print("</tr>");
			} 
			if (lat != null) {
				count++;
			}
			StringBuffer sql2 = new StringBuffer();
			sql2.append("UPDATE dbCENTRAL SET NU_LATITUDE = ?, NU_LONGITUDE = ?, DT_ULTIMA_LOCALIZACAO = GETDATE() WHERE CD_CLIENTE = ? ");
			PreparedStatement st2 = conn.prepareStatement(sql2.toString());
			st2.setString(1, lat);
			st2.setString(2, lng);
			st2.setLong(3, rs.getLong("CD_CLIENTE"));
			st2.executeUpdate();
			st2.close();
			
			System.out.println(count + ":" + rs.getString("CD_CLIENTE") + ": " + lat + "," + lng);
			
		}
		rs.close();
		st.close();		
	} catch (Exception e) {
		out.print("Erro ao tentar localizar coordenadas: " + e.getMessage());
		e.printStackTrace();	
	}
	if (!show) {
		if (count > 0) {
			out.print("OK");
		} else {
			out.print("Coordenadas n�o localizadas.");
		}
	}
	
%>
<%
if (show) {
%>
	</tbody>
	</table>
	
	<br>

	
<%
} //if show
%>