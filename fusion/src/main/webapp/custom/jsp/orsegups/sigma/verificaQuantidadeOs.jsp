<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>

<%
	PreparedStatement stSigma = null;
	String cd_cliente = request.getParameter("cd_cliente");
	Long id_ordem = Long.valueOf(request.getParameter("id_ordem"));
	String ordem = id_ordem.toString();

	ResultSet rsSigma = null;
	int cont = 0;
	String ordens = "";
	Connection connSapiens = PersistEngine.getConnection("SIGMA90");

	try
	{
		String sqlSigma = "";
		sqlSigma = "SELECT ID_ORDEM, CD_CLIENTE FROM DBORDEM WHERE IDOSDEFEITO IN (184,178,134,167,872) AND CD_CLIENTE in (" + cd_cliente + ") AND FECHAMENTO is null order by ID_ORDEM";

		stSigma = connSapiens.prepareStatement(sqlSigma.toString());

		rsSigma = stSigma.executeQuery();

		while (rsSigma.next())
		{

			String idOrdem = rsSigma.getString("id_ordem");
			if (ordem!=idOrdem)
			{
				ordens = ordens + "," + idOrdem;
				String idCliente = rsSigma.getString("cd_cliente");
				cont++;

			}
		}

		if (cont != 1)
		{
			out.print("Existem outras OS neste Contrato " + ordens + " Deseja Continuar com a OS " + id_ordem + "?[&N�o,&Sim]");
			return;
		}

	}
	catch (Exception e)
	{
		e.printStackTrace();
	}
	finally
	{

		OrsegupsUtils.closeConnection(connSapiens, stSigma, rsSigma);

	}
%>
