<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>JMS Console</title>

<link rel="stylesheet" type="text/css" href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jnotify/jquery/jNotify.jquery.css" media="screen" />

<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/amq_jquery_adapter.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/amq.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>core/portal/portal.js.jsp"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jnotify/jquery/jNotify.jquery.js"></script>

<style type="text/css">

.modal {
    display:    none;
    position:   fixed;
    z-index:    1000;
    top:        0;
    left:       0;
    height:     100%;
    width:      100%;
    background: rgba( 255, 255, 255, .8 ) 
                url('http://sampsonresume.com/labs/pIkfp.gif') 
                50% 50% 
                no-repeat;
}

/* When the body has the loading class, we turn
   the scrollbar off with overflow:hidden */
body.loading {
    overflow: hidden;   
}

/* Anytime the body has the loading class, our
   modal element will be visible */
body.loading .modal {
    display: block;
}

</style>

</head>
<body>


<div id="all">
	<div id="cabecalho">
		<textarea id="pesquisa" rows="4" cols="50"></textarea>
		<input type="button" onclick="iniciaTestes()" value="Iniciar testes">
	</div>
	<div id="corpo">
		<div style="float: left;border: outset 1px;  width: 46%;  height: 500px; " id="esquerda">
			<div style=" height: 25px; background-color: cadetblue; padding-left: 5px;"> 
				<span style=" color: white;" id="console-esquerda-title">Mensagens Enviadas</span>
			</div>
			<div id="console-esquerda" style="height: 455px; margin: 10px; overflow: auto;"></div>
		</div>
		
		<div style="float: right;border: outset 1px;  width: 46%;  height: 500px; " id="direita">
			<div style=" height: 25px; background-color: cadetblue; padding-left: 5px;"> 
				<span style=" color: white;" id="console-direita-title">Mensagens Recebidas</span>
			</div>
			<div id="console-direita" style="height: 455px; margin: 10px; overflow: auto;"></div>
		</div>
	</div>
</div>
<div class="modal"></div>
</body>

<script type="text/javascript">

var fila = new Array();
var enviadas = 0;
var recebidas = 0;

function iniciaTestes(){
	limpaTudo();
	populaArray();
	pesquisar();
}

function limpaTudo(){
	fila = new Array();
	enviadas = 0;
	recebidas = 0;
	$("#console-esquerda").html("");
	$("#console-direita").html("");
}

function populaArray(){
	var text = $("#pesquisa").val();
	var valores = text.split(";");
	
	for (var x in valores){
		fila.push({nome:valores[x], enviado:false, recebido:false});
	}
}

function pesquisar(){
	for (var x in fila){
		var searchObject = new Object();
		searchObject.tipoBusca = "quickSearch";
		searchObject.termoGenerico = fila[x]["nome"];
		var jsonSearchObject = JSON.stringify(searchObject);
		//chama a url da servlet do fusion
		
		$.ajax({
  			url: "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.callcenter.OrsegupsIncommingCallServlet?searchObject="+jsonSearchObject+"&extensionNumber=6606&r" + Math.random(),
  			context: document.body
		});
		//seta como enviado
		fila[x]["enviado"] = true;
		
		startLoading();
		
		enviadas++;
		$("#console-esquerda-title").html("Mensagens Enviadas - " + enviadas);
		$("#console-esquerda").append("<span> " + fila[x]["nome"] + "</span><br />");
	}
}

var amq = org.activemq.Amq;
amq.init({ 
  	uri: '<%=PortalUtil.getBaseURL()%>amq',
	logging : true,
	timeout: 45, 
	clientId:(new Date()).getTime().toString() 
});

var myHandler =
{
  rcvMessage: function(message)
  {
	  	var decodedMessage = decodeURIComponent(message.data.replace(/\+/g,  " "));
		var json =  JSON.parse(decodedMessage);
	  
		recebidas++;
		$("#console-direita-title").html("Mensagens Recebidas - " + recebidas);
	  	$("#console-direita").append("<span>Chegou uma mensagem</span><br />");
	  	
	  	stopLoading();
  }
};

amq.addListener('callCenterListener',"topic://messageTopic" ,myHandler.rcvMessage, { selector:"identifier='6606'" });

var startLoading = function(){
    $("body").addClass("loading");
    setTimeout('error()', 10000);
};

var error = function(){
	if ($("body").hasClass("loading")){
	 	$("body").removeClass("loading");
		jError('Ocorreu um erro ao realizar a pesquisa',
	   	{
	   		autoHide : true,
	   		clickOverlay : false,
	   		MinWidth : 250,
	   		TimeShown : 3000,
	   		ShowTimeEffect : 200,
	   		HideTimeEffect : 200,
	   		LongTrip :20,
	   		HorizontalPosition : 'center',
	   		VerticalPosition : 'center',
	   		ShowOverlay : true,
	      	ColorOverlay : '#000',
	   		OpacityOverlay : 0.3,
	   	});
	}
}

var stopLoading = function(){
    $("body").removeClass("loading");
};

</script>

</html>