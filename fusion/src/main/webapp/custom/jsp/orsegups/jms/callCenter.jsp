<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.i18n.I18nUtils"%>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />

<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/icon.css">
<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jnotify/jquery/jNotify.jquery.css" media="screen" />

<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>core/portal/portal.js.jsp"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/amq_jquery_adapter.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/amq.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.mask.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.validate.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.callcenter.validade.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jnotify/jquery/jNotify.jquery.js"></script>
	
<style type="text/css">

#bodyId div{
	color: #08285a;
	font-family: Tahoma, Arial, Verdana,"Trebuchet MS";
}

#detalhesContrato ul {
	margin-top: 5px;
	margin-left: 5px;
	margin-right: 5px;
	margin-bottom: 5px;
	padding: 0;
}

#detalhesContrato ul li{
	margin: 0;
	padding: 0;
	float: left;
	width: 50%;
	list-style: none;
	font-size: 12px;
}

#detalhesContrato ul li span{
	font-weight: bold;
}

#detalhesContrato ul fieldset {
	border: 0;
	background: #e7efff;
	margin-bottom: 5px;
	padding-bottom: 10px;
	text-indent: 10px;
}

#detalhesContrato ul fieldset legend{
	font-weight: bold;
	font-size: 12px;
	background: transparent;
}

#buscaAvancadaWest ul {
	margin-top: 0px;
	margin-left: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	padding: 0;
}

#buscaAvancadaWest ul li{
	margin: 0;
	padding: 0;
	float: left;
	width: 100%;
	list-style: none;
	font-size: 12px;
}

#buscaAvancadaWest ul li span{
	font-weight: normal;
	width:100px;
	display:inline-block;
}

#buscaAvancadaWest ul fieldset {
	border: 0;
	background: #e7efff;
	margin-bottom: 5px;
	padding-bottom: 10px;
	text-indent: 0px;
}

.destaque{
	font-weight: bold;
	font-size: 12px;
	color: #dd0005;
}

.ramal{
	padding: 5px;
	font-weight: bold;
	font-size: 12px;
}

.externalNumber{
	padding: 5px;
	font-weight: bold;
	font-size: 12px;
}

.modal {
    display:    none;
    position:   fixed;
    z-index:    1000;
    top:        0;
    left:       0;
    height:     100%;
    width:      100%;
    background: rgba( 255, 255, 255, .8 ) 
                url('<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/images/loading.gif') 
                50% 50% 
                no-repeat;
}

/* When the body has the loading class, we turn
   the scrollbar off with overflow:hidden */
body.loading {
    overflow: hidden;   
}

/* Anytime the body has the loading class, our
   modal element will be visible */
body.loading .modal {
    display: block;
}

.invisible {
    display: none;
}

.visible {
    display:block;
}

.left{
    float: left;
    margin-right: 3px;
}

.fieldContainer {
	display: inline-block;
}

.spanValue {
	margin-top: 3px;
}
	
.ico {
	width: 12px
}
.iconDat {
  		cursor:pointer;
  		width:16px;
   		height:16px;
   		float: left;
   		background: url('<%=PortalUtil.getBaseURL()%>imagens/custom/papel_16x16.png') no-repeat;
	}
.txtDat{
     background:red; 
     border-width:1px;
     border-color:black;
     color:white;
     font-weight:bold;
     font-size:9px;
     margin-top:6px;
     margin-left:5px;
     text-align: center;
      padding-right: 1px;
}

.float-rigth{
	float:right;
}

.info-empresa-green{
	color: #5a9c41;
	font-size: 36px;
	margin-left: 20px;
}

.info-empresa-red{
	color: #dd0005;
	font-size: 36px;
	margin-left: 20px;
}

.info-empresa-blue{
	color: blue;
	font-size: 36px;
	margin-left: 20px;
}

.info-empresa-purple{
	color: #570054;
	font-size: 30px;
	margin-left: 20px;
}

.info-empresa-darkblue{
	color: #17415a;
	font-size: 30px;
	margin-left: 20px;
}
</style>
<%
	String showTab = request.getParameter("showTab");
	
	NeoUser currentUser = PortalUtil.getCurrentUser();
	EntityWrapper ewUser = new EntityWrapper(currentUser);
    String extensionNumber = (String)ewUser.findValue("ramal");
	
	if (currentUser != null){
		String groupName = currentUser.getGroup().getName();
		if (groupName.equalsIgnoreCase("Call Center")){
			showTab = "contratos";
		} else if(groupName.equalsIgnoreCase("Central de Monitoramento")){
			showTab = "sigma";
		}
	}
	
	
%>
<script type="text/javascript">
	
	var extensionNumber = '<%=extensionNumber%>';
	var showTab = '<%=showTab%>';
	var processWindow = null;
	var flag = 0;
	var telefoneCliente;
	var atendendo = false;
	var timeOutLigacao;
	var tamanhoResultado = 0;
	var id = null;
	var nomeUsuario = '<%=PortalUtil.getCurrentUser().getCode()%>';
	var codigoClienteSelecionado = 0;
	var externalNumber;
	var local = '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/icons/';
	
	$(document).ready(function() {
			$('#ttcontrato').treegrid({
				onClickRow: function(row){
					showContractDetail(row);
					showSigmaDetailOSPause(row);
				}
			});
			
			$('#ttsigma').treegrid({
				onClickRow: function(row){
					showSigmaDetail(row);
				}
			});
			
			$('#ttsigma').treegrid({
				onClickRow: function(row){
					showSigmaDetailOSPause(row);
				}
			});
			
			$('#tabsId').tabs({
				
	    	    onSelect: function(title, index){
	    			var tab = $('#tabsId').tabs('getSelected');
	    			var hest = $('#tabsId').tabs('getTabIndex',tab);
	    			if(hest == 2 && flag == 0){
	    				getAtualizaListaClientes();
	    				createTooltip();
	    				flag = 1;
	    				
	    			}
	    				
	    	    }
	 	   });
			
			
			/*
			$('#wfprocess').window({
				onLoad: function(){
					resizeProcessIframe();
				},
				onResize: function(newWidth, newHeight){
					resizeProcessIframe();
				}
			});
			*/
			
			$('#tabsId').tabs({
				tools:'#tab-tools'
			});
			
			if(showTab == 'sigma')
			{
				$('#tabsId').tabs('select', 'Contas Sigma');
				$('#idTabsProvOS').tabs('select', 'Provid�ncia');
			} else if (showTab == 'contratos'){
				$('#tabsId').tabs('select', 'Contratos');
			}
		
// 			createEditableField("cnpjCpfSigma", 'codigoCliente');
// 			createEditableField("emailCentral","codigoCliente");
			
			//createEditableField("enderecoCobranca","codigo");
			//createEditableField("numeroCobranca","codigo");
			//createEditableField("complementoCobranca","codigo");
			//createEditableField("cepCobranca","codigo");
			//createEditableField("cidadeCobranca","codigo");
			//createEditableField("ufCobranca","codigo");
			
			
			//createEditableField("emailCliente","codigo");
			//createEditableField("emailContrato","codigo");
			//createEditableField("telefone1","codigo");
			//createEditableField("telefone2","codigo");
			//createEditableField("telefone3","codigo");
			//createEditableField("telefone4","codigo");
			//createEditableField("telefone5","codigo");
			//createEditableField("foneContrato","codigo")
			//createEditableField("foneFax","codigo");
			
			$("#formPrincipal").validate({
				rules: {
					cnpjCpfSigma: {cpfCnpj : true},
					cnpjCpf: {cpfCnpj : true}
				},
				messages: {
					cnpjCpfSigma: {cpfCnpj: 'CPF/CNPJ Inv�lidos'},
					cnpjCpf: {cpfCnpj: 'CPF/CNPJ Inv�lidos'}
				}
			});
			
			removeEditButtons();
			
			initialize();
			
	});
	
	function initialize() 
	{
		
		var id = '<%=PortalUtil.getCurrentUser().getCode()%>';
		
		var amq = org.activemq.Amq;
		  amq.init({ 
		    uri: '<%=PortalUtil.getBaseURL()%>amq',
			logging : true,
			timeout: 45, 
			clientId:(id) 
		});
		  reloadData();  
		amq.addListener('callCenterListener',"topic://messageTopic" ,myHandler.rcvMessage, { selector:"identifier='"+extensionNumber+"'" });
		
	}
	
	function reloadData(){
	
		var d = new Date();
		var m = d.getMilliseconds() + (d.getSeconds() * 1000);
		var load;
		if (m < 20000)
		{
			load = 20000 - m;
		} 
		else if (m < 40000)
		{
			load = 40000 - m;
		} 
		else
		{
			load = 60000 - m;
		}
		getAtualizaListaClientes();
		timeOutLigacao = setTimeout("reloadData()",load);
		createTooltip();
	

	}
	
	var myHandler = {
		rcvMessage : function(message) {
			
			var decodedMessage = decodeURIComponent(message.data.replace(/\+/g,  " "));
			var callCenterVOList =  JSON.parse(decodedMessage);
			
			console.log(callCenterVOList);
			
			var contratos = callCenterVOList.contratos;
			var contas = callCenterVOList.contas;
			//var clientes = callCenterVOList.clientes;
			
			externalNumber = callCenterVOList.externalNumber;
			
			if (externalNumber != null){
				$("#externalNumber").html('Telefone: ' + externalNumber);
			} else {
				$("#externalNumber").html('');
			}
			
			$('#ttcontrato').treegrid('loadData', contratos);
			$('#ttsigma').treegrid('loadData', contas);
			
			
			cleanContractData();
			cleanSigmaData();
			
			if (contratos.length == 0 && contas.length == 0){
				removeEditButtons();
				callCenterAlert('A pesquisa n�o encontrou nada');
			} else if (contratos.length > 0 && contas.length == 0){
				$('#tabsId').tabs('select', 'Contratos');
			}else if (contratos.length == 0 && contas.length > 0){
				$('#tabsId').tabs('select', 'Contas Sigma');
				$('#idTabsProvOS').tabs('select', 'Provid�ncia');
				
			}
			
			//seleciona e carrega os detalhes do primeiro contrato
			if(contratos != null && contratos.length > 0)
			{
				addEditButtons();
				var primeiroCliente = contratos[0];
				if(primeiroCliente != null && primeiroCliente.children != null && primeiroCliente.children.length > 0)
				{
					var primeiroContrato = primeiroCliente.children[0];
					
					if(primeiroContrato != null)
					{
						$('#ttcontrato').treegrid('select', primeiroContrato.id);
						showContractDetail(primeiroContrato);
						showSigmaDetailOSPause(primeiroContrato);
					}
				}
			}
			
			//seleciona e carrega os detalhes da primeira conta
			if(contas != null && contas.length > 0)
			{
				addEditButtons();
				var primeiraConta = contas[0];
				if(primeiraConta != null)
				{
					$('#ttsigma').treegrid('select', primeiraConta.id);
					showSigmaDetail(primeiraConta);
				}
			}
			stopLoading();
		}
	};
	
	function getAtualizaListaClientes(){ 
		var callBack = 'out';
		$.ajax({
            	method: 'post',
                url: "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.callcenter.CallCenterServlet?action=buscarLigacaoRecebida&callBack="+callBack+"&r" + Math.random(),
                dataType: 'json',
                success: function(result){ 
					$('#ttligacao').datagrid('loadData', new Array());
                	$('#ttligacao').datagrid('loadData', result);
                	createTooltip();
					$('#ttligacao').datagrid('load', new Array());
                	$('#ttligacao').datagrid('load',result);
                	verificarNovoNumero();	
                },  
       			error: function(e){  
					 alerta('error','Erro ao carregar informa��es!\n'+e);
      			}
            });
    }
	
	
	
	
	function seeProcess(processType)
	{
		var codigoCliente = '';
		var row = $('#ttsigma').treegrid('getSelected');
		if(row != null )
		{
			codigoCliente = row.codigoCliente;
			codigoClienteSelecionado = codigoCliente;
		}
		
		var servlet = '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/listaOSAbertas.jsp?codigoCliente='+codigoCliente;
		window.open(servlet,'','location=no, toolbar=no, width=1200, height=400, left=200, top=200');
	}
	
	function seeProcessOSPausadas(processType)
	{
		var codigoCliente = '';
		var row = $('#ttsigma').treegrid('getSelected');
		if(row != null )
		{
			codigoCliente = row.codigoCliente;
			codigoClienteSelecionado = codigoCliente;
		}
		
		var servlet = '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/listaOSPausada.jsp?codigoCliente='+codigoCliente;
		window.open(servlet,'','location=no, toolbar=no, width=1200, height=400, left=200, top=200');
	}
	
	
	function seeRSC(processType)
	{
		var codigoCliente = '';
		var row = $('#ttcontrato').treegrid('getSelected');
		if(row != null )
		{
			codigoCliente = row.codigo;
		}
		
		var servlet = '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/listaRSC.jsp?codigoCliente='+codigoCliente;
		window.open(servlet,'','location=no, toolbar=no, width=1200, height=400, left=200, top=200');
	}
	
	function seeRRC(processType)
	{
		var codigoCliente = '';
		var row = $('#ttcontrato').treegrid('getSelected');
		if(row != null )
		{
			codigoCliente = row.codigo;
		}
		
		var servlet = '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/listaRRC.jsp?codigoCliente='+codigoCliente;
		window.open(servlet,'','location=no, toolbar=no, width=1200, height=400, left=200, top=200');
	}
	
	function startProcess(processType)
	{
		var codigoCliente = '';
		if(processType == 'OsSigma')
		{
			var row = $('#ttsigma').treegrid('getSelected');
			if(row != null )
			{
				codigoCliente = row.codigoCliente;
				codigoClienteSelecionado = codigoCliente;
			}
		}
		else
		{
			var row = $('#ttcontrato').treegrid('getSelected');
			if(row != null )
			{
				codigoCliente = row.codigo;
				codigoClienteSelecionado = codigoCliente;
			}
		}
		
		var startProcessUrl = "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.callcenter.CallCenterStartProcessServlet?processType="+processType+"&codigoCliente="+codigoCliente;
		
		var taskUrl = callSync(startProcessUrl);
		if(taskUrl != null)
		{
			var result = taskUrl.split(";");
			
			if(result[0] == "1")
			{
				window.open(result[1], 'callcenterProcess');
			}
			else
			{
				alert(result[1]);
			}
		}
			
		
		//$('#wfprocess').window('open');
		//$('#wfprocess').window('refresh', processUrl);
	}
	
	/*
	function resizeProcessIframe()
	{
		var windowHeight = $('#wfprocess').height();
		var windowWidth = $('#wfprocess').width();
		
		if($('#processIframe') != null)
		{
			$('#processIframe').height(windowHeight);
			$('#processIframe').width(windowWidth);
		}
	}
	*/
	
	/*
	function changePortlet(wName, callBackURL)
	{
		//$('#wfprocess').window('close');
		if(processWindow.closed == false)
		{
			processWindow.close();
		}
	}
	*/
	
	function searchClient(value)
	{
		if(value != null && value != "")
		{	
			cleanContractData();
			cleanSigmaData();
			
			$('#ttcontrato').treegrid('loadData', new Array());	// loadData
			$('#ttsigma').treegrid('loadData', new Array());	// loadData
			$('#ttsigmaOSPause').treegrid('loadData', new Array());
			//$('#ttligacao').treegrid('loadData', new Array());
			var searchObject = new Object();
			searchObject.tipoBusca = "quickSearch";
			searchObject.termoGenerico = value;
			
			var jsonSearchObject = JSON.stringify(searchObject);
			
			startLoading();
			
			$.ajax({
	  			url: "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.callcenter.OrsegupsIncommingCallServlet?searchObject="+jsonSearchObject+"&extensionNumber="+extensionNumber+"&codCli="+codigoClienteSelecionado+"&r=" + Math.random(),
	  			context: document.body
			});
			
		}
		else
		{
			alert("Informe um valor para pesquisar!");
		}
	}
	
	function advancedSearch()
	{
		var searchObject = new Object();
		
		searchObject.tipoBusca = "advancedSearch";
		searchObject.codigoClienteSearch = $("#codigoClienteSearch").val();
		searchObject.razaoSearch = $("#razaoSearch").val();
		searchObject.cpfCnpjSearch = $("#cpfCnpjSearch").val();
		searchObject.telefoneSearch = $("#telefoneSearch").val();
		searchObject.enderecoSearch = $("#enderecoSearch").val();
		searchObject.bairroSearch = $("#bairroSearch").val();
		searchObject.cidadeSearch = $("#cidadeSearch").val();
		searchObject.ufSearch = $("#ufSearch").val();
		searchObject.cepSearch = $("#cepSearch").val();
		searchObject.emailSearch = $("#emailSearch").val();
		
		var jsonSearchObject = JSON.stringify(searchObject);
		
		startLoading();
		
		$.ajax({
  			url: "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.callcenter.OrsegupsIncommingCallServlet?searchObject="+jsonSearchObject+"&extensionNumber="+extensionNumber+"&r" + Math.random(),
  			context: document.body
		});
		
		cleanAdvancedSearchFields();
		
		$('#contaSigmaLayout').layout('collapse','east');
	}
	
	function openAdvancedSearchBox()
	{
		
		$('#contaSigmaLayout').layout('expand','east');
	}
	
	
	function closeAdvancedSearchBox()
	{
		
		$('#contaSigmaLayout').layout('collapse','east');
	}
	
	function cleanAdvancedSearchFields()
	{
		$("#codigoClienteSearch").val("");
		$("#razaoSearch").val("");
		$("#cpfCnpjSearch").val("");
		$("#telefoneSearch").val("");
		$("#enderecoSearch").val("");
		$("#bairroSearch").val("");
		$("#cidadeSearch").val("");
		$("#ufSearch").val("");
		$("#cepSearch").val("");
	}
	
	function showSigmaDetail(row)
	{
		//DADOS GERAIS DO CLIENTE
		$("#codigoCliente").html(row.codigoCliente);
		$("#codigoEmpresa").html(row.codigoEmpresa);
		$("#razaoSocialSigma").html(row.razaoSocial);
		$("#cnpjCpfSigma").html(row.cnpjCpf);
		$("#senhaSiteSigma").html(row.senhaSiteSigma);
		
		var empresa = row.complementoEmpresa;
		
		var content;
		
		if (empresa == "AUTOCARGO"){
			content = "<span class=\"info-empresa-purple\" \"><b>"+empresa+"<b/></span>"
		}else if (empresa == "VIGZUL"){
			content = "<span class=\"info-empresa-blue\" \"><b>"+empresa+"</b></span>"
		}else if (empresa == "SEYPROL"){
			content = "<span class=\"info-empresa-red\" \"><b>"+empresa+"</b></span>"
		}else if (empresa == "VIASEG"){
			content = "<span class=\"info-empresa-green\" \"><b>"+empresa+"</b></span>"
		}else if (empresa == "SECURISYSTEM"){
			content = "<span class=\"info-empresa-darkblue\" \"><b>"+empresa+"</b></span>"
		}else{
			content = "";
		}
		
		$("#empresaSigma").html(content);
		
		
		//DADOS CONTA/PARTICAO
		$("#codigoCentral").html(row.codigoCentral);
		$("#particao").html(row.particao);
		$("#rota").html(row.rota);
		$("#logradouro").html(row.logradouro);
		$("#bairroSigma").html(row.bairro);
		$("#cidadeSigma").html(row.cidade);
		$("#ufSigma").html(row.uf);
		$("#cepSigma").html(row.cep);
		$("#cepSigma").mask('99999-999');
		
		//CONTATOS - TELEFONES/E-MAIL
		$("#telefoneCentral1").html(row.telefoneCentral1);
		//$("#telefoneCentral1").mask('(99) 9999-9999');
		$("#telefoneCentral2").html(row.telefoneCentral2);
		//$("#telefoneCentral2").mask('(99) 9999-9999');
		$("#emailCentral").html(row.emailCentral);
		$("#responsavelCentral").html(row.responsavelCentral);
		
		$("#propriedadeEquipamento").html(row.equipamento);
		
		//PALAVRA CHAVE
		$("#pergunta").html(row.pergunta);
		$("#resposta").html(row.resposta);
		//LISTAR OS
		if(row.possuiOSAbertas)
// 			$("#showOSAbertas").show();
// 		else
// 			$("#showOSAbertas").show();
		$("#showOSAbertas").show();
		
		//PROVIDENCIA
		if (row.providenciaVOs != undefined && row.providenciaVOs != null){
			$('#dgProvidencia').datagrid('loadData', row.providenciaVOs);
		}
		
		
	}
	
	function showSigmaDetailOSPause(row)
	{
		//DADOS GERAIS DO CLIENTE
		$("#codigoCliente").html(row.codigoCliente);
		$("#codigoEmpresa").html(row.codigoEmpresa);
		$("#razaoSocialSigma").html(row.razaoSocial);
		$("#cnpjCpfSigma").html(row.cnpjCpf);
		$("#senhaSiteSigma").html(row.senhaSiteSigma);
		//DADOS CONTA/PARTICAO
		$("#codigoCentral").html(row.codigoCentral);
		$("#particao").html(row.particao);
		$("#rota").html(row.rota);
		$("#logradouro").html(row.logradouro);
		$("#bairroSigma").html(row.bairro);
		$("#cidadeSigma").html(row.cidade);
		$("#ufSigma").html(row.uf);
		$("#cepSigma").html(row.cep);
		$("#cepSigma").mask('99999-999');
		
		var empresa = row.complementoEmpresa;
		
		var content;
		
		if (empresa == "AUTOCARGO"){
			content = "<span class=\"info-empresa-purple\" \"><b>"+empresa+"<b/></span>"
		}else if (empresa == "VIGZUL"){
			content = "<span class=\"info-empresa-blue\" \"><b>"+empresa+"</b></span>"
		}else if (empresa == "SEYPROL"){
			content = "<span class=\"info-empresa-red\" \"><b>"+empresa+"</b></span>"
		}else if (empresa == "VIASEG"){
			content = "<span class=\"info-empresa-green\" \"><b>"+empresa+"</b></span>"
		}else{
			content = "";
		}
		
		$("#empresaSigma").html(content);
		
		
		//CONTATOS - TELEFONES/E-MAIL
		$("#telefoneCentral1").html(row.telefoneCentral1);
		//$("#telefoneCentral1").mask('(99) 9999-9999');
		$("#telefoneCentral2").html(row.telefoneCentral2);
		//$("#telefoneCentral2").mask('(99) 9999-9999');
		$("#emailCentral").html(row.emailCentral);
		$("#responsavelCentral").html(row.responsavelCentral);
		
		$("#propriedadeEquipamento").html(row.equipamento);
		
		//PALAVRA CHAVE
		$("#pergunta").html(row.pergunta);
		$("#resposta").html(row.resposta);
		//LISTAR OS Pause
// 		if(row.possuiOSAbertas)
// 			$("#showOSPausadas").show();
// 		else
// 			$("#showOSPausadas").show();
		$("#showOSPausadas").show();
		
		//PROVIDENCIA
		if (row.providenciaVOs != undefined && row.providenciaVOs != null){
			$('#dgProvidencia').datagrid('loadData', row.providenciaVOs);
		}
		
		
	}
	
	function showContractDetail(row)
	{
		//var node = $('#ttcontrato').treegrid('getSelected');
		if(row.isContrato)
		{
			//DADOS GERAIS DO CLIENTE
			$("#codigo").html(row.codigo);
			$("#razaoSocial").html(row.razaoSocial);
			$("#nomeFantasia").html(row.nomeFantasia);
			$("#cnpjCpf").html(row.cnpjCpf);
			$("#senhaSite").html(row.senhaSite);
			
			$("#tipoCliente").html(row.tipoCliente);
			$("#tipoContrato").html(row.tipoContrato);
			
			//DADOS ENDERE�O DO CLIENTE
			$("#enderecoCliente").html(row.enderecoCliente);
			$("#complementoCliente").html(row.complementoCliente);
			$("#bairroClient").html(row.bairroClient);
			$("#cepCliente").html(row.cepCliente);
			$("#cepCliente").mask('99999-999');
			$("#cidadeCliente").html(row.cidadeCliente);
			$("#ufCliente").html(row.ufCliente);
			
			//DADOS ENDERE�O DE COBRAN�A
			$("#enderecoCobranca").html(row.enderecoCobranca);
			
			if(row.numeroCobranca != null && row.numeroCobranca != 0)
			{	
				$("#numeroCobranca").html(row.numeroCobranca);
			}
			else
			{
				$("#numeroCobranca").html("");
			}
			
			$("#complementoCobranca").html(row.complementoCobranca);
			$("#cepCobranca").html(row.cepCobranca);
			$("#cepCobranca").mask('99999-999');
			$("#cidadeCobranca").html(row.cidadeCobranca);
			$("#ufCobranca").html(row.ufCobranca);
			
			//CONTATOS - TELEFONES/E-MAIL
			$("#telefone1").html(row.telefone1);
			/*
			if(row.telefone1 != null && row.telefone1 != " ")
			{
				$("#telefone1").mask('(99) 9999-9999');
			}
			*/
			$("#telefone2").html(row.telefone2);
			/*
			if(row.telefone2 != null && row.telefone2 != " ")
			{
				$("#telefone2").mask('(99) 9999-9999');
			}
			*/
			$("#telefone3").html(row.telefone3);
			/*
			if(row.telefone3 != null && row.telefone3 != " ")
			{
				$("#telefone3").mask('(99) 9999-9999');
			}
			*/
			$("#telefone4").html(row.telefone4);
			/*
			if(row.telefone4 != null && row.telefone4 != " ")
			{
				$("#telefone4").mask('(99) 9999-9999');
			}
			*/
			$("#telefone5").html(row.telefone5);
			/*
			if(row.telefone5 != null && row.telefone5 != " ")
			{
				$("#telefone5").mask('(99) 9999-9999');
			}
			*/
			$("#foneFax").html(row.foneFax);
			/*
			if(row.foneFax != null && row.foneFax != " ")
			{
				$("#foneFax").mask('(99) 9999-9999');
			}
			*/
			$("#foneContrato").html(row.foneContrato);
			/*
			if(row.foneContrato != null && row.foneContrato != " ")
			{
				$("#foneContrato").mask('(99) 9999-9999');
			}
			*/
			$("#pessoaContato").html(row.pessoaContato);
			
			$("#emailCliente").html(row.emailCliente);
			$("#emailContrato").html(row.emailContrato);
			
			//DADOS DO CONTRATO
			$("#numeroContrato").html(row.numeroContrato);
			$("#numeroOficialContrato").html(row.numeroOficialContrato);
			$("#regionalContrato").html(row.regionalContrato);
			$("#situacaoContrato").html(row.situacaoContrato);
			$("#codigoServico").html(row.codigoServico);
			$("#dataBaseContrato").html(row.dataBaseContrato);
			$("#formPagamentoContrato").html(row.formPagamentoContrato);
			$("#vencimento").html(row.vencimento);
			$("#inicioVigenciaContrato").html(row.inicioVigenciaContrato);
			$("#fimVigenciaContrato").html(row.fimVigenciaContrato);
			$("#inicioFaturamentoContrato").html(row.inicioFaturamentoContrato);
			$("#fimFaturamentoContrato").html(row.fimFaturamentoContrato);
			$("#codigoMotivoInativacao").html(row.codigoMotivoInativacao);
			$("#motivoInativacao").html(row.motivoInativacao);
			$("#dataInativacao").html(row.dataInativacao);
			
// 			if(row.possuiRSC || row.possuiRRC)
// 				$("#showRSC").show();
// 			else
// 				$("#showRSC").hide();
			$("#showRSC").show();

		}
		else
		{
			cleanContractData();
		}
		
	}
	
	function cleanSigmaData()
	{
		//DADOS GERAIS DO CLIENTE
		$("#codigoCliente").html("");
		$("#codigoEmpresa").html("");
		$("#razaoSocialSigma").html("");
		$("#cnpjCpfSigma").html("");
		
		//DADOS CONTA/PARTICAO
		$("#codigoCentral").html("");
		$("#particao").html("");
		$("#rota").html("");
		$("#logradouro").html("");
		$("#bairroSigma").html("");
		$("#cidadeSigma").html("");
		$("#ufSigma").html("");
		$("#cepSigma").html("");
		
		$("#empresaSigma").html("");
		
		//CONTATOS - TELEFONES/E-MAIL
		$("#telefoneCentral1").html("");
		$("#telefoneCentral2").html("");
		$("#emailCentral").html("");
		$("#responsavelCentral").html("");
		
		//PALAVRA CHAVE
		$("#pergunta").html("");
		$("#resposta").html("");
		
		//PROVIDENCIA
		$('#dgProvidencia').datagrid('loadData', new Array());
	}
	
	function cleanContractData()
	{
		//DADOS GERAIS DO CLIENTE
		$("#codigo").html("");
		$("#razaoSocial").html("");
		$("#nomeFantasia").html("");
		$("#cnpjCpf").html("");
		$("#tipoCliente").html("");
		$("#tipoContrato").html("");
		
		//DADOS ENDERE�O DO CLIENTE
		$("#enderecoCliente").html("");
		$("#complementoCliente").html("");
		$("#bairroClient").html("");
		$("#cepCliente").html("");
		$("#cidadeCliente").html("");
		$("#ufCliente").html("");
		
		//DADOS ENDERE�O DE COBRAN�A
		$("#enderecoCobranca").html("");
		$("#numeroCobranca").html("");
		$("#complementoCobranca").html("");
		$("#cepCobranca").html("");
		$("#cidadeCobranca").html("");
		$("#ufCobranca").html("");
		
		
		//CONTATOS - TELEFONES/E-MAIL
		$("#telefone1").html("");
		$("#telefone2").html("");
		$("#telefone3").html("");
		$("#telefone4").html("");
		$("#telefone5").html("");
		$("#foneFax").html("");
		$("#foneContrato").html("");
		$("#pessoaContato").html("");
		$("emailCliente").html("");
		$("emailContrato").html("");
		
		//DADOS DO CONTRATO
		$("#numeroContrato").html("");
		$("#numeroOficialContrato").html("");
		$("#regionalContrato").html("");
		$("#situacaoContrato").html("");
		$("#codigoServico").html("");
		$("#dataBaseContrato").html("");
		$("#formPagamentoContrato").html("");
		$("#vencimento").html("");
		$("#inicioVigenciaContrato").html("");
		$("#fimVigenciaContrato").html("");
		$("#inicioFaturamentoContrato").html("");
		$("#fimFaturamentoContrato").html("");
		$("#codigoMotivoInativacao").html("");
		$("#motivoInativacao").html("");
		$("#dataInativacao").html("");
	}
	
	var startLoading = function(){
	    $("body").addClass("loading");
	    //setTimeout('error()', 2000);
	};

	var error = function(){
		if ($("body").hasClass("loading")){
			stopLoading();
			jError('Ocorreu um erro ao realizar a pesquisa',
		   	{
		   		autoHide : true,
		   		clickOverlay : false,
		   		MinWidth : 250,
		   		TimeShown : 3000,
		   		ShowTimeEffect : 200,
		   		HideTimeEffect : 200,
		   		LongTrip :20,
		   		HorizontalPosition : 'center',
		   		VerticalPosition : 'center',
		   		ShowOverlay : true,
		      	ColorOverlay : '#000',
		   		OpacityOverlay : 0.3
		   	});
		}
	}

	var stopLoading = function(){
	    $("body").removeClass("loading");
	};
	
	//--------------------- campos edit�veis ------------------------------
	
	var visible = function(element){
	    $(element).removeClass("invisible");
	    $(element).addClass("visible");
	}

	var invisible = function(element){
	    $(element).removeClass("visible");
	    $(element).addClass("invisible");
	}

	var createEditableField = function (fieldId, pk){
	    
	    createEditableHTML(fieldId);
	    
	    $('#editImg-' + fieldId).live('click', function(){
	        var val = $("#" + fieldId).html();
	        $("#old-" + fieldId + "-value").val(val);
	        $("#" + fieldId)
	            .replaceWith("<input name='" +fieldId+ "'id='" + fieldId + "-text' class='left' value='" + val +"'/>");
	        invisible(this);
	        visible("#saveImg-" + fieldId);
	        visible("#closeImg-" + fieldId);	        
	        $("#" + fieldId + "-text").focus();
	        
	        if (fieldId == "cnpjCpfSigma"){
	        	$("#infoEmpresa").css('margin-top',0);
	        }
	        
	    });
	    
	    $('#saveImg-' + fieldId).live('click', function(){
	        var val = $("#" + fieldId + "-text").val();
	        
	        //primeiro pergunta se a pessoa tem certeza se quer salvar a altera��o
	        
	        // verifica se n�o tem nenhum erro
	        if ($("#" + fieldId + "-text").hasClass("error")){
	        	callCenterAlert('N�o � poss�vel salvar pois existe erro de valida��o');
	        } else {
		        //chama a url para salvar as altera��es
		        var url = "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.callcenter.CallCenterUpdateValuesServlet";
		        url = url.concat("?value=").concat(val);
		        url = url.concat("&fieldId=").concat(fieldId);
		        url = url.concat("&codCli=").concat($('#' + pk).html());
				var result = callSync(url);
				
				if (result != null && result != ""){
					callCenterAlert(result);
				}
		        
		        $("#" + fieldId + "-text")
		            .replaceWith("<span id='" + fieldId + "' class='left spanValue'>" + val +"</span>");
		        visible(this);
		        visible("#editImg-" + fieldId);
		        invisible("#saveImg-" + fieldId);
		        invisible("#closeImg-" + fieldId);
		        
		        if (fieldId == "cnpjCpfSigma"){
		        	$("#infoEmpresa").css('margin-top','0');
		        }
		        
	        }
	    });
	    
	    $('#closeImg-' + fieldId).live('click', function(){
	        var val = $("#old-" + fieldId + "-value").val();
	        $("#" + fieldId + "-text")
	            .replaceWith("<span name='" +fieldId+ "'id='" + fieldId +"' class='left spanValue'>" + val +"</span>");
	        visible(this);
	        visible("#editImg-" + fieldId);
	        invisible("#saveImg-" + fieldId);
	        invisible("#closeImg-" + fieldId);
	        
	        if (fieldId == "cnpjCpfSigma"){
	        	$("#infoEmpresa").css('margin-top','0');
	        }
	        
	    });
	}

	var removeEditButtons = function(){
		$(".edit").each(function(){
			invisible("#" + this.id);
		});
	}
	
	var addEditButtons = function(){
		$(".edit").each(function(){
			visible("#" + this.id);
		});
	}
	
	var createEditableHTML = function(fieldId){
	    var divFieldContainer = $('<div>').attr({id: 'fieldcontainer-' + fieldId, 'class': 'fieldContainer'});
	    var divValueContainer = $('<div>').attr({id: 'valuecontainer-' + fieldId, 'class': 'left'});
	    var inputOldValue = $('<input>').attr({type: 'hidden', id:'old-' + fieldId + '-value'});
	    var spanValue = $('<span>').attr({id:fieldId + '-value',name: fieldId, 'class': 'left spanValue'});
	    spanValue.html($("#" + fieldId).html());
	    
	    var divActionButtons = $('<div>').attr({id: 'actionButtons-' + fieldId, 'class': 'left'});
	    var imgEdit = $('<img>').attr({id:'editImg-' + fieldId, 'class': 'visible left ico edit', src: '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/images/edit.png'});
	    var imgSave = $('<img>').attr({id:'saveImg-' + fieldId, 'class': 'invisible left ico', src: '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/images/save.png'});
	    var imgClose = $('<img>').attr({id:'closeImg-' + fieldId, 'class': 'invisible left ico', src: '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/images/close.png'});
	    
	    inputOldValue.appendTo(divValueContainer);
	    spanValue.appendTo(divValueContainer);
	    imgEdit.appendTo(divActionButtons);
	    imgSave.appendTo(divActionButtons);
	    imgClose.appendTo(divActionButtons);
	    divValueContainer.appendTo(divFieldContainer);
	    divActionButtons.appendTo(divFieldContainer);
	    $("#" + fieldId).replaceWith(divFieldContainer);
	    
	    spanValue.attr({id:fieldId});
	}
	
	var callCenterAlert = function(msg){
		jNotify(
				msg,
				{
				  autoHide : true,
				  clickOverlay : false,
				  MinWidth : 250,
				  TimeShown : 3000,
				  ShowTimeEffect : 200,
				  HideTimeEffect : 200,
				  LongTrip :20,
				  HorizontalPosition : 'center',
				  VerticalPosition : 'center',
				  ShowOverlay : true,
		   		  ColorOverlay : '#000',
				  OpacityOverlay : 0.3
				  
				});
	}
	
	function dial(dst) {
		
		var valorTel = document.getElementById("lbTelefone").innerHTML;
		var row = $('#ttligacao').datagrid('getSelected');
		var src = '<%=extensionNumber %>';
		var telRetorno = dst;
		if(!row.emAtendimento && !verificaLigacaoUsuario() && ((row.usuario == null || row.usuario == ' ') && (valorTel == '' || valorTel == null || valorTel == undefined || valorTel == dst)) || row.usuario == nomeUsuario){
		id = row.id;
		if (dst.length >= 10)
  			dst = '0'+dst;
		  $.ajax({
          	method: 'post',
              url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.callcenter.CallCenterServlet?action=atualizaStatusAtendimento&ramalDestino='+dst+'&ramalOrigem='+src+'&id='+id+'&nomeUsuario='+nomeUsuario+'&r' + Math.random(),
              success: function(result){
            	
				if(result == 'true'){
            	searchClient(telRetorno);
				getAtualizaListaClientes();
          		
          		telefoneCliente = dst;
          		document.getElementById("lbTelefone").innerHTML = telefoneCliente;
          	
<%--           		var resultado = callSync('<%= PortalUtil.getBaseURL() %>servlet/com.neomind.fusion.custom.orsegups.servlets.RamalServlet?ramaOrigem='+src+'&ramalDestino='+dst); --%>

//           		if (resultado.indexOf("Chamada efetuada") == -1) {
//           			alert("N�o foi poss�vel discar: " + response);
//           		}else{
//           			alerta('info','A liga��o esta sendo efetuada!');
//           			atendendo = true;
//           		}
          		}else{
          			alerta('error','Liga��o j� esta sendo executada!');
          		}
				getAtualizaListaClientes();
              },  
     			error: function(e){ 
     				alerta('error','Verifique com o administrador! '+e);
    			}
          });
		}else{
			if(verificaLigacaoUsuario())
				alerta('error','Finalize o atendimento anterior!');
			else
				alerta('error','O atendimento est� sendo realizado! '+verificaUsuario(row.id));
		}
		
	}
	
	function formatLog(value,row,index){
		 var iconLocal = '<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/images/searchbox_button.png';
		 var format = "<span data-p1="+index+" class=\"easyui-tooltip\"> <img src=\""+iconLocal+"\"/> </span>";
		
		 return format;
		 
	 }
	
	 function createTooltip(){
			
		    $('#ttligacao').datagrid('getPanel').find('.easyui-tooltip').each(function(){
		    	$(this).tooltip('destroy');
		    	var index = parseInt($(this).attr('data-p1'));  
		        
		        $(this).tooltip({
		            content: $('<div> </div>'),
		            position: 'left',
		            hideDelay: 100,
		            onUpdate: function(cc){
		            	
		                var row = $('#ttligacao').datagrid('getRows')[index];
		                var content = '<ul>';
		                content += '<li>'+row.descricao+'</li>';
		                content += '</ul>';
		                cc.panel({
		                    width:400,
		                    height: 300,
		                    title: 'LOG',
		                    content:content
		                });
		            },
					 onShow: function(){
	                    var t = $(this);
	                    t.tooltip('tip').unbind().bind('mouseenter', function(){
	                        t.tooltip('show');
	                    }).bind('mouseleave', function(){
	                        t.tooltip('hide');
	                    });
	                },
	                onPosition: function(){
	                    $(this).tooltip('tip').css('top', $(this).offset().top);
	                    $(this).tooltip('arrow').css('top', 10);
	                },
	               
		            position:'left'
		            
		        });
		    });
		    
	}

	 function formatRetornoLigacao(val,row)
	 {
         if (val)
         {
             return '<span style="color:green;">(Sim)</span>';
         } else {
             return '<span style="color:red;">(N�o)</span>';
         }
     }
	 
	 function popUpAtendimento(confir){
		var label = 'Insira a informa��o.';
		var texto = prompt(label,"");
	
		if(texto != null && texto != "")
		{
       
		 var encerrar = 0;
		 var nomeCol = '<%=PortalUtil.getCurrentUser().getFullName()%>';
		 var descricao = ' Atendente '+nomeCol+' - ramal: '+ extensionNumber  + ' ' + texto;
		
             if (confir){
            	 encerrar = 1; 
            	 atendendo = false;
            	 encerrarAtendimento(encerrar, descricao);
            	 
             }else{
            	 atendendo = false;
            	 encerrarAtendimento(encerrar, descricao);
            	 
             }
		}
         
	 }
	 
	 function encerrarAtendimento(encerrar, descricao){
		
		 if(descricao == null){
			 descricao = '';
		 }
			
		 $.ajax({
          	method: 'get',
              url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.callcenter.CallCenterServlet?action=atualizaLigacaoRecebida&descricao='+descricao+'&encerrar='+encerrar+'&telefone='+telefoneCliente+'&id='+id+'&nomeUsuario='+nomeUsuario+'',
              success: function(result){
             	 sucess();
             	 telefoneCliente = 0;
             	 document.getElementById("lbTelefone").innerHTML = '';
             	 getAtualizaListaClientes();
              },  
     			error: function(e){  
     			error();
    		  }
          });
	 }
	 
	 function sucess(){
         $.messager.show({
             title:'Aten��o',
             msg:'<div class=\'messager-icon messager-info\'></div>Dados salvos com sucesso!',
             showType:'show'
         });
     }
	 
	 function error(){
         $.messager.show({
             title:'Aten��o',
             msg:'<div class=\'messager-icon messager-error\'></div>Erro ao salvar os dados!',
             showType:'show'
         });
     }
	 
	 function alerta(icone,texto){
         $.messager.show({
             title:'Aten��o',
             msg:'<div class=\'messager-icon messager-'+icone+'\'></div>'+texto,
             showType:'show'
         });
     }
	 
	 function formatCliente(val,row){
         var loc = "";
		 if (val){
			 loc = local + "ok.png";
        	 return "<span title=\"O numero pertence a um cliente!\"><img src=\""+loc+"\"/> </span>";
         } else {
        	 loc = local + "no.png";
             return "<span title=\"O numero n�o pertence a um cliente!\"><img src=\""+loc+"\"/> </span>";
         }
     }
	 
     function changeImage() { 
 		document.getElementById("imgLig").src = "images/validatebox_warning.png";
     }
     
     function verificarNovoNumero(){
    	 
    	 var rows = $('#ttligacao').datagrid('getRows');
    	 var count = 0;
    	 for (var i=0;i<rows.length;i++){
    		 if(!rows[i].atendimentoEfetuado){
    			 count++; 
    		 }
    	 }
    	 if(count != null && count > 0){
    	 document.getElementById("lbDat").innerHTML = count;
    	 }else{
         document.getElementById("lbDat").innerHTML = null;	 
    	 }
    	 
    	 
     }
	 
	function verificaLigacaoUsuario(){
    	 
    	 var rows = $('#ttligacao').datagrid('getRows');
    	 var count = 0;
    	 for (var i=0;i<rows.length;i++){
    		 if(rows[i].usuario == nomeUsuario){
    			 count++; 
    		 }
    	 }
    	 if(count != null && count > 0){
    	 return true;
    	 }else{
         return false;
    	 } 
    	 
     }
	 
	function verificaUsuario(idUsuario){
   	 
   	 var rows = $('#ttligacao').datagrid('getRows');
   	 var usuarioStr = '';
   	 for (var i=0;i<rows.length;i++){
   		 if(rows[i].id == idUsuario){
   			usuarioStr = rows[i].usuario; 
   		 }
   	 }
   	 return usuarioStr;
   	 
    }
	
	function verificaUsuarioId(idUsuario){
   	 
   	 var rows = $('#ttligacao').datagrid('getRows');
   	 var usuarioStr = '';
   	 for (var i=0;i<rows.length;i++){
   		 if(rows[i].id == idUsuario){
   			return true;
   		 }
   	 }
   	 return false;
   	 
    }
	
	function alterarSenhaParaPadrao(senha)
	{
		var novoCgcCpf = 0;
		if(senha != null)
		{
			novoCgcCpf = senha;
			
			 $.ajax({
		          	method: 'get',
		              url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.callcenter.CallCenterServlet?action=recuperarSenha&cgccpf='+novoCgcCpf,
		              success: function(result){
		            	 callCenterAlert('Senha redefinida com sucesso!');
		              },  
		     			error: function(e){  
		     		     callCenterAlert('Erro ao redefinir senha!');
		    		  }
		          });
		}
	}
	
	function pesquisarResponsavelEquipamento(codigoCliente)
	{
	
		if(codigoCliente != null)
		{
			 $.ajax({
		          	method: 'get',
		              url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.callcenter.CallCenterServlet?action=getSigmaOSData&codigoCliente='+codigoCliente,
		              success: function(result){
		            	  $("#propriedadeEquipamento").html(result);
		              },  
		     			error: function(e){  
		     			  callCenterAlert('Erro ao pesquisar respons�vel pelo equipamento.');
		    		  }
		          });
		}
		else
		{
			callCenterAlert('C�digo cliente inv�lido.');
		}
	}

	function recuperaSenhaSite(cgcCpf,tipoSenha)
	{
		
		if(cgcCpf != null)
		{
			 $.ajax({
		          	method: 'get',
		              url: '<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.callcenter.CallCenterServlet?action=recuperaSenhaSite&cgcCpf='+cgcCpf,
		              success: function(result){
		            	 if(tipoSenha == 1)
		            	 {
		            		 $("#senhaSite").html(result);
		            	 }
		            	 else if(tipoSenha == 2)
		            	 {
		            		 $("#senhaSiteSigma").html(result);
		            	 }


		              },  
		     			error: function(e){  
		     			  callCenterAlert('Erro ao pesquisar senha.');
		    		  }
		          });
		}
		else
		{
			callCenterAlert('CNPJ/CPF inv�lido.');
		}
	}


	 	
</script>
</head>
<body id="bodyId">
	<form id="formPrincipal" action="">
	   	<div class="easyui-tabs" id="tabsId">  
			<div title="Contas Sigma" style="padding:5px">
				<div id="contaSigmaLayout" class="easyui-layout" style="width:100%;height:680px;">
					<div data-options="region:'west',split:true" title="Contas" style="width:400px">
						<table id="ttsigma" class="easyui-treegrid" style="width:392px;height:600px"
								idField="id" treeField="codigoCliente">
						    <thead>
						        <tr>
						            <th data-options="field:'codigoCliente'" width="100px">C�digo Cliente</th>
						            <th data-options="field:'razaoSocial'" width="295px">Raz�o Social</th>
						        </tr>
						    </thead>
						</table>
					</div>
					<div id="detalhesContrato" data-options="region:'center',title:'Detalhes Conta'">
						<ul>
							<fieldset>
								<legend>Dados Cliente</legend>
								<li>C�digo Cliente: <span id="codigoCliente"></span></li>
								<li>C�digo Empresa: <span id="codigoEmpresa"></span></li>
								<li>Raz�o Social: <span id="razaoSocialSigma"></span></li>
								<li>CPF/CNPJ: <span id="cnpjCpfSigma"></span></li>
								<li>Senha Site: <span id="senhaSiteSigma" ></span></li>
								<fieldset id="infoEmpresa" class="float-rigth">
									<li><span id="empresaSigma"></span></li>								
								</fieldset>
								
							</fieldset>
							<fieldset>
								<legend>Dados Conta/Parti��o</legend>
								<li>C�digo Central: <span class="destaque" id="codigoCentral"></span></li>
								<li>Parti��o: <span class="destaque" id="particao"></span></li>
								<li>Rota: <span id="rota"></span></li>
								<li>Endere�o: <span id="logradouro"></span></li>
								<li>Bairro: <span id="bairroSigma"></span></li>
								<li>Cidade: <span id="cidadeSigma"></span></li>
								<li>estado: <span id="ufSigma"></span></li>
								<li>CEP: <span id="cepSigma"></span></li>
							</fieldset>
							<fieldset>
								<legend>Contato</legend>
								<li>Respons�vel: <span id="responsavelCentral"></span></li>
								<li>Email: <span id="emailCentral"></span></li>
								<li>Telefone Central 1: <span id="telefoneCentral1"></span></li>
								<li>Telefone Central 2: <span id="telefoneCentral2"></span></li>
							</fieldset>
							<fieldset>
								<legend>Equipamento</legend>
								<li>Propriedade dos equipamentos: <span id="propriedadeEquipamento"></span></li>
							</fieldset>
							<fieldset>
								<legend>Palavra Chave</legend>
								<li>Pergunta: <span class="destaque" id="pergunta"></span></li>
								<li>Resposta / Senha Verbal: <span class="destaque" id="resposta"></span></li>
							</fieldset>
							 
							
							<fieldset>
								<legend>Contatos</legend>
								<table id="dgProvidencia" class="easyui-datagrid" style="width:800px;height:250px"  
									data-options="fitColumns:true,singleSelect:true,
									 rowStyler: function(index,row){
										var telAux = row.telefone1;
										var telAux2 = row.telefone2;
								        if(telAux != null && telAux != '' && telAux != 'undefined')
								        {
									        telAux = telAux.replace('(', '');
									        telAux = telAux.replace(')', '');
									        telAux = telAux.replace('-', '');
								
											 if (telAux == externalNumber)
					                        	return 'background-color:#ffee00;color:red;';
								        }
								        
								        if(telAux2 != null && telAux2 != '' && telAux2 != 'undefined')
								        {
								        	telAux2 = telAux2.replace('(', '');
									        telAux2 = telAux2.replace(')', '');
									        telAux2 = telAux2.replace('-', '');
								
											 if (telAux2 == externalNumber)
					                        	return 'background-color:#ffee00;color:red;';
								        }
					                   
					                }">  
							    	<thead>  
								        <tr>  
								            <th data-options="field:'codigoProvidencia',width:40,align:'center'">Provid�ncia</th>  
								            <th data-options="field:'nome',width:150">Nome</th>  
								            <th data-options="field:'telefone1',width:60">Telefone 1</th>  
								            <th data-options="field:'telefone2',width:60">Telefone 2</th>  
								            <th data-options="field:'email',width:90">Email</th>  
								            <th data-options="field:'prioridade',width:55,align:'center'">Prioridade</th>  
								        </tr>  
								    </thead>  
								</table>  
							</fieldset>
								
							
							</li>							<li>
								<a href="javascript:void(0)" class="easyui-linkbutton" onclick="startProcess('OsSigma')">Abrir OS Sigma</a>
								<a href="javascript:void(0)" id="showOSAbertas" class="easyui-linkbutton" onclick="seeProcess('OsSigma')">Visualizar OS`s Abertas</a>
								<a href="javascript:void(0)" id="showOSPausadas" class="easyui-linkbutton" onclick="seeProcessOSPausadas('OsSigma')">Visualizar OS`s Pausadas</a>
								<a href="javascript:void(0)" class="easyui-linkbutton" onclick="openAdvancedSearchBox()">Busca Avan�ada</a>
							</li>
						</ul>
					</div>
					<div id="buscaAvancadaWest" data-options="region:'east',split:true,collapsed:true" title="Pesquisa Avan�ada" style="width:310px">
						<ul>
							<fieldset>
								<li><span align="right">C�digo Cliente:</span> <input type="text" id="codigoClienteSearch"></input> </li>
								<li><span align="right">Raz�o Social:</span> <input type="text" id="razaoSearch"></input></span></li>
								<li><span align="right">CPF/CNPJ:</span> <input type="text" id="cpfCnpjSearch"></input></span></li>
								<li><span align="right">Telefone:</span> <input type="text" id="telefoneSearch"></input></span></li>
								<li><span align="right">Endere�o:</span> <input type="text" id="enderecoSearch"></input></span></li>
								<li><span align="right">Bairro:</span> <input type="text" id="bairroSearch"></input></span></li>
								<li><span align="right">Cidade:</span> <input type="text" id="cidadeSearch"></input></span></li>
								<li><span align="right">Estado:</span> <input type="text" id="ufSearch"></input></span></li>
								<li><span align="right">CEP:</span> <input type="text" id="cepSearch"></input></span></li>
								<li><span align="right">E-mail:</span> <input type="text" id="emailSearch"></input></span></li>
							</fieldset>
							<div style="width:100%" align="right">
								<a href="javascript:void(0)" style="font-weight:bold" class="easyui-linkbutton" onclick="advancedSearch()">Pesquisar</a>
								<a href="javascript:void(0)" style="font-weight:bold" class="easyui-linkbutton" onclick="closeAdvancedSearchBox()">Fechar</a>
							</div>
						</ul>
					</div>
				</div>
			</div>
			<div title="Contratos" style="padding:5px">
	       		<div class="easyui-layout" style="width:100%;height:630px;">
	       			 <div data-options="region:'west',split:true" title="Clientes" style="width:400px">
	       			 	<table id="ttcontrato" class="easyui-treegrid" style="width:392px;height:600px"  
						        idField="id" treeField="treeLabel" >
						    <thead>
						        <tr>
						            <th data-options="field:'treeLabel'" width="390px">Cliente/Contratos</th>
						        </tr>
						    </thead>
						</table>
	       			 </div>
	       			 <div id="detalhesContrato" data-options="region:'center',title:'Detalhes Contrato'">
	       			 		<ul>
	       			 			<fieldset>
		       			 			<legend>Dados Gerais Cliente</legend>
		       			 			<li>Codigo: <span id="codigo"></span></li>
		       			 			<li>Raz�o Social: <span id="razaoSocial"></span></li>
		       			 			<li>Nome Fantasia: <span id="nomeFantasia"></span></li>
		       			 			<li>CPF/CNPJ: <span  id="cnpjCpf"></span></li>
		       			 			<li>Tipo Cliente: <span id="tipoCliente"></span></li>
		       			 			<li>Tipo de Empresa: <span id="tipoContrato"></span></li>
		       			 			<li>Senha Site: <span id="senhaSite"></span></li>
	       			 			</fieldset>
	       			 			<fieldset>
		       			 			<legend>Endere�o do Cliente</legend>
		       			 			<li>Endere�o: <span id="enderecoCliente"></span></li>
		       			 			<li>Complemento: <span id="complementoCliente"></span></li>
		       			 			<li>Bairro: <span id="bairroClient"></span></li>
		       			 			<li>CEP: <span id="cepCliente"></span></li>
		       			 			<li>Estado: <span id="ufCliente"></span></li>
		       			 			<li>Cidade: <span id="cidadeCliente"></span></li>
	       			 			</fieldset>
	       			 			<fieldset>
		       			 			<legend>Endere�o de Cobran�a</legend>
		       			 			<li>Endere�o: <span id="enderecoCobranca"></span></li>
		       			 			<li>Numero: <span id="numeroCobranca"></span></li>
		       			 			<li>Complemento: <span id="complementoCobranca"></span></li>
		       			 			<li>CEP: <span id="cepCobranca"></span></li>
		       			 			<li>Estado: <span id="ufCobranca"></span></li>
		       			 			<li>Cidade: <span id="cidadeCobranca"></span></li>
	       			 			</fieldset>
	       			 			<fieldset>
		       			 			<legend>Telefones/Email</legend>
		       			 			<li>Pessoa Contato: <span id="pessoaContato"></span></li>
		       			 			<li>Telefone 1: <span id="telefone1"></span></li>
		       			 			<li>Telefone 2: <span id="telefone2"></span></li>
		       			 			<li>Telefone 3: <span id="telefone3"></span></li>
		       			 			<li>Telefone 4: <span id="telefone4"></span></li>
		       			 			<li>Telefone 5: <span id="telefone5"></span></li>
		       			 			<li>Telefone/Fax: <span id="foneFax"></span></li>
		       			 			<li>Telefone Contrato: <span id="foneContrato"></span></li>
		       			 			<li>Email Cliente: <span id="emailCliente"></span></li>
		       			 			<li>Email Contrato: <span id="emailContrato"></span></li>
	       			 			</fieldset>
	       			 			<fieldset>
		       			 			<legend>Dados Contrato</legend>
		       			 			<li>N�mero: <span id="numeroContrato"></span></li>
		       			 			<li>N�mero Oficial: <span id="numeroOficialContrato"></span></li>
		       			 			<li>Regional: <span class="destaque" id="regionalContrato"></span></li>
		       			 			<li>Situa��o: <span id="situacaoContrato"></span></li>
		       			 			<li>Tipo Servi�o: <span id="codigoServico"></span></li>
		       			 			<li>Data Base: <span id="dataBaseContrato"></span></li>
		       			 			<li>Forma Pagamento: <span id="formPagamentoContrato"></span></li>
		       			 			<li>Vencimento: <span id="vencimento"></span></li>
		       			 			<li>In�cio Vig�ncia: <span id="inicioVigenciaContrato"></span></li>
		       			 			<li>Fim Vig�ncia: <span id="fimVigenciaContrato"></span></li>
		       			 			<li>In�cio Faturamento: <span id="inicioFaturamentoContrato"></span></li>
		       			 			<li>Fim Faturamento: <span id="fimFaturamentoContrato"></span></li>
		       			 			<li>C�digo Motivo Inativa��o: <span id="codigoMotivoInativacao"></span></li>
		       			 			<li>Motivo Inativa��o: <span id="motivoInativacao"></span></li>
		       			 			<li>Data inativa��o: <span id="dataInativacao"></span></li>
	       			 			</fieldset>
	    			 			<li>
	    			 				<a href="javascript:void(0)" class="easyui-linkbutton" onclick="startProcess('RSC')">Iniciar RSC</a>
	    			 				<a href="javascript:void(0)" class="easyui-linkbutton" onclick="startProcess('RRC')">Iniciar RRC</a>
	    			 				<a href="javascript:void(0)" id="showRSC" class="easyui-linkbutton" onclick="seeRSC('RSC')">Visualizar RSC`s / RRC`s Abertas</a>
								
	    			 			</li>
	       			 		</ul>
	       			 </div>
	       		</div>
			</div>
		
<%-- 			<div  title="<span ><img  style='border:none;' id='imgLig' ></img>&nbsp Liga��es � retornar</span>"  data-options="tools:'#p-tools'" style="padding:5px"> --%>
			<div  title="<label id='lbDat' align='center' class='txtDat' ></label>&nbsp Liga��es � retornar"  data-options="tools:'#p-tools'" style="padding:5px">
				<div id="licagaoLayout" class="easyui-layout" style="width:100%;height:630px;">
					<div data-options="region:'west',rownumbers:true,singleSelect:true" title="Listagem de liga��es CallBack ou Abandono." style="width:1180px">
						<table id="ttligacao" data-options="rownumbers:true,singleSelect:true,toolbar: '#tb', rowStyler: function(index,row){
                   				 if (row.atendimentoEfetuado){
                       				 return 'background-color:#FFF0F5;color:#fff;font-weight:bold;';
                   				 }
               			 }" class="easyui-datagrid" style="width:1170px;height:600px"  >
						    <thead>
						        <tr>
						           	<th data-options="field:'cliente',align:'center',formatter:formatCliente" width="25px">Cliente</th>
						            <th data-options="field:'origem'" width="80px">Origem</th>
						            <th data-options="field:'telefone',align:'center'" width="80px">Telefone</th>
						            <th data-options="field:'dataLigacao',align:'center'" width="125px">Data Liga��o</th>
						            <th data-options="field:'dataPrimeiraLigacao',align:'center'" width="125px">Data Tentativa</th>
						            <th data-options="field:'dataRetorno',align:'center'" width="125px">Data Retorno</th>
						            <th data-options="field:'retornouCliente',align:'center',formatter:formatRetornoLigacao" width="75px">Retornou?</th>
						            <th data-options="field:'atendimentoEfetuado',align:'center',formatter:formatRetornoLigacao" width="75px">Atendeu?</th>
						            <th data-options="field:'emAtendimento',align:'center',formatter:formatRetornoLigacao" width="75px">Ligando?</th>
						            <th data-options="field:'usuario',align:'center'" width="190px">Usu�rio</th>
						            <th data-options="field:'descricao',align:'center',formatter:formatLog" width="60px" >Log</th>
						            
						            
						        </tr>
						    </thead>
						</table>
						<div id="tb" style="height:auto">
							 &nbsp Ligando para: <label id="lbTelefone" style="font-family:arial;font-weight:bold;color:red;font-size:12px;" ></label>
							<br>	
        					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-ok',plain:true" onclick="popUpAtendimento(true)">Confirmar Atendimento</a>
        					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-cancel',plain:true" onclick="popUpAtendimento(false)">Atendimento Pendente</a>
    					</div>
					</div>
				</div>
		   </div>
		</div>
	</form>
	<!-- 
	<div id="wfprocess" class="easyui-window" title="Nova Solicita��o" data-options="modal:true,closed:true,minimizable:false" style="width:900px;height:600px;padding:0px;">
	</div>
	 -->
	<div id="tab-tools" style="height:24px" >
		<span class="externalNumber" id="externalNumber"></span>
		<input id="searchBoxId" class="easyui-searchbox" style="width:350px;"
        	data-options="searcher:searchClient,prompt:'CPF/CNPJ, C�digo Cliente, Raz�o Social, Telefone, e-mail'"></input>
		<span class="ramal">Ramal: <%=extensionNumber %></span>
	</div>
	<div class="modal"></div>
		<div id="p-tools"> 
<!--         <a href="javascript:void(0)" title="Salvar informa��es sobre a liga��o!" class="icon-mini-add" onclick="popUpAtendimento()"></a> -->
<!--         <a href="javascript:void(0)" title="Atualizar lista!" class="icon-mini-refresh" onclick="getAtualizaListaClientes()"></a> -->
 		</div> 

</body>
</html>