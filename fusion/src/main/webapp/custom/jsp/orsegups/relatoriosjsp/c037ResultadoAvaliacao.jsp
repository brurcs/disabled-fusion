<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="java.text.DecimalFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.custom.orsegups.contract.ContratoLogUtils"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.persist.QLFilterIsNull"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="java.util.ArrayList"%>
<%@page import="javax.persistence.Query"%>
<%@page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>C037 - Resultado da Avalia��o trimestral de t�cnicos terceirizados</title>
</head>
<body>

<%!


public static boolean validaOSFinalizada(String numeroOS)
{
	StringBuilder sql = new StringBuilder();
	sql.append("select "); 
	sql.append("os.ID_ORDEM ");

	sql.append("from dbordem os ");
	
	sql.append("where ");
	sql.append("os.FECHADO = 1 and ");
	sql.append("os.FECHAMENTO is not null and ");
	sql.append("os.ID_ORDEM = " + numeroOS);
	sql.append("order by os.ID_ORDEM ");
	
	Query query = PersistEngine.getEntityManager("SIGMA90").createNativeQuery(sql.toString());
	Collection<Object> resultList = query.getResultList();
	if (resultList != null && !resultList.isEmpty())
	{
		//se achou alguma coisa o chamado est� finalizado
		return true;
	}
	
	return false;
}


public static String retornaOsAtendidas(String cdColaborador, String datIni, String datFim){
	String retorno = "0";
	
	String sql = " select sum( (LEN(avt.idOrdem) - LEN(REPLACE(avt.idOrdem,',',''))) +1 ) "+ 
			" from wfProcess wf "+
			" join D_AVTPrincipal ap on (wf.neoId = ap.wfprocess_neoId  ) "+  
			" join D_AVTTerceirizado avt on (ap.terceirizado_neoId = avt.neoID) "+ 
			" join X_SIGMACOLABORADOR xcolab on (avt.terceirizado_neoId = xcolab.neoID) "+ 
			"         left join d_avtconceito dac1 on (ap.apresRelacionamento_neoID = dac1.neoID) "+                                             
			"         left join d_avtconceito dac2 on (ap.apresUniforme_neoID = dac2.neoID) "+                                                   
			"         left join d_avtconceito dac3 on (ap.condGerIns_neoID = dac3.neoID) "+                                                      
			"         left join d_avtconceito dac4 on (ap.limpeza_neoID = dac4.neoID) "+                                                         
			"         left join d_avtconceito dac5 on (ap.prazoEntregaInstalacao_neoID = dac5.neoID) "+                                          
			"         left join d_avtconceito dac6 on (ap.servPrestIns_neoID = dac6.neoID) "+                                                    
			"         left join d_avtconceito dac7 on (ap.orientacoesFuncionamentoSistema_neoID = dac7.neoID) "+ 
			" where xcolab.cd_colaborador =" + cdColaborador + 
			" and wf.processState <> 2 "+                                                                                              
			" and wf.finishDate is not null "+
			" and (dac1.codigo > 0 and dac2.codigo > 0 and dac3.codigo > 0 and dac4.codigo > 0 and dac5.codigo > 0 and dac6.codigo > 0 ) "+   
			" and dataAvaliacao between cast('"+datIni+" 00:00:00.000' as Date) and cast('"+datFim+" 00:00:00.000' as Date) ";
	
		
	Query query = PersistEngine.getEntityManager().createNativeQuery(sql.toString());
	Object result = query.getSingleResult();
	if (result != null){
		retorno = NeoUtils.safeOutputString(result);
	}
	return retorno;
			
}

public static String retornaNomeInstalador(String cdColaborador){
	String retorno = "";
	
	String sql = " select nm_colaborador "+
				" from COLABORADOR xcolab " +
				" where xcolab.cd_colaborador = " + cdColaborador ; 
	//System.out.println(sql);
		
	Query query = PersistEngine.getEntityManager("SIGMA90").createNativeQuery(sql.toString());
	Object result = query.getSingleResult();
	if (result != null){
		retorno = NeoUtils.safeOutputString(result);
	}
	return retorno;
			
}

public static ArrayList<String[]> retornarPontuacao(String datIni, String datFim)
{
	
	ArrayList<String[]> retorno = new ArrayList();
	
	
	String sql = " select x.cd_colaborador  																						  "+
			" , cast ( (( sum(x.d2) +    sum(x.d1) +   sum(x.d6) +  sum(x.d4) +   sum(x.d5) + sum(x.d3) )/10)*3  as numeric)                "+
			" from                                                                                                                    "+
			" (                                                                                                                       "+
			" select xcolab.cd_colaborador  ,dac1.codigo \"d1\" ,dac2.codigo \"d2\" ,dac3.codigo \"d3\",dac4.codigo \"d4\"            "+
			" ,dac5.codigo \"d5\" ,dac6.codigo \"d6\" ,dac7.codigo \"d7\"                                                             "+
			" from wfProcess wf                                                                                                       "+
			" join D_AVTprincipal ap on (wf.neoId = ap.wfprocess_neoId  )                                                             "+
			" join D_AVTTerceirizado dterc	on (ap.terceirizado_neoID = dterc.neoID)                                                  "+
			" join X_SIGMACOLABORADOR xcolab	on (dterc.terceirizado_neoID = xcolab.neoID)                                          "+
			" left join d_avtconceito dac1 on (ap.apresRelacionamento_neoID = dac1.neoID)                                             "+
			" left join d_avtconceito dac2 on (ap.apresUniforme_neoID = dac2.neoID)                                                   "+
			" left join d_avtconceito dac3 on (ap.condGerIns_neoID = dac3.neoID)                                                      "+
			" left join d_avtconceito dac4 on (ap.limpeza_neoID = dac4.neoID)                                                         "+
			" left join d_avtconceito dac5 on (ap.prazoEntregaInstalacao_neoID = dac5.neoID)                                          "+
			" left join d_avtconceito dac6 on (ap.servPrestIns_neoID = dac6.neoID)                                                    "+
			" left join d_avtconceito dac7 on (ap.orientacoesFuncionamentoSistema_neoID = dac7.neoID)                                 "+
			" where wf.processState <> 2                                                                                              "+
			" and wf.finishDate is not null                                                                                           "+
			" and (dac1.codigo > 0 and dac2.codigo > 0 and dac3.codigo > 0 and dac4.codigo > 0                                        "+
			" and dac5.codigo > 0 and dac6.codigo > 0 )                                                                               "+
			//" and wf.startDate between cast('2014-01-01 00:00:00.000' as Date) and cast('2014-09-30 00:00:00.000' as Date)   		  "+
			" and dataAvaliacao between cast('"+datIni+" 00:00:00.000' as Date) and cast('"+datFim+" 00:00:00.000' as Date)   "+		
			" and wf.finishDate is not null 																						  "+
			" ) x                                                                                                                     "+
			" group by x.cd_colaborador                                                                                               "+
			" order by 2 desc";                                                                                                       
	//System.out.println(sql);
	
	Query query = PersistEngine.getEntityManager().createNativeQuery(sql.toString());
	Collection<Object[]> resultList = query.getResultList();
	if (resultList != null && !resultList.isEmpty())
	{
		String cabecalho[] = {"Colaborador", "<center>Pontuacao <br> (valor*3)</center>","OS Atendidas","Resultado Final"};
		retorno.add(cabecalho);
		for (Object[] rs : resultList){
			String linha[] = {
					String.valueOf(rs[0]),
					String.valueOf(rs[1]),
					retornaOsAtendidas(String.valueOf(rs[0]), datIni, datFim),
					NeoUtils.safeOutputString(  
							 	NeoUtils.safeDouble( String.valueOf(rs[1]) ) / NeoUtils.safeDouble(retornaOsAtendidas(String.valueOf(rs[0]), datIni, datFim))   
							)
					};
			retorno.add(linha);
		}
	}
	
	return retorno;
}

%>


<%
String strTrimestre = request.getParameter("cmb_trimestre");
String strAno = request.getParameter("cmb_ano");
ArrayList<NeoObject> oTrimestres = (ArrayList<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("ADTTrimestre"));
GregorianCalendar gc = new GregorianCalendar();
int anoAtual = gc.get(GregorianCalendar.YEAR); 
int anoPassado = anoAtual -1;

GregorianCalendar datIni = new GregorianCalendar();
GregorianCalendar datFim = new GregorianCalendar();

if (strTrimestre != null){
	for (NeoObject trimestre : oTrimestres){
		EntityWrapper wT = new EntityWrapper(trimestre);
		if (NeoUtils.safeOutputString(wT.findValue("trimestre")).equals(strTrimestre)){
			System.out.println("trimestre..." + strTrimestre);
			datIni.set(GregorianCalendar.MONTH, Integer.parseInt( NeoUtils.safeOutputString( wT.findValue("mesIni") ) )-1 );
			datFim.set(GregorianCalendar.MONTH, Integer.parseInt( NeoUtils.safeOutputString( wT.findValue("mesFim") ) )-1 );
			break;
		}
	}
}
if (strAno != null){
	datIni.set(GregorianCalendar.YEAR, Integer.parseInt( strAno ) );
	datFim.set(GregorianCalendar.YEAR, Integer.parseInt( strAno ) );
}
	datIni.set(GregorianCalendar.DATE, 1);
	datFim.set(GregorianCalendar.DATE, datFim.getActualMaximum(GregorianCalendar.DATE));
	System.out.println("datas" + NeoUtils.safeDateFormat(datIni.getTime(), "yyyy-MM-dd") + " - " + NeoUtils.safeDateFormat(datFim.getTime(), "yyyy-MM-dd") );

%>
	<form>
		<span>Trimestre</span>
		<select id="cmb_trimestre" name="cmb_trimestre">
			<%
			for (NeoObject trimestre : oTrimestres){
				EntityWrapper wTrim = new EntityWrapper(trimestre);
			%>
			<option value="<%=wTrim.findValue("trimestre")%>" <%= strTrimestre != null && strTrimestre.equals(NeoUtils.safeOutputString(wTrim.findValue("trimestre")) )? "selected='selected'" : "" %> ><%= wTrim.findValue("trimestre") %></option>
			<%
			}
			%>

		</select>
		&nbsp;
		<span>Ano</span>
		<select id="cmb_ano" name="cmb_ano">
			<option value="<%= anoAtual %>" selected="selected"><%= anoAtual %></option>
			<option value="<%= anoPassado %>" ><%= anoPassado %></option>
		</select>
		<input type="submit" value="Processar">
	</form>

<%

	DecimalFormat formatoReal = new DecimalFormat("#,##0.00");

	ArrayList<String[]> lista = retornarPontuacao(NeoUtils.safeDateFormat(datIni.getTime(), "yyyy-MM-dd"), NeoUtils.safeDateFormat(datFim.getTime(), "yyyy-MM-dd"));
	%>
	<table style="border: black solid 1px;">
		<tr align="center" style="background-color: silver;">
			<td colspan="5" style="border: black solid 1px;"><h3>Classifica��o de T�cnicos Terceirizados  <%= strTrimestre != null ? (" - " + strTrimestre + "� trimestre de " + strAno) : "" %></h3></td>
		</tr>
	<%
	int total = lista.size();
	int faturados = 0;
	int pendentes = 0;
	int errados = 0;
	
	int c = 0;
	for(String linha[]: lista){
		String stylo = "";
		
		%>
		<tr style='<%=  c%2 == 0 ? "background-color:silver;":"" %>' >
			<td style="border: black solid 1px;" ><%= c > 0 ? c : ""  %></td> 
			<td style="border: black solid 1px;" ><%= linha[0] + ( c>0? "-" +retornaNomeInstalador(linha[0]): "") %></td>
			<td style="border: black solid 1px;" ><%= linha[1] %></td>
			<td style="border: black solid 1px;" ><%= linha[2] %></td>
			<td style="border: black solid 1px;"> <%= c>0? NeoUtils.safeFormat(formatoReal, NeoUtils.safeDouble(linha[3])) : linha[3]  %></td>
		</tr>
		<%
		c++;
	}
	%>
	</table>
		<br>
</body>
</html>