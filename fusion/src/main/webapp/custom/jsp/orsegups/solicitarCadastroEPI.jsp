<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.neomind.fusion.workflow.exception.WorkflowException"%>
<%@page import="java.util.Collection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.doc.NeoDocument"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="/WEB-INF/favorite.tld" prefix="favorite"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/webui.tld" prefix="wui"%>

<%@page import="net.sf.jasperreports.engine.JasperCompileManager"%>
<%@page import="net.sf.jasperreports.engine.JRException"%>

<%@page import="com.neomind.fusion.entity.FieldWrapperGroup"%>
<%@page import="com.neomind.fusion.entity.FieldWrapper"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>


<%@page import="com.neomind.fusion.doc.NeoStorage"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.workflow.Task"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.workflow.UserActivity"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.datawarehouse.ExternalEntityInfo"%>

<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.List"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.net.URL"%>

<%
	Long numemp = Long.parseLong(request.getParameter("numemp"));
	Long tipcol = Long.parseLong(request.getParameter("tipcol"));
	Long numcad = Long.parseLong(request.getParameter("numcad"));
	Long regional = Long.parseLong(request.getParameter("regional"));
	String nomfun = null;
	NeoUser usuFus = null;
	String solicitante = null;
	String executor = null;

	QLGroupFilter qft = new QLGroupFilter("AND");
	qft.addFilter(new QLEqualsFilter("numemp", numemp));
	qft.addFilter(new QLEqualsFilter("tipcol", tipcol));
	qft.addFilter(new QLEqualsFilter("numcad", numcad));
	List<NeoObject> objTarefa = PersistEngine.getObjects(AdapterUtils.getEntityClass("FUETarefaCadastroEPI"), qft);
	if (objTarefa.isEmpty())
	{
	    QLGroupFilter filtroExecutor = new QLGroupFilter("AND");
  		filtroExecutor.addFilter(new QLEqualsFilter("definicaoDeposito.DepositoEntrega.escritorioRegional.codigo", regional));
  		
  		if(regional == 0L || regional == 1L || regional == 9L){
  		  filtroExecutor.addFilter(new QLEqualsFilter("usuarioFusion.code", "ingrid.silva"));    
  		}
		
  		List<NeoObject> objLUD = PersistEngine.getObjects(AdapterUtils.getEntityClass("FUELigacaoUsuarioDeposito"), filtroExecutor);
		for (NeoObject objL : objLUD)
		{
			EntityWrapper wUsu = new EntityWrapper(objL);
			usuFus = (NeoUser) wUsu.findValue("usuarioFusion");
			break;
		}

		Connection conn = PersistEngine.getConnection("VETORH");
		PreparedStatement st = null;
		try
		{
			StringBuffer sql1 = new StringBuffer();
			sql1.append("SELECT NomFun FROM R034FUN WHERE ");
			sql1.append("NUMEMP = ? AND TIPCOL = ? AND NUMCAD = ?");

			st = conn.prepareStatement(sql1.toString());
			st.setLong(1, numemp);
			st.setLong(2, tipcol);
			st.setLong(3, numcad);

			ResultSet rs = st.executeQuery();

			if (rs.next())
			{
				nomfun = rs.getString("NomFun");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	
			QLGroupFilter groupFilter = new QLGroupFilter("AND");
			groupFilter.addFilter(new QLEqualsFilter("codigoRegional", regional));
			NeoObject objs = PersistEngine.getObject(AdapterUtils.getEntityClass("EPITarefaAutomatica"), groupFilter);
			EntityWrapper Wrapper = new EntityWrapper(objs);
			NeoUser responsavelRegional = (NeoUser) Wrapper.findField("responsavel").getValue();
			NeoUser neoSolicitante = responsavelRegional;
			solicitante = neoSolicitante.getCode();
			
			executor = usuFus.getCode();
		

		String titulo = "Cadastro da EPI - " + numemp + "/" + numcad + " - " + nomfun;
		titulo = URLEncoder.encode(titulo, "ISO-8859-1");

		String descricao = "";
		descricao = " <strong>Cadastrar EPI para o colaborador(a):</strong> " + numemp + "/" + numcad + " - " + nomfun + "<br>";
		descricao = URLEncoder.encode(descricao, "ISO-8859-1");

		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 3L);

		String urlTarefa = "";
		urlTarefa = OrsegupsUtils.URL_HOMOLOGACAO + ":8080/fusion/custom/jsp/orsegups/iniciarTarefaSimples.jsp?avanca=sim&solicitante=" + solicitante + "&executor=" + executor + "&origem=1&descricao=" + descricao + "&titulo=" + titulo + "&prazo=" + NeoUtils.safeDateFormat(prazo, "dd/MM/yyyy");

		URL url = new URL(urlTarefa);
		URLConnection uc = url.openConnection();
		BufferedReader leitorArquivo = new BufferedReader(new InputStreamReader(uc.getInputStream()));
		String tarefa = "";
		String leitor = "";
		while ((leitor = leitorArquivo.readLine()) != null)
		{
			tarefa = leitor;
		}

		InstantiableEntityInfo tarefaEPI = AdapterUtils.getInstantiableEntityInfo("FUETarefaCadastroEPI");
		NeoObject no = tarefaEPI.createNewInstance();
		EntityWrapper tWrapper = new EntityWrapper(no);

		tWrapper.findField("numemp").setValue(numemp);
		tWrapper.findField("tipcol").setValue(tipcol);
		tWrapper.findField("numcad").setValue(numcad);
		tWrapper.findField("tarefa").setValue(tarefa);
		PersistEngine.persist(no);

		out.print(tarefa);
	}
%>