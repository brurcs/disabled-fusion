<%@page import="com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples"%>
<%@page import="com.neomind.fusion.security.NeoPaper"%>
<%@page import="com.neomind.fusion.security.SecurityEntity"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.doc.NeoDocument"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="/WEB-INF/favorite.tld" prefix="favorite"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/webui.tld" prefix="wui"%>

<%@page import="com.neomind.fusion.entity.FieldWrapperGroup"%>
<%@page import="com.neomind.fusion.entity.FieldWrapper"%>
<%@page import="com.neomind.fusion.doc.NeoStorage"%>
<%@page import="net.sf.jasperreports.engine.JasperCompileManager"%>
<%@page import="net.sf.jasperreports.engine.JRException"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.workflow.Task"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.workflow.UserActivity"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="java.util.HashSet"%>
<body><%
	
	String executor = "";
	Long regional = NeoUtils.safeLong(request.getParameter("regional"));
	String papel = NeoUtils.safeString(request.getParameter("papel")); 

	NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", request.getParameter("solicitante")));
	if(solicitante == null)
	{
		out.print("Erro #1 - Solicitante n�o encontrado (#"+request.getParameter("solicitante")+")");
		return;
	}	
	
	QLEqualsFilter filtroRegional = new QLEqualsFilter("codigo",regional);
	NeoObject reg = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("GCEscritorioRegional"),filtroRegional);
	
	if(NeoUtils.safeIsNull(reg)){
		out.print("Erro #2 - Valor de regional n�o encontrado");
		return;	
	}
	
	QLEqualsFilter filtroRegionalObj = new QLEqualsFilter("regional", reg);
	NeoObject checkList = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("CHECKLISTRegionais"),filtroRegionalObj);
	EntityWrapper checklistWrapper = new EntityWrapper(checkList);
	List<NeoPaper> listaPapeis = (List<NeoPaper>) checklistWrapper.findField("papel").getValue();
	
	if(NeoUtils.safeIsNull(listaPapeis)){		
		out.print("Erro #7 - Lista de pap�is no checklist n�o encontrado");
		return;		
	}
	
	for(NeoPaper paper : listaPapeis){		
		if(NeoUtils.safeIsNotNull(paper)){			
			if(paper.getCode().startsWith(papel)){
				for(NeoUser user : paper.getAllUsers()){					
					if(user.isActive()){
						executor = user.getCode();
						break;
					}
				}
			}
		}
	}
	
	if(executor.equals("")){
		out.print("Erro #8 - Nenhum usu�rio ativo para este papel");
		return;	
	}
	
	String titulo = request.getParameter("titulo");
	String descricao = request.getParameter("descricao");

	GregorianCalendar prazo = AdapterUtils.getGregorianCalendar(request.getParameter("prazo"), "dd/MM/yyyy");
	if(prazo == null)
	{
		out.print("Erro #3 - Erro ao gerar o prazo (#"+request.getParameter("prazo")+")");
		return;
	}
	
	InstantiableEntityInfo eOrigem = AdapterUtils.getInstantiableEntityInfo("origemTarefa");
	if(eOrigem == null)
	{
		out.print("Erro #5 - Eform origemTarefa n�o encontrado");
		return;
	}
	
	NeoObject origem = (NeoObject) PersistEngine.getObject(eOrigem.getEntityClass(), new QLEqualsFilter("codigo", 2));
	if(origem == null)
	{
		out.print("Erro #6 - Valor de origem para Check List n�o encontrado");
		return;
	}
	
	IniciarTarefaSimples tarefaSimples = new IniciarTarefaSimples();
	String retorno = tarefaSimples.abrirTarefa(solicitante.getCode(), executor, titulo, descricao, "2", "sim", prazo);
	
	out.print(retorno);
%>
</body>
