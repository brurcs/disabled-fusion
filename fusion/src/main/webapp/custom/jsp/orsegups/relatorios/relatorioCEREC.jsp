<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/form.tld" prefix="form"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>


<portlet:defineObjects />
<form:defineObjects />
<head>
	<meta charset="UTF-8">
</head>
<portal:head title="Relat�rio CEREC">
<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/icon.css"> 
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui-lang-pt_BR.js"></script>
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/datagrid-detailview.js"></script>	
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL() %>custom/jsp/orsegups/overlib/overlib.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
<script language="JavaScript" type="text/javascript">
		
</script>
<cw:main>

	<cw:header title="Relat�rios CEREC" />
	
	<cw:body id="area_scroll">
	
	<div class="float_icos">
		<a href="javascript:void(0)" plain="true" onclick="abrirRelatorioProdutividadeCEREC()">
	<p>
	<img src="http://intranet.orsegups.com.br/fusion/imagens/icones_final/report_vis_64x64-trans.png">
	</p>
	<p>
	<strong>Produtividade CEREC</strong>
	</p>
	
	<p>Relat�rio de Produtividade CEREC</p>
	</a>
	</a>
	</div>
	
	<div class="float_icos">
		<a href="javascript:void(0)" plain="true" onclick="abrirRelatorioNegociacaoCEREC()">
	<p>
	<img src="http://intranet.orsegups.com.br/fusion/imagens/icones_final/report_vis_64x64-trans.png">
	</p>
	<p>
	<strong>Negocia��o CEREC</strong>
	</p>
	
	<p>Relat�rio de Negocia��o CEREC</p>
	</a>
	</a>
	</div>
	
	<div class="float_icos">
		<a href="javascript:void(0)" plain="true" onclick="abrirRelatorioCobrancaEletronica()">
	<p>
	<img src="http://intranet.orsegups.com.br/fusion/imagens/icones_final/report_vis_64x64-trans.png">
	</p>
	<p>
	<strong>Cobran��o El�tronica Inadimpl�ncia</strong>
	</p>
	
	<p>Relat�rio Cobran��o El�tronica Inadimpl�ncia</p>
	</a>
	</a>
	</div>
	
	<div class="float_icos">
		<a href="javascript:void(0)" plain="true" onclick="abrirRelatorioCobrancaHumana()">
	<p>
	<img src="http://intranet.orsegups.com.br/fusion/imagens/icones_final/report_vis_64x64-trans.png">
	</p>
	<p>
	<strong>Cobran��o Humana Inadimpl�ncia</strong>
	</p>
	
	<p>Relat�rio Cobran��o Humana Inadimpl�ncia</p>
	</a>
	</a>
	</div>
	
	<div class="float_icos">
		<a href="javascript:void(0)" plain="true" onclick="abrirRelatorioDebitoAutomatico()">
	<p>
	<img src="http://intranet.orsegups.com.br/fusion/imagens/icones_final/report_vis_64x64-trans.png">
	</p>
	<p>
	<strong>D�bito Autom�tico</strong>
	</p>
	
	<p>Relat�rio D�bito Autom�tico</p>
	</a>
	</a>
	</div>
		
    <div id="dlg" class="easyui-dialog" style="width:420px;height:260px;padding:5px 10px,"
            closed="true" buttons="#dlg-buttons" data-options="iconCls:'icon-print',modal:true">
        <div class="ftitle">Informa��es</div>
        <form id="fm" method="post" novalidate>
            <div class="fitem">
                <label>Data Inicial:</label>
                <input
				id="datIni" name="datIni" type="text" class="easyui-datebox" required="true"
				data-options="formatter:myformatter,parser:myparser, editable:false"
				style="width: 100px;font-size:11px;">

            </div>
            <div class="fitem">
                <label>Data Final:</label>
               <input 
				id="datFim" name="datFim" type="text" class="easyui-datebox" required="true" 
				data-options="formatter:myformatter,parser:myparser, editable:false"
				style="width: 100px;font-size:11px;">

            </div>
            <div class="fitem">
                <label>Colaborador:</label>
               <input id="cbColaborador" name="cbColaborador" class="easyui-combobox"  style="width: 200px;font-size:11px;" data-options="
                    url:'servlet/com.neomind.fusion.custom.orsegups.servlets.ListJsonServlet?action=listaColaboradoresCerec',
                    valueField:'fullname',method:'get',required:false,
                    textField:'fullname', multiple:false, editable:true">

            </div>
            <div class="fitem">
                <label>Grupo:</label>
            <select
				id="setGrup" name="setGrup" class="easyui-combobox" style="width: 200px;font-size:11px;" required="true"
				data-options="panelHeight:'120', editable:false">
					<option value="Execu��o RSC">Execu��o RSC</option>
					<option value="Cobran�a">Cobran�a</option>
					<option value="Call Center">Call Center</option>
					<option value="Call Center 2">Call Center 2</option>
					<option value="Supervis�o NAC Interno">Supervis�o NAC Interno</option>
					<option value="Supervis�o NAC Interno">Gest�o CEREC</option>
					<option value="Ouvidorias">Ouvidoria</option>
			</select>
			 </div>
            
            <div class="fitem">
                <label>Formato :</label>
            <select
				id="setFor" name="setFor" class="easyui-combobox" style="width: 100px;font-size:11px;" required="true"
				data-options="panelHeight:'46', editable:false">
					<option value="PDF">PDF</option>
					<option value="XLS">XLS</option>
			</select>
			 </div>
        </form>
    </div>
    <div id="dlg-buttons">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-print" onclick="javascript:gerarRelatorioProdutividadeCEREC()">Imprimir</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Cancelar</a>
    </div>
    
    <div id="dlgNeg" class="easyui-dialog" style="width:420px;height:260px;padding:5px 10px"
            closed="true" buttons="#dlg-buttons-Neg" data-options="iconCls:'icon-print',modal:true">
        <div class="ftitle">Informa��es</div>
        <form id="fmNeg" method="post" novalidate>
            <div class="fitem">
                <label>Data Inicial:</label>
                <input
				id="datIniNeg" name="datIniNeg" type="text" class="easyui-datebox" required="true"
				data-options="formatter:myformatter,parser:myparser, editable:false"
				style="width: 100px;font-size:11px;">

            </div>
            <div class="fitem">
                <label>Data Final:</label>
               <input 
				id="datFimNeg" name="datFimNeg" type="text" class="easyui-datebox" required="true" 
				data-options="formatter:myformatter,parser:myparser, editable:false"
				style="width: 100px;font-size:11px;">

            </div>
            <div class="fitem">
                <label>Formato :</label>
            <select
				id="setForNeg" name="setForNeg" class="easyui-combobox" style="width: 100px;font-size:11px;" required="true"
				data-options="panelHeight:'46', editable:false">
					<option value="PDF">PDF</option>
					<option value="XLS">XLS</option>
			</select>
			 </div>
        </form>
    </div>
    <div id="dlg-buttons-Neg">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-print" onclick="javascript:gerarRelatorioNegociacaoCEREC()">Imprimir</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Cancelar</a>
    </div>
    
     <div id="dlgCobEletr" class="easyui-dialog" style="width:420px;padding:5px 50px"
            closed="true" buttons="dlg-buttons-CobrEletr" data-options="iconCls:'icon-print',modal:true">
        <div class="ftitle">Informa��es</div>
        <form id="fmCobEletr" method="post" novalidate>
        
        	 <div class="fitem">
                <label>Relat�rios :</label>
            <select
				id="setRelCobEletr" name="setRelCobEletr" class="easyui-combobox" style="width: 260px;font-size:11px;" required="true"
				data-options="panelHeight:'46', editable:false">
					<option value="1234P">1�, 2�, 3� e 4� Negociou </option>
					<!-- <option value="GC">Gestor Cobrou</option> -->
					<option value="123GuedesSimNao">1�, 2� e 3� Guedes Pinto Negociou Sim/N�o</option>
			</select>
			 </div>
        	
            <div class="fitem">
                <label>Data Inicial:</label>
                <input
				id="datIniCobEletr" name="datIniCobEletr" type="text" class="easyui-datebox" required="true"
				data-options="formatter:myformatter,parser:myparser, editable:false"
				style="width: 260px;font-size:11px;">

            </div>
            <div class="fitem">
                <label>Data Final:</label>
               <input 
				id="datFimCobEletr" name="datFimCobEletr" type="text" class="easyui-datebox" required="true" 
				data-options="formatter:myformatter,parser:myparser, editable:false"
				style="width: 260px;font-size:11px;">

            </div>
            
            <div class="fitem">
                <label>Formato :</label></br>
            <select
				id="setForCobEletr" name="setForCobEletr" class="easyui-combobox" style="width: 100px;font-size:11px;" required="true"
				data-options="panelHeight:'46', editable:false">
					<option value="PDF">PDF</option>
					<option value="XLS">XLS</option>
			</select>
			 </div>
			 
        </form>
        <div id="dlg-buttons-CobrEletr">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-print" onclick="javascript:gerarRelatorioCobrancaEletronica()">Imprimir</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgCobEletr').dialog('close')">Cancelar</a>
    	</div>
    </div>
    
    <div id="dlgDebito" class="easyui-dialog" style="width:420px;height:260px;padding:5px 10px"
            closed="true" buttons="#dlg-buttons-Deb" data-options="iconCls:'icon-print',modal:true">
        <div class="ftitle">Informa��es</div>
        <form id="fmDeb" method="post" novalidate>
            <div class="fitem">
                <label>Data Inicial:</label>
                <input
				id="datIniDeb" name="datIniDeb" type="text" class="easyui-datebox" required="true"
				data-options="formatter:myformatter,parser:myparser, editable:false"
				style="width: 100px;font-size:11px;">

            </div>
            <div class="fitem">
                <label>Data Final:</label>
               <input 
				id="datFimDeb" name="datFimDeb" type="text" class="easyui-datebox" required="true" 
				data-options="formatter:myformatter,parser:myparser, editable:false"
				style="width: 100px;font-size:11px;">

            </div>
			
			<div class="fitem">
                <label>Formato :</label>
            <select
				id="setForDeb" name="setForDeb" class="easyui-combobox" style="width: 100px;font-size:11px;" required="true"
				data-options="panelHeight:'46', editable:false">
					<option value="PDF">PDF</option>
					<option value="XLS">XLS</option>
			</select>
			 </div>
						
        </form>
        <div id="#dlg-buttons-Deb">
		        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-print" onclick="javascript:gerarRelatorioDebitoAutomatico()">Imprimir</a>
		        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgDebito').dialog('close')">Cancelar</a>
		    </div>
    </div>
    
    
    <div id="dlgCobHum" class="easyui-dialog" style="width:420px;padding:5px 50px"
            closed="true" buttons="dlg-buttons-CobHum" data-options="iconCls:'icon-print',modal:true">
        <div class="ftitle">Informa��es</div>
        <form id="fmCobHum" method="post" novalidate>
        
        	 <div class="fitem">
                <label>Relat�rios :</label>
            <select
				id="setRelCobHum" name="setRelCobHum" class="easyui-combobox" style="width: 260px;font-size:11px;" required="true"
				data-options="panelHeight:'70', editable:false">
					<option value="1234P">1�, 2�, 3� e 4� Negociou</option>
					<option value="GC">Gestor Negociou </option>
					<option value="abriuTarefaCancelamento">Cliente N�o Negociou na Cobran�a</option>
			</select>
			 </div>
        	
            <div class="fitem">
                <label>Data Inicial:</label>
                <input
				id="datIniCobHum" name="datIniCobHum" type="text" class="easyui-datebox" required="true"
				data-options="formatter:myformatter,parser:myparser, editable:false"
				style="width: 260px;font-size:11px;">

            </div>
            <div class="fitem">
                <label>Data Final:</label>
               <input 
				id="datFimCobHum" name="datFimCobHum" type="text" class="easyui-datebox" required="true" 
				data-options="formatter:myformatter,parser:myparser, editable:false"
				style="width: 260px;font-size:11px;">

            </div>
            
            <div class="fitem">
                <label>Formato :</label></br>
            <select
				id="setForCobHum" name="setForCobHum" class="easyui-combobox" style="width: 100px;font-size:11px;" required="true"
				data-options="panelHeight:'46', editable:false">
					<option value="PDF">PDF</option>
					<option value="XLS">XLS</option>
			</select>
			 </div>
			 
        </form>
        <div id="dlg-buttons-CobHum">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-print" onclick="javascript:gerarRelatorioCobrancaHumana()">Imprimir</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlgCobHum').dialog('close')">Cancelar</a>
    	</div>
    </div>
    
    				
   	<script type="text/javascript">
        var url;
        function abrirRelatorioProdutividadeCEREC(){
            $('#dlg').dialog('open').dialog('setTitle','&nbspProdutividade CEREC');
            $('#fm').form('clear');
        }
        function gerarRelatorioProdutividadeCEREC(){
        	
    		var datIni = $('#datIni').datebox('getValue');
    		var datFim =  $('#datFim').datebox('getValue');
    		var nome = $('#cbColaborador').combobox('getValue');
    		var setFor = $('#setFor').combobox('getValue');
    		var setGrup = $('#setGrup').combobox('getValue');
    		var servlet = "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.servlets.RelatoriosServlet?action=produtividadeCerec&datini=+"+datIni+"+&datfim=+"+datFim+"+&fullName=+"+nome+"+&setfor=+"+setFor+"+&setGrup=+"+setGrup;
    		
            $('#fm').form('submit',{
                url: servlet,
                onSubmit: function(){
                	var isValid = $(this).form('validate');
                	if (!isValid){
                		alertaCampos();
            		}else{
            			progress();
            		}
                    return isValid;
                },
                success: function(result){
                    var result = eval('('+result+')');
                    if (result.errorMsg){
                    	error();
                    	$.messager.show({
                            title: 'Error',
                            msg: result.errorMsg
                        });
                    } else {
//                     	document.location.href=servlet;
                    	result();
                        $('#dlg').dialog('close');        // close the dialog
                        $('#dg').datagrid('reload');    // reload the user data
                    }
                }
            });
        }
        
        function abrirRelatorioNegociacaoCEREC(){
            $('#dlgNeg').dialog('open').dialog('setTitle','&nbspNegocia��o CEREC');
            $('#fmNeg').form('clear');
        }
        function gerarRelatorioNegociacaoCEREC(){
        	
    		var datIni = $('#datIniNeg').datebox('getValue');
    		var datFim =  $('#datFimNeg').datebox('getValue');
    		var setFor = $('#setForNeg').combobox('getValue');
    		var servlet = "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.servlets.RelatoriosServlet?action=negociacaoCEREC&datini=+"+datIni+"+&datfim=+"+datFim+"+&setfor=+"+setFor;														
    		
            $('#fmNeg').form('submit',{
                url: servlet,
                onSubmit: function(){
                	var isValid = $(this).form('validate');
                	if (!isValid){
                		alertaCampos();
            		}else{
            			progress();
            		}
                    return isValid;
                },
                success: function(result){
                    var result = eval('('+result+')');
                    if (result.errorMsg){
                    	error();
                    	$.messager.show({
                            title: 'Error',
                            msg: result.errorMsg
                        });
                    } else {
//                     	document.location.href=servlet;
                    	result();
                        $('#dlgNeg').dialog('close');        // close the dialog
                        $('#dgNeg').datagrid('reload');    // reload the user data
                    }
                }
            });
        }
        
        function abrirRelatorioCobrancaEletronica(){
            $('#dlgCobEletr').dialog('open').dialog('setTitle','&nbspCobran�a Eletr�nica Inadimpl�ncia');
            $('#fmCobEletr').form('clear');
        }
        function gerarRelatorioCobrancaEletronica(){
        	
        	var relatorio = $('#setRelCobEletr').datebox('getValue');
    		var datIni = $('#datIniCobEletr').datebox('getValue');
    		var datFim =  $('#datFimCobEletr').datebox('getValue');
    		var nome = $('#cbColaborador').combobox('getValue');
    		var setFor = $('#setForCobEletr').combobox('getValue');
    		var servlet = "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.servlets.RelatoriosServlet?action=relatorioCobrancaInadimplencia&datini=+"+datIni+"+&datfim=+"+datFim+"+&fullName=+"+nome+"+&setRel=+"+relatorio+"+&setfor=+"+setFor;
    		
            $('#fmCobEletr').form('submit',{
                url: servlet,
                onSubmit: function(){
                	var isValid = $(this).form('validate');
                	if (!isValid){
                		alertaCampos();
            		}else{
            			progress();
            		}
                    return isValid;
                },
                errorDias: function(result){
                	stopProgress();
                 	$.messager.show({
                       	title:'Mensagem',
                       	msg:'Per�odo Maior que 31 Dias!.',
                       	timeout:5000,
                       	showType:'slide'
                	});
                 	return;
                },
                success: function(result){
                    var result = eval('('+result+')');
                    if (result.errorMsg){
                    	error();
                    	$.messager.show({
                            title: 'Error',
                            msg: result.errorMsg
                        });
                    } else {
//                     	document.location.href=servlet;
                    	result();
                        $('#dlgCobEletr').dialog('close');        // close the dialog
                        $('#dlgCobEletr').datagrid('reload');    // reload the user data
                    }
                }
            });
        }
        
        function abrirRelatorioCobrancaHumana(){
            $('#dlgCobHum').dialog('open').dialog('setTitle','&nbspCobran�a Humana Inadimpl�ncia');
            $('#fmCobHum').form('clear');
        }
        function gerarRelatorioCobrancaHumana(){
        	
        	var relatorio = $('#setRelCobHum').datebox('getValue');
    		var datIni = $('#datIniCobHum').datebox('getValue');
    		var datFim =  $('#datFimCobHum').datebox('getValue');
    		var setFor = $('#setForCobHum').combobox('getValue');
    		var servlet = "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.servlets.RelatoriosServlet?action=relatorioCobrancaHuma&datini=+"+datIni+"+&datfim=+"+datFim+"+&setRel=+"+relatorio+"+&setfor=+"+setFor;
    		
            $('#fmCobHum').form('submit',{
                url: servlet,
                onSubmit: function(){
                	var isValid = $(this).form('validate');
                	if (!isValid){
                		alertaCampos();
            		}else{
            			progress();
            		}
                    return isValid;
                },
                success: function(result){
                    var result = eval('('+result+')');
                    if (result.errorMsg){
                    	error();
                    	$.messager.show({
                            title: 'Error',
                            msg: result.errorMsg
                        });
                    } else {
//                     	document.location.href=servlet;
                    	result();
                        $('#dlgCobHum').dialog('close');        // close the dialog
                        $('#dlgCobHum').datagrid('reload');    // reload the user data
                    }
                }
            });
        }
        
        function abrirRelatorioDebitoAutomatico(){
            $('#dlgDebito').dialog('open').dialog('setTitle','&nbspD�bito Autom�tico');
            $('#fmDeb').form('clear');
        }
        function gerarRelatorioDebitoAutomatico(){
        	
    		var datIni = $('#datIniDeb').datebox('getValue');
    		var datFim =  $('#datFimDeb').datebox('getValue');
			var setFor = $('#setForDeb').combobox('getValue');
    		var servlet = "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.servlets.RelatoriosServlet?action=debitoAutomatico&datini=+"+datIni+"+&datfim=+"+datFim+"+&setfor=+"+setFor;																
    		
            $('#fmDeb').form('submit',{
                url: servlet,
                onSubmit: function(){
                	var isValid = $(this).form('validate');
                	if (!isValid){
                		alertaCampos();
            		}else{
            			progress();
            		}
                    return isValid;
                },
                success: function(result){
                    var result = eval('('+result+')');
                    if (result.errorMsg){
                    	error();
                    	$.messager.show({
                            title: 'Error',
                            msg: result.errorMsg
                        });
                    } else {
//                     	document.location.href=servlet;
                    	result();
                        $('#dlgDeb').dialog('close');        // close the dialog
                        $('#dgDeb').datagrid('reload');    // reload the user data
                    }
                }
            });
        }
        
       
        
        function myformatter(date) {
    		var y = date.getFullYear();
    		var m = date.getMonth() + 1;
    		var d = date.getDate();
    		return (d < 10 ? ('0' + d) : d) + '/' + (m < 10 ? ('0' + m) : m) + '/'
    				+ y;

    	}
    	function myparser(s) {
    		if (!s)
    			return new Date();
    		var ss = (s.split('-'));
    		var y = parseInt(ss[0], 10);
    		var m = parseInt(ss[1], 10);
    		var d = parseInt(ss[2], 10);
    		if (!isNaN(y) && !isNaN(m) && !isNaN(d)) {
    			return new Date(d, m - 1, y);
    		} else {
    			return new Date();
    		}
    	}
    	
    	function alertaCampos(){
            $.messager.alert('Aten��o','Campos Obrigat�rios!','warning');
        }
    	
    	function progress(){
	            var win = $.messager.progress({
	                title:'Por favor, aguarde!',
	                msg:'Carregando dados...'
	            });
	            setTimeout(function(){
	                $.messager.progress('close');
	                result();
	            },7000)
	    }
    	 
    	function result(){
    			$.messager.alert('Aten��o','Relat�rio gerado com sucesso!','info');

    			
    	}
    		
    	function error(){
    			$.messager.alert('Aten��o','Erro ao listar informa��es!','error');
    			
    	}
    	
    	function errorDias(){
			$.messager.alert('Aten��o','Per�odo Maior que 31 Dias','error');
			
		}
    	
    	function formatItemColaborador(row){
            var s = '<span style="color:#888">' + row.fullname + '</span>';
            return s;
        }
    	
    </script>
    <style type="text/css">
        #fm{
            margin:0;
            padding:10px 30px;
        }
        .ftitle{
            font-size:14px;
            font-weight:bold;
            padding:5px 0;
            margin-bottom:10px;
            border-bottom:1px solid #ccc;
        }
        .fitem{
            margin-bottom:5px;
        }
        .fitem label{
            display:inline-block;
            width:80px;
        }
    </style>
	
	
	
	
	
<style type="text/css">
input,textarea {
	border: 1px solid #95B8E7;
	padding: 2px;
}

       
</style>
	</cw:body>
	
</cw:main>
</portal:head>
