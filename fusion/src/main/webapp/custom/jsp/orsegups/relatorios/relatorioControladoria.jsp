<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/form.tld" prefix="form"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/content_window.tld" prefix="cw"%>


<portlet:defineObjects />
<form:defineObjects />
<head>
	<meta charset="UTF-8">
</head>
<portal:head title="Relatório Financeiro">
<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" 
	href="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/themes/icon.css"> 
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/jquery.easyui-lang-pt_BR.js"></script>
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/jms/datagrid-detailview.js"></script>	
<script type="text/javascript" 
	src="<%=PortalUtil.getBaseURL() %>custom/jsp/orsegups/overlib/overlib.js"></script>
<script type="text/javascript"
	src="<%=PortalUtil.getBaseURL()%>custom/jsp/orsegups/maps/js/json2.js"></script>
<script language="JavaScript" type="text/javascript">

		
		function gerarRelatorioRateioNF(){
			$.messager.confirm('Relatório RateioNF', 'Deseja gerar relatório de Rateio NF?', function(r){
				if (r){
					var servlet = "<%=PortalUtil.getBaseURL()%>servlet/com.neomind.fusion.custom.orsegups.servlets.RelatoriosServlet?action=rnf";
					document.location.href=servlet;
				}
			});
		}

	
	</script>
<cw:main>

	<cw:header title="Relatórios Controladoria" />
	
	<cw:body id="area_scroll">	
		
	<div class="float_icos">
	
	
	 <a href="javascript:void(0)" plain="true" onclick="gerarRelatorioRateioNF()">
	
	<p>
	<img src="http://intranet.orsegups.com.br/fusion/imagens/icones_final/report_vis_64x64-trans.png">
	</p>
	<p>
	<strong>Rateio NF</strong>
	</p>
	
	<p>Relatório de Rateio NF</p>
	</a>
	
	</div>
	
    <style type="text/css">
        #fm{
            margin:0;
            padding:10px 30px;
        }
        .ftitle{
            font-size:14px;
            font-weight:bold;
            padding:5px 0;
            margin-bottom:10px;
            border-bottom:1px solid #ccc;
        }
        .fitem{
            margin-bottom:5px;
        }
        .fitem label{
            display:inline-block;
            width:80px;
        }
    </style>
	
	
	
	
	
<style type="text/css">
input,textarea {
	border: 1px solid #95B8E7;
	padding: 2px;
}

       
</style>
	</cw:body>
	
</cw:main>
</portal:head>
