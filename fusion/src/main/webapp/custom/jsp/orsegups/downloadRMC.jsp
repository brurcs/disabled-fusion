<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.doc.NeoDocument"%>
<%@page import="com.neomind.fusion.entity.FieldWrapperGroup"%>
<%@page import="com.neomind.fusion.entity.FieldWrapper"%>
<%@page import="com.neomind.fusion.doc.NeoStorage"%>
<%@page import="net.sf.jasperreports.engine.JasperCompileManager"%>
<%@page import="net.sf.jasperreports.engine.JRException"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.doc.NeoFile"%>
<%@page import="java.io.File"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.BufferedInputStream"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="com.neomind.fusion.workflow.Task"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.workflow.UserActivity"%>
<%@page import="java.util.HashSet"%>
<%@page import="com.neomind.fusion.datawarehouse.ExternalEntityInfo"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.persist.QLGroupFilter"%>
<%@page import="java.util.Collection"%>
<%@page import="com.neomind.fusion.security.SecurityEntity"%>
<%@page import="com.neomind.fusion.security.NeoPaper"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.StringTokenizer"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>


<body><%


	String nomeFonteDados = "SAPIENS";
	PreparedStatement st = null;	
	PreparedStatement st2 = null;	
	String hash = request.getParameter("arq");
	Connection conn = null;

	if (hash == null) {
		hash = "";
	}
	Date dataDownload = null;
	String nomeArquivo = null;

	try {
		conn = PersistEngine.getConnection(nomeFonteDados);

		StringBuffer sql1 = new StringBuffer();
		sql1.append("SELECT *  ");
		sql1.append("FROM USU_T160RXF ");
		sql1.append("WHERE usu_hasace = '" + hash + "'");
		
		st = conn.prepareStatement(sql1.toString());
		ResultSet rs = st.executeQuery();
	
		if (rs.next()) {
			nomeArquivo = rs.getString("usu_nomarq");
			if(nomeArquivo == null || nomeArquivo.equals("")) {
				out.print("Erro #1 - Nome do arquivo recuperado � inv�lido: " + nomeArquivo + ".");
				return;
			}
			dataDownload = rs.getDate("usu_dowdat");
		} else {
			out.print("Erro #2 - Nenhum hash encontrado para download. (HASH: " + hash + ")");
			return;
		} 

		if (dataDownload == null) {
			StringBuffer sql2 = new StringBuffer();
			sql2.append("UPDATE USU_T160RXF ");
			sql2.append("SET usu_dowdat = GETDATE(), usu_dowhor = (datepart(hour,getdate()) * 60) +datepart(minute,getdate()), usu_dowhos = ? ");
			sql2.append("WHERE usu_hasace = ? ");
			st2 = conn.prepareStatement(sql2.toString());
			String src = "WAN: " + request.getRemoteAddr() + " - LAN: " + request.getHeader("x-forwarded-for");
			int length = 255;
			if (src.length() < 255) {
				length = src.length();
			}
			st2.setString(1, src.substring(0, length));
			st2.setString(2, hash);
			st2.executeUpdate();			
		}
		
	}  catch (Exception e) {
		e.printStackTrace();
		out.print("Erro #3 - Erro ao recuperar informa��es do download.");
		return;    
	} finally {
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			out.print("Erro #4 - Erro ao finalizar conex�o ");
			return;    
		}
	}
		
	try {
		String fileName = nomeArquivo.substring(nomeArquivo.lastIndexOf("\\")+1);
		File file = new File(nomeArquivo);
		FileInputStream fileIn = new FileInputStream(file);
		ServletOutputStream outJsp = response.getOutputStream();
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition","attachment;filename="+fileName);

		byte[] outputByte = new byte[4096];
		//copy binary contect to output stream
		while(fileIn.read(outputByte, 0, 4096) != -1) {
			outJsp.write(outputByte, 0, 4096);
		}
		fileIn.close();	
	} catch (Exception e) {
		e.printStackTrace();
		out.print("Erro #5 - Arquivo n�o encontrado: " + nomeArquivo);
		return;
	}


%>
</body>

