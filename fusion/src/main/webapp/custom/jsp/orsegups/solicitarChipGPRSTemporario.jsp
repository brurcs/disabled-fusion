<%@page
	import="com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper"%>
<%@page import="com.neomind.fusion.workflow.simulation.WorkflowService"%>
<%@page import="com.neomind.util.NeoUtils"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.doc.NeoDocument"%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="/WEB-INF/i18n.tld" prefix="i18n"%>
<%@ taglib uri="/WEB-INF/portal.tld" prefix="portal"%>
<%@ taglib uri="/WEB-INF/favorite.tld" prefix="favorite"%>
<%@ taglib uri="/WEB-INF/workflow.tld" prefix="wf"%>
<%@ taglib uri="/WEB-INF/webui.tld" prefix="wui"%>

<%@page import="com.neomind.fusion.entity.FieldWrapperGroup"%>
<%@page import="com.neomind.fusion.entity.FieldWrapper"%>
<%@page import="com.neomind.fusion.doc.NeoStorage"%>
<%@page import="net.sf.jasperreports.engine.JasperCompileManager"%>
<%@page import="net.sf.jasperreports.engine.JRException"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@page import="com.neomind.fusion.security.NeoUser"%>
<%@page import="com.neomind.fusion.security.NeoPaper"%>
<%@page import="com.neomind.fusion.security.SecurityEntity"%>
<%@page import="com.neomind.fusion.workflow.model.ProcessModel"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="com.neomind.fusion.persist.QLEqualsFilter"%>
<%@page import="com.neomind.fusion.workflow.WFProcess"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityWrapper"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.workflow.Task"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.workflow.UserActivity"%>
<%@page import="com.neomind.fusion.workflow.adapter.AdapterUtils"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="java.util.HashSet"%>
<%@page import="com.neomind.fusion.datawarehouse.ExternalEntityInfo"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils"%>
<%
	Long tipoSolicitacao = Long.parseLong(request.getParameter("tipoSolicitacao"));
	String isRastreamento = request.getParameter("isRastreamento");
	if (tipoSolicitacao < 1L)
	{
		out.print("Erro #4 - Tipo da Solicita��o nula");
		return;
	}
	NeoObject tipSol = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("ChipsGPRSPostoTipoSolicitacao"), new QLEqualsFilter("codigo", tipoSolicitacao));

	GregorianCalendar prazo = new GregorianCalendar();
	NeoUser executor = null;
	NeoUser solicitante = null;
	if (tipoSolicitacao == 1L)
	{ //Solicita��o
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
		if (isRastreamento == null || isRastreamento.equals("") || !isRastreamento.equals("SIM"))
		{
		    NeoPaper responsavel = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "GPRSResponsavelSolicitante"));
			for (NeoUser usr : responsavel.getUsers())
			{
			    solicitante = usr;
				break;
			}
		    
// 			solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "jessica.oliveira"));
			if (solicitante == null)
			{
				out.print("Erro #1 - Solicitante n�o encontrado (#"+solicitante.getName()+")");
				return;
			}

			Long regional = Long.parseLong(request.getParameter("regcvs"));
			NeoObject reg = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("GCEscritorioRegional"), new QLEqualsFilter("codigo", regional));
			if (NeoUtils.safeIsNull(reg))
			{
				out.print("Erro #2 - Valor de regional n�o encontrado");
				return;
			}
			NeoObject checkList = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("CHECKLISTRegionais"), new QLEqualsFilter("regional", reg));
			EntityWrapper checklistWrapper = new EntityWrapper(checkList);
			List<NeoPaper> listaPapeis = (List<NeoPaper>) checklistWrapper.findField("papel").getValue();
			for (NeoPaper paper : listaPapeis)
			{
				if (NeoUtils.safeIsNotNull(paper))
				{
					if (paper.getCode().startsWith("GPRS"))
					{
						for (NeoUser user : paper.getAllUsers())
						{
							if (NeoUtils.safeIsNotNull(user))
							{
								if (user.isActive())
								{
									executor = user;
									break;
								}
								else
								{
									out.print("Erro #3 - Usu�rio Inativo no Papel: " + paper.getCode());
									return;
								}
							}
						}
					}
				}
			}
		}
		else{
			
			NeoPaper responsavel = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "GPRSResponsavelSolicitante"));
			for (NeoUser usr : responsavel.getUsers())
			{
			    solicitante = usr;
				break;
			}
			
			responsavel = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "GPRSResponsavelExecutor"));
			for (NeoUser usr : responsavel.getUsers())
			{
			    executor = usr;
				break;
			}
//			executor = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "karine.lofi"));
// 			executor = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "jessica.oliveira"));
		}
	}
	else if (tipoSolicitacao == 2L)
	{ //Devolu��o
		String sPrazo = request.getParameter("dataFim");
		//prazo = AdapterUtils.getGregorianCalendar(sPrazo, "dd/MM/yyyy");
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 7L);

		NeoPaper responsavel = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "GPRSResponsavelSolicitante"));
		for (NeoUser usr : responsavel.getUsers())
		{
		    solicitante = usr;
			break;
		}
		
// 		solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "jessica.oliveira"));
		if (solicitante == null)
		{
			out.print("Erro #1 - Solicitante n�o encontrado (#"+solicitante.getName()+")");
			return;
		}

		Long regional = Long.parseLong(request.getParameter("regcvs"));
		NeoObject reg = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("GCEscritorioRegional"), new QLEqualsFilter("codigo", regional));
		if (NeoUtils.safeIsNull(reg))
		{
			out.print("Erro #2 - Valor de regional n�o encontrado");
			return;
		}
		NeoObject checkList = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("CHECKLISTRegionais"), new QLEqualsFilter("regional", reg));
		EntityWrapper checklistWrapper = new EntityWrapper(checkList);
		List<NeoPaper> listaPapeis = (List<NeoPaper>) checklistWrapper.findField("papel").getValue();
		for (NeoPaper paper : listaPapeis)
		{
			if (NeoUtils.safeIsNotNull(paper))
			{
				if (paper.getCode().startsWith("GPRS"))
				{
					for (NeoUser user : paper.getAllUsers())
					{
						if (NeoUtils.safeIsNotNull(user))
						{
							if (user.isActive())
							{
								executor = user;
								break;
							}
							else
							{
								out.print("Erro #3 - Usu�rio Inativo no Papel " + paper.getCode());
								return;
							}
						}
					}
				}
			}
		}
	}

	if (NeoUtils.safeIsNull(executor))
	{
		executor = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", executor));
	}
	if (NeoUtils.safeIsNull(solicitante))
	{
		solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", solicitante));
	}

	String titulo = request.getParameter("titulo");
	String descricao = request.getParameter("descricao");
	Long qtdechip = Long.parseLong(request.getParameter("qtdechip"));
	String osSigma = "";
	if (tipoSolicitacao == 1L)
	{
		osSigma = request.getParameter("txtObs");
	}

	String pk = request.getParameter("pk");
	if (pk == null)
	{
		out.print("Erro #5 - Erro ao gerar contrato chave do Sapiens (#" + request.getParameter("pk") + ")");
		return;
	}
	//pk = '0018000203265200137830' = CodEmp = 0018 CodFil = 0002 NumCtr = 0326520 NumPos = 0137830
	System.out.println("PK: " + pk);
	NeoObject posto = null;
	ExternalEntityInfo eContrato = (ExternalEntityInfo) EntityRegister.getCacheInstance().getByType("VCTRCVSCLI");
	List<NeoObject> listacontrato = (List<NeoObject>) PersistEngine.getObjects(eContrato.getEntityClass(), new QLEqualsFilter("pk", pk));

	System.out.println("COUNT : " + listacontrato.size());
	if (listacontrato != null && listacontrato.size() > 0)
	{
		posto = listacontrato.get(0);

	}
	else
	{
		out.print("Erro #6 - Erro ao localizar o contrato/posto do Sapiens pela chave.");
		return;
	}

	if (executor == null)
	{
		out.print("Erro #7 - Executor n�o encontrado (#" + executor + ")");
		return;
	}
	if (titulo == null)
	{
		out.print("Erro #8 - T�tulo n�o encontrado");
		return;
	}
	if (descricao == null)
	{
		out.print("Erro #9 - Descri��o n�o encontrado");
		return;
	}
	if (qtdechip == null)
	{
		out.print("Erro #10 - Quantidade de chip n�o encontrado");
		return;
	}

	//abre a tarefa
	final ProcessModel pm = (ProcessModel) PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("name", "C034 - Solicitar Chip GPRS para Posto"));

	/**
	 * @author neomind willian.mews - Alterado para utilizar a nova arquitetura de processos do Fusion
	 * @date 12/03/2015
	 */

	//final NeoObject wkfChipGPRS = pmProcess.getEntity();
	NeoObject wkfChipGPRS = AdapterUtils.createNewEntityInstance("TELECOMChipsGPRSPosto");
	EntityWrapper ewwkfChipGPRS = new EntityWrapper(wkfChipGPRS);

	ewwkfChipGPRS.findField("postoSapiens").setValue(posto);
	ewwkfChipGPRS.findField("solicitante").setValue(solicitante);
	ewwkfChipGPRS.findField("executor").setValue(executor);
	ewwkfChipGPRS.findField("tipoSolicitacao").setValue(tipSol);
	ewwkfChipGPRS.findField("titulo").setValue(titulo);
	ewwkfChipGPRS.findField("descricao").setValue(descricao);
	ewwkfChipGPRS.findField("quantidadeChip").setValue(qtdechip);
	ewwkfChipGPRS.findField("prazo").setValue(prazo);
	ewwkfChipGPRS.findField("regraAutomatica").setValue(true);

	if (tipoSolicitacao == 1L)
	{
		ewwkfChipGPRS.findField("OSSigma").setValue(osSigma);
	}

	PersistEngine.persist(wkfChipGPRS);
	//WFProcess pmProcess = WorkflowService.startProcess(pm, true, solicitante);
	//String code = OrsegupsWorkflowHelper.iniciaProcesso(pm, wkfChipGPRS, false, solicitante, true);

	//pmProcess.setEntity(wkfChipGPRS);

	/**
	 * FIM ALTERA��ES - NEOMIND
	 **/

	//apresenta na tela o c�digo da tarefa gerada
	out.print(code);
%>