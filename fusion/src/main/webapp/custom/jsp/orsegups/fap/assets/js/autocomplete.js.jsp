<%@page import="com.neomind.fusion.portal.PortalUtil"%>
<%@ page language="java" contentType="text/javascript; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

__AutoComplete = new Array();

function initAutoComplete(id,idInput)
{
	__AutoComplete[id] = {
		'cache': new Array(),
		'rawFilter': '',
		'idInput': idInput
	}
}

function AutoComplete_setRawFilter(id, raw)
{
	__AutoComplete[id]['rawFilter'] = raw;
}

function AutoComplete_ResetValue(id)
{
	$('#' + id).removeAttr('selectedvalue');
	$('#' + id).val('');
	$(__AutoComplete[id]['idInput']).val('-1');
}

function createAutoComplete(idInput, id, typeName, fieldName, filterId, fieldId, eForm, idTarget, label)
{   
	var url = '<%= PortalUtil.getBaseURL() %>entity/like';
	initAutoComplete(id, idInput);
	
	var fireOnchange = function() {
		if(idInput.onchange)
			idInput.onchange();
		if(getOnchange2(idInput))
			executeOnChangeFunc(getOnchange2(idInput));
	};
	
    $(document).ready(function () {
	    $('#' + id).autocomplete({
	    	appendTo: idTarget ? $('#'+idTarget) : null,
			source: function( request, response ) {
				if(__AutoComplete[id]['cache'][request.term])
				{
					response(__AutoComplete[id]['cache'][request.term]);
					return;
				}
				
				var loading = $('<span style="height: 35px; vertical-align:middle;" class="autocomplete-loading"><img src="<%=PortalUtil.getBaseURL()%>imagens/loadingImg.gif" id="'+id+'loading"></span>');
				if ($('#'+id+'loading').length == 0)
					$('#' + label).append(loading);
					
				$.ajax({
	                url: url + '?fusionX=' + Math.random(),
	                type: 'POST',
	                dataType: 'json',
	                data: { 
	              		info: typeName, 
	                	value: request.term, 
	                	field: fieldName,
	                	eform: eForm,
	                	filter: filterId,
	                	rawFilter: __AutoComplete[id]['rawFilter'] ? __AutoComplete[id]['rawFilter']() : '',
	                	fieldInfoId: fieldId
	                },
	                success: function(data) {
	                	loading.remove(); 
	                	__AutoComplete[id]['cache'][request.term] = data;
	                    response(data);
					}
                });
            },
            change: function( event, ui ) {
            	if(!ui.item)
            	{
            		$(idInput).val(-1);
            		$('#' + id).attr('selectedvalue', '');
            		$('#' + id).attr('value', '');
            		fireOnchange();
            	}
            },
            open: function(event, ui){
            	var iframeA = ensureGetIFrame();
            	var offset =  $('#' + id).offset();
                var autocomplete = $(this).autocomplete('widget');
                var top = offset.top + ($('#' + id).height() * 1.6);
                var isDivHigher = ((top * 1.2) + autocomplete.height() > iframeA.height());
                
                var oldTop = autocomplete.css("top").replace(/[^-\d\.]/g, '');
                var newTop = oldTop - autocomplete.height() - ($('#' + id).outerHeight(true) * 1.3);
               
                if(isDivHigher)
					autocomplete.css("top", newTop);
            },
            select: function( event, ui ) {
            	$(idInput).val(ui.item.neoid);
            	$('#' + id).attr('selectedvalue', ui.item.value);
            	$('#' + id).attr('value', ui.item.value);
            	
            	fireOnchange();
            },
            delay: 900
		});
	});
}