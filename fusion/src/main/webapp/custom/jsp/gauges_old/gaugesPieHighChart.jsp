<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>

<!DOCTYPE html>
<html>
<%
	String regional = request.getParameter("regional");
	String kpi = request.getParameter("kpi");
	String width = request.getParameter("width");
	if (width == null) width = "250";		
	String height = request.getParameter("height");
	if (height == null) height = "200";		
	String redFrom = request.getParameter("redFrom");
	if (redFrom == null) redFrom = "8";		
	String redTo = request.getParameter("redTo");
	if (redTo == null) redTo = "10";		
	String yellowFrom = request.getParameter("yellowFrom");
	if (yellowFrom == null) yellowFrom = "5";		
	String yellowTo = request.getParameter("yellowTo");
	if (yellowTo == null) yellowTo = "8";		
	String greenFrom = request.getParameter("greenFrom");
	if (greenFrom == null) greenFrom = "0";		
	String greenTo = request.getParameter("greenTo");
	if (greenTo == null) greenTo = "5";		
	String min = request.getParameter("min");
	if (min == null) min = "0";		
	String max = request.getParameter("max");
	if (max == null) max = "10";
	String showMajorTicks = request.getParameter("showMajorTicks");
	String majorTicks = request.getParameter("majorTicks");
	if (showMajorTicks == null || showMajorTicks.equals("yes")) 
	{
		if (majorTicks == null) 
		{
		  majorTicks = max;
		}

		int iMax = Integer.parseInt(majorTicks);
		majorTicks = "";
		for (int i=0;i<=iMax;i++) 
		{
		  if (i == 0)
		  {
		  majorTicks = ", majorTicks: ['" + i + "'";	  
		  } 
		  else 
		  {
		  majorTicks = majorTicks + ", '"+i+"'";	  
		  }
		  if (i == iMax)
		  {
			majorTicks = majorTicks + "]";
		  } 
		  
		}
	}
	else 
	{
		majorTicks = "";
	}
%>

  <head>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>core/portal/portal.js.jsp"></script>
  
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="./js/jquery-1.7.2.min.js"></script>
    <script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/highcharts-3d.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>
    <script type="text/javascript">
   
	
    // Load the Visualization API and the gauge package.
    google.load('visualization', '1', {packages:['corechart','gauge']});
      
    // Set a callback to run when the Google Visualization API is loaded.
    google.setOnLoadCallback(drawChart);
      
    function drawChart() {
		var colorPie = "[";
		
		var jsonData = callSync("<%=PortalUtil.getBaseURL()%>/custom/jsp/orsegups/gauges/getData.jsp?kpi=<%= kpi %>&greenFrom=<%=greenFrom%>&greenTo=<%=greenTo%>&yellowFrom=<%=yellowFrom%>&yellowTo=<%=yellowTo%>&redFrom=<%=redFrom%>&width=<%=width%>&height=<%=height%>&regional=<%= regional %>");

		var jsonDataPie = callSync("<%=PortalUtil.getBaseURL()%>/custom/jsp/orsegups/gauges/getDataPieChart.jsp?kpi=<%= kpi %>&greenFrom=<%=greenFrom%>&greenTo=<%=greenTo%>&yellowFrom=<%=yellowFrom%>&yellowTo=<%=yellowTo%>&redFrom=<%=redFrom%>&width=<%=width%>&height=<%=height%>&regional=<%= regional %>");
		// Create our data table out of JSON data loaded from server.
		if (jsonDataPie.indexOf("At�") !=-1 && jsonDataPie.indexOf("De") !=-1 && jsonDataPie.indexOf("Mais que") !=-1)
			colorPie += "\"#32CD32\",\"#FFD700\",\"#FF0000\"";
		else if (jsonDataPie.indexOf("At�") !=-1 && jsonDataPie.indexOf("De") !=-1 && !jsonDataPie.indexOf("Mais que") !=-1)
			colorPie += "\"#32CD32\",\"#FFD700\"";
		else if (jsonDataPie.indexOf("At�") !=-1 && !jsonDataPie.indexOf("De") !=-1 && jsonDataPie.indexOf("Mais que") !=-1)
			colorPie += "\"#32CD32\",\"#FF0000\"";
		else if (!jsonDataPie.indexOf("At�") !=-1 && jsonDataPie.indexOf("De") !=-1 && jsonDataPie.indexOf("Mais que") !=-1)
			colorPie += "\"#FFD700\",\"#FF0000\"";
		else if (!jsonDataPie.indexOf("At�") !=-1 && jsonDataPie.indexOf("De") !=-1 && !jsonDataPie.indexOf("Mais que") !=-1)
			colorPie += "\"#FFD700\"";
		else if (!jsonDataPie.indexOf("At�") !=-1 && !jsonDataPie.indexOf("De") !=-1 && jsonDataPie.indexOf("Mais que") !=-1)
			colorPie += "\"#FF0000\"";
		else if (jsonDataPie.indexOf("At�") !=-1 && !jsonDataPie.indexOf("De") !=-1 && !jsonDataPie.indexOf("Mais que") !=-1)
			colorPie += "\"#32CD32\"";
		var dataAsArray = "[[\"Name\", \"Number\"],        [\"Ramanujan\", 1729],        [\"Gauss\", 5050]]";
		var obj = jQuery.parseJSON(dataAsArray);
		var obj2 = jQuery.parseJSON(jsonDataPie);
		colorPie += "]";
		
		colorPie = jQuery.parseJSON(colorPie);
		
		var data = new google.visualization.DataTable(jsonData);
		var data2 = new google.visualization.arrayToDataTable(obj2);

		
		var options = {
		  width: <%= width %>, height: <%= height %>,
		  redFrom: <%= redFrom %>, redTo: <%= redTo %>,
		  yellowFrom:<%= yellowFrom %>, yellowTo: <%= yellowTo %>,
		  greenFrom: <%= greenFrom %>, greenTo: <%= greenTo %>,
		  min: <%= min %>, max: <%= max %>,
		  minorTicks: 10
		  <%= majorTicks %>
		};	  
	
		var options3 = {
				title: '<%= kpi %> - '+'<%= regional %>',
		        is3D: true,
				colors: colorPie,
		        chartArea: { bottom: 5, top: 5, width: <%= width %>, height: <%= height %> },
		        height: '90%',
		        width: '50%',
		        legend: { position: 'bottom',maxLines: 3, textStyle: { fontSize: 10, alignment: 'center' } }};
		
		// Instantiate and draw our chart, passing in some options.
		var chart = new google.visualization.Gauge(document.getElementById('chart_div'));
		chart.draw(data, options);
		var chart2 = new google.visualization.PieChart(document.getElementById('chart_div2'));
        chart2.draw(data2, options3);
		var timeoutGauge = setTimeout("drawChart()",360000);
			
    }
	
    $(function () {
        $('#container').highcharts({
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
           
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Browser share',
                data: [
                    ['Firefox',   45.0],
                    ['IE',       26.8],
                    {
                        name: 'Chrome',
                        y: 12.8,
                        sliced: true,
                        selected: true
                    },
                    ['Safari',    8.5],
                    ['Opera',     6.2],
                    ['Others',   0.7]
                ]
            }]
        });
    });
    </script>
  </head>

  <body>
    <!--Div that will hold the pie chart-->
	
    <div id="chart_div" style="height: 150px;width: 150px;float:left;position: relative;"></div>
<!--     <div id="chart_div2"  style="width: 250px; height: 200px;float:left;position: relative;"></div> -->
	<div id="container" style="height: 150px;width: 150px;float:left;position: relative;"></div>
	
  </body>
</html>