<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="com.neomind.fusion.portal.PortalUtil"%>

<!DOCTYPE html>
<html>
<%
	String regional = request.getParameter("regional");
	String kpi = request.getParameter("kpi");
	String width = request.getParameter("width");
	if (width == null) width = "300";		
	String height = request.getParameter("height");
	if (height == null) height = "220";		
	String redFrom = request.getParameter("redFrom");
	if (redFrom == null) redFrom = "8";		
	String redTo = request.getParameter("redTo");
	if (redTo == null) redTo = "10";		
	String yellowFrom = request.getParameter("yellowFrom");
	if (yellowFrom == null) yellowFrom = "5";		
	String yellowTo = request.getParameter("yellowTo");
	if (yellowTo == null) yellowTo = "8";		
	String greenFrom = request.getParameter("greenFrom");
	if (greenFrom == null) greenFrom = "0";		
	String greenTo = request.getParameter("greenTo");
	if (greenTo == null) greenTo = "5";		
	String min = request.getParameter("min");
	if (min == null) min = "0";		
	String max = request.getParameter("max");
	if (max == null) max = "10";
	String showMajorTicks = request.getParameter("showMajorTicks");
	String majorTicks = request.getParameter("majorTicks");
	if (showMajorTicks == null || showMajorTicks.equals("yes")) 
	{
		if (majorTicks == null) 
		{
		  majorTicks = max;
		}

		int iMax = Integer.parseInt(majorTicks);
		majorTicks = "";
		for (int i=0;i<=iMax;i++) 
		{
		  if (i == 0)
		  {
		  majorTicks = ", majorTicks: ['" + i + "'";	  
		  } 
		  else 
		  {
		  majorTicks = majorTicks + ", '"+i+"'";	  
		  }
		  if (i == iMax)
		  {
			majorTicks = majorTicks + "]";
		  } 
		  
		}
	}
	else 
	{
		majorTicks = "";
	}
%>

  <head>
	<script type="text/javascript"
		src="<%=PortalUtil.getBaseURL()%>core/portal/portal.js.jsp"></script>
  
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="jquery-1.6.2.min.js"></script>
    <script type="text/javascript">
   
	
    // Load the Visualization API and the gauge package.
    google.load('visualization', '1', {packages:['gauge']});
      
    // Set a callback to run when the Google Visualization API is loaded.
    google.setOnLoadCallback(drawChart);
      
    function drawChart() {
		var jsonData = callSync("<%=PortalUtil.getBaseURL()%>/custom/jsp/orsegups/gauges/getData.jsp?kpi=<%= kpi %>&regional=<%= regional %>");

		// Create our data table out of JSON data loaded from server.
		var data = new google.visualization.DataTable(jsonData);

		var options = {
		  width: <%= width %>, height: <%= height %>,
		  redFrom: <%= redFrom %>, redTo: <%= redTo %>,
		  yellowFrom:<%= yellowFrom %>, yellowTo: <%= yellowTo %>,
		  greenFrom: <%= greenFrom %>, greenTo: <%= greenTo %>,
		  min: <%= min %>, max: <%= max %>,
		  minorTicks: 10
		  <%= majorTicks %>
		};	  
	  
		// Instantiate and draw our chart, passing in some options.
		var chart = new google.visualization.Gauge(document.getElementById('chart_div'));
		chart.draw(data, options);

		var timeoutGauge = setTimeout("drawChart()",360000);
			
    }
	
    </script>
  </head>

  <body>
    <!--Div that will hold the pie chart-->
    <div id="chart_div"></div>
  </body>
</html>