<%@page import="com.neomind.fusion.scheduler.quartz.JobsRegister"%>
<%@page import="com.neomind.fusion.scheduler.Job"%>
<%@page import="com.neomind.fusion.common.NeoType"%>
<%@page import="com.neomind.fusion.engine.PersistenceReloader"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityInfo"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.entity.dynamic.SequenceFactory"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.entity.dateevent.DateEventEntity"%>
<%@page import="com.neomind.fusion.doc.NeoDocument"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="org.json.JSONObject"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<% 

List<Job> jobs = PersistEngine.getObjects(Job.class, null);

if (jobs != null && !jobs.isEmpty()){
	for (Job job : jobs){
		pauseJob(job);
	}
}


%>

<%! 

private void pauseJob(Job job)
{
	job.setEnabled(false);
	JobsRegister.pause(job);
}

%>

</body>
</html>