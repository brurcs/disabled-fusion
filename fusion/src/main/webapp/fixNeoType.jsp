<%@page import="com.neomind.fusion.common.NeoType"%>
<%@page import="com.neomind.fusion.engine.PersistenceReloader"%>
<%@page import="com.neomind.fusion.entity.InstantiableEntityInfo"%>
<%@page import="com.neomind.fusion.common.NeoObject"%>
<%@page import="com.neomind.fusion.entity.EntityInfo"%>
<%@page import="com.neomind.fusion.entity.EntityRegister"%>
<%@page import="com.neomind.fusion.workflow.Activity"%>
<%@page import="com.neomind.fusion.entity.dynamic.SequenceFactory"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.neomind.fusion.entity.dateevent.DateEventEntity"%>
<%@page import="com.neomind.fusion.doc.NeoDocument"%>
<%@page import="java.util.List"%>
<%@page import="com.neomind.fusion.persist.PersistEngine"%>
<%@page import="org.json.JSONObject"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%! 

private EntityInfo getFirstResult(final EntityInfo info, final long id)
{
	Long l = null;
	Long lChild = null;
	
	EntityInfo r = null;
	EntityInfo rChild = null;

	String type = ((InstantiableEntityInfo)info).getEntityClassName();
	try{		
			l = (Long) PersistEngine.getEntityManager().createQuery("select x.neoId from " + type + " x where x.neoId = " + id).getSingleResult();
			if(l != null && l > 0 )
			{
				r = info;
				if(!info.getSubClassSet().isEmpty())
				{
					for(EntityInfo child : info.getSubClassSet())
					{
						rChild = getFirstResult(child, id);
						if(rChild != null)
						{
							r = rChild;
							break;
						}
					}
				}
			}
	}catch(javax.persistence.NoResultException ex){}
	
	return r;
}

%>
<%
String where = "where neoId in (134733222,225431636,238200742,294438370,54488528,317205575,7362,295904824,119930041,119930079,210293982,221319559,221322991,1326115)";
List<Object> ids = PersistEngine.getEntityManager().createQuery("select neoId from NeoObject " + where).getResultList();
List<Object> types = PersistEngine.getEntityManager().createQuery("select typeName from EntityInfo").getResultList();

EntityInfo info = null;
EntityInfo target = null;
String typeName = null;
NeoType t = null;

for(Object id : ids)
{
	for(Object type : types)
	{
		
		info = EntityRegister.getEntityInfo((String)type);
		
		if(info == null || info.getTypeName().equalsIgnoreCase("NeoObject") || !(info instanceof InstantiableEntityInfo))
		{
			continue;
		}

		target = getFirstResult(info, (Long)id);
		if(target != null)
		{		
			typeName = NeoObject.encodeNeoType(((com.neomind.fusion.entity.InstantiableEntityInfo)target).getEntityClassName());
			t = PersistenceReloader.getInstance().getNeoType(typeName);
			out.print("update NeoObject set neoObjectType_id = " + t.getId() + " where neoId = " + id + "; --" + typeName);
			out.print("<br/>");
		}
	}
}

out.print("<br/>ok!");
%>

</body>
</html>