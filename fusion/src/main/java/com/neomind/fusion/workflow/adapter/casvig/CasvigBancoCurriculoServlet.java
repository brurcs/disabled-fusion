package com.neomind.fusion.workflow.adapter.casvig;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

@WebServlet(name="CasvigBancoCurriculoServlet", urlPatterns={"/servlet/com.neomind.fusion.workflow.adapter.casvig.CasvigBancoCurriculoServlet"})
public class CasvigBancoCurriculoServlet extends HttpServlet
{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	@SuppressWarnings("unchecked")
	private void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		final PrintWriter out = response.getWriter();
		
		response.setContentType("text/plain");

		try
		{
			String action = "";
		
			if (request.getParameter("action") != null)
			{
				action = request.getParameter("action");
			}

			if (action != null)
			{
				if (action.equalsIgnoreCase("getSubAreaInteresse"))
				{
					String areaPai = request.getParameter("areaPai");

					QLEqualsFilter filter = new QLEqualsFilter("areaPai.descricao", areaPai);

					Class<? extends NeoObject> clazz = AdapterUtils.getEntityClass("CVSubAreaInteresse");
					Collection<NeoObject> subAreaList = (Collection<NeoObject>) PersistEngine.getObjects(clazz, filter);
					
					if(subAreaList != null && !subAreaList.isEmpty())
					{
						StringBuilder options = new StringBuilder();
						options.append("<option value=''></option>");

						for (NeoObject neoObject : subAreaList)
						{
							EntityWrapper wrapper = new EntityWrapper(neoObject);
							
							options.append("<option value='" + neoObject.getNeoId() + "'>" + NeoUtils.encodeHTMLValue(wrapper.findValue("descricao")) + "</option>");
						}

						out.println(options.toString());
					}
					else
					{
						out.print("");
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			out.print("");
		}
		finally
		{
			out.close();
		}
	}
}
