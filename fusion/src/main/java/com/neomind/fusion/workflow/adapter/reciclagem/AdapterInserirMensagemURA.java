package com.neomind.fusion.workflow.adapter.reciclagem;

import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.UraPresencaUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

//com.neomind.fusion.workflow.adapter.reciclagem.AdapterInserirMensagemURA
public class AdapterInserirMensagemURA implements AdapterInterface
{

	private static final Log log = LogFactory.getLog(AdapterInserirMensagemURA.class);

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{

		Long numcpf = NeoUtils.safeLong(NeoUtils.safeOutputString(processEntity.findValue("numcpf")));
		try
		{
			GregorianCalendar datIni = new GregorianCalendar();
			GregorianCalendar datFim = (GregorianCalendar) processEntity.findValue("dataCurso");
			GregorianCalendar datFimCur = (GregorianCalendar) processEntity.findValue("dataFimCurso");
			
			String mensagem = datFim.getTimeInMillis() / 1000 + "_" + datFimCur.getTimeInMillis() / 1000;
			Boolean gravar = false;
			if (datIni.before(datFim))
			{
				gravar = UraPresencaUtils.gravarMensagem(numcpf, 0, 1, mensagem, datIni, datFim, 0);
			}
			if (gravar)
			{
				System.out.println("Mensagem gravada com sucesso! CPF: " + numcpf);
			}else{
				System.out.println("Mensagem não gravada para o CPF: " + numcpf);
			}

		}
		catch (Exception e)
		{
			log.error("Erro ao cadastrar mensagem de reciclagem para a URA");
			log.info("Erro ao cadastrar mensagem de reciclagem para a URA");
			e.printStackTrace();
			throw new WorkflowException("Erro ao cadastrar mensagem de reciclagem para a URA para o CPF:" + numcpf);
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}

}
