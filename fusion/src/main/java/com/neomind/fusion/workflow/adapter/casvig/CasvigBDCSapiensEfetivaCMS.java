package com.neomind.fusion.workflow.adapter.casvig;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class CasvigBDCSapiensEfetivaCMS implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(CasvigBDCSapiensEfetivaCMS.class);
	
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		Collection<NeoObject> col = processEntity.findField("movimentos").getValues();
		
		String codigoTarefa = processEntity.findGenericValue("wfprocess.code");
		String resulting = "Inicia Conexão com o Sapiens, classe CasvigBDCSapiensEfetivaCMS. Tarefa " + codigoTarefa;
		
		Connection conn = null;
		PreparedStatement st = null;
		try
		{
			conn = PersistEngine.getConnection("SAPIENS");
			for (NeoObject no : col)
			{
				EntityWrapper wrapperLista = new EntityWrapper(no);
				Long codEmp = (Long) wrapperLista.findField("usu_codemp").getValue();
				Long codFil = (Long) wrapperLista.findField("usu_codfil").getValue();
				Long numCtr = (Long) wrapperLista.findField("usu_numctr").getValue();
				Long numPos = (Long) wrapperLista.findField("usu_numpos").getValue();
				Long seqMov = (Long) wrapperLista.findField("usu_seqmov").getValue();
				Boolean aprovado = (Boolean) processEntity.findField("aprovacaoCMS").getValue();
				
				StringBuffer sql = new StringBuffer();
				sql.append("UPDATE USU_T160CMS ");
				sql.append("SET usu_libapo = ? ");
				sql.append("WHERE usu_codemp = ? AND usu_codfil = ? AND usu_numctr = ? AND usu_numpos = ? AND usu_seqmov = ? ");
				
				
				st = conn.prepareStatement(sql.toString());
				if (aprovado.booleanValue())
					st.setString(1, "S");
				else
				{
					st.setString(1, "N");
				}
				st.setLong(2, codEmp.longValue());
				st.setLong(3, codFil.longValue());
				st.setLong(4, numCtr.longValue());
				st.setLong(5, numPos.longValue());
				st.setLong(6, seqMov.longValue());

				log.warn(sql.toString() + "classe CasvigBDCSapiensEfetivaCMS. Tarefa " + codigoTarefa);
				System.out.println(sql.toString() + "classe CasvigBDCSapiensEfetivaCMS. Tarefa " + codigoTarefa);
				
				int lines = st.executeUpdate();
				PersistEngine.commit(true); 

				String result = lines+" linhas atualizadas com a query: 'UPDATE USU_T160CMS SET usu_libapo = "+((aprovado)?"S":"N")+" " +
						"WHERE usu_codemp = "+codEmp+" AND usu_codfil = "+codFil+" AND usu_numctr = "+numCtr+" AND usu_numpos = "+numPos+" AND usu_seqmov = "+seqMov+"' para o processo: '"+activity.getProcess().toString()+"'.";
				
				System.out.println(result + " Tarefa " + codigoTarefa);
				log.warn(result + " Tarefa " + codigoTarefa);
				
			}
			
			if (processEntity.findField("baixaPorPerda").getValue() != null && (boolean) processEntity.findField("baixaPorPerda").getValue()) {
			
				String empresa = processEntity.findField("empresa").getValueAsString();
				String filial = processEntity.findField("filial").getValueAsString();
				String numeroContrato = "";
				String numnfv = "";
				double vlrbse = 0;
				String numdfs = "";
				String nomeCliente = "";
				String cnpjCliente = "";
				
				if (processEntity.findValue("nfSapiens") != null) {
					
					numeroContrato = NeoUtils.safeOutputString(processEntity.findValue("nfSapiens.numctr"));
					numnfv = NeoUtils.safeOutputString(processEntity.findValue("nfSapiens.numnfv"));
					vlrbse = Double.parseDouble(NeoUtils.safeOutputString(processEntity.findValue("nfSapiens.vlrbse")));
					numdfs = NeoUtils.safeOutputString(processEntity.findValue("nfSapiens.numdfs"));
					nomeCliente = NeoUtils.safeOutputString(processEntity.findValue("nfSapiens.nomcli"));
					cnpjCliente = NeoUtils.safeOutputString(processEntity.findValue("nfSapiens.cgccpf"));
					
				} else if (processEntity.findValue("contrato") != null){
					
					numeroContrato = NeoUtils.safeOutputString(processEntity.findValue("contrato.usu_numctr"));
					numnfv = " - ";
					nomeCliente = NeoUtils.safeOutputString(processEntity.findValue("contrato.nomcli"));
					cnpjCliente = NeoUtils.safeOutputString(processEntity.findValue("contrato.cgccpf"));
					
				}
				
				
				double custoBaixaPorPerda = Double.parseDouble(processEntity.findField("custoBaixaPorPerda").getValueAsString());
				
				log.warn(resulting);
				
				String titulo = "Justificar Baixa por Perda (C023) - Tarefa "+codigoTarefa;
				
				DecimalFormat df = new DecimalFormat(",##0.00");
		       
				String descricaoTarefaBaixaPorPerda = "Justificar Baixa por Perda (C023) - Tarefa "+codigoTarefa+"</br>Contrato: "+numeroContrato+"</br>Empresa/Filial: "+empresa+"/"+filial
						+ "</br>Cliente: "+cnpjCliente+" - "+nomeCliente
						+ "</br>Custo Baixa por Perda: R$ "+ df.format(custoBaixaPorPerda)
						+ "</br>RPS: "+numnfv
						+ "</br>Valor Bruto: R$ "+df.format(vlrbse)
						+ "</br>NF Eletrônica: "+numdfs;
				
				abrirTarefaSimples(processEntity,titulo,descricaoTarefaBaixaPorPerda);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Não foi possível efetivar aprovação no Sapiens. Erro no Adapter, entre em contato com a TI.");
		}
		finally
		{
			try
			{
				log.warn("Fechando conexão...");
				conn.close();
				log.warn(conn.isClosed());
				
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public static void abrirTarefaSimples(EntityWrapper w, String titulo, String descricao){
		
		NeoUser usuarioResponsavel = (NeoUser) w.findField("gestorResponsavelBaixaPorPerda").getValue();
		NeoUser usuarioSolicitante = PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "dilmoberger") );
		
		GregorianCalendar prazo = new GregorianCalendar();
		prazo.add(GregorianCalendar.DAY_OF_MONTH, 2);
		prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
		prazo.set(GregorianCalendar.MINUTE, 23);
		prazo.set(GregorianCalendar.SECOND, 59);
		
		
		
		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		String tarefa = iniciarTarefaSimples.abrirTarefa(usuarioSolicitante.getCode(), usuarioResponsavel.getCode(), titulo, descricao, "1", "sim", OrsegupsUtils.getNextWorkDay(prazo));
		
		
		QLFilter filter = new QLEqualsFilter("wfprocess.code", tarefa);
		NeoObject objTarefaSimples = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("Tarefa"), filter);
		PersistEngine.persist(objTarefaSimples);
		
		w.findField("tarefaSimplesPerdaPorBaixa").setValue(objTarefaSimples);
				
		System.out.println(tarefa);
	}
}