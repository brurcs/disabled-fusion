package com.neomind.fusion.workflow.adapter.casvig;

import java.util.GregorianCalendar;
import java.util.Set;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.ActivityException;
import com.neomind.fusion.workflow.exception.TaskException;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class CasvigPrazoTarefaSimples implements AdapterInterface
{
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	public void start(Task origin, EntityWrapper processEntity, Activity activity) throws ActivityException, TaskException
	{

		try
		{

			String atividadeAnterior = origin != null ? origin.getInstance().getActivity().getActivityName() : "";

			Boolean diretorReenviou = processEntity.findGenericValue("diretorReenviou");

			NeoUser userExec = (NeoUser) (origin != null ? origin.getInstance().getOwner() : processEntity.findGenericValue("Solicitante"));

			Boolean eDiretor = Boolean.FALSE;

			Set<NeoPaper> papeisUserExec = userExec.getPapers();

			for (NeoPaper papelUserExec : papeisUserExec)
			{

				String paperName = papelUserExec.getName();

				if (paperName.contains("Diretor "))
					eDiretor = Boolean.TRUE;

			}

			if (atividadeAnterior.contains("Realizar tarefa - Escalada") && eDiretor && !atividadeAnterior.contains("Último Nível"))
			{

				if (diretorReenviou != null && diretorReenviou)
				{

					throw new WorkflowException("NÃO É POSSÍVEL REENVIAR TAREFA POIS JÁ EXISTEM REGISTROS DE REENVIO POR DIRETOR!");

				}
				else
				{

					GregorianCalendar dataAtual = new GregorianCalendar(new GregorianCalendar().get(1), new GregorianCalendar().get(2), new GregorianCalendar().get(5), 23, 59, 59);
					GregorianCalendar prazo = (GregorianCalendar) processEntity.findField("Prazo").getValue();

					if (!OrsegupsUtils.isWorkDay(prazo))
					{
						throw new WorkflowException("Prazo informado deve ter um dia útil!");
					}

					if ((prazo.before(dataAtual)) || (prazo.equals(dataAtual)))
					{
						throw new WorkflowException("Prazo informado deve ser de pelo menos 1 dia!");
					}

					processEntity.findField("diretorReenviou").setValue(Boolean.TRUE);

				}

			}
			else
			{

				GregorianCalendar dataAtual = new GregorianCalendar(new GregorianCalendar().get(1), new GregorianCalendar().get(2), new GregorianCalendar().get(5), 23, 59, 59);
				GregorianCalendar prazo = (GregorianCalendar) processEntity.findField("Prazo").getValue();

				if (!OrsegupsUtils.isWorkDay(prazo))
				{
					throw new WorkflowException("Prazo informado deve ter um dia útil!");
				}

				if ((prazo.before(dataAtual)) || (prazo.equals(dataAtual)))
				{
					throw new WorkflowException("Prazo informado deve ser de pelo menos 1 dia!");
				}

			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw (WorkflowException) e;
		}
	}

}