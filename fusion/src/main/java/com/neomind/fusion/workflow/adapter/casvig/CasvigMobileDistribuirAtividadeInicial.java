package com.neomind.fusion.workflow.adapter.casvig;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

/**
 * 
 * @author Daniel Henrique Joppi
 * 
 */
public class CasvigMobileDistribuirAtividadeInicial implements AdapterInterface
{
	@SuppressWarnings("unused")
	private static final Log log = LogFactory.getLog(CasvigMobileDistribuirAtividadeInicial.class);

	@Override
	public void start(Task origin, EntityWrapper processEntity,	Activity activity) 
	{
        if(processEntity != null)
        {
        	processEntity.setValue("codigoProcesso", activity.getProcess().getCode());
        }
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
		
	}
}