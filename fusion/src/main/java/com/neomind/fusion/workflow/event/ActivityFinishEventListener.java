package com.neomind.fusion.workflow.event;

import com.neomind.fusion.workflow.exception.ActivityException;

public interface ActivityFinishEventListener extends WorkflowEventListener
{
	public void onFinish(ActivityEvent event) throws ActivityException;
}
