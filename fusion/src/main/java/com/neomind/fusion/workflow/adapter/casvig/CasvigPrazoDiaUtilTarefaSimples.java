package com.neomind.fusion.workflow.adapter.casvig;

import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class CasvigPrazoDiaUtilTarefaSimples implements AdapterInterface
{
       public void back(EntityWrapper processEntity, Activity activity)
       {
       }
       public void start(Task origin, EntityWrapper processEntity, Activity activity)
       {
             GregorianCalendar dataAtual = new GregorianCalendar(new GregorianCalendar().get(1), new GregorianCalendar().get(2), new GregorianCalendar().get(5), 23, 59, 59);
             GregorianCalendar prazo = (GregorianCalendar) processEntity.findField("Prazo").getValue();

             if (!OrsegupsUtils.isWorkDay(prazo))
             {
                    throw new WorkflowException("Prazo informado deve ter um dia útil!");
             }
             

       }
}
