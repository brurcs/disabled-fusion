package com.neomind.fusion.workflow.adapter.CNV;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.fusion.workflow.simulation.WorkflowService;
import com.neomind.util.NeoCalendarUtils;

public class IniciaFluxoEmissaoRenovacaoCNV implements CustomJobAdapter

{

	@Override
	public void execute(CustomJobContext ctx)
	{
		if (OrsegupsUtils.isWorkDay(new GregorianCalendar()))
		{
			System.out.println("###[Q022] - Inicia fluxo de Emissao/Renovacao CNV");
			processaJob(ctx);
		}
	}

	public static void processaJob(CustomJobContext ctx)
	{
		Connection conn = PersistEngine.getConnection("");
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;

		try
		{
			sql.append(" SELECT FUN.NUMEMP,FUN.NUMCAD,FUN.NOMFUN,FUN.CODCCU,FUN.NUMCPF,REG.USU_CODREG,REG.USU_NOMREG,isnull(CUR.USU_DATVALCNV,'1900-12-31') USU_DATVALCNV ,CUR.USU_NUCANAVI,FUN.DATADM     ");
			sql.append(" FROM [FSOODB04\\SQL02].VETORH.DBO.USU_TCADCUR CUR    WITH (NOLOCK)                                                                                                      ");
			sql.append("     INNER JOIN [FSOODB04\\SQL02].VETORH.DBO.R038HLO HLO WITH (NOLOCK) ON HLO.NUMEMP = CUR.USU_NUMEMP AND                                                                   ");
			sql.append("               HLO.TIPCOL = CUR.USU_TIPCOL AND                                                                                                 ");
			sql.append("               HLO.NUMCAD = CUR.USU_NUMCAD AND                                                                                                 ");
			sql.append("               HLO.DATALT = (SELECT MAX (DATALT) FROM [FSOODB04\\SQL02].VETORH.DBO.R038HLO TABELA001 WITH (NOLOCK) WHERE TABELA001.NUMEMP = HLO.NUMEMP AND   ");
			sql.append("                                 TABELA001.TIPCOL = HLO.TIPCOL AND                                ");
			sql.append("                                 TABELA001.NUMCAD = HLO.NUMCAD AND                                ");
			sql.append("                              TABELA001.DATALT <= DATEADD(DD, 0, DATEDIFF(DD, 0, GETDATE()+15)))  ");
			sql.append("     INNER JOIN [FSOODB04\\SQL02].VETORH.DBO.R016ORN ORN WITH (NOLOCK) ON ORN.NUMLOC = HLO.NUMLOC AND ORN.TABORG = HLO.TABORG                                                             ");
			sql.append("     INNER JOIN [FSOODB04\\SQL02].VETORH.DBO.R034FUN FUN WITH (NOLOCK)ON FUN.NUMCAD = CUR.USU_NUMCAD AND FUN.NUMEMP = CUR.USU_NUMEMP AND FUN.TIPCOL = CUR.USU_TIPCOL                     ");
			sql.append("     INNER JOIN [FSOODB04\\SQL02].VETORH.DBO.R038HCA HCA WITH (NOLOCK) ON HCA.CODCAR = FUN.CODCAR AND                                                                            ");
			sql.append("                HCA.NUMCAD = FUN.NUMCAD AND                                                                                                          ");
			sql.append("                HCA.TIPCOL = FUN.TIPCOL AND                                                                                                          ");
			sql.append("                HCA.NUMEMP = FUN.NUMEMP AND                                                                                                          ");
			sql.append("                HCA.DATALT = (SELECT MAX(DATALT) FROM [FSOODB04\\SQL02].VETORH.DBO.R038HCA HCA2 WITH (NOLOCK) WHERE HCA2.NUMCAD = HCA.NUMCAD AND                   ");
			sql.append("                               HCA2.NUMEMP = HCA.NUMEMP AND                                                ");
			sql.append("                            HCA2.TIPCOL = HCA.TIPCOL AND                                                ");
			sql.append("                            HCA2.DATALT <= DATEADD(DD, 0, DATEDIFF(DD, 0, GETDATE()+15)))               ");
			sql.append("     INNER JOIN [FSOODB04\\SQL02].VETORH.DBO.R024CAR CAR WITH (NOLOCK) ON CAR.CODCAR = HCA.CODCAR AND CAR.ESTCAR = 1                                                             ");
			sql.append("     INNER JOIN D_REGIONAISCNV DR WITH (NOLOCK) ON DR.CODREG = ORN.USU_CODREG AND DR.HABILITAR = 1                                                                                        ");
			sql.append("     INNER JOIN [FSOODB04\\SQL02].VETORH.DBO.USU_T200REG REG WITH (NOLOCK) ON REG.USU_CODREG = ORN.USU_CODREG                                                                             ");
			sql.append("  WHERE FUN.SITAFA in(1,2) AND CAR.TITRED LIKE 'VIGIL%' AND FUN.NUMEMP NOT IN (22,28,29) AND                               ");
			sql.append(" (USU_DATVALCNV = '1900-12-31 00:00:00.000' OR USU_DATVALCNV IS NULL OR USU_DATVALCNV <= DATEADD(DD, 0, DATEDIFF(DD, 0, GETDATE()+15)))                                        ");
			sql.append("  AND NOT EXISTS (                                         ");
			sql.append("       SELECT 1 FROM D_CNVTAREFAS CNV                                  ");
			sql.append("     INNER JOIN WFProcess W ON W.code = CNV.code                              ");
			sql.append("     INNER JOIN ProcessModel PM ON PM.neoId = W.model_neoId                           ");
			sql.append("     WHERE CNV.NUMCAD = FUN.NUMCAD AND CNV.NUMEMP = FUN.NUMEMP and PM.name = 'Q022 - Emissão/Renovação CNV' AND processState = 0 AND saved = 1           ");
			sql.append("     )                                         ");
			sql.append(" AND NOT EXISTS (select 1 from  [CACUPE\\SQL02].fusion_producao.DBO.WFProcess w WITH (NOLOCK) "
					+ " inner join [CACUPE\\SQL02].fusion_producao.DBO.D_Tarefa d WITH (NOLOCK) on d.wfprocess_neoId = w.neoId "
					+ " where d.titulo = 'Tarefa escalada do fluxo Q022 - Emissão/Renovação CNV com código% colaborador '+cast(FUN.numemp as varchar)+ '/' + cast(FUN.numCad as varchar) + ' - %'+FUN.nomfun"
					+ " and w.processState = 0)");

			sql.append(" ORDER BY 5,1,2,3                                        ");

			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();

			while (rs.next())
			{
				InstantiableEntityInfo cnvPrincipal = AdapterUtils.getInstantiableEntityInfo("CNVPrincipal");
				NeoObject objCnvPrincipal = cnvPrincipal.createNewInstance();
				EntityWrapper wCnvPrincipal = new EntityWrapper(objCnvPrincipal);
				wCnvPrincipal.findField("numEmp").setValue(rs.getLong("NUMEMP"));
				wCnvPrincipal.findField("numCad").setValue(rs.getLong("NUMCAD"));
				wCnvPrincipal.findField("nomFun").setValue(rs.getString("NOMFUN"));
				wCnvPrincipal.findField("numCpf").setValue(rs.getString("NUMCPF"));
				wCnvPrincipal.findField("codReg").setValue(rs.getLong("USU_CODREG"));
				wCnvPrincipal.findField("nomReg").setValue(rs.getString("USU_NOMREG"));
				wCnvPrincipal.findField("codCcu").setValue(rs.getString("CODCCU"));
				
				GregorianCalendar datAdm = new GregorianCalendar();
				datAdm.set(GregorianCalendar.HOUR, 0);
				datAdm.set(GregorianCalendar.MINUTE, 0);
				datAdm.set(GregorianCalendar.SECOND, 0);
				datAdm.setTime(rs.getDate("datAdm"));
				wCnvPrincipal.findField("datAdm").setValue(datAdm);
				
				GregorianCalendar datValCNV = new GregorianCalendar();
				datValCNV.set(GregorianCalendar.HOUR, 0);
				datValCNV.set(GregorianCalendar.MINUTE, 0);
				datValCNV.set(GregorianCalendar.SECOND, 0);
				GregorianCalendar dat1900 = NeoCalendarUtils.stringToDate("31/12/1900");
				dat1900.set(GregorianCalendar.HOUR, 0);
				dat1900.set(GregorianCalendar.MINUTE, 0);
				dat1900.set(GregorianCalendar.SECOND, 0);
				datValCNV.setTime(rs.getDate("USU_DATVALCNV"));
				wCnvPrincipal.findField("dataVenCnv").setValue(datValCNV);
				//Se não há uma data informando se o mesmo possui uma CNV, direcionar para consulta.
				if (datValCNV == null || datValCNV.equals(dat1900))
				{
					wCnvPrincipal.findField("acao").setValue("Consultar");
					wCnvPrincipal.findField("dataVenCnv").setValue(null);
				}
				else
				{
					wCnvPrincipal.findField("acao").setValue("Solicitar");
				}
				//Define o executor da tarefa pelo papel CNVResponsavelQualidade
				NeoPaper papel = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "CNVResponsavelQualidade"));
				NeoUser neoExecutor = null;
				if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
				{
					for (NeoUser user : papel.getUsers())
					{
						neoExecutor = user;
						break;
					}
				}
				//Solicitando
				NeoUser solicitante = neoExecutor;
				//Iniciar o fluxo
				QLEqualsFilter equal = new QLEqualsFilter("Name", "Q022 - Emissão/Renovação CNV");
				ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);
				InstantiableEntityInfo tarefaCnv = AdapterUtils.getInstantiableEntityInfo("CNVTarefas");
				//Preencho o campo respReg com o reposanvel da regional do e-form regionaisCNV
				//final WFProcess processo = WorkflowService.startProcess(processModel, objCnvPrincipal, false, solicitante);
				
				String tarefa = OrsegupsWorkflowHelper.iniciaProcesso(processModel, objCnvPrincipal, false, solicitante, false, null);
				
				
				NeoObject noAJ = tarefaCnv.createNewInstance();
				EntityWrapper ajWrapper = new EntityWrapper(noAJ);
				ajWrapper.findField("numEmp").setValue(rs.getLong("NUMEMP"));
				ajWrapper.findField("numCad").setValue(rs.getLong("NUMCAD"));
				ajWrapper.findField("datValCnv").setValue(datValCNV);
				ajWrapper.findField("executor").setValue(neoExecutor.getCode());
				ajWrapper.findField("code").setValue(tarefa);
				PersistEngine.persist(noAJ);
			}
		}
		catch (Exception e)
		{
			System.out.println("###[FLUXO Q022]Erro ao tentar iniciar o JOB do processo Q022 - Emissão/Renovação CNV ");
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
			System.out.println("###[FLUXO Q022] - Fluxo de Emissao/Renovacao CNV finalizado com Sucesso!");
		}
	}

}
