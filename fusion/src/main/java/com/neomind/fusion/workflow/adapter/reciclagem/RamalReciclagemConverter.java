package com.neomind.fusion.workflow.adapter.reciclagem;

import java.util.StringTokenizer;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.util.NeoUtils;
//com.neomind.fusion.workflow.adapter.reciclagem.RamalReciclagemConverter
public class RamalReciclagemConverter extends StringConverter {

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin) {
				
		String result = "";
		String ramalDestino = "";
		String ramalOrigem = "";
		String link = "";

		//pega ramal do usuário logado no sistema
		EntityWrapper usuarioOrigemWrapper = new EntityWrapper(PortalUtil.getCurrentUser());
		ramalOrigem = (String) usuarioOrigemWrapper.findValue("ramal");
		
		
		String numero = "";
		int cont = 0;
		//field.getValue() pega o valor já preenchido no campo
		ramalDestino = NeoUtils.safeOutputString(field.getValue());
		ramalDestino = ramalDestino.replaceAll("\\(|\\)|\\-|\\.|\\ ", "");
		
		StringTokenizer st = new StringTokenizer(ramalDestino, "/");
		while(st.hasMoreTokens()){
			numero = st.nextToken();
			
			if(numero.length() == 10 && !numero.startsWith("0")){
				numero = "0" + numero;
			}
			
			if (numero.length() == 11) {
				link = link + OrsegupsUtils.getCallLink(ramalOrigem, numero, true);
				if (cont < st.countTokens()){
					link = link + " / ";
				}
			}
			cont++;	
		}
		
		if (NeoUtils.safeIsNotNull(link)) {
			result = link;
		} else {
			result = super.getHTMLView(field, origin);
		}
		
		return result;
	}	
}
