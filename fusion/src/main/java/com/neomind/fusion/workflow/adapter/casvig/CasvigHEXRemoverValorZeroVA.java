package com.neomind.fusion.workflow.adapter.casvig;

import java.util.ArrayList;
import java.util.Collection;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.dynamic.EntityCloner;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class CasvigHEXRemoverValorZeroVA implements AdapterInterface
{
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		boolean impVa = true;
		try
		{
			Collection<NeoObject> va = processEntity.findField("listaRegistro").getValues();
			ArrayList<NeoObject> novoVA = new ArrayList<NeoObject>();

			for (NeoObject no : va)
			{
				NeoObject novoObject = EntityCloner.cloneNeoObject(no);
				EntityWrapper wrapperLista = new EntityWrapper(novoObject);
				Long numCad = (Long) wrapperLista.findField("colaborador.numcad").getValue();
				Long vaExt = (Long) wrapperLista.findField("vaExtra").getValue();

				System.out.println("numCad=" + numCad + " vaExt=" + vaExt);

				if (vaExt.longValue() <= 0L)
					continue;
				PersistEngine.persist(novoObject);
				novoVA.add(novoObject);
			}

			if ((novoVA == null) || (novoVA.size() == 0))
			{
				impVa = false;
			}

			processEntity.setValue("impVa", Boolean.valueOf(impVa));
			processEntity.setValue("listaRegistroC", novoVA);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}