package com.neomind.fusion.workflow.adapter.reciclagem;

import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;

public class AdapterDefinirResponsavelCM implements AdapterInterface
{
    private static final Log log = LogFactory.getLog(AdapterDefinirResponsavelCM.class);
    @Override
    public void start(Task origin, EntityWrapper processEntity, Activity activity){
	try{
	    NeoPaper responsavel = null;
	    responsavel = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "rclResponsavelCM"));
	    
	    processEntity.setValue("papelRespCM", responsavel);
	}catch(Exception e){
	    e.printStackTrace();
	    log.error("##### ERRO AO indexar documento: " + e + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
	    throw new WorkflowException(e.getMessage());
	}
    }

    @Override
    public void back(EntityWrapper processEntity, Activity activity){
	
    }

}
