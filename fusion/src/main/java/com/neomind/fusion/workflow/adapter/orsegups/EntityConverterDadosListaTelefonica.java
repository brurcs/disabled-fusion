package com.neomind.fusion.workflow.adapter.orsegups;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.EntityConverter;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.entity.form.FormUtils;
import com.neomind.util.NeoUtils;

public class EntityConverterDadosListaTelefonica extends EntityConverter
{

	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin)
	{
			// TODO Auto-generated method stub
			String neoIdObj = NeoUtils.safeOutputString(field.getForm().getObjectId());
			String fieldPath = field.getForm().getFieldPath();
			String fieldName = field.getFieldCode();
			String fieldPathComplete = fieldPath + fieldName + FormUtils.HTML_FIELD_SEPARATOR;
			String textId = "id_txt_" + NeoUtils.safeJSName(fieldPathComplete);
			String id = "id_" + NeoUtils.safeJSName(fieldPathComplete);
			return super.getHTMLInput(field, origin ) + OrsegupsUtils.rodaVm("custom/orsegups/searchDadosListaTelefonica.vm", neoIdObj, textId, id);
	}
}