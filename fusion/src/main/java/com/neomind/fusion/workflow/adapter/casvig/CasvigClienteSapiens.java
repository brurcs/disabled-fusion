package com.neomind.fusion.workflow.adapter.casvig;

import javax.persistence.Entity;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

/**
 * 
 * @author Nilcimar Neitzel
 * 
 */

public class CasvigClienteSapiens implements AdapterInterface
{ 

	@SuppressWarnings("unused")
	private static final Log log = LogFactory.getLog(CasvigClienteSapiens.class);
	
	/**
	 * 
	 * Copia NomCli do e-form externo clienteSapiens para o campo
	 * cliente do e-form RRCRelatorioReclamacaoCliente,
	 * logo após a abertura do processo de reclamação.
	 * 
	 */
	
	@Override
	public void start(Task origin, EntityWrapper wrapperRRC, Activity activity)
	{
		wrapperRRC.setValue("cliente", wrapperRRC.findValue("clienteSapiens.nomcli"));
	}
	
	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}
}