package com.neomind.fusion.workflow.adapter.reciclagem;

import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class RCLRegistroAtividade implements AdapterInterface {

    @Override
    public void start(Task origin, EntityWrapper processEntity, Activity activity) {
	try {
	    
	
	    origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);

	    // String erro = "Por favor, contatar o administrador do sistema!";

	    boolean prazoVerificar = false;

	    InstantiableEntityInfo registroAtividade = AdapterUtils.getInstantiableEntityInfo("rclRegistroAtividade");
	    NeoObject registro = registroAtividade.createNewInstance();
	    EntityWrapper wRegistro = new EntityWrapper(registro);

	    if (NeoUtils.safeIsNull(origin)){
		NeoPaper papel = new NeoPaper();
		papel = OrsegupsUtils.getPaper("sistemaFusionPaper");
		NeoUser usuarioResponsavel = new NeoUser();
		if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
		{
		    for (NeoUser user : papel.getUsers())
		    {
			usuarioResponsavel = user;
			break;
		    }
		}
		wRegistro.findField("responsavel").setValue(usuarioResponsavel);
		
		wRegistro.findField("dataInicial").setValue(new GregorianCalendar());
		
		wRegistro.findField("dataFinal").setValue(new GregorianCalendar());
	    }else{
		wRegistro.findField("responsavel").setValue(origin.getUser());
		
		wRegistro.findField("dataInicial").setValue(origin.getStartDate());
		
		wRegistro.findField("dataFinal").setValue(origin.getFinishDate());
	    }

	    if (NeoUtils.safeIsNull(origin)) {
		wRegistro.findField("descricao").setValue("Anexar documento");
		wRegistro.findField("atividade").setValue("Tarefa avançada em pool");
	    } else if (origin.getActivityName().equalsIgnoreCase("Convocar Vigilante Reciclagem")) {
		wRegistro.findField("atividade").setValue("Avançou tarefa");	 
		String nome = (String) processEntity.findValue("nomfun");
		Boolean isConvocou = (Boolean) processEntity.findValue("convocou");
		String obsConvocacao = (String) processEntity.findValue("obs");
		if(isConvocou){
		    wRegistro.findField("descricao").setValue("Vigilante "+nome+" foi convocado para reciclagem");
		}else{
		    wRegistro.findField("descricao").setValue("Vigilante "+nome+" não foi convocado para reciclagem. Obs.: "+obsConvocacao);
		}
	    } else if (origin.getActivityName().equalsIgnoreCase("Pool iniciar tarefa simples cobertura")) {
		wRegistro.findField("atividade").setValue("Avançou tarefa");	 
		wRegistro.findField("descricao").setValue("Iniciar tarefa simples cobertura");
	    } else if (origin.getActivityName().equalsIgnoreCase("Pool aguardar 5 dias após data do curso")) {
		wRegistro.findField("atividade").setValue("Avançou tarefa");	 
		wRegistro.findField("descricao").setValue("Aguardar 5 dias após data do curso");
	    } else if (origin.getActivityName().equalsIgnoreCase("Tomar Ciência - Curso de Reciclagem")) {
		wRegistro.findField("atividade").setValue("Avançou tarefa");	 
		wRegistro.findField("descricao").setValue("Curso de Reciclagem realizado!");
	    } else if (origin.getActivityName().equalsIgnoreCase("Confirmar realização do curso")) {
		if (processEntity.findValue("prazoAnexarDocumentos") != null){
		    wRegistro.findField("atividade").setValue("Ajustou tarefa");	 
		}else{
		    wRegistro.findField("atividade").setValue("Avançou tarefa");	 
		}
		wRegistro.findField("descricao").setValue("Realização do curso confirmado");
	    } else if (origin.getActivityName().equalsIgnoreCase("Pool aguardar 30 dias anexar documentos")) {
		wRegistro.findField("atividade").setValue("Avançou tarefa");	 
		wRegistro.findField("descricao").setValue("Anexar documento");
	    } else if (origin.getActivityName().equalsIgnoreCase("Anexar Certificado e Comprovante de Entrega")) {
		if (origin.getFinishByUser() == null){
		    wRegistro.findField("atividade").setValue("Tarefa escalada");	
		    wRegistro.findField("descricao").setValue("Tarefa não tratada.");
		}else{
		    NeoObject rAcao = (NeoObject) processEntity.findValue("acaoTarefa");
		    Long codigo = 0L;
		    if (NeoUtils.safeIsNotNull(rAcao))
		    {
			EntityWrapper wAcao = new EntityWrapper(rAcao);
			codigo = (Long) wAcao.findValue("codigo");
		    }
		    if (codigo.equals(2L))
		    {
			wRegistro.findField("atividade").setValue("Solicitou ajuste");
			String obs = (String) processEntity.findValue("obsAjuste");
			wRegistro.findField("descricao").setValue(obs);
		    }
		    else if (codigo.equals(1L))
		    {
			wRegistro.findField("atividade").setValue("Realizou tarefa");
			if (wRegistro.findField("vigilanteAprovado") != null && (Boolean)wRegistro.findField("vigilanteAprovado").getValue()){
			    wRegistro.findField("descricao").setValue("Certificado e comprovante anexados");
			}else{
			    wRegistro.findField("descricao").setValue("Demissional anexado");
			}
		    }
		    processEntity.setValue("obsAjuste", null);
		}
	    } else if (origin.getActivityName().equalsIgnoreCase("Validar Certificado e Comprovante de Entrega")) {
		wRegistro.findField("atividade").setValue("Avançou tarefa");	

		Boolean isDocValidos = (Boolean) processEntity.findValue("isDocumentosValidos");
		String motivoRecusa = (String) processEntity.findValue("motivoRecusa");
		if(isDocValidos){
		    wRegistro.findField("descricao").setValue("Documentos validados!");
		}else{
		    wRegistro.findField("descricao").setValue("Documentos recusados, obs.: "+motivoRecusa);
		    GregorianCalendar deadLineDtAud = new GregorianCalendar();
		    deadLineDtAud.add(GregorianCalendar.DATE, 2);
		    deadLineDtAud.add(GregorianCalendar.HOUR_OF_DAY, 23);
		    deadLineDtAud.add(GregorianCalendar.MINUTE, 59);
		    deadLineDtAud.add(GregorianCalendar.SECOND, 59);
		    processEntity.setValue("deadLineDtAnexarDoc", deadLineDtAud);
		}
	    }


	    processEntity.setValue("motivoRecusa", null);

	    PersistEngine.persist(registro);

	    processEntity.findField("registroAtividadesRCL").addValue(registro);
	}catch(Exception e){
	    e.printStackTrace();
	    throw new WorkflowException(e.getMessage());
	}

    }

    @Override
    public void back(EntityWrapper processEntity, Activity activity) {

    }

}
