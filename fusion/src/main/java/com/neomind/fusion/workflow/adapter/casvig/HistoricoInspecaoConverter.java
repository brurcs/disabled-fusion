package com.neomind.fusion.workflow.adapter.casvig;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class HistoricoInspecaoConverter extends StringConverter
{

	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin)
	{
		
		Long idPai = field.getForm().getObjectId();
		NeoObject tarefa = PersistEngine.getNeoObject(idPai);
		EntityWrapper wrapper = new EntityWrapper(tarefa);
		
		List<NeoObject> registroAtividades = (List<NeoObject>) wrapper.findValue("registroAtividades");
		StringBuilder textoTable = new StringBuilder();
		
		if (NeoUtils.safeIsNotNull(registroAtividades) && !registroAtividades.isEmpty())
		{
			textoTable.append("		<table  class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">");
			textoTable.append("			<tr style=\"cursor: auto\">");
			textoTable.append("				<th style=\"cursor: auto\">Justificativa</th>");
			textoTable.append("				<th style=\"cursor: auto\">Data da Ação</th>");
			textoTable.append("				<th style=\"cursor: auto\">Usuário</th>");
			textoTable.append("			</tr>");
			textoTable.append("			<tbody>");	
			
			for (NeoObject obj : registroAtividades)
			{
				EntityWrapper registroWrapper = new EntityWrapper(obj);
				String descricao = (String) registroWrapper.findValue("observacao");
				GregorianCalendar dataRegistro = (GregorianCalendar) registroWrapper.findValue("dataRegistro");
				String usuario = (String ) registroWrapper.findValue("usuario");
				textoTable.append("		<tr>");
				textoTable.append("			<td style=\"white-space: normal\">" + descricao + "</td>");
				textoTable.append("			<td>" + NeoDateUtils.safeDateFormat(dataRegistro,"dd/MM/yyyy HH:mm:ss") + "</td>");
				textoTable.append("			<td style=\"white-space: normal\">" + usuario + "</td>");
				textoTable.append("		</tr>");
			}
			
			textoTable.append("			</tbody>");
			textoTable.append("		</table>");
		}
		String txtFinal = textoTable.toString();
		
		txtFinal = txtFinal.replaceAll("\n", " ").replaceAll("\r", " ").replaceAll("&Acirc;&#160;", " ").replaceAll("Â&nbsp;", "");
		return txtFinal;
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}
}