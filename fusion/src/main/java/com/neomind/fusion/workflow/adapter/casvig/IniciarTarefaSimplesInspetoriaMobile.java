package com.neomind.fusion.workflow.adapter.casvig;

import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.util.NeoDateUtils;

public class IniciarTarefaSimplesInspetoriaMobile implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		String solicitante = "dilmoberger";
		String executor = "";
		String titulo = "Justificar o não envio da reinspeção referente a tarefa " + activity.getCode();
		String descricao = "Por qual motivo a solicitação a seguir, do fluxo de Inspetoria Mobile, não foi atendida?<br><br>";
		descricao += " Tarefa:" + activity.getCode() + "<br>";

		if (processEntity.getValue("obstarSim") != null)
		{
			String obsTarSim = String.valueOf(processEntity.getValue("obstarSim"));
			if (!obsTarSim.equals(""))
			{
				descricao = descricao + "<br>Obs: " + obsTarSim;
				processEntity.setValue("obstarSim",null);
			}
		}

		GregorianCalendar dataPrazo = new GregorianCalendar();
		dataPrazo.add(GregorianCalendar.DATE, 2);

		boolean diaUtil = false;
		while (!diaUtil)
		{
			if (!OrsegupsUtils.isWorkDay(dataPrazo))
			{
				dataPrazo.add(GregorianCalendar.DATE, 1);
			}
			else
			{
				diaUtil = true;
			}
		}

		dataPrazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
		dataPrazo.set(GregorianCalendar.MINUTE, 59);
		dataPrazo.set(GregorianCalendar.SECOND, 59);

		NeoUser neoExecutor = (NeoUser) processEntity.getValue("executorTarefaSimples");
		executor = neoExecutor.getCode();

		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "2", "sim", dataPrazo);
		processEntity.setValue("tarefaSimples", tarefa);
		processEntity.setValue("executorTarefaSimples", null);
		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
