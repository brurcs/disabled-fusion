package com.neomind.fusion.workflow.adapter.ferias;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.presenca.vo.HistoricoLocalVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;
//com.neomind.fusion.workflow.adapter.ferias.AdapterDefinePrazoTarefaRH
//com.neomind.fusion.workflow.adapter.ferias-AdapterDefinePrazoTarefaRH

public class AdapterDefinePrazoTarefaRH implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		
		List<NeoObject> registroAtividades = (List<NeoObject>) processEntity.getValue("r001RegistroAtividade");
		String observacao = "";
		observacao = String.valueOf(processEntity.getValue("obsAjuste"));

		if(observacao == null){
			observacao = "";
		}
		
		InstantiableEntityInfo insRegAti = AdapterUtils.getInstantiableEntityInfo("r001RegistroAtividades");
		NeoObject objRegAti = insRegAti.createNewInstance();
		EntityWrapper wRegAti = new EntityWrapper(objRegAti);

		String responsavel = origin.returnResponsible();
		GregorianCalendar dataAcao = new GregorianCalendar();

		wRegAti.setValue("usuario", responsavel);
		wRegAti.setValue("dataAcao", dataAcao);
		wRegAti.setValue("descricao", observacao);

		PersistEngine.persist(objRegAti);
		registroAtividades.add(objRegAti);

		processEntity.setValue("r001RegistroAtividade", registroAtividades);
		processEntity.setValue("obsAjuste","");
		
		if (processEntity.getValue("prazoRH") == null)
		{
			GregorianCalendar datPrazo = null;
			datPrazo = new GregorianCalendar();
			datPrazo.add(GregorianCalendar.DATE, 2);
			System.out.println("Data: " + NeoDateUtils.safeDateFormat(datPrazo, "dd/MM/yyyy"));
			if (!OrsegupsUtils.isWorkDay(datPrazo))
			{
				datPrazo = OrsegupsUtils.getNextWorkDay(datPrazo);
			}
			System.out.println("Data: " + NeoDateUtils.safeDateFormat(datPrazo, "dd/MM/yyyy"));
			datPrazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
			datPrazo.set(GregorianCalendar.MINUTE, 59);
			datPrazo.set(GregorianCalendar.SECOND, 59);
			processEntity.setValue("prazoRH", datPrazo);

			NeoObject colaborador = (NeoObject) processEntity.findField("primeiraSugestao").getValue();
			EntityWrapper wColaborador = new EntityWrapper(colaborador);
			long codreg = (long) wColaborador.findField("usu_codreg").getValue();
			long numemp = (long) wColaborador.findField("numemp").getValue();
			long numcad = (long) wColaborador.findField("numcad").getValue();
			long tipcol = (long) wColaborador.findField("tipcol").getValue();
			GregorianCalendar datAtu = new GregorianCalendar();
			HistoricoLocalVO lotacao = QLPresencaUtils.listaHistoricoLocal(numemp, tipcol, numcad, NeoDateUtils.safeDateFormat(datAtu, "yyyy-MM-dd"));
			NeoPaper coordernador = OrsegupsUtils.getPapelResponsavelJ002ReciboFerias(codreg, lotacao.getPosto().getNomePosto());
			NeoUser responsavelRegional = null;
			if (coordernador == null)
			{
				coordernador = OrsegupsUtils.getPapelResponsavelJ002ReciboFerias(1L, lotacao.getPosto().getNomePosto());
				for (NeoUser usr : coordernador.getUsers())
				{
					responsavelRegional = usr;
					break;
				}
			}
			else
			{
				for (NeoUser usr : coordernador.getUsers())
				{
					responsavelRegional = usr;
					break;
				}
			}
			processEntity.findField("responsavelRegional").setValue(responsavelRegional);
		}
		else
		{
			if ((Boolean) processEntity.getValue("concederPrazo"))
			{
				GregorianCalendar novoPrazoRh = (GregorianCalendar) processEntity.getValue("novoPrazoRh");
				if(!OrsegupsUtils.isWorkDay(novoPrazoRh)){
					throw new WorkflowException("O dia informado não é util.");
				}
				novoPrazoRh.set(GregorianCalendar.HOUR_OF_DAY, 23);
				novoPrazoRh.set(GregorianCalendar.MINUTE, 59);
				novoPrazoRh.set(GregorianCalendar.SECOND, 59);
				processEntity.setValue("prazoRH", novoPrazoRh);
				
			}
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}
}
