package com.neomind.fusion.workflow.adapter.casvig;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class CasvigDeadline implements AdapterInterface
{

	@Override
	public void back(EntityWrapper processEntity, Activity activity) 
	{
		
	}

	@Override
	public void start(Task origin, EntityWrapper processEntity,
			Activity activity) 
	{
		GregorianCalendar dataInicial = (GregorianCalendar) processEntity.findField("wfprocess.startDate").getValue();
		
		GregorianCalendar dataDez = (GregorianCalendar) dataInicial.clone();
		GregorianCalendar dataVinte = (GregorianCalendar) dataInicial.clone();
		GregorianCalendar dataTrinta = (GregorianCalendar) dataInicial.clone();
		
		dataDez.add(Calendar.DAY_OF_MONTH, 10);
		dataVinte.add(Calendar.DAY_OF_MONTH, 20);
		dataTrinta.add(Calendar.DAY_OF_MONTH, 30);
		
		processEntity.setValue("dataSistema40dias", dataDez);
		processEntity.setValue("dataSistema50dias", dataVinte);
		processEntity.setValue("dataSistema60dias", dataTrinta);
		
	}

}
