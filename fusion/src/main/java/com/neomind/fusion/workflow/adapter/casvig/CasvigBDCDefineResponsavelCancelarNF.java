package com.neomind.fusion.workflow.adapter.casvig;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class CasvigBDCDefineResponsavelCancelarNF implements AdapterInterface {

	@Override
	public void start(Task origin, EntityWrapper processEntity,
			Activity activity) {
		String mensagem = "";
		try {
			Long codEmp = (Long) processEntity.findValue("empresa.codemp");

			NeoPaper papel = new NeoPaper();
			NeoUser executor = null;

			papel = OrsegupsUtils.getPapelResponsavelCancelarNF(codEmp);

			if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty()) {
				for (NeoUser user : papel.getUsers()) {
					executor = user;
					break;
				}
			} else {
				mensagem = "Papel não Encontrado no Sitema!";
				throw new WorkflowException(mensagem);
			}
			if (executor != null) {
				processEntity.findField("usuarioResponsavelCancelamentoNF").setValue(executor);
			} else {
				mensagem = "Usuario não Encontrato no Papel - " + papel;
				throw new WorkflowException(mensagem);
			}
		} catch (WorkflowException e) {
			e.printStackTrace();
			throw new WorkflowException(mensagem);
		} catch (Exception e) {
			e.printStackTrace();
			throw new WorkflowException(
					"Erro ao Abrir tarefa Simples para Envio de Boleto!");
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity) {

	}

}
