package com.neomind.fusion.workflow.adapter.reciclagem;

import java.io.PrintWriter;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.GregorianCalendar;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.presenca.vo.EscalaVO;
import com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

//com.neomind.fusion.workflow.adapter.reciclagem.InserirAfastamentoReciclagemAdapter
public class InserirAfastamentoReciclagemAdapter implements AdapterInterface
{
	private static final long serialVersionUID = 1L;
	private static final Log log = LogFactory.getLog(InserirAfastamentoReciclagemAdapter.class);
	EntityManager entityManager;
	EntityTransaction transaction;
	
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{
			String numemp = NeoUtils.safeOutputString( processEntity.findValue("numemp"));
			String tipcol = NeoUtils.safeOutputString( processEntity.findValue("tipcol"));
			String numcad = NeoUtils.safeOutputString( processEntity.findValue("numcad"));
			GregorianCalendar datafa =  (GregorianCalendar) processEntity.findValue("dataCurso");
			String horini = "0";
			GregorianCalendar datTermino  = (GregorianCalendar) processEntity.findValue("dataFimCurso");
			String horFim = "0";
			
			EscalaVO escala = QLPresencaUtils.getEscalaColaborador(NeoUtils.safeLong(numcad), NeoUtils.safeLong(numemp), NeoUtils.safeLong(tipcol));
			
			String codesc = NeoUtils.safeOutputString( escala.getCodigoEscala() );
			String codtma = NeoUtils.safeOutputString( escala.getCodigoTurma() );
		
			insertAfastamentoReciclagem(numemp, tipcol, numcad, datafa, codesc, codtma, horini, datTermino, horFim);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Erro ao inserir afastamento de reciclagem para o colaborador, motivo: "+e.getMessage());
		}
		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}
	
	
	private void insertAfastamentoReciclagem(String numemp, String tipcol, String numcad, GregorianCalendar datafa, String codesc, String codtma, String horini, GregorianCalendar datTermino, String horFim) throws Exception
	{

		this.entityManager = PersistEngine.getEntityManager("VETORH");
		this.transaction = this.entityManager.getTransaction();

		Integer result = 0;
		System.out.println("Inserir afastamento de reciclagem. numcad:"+numcad+" numemp:"+numemp);
		try
		{

			GregorianCalendar dataNula = new GregorianCalendar(1900, GregorianCalendar.DECEMBER, 31);
			GregorianCalendar dataInicioEscala = new GregorianCalendar();
			GregorianCalendar dataFinalEscala = new GregorianCalendar();
			GregorianCalendar dataInicioAfa = new GregorianCalendar();
			GregorianCalendar datFimAfa = new GregorianCalendar();
			GregorianCalendar dataAtual = new GregorianCalendar();
			
			dataInicioAfa = (GregorianCalendar) datafa.clone();
			datFimAfa = (GregorianCalendar) datTermino.clone();
			
			String afastamentoInformado = null;
			
			//Valida escala para inserir afastamento informado
//			if (horini.equals("0"))
//			{
//				List<EscalaVO> escalas = QLPresencaUtils.getEscalasColaboradores(dataInicioAfa, Long.valueOf(codesc), Long.valueOf(codtma),true);
//				//List<EscalaVO> escalas = null;
//				
//				if (escalas != null && !escalas.isEmpty() && escalas.get(0).getHorarios() != null && escalas.get(0).getHorarios().get(0).getDataInicial() != null)
//				{
//					dataInicioEscala = escalas.get(0).getHorarios().get(0).getDataInicial();
//					long horas = (dataInicioEscala.getTimeInMillis() - dataAtual.getTimeInMillis()) / 3600000;
//					
//					if (horas >= 6L)
//					{
//						afastamentoInformado = "S";
//					}
//				}
//			}
//			else
//			{
//				Integer hora = Integer.valueOf(horini) / 60;
//				Integer minuto = Integer.valueOf(horini) % 60;
//				
//				GregorianCalendar dataEntrada = (GregorianCalendar) datafa.clone();
//				dataEntrada.add(Calendar.HOUR_OF_DAY, hora);
//				dataEntrada.add(Calendar.MINUTE, minuto);
//				
//				long horas = (dataEntrada.getTimeInMillis() - dataAtual.getTimeInMillis()) / 3600000;
//				
//				if (horas >= 6L)
//				{
//					afastamentoInformado = "S";
//				}
//			}
			afastamentoInformado = "S";
			if (!this.transaction.isActive())
			{
				this.transaction.begin();
			}
			
			StringBuilder admColaborador = new StringBuilder();
			admColaborador.append(" SELECT DATADM FROM R034FUN WHERE numemp = :numemp AND tipcol = :tipcol AND numcad = :numcad ");

			Query queryAdm = this.entityManager.createNativeQuery(admColaborador.toString());
			queryAdm.setParameter("numemp", numemp);
			queryAdm.setParameter("tipcol", tipcol);
			queryAdm.setParameter("numcad", numcad);
			queryAdm.setMaxResults(1);

		    Timestamp timeAdm = (Timestamp) queryAdm.getSingleResult();
		    GregorianCalendar dataAdm = new GregorianCalendar(); 
		    dataAdm.setTimeInMillis(timeAdm.getTime());
		    System.out.println("Verificação de data de admissão do colaborador. numcad:"+numcad+" numemp:"+numemp);
			if (NeoUtils.safeIsNotNull(dataAdm) && dataInicioAfa.before(dataAdm))
			{
				result = 1000;
			}
			else
			{
				StringBuilder buscaAfastamento = new StringBuilder();
				buscaAfastamento.append(" SELECT COUNT(*) FROM R038AFA WHERE numemp = :numemp ");
				buscaAfastamento.append(" AND tipcol = :tipcol ");
				buscaAfastamento.append(" AND numcad = :numcad ");
				buscaAfastamento.append(" AND CAST(FLOOR(CAST(datafa AS float)) AS datetime) <= :dataInicio ");
				buscaAfastamento.append(" AND (CAST(FLOOR(CAST(datter AS float)) AS datetime) >= :dataTermino OR datter = :dataNula)  ");

				Query queryBuscaAfa = this.entityManager.createNativeQuery(buscaAfastamento.toString());
				queryBuscaAfa.setParameter("numemp", numemp);
				queryBuscaAfa.setParameter("tipcol", tipcol);
				queryBuscaAfa.setParameter("numcad", numcad);
				queryBuscaAfa.setParameter("dataInicio", dataInicioAfa);
				queryBuscaAfa.setParameter("dataTermino", datFimAfa);
				queryBuscaAfa.setParameter("dataNula", dataNula);
				queryBuscaAfa.setMaxResults(1);

				Integer count = (Integer) queryBuscaAfa.getSingleResult();
				Boolean inserirAfastamento = false;
				System.out.println("Verificação de afastamentos do colaborador no período. numcad:"+numcad+" numemp:"+numemp);
				if (count != null && count > 0)
				{
					result = 100;
					if (possuiAfastamentoNoPeriodo(numemp, tipcol, numcad, dataInicioAfa, datFimAfa, dataNula)){
					    inserirAfastamento = false;
					}else{
					    inserirAfastamento = true;
					}
				}else{
				    inserirAfastamento = true;
				}
				System.out.println("Verificação se é para inserir afastamento. numcad:"+numcad+" numemp:"+numemp);
				if (inserirAfastamento)
				{
				    System.out.println("Inserir afastamento. numcad:"+numcad+" numemp:"+numemp);
					NeoUser user = PortalUtil.getCurrentUser();
					StringBuilder insereAfastamento = new StringBuilder();

					String dataString = NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm");
					String ObsAfa = "Inserido via tarefa de  Reciclagem por " + user.getFullName() + " em " + dataString;

					insereAfastamento.append(" Insert Into R038AFA (NumEmp, TipCol, NumCad, DatAfa, HorAfa, DatTer, SitAfa, OriAfa, ObsAfa, StaAtu, usu_codusu, usu_datger, usu_afainf) ");
					insereAfastamento.append(" 				Values (:numemp,:tipcol,:numcad,:dataInicio,0,:dataTermino,117,0,:ObsAfa,0,389,:dataAtual, :afainf) ");

					Query queryInsereAfa = this.entityManager.createNativeQuery(insereAfastamento.toString());

					queryInsereAfa.setParameter("numemp", numemp);
					queryInsereAfa.setParameter("tipcol", tipcol);
					queryInsereAfa.setParameter("numcad", numcad);
					queryInsereAfa.setParameter("dataInicio", new Timestamp(dataInicioAfa.getTime().getTime()));
					queryInsereAfa.setParameter("dataTermino", new Timestamp(datFimAfa.getTime().getTime()));
					queryInsereAfa.setParameter("ObsAfa", ObsAfa);
					queryInsereAfa.setParameter("dataAtual", new Timestamp(dataAtual.getTime().getTime()));
					queryInsereAfa.setParameter("afainf", afastamentoInformado);

					result = queryInsereAfa.executeUpdate();

					//LOG Colaborador
					String txtLog = "Inserido afastamento em " + NeoUtils.safeDateFormat(datafa, "dd/MM/yyyy")+" à "+NeoUtils.safeDateFormat(datTermino, "dd/MM/yyyy");

					StringBuilder buscaCpf = new StringBuilder();
					buscaCpf.append(" SELECT numcpf FROM R034FUN WHERE numemp = :numemp ");
					buscaCpf.append(" AND tipcol = :tipcol ");
					buscaCpf.append(" AND numcad = :numcad ");

					Query queryBuscaCpf = this.entityManager.createNativeQuery(buscaCpf.toString());
					queryBuscaCpf.setParameter("numemp", numemp);
					queryBuscaCpf.setParameter("tipcol", tipcol);
					queryBuscaCpf.setParameter("numcad", numcad);
					queryBuscaCpf.setMaxResults(1);

					BigInteger cpf = (BigInteger) queryBuscaCpf.getSingleResult();
					String cpfString = cpf.toString();

					if (cpfString != null && !cpfString.equals(""))
					{
						this.saveLog(txtLog, null, cpfString, "colaborador", null);
					}

					System.out.println("Cadastro Afastamento -  NumEmp:" + numemp + " - NumCad: " + numcad + ")");

					this.transaction.commit();
				}
			}
		}
		catch (Exception ex)
		{
		    ex.printStackTrace();
		    if (this.transaction.isActive()){
			this.transaction.rollback();
		    }
		    System.out.println("Erro ao inserir afastamento de reciclagem. ");
		    throw new Exception("Erro ao inserir afastamento de reciclagem. ", ex);
		}

	}
	
	private void saveLog(String texto, String idposto, String idColaborador, String tipo, PrintWriter out)
	{
		if (tipo != null && tipo.equalsIgnoreCase("posto"))
		{
			QLGroupFilter groupFilter = new QLGroupFilter("AND");
			groupFilter.addFilter(new QLEqualsFilter("codloc", idposto));

			/**
		 	* @author orsegups lucas.avila - Classe generica.
		 	* @date 08/07/2015
		 	*/
			Class<? extends NeoObject> clazz = AdapterUtils.getEntityClass("VETORH_USU_Vorg203nv8");

			NeoObject postoObject = PersistEngine.getObject(clazz, groupFilter);

			if (postoObject != null)
			{
				NeoObject logPosto = AdapterUtils.createNewEntityInstance("QLLogPresencaPosto");

				if (logPosto != null)
				{
					EntityWrapper postoWrapper = new EntityWrapper(logPosto);
					postoWrapper.setValue("posto", postoObject);
					postoWrapper.setValue("dataLog", new GregorianCalendar());
					postoWrapper.setValue("textoLog", texto);
					postoWrapper.setValue("usuario", PortalUtil.getCurrentUser());

					PersistEngine.persist(logPosto);
					if (out != null)
					{
						out.print("Log salvo com sucesso.");
					}
					return;
				}
			}
		}
		else if (tipo != null && tipo.equalsIgnoreCase("colaborador"))
		{
			NeoObject log = AdapterUtils.createNewEntityInstance("QLLogPresencaColaborador");

			if (log != null && idColaborador != null)
			{
				texto = texto.replaceAll("'", "´");
				texto = texto.replaceAll("\"", "´");

				EntityWrapper logWrapper = new EntityWrapper(log);
				logWrapper.setValue("cpf", Long.valueOf(idColaborador));
				logWrapper.setValue("dataLog", new GregorianCalendar());
				logWrapper.setValue("textoLog", texto);
				logWrapper.setValue("usuario", PortalUtil.getCurrentUser());

				if (idposto != null)
				{
					QLGroupFilter groupFilter = new QLGroupFilter("AND");
					groupFilter.addFilter(new QLEqualsFilter("codloc", idposto));

					/**
		 			* @author orsegups lucas.avila - Classe generica.
		 			* @date 08/07/2015
		 			*/
					Class<? extends NeoObject> clazz = AdapterUtils.getEntityClass("VETORH_USU_Vorg203nv8");

					NeoObject postoObject = PersistEngine.getObject(clazz, groupFilter);

					if (postoObject != null)
					{
						logWrapper.setValue("posto", postoObject);
					}
				}

				PersistEngine.persist(log);

				if (out != null)
				{
					out.print("Log salvo com sucesso.");
				}
				return;
			}
		}

		out.print("Erro ao salvar log.");
	}

	private Boolean possuiAfastamentoNoPeriodo(String numemp, String tipcol, String numcad, GregorianCalendar dataInicioAfa, GregorianCalendar datFimAfa, GregorianCalendar dataNula){
	    System.out.println("Inicio da verificação de afastamento de reciclagem no período. numcad:"+numcad+" numemp:"+numemp);
	    StringBuilder buscaAfastamento = new StringBuilder();
	    buscaAfastamento.append(" SELECT COUNT(*) FROM R038AFA WHERE numemp = :numemp ");
	    buscaAfastamento.append(" AND tipcol = :tipcol ");
	    buscaAfastamento.append(" AND numcad = :numcad ");
	    buscaAfastamento.append(" AND CAST(FLOOR(CAST(datafa AS float)) AS datetime) <= :dataInicio ");
	    buscaAfastamento.append(" AND (CAST(FLOOR(CAST(datter AS float)) AS datetime) >= :dataTermino OR datter = :dataNula) AND sitafa = 117 ");
    
	    Query queryBuscaAfa = this.entityManager.createNativeQuery(buscaAfastamento.toString());
	    queryBuscaAfa.setParameter("numemp", numemp);
	    queryBuscaAfa.setParameter("tipcol", tipcol);
	    queryBuscaAfa.setParameter("numcad", numcad);
	    queryBuscaAfa.setParameter("dataInicio", dataInicioAfa);
	    queryBuscaAfa.setParameter("dataTermino", datFimAfa);
	    queryBuscaAfa.setParameter("dataNula", dataNula);
	    queryBuscaAfa.setMaxResults(1);

	    Integer count = (Integer) queryBuscaAfa.getSingleResult();
	    System.out.println("Fim da verificação de afastamento de reciclagem no período. numcad:"+numcad+" numemp:"+numemp);
	    return count > 0;
	}
	
}


