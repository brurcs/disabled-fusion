package com.neomind.fusion.workflow.adapter.casvig;

import java.sql.Time;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.entity.dynamic.EntityCloner;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class CasvigBancoCurriculo implements AdapterInterface
{
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		long numeroCPF = ((Long) processEntity.findField("cvTemp.numeroCPF").getValue()).longValue();

		InstantiableEntityInfo eBancoCV = AdapterUtils.getInstantiableEntityInfo("CVbancoDeCurriculos");

		List<NeoObject> listaCV = PersistEngine.getObjects(eBancoCV.getEntityClass(), new QLEqualsFilter("numeroCPF", Long.valueOf(numeroCPF)));
		if ((listaCV != null) && (listaCV.size() > 0))
		{
			NeoObject cvCadastrado = (NeoObject) listaCV.get(0);

			EntityWrapper wrpOld = new EntityWrapper(cvCadastrado);
			wrpOld.findField("dataUltimaAtualizacao").setValue(new GregorianCalendar());
			wrpOld.findField("horaUltimaAtualizacao").setValue(new Time(new Date().getTime()));
			wrpOld.findField("areaInteresse").setValue(processEntity.findField("cvTemp.areaInteresse").getValue());
			wrpOld.findField("subAreaInteresse").setValue(processEntity.findField("cvTemp.subAreaInteresse").getValue());
			wrpOld.findField("nomeCompleto").setValue(processEntity.findField("cvTemp.nomeCompleto").getValue());
			wrpOld.findField("numeroCPF").setValue(processEntity.findField("cvTemp.numeroCPF").getValue());
			wrpOld.findField("sexo").setValue(processEntity.findField("cvTemp.sexo").getValue());
			wrpOld.findField("dataNascimento").setValue(processEntity.findField("cvTemp.dataNascimento").getValue());
			wrpOld.findField("foto").setValue(processEntity.findField("cvTemp.foto").getValue());
			wrpOld.findField("rg").setValue(processEntity.findField("cvTemp.rg").getValue());
			wrpOld.findField("idadeFilhos").setValue(processEntity.findField("cvTemp.idadeFilhos").getValue());

			Calendar origDate = (GregorianCalendar) processEntity.findField("cvTemp.dataNascimento").getValue();
			Calendar birthDate = (Calendar) origDate.clone();
			Calendar today = Calendar.getInstance();
			int age = today.get(1) - birthDate.get(1);
			birthDate.add(1, age);

			if (today.before(birthDate))
			{
				--age;
			}
			wrpOld.findField("idade").setValue(new Long(age));
			wrpOld.findField("endereco").setValue(processEntity.findField("cvTemp.endereco").getValue());
			wrpOld.findField("email").setValue(processEntity.findField("cvTemp.email").getValue());
			wrpOld.findField("numero").setValue(processEntity.findField("cvTemp.numero").getValue());
			wrpOld.findField("complemento").setValue(processEntity.findField("cvTemp.complemento").getValue());
			wrpOld.findField("cep").setValue(processEntity.findField("cvTemp.cep").getValue());
			wrpOld.findField("cidade").setValue(processEntity.findField("cvTemp.cidade").getValue());

			long codReg = ((Long) processEntity.findField("cvTemp.cidade.usu_codreg").getValue()).longValue();
			NeoObject regional = null;
			List<NeoObject> listaRegional = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUREG"), new QLEqualsFilter("usu_codreg", Long.valueOf(codReg)));
			if ((listaRegional != null) && (listaRegional.size() > 0))
			{
				regional = (NeoObject) listaRegional.get(0);
			}
			wrpOld.findField("regional").setValue(regional);

			wrpOld.findField("bairro").setValue(processEntity.findField("cvTemp.bairro").getValue());
			wrpOld.findField("telefone1").setValue(processEntity.findField("cvTemp.telefone1").getValue());
			wrpOld.findField("telefone2").setValue(processEntity.findField("cvTemp.telefone2").getValue());
			wrpOld.findField("telefone3").setValue(processEntity.findField("cvTemp.telefone3").getValue());
			wrpOld.findField("motocicleta").setValue(processEntity.findField("cvTemp.motocicleta").getValue());
			wrpOld.findField("estadoCivil").setValue(processEntity.findField("cvTemp.estadoCivil").getValue());
			wrpOld.findField("numeroFilhos").setValue(processEntity.findField("cvTemp.numeroFilhos").getValue());
			wrpOld.findField("altura").setValue(processEntity.findField("cvTemp.altura").getValue());
			wrpOld.findField("peso").setValue(processEntity.findField("cvTemp.peso").getValue());
			wrpOld.findField("turnoDesejado").setValue(processEntity.findField("cvTemp.turnoDesejado").getValue());
			wrpOld.findField("grauInstrucao").setValue(processEntity.findField("cvTemp.grauInstrucao").getValue());
			wrpOld.findField("estudaAtualmente").setValue(processEntity.findField("cvTemp.estudaAtualmente").getValue());
			wrpOld.findField("curso").setValue(processEntity.findField("cvTemp.curso").getValue());
			wrpOld.findField("turnoEstudo").setValue(processEntity.findField("cvTemp.turnoEstudo").getValue());
			wrpOld.findField("cursoVigilante").setValue(processEntity.findField("cvTemp.cursoVigilante").getValue());
			wrpOld.findField("dataCursoVigilante").setValue(processEntity.findField("cvTemp.dataCursoVigilante").getValue());
			wrpOld.findField("escolaVigilante").setValue(processEntity.findField("cvTemp.escolaVigilante").getValue());
			wrpOld.findField("dataReciclagem").setValue(processEntity.findField("cvTemp.dataReciclagem").getValue());
			wrpOld.findField("escolaReciclagem").setValue(processEntity.findField("cvTemp.escolaReciclagem").getValue());
			NeoObject ultimoEmprego = (NeoObject) processEntity.findField("cvTemp.ultimoEmprego").getValue();
			wrpOld.findField("ultimoEmprego").setValue(EntityCloner.cloneNeoObject(ultimoEmprego));
			NeoObject penultimoEmprego = (NeoObject) processEntity.findField("cvTemp.penultimoEmprego").getValue();
			wrpOld.findField("penultimoEmprego").setValue(EntityCloner.cloneNeoObject(penultimoEmprego));
			wrpOld.findField("conheceuEmpresa").setValue(processEntity.findField("cvTemp.conheceuEmpresa").getValue());
			wrpOld.findField("trabalhouGrupo").setValue(processEntity.findField("cvTemp.trabalhouGrupo").getValue());
			wrpOld.findField("empresaTrabalhouGrupo").setValue(processEntity.findField("cvTemp.empresaTrabalhouGrupo").getValue());
			
			//replica a cidade e o bairro para poder pesquiser no fusion.
			wrpOld.setValue("cidadeStr", processEntity.findValue("cvTemp.cidade.nomcid"));
			wrpOld.setValue("bairroStr", processEntity.findValue("cvTemp.bairro.nombai"));

			PersistEngine.persist(cvCadastrado);
		}
		else
		{
			NeoObject novoCV = eBancoCV.createNewInstance();
			EntityWrapper wrp = new EntityWrapper(novoCV);
			wrp.findField("dataEnvio").setValue(new GregorianCalendar());
			wrp.findField("horaEnvio").setValue(new Time(new Date().getTime()));
			wrp.findField("dataUltimaAtualizacao").setValue(new GregorianCalendar());
			wrp.findField("horaUltimaAtualizacao").setValue(new GregorianCalendar());
			wrp.findField("areaInteresse").setValue(processEntity.findField("cvTemp.areaInteresse").getValue());
			wrp.findField("subAreaInteresse").setValue(processEntity.findField("cvTemp.subAreaInteresse").getValue());
			wrp.findField("nomeCompleto").setValue(processEntity.findField("cvTemp.nomeCompleto").getValue());
			wrp.findField("numeroCPF").setValue(processEntity.findField("cvTemp.numeroCPF").getValue());
			wrp.findField("sexo").setValue(processEntity.findField("cvTemp.sexo").getValue());
			wrp.findField("dataNascimento").setValue(processEntity.findField("cvTemp.dataNascimento").getValue());
			wrp.findField("foto").setValue(processEntity.findField("cvTemp.foto").getValue());
			wrp.findField("rg").setValue(processEntity.findField("cvTemp.rg").getValue());
			wrp.findField("idadeFilhos").setValue(processEntity.findField("cvTemp.idadeFilhos").getValue());

			Calendar origDate = (GregorianCalendar) processEntity.findField("cvTemp.dataNascimento").getValue();
			Calendar birthDate = (Calendar) origDate.clone();
			Calendar today = Calendar.getInstance();
			int age = today.get(1) - birthDate.get(1);
			birthDate.add(1, age);
			if (today.before(birthDate))
			{
				--age;
			}
			wrp.findField("idade").setValue(new Long(age));
			wrp.findField("endereco").setValue(processEntity.findField("cvTemp.endereco").getValue());
			wrp.findField("email").setValue(processEntity.findField("cvTemp.email").getValue());
			wrp.findField("numero").setValue(processEntity.findField("cvTemp.numero").getValue());
			wrp.findField("complemento").setValue(processEntity.findField("cvTemp.complemento").getValue());
			wrp.findField("cep").setValue(processEntity.findField("cvTemp.cep").getValue());
			wrp.findField("cidade").setValue(processEntity.findField("cvTemp.cidade").getValue());

			long codReg = ((Long) processEntity.findField("cvTemp.cidade.usu_codreg").getValue()).longValue();
			NeoObject regional = null;
			List<NeoObject> listaRegional = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUREG"), new QLEqualsFilter("usu_codreg", Long.valueOf(codReg)));
			if ((listaRegional != null) && (listaRegional.size() > 0))
			{
				regional = (NeoObject) listaRegional.get(0);
			}
			wrp.findField("regional").setValue(regional);

			wrp.findField("bairro").setValue(processEntity.findField("cvTemp.bairro").getValue());
			wrp.findField("telefone1").setValue(processEntity.findField("cvTemp.telefone1").getValue());
			wrp.findField("telefone2").setValue(processEntity.findField("cvTemp.telefone2").getValue());
			wrp.findField("telefone3").setValue(processEntity.findField("cvTemp.telefone3").getValue());
			wrp.findField("motocicleta").setValue(processEntity.findField("cvTemp.motocicleta").getValue());
			wrp.findField("estadoCivil").setValue(processEntity.findField("cvTemp.estadoCivil").getValue());
			wrp.findField("numeroFilhos").setValue(processEntity.findField("cvTemp.numeroFilhos").getValue());
			wrp.findField("altura").setValue(processEntity.findField("cvTemp.altura").getValue());
			wrp.findField("peso").setValue(processEntity.findField("cvTemp.peso").getValue());
			wrp.findField("turnoDesejado").setValue(processEntity.findField("cvTemp.turnoDesejado").getValue());
			wrp.findField("grauInstrucao").setValue(processEntity.findField("cvTemp.grauInstrucao").getValue());
			wrp.findField("estudaAtualmente").setValue(processEntity.findField("cvTemp.estudaAtualmente").getValue());
			wrp.findField("curso").setValue(processEntity.findField("cvTemp.curso").getValue());
			wrp.findField("turnoEstudo").setValue(processEntity.findField("cvTemp.turnoEstudo").getValue());
			wrp.findField("cursoVigilante").setValue(processEntity.findField("cvTemp.cursoVigilante").getValue());
			wrp.findField("dataCursoVigilante").setValue(processEntity.findField("cvTemp.dataCursoVigilante").getValue());
			wrp.findField("escolaVigilante").setValue(processEntity.findField("cvTemp.escolaVigilante").getValue());
			wrp.findField("dataReciclagem").setValue(processEntity.findField("cvTemp.dataReciclagem").getValue());
			wrp.findField("escolaReciclagem").setValue(processEntity.findField("cvTemp.escolaReciclagem").getValue());
			NeoObject ultimoEmprego = (NeoObject) processEntity.findField("cvTemp.ultimoEmprego").getValue();
			wrp.findField("ultimoEmprego").setValue(EntityCloner.cloneNeoObject(ultimoEmprego));
			NeoObject penultimoEmprego = (NeoObject) processEntity.findField("cvTemp.penultimoEmprego").getValue();
			wrp.findField("penultimoEmprego").setValue(EntityCloner.cloneNeoObject(penultimoEmprego));
			wrp.findField("conheceuEmpresa").setValue(processEntity.findField("cvTemp.conheceuEmpresa").getValue());
			wrp.findField("trabalhouGrupo").setValue(processEntity.findField("cvTemp.trabalhouGrupo").getValue());
			wrp.findField("empresaTrabalhouGrupo").setValue(processEntity.findField("cvTemp.empresaTrabalhouGrupo").getValue());
			
			//replica a cidade e o bairro para poder pesquiser no fusion.
			wrp.setValue("cidadeStr", processEntity.findValue("cvTemp.cidade.nomcid"));
			wrp.setValue("bairroStr", processEntity.findValue("cvTemp.bairro.nombai"));
			
			PersistEngine.persist(novoCV);
		}
	}
}