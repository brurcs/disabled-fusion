package com.neomind.fusion.workflow.adapter.ferias;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.presenca.vo.ColaboradorVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EntradaAutorizadaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EscalaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.HistoricoLocalVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.PostoVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;
import com.sun.xml.bind.v2.TODO;

public class IniciaFluxoFeriasProgramadas implements CustomJobAdapter
{
	@Override
	public void execute(CustomJobContext arg0)
	{
		Connection conn = PersistEngine.getConnection("VETORH");
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		try
		{
			sql.append(" Select fun.numemp, fun.numcad, fun.nomfun, fun.numcpf, reg.USU_CodReg, reg.USU_NomReg, cob.usu_datalt, cob.usu_horini, cob.usu_datfim, car.TitRed, ");
			sql.append("		cob.usu_numempcob, cob.usu_numcadcob, fis.USU_NomFis,cpl.endrua,cpl.endcpl,cpl.endnum,bai.NomBai,cid.NomCid, orn.usu_codccu, orn.usu_lotorn From USU_T038COBFUN cob ");
			sql.append(" Inner Join R034FUN fun On fun.numemp = cob.usu_numemp And fun.tipcol = cob.usu_tipcol And fun.numcad = cob.usu_numcad ");
			sql.append(" Inner Join R038HLO hlo On hlo.NumEmp = fun.numemp And hlo.tipcol = fun.tipcol And hlo.numcad = fun.numcad And hlo.DatAlt = (Select Max(hlo2.datalt) From R038HLO hlo2 Where hlo2.numemp = hlo.numemp And hlo2.tipcol = hlo.tipcol And hlo2.numcad = hlo.numcad And hlo2.DatAlt <= cob.usu_datfim) ");
			sql.append(" Inner Join R016ORN orn On orn.taborg = hlo.TabOrg And orn.NumLoc = hlo.NumLoc ");
			sql.append(" Inner join USU_T200REG reg On reg.USU_CodReg = orn.usu_codreg ");
			sql.append(" Inner Join R038HCA hca On hca.NumEmp = fun.numemp And hca.tipcol = fun.tipcol And hca.numcad = fun.numcad And hca.DatAlt = (Select Max(hca2.datalt) From R038HCA hca2 Where hca2.numemp = hca.numemp And hca2.tipcol = hca.tipcol And hca2.numcad = hca.numcad And hca2.DatAlt <= cob.usu_datfim) ");
			sql.append(" Inner Join R024CAR car On car.CodCar = fun.codcar And car.EstCar = fun.estcar ");
			sql.append(" Inner JOIN USU_TFisRes fis ON fis.USU_FisRes = fun.usu_fisres ");
			sql.append(" Inner join r034cpl cpl on cpl.numcad = fun.numcad and cpl.numemp = fun.numemp and cpl.tipcol = fun.tipcol");
			sql.append(" Inner join R074BAI bai on bai.CodBai = cpl.codbai and bai.CodCid = cpl.codcid");
			sql.append(" Inner join R074CID cid on cid.CodCid = cpl.codcid");
			sql.append(" Where cob.usu_codmot = 3 AND cob.usu_nextid is not null And cob.usu_datfim >= '2014-10-19' And reg.USU_CodReg <> 0 AND fun.sitafa <> 7 ");
			sql.append(" And cob.usu_datfim Between GetDate()-4 And GetDate() + 18 ");
			sql.append(" And (fun.numemp <> cob.usu_numempcob Or fun.tipcol <> cob.usu_tipcolcob Or fun.numcad <> cob.usu_numcadcob)");
			sql.append(" And not exists (select * FROM [CACUPE\\SQL02].fusion_producao.DBO.d_TarefaFeriasProgramadas fp WITH (NOLOCK) ");
			sql.append("  			  	 where fp.numcpf = fun.numcpf and cast(floor(cast(fp.datalt as float)) as datetime) = cast(floor(cast(cob.usu_datalt as float)) as datetime) and fp.horini = cob.usu_horini) ");
			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();

			while (rs.next())
			{

				InstantiableEntityInfo insFerias = AdapterUtils.getInstantiableEntityInfo("principal");
				NeoObject objFerias = insFerias.createNewInstance();
				EntityWrapper wFerias = new EntityWrapper(objFerias);

				wFerias.setValue("numempFer", rs.getLong("numemp"));
				wFerias.setValue("numcadFer", rs.getLong("numcad"));
				wFerias.setValue("codccuFer", String.valueOf(rs.getLong("usu_codccu")));
				wFerias.setValue("nomfunFer", rs.getString("nomfun"));
				wFerias.setValue("titcarFer", rs.getString("titred"));
				wFerias.setValue("lotacaoFer", rs.getString("usu_lotorn"));
				wFerias.setValue("codregFer", rs.getLong("USU_CodReg"));
				wFerias.setValue("nomregFer", rs.getString("USU_NomReg"));
				Long horini = rs.getLong("usu_horini");

				GregorianCalendar dataInicio = new GregorianCalendar();
				dataInicio.setTime(rs.getDate("usu_datalt"));

				GregorianCalendar dataFim = new GregorianCalendar();
				dataFim.setTime(rs.getDate("usu_datfim"));
				dataFim.add(Calendar.DAY_OF_MONTH, +1);
				GregorianCalendar dataIni = new GregorianCalendar();
				dataIni.setTime(rs.getDate("usu_datalt"));
				GregorianCalendar prazo = new GregorianCalendar();
				prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
				prazo.set(Calendar.HOUR_OF_DAY, 23);
				prazo.set(Calendar.MINUTE, 59);
				prazo.set(Calendar.SECOND, 59);

				GregorianCalendar dataAtual = new GregorianCalendar();
				NeoFile anexo = null;

				if (rs.getLong("numemp") == 1 || rs.getLong("numemp") == 15 || rs.getLong("numemp") == 17 || rs.getLong("numemp") == 18 || rs.getLong("numemp") == 19 || rs.getLong("numemp") == 21 || rs.getLong("numemp") == 27 || rs.getLong("numemp") == 28 || rs.getLong("numemp") == 29)
				{
					anexo = OrsegupsUtils.criaNeoFile(new File("\\\\fsoofs01\\Vetorh\\Relatorios\\Avos de Ferias\\Regionais\\FPPF204COL_Vigilancia_Regional_" + rs.getLong("USU_CodReg") + "_Data_" + NeoUtils.safeDateFormat(dataAtual, "dd-MM-yyyy") + ".pdf"));
				}
				else if (rs.getLong("numemp") == 2 || rs.getLong("numemp") == 6 || rs.getLong("numemp") == 7 || rs.getLong("numemp") == 8 || rs.getLong("numemp") == 22)
				{
					anexo = OrsegupsUtils.criaNeoFile(new File("\\\\fsoofs01\\Vetorh\\Relatorios\\Avos de Ferias\\Regionais\\FPPF204COL_Asseio_Regional_" + rs.getLong("USU_CodReg") + "_Data_" + NeoUtils.safeDateFormat(dataAtual, "dd-MM-yyyy") + ".pdf"));
				}

				if (anexo != null)
				{
					wFerias.setValue("relatorioAvosDeFerias", anexo);
				}
				ColaboradorVO colaborador = new ColaboradorVO();
				colaborador.setEscala(new EscalaVO());
				colaborador.setEscala(QLPresencaUtils.getEscalaColaborador(rs.getLong("numcad"), rs.getLong("numemp"), 1L));
				wFerias.setValue("escalaFer", colaborador.getEscala().getDescricao() + "/ T: " + colaborador.getEscala().getCodigoTurma());
				wFerias.setValue("endFer", rs.getString("endrua") + ", Complemento: " + rs.getString("endcpl") + ", Nº: " + rs.getString("endnum"));
				wFerias.setValue("endFer", wFerias.findField("endFer").getValue() + " Bairro: " + rs.getString("NomBai") + " - Cidade: " + rs.getString("NomCid"));

				String dataCor = NeoUtils.safeDateFormat(dataIni, "yyyy-MM-dd");
				HistoricoLocalVO local = new HistoricoLocalVO();
				local.setPosto(new PostoVO());
				local = QLPresencaUtils.listaHistoricoLocal(rs.getLong("numemp"), 1L, rs.getLong("numcad"), dataCor);
				List<EntradaAutorizadaVO> coberturas = new ArrayList<EntradaAutorizadaVO>();
				coberturas = QLPresencaUtils.listaCoberturasTarefasProgramacaoDeFerias(rs.getLong("numemp"), 1L, rs.getLong("numcad"));
				SimpleDateFormat sfinc = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				wFerias.setValue("ultimasCobFer", "");
				if (coberturas != null && coberturas.size() > 0)
				{
					wFerias.setValue("ultimasCobFer", wFerias.findField("ultimasCobFer").getValue() + " Ultimas Coberturas<br>");
					for (EntradaAutorizadaVO entradaAutorizadaVO : coberturas)
					{
						wFerias.setValue("ultimasCobFer", wFerias.findField("ultimasCobFer").getValue() + "Periodo: " + sfinc.format(entradaAutorizadaVO.getDataInicial().getTime()) + " à " + sfinc.format(entradaAutorizadaVO.getDataFinal().getTime()));
						wFerias.setValue("ultimasCobFer", wFerias.findField("ultimasCobFer").getValue() + " - Operação: " + entradaAutorizadaVO.getDescricaoCobertura());
						if (entradaAutorizadaVO.getColaboradorSubstituido().getNomeColaborador() != null)
						{
							wFerias.setValue("ultimasCobFer", wFerias.findField("ultimasCobFer").getValue() + " - Substituído: " + entradaAutorizadaVO.getColaboradorSubstituido().getNomeColaborador());
						}
						wFerias.setValue("ultimasCobFer", wFerias.findField("ultimasCobFer").getValue() + " - Local: " + entradaAutorizadaVO.getPosto().getNomePosto());
						wFerias.setValue("ultimasCobFer", wFerias.findField("ultimasCobFer").getValue() + " - Obs: " + entradaAutorizadaVO.getObservacao() + "<br>");

					}
				}
				
				GregorianCalendar dataCorteIni = new GregorianCalendar();
				dataCorteIni.set(GregorianCalendar.HOUR_OF_DAY, 23);
				dataCorteIni.set(GregorianCalendar.MINUTE, 59);
				dataCorteIni.set(GregorianCalendar.SECOND, 59);
				dataCorteIni.set(GregorianCalendar.MILLISECOND, 59);
				dataCorteIni.set(GregorianCalendar.DAY_OF_MONTH, 25);
				dataCorteIni.set(GregorianCalendar.MONTH, 10);
				dataCorteIni.set(GregorianCalendar.YEAR, 2018);
				

				GregorianCalendar datFix = new GregorianCalendar();
				datFix.set(GregorianCalendar.HOUR_OF_DAY, 0);
				datFix.set(GregorianCalendar.MINUTE, 0);
				datFix.set(GregorianCalendar.SECOND, 0);
				datFix.set(GregorianCalendar.MILLISECOND, 0);

				GregorianCalendar dataCorte = new GregorianCalendar();
				dataCorte.set(GregorianCalendar.HOUR_OF_DAY, 23);
				dataCorte.set(GregorianCalendar.MINUTE, 59);
				dataCorte.set(GregorianCalendar.SECOND, 59);
				dataCorte.set(GregorianCalendar.MILLISECOND, 59);
				dataCorte.set(GregorianCalendar.DAY_OF_MONTH, 16);
				dataCorte.set(GregorianCalendar.MONTH, 11);
				dataCorte.set(GregorianCalendar.YEAR, 2018);

				if(datFix.after(dataCorteIni) && datFix.before(dataCorte)){
				    datFix.set(GregorianCalendar.DAY_OF_MONTH, 07);
				    datFix.set(GregorianCalendar.MONTH, 00);
				    datFix.set(GregorianCalendar.YEAR, 2019);
				    wFerias.setValue("dataFimCob", datFix);
				} else {
				    wFerias.setValue("dataFimCob", dataFim);
				}

				
				
				System.out.println("Data: "+NeoDateUtils.safeDateFormat(datFix,"dd/MM/yyyy"));
				//Fim
				NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "beatriz.malmann"));
				GregorianCalendar datAtu = new GregorianCalendar();
				datAtu.add(GregorianCalendar.DATE, 5);
				datAtu.set(GregorianCalendar.HOUR_OF_DAY, 23);
				datAtu.set(GregorianCalendar.MINUTE, 59);
				datAtu.set(GregorianCalendar.SECOND, 59);
				wFerias.setValue("prazoTarefa", datAtu);

				//INICIO - Criar objeto do colaborador cobertura para o R030
				QLOpFilter qlNumCad = new QLOpFilter("numcad", "=", rs.getLong("numcad"));
				QLOpFilter qlNumEmp = new QLOpFilter("numemp", "=", rs.getLong("numemp"));
				QLGroupFilter groupFilterColaborador = new QLGroupFilter("AND");
				groupFilterColaborador.addFilter(qlNumCad);
				groupFilterColaborador.addFilter(qlNumEmp);
				List<NeoObject> objsColaborador = PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORHUSUFUNFUSION"), groupFilterColaborador);
				for (NeoObject objCol : objsColaborador)
				{
					wFerias.findField("colCobFer").setValue(objCol);
					break;
				}
				//FIM - Criar objeto do colaborador cobertura para o R030

				//INICIO - Definir as operações para Fluxo R030.
				QLOpFilter codigoFerias = new QLOpFilter("codigo", "=", "0003");
				QLOpFilter codigoTrocaEscala = new QLOpFilter("codigo", "=", "0008");
				QLOpFilter codigoTreinamento = new QLOpFilter("codigo", "=", "0007");
				QLGroupFilter groupFilter = new QLGroupFilter("OR");
				groupFilter.addFilter(codigoFerias);
				groupFilter.addFilter(codigoTreinamento);
				groupFilter.addFilter(codigoTrocaEscala);
				List<NeoObject> objs = PersistEngine.getObjects(AdapterUtils.getEntityClass("QLOperacoes"), groupFilter);
				for (NeoObject objOp : objs)
				{
					EntityWrapper wOp = new EntityWrapper(objOp);
					String codOpe = String.valueOf(wOp.findField("codigo").getValue());
					if (codOpe.equals("0003"))
					{
						wFerias.findField("operacaoFerias").setValue(objOp);

					}
					else if (codOpe.equals("0008"))
					{
						wFerias.findField("operacaoTrocaEscala").setValue(objOp);
					}
					else if (codOpe.equals("0007"))
					{
						wFerias.findField("operacaoTreinamento").setValue(objOp);
					}
				}
				//FIM - Definir as operações para Fluxo R030.

				//Inicio - Start processo
				QLEqualsFilter equal = new QLEqualsFilter("Name", "R001 -  Programação de Férias");
				ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);
				//final WFProcess processo = WorkflowService.startProcess(processModel, objFerias, false, solicitante);
				wFerias.setValue("iniViaProAut", true);
				final WFProcess processo = processModel.startProcess(objFerias, false, null, null, null, null, solicitante);
				processo.setSaved(true);
				PersistEngine.persist(processo);
				PersistEngine.commit(true);
				
				try
				{
					new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividade(solicitante, true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				//FIM - Start processo

				InstantiableEntityInfo tarefaAJ = AdapterUtils.getInstantiableEntityInfo("TarefaFeriasProgramadas");
				NeoObject noAJ = tarefaAJ.createNewInstance();
				EntityWrapper ajWrapper = new EntityWrapper(noAJ);

				ajWrapper.findField("numcpf").setValue(rs.getLong("NumCpf"));
				ajWrapper.findField("datalt").setValue(dataInicio);
				ajWrapper.findField("horini").setValue(horini);
				ajWrapper.findField("tarefa").setValue(processo.getCode());
				PersistEngine.persist(noAJ);
				Thread.sleep(1000);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
	}
}
