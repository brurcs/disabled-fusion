package com.neomind.fusion.workflow.adapter.casvig;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.FieldWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoGroup;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class CasvigBDCDefinePrazoAprovacao implements AdapterInterface
{
  public void back(EntityWrapper processEntity, Activity activity)
  {
  }

  public void start(Task origin, EntityWrapper processEntity, Activity activity)
  {
	GregorianCalendar dataAtual = OrsegupsUtils.atribuiHora(new GregorianCalendar(), 23, 59, 59, 0);
    GregorianCalendar prazoAprovacao = (GregorianCalendar)dataAtual.clone();
	
	Boolean temContrato = (Boolean)processEntity.findField("tipoSolicitacao.exigeCTR").getValue();
	// Se tem contrato usa a data base, caso contrario o prazo é de 30 dias.
	if (temContrato) {
		int diaBase = NeoUtils.safeInteger(((Long)processEntity.findField("contrato.usu_diabas").getValue()).toString());
	    
	    if (diaBase < 1) diaBase = 1;
	    if (diaBase > 30) diaBase = 30;
	    prazoAprovacao.set(Calendar.DAY_OF_MONTH, diaBase);
	    prazoAprovacao.add(Calendar.DAY_OF_MONTH, -2);

	    while (prazoAprovacao.compareTo(dataAtual) < 0) {
	    	prazoAprovacao.add(Calendar.MONTH, 1);
	    }
	} else {
		prazoAprovacao.add(Calendar.MONTH, 1);
	}
	while (!OrsegupsUtils.isWorkDay(prazoAprovacao)) {
		prazoAprovacao.add(Calendar.DAY_OF_MONTH, -1);
	}
    processEntity.setValue("prazoAprovacao", prazoAprovacao);
  }
}