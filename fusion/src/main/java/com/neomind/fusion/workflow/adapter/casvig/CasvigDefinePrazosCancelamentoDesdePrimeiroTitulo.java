package com.neomind.fusion.workflow.adapter.casvig;

import java.util.GregorianCalendar;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class CasvigDefinePrazosCancelamentoDesdePrimeiroTitulo implements AdapterInterface
{
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		GregorianCalendar dataInicial = (GregorianCalendar) processEntity.findField("dataVencPrimeiroTitulo").getValue();

		GregorianCalendar dataDez = (GregorianCalendar) dataInicial.clone();
		GregorianCalendar dataVinte = (GregorianCalendar) dataInicial.clone();
		GregorianCalendar dataTrinta = (GregorianCalendar) dataInicial.clone();

		dataDez.add(5, 40);
		dataVinte.add(5, 50);
		dataTrinta.add(5, 53);

		processEntity.setValue("dataSistema40dias", dataDez);
		processEntity.setValue("dataSistema50dias", dataVinte);
		processEntity.setValue("dataSistema60dias", dataTrinta);
	}
}