package com.neomind.fusion.workflow.adapter.reciclagem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.DefaultConverter;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;
//com.neomind.fusion.workflow.adapter.reciclagem.AcessoColaboradorConverter
public class AcessoColaboradorConverter extends DefaultConverter {

	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin){
		
		String retorno = "";
				
		try{
			String strNumCPF =  NeoUtils.safeOutputString( field.getForm().getField("numcpf").getValue() );
			
			retorno +=  getUltimoAcessoColaborador(strNumCPF);
			
			return retorno;
		}catch(Exception e){
			System.out.println("Erro ao converter acesso do colaborador na tarefa de reciclagem");
			e.printStackTrace();
			return retorno;
		}
	}	

	public Object getValueFromHTMLString(EFormField field, OriginEnum origin){
		return getHTMLInput(field, origin);
	}

	
	private static String getUltimoAcessoColaborador(String numcpf){
		String retorno = "";
		StringBuilder sql = new StringBuilder();
		
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		sql.append(" SELECT top 1 acc.DatAcc, acc.HorAcc, DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) as data, acc.dirAcc, acc.codrlg, fun.numcad, fun.numemp, fun.tipcol, jma.DesJMa, acc.usu_obsjma, acc.TipAcc, tac.DesTAc FROM R070ACC acc "); 
		sql.append(" inner join R070TAC tac on (acc.tipacc = tac.TipAcc) "); 
		sql.append(" left join r076jma jma on jma.CodJMa = acc.usu_codjma "); 
		sql.append(" INNER JOIN R038HCH hch ON hch.NumCra = acc.NumCra "); 
		sql.append(" INNER JOIN R034FUN fun ON fun.NumEmp = hch.NumEmp AND fun.NumCad = hch.NumCad AND fun.TipCol = hch.TipCol "); 
		sql.append(" WHERE acc.CodPlt = 2 AND (acc.TipAcc >= 100 or (acc.TipAcc = 1 and acc.oriacc = 'R')) AND acc.DatAcc >= getdate()-60 AND acc.DatAcc <= getdate() "); 
		sql.append(" and fun.NumCpf = ? ");
		sql.append(" GROUP BY acc.DatAcc, acc.HorAcc, acc.dirAcc, acc.codrlg, fun.numcad, fun.numemp, fun.tipcol, jma.DesJMa, acc.usu_obsjma, acc.TipAcc, tac.DesTAc ");  
		sql.append(" ORDER BY  acc.DatAcc desc,acc.HorAcc desc ");
		
		try{
			
			con = OrsegupsUtils.getConnection("VETORH");
			
			pst = con.prepareStatement(sql.toString());
			pst.setString(1, numcpf);
			
			rs = pst.executeQuery();
			
			if (rs.next()){
				GregorianCalendar gc = new GregorianCalendar();
				gc.setTimeInMillis(rs.getTimestamp("data").getTime());
				retorno += NeoDateUtils.safeDateFormat(gc) + " - " + ( NeoUtils.safeOutputString(rs.getString("dirAcc")).equals("E")? "Entrada":"Saída" );
			}
			
			return retorno;
		}catch(Exception e){
			e.printStackTrace();
			return retorno;
		}finally{
			OrsegupsUtils.closeConnection(con, pst, rs);
		}
		
	}
	
}
