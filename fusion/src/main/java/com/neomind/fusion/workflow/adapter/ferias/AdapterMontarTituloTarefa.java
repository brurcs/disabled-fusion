package com.neomind.fusion.workflow.adapter.ferias;

import java.util.Date;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class AdapterMontarTituloTarefa implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		NeoObject colaborador = (NeoObject) processEntity.findField("primeiraSugestao").getValue();
		EntityWrapper wColaborador = new EntityWrapper(colaborador);

		long numemp = (long) wColaborador.findField("numemp").getValue();
		long numcad = (long) wColaborador.findField("numcad").getValue();
		String nomReg = String.valueOf(wColaborador.getValue("usu_nomreg"));
		String nomFun = String.valueOf(wColaborador.findField("nomfun").getValue());

		String tituloFluxo = numemp + "/" + numcad + " - " + nomFun + " - " + nomReg;
		processEntity.setValue("tituloFluxo", tituloFluxo);
		long key = new Date().getTime();
		try{
		    NeoObject oTarefaSimples = AdapterUtils.createNewEntityInstance("Tarefa");
		    EntityWrapper wrpTarefaSimples = new EntityWrapper(oTarefaSimples);
		    
		    String titulo = "Tarefa Escalada Fluxo de Férias";
		    String descricao = "<strong>Código:</strong> " + activity.getCode() + "<br>";
		    descricao = descricao + "<strong>Colaborador:</strong> " + nomFun + "<br>";
		    descricao = descricao + "<strong>Matrícula:</strong> " + numcad + "<br>";
		    descricao = descricao + "<strong>Empresa:</strong> " + numemp + "<br>";
		    descricao = descricao + "<strong>Regional:</strong> " + nomReg + "<br>";
		    
		    wrpTarefaSimples.setValue("Titulo", titulo);		
		    wrpTarefaSimples.setValue("DescricaoSolicitacao", NeoUtils.encodeHTMLValue(descricao));
		    
		    processEntity.setValue("tarefaSimples", oTarefaSimples);
		} catch (Exception e) {
		    e.printStackTrace();
		    System.out.println("ERRO ["+key+"] ao inserir preenchimento tarefa simples: " + e);
		    throw new WorkflowException("ERRO ["+key+"] ao inserir preenchimento tarefa simples: " + e);
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}
}
