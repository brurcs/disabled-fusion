package com.neomind.fusion.workflow.Job.Job;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class EnviaEmailRelacaoJobs implements CustomJobAdapter

{
	@Override
	public void execute(CustomJobContext ctx)
	{
		System.out.println("[JOB] - Inicia Relação de Job's");
		processaJob(ctx);
	}

	public static void processaJob(CustomJobContext ctx)
	{
		try
		{
			encaminharEmail();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void encaminharEmail() throws EmailException
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		conn = PersistEngine.getConnection("");

		try
		{
			sql.append(" Select ");
			sql.append(" Case  ");
			sql.append(" When sch.schedulerType = 1 Then 'Por Hora'  ");
			sql.append(" When sch.schedulerType = 2 Then 'Todos os Dias da Semana'  ");
			sql.append(" When sch.schedulerType = 3 Then 'Diário'  ");
			sql.append(" When sch.schedulerType = 4 Then 'Semanal'  ");
			sql.append(" When sch.schedulerType = 5 Then 'Mensal'  ");
			sql.append(" When sch.schedulerType = 6 Then 'Anual'  ");
			sql.append(" When sch.schedulerType = 7 Then 'Por Minuto'  ");
			sql.append(" End As repeticao, ");
			sql.append(" Case  ");
			sql.append(" When log.state = 6 Then 'JOB_RESUMED'  ");
			sql.append(" When log.state = 7 Then 'JOB_WASEXECUTED_EXCEPTION'  ");
			sql.append(" When log.state = 8 Then 'JOB_EXECUTION_EXCEPTION'  ");
			sql.append(" When log.state = 7 Then 'TRIGGER_MISFIRED' ");
			sql.append(" else ''  ");
			sql.append(" End As error, job.name, log.details,isnull(CONVERT(VARCHAR(10), job.prevFireTime, 103) + ' ' + CONVERT(VARCHAR(8), job.prevFireTime, 108), '') as prevFireTime, ");
			sql.append(" 									 isnull(CONVERT(VARCHAR(10), job.nextFireTime, 103) + ' ' + CONVERT(VARCHAR(8), job.nextFireTime, 108), '') as nextFireTime ");
			sql.append(" From Job job With (NOLOCK)  ");
			sql.append(" Inner join dbo.JobEventLog log With (NOLOCK) on log.job_neoId = job.neoId  ");
			sql.append(" Inner join dbo.NeoTrigger tri With (NOLOCK) on tri.neoId = log.trigger_neoId  ");
			sql.append(" Inner join dbo.Scheduler sch With (NOLOCK) on sch.neoid = tri.scheduler_neoId  ");
			sql.append(" Where  ");
			sql.append(" log.firedTime >= DATEADD(HH,-1,GETDATE()) And log.firedTime <= GETDATE() ");
			sql.append(" group by schedulerType,state,job.name,log.details,job.prevFireTime,job.nextFireTime ");
			sql.append(" Order By schedulerType,state,job.name,log.details,job.prevFireTime,job.nextFireTime ");
			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();
			String nomeJob = "";
			String ultExecucao = "";
			String proExecucao = "";
			String periodicidade = "";
			String erro = "";
			String tarefa = "";

			StringBuilder html = new StringBuilder();
			html.append("<html>");
			html.append("<center><h1> Relação de Job's executados na ultima hora </h1></center>");
			html.append("<br/><br/>");
			html.append(" Segue relação de job's executadas na ultima hora: ");
			html.append("<br/><br/>");
			html.append("<TABLE BORDER=1 style=\"table-layout: fixed; width: 1200px; overflow: hidden;\">");
			html.append("<TR>");
			html.append("<TD WIDTH=15%>NOME DO JOB </TD>");
			html.append("<TD WIDTH=7%>ULTIMA EXECUÇÃO</TD>");
			html.append("<TD WIDTH=7%>PROXIMA EXECUÇÃO </TD>");
			html.append("<TD WIDTH=7%>PERIODICIDADE</TD>");
			html.append("<TD WIDTH=12%>ERRO</TD>");
			html.append("</TR>");

			while (rs.next())
			{
				nomeJob = rs.getString("name");
				ultExecucao = rs.getString("prevFireTime");
				proExecucao = rs.getString("nextFireTime");
				periodicidade = rs.getString("repeticao");
				erro = rs.getString("error");

				html.append("<TR>");
				html.append("<TD>" + nomeJob + "</TD>");
				html.append("<TD>" + rs.getString("prevFireTime") + "</TD>");
				html.append("<TD>" + rs.getString("nextFireTime") + "</TD>");
				html.append("<TD>" + periodicidade + "</TD>");
				html.append("<TD>" + erro + "</TD>");
				html.append("</TR>");
			}
			html.append("</TABLE>");
			html.append("</html>");

			String emailDst = "thiago.coelho@orsegups.com.br";
			String emailRemetente = "fusion@orsegups.com.br";
			List<String> comCopiaOc = new ArrayList<String>();
			comCopiaOc.add("jorge.filho@orsegups.com.br");
			OrsegupsUtils.sendEmail2Orsegups(emailDst, "fusion@orsegups.com.br", "Relação de Job's", "", html.toString(), emailRemetente, null, comCopiaOc);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
