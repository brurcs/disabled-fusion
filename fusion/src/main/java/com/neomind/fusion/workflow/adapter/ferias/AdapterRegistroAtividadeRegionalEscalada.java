package com.neomind.fusion.workflow.adapter.ferias;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class AdapterRegistroAtividadeRegionalEscalada implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		List<NeoObject> registroAtividades = (List<NeoObject>) processEntity.getValue("r001RegistroAtividade");
		
		InstantiableEntityInfo insRegAti = AdapterUtils.getInstantiableEntityInfo("r001RegistroAtividades");
		NeoObject objRegAti = insRegAti.createNewInstance();
		EntityWrapper wRegAti = new EntityWrapper(objRegAti);

		String responsavel = origin.returnResponsible();

		GregorianCalendar dataAcao = new GregorianCalendar();
		GregorianCalendar prazo = (GregorianCalendar) processEntity.getValue("prazoReciboRegional");

		wRegAti.setValue("usuario", responsavel);
		wRegAti.setValue("dataAcao", dataAcao);
		wRegAti.setValue("descricao", "Tarefa escalada devido não ter sido atendida no prazo de "+NeoDateUtils.safeDateFormat(prazo,"dd/MM/yyyy"));
		
		PersistEngine.persist(objRegAti);
		registroAtividades.add(objRegAti);

		processEntity.setValue("r001RegistroAtividade", registroAtividades);
		processEntity.setValue("anexoCMEReg", null);
		System.out.println();
        	NeoUser resp = (NeoUser) processEntity.getValue("responsavelRegional");
        	if (resp.getGroup().getGroupLevel() != 1000) {
            	    NeoPaper papelResSup = resp.getGroup().getResponsible();
            	    NeoUser novoUsuario = null;
            	    for (NeoUser u : papelResSup.getUsers()) {
            		novoUsuario = u;
            		break;
            	    }
            
            	    if (resp == novoUsuario) {
            		NeoPaper papelResSupGroup = resp.getGroup().getUpperLevel().getResponsible();
            		for (NeoUser u : papelResSupGroup.getUsers()) {
            		    novoUsuario = u;
            		    break;
            		}
             	    }
            
            	    processEntity.setValue("responsavelRegional", novoUsuario);
            	}

	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
