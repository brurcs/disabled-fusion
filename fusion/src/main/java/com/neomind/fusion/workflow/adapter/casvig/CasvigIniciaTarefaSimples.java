package com.neomind.fusion.workflow.adapter.casvig;

import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.fusion.workflow.simulation.WorkflowService;
import com.neomind.util.NeoDateUtils;

public class CasvigIniciaTarefaSimples implements AdapterInterface
{

	private static final Log log = LogFactory.getLog(CasvigIniciaTarefaSimples.class);

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		QLEqualsFilter equal = new QLEqualsFilter("Name", "G001 - Tarefa Simples");
		ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);

		NeoUser usuario = PortalUtil.getCurrentUser();

		/**
		 * @author neomind willian.mews - Alterado para utilizar a nova arquitetura de processos do
		 *         Fusion
		 * @date 11/03/2015
		 */

		/*final WFProcess processo = WorkflowService.startProcess(processModel, false, usuario);
		final NeoObject formularioProcesso = processo.getEntity();*/
		
		InstantiableEntityInfo objFormPrincipal = AdapterUtils.getInstantiableEntityInfo("tarefa");
		final NeoObject formularioProcesso = objFormPrincipal.createNewInstance();
		
		final EntityWrapper formularioWrapper = new EntityWrapper(formularioProcesso);

		if (activity.getProcess().getModel().getName().equals("C005 - Distribuição de RMC") || activity.getProcess().getModel().getName().equals("C027 - RSC - Relatório de Solicitação de Cliente Novo") || activity.getProcess().getModel().getName().equals("C027.001 - RSC - Categoria Diversos") || activity.getProcess().getModel().getName().equals("C027.004 - RSC - Categoria Atualização Cadastral")
				|| activity.getProcess().getModel().getName().equals("C027.005 - RSC - Categoria Alteração Boleto NF") || activity.getProcess().getModel().getName().equals("C027.002 - RSC - Categoria Orçamento") || activity.getProcess().getModel().getName().equals("C027.003 - RSC - Categoria Cancelamento") || activity.getProcess().getModel().getName().equals("C027.099 - RSC - Verificação de Eficácia")
				|| activity.getProcess().getModel().getName().equals("C033 - CSE - Cobrança Serviço Extra"))
		{
			//copia os campos do eForm
			formularioWrapper.findField("Solicitante").setValue(processEntity.findValue("tarefaSimples.Solicitante"));
			formularioWrapper.findField("Executor").setValue(processEntity.findValue("tarefaSimples.Executor"));
			formularioWrapper.findField("Titulo").setValue(processEntity.findValue("tarefaSimples.Titulo"));
			formularioWrapper.findField("DescricaoSolicitacao").setValue(processEntity.findValue("tarefaSimples.DescricaoSolicitacao"));
			formularioWrapper.findField("AnexoSolicitante").setValue(processEntity.findValue("tarefaSimples.AnexoSolicitante"));
			formularioWrapper.findField("Prazo").setValue(processEntity.findValue("tarefaSimples.Prazo"));
			formularioWrapper.findField("codigoProcesso").setValue(activity.getProcess().getNeoId());
			
			String tarefa = OrsegupsWorkflowHelper.iniciaProcesso(processModel, formularioProcesso, false, usuario, true, usuario);

			if (activity.getProcess().getModel().getName().equals("C027 - RSC - Relatório de Solicitação de Cliente Novo") && ((String) processEntity.findValue("")) != null)
			{
				String descricao = (String) processEntity.findValue("obsSolicitacao");
				descricao = descricao + " Aberto a Tarefa Simples " + tarefa;
				processEntity.findField("obsSolicitacao").setValue(descricao);
			}

			if (activity.getProcess().getModel().getName().equals("C027.001 - RSC - Categoria Diversos") && ((String) processEntity.findValue("providenciasTomadas")) != null)
			{
				String descricao = (String) processEntity.findValue("providenciasTomadas");
				descricao = descricao + " Aberto a Tarefa Simples " + tarefa;
				processEntity.findField("providenciasTomadas").setValue(descricao);
			}

			if (activity.getProcess().getModel().getName().equals("C027.004 - RSC - Categoria Atualização Cadastral") && ((String) processEntity.findValue("providenciasTomadas")) != null)
			{
				String descricao = (String) processEntity.findValue("providenciasTomadas");
				descricao = descricao + " Aberto a Tarefa Simples " + tarefa;
				processEntity.findField("providenciasTomadas").setValue(descricao);
			}

			if (activity.getProcess().getModel().getName().equals("C027.005 - RSC - Categoria Alteração Boleto NF") && ((String) processEntity.findValue("providenciasTomadas")) != null)
			{
				String descricao = (String) processEntity.findValue("providenciasTomadas");
				descricao = descricao + " Aberto a Tarefa Simples " + tarefa;
				processEntity.findField("providenciasTomadas").setValue(descricao);
			}

			if (activity.getProcess().getModel().getName().equals("C027.002 - RSC - Categoria Orçamento") && ((String) processEntity.findValue("observacao")) != null)
			{
				String descricao = (String) processEntity.findValue("observacao");
				descricao = descricao + " Aberto a Tarefa Simples " + tarefa;
				processEntity.findField("observacao").setValue(descricao);
			}

			if (activity.getProcess().getModel().getName().equals("C027.003 - RSC - Categoria Cancelamento") && ((String) processEntity.findValue("observacao")) != null)
			{
				String descricao = (String) processEntity.findValue("observacao");
				descricao = descricao + " Aberto a Tarefa Simples " + tarefa;
				processEntity.findField("observacao").setValue(descricao);
			}
		}
		else
		{
			//copia os campos do eForm
			formularioWrapper.findField("Solicitante").setValue(processEntity.findValue("formTarefaSimples.Solicitante"));
			formularioWrapper.findField("Executor").setValue(processEntity.findValue("formTarefaSimples.Executor"));
			formularioWrapper.findField("Titulo").setValue(processEntity.findValue("formTarefaSimples.Titulo"));
			formularioWrapper.findField("DescricaoSolicitacao").setValue(processEntity.findValue("formTarefaSimples.DescricaoSolicitacao"));
			formularioWrapper.findField("AnexoSolicitante").setValue(processEntity.findValue("formTarefaSimples.AnexoSolicitante"));

			GregorianCalendar dataPrazo = (GregorianCalendar) processEntity.findValue("formTarefaSimples.Prazo");
			GregorianCalendar prazo = new GregorianCalendar();
			System.out.println("Prazo da tarefa simples: " + NeoDateUtils.safeDateFormat(dataPrazo));
			if (OrsegupsUtils.isWorkDay(dataPrazo))
			{
				prazo = (GregorianCalendar) dataPrazo.clone();
			}
			else
			{
				prazo = OrsegupsUtils.getNextWorkDay(dataPrazo);
				System.out.println("Não é dia util. Processando próxima data util:" + NeoDateUtils.safeDateFormat(prazo));
			}
			formularioWrapper.findField("Prazo").setValue(prazo);
			formularioWrapper.findField("codigoProcesso").setValue(activity.getProcess().getNeoId());
			
			String tarefa = OrsegupsWorkflowHelper.iniciaProcesso(processModel, formularioProcesso, false, usuario, true, usuario);
		}

		/*try
		{
			new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividade(usuario);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro #8 - Erro ao iniciar a primeira atividade do processo.");
		}
*/
		/**
		 * FIM ALTERAÇÕES - NEOMIND
		 */
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}

}