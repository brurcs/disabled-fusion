package com.neomind.fusion.workflow.adapter.casvig;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.mobile.engine.CasvigMobilePool;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.CustomActivityAdapter;

/**
 * 
 * @author Daniel Henrique Joppi
 * 
 */
public class CasvigMobilePararAtividade implements AdapterInterface
{
	@SuppressWarnings("unused")
	private static final Log log = LogFactory.getLog(CasvigMobileDistribuirAtividadeInicial.class);

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		
		activity.getProcess().setSaved(true);
		
		CasvigMobilePool pool = new CasvigMobilePool(activity, origin);
		
		System.out.println("[INSPETORIA MOBILE] - tarefa = "+ activity.getProcess().getCode() + " taskName = "  + activity.getModel().getName() );
		
		if (activity.getModel().getName().equals("Reinspecionar")){
			pool.setIsReinspection(true);
		}else{
			// Verificar Eficácia
			pool.setIsEfficacy(true);
		}
		
		NeoObject entity = activity.getProcess().getEntity();
		EntityWrapper wrapper = new EntityWrapper(entity);
		
		NeoPaper paper = (NeoPaper) wrapper.getValue("MobilePoolUsers");
		
		List<NeoUser> user = new ArrayList<NeoUser>();
		
		if(paper != null)
		{
			System.out.println("[INSPETORIA MOBILE] - campo MobilePoolUsers não é nulo. ");
			Set<NeoUser> hashUser = (Set<NeoUser>) paper.getAllUsers();
			
			for (NeoUser neoUser : hashUser)
			{
				user.add(neoUser);
			}
		}
						
		// senão exixstir nenhum elemento na lista 
		// adiciona o inspetor que enviou a inspeção.
		if(user.size() == 0)
		{
			System.out.println("[INSPETORIA MOBILE] - adicionar o inspetor que enviou a inspeção. ");
			user.add(activity.getProcess().getRequester());
		}
		
		
		pool.setUsers(user);
		System.out.println("[INSPETORIA MOBILE] - setando para envio ao mobile ");
		pool.setSendToMobile(true);
		
		PersistEngine.persist(pool);

		System.out.println("[INSPETORIA MOBILE] - Mobile - stop activity -|- Activity = " + String.valueOf(activity.getNeoId()) + " - Task = "
				+ String.valueOf(origin.getNeoId()));
		
		/*if (processEntity.findValue("dataEnvioReinspecao") == null){ // se a reinspeção ainda nao foi enviada, manter activity aberta
			((CustomActivityAdapter) activity).setKeepOpen(true);
		}*/
		
	}
	
	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}
}