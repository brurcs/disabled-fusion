package com.neomind.fusion.workflow.adapter.casvig;

import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.model.ProcessModel;

public class CasvigDistribuiValidar implements AdapterInterface
{
	@SuppressWarnings("unused")
	private static final Log log = LogFactory.getLog(CasvigDistribuiValidar.class);

	@Override
	public void start(Task origin, EntityWrapper wrapperProcessoPlanoAcao, Activity activity)
	{
		if (wrapperProcessoPlanoAcao != null)
		{
		    /* Instância um processModel para o subprocesso "Validar Tarefa Plano Ação" */
			QLEqualsFilter equal = new QLEqualsFilter("Name", "Validar Tarefa Plano Ação"); 
			ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);

			/* planoList coleta cada uma das linhas do eform PlanoAcao que corresponde ao ProcessoPlanoAcao */
			Collection planoList = (Collection) wrapperProcessoPlanoAcao.findValue("PlanoAcao");
			
			for (Object oPlano : planoList)
			{	
				//FIXME NEOMIND Alterar para o novo padão
				Activity atividade = null;
				
				/* Busca a referência para proxíma atividade */
				
				/* Insere a próxima atividade (referência) no eform do subProcesso que será iniciado */
				EntityWrapper wrapperPlanoAcao = new EntityWrapper((NeoObject)oPlano);
				wrapperPlanoAcao.setValue("NextActivityLink", atividade);
			  
				if(!wrapperPlanoAcao.findValue("Solicitante.code").toString().equalsIgnoreCase((((NeoUser)(wrapperPlanoAcao.findValue("Quem"))).getGroup().getResponsible().getUsers().toArray()[0].toString()))) 
				{					
					/*Inicia um novo processo */				
					OrsegupsWorkflowHelper.iniciaProcesso(processModel, (NeoObject) oPlano, false, PortalUtil.getCurrentUser());
				}
			}
		}
	}
	
	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}
}