package com.neomind.fusion.workflow.adapter.casvig;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

//com.neomind.fusion.workflow.adapter.casvig.RegistraHistoricoAtividade
public class RegistraHistoricoAtividade implements AdapterInterface
{

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		String usuarioLogado = PortalUtil.getCurrentUser().getCode();

		if (processEntity.getValue("obsEscalada") != null)
		{
			String obsEscalada = (String) processEntity.getValue("obsEscalada");
			InstantiableEntityInfo registro = AdapterUtils.getInstantiableEntityInfo("IMRegistroAtividade");
			NeoObject objRegistro = registro.createNewInstance();
			EntityWrapper wRegistro = new EntityWrapper(objRegistro);
			GregorianCalendar datAtu = new GregorianCalendar();
			wRegistro.setValue("observacao", obsEscalada);
			System.out.println(NeoDateUtils.safeDateFormat(datAtu, "dd/MM/yyyy HH:mm:ss"));
			wRegistro.setValue("dataRegistro", datAtu);
			wRegistro.setValue("usuario", usuarioLogado);
			PersistEngine.persist(objRegistro);
			List<NeoObject> registros = (List<NeoObject>) processEntity.getValue("registroAtividade2");
			registros.add(objRegistro);
			processEntity.setValue("registroAtividade2", registros);

			processEntity.setValue("obsEscalada", "");
		}

	}

}
