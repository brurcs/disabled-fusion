package com.neomind.fusion.workflow.adapter.orsegups.batismo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.lowagie.text.Image;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.doc.pdf.FileNotConvertedException;
import com.neomind.fusion.doc.pdf.ListaPag;
import com.neomind.fusion.doc.pdf.PDFManager;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.exception.NeoConvertException;
import com.neomind.fusion.image.DocumentConverterCache;
import com.neomind.fusion.image.DocumentConverterSettings;
import com.neomind.fusion.image.JodeGenericImageConverter;
import com.neomind.fusion.persist.PersistEngine;

public class FileManager
{

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static File getDocumentsAsPDF(Collection<NeoFile> fileList, String identifier,
			String fileName, Boolean showFisrtPageNumber, NeoFile imagemAssinatura)
	{
		File pdf = null;

		Map pdfOptions = new HashMap();
		pdfOptions.put("EncryptFile", Boolean.FALSE);
		pdfOptions.put("RestrictPermissions", Boolean.TRUE);
		pdfOptions.put("Printing", new Integer(2));
		pdfOptions.put("Changes", new Integer(0));
		pdfOptions.put("EnableCopyingOfContent", Boolean.TRUE);
		pdfOptions.put("EnableTextAccessForAccessibilityTools", Boolean.TRUE);
		pdfOptions.put("path", NeoStorage.getDefault().getPath());
		pdfOptions.put("newFileName", "processo.pdf");

		List<File> documentsDestin = new ArrayList<File>();

		try
		{
			// converte para pdf todos os arquivos que se encontram na pasta do
			// anexo principal
			for (NeoFile neoFile : fileList)
			{
				if (neoFile != null)
				{
					validateNeoFileCache(neoFile, pdfOptions, documentsDestin);
				}
			}
			String finalDocumetDir = NeoStorage.getDefault().getPath() + File.separator + "export";

			File dir = new File(finalDocumetDir);
			if (!dir.exists())
			{
				dir.mkdir();
			}

			String finalDocumetPath = dir + File.separator + fileName + identifier + "_tmp.pdf";

			// concatena os arquivos em um único
			File processPdf = PDFManager.concatenatePDFs(documentsDestin, finalDocumetPath);

			// imprime a marca d'agua no documento
			String finalDocumetPath1 = dir + File.separator + fileName + identifier + ".pdf";
			File processPdfPageNumbers = new File(finalDocumetPath1);

			processPdfPageNumbers = printPDFIntervalPagesAndWaterMark(processPdf.getPath(),
					processPdfPageNumbers.getPath(), showFisrtPageNumber, imagemAssinatura);

			pdf = processPdfPageNumbers;
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

		return pdf;
	}

	@SuppressWarnings("rawtypes")
	private static void validateNeoFileCache(NeoFile file, Map pdfOptions, List<File> documentsDestin)
	{
		try
		{
			file = PersistEngine.reload(file);
			DocumentConverterCache cache = new DocumentConverterCache(); //DocumentConverterEngine.getInstance().getCache();
			File newFile = null;
			if (file != null)
			{
				//File aux = File.createTempFile("export", ".pdf");

				try
				{
					{
						newFile = cache.getCachedImageFile(file);
						if (newFile != null && newFile.exists())
						{
							//NeoDocUtils.copyFile(newFile.getAbsolutePath(), aux.getAbsolutePath());
							//documentsDestin.add(aux);
							documentsDestin.add(newFile);
						}
						else
						{
							try
							{
								DocumentConverterSettings settings = FusionRuntime.getInstance()
										.getSettings().getDocumentConverterSettings();
								//nem sempre Ã© um new file, pois quando Ã© passado um arquivo pdf ele retorna o proprio file
								newFile = convertDocumentToPdf(file.getAsFile(), file.getName(),
										pdfOptions, file.getNeoId(), settings.getCachePath());
							}
							catch (NeoConvertException e)
							{
								e.printStackTrace();
							}
							if (newFile != null && newFile.exists())
							{
								documentsDestin.add(newFile);

							}
						}
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public static File convertDocumentToPdf(final File source, String fileName,
			final Map<String, Object> pdfOptions, final long neoId, final String cachePath)
			throws Exception
	{
		File pdfFile = null;
		final ListaPag listapag = new ListaPag();
		if ((listapag.convertStringPaginas((String) pdfOptions.get("pages"))))
		{
			if (source != null)
			{
				// Verifica o tipo de documento de entrada para conversÃ£o
				fileName = fileName.toLowerCase().trim();
				final JodeGenericImageConverter jode = new JodeGenericImageConverter();
				final boolean usarMSOffice = false;

				if (fileName.endsWith("pdf"))
				{
					pdfFile = source;
				}

			}
			else
			{
				try
				{
					pdfFile = PDFManager.createNullPage(File.createTempFile("fusexp", ".pdf"));
				}
				catch (final IOException e)
				{
					e.printStackTrace();
				}
			}
		}

		return pdfFile;
	}

	@SuppressWarnings({ "unused", "rawtypes" })
	public static File printPDFIntervalPagesAndWaterMark(String pdfPathOrigin,
			String pdfPathDestination, Boolean showFisrtAssigned, NeoFile imagemAssinatura)
			throws NeoConvertException
	{
		Boolean showPageNumber = false;

		try
		{
			PdfReader reader = new PdfReader(pdfPathOrigin);
			int n = reader.getNumberOfPages();
			// we create a stamper that will copy the document to a new file
			PdfStamper stamp = new PdfStamper(reader, new FileOutputStream(pdfPathDestination));
			// adding some metadata
			HashMap moreInfo = new HashMap();

			stamp.setMoreInfo(moreInfo);
			// adding content to each page
			// ComeÃ§a no 1, porque a capa nÃ£o leva carimbo
			int i = 1;

			if (showFisrtAssigned)
			{
				i = 0;
			}

			PdfContentByte under;
			PdfContentByte over;

			BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.EMBEDDED);

			Image img = null;

			if (imagemAssinatura != null)
				img = Image.getInstance(imagemAssinatura.getAbsolutePath());

			while (i < n)
			{
				i++;
				Rectangle psize = reader.getPageSize(i);
				float width = psize.getWidth();
				float height = psize.getHeight();

				//Caso exista uma imagem valida de assinatura
				if (img != null)
				{
					//posiÃ§Ã£o da assinatura
					img.setAbsolutePosition(width - 220, 20);

					// watermark under the existing page
					under = stamp.getOverContent(i);

					//edgar adiciona a imagem de assinatura
					under.addImage(img);
				}

				// text over the existing page

			}
			stamp.close();
			return new File(pdfPathDestination);

		}
		catch (Exception de)
		{
			de.printStackTrace();
			throw new FileNotConvertedException(de);
		}
	}
}
