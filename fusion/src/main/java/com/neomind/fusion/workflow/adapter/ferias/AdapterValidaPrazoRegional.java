package com.neomind.fusion.workflow.adapter.ferias;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.presenca.vo.HistoricoLocalVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class AdapterValidaPrazoRegional implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		List<NeoObject> registroAtividades = (List<NeoObject>) processEntity.getValue("r001RegistroAtividade");
		String observacao = "";
		if (origin.getActivityName().toString().contains("Informar Colaborador de Férias"))
		{
//			if (processEntity.findField("prazoTarefaregional").getValue() == null)
//			{
				GregorianCalendar prazo = new GregorianCalendar();
				prazo.add(GregorianCalendar.DATE, 2);
				if (!OrsegupsUtils.isWorkDay(prazo))
				{
					prazo = OrsegupsUtils.getNextWorkDay(prazo);
				}
				prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
				prazo.set(GregorianCalendar.MINUTE, 59);
				prazo.set(GregorianCalendar.SECOND, 59);
				processEntity.setValue("prazoTarefaregional", prazo);

//			}

			observacao = String.valueOf(processEntity.getValue("obsCm"));
			processEntity.setValue("obsCm", "");

			NeoObject colaborador = (NeoObject) processEntity.getValue("primeiraSugestao");
			EntityWrapper wColaborador = new EntityWrapper(colaborador);
			long codreg = (long) wColaborador.findField("usu_codreg").getValue();
			long numemp = (long) wColaborador.findField("numemp").getValue();
			long numcad = (long) wColaborador.findField("numcad").getValue();
			long tipcol = (long) wColaborador.findField("tipcol").getValue();

			GregorianCalendar datAtu = new GregorianCalendar();
			HistoricoLocalVO lotacao = QLPresencaUtils.listaHistoricoLocal(numemp, tipcol, numcad, NeoDateUtils.safeDateFormat(datAtu, "yyyy-MM-dd"));
			NeoPaper coordernador = OrsegupsUtils.getPapelResponsavelJ002ReciboFerias(codreg, lotacao.getPosto().getNomePosto());
			NeoUser responsavelRegional = null;
			for (NeoUser usr : coordernador.getUsers())
			{
				responsavelRegional = usr;
				break;
			}
			processEntity.findField("responsavelRegional").setValue(responsavelRegional);
		}
		else if (origin.getActivityName().toString().contains("Validar Recibo de férias"))
		{
			observacao = String.valueOf(processEntity.getValue("obsCMComunicadoFerias"));
			processEntity.setValue("obsCMComunicadoFerias", "");
			GregorianCalendar prazo = new GregorianCalendar();
			prazo.add(GregorianCalendar.DATE, 1);
			while (!OrsegupsUtils.isWorkDay(prazo))
			{
				prazo = OrsegupsUtils.getNextWorkDay(prazo);
			}
			prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
			prazo.set(GregorianCalendar.MINUTE, 59);
			prazo.set(GregorianCalendar.SECOND, 59);

			processEntity.setValue("prazoReciboRegional", prazo);

		}
		else if (origin.getActivityName().toString().contains("Anexar Recibo de Férias"))
		{
			if (processEntity.getValue("obsRegComunicado") != null)
			{
				observacao = String.valueOf(processEntity.getValue("obsRegComunicado"));
			}
			else
			{
				observacao = "<i>Sem observações</i>";
			}
			processEntity.setValue("obsRegComunicado", "");
		}
		else if (origin.getActivityName().toString().contains("Informar dias de Férias"))
		{
			if (processEntity.getValue("obsCMparaCm") != null)
			{
				observacao = String.valueOf(processEntity.getValue("obsCMparaCm"));
			}
			else
			{
				observacao = "<i>Sem observações</i>";
			}
			processEntity.setValue("obsCMparaCm", "");
			processEntity.setValue("encaminharRH", true);
		}
		else
		{

			observacao = String.valueOf(processEntity.getValue("obsregional"));
			processEntity.setValue("obsregional", "");
			processEntity.setValue("destinoRegional", false);
		}

		NeoFile anexo = null;
		if (processEntity.getValue("anexoCMEReg") != null)
		{
			anexo = (NeoFile) processEntity.getValue("anexoCMEReg");
		}

		InstantiableEntityInfo insRegAti = AdapterUtils.getInstantiableEntityInfo("r001RegistroAtividades");
		NeoObject objRegAti = insRegAti.createNewInstance();
		EntityWrapper wRegAti = new EntityWrapper(objRegAti);

		String responsavel = origin.returnResponsible();

		GregorianCalendar dataAcao = new GregorianCalendar();

		wRegAti.setValue("usuario", responsavel);
		wRegAti.setValue("dataAcao", dataAcao);
		wRegAti.setValue("descricao", observacao);
		wRegAti.setValue("anexo", anexo);

		PersistEngine.persist(objRegAti);
		registroAtividades.add(objRegAti);

		processEntity.setValue("r001RegistroAtividade", registroAtividades);
		processEntity.setValue("anexoCMEReg", null);

	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
