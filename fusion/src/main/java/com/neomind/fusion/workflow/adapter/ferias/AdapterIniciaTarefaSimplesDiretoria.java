package com.neomind.fusion.workflow.adapter.ferias;

import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class AdapterIniciaTarefaSimplesDiretoria implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		NeoObject objTarSim = (NeoObject) processEntity.getValue("tarefaSimples");
		EntityWrapper wTarSim = new EntityWrapper(objTarSim);

		GregorianCalendar prazo = (GregorianCalendar) wTarSim.getValue("Prazo");
		GregorianCalendar datAtu = new GregorianCalendar();
		if (!OrsegupsUtils.isWorkDay(prazo))
		{
			throw new WorkflowException("O Prazo da tarefa precisa ser um dia util.");
		}
		prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
		prazo.set(GregorianCalendar.MINUTE, 59);
		prazo.set(GregorianCalendar.SECOND, 59);
		prazo.set(GregorianCalendar.MILLISECOND, 0);

		datAtu.set(GregorianCalendar.HOUR_OF_DAY, 23);
		datAtu.set(GregorianCalendar.MINUTE, 59);
		datAtu.set(GregorianCalendar.SECOND, 59);
		datAtu.set(GregorianCalendar.MILLISECOND, 0);

		if (prazo.getTimeInMillis() <= datAtu.getTimeInMillis())
		{
			throw new WorkflowException("O Prazo da tarefa deve ser no minimo um dia util.");
		}

		String descricao = String.valueOf(wTarSim.getValue("DescricaoSolicitacao"));
		NeoUser executorNeo = (NeoUser) wTarSim.getValue("Executor");
		String executor = executorNeo.getCode();
		String solicitante = "Dilmoberger";
		String titulo = String.valueOf(wTarSim.getValue("Titulo"));
		NeoFile anexo = null;
		if (wTarSim.getValue("AnexoSolicitante") != null)
		{
			anexo = (NeoFile) wTarSim.getValue("AnexoSolicitante");
		}

		IniciarTarefaSimples tarefaSimples = new IniciarTarefaSimples();
		String code = tarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao.toString(), "1", "Sim", prazo, anexo);
		System.out.println("code: " + code);
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
