package com.neomind.fusion.workflow.adapter.casvig;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.Entity;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.DeadLine;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.handler.ActivityHandler;
import com.neomind.fusion.workflow.handler.HandlerFactory;

public class CasvigRetornaCheckPoint implements AdapterInterface
{	
	@SuppressWarnings("unused")
	private static final Log log = LogFactory.getLog(CasvigRetornaCheckPoint.class);
	
	@Override
	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{	
		/* Ativa a atividade Check Point Distribuição */
	    NeoObject atividade = (NeoObject) wrapper.getValue("NextActivityLink");
		Activity a = (Activity) atividade;
		
		ActivityHandler handler2 = HandlerFactory.ACTIVITY.get(this);
		
		Set<Activity> originList = new HashSet<Activity>();
        handler2.start(originList);
	}
	
	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}
}