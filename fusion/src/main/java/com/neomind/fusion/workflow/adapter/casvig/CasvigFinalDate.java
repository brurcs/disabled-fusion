package com.neomind.fusion.workflow.adapter.casvig;

import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class CasvigFinalDate implements AdapterInterface
{
	@SuppressWarnings("unused")
	private static final Log log = LogFactory.getLog(CasvigFinalDate.class);
	
	@Override
	public void start(Task origin, EntityWrapper wrapperTarefaSimples, Activity activity)
	{
		if (wrapperTarefaSimples != null)
		{
			wrapperTarefaSimples.setValue("dataConclusao", new GregorianCalendar());
			wrapperTarefaSimples.setValue("finalizado", true);
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}
}
