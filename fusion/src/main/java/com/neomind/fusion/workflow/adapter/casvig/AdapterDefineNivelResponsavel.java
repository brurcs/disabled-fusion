package com.neomind.fusion.workflow.adapter.casvig;

import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

//com.neomind.fusion.workflow.adapter.casvig.AdapterValidaPrazoAguardarEnvioReinspecao

public class AdapterDefineNivelResponsavel implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		if (processEntity.getValue("nivEscalada") == null)
		{
			processEntity.setValue("nivEscalada", 0L);
		}

		long nivEsc = (long) processEntity.getValue("nivEscalada");
		if (nivEsc > 4)
		{
			nivEsc = 0;
		}
		nivEsc = nivEsc + 1;
		processEntity.setValue("nivEscalada", nivEsc);
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
