package com.neomind.fusion.workflow.adapter.reciclagem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.e2doc.E2docIndexacaoBean;
import com.neomind.fusion.custom.orsegups.e2doc.E2docUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;

public class AdapterIndexacaoAutomatica implements AdapterInterface
{
    
    	private static final Log log = LogFactory.getLog(AdapterIndexacaoAutomatica.class);	

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity){
	    
	    try{
    		NeoFile certificado = (NeoFile) processEntity.findValue("anexoCertificado");
    		NeoFile comprovante = (NeoFile) processEntity.findValue("anexoComprovante");
    		NeoFile demissional = (NeoFile) processEntity.findValue("anexoDemissao");
    		GregorianCalendar dataCertificado = (GregorianCalendar) processEntity.getValue("dataCertificado");
    		GregorianCalendar dataComprovante = (GregorianCalendar) processEntity.getValue("dataComprovante");
    		GregorianCalendar dataDemissao = (GregorianCalendar) processEntity.getValue("dataDemissao");
    		
    		Boolean vigilanteAprovado = (Boolean) processEntity.getValue("vigilanteAprovado");
    		
    		if (vigilanteAprovado != null && vigilanteAprovado){
    		    indexar(processEntity, certificado, "CERTIFICADO DE FORMAÇÃO DE VIGILANTE E RECICLAGEM", dataCertificado);
    		    if (dataComprovante != null && comprovante != null){
    			indexar(processEntity, comprovante, "PROTOCOLOS", dataComprovante);
    		    }
    		}else{
    		    indexar(processEntity, demissional, "DEMISSIONAL", dataDemissao);
    		}
    		
	    }catch(Exception e){
		e.printStackTrace();
		log.error("##### ERRO AO indexar documento: " + e + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		throw new WorkflowException(e.getMessage());
	    }
	    
	}
	
	public void indexar(EntityWrapper processEntity, NeoFile arquivo, String tipoDocumento, GregorianCalendar gc){
	    try{
		if (arquivo != null){
		    // colaborador
		    E2docIndexacaoBean e2docIndexacaoBean = new E2docIndexacaoBean();
		    Long empresa = (Long) processEntity.findValue("numemp");
		    Connection conn = PersistEngine.getConnection("VETORH");
		    StringBuilder sql = new StringBuilder();
		    PreparedStatement pstm = null;
		    ResultSet rs = null;
			
		    try{
			sql.append("select nomemp from r030emp with(nolock) where numemp = ?");
			pstm = conn.prepareStatement(sql.toString());
			pstm.setLong(1,empresa);
			rs = pstm.executeQuery();
			while (rs.next()){
			    e2docIndexacaoBean.setEmpresa(rs.getString("nomemp")); 
			}
		    }catch (Exception e){
			e.printStackTrace();
		    }finally{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		    }
			
		    e2docIndexacaoBean.setMatricula(((Long) processEntity.findValue("numcad")).toString());
		    e2docIndexacaoBean.setColaborador((String) processEntity.findValue("nomfun"));
		    e2docIndexacaoBean.setCpf((Long) processEntity.findValue("numcpf"));	
		    
		    e2docIndexacaoBean.setTipoDocumento(tipoDocumento);
		    e2docIndexacaoBean.setClasse("outros");
		    e2docIndexacaoBean.setData(gc.getTime());
			
		    e2docIndexacaoBean.setObservacao(String.valueOf(Calendar.getInstance().getTimeInMillis()));
			
		    // requisição
		    e2docIndexacaoBean.setTipo("i");
		    e2docIndexacaoBean.setModelo("documentação rh");
		    e2docIndexacaoBean.setFormato(arquivo.getSufix());
		    e2docIndexacaoBean.setDocumento(arquivo.getBytes());
			
		    E2docUtils.indexarDocumento(e2docIndexacaoBean);
		}
	    }catch(Exception e){
		e.printStackTrace();
		log.error("##### ERRO AO indexar documento: Tipo de Documento:"+tipoDocumento+" | " + e + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		throw new WorkflowException(e.getMessage());
	    }
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity){

	}

}