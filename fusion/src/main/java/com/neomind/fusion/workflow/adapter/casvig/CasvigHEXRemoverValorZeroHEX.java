package com.neomind.fusion.workflow.adapter.casvig;

import java.util.ArrayList;
import java.util.Collection;
import java.util.StringTokenizer;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.dynamic.EntityCloner;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class CasvigHEXRemoverValorZeroHEX implements AdapterInterface
{
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		boolean impHex = true;
		try
		{
			Collection<NeoObject> hex = processEntity.findField("listaRegistro").getValues();
			ArrayList<NeoObject> novaHex = new ArrayList<NeoObject>();

			for (NeoObject no : hex)
			{
				NeoObject novoObject = EntityCloner.cloneNeoObject(no);
				EntityWrapper wrapperLista = new EntityWrapper(novoObject);
				Long numCad = (Long) wrapperLista.findField("colaborador.numcad").getValue();
				String hex50 = (String) wrapperLista.findField("he50").getValue();
				String he100 = (String) wrapperLista.findField("he100").getValue();

				int[] Array = (int[]) null;
				Array = new int[2];

				int valor = 0;

				int count = 0;
				StringTokenizer aux = new StringTokenizer(hex50, ":");
				while (aux.hasMoreTokens())
				{
					valor = Integer.parseInt(aux.nextToken());
					Array[count] = valor;
					count++;
				}
				int totHex50 = Array[0] * 60 + Array[1];

				count = 0;
				aux = new StringTokenizer(he100, ":");
				while (aux.hasMoreTokens())
				{
					valor = Integer.parseInt(aux.nextToken());
					Array[count] = valor;
					count++;
				}
				int totHe100 = Array[0] * 60 + Array[1];

				System.out.println("numCad=" + numCad + " totHex50=" + totHex50 + " totHe100=" + totHe100);

				if ((totHex50 <= 0) && (totHe100 <= 0))
					continue;
				PersistEngine.persist(novoObject);
				novaHex.add(novoObject);
			}

			if ((novaHex == null) || (novaHex.size() == 0))
			{
				impHex = false;
			}
			processEntity.setValue("impHex", Boolean.valueOf(impHex));
			processEntity.setValue("importarHex", novaHex);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}