package com.neomind.fusion.workflow.adapter.CNV;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class AbreTarefaSimplesEscaladaCNV implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		Long numEmp = (Long) processEntity.findValue("numEmp");
		Long numCad = (Long) processEntity.findValue("numCad");
		String nomFun = (String) processEntity.findValue("nomFun");

		String solicitante = "maurelio.pinto";
		String executor = "luana.martins";
		String titulo = "";
		String descricao = "";

		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);

		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();

		try
		{
			titulo = "Tarefa escalada do fluxo Q022 - Emissão/Renovação CNV com código " + activity.getCode() + " colaborador " + numEmp + "/" + numCad + " - " + nomFun;
			descricao = " Informar motivo de não ter sido atendido à tarefa " + activity.getCode() + " do fluxo de CNV referente ao colaborador " + numEmp + "/" + numCad + " - " + nomFun;
			String code = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "2", "sim", prazo);
			System.out.println("Code: "+code);
		}
		catch (Exception e)
		{
			try
			{
				throw new Exception("Erro ao tentar iniciar tarefa simples escalada, entrar em contato com a TI.");
			}
			catch (Exception e1)
			{
				e1.printStackTrace();
			}
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
