package com.neomind.fusion.workflow.adapter.ferias;

import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class AdapterPrazoReciboRegional implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		GregorianCalendar prazo = new GregorianCalendar();
		prazo.add(GregorianCalendar.DATE, 5);
	
		while (!OrsegupsUtils.isWorkDay(prazo))
		{
			prazo = OrsegupsUtils.getNextWorkDay(prazo);
		}
		prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
		prazo.set(GregorianCalendar.MINUTE, 59);
		prazo.set(GregorianCalendar.SECOND, 59);
		processEntity.setValue("prazoReciboRegional", prazo);
		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
