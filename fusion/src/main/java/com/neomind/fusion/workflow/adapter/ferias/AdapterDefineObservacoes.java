package com.neomind.fusion.workflow.adapter.ferias;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class AdapterDefineObservacoes implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		String obs = "Tarefa origem do Fluxo R001 - Programação de Férias com o seguinte código: " + origin.getCode() + ".";

		NeoObject colaborador2 = (NeoObject) processEntity.findField("primeiraSugestao").getValue();
		EntityWrapper wColaborador = new EntityWrapper(colaborador2);

		long numemp = (long) wColaborador.findField("numemp").getValue();
		long numcad = (long) wColaborador.findField("numcad").getValue();
		String nomFun = String.valueOf(wColaborador.findField("nomfun").getValue());
		String escala = String.valueOf(processEntity.getValue("escalaFer"));
		obs = obs + " Colaborador de férias: " + numemp + "/" + numcad + " - " + nomFun + " - Escala: " + escala;

		processEntity.setValue("obsTrocaDeEscala", obs);
		processEntity.setValue("obsTreinamento", obs);

		GregorianCalendar datIniFer = (GregorianCalendar) processEntity.findField("inicioFerias").getValue();
		GregorianCalendar datFimFer = (GregorianCalendar) datIniFer.clone();

		long qtdDiasFer = (long) processEntity.findField("qtdDias").getValue();
		int qtdDias = (int) qtdDiasFer;
		datFimFer.add(GregorianCalendar.DATE, qtdDias);
		datFimFer.set(GregorianCalendar.HOUR_OF_DAY, 23);
		datFimFer.set(GregorianCalendar.MINUTE, 59);
		processEntity.findField("fimFerias").setValue(datFimFer);

		NeoObject obj = (NeoObject) processEntity.findField("primeiraSugestao").getValue();
		EntityWrapper colaborador = new EntityWrapper(obj);
		long numLoc = (long) colaborador.findField("numloc").getValue();
		QLOpFilter qlNumLoc = new QLOpFilter("numloc", "=", numLoc);
		QLGroupFilter groupFilter = new QLGroupFilter("OR");
		groupFilter.addFilter(qlNumLoc);
		List<NeoObject> objs = PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORH_USU_Vorg203nv8"), groupFilter);
		if (objs != null && objs.size() > 0)
		{
			NeoObject objLotacao = objs.get(0);
			processEntity.findField("posto").setValue(objLotacao);
		}

		long codReg = (long) colaborador.getValue("usu_codreg");
		QLOpFilter qlCodReg = new QLOpFilter("usu_codreg", "=", codReg);
		QLGroupFilter groupFilterReg = new QLGroupFilter("AND");
		groupFilterReg.addFilter(qlCodReg);
		List<NeoObject> objsReg = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUREG"), groupFilterReg);
		if (objsReg != null && objsReg.size() > 0)
		{
			NeoObject objRegional = objsReg.get(0);
			processEntity.setValue("objRegional", objRegional);
		}

	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
