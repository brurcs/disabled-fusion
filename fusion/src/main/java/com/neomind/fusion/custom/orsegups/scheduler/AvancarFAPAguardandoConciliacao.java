package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.common.NeoRunnable;
import com.neomind.fusion.custom.orsegups.utils.FAPAgrupadorVO;
import com.neomind.fusion.custom.orsegups.utils.FornecedorVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.UserActivity;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.util.NeoUtils;

public class AvancarFAPAguardandoConciliacao implements CustomJobAdapter
{

	private static final Log log = LogFactory.getLog(AvancarFAPAguardandoConciliacao.class);

	@Override
	public void execute(CustomJobContext ctx)
	{
		Connection conn = null;
		StringBuilder sqlFapPorFornecedor = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;

		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		
		try
		{
			log.warn("##### INICIAR PROCESSO DE FINALIZAÇÃO DAS TAREFAS EM FASE DE CONCILIAÇÃO DE PAGAMENTO (FAP) - Data " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));

			GregorianCalendar diaCorte = new GregorianCalendar();
			diaCorte = OrsegupsUtils.addDays(diaCorte, -1, false);
			int dia = diaCorte.get(GregorianCalendar.DAY_OF_MONTH);
			int diaSem = diaCorte.get(GregorianCalendar.DAY_OF_WEEK);

			conn = PersistEngine.getConnection("SAPIENS");

			List<FornecedorVO> lstFornecedor = new ArrayList<FornecedorVO>();
			sqlFapPorFornecedor.append("SELECT CODFOR, CASE WHEN USU_DATCOR > 0 THEN USU_DATCOR ELSE USU_DIASEM END AS DATCOR FROM E095FOR WITH (NOLOCK) ");
			sqlFapPorFornecedor.append("WHERE (USU_DATCOR = " + dia + " OR USU_DIASEM = " + diaSem + ")                                                  ");

			//Abre Conexão com o Sapiens para consultar os fornecedores com dataCorte (getDate() - 1))
			pstm = conn.prepareStatement(sqlFapPorFornecedor.toString());
			rs = pstm.executeQuery();

			while (rs.next())
			{
				FornecedorVO fornecedorVO = new FornecedorVO();
				fornecedorVO.setCodigoFornecedor(rs.getLong("CODFOR"));
				fornecedorVO.setDataCorte(rs.getLong("DATCOR"));
				lstFornecedor.add(fornecedorVO);
			}

			OrsegupsUtils.closeConnection(conn, pstm, rs);
			//Fecha Conexão com o Sapiens para consultar os fornecedores com dataCorte (getDate() - 1))

			if (lstFornecedor != null && !lstFornecedor.isEmpty())
			{
				for (FornecedorVO forFornecedorVO : lstFornecedor)
				{
					if (forFornecedorVO.getDataCorte() == dia || forFornecedorVO.getDataCorte() == diaSem)
					{
						GregorianCalendar dtCorte = new GregorianCalendar();
						dtCorte.set(Calendar.DAY_OF_MONTH, dia);
						dtCorte.set(Calendar.HOUR_OF_DAY, 23);
						dtCorte.set(Calendar.MINUTE, 59);
						dtCorte.set(Calendar.SECOND, 59);
						//Produção
						String dataCorteString = NeoUtils.safeDateFormat(dtCorte, "yyyy-MM-dd HH:mm:ss");
						//Local
						//String dataCorteString = NeoUtils.safeDateFormat(dtCorte, "dd-MM-yyyy HH:mm:ss");

						AvançarFapAgrupadoraeFilhas(forFornecedorVO.getCodigoFornecedor(), dataCorteString);
					}
				}
			}
		}
		catch (Exception e)
		{
			log.error("##### ERRO AGENDADOR DE TAREFA: FAP - Avanca Tarefas Aguardando Conciliacao");
			System.out.println("["+key+"] ##### ERRO AGENDADOR DE TAREFA: FAP - Avanca Tarefas Aguardando Conciliacao");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				log.error("##### ERRO AO INICIAR O PROCESSO DE FINALIZAÇÃO DAS TAREFAS EM FASE DE CONCILIAÇÃO DE PAGAMENTO (FAP) - Data " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
				e.printStackTrace();
			}

			log.warn("##### FIM DO PROCESSO DE FINALIZAÇÃO DAS TAREFAS EM FASE DE CONCILIAÇÃO DE PAGAMENTO (FAP) - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		}
	}

	public static void AvançarFapAgrupadoraeFilhas(Long codFor, String dataCorte)
	{
		Connection connTarefasFap = null;
		StringBuilder sqlFapsPorFornecedor = new StringBuilder();
		PreparedStatement pstmFapsPorFornecedor = null;
		ResultSet rsFapsPorFornecedor = null;

		try
		{
			log.warn("##### INICIANDO O AVANÇO DAS FAPS AGRUPADAS - Data " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));

			connTarefasFap = PersistEngine.getConnection("");

			List<FAPAgrupadorVO> fapsagrupadoras = new ArrayList<FAPAgrupadorVO>();
			sqlFapsPorFornecedor.append(" SELECT WF.CODE, FAP.VALORTOTAL FROM D_FAPAUTORIZACAODEPAGAMENTO FAP WITH (NOLOCK) ");
			sqlFapsPorFornecedor.append(" INNER JOIN X_ETABFOR FORN WITH (NOLOCK) ON FORN.NEOID = FAP.FORNECEDOR_NEOID 		");
			sqlFapsPorFornecedor.append(" INNER JOIN WFPROCESS WF WITH (NOLOCK) ON FAP.WFPROCESS_NEOID = WF.NEOID 			");
			sqlFapsPorFornecedor.append(" INNER JOIN ACTIVITY A WITH (NOLOCK) ON WF.NEOID = A.PROCESS_NEOID 				");
			sqlFapsPorFornecedor.append(" LEFT JOIN ACTIVITYMODEL AM WITH (NOLOCK) ON AM.NEOID = A.MODEL_NEOID 				");
			sqlFapsPorFornecedor.append(" WHERE WF.PROCESSSTATE = 0 AND											            ");
			sqlFapsPorFornecedor.append(" 	    WF.SAVED = 1 AND												  			");
			sqlFapsPorFornecedor.append("       FAP.APROVADO = 1 AND											  			");
			sqlFapsPorFornecedor.append("       FAP.DATASOLICITACAO >= '2015-07-01 00:00:00.000' AND              			");
			sqlFapsPorFornecedor.append("       FORN.CODFOR = " + codFor + " AND                                   			");
			sqlFapsPorFornecedor.append("       A.STARTDATE >= '2015-07-01 00:00:00.000' AND					  			");
			sqlFapsPorFornecedor.append("       A.STARTDATE <= '" + dataCorte + "' AND							  			");
			sqlFapsPorFornecedor.append("       A.FINISHDATE IS NULL AND										  			");
			sqlFapsPorFornecedor.append("       AM.NAME = 'AGUARDANDO CONCILIAÇÃO' 								  			");
			sqlFapsPorFornecedor.append(" ORDER BY WF.CODE ASC													  			");

			//Abre Conexão com o Fusion para consultar as tarefas em fase de "Aguardando Conciliação" para agrupa-las
			pstmFapsPorFornecedor = connTarefasFap.prepareStatement(sqlFapsPorFornecedor.toString());
			rsFapsPorFornecedor = pstmFapsPorFornecedor.executeQuery();

			while (rsFapsPorFornecedor.next())
			{
				FAPAgrupadorVO faps = new FAPAgrupadorVO();
				faps.setCodigoProcesso(rsFapsPorFornecedor.getString("code"));
				faps.setValorTotal(rsFapsPorFornecedor.getBigDecimal("valortotal"));
				fapsagrupadoras.add(faps);
			}

			OrsegupsUtils.closeConnection(connTarefasFap, pstmFapsPorFornecedor, rsFapsPorFornecedor);
			//Fecha Conexão com o Fusion para consultar as tarefas em fase de "Aguardando Conciliação" para agrupa-las

			if (fapsagrupadoras != null && !fapsagrupadoras.isEmpty())
			{
				FAPAgrupadorVO agrupadorVO = fapsagrupadoras.get(fapsagrupadoras.size() - 1);
				final String codigoTarefaAgrupadora = agrupadorVO.getCodigoProcesso();

				for (final FAPAgrupadorVO fapAgrupadorVO : fapsagrupadoras)
				{
					final NeoRunnable work = new NeoRunnable()
					{
						public void run() throws Exception
						{
							//Código da sub-transação, exemplo da classe AvançarFAPAguardandoConciliacao
							if (fapAgrupadorVO.getCodigoProcesso().equals(codigoTarefaAgrupadora))
							{
								AvançarFapAgrupadora(fapAgrupadorVO.getCodigoProcesso(), true, codigoTarefaAgrupadora);
							}
							else
							{
								AvançarFapAgrupadora(fapAgrupadorVO.getCodigoProcesso(), false, codigoTarefaAgrupadora);
							}
						}
					};
					try
					{
						PersistEngine.managedRun(work);
					}
					catch (final Exception e)
					{
						log.error(e.getMessage(), e);
					}
				}
			}
		}
		catch (Exception e)
		{
			log.error("##### ERRO AO AVANÇAR AS TAREFAS DE FAPS AGRUPADAS - Data " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
			e.printStackTrace();
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(connTarefasFap, pstmFapsPorFornecedor, rsFapsPorFornecedor);
			}
			catch (Exception e)
			{
				log.error("##### ERRO AO AVANÇAR AS TAREFAS DE FAPS AGRUPADAS - Data " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
				e.printStackTrace();
			}
			log.warn("##### FINALIZANDO O AVANÇO DAS FAPS AGRUPADAS - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		}
	}

	public static void AvançarFapAgrupadora(String codigoTarefa, Boolean fapAgrupadora, String codigoTarefaAgrupadora)
	{
		NeoUser objSolicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "sistemaFusion"));
		if (NeoUtils.safeIsNull(objSolicitante))
		{
			log.error("Erro #1 - Solicitante não encontrado - Data " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		}

		WFProcess processo = null;
		NeoObject wkfTarefaSimples = null;
		EntityWrapper ewWkfTarefaSimples = null;

		processo = (WFProcess) PersistEngine.getNeoObject(WFProcess.class, new QLRawFilter("code = '" + codigoTarefa + "' and saved = 1 and processstate = 0 and model.name = 'F001 - FAP - AutorizacaoDePagamento'"));
		wkfTarefaSimples = processo.getEntity();
		ewWkfTarefaSimples = new EntityWrapper(wkfTarefaSimples);

		if (fapAgrupadora == true)
		{
			ewWkfTarefaSimples.setValue("fapAgrupadora", true);
			ewWkfTarefaSimples.setValue("codigoTarefa", codigoTarefaAgrupadora);
		}
		else
		{
			ewWkfTarefaSimples.setValue("fapAgrupadora", false);
			ewWkfTarefaSimples.setValue("codigoTarefa", codigoTarefaAgrupadora);
		}

		// Trata a primeira tarefa
		Task task = null;
		final List acts = processo.getOpenActivities();

		List<Activity> activity1 = (List<Activity>) acts;
		for (Activity activity2 : activity1)
		{
			System.out.println("Avançando a Tarefa: " + ewWkfTarefaSimples.findValue("wfprocess.code") + " para a próxima atividade!");
			if (activity2.getActivityName().equals("Aguardando Conciliação"))
			{
				if (activity2 instanceof UserActivity)
				{
					try
					{
						if (((UserActivity) activity2).getTaskList().size() <= 0)
						{
							task = activity2.getModel().getTaskAssigner().assign((UserActivity) activity2, objSolicitante);
							OrsegupsWorkflowHelper.finishTaskByActivity(activity2);
						}
						else
						{
							task = ((UserActivity) activity2).getTaskList().get(0);
							OrsegupsWorkflowHelper.finishTaskByActivity(activity2);
						}
					}
					catch (Exception e)
					{
						log.error("Erro #2 - Erro ao avançar a primeira tarefa - Data " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
						e.printStackTrace();
					}
				}
			}
		}
	}
}
