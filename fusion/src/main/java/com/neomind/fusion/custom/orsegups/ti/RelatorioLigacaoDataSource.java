package com.neomind.fusion.custom.orsegups.ti;

import java.sql.Timestamp;


public class RelatorioLigacaoDataSource
{
    private String nomeRegional;
    private String origem;
    private String destino;
    private Long duracao;
    private Timestamp dataLigacao;
    private String departamento;
    private String usuario;
    private Timestamp duracaoLigacao;
    
    
    public String getNomeRegional() {
        return nomeRegional;
    }
    public void setNomeRegional(String nomeRegional) {
        this.nomeRegional = nomeRegional;
    }
    public String getOrigem() {
        return origem;
    }
    public void setOrigem(String origem) {
        this.origem = origem;
    }
    public String getDestino() {
        return destino;
    }
    public void setDestino(String destino) {
        this.destino = destino;
    }
    public Long getDuracao() {
        return duracao;
    }
    public void setDuracao(Long duracao) {
        this.duracao = duracao;
    }
    public Timestamp getDataLigacao() {
        return dataLigacao;
    }
    public void setDataLigacao(Timestamp dataLigacao) {
        this.dataLigacao = dataLigacao;
    }
    public String getDepartamento() {
        return departamento;
    }
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }
    public String getUsuario() {
        return usuario;
    }
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    public Timestamp getDuracaoLigacao() {
	return duracaoLigacao;
    }
    public void setDuracaoLigacao(Timestamp duracaoLigacao) {
	this.duracaoLigacao = duracaoLigacao;
    }
    
    
}
