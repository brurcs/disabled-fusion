package com.neomind.fusion.custom.casvig.mobile.action;

import java.io.DataOutputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

import com.neomind.fusion.custom.casvig.mobile.CasvigMobileServlet;
import com.neomind.fusion.custom.casvig.mobile.MobileUtils;
import com.neomind.fusion.custom.casvig.mobile.xml.XmlMobileBuilder;
import com.neomind.fusion.custom.casvig.mobile.xml.XmlMobileSplit;

/**
 * Classe que envia o XML de Clientes para o Mobile da Casvig usado nas
 * inspetorias de qualidade
 * 
 * @author Daniel Henrique Joppi
 * 
 */
@WebServlet(name="ActionUpdateClient", urlPatterns={"/servlet/com.neomind.fusion.custom.casvig.mobile.action.ActionUpdateClient"})
public class ActionUpdateClient extends HttpServlet
{
	public static ActionUpdateClient getInstance()
	{
		return new ActionUpdateClient();
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String msg = null;
		DataOutputStream out = null;

		boolean error = false;
		try
		{
			out = new DataOutputStream(response.getOutputStream());

			CasvigMobileServlet.log.info("Mobile request Cliente.xml");
			
			// tipo de conexão
			String type = request.getParameter("type");
			
			// caso o mobile requisite o envio do XML completo
			if(type == null || type.equals("xml-full")) 
			{
				Document docXML = XmlMobileBuilder.buildXMLClient();
				
				msg = new String("Send to mobile: Cliente.xml");
				
				MobileUtils.sendFile(docXML, response);
			} 
			// mobile requisite o envio do XML em partes
			else if(type.equals("xml-split")) 
			{
				String split = request.getParameter("split-part");
				
				// divide o xml em varias partes e envia pro mobile um identificador
				if(split == null || split.equals("generate"))
				{
					String idSplit = XmlMobileSplit.giveMeIdSplit();
					if(idSplit == null )
					{
						Document docXML = XmlMobileBuilder.buildXMLClient();
						idSplit = XmlMobileSplit.splitClient(docXML);
					}
					
					msg = new String("Send to mobile: ID Split - " + idSplit);
					MobileUtils.sendString(idSplit, response);
				}
				// remove fila que continha as partes do XML
				else if(split.equals("remove"))
				{
					msg = new String("No have more parts");
					
					String idSplit = request.getParameter("id-split");
					
					XmlMobileSplit.removeXMLClient(idSplit);
				}
				// envia a parte do XML dividida anteriormente
				else 
				{
					Integer part = Integer.parseInt(split);
					String idSplit = request.getParameter("id-split");
					String fileSplit = request.getParameter("file-split");
					
					if(part == 0)
						msg = new String("Send to mobile: Cliente.xml (index)");
					else
						msg = new String("Send to mobile: Cliente-Part"+(part)+".xml");
					
					Document docXMLPart = XmlMobileSplit.getXMLClientPart(idSplit, part, fileSplit);
					
					MobileUtils.sendFile(docXMLPart, response);
				}
			}
			
			CasvigMobileServlet.log.info(msg);
			return;
			
		}
		catch (Exception e)
		{
			CasvigMobileServlet.log.error(e.getMessage(), e);
			error = true;
		}
		finally
		{
			if (out != null)
			{
				try
				{
					out.close();
				}
				catch (IOException ioe)
				{
					ioe.printStackTrace();
					error = true;
				}
			}
			if (error)
			{
				if (msg != null)
					CasvigMobileServlet.log.error(msg);
			}
		}
	}

}
