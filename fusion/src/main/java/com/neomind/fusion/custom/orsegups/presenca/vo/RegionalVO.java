package com.neomind.fusion.custom.orsegups.presenca.vo;

public class RegionalVO
{
	Long usuCodReg;
	String usuNomReg;
	public Long getUsuCodReg()
	{
		return usuCodReg;
	}
	public void setUsuCodReg(Long usuCodReg)
	{
		this.usuCodReg = usuCodReg;
	}
	public String getUsuNomReg()
	{
		return usuNomReg;
	}
	public void setUsuNomReg(String usuNomReg)
	{
		this.usuNomReg = usuNomReg;
	}
	@Override
	public String toString()
	{
		return "RegionalVO [usuCodReg=" + usuCodReg + ", usuNomReg=" + usuNomReg + "]";
	}
	
	
	
}
