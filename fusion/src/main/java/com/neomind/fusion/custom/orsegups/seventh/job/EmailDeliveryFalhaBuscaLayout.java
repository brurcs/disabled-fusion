package com.neomind.fusion.custom.orsegups.seventh.job;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.google.gson.Gson;
import com.neomind.fusion.custom.orsegups.seventh.dto.CFTVConfigDTO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;

public class EmailDeliveryFalhaBuscaLayout implements CustomJobAdapter {

    @Override
    public void execute(CustomJobContext ctx) {

	StringBuilder retorno = new StringBuilder();

	try {

	    URL url = new URL("https://apps.orsegups.com.br:8080/AtendimentoEventosServices/seventh/relacaoFalhaBuscaLayoutServidoresHabilitados");
	    //URL url = new URL("http://localhost:8081/seventh/relacaoFalhaBuscaLayoutServidoresHabilitados");
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setRequestMethod("GET");
	    conn.setRequestProperty("x-auth-token", "682ee2548d4a73d8fadf72f32c52df790eb72aff");

	    if (conn.getResponseCode() == 200) {
		// inserirLogErro(historico, conn.getResponseMessage());
		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream()), "UTF-8"));

		String output;

		while ((output = br.readLine()) != null) {
		    retorno.append(output + "\n");
		}
	    }

	    conn.disconnect();

	} catch (MalformedURLException e) {
	    // inserirLogErro(historico, e.getMessage());
	    e.printStackTrace();

	} catch (IOException e) {
	    // inserirLogErro(historico, e.getMessage());
	    e.printStackTrace();

	}

	CFTVConfigDTO[] result = null;

	if (!retorno.toString().isEmpty()) {
	    Gson gson = new Gson();

	    try {
		result = (CFTVConfigDTO[]) gson.fromJson(retorno.toString(), CFTVConfigDTO[].class);
	    } catch (Exception e) {
		e.printStackTrace();
	    }

	}

	StringBuilder dadosApontamentoEmail = new StringBuilder();

	if (result != null) {

	    int contApontamentos = 0;

	    for (CFTVConfigDTO c : result) {

		String central = c.getCodigoCentral().trim();
		int empresa = c.getIdEmpresa();
		String servidor = c.getServidorCFTV().trim();
		String usuario = c.getUsuarioCFTV().trim();
		String senha = c.getSenhaCFTV().trim();

		if (servidor.length() > 80) {
		    servidor = servidor.substring(0, 40);
		}

		contApontamentos++;
		int resto = (contApontamentos) % 2;
		if (resto == 0) {
		    dadosApontamentoEmail.append("<tr style=\"background-color: #d6e9f9\" ><td>" + central + "</td><td>" + empresa + "</td><td>" + servidor + "</td><td>" + usuario + "</td><td>" + senha + "</td></tr>");
		} else {
		    dadosApontamentoEmail.append("<tr><td>" + central + "</td><td>" + empresa + "</td><td>" + servidor + "</td><td>" + usuario + "</td><td>" + senha + "</td></tr>");
		}

	    }

	}

	StringBuilder corpoEmail = new StringBuilder();

	corpoEmail.append("<!DOCTYPE html>");
	corpoEmail.append("<html>");
	corpoEmail.append("<head>");
	corpoEmail.append("<meta charset=\"UTF-8\">");
	corpoEmail.append("<title>Relação Falha Busca Layout</title>");
	corpoEmail.append("<style>");
	corpoEmail.append("h2{");
	corpoEmail.append("font-weight:bold; color:#303090; letter-spacing:1pt; word-spacing:2pt; font-size:20px; text-align:center; font-family:Verdana; line-height:1;");
	corpoEmail.append("}");
	corpoEmail.append(".table-p{");
	corpoEmail.append("font-family: Verdana; font-weight:normal; font-size:12px; text-align:justify;");
	corpoEmail.append(" }");
	corpoEmail.append(".table-info{");
	corpoEmail.append("table-layout: fixed;");
	corpoEmail.append("text-align:left;");
	corpoEmail.append("font-family: Verdana;");
	corpoEmail.append("}");
	corpoEmail.append(".table-info td{");
	corpoEmail.append("overflow-x: hidden;");
	corpoEmail.append(" }");
	corpoEmail.append(".table-info thead{");
	corpoEmail.append("background-color:#303090;");
	corpoEmail.append("color: white;");
	corpoEmail.append("font-weight:bold;");
	corpoEmail.append("	}");
	corpoEmail.append("</style>");
	corpoEmail.append("</head>");
	corpoEmail.append("<body>");
	corpoEmail.append("<div>");
	corpoEmail.append("<table width=\"600\" align=\"center\">");
	corpoEmail.append("<tr><td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/header.jpg\" width=\"600\" height=\"145\"></td></tr>");
	corpoEmail.append("</table>");
	corpoEmail.append("<td><h2>Relação Falha Busca Layout</h2></td>");
	corpoEmail.append("<table width=\"600\" align=\"center\" class=\"table-p\"> ");
	corpoEmail.append("<tr><td>Seguem detalhes das inconsistências:</td></tr>");
	corpoEmail.append("</table>");
	corpoEmail.append("<table width=\"600\" align=\"center\" class=\"table-info\"> ");
	corpoEmail.append("<thead>");
	corpoEmail.append("<tr>");
	corpoEmail.append("<td>Conta</td>");
	corpoEmail.append("<td>Empresa</td>");
	corpoEmail.append("<td>Servidor</td>");
	corpoEmail.append("<td>Usuario</td>");
	corpoEmail.append("<td>Senha</td>");
	corpoEmail.append("</tr>");
	corpoEmail.append("</thead>");
	corpoEmail.append("<tbody>");
	corpoEmail.append("<!-- Recebe dados do método -->");
	corpoEmail.append(dadosApontamentoEmail);
	corpoEmail.append("<br>");
	corpoEmail.append("</tbody>");
	corpoEmail.append("</table>");
	corpoEmail.append("</div>");
	corpoEmail.append("</div>");
	corpoEmail.append("</body>");
	corpoEmail.append("</html>");

	String[] addTo = new String[] { "laercio.leite@orsegups.com.br" };
	String[] comCopia = new String[] { "mateus.batista@orsegups.com.br","emailautomatico@orsegups.com.br" };
	String from = "fusion@orsegups.com.br";
	String subject = "Relação Falha Busca Layout";
	String html = corpoEmail.toString();

	OrsegupsUtils.sendEmail2Orsegups(addTo, from, subject, html, comCopia);

    }

}
