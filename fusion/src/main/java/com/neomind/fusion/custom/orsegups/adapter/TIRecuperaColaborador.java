package com.neomind.fusion.custom.orsegups.adapter;

import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class TIRecuperaColaborador implements AdapterInterface {

	@Override
	public void back(EntityWrapper arg0, Activity arg1) {
	}

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity) {
		 
		Long numcad = (Long) processEntity.getValue("numcad");
		Long numemp = (Long) processEntity.getValue("numemp");
		Long tipcol = (Long) processEntity.getValue("tipcol");
		Long sitafa = 7L;
		
		QLEqualsFilter filtroNumCad = new QLEqualsFilter("numcad",numcad);
		QLEqualsFilter filtroNumEmp = new QLEqualsFilter("numemp", numemp);
		QLEqualsFilter filtroTipCol = new QLEqualsFilter("tipcol", tipcol);
		QLEqualsFilter filtroSitAfa = new QLEqualsFilter("sitafa", sitafa);
		
		QLGroupFilter filtroColaborador = new QLGroupFilter("AND");
		filtroColaborador.addFilter(filtroNumCad);
		filtroColaborador.addFilter(filtroNumEmp); 
		filtroColaborador.addFilter(filtroTipCol); 
		filtroColaborador.addFilter(filtroSitAfa); 
		
		List<NeoObject> colaboradores = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORHRFUN"),filtroColaborador);
		
		if (colaboradores != null && !colaboradores.isEmpty())
		{
			processEntity.setValue("colaborador", colaboradores.get(0));
			processEntity.setValue("estaDemitido", true);
		}
		else
		{
			processEntity.setValue("estaDemitido", false);
		}
	}

}
