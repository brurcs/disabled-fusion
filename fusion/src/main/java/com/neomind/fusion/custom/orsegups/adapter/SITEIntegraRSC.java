package com.neomind.fusion.custom.orsegups.adapter;

import java.util.HashMap;
import java.util.Map;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.fusion.workflow.simulation.WorkflowService;

//TODO RSC - Esta classe é utilizada pelo fluxo: [SITE] Processo de recebimento de contato, porém o mesmo não está em uso
@Deprecated
public class SITEIntegraRSC implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		InstantiableEntityInfo infoRSC = AdapterUtils.getInstantiableEntityInfo("RSCRelatorioSolicitacaoCliente"); 
		NeoObject noRSC = infoRSC.createNewInstance();
		EntityWrapper rscWrapper = new EntityWrapper(noRSC);
		
		InstantiableEntityInfo infoOrigem = AdapterUtils.getInstantiableEntityInfo("RRCOrigem");
		QLEqualsFilter filterOrigem = new QLEqualsFilter("sequencia", "05");
		
		rscWrapper.findField("origem").setValue((NeoObject) PersistEngine.getObject(infoOrigem.getEntityClass(), filterOrigem));
		rscWrapper.findField("cidade").setValue((String) processEntity.findValue("cidadeExt.nomcid"));
		rscWrapper.findField("lotacao").setValue(processEntity.findValue("nome"));
		rscWrapper.findField("email").setValue(processEntity.findValue("email"));
		rscWrapper.findField("telefones").setValue(processEntity.findValue("telefone"));
		
		EntityWrapper tipoContatoWrapper = new EntityWrapper((NeoObject)processEntity.findValue("tipoContato"));
		String objetivo = (String) tipoContatoWrapper.findValue("tipoContato.tipo");
		//rscWrapper.findField("descricao").setValue("Aberto via processo de cobrança " + origin.getProcessName() + " tarefa " + origin.getCode() + "\nIntenção: "+objetivo+"\nDescrição: " + (String) processEntity.findValue("mensagem"));
		rscWrapper.findField("descricao").setValue((String) processEntity.findValue("mensagem"));

		PersistEngine.persist(noRSC);

		QLEqualsFilter equal = new QLEqualsFilter("Name", "C027 - RSC - Relatório de Solicitação de Cliente");
		ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);

		NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code","fernanda.oliveira"));

		/**
		 * @author neomind willian.mews - Alterado para utilizar a nova arquitetura de processos do Fusion
		 * @date 11/03/2015
		 */
		
		final WFProcess processo = WorkflowService.startProcess(processModel, noRSC, false, solicitante);
		
		/**
		 * FIM ALTERAÇÕES - NEOMIND
		 */
		
		//if (((String) processEntity.findValue("mensagem")).equalsIgnoreCase("contato")){
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		paramMap.put("tarefa", processo.getCode());
		paramMap.put("nome", processEntity.findValue("nome"));
		paramMap.put("tipo", objetivo);
		paramMap.put("mensagem", processEntity.findValue("mensagem"));
		
		FusionRuntime.getInstance().getMailEngine().sendEMail((String) processEntity.findField("email").getValue()+";sistemas@orsegups.com.br", "/portal_orsegups/emailAberturaRSC.jsp", paramMap);
		//}		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}
}
