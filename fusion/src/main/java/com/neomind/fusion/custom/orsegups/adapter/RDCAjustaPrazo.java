package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class RDCAjustaPrazo implements AdapterInterface {

	@Override
	public void start(Task origin, EntityWrapper processEntity,
			Activity activity) {
		GregorianCalendar dataNegociacao = (GregorianCalendar) processEntity.findField("dataNegociacao").getValue();
		dataNegociacao = OrsegupsUtils.addDays(dataNegociacao, 3, true);
		
		 if(!OrsegupsUtils.isWorkDay(dataNegociacao)){
		   dataNegociacao = OrsegupsUtils.getNextWorkDay(dataNegociacao); 
		 }		 
		processEntity.findField("dataNegociacao").setValue(dataNegociacao);
		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
	}

}
