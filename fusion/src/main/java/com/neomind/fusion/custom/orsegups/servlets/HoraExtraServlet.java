package com.neomind.fusion.custom.orsegups.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.vetorh.HoraExtraVO;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.util.NeoUtils;

@WebServlet(name="HoraExtraServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.servlets.HoraExtraServlet"})
public class HoraExtraServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		this.doRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		this.doRequest(request, response);
	}

	private void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String action = "";

		if (request.getParameter("action") != null)
		{
			action = request.getParameter("action");
		}

		if (action.equalsIgnoreCase("importarHE"))
		{
			this.importarHE();
		}

		if (action.equalsIgnoreCase("importarVA"))
		{
			this.importarVA();
		}
	}

	private void importarHE()
	{
		try
		{
			//Feriados
			String competencia = "12/2014";
			List<GregorianCalendar> feriados = new ArrayList<GregorianCalendar>();
			feriados = listaFeriados(competencia);
			
			Long codCal21 = buscaCalculo(21L, competencia);
			Long codCal22 = buscaCalculo(22L, competencia);
			
			GregorianCalendar dataInicioCompetencia = OrsegupsUtils.getDiaInicioCompetencia(competencia);
			GregorianCalendar dataFimCompetencia = OrsegupsUtils.getDiaFimCompetencia(competencia);
			
			List<HoraExtraVO> horas = new ArrayList<HoraExtraVO>();
			List<HoraExtraVO> horasFinal = new ArrayList<HoraExtraVO>();
			
			horas = listarHE(dataInicioCompetencia, dataFimCompetencia);
			
			for (HoraExtraVO h : horas)
			{
				//Feriado para empresa 21
				if (h.getNumemp() == 21L)
				{
					if (NeoUtils.safeIsNotNull(feriados))
					{
						for (GregorianCalendar fer : feriados)
						{
							if (NeoUtils.safeDateFormat(h.getgDataI(), "dd/MM/yyyy").equals(NeoUtils.safeDateFormat(fer, "dd/MM/yyyy")))
							{
								h.setHorario(9997L);
								break;
							}
						}
					}
				}

				Integer minutosInicio = h.getgDataI().get(Calendar.HOUR_OF_DAY) * 60 + h.getgDataI().get(Calendar.MINUTE);
				Integer minutosFim = h.getgDataF().get(Calendar.HOUR_OF_DAY) * 60 + h.getgDataF().get(Calendar.MINUTE);

				if (h.getgDataI().get(Calendar.DAY_OF_MONTH) != h.getgDataF().get(Calendar.DAY_OF_MONTH) || h.getgDataI().get(Calendar.MONTH) != h.getgDataF().get(Calendar.MONTH))
				{
					minutosFim = minutosFim + 1440;
				}
				
				Long minutosDiurnos = 0L;
				Long minutosNoturnos = 0L;

				for (int j = minutosInicio; j < minutosFim; j++)
				{
					if (j >= 300 && j < 1320)
					{
						minutosDiurnos++;
					}
					else
					{
						minutosNoturnos++;
					}
				}

				if (minutosDiurnos > 0L)
				{
					/**
					 *  1.Empresa 21 (Metropolitana)
					 */
					if (h.getNumemp() == 21L)
					{
						h.setCalculo(codCal21);
						Integer porcentagem = defineEvento(h, null);
						
						switch (porcentagem)
						{
							case 50:
								addListaHE(horasFinal, h, minutosDiurnos, 1182L, "Horas Extras c/ 50%", codCal21, codCal22);
								break;
							case 60:
								addListaHE(horasFinal, h, minutosDiurnos, 1184L, "Horas Extras c/ 60%", codCal21, codCal22);
								break;
							case 100:
								addListaHE(horasFinal, h, minutosDiurnos, 1183L, "Horas Extras c/ 100%", codCal21, codCal22);
								break;
							case 150:
								addListaHE(horasFinal, h, minutosDiurnos, 1229L, "Horas Domingo/Feriado 50%", codCal21, codCal22);
								break;
							case 200:
								addListaHE(horasFinal, h, minutosDiurnos, 1225L, "Hora Domingo/Feriado 100%", codCal21, codCal22);
								break;	
						}
					}
					/**
					 * 2.Empresa 22 (Objetiva)
					 */
					else if (h.getNumemp() == 22L)
					{
						h.setCalculo(codCal22);
						Integer porcentagem = defineEvento(h, minutosDiurnos);
						
						switch (porcentagem)
						{
							case 50:
								addListaHE(horasFinal, h, minutosDiurnos, 1182L, "Horas Extras c/ 50%", codCal21, codCal22);
								break;
								
							case 100:
								addListaHE(horasFinal, h, minutosDiurnos, 1183L, "Horas Extras c/ 100%", codCal21, codCal22);
								break;
								
							case 50100:
								addListaHE(horasFinal, h, 120L, 1182L, "Horas Extras c/ 50%", codCal21, codCal22);
								addListaHE(horasFinal, h, minutosDiurnos - 120L, 1183L, "Horas Extras c/ 100%", codCal21, codCal22);
								break;
						}
					}
				}

				if (minutosNoturnos > 0)
				{
					if (h.getNumemp() == 22 && minutosDiurnos >= 120L)
					{
						addListaHE(horasFinal, h, minutosNoturnos, 1221L, "Horas Extras 100% Noturna", codCal21, codCal22);
					}
					else
					{
						Integer porcentagem = defineEvento(h, minutosDiurnos + minutosNoturnos);
						
						switch (porcentagem)
						{
							case 50:
								addListaHE(horasFinal, h, minutosNoturnos, 1220L, "Horas Extras 50% Noturna", codCal21, codCal22);
								break;
								
							case 60:
								addListaHE(horasFinal, h, minutosNoturnos, 1222L, "Horas Extras 60% Noturna", codCal21, codCal22);
								break;	
								
							case 100:	
								addListaHE(horasFinal, h, minutosNoturnos, 1221L, "Horas Extras 100% Noturna", codCal21, codCal22);
								break;
							
							case 50100:
								addListaHE(horasFinal, h, 120L - minutosDiurnos, 1220L, "Horas Extras 50% Noturna", codCal21, codCal22);
								addListaHE(horasFinal, h, minutosNoturnos - (120L - minutosDiurnos), 1221L, "Horas Extras 100% Noturna", codCal21, codCal22);
								break;
							
							case 150:
								addListaHE(horasFinal, h, minutosNoturnos, 1229L, "Horas Domingo/Feriado 50% ", codCal21, codCal22);
								break;
							case 200:
								addListaHE(horasFinal, h, minutosNoturnos, 1225L, "Hora Domingo/Feriado 100%", codCal21, codCal22);
								break;	
						}
					}
				}
			}
			/*
			for (HoraExtraVO h : horasFinal)
			{
				System.out.println(h);
			}
			 */
			Integer codSeq = 10;
			for (HoraExtraVO h : horasFinal)
			{
				System.out.println("INSERT INTO R044MOV VALUES ("+ h.getNumemp() + ", 1, " + h.getNumcad() + ", " + h.getCalculo() + ", 941, " + h.getCodeve() + ", 0, " + codSeq + ", 'I', " + h.getRefeve() + ", 0);");
				codSeq ++;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private List<GregorianCalendar> listaFeriados(String competencia) throws ParseException
	{
		Connection connVetorh = PersistEngine.getConnection("VETORHPROD");
		List<GregorianCalendar> feriados = new ArrayList<GregorianCalendar>();

		GregorianCalendar inicioCompetencia = OrsegupsUtils.getDiaInicioCompetencia(competencia);
		GregorianCalendar fimCompetencia = OrsegupsUtils.getDiaFimCompetencia(competencia);

		try
		{
			StringBuffer queryColaborador = new StringBuffer();
			queryColaborador.append(" SELECT fec.DatFer from R002FEM fem ");
			queryColaborador.append(" INNER JOIN R002FEC fec on fec.CodFer = fem.CodFer ");
			queryColaborador.append(" WHERE fec.DatFer >= ? AND fec.DatFer <= ? AND fem.NomFer = 'Curitiba-PR' ");

			Timestamp dataInicioTS = new Timestamp(inicioCompetencia.getTimeInMillis());
			Timestamp dataFimTS = new Timestamp(fimCompetencia.getTimeInMillis());
			PreparedStatement st = connVetorh.prepareStatement(queryColaborador.toString());
			st.setTimestamp(1, dataInicioTS);
			st.setTimestamp(2, dataFimTS);

			ResultSet rs = st.executeQuery();

			while (rs.next())
			{
				Timestamp dataApuracao = rs.getTimestamp("DatFer");
				GregorianCalendar gData = new GregorianCalendar();
				gData.setTimeInMillis(dataApuracao.getTime());

				feriados.add(gData);
			}
			rs.close();
			st.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				connVetorh.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();

			}
		}

		return feriados;
	}
	
	private List<HoraExtraVO> listarHE(GregorianCalendar dataInicio, GregorianCalendar dataFim)
	{
		Connection connVetorh = PersistEngine.getConnection("VETORHPROD");
		List<HoraExtraVO> horas = new ArrayList<HoraExtraVO>();

		try
		{
			StringBuffer queryColaborador = new StringBuffer();
			queryColaborador.append(" SELECT fun.numemp, fun.numcad, fun.nomfun, hfi.codfil, cob.usu_datalt, cob.usu_datfim, esc.CodEsc, tma.CodTma, esc.NomEsc, esc.usu_gruesc, gru.usu_desgruesc, esc.HorSem/60 AS HorSem, hsi.CodSin, hor.CodHor, hor2.DesHor ");
			queryColaborador.append(" FROM USU_T038COBFUN cob ");
			queryColaborador.append(" INNER JOIN R034FUN fun On fun.numemp = cob.usu_numemp AND fun.tipcol = cob.usu_tipcol AND fun.numcad = cob.usu_numcad ");
			queryColaborador.append(" INNER JOIN R038HFI hfi WITH (NOLOCK) ON hfi.NumEmp = fun.numemp AND hfi.TipCol = fun.tipcol AND hfi.NumCad = fun.numcad AND hfi.DatAlt = (SELECT MAX(hfi2.DatAlt) FROM R038HFI hfi2 WHERE hfi2.NumEmp = hfi.NumEmp AND hfi2.TipCol = hfi.TipCol AND hfi2.NumCad = hfi.NumCad AND hfi2.DatAlt <= cob.usu_datalt) ");
			queryColaborador.append(" INNER JOIN R038HES hes WITH (NOLOCK) ON hes.NumEmp = fun.numemp AND hes.TipCol = fun.tipcol AND hes.NumCad = fun.numcad AND hes.DatAlt = (SELECT MAX(hes2.DatAlt) FROM R038HES hes2 WHERE hes2.NumEmp = hes.NumEmp AND hes2.TipCol = hes.TipCol AND hes2.NumCad = hes.NumCad AND hes2.DatAlt <= cob.usu_datalt) ");
			queryColaborador.append(" INNER JOIN R006ESC esc ON esc.CodEsc = hes.CodEsc ");
			queryColaborador.append(" INNER JOIN R006TMA tma ON tma.CodEsc = esc.CodEsc AND tma.CodTma = hes.CodTma ");
			queryColaborador.append(" LEFT JOIN R006HOR hor ON hor.CodEsc = esc.CodEsc AND DATEDIFF(dd,tma.DatBas,cob.usu_datalt) % (SELECT COUNT(*) FROM R006HOR hr2 WHERE hr2.CodEsc = hor.CodEsc) + 1 = hor.SeqReg ");
			queryColaborador.append(" LEFT JOIN R004HOR hor2 ON hor2.CodHor = hor.CodHor ");
			queryColaborador.append(" LEFT JOIN R038HSI hsi WITH (NOLOCK) ON hsi.NumEmp = fun.numemp AND hsi.TipCol = fun.tipcol AND hsi.NumCad = fun.numcad AND hsi.DatAlt = (SELECT MAX(hsi2.DatAlt) FROM R038HSI hsi2 WHERE hsi2.NumEmp = hsi.NumEmp AND hsi2.TipCol = hsi.TipCol AND hsi2.NumCad = hsi.NumCad AND hsi2.DatAlt <= cob.usu_datalt) ");
			queryColaborador.append(" LEFT JOIN USU_T006GRES gru on gru.usu_gruesc = esc.usu_gruesc ");
			queryColaborador.append(" WHERE fun.numemp IN (21,22)  ");
			queryColaborador.append(" AND cob.usu_datalt >= ? ");
			queryColaborador.append(" AND cob.usu_datalt <= ? ");
			queryColaborador.append(" AND cob.usu_codmot IN (13,14) ");
			queryColaborador.append(" ORDER BY fun.numemp, fun.numcad, cob.usu_datalt ");

			Timestamp dataInicioTS = new Timestamp(dataInicio.getTimeInMillis());
			Timestamp dataFimTS = new Timestamp(dataFim.getTimeInMillis());
			PreparedStatement st = connVetorh.prepareStatement(queryColaborador.toString());
			st.setTimestamp(1, dataInicioTS);
			st.setTimestamp(2, dataFimTS);

			ResultSet rs = st.executeQuery();
			
			while (rs.next())
			{
				HoraExtraVO hora = new HoraExtraVO();
				
				hora.setNumemp(rs.getLong("numemp"));
				hora.setNumcad(rs.getLong("numcad"));
				hora.setCodfil(rs.getLong("codfil"));
				hora.setNomfun(rs.getString("nomfun"));

				Timestamp tsInicio = rs.getTimestamp("usu_datalt");
				GregorianCalendar gDataInicio = new GregorianCalendar();
				gDataInicio.setTimeInMillis(tsInicio.getTime());
				hora.setgDataI(gDataInicio);
				
				Timestamp tsFim = rs.getTimestamp("usu_datfim");
				GregorianCalendar gDataFim = new GregorianCalendar();
				gDataFim.setTimeInMillis(tsFim.getTime());
				hora.setgDataF(gDataFim);
				
				hora.setEscala(rs.getLong("codesc"));
				hora.setTurma(rs.getLong("codtma"));
				hora.setDescricaoEscala(rs.getString("nomesc"));
				hora.setGrupoEscala(rs.getLong("usu_gruesc"));
				hora.setGrupoEscalaDescricao(rs.getString("usu_desgruesc"));
				hora.setHorasSemana(rs.getLong("HorSem"));
				hora.setSindicato(rs.getLong("CodSin"));
				hora.setHorario(rs.getLong("CodHor"));
				hora.setDescricaoHorario(rs.getString("DesHor"));
				
				horas.add(hora);
			}
			rs.close();
			st.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				connVetorh.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();

			}
		}

		return horas;
	}
	
	private Integer defineEvento(HoraExtraVO h, Long minutosDiurnos)
	{
		Integer porcentagem = 0;
		
		/**
		 *  1.Empresa 21 (Metropolitana)
		 */
		if (h.getNumemp() == 21L)
		{
			/**
			 *  1.1.Operacional (Filial diferente de 101)
			 */
			if (h.getCodfil() != 101L || h.getCodfil() != 1L) 
			{
				/**
				 *  1.1.1.Sindicato 51 e 52 (Sorocaba):
				 */
				if (h.getSindicato() == 51 || h.getSindicato() == 52)
				{
					/**
					 *  1.1.1.1.Escalas 12x36:
					 *  Recebe 60% exceto no feriado que recebe 100%. Nas escalas de um dia para o outro recebe o relativo ao dia do feriado.
					 */
					if (h.getGrupoEscala() == 1L || h.getGrupoEscala() == 2L || h.getGrupoEscala() == 7L || h.getGrupoEscala() == 11L || 
						h.getGrupoEscala() == 12L || h.getGrupoEscala() == 14L || h.getGrupoEscala() == 15L)
					{
						if (h.getHorario() == 9997L)
						{
							//Feriado 100%
							porcentagem = 200;
						}
						else
						{
							porcentagem = 60;
						}
					}
					else
					/**
					 *  1.1.1.2.Demais escalas:	
					 *  Quando trabalhado no Feriado ou DSR recebe 100%, senão 60%.
					 */
					{
						if (h.getHorario() == 9997L)
						{
							//Feriado 100%
							porcentagem = 200;
						}
						else if (h.getHorario() != 9999L)
						{
							porcentagem = 100;
						}
						else
						{
							porcentagem = 60;
						}
					}
				}
				else
				/**
				 *  1.1.2.Demais Sindicatos	
				 */
				{
					/**
					 *  1.1.2.1.Escalas 12x36 e SDF:
					 *  Recebe sempre 50%;
					 */
					if (h.getGrupoEscala() == 1L || h.getGrupoEscala() == 2L || h.getGrupoEscala() == 7L || h.getGrupoEscala() == 11L || 
						h.getGrupoEscala() == 12L || h.getGrupoEscala() == 13L || h.getGrupoEscala() == 14L || h.getGrupoEscala() == 15L)
					{
						if (h.getHorario() == 9997L)
						{
							//Feriado 50%
							porcentagem = 150;
						}
						else
						{
							porcentagem = 50;
						}
					}
					else
					/**
					 *  1.1.2.2.Demais escalas:	
					 *  Quando trabalhado no Feriado ou DSR recebe 100%, senão 50%.
					 */
					{
						if (h.getHorario() == 9997L)
						{
							porcentagem = 200;
						}
						else if (h.getHorario() == 9999L)
						{
							porcentagem = 100;
						}
						else
						{
							porcentagem = 50;
						}
					}
				}
			}
			else
			/**
			 *  1.2.ADM (Filial 101)	
			 *  1.2.1.Todos os Sindicatos
			 */
			{
				/**
				 *  1.2.1.1.Escalas 12x36 e SDF:
				 *  Recebe sempre 50%;
				 *  		
				 */
				if (h.getGrupoEscala() == 1L || h.getGrupoEscala() == 2L || h.getGrupoEscala() == 7L || h.getGrupoEscala() == 11L || 
					h.getGrupoEscala() == 12L || h.getGrupoEscala() == 13L || h.getGrupoEscala() == 14L || h.getGrupoEscala() == 15L)
				{
					if (h.getHorario() == 9997L)
					{
						porcentagem = 150;
					}
					else
					{
						porcentagem = 50;
					}
				}
				else
				/**
				 *  1.2.1.2.Demais escalas:	
				 *  Quando trabalhado no Feriado ou DSR recebe 100%, senão 50%.
				 */
				{
					if (h.getHorario() == 9997L)
					{
						porcentagem = 200;
					}
					else if (h.getHorario() == 9999L)
					{
						porcentagem = 100;
					}
					else
					{
						porcentagem = 50;
					}
				}
			}
		}	
		/**
		 * 2.Empresa 22 (Objetiva)
		 * As primeiras 2 horas recebem 50%, nas demais 100%.
		 * Não Recebe HE no feriado trabalhado.
		 */
		else if (h.getNumemp() == 22L)
		{
			if (minutosDiurnos > 120L)
			{
				//2 horas 50% e demais 100%
				porcentagem = 50100;
			}
			else
			{
				porcentagem = 50;
			}
		}
		return porcentagem;
	}
	
	private void addListaHE(List<HoraExtraVO> horas, HoraExtraVO h, Long refeve, Long codEvento, String desEvento, Long codCal21, Long codCal22)
	{
		HoraExtraVO hora = new HoraExtraVO();
		hora.setNumcad(h.getNumcad());
		hora.setNumemp(h.getNumemp());
		hora.setNomfun(h.getNomfun());
		hora.setCodfil(h.getCodfil());
		hora.setDataI(NeoUtils.safeDateFormat(h.getgDataI()));
		hora.setDataF(NeoUtils.safeDateFormat(h.getgDataF()));
		hora.setCodeve(codEvento);
		hora.setDescricaoEvento(desEvento);
		hora.setRefeve(refeve);
		hora.setEscala(h.getEscala());
		hora.setTurma(h.getTurma());
		hora.setHorario(h.getHorario());
		hora.setDescricaoHorario(h.getDescricaoHorario());
		hora.setDescricaoEscala(h.getDescricaoEscala());
		hora.setGrupoEscala(h.getGrupoEscala());
		hora.setGrupoEscalaDescricao(h.getGrupoEscalaDescricao());
		hora.setSindicato(h.getSindicato());
		
		switch (h.getNumemp().intValue())
		{
			case 21:
				hora.setCalculo(codCal21);
				break;

			case 22:
				hora.setCalculo(codCal22);
				break;
		}
		int hor = hora.getRefeve().intValue() / 60;
		int min = hora.getRefeve().intValue() % 60;
		
		hora.setHorasExtras(String.format("%02d", hor) + ":" + String.format("%02d", min));

		if (!horas.contains(hora))
		{
			horas.add(hora);
		}
	}

	private Long buscaCalculo(Long numemp, String competencia)
	{
		Connection connVetorh = PersistEngine.getConnection("VETORHPROD");
		Long codCal = 0L;

		try
		{
			GregorianCalendar periodo = OrsegupsUtils.stringToGregorianCalendar("01/" + competencia, "dd/MM/yyyy");
			
			StringBuffer queryColaborador = new StringBuffer();
			queryColaborador.append(" SELECT cal.CodCal FROM R044CAL cal ");
			queryColaborador.append(" WHERE cal.TipCal = 11 AND cal.NumEmp = ? AND cal.PerRef = ? ");

			PreparedStatement st = connVetorh.prepareStatement(queryColaborador.toString());
			st.setLong(1, numemp);
			st.setTimestamp(2, new Timestamp(periodo.getTimeInMillis()));

			ResultSet rs = st.executeQuery();

			if (rs.next())
			{
				codCal = rs.getLong("CodCal");
			}

			rs.close();
			st.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				connVetorh.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();

			}
		}
		return codCal;
	}

	private void importarVA()
	{
		try
		{
			String competencia = "10/2015";
			GregorianCalendar dataInicioCompetencia = OrsegupsUtils.getDiaInicioCompetencia(competencia);
			GregorianCalendar dataFimCompetencia = OrsegupsUtils.getDiaFimCompetencia(competencia);
			
			List<HoraExtraVO> horas = new ArrayList<HoraExtraVO>();
			List<HoraExtraVO> horasFinal = new ArrayList<HoraExtraVO>();
			horas = listarHE(dataInicioCompetencia, dataFimCompetencia);
			
			for (HoraExtraVO h : horas)
			{
				Long minutos = (h.getgDataF().getTimeInMillis() - h.getgDataI().getTimeInMillis()) / 1000 / 60;

				if (minutos >= 360)
				{
					HoraExtraVO hora = new HoraExtraVO();
					hora.setNumemp(h.getNumemp());
					hora.setNumcad(h.getNumcad());
					hora.setDataI(NeoUtils.safeDateFormat(h.getgDataI(), "dd/MM/yyyy"));
					hora.setRefeve(1L);
					horasFinal.add(hora);
				}
			}
			
			for (int i = 0; i < horasFinal.size() - 1; i++)
			{
				if (i > 0 && horasFinal.get(i).getNumemp().equals(horasFinal.get(i - 1).getNumemp()) && horasFinal.get(i).getNumcad().equals(horasFinal.get(i - 1).getNumcad()))
				{
					horasFinal.get(i).setRefeve(horasFinal.get(i - 1).getRefeve() + horasFinal.get(i).getRefeve());
					horasFinal.remove(i - 1);
					i--;
				}
			}

			for (HoraExtraVO h : horasFinal)
			{
				System.out.println(h.getNumemp() + ";1;" + h.getNumcad() + ";" + h.getRefeve());
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}