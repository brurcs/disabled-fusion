package com.neomind.fusion.custom.orsegups.rotinaInadimplencia.vo;

import java.util.List;

public class ClienteInadimplenteVO {
    
    private int codigoCliente;
    private String nomeCliente;
    private String siglaEmpresa;
        
    private List<ContratoVO> listaContratos;
    
    public int getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(int codigoCliente) {
        this.codigoCliente = codigoCliente;
    }


    public List<ContratoVO> getListaContratos() {
        return listaContratos;
    }

    public void setListaContratos(List<ContratoVO> listaContratos) {
        this.listaContratos = listaContratos;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public String getSiglaEmpresa() {
        return siglaEmpresa;
    }

    public void setSiglaEmpresa(String siglaEmpresa) {
        this.siglaEmpresa = siglaEmpresa;
    }
    
    

}
