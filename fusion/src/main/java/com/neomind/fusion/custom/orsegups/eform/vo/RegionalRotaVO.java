package com.neomind.fusion.custom.orsegups.eform.vo;

public class RegionalRotaVO {
    
    private Integer cdRegionalRota;
    private String nmRegionalRota;
    public Integer getCdRegionalRota() {
        return cdRegionalRota;
    }
    public void setCdRegionalRota(Integer cdRegionalRota) {
        this.cdRegionalRota = cdRegionalRota;
    }
    public String getNmRegionalRota() {
        return nmRegionalRota;
    }
    public void setNmRegionalRota(String nmRegionalRota) {
        this.nmRegionalRota = nmRegionalRota;
    }

}
