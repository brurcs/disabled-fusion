package com.neomind.fusion.custom.orsegups.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

@WebServlet(name="RamalServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.servlets.RamalServlet"})
public class RamalServlet extends HttpServlet
{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}
	
	private void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String ramalOrigem = request.getParameter("ramaOrigem");
		String ramalDestino = request.getParameter("ramalDestino");
		
		
		/**
		 * Adição do novo digito para telefones móveis
		 */
		List<NeoObject> listaSubstituicao = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TINonoDigito"));

		if (listaSubstituicao != null && !listaSubstituicao.isEmpty()) {

		    NeoObject objAdicionar = listaSubstituicao.get(0);

		    EntityWrapper wAdicionar = new EntityWrapper(objAdicionar);
		    
		    boolean adicionar = (boolean) wAdicionar.findField("adicionar").getValue();
		    			    
		    if(adicionar){
			
			if (ramalDestino.startsWith("0")){
			    ramalDestino = ramalDestino.substring(1);
			}
			
			if (ramalDestino.length() == 8){
			    String subFone = ramalDestino.substring(1);
			    
			    if(subFone.startsWith("7") || subFone.startsWith("8") || subFone.startsWith("9")){
				subFone = "9"+subFone;
				ramalDestino = ramalDestino.substring(0, 0)+subFone;
			    }
	    
			}else if (ramalDestino.length() == 10){
			    String subFone = ramalDestino.substring(2);
			    
			    if(subFone.startsWith("7") || subFone.startsWith("8") || subFone.startsWith("9")){
				subFone = "9"+subFone;
				ramalDestino = ramalDestino.substring(0, 2)+subFone;
			    }			
			}
			
		    }
		}
		
		
		String url = "http://192.168.20.241/snep/services/?service=Dial&ramal="+ramalOrigem+"&exten="+ramalDestino;
		
		if(ramalDestino != null && !ramalDestino.isEmpty() && ramalOrigem != null && !ramalOrigem.isEmpty())
		{
			String message;
			
			if(ramalDestino.equals(ramalOrigem))
			{
				message = "Erro ao efetuar chamada.\nRamal de origem igual ao ramal de destino.";
			}
			else
			{
				StringBuffer buffer = this.callUrl(url);
				
				String result = buffer.toString();
				result = result.replaceAll("\\{", "");
				result = result.replaceAll("\\}", "");
				String[] params = result.split(",");
				
				
				if(params[0] != null && params[0].contains("ok"))
				{
					message = "Chamada efetuada a partir do ramal "+ramalOrigem+" para o ramal "+ramalDestino+".";
				}
				else
				{
					String erro[] = params[1].split(":"); 
					
					message = "Erro ao efetuar chamada.\n"+erro[1];
				}
			}
			
			PrintWriter out = response.getWriter();
			out.print(message);
			out.close();
		}
	}
	
	private StringBuffer callUrl(String url)
	{
		StringBuffer inputLine = new StringBuffer();

		try
		{
			URL murl = new URL(url);
			URLConnection murlc = murl.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(murlc.getInputStream()));

			char letter;

			while ((letter = (char) in.read()) != '\uFFFF')
			{
				inputLine.append(letter);
			}

			in.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return inputLine;
	}
}
