package com.neomind.fusion.custom.orsegups.adapter.q004;

import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.SecurityEntity;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class AlterarResponsavel implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		Long nvEscala = processEntity.findGenericValue("nivelEscalada");

		SecurityEntity seResponsavel = (SecurityEntity) (origin != null ? origin.getUser() : processEntity.findGenericValue("tarefaSimples.Solicitante"));

		seResponsavel = origin.getUser().getGroup().getUpperLevel().getResponsible();
		
		if (seResponsavel.getName().contains("Presidente")){
			
		}

		processEntity.setValue("nivelEscalada", ++nvEscala);
		processEntity.setValue("responsavelAprovacao", seResponsavel);
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}

}
