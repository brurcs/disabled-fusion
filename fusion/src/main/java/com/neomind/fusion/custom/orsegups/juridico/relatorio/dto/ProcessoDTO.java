package com.neomind.fusion.custom.orsegups.juridico.relatorio.dto;

public class ProcessoDTO {

	String autor;
	String centroDeCusto;
	String centroDeCustoSit;
	String centroDeCustoManual;
	String centroDeCustoManualSit;
	String nroProcesso;
	String dataCitacao;
	String dataFinalizacao;
	String mesesDiff;//tempo entre abertura e finaliza��o
	String nroTarefa;
	String escritorio;
	
	public ProcessoDTO() {
		
	}

	public ProcessoDTO(String autor,String centroDeCusto,String centroDeCustoSit, String centroDeCustoManual, String centroDeCustoManualSit, String nroProcesso, String dataCitacao, String dataFinalizacao, String mesesDiff, String nroTarefa, String escritorio) {
		super();
		this.autor = autor;
		this.centroDeCusto = centroDeCusto;
		this.centroDeCustoSit = centroDeCustoSit;
		this.centroDeCustoManual = centroDeCustoManual;
		this.centroDeCustoManualSit = centroDeCustoManualSit;
		this.nroProcesso = nroProcesso;
		this.dataCitacao = dataCitacao;
		this.dataFinalizacao = dataFinalizacao;
		this.mesesDiff = mesesDiff;
		this.nroTarefa = nroTarefa;
		this.escritorio = escritorio;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getNroProcesso() {
		return nroProcesso;
	}

	public void setNroProcesso(String nroProcesso) {
		this.nroProcesso = nroProcesso;
	}

	public String getDataCitacao() {
		return dataCitacao;
	}

	public void setDataCitacao(String dataCitacao) {
		this.dataCitacao = dataCitacao;
	}

	public String getDataFinalizacao() {
		return dataFinalizacao;
	}

	public void setDataFinalizacao(String dataFinalizacao) {
		this.dataFinalizacao = dataFinalizacao;
	}

	public String getMesesDiff() {
		return mesesDiff;
	}

	public void setMesesDiff(String mesesDiff) {
		this.mesesDiff = mesesDiff;
	}

	public String getNroTarefa() {
		return nroTarefa;
	}

	public void setNroTarefa(String nroTarefa) {
		this.nroTarefa = nroTarefa;
	}

	public String getCentroDeCusto() {
		return centroDeCusto;
	}

	public void setCentroDeCusto(String centroDeCusto) {
		this.centroDeCusto = centroDeCusto;
	}

	public String getCentroDeCustoSit() {
	    return centroDeCustoSit;
	}

	public void setCentroDeCustoSit(String centroDeCustoSit) {
	    this.centroDeCustoSit = centroDeCustoSit;
	}

	public String getCentroDeCustoManual() {
	    return centroDeCustoManual;
	}

	public void setCentroDeCustoManual(String centroDeCustoManual) {
	    this.centroDeCustoManual = centroDeCustoManual;
	}

	public String getCentroDeCustoManualSit() {
	    return centroDeCustoManualSit;
	}

	public void setCentroDeCustoManualSit(String centroDeCustoManualSit) {
	    this.centroDeCustoManualSit = centroDeCustoManualSit;
	}

	public String getEscritorio() {
	    return escritorio;
	}

	public void setEscritorio(String escritorio) {
	    this.escritorio = escritorio;
	}
	
}
