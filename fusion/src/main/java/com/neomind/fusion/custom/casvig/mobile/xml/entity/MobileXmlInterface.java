package com.neomind.fusion.custom.casvig.mobile.xml.entity;

import java.util.Map;

import org.w3c.dom.Document;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.InstantiableEntityInfo;

/**
 * Classe que recebe os XML e isntancia os seus dados num NeoObject, podendo dar
 * prosseguimento num workflow.
 * 
 * @author Daniel Henrique Joppi
 * 
 */
public interface MobileXmlInterface
{
	/**
	 * Cria nova instancia da interface.
	 * 
	 * @param entityInfo
	 * do E-Form a ser criado.
	 * @return nova instancia do E-Form.
	 */
	public NeoObject createNewInstance(InstantiableEntityInfo entityInfo);

	/**
	 * Manupula o document passado e pesiste as informações deste xml no E-Form.
	 * 
	 * @param document
	 * xml que contêm informações a serem inseridas no E-Form.
	 * @return E-Form já persistido.
	 */
	public NeoObject setInstance(Document document);

	/**
	 * Inicia um novo workflow.
	 * 
	 * @return se não ocorrer nenhum erro retorna true.
	 */
	public boolean startProcess();

	/**
	 * Finaliza uma Atividade de sistema.
	 * 
	 * @return se não ocorrer nenhum erro retorna true.
	 */
	public boolean finishActivity();
}
