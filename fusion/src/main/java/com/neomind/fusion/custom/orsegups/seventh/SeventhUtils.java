package com.neomind.fusion.custom.orsegups.seventh;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.seventh.bean.EventoArmeDesarme;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

/**
 * Metodos utilitários da plataforma Seventh D-Guard
 * 
 * @author mateus.batista
 * @since 08/11/2016
 * @version 1.0
 */
public class SeventhUtils {

    private final Log log = LogFactory.getLog(SeventhUtils.class);

    /**
     * Inicia a gravação de todas as cameras de uma determinada conta
     * 
     * @param servidor
     * @param usuario
     * @param senha
     * @param empresa
     * @param idCentral
     * @return
     * @throws IOException
     */
    public String iniciarGravacaoTodasCameras(EventoArmeDesarme evento) throws IOException {

	StringBuilder resultado = new StringBuilder();

	resultado.append("=============================== \r\n");

	resultado.append("Iniciar gravação " + evento.getIdCentral() + "[" + evento.getParticao() + "] Empresa: " + evento.getIdEmpresa() + " \r\n");

	int tempoTotal = 0;

	long inicio3 = Calendar.getInstance().getTimeInMillis();

	long fim3 = Calendar.getInstance().getTimeInMillis();

	resultado.append("Tempo para obter câmeras: " + (fim3 - inicio3) + "milessimos de segundo. \r\n");

	tempoTotal += (fim3 - inicio3);

	Map<String, String> camerasMap = this.getCameraCliente(evento);

	if (camerasMap != null && !camerasMap.isEmpty()) {
	    for (Entry<String, String> entry : camerasMap.entrySet()) {

		String camera = "";
		if (entry != null) {
		    camera = String.valueOf(entry.getKey());

		    long inicio = Calendar.getInstance().getTimeInMillis();

		    String retorno = this.iniciarGravacao(evento, camera);

		    long fim = Calendar.getInstance().getTimeInMillis();

		    resultado.append("Camera: " + camera + ". Retorno: " + retorno + ". Tempo: " + (fim - inicio) + " milessimos de segundo. \r\n");

		    tempoTotal += (fim - inicio);

		    int maxT = 0;

		    while (!retorno.equals("OK") && maxT < 2) {

			long inicio2 = Calendar.getInstance().getTimeInMillis();

			retorno = this.iniciarGravacao(evento, camera);

			long fim2 = Calendar.getInstance().getTimeInMillis();

			resultado.append((maxT + 2) + "º Tentativa Camera: " + camera + ". Retorno: " + retorno + ". Tempo: " + (fim2 - inicio2) + " milessimos de segundo. \r\n");

			tempoTotal += (fim2 - inicio2);

			maxT++;
			log.error("SeventhUtils - ERRO iniciarGravacao - Servidor:" + evento.getServidorCFTV() + " ID Central:" + evento.getIdCentral() + " Empresa:" + evento.getIdEmpresa() + " Câmera:" + camera + " - Data: "
				+ NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		    }
		    //TODO ADICIONADO BREAK EM CARATER DE TESTE. SEGUNDO INFORMAÇÃO RECEBIDA, É NECESSÁRIO ENVIAR O COMANDO APENAS PARA UMA CAMERA
		    if (retorno.equals("OK")){
			break;
		    }
		}
	    }
	    resultado.append("\r\n\r\n");
	    resultado.append("Tempo total: " + tempoTotal / 1000 + " segundos. \r\n");

	    resultado.append("=============================== \r\n");

	}

	return resultado.toString();
    }

    /**
     * Pausa a gravação de todas as cameras de uma determinada conta
     * 
     * @param servidor
     * @param usuario
     * @param senha
     * @param empresa
     * @param idCentral
     * @return
     * @throws IOException
     */
    public String pausarGravacaoTodasCameras(EventoArmeDesarme evento) throws IOException {

	StringBuilder resultado = new StringBuilder();

	resultado.append("=============================== \r\n");
	resultado.append("Pausar gravação " + evento.getIdCentral() + "[" + evento.getParticao() + "] Empresa: " + evento.getIdEmpresa() + " \r\n ");

	int tempoTotal = 0;

	long inicio3 = Calendar.getInstance().getTimeInMillis();

	Map<String, String> camerasMap = this.getCameraCliente(evento);

	long fim3 = Calendar.getInstance().getTimeInMillis();

	resultado.append("Tempo para obter câmeras: " + (fim3 - inicio3) + " milessimos de segundo. \r\n");

	tempoTotal += (fim3 - inicio3);

	if (camerasMap != null && !camerasMap.isEmpty()) {

	    resultado.append("\r\n\r\n");
	    resultado.append("Tempo para pausar cada câmera. \r\n");

	    for (Entry<String, String> entry : camerasMap.entrySet()) {

		String camera = "";
		if (entry != null) {
		    camera = String.valueOf(entry.getKey());

		    long inicio = Calendar.getInstance().getTimeInMillis();

		    String retorno = this.pausarGravacao(evento, camera);

		    long fim = Calendar.getInstance().getTimeInMillis();

		    resultado.append("Camera: " + camera + ". Retorno: " + retorno + ". Tempo: " + (fim - inicio) + " milessimos de segundo. \r\n");

		    tempoTotal += (fim - inicio);

		    int maxT = 0;

		    while (!retorno.equals("OK") && maxT < 2) {
			long inicio2 = Calendar.getInstance().getTimeInMillis();

			retorno = this.pausarGravacao(evento, camera);

			long fim2 = Calendar.getInstance().getTimeInMillis();

			resultado.append((maxT + 2) + "º Tentativa Camera: " + camera + ". Retorno: " + retorno + ". Tempo: " + (fim2 - inicio2) + " milessimos de segundo. \r\n");

			tempoTotal += (fim2 - inicio2);
			maxT++;
			log.error("SeventhUtils - ERRO pausarGravacao - Servidor:" + evento.getServidorCFTV() + " ID Central:" + evento.getIdCentral() + " Empresa:" + evento.getIdEmpresa() + " Câmera:" + camera + " - Data: "
				+ NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		    }
		    //TODO ADICIONADO BREAK EM CARATER DE TESTE. SEGUNDO INFORMAÇÃO RECEBIDA, É NECESSÁRIO ENVIAR O COMANDO APENAS PARA UMA CAMERA
		    
		    if (retorno.equals("OK")){
			break;
		    }
		    
		}
	    }

	    resultado.append("\r\n\r\n");
	    resultado.append("Tempo total: " + tempoTotal / 1000 + " segundos. \r\n");
	    resultado.append("=============================== \r\n");
	}

	return resultado.toString();
    }

    /**
     * Inicia a gravação de uma camera a partir de seu ID
     * 
     * @param servidor
     * @param usuario
     * @param senha
     * @param idCamera
     * @return String - OK para sucesso - CAMERAINDISPONIVEL para falha
     * @throws IOException
     */
    public String iniciarGravacao(EventoArmeDesarme evento, String idCamera) throws IOException {

	String resposta = "";
	BufferedReader in = null;

	try {
	    String userpass = evento.getUsuarioCFTV() + ":" + evento.getSenhaCFTV();
	    String basicAuth = "Basic " + new String(new Base64().encode(userpass.getBytes()));

	    String prefixo = "http://";

	    URL url = new URL(prefixo + evento.getServidorCFTV() + "/camerasetgravacao.cgi?camera=" + idCamera + "&estado=on&ininterrupta=on");
	    URLConnection conn = url.openConnection();
	    conn.setRequestProperty("Authorization", basicAuth);

	    in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	    String inputLine;

	    while ((inputLine = in.readLine()) != null) {
		resposta = inputLine;
	    }
	    in.close();

	} catch (Exception e) {
	    this.registrarErro("Operação: Iniciar gravação <br> " + e.getMessage(), evento);
	    log.error("SeventhUtils - ERRO iniciarGravacao - Servidor:" + evento.getServidorCFTV() + " Câmera:" + idCamera + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	    e.printStackTrace();
	} finally {
	    if (in != null) {
		in.close();
	    }
	}
	if (!resposta.equals("OK")) {
	    this.registrarErro("Operação: Iniciar gravação <br> " + resposta, evento);
	    log.error("SeventhUtils - ERRO iniciarGravacao - Servidor:" + evento.getServidorCFTV() + " Câmera:" + idCamera + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	}
	return resposta;
    }

    /**
     * Pausa a gravação de uma câmera a partir de seu ID
     * 
     * @param servidor
     * @param usuario
     * @param senha
     * @param idCamera
     * @return String - OK para sucesso - CAMERAINDISPONIVEL para falha
     * @throws IOException
     */
    public String pausarGravacao(EventoArmeDesarme evento, String idCamera) throws IOException {
	String resposta = "";
	BufferedReader in = null;

	try {
	    String userpass = evento.getUsuarioCFTV() + ":" + evento.getSenhaCFTV();
	    String basicAuth = "Basic " + new String(new Base64().encode(userpass.getBytes()));

	    String prefixo = "http://";

	    URL url = new URL(prefixo + evento.getServidorCFTV() + "/camerasetgravacao.cgi?camera=" + idCamera + "&estado=off&ininterrupta=on");
	    URLConnection conn = url.openConnection();
	    conn.setRequestProperty("Authorization", basicAuth);

	    in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	    String inputLine;

	    while ((inputLine = in.readLine()) != null) {
		resposta = inputLine;
	    }
	    in.close();

	} catch (Exception e) {
	    this.registrarErro("Operação: Pausar gravação <br> " + e.getMessage(), evento);
	    log.error("SeventhUtils - ERRO pausarGravacao - Servidor:" + evento.getServidorCFTV() + " Câmera:" + idCamera + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	    e.printStackTrace();
	} finally {
	    if (in != null) {
		in.close();
	    }
	}
	if (!resposta.equals("OK")) {
	    this.registrarErro("Operação: Pausar gravação <br> " + resposta, evento);
	    log.error("SeventhUtils - ERRO pausarGravacao - Servidor:" + evento.getServidorCFTV() + " Câmera:" + idCamera + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	}
	return resposta;
    }

    /**
     * Metodo para retornar as cameras vinculadas a um cliente
     * 
     * @param servidor
     * @param empresa
     * @param usario
     * @param senha
     * @param senha
     * @return retorna mapa com chave id da camera e valor nome da camera
     * @throws IOException
     */
    public TreeMap<String, String> getCameraCliente(EventoArmeDesarme evento) throws IOException {

	String resposta = "";

	TreeMap<String, String> treeMap = null;

	BufferedReader in = null;

	try {
	    String userpass = evento.getUsuarioCFTV() + ":" + evento.getSenhaCFTV();
	    String basicAuth = "Basic " + new String(new Base64().encode(userpass.getBytes()));

	    String prefixo = "http://";

	    URL url = new URL(prefixo + evento.getServidorCFTV() + "/camerasnomes.cgi?receiver=" + evento.getIdEmpresa() + "&server=" + evento.getIdCentral());
	    URLConnection conn = url.openConnection();
	    conn.setRequestProperty("Authorization", basicAuth);
	    conn.setConnectTimeout(20 * 1000);
	    in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	    String inputLine;

	    while ((inputLine = in.readLine()) != null) {
		resposta = inputLine;
	    }
	    in.close();
	    if (resposta != null && !resposta.isEmpty() && resposta.contains("=")) {
		String array[] = resposta.split("&");
		treeMap = new TreeMap<String, String>();

		for (String valor : array) {
		    if (valor != null && !valor.isEmpty() && valor.contains("=")) {
			String parametros[] = valor.split("=");
			treeMap.put(parametros[0], (String) parametros[1]);
		    }

		}
	    }
	} catch (Exception e) {
	    this.registrarErro("Operação: Obter câmeras <br> " + e.getMessage(), evento);
	    log.error("SeventhUtils - ERRO getCameraCliente - Servidor:" + evento.getServidorCFTV() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	    e.printStackTrace();
	} finally {
	    if (in != null) {
		in.close();
	    }
	}

	return treeMap;
    }

    /**
     * Metodo para registrar erros ao buscar dados dos servidores D-Guard
     * 
     * @param message
     * @param servidor
     * @param usuario
     * @param senha
     * @param idCentral
     * @param empresa
     */
    private void registrarErro(String message, EventoArmeDesarme evento) {

	Connection conn = null;
	PreparedStatement pstm = null;

	String sql = "INSERT INTO GravacaoOnDemandLogs (MENSAGEM, SERVIDOR, USUARIO, SENHA, ID_CENTRAL, EMPRESA, DATA_ERRO, PARTICAO) VALUES (?,?,?,?,?,?,?,?)";

	try {
	    conn = PersistEngine.getConnection("TIDB");

	    pstm = conn.prepareStatement(sql);

	    pstm.setString(1, message);
	    pstm.setString(2, evento.getServidorCFTV() + ":" + evento.getPortaCFTV());
	    pstm.setString(3, evento.getUsuarioCFTV());
	    pstm.setString(4, evento.getSenhaCFTV());
	    pstm.setString(5, evento.getIdCentral());
	    pstm.setInt(6, Integer.parseInt(evento.getIdEmpresa()));
	    pstm.setTimestamp(7, new Timestamp(Calendar.getInstance().getTimeInMillis()));
	    pstm.setString(8, evento.getParticao());

	    pstm.executeUpdate();

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}

    }

    /**
     * Metodo para obter o layout vinculados a uma conta SIGMA
     * 
     * @param servidor
     * @param usuario
     * @param senha
     * @param idCentral
     * @return Nome do layout vinculado a conta SIGMA
     */
    public String getLayoutCliente(String servidor, String usuario, String senha, String idCentral) {

	String urlStr = "http://" + servidor + "/layoutslist.cgi";

	String retorno = "";
	try {
	    TreeMap<String, String> treeMapLayout;

	    treeMapLayout = returnMap(urlStr, usuario, senha);

	    if (treeMapLayout != null && !treeMapLayout.isEmpty() && treeMapLayout.containsKey(idCentral)) {

		retorno = treeMapLayout.get(idCentral);
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	    log.error("SeventhUtils - ERRO getLayoutCliente - Servidor:" + servidor + " Central: " + idCentral + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	}

	return retorno;
    }

    /**
     * Metodo que monta o mapa contendo os layouts vinculados a um servidor
     * 
     * @param urlStr
     * @param usuario
     * @param senha
     * @return Map chave ID_CENTRAL e valor Nome Layout
     * @throws IOException
     */
    private TreeMap<String, String> returnMap(String urlStr, String usuario, String senha) throws IOException {

	TreeMap<String, String> map = null;

	String resposta = "";
	URL url = null;
	HttpURLConnection conn = null;
	BufferedReader in = null;

	try {
	    String userpass = usuario + ":" + senha;
	    String basicAuth = "Basic " + new String(new Base64().encode(userpass.getBytes()));

	    url = new URL(urlStr.trim());
	    conn = (HttpURLConnection) url.openConnection();
	    conn.setRequestProperty("Authorization", basicAuth);
	    if (conn != null && conn.getResponseCode() == 200) {
		in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String inputLine;
		while ((inputLine = in.readLine()) != null) {
		    resposta = inputLine;
		}
		in.close();
		if (resposta != null && !resposta.isEmpty() && resposta.contains("=")) {
		    String array[] = resposta.split("&");
		    map = new TreeMap<String, String>();
		    for (String valor : array) {
			if (valor != null && !valor.isEmpty() && valor.contains("=")) {
			    String parametros[] = valor.split("=");
			    String key[] = parametros[1].split("-");
			    if (parametros.length > 1 && key.length > 1)
				map.put((String) key[1], (String) parametros[1]);
			}
		    }
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    throw new IOException();
	} finally {
	    if (in != null) {
		in.close();
	    }
	}

	return map;
    }

    /**
     * Retorna lista de servidores habilitados para gravação; nome do servidor
     * será cercado por aspas simples. '192.168.0.1'
     * 
     * @return Lista de servidores
     */
    public static List<String> getServidoresHabilitados() {

	List<NeoObject> listaServidores = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("seventhServidoresHabilitados"));

	List<String> retorno = new ArrayList<String>();

	if (listaServidores != null && !listaServidores.isEmpty()) {
	    for (NeoObject servidor : listaServidores) {

		EntityWrapper wrappperExcecao = new EntityWrapper(servidor);

		String sv = (String) wrappperExcecao.findField("servidor").getValue();

		retorno.add("'" + sv + "'");

	    }
	}

	return retorno;
    }

    /**
     * Retorna hostname vinculado ao IP do servidor
     * @param servidor
     * @return
     */
    public static String getServerHostname(String servidor) {

	List<NeoObject> listaServidores = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("seventhServidoresHabilitados"), new QLEqualsFilter("servidor", servidor));

	if (listaServidores != null && !listaServidores.isEmpty()) {
	    NeoObject o = listaServidores.get(0);

	    EntityWrapper wrappperExcecao = new EntityWrapper(o);

	    String hostname = (String) wrappperExcecao.findField("hostname").getValue();

	    return hostname;

	} else {
	    return null;
	}

    }

}
