package com.neomind.fusion.custom.orsegups.site;

public enum LinkActionsEnum
{
	RPS(1, "rps"),NFSE(2, "nfse"),BOLETO(3,"boleto"),XML(4,"xml"),NFSED(5,"NFSED"),DARFPCC(6,"DARFPCC"),DARFIRRF(7,"DARFIRRF"),GPS(8,"GPS");
	
	private int codigo;
	private String descricao;
	
	LinkActionsEnum( int codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	public int getCodigo() {
		return codigo;
	}
	
	public String getDescricao() {
		return descricao;
	}
}
