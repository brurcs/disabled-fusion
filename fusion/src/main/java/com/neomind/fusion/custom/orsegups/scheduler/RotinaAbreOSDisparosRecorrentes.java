package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;

public class RotinaAbreOSDisparosRecorrentes implements CustomJobAdapter {

    @Override
    public void execute(CustomJobContext ctx) {
		
		
 	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();
	
	sql.append(" SELECT CD_CLIENTE, CD_TECNICO_RESPONSAVEL, DISPAROS, TIPO FROM ( ");

	sql.append(" SELECT H.CD_CLIENTE, C.CD_TECNICO_RESPONSAVEL,COUNT(H.CD_HISTORICO_ALARME) AS DISPAROS, 1 AS TIPO ");
	sql.append(" FROM HISTORICO_ALARME H WITH(NOLOCK) ");
	sql.append(" INNER JOIN dbCENTRAL C WITH(NOLOCK) ON C.CD_CLIENTE = H.CD_CLIENTE ");
	sql.append(" LEFT JOIN COLABORADOR COL WITH(NOLOCK) ON C.CD_TECNICO_RESPONSAVEL = COL.CD_COLABORADOR ");
	sql.append(" WHERE C.CTRL_CENTRAL=1 ");
	sql.append(" AND H.DT_PROCESSADO >= GETDATE()-1 ");
	sql.append(" AND H.CD_EVENTO IN ('E120','E121','E122','E123','E124','E125','E130','E131','E132','E133','E134','E135','E136') ");
	sql.append(" AND (H.TX_OBSERVACAO_FECHAMENTO NOT LIKE 'Evento fechado automaticamente. Cliente em manutenção.%' AND H.CD_USUARIO_FECHAMENTO != 9999) ");
	sql.append(" AND (C.RAZAO NOT LIKE '%TRANSMISOR%' OR C.FANTASIA NOT LIKE '%TRANSMISOR%' ");
	sql.append(" AND (C.RAZAO NOT LIKE '%TRANSMISSOR%' OR C.FANTASIA NOT LIKE '%TRANSMISSOR%' ");
	sql.append(" AND (C.CD_TECNICO_RESPONSAVEL IS NULL OR COL.NM_COLABORADOR NOT LIKE '%-%TERC%') ");
	sql.append(" AND NOT EXISTS (SELECT 1 FROM dbORDEM OS WITH(NOLOCK) WHERE OS.CD_CLIENTE = H.CD_CLIENTE AND OS.IDOSDEFEITO = 1020 AND OS.FECHADO != 1) ");
	sql.append(" GROUP BY H.CD_CLIENTE, C.ID_CENTRAL, C.PARTICAO, C.ID_EMPRESA, C.CD_TECNICO_RESPONSAVEL ");
	sql.append(" HAVING COUNT(H.CD_HISTORICO_ALARME)>=10 ");

	sql.append(" UNION ");

	sql.append(" SELECT H.CD_CLIENTE, C.CD_TECNICO_RESPONSAVEL,COUNT(H.CD_HISTORICO_ALARME) AS DISPAROS, 2 AS TIPO ");
	sql.append(" FROM HISTORICO_ALARME H WITH(NOLOCK) ");
	sql.append(" INNER JOIN dbCENTRAL C WITH(NOLOCK) ON C.CD_CLIENTE = H.CD_CLIENTE ");
	sql.append(" LEFT JOIN COLABORADOR COL WITH(NOLOCK) ON C.CD_TECNICO_RESPONSAVEL = COL.CD_COLABORADOR ");
	sql.append(" WHERE C.CTRL_CENTRAL=1 ");
	sql.append(" AND H.DT_PROCESSADO >= (SELECT TOP 1 DT_FECHAMENTO FROM DBORDEM OS2 WITH(NOLOCK) WHERE OS2.CD_CLIENTE = H.CD_CLIENTE AND OS2.IDOSDEFEITO = 1020 AND OS2.FECHADO =1 ORDER BY DT_FECHAMENTO DESC) ");
	sql.append(" AND H.DT_PROCESSADO >= GETDATE()-5 ");
	sql.append(" AND H.CD_EVENTO IN ('E120','E121','E122','E123','E124','E125','E130','E131','E132','E133','E134','E135','E136') ");
	sql.append(" AND (H.TX_OBSERVACAO_FECHAMENTO NOT LIKE 'Evento fechado automaticamente. Cliente em manutenção.%' AND H.CD_USUARIO_FECHAMENTO != 9999) ");
	sql.append(" AND (C.CD_TECNICO_RESPONSAVEL IS NULL OR COL.NM_COLABORADOR NOT LIKE '%-%TERC%') ");
	sql.append(" AND (C.RAZAO NOT LIKE '%TRANSMISOR%' OR C.FANTASIA NOT LIKE '%TRANSMISOR%' ");
	sql.append(" AND (C.RAZAO NOT LIKE '%TRANSMISSOR%' OR C.FANTASIA NOT LIKE '%TRANSMISSOR%' ");
	sql.append(" AND NOT EXISTS (SELECT 1 FROM dbORDEM OS WITH(NOLOCK) WHERE OS.CD_CLIENTE = H.CD_CLIENTE AND OS.IDOSDEFEITO = 1020 AND OS.FECHADO != 1) ");
	sql.append(" GROUP BY H.CD_CLIENTE, C.ID_CENTRAL, C.PARTICAO, C.ID_EMPRESA, C.CD_TECNICO_RESPONSAVEL ");
	sql.append(" HAVING COUNT(H.CD_HISTORICO_ALARME)>=30 ");

	sql.append(" ) AS LISTA ORDER BY CD_CLIENTE, tipo ");

	try {
	    conn = PersistEngine.getConnection("SIGMA90");
	    pstm = conn.prepareStatement(sql.toString());

	    rs = pstm.executeQuery();

	    while (rs.next()) {
		int cdCliente = rs.getInt("CD_CLIENTE");
		int cdTecnico = rs.getInt("CD_TECNICO_RESPONSAVEL");
		int disparos = rs.getInt("DISPAROS");
		int tipo = rs.getInt("TIPO");
		
		this.abrirOrdemServico(cdCliente, cdTecnico, disparos, tipo);
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
	
    }
    
    private void abrirOrdemServico(int cdCliente, int cdTecnico, int disparos, int tipo){
	
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();
	sql.append(" INSERT INTO dbORDEM ");
	sql.append(" (CD_CLIENTE, ID_INSTALADOR, ABERTURA,  DEFEITO, OPABRIU, IDOSDEFEITO, FG_EMAIL_ENVIADO, CD_OS_SOLICITANTE, FG_TODAS_PARTICOES_EM_MANUTENCAO,  EXECUTADO,	TEMPOEXECUCAOPREVISTO )");
	sql.append(" VALUES(?,          ?,        GETDATE(), ?,       ?,       ?,           ?,                ?,                 ?,					?,		1 )");

	String defeito = "Verificar disparos recorrentes para a conta. ";
	
	if (tipo == 1){
	   defeito += "Conta teve "+disparos+" disparos nas últimas 24 horas.";
	}else {
	    defeito += "Conta teve "+disparos+" disparos nos últimos 5 dias.";
	}
	
	try {
	    conn = PersistEngine.getConnection("SIGMA90");
	    pstm = conn.prepareStatement(sql.toString());
	    
	    pstm.setInt(1, cdCliente);
	    pstm.setInt(2, cdTecnico);
	    pstm.setString(3, defeito);
	    pstm.setInt(4, 11010); //FUSION
	    pstm.setInt(5, 1020); // 1020 OS-DISPAROS RECORRENTES
	    pstm.setInt(6, 0); //FALSO	    
	    pstm.setInt(7, 10011); //5-DEMAIS ORDENS DE SERVIÇO
	    pstm.setInt(8, 0); //FALSO
	    pstm.setString(9, "");

	    pstm.executeUpdate();
	    
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
	

    }

}
