package com.neomind.fusion.custom.orsegups.presenca.vo;

public class JornadaPostoVO
{
	private Long numloc;
	private Long taborg;
	private Long horini;
	private Long horfim;
	private Long diasem;
	private Long peresc;
	
	public Long getNumloc()
	{
		return numloc;
	}
	public void setNumloc(Long numloc)
	{
		this.numloc = numloc;
	}
	public Long getTaborg()
	{
		return taborg;
	}
	public void setTaborg(Long taborg)
	{
		this.taborg = taborg;
	}
	public Long getHorini()
	{
		return horini;
	}
	public void setHorini(Long horini)
	{
		this.horini = horini;
	}
	public Long getHorfim()
	{
		return horfim;
	}
	public void setHorfim(Long horfim)
	{
		this.horfim = horfim;
	}
	public Long getDiasem()
	{
		return diasem;
	}
	public void setDiasem(Long diasem)
	{
		this.diasem = diasem;
	}
	public Long getPeresc()
	{
		return peresc;
	}
	public void setPeresc(Long peresc)
	{
		this.peresc = peresc;
	}
}
