package com.neomind.fusion.custom.orsegups.maps.engine;

import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.ti.sigma.engine.SIGMAUtilsEngine;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class MapsRESTEngineActions {

    SIGMAUtilsEngine sigmaEngine = new SIGMAUtilsEngine();

    public String addRotaEfetiva(long viatura, String rota) {

	String retorno = "";

	String regional = rota.substring(0, 3);

	boolean encontrouAlguemNaRota = false;

	QLGroupFilter filtroViaturaRegional = new QLGroupFilter("AND");
	filtroViaturaRegional.addFilter(new QLEqualsFilter("emUso", Boolean.TRUE));
	filtroViaturaRegional.addFilter(new QLEqualsFilter("ativaNaFrota", Boolean.TRUE));
	filtroViaturaRegional.addFilter(new QLEqualsFilter("codigoRegional", regional));

	List<NeoObject> listViaturaRegional = PersistEngine.getObjects(AdapterUtils.getEntityClass("OTSViatura"), filtroViaturaRegional);

	if (listViaturaRegional != null && !listViaturaRegional.isEmpty()) {

	    for (NeoObject objViatura : listViaturaRegional) {
		EntityWrapper wViatura = new EntityWrapper(objViatura);

		@SuppressWarnings("unchecked")
		List<NeoObject> listRotas = (List<NeoObject>) wViatura.getValue("listaRotasEfetivas");

		if (listRotas != null && !listRotas.isEmpty()) {
		    for (NeoObject objRota : listRotas) {
			EntityWrapper wRota = new EntityWrapper(objRota);

			String rotaNaLista = (String) wRota.getValue("nomeRota");

			if (rotaNaLista.equalsIgnoreCase(rota)) {
			    encontrouAlguemNaRota = true;

			    String nomeViaturaNaFrota = (String) wViatura.getValue("motorista");

			    retorno = "A viatura " + nomeViaturaNaFrota + " encontra-se ativa nessa rota.";

			}
		    }
		}
	    }
	}

	if (!encontrouAlguemNaRota) {
	    QLGroupFilter filtroViatura = new QLGroupFilter("AND");
	    filtroViatura.addFilter(new QLEqualsFilter("emUso", Boolean.TRUE));
	    filtroViatura.addFilter(new QLEqualsFilter("ativaNaFrota", Boolean.TRUE));
	    filtroViatura.addFilter(new QLEqualsFilter("codigoViatura", String.valueOf(viatura)));

	    List<NeoObject> listViatura = PersistEngine.getObjects(AdapterUtils.getEntityClass("OTSViatura"), filtroViatura);

	    if (listViatura != null && !listViatura.isEmpty()) {
		NeoObject objViatura = listViatura.get(0);

		EntityWrapper wViatura = new EntityWrapper(objViatura);

		InstantiableEntityInfo OTSRota = AdapterUtils.getInstantiableEntityInfo("OTSItemRotaEfetiva");
		NeoObject novaRota = OTSRota.createNewInstance();
		EntityWrapper wRota = new EntityWrapper(novaRota);
		wRota.setValue("nomeRota", rota);
		PersistEngine.persist(novaRota);
		wViatura.findField("listaRotasEfetivas").addValue(novaRota);

		try {
		    PersistEngine.persist(objViatura);
		    retorno = "OK! Alteração gravada com sucesso.";
		} catch (Exception e) {
		    e.printStackTrace();
		    retorno = "Erro ao gravar alteração. Por favor tente novamente.";
		}

	    } else {
		retorno = "Parece que a viatura não encontra-se mais em uso.";
	    }

	}

	return retorno;
    }

    public String removerRotaEfetivaLista(long viatura, String rota) {

	String retorno = "";

	QLGroupFilter filtroViaturaNaRota = new QLGroupFilter("AND");
	filtroViaturaNaRota.addFilter(new QLEqualsFilter("emUso", Boolean.TRUE));
	filtroViaturaNaRota.addFilter(new QLEqualsFilter("ativaNaFrota", Boolean.TRUE));
	filtroViaturaNaRota.addFilter(new QLEqualsFilter("codigoViatura", String.valueOf(viatura)));
	filtroViaturaNaRota.addFilter(new QLEqualsFilter("listaRotasEfetivas.nomeRota", rota));

	List<NeoObject> listViaturaNaRota = PersistEngine.getObjects(AdapterUtils.getEntityClass("OTSViatura"), filtroViaturaNaRota);

	if (listViaturaNaRota != null && !listViaturaNaRota.isEmpty()) {
	    NeoObject objViatura = listViaturaNaRota.get(0);

	    EntityWrapper wViatura = new EntityWrapper(objViatura);

	    @SuppressWarnings("unchecked")
	    List<NeoObject> listRotas = (List<NeoObject>) wViatura.getValue("listaRotasEfetivas");

	    for (NeoObject objRota : listRotas) {
		EntityWrapper wRota = new EntityWrapper(objRota);

		String rotaNaLista = (String) wRota.getValue("nomeRota");

		if (rotaNaLista.equalsIgnoreCase(rota)) {
		    try {
			
			listRotas.remove(objRota);

			retorno = "OK! Alteração gravada com sucesso.";
			break;
		    } catch (Exception e) {
			e.printStackTrace();
			retorno = "Ops! Erro ao gravar alteração. Por favor tente novamente.";
		    }
		} 
	    }
	} else {
	    retorno = "Ops! Parece que a viatura não está mais em uso.";
	}

	return retorno;
    }
}
