package com.neomind.fusion.custom.orsegups.adapter;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.dao.FapDAO;
import com.neomind.fusion.custom.orsegups.fap.dto.FornecedorDTO;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class FAPRecuperarInformaçõesFornecedor implements AdapterInterface {
    @Override
    public void start(Task origin, EntityWrapper processEntity, Activity activity) {

	String mensagem = "";

	try {
	    // Variáveis para atribuição de valores no e-form
	    FornecedorDTO fornecedor = null;
	    String estado = null;
	    String banco = null;
	    String agencia = null;
	    String conta = null;
	    Long codigoEmpresa = null;

	    // Dados do e-form
	    String bancoFornecedor = (String) processEntity.findValue("codigoBanco.codban");
	    String agenciaFornecedor = (String) processEntity.findValue("agenciaFornecedor");
	    String contaFornecedor = (String) processEntity.findValue("contaFornecedor");

	    Long aplicacaoPagamento = (Long) processEntity.findValue("aplicacaoPagamento.codigo");
	    Long codigoFornecedor = (Long) processEntity.findValue("fornecedor.codfor");
	    Long formaPagamento = (Long) processEntity.findValue("formaPagamento.codigoFormaPagamento");

	    if (formaPagamento == 2L) {
		if (NeoUtils.safeIsNull(bancoFornecedor, agenciaFornecedor, contaFornecedor)) {
		    // Manutenção Alarme e CFTV - Mercado Público e Privado
		    if ((aplicacaoPagamento == 2L) || (aplicacaoPagamento == 3L) || (aplicacaoPagamento == 5L) || (aplicacaoPagamento == 6L) || (aplicacaoPagamento == 8L) || (aplicacaoPagamento == 9L)) {

			Long clienteSigma = (Long) processEntity.findValue("contaSigma.cd_cliente");
			String razaoDoClienteSigma = (String) processEntity.findValue("contaSigma.razao");

			estado = FapDAO.consultaEstadoContrato(clienteSigma, razaoDoClienteSigma);

			switch (estado) {
			case "SC":
			    codigoEmpresa = 18L;
			    break;
			case "PR":
			    codigoEmpresa = 21L;
			    break;
			default:
			    throw new WorkflowException("Estado de prestação de serviço do posto do cliente " + razaoDoClienteSigma + " não configurado para este tipo de FAP ou não existe conciliação Sapiens x Sigma.");
			}

			// tatico ou técnico
			fornecedor = FapDAO.consultaDadosFornecedor(codigoEmpresa, codigoFornecedor);
			if (NeoUtils.safeIsNull(fornecedor.getAgencia(), fornecedor.getConta(), fornecedor.getBanco())) {
			    
			    //Busca dados bancários do favorecido para pessoa física como fornecedor
			    fornecedor = FapDAO.consultaDadosFavorecido(codigoEmpresa, codigoFornecedor);
			    if (NeoUtils.safeIsNull(fornecedor.getAgencia(), fornecedor.getConta(), fornecedor.getBanco())) {

				throw new WorkflowException("Não foi possível recuperar os dados bancários do fornecedor " + fornecedor + " ou seu favorecido. Verifique as definições do fornecedor.");
			    }
			}

			agencia = fornecedor.getAgencia();
			conta = fornecedor.getConta();
			banco = fornecedor.getBanco();
			NeoObject efBanco = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("SAPIENSE030BAN"), banco);

			processEntity.findField("agenciaFornecedor").setValue(agencia);
			processEntity.findField("contaFornecedor").setValue(conta);
			processEntity.findField("codigoBanco").setValue(efBanco);

		    } else {
			switch (Integer.parseInt(aplicacaoPagamento.toString())) {
			case 1: // Manutenção de Frota
			    codigoEmpresa = 1L;
			    break;
			case 4: // Manutenção - Rastreador Veicular
			    codigoEmpresa = 18L;
			    break;
			case 7:
			    codigoEmpresa = 18L;
			    break;
			case 10: // Técnico
			    codigoEmpresa = 18L;
			    break;
			case 11: // Tático
			    codigoEmpresa = 18L;
			    break;
			case 12: // Patrimônio
			    codigoEmpresa = 18L;
			    break;
			default:
			    mensagem = "Aplicação da FAP sem definição de empresa padrão. Favor entrar em contato com a TI.";
			    throw new WorkflowException(mensagem);
			}
			
			// tatico ou técnico
			fornecedor = FapDAO.consultaDadosFornecedor(codigoEmpresa, codigoFornecedor);
			if (NeoUtils.safeIsNull(fornecedor.getAgencia(), fornecedor.getConta(), fornecedor.getBanco())) {

			    fornecedor = FapDAO.consultaDadosFavorecido(codigoEmpresa, codigoFornecedor);
			    if (NeoUtils.safeIsNull(fornecedor.getAgencia(), fornecedor.getConta(), fornecedor.getBanco())) {

				throw new WorkflowException("Não foi possível recuperar os dados bancários do fornecedor " + fornecedor + " ou seu favorecido. Verifique as definições do fornecedor.");
			    }
			}			
			
			agencia = fornecedor.getAgencia();
			conta = fornecedor.getConta();
			banco = fornecedor.getBanco();
			NeoObject efBanco = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("SAPIENSE030BAN"), banco);

			processEntity.findField("agenciaFornecedor").setValue(agencia);
			processEntity.findField("contaFornecedor").setValue(conta);
			processEntity.findField("codigoBanco").setValue(efBanco);

		    }
		}
	    }
	} catch (WorkflowException e) {
	    e.printStackTrace();
	    throw (WorkflowException) e;
	} catch (Exception e) {
	    e.printStackTrace();
	    throw new WorkflowException("Erro inesperado na classe " + this.getClass().getName() + "Favor contatar a TI.");
	}
    }

    @Override
    public void back(EntityWrapper processEntity, Activity activity) {
	// TODO Auto-generated method stub

    }
}
