package com.neomind.fusion.custom.orsegups.sigma.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.callcenter.ProvidenciaVO;
import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.sigma.SigmaUtils;
import com.neomind.fusion.custom.orsegups.sigma.vo.ColaboradorVO;
import com.neomind.fusion.custom.orsegups.sigma.vo.DBOrdemVO;
import com.neomind.fusion.custom.orsegups.sigma.vo.OSDefeitoVO;
import com.neomind.fusion.custom.orsegups.sigma.vo.OSSolicitanteVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoCalendarUtils;
import com.neomind.util.NeoUtils;

@WebServlet(name="SigmaServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.sigma.servlet.SigmaServlet"})
public class SigmaServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	private static final Log log = LogFactory.getLog(SigmaServlet.class);
	EntityManager entityManager;
	EntityTransaction transaction;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	private void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		try
		{
			response.setContentType("text/plain");
			response.setCharacterEncoding("ISO-8859-1");

			final PrintWriter out = response.getWriter();
			Gson gson = new Gson();

			String action = "";

			if (request.getParameter("action") != null)
			{
				action = request.getParameter("action");
			}

			if (action.equalsIgnoreCase("saveLog"))
			{
				String texto = gson.fromJson(request.getParameter("texto"), String.class);
				String idEntidade = request.getParameter("idEntidade");
				String tipo = request.getParameter("tipo");
				this.saveLog(texto, idEntidade, tipo, out);
			}

			if (action.equalsIgnoreCase("getLog"))
			{
				String idOS = request.getParameter("idOs");
				Long idOsL = Long.parseLong(idOS);
				String retorno = this.getLogOS(idOsL);
				out.print(retorno);
			}

			if (action.equalsIgnoreCase("listaColaboradores"))
			{
				String idInstalador = request.getParameter("idInstalador");
				this.listaColaboradores(response, idInstalador);
			}

			if (action.equalsIgnoreCase("listaDefeitos"))
			{
				String idDefeito = request.getParameter("idDefeito");
				this.listaDefeitos(response, idDefeito);
			}

			if (action.equalsIgnoreCase("listaSolicitantes"))
			{
				String idSolicitante = request.getParameter("idSolicitante");
				this.listaSolicitantes(response, idSolicitante);
			}

			if (action.equalsIgnoreCase("editaOS"))
			{
				String idOrdem = request.getParameter("idOrdem");
				String cdCliente = request.getParameter("cdCliente");
				String idInstalador = request.getParameter("idInstalador");
				String idDefeito = request.getParameter("idDefeito");
				String idSolicitante = request.getParameter("idSolicitante");
				String idSolicitanteAnt = request.getParameter("idSolicitanteAnt");
				String descricao = request.getParameter("descricao");
				String data = request.getParameter("data");
				String hora = request.getParameter("hora");
				String minuto = request.getParameter("minuto");

				this.editaOS(response, idOrdem, cdCliente, idInstalador, idDefeito, idSolicitante, idSolicitanteAnt, descricao, data, hora, minuto);
			}

			if (action.equalsIgnoreCase("consultaDadosProvidencia"))
			{
				this.consultaDadosProvidencia(request, response, out);
			}
			
			if (action.equalsIgnoreCase("consultaHistoricoOS"))
			{
				this.consultaHistoricoOS(request, response, out);
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void saveLog(String texto, String idEntidade, String tipo, PrintWriter out)
	{
		if (tipo != null && tipo.equalsIgnoreCase("viatura"))
		{
			QLGroupFilter groupFilter = new QLGroupFilter("AND");
			groupFilter.addFilter(new QLEqualsFilter("cd_viatura", Long.parseLong(idEntidade)));

			/**
		 	* @author orsegups lucas.avila - Classe generica.
		 	* @date 08/07/2015
		 	*/
			Class<? extends NeoObject> clazz = AdapterUtils.getEntityClass("SIGMA90VIATURA");

			NeoObject viaturaObject = PersistEngine.getObject(clazz, groupFilter);
			EntityWrapper viaturaWrapper = new EntityWrapper(viaturaObject);
			String placa = (String) viaturaWrapper.findValue("nm_placa");

			if (viaturaObject != null)
			{
				NeoObject logViatura = AdapterUtils.createNewEntityInstance("SIGMALogViatura");

				if (logViatura != null)
				{
					EntityWrapper postoWrapper = new EntityWrapper(logViatura);
					postoWrapper.setValue("viatura", viaturaObject);
					postoWrapper.setValue("dataLog", new GregorianCalendar());
					postoWrapper.setValue("textoLog", texto);
					postoWrapper.setValue("usuario", PortalUtil.getCurrentUser());
					postoWrapper.setValue("placaViatura", placa);

					PersistEngine.persist(logViatura);

					out.print("Log salvo com sucesso.");
					return;
				}
			}
		}
		else if (tipo != null && tipo.equalsIgnoreCase("colaborador"))
		{
			QLGroupFilter groupFilter = new QLGroupFilter("AND");
			groupFilter.addFilter(new QLEqualsFilter("cd_colaborador", Long.parseLong(idEntidade)));

			/**
		 	* @author orsegups lucas.avila - Classe generica.
		 	* @date 08/07/2015
		 	*/			
			Class<? extends NeoObject> clazz = AdapterUtils.getEntityClass("SIGMACOLABORADOR");

			NeoObject colaboradorObject = PersistEngine.getObject(clazz, groupFilter);
			EntityWrapper colaboradorWrapper = new EntityWrapper(colaboradorObject);
			String nomeColaborador = (String) colaboradorWrapper.findValue("nm_colaborador");
			Long codigoColaborador = (Long) colaboradorWrapper.findValue("cd_colaborador");

			if (colaboradorObject != null)
			{
				NeoObject logColaborador = AdapterUtils.createNewEntityInstance("SIGMALogColaborador");

				if (logColaborador != null)
				{
					EntityWrapper postoWrapper = new EntityWrapper(logColaborador);
					postoWrapper.setValue("colaborador", colaboradorObject);
					postoWrapper.setValue("dataLog", new GregorianCalendar());
					postoWrapper.setValue("textoLog", texto);
					postoWrapper.setValue("usuario", PortalUtil.getCurrentUser());
					postoWrapper.setValue("nomeColaborador", nomeColaborador);
					postoWrapper.setValue("codigoColaborador", codigoColaborador);
					PersistEngine.persist(logColaborador);

					out.print("Log salvo com sucesso.");
					return;
				}
			}
		}
		else if (tipo != null && tipo.equalsIgnoreCase("os"))
		{
			QLGroupFilter groupFilter = new QLGroupFilter("AND");
			groupFilter.addFilter(new QLEqualsFilter("id_ordem", Long.parseLong(idEntidade)));

			/**
		 	* @author orsegups lucas.avila - Classe generica.
		 	* @date 08/07/2015
		 	*/
			Class<? extends NeoObject> clazz = AdapterUtils.getEntityClass("dbORDEM");

			NeoObject objs = PersistEngine.getObject(clazz, groupFilter);

			if (objs != null)
			{
				if (objs != null)
				{
					NeoObject logOS = AdapterUtils.createNewEntityInstance("SIGMALogOS");

					if (logOS != null)
					{
						EntityWrapper osWrapper = new EntityWrapper(logOS);
						osWrapper.setValue("os", objs);
						osWrapper.setValue("numeroOS", Long.parseLong(idEntidade));
						osWrapper.setValue("dataLog", new GregorianCalendar());
						osWrapper.setValue("textoLog", texto);
						osWrapper.setValue("usuario", PortalUtil.getCurrentUser());

						PersistEngine.persist(logOS);

						if (out != null)
						{
							out.print("Log salvo com sucesso.");
						}
						return;
					}
				}
			}
		}
		out.print("Erro ao salvar log.");
	}

	private void getContasTecnico()
	{
		log.warn("##### INICIAR CONSULTA QUANT CONTAS TECNICO - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		String returnFromAccess = "";

		try
		{

			conn = PersistEngine.getConnection("SIGMA90");

			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT NM_COLABORADOR, Tipo, COUNT(*) AS TOTAL ");
			sql.append(" FROM ( ");
			sql.append(" SELECT DISTINCT t.NM_COLABORADOR, c.ID_EMPRESA, c.ID_CENTRAL, CASE WHEN PARTICAO LIKE '098' OR PARTICAO LIKE '099' THEN 'CFTV' ELSE 'ALARME' END Tipo ");
			sql.append(" FROM dbCENTRAL c WITH (NOLOCK)  ");
			sql.append(" INNER JOIN COLABORADOR t WITH (NOLOCK) ON t.CD_COLABORADOR = c.CD_TECNICO_RESPONSAVEL ");
			sql.append(" WHERE c.CTRL_CENTRAL = 1  ");
			sql.append(" AND c.ID_CENTRAL NOT LIKE 'AAA%' AND c.ID_CENTRAL NOT LIKE 'DDDD' 	AND c.ID_CENTRAL NOT LIKE 'FFFF' ");
			sql.append(" ) AS base GROUP BY NM_COLABORADOR, Tipo ORDER BY 1, 2 ");

			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();
			while (rs.next())
			{
				String colaborador = rs.getString("NM_COLABORADOR");
				String tipo = rs.getString("Tipo");
				String total = rs.getString("TOTAL");

			}

			log.warn("##### EXECUTAR CONSULTA QUANT CONTAS TECNICO - RETORNO " + returnFromAccess + " Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO CONSULTA QUANT CONTAS TECNICO - Data: " + e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				log.error("##### ERRO ROTINA  GERAR EVENTO XLIG - Data: " + e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			}
			log.warn("##### FINALIZAR ROTINA  GERAR EVENTO XLIG - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}

	private static String getLogOS(Long idOS)
	{
		String resultLog = "<b>Log:</b></br>";

		List<NeoObject> objOS = PersistEngine.getObjects(AdapterUtils.getEntityClass("dbORDEM"), new QLEqualsFilter("id_ordem", idOS));

		if (objOS != null && objOS.size() > 0)
		{

			QLEqualsFilter osFilter = new QLEqualsFilter("os", objOS.get(0));

			GregorianCalendar datalimite = new GregorianCalendar();
			Long tempoLogViatura = ((Long) getQLParameters().findValue("tempoLogOS"));
			datalimite.add(Calendar.DAY_OF_MONTH, -tempoLogViatura.intValue());

			QLOpFilter datefilter = new QLOpFilter("dataLog", ">=", (GregorianCalendar) datalimite);

			QLGroupFilter groupFilterOS = new QLGroupFilter("AND");
			groupFilterOS.addFilter(osFilter);
			groupFilterOS.addFilter(datefilter);

			/**
		 	* @author orsegups lucas.avila - Classe generica.
		 	* @date 08/07/2015
		 	*/
			Class<? extends NeoObject> clazz = AdapterUtils.getEntityClass("SIGMALogOS");

			Collection<NeoObject> logs = PersistEngine.getObjects(clazz, groupFilterOS, -1, -1, "dataLog desc");

			if (logs != null && logs.size() > 0)
			{
				for (NeoObject log : logs)
				{
					EntityWrapper logWrapper = new EntityWrapper(log);

					String formatedDate = NeoCalendarUtils.formatDate(((GregorianCalendar) logWrapper.findValue("dataLog")).getTime(), NeoCalendarUtils.FULL_DATE_PATTERN);
					String texto = (String) logWrapper.findValue("textoLog");
					String usuario = (String) logWrapper.findValue("usuario.fullName");

					if (texto != null && !texto.isEmpty())
					{
						String linha = "<li>" + formatedDate + " - " + texto + " - Op. " + usuario + "</li>";
						resultLog += linha;
					}
				}
			}
		}
		return resultLog;
	}

	public static EntityWrapper getQLParameters()
	{
		List<NeoObject> listaParametros = PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMAParametros"));
		EntityWrapper wrapperParametros = null;

		if (listaParametros != null)
		{
			wrapperParametros = new EntityWrapper(listaParametros.get(0));
		}

		return wrapperParametros;
	}

	private void listaColaboradores(HttpServletResponse response, String idInstalador) throws IOException, JSONException
	{
		PrintWriter out = response.getWriter();
		SigmaUtils sigmaUtils = new SigmaUtils();

		List<ColaboradorVO> colaboradores = new ArrayList<ColaboradorVO>();
		colaboradores = sigmaUtils.listaColaboradores();

		JSONArray jsonColaboradores = new JSONArray();

		for (ColaboradorVO col : colaboradores)
		{
			JSONObject jsonColaborador = new JSONObject();
			jsonColaborador.put("id", col.getId());
			jsonColaborador.put("nome", col.getNome());

			int col1 = col.getId().intValue();
			int col2 = Integer.parseInt(idInstalador);

			if (col1 == col2)
			{
				jsonColaborador.put("selected", "true");
			}
			jsonColaboradores.put(jsonColaborador);
		}
		out.print(jsonColaboradores);
		out.flush();
		out.close();
	}

	private void listaDefeitos(HttpServletResponse response, String idDefeito) throws IOException, JSONException
	{
		PrintWriter out = response.getWriter();
		SigmaUtils sigmaUtils = new SigmaUtils();

		List<OSDefeitoVO> defeitos = new ArrayList<OSDefeitoVO>();
		defeitos = sigmaUtils.listaDefeitos();

		JSONArray jsonDefeitos = new JSONArray();

		for (OSDefeitoVO def : defeitos)
		{
			JSONObject jsonDefeito = new JSONObject();
			jsonDefeito.put("id", def.getId());
			jsonDefeito.put("descricao", def.getDescricao());

			int def1 = def.getId().intValue();
			int def2 = Integer.parseInt(idDefeito);

			if (def1 == def2)
			{
				jsonDefeito.put("selected", "true");
			}
			jsonDefeitos.put(jsonDefeito);
		}
		out.print(jsonDefeitos);
		out.flush();
		out.close();
	}

	private void listaSolicitantes(HttpServletResponse response, String idSolicitante) throws IOException, JSONException
	{
		PrintWriter out = response.getWriter();
		SigmaUtils sigmaUtils = new SigmaUtils();

		List<OSSolicitanteVO> defeitos = new ArrayList<OSSolicitanteVO>();
		defeitos = sigmaUtils.listaSolicitantes();

		JSONArray jsonSolicitantes = new JSONArray();

		for (OSSolicitanteVO def : defeitos)
		{
			JSONObject jsonSolicitante = new JSONObject();
			jsonSolicitante.put("id", def.getId());
			jsonSolicitante.put("descricao", def.getDescricao());

			int def1 = def.getId().intValue();
			int def2 = Integer.parseInt(idSolicitante);

			if (def1 == def2)
			{
				jsonSolicitante.put("selected", "true");
			}
			jsonSolicitantes.put(jsonSolicitante);
		}
		out.print(jsonSolicitantes);
		out.flush();
		out.close();
	}

	private void editaOS(HttpServletResponse response, String idOrdem, String cdCliente, String idInstalador, String idDefeito, String idSolicitante, String idSolicitanteAnt, String descricao, String data, String hora, String minuto) throws IOException, JSONException
	{
		PrintWriter out = response.getWriter();
		SigmaUtils sigmaUtils = new SigmaUtils();
		boolean equipOrsegups = sigmaUtils.equipamentoOrsegups(cdCliente);
		String msg = "";

		//Quando solicitante diferente de cadastrar senha, não cobrar quando equipamento da orsegups
		if (!idDefeito.equalsIgnoreCase("176") && equipOrsegups && (idSolicitante.equals("10026") || idSolicitante.equals("10027") || idSolicitante.equals("10028") || idSolicitante.equals("10029")))
		{
			msg = "Quando equipamento da Orsegups, não cobrar quando defeito diferente de Cadastrar senha.";
		}
		else
		{
			descricao = OrsegupsUtils.removeAccents(descricao);
			msg = sigmaUtils.receptorOSWebService(idInstalador, idDefeito, idSolicitante, descricao, data, hora, minuto, idOrdem);

			if (msg != null && msg.equals("OK"))
			{
				String texto = "";
				if ((idSolicitante.equals("10026") || idSolicitante.equals("10027") || idSolicitante.equals("10028") || idSolicitante.equals("10029")) && (!idSolicitanteAnt.equals("10026") && !idSolicitanteAnt.equals("10027") && !idSolicitanteAnt.equals("10028") && !idSolicitanteAnt.equals("10029")))
				{
					texto = "OS editada à cobrar.";
				}
				else
				{
					texto = "OS editada.";
				}
				this.saveLog(texto, idOrdem, "os", null);
			}
		}

		if (msg.contains("ERROR_TECHNICAL_WITHOUT_PERMISSION"))
		{
			msg = "Este técnico não possui perfil de instalador no Sigma, entre em contato com a CM.";
		}

		JSONObject jsonMsg = new JSONObject();
		jsonMsg.put("msg", msg);

		out.print(jsonMsg);
		out.flush();
		out.close();
	}

	private void consultaDadosProvidencia(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{

		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		EventoVO vo = null;
		String cdCliente = null;
		try
		{
			cdCliente = request.getParameter("cdCliente");
			if (cdCliente != null)
			{
				connection = PersistEngine.getConnection("SIGMA90");
				StringBuilder sql = new StringBuilder();
				sql.append(" SELECT ");
				sql.append("	PROV.CD_CLIENTE, ");
				sql.append("	CD_PROVIDENCIA, ");
				sql.append("	PROV.NOME, ");
				sql.append("	PROV.FONE1, ");
				sql.append("	PROV.FONE2, ");
				sql.append("	PROV.EMAIL, ");
				sql.append("	PROV.NU_PRIORIDADE ");
				sql.append(" FROM ");
				sql.append("	dbo.dbPROVIDENCIA AS PROV WITH (NOLOCK) ");
				sql.append("	INNER JOIN dbo.dbCENTRAL as CEN WITH (NOLOCK)  ON CEN.CD_CLIENTE = PROV.CD_CLIENTE ");
				sql.append(" WHERE  ");
				sql.append("	CEN.CD_CLIENTE = ? ");
				sql.append(" ORDER BY PROV.NU_PRIORIDADE_NIVEL2 ");

				statement = connection.prepareStatement(sql.toString());
				statement.setString(1, cdCliente);
				resultSet = statement.executeQuery();

				ProvidenciaVO providenciaVO = null;
				vo = new EventoVO();
				while (resultSet.next())
				{

					providenciaVO = new ProvidenciaVO();
					String cliente = resultSet.getString("CD_CLIENTE");
					String providencia = resultSet.getString("CD_PROVIDENCIA");
					String nome = resultSet.getString("NOME");
					String telefone1 = resultSet.getString("FONE1");
					String telefone2 = resultSet.getString("FONE2");
					String email = resultSet.getString("EMAIL");
					String prioridade = resultSet.getString("NU_PRIORIDADE");

					if (NeoUtils.safeIsNotNull(cliente))
					{
						providenciaVO.setCodigoCliente(Integer.parseInt(cliente));
					}
					else
					{
						providenciaVO.setCodigoCliente(0);
					}

					if (NeoUtils.safeIsNotNull(providencia))
					{
						providenciaVO.setCodigoProvidencia(Integer.parseInt(providencia));
					}
					else
					{
						providenciaVO.setCodigoProvidencia(0);
					}

					if (NeoUtils.safeIsNotNull(nome))
					{
						providenciaVO.setNome(nome);
					}
					else
					{
						providenciaVO.setCodigoCliente(0);
					}

					if (NeoUtils.safeIsNotNull(telefone1) && !telefone1.isEmpty())
					{
						telefone1 = telefone1.replace("(", "").replace(")", "").replace("-", "").trim();
						telefone1 = telefone1.replace(" ", "");
						providenciaVO.setTelefone1(telefone1);
					}
					else
					{
						providenciaVO.setTelefone1("");
					}
					if (NeoUtils.safeIsNotNull(telefone2) && !telefone2.isEmpty())
					{
						telefone2 = telefone2.replace("(", "").replace(")", "").replace("-", "").trim();
						telefone2 = telefone2.replace(" ", "");
						providenciaVO.setTelefone2(telefone2);
					}
					else
					{
						providenciaVO.setTelefone2("");
					}
					if (NeoUtils.safeIsNotNull(email))
					{
						providenciaVO.setEmail(email);
					}
					else
					{
						providenciaVO.setEmail("");
					}
					if (NeoUtils.safeIsNotNull(prioridade))
					{
						providenciaVO.setPrioridade(Integer.parseInt(prioridade));
					}
					else
					{
						providenciaVO.setPrioridade(0);
					}
					vo.addProvidencia(providenciaVO);

				}

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("ERRO SIGMA PROVIDENCIAS : " + e.getMessage());
		}
		finally
		{
			OrsegupsUtils.closeConnection(connection, statement, resultSet);
			Gson gson = new Gson();
			String eventosJSON = gson.toJson(vo.getProvidenciaVOs());
			out.print(eventosJSON);

		}

	}

	private void consultaHistoricoOS(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{

		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String cdCliente = null;
		List<DBOrdemVO> dbOrdemVOs = null;
		try
		{
			cdCliente = request.getParameter("cdCliente");
			if (cdCliente != null)
			{
				connection = PersistEngine.getConnection("SIGMA90");
				StringBuilder sql = new StringBuilder();

				sql.append(" SELECT TOP(50)OS.ID_ORDEM,OD.DESCRICAODEFEITO,CO.NM_COLABORADOR,OS.ABERTURA,OS.FECHAMENTO,OS.FECHADO ");
				sql.append(" FROM dbORDEM OS WITH(NOLOCK) ");
				sql.append(" INNER JOIN OSDEFEITO OD WITH(NOLOCK) ON OD.IDOSDEFEITO = OS.IDOSDEFEITO ");
				sql.append(" INNER JOIN COLABORADOR CO WITH(NOLOCK) ON CO.CD_COLABORADOR = OS.ID_INSTALADOR ");
				sql.append(" WHERE OS.CD_CLIENTE = ? ");
				sql.append(" ORDER BY OS.ABERTURA DESC ");

				statement = connection.prepareStatement(sql.toString());
				statement.setString(1, cdCliente);
				resultSet = statement.executeQuery();
				DBOrdemVO dbOrdemVO = null;
				dbOrdemVOs = new ArrayList<DBOrdemVO>();
				while (resultSet.next())
				{

					dbOrdemVO = new DBOrdemVO();
					if (resultSet.getString("ID_ORDEM") != null)
					{
						dbOrdemVO.setId(Long.parseLong(resultSet.getString("ID_ORDEM")));
					}
					else
					{
						dbOrdemVO.setId(0L);
					}

					if (resultSet.getString("DESCRICAODEFEITO") != null)
					{
						dbOrdemVO.setDescricaoDefeito(resultSet.getString("DESCRICAODEFEITO"));
					}
					else
					{
						dbOrdemVO.setDescricaoDefeito("");
					}

					if (resultSet.getString("NM_COLABORADOR") != null)
					{
						dbOrdemVO.setNomeColaborador(resultSet.getString("NM_COLABORADOR"));
					}
					else
					{
						dbOrdemVO.setNomeColaborador("");
					}

					if (resultSet.getTimestamp("ABERTURA") != null)
					{
						GregorianCalendar dataAbertura = new GregorianCalendar();
						dataAbertura.setTime(resultSet.getTimestamp("ABERTURA"));
						dbOrdemVO.setDataAbertuda(NeoUtils.safeDateFormat(dataAbertura, "dd/MM/yyyy HH:mm"));

					}
					else
					{
						dbOrdemVO.setDataAbertuda("");
					}

					if (resultSet.getTimestamp("FECHAMENTO") != null)
					{
						GregorianCalendar dataFechamento = new GregorianCalendar();
						dataFechamento.setTime(resultSet.getTimestamp("FECHAMENTO"));
						dbOrdemVO.setDataFechamento(NeoUtils.safeDateFormat(dataFechamento, "dd/MM/yyyy HH:mm"));

					}
					else
					{
						dbOrdemVO.setDataFechamento("");
					}

					

					if (resultSet.getString("FECHADO") != null)
					{
						dbOrdemVO.setStatus(Long.parseLong((resultSet.getString("FECHADO"))));
					}
					dbOrdemVOs.add(dbOrdemVO);
				}

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("ERRO SIGMA HISTÓRICO DE OS : " + e.getMessage());
		}
		finally
		{
			OrsegupsUtils.closeConnection(connection, statement, resultSet);
			Gson gson = new Gson();
			String eventosJSON = gson.toJson(dbOrdemVOs);
			out.print(eventosJSON);

		}

	}
}
