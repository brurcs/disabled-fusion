package com.neomind.fusion.custom.orsegups.maps.vo;

public class AtualizaPosicaoVO
{
	String codigoCliente;
	
	Float latitude;
	
	Float longitude;

	public AtualizaPosicaoVO()
	{
		
	}
	
	public String getCodigoCliente()
	{
		return codigoCliente;
	}

	public void setCodigoCliente(String codigoCliente)
	{
		this.codigoCliente = codigoCliente;
	}

	public Float getLatitude()
	{
		return latitude;
	}

	public void setLatitude(Float latitude)
	{
		this.latitude = latitude;
	}

	public Float getLongitude()
	{
		return longitude;
	}

	public void setLongitude(Float longitude)
	{
		this.longitude = longitude;
	}
}
