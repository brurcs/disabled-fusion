package com.neomind.fusion.custom.orsegups;

import javax.servlet.annotation.WebServlet;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityInfoCache;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.LoginControl;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.model.ProcessModel;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@WebServlet(name="ServletEnvioCurriculo", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.ServletEnvioCurriculo"})
public class ServletEnvioCurriculo extends HttpServlet
{
  private static final Log log = LogFactory.getLog(ServletEnvioCurriculo.class);
  private static final long serialVersionUID = 1L;

  protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    doGet(request, response);
  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    request.setAttribute("user", "convidadoCurriculo");
    request.setAttribute("pass", "guestCV0rs3gup5");

    Boolean lc = Boolean.valueOf(LoginControl.getInstance().login(request, response));
    String url = PortalUtil.getBaseURL();
    try
    {
      String model = "R021%";
      String entity = "CVprocessoBancoDeCurriculos";

      ProcessModel processModel = null;

      QLOpFilter filter = new QLOpFilter("name", "LIKE", model);
      processModel = (ProcessModel)PersistEngine.getObject(ProcessModel.class, filter);

      InstantiableEntityInfo entityInfo = (InstantiableEntityInfo)EntityRegister.getInstance().getCache().getByType(entity);
      NeoObject processEntity = entityInfo.createNewInstance();

      WFProcess proc = processModel.startProcess(processEntity, null, null);
      PersistEngine.persist(proc);
      PersistEngine.commit(true);

      Task task = (Task)proc.getAllTasks().get(0);
      if (!lc.booleanValue()) {
        url = url.concat("portal/render/Task?type=Task&id=" + task.getNeoId() + "&edit=true&select=true&root=true&form=true&full=true&mutable=false&toolbar=true&callBackURL=" + PortalUtil.getBaseURL() + "portal_orsegups/curriculoEnviado.jsp?mensagem=erro");
        return;
      }
      url = url.concat("portal/render/Task?type=Task&id=" + task.getNeoId() + "&edit=true&select=true&root=true&form=true&full=true&mutable=false&toolbar=true&callBackURL=" + PortalUtil.getBaseURL() + "portal_orsegups/curriculoEnviado.jsp?mensagem=ok");
      response.sendRedirect(url);
    }
    catch (NullPointerException e)
    {
      e.printStackTrace();
    }
  }

  public void process(HttpServletRequest request, HttpServletResponse response, String[] params)
    throws IOException
  {
  }
}