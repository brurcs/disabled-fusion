package com.neomind.fusion.custom.orsegups.e2doc.vo;

public class E2docVO
{
	private String ip;
	private String usuario;
	private String senha;
	private String key;
	private String base;
	
	
	/**
	 * @return the key
	 */
	public String getKey()
	{
		return key;
	}
	/**
	 * @param key the key to set
	 */
	public void setKey(String key)
	{
		this.key = key;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "E2docVO [ip=" + ip + ", usuario=" + usuario + ", senha=" + senha + ", base=" + base + "]";
	}
	/**
	 * @return the ip
	 */
	public String getIp()
	{
		return ip;
	}
	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip)
	{
		this.ip = ip;
	}
	/**
	 * @return the usuario
	 */
	public String getUsuario()
	{
		return usuario;
	}
	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario)
	{
		this.usuario = usuario;
	}
	/**
	 * @return the senha
	 */
	public String getSenha()
	{
		return senha;
	}
	/**
	 * @param senha the senha to set
	 */
	public void setSenha(String senha)
	{
		this.senha = senha;
	}
	/**
	 * @return the base
	 */
	public String getBase()
	{
		return base;
	}
	/**
	 * @param base the base to set
	 */
	public void setBase(String base)
	{
		this.base = base;
	}
	
}
