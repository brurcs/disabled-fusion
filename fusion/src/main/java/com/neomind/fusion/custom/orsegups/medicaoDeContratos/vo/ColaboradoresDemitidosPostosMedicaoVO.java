package com.neomind.fusion.custom.orsegups.medicaoDeContratos.vo;

import java.util.GregorianCalendar;

public class ColaboradoresDemitidosPostosMedicaoVO
{
	public long numcad;
	public long numemp;
	public String nomfun;
	public String datDmsString;
	public GregorianCalendar datDms;
	public long numpos;
	public long numctr;

	public long getNumctr()
	{
		return numctr;
	}

	public void setNumctr(long numctr)
	{
		this.numctr = numctr;
	}

	public final long getNumpos()
	{
		return numpos;
	}

	public final void setNumpos(long numpos)
	{
		this.numpos = numpos;
	}

	public final long getNumcad()
	{
		return numcad;
	}

	public final void setNumcad(long numcad)
	{
		this.numcad = numcad;
	}

	public final long getNumemp()
	{
		return numemp;
	}

	public final void setNumemp(long numemp)
	{
		this.numemp = numemp;
	}

	public final String getNomfun()
	{
		return nomfun;
	}

	public final void setNomfun(String nomfun)
	{
		this.nomfun = nomfun;
	}

	public final String getDatDmsString()
	{
		return datDmsString;
	}

	public final void setDatDmsString(String datDmsString)
	{
		this.datDmsString = datDmsString;
	}

	public final GregorianCalendar getDatDms()
	{
		return datDms;
	}

	public final void setDatDms(GregorianCalendar datDms)
	{
		this.datDms = datDms;
	}

}
