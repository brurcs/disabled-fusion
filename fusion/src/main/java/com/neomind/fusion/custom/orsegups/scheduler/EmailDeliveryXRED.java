package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.HtmlEmail;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.integracaoMobile.IntegracaoPortalMobile;
import com.neomind.fusion.custom.orsegups.objRatMobile.ObjRatMobile;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsEmailUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class EmailDeliveryXRED implements CustomJobAdapter {
    private static final Log log = LogFactory.getLog(EmailDeliveryXRED.class);
    private Pattern pattern;
    private Matcher matcher;
    private final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public static final String EVT_XRED = "XRED";

    @SuppressWarnings({ "unchecked", "static-access", "deprecation" })
    @Override
    public void execute(CustomJobContext arg0) {

	int adicionados = 0;
	log.warn("E-Mail XRED Inicio execução em: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	Long key = GregorianCalendar.getInstance().getTimeInMillis();

	InstantiableEntityInfo infoHis = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("EmailAutomaticoDesvioDeHabito");
	InstantiableEntityInfo ExcecoesEmail = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("CadastroExcecoesEmailsDesvioDeHabito");
	InstantiableEntityInfo infoEnvioEmail = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("EmailDelivery");

	Set<Integer> empresasNotificadas = new HashSet<Integer>();

	Connection conn = null;
	StringBuilder strSigma = new StringBuilder();
	PreparedStatement pstm = null;
	ResultSet rs = null;

	pattern = Pattern.compile(EMAIL_PATTERN);

	String ultimaExecucaoRotina = ultimaExecucaoRotina();
	String ultimaExecucaoRotinaAux = ultimaExecucaoRotinaRange();

	try {
	    
	    strSigma.append(" 	 SELECT c.CGCCPF, c.FANTASIA, c.RAZAO, c.ID_CENTRAL, c.PARTICAO, c.ENDERECO, cid.NOME AS NM_CIDADE, bai.NOME AS NM_BAIRRO, h.DT_FECHAMENTO, c.EMAILRESP,");
	    strSigma.append(" 	 hfe.NM_FRASE_EVENTO as NM_FRASE_EVENTO, ");
	    strSigma.append(" 	 h.TX_OBSERVACAO_FECHAMENTO, ISNULL(ma.DS_MOTIVO_ALARME, 'N/A - SEM ALTERAÇÃO') as motivo, h.CD_HISTORICO AS CD_HISTORICO, ");
	    strSigma.append(" 	 h.CD_EVENTO, c.CD_CLIENTE,  c.NU_LATITUDE, c.NU_LONGITUDE, h.DT_RECEBIDO, c.ID_EMPRESA ");
	    strSigma.append(" 	  FROM VIEW_HISTORICO h  WITH (NOLOCK)    ");
	    strSigma.append(" 	 INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
	    strSigma.append(" 	 INNER JOIN ROTA r  WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA   ");
	    strSigma.append(" 	 INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE  ");
	    strSigma.append(" 	 INNER JOIN HISTORICO_FRASE_EVENTO hfe ON hfe.CD_FRASE_EVENTO = h.CD_FRASE_EVENTO  ");
	    strSigma.append(" 	 LEFT JOIN MOTIVO_ALARME ma ON ma.CD_MOTIVO_ALARME = h.CD_MOTIVO_ALARME  ");
	    strSigma.append(" 	 LEFT JOIN dbCIDADE cid ON cid.ID_CIDADE = c.ID_CIDADE  ");
	    strSigma.append(" 	 LEFT JOIN dbBAIRRO bai ON bai.ID_CIDADE = c.ID_CIDADE AND bai.ID_BAIRRO = c.ID_BAIRRO  ");
	    strSigma.append(" 	 WHERE  H.CD_EVENTO = 'XRED' AND C.TP_PESSOA != 2 ");
	    strSigma.append(" 	 AND NOT EXISTS (SELECT * FROM [CACUPE\\SQL02].Fusion_Producao.dbo.D_EmailAutomaticoDesvioDeHabito em where em.historico = h.CD_HISTORICO)  ");
	    strSigma.append("    AND (h.TX_OBSERVACAO_FECHAMENTO NOT LIKE '%#AtualizarCadastro%' AND h.TX_OBSERVACAO_FECHAMENTO NOT LIKE '%#NaoEnviaComunicado%')   ");
	    // strSigma.append(" 	 AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
	    strSigma.append(" 	 AND CD_USUARIO_FECHAMENTO <> 9999     ");
	    strSigma.append("   	 AND LEN(h.TX_OBSERVACAO_FECHAMENTO)>1 ");
	    strSigma.append(" 	 AND ( DT_RECEBIDO > '" + ultimaExecucaoRotina + "' OR  DT_RECEBIDO > '" + ultimaExecucaoRotinaAux + "' ) ");

	    conn = PersistEngine.getConnection("SIGMA90");

	    pstm = conn.prepareStatement(strSigma.toString());

	    inserirFimRotina();

	    rs = pstm.executeQuery();

	    String calendarDDMM = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM");
	    // String calendarDM = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "d/M");

	    while (rs.next()) {

		boolean clienteComExcecao = OrsegupsEmailUtils.verificarExcecaoCNPJ(rs.getInt("CD_CLIENTE"));

		if (!clienteComExcecao) {
		    String cgcCpf = rs.getString("CGCCPF") == null ? "" : rs.getString("CGCCPF").replaceAll("([.\\-/])", "").trim();
		    String fantasia = (rs.getString("FANTASIA") == null ? "" : rs.getString("FANTASIA"));
		    String razao = (rs.getString("RAZAO") == null ? "" : rs.getString("RAZAO"));
		    String particao = (rs.getString("PARTICAO") == null ? "" : rs.getString("PARTICAO"));
		    String endereco = (rs.getString("ENDERECO") == null ? "" : rs.getString("ENDERECO"));
		    String cidade = (rs.getString("NM_CIDADE") == null ? "" : rs.getString("NM_CIDADE"));
		    String bairro = (rs.getString("NM_BAIRRO") == null ? "" : rs.getString("NM_BAIRRO"));
		    Timestamp dtViaturaNoLocal = rs.getTimestamp("DT_FECHAMENTO");

		    String nomeFraseEvento = (rs.getString("NM_FRASE_EVENTO") == null ? "" : rs.getString("NM_FRASE_EVENTO"));
		    String txObsevacaoFechamento = (rs.getString("TX_OBSERVACAO_FECHAMENTO") == null ? "" : rs.getString("TX_OBSERVACAO_FECHAMENTO"));
		    String motivo = (rs.getString("motivo") == null ? "" : rs.getString("motivo"));
		    String evento = (rs.getString("CD_EVENTO") == null ? "" : rs.getString("CD_EVENTO"));
		    String email = (rs.getString("EMAILRESP") == null ? "" : rs.getString("EMAILRESP"));
		    String cdCliente = (rs.getString("CD_CLIENTE") == null ? "" : rs.getString("CD_CLIENTE"));
		    String dataAtendimento = NeoDateUtils.safeDateFormat(dtViaturaNoLocal, "dd/MM/yyyy");
		    String horaAtendimento = rs.getString("DT_FECHAMENTO").substring(11, 16);
		    String historico = (rs.getString("CD_HISTORICO") == null ? "" : rs.getString("CD_HISTORICO"));
		    // Timestamp dtRecebido = rs.getTimestamp("DT_RECEBIDO");
		    // String dtRecebidoStr = NeoDateUtils.safeDateFormat(dtRecebido, "dd/MM/yyyy HH:mm:ss");
		    // String central = rs.getString("ID_CENTRAL");
		    int empresa = rs.getInt("ID_EMPRESA");
		    log.warn("[XRED] fantasia:" + fantasia + ", razao:" + razao + ", email" + email);

		    System.out.println();
		    if (txObsevacaoFechamento != null && !txObsevacaoFechamento.isEmpty()) {
			String array[] = txObsevacaoFechamento.split("###");
			String textoFormatado = "";
			for (String string : array) {
			    textoFormatado += string + "<br/>";
			}
			array = txObsevacaoFechamento.split(" ");
			List<String> respostas = new ArrayList<String>();
			List<String> cameras = new ArrayList<String>();
			int i = 0;
			for (String string : array) {
			    if (string.contains("R:")) {
				String arrayResp[] = string.split("\\n");
				if (!respostas.contains(arrayResp[0])) {
				    respostas.add(arrayResp[0]);
				}
			    }
			    if (string.equals("camera") || string.equals("câmera")) {
				if (array.length > i + 1) {
				    String c = string + " " + array[i + 1];
				    if (!c.contains("(") && !c.contains(")") && !c.contains("{") && !c.contains("}") && !c.contains("[") && !c.contains("]")) {
					cameras.add(c);
				    }
				}
			    } else {
				if (string.contains("camera") || string.contains("câmera")) {
				    if (string.contains(":")) {
					String arrayCam[] = string.split(":");
					cameras.clear();
					if (arrayCam.length > 1 && array.length > i + 1) {
					    cameras.add(arrayCam[1] + " " + array[i + 1]);
					}
				    } else {
					String arrayCam[] = string.split("\\n");
					for (String s : arrayCam) {
					    if (s.equals("camera") || s.equals("câmera")) {
						if (array.length > i + 1) {
						    cameras.add(s + " " + array[i + 1]);
						}
					    }
					}
				    }
				}
			    }
			    i++;
			}

			if (!textoFormatado.isEmpty()) {
			    textoFormatado = textoFormatado.replaceAll("#", "");
			    textoFormatado = textoFormatado.replaceAll("\\r\\n", "");
			    textoFormatado = textoFormatado.replaceAll(";", "");
			    textoFormatado = textoFormatado.replaceAll(calendarDDMM, "<br/><br/>" + calendarDDMM);
			    // textoFormatado = textoFormatado.replaceAll(calendarDM, "<br/><br/>" + calendarDDMM);

			    if (respostas.size() > 0) {
				for (String resposta : respostas) {
				    textoFormatado = textoFormatado.replaceAll(resposta, "<br/>" + resposta + "<br/><br/>");
				}
			    }

			    if (cameras.size() > 0) {
				for (String camera : cameras) {
				    textoFormatado = textoFormatado.replaceAll(camera, "<br/>" + camera);
				}
			    }

			    textoFormatado = textoFormatado.replaceAll("Ligando para cliente!", "Ligando para cliente ");
			    textoFormatado = textoFormatado.replaceAll("Ramal", "a partir do ramal ");

			    textoFormatado = textoFormatado.replaceAll("FecharEvento", " Finalizar evento.");
			    textoFormatado = textoFormatado.replaceAll("EmEspera", " Aguardo deslocamento de um atendente.");
			    textoFormatado = textoFormatado.replaceAll("TratarSigma", "");

			    textoFormatado = textoFormatado.replaceAll("AtendimentoRealizado", " Finalizar evento.");
			    textoFormatado = textoFormatado.replaceAll("SemContato", " Não houve sucesso em nenhuma tentativa de contato.");
			    textoFormatado = textoFormatado.replaceAll("AtualizarCadastro", " Atualizar cadastro do cliente.");
			    textoFormatado = textoFormatado.replaceAll("NaoEnviaComunicado", " Comunicado de alarme com a central de monitoramento.");

			    txObsevacaoFechamento = textoFormatado;
			}
		    }
		    List<String> emailClie = this.validarEmail(email, cdCliente);
		    if ((emailClie != null) && (!emailClie.isEmpty())) {
			log.warn("[XRED] email do cliente validado");

			final String tipo = OrsegupsEmailUtils.TIPO_RAT_DESCONEXAO_CFTV;

			Map<Integer, String> params = OrsegupsEmailUtils.getConfiguracaoEmpresa(empresa);

			String pasta = null;
			String remetente = null;
			String grupo = null;

			if (params != null) {

			    pasta = params.get(OrsegupsEmailUtils.RAT_PASTA);
			    remetente = params.get(OrsegupsEmailUtils.RAT_REMETENTE);
			    grupo = params.get(OrsegupsEmailUtils.RAT_GRUPO);
			    String neoId = params.get(OrsegupsEmailUtils.RAT_NEOID);
			    Long tipRat = 8L;
			    String ratingToken = DigestUtils.sha256Hex(cgcCpf + Long.toString(tipRat) + Long.toString(new Date().getTime()));
			    
			    for (String emailFor : emailClie) {
				log.warn("[XRED] addTo: " + emailFor);
				StringBuilder noUserMsg = new StringBuilder();
				List<NeoObject> neoObjects = null;
				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				groupFilter.addFilter(new QLEqualsFilter("email", emailFor));
				groupFilter.addFilter(new QLEqualsFilter("tratarXRED", Boolean.TRUE));

				neoObjects = PersistEngine.getObjects(ExcecoesEmail.getEntityClass(), groupFilter);

				if (neoObjects == null || (neoObjects != null && neoObjects.isEmpty())) {

				    noUserMsg.append(OrsegupsEmailUtils.getCabecalhoEmail(tipo, pasta, ratingToken));

				    noUserMsg.append("\n <table width=\"600\" border=\"0\">");
				    noUserMsg.append("\n <tbody>");
				    noUserMsg.append("\n <tr>");

				    noUserMsg.append("\n <td><table width=\"600\"align=\"center\" cellpadding=\"0\" cellspacing=\"0\">");
				    noUserMsg.append("\n <tbody>");
				    noUserMsg.append("\n <tr>");
				    noUserMsg.append("\n <td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/header.jpg\" width=\"600\" height=\"145\" alt=\"\"/></td>");
				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n <tr> ");
				    noUserMsg.append("\n <td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/separator-01.jpg\" width=\"600\" height=\"22\" alt=\"\"/></td>");
				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n <tr> ");
				    noUserMsg.append("\n <td><table width=\"100%\" border=\"0\">");
				    noUserMsg.append("\n <tbody>");
				    noUserMsg.append("\n <tr>");
				    noUserMsg.append("\n <td colspan=\"5\" style=\"padding-left:1%;padding-right:18%;\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Informamos o atendimento ao seguinte evento de seu sistema de segurança:</p>");
				    noUserMsg.append("\n </td> </tr> </br>");
				    noUserMsg.append("\n <tr> ");
				    noUserMsg.append("\n <td colspan=\"5\" style=\"text-align: center;\"> <p style=\"font-family: 'Verdana';font-weight:normal;font-size:20px;text-align:center;color:#007FFF;font-weight:bold;padding:10px;margin-top:0px;\">FALHA DE CONEXÃO REMOTA DE CFTV</p>");
				    noUserMsg.append("\n </td> </tr><br/>");

				    // Aviso ao cliente para atualização de contatos
				    if (txObsevacaoFechamento.contains("#SemContato")) {
					noUserMsg.append("\n <tr>");
					noUserMsg.append("\n <td>");

					noUserMsg.append("\n </br><b><div style=\"font-family: 'Verdana';font-weight:normal;font-size:20px;color:red; text-align: center\">ATENÇÃO: Aviso Importante</b></div>");
					noUserMsg.append("\n </td>");
					noUserMsg.append("\n </tr>");
					noUserMsg.append("\n <tr> ");
					noUserMsg.append("\n <td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/separator-01.jpg\" width=\"600\" height=\"22\" alt=\"\"/></td>");
					noUserMsg.append("\n </tr>");
					noUserMsg.append("\n </tr> ");
					noUserMsg.append("\n <tr> ");
					noUserMsg.append("\n <td>");
					noUserMsg.append("\n <div style=\"font-family: 'Verdana';font-weight:normal;font-size:16px;color:#787878; text-align: justify\"></br>");
					noUserMsg.append("Informamos que em <strong>" + dataAtendimento + "&nbsp;" + horaAtendimento + " </strong> nossa Central de ");
					noUserMsg.append("Monitoramento 24h identificou que seu sistema de <strong> câmeras </strong> está com uma <strong> falha de comunicação </strong>.</div>");

					noUserMsg.append("\n <div style=\"font-family: 'Verdana';font-weight:normal;font-size:16px;color:#787878; text-align: justify\"></br>");
					noUserMsg.append("Conforme pode-se constatar no relatório abaixo, durante este atendimento, nossos operadores tentaram realizar contato telefônico com as pessoas ");
					noUserMsg.append("indicadas no cadastro para verificar necessidade de auxílio com o sistema, <strong> não houve sucesso em nenhuma tentativa. </strong></div>");

					noUserMsg.append("\n <div style=\"font-family: 'Verdana';font-weight:normal;font-size:16px;color:#787878; text-align: justify\"></br>");
					noUserMsg.append("Por esse motivo, foi aberta uma ordem de serviço para manutenção técnica.</div>");

					noUserMsg.append("\n <div style=\"font-family: 'Verdana';font-weight:normal;font-size:16px;color:#787878; text-align: justify\"></br>");
					noUserMsg.append("<strong>Lembre-se:</strong> é imprescindível que seus contatos emergenciais estejam atualizados.</div></br>");

					noUserMsg.append("\n </td>");
					noUserMsg.append("\n </tr> ");

					noUserMsg.append("\n </table>");

					noUserMsg.append("\n </td>");
					noUserMsg.append("\n </tr>");
				    }

				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n <tr>");
				    noUserMsg.append("\n <td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/separator-02.jpg\" width=\"600\" height=\"8\" alt=\"\"/></td>");
				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n <tr>");
				    noUserMsg.append("\n <td><table width=\"100%\" border=\"0\">");
				    noUserMsg.append("\n <tbody>");
				    noUserMsg.append("\n <tr> ");
				    noUserMsg
					    .append("\n <td colspan=\"6\" style=\"padding:5px;\"><h2 style=\"border-bottom:1px solid #CCC;margin:0px 15px 0px 5px;padding-bottom:10px;line-height:14px;font-size:20px;text-align:center;font-family: 'Verdana';font-weight:normal;\">DETALHES SOBRE O ATENDIMENTO</td>");
				    noUserMsg.append("\n </tr>");
				    if (txObsevacaoFechamento != null && !txObsevacaoFechamento.equals("")) {
					noUserMsg.append("\n <tr>");
					noUserMsg.append("\n <td colspan=\"6\">");
					noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align:justify;\"> " + txObsevacaoFechamento + "</p></td>");
					noUserMsg.append("\n </tr><br/>");
				    }
				    noUserMsg.append("\n <tr>");
				    noUserMsg.append("\n <td colspan=\"4\" align=\"left\" valign=\"top\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:14px;\">");
				    noUserMsg.append("\n <img src=\"https://maps.googleapis.com/maps/api/staticmap?center=" + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE")
					    + "&zoom=19&size=300x300&maptype=hybrid&format=png&markers=icon:http://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/maps/images/casa_rat_email.png%7C" + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE") + "&path=color:0xf9dc00|"
					    + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE") + "\" width=\"300\" height=\"300\" alt=\"\"/></p><p style=\"font-family: 'Verdana';font-weight:normal;font-size:8px;width: 300px;\"></p></td>");
				    noUserMsg.append("\n <td width=\"10%\"><p><br>");
				    noUserMsg.append("\n </p></td>");
				    noUserMsg.append("\n <td width=\"51%\" align=\"left\" valign=\"top\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Local </br> <strong>" + razao + "</strong></br>");
				    noUserMsg.append("\n <strong>" + endereco + "</strong></br>");
				    noUserMsg.append("\n <strong>" + bairro + "</strong></br>");
				    noUserMsg.append("\n <strong>" + cidade + "</strong></p>");
				    noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Data <br> <strong>" + dataAtendimento + "</strong></br>");
				    noUserMsg.append("\n Hora  </br> <strong>" + horaAtendimento + "</strong></p>");
				    noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Evento </br> ");
				    noUserMsg.append("\n <strong>CLIENTE CFTV DESCONECTADO</strong></p> ");

				    // noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Resultado do atendimento</br>");
				    // noUserMsg.append("\n <b><p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Atendimento realizado, pelo nosso operador de monitoramento.</b></br>");

				    noUserMsg.append("\n </tr>");

				    noUserMsg.append("\n </tbody>");
				    noUserMsg.append("\n </table>");
				    noUserMsg.append(OrsegupsEmailUtils.getRodapeEmail(tipo, pasta, grupo, email, remetente, ratingToken));

				    // ADD na lista de e-mail
				    NeoObject emailHis = infoHis.createNewInstance();
				    EntityWrapper emailHisWp = new EntityWrapper(emailHis);
				    adicionados = adicionados + 1;
				    emailHisWp.findField("fantasia").setValue(fantasia);
				    emailHisWp.findField("razao").setValue(razao);
				    emailHisWp.findField("particao").setValue(particao);
				    emailHisWp.findField("endereco").setValue(endereco);
				    emailHisWp.findField("cidade").setValue(cidade);
				    emailHisWp.findField("bairro").setValue(bairro);
				    emailHisWp.findField("dataFechamento").setValue(dataAtendimento);
				    emailHisWp.findField("horaFechamento").setValue(horaAtendimento);
				    // emailHisWp.findField("nomeViatura").setValue(nomeViatura);
				    // emailHisWp.findField("textoObservacaoFechamentos").setValue(txObsevacaoFechamento);
				    emailHisWp.findField("motivoAlarme").setValue(motivo);
				    emailHisWp.findField("evento").setValue(evento);
				    emailHisWp.findField("enviadoPara").setValue(emailClie.toString());
				    emailHisWp.findField("nomeFraseEvento").setValue(nomeFraseEvento);
				    emailHisWp.findField("historico").setValue(historico);
				    PersistEngine.persist(emailHis);
				    
				    // TESTE E-MAIL UTILIZANDO RECURSO FUSION
				    //String subject = "Relatório de Atendimento XRED - " + fantasia;
				    //OrsegupsEmailUtils.sendTestEmail(noUserMsg, subject);

				    GregorianCalendar dataCad = (GregorianCalendar) GregorianCalendar.getInstance();
				    NeoObject emaiEnvio = infoEnvioEmail.createNewInstance();
				    EntityWrapper emailEnvioWp = new EntityWrapper(emaiEnvio);
				    emailEnvioWp.findField("de").setValue("cm.rat" + remetente);
				    // emailEnvioWp.findField("para").setValue(emailFor +
				    // ";emailautomatico@orsegups.com.br;giliardi@orsegups.com.br;dilmoberger@orsegups.com.br;dirceu.costa@orsegups.com.br");
				    emailEnvioWp.findField("para").setValue(emailFor + ";emailautomatico@orsegups.com.br;copia@orsegups.com.br");
				    emailEnvioWp.findField("assunto").setValue("Relatório de Atendimento XRED - " + fantasia + " - " + dataAtendimento + " " + horaAtendimento);
				    emailEnvioWp.findField("Mensagem").setValue(noUserMsg.toString());
				    emailEnvioWp.findField("datCad").setValue(dataCad);
				    PersistEngine.persist(emaiEnvio);

				    // Montando informações para serviço Mobile - Inicio
				    ObjRatMobile objRat = new ObjRatMobile();
				    objRat.setTipRat(tipRat);
				    objRat.setRatingToken(ratingToken);
				    objRat.setInformativo("Informamos o atendimento ao seguinte evento de seu sistema de segurança:");
				    objRat.setEvento(nomeFraseEvento);
				    objRat.setResultado("");
				    objRat.setHashId("Relatório de Atendimento XRED - " + fantasia + " - " + dataAtendimento + " " + horaAtendimento);
				    objRat.setResultadoDoAtendimento("FALHA DE CONEXÃO REMOTA DE CFTV");
				    objRat.setAtendidoPor("");
				    objRat.setLocal(razao + " - " + endereco + " - " + bairro + " - " + cidade);
				    objRat.setDataAtendimento(dataAtendimento);
				    objRat.setHoraAtendimento(horaAtendimento);
				    objRat.setLnkFotoLocal("https://maps.googleapis.com/maps/api/staticmap?key=AIzaSyDie-lwDjXfRv0diccYyQtC8ZuGt-P4scs&center=" + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE")
					    + "&zoom=19&size=500x300&maptype=hybrid&format=png&markers=icon:http://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/maps/images/casa_rat_email.png%7C" + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE") + "&path=color:0xf9dc00|"
					    + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE"));
				    objRat.setLnkFotoAit("");

				    if (txObsevacaoFechamento != null && !txObsevacaoFechamento.equals("")) {
					objRat.setObservacao(txObsevacaoFechamento);
				    } else {
					objRat.setObservacao("");
				    }

				    objRat.setEmpRat(grupo);
				    objRat.setNeoId(neoId);

				    if (!cgcCpf.equals("")) {
					objRat.setCgcCpf(Long.parseLong(cgcCpf));
					IntegracaoPortalMobile integracao = new IntegracaoPortalMobile();
					integracao.inserirInformacoesPush(objRat);
				    }

				    log.warn("Cliente [XRED]: " + fantasia + ", E-mail: " + emailFor);
				}
			    }
			    OrsegupsUtils.sendEmailWhatsAppRAT(Long.valueOf(cdCliente), "Relatório de Atendimento XRED - " + fantasia + " - " + dataAtendimento + " " + horaAtendimento, endereco + " - " + bairro + " - " + cidade, dataAtendimento + " " + horaAtendimento);
			} else {
			    if (!empresasNotificadas.contains(empresa)) {
				OrsegupsEmailUtils.enviarNotificacaoEmpresaSemConfig(empresa);
				empresasNotificadas.add(empresa);
			    }
			}

		    } else {
			log.warn("[XRED] Email do cliente invalido vou vazio ");
		    }
		}
	    }
	    log.warn("E-Mail XRED Fim execução em: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	} catch (Exception e) {
	    log.error("E-Mail XRED erro no processamento:");
	    System.out.println("[" + key + "] E-Mail XRED erro no processamento: " + e.getMessage());
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

    }

    public List<String> validarEmail(String emails, String cdCliente) {

	List<String> listaDeEmail = new ArrayList<String>();

	emails = emails.replaceAll(",", ";");

	String emailsSigma[] = emails.split(";");

	Connection conn = PersistEngine.getConnection("SIGMA90");
	PreparedStatement pstm = null;
	ResultSet rsSapiens = null;
	ResultSet rs = null;
	try {

	    for (String mail : emailsSigma) {
		matcher = pattern.matcher(mail);

		if (matcher.matches()) {
		    if (!listaDeEmail.contains(mail.toLowerCase().trim())) {
			listaDeEmail.add(mail.toLowerCase().trim());
		    }
		}
	    }

	    String sqlProv = " SELECT email FROM DBPROVIDENCIA WITH(NOLOCK) WHERE cd_cliente = " + Long.valueOf(cdCliente) + " AND  email IS NOT NULL AND email <> ' ' ";

	    pstm = conn.prepareStatement(sqlProv);

	    rs = pstm.executeQuery();

	    while (rs.next()) {

		String emailRs = rs.getString("email");

		if (emailRs != null && !emailRs.isEmpty()) {
		    emailRs = emailRs.replaceAll(",", ";");

		    String emailsProv[] = emailRs.split(";");

		    for (String mail : emailsProv) {
			matcher = pattern.matcher(emailRs);

			if (matcher.matches()) {
			    if (!listaDeEmail.contains(mail.toLowerCase().trim())) {
				listaDeEmail.add(mail.toLowerCase().trim());
			    }
			}
		    }
		}
	    }

	    conn = PersistEngine.getConnection("SAPIENS");

	    if (pstm != null) {
		pstm.close();
	    }

	    String sqlSap = "" + " SELECT DISTINCT cli.IntNet, cli.emanfe, ctr.usu_emactr " + " FROM USU_T160SIG sig WITH(NOLOCK) " + " INNER JOIN USU_T160CTR ctr WITH(NOLOCK) ON  ctr.usu_codemp = sig.usu_codemp and ctr.usu_codfil = sig.usu_codfil and ctr.usu_numctr = sig.usu_numctr "
		    + " inner join dbo.E085CLI cli WITH(NOLOCK) ON CLI.CodCli = CTR.usu_codcli " + " WHERE sig.usu_codcli = " + cdCliente;

	    pstm = conn.prepareStatement(sqlSap);
	    rsSapiens = pstm.executeQuery();

	    while (rsSapiens.next()) {

		String intNet = rsSapiens.getString("IntNet");
		String emanfe = rsSapiens.getString("emanfe");
		String usu_emactr = rsSapiens.getString("usu_emactr");

		if (intNet != null && !intNet.isEmpty()) {
		    intNet = intNet.replaceAll(",", ";");
		    String rsEmails[] = intNet.split(";");

		    for (String mail : rsEmails) {
			matcher = pattern.matcher(mail);
			if (matcher.matches()) {
			    if (!listaDeEmail.contains(mail.toLowerCase().trim())) {
				listaDeEmail.add(mail.toLowerCase().trim());
			    }
			}
		    }
		}

		if (emanfe != null && !emanfe.isEmpty()) {
		    emanfe = emanfe.replaceAll(",", ";");
		    String rsEmails[] = emanfe.split(";");

		    for (String mail : rsEmails) {
			matcher = pattern.matcher(mail);
			if (matcher.matches()) {
			    if (!listaDeEmail.contains(mail.toLowerCase().trim())) {
				listaDeEmail.add(mail.toLowerCase().trim());
			    }
			}
		    }
		}

		if (usu_emactr != null && !usu_emactr.isEmpty()) {
		    usu_emactr = usu_emactr.replaceAll(",", ";");
		    String rsEmails[] = usu_emactr.split(";");

		    for (String mail : rsEmails) {
			matcher = pattern.matcher(mail);
			if (matcher.matches()) {
			    if (!listaDeEmail.contains(mail.toLowerCase().trim())) {
				listaDeEmail.add(mail.toLowerCase().trim());
			    }
			}
		    }
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("E-Mail XRED erro validação do e-mail: " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(null, null, rs);
	    OrsegupsUtils.closeConnection(conn, pstm, rsSapiens);
	}
	return listaDeEmail;

    }

    private void inserirFimRotina() {

	try {
	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("nomeRotina", "EmailAtendimentoXRED"));

	    List<NeoObject> monitoraAgendador = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);
	    PersistEngine.getEntityManager().flush();

	    if (monitoraAgendador != null && !monitoraAgendador.isEmpty()) {
		NeoObject neoObject = (NeoObject) monitoraAgendador.get(0);

		EntityWrapper monitoraAgendadorWrapper = new EntityWrapper(neoObject);

		monitoraAgendadorWrapper.findField("dataFinalAgendador").setValue(new GregorianCalendar());

		PersistEngine.persist(neoObject);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}

    }

    private String ultimaExecucaoRotina() {

	String retorno = "";
	try {
	    Collection<NeoObject> monitoraAgendador = null;

	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("nomeRotina", "EmailAtendimentoXRED"));

	    monitoraAgendador = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);

	    if (monitoraAgendador != null && !monitoraAgendador.isEmpty()) {
		for (NeoObject neoObject : monitoraAgendador) {
		    EntityWrapper monitoraAgendadorObj = new EntityWrapper(neoObject);

		    GregorianCalendar dataFinalAgendador = (GregorianCalendar) monitoraAgendadorObj.getValue("dataFinalAgendador");

		    retorno = NeoDateUtils.safeDateFormat(dataFinalAgendador, "yyyy-MM-dd HH:mm:ss");
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {

	}
	return retorno;

    }

    private String ultimaExecucaoRotinaRange() {

	String retorno = "";
	try {
	    Collection<NeoObject> monitoraAgendador = null;

	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("nomeRotina", "EmailAtendimentoXRED"));

	    monitoraAgendador = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);

	    if (monitoraAgendador != null && !monitoraAgendador.isEmpty()) {
		for (NeoObject neoObject : monitoraAgendador) {
		    EntityWrapper monitoraAgendadorObj = new EntityWrapper(neoObject);

		    GregorianCalendar dataFinalAgendador = (GregorianCalendar) monitoraAgendadorObj.getValue("dataFinalAgendador");

		    GregorianCalendar dataFinalAgendadorAux = (GregorianCalendar) dataFinalAgendador.clone();

		    dataFinalAgendadorAux.add(Calendar.MINUTE, -15);

		    retorno = NeoDateUtils.safeDateFormat(dataFinalAgendadorAux, "yyyy-MM-dd HH:mm:ss");
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {

	}
	return retorno;

    }

    public static List<String> dias(GregorianCalendar calendar) {
	List<String> cptList = null;
	if (NeoUtils.safeIsNotNull(calendar)) {
	    long milisecInicial = calendar.getTime().getTime();
	    long milisecFinal = new GregorianCalendar().getTime().getTime();
	    long dif = milisecFinal - milisecInicial;

	    long dias = (((dif / 1000) / 60) / 60) / 24;
	    cptList = new ArrayList<String>();
	    Calendar dia = Calendar.getInstance();
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM");
	    for (int i = 0; i <= dias; i++) {
		String diaStr = dateFormat.format(dia.getTime());
		cptList.add(diaStr);
		dia.add(Calendar.DAY_OF_MONTH, -1);
	    }

	}
	return cptList;
    }
}