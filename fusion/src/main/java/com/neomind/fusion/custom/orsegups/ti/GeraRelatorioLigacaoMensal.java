package com.neomind.fusion.custom.orsegups.ti;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.MultiPartEmail;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;


public class GeraRelatorioLigacaoMensal implements CustomJobAdapter
{
 
    @Override
    public void execute(CustomJobContext ctx)
    {
	System.out.println("Inicio para gerar relatório de ligação");
	processaJob(ctx);
    }


    public static void processaJob(CustomJobContext ctx){
	try {
	    NeoObject objeto = null;
	    Collection<NeoObject> objetosEmEspera = new ArrayList<NeoObject>();
	    Collection<NeoObject> objetos = (Collection<NeoObject>)PersistEngine.getObjects(AdapterUtils.getEntityClass("responsavelDepartamentoLigacao")); 
	    if(objetos != null && !objetos.isEmpty()){
		for (NeoObject neoObject : objetos)
		{
		    objeto = neoObject;
		    if (objeto != null){
			EntityWrapper wrapper = new EntityWrapper(objeto);
			NeoUser responsavel = (NeoUser) wrapper.findValue("responsavel");
			if (responsavel.getCode().equals("edson.heinz") || responsavel.getCode().equals("fernanda.maciel")){
			    objetosEmEspera.add(objeto);
			}else{
			    gerarRelatorio(objeto);
			}
		    }
		}
		for (NeoObject neoObject : objetosEmEspera) {
		    gerarRelatorio(neoObject);
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por [ GeraRelatorioLigacaoMensal ]");
	}
    }
    
    private static void gerarRelatorio(NeoObject objeto) throws Exception{
	EntityWrapper wrapper = new EntityWrapper(objeto);
	NeoUser responsavel = (NeoUser) wrapper.findValue("responsavel");
	RelatorioLigacao relatorioLigacao = new RelatorioLigacao();
	File file = relatorioLigacao.geraPDF(objeto);
	File fileSintetico = relatorioLigacao.geraPDFSintetico(objeto);

	MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

	EmailAttachment attachment = new EmailAttachment();
	attachment.setPath(file.getAbsolutePath());
	attachment.setDisposition(EmailAttachment.ATTACHMENT);
	attachment.setDescription("Relatório de Ligações das regionais");
	attachment.setName(file.getName());

	EmailAttachment attachmentSintetico = new EmailAttachment();
	attachmentSintetico.setPath(fileSintetico.getAbsolutePath());
	attachmentSintetico.setDisposition(EmailAttachment.ATTACHMENT);
	attachmentSintetico.setDescription("Relatório de Ligações das regionais - Sintético");
	attachmentSintetico.setName(fileSintetico.getName());

	MultiPartEmail email = new MultiPartEmail();
	String emailResp = responsavel.getEmail();
	email.addTo(emailResp);		    
	email.addCc("kelli.cunha@orsegups.com.br");
	email.addCc("controladoria@orsegups.com.br");
	email.addBcc("lucas.alison@orsegups.com.br");
	email.addBcc("emailautomatico@orsegups.com.br");
	email.setSubject("Relatório de Ligações");
	email.setMsg("Segue em anexo relatório Sintético e Analítico das ligações realizadas pelo seu setor.");
	email.attach(attachment);
	email.attach(attachmentSintetico);

	email.setFrom(settings.getFromEMail(),settings.getFromName());
	email.setSmtpPort(settings.getPort());
	email.setHostName(settings.getSmtpServer());
	email.send();
    }

}