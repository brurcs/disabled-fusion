package com.neomind.fusion.custom.orsegups.sapiens.vo;

import java.util.GregorianCalendar;

public class ContratoClienteVO {

    private int codEmp;
    private int codFil;
    private int numCtr;
    private GregorianCalendar datSus;
    private int codCli;
    private String nomCli;
    private int tipEmc;
    
    
    
    public int getTipEmc() {
        return tipEmc;
    }
    public void setTipEmc(int tipEmc) {
        this.tipEmc = tipEmc;
    }
    public int getCodEmp() {
        return codEmp;
    }
    public void setCodEmp(int codEmp) {
        this.codEmp = codEmp;
    }
    public int getCodFil() {
        return codFil;
    }
    public void setCodFil(int codFil) {
        this.codFil = codFil;
    }
    public int getNumCtr() {
        return numCtr;
    }
    public void setNumCtr(int numCtr) {
        this.numCtr = numCtr;
    }
    public GregorianCalendar getDatSus() {
        return datSus;
    }
    public void setDatSus(GregorianCalendar datSus) {
        this.datSus = datSus;
    }
    public int getCodCli() {
        return codCli;
    }
    public void setCodCli(int codCli) {
        this.codCli = codCli;
    }
    public String getNomCli() {
        return nomCli;
    }
    public void setNomCli(String nomCli) {
        this.nomCli = nomCli;
    }
    
    
    
}
