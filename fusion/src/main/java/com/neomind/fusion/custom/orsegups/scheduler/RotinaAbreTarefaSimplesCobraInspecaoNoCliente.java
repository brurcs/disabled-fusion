package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.common.NeoRunnable;
import com.neomind.fusion.custom.orsegups.mobile.vo.InspecaoClienteCobrancaVO;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

//com.neomind.fusion.custom.orsegups.scheduler.RotinaAbreTarefaSimplesCobraInspecaoNoCliente
public class RotinaAbreTarefaSimplesCobraInspecaoNoCliente implements CustomJobAdapter
{
	GregorianCalendar prazo;
	GregorianCalendar datIni;
	GregorianCalendar datFim;

	List<InspecaoClienteCobrancaVO> listaInspCliCobVO;

	final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.RotinaAbreTarefaSimplesCobraInspecaoNoCliente");

	@Override
	public void execute(CustomJobContext ctx)
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();

		log.warn("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Simples Cobrança Inspetoria no Cliente - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		try
		{
			listaInspCliCobVO = new ArrayList<InspecaoClienteCobrancaVO>();
			prazo = new GregorianCalendar();
			prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);

			datIni = new GregorianCalendar();
			datIni.set(GregorianCalendar.DATE, 1); // seta para 1 no mes atual
			datIni.add(GregorianCalendar.DATE, -1); // vira para o mes anterior
			datIni.set(GregorianCalendar.DATE, 1); // e seta o primeiro dia do mes anterior

			datFim = new GregorianCalendar();
			datFim.set(GregorianCalendar.DATE, 1); // seta para 1 no mes atual
			datFim.add(GregorianCalendar.DATE, -1); // vira para o mes anterior

			HashMap<String, List<InspecaoClienteCobrancaVO>> cobrancas = retornaClientesSemInspecao(datIni, datFim);

			Iterator<String> ite = cobrancas.keySet().iterator();

			while (ite != null && ite.hasNext())
			{

				listaInspCliCobVO.clear();
				listaInspCliCobVO = cobrancas.get(ite.next());

				final NeoRunnable work = new NeoRunnable()
				{
					public void run() throws Exception
					{

						Long codReg = listaInspCliCobVO.get(0).getCodReg();
						String nomReg = listaInspCliCobVO.get(0).getNomReg();
						String titulo = nomReg + " - Justificar falta de inspeção em clientes no período de " + NeoDateUtils.safeDateFormat(datIni, "dd/MM/yyyy") + " à " + NeoDateUtils.safeDateFormat(datFim, "dd/MM/yyyy");
						String solicitante = "beatriz.malmann";
						NeoPaper papel = OrsegupsUtils.getPapelCoordenadorRegional(codReg);

						NeoUser usuarioResponsavel = null;
						if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
						{
							for (NeoUser user : papel.getUsers())
							{
								usuarioResponsavel = user;
								break;
							}
						}

						String executor = "beatriz.malmann";
						if (usuarioResponsavel != null)
						{
							executor = usuarioResponsavel.getCode();
						}
						StringBuilder descricao = new StringBuilder();
						descricao.append("Identificamos que no período de " + NeoDateUtils.safeDateFormat(datIni, "dd/MM/yyyy") + " à " + NeoDateUtils.safeDateFormat(datFim, "dd/MM/yyyy") + " não houve nenhuma tarefa de inspetoria lançada para os clientes em anexo. Por qual motivo?");

						abrirTarefaSimplesCobrançaNoCliente(solicitante, executor, titulo, descricao, prazo, listaInspCliCobVO);
					}

				};
				try
				{
					PersistEngine.managedRun(work);
				}
				catch (final Exception e)
				{
					log.error("Erro ao abrir tarefa de cobranca de inspecao no clieente. ", e);
					e.printStackTrace();
					throw new Exception("Erro ao fazer managedRun");
				}

			}

		}
		catch (Exception e)
		{
			log.error("##### AGENDADOR DE TAREFA: Erro Abrir Tarefa Simples Cobrança Inspetoria no Cliente [" + key + "]" + e.getMessage());
			e.printStackTrace();
			throw new JobException("Erro ao processar job, procurar no log por:" + key);
		}
		finally
		{
			log.warn("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Simples Cobrança Inspetoria no Cliente - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}

	public HashMap<String, List<InspecaoClienteCobrancaVO>> retornaClientesSemInspecao(GregorianCalendar datIni, GregorianCalendar datFim) throws Exception
	{
		HashMap<String, List<InspecaoClienteCobrancaVO>> retorno = new HashMap<String, List<InspecaoClienteCobrancaVO>>();
		Long key = GregorianCalendar.getInstance().getTimeInMillis();

		Connection conn = PersistEngine.getConnection("SAPIENS");
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		Connection connFus = PersistEngine.getConnection("");
		StringBuilder sqlFus = new StringBuilder();
		PreparedStatement pstmFus = null;
		ResultSet rsFus = null;

		try
		{

			//			sql.append(" DECLARE @datIni  Datetime ");
			//			sql.append(" DECLARE @datFim  Datetime ");
			//			sql.append("  SELECT @datIni = dateadd(d, datediff(d,0, ? ), 0) "); 
			//			sql.append("  SELECT @datFim = dateadd(d, datediff(d,0, ? ), 0) ");
			//			sql.append(" select distinct cvs.usu_regcvs,reg.usu_nomreg, cli.codcli , cli.nomcli,cvs.usu_numctr, cvs.usu_numpos, vinsp.codigo, cvs.usu_endctr,cvs.usu_baictr, cvs.usu_cidctr  from e085cli cli                                              "); 
			//			sql.append(" inner join usu_t160ctr ctr with (nolock) on cli.codcli = ctr.usu_codcli                                                                              ");
			//			sql.append(" inner join usu_t160cvs cvs with (nolock) on ctr.usu_numctr = cvs.usu_numctr and ctr.usu_codemp = cvs.usu_codemp and ctr.usu_codfil = cvs.usu_codfil  ");
			//			sql.append(" and ( (cvs.usu_sitcvs = 'A') or (cvs.usu_sitcvs = 'I' and cvs.usu_datfim >= @datFim))                                                                ");
			//			sql.append(" inner join usu_t200reg reg with (nolock) on cvs.usu_regcvs = reg.usu_codreg                                                                          ");
			//			sql.append(" inner join VIEW_LOC_INSP_MOBILE vinsp with (nolock) on cvs.usu_codccu = vinsp.codigo                                                                 ");
			//			sql.append(" inner join E080SER SER ON SER.CODSER = cvs.usu_codser AND SER.CodEmp = cvs.usu_codemp                                                                ");
			//			sql.append(" where not exists (                                                                                                                                   ");
			//            sql.append("                                                                                                                                                      ");
			//			sql.append("         select pr.codCcu  from [cacupe\\sql02].Fusion_Producao.dbo.WFProcess wf with (nolock)                                                        ");
			//			sql.append("         inner join [cacupe\\sql02].Fusion_Producao.dbo.D_IMInspetoria i with (nolock) on i.neoId = wf.entity_neoId                                   ");
			//			sql.append("         inner join [cacupe\\sql02].Fusion_Producao.dbo.D_PesquisaResultado pr with (nolock) on i.relatorioInspecao_neoId = pr.neoId                  ");
			//			sql.append("         where wf.processState != 2                                                                                                                   ");
			//			sql.append("         and DATEADD(dd, DATEDIFF(dd, 0, wf.startDate), 0) >= DATEADD(dd, DATEDIFF(dd, 0, @datIni ), 0)                                          	  ");
			//			sql.append("         and DATEADD(dd, DATEDIFF(dd, 0, wf.startDate), 0) <= DATEADD(dd, DATEDIFF(dd, 0, @datFim ), 0)                                          	  ");
			//			sql.append("         and pr.codCcu = vinsp.codigo                                                                                                                 ");
			//			sql.append("                                                                                                                                                      ");
			//			sql.append(" )                                                                                                                                                    ");
			//			sql.append(" and ctr.usu_numctr <> 1                                                                                                     ");
			//			sql.append(" and cli.codcli not in(26358)                                                                                                                         ");
			//			sql.append(" and SER.DesSer not like '%extra%' and SER.CodSer <> '9001513MTA' 																					  ");
			//			sql.append(" and SER.CodFam in('SER101','SER103') 																												  ");
			//			sql.append(" group by cvs.usu_regcvs,reg.usu_nomreg, cli.codcli , cli.nomcli,cvs.usu_numctr, cvs.usu_numpos, vinsp.codigo, cvs.usu_endctr,cvs.usu_baictr, cvs.usu_cidctr   ");
			//			sql.append(" order by cvs.usu_regcvs, cli.codcli                																								  ");

			sql.append(" DECLARE @datIni  Datetime ");
			sql.append(" DECLARE @datFim  Datetime ");
			sql.append(" SELECT @datIni = dateadd(d, datediff(d,0, ? ), 0) ");
			sql.append("  SELECT @datFim = dateadd(d, datediff(d,0, ? ), 0) ");
			sql.append(" select cvs.usu_regcvs,reg.usu_nomreg, cli.codcli , cli.nomcli,cvs.usu_numctr, cvs.usu_numpos, vinsp.codigo, cvs.usu_endctr,cvs.usu_baictr, cvs.usu_cidctr  from e085cli cli	");
			sql.append(" inner join usu_t160ctr ctr with (nolock) on cli.codcli = ctr.usu_codcli                                                                              			");
			sql.append(" inner join usu_t160cvs cvs with (nolock) on ctr.usu_numctr = cvs.usu_numctr and ctr.usu_codemp = cvs.usu_codemp and ctr.usu_codfil = cvs.usu_codfil  			");
			sql.append(" and ( (cvs.usu_sitcvs = 'A') or (cvs.usu_sitcvs = 'I' and cvs.usu_datfim >= @datFim))                                                                			");
			sql.append(" inner join usu_t200reg reg with (nolock) on cvs.usu_regcvs = reg.usu_codreg                                                                          			");
			sql.append(" inner join VIEW_LOC_INSP_MOBILE vinsp with (nolock) on cvs.usu_codccu = vinsp.codigo                                                                 			");
			sql.append(" inner join E080SER SER ON SER.CODSER = cvs.usu_codser AND SER.CodEmp = cvs.usu_codemp                                                                			");
			sql.append(" where ctr.usu_numctr <> 1                                                                                                    						  			");
			sql.append(" and cli.codcli not in ( 26358,71296,74334,74676,68874,74934,76003,90206,90217,90703,92096,92224,92694,93392,94780,97323,98398,102140,92097,102911,103592,157964,90342,72099,102063,102948,156017 ) ");
			sql.append(" and SER.DesSer not like '%extra%' and SER.CodSer <> '9001513MTA' 																					  		   	");
			sql.append(" and SER.CodFam in('SER101','SER103')																					 		   	");
			sql.append(" group by cvs.usu_regcvs,reg.usu_nomreg, cli.codcli , cli.nomcli,cvs.usu_numctr, cvs.usu_numpos, vinsp.codigo, cvs.usu_endctr,cvs.usu_baictr, cvs.usu_cidctr   	");
			sql.append(" order by cvs.usu_regcvs, cli.codcli                																								  			");

			pstm = conn.prepareStatement(sql.toString());
			pstm.setDate(1, new Date(datIni.getTimeInMillis()));
			pstm.setDate(2, new Date(datFim.getTimeInMillis()));

			rs = pstm.executeQuery();
			Long codReg = -1L;
			String nomReg = "";
			List<InspecaoClienteCobrancaVO> listaClientesRegional = new ArrayList<InspecaoClienteCobrancaVO>();
			Long posAdd = 0L;
			Long cliAnt = 0L;
			Long cliNov = -1L;

			while (rs.next())
			{
				cliNov = rs.getLong("codcli");
				sqlFus = new StringBuilder();
				connFus = PersistEngine.getConnection("");

				sqlFus.append(" DECLARE @datIni  Datetime ");
				sqlFus.append(" DECLARE @datFim  Datetime ");
				sqlFus.append(" SELECT @datIni = dateadd(d, datediff(d,0, ? ), 0) ");
				sqlFus.append(" SELECT @datFim = dateadd(d, datediff(d,0, ? ), 0) ");
				sqlFus.append(" 	select pr.codCcu  from [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.WFProcess wf with (nolock)   ");
				sqlFus.append("			inner join [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.D_IMInspetoria i with (nolock) on i.neoId = wf.entity_neoId");
				sqlFus.append("			inner join [CACUPE\\SQL02].FUSION_PRODUCAO.DBO.D_PesquisaResultado pr with (nolock) on i.relatorioInspecao_neoId = pr.neoId   ");
				sqlFus.append(" 		where wf.processState != 2      ");
				sqlFus.append("			and DATEADD(dd, DATEDIFF(dd, 0, wf.startDate), 0) >= DATEADD(dd, DATEDIFF(dd, 0, @datIni ), 0)   ");
				sqlFus.append("			and DATEADD(dd, DATEDIFF(dd, 0, wf.startDate), 0) <= DATEADD(dd, DATEDIFF(dd, 0, @datFim ), 0)   ");
				sqlFus.append("			and pr.codCcu = '" + rs.getString("codigo")+"'");
				
				pstmFus = connFus.prepareStatement(sqlFus.toString());
				pstmFus.setDate(1, new Date(datIni.getTimeInMillis()));
				pstmFus.setDate(2, new Date(datFim.getTimeInMillis()));

				rsFus = pstmFus.executeQuery();

				if (!rsFus.next())
				{
					if (!cliAnt.equals(cliNov))
					{
						posAdd = 0L;
						cliAnt = cliNov;
					}

					if (posAdd == 0L)
					{
						posAdd = 1L;
						InspecaoClienteCobrancaVO ic = new InspecaoClienteCobrancaVO();
						ic.setCodCli(rs.getLong("codcli"));
						ic.setNomCli(rs.getString("nomcli"));
						ic.setNomReg(rs.getString("usu_nomreg"));
						ic.setCodReg(rs.getLong("usu_regcvs"));
						ic.setNumCtr(rs.getLong("usu_numctr"));
						ic.setNumPos(rs.getLong("usu_numpos"));
						ic.setCodCcu(rs.getString("codigo"));
						ic.setEndCtr(rs.getString("usu_endctr"));
						ic.setBaiCtr(rs.getString("usu_baictr"));
						ic.setCidCtr(rs.getString("usu_cidctr"));
						ic.setCodCcu(rsFus.getString("codCcu"));

						if (codReg.equals(-1L))
						{
							codReg = rs.getLong("usu_regcvs");
							nomReg = rs.getString("usu_nomreg");
						}

						if (codReg == rs.getLong("usu_regcvs"))
						{

							listaClientesRegional.add(ic);

						}
						else
						{
							if (listaClientesRegional.size() == 0)
							{
								listaClientesRegional.add(ic);
							}
							else
							{
								retorno.put(nomReg, listaClientesRegional);

								nomReg = rs.getString("usu_nomreg");
								codReg = rs.getLong("usu_regcvs");
								listaClientesRegional = new ArrayList<InspecaoClienteCobrancaVO>();
								listaClientesRegional.add(ic);
							}
						}
					}
				}
				else
				{
					if (cliAnt.equals(cliNov) && !posAdd.equals(2L))
					{
						posAdd = 2L;
						listaClientesRegional.remove(listaClientesRegional.size() - 1);
					}
				}

				OrsegupsUtils.closeConnection(connFus, pstmFus, rsFus);

				//			if (rs.next())
				//			{
				//				nomReg = rs.getString("usu_nomreg");
				//				codReg = rs.getLong("usu_regcvs");
				//				do
				//				{
				//
				//					InspecaoClienteCobrancaVO ic = new InspecaoClienteCobrancaVO();
				//					ic.setCodCli(rs.getLong("codcli"));
				//					ic.setNomCli(rs.getString("nomcli"));
				//					ic.setNomReg(rs.getString("usu_nomreg"));
				//					ic.setCodReg(rs.getLong("usu_regcvs"));
				//					ic.setNumCtr(rs.getLong("usu_numctr"));
				//					ic.setNumPos(rs.getLong("usu_numpos"));
				//					ic.setCodCcu(rs.getString("codigo"));
				//					ic.setEndCtr(rs.getString("usu_endctr"));
				//					ic.setBaiCtr(rs.getString("usu_baictr"));
				//					ic.setCidCtr(rs.getString("usu_cidctr"));
				//
				//					if (codReg == rs.getLong("usu_regcvs"))
				//					{
				//
				//						listaClientesRegional.add(ic);
				//
				//					}
				//					else
				//					{
				//						retorno.put(nomReg, listaClientesRegional);
				//
				//						nomReg = rs.getString("usu_nomreg");
				//						codReg = rs.getLong("usu_regcvs");
				//						listaClientesRegional = new ArrayList<InspecaoClienteCobrancaVO>();
				//
				//						listaClientesRegional.add(ic);
				//
				//					}
				//
				//				}
			}

			if (listaClientesRegional.size() > 0)
			{
				retorno.put(nomReg, listaClientesRegional);
			}
			return retorno;
		}
		catch (Exception e)
		{
			log.error("Erro ao consultar clientes sem inspeção [" + key + "]" + e.getMessage());
			e.printStackTrace();
			throw new Exception("Erro ao consultar clientes sem inspeção: chave de log:" + key);
		}
		finally
		{

			OrsegupsUtils.closeConnection(conn, pstm, rs);
			OrsegupsUtils.closeConnection(connFus, pstmFus, rsFus);

		}
	}

	private void abrirTarefaSimplesCobrançaNoCliente(String solicitante, String executor, String titulo, StringBuilder descricao, GregorianCalendar prazo, List<InspecaoClienteCobrancaVO> InspCliCobVO) throws Exception
	{

		NeoFile anexo = criaAnexoTarefa(InspCliCobVO);

		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao.toString(), "1", "sim", prazo, anexo);

	}

	/**
	 * Gera um arquivo de excel e retorna o neofile criado pelo fusion para anexar na tarefa simples.
	 * 
	 * @param InspCliCobVO
	 * @return
	 * @throws Exception
	 */
	public NeoFile criaAnexoTarefa(List<InspecaoClienteCobrancaVO> InspCliCobVO) throws Exception
	{
		NeoFile anexo = null;
		try
		{
			String nomreg = InspCliCobVO.get(0).getNomReg();

			String filename = "\\\\ssoovt09\\f$\\Sistemas\\Fusion\\Temp\\Cobranca_" + nomreg + ".xls";
			FileOutputStream fileOut = new FileOutputStream(filename);
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("Clientes sem inspeção");

			HSSFRow rowhead = sheet.createRow((short) 0);
			rowhead.createCell(0).setCellValue("Código Regional");
			rowhead.createCell(1).setCellValue("Regional");
			rowhead.createCell(2).setCellValue("Código Cliente");
			rowhead.createCell(3).setCellValue("Cliente");
			rowhead.createCell(4).setCellValue("Centro de Custo");
			
			int i = 1;
			for (InspecaoClienteCobrancaVO ic : InspCliCobVO)
			{
				HSSFRow row = sheet.createRow((short) i);
				row.createCell(0).setCellValue(ic.getCodReg());
				row.createCell(1).setCellValue(ic.getNomReg());
				row.createCell(2).setCellValue(ic.getCodCli());
				row.createCell(3).setCellValue(ic.getNomCli());
				row.createCell(4).setCellValue(ic.getCodCcu());
			
				i++;
			}

			workbook.write(fileOut);
			fileOut.close();

			anexo = OrsegupsUtils.criaNeoFile(new File("\\\\ssoovt09\\f$\\Sistemas\\Fusion\\Temp\\Cobranca_" + nomreg + ".xls"));
			return anexo;
		}
		catch (Exception ex)
		{
			System.out.println("Erro ao gerar anexo para a tarefas de cobrança de inspeção de clientes");
			throw new Exception("Erro ao gerar anexo para a tarefas de cobrança de inspeção de clientes. msg:", ex);
		}

	}

}
