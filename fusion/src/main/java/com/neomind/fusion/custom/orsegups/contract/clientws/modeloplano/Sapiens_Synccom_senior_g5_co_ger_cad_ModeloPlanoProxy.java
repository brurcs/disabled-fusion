package com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano;

import java.rmi.RemoteException;


public class Sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoProxy implements com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano.Sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlano {
  private String _endpoint = null;
  private com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano.Sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlano sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlano = null;
  
  public Sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoProxy() {
    _initSapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoProxy();
  }
  
  public Sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoProxy(String endpoint) {
    _endpoint = endpoint;
    _initSapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoProxy();
  }
  
  private void _initSapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoProxy() {
    try {
      sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlano = (new com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano.G5SeniorServicesLocator()).getsapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoPort();
      if (sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlano != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlano)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlano)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlano != null)
      ((javax.xml.rpc.Stub)sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlano)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.neomind.fusion.custom.orsegups.contract.clientws.modeloplano.Sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlano getSapiens_Synccom_senior_g5_co_ger_cad_ModeloPlano() {
    if (sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlano == null)
      _initSapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoProxy();
    return sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlano;
  }

@Override
public ModeloPlanoGerarContasOut gerarContas(String user, String password,
		int encryption, ModeloPlanoGerarContasIn parameters)
		throws RemoteException {
	if (sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlano == null){
		_initSapiens_Synccom_senior_g5_co_ger_cad_ModeloPlanoProxy();
	}
	return sapiens_Synccom_senior_g5_co_ger_cad_ModeloPlano.gerarContas(user, password, encryption, parameters);
}
  
  
}