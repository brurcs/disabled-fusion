package com.neomind.fusion.custom.orsegups.contract;

public class ListaEquipamentosMinutaDataSource
{
	private String qtd = "";
	private String item = "";
	private String locado = "";
	private String IX = "";
	
	public String getQtd()
	{
		return qtd;
	}
	public void setQtd(String qtd)
	{
		this.qtd = qtd;
	}
	public String getItem()
	{
		return item;
	}
	public void setItem(String item)
	{
		this.item = item;
	}
	public String getLocado()
	{
		return locado;
	}
	public void setLocado(String locado)
	{
		this.locado = locado;
	}
	public String getIX()
	{
		return IX;
	}
	public void setIX(String iX)
	{
		IX = iX;
	}
}
