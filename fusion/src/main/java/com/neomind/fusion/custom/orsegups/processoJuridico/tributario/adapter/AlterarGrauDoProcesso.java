package com.neomind.fusion.custom.orsegups.processoJuridico.tributario.adapter;

import java.util.ArrayList;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.AlterarGrauDoProcesso

public class AlterarGrauDoProcesso implements AdapterInterface
{
	@Override
	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
	    try{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		List<NeoObject> lisPedAnt = new ArrayList<NeoObject>();
		List<NeoObject> lisPedNova = new ArrayList<NeoObject>();

		if (origin.getActivityName().toString().contains("Movimentar Processo"))
		{
		    
        		Integer iTipoProcesso = 0;
        		NeoObject tipoProcesso = (NeoObject) processEntity.getValue("tipoProcesso");
        	    	if (tipoProcesso != null) {
        	    	    EntityWrapper wTipoProcesso = new EntityWrapper(tipoProcesso);
        	    	    iTipoProcesso = Integer.parseInt(wTipoProcesso.getValue("codigo").toString());
        	    	}
        	    	
        	    	if (iTipoProcesso == 3) {
                	    	if (String.valueOf(processEntity.getValue("grauProcesso")).contains("1º"))
        			{
        				processEntity.setValue("grauProcesso", "2º Grau");
        				lisPedAnt = (List<NeoObject>) processEntity.getValue("lisPed");
        			}
        			else if (String.valueOf(processEntity.getValue("grauProcesso")).contains("2º"))
        			{
        				processEntity.setValue("grauProcesso", "3º Grau");
        				lisPedAnt = (List<NeoObject>) processEntity.getValue("lisPedSegGrau");
        			}
        			else if (String.valueOf(processEntity.getValue("grauProcesso")).contains("3º"))
        			{
        				processEntity.setValue("grauProcesso", "4º Grau");
        				lisPedAnt = (List<NeoObject>) processEntity.getValue("lisPedTerGrau");
        			}
        			else
        			{
        				processEntity.setValue("grauProcesso", "5º Grau");
        			}
        	    	} else {

        			if (String.valueOf(processEntity.getValue("grauProcesso")).contains("1º"))
        			{
        				processEntity.setValue("grauProcesso", "2º Grau");
        				lisPedAnt = (List<NeoObject>) processEntity.getValue("lisPed");
        			}
        			else if (String.valueOf(processEntity.getValue("grauProcesso")).contains("2º"))
        			{
        				processEntity.setValue("grauProcesso", "3º Grau");
        				lisPedAnt = (List<NeoObject>) processEntity.getValue("lisPedSegGrau");
        			}
        			else
        			{
        				processEntity.setValue("grauProcesso", "4º Grau");
        			}
        	    	}

			if (lisPedAnt.size() > 0)
			{
				for (NeoObject pedido : lisPedAnt)
				{
					EntityWrapper wPedido = new EntityWrapper(pedido);
					InstantiableEntityInfo insPedido = AdapterUtils.getInstantiableEntityInfo("j002LisPed");
					NeoObject objPedido = insPedido.createNewInstance();
					EntityWrapper wNovoPedido = new EntityWrapper(objPedido);
					wNovoPedido.setValue("tipoPedido", wPedido.getValue("tipoPedido"));
					wNovoPedido.setValue("obsGeral", wPedido.getValue("obsGeral"));
					wNovoPedido.setValue("j002ClaPed", wPedido.getValue("j002ClaPed"));
					wNovoPedido.setValue("obsAdmOpe", wPedido.getValue("obsAdmOpe"));
					wNovoPedido.setValue("recorrente", wPedido.getValue("recorrente"));
					wNovoPedido.setValue("qtdMesTrab", wPedido.getValue("qtdMesTrab"));
					wNovoPedido.setValue("vlrUniMen", wPedido.getValue("vlrUniMen"));
					wNovoPedido.setValue("vlrPed", wPedido.getValue("vlrPed"));
					wNovoPedido.setValue("vlrFinalPedido", wPedido.getValue("vlrFinalPedido"));
					List<NeoObject> lisTar = (List<NeoObject>) wPedido.getValue("lisTar");
					List<NeoObject> novaLisTar = new ArrayList<NeoObject>();

					for (NeoObject tarefa : lisTar)
					{
						EntityWrapper wTarefa = new EntityWrapper(tarefa);
						InstantiableEntityInfo insTarefa = AdapterUtils.getInstantiableEntityInfo("j002LisTar");
						NeoObject objTarefa = insTarefa.createNewInstance();
						EntityWrapper wNovaTarefa = new EntityWrapper(objTarefa);
						wNovaTarefa.setValue("solicitante", wTarefa.getValue("solicitante"));
						wNovaTarefa.setValue("executorTar", wTarefa.getValue("executorTar"));
						wNovaTarefa.setValue("titulo", wTarefa.getValue("titulo"));
						wNovaTarefa.setValue("descricao", wTarefa.getValue("descricao"));
						wNovaTarefa.setValue("prazo", wTarefa.getValue("prazo"));
						wNovaTarefa.setValue("anexo", wTarefa.getValue("anexo"));
						wNovaTarefa.setValue("neoIdTarefa", wTarefa.getValue("neoIdTarefa"));
						PersistEngine.persist(tarefa);
						novaLisTar.add(tarefa);
					}

					wNovoPedido.setValue("lisTar", novaLisTar);

					PersistEngine.persist(objPedido);
					lisPedNova.add(objPedido);
				}

				if (String.valueOf(processEntity.getValue("grauProcesso")).contains("2"))
				{
					processEntity.setValue("lisPedSegGrau", lisPedNova);
				}
				else if (String.valueOf(processEntity.getValue("grauProcesso")).contains("3"))
				{
					processEntity.setValue("lisPedTerGrau", lisPedNova);
				}
				else if (String.valueOf(processEntity.getValue("grauProcesso")).contains("4"))
				{
					processEntity.setValue("lisPedQuaGrau", lisPedNova);
				}
			}
		}
		else
		{
			NeoObject objContador = (NeoObject) processEntity.getValue("j002ContadorProcessoJuridico");
			EntityWrapper wContador = new EntityWrapper(objContador);
			
			processEntity.setValue("j002ContadorProcessoJuridico", lisPedNova);
			

		}
	    }catch(Exception e){
		System.out.println("Erro na classe AlterarGrauDoProcesso do fluxo J002.");
		e.printStackTrace();
		throw new WorkflowException(e.getMessage());
	    }
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
