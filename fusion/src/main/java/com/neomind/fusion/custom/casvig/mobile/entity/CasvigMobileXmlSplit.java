package com.neomind.fusion.custom.casvig.mobile.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.annotations.FieldShowOrder;
import com.neomind.fusion.entity.annotations.NeoField;

/**
 * Entidade resposável por fazer o controle da geração de partes de XML que
 * devem ser enviados para o mobile.
 * 
 * @author Daniel Henrique Joppi 19/11/2009
 * 
 */
@Entity
public class CasvigMobileXmlSplit extends NeoObject {

	/*
	 * tipos de mobile entity
	 */
	public static final String ME_CLIENT = "Client";
	public static final String ME_RESEARCH = "Research";
	
	private String mobileEntity;
	
	private String idSplit;
	
	private Integer part;
	
	private String xml;
	
	public CasvigMobileXmlSplit() {
		
	}

	public CasvigMobileXmlSplit(String mobileEntity, String idSplit, Integer part, String xmlPart) {
		this.mobileEntity = mobileEntity;
		this.idSplit = idSplit;
		this.part = part;
		this.xml = xmlPart;
	}
	
	@NeoField(viewable = false, editable = false, selectable = false)
	@FieldShowOrder(order = 10)
	public String getMobileEntity() {
		return mobileEntity;
	}

	public void setMobileEntity(String mobileEntity) {
		this.mobileEntity = mobileEntity;
	}

	@NeoField(viewable = false, editable = false, selectable = false)
	@FieldShowOrder(order = 20)
	public String getIdSplit() {
		return idSplit;
	}

	public void setIdSplit(String idSplit) {
		this.idSplit = idSplit;
	}

	@NeoField(viewable = false, editable = false, selectable = false)
	@FieldShowOrder(order = 30)
	public Integer getPart() {
		return part;
	}

	public void setPart(Integer part) {
		this.part = part;
	}
	
	@NeoField(viewable = false, editable = false, selectable = false)
	@FieldShowOrder(order = 50)
	@Lob
	@Column(length = Integer.MAX_VALUE)
	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}
}
