package com.neomind.fusion.custom.orsegups.utils;

import java.util.GregorianCalendar;

import org.joda.time.DateTime;

public class AnulacaoPontoVO {

	private Long codigoCliente = 0L;
	private String razaoSocial = null;
	private String zonaAnulada = null;
	private GregorianCalendar dataRecebido = null;
	
	
	public Long getCodigoCliente() {
		return codigoCliente;
	}
	public void setCodigoCliente(Long codigoCliente) {
		this.codigoCliente = codigoCliente;
	}
	public String getZonaAnulada() {
		return zonaAnulada;
	}
	public void setZonaAnulada(String zonaAnulada) {
		this.zonaAnulada = zonaAnulada;
	}
	public GregorianCalendar getDataRecebido() {
		return dataRecebido;
	}
	public void setDataRecebido(GregorianCalendar dataRecebido) {
		this.dataRecebido = dataRecebido;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	
	
	
	
}
