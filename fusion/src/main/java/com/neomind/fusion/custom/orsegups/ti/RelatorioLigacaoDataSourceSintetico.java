package com.neomind.fusion.custom.orsegups.ti;

import java.sql.Timestamp;





public class RelatorioLigacaoDataSourceSintetico{
    
    private String nomeRegional;
    private String departamento;
    private String usuario;
    private Long duracao;
    private Long totalLigacao;
    private String competencia;
    private Timestamp duracaoLigacao;
    
    public String getNomeRegional() {
        return nomeRegional;
    }
    public void setNomeRegional(String nomeRegional) {
        this.nomeRegional = nomeRegional;
    }
    public String getDepartamento() {
        return departamento;
    }
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }
    public String getUsuario() {
        return usuario;
    }
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    public Long getDuracao() {
        return duracao;
    }
    public void setDuracao(Long duracao) {
        this.duracao = duracao;
    }
    public Long getTotalLigacao() {
        return totalLigacao;
    }
    public void setTotalLigacao(Long totalLigacao) {
        this.totalLigacao = totalLigacao;
    }       
    public String getCompetencia() {
	return competencia;
    }
    public void setCompetencia(String competencia) {
	this.competencia = competencia;
    }
    public Timestamp getDuracaoLigacao() {
	return duracaoLigacao;
    }
    public void setDuracaoLigacao(Timestamp duracaoLigacao) {
	this.duracaoLigacao = duracaoLigacao;
    }
}
