package com.neomind.fusion.custom.orsegups.scheduler;

import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.UserActivity;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoUtils;

public class AbreTarefaSimplesOciosoReservaTecnica implements CustomJobAdapter
{

	@Override
	public void execute(CustomJobContext ctx)
	{
	}
	
	
	public static void abrirTarefaSimples(Long numcad, String nomfun, Long numemp, Long codReg, String lotacao ){
		
		
		GregorianCalendar cpt = new GregorianCalendar();

				
		
		String msg = "Colaborador ocioso lotado em RT: "+numemp+" "+numcad+"-"+nomfun+"";
		
		NeoObject oTarefa = AdapterUtils.createNewEntityInstance("Tarefa");
		
		final EntityWrapper wTarefa= new EntityWrapper(oTarefa);
		
		NeoUser usuarioSolicitante = PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "beatriz.malmann" ) );
		NeoUser usuarioResponsavel = PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "danilo.silva") );
		

		/*
		NeoPaper papel = OrsegupsUtils.getPapelJustificarAfastamento(codReg, lotacao);
		if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
		{
			for (NeoUser user : papel.getUsers())
			{
				usuarioResponsavel = user;
				break;
			}
		}
		*/
		
		
		GregorianCalendar prazo = new GregorianCalendar();
		prazo.add(GregorianCalendar.DAY_OF_MONTH, 2);
		prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
		prazo.set(GregorianCalendar.MINUTE, 23);
		prazo.set(GregorianCalendar.SECOND, 59);
		
		
		
		wTarefa.findField("Solicitante").setValue(usuarioSolicitante);
		wTarefa.findField("Executor").setValue(usuarioResponsavel);
		wTarefa.findField("Titulo").setValue("Ocioso em Reserva Técnica - " + nomfun);
		wTarefa.findField("DescricaoSolicitacao").setValue(msg);
		wTarefa.findField("Prazo").setValue(OrsegupsUtils.getNextWorkDay(prazo));
		wTarefa.findField("dataSolicitacao").setValue(new GregorianCalendar());
		
		PersistEngine.persist(oTarefa);
		
		iniciaProcessoTarefaSimples(oTarefa, usuarioSolicitante, usuarioResponsavel);
	}

	public static Boolean iniciaProcessoTarefaSimples(NeoObject eformProcesso, NeoUser requester, NeoUser responsavel)
	{
		Boolean result = false;
	
		QLEqualsFilter equal = new QLEqualsFilter("Name", "G001 - Tarefa Simples");
		ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);
		
		WFProcess proc = processModel.startProcess(eformProcesso, false, null, null, null, null, requester);
		proc.setRequester(requester);
		System.out.println("[OCIOSO RESERVA TECNICA] - Abriu Tarefa simples n. " + proc.getCode());
	
		proc.setSaved(true);
	
		// Necessita persistir para que o metodo getOpenActivities() consiga buscar as atividades abertas
		PersistEngine.persist(proc);
		PersistEngine.commit(true);
	
		/* Finaliza a primeira tarefa */
		Task task = null;
	
		final List<Activity> acts = proc.getOpenActivities();
		System.out.println("acts: " + acts);
		if (acts != null)
		{
			System.out.println("acts.size(): " + acts.size());
		}
		final Activity activity1 = (Activity) acts.get(0);
		if (activity1 instanceof UserActivity)
		{
			try
			{
				if (((UserActivity) activity1).getTaskList().size() <= 0)
				{
					task = activity1.getModel().getTaskAssigner().assign((UserActivity) activity1, responsavel);
					OrsegupsWorkflowHelper.finishTaskByActivity(activity1);
				}
				else
				{
					task = ((UserActivity) activity1).getTaskList().get(0);
					OrsegupsWorkflowHelper.finishTaskByActivity(activity1);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				System.err.println("[OCIOSO RESERVA TECNICA] - Erro ao iniciar a primeira atividade do processo.");
			}
		}
		return result;
	}

	

}
