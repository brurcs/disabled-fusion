package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class RDCGeraOSSigma implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{

		Connection connectionSigma = OrsegupsUtils.getSqlConnection("SIGMA90");
		PreparedStatement pstm = null;

		StringBuffer sqlInsertOS = new StringBuffer();
		sqlInsertOS.append(" INSERT INTO dbORDEM (CD_CLIENTE, ID_INSTALADOR, ABERTURA,  DEFEITO, OPABRIU, IDOSDEFEITO, FG_EMAIL_ENVIADO, CD_OS_SOLICITANTE, FG_TODAS_PARTICOES_EM_MANUTENCAO, DATAAGENDADA, EXECUTADO, TEMPOEXECUCAOPREVISTO ) ");
		sqlInsertOS.append(" VALUES(               ?,         ?,             GETDATE(), ?,       ?,       ?,           ?,                ?,                 ?,                                ?,            ?,         1 )");

		StringBuffer sqlSelectDados = new StringBuffer();
		sqlSelectDados.append(" SELECT CD_CLIENTE, ID_INSTALADOR, DEFEITO FROM dbOrdem ");
		sqlSelectDados.append(" WHERE ID_ORDEM = ? ");

		try
		{
			connectionSigma.setAutoCommit(false);

			//SELECIONAR DADOS PARA INSERCAO
			pstm = connectionSigma.prepareStatement(sqlSelectDados.toString());
			pstm.setLong(1, (Long) processEntity.findValue("idordem"));
			ResultSet rs = pstm.executeQuery();

			String defeito = "";
			Long cdTecnico = null;
			Long cdCliente = null;
			boolean existeEqp = false;

			if (rs.next())
			{
				defeito += rs.getString("DEFEITO");
				cdTecnico = rs.getLong("ID_INSTALADOR");
				cdCliente = rs.getLong("CD_CLIENTE");
			}

			//LIMPA PREPAREDSTATEMENT PARA REALIZAR INSERT
			pstm.close();
			pstm = null;
			pstm = connectionSigma.prepareStatement(sqlInsertOS.toString());

			// CD_CLIENTE
			pstm.setLong(1, cdCliente);

			// ID_INSTALADOR - Técnico responsável informado no cadastro
			pstm.setLong(2, cdTecnico);

			//LIKE '%Equipamentos a Retirar%'
			if ((defeito.contains("Equipamentos a Retirar")) && (defeito.contains("ATENÇÃO")))
			{
				//SET FLAG PARA REALIZAR VERIFICAÇÃO DE EQUIPAMENTO EM IDOSDEFEITO
				existeEqp = true;
			}

			//VERIFICA-SE QUAL ATIVIDADE DEU ORIGEM A TAREFA
			Long situacao = (Long) processEntity.findValue("tipoSituacao");
			String frase = "";
			if (situacao == 0L)
			{
				frase = "Sem sucesso no fechamento do acordo. Providenciar a execução desta OS e não pausá-la novamente.";
			}
			else
			{
				frase = "Cliente não cumpriu o acordo da negociação. Providenciar a execução desta OS e não pausá-la novamente.";
			}

			//FORMATAMOS O TEXTO DA OS
			String textoDefeito = "ATENÇÃO:_ Verificar se o cliente possui placas e se todas estão em perfeitas condições, enviar FOTO no anexo. Conferir se a(s) central(is) deste cliente estão com a programação correta de envio de RESTAURO no ato do fechamento da zona. Dúvidas entrar em contato com a Assessoria Técnica. \r\n";
			textoDefeito = textoDefeito + frase + "\n";
			textoDefeito = textoDefeito + defeito;

			// DEFEITO
			pstm.setString(3, textoDefeito);

			if (existeEqp)
			{
				// IDOSDEFEITO = 167 OS-RETIRAR EQUIPAMENTO
				pstm.setInt(5, 167);
			}
			else
			{
				// IDOSDEFEITO = 166 OS-DESABILITAR MONITORAMENT
				pstm.setInt(5, 166);
			}

			// OPABRIU
			pstm.setInt(4, 11010);
			// Se contrato for com locação (total ou parcial), Motivo da
			// OS muda para Retira equipamento

			// FG_EMAIL_ENVIADO = 0
			pstm.setInt(6, 0);

			// CD_OS_SOLICITANTE = 10011 - DEMAIS ORDENS DE SERVIÇO
			pstm.setInt(7, 10011);

			// FG_TODAS_PARTICOES_EM_MANUTENCAO
			pstm.setInt(8, 0);

			// DATA AGENDADA
			GregorianCalendar dataFinalMonitoramento = new GregorianCalendar();
			dataFinalMonitoramento = OrsegupsUtils.addDays(dataFinalMonitoramento, +2, true);
			if(OrsegupsUtils.isWorkDay(dataFinalMonitoramento)){
				dataFinalMonitoramento = OrsegupsUtils.addDays(dataFinalMonitoramento, +2, true);
			}
			dataFinalMonitoramento = OrsegupsUtils.getNextWorkDay(dataFinalMonitoramento);
			Timestamp timestamp = new Timestamp(dataFinalMonitoramento.getTimeInMillis());

			pstm.setTimestamp(9, timestamp);

			// EXECUTADO
			pstm.setString(10, "");

			pstm.executeUpdate();

		}
		catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				connectionSigma.rollback();
			}
			catch (SQLException e1)
			{
				e1.printStackTrace();
				throw new WorkflowException("Falha ao tentar gerar OS.");
			}
			finally
			{
				throw new WorkflowException("Falha ao tentar gerar OS. Erro: " + e.getMessage());
			}
		}
		finally
		{
			try
			{
				if (pstm != null)
				{
					pstm.close();
				}
				if (connectionSigma != null)
				{
					connectionSigma.commit();
					connectionSigma.close();
				}
			}
			catch (SQLException e)
			{
				e.printStackTrace();
				throw new WorkflowException("Erro ao tentar gerar OS.");
			}
		}

	}

	@Override
	public void back(EntityWrapper arg0, Activity arg1)
	{
		// TODO Auto-generated method stub

	}
}
