package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.ql.QLAdapter;
import com.neomind.fusion.datawarehouse.ExternalEntityInfo;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilterIsNull;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class FCIRegistraSPCSERASA implements AdapterInterface
{

	private static final Log log = LogFactory.getLog(FCIRegistraSPCSERASA.class);
	EntityManager entityManager;
	EntityTransaction transaction;

	public void back(EntityWrapper entitywrapper, Activity activity1)
	{
	}

	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{
			Long ocorrencia = 1L; // SPC
			String tipoCliente = (String) processEntity.findField("contratoSapiens.tipcli").getValue();
			if ("J".equals(tipoCliente))
			{
				ocorrencia = 2L; // SERASA
			}
			List<NeoObject> listaTitulos = (List<NeoObject>) processEntity.findField("listaTitulosSPC").getValue();

			if (listaTitulos != null && !listaTitulos.isEmpty())
			{

				for (NeoObject titulo : listaTitulos)
				{
					EntityWrapper wrapper = new EntityWrapper(titulo);
					if (wrapper != null)
					{
						Long codEmp = (Long) wrapper.findValue("empresa");
						Long codFil = (Long) wrapper.findValue("filial");
						String numTit = (String) wrapper.findValue("numeroTitulo");
						String codTpt = (String) wrapper.findValue("tipoTitulo");
						Boolean cadSPC = (Boolean) wrapper.findValue("cadSPC");
						System.out.println("Começou o processo da Classe FCIRegistraSPCSERASA metodo start. Empresa : " + codEmp + ", filial : " + codFil + ", nº Titulo : " + numTit + ", Tipo Titulo : " + codTpt + ", CadSPC : " + cadSPC);
						// Verifica se jah estah registrado
						if ((!tituloJaRegistrado(codEmp, codFil, numTit, codTpt)) && (cadSPC == true))
						{
							GregorianCalendar data = new GregorianCalendar((new GregorianCalendar()).get(GregorianCalendar.YEAR), (new GregorianCalendar()).get(GregorianCalendar.MONTH), (new GregorianCalendar()).get(GregorianCalendar.DAY_OF_MONTH));
							GregorianCalendar hora = new GregorianCalendar();
							Long minutosInicio = (long) (hora.get(Calendar.HOUR_OF_DAY) * 60 + hora.get(Calendar.MINUTE));

							String texto = "Fusion: Inserido o título no orgão de crédito pela Tarefa FCI: " + activity.getProcess().getCode();
							Long codigoUsuarioSapiens = buscaUsuarioSapiensCorrespondente(((NeoUser) processEntity.findField("usuario13").getValue()).getCode());

							try
							{

								this.insereRegistroOrgaoCreditoSapiens(codEmp, codFil, numTit, codTpt, data, texto, codigoUsuarioSapiens, ocorrencia);
								this.insereObservacaoTituloSapiens(codEmp, codFil, numTit, codTpt, data, minutosInicio, texto, codigoUsuarioSapiens, ocorrencia);

							}
							catch (WorkflowException we)
							{
								System.out.println("Erro na Classe FCIRegistraSPCSERASA metodo start catch (WorkflowException we). Empresa : " + codEmp + ", filial : " + codFil + ", nº Titulo : " + numTit + ", Tipo Titulo : " + codTpt + ", CadSPC : " + cadSPC);
								throw we;
							}
							catch (Exception e)
							{
								e.printStackTrace();
								System.out.println("Erro na Classe FCIRegistraSPCSERASA metodo start catch (Exception e). Empresa : " + codEmp + ", filial : " + codFil + ", nº Titulo : " + numTit + ", Tipo Titulo : " + codTpt + ", CadSPC : " + cadSPC);
								throw new WorkflowException(e.getMessage());
							}

						}

					}
				}
			}
		}
		catch (Exception e)
		{
			System.out.println("Erro na Classe FCIRegistraSPCSERASA metodo start no (Exception e) principal.");
		}

	}

	public void insereRegistroOrgaoCreditoSapiens(Long codEmp, Long codFil, String numTit, String codTpt, GregorianCalendar data, String texto, Long usuario, Long ocorrencia) throws Exception
	{
		try
		{
			System.out.println("Entrou insereRegistroOrgaoCreditoSapiens. Empresa : " + codEmp + ", filial : " + codFil + ", nº Titulo : " + numTit + ", Tipo Titulo : " + codTpt);
			this.entityManager = PersistEngine.getEntityManager("SAPIENS");
			this.transaction = this.entityManager.getTransaction();
			Integer result = 0;

			if (!this.transaction.isActive())
			{
				this.transaction.begin();
			}

			StringBuilder insReg = new StringBuilder();

			insReg.append(" INSERT INTO USU_T301OTI (usu_codemp, usu_codfil, usu_numtit, usu_codtpt, usu_codocr, usu_datins, usu_datrem, usu_obsocr, usu_usuins, usu_usurem) ");
			insReg.append(" VALUES (:codemp, :codfil, :numtit, :codtpt, :codocr, :datins, :datrem, :obsocr, :usuins, :usurem) ");

			Query queryInsereAfa = this.entityManager.createNativeQuery(insReg.toString());

			queryInsereAfa.setParameter("codemp", codEmp);
			queryInsereAfa.setParameter("codfil", codFil);
			queryInsereAfa.setParameter("numtit", numTit);
			queryInsereAfa.setParameter("codtpt", codTpt);
			queryInsereAfa.setParameter("codocr", ocorrencia);
			queryInsereAfa.setParameter("datins", new Timestamp(data.getTimeInMillis()));
			queryInsereAfa.setParameter("datrem", new Timestamp((new GregorianCalendar(1900, GregorianCalendar.DECEMBER, 31)).getTimeInMillis()));
			queryInsereAfa.setParameter("obsocr", texto);
			queryInsereAfa.setParameter("usuins", usuario);
			
			queryInsereAfa.setParameter("usurem", 0L);

			result = queryInsereAfa.executeUpdate();
			this.transaction.commit();

			log.warn("INSERT USU_T301OTI: " + result);
			System.out.println("saiu insereRegistroOrgaoCreditoSapiens. Empresa : " + codEmp + ", filial : " + codFil + ", nº Titulo : " + numTit + ", Tipo Titulo : " + codTpt);
		}
		catch (WorkflowException e)
		{
			if (this.transaction.isActive())
			{
				this.transaction.rollback();
			}
			System.out.println("Erro na Classe FCIRegistraSPCSERASA metodo insereRegistroOrgaoCreditoSapiens catch (WorkflowException e). Empresa : " + codEmp + ", filial : " + codFil + ", nº Titulo : " + numTit + ", Tipo Titulo : " + codTpt);
			if (e.getErrorList() != null && e.getErrorList().size() > 0)
			{
				QLAdapter.erros.add(e.getErrorList().get(0).toString());
			}
			else
			{
				QLAdapter.erros.add("Erro ao realizar a operação! " + e.getMessage());
			}
			e.printStackTrace();
		}
		catch (Exception ex)
		{
			if (this.transaction.isActive())
			{
				this.transaction.rollback();
			}
			System.out.println("Erro na Classe FCIRegistraSPCSERASA metodo insereRegistroOrgaoCreditoSapiens catch (SQLException e). Empresa : " + codEmp + ", filial : " + codFil + ", nº Titulo : " + numTit + ", Tipo Titulo : " + codTpt);
			ex.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public void insereObservacaoTituloSapiens(Long codEmp, Long codFil, String numTit, String codTpt, GregorianCalendar data, Long hora, String texto, Long usuario, Long ocorrencia) throws Exception
	{
		try
		{
			System.out.println("Entrou insereObservacaoTituloSapiens. Empresa : " + codEmp + ", filial : " + codFil + ", nº Titulo : " + numTit + ", Tipo Titulo : " + codTpt);
			Integer sequencial = 1;

			this.entityManager = PersistEngine.getEntityManager("SAPIENS");
			this.transaction = this.entityManager.getTransaction();
			Integer result = 0;

			if (!this.transaction.isActive())
			{
				this.transaction.begin();
			}

			StringBuilder count = new StringBuilder();
			count.append("SELECT COUNT(*)+1 FROM E301MOR WHERE codemp = :codemp AND codfil = :codfil AND numtit = :numtit AND codtpt = :codtpt ");

			Query queryAdm = this.entityManager.createNativeQuery(count.toString());
			queryAdm.setParameter("codemp", codEmp);
			queryAdm.setParameter("codfil", codFil);
			queryAdm.setParameter("numtit", numTit);
			queryAdm.setParameter("codtpt", codTpt);
			List<Object> resultList = queryAdm.getResultList();

			if (resultList != null && !resultList.isEmpty())
			{
				sequencial = (Integer) resultList.get(0);
			}

			System.out.println("Texto 2: " + texto);

			StringBuilder insinsObs = new StringBuilder();
			insinsObs.append(" INSERT INTO E301MOR (CodEmp, CodFil, NumTit, CodTpt, SeqMov, TipObs, ObsTit, UsuMov, DatMov, HorMov, USU_CodOcr) ");
			insinsObs.append(" VALUES (:CodEmp, :CodFil, :NumTit, :CodTpt, :SeqMov, :TipObs, :ObsTit, :UsuMov, :DatMov, :HorMov, :USU_CodOcr) ");

			Query queryInsereAfa = this.entityManager.createNativeQuery(insinsObs.toString());

			queryInsereAfa.setParameter("CodEmp", codEmp);
			queryInsereAfa.setParameter("CodFil", codFil);
			queryInsereAfa.setParameter("NumTit", numTit);
			queryInsereAfa.setParameter("CodTpt", codTpt);
			queryInsereAfa.setParameter("SeqMov", sequencial);
			queryInsereAfa.setParameter("TipObs", "M");
			queryInsereAfa.setParameter("ObsTit", texto);
			queryInsereAfa.setParameter("UsuMov", usuario);
			queryInsereAfa.setParameter("DatMov", new Timestamp(data.getTimeInMillis()));
			queryInsereAfa.setParameter("HorMov", hora);
			queryInsereAfa.setParameter("USU_CodOcr", ocorrencia);

			result = queryInsereAfa.executeUpdate();
			this.transaction.commit();

			log.warn("INSERT USU_T301OTI: " + result);
			System.out.println("saiu insereObservacaoTituloSapiens. Empresa : " + codEmp + ", filial : " + codFil + ", nº Titulo : " + numTit + ", Tipo Titulo : " + codTpt);
		}
		catch (WorkflowException e)
		{
			if (this.transaction.isActive())
			{
				this.transaction.rollback(); 
			}
			System.out.println("Erro na Classe FCIRegistraSPCSERASA metodo insereObservacaoTituloSapiens catch (WorkflowException e). Empresa : " + codEmp + ", filial : " + codFil + ", nº Titulo : " + numTit + ", Tipo Titulo : " + codTpt);
			if (e.getErrorList() != null && e.getErrorList().size() > 0)
			{
				QLAdapter.erros.add(e.getErrorList().get(0).toString());
			}
			else
			{
				QLAdapter.erros.add("Erro ao realizar a operação! " + e.getMessage());
			}
			e.printStackTrace();
		}
		catch (Exception ex)
		{
			if (this.transaction.isActive())
			{
				this.transaction.rollback();
			}
			System.out.println("Erro na Classe FCIRegistraSPCSERASA metodo insereRegistroOrgaoCreditoSapiens catch (SQLException e). Empresa : " + codEmp + ", filial : " + codFil + ", nº Titulo : " + numTit + ", Tipo Titulo : " + codTpt);
			ex.printStackTrace();
		}

	}

	private Boolean tituloJaRegistrado(Long codEmp, Long codFil, String numTit, String codTpt)
	{
		try
		{
			System.out.println(" Entrou tituloJaRegistrado");
			Boolean tituloRegistrado = false;

			QLGroupFilter filterRegistro = new QLGroupFilter("AND");
			filterRegistro.addFilter(new QLEqualsFilter("usu_codemp", codEmp));
			filterRegistro.addFilter(new QLEqualsFilter("usu_codfil", codFil));
			filterRegistro.addFilter(new QLEqualsFilter("usu_numtit", numTit));
			filterRegistro.addFilter(new QLEqualsFilter("usu_codtpt", codTpt));
			QLGroupFilter filterRegistroSemRemocao = new QLGroupFilter("OR");
			GregorianCalendar gc = new GregorianCalendar(1900, GregorianCalendar.DECEMBER, 31);
			filterRegistroSemRemocao.addFilter(new QLEqualsFilter("usu_datrem", gc));
			filterRegistroSemRemocao.addFilter(new QLFilterIsNull("usu_datrem"));
			filterRegistro.addFilter(filterRegistroSemRemocao);

			List<NeoObject> listaRegistro = (List<NeoObject>) PersistEngine.getObjects(((ExternalEntityInfo) EntityRegister.getInstance().getCache().getByType("SAPIENSUSUTOTI")).getEntityClass(), filterRegistro);
			if (listaRegistro != null && (!listaRegistro.isEmpty()))
			{
				tituloRegistrado = true;
			}
			System.out.println(" saiu tituloJaRegistrado");
			return tituloRegistrado;
		}
		catch (Exception e)
		{
			System.out.println("Erro na Classe FCIRegistraSPCSERASA metodo tituloJaRegistrado. Empresa : " + codEmp + ", filial : " + codFil + ", nº Titulo : " + numTit + ", Tipo Titulo : " + codTpt);
			return null;
		}
	}

	private Long buscaUsuarioSapiensCorrespondente(String userCode)
	{
		System.out.println(" Entrou buscaUsuarioSapiensCorrespondente userCode : " + userCode);
		//Logica para buscar codigo do usuario do sapiens referente ao usuario fusion
		Long sapiensUserCode = 0L;
		NeoObject sapiensUser = null;
		InstantiableEntityInfo ieiEX = AdapterUtils.getInstantiableEntityInfo("SAPIENSUSUARIO");
		String login = userCode;

		QLEqualsFilter loginFilter = new QLEqualsFilter("nomusu", login);
		@SuppressWarnings("unchecked")
		ArrayList<NeoObject> sapiensUsers = (ArrayList<NeoObject>) PersistEngine.getObjects(ieiEX.getEntityClass(), loginFilter);

		if (sapiensUsers != null && sapiensUsers.size() > 0)
		{
			sapiensUser = sapiensUsers.get(0);
		}

		if (sapiensUser != null)
		{
			EntityWrapper sapiensUserEw = new EntityWrapper(sapiensUser);
			sapiensUserCode = (Long) sapiensUserEw.findValue("codusu");
		}

		if (sapiensUserCode == null || sapiensUserCode == 0L)
		{
			log.error("Não foi possivel buscar o codigo de usuario sapiens referente ao usuário logado no Fusion.");
			System.out.println("Não foi possivel buscar o codigo de usuario sapiens referente ao usuário logado no Fusion.");
			throw new WorkflowException("Não foi possivel buscar o codigo de usuario sapiens referente ao usuário logado no Fusion.");
		}
		System.out.println(" saiu buscaUsuarioSapiensCorrespondente userCode : " + userCode);
		return sapiensUserCode;
	}

}
