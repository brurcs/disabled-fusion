package com.neomind.fusion.custom.orsegups.contract;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.Query;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.adapter.CadastraCentroCusto;
import com.neomind.fusion.custom.orsegups.fap.exception.NaoEncontradoException;
import com.neomind.fusion.custom.orsegups.fap.utils.FapUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.dynamic.EntityCloner;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class CadastraCentroCustoClienteContrato implements AdapterInterface{
	
	
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		Long key = new GregorianCalendar().getTimeInMillis();
		Long tipoAcao = NeoUtils.safeLong(NeoUtils.safeOutputString(processEntity.findValue("movimentoContratoNovo.codTipo")));
		Long tipoAlteracao = NeoUtils.safeLong(NeoUtils.safeOutputString(processEntity.findValue("movimentoAlteracaoContrato.codTipo")));
		String modeloContrato = processEntity.findGenericValue("modelo.codigo");
		int qtdPostos = processEntity.findField("postosContrato").getValues().size();
		boolean adereProtecaoG = NeoUtils.safeBoolean(processEntity.findValue("adereProtecaoGarantida"));
		ContratoLogUtils.logInfo("[Tarefa:"+origin.getCode()+"] - ["+key+"] - Iniciando Cadastro de Centro de custo" );
		try{
			if((tipoAcao <= 4 || tipoAcao == 6) && qtdPostos == 0)
			{
				/*
				 * criar automaticamente os postos previstos se não houverem postos cadastrados
				 * isso serve para tratar o caso do usuário recusar a tarefa e passar por este adapter
				 * novamente
				 */
				
				///////
				
				long postosPrevistos = 1L; 
				String placas;
				String[] placaSplit = null;
				
				NeoObject noEnvioEquipInst = null;
				EntityWrapper wEnvioEquipInst = null;

				if (modeloContrato.equals("05"))
				{
					postosPrevistos = processEntity.findGenericValue("dadosGeraisContrato.numVeiculos");
					placas = processEntity.findGenericValue("placaVeiculos");
					placaSplit = placas.split(",");
					
					noEnvioEquipInst = AdapterUtils.createNewEntityInstance("FGCEnvioEquipInstalacao");
					wEnvioEquipInst = new EntityWrapper(noEnvioEquipInst);
				}
				else
				{
					if (!NeoUtils.safeOutputString(processEntity.findValue("dadosGeraisContrato.numPostosPrevistos")).isEmpty())
						postosPrevistos = NeoUtils.safeLong(NeoUtils.safeOutputString(processEntity.findValue("dadosGeraisContrato.numPostosPrevistos")));
				}				
				
				for(int i=0; i<postosPrevistos; i++)
				{
					NeoObject tipoPostoNormal = ContratoUtils.getFirstNeoObjectFromList( PersistEngine.getObjects(AdapterUtils.getEntityClass("FGCTipoPosto"), new QLEqualsFilter("codigo", 1L))); // normal
					NeoObject posto = null;
					if(modeloContrato.equals("05"))
					{
						String placa = placaSplit[i];
						posto = criarPostoRastreamento(processEntity, tipoPostoNormal, placa);
						PersistEngine.persist(posto);
						wEnvioEquipInst.findField("postos").addValue(posto);
					}
					else 
					{
						posto = criarPosto(processEntity, tipoPostoNormal );
						PersistEngine.persist(posto);
					}
					
					
					
					processEntity.findField("postosContrato").addValue(posto);
					if (adereProtecaoG){
						ContratoLogUtils.logInfo("Inserindo posto de proteção Garantida");
						
						NeoObject tipoPostoProtecaoG = ContratoUtils.getFirstNeoObjectFromList( PersistEngine.getObjects(AdapterUtils.getEntityClass("FGCTipoPosto"), new QLEqualsFilter("codigo", 2L))); // normal
						
						if(modeloContrato.equals("05"))
						{
							String placa = placaSplit[i];
							posto = criarPostoRastreamento(processEntity, tipoPostoProtecaoG, placa);
							PersistEngine.persist(posto);
							wEnvioEquipInst.findField("postos").addValue(posto);
						} else 
						{
							posto = criarPosto(processEntity, tipoPostoProtecaoG );
						}
						
						NeoObject servico = ContratoUtils.getFirstNeoObjectFromList( PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSESER"), new QLEqualsFilter("codser","9002037")));
						
						EntityWrapper wPosto = new EntityWrapper(posto);						
						wPosto.setValue("complementoServico", servico);
						
						PersistEngine.persist(posto);
						processEntity.findField("postosContrato").addValue(posto);
					}
				}
				
				Boolean inserirRFID = processEntity.findGenericValue("dadosGeraisContrato.adereCartaoRFID");
				if (inserirRFID != null && inserirRFID)
				{
					NeoObject tipoPostoRFID = ContratoUtils.getFirstNeoObjectFromList(PersistEngine.getObjects(AdapterUtils.getEntityClass("FGCTipoPosto"), new QLEqualsFilter("codigo", 3L))); // RFID
					NeoObject posto = criarPostoRastreamento(processEntity, tipoPostoRFID, "");
					EntityWrapper wPosto = new EntityWrapper(posto);
					NeoObject servico = ContratoUtils.getFirstNeoObjectFromList( PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSESER"), new QLEqualsFilter("codser","9002051")));
					wPosto.setValue("complementoServico", servico);
					PersistEngine.persist(posto);
					processEntity.findField("postosContrato").addValue(posto);
				}
				
				processEntity.setValue("envioEquipInstalacao", noEnvioEquipInst);
				
				String obsComercial = "VIGENCIA " + processEntity.findGenericValue("dadosGeraisContrato.peridoMaximoVigencia2.meses") + " MESES";
				obsComercial += " <br>C001 " + activity.getCode();
				obsComercial += " <br>EQUIPAMENTOS LOCADOS";
				processEntity.findField("dadosGeraisContrato.observacaoComerciais").setValue(obsComercial);
			}
			
			String codEmp = NeoUtils.safeOutputString(processEntity.findValue("empresa.codemp"));
			String codFil = NeoUtils.safeOutputString(processEntity.findValue("empresa.codfil"));
			String codCli = NeoUtils.safeOutputString(processEntity.findValue("buscaNomeCliente.codcli"));
			String numContrato = "";
			if((tipoAcao > 4 && tipoAcao != 6) && tipoAlteracao != 1)
			{
				numContrato = NeoUtils.safeOutputString(processEntity.findValue("numContrato.usu_numctr"));
				/*if (tipoAlteracao == 2){ // alteração de razão social nos centros de custos
				CadastraCentroCusto.updateRazaoSocial(processEntity);
			}*/
			}
			
			String classificacaoQuartoNivel = CadastraCentroCusto.getClassificacaoCentroCusto(codEmp, codCli, 4, numContrato);
			ContratoLogUtils.logInfo("[Tarefa:"+origin.getCode()+"] - ["+key+"] - Classificação de Centro de Custos existente no sapiens:" + classificacaoQuartoNivel );
			if (classificacaoQuartoNivel == null || "".equals(classificacaoQuartoNivel) ){ // busca nas tarefas do fusion um arvore para reutilizar
				classificacaoQuartoNivel = CadastraCentroCusto.getClassificacaoCC4NivelTarefas(codEmp, codCli);
				ContratoLogUtils.logInfo("[Tarefa:"+origin.getCode()+"] - ["+key+"] - Classificação Centro de custo de 4 nivel encontrado em outra tarefa:" + classificacaoQuartoNivel );
			}
			
			String centroCustoQuartoNivel = CadastraCentroCusto.getCentroCusto(classificacaoQuartoNivel, 4, codEmp);
			ContratoLogUtils.logInfo("[Tarefa:"+origin.getCode()+"] - ["+key+"] - Centro de custo de 4 nivel:" + centroCustoQuartoNivel );
			
			//CadastraCentroCusto cadastraContrato = new CadastraCentroCusto();
			
			/*
			 * se for cliente novo vai entrar aqui, mas caso de algum problema na frente
			 * o centro de custo já vai existir, logo mesmo sendo cliente novo, o centro de
			 * custo ja existe, n precisa passar aqui
			 * 
			 * Se for uma alteração de CNPJ do cliente, toda arvore de centro de custo deve ser refeita
			 */
			
			ContratoIntegracaoSapiens integrator = ContratoIntegracaoSapiens.getInstance();
			if(((tipoAcao == 3 || tipoAcao == 4 || tipoAcao == 6) && centroCustoQuartoNivel.equals(""))
					|| (tipoAcao == 5 && tipoAlteracao == 1)
					|| (tipoAcao == 1 && centroCustoQuartoNivel.equals("")))
			{
				//cadastraContrato.cadastraClienteCentroCusto(processEntity.getObject());
				ContratoLogUtils.logInfo("[Tarefa:"+origin.getCode()+"] - ["+key+"] - Cadastrando Centros de custo de 4,5 e 6 niveis");
				integrator.insert(processEntity.getObject());
				ContratoLogUtils.logInfo("[Tarefa:"+origin.getCode()+"] - ["+key+"] - Cadastrastro dos Centros de custo de 4,5 e 6 niveis - OK");
			}else if((tipoAcao == 1 || tipoAcao == 2) && !centroCustoQuartoNivel.equals(""))
			{
				//criar um novo código agrupador e colocar a arvore abaixo dele
				Long numeroContrato = NeoUtils.safeLong(NeoUtils.safeOutputString(processEntity.findValue("dadosGeraisContrato.numeroContratoSapiens")));
				String codigoAgr = "AGR" + numeroContrato;
				
				ContratoLogUtils.logInfo("[Tarefa:"+origin.getCode()+"] - ["+key+"] - Cadastrando Centro de custo de 5 e 6 niveis");
				integrator.insert(processEntity.getObject(), centroCustoQuartoNivel, NeoUtils.safeOutputString(numeroContrato), codEmp, codFil, codCli, codigoAgr, false);
				ContratoLogUtils.logInfo("[Tarefa:"+origin.getCode()+"] - ["+key+"] - Cadastrando Centro de custo de 5 e 6 niveis - OK");
			}
			
		}catch(Exception e){
			ContratoLogUtils.logInfo("[Tarefa:"+origin.getCode()+"] - ["+key+"] - Erro ao cadastrar Centro de custo do cliente");
			e.printStackTrace();
			throw new WorkflowException("["+key+"] - Erro ao cadastrar centro de custo do contrato. Erro:" + e);
		}
	}
	
	public static boolean insereRegistroAgrupador(String codemp, String codfil, String codcli, String codagr)
	{
		//UPDATE E044CCU
		String update = "INSERT INTO USU_T160AGR (USU_CTRAGR, USU_CODEMP, USU_CODFIL, USU_CODCLI) VALUES ('" + codagr + "'," + codemp + "," + codfil + "," + codcli + ")";
		StringBuilder sql = new StringBuilder();
		sql.append(update);
		Query queryExecute = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		try
		{
			queryExecute.executeUpdate();
			return true;
		}catch(Exception e)
		{
			System.out.println("[FLUXO CONTRATOS]- Agrupador já existente.");
			e.printStackTrace();
			//se ja existe, n precisa tratar
			//throw new WorkflowException("Erro: já existe agrupador " + codagr );
			return false;
		}
	}
	
	/**
	 * SAPIENSE008CEP
	 * @param uf
	 * @param cidade
	 * @return NeoObject SAPIENSE008CEP
	 */
	private static NeoObject retornaCidadeSapiens8Cep(String uf, String cidade, Long cep)
	{
		QLGroupFilter gp = new QLGroupFilter("AND");
		gp.addFilter(new QLEqualsFilter("sigufs", uf));
		gp.addFilter(new QLEqualsFilter("nomcid", cidade));
		Collection<NeoObject> objCidade = (Collection<NeoObject>)PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE008CEP"), gp);
		NeoObject cidadeAlvo = null;
		for(NeoObject obj : objCidade)
		{
			EntityWrapper w = new EntityWrapper(obj);
			String cepini = NeoUtils.safeOutputString(w.findValue("cepini"));
			String cepfim = NeoUtils.safeOutputString(w.findValue("cepfim"));

			if((cep >= NeoUtils.safeLong(cepini)) &&  cep <= NeoUtils.safeLong(cepfim))
			{
				cidadeAlvo = obj;
			}
		}
		return cidadeAlvo;
	}
	
	public NeoObject criarPosto(EntityWrapper processEntity, NeoObject tipoPosto){
		
		Long codEmpresa = (Long) processEntity.findField("empresa.codemp").getValue();
		
		EntityWrapper wTipoPosto = new EntityWrapper(tipoPosto);
		
		NeoObject posto = AdapterUtils.createNewEntityInstance("FGCCadastroPostos");
		EntityWrapper wPosto = new EntityWrapper(posto);
		
		//ContratoLogUtils.logInfo("postoLiberadoFaturamento-> sim, para movimento do tipo :" +tipoAcao);
		wPosto.setValue("postoLiberadoFaturamento", true ); 
		
		if(processEntity.findValue("dadosGeraisContrato.regional") != null)
		{
			wPosto.setValue("regionalPosto", processEntity.findValue("dadosGeraisContrato.regional"));
		}
		if(processEntity.findValue("dadosGeraisContrato.representante") != null)
		{
			wPosto.setValue("representante", processEntity.findValue("dadosGeraisContrato.representante"));
		}
		
		if ( NeoUtils.safeBoolean(processEntity.findValue("adereProtecaoGarantida")) && NeoUtils.safeOutputString(wTipoPosto.findValue("codigo")).equals("2") ){
			BigDecimal valorProtecaoG = processEntity.findValue("valorProtecaoG") != null ? (BigDecimal) processEntity.findValue("valorProtecaoG")  : new BigDecimal("0");
			if (valorProtecaoG.compareTo(new BigDecimal("29")) == 0 || valorProtecaoG.compareTo(new BigDecimal("39")) == 0 ){
				NeoObject formacaoPreco = (NeoObject) wPosto.findValue("formacaoPreco");
				if (formacaoPreco == null){
					formacaoPreco = AdapterUtils.createNewEntityInstance("FGCFormacaoPreco");
				}
				EntityWrapper wFormacaoPreco = new EntityWrapper(formacaoPreco);
				wFormacaoPreco.setValue("valorMontanteB", valorProtecaoG);
				wPosto.setValue("precoUnitarioPosto", valorProtecaoG);
				wPosto.setValue("valorTotalPosto", valorProtecaoG);
				wPosto.setValue("formacaoPreco", formacaoPreco);
				
				QLGroupFilter qgf = new QLGroupFilter("AND");
				qgf.addFilter(new QLEqualsFilter("codser","9002037"));
				qgf.addFilter(new QLEqualsFilter("codemp",codEmpresa));
				NeoObject servico = ContratoUtils.getFirstNeoObjectFromList( PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSESER"), qgf ) );
				wPosto.setValue("complementoServico", servico);
				
				//wPosto.setValue("aberturaOSHabilitacao", false);
				//wPosto.setValue("aberturaOSInstalacao", false);
				//wPosto.setValue("aberturaOSReinstalacao", false);
				
				PersistEngine.persist(formacaoPreco);
				PersistEngine.persist(posto);
			}else{
				ContratoLogUtils.logInfo("Valor de Proteção garantida fora da faixa. Provalemente o valor do seguro foi alterado e essa validação nao foi atualizada");
			}
		}
		
		// setar o email que vai pro sigma com o intnet do cliente. Definido em reunão c/ Giliardi
		EntityWrapper wPostoFusion = new EntityWrapper(posto);
		NeoObject oVigilanciaEletronica = AdapterUtils.createNewEntityInstance("FGCVigilanciaEletronica");
		EntityWrapper wVigilanciaEletronica = new EntityWrapper(oVigilanciaEletronica);
		
		NeoObject oDadosCliente = (NeoObject) processEntity.findValue("novoCliente");
		if (oDadosCliente != null){
			EntityWrapper wDadosCliente = new EntityWrapper(oDadosCliente);
			if (wDadosCliente.findValue("email") != null){
				wVigilanciaEletronica.setValue("emailReferencia", (String) wDadosCliente.findValue("email"));
			}
		}
		PersistEngine.persist(oVigilanciaEletronica);
		wPosto.setValue("vigilanciaEletronica", oVigilanciaEletronica);
		
		//situacaoPosto
		NeoObject situacaoAtiva = ContratoUtils.getFirstNeoObjectFromList( PersistEngine.getObjects(AdapterUtils.getEntityClass("FGCSituacaoPosto"), new QLEqualsFilter("situacao", "A")));
		wPosto.setValue("situacaoPosto", situacaoAtiva);
		
		wPosto.setValue("tipoPosto", tipoPosto);
		return posto;
	}
	
	public NeoObject criarPostoRastreamento(EntityWrapper processEntity, NeoObject tipoPosto, String placa){
		Long codEmpresa = (Long) processEntity.findField("empresa.codemp").getValue();
		
		EntityWrapper wTipoPosto = new EntityWrapper(tipoPosto);
		String codTipoPosto = NeoUtils.safeOutputString(wTipoPosto.findValue("codigo"));
		
		NeoObject noPosto = AdapterUtils.createNewEntityInstance("FGCCadastroPostos");
		EntityWrapper wPosto = new EntityWrapper(noPosto);
		
		NeoObject noEquipItens = AdapterUtils.createNewEntityInstance("FGCEquipamentosItens");
		EntityWrapper wEquipItens = new EntityWrapper(noEquipItens);
		
		NeoObject noInstalador = processEntity.findGenericValue("dadosGeraisContrato.instalador");
		NeoObject noTipoInstalacao = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FGCTipoInstalacao"), new QLEqualsFilter("codigo", "N"));
		NeoObject noEquipamento = processEntity.findGenericValue("dadosGeraisContrato.equipamento");
		NeoObject noServico = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSESER"), new QLEqualsFilter("codser", "9002046"));
		NeoObject noRegional = processEntity.findGenericValue("dadosGeraisContrato.regional");
		NeoObject noTransacao = processEntity.findGenericValue("dadosGeraisContrato.transacao");
		
		NeoObject noCombo = processEntity.findGenericValue("dadosGeraisContrato.combo");	
		if (noCombo != null && !codTipoPosto.equals("3"))
		{
			NeoObject noListCombo = AdapterUtils.createNewEntityInstance("FGCListaCombos");
			EntityWrapper wListCombo = new EntityWrapper(noListCombo);	
			
			wListCombo.setValue("kit", noCombo);
			wListCombo.setValue("quantidade", 1l);
			wListCombo.setValue("instaladores", noInstalador);
			wListCombo.setValue("tipoInstalacao", noTipoInstalacao);
			wEquipItens.findField("kitsPosto").addValue(noListCombo);
			
			EntityWrapper wCombo = new EntityWrapper(noCombo);
			Long usu_codkit = wCombo.findGenericValue("usu_codkit");
			
			List<NeoObject> listKitProdutos = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUKPR"), new QLEqualsFilter("usu_codkit", usu_codkit));
			for (NeoObject kitProdutos : listKitProdutos)
			{
				EntityWrapper wKitProdutos = new EntityWrapper(kitProdutos);
				String usu_codpro = wKitProdutos.findGenericValue("usu_codpro");
				
				NeoObject noProduto = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSEPRO"), new QLEqualsFilter("codpro", usu_codpro));
				if (noProduto != null)
				{
					NeoObject noListEquipsPosto = AdapterUtils.createNewEntityInstance("FGCListaEquipamentosPosto");
					EntityWrapper wEquipsPosto = new EntityWrapper(noListEquipsPosto);
					
					wEquipsPosto.setValue("quantidade", 1l);
					wEquipsPosto.setValue("produto", noProduto);
					wEquipsPosto.setValue("instaladores", noInstalador);
					wEquipsPosto.setValue("tipoInstalacao", noTipoInstalacao);
					wEquipItens.findField("equipamentosPosto").addValue(noListEquipsPosto);
				}
			}
		}		
		
		if (!codTipoPosto.equals("3"))
		{
			NeoObject noListChecklist = AdapterUtils.createNewEntityInstance("FGCListaChecklist");
			EntityWrapper wChecklist = new EntityWrapper(noListChecklist);
			
			NeoObject noItemChecklist = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSUSUITM"), new QLEqualsFilter("usu_coditm", 28l));
			
			wChecklist.setValue("itemChecklist", noItemChecklist);
			wChecklist.setValue("descricao", "CHIP");
			wChecklist.setValue("quantidade", 1l);
			//wChecklist.setValue("prazoPadrao", value);
			//wChecklist.setValue("prazo", value);
			wEquipItens.findField("itensChecklist").addValue(noListChecklist);
		}
		
		NeoObject noMonitoramento = AdapterUtils.createNewEntityInstance("FGCVigilanciaEletronica");
		EntityWrapper wMonitoramento = new EntityWrapper(noMonitoramento);
		
		BigDecimal valorInstalacao = processEntity.findGenericValue("dadosGeraisContrato.valorInstalacao");
		NeoObject noParcela = processEntity.findGenericValue("dadosGeraisContrato.parcelas");
		String responsavel = processEntity.findGenericValue("dadosGeraisContrato.responsavelContrato");
		String cpfResponsavel = processEntity.findGenericValue("dadosGeraisContrato.cpfResponsavelContrato");
		String telefone1 = processEntity.findGenericValue("novoCliente.telefone1");
		String telefone2 = processEntity.findGenericValue("novoCliente.telefone1");
		
		NeoObject noContato = AdapterUtils.createNewEntityInstance("FGCContato");
		EntityWrapper wContato = new EntityWrapper(noContato);
		wContato.setValue("nomecontato", responsavel);
		wContato.setValue("telefone1", telefone1);
		
		NeoObject noContato1 = AdapterUtils.createNewEntityInstance("FGCContato");
		EntityWrapper wContato1 = new EntityWrapper(noContato1);
		wContato1.setValue("nomecontato", responsavel);
		wContato1.setValue("telefone1", telefone2);
		
		BigDecimal valorPosto = processEntity.findGenericValue("dadosGeraisContrato.valorPosto");
		Boolean cartaoRFID = processEntity.findGenericValue("dadosGeraisContrato.adereCartaoRFID");
		Long qtdEquipamento = 1l;
		
		if (cartaoRFID && codTipoPosto.equals("3"))
		{
			valorInstalacao = new BigDecimal(0);
			valorPosto = processEntity.findGenericValue("dadosGeraisContrato.valorCartaoRFID");
			qtdEquipamento = processEntity.findGenericValue("dadosGeraisContrato.qtCartaoRFID");
			noEquipamento = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSEPRO"), new QLEqualsFilter("codpro", "205825"));
		}
		
		NeoObject noListEquipsPosto = AdapterUtils.createNewEntityInstance("FGCListaEquipamentosPosto");
		EntityWrapper wEquipsPosto = new EntityWrapper(noListEquipsPosto);
		
		wEquipsPosto.setValue("quantidade", qtdEquipamento);
		wEquipsPosto.setValue("produto", noEquipamento);
		wEquipsPosto.setValue("instaladores", noInstalador);
		wEquipsPosto.setValue("tipoInstalacao", noTipoInstalacao);
		wEquipItens.findField("equipamentosPosto").addValue(noListEquipsPosto);
		
		// setar o email que vai pro sigma com o intnet do cliente. Definido em reunão c/ Giliardi
		wMonitoramento.setValue("emailReferencia", processEntity.findGenericValue("novoCliente.email"));
		wMonitoramento.setValue("valorInstalacao", valorInstalacao);
		wMonitoramento.setValue("parcelas", noParcela);
		wMonitoramento.setValue("responsavel", responsavel);
		wMonitoramento.setValue("cpfResponsavel", cpfResponsavel);
		wMonitoramento.findField("listaContatosPosto").addValue(noContato);
		wMonitoramento.findField("listaContatosPosto").addValue(noContato1);
		
		NeoObject noFormacaoPreco = AdapterUtils.createNewEntityInstance("FGCFormacaoPreco");
		EntityWrapper wFormacaoPreco = new EntityWrapper(noFormacaoPreco);
		wFormacaoPreco.setValue("valorMontanteB", valorPosto);
		
		wPosto.setValue("vigilanciaEletronica", noMonitoramento);
		wPosto.setValue("equipamentosItens", noEquipItens);
		wPosto.setValue("formacaoPreco", noFormacaoPreco);
		wPosto.setValue("complementoServico", noServico);
		wPosto.setValue("regionalPosto", noRegional);
		wPosto.setValue("transacao", noTransacao);
		wPosto.setValue("aberturaOSInstalacao", true);
		
		NeoObject noPeriodoFaturamento = AdapterUtils.createNewEntityInstance("FGCPeriodoFaturamento");
		EntityWrapper wPeriodoFaturamento = new EntityWrapper(noPeriodoFaturamento);
		wPeriodoFaturamento.setValue("de", new GregorianCalendar());
		wPeriodoFaturamento.setValue("obs", placa);
		wPosto.setValue("periodoFaturamento", noPeriodoFaturamento);
		
		EntityWrapper wRegional = new EntityWrapper(noRegional);
		String regional = wRegional.findGenericValue("usu_nomreg");
		//String siglaRegional = "";
		NeoObject noOperadorCM = null;
		try
		{
			noOperadorCM = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SIGMACOLABORADOR"), new QLEqualsFilter("nm_colaborador", FapUtils.getSiglaRegional(regional) + " - TERC - RASTR - UORV"));
		}
		catch (NaoEncontradoException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		wPosto.setValue("buscaOperadorCM", noOperadorCM);
		
		wPosto.setValue("placa", placa.equals("RFID") ? "" : placa.toUpperCase());		
		
		//ContratoLogUtils.logInfo("postoLiberadoFaturamento-> sim, para movimento do tipo :" +tipoAcao);
		wPosto.setValue("postoLiberadoFaturamento", true ); 
		
		if(processEntity.findValue("dadosGeraisContrato") != null)
		{
			wPosto.setValue("regionalPosto", processEntity.findValue("dadosGeraisContrato.regional"));
		}
		if(processEntity.findValue("dadosGeraisContrato") != null)
		{
			wPosto.setValue("representante", processEntity.findValue("dadosGeraisContrato.representante"));
		}
		
		if ( NeoUtils.safeBoolean(processEntity.findValue("adereProtecaoGarantida")) && codTipoPosto.equals("2") ){
			BigDecimal valorProtecaoG = processEntity.findValue("valorProtecaoG") != null ? (BigDecimal) processEntity.findValue("valorProtecaoG")  : new BigDecimal("0");
			if (valorProtecaoG.compareTo(new BigDecimal("29")) == 0 || valorProtecaoG.compareTo(new BigDecimal("39")) == 0 ){
				wPosto.setValue("precoUnitarioPosto", valorProtecaoG);
				wPosto.setValue("valorTotalPosto", valorProtecaoG);
				
				QLGroupFilter qgf = new QLGroupFilter("AND");
				qgf.addFilter(new QLEqualsFilter("codser","9002037"));
				qgf.addFilter(new QLEqualsFilter("codemp",codEmpresa));
				NeoObject servico = ContratoUtils.getFirstNeoObjectFromList( PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSESER"), qgf ) );
				wPosto.setValue("complementoServico", servico);
			}else{
				ContratoLogUtils.logInfo("Valor de Proteção garantida fora da faixa. Provalemente o valor do seguro foi alterado e essa validação nao foi atualizada");
			}
		}
		
		
		//situacaoPosto
		NeoObject situacaoAtiva = ContratoUtils.getFirstNeoObjectFromList( PersistEngine.getObjects(AdapterUtils.getEntityClass("FGCSituacaoPosto"), new QLEqualsFilter("situacao", "A")));
		wPosto.setValue("situacaoPosto", situacaoAtiva);
		
		wPosto.setValue("tipoPosto", tipoPosto);
		
		NeoObject noEndereco = processEntity.findGenericValue("novoCliente.enderecoCliente");
		NeoObject noEnderecoClone = EntityCloner.cloneNeoObject(noEndereco);
		wPosto.setValue("enderecoPosto", noEnderecoClone);
		
		return noPosto;
	}

	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}
}
