package com.neomind.fusion.custom.orsegups;

import java.util.GregorianCalendar;
import java.util.List;

public class FCNCancelarContratoEletrônica
{
	private String code;
	private int countTitulos;
	private int countTitulosSapiensLQ;
	private List<GregorianCalendar> list;

	public String getCode()
	{
		return code;
	}

	public void setCode(String code)
	{
		this.code = code;
	}

	public int getCountTitulos()
	{
		return countTitulos;
	}

	public void setCountTitulos(int countTitulos)
	{
		this.countTitulos = countTitulos;
	}

	public int getCountTitulosSapiensLQ()
	{
		return countTitulosSapiensLQ;
	}

	public void setCountTitulosSapiensLQ(int countTitulosSapiensLQ)
	{
		this.countTitulosSapiensLQ = countTitulosSapiensLQ;
	}

	public List<GregorianCalendar> getList()
	{
		return list;
	}

	public void setList(GregorianCalendar tessss)
	{
		this.list = (List<GregorianCalendar>) tessss;
	}
	
}
