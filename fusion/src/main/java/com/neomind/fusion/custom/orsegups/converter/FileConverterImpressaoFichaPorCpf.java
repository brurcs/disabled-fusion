package com.neomind.fusion.custom.orsegups.converter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.FileConverter;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.portal.PortalUtil;

public class FileConverterImpressaoFichaPorCpf extends FileConverter
{
	private static final Log log = LogFactory.getLog(FileConverterImpressaoFichaPorCpf.class);

	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin)
	{

		// Botão para imprimir a ficha - via fluxo ou consulta

		Long idPai = field.getForm().getObjectId();
		if (field.getFieldCode().equals("impressaoFichaPorCpf"))
		{
			String servlet = PortalUtil.getBaseURL() + "custom/jsp/orsegups/FichaServletImpressaoPorCpf.jsp?idPai=" + idPai+"&cpf=";
			String retorno = "";
			//+"&cpf=document.getElementById(\'var_cpfGerarRelatorio__\').value
			retorno =    "<script>$('label[for*=__impressaoFicha__]').hide();</script>"
				+ " <script> "
				+ "     function abrir(url) {"
				+"          var vCpf = ''; "
				+ "         vCpf = document.getElementById(\'var_cpfGerarRelatorio__\').value; "
				+ "         if (vCpf == '') { "
				+ "            alert('Preencha um CPF para gerar o relatório!');"
				+ "         } else { "
				+ "           window.open(url+vCpf);   "
				+ "         } "
				+ "     }"
				+ "</script>"
				+ "<div align='left'>"
				+ "   <a class='acao' id='imprimirFicha' onClick='abrir(\"" + servlet+ "\");' title='Imprimir Ficha'>" 
				+ "      <img src='imagens/custom/ico_20_dash_imp.gif' alt='Imprimir Ficha' align='absmiddle' />" + "&nbsp;" + "Imprimir Ficha"
				+ "   </a>"
				+ "</div>";
			
			return retorno;

		}

		return super.getHTMLInput(field, origin);
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		// Botão para imprimir a ficha - via fluxo ou consulta

		Long idPai = field.getForm().getObjectId();
		if (field.getFieldCode().equals("impressaoFichaPorCpf"))
		{
			String servlet = PortalUtil.getBaseURL() + "custom/jsp/orsegups/FichaServletImpressao.jsp?idPai=" + idPai;
			String servletFicha = servlet + "&autorizacao=false";
			
			
			return "<script>$('label[for*=__impressaoFicha__]').hide();</script><div align='center'><a class='acao' id='imprimirFicha' onClick='javascript:window.open(\"" + servletFicha + "\",\"Imprimir\",\"status=0, resizable=0, scrollbars=NO, width=100, height=75\");' title='Imprimir Ficha'>" + "<img src='imagens/custom/ico_20_dash_imp.gif' alt='Imprimir Ficha' align='absmiddle' />" + "&nbsp;" + "Imprimir Ficha</a></div>";
			
		}

		return super.getHTMLView(field, origin);
	}

}
