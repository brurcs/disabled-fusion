package com.neomind.fusion.custom.orsegups.presenca.vo;

import java.util.GregorianCalendar;

public class HorarioVO
{
	private GregorianCalendar dataInicial;
	private GregorianCalendar dataFinal;
	private Long tipoHorario;
	
	public GregorianCalendar getDataInicial()
	{
		return dataInicial;
	}
	public void setDataInicial(GregorianCalendar dataInicial)
	{
		this.dataInicial = dataInicial;
	}
	public GregorianCalendar getDataFinal()
	{
		return dataFinal;
	}
	public void setDataFinal(GregorianCalendar dataFinal)
	{
		this.dataFinal = dataFinal;
	}
	
	public Long getTipoHorario() {
	    return tipoHorario;
	}
	
	public void setTipoHorario(Long tipoHorario) {
	    this.tipoHorario = tipoHorario;
	}
	
	@Override
	public String toString()
	{
		return "HorarioVO [dataInicial=" + dataInicial + ", dataFinal=" + dataFinal + "]";
	}
	
}