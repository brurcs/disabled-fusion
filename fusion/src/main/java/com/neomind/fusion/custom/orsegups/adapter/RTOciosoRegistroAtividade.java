package com.neomind.fusion.custom.orsegups.adapter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class RTOciosoRegistroAtividade implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(RTOciosoRegistroAtividade.class);

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		String mensagem = "Por favor, contatar o administrador do sistema!";
		WorkflowException exception = null;
		try
		{
			InstantiableEntityInfo registroAtividade = AdapterUtils.getInstantiableEntityInfo("RTOciosoRegistroAtividade");
			NeoObject registro = registroAtividade.createNewInstance();
			EntityWrapper wRegistro = new EntityWrapper(registro);

			wRegistro.findField("responsavel").setValue(origin.getUser());
			
			wRegistro.findField("dataInicial").setValue(origin.getStartDate());
			wRegistro.findField("dataFinal").setValue(origin.getFinishDate());

			if (origin.getActivityName().equalsIgnoreCase("Justificar RT Ocioso"))
			{
				Boolean aprovado = (Boolean) processEntity.findValue("ocioso");
				if(!aprovado){
					wRegistro.findField("atividadeAnterior").setValue("Constatou ociosidade, enviou para central de monitoramento.");
				}
				else
				{
					wRegistro.findField("atividadeAnterior").setValue("Enviou relato para presidência.");
				}
				
				String txtDescricao = (String) processEntity.findValue("resposta");
				wRegistro.findField("descricao").setValue(txtDescricao);
				processEntity.setValue("resposta", null);
			}
			else if (origin.getActivityName().equalsIgnoreCase("Verificar Ociosidade"))
			{
				
				Boolean aprovado = (Boolean) processEntity.findValue("aprovacaoSolicitante");
				if(!aprovado){
					wRegistro.findField("atividadeAnterior").setValue("Encaminhou para finalização.");
				}
				else
				{
					wRegistro.findField("atividadeAnterior").setValue("Enviou para presidência.");
				}
				
				String txtDescricao = (String) processEntity.findValue("resposta");
				wRegistro.findField("descricao").setValue(txtDescricao);
				processEntity.setValue("resposta", null);
			}
			else if (origin.getActivityName().equalsIgnoreCase("Aprovar Presidência"))
			{
				Boolean aprovado = (Boolean) processEntity.findValue("aprovacaoSolicitante");
				if(!aprovado){
					wRegistro.findField("atividadeAnterior").setValue("Enviou para solicitante argumentar sobre o fato.");
				}
				else
				{
					wRegistro.findField("atividadeAnterior").setValue("Encaminhou para finalização.");
				}
				
				String txtDescricao = (String) processEntity.findValue("resposta");
				wRegistro.findField("descricao").setValue(txtDescricao);
				processEntity.setValue("resposta", null);
			}

			PersistEngine.persist(registro);
			processEntity.findField("registroAtividades").addValue(registro);
		}
		catch(Exception e)
		{
			log.error(mensagem);
			e.printStackTrace();
			throw new WorkflowException(mensagem);
		}
		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}

}
