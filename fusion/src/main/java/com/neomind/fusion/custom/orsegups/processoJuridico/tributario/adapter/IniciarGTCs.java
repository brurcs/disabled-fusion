package com.neomind.fusion.custom.orsegups.processoJuridico.tributario.adapter;

import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.fusion.workflow.model.ProcessModel;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.IniciarGTCs

public class IniciarGTCs implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
	    try{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		List<NeoObject> listaGTCsLancar = (List<NeoObject>) processEntity.getValue("listaGTCLancar");
		List<NeoObject> listaGTCComprovantePgto = (List<NeoObject>) processEntity.getValue("ListGTCComprovante");
		NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("name", origin.returnResponsible()));
		if (listaGTCsLancar != null && listaGTCsLancar.size() > 0)
		{
			String msg = "<br>";
			for (NeoObject gtcLancar : listaGTCsLancar)
			{
				EntityWrapper wGtc = new EntityWrapper(gtcLancar);
				if (wGtc.getValue("titulo") == null || (String.valueOf(wGtc.getValue("titulo"))).equals(""))
				{
					msg = msg + " O campo <b>Titulo</b> deve ser preenchido!<br>";
				}

				if (wGtc.getValue("tipoTitulo") == null || (String.valueOf(wGtc.getValue("tipoTitulo"))).equals(""))
				{
					msg = msg + " O campo <b>Tipo do titulo</b> deve ser preenchido!<br>";
				}

				if (wGtc.getValue("fornecedor") == null || (String.valueOf(wGtc.getValue("fornecedor"))).equals(""))
				{
					msg = msg + " O campo <b>Fornecedor</b> deve ser preenchido!<br>";
				}

				if (wGtc.getValue("transacao") == null || (String.valueOf(wGtc.getValue("transacao"))).equals(""))
				{
					msg = msg + " O campo <b>Transação</b> deve ser preenchido!<br>";
				}

				if (wGtc.getValue("dataEntrada") == null || (String.valueOf(wGtc.getValue("dataEntrada"))).equals(""))
				{
					msg = msg + " O campo <b>Data de Entrada</b> deve ser preenchido!<br>";
				}

				if (wGtc.getValue("dataEmissao") == null || (String.valueOf(wGtc.getValue("dataEmissao"))).equals(""))
				{
					msg = msg + " O campo <b>Data de Emissão</b> deve ser preenchido!<br>";
				}

				if (wGtc.getValue("vencimentoOriginal") == null || (String.valueOf(wGtc.getValue("vencimentoOriginal"))).equals(""))
				{
					msg = msg + " O campo <b>Vencimento do Título</b> deve ser preenchido!<br>";
				}

				if (wGtc.getValue("observacao") == null || (String.valueOf(wGtc.getValue("observacao"))).equals(""))
				{
					msg = msg + " O campo <b>Observação</b> deve ser preenchido!<br>";
				}

				if (wGtc.getValue("listaLancamento") == null || (String.valueOf(wGtc.getValue("listaLancamento"))).equals(""))
				{
					msg = msg + " O campo <b>Forma de Lançamento do Rateio</b> deve ser preenchido!<br>";
				}

				if (!msg.equals("<br>"))
				{
					//throw new WorkflowException(msg);
				}

			}
			for (NeoObject gtcLancar : listaGTCsLancar)
			{
				EntityWrapper wGtc = new EntityWrapper(gtcLancar);
				Long neoIdGtcAnt = Long.parseLong(String.valueOf(wGtc.getValue("neoId")));

				InstantiableEntityInfo insGTCComprovante = AdapterUtils.getInstantiableEntityInfo("GTCGestaoTitulosControladoria");
				NeoObject objGtcComprovante = insGTCComprovante.createNewInstance();
				EntityWrapper woGtCComprovante = new EntityWrapper(objGtcComprovante);

				woGtCComprovante.setValue("contaFinanceira", wGtc.getValue("contaFinanceira"));

				if (wGtc.getValue("usuarioResponsavel") != null && !String.valueOf(wGtc.getValue("usuarioResponsavel")).equals(""))
				{
					woGtCComprovante.setValue("usuarioResponsavel", wGtc.getValue("usuarioResponsavel"));
				}
				else
				{
					woGtCComprovante.setValue("usuarioResponsavel", solicitante);
				}

				woGtCComprovante.setValue("listaLancamento", wGtc.getValue("listaLancamento"));
				woGtCComprovante.setValue("contaFinanceira", wGtc.getValue("contaFinanceira"));
				woGtCComprovante.setValue("contaContabil", wGtc.getValue("contaContabil"));
				woGtCComprovante.setValue("centroCusto", wGtc.getValue("centroCusto"));
				woGtCComprovante.setValue("empresa", wGtc.getValue("empresa"));
				woGtCComprovante.setValue("codigoFilial", wGtc.getValue("codigoFilial"));
				woGtCComprovante.setValue("titulo", wGtc.getValue("titulo"));
				woGtCComprovante.setValue("tipoTitulo", wGtc.getValue("tipoTitulo"));
				woGtCComprovante.setValue("fornecedor", wGtc.getValue("fornecedor"));
				woGtCComprovante.setValue("observacao", wGtc.getValue("observacao"));
				woGtCComprovante.setValue("valorOriginal", wGtc.getValue("valorOriginal"));
				woGtCComprovante.setValue("dataEntrada", wGtc.getValue("dataEntrada"));
				woGtCComprovante.setValue("dataEmissao", wGtc.getValue("dataEmissao"));
				woGtCComprovante.setValue("transacao", wGtc.getValue("transacao"));
				woGtCComprovante.setValue("vencimentoOriginal", wGtc.getValue("vencimentoOriginal"));
				woGtCComprovante.setValue("formaPagamento", wGtc.getValue("formaPagamento"));
				woGtCComprovante.setValue("j002Anexo", wGtc.getValue("j002Anexo"));
				woGtCComprovante.setValue("j002ClaPgtos", wGtc.getValue("j002ClaPgtos"));
				woGtCComprovante.setValue("isDefinitivo", wGtc.getValue("isDefinitivo"));
				woGtCComprovante.setValue("codigoAgencia", wGtc.getValue("codigoAgencia"));
				woGtCComprovante.setValue("codigoBanco", wGtc.getValue("codigoBanco"));
				woGtCComprovante.setValue("contaBanco", wGtc.getValue("contaBanco"));

				PersistEngine.persist(objGtcComprovante);

				QLEqualsFilter equal = new QLEqualsFilter("Name", "GTC - Gestão de Títulos da Controladoria");
				ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);
				woGtCComprovante.setValue("isFluxoJ002", true);

				final WFProcess processo = processModel.startProcess(objGtcComprovante, false, null, null, null, null, solicitante);
				processo.setRequester(solicitante);
				processo.setSaved(true);
				PersistEngine.persist(processo);

				try
				{
					new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividade(solicitante, true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}

				EntityWrapper wGtcNova = new EntityWrapper(objGtcComprovante);
				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				QLFilter qlNeoId = new QLEqualsFilter("neoIdGtc", neoIdGtcAnt);
				groupFilter.addFilter(qlNeoId);

				List<NeoObject> objsHistorico = PersistEngine.getObjects(AdapterUtils.getEntityClass("j002HistoricoGTCs"), groupFilter);
				if (objsHistorico != null && objsHistorico.size() > 0)
				{
					EntityWrapper wRegistroHistorico = new EntityWrapper(objsHistorico.get(0));

					wRegistroHistorico.setValue("neoIdGtc", wGtcNova.getValue("neoId"));
					wRegistroHistorico.setValue("StatusGTC", "Aguardando Comprovante de Pagamento!");
					PersistEngine.persist(objsHistorico.get(0));
				}
				listaGTCComprovantePgto.add(objGtcComprovante);
			}

			if (listaGTCComprovantePgto == null || listaGTCComprovantePgto.size() == 0)
			{
				processEntity.setValue("possuiGTC", false);
			}
			else
			{
				processEntity.setValue("possuiGTC", true);
			}
			listaGTCsLancar.clear();
			processEntity.setValue("listaGTCLancar", listaGTCsLancar);
			processEntity.setValue("ListGTCComprovante", listaGTCComprovantePgto);
		}
		else
		{
			processEntity.setValue("possuiGTC", false);
		}
	    }catch(Exception e){
		System.out.println("Erro na classe IniciarGTCs do fluxo J002.");
		e.printStackTrace();
		throw new WorkflowException(e.getMessage());
	    }
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
