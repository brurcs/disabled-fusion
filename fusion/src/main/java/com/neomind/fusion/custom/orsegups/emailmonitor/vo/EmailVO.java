package com.neomind.fusion.custom.orsegups.emailmonitor.vo;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.security.NeoUser;

public class EmailVO extends NeoObject
{

	private NeoUser remetente;

	private List<NeoUser> destinatarios;

	private String assunto;

	private String textoEmail;

	private List<File> anexos;

	public NeoUser getRemetente()
	{
		return remetente;
	}

	public void setRemetente(NeoUser remetente)
	{
		this.remetente = remetente;
	}

	public List<NeoUser> getDestinatarios()
	{
		if (destinatarios == null)
		{
			this.destinatarios = new ArrayList<NeoUser>();
		}
		return destinatarios;
	}

	public void setDestinatarios(List<NeoUser> destinatarios)
	{
		this.destinatarios = destinatarios;
	}

	public String getAssunto()
	{
		return assunto;
	}

	public void setAssunto(String assunto)
	{
		this.assunto = assunto;
	}

	public String getTextoEmail()
	{
		return textoEmail;
	}

	public void setTextoEmail(String textoEmail)
	{
		this.textoEmail = textoEmail;
	}

	public List<File> getAnexos()
	{
		return anexos;
	}

	public void setAnexos(List<File> anexos)
	{
		this.anexos = anexos;
	}
}
