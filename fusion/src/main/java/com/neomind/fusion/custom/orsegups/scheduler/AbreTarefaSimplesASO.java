package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class AbreTarefaSimplesASO implements CustomJobAdapter
{
	@Override
	public void execute(CustomJobContext arg0)
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		
		Connection conn = PersistEngine.getConnection("VETORH");
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.AbreTarefaSimplesASO");
		log.warn("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Simples ASO - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		
		GregorianCalendar dataCorte = new GregorianCalendar();
		dataCorte = OrsegupsUtils.getSpecificWorkDay(dataCorte, -30L);
		dataCorte.set(Calendar.HOUR_OF_DAY, 0);
		dataCorte.set(Calendar.MINUTE, 0);
		dataCorte.set(Calendar.SECOND, 0);
		String dataCorteString = NeoUtils.safeDateFormat(dataCorte, "yyyy-MM-dd HH:mm:ss");

		try
		{
			sql.append(" Select fun.numemp, fun.tipcol, fun.numcad, fun.nomfun, fun.datadm, fun.numcpf, reg.USU_CodReg, ");
			sql.append("  		cpl.ultexm, DATEADD(year,1,cpl.ultexm) as proexm, car.TitRed, orn.USU_LotOrn, orn.NomLoc ");
			sql.append(" From R034FUN fun ");
			sql.append(" Inner Join R034CPL cpl On cpl.numemp = fun.numemp And cpl.tipcol = fun.tipcol And cpl.numcad = fun.numcad ");
			sql.append(" Inner Join R024CAR car On car.EstCar = fun.EstCar And car.CodCar = fun.CodCar ");
			sql.append(" Inner Join R016ORN orn On orn.numloc = fun.numloc And orn.taborg = fun.taborg ");
			sql.append(" Inner Join USU_T200REG reg On reg.USU_CodReg = orn.usu_codreg ");
			sql.append(" Where fun.tipcol = 1 And fun.sitafa in (1,2) And orn.usu_atuaso = 'S' And reg.USU_CodReg <> 0 And cpl.ultexm <> '1900-12-31'");
			sql.append(" And Case ");
			sql.append(" 	When DATEDIFF(year , fun.datnas , getdate()) > 45 ");
			sql.append(" 		Then DATEADD(month,11,cpl.ultexm) ");
			sql.append(" 		Else DATEADD(month,23,cpl.ultexm) ");
			sql.append(" End < getdate() ");
			sql.append(" And Not Exists (Select * From [CACUPE\\SQL02].fusion_producao.DBO.d_TarefaASO tas WITH (NOLOCK) WHERE tas.numcpf = fun.numcpf ");
			sql.append("			     And CAST(floor(CAST(tas.proexm As FLOAT)) As datetime) > CAST(floor(CAST(cpl.ultexm AS FLOAT)) As datetime)) ");
			sql.append(" UNION ");
			sql.append(" Select fun.numemp, fun.tipcol, fun.numcad, fun.nomfun, fun.datadm, fun.numcpf, reg.USU_CodReg, ");
			sql.append(" 		cpl.ultexm, DATEADD(year,1,cpl.ultexm) as proexm, car.TitRed, orn.USU_LotOrn , orn.NomLoc ");
			sql.append(" From R034FUN fun ");
			sql.append(" Inner Join R034CPL cpl On cpl.numemp = fun.numemp And cpl.tipcol = fun.tipcol And cpl.numcad = fun.numcad ");
			sql.append(" Inner Join R024CAR car On car.EstCar = fun.EstCar And car.CodCar = fun.CodCar ");
			sql.append(" Inner Join USU_T038COBFUN cob On fun.numemp = cob.usu_numemp and fun.tipcol = cob.usu_tipcol and fun.numcad = cob.usu_numcad ");
			sql.append(" Inner Join R016ORN orn On orn.numloc = cob.usu_numloc And orn.taborg = cob.usu_taborg ");
			sql.append(" Inner Join USU_T200REG reg On reg.USU_CodReg = orn.usu_codreg ");
			sql.append(" where fun.tipcol = 1 And fun.sitafa in (1,2) And orn.usu_atuaso = 'S' And reg.USU_CodReg <> 0 And cpl.ultexm <> '1900-12-31'");
			sql.append(" And Case ");
			sql.append(" 	When DATEDIFF(year , fun.datnas , getdate()) > 45 ");
			sql.append(" 		Then DATEADD(month,11,cpl.ultexm) ");
			sql.append(" 		Else DATEADD(month,23,cpl.ultexm) ");
			sql.append(" End < getdate() ");
			sql.append(" And Not Exists (Select * From [CACUPE\\SQL02].fusion_producao.DBO.d_TarefaASO tas WITH (NOLOCK) WHERE tas.numcpf = fun.numcpf ");
			sql.append(" And CAST(floor(CAST(tas.proexm As FLOAT)) As datetime) > CAST(floor(CAST(cpl.ultexm AS FLOAT)) As datetime)) ");
			sql.append(" And ((cob.usu_datfim > '"+dataCorteString+"') or (usu_datinccob > '"+dataCorteString+"'))");
			sql.append(" group By fun.numemp, fun.tipcol, fun.numcad, fun.nomfun, fun.datadm, fun.numcpf, reg.USU_CodReg,cpl.ultexm, car.TitRed, orn.USU_LotOrn, orn.NomLoc ");
			sql.append(" Order By reg.USU_CodReg, fun.numemp, fun.numcad ");
			
			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();

			while (rs.next())
			{
				String ultimoExame = NeoUtils.safeDateFormat(rs.getDate("ultexm"), "dd/MM/yyyy");
				String proximoExame = NeoUtils.safeDateFormat(rs.getDate("proexm"), "dd/MM/yyyy");
				
				String solicitante = "leonardo.gargioni";
				String titulo = "Agendar o próximo ASO - Atestado de Saúde Ocupacional do colaborador " + rs.getInt("NumEmp") + "/" + rs.getInt("NumCad") + " - " + rs.getString("NomFun");
				
				String descricao = "";
				descricao = " <strong>Colaborador:</strong> " + rs.getInt("NumEmp") + "/" + rs.getInt("NumCad") + " - " + rs.getString("NomFun") + "<br>";
				descricao = descricao + " <strong>Cargo:</strong> " + rs.getString("TitRed") + "<br>";
				descricao = descricao + " <strong>Posto:</strong> " + rs.getString("USU_LotOrn") + " - " + rs.getString("NomLoc") + "<br>";
				descricao = descricao + " <strong>Último Exame:</strong> " + ultimoExame + "<br>";
				descricao = descricao + " <strong>Próximo Exame:</strong> " + proximoExame + "<br>";
				
				GregorianCalendar prazo = new GregorianCalendar();
				prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 20L);
				prazo.set(Calendar.HOUR_OF_DAY, 23);
				prazo.set(Calendar.MINUTE, 59);
				prazo.set(Calendar.SECOND, 59);

				Long codReg = rs.getLong("USU_CodReg"); 

				NeoPaper papel = new NeoPaper();
				String executor = "";

				papel = OrsegupsUtils.getPapelResponsavelASO(codReg, null);

				if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
				{
					for (NeoUser user : papel.getUsers())
					{
						executor = user.getCode();
						break;
					}
				}
				else
				{
					log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples ASO - Papel " + papel.getCode() + " sem usuário definido - Colaborador: " + rs.getInt("numemp") + "/" + rs.getInt("numcad") + " - Data: " + NeoUtils.safeDateFormat(rs.getDate("proexm"), "dd/MM/yyyy"));
					continue;
				}

				IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
				String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);
				
				InstantiableEntityInfo tarefaAS = AdapterUtils.getInstantiableEntityInfo("tarefaASO");
				NeoObject no = tarefaAS.createNewInstance();
				EntityWrapper wrapper = new EntityWrapper(no);

				GregorianCalendar proexm = new GregorianCalendar();
				proexm.setTime(rs.getDate("proexm"));

				wrapper.findField("numemp").setValue(rs.getLong("numemp"));
				wrapper.findField("tipcol").setValue(rs.getLong("tipcol"));
				wrapper.findField("numcad").setValue(rs.getLong("numcad"));
				wrapper.findField("numcpf").setValue(rs.getLong("numcpf"));
				wrapper.findField("proexm").setValue(proexm);
				wrapper.findField("tarefa").setValue(tarefa);
				PersistEngine.persist(no);

				log.warn("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples ASO - Responsavel " + executor + " Colaborador: " + rs.getInt("NumEmp") + "/" + rs.getInt("NumCad") + " - Data: " + NeoUtils.safeDateFormat(rs.getDate("DatAdm"), "dd/MM/yyyy"));
				
				Thread.sleep(1000);
			}
		}
		catch (Exception e)
		{
			log.error("##### ERRO AGENDADOR DE TAREFA: Abre Tarefa Simples Validade Registro Armamento");
			System.out.println("["+key+"] ##### ERRO AGENDADOR DE TAREFA: Abre Tarefa Simples Validade Registro Armamento");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
		}
		finally
		{

			try
			{
				rs.close();
				pstm.close();
				conn.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}

			log.warn("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Simples ASO - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}
}
