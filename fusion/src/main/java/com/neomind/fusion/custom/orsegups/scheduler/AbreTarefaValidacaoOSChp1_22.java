package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoDateUtils;

public class AbreTarefaValidacaoOSChp1_22 implements CustomJobAdapter {

    @SuppressWarnings("unused")
    private static final Log log = LogFactory.getLog(AbreTarefaValidacaoOSChp1.class);

    @Override
    public void execute(CustomJobContext arg0)
    {	
	Connection conn = PersistEngine.getConnection("SIGMA90"); 
	PreparedStatement pstm = null;
	ResultSet rs = null;
	final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.AbreTarefaValidacaoOSChp1_22");
	log.warn("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Validacao OS Chp1 - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	System.out.println("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Validacao OS Chp1 AbreTarefaValidacaoOSChp1_22 - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	Long key = GregorianCalendar.getInstance().getTimeInMillis();
	try
	{
	    StringBuffer  varname1 = new StringBuffer();
	    varname1.append("                    select SUBSTRING(NM_ROTA,1,3) AS REGIONAL, ");
	    varname1.append("                           (SELECT COUNT(1) ");
	    varname1.append("                              FROM [FSOODB03\\SQL01].SIGMA90.dbo.OS_IMAGEM oi ");
	    varname1.append("                             WHERE a.ID_ORDEM = oi.CD_ORDEM_SERVICO) AS QTDE_IMAGEM_OS, ");
	    varname1.append("                           (SELECT COUNT(1) ");
	    varname1.append("                              FROM [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente b ");
	    varname1.append("                             WHERE a.CD_CLIENTE = b.cdCliente) AS QTDE_TAREFA_VALIDACAO, ");
	    varname1.append("                           (SELECT COUNT(1) ");
	    varname1.append("                              FROM [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente b ");
	    varname1.append("                             WHERE a.CD_CLIENTE = b.cdCliente ");
	    varname1.append("                               AND b.visitaValidada = 1 ");
	    varname1.append("                               AND b.dataRemocao is null) AS QTDE_VALIDACAO_POSITIVA, ");
	    varname1.append("                           (SELECT COUNT(1) ");
	    varname1.append("                              FROM [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente b ");
	    varname1.append("                             WHERE a.CD_CLIENTE = b.cdCliente ");
	    varname1.append("                               AND b.visitaValidada = 1 ");
	    varname1.append("                               AND b.dataRemocao is not null) AS QTDE_VALIDACAO_POSITIVA_RETIRADO, ");
	    varname1.append("                           (SELECT COUNT(1) ");
	    varname1.append("                              FROM [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente b ");
	    varname1.append("                             WHERE a.CD_CLIENTE = b.cdCliente ");
	    varname1.append("                               AND b.visitaValidada = 0) AS QTDE_VALIDACAO_NEGATIVA, ");
	    varname1.append("                           (SELECT COUNT(1) ");
	    varname1.append("                              FROM [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente b ");
	    varname1.append("                             WHERE a.CD_CLIENTE = b.cdCliente ");
	    varname1.append("                               AND b.visitaValidada is null) AS QTDE_AGUARDANDO_VALIDACAO, ");
	    varname1.append("                           * ");
	    varname1.append("                      from [FSOODB03\\SQL01].SIGMA90.dbo.dbOrdem a ");
	    varname1.append("                inner join [FSOODB03\\SQL01].SIGMA90.dbo.dbCentral c on a.cd_cliente = c.cd_cliente ");
	    varname1.append("           left outer join [FSOODB03\\SQL01].SIGMA90.DBO.dbcidade cid on cid.id_cidade = c.id_cidade ");
	    varname1.append("           left outer join [FSOODB03\\SQL01].SIGMA90.DBO.dbbairro bai on bai.id_cidade = c.id_cidade and bai.id_bairro = c.id_bairro ");
	    varname1.append("           left outer join [FSOODB03\\SQL01].SIGMA90.DBO.ROTA R on R.CD_ROTA = C.ID_ROTA ");
	    varname1.append("                     where a.fechado = 1 ");
	    varname1.append("                       and a.fg_visualizado_mobile = 1 ");
	    varname1.append("                       and (a.defeito like '%#FOTOCHIP#%' or a.executado like '%#FOTOCHIP#%') ");
	    varname1.append("                       and a.FECHAMENTO > DATEADD(DAY,-1,GETDATE()) ");
	    varname1.append("            order by a.CD_CLIENTE,a.ID_ORDEM");
	    
	    pstm = conn.prepareStatement(varname1.toString());

	    rs = pstm.executeQuery();

	    while (rs.next())
	    {
		Long qtdeImagemOs = rs.getLong("QTDE_IMAGEM_OS");
		Long qtdeTarefaValidacao = rs.getLong("QTDE_TAREFA_VALIDACAO");
		Long qtdeValidacaoPositiva = rs.getLong("QTDE_VALIDACAO_POSITIVA");
		Long qtdeValidacaoPositivaRetirado = rs.getLong("QTDE_VALIDACAO_POSITIVA_RETIRADO");
		Long qtdeValidacaoNegativa = rs.getLong("QTDE_VALIDACAO_NEGATIVA");
		Long qtdeAguardandoValidacao = rs.getLong("QTDE_AGUARDANDO_VALIDACAO");
		
		
		Long cdHistorico = 0l; 
		Long empresaConta = rs.getLong("ID_EMPRESA");
		Long cdCliente = rs.getLong("CD_CLIENTE");
		Integer IcdCliente = cdCliente.intValue();
		Long cdTecnico = rs.getLong("ID_INSTALADOR");
		//String valida = rs.getString("VISITA");
		Integer Icdtecnico = cdTecnico.intValue();
		//String existeImagem = rs.getString("EXISTE_IMAGEM");
		String cdEvento = "0";
		String idCentral = rs.getString("ID_CENTRAL");
		String particao = rs.getString("PARTICAO");
		String razaoSocial = rs.getString("RAZAO");
		String endereco = rs.getString("ENDERECO");
		//String existeCliente = rs.getString("EXISTE_CLIENTE");
		String viatura = "Gerado por OS";
		String defeito = rs.getString("DEFEITO");
		Integer idosdefeito = 1019;//código padrão
		Long lOpabriu = rs.getLong("OPABRIU");
		Integer opabriu = lOpabriu.intValue();
		Long lcdossolicitante = rs.getLong("CD_OS_SOLICITANTE");
		Integer cdossolicitante = lcdossolicitante.intValue();
		String logViatura = "";
		Long idOrdem = rs.getLong("ID_ORDEM");
		String dataBruta = rs.getString("FECHAMENTO");
		String anoData = dataBruta.substring(0, 4);
		String mesData = dataBruta.substring(5, 7);
		String diaData = dataBruta.substring(8, 10);
		String dataFechamento = diaData + "/" + mesData + "/" + anoData;

		Long codregional = OrsegupsUtils.getCodigoRegional(rs.getString("REGIONAL"));

		NeoPaper papelCoordRegional = OrsegupsUtils.getPapelCoordenadorRegional(codregional);

		@SuppressWarnings("unused")
		NeoUser respCoordRegional = null;

		if(papelCoordRegional != null){

		    @SuppressWarnings("unchecked")
		    Set<NeoUser> coordenadores = papelCoordRegional.getAllUsers();

		    for(NeoUser coordenador : coordenadores){

			respCoordRegional = coordenador;

		    }

		}else{

		    //seta Laercio caso não encontre o papel dos coordenadores responsáveis
		    respCoordRegional = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("neoId", 11566071));

		}

		String userSolicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteChp1Cliente"); 
		NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", userSolicitante));
		NeoPaper papel = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "SolicitanteChp1Cliente"));

		
		if (qtdeImagemOs == 0 && qtdeTarefaValidacao == 0) {
		    
		    abreOSeGeraTarefa(idOrdem, respCoordRegional, idCentral, particao, razaoSocial, dataFechamento, IcdCliente, Icdtecnico, defeito, idosdefeito, opabriu, cdossolicitante);
		    
		} else if (qtdeImagemOs == 0 && qtdeTarefaValidacao > 0 && qtdeValidacaoPositiva == 0 && qtdeValidacaoPositivaRetirado == 0 && qtdeValidacaoNegativa > 0 && qtdeAguardandoValidacao == 0) {
		    
		    abreOSeGeraTarefa(idOrdem, respCoordRegional, idCentral, particao, razaoSocial, dataFechamento, IcdCliente, Icdtecnico, defeito, idosdefeito, opabriu, cdossolicitante);
		    
		} else if (qtdeImagemOs == 0 && qtdeTarefaValidacao > 0 && qtdeValidacaoPositiva == 0 && qtdeValidacaoNegativa > 0 && qtdeAguardandoValidacao == 0) {
		    
		    abreOSeGeraTarefa(idOrdem, respCoordRegional, idCentral, particao, razaoSocial, dataFechamento, IcdCliente, Icdtecnico, defeito, idosdefeito, opabriu, cdossolicitante);
		    
		} else if (qtdeImagemOs > 0 && qtdeTarefaValidacao == 0 ) {
		    
		    abreTarefaValidacao(cdHistorico, idCentral, empresaConta, particao, razaoSocial, endereco, viatura, solicitante, papel, cdCliente, idOrdem, logViatura, cdEvento);
		    
		} else if (qtdeImagemOs > 0 && qtdeTarefaValidacao > 0 && qtdeValidacaoPositiva == 0 && (qtdeTarefaValidacao == qtdeValidacaoPositivaRetirado + qtdeValidacaoNegativa)) {
		    
		    abreTarefaValidacao(cdHistorico, idCentral, empresaConta, particao, razaoSocial, endereco, viatura, solicitante, papel, cdCliente, idOrdem, logViatura, cdEvento);
		    
		}
	    }
	    
	}catch (Exception e) {
	    log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Validacao OS Chp1");
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
	}
	finally {
	    try {
		rs.close();
		pstm.close();
		conn.close();
	    } catch (SQLException e) {
		e.printStackTrace();
	    }

	    log.warn("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Vaidacao OS Chp1 - - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	    System.out.println("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Validacao OS Chp1 AbreTarefaValidacaoOSChp1_22 - - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	}
    }
	
    public void abreTarefaValidacao(Long _cdHistorico, String _idCentral, Long _empresaConta, String _particao, String _razaoSocial, String _endereco, 
	    String _viatura, NeoUser _solicitante, NeoPaper _papel, Long _cdCliente, Long _idOrdem, String _logViatura, String _cdEvento) {
	NeoObject wkfOCP = AdapterUtils.createNewEntityInstance("Chp1Cliente");
	    final EntityWrapper ewWkfOCP = new EntityWrapper(wkfOCP);

	    ewWkfOCP.findField("cdHistorico").setValue(_cdHistorico); 
	    ewWkfOCP.findField("contaSIGMA").setValue(_idCentral);
	    ewWkfOCP.findField("empresaConta").setValue(_empresaConta);
	    ewWkfOCP.findField("particao").setValue(_particao);
	    ewWkfOCP.findField("razaoSocial").setValue(_razaoSocial);
	    ewWkfOCP.findField("endereco").setValue(_endereco);
	    ewWkfOCP.findField("viatura").setValue(_viatura);
	    ewWkfOCP.findField("solicitante").setValue(_solicitante);
	    ewWkfOCP.findField("pool").setValue(_papel);
	    ewWkfOCP.findField("cdCliente").setValue(_cdCliente);
	    ewWkfOCP.findField("idOrdem").setValue(_idOrdem);

	    int index = _logViatura.indexOf("#");
	    String resultadoVisita = "";
	    if (index > 0) {
		resultadoVisita = _logViatura.substring(0, index);					
	    } else {
		resultadoVisita = _logViatura.toString();					
	    }
	    Boolean visitaOK = Boolean.TRUE;
	    if (resultadoVisita.contains("PENDENTE")) {
		ewWkfOCP.findField("eventoAGerar").setValue(_cdEvento.substring(0,_cdEvento.length()-1)+"3");
	    } else {
		ewWkfOCP.findField("eventoAGerar").setValue(_cdEvento.substring(0,_cdEvento.length()-1)+"2");
	    }
	    ewWkfOCP.findField("visitaOK").setValue(visitaOK);		
	    PersistEngine.persist(wkfOCP);
	    ProcessModel pm = (ProcessModel) PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("name", "O040-OperacaoChp1Cliente"));		
	    OrsegupsWorkflowHelper.iniciaProcesso(pm, wkfOCP, true, _solicitante, true, null);
    }
    
    public void abreOSeGeraTarefa(Long _idOrdem, NeoUser _respCoordRegional, String _idCentral, String _particao, String _razaoSocial, String _dataFechamento, Integer _IcdCliente, Integer _Icdtecnico,
	    String _defeito, Integer _idosdefeito, Integer _opabriu, Integer _cdossolicitante){
	//Verifica se OS já gerou uma tarefa Simples
	    List<NeoObject> registroChp1 = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("Chp1RegistroOS"), new QLRawFilter("os = " + _idOrdem));

	    if(registroChp1.size() == 0){

		//abre tarefa simples
		NeoObject oTarefaSimples = AdapterUtils.createNewEntityInstance("Tarefa");
		EntityWrapper wTarefaSimples = new EntityWrapper(oTarefaSimples);

		//Setado o usuário do maurelio.pinto fixamente para a função do SOLICITANTE da tarefa simples
		NeoUser newSolicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("neoId", 15382777));

		wTarefaSimples.findField("Solicitante").setValue(newSolicitante);

		NeoUser executor = _respCoordRegional;

		wTarefaSimples.findField("Executor").setValue(executor);

		wTarefaSimples.findField("Titulo").setValue("Justificar OS sem foto do Chip - Cliente: " + _idCentral + "[" +_particao +"] - "+_razaoSocial);

		wTarefaSimples.findField("DescricaoSolicitacao").setValue("No fechamento da OS " + _idOrdem + " no dia " + _dataFechamento + " foi solicitado tirar foto do Chip no cliente porém a foto"
			+ " não foi anexada na OS cidada.<br><br>Favor justificar o motivo porque não foi anexada a foto do Chip!");

		GregorianCalendar prazoTarefaSimples = new GregorianCalendar();

		prazoTarefaSimples.add(Calendar.HOUR, 48);

		wTarefaSimples.findField("Prazo").setValue(prazoTarefaSimples);

		ProcessModel pmS = PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("name", "G001 - Tarefa Simples"));

		OrsegupsWorkflowHelper.iniciaProcesso(pmS, oTarefaSimples, true, newSolicitante, true, null);

		abrirOrdemServico(_IcdCliente, _Icdtecnico, _defeito, _idosdefeito, _opabriu, _cdossolicitante);

		//ADICIONA REGISTRO NO EFORM
		adicionaRegistroChp1(_idOrdem);

	    }
    }

    public NeoObject adicionaRegistroChp1(Long OS){

	NeoObject oRegistrosChp1 = AdapterUtils.createNewEntityInstance("Chp1RegistroOS");

	EntityWrapper wRegistrosChp1 = new EntityWrapper(oRegistrosChp1);

	if(OS == null){
	    log.warn("REGISTRO DE CHP1 VAZIO");
	}else{
	    wRegistrosChp1.setValue("os", OS);	
	}

	PersistEngine.persist(oRegistrosChp1);

	return oRegistrosChp1;

    }

    private void abrirOrdemServico(int cdCliente, int cdTecnico, String defeito, int idosdefeito, int opabriu, int cdossolicitante){

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();
	sql.append(" INSERT INTO dbORDEM ");
	sql.append(" (CD_CLIENTE, ID_INSTALADOR, ABERTURA,  DEFEITO, OPABRIU, IDOSDEFEITO, FG_EMAIL_ENVIADO, CD_OS_SOLICITANTE, FG_TODAS_PARTICOES_EM_MANUTENCAO,  EXECUTADO,	TEMPOEXECUCAOPREVISTO )");
	sql.append(" VALUES(?,          ?,        GETDATE(), ?,       ?,       ?,           ?,                ?,                 ?,					?,		1 )");

	try {
	    conn = PersistEngine.getConnection("SIGMA90");
	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setInt(1, cdCliente);
	    pstm.setInt(2, cdTecnico);
	    pstm.setString(3, "Registrar a foto do Chip GPRS da central. Havendo a necessidade de troca(Valide no link: http://chip.legal), retirar fotos dos 2 Chips(Novo e Antigo), marcar um X no Chip "
		    + "antigo para diferenciar.#FOTOCHIP#");
	    pstm.setInt(4, 11010); //FUSION
	    pstm.setInt(5, idosdefeito);
	    pstm.setInt(6, 0); //FALSO	    
	    pstm.setInt(7, 10011); //5-DEMAIS ORDENS DE SERVIÇO
	    pstm.setInt(8, 0); //FALSO
	    pstm.setString(9, "");

	    pstm.executeUpdate();

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}


    }
}
