package com.neomind.fusion.custom.orsegups.scheduler;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoUtils;

public class AbreTarefaSimplesPostagemSite implements CustomJobAdapter
{
	@Override
	public void execute(CustomJobContext arg0)
	{
		
		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.AbreTarefaSimplesPostagemSite");
		log.warn("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Simples Postagem Site - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		Long key = GregorianCalendar.getInstance().getTimeInMillis(); 
		
		try
		{
			String solicitante = "leticia.mendes";
			String executor = "rogerio.siqueira";
			String titulo = "Postagem de matéria no site";
			String descricao = " Favor informar se houve fechamento de contratos coorporativos para postagem de matéria no site.";
			
			GregorianCalendar dataPrazo = new GregorianCalendar();
			dataPrazo.add(Calendar.DAY_OF_MONTH, +1);
			
			dataPrazo = OrsegupsUtils.getNextWorkDay(dataPrazo);
			dataPrazo.set(Calendar.HOUR_OF_DAY, 23);
			dataPrazo.set(Calendar.MINUTE, 59);
			dataPrazo.set(Calendar.SECOND, 59);
			
			IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
			String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", dataPrazo);
			
			log.warn("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Postagem Site - Tarefa aberta: "+ tarefa);
		}
		catch (Exception e)
		{

			log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Postagem Site");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
		}
		finally
		{
			log.warn("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Simples Postagem Site - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}
}