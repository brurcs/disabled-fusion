package com.neomind.fusion.custom.orsegups.contract.clientws.cliente;

import java.rmi.RemoteException;

public class Sapiens_Synccom_senior_g5_co_ger_cad_clientesProxy
		implements
		com.neomind.fusion.custom.orsegups.contract.clientws.cliente.Sapiens_Synccom_senior_g5_co_ger_cad_clientes {
	private String _endpoint = null;
	private com.neomind.fusion.custom.orsegups.contract.clientws.cliente.Sapiens_Synccom_senior_g5_co_ger_cad_clientes sapiens_Synccom_senior_g5_co_ger_cad_clientes = null;

	public Sapiens_Synccom_senior_g5_co_ger_cad_clientesProxy() {
		_initSapiens_Synccom_senior_g5_co_ger_cad_clientesProxy();
	}

	public Sapiens_Synccom_senior_g5_co_ger_cad_clientesProxy(String endpoint) {
		_endpoint = endpoint;
		_initSapiens_Synccom_senior_g5_co_ger_cad_clientesProxy();
	}

	private void _initSapiens_Synccom_senior_g5_co_ger_cad_clientesProxy() {
		try {
			sapiens_Synccom_senior_g5_co_ger_cad_clientes = (new com.neomind.fusion.custom.orsegups.contract.clientws.cliente.G5SeniorServicesLocator())
					.getsapiens_Synccom_senior_g5_co_ger_cad_clientesPort();
			if (sapiens_Synccom_senior_g5_co_ger_cad_clientes != null) {
				if (_endpoint != null)
					((javax.xml.rpc.Stub) sapiens_Synccom_senior_g5_co_ger_cad_clientes)
							._setProperty(
									"javax.xml.rpc.service.endpoint.address",
									_endpoint);
				else
					_endpoint = (String) ((javax.xml.rpc.Stub) sapiens_Synccom_senior_g5_co_ger_cad_clientes)
							._getProperty("javax.xml.rpc.service.endpoint.address");
			}

		} catch (javax.xml.rpc.ServiceException serviceException) {
		}
	}

	public String getEndpoint() {
		return _endpoint;
	}

	public void setEndpoint(String endpoint) {
		_endpoint = endpoint;
		if (sapiens_Synccom_senior_g5_co_ger_cad_clientes != null)
			((javax.xml.rpc.Stub) sapiens_Synccom_senior_g5_co_ger_cad_clientes)
					._setProperty("javax.xml.rpc.service.endpoint.address",
							_endpoint);

	}

	public com.neomind.fusion.custom.orsegups.contract.clientws.cliente.Sapiens_Synccom_senior_g5_co_ger_cad_clientes getSapiens_Synccom_senior_g5_co_ger_cad_clientes() {
		if (sapiens_Synccom_senior_g5_co_ger_cad_clientes == null)
			_initSapiens_Synccom_senior_g5_co_ger_cad_clientesProxy();
		return sapiens_Synccom_senior_g5_co_ger_cad_clientes;
	}

	@Override
	public ClientesGravarClientesOut gravarClientes(String user,
			String password, int encryption, ClientesGravarClientesIn parameters)
			throws RemoteException {
		if (sapiens_Synccom_senior_g5_co_ger_cad_clientes == null){
			_initSapiens_Synccom_senior_g5_co_ger_cad_clientesProxy();
		}
		return sapiens_Synccom_senior_g5_co_ger_cad_clientes.gravarClientes(user, password, encryption, parameters);
	}

}