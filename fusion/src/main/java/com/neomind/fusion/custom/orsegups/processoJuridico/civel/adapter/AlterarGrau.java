package com.neomind.fusion.custom.orsegups.processoJuridico.civel.adapter;

import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class AlterarGrau implements AdapterInterface
{
	@Override
	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{
			origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);

			processEntity.setValue("recorrer", false);
			processEntity.setValue("acao", null);

			if (origin.getActivityName().toString().contains("Movimentar Processo"))
			{

				if (String.valueOf(processEntity.getValue("grauProcesso")).contains("1º"))
					processEntity.setValue("grauProcesso", "2º Grau");
				else if (String.valueOf(processEntity.getValue("grauProcesso")).contains("2º"))
					processEntity.setValue("grauProcesso", "3º Grau");
				else if (String.valueOf(processEntity.getValue("grauProcesso")).contains("3º"))
					processEntity.setValue("grauProcesso", "4º Grau");
				else
					processEntity.setValue("grauProcesso", "5º Grau");
			}
		}
		catch (Exception e)
		{
			System.out.println("Erro na classe AlterarGrauDoProcesso do fluxo J004.");
			e.printStackTrace();
			throw new WorkflowException(e.getMessage());
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
