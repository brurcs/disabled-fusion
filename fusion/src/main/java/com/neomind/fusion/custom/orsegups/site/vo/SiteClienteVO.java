package com.neomind.fusion.custom.orsegups.site.vo;

public class SiteClienteVO {

	private String empresa;
	private String filial;
	private String codigoCliente;
	private String intnet;
	
	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public String getFilial() {
		return filial;
	}
	public void setFilial(String filial) {
		this.filial = filial;
	}
	public String getCodigoCliente() {
		return codigoCliente;
	}
	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}
	public String getIntnet()
	{
		return intnet;
	}
	public void setIntnet(String intnet)
	{
		this.intnet = intnet;
	}
	
	
	
}
