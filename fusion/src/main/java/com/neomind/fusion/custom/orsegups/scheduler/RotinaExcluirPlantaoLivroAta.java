package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoUtils;

public class RotinaExcluirPlantaoLivroAta implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(RotinaExcluirPlantaoLivroAta.class);

	public void execute(CustomJobContext arg0)
	{
		log.warn("##### INICIAR ROTINA EXCLUIR PLANTÃO LIVRO ATA - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		Connection conn = null;
		PreparedStatement pstmLF  = null;
		PreparedStatement pstmLC = null; 
		Long key = GregorianCalendar.getInstance().getTimeInMillis(); 

		try
		{
			String deleteLF = "DELETE FROM LIVRO_ATA_FROTA WHERE DT_FIM_USO_VIATURA IS NOT NULL";
			String deleteLC = "DELETE FROM LIVRO_ATA_COLABORADOR WHERE DT_SAIDA_PLANTAO IS NOT NULL";

			conn = PersistEngine.getConnection("SIGMA90");
			conn.setAutoCommit(false);

			pstmLF = conn.prepareStatement(deleteLF.toString());
			pstmLC = conn.prepareStatement(deleteLC.toString());
			pstmLF.executeUpdate();
			pstmLC.executeUpdate();
			conn.commit();
		

			log.warn("##### EXECUTAR ROTINA EXCLUIR PLANTÃO LIVRO ATA - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		catch (Exception e)
		{
			
			
			try
			{
				conn.rollback();
				log.error("##### ERRO ROTINA EXCLUIR PLANTÃO LIVRO ATA: " + e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
				System.out.println("["+key+"] ##### ERRO ROTINA GERAR EVENTO XLIG - Data: " + e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
				e.printStackTrace();
				throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
			}
			catch (SQLException e1)
			{
				e1.printStackTrace();
			}
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(null, pstmLF, null);
				OrsegupsUtils.closeConnection(conn, pstmLC, null);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				log.error("##### ERRO ROTINA EXCLUIR PLANTÃO LIVRO ATA: " + e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			}
			log.warn("##### FINALIZAR ROTINA EXCLUIR PLANTÃO LIVRO ATA - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}

}
