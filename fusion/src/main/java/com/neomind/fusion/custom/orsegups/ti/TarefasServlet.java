package com.neomind.fusion.custom.orsegups.ti;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.HtmlToText;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

@WebServlet(name="TarefasServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.ti.TarefasServlet"})
public class TarefasServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	private static final Log log = LogFactory.getLog(TarefasServlet.class);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	public void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException
	{
		response.setContentType("text/html");
		response.setCharacterEncoding("ISO-8859-1");

		PrintWriter out;
		try
		{
			out = response.getWriter();

			String action = "";

			if (request.getParameter("action") != null)
			{
				action = request.getParameter("action");

			}

			if (action.equalsIgnoreCase("trocarTipo"))
			{
				this.trocarTipo(request, response, out);
			}

			if (action.equalsIgnoreCase("trocarSituacao"))
			{
				this.trocarSituacao(request, response, out);
			}

			if (action.equalsIgnoreCase("atualizaPosicaoTarefa"))
			{
				this.atualizaPosicaoTarefa(request, response, out);
			}

			if (action.equalsIgnoreCase("chartTarefasPorGrupo"))
			{
				this.chartTarefasPorGrupo(request, response, out);
			}

			if (action.equalsIgnoreCase("montaTabelaTarefasUsuario"))
			{
				this.montaTabelaTarefasUsuario(request, response, out);
			}
			if (action.equalsIgnoreCase("montaTabelaTarefasGrupo"))
			{
				this.montaTabelaTarefasGrupo(request, response, out);
			}
			if (action.equalsIgnoreCase("montaTabelaChipGPRSUsuario"))
			{
				this.montaTabelaChipGPRSUsuario(request, response, out);
			}
			if (action.equalsIgnoreCase("montaTabelaTarefasGrupoSolicitante"))
			{
				this.montaTabelaTarefasGrupo(request, response, out);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	private void trocarTipo(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{
		String tarefa = request.getParameter("tarefa");
		String tipo = request.getParameter("tipo");
		String modelo = request.getParameter("model");
		if (tarefa != null && !tarefa.isEmpty() && tipo != null && !tipo.isEmpty())
		{
			try
			{
				QLGroupFilter filter = new QLGroupFilter("AND");
				filter.addFilter(new QLEqualsFilter("tarefa", tarefa));
				filter.addFilter(new QLEqualsFilter("tipo", tipo));
				filter.addFilter(new QLEqualsFilter("modelo", Long.parseLong(modelo)));
				List<NeoObject> tarefas = null;
				tarefas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICONTROLETAREFAS"), filter);
				PersistEngine.getEntityManager().flush();

				if (tarefas != null && !tarefas.isEmpty())
				{
					NeoObject neoObject = (NeoObject) tarefas.get(0);
					if (tipo.equals("D"))
						tipo = "S";
					else
						tipo = "D";
					EntityWrapper clienteWrapper = new EntityWrapper(neoObject);

					clienteWrapper.findField("tipo").setValue(tipo);

					PersistEngine.persist(neoObject);

				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	private void trocarSituacao(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{
		String tarefa = request.getParameter("tarefa");
		String situacao = request.getParameter("situacao");
		String model = request.getParameter("model");
		if (tarefa != null && !tarefa.isEmpty() && situacao != null && !situacao.isEmpty())
		{
			try
			{
				QLGroupFilter filter = new QLGroupFilter("AND");
				filter.addFilter(new QLEqualsFilter("tarefa", tarefa));
				filter.addFilter(new QLEqualsFilter("modelo", Long.parseLong(model)));
				List<NeoObject> tarefas = null;
				tarefas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICONTROLETAREFAS"), filter);
				PersistEngine.getEntityManager().flush();

				if (tarefas != null && !tarefas.isEmpty())
				{
					NeoObject neoObject = (NeoObject) tarefas.get(0);
					if (situacao.equals("E"))
						situacao = "T";
					else
						situacao = "E";
					EntityWrapper clienteWrapper = new EntityWrapper(neoObject);

					clienteWrapper.findField("situacao").setValue(situacao);

					PersistEngine.persist(neoObject);

				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	private void atualizaPosicaoTarefa(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{
		String tarefa = request.getParameter("tarefa");
		String posicao = request.getParameter("posicao");
		String usuarioCode = request.getParameter("code");
		String modelo = request.getParameter("model");
		Long count = 1L;
		Long origem = 0L;
		if (tarefa != null && !tarefa.isEmpty() && !tarefa.contains("null") && usuarioCode != null && !usuarioCode.isEmpty() && !usuarioCode.contains("null") && posicao != null && !posicao.isEmpty() && !posicao.contains("null"))
		{
			try
			{
				QLGroupFilter filter = new QLGroupFilter("AND");
				NeoUser usuario = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", usuarioCode));
				filter.addFilter(new QLEqualsFilter("usuario", usuario));
				filter.addFilter(new QLEqualsFilter("modelo", Long.parseLong(modelo)));
				List<TarefasVO> tarefasVOs = new ArrayList<TarefasVO>();
				List<NeoObject> tarefas = null;
				tarefas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICONTROLETAREFAS"), filter, -1, -1, "posicao asc");
				PersistEngine.getEntityManager().flush();

				if (tarefas != null && !tarefas.isEmpty())
				{
					for (NeoObject neoObject : tarefas)
					{
						EntityWrapper tarefaObj = new EntityWrapper(neoObject);
						TarefasVO tarefasVO = new TarefasVO();
						tarefasVO.setCodigoTarefa(Long.parseLong((String) tarefaObj.findValue("tarefa")));
						if (((String) tarefaObj.findValue("tarefa")).contains(tarefa))
						{
							origem = (Long) tarefaObj.findValue("posicao");

						}
						tarefasVOs.add(tarefasVO);
					}

					TarefasVO vo = tarefasVOs.get(origem.intValue() - 1);
					tarefasVOs.remove(origem.intValue() - 1);
					vo.setPosicaoTarefa(Long.parseLong(String.valueOf(posicao)));
					int pos = Integer.parseInt(posicao) - 1;
					tarefasVOs.add(pos, vo);

					for (TarefasVO tarefasVO : tarefasVOs)
					{
						QLGroupFilter filterAux = new QLGroupFilter("AND");
						filterAux.addFilter(new QLEqualsFilter("usuario", usuario));
						filterAux.addFilter(new QLEqualsFilter("tarefa", tarefasVO.getCodigoTarefa().toString()));
						filterAux.addFilter(new QLEqualsFilter("modelo", Long.parseLong(modelo)));
						tarefas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICONTROLETAREFAS"), filterAux);
						PersistEngine.getEntityManager().flush();
						if (tarefas != null && !tarefas.isEmpty())
						{

							EntityWrapper clienteWrapper = new EntityWrapper(tarefas.get(0));

							clienteWrapper.findField("posicao").setValue(count);

							PersistEngine.persist(tarefas.get(0));

						}
						count++;

					}
				}

			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	private void chartTarefasPorGrupo(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{

		Connection conn = PersistEngine.getConnection("FUSIONPROD");
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		List<TarefasVO> tarefasVOs = null;
		List<TarefasUsuarioVO> tarefasUsuarioVOs = null;
		List<TarefasGrupoUsuarioVO> tarefasGrupoUsuarioVOs = null;
		String wfProcess = "18317794,18317811,638564840,639014270,647233104,638338425,638347864";
		String model = "18888";
		log.warn("##### INICIAR PESQUISA POR TAREFAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		String dataJSON = "[";

		try
		{

			sql.append(" select p.neoId as wfprocess, p.code, us.neoid as codigoUsuario,see.code as usuario, us.fullName,ta.titulo,tas.descricao,p.startDate, t.startDate,ta.prazo, Replace(se.code, 'SupervisaoSustentacaoSistemas', 'Coordenação de Sistemas') as grupo ");
			sql.append(" from wfProcess p with(nolock)  ");
			sql.append(" inner join activity a with(nolock)  on p.neoId = a.process_neoId ");
			sql.append(" inner join task t with(nolock)  on t.activity_neoid = a.neoid ");
			sql.append(" inner join neouser us with(nolock)  on us.neoid = t.user_neoId ");
			sql.append(" inner join neogroup g with(nolock)  on g.neoId = us.group_neoId ");
			sql.append(" inner join securityentity se with(nolock)  on se.neoid = g.neoid ");
			sql.append(" inner join securityentity see with(nolock)  on see.neoid = us.neoid ");
			sql.append(" inner join d_tarefa ta with(nolock)  on p.entity_neoid = ta.neoid ");
			sql.append(" inner join d_TarefaRegistroAtividades tas with(nolock)   ");
			sql.append(" on tas.neoid = (select min(tar.registroAtividades_neoid) from d_tarefa_registroAtividades tar with(nolock)  where ta.neoid = tar.d_tarefa_neoid ) ");
			sql.append(" where g.neoId IN ( " + wfProcess + " )");
			sql.append(" and p.processState = 0 ");
			sql.append(" and p.model_neoId in (select pmaux.neoId from ProcessModel pmaux where pmaux.name like '%G001 - Tarefa Simples%')   ");
			sql.append(" and p.saved = 1 ");
			sql.append(" and t.finishDate is null ");
			sql.append(" and p.startDate is not null ");
			sql.append(" order by Replace(se.code, 'SupervisaoSustentacaoSistemas', 'Coordenação de Sistemas'),us.fullName asc ");

			pstm = conn.prepareStatement(sql.toString());
			//pstm.setLong(1, Long.parseLong(wfProcess));
			//pstm.setLong(1, Long.parseLong(model));

			rs = pstm.executeQuery();
			tarefasVOs = new ArrayList<TarefasVO>();
			tarefasUsuarioVOs = new ArrayList<TarefasUsuarioVO>();
			tarefasGrupoUsuarioVOs = new ArrayList<TarefasGrupoUsuarioVO>();
			TarefasVO tarefasVO = null;
			TarefasUsuarioVO tarefasUsuarioVO = null;
			TarefasGrupoUsuarioVO tarefasGrupoUsuarioVO = null;
			int cont = 0;
			int total = 0;
			int totalGrupo = 0;
			String nomeAtual = "";
			String grupoAtual = "";
			String nomeUsuario = "";
			Long neoIdUsuario = 0L;
			HtmlToText htmlToText = new HtmlToText();

			while (rs.next())
			{
				tarefasVO = new TarefasVO();
				if (nomeAtual != null && !nomeAtual.equals(rs.getString("fullName")) && cont > 0)
				{

					tarefasUsuarioVO = new TarefasUsuarioVO();
					tarefasUsuarioVO.setCodigoUsuario(neoIdUsuario);
					tarefasUsuarioVO.setUsuario(nomeAtual);
					tarefasUsuarioVO.setTotal(String.valueOf(total));
					tarefasUsuarioVO.setTarefas(tarefasVOs);
					tarefasUsuarioVOs.add(tarefasUsuarioVO);
					if (grupoAtual != null && !grupoAtual.equals(rs.getString("grupo")) && cont > 0)
					{
						tarefasGrupoUsuarioVO = new TarefasGrupoUsuarioVO();
						tarefasGrupoUsuarioVO.setGrupo(grupoAtual);
						tarefasGrupoUsuarioVO.setTotal(totalGrupo);
						if (grupoAtual.contains("Sistemas"))
							tarefasGrupoUsuarioVO.setId("sistemas");
						tarefasGrupoUsuarioVO.setTarefas(tarefasUsuarioVOs);
						tarefasGrupoUsuarioVOs.add(tarefasGrupoUsuarioVO);
						tarefasUsuarioVOs = new ArrayList<TarefasUsuarioVO>();
						totalGrupo = 0;
					}
					tarefasVOs = new ArrayList<TarefasVO>();
					total = 0;

				}
				neoIdUsuario = rs.getLong("codigoUsuario");
				nomeUsuario = rs.getString("usuario");
				nomeAtual = rs.getString("fullName");
				tarefasVO.setWfProcess(rs.getLong("wfprocess"));
				tarefasVO.setCodigoTarefa(rs.getLong("code"));
				tarefasVO.setNomeCompleto(nomeAtual);
				tarefasVO.setUsuarioCode(nomeUsuario);
				tarefasVO.setTituloTarefa(htmlToText.parse(rs.getString("titulo")));

				GregorianCalendar dataAux = new GregorianCalendar();
				dataAux.setTime(rs.getTimestamp("startDate"));
				tarefasVO.setDataAberturaTarefa(NeoDateUtils.safeDateFormat(dataAux, "dd/MM/yyyy"));
				GregorianCalendar dataAux2 = new GregorianCalendar();
				if(rs.getTimestamp("prazo") != null)
				dataAux2.setTime(rs.getTimestamp("prazo"));
				tarefasVO.setPrazoTarefa(NeoDateUtils.safeDateFormat(dataAux2, "dd/MM/yyyy HH:mm"));
				GregorianCalendar dataAux3 = new GregorianCalendar();
				dataAux3.setTime(rs.getTimestamp("ultimaAlteracao"));
				tarefasVO.setDataUltimaAlteracao(NeoDateUtils.safeDateFormat(dataAux3, "dd/MM/yyyy"));
				grupoAtual = rs.getString("grupo");
				String descricao = rs.getString("descricao");
				descricao = htmlToText.parse(descricao);
				descricao = descricao.replaceAll("\\<.*?>", "");
				descricao = descricao.replaceAll("&nbsp;", "");
				descricao = descricao.replaceAll("&amp;", "");
				tarefasVO.setDescricaoTarefa(descricao + "\n Prazo:" + tarefasVO.getPrazoTarefa());

				tarefasVOs.add(tarefasVO);
				cont++;
				total++;
				totalGrupo++;

			}
			if (cont > 0)
			{ //TODO ADICIONAR TOTAL
			    	
				tarefasUsuarioVO = new TarefasUsuarioVO();
				tarefasUsuarioVO.setCodigoUsuario(neoIdUsuario);
				tarefasUsuarioVO.setUsuario(nomeAtual);
				
				Map<Long, Long> mapTotaisTarefas = this.getTotaisGeraisUsuario(String.valueOf(tarefasUsuarioVO.getCodigoUsuario()));
				Long totalExecMesUsuario = mapTotaisTarefas.get(tarefasUsuarioVO.getCodigoUsuario());
				if (totalExecMesUsuario == null){
				    totalExecMesUsuario = 0L;
				}
				tarefasUsuarioVO.setTotalExecMes(totalExecMesUsuario);
				
				tarefasUsuarioVO.setTotal(String.valueOf(total));
				tarefasUsuarioVO.setTarefas(tarefasVOs);
				tarefasUsuarioVOs.add(tarefasUsuarioVO);

				tarefasGrupoUsuarioVO = new TarefasGrupoUsuarioVO();
				tarefasGrupoUsuarioVO.setGrupo(grupoAtual);
				tarefasGrupoUsuarioVO.setTotal(totalGrupo);
				if (grupoAtual.contains("Infra"))
					tarefasGrupoUsuarioVO.setId("infra");
				tarefasGrupoUsuarioVO.setTarefas(tarefasUsuarioVOs);
				tarefasGrupoUsuarioVOs.add(tarefasGrupoUsuarioVO);

			}
			log.warn("##### EXECUTAR PESQUISA POR TAREFAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		}
		catch (Exception e)
		{
			log.error("##### ERRO PESQUISA POR TAREFAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			e.printStackTrace();
		}
		finally
		{

			try
			{

				OrsegupsUtils.closeConnection(conn, pstm, rs);
				int countAux = 0;
				for (TarefasGrupoUsuarioVO grupoUsuarioVO : tarefasGrupoUsuarioVOs)
				{
					for (TarefasUsuarioVO tarefasUsuarioVO : grupoUsuarioVO.getTarefas())
					{
						for (TarefasVO tarefasVO : tarefasUsuarioVO.getTarefas())
						{
							if (tarefasVO.getCodigoTarefa() != null)
							{

								QLGroupFilter groupFilter = new QLGroupFilter("AND");
								groupFilter.addFilter(new QLEqualsFilter("tarefa", tarefasVO.getCodigoTarefa().toString()));
								groupFilter.addFilter(new QLEqualsFilter("modelo", Long.parseLong("18888")));
								List<NeoObject> tarefasObj = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICONTROLETAREFAS"), groupFilter);
								if (tarefasObj != null && !tarefasObj.isEmpty())
								{
									EntityWrapper entityWrapper = new EntityWrapper(tarefasObj.get(0));
									tarefasVO.setPosicaoTarefa((Long) entityWrapper.findValue("posicao"));
									tarefasVO.setTipoTarefa((String) entityWrapper.findValue("tipo"));
									tarefasVO.setSituacaoTarefa((String) entityWrapper.findValue("situacao"));
								}
							}
						}
						Collections.sort((List<TarefasVO>) tarefasUsuarioVO.getTarefas());

					}
					for (TarefasUsuarioVO tarefasVO : grupoUsuarioVO.getTarefas())
					{
						if (countAux > 0)
							dataJSON += ",";
						if (tarefasVO.getTotal() == null)
							dataJSON += "[\"" + null + "\" , " + null + "]";
						else
							dataJSON += "[\"" + tarefasVO.getUsuario() + "\"  ," + Long.parseLong(tarefasVO.getTotal()) + "]";

						countAux++;
					}
				}

			}
			catch (Exception e)
			{
				log.error("##### ERRO PESQUISA POR TAREFAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
				e.printStackTrace();
			}
			finally
			{
				log.warn("##### FINALIZAR  PESQUISA POR TAREFAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
				dataJSON += "]";

				out.print(dataJSON);
			}

		}

	}
	
	//FINALIZAR
	private TarefasUsuarioVO pesquisaTarefas(String wfProcess, String model, String code)
	{

		Connection conn = PersistEngine.getConnection("FUSIONPROD");
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		List<TarefasVO> tarefasVOs = null;
		List<TarefasUsuarioVO> tarefasUsuarioVOs = null;
		List<TarefasGrupoUsuarioVO> tarefasGrupoUsuarioVOs = null;
		log.warn("##### INICIAR PESQUISA POR TAREFAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		//TIGESTAOTAREFASCHIP
		Long neoIdUsuario = 0L;
		try
		{
			String where = "";
			String replace = "se.code";
			
			if (wfProcess.equals("18317794") || wfProcess.equals("638564840")){
			    replace = "Replace(se.code, 'SupervisaoSustentacaoSistemas', 'Coordenação de Sistemas')";
			}
			
			if (wfProcess.equals("647233104") || wfProcess.equals("638338425") || wfProcess.equals("638347864") ){
			    replace = "Replace(Replace(Replace(se.code, 'ContabilidadeOrsitec', 'Consultoria'),'CoordenacaoOrsitec','Consultoria'),'Sins','Consultoria')";
			}
			
			sql.append(" select p.neoId as wfprocess, p.code, us.neoid as codigoUsuario,see.code as usuario, us.fullName,ta.titulo,p.startDate,t.startDate as ultimaAlteracao,ta.prazo, "+replace+" as grupo, usr.fullName as solicitante, ");
			sql.append(" gu.neoid as codigoGrupoSolicitante,seu.code as grupoSolicitante, usr.neoid as codigoSolicitante ");
			if (NeoUtils.safeIsNotNull(model) && !model.isEmpty() && model.equals("18888"))
				sql.append(" ,tas.descricao ");
			sql.append(" from wfProcess p with(nolock)  ");
			sql.append(" inner join activity a with(nolock)  on p.neoId = a.process_neoId ");
			sql.append(" inner join task t with(nolock)  on t.activity_neoid = a.neoid ");
			sql.append(" inner join neouser usr with(nolock) on usr.neoid = p.requester_neoid  ");
			sql.append(" inner join neouser us with(nolock)  on us.neoid = t.user_neoId ");
			sql.append(" inner join neogroup g with(nolock)  on g.neoId = us.group_neoId ");
			sql.append(" inner join neogroup gu with(nolock) on gu.neoId = usr.group_neoId  ");
			sql.append(" inner join securityentity se with(nolock)  on se.neoid = g.neoid ");
			sql.append(" inner join securityentity see with(nolock)  on see.neoid = us.neoid ");
			sql.append(" inner join securityentity seu with(nolock) on seu.neoid = gu.neoid  ");
			if (NeoUtils.safeIsNotNull(model) && !model.isEmpty() && model.equals("18888"))
			{
				sql.append(" inner join d_tarefa ta with(nolock)  on p.entity_neoid = ta.neoid ");
				sql.append(" inner join d_TarefaRegistroAtividades tas with(nolock)   ");
				sql.append(" on tas.neoid = (select min(tar.registroAtividades_neoid) from d_tarefa_registroAtividades tar with(nolock)  where ta.neoid = tar.d_tarefa_neoid ) ");
				where = " where  p.model_neoId in (select pmaux.neoId from ProcessModel pmaux where pmaux.name like '%G001 - Tarefa Simples%') ";
			}
			if (NeoUtils.safeIsNotNull(model) && !model.isEmpty() && model.equals("186043073"))
			{
				sql.append(" inner join d_TELECOMChipsGPRSPosto ta with(nolock)  on p.neoid = ta.wfprocess_neoId ");
				where = " where  p.model_neoId in (select pmaux.neoId from ProcessModel pmaux where pmaux.name like '%C034 - Solicitar Chip GPRS para Posto%') ";
			}
			//sql.append(" where g.neoId IN ( " + wfProcess + " )");
		
			sql.append(where);
			sql.append(" and p.processState = 0 ");
			if (code != null && !code.isEmpty() && !code.contains("null"))
				sql.append(" and us.neoid = ? ");
			sql.append(" and p.saved = 1 ");
			sql.append(" and t.finishDate is null ");
			sql.append(" and p.startDate is not null ");
			sql.append(" order by "+replace+",us.fullName asc ");

			pstm = conn.prepareStatement(sql.toString());
			//pstm.setLong(1, Long.parseLong(wfProcess));
			//pstm.setLong(1, Long.parseLong(model));
			if (code != null && !code.isEmpty() && !code.contains("null"))
				pstm.setLong(1, Long.parseLong(code));

			rs = pstm.executeQuery();
			tarefasVOs = new ArrayList<TarefasVO>();
			tarefasUsuarioVOs = new ArrayList<TarefasUsuarioVO>();
			tarefasGrupoUsuarioVOs = new ArrayList<TarefasGrupoUsuarioVO>();
			TarefasVO tarefasVO = null;
			TarefasUsuarioVO tarefasUsuarioVO = null;
			TarefasGrupoUsuarioVO tarefasGrupoUsuarioVO = null;
			int cont = 0;
			int total = 0;
			int totalGrupo = 0;
			String nomeAtual = "";
			String grupoAtual = "";
			String nomeUsuario = "";

			HtmlToText htmlToText = new HtmlToText();
			                	
			Map<Long, Long> mapTotaisTarefas = this.getTotaisGeraisUsuario(code);
			
			while (rs.next())
			{
				tarefasVO = new TarefasVO();
				if (nomeAtual != null && !nomeAtual.equals(rs.getString("fullName")) && cont > 0)
				{
				    
					List<NeoObject> listaTarefas = retornaObjects(nomeUsuario, Long.parseLong(model));
					verificaTarefas(listaTarefas, tarefasVOs, nomeUsuario);
					tarefasUsuarioVO = new TarefasUsuarioVO();
					tarefasUsuarioVO.setCodigoUsuario(neoIdUsuario);
					tarefasUsuarioVO.setUsuario(nomeAtual);
					tarefasUsuarioVO.setTotal(String.valueOf(total));
					/** Tras totais por usuário */
					
					
					Long totalExecMesUsuario = mapTotaisTarefas.get(tarefasUsuarioVO.getCodigoUsuario());
					if (totalExecMesUsuario == null){
					    totalExecMesUsuario = 0L;
					}
					
					tarefasUsuarioVO.setTarefas(tarefasVOs);
					tarefasUsuarioVOs.add(tarefasUsuarioVO);
					if (grupoAtual != null && !grupoAtual.equals(rs.getString("grupo")) && cont > 0)
					{
						tarefasGrupoUsuarioVO = new TarefasGrupoUsuarioVO();
						tarefasGrupoUsuarioVO.setGrupo(grupoAtual);
						tarefasGrupoUsuarioVO.setTotal(totalGrupo);
						if (grupoAtual.contains("Sistemas"))
							tarefasGrupoUsuarioVO.setId("sistemas");
						tarefasGrupoUsuarioVO.setTarefas(tarefasUsuarioVOs);
						tarefasGrupoUsuarioVOs.add(tarefasGrupoUsuarioVO);
						tarefasUsuarioVOs = new ArrayList<TarefasUsuarioVO>();
						totalGrupo = 0;
					}
					tarefasVOs = new ArrayList<TarefasVO>();
					total = 0;

				}
				neoIdUsuario = rs.getLong("codigoUsuario");
				nomeUsuario = rs.getString("usuario");
				nomeAtual = rs.getString("fullName");
				tarefasVO.setWfProcess(rs.getLong("wfprocess"));
				tarefasVO.setCodigoTarefa(rs.getLong("code"));
				tarefasVO.setSolicitante(rs.getString("solicitante"));
				tarefasVO.setCodigoSolicitante(rs.getLong("codigoSolicitante"));
				tarefasVO.setGrupoSolicitante(rs.getString("grupoSolicitante"));
				tarefasVO.setCodigoGrupoSolicitante(rs.getLong("codigoGrupoSolicitante"));
				tarefasVO.setNomeCompleto(nomeAtual);
				tarefasVO.setUsuarioCode(nomeUsuario);
				tarefasVO.setTituloTarefa(htmlToText.parse(rs.getString("titulo")));
				tarefasVO.setModelo(Long.parseLong(model));

				GregorianCalendar dataAux = new GregorianCalendar();
				dataAux.setTime(rs.getTimestamp("startDate"));
				tarefasVO.setDataAberturaTarefa(NeoDateUtils.safeDateFormat(dataAux, "dd/MM/yyyy"));
				GregorianCalendar dataAux2 = new GregorianCalendar();
				dataAux2.setTime(rs.getTimestamp("prazo"));
				tarefasVO.setPrazoTarefa(NeoDateUtils.safeDateFormat(dataAux2, "dd/MM/yyyy HH:mm"));
				GregorianCalendar dataAux3 = new GregorianCalendar();
				dataAux3.setTime(rs.getTimestamp("ultimaAlteracao"));
				tarefasVO.setDataUltimaAlteracao(NeoDateUtils.safeDateFormat(dataAux3, "dd/MM/yyyy"));
				grupoAtual = rs.getString("grupo");
				if (NeoUtils.safeIsNotNull(model) && !model.isEmpty() && model.equals("18888"))
				{
					String descricao = rs.getString("descricao");
					descricao = htmlToText.parse(descricao);
					descricao = descricao.replaceAll("\\<.*?>", "");
					descricao = descricao.replaceAll("&nbsp;", "");
					descricao = descricao.replaceAll("&amp;", "");
					tarefasVO.setDescricaoTarefa(descricao + "\n Prazo:" + tarefasVO.getPrazoTarefa());
				}
				tarefasVOs.add(tarefasVO);
				cont++;
				total++;
				totalGrupo++;

			}
			if (cont > 0)
			{
				List<NeoObject> listaTarefas = retornaObjects(nomeUsuario, Long.parseLong(model));
				verificaTarefas(listaTarefas, tarefasVOs, nomeUsuario);
				tarefasUsuarioVO = new TarefasUsuarioVO();
				tarefasUsuarioVO.setCodigoUsuario(neoIdUsuario);
				tarefasUsuarioVO.setUsuario(nomeAtual);
				tarefasUsuarioVO.setTotal(String.valueOf(total));
				Long totalExecMesUsuario = mapTotaisTarefas.get(tarefasUsuarioVO.getCodigoUsuario());
				if (totalExecMesUsuario == null){
				    totalExecMesUsuario = 0L;
				}
				tarefasUsuarioVO.setTotalExecMes(totalExecMesUsuario);
				tarefasUsuarioVO.setTarefas(tarefasVOs);
				tarefasUsuarioVOs.add(tarefasUsuarioVO);

				tarefasGrupoUsuarioVO = new TarefasGrupoUsuarioVO();
				tarefasGrupoUsuarioVO.setGrupo(grupoAtual);
				tarefasGrupoUsuarioVO.setTotal(totalGrupo);
				if (grupoAtual.contains("Infra"))
					tarefasGrupoUsuarioVO.setId("infra");
				tarefasGrupoUsuarioVO.setTarefas(tarefasUsuarioVOs);
				tarefasGrupoUsuarioVOs.add(tarefasGrupoUsuarioVO);

			}
			log.warn("##### EXECUTAR PESQUISA POR TAREFAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		}
		catch (Exception e)
		{
			log.error("##### ERRO PESQUISA POR TAREFAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			e.printStackTrace();
		}
		finally
		{

			TarefasUsuarioVO tarefasUsuarioVOAux = new TarefasUsuarioVO();
			try
			{

				OrsegupsUtils.closeConnection(conn, pstm, rs);
				int countAux = 0;
				for (TarefasGrupoUsuarioVO grupoUsuarioVO : tarefasGrupoUsuarioVOs)
				{
					for (TarefasUsuarioVO tarefasUsuarioVO : grupoUsuarioVO.getTarefas())
					{

						for (TarefasVO tarefasVO : tarefasUsuarioVO.getTarefas())
						{
							if (tarefasVO.getCodigoTarefa() != null)
							{

								QLGroupFilter groupFilter = new QLGroupFilter("AND");
								groupFilter.addFilter(new QLEqualsFilter("tarefa", tarefasVO.getCodigoTarefa().toString()));
								groupFilter.addFilter(new QLEqualsFilter("modelo", tarefasVO.getModelo()));
								List<NeoObject> tarefasObj = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICONTROLETAREFAS"), groupFilter);
								if (tarefasObj != null && !tarefasObj.isEmpty())
								{
									EntityWrapper entityWrapper = new EntityWrapper(tarefasObj.get(0));
									tarefasVO.setPosicaoTarefa((Long) entityWrapper.findValue("posicao"));
									tarefasVO.setTipoTarefa((String) entityWrapper.findValue("tipo"));
									tarefasVO.setSituacaoTarefa((String) entityWrapper.findValue("situacao"));
								}
							}
						}
						Collections.sort((List<TarefasVO>) tarefasUsuarioVO.getTarefas());
						if (code != null && !code.isEmpty() && !code.contains("null"))
						{

							tarefasUsuarioVOAux = tarefasUsuarioVO;
						}

					}

				}

			}
			catch (Exception e)
			{
				log.error("##### ERRO PESQUISA POR TAREFAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
				e.printStackTrace();
			}
			log.warn("##### FINALIZAR  PESQUISA POR TAREFAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

			tarefasUsuarioVOAux.setCodigoUsuario(neoIdUsuario);
			return tarefasUsuarioVOAux;

		}

	}

	

	private void montaTabelaTarefasUsuario(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{

		String wfProcess = request.getParameter("wfProcess");
		String model = request.getParameter("model");
		String code = request.getParameter("code");
		StringBuilder stringBuilder = null;

		try
		{	
			TarefasUsuarioVO tarefasUsuarioVOAux = (TarefasUsuarioVO) pesquisaTarefas(wfProcess, model, code);
			if (tarefasUsuarioVOAux != null)
			{

				stringBuilder = new StringBuilder();
				stringBuilder.append(" <div class=\"panel-heading\" > ");
				String executor = "";
				NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "TIGESTAOTAREFASCHIP"));
				if (obj != null && obj.getAllUsers() != null && !obj.getAllUsers().isEmpty())
				{
					for (NeoUser usuarioTarefa : obj.getUsers())
					{
						executor += usuarioTarefa.getNeoId();
					}
				}
				if (executor != null && !executor.isEmpty() && executor.contains(String.valueOf(code)))
				{
					stringBuilder.append(" <div align=\"left\"> ");
					stringBuilder.append(" <div class=\"btn-group btn-group-xs\" role=\"group\" aria-label=\"Extra-small button group\"> ");
					stringBuilder.append("  <button type=\"button\" class=\"btn btn-default\" title=\"Tarefa Simples\"  onclick=\"javascript:showAtualizaTabela('" + code + "','" + code + "')\"><span class=\"glyphicon glyphicon-step-backward\" ></span></button> ");
					stringBuilder.append("  <button type=\"button\" class=\"btn btn-default\" title=\"CHIP GPRS\"  onclick=\"javascript:showAtualizaTabelaChipGPRS('" + code + "','" + code + "')\"><span class=\"glyphicon glyphicon-step-forward\" ></span></button> ");
					stringBuilder.append(" </div> ");
					stringBuilder.append(" </div> ");
				}

				stringBuilder.append(" <div align=\"right\"> ");
				stringBuilder.append(" <span  class=\"badge progress-bar-success\" style=\"font-family: verdana; font-size: 9px;color:white;\">" + tarefasUsuarioVOAux.getTotalExecMes() + "</span> ");
				stringBuilder.append(" <span  class=\"badge progress-bar-danger\" style=\"font-family: verdana; font-size: 9px;color:white;\">" + tarefasUsuarioVOAux.getTotal() + "</span></div> ");
				stringBuilder.append(" <span class=\"glyphicon glyphicon-user\" ></span> ");
				stringBuilder.append(" <span style=\"font-family: verdana; font-size: 9px;\">" + tarefasUsuarioVOAux.getUsuario() + "</span>");
				stringBuilder.append(" </div> ");
				int count = 0;
				stringBuilder.append(" <ul id=\"tarefa-list\" name=\"tarefa-list\"  class=\"list-group\" style=\"width: 100%; font-family: verdana; font-size: 10px;\"> ");
				for (TarefasVO tarefasUsuarioVO : tarefasUsuarioVOAux.getTarefas())
				{

					//					if (count < 5)
					//					{
					//						stringBuilder.append(" <li class=\"list-group-item list-group-item-danger\" data-type=" + tarefasUsuarioVO.getCodigoTarefa() + "> ");
					//					}
					//					if (count >= 5 && count <= 9)
					//					{
					//						stringBuilder.append(" <li class=\"list-group-item list-group-item-warning\" data-type=" + tarefasUsuarioVO.getCodigoTarefa() + "> ");
					//					}
					//					if (count > 9)
					//					{
					//						stringBuilder.append(" <li class=\"list-group-item list-group-item-success\" data-type=" + tarefasUsuarioVO.getCodigoTarefa() + "> ");
					//					}

					stringBuilder.append(" <li class=\"list-group-item list-group-item-default\" data-type=" + tarefasUsuarioVO.getCodigoTarefa() + "> ");

					Boolean isDiretoria = ((PortalUtil.getCurrentUser().getGroup().getCode().contains("Diretoria")) || (PortalUtil.getCurrentUser().getGroup().getCode().contains("Presidência")));

					String tarefaToolTip = tarefasUsuarioVO.getCodigoTarefa() + " - " + tarefasUsuarioVO.getTituloTarefa();
					stringBuilder.append(" " + tarefasUsuarioVO.getPosicaoTarefa() + "º - " + tarefasUsuarioVO.getCodigoTarefa() + "</br>");
					//TODO ADICIONAR NOVO BOTAO
					stringBuilder.append("   Título: " + tarefasUsuarioVO.getTituloTarefa());
					stringBuilder.append("</br> Solicitante: " + tarefasUsuarioVO.getSolicitante() + "</br>  Abertura: " + tarefasUsuarioVO.getDataAberturaTarefa() + "<br> Última Alteração: " + tarefasUsuarioVO.getDataUltimaAlteracao() + "<br>");
					stringBuilder.append(" <button type=\"button\" class=\"btn btn-default btn-xs\" data-container=\"body\"");
					stringBuilder.append(" data-toggle=\"popover\" data-placement=\"bottom\" title=\"" + tarefaToolTip + "\"");
					stringBuilder.append(" data-content=\"" + tarefasUsuarioVO.getDescricaoTarefa().toString() + "\"> ");
					stringBuilder.append(" <span class=\"glyphicon glyphicon-search\" aria-hidden=\"true\"></span> ");
					stringBuilder.append(" </button> ");

					stringBuilder.append(" <button type=\"button\" class=\"btn btn-default btn-xs\" data-container=\"body\" data-toggle=\"popover\"  ");
					stringBuilder.append(" title=\"Verificar tarefa detalhada\" data-content=\"Clique no botão para verificar!\" ");
					stringBuilder.append(" onclick=\"javascript:showTarefa('" + tarefasUsuarioVO.getWfProcess().toString() + "')\"> ");
					stringBuilder.append(" <span class=\"glyphicon glyphicon-zoom-in\" aria-hidden=\"true\"></span> ");
					stringBuilder.append(" </button> ");
					stringBuilder.append(" <div class=\"btn-group\"> ");
					Boolean flag = PortalUtil.getCurrentUser() != null && PortalUtil.getCurrentUser().getNeoId() != 730475369 && PortalUtil.getCurrentUser().getNeoId() != 598912158 && PortalUtil.getCurrentUser().getNeoId() != 47133909 && PortalUtil.getCurrentUser().getNeoId() != 20166 && PortalUtil.getCurrentUser().getNeoId() != 106214471 && !isDiretoria;

					if (flag)
					{
						stringBuilder.append(" <button class=\"btn btn-default btn-xs dropdown-toggle\" type=\"button\" id=\"dropdownMenu\"  data-toggle=\"dropdown\" aria-expanded=\"false\" disabled=\"true\"> ");

					}
					else
					{
						stringBuilder.append(" <button class=\"btn btn-default btn-xs dropdown-toggle\" type=\"button\" id=\"dropdownMenu\"  data-toggle=\"dropdown\" aria-expanded=\"false\" > ");

					}
					stringBuilder.append(" <span class=\"glyphicon glyphicon-sort-by-attributes\" id=\"dropdown_title\"></span><span class=\"caret\"></span> ");
					stringBuilder.append(" </button> ");
					stringBuilder.append(" <ul class=\"dropdown-menu pull-center\" role=\"menu\"> ");
					for (int i = 1; i <= Integer.parseInt(tarefasUsuarioVOAux.getTotal()); i++)
					{
						if (flag)
						{
							stringBuilder.append(" <li class=\"disabled\"> ");

						}
						else
						{
							stringBuilder.append(" <li> ");
						}

						stringBuilder.append(" <a id=\"dpPosicao\" onclick=\"javascript:showPosicao('" + tarefasUsuarioVO.getCodigoTarefa() + "','" + i + "','" + tarefasUsuarioVO.getUsuarioCode() + "',this,'" + tarefasUsuarioVOAux.getCodigoUsuario() + "','" + model + "')\" href=\"#\">");
						stringBuilder.append(" " + i + "</a></li>");
					}

					stringBuilder.append("  </ul> ");
					stringBuilder.append(" </div> ");
					if (tarefasUsuarioVO.getTipoTarefa() != null && tarefasUsuarioVO.getTipoTarefa().contains("D") && (tarefasUsuarioVOAux.getUsuario().contains(PortalUtil.getCurrentUser().getFullName()) || !flag))
					{
						stringBuilder.append(" 	<span class=\"badge progress-bar-success\"  title=\"Desenvolvimento\" onclick=\"javascript:showTipo('" + tarefasUsuarioVO.getCodigoTarefa() + "','" + tarefasUsuarioVO.getTipoTarefa() + "',this,'" + tarefasUsuarioVOAux.getCodigoUsuario() + "','" + tarefasUsuarioVO.getUsuarioCode() + "','" + model + "')\" style=\"cursor: pointer;\">");
						stringBuilder.append(" " + tarefasUsuarioVO.getTipoTarefa() + "</span> ");
					}
					else if (tarefasUsuarioVO.getTipoTarefa() != null && tarefasUsuarioVO.getTipoTarefa().contains("D"))
					{
						stringBuilder.append(" 	<span class=\"badge progress-bar-success\"  title=\"Desenvolvimento\" >");
						stringBuilder.append(" " + tarefasUsuarioVO.getTipoTarefa() + "</span> ");
					}

					else if (tarefasUsuarioVO.getTipoTarefa() != null && tarefasUsuarioVO.getTipoTarefa().contains("S") && (tarefasUsuarioVOAux.getUsuario().equals(PortalUtil.getCurrentUser().getFullName()) || !flag))
					{
						stringBuilder.append(" 	<span class=\"badge progress-bar-warning\"  title=\"Suporte\" onclick=\"javascript:showTipo('" + tarefasUsuarioVO.getCodigoTarefa() + "','" + tarefasUsuarioVO.getTipoTarefa() + "',this,'" + tarefasUsuarioVOAux.getCodigoUsuario() + "','" + tarefasUsuarioVO.getUsuarioCode() + "','" + model + "')\" style=\"cursor: pointer;\" >");
						stringBuilder.append(" " + tarefasUsuarioVO.getTipoTarefa() + "</span> ");
					}
					else if (tarefasUsuarioVO.getTipoTarefa() != null && tarefasUsuarioVO.getTipoTarefa().contains("S"))
					{
						stringBuilder.append(" 	<span class=\"badge progress-bar-warning\"  title=\"Suporte\">");
						stringBuilder.append(" " + tarefasUsuarioVO.getTipoTarefa() + "</span> ");
					}

					else if (tarefasUsuarioVO.getTipoTarefa() == null && (tarefasUsuarioVOAux.getUsuario().contains(PortalUtil.getCurrentUser().getFullName()) || !flag))
					{
						stringBuilder.append(" 	<span class=\"badge progress-bar-info\"  title=\"Indefinido\" onclick=\"javascript:showTipo('" + tarefasUsuarioVO.getCodigoTarefa() + "','" + tarefasUsuarioVO.getTipoTarefa() + "',this,'" + tarefasUsuarioVOAux.getCodigoUsuario() + "','" + tarefasUsuarioVO.getUsuarioCode() + "','" + model + "')\" style=\"cursor: pointer;\">");
						stringBuilder.append(" I </span> ");
					}
					else if (tarefasUsuarioVO.getTipoTarefa() == null)
					{
						stringBuilder.append(" 	<span class=\"badge progress-bar-info\"  title=\"Indefinido\" >");
						stringBuilder.append(" I </span> ");
					}

					if (tarefasUsuarioVO.getSituacaoTarefa() != null && tarefasUsuarioVO.getSituacaoTarefa().contains("T") && (tarefasUsuarioVOAux.getUsuario().contains(PortalUtil.getCurrentUser().getFullName()) || !flag))
					{
						stringBuilder.append(" 	<span class=\"badge progress-bar-danger\"  title=\"Trabalhando\" onclick=\"javascript:showSituacao('" + tarefasUsuarioVO.getCodigoTarefa() + "','" + tarefasUsuarioVO.getSituacaoTarefa() + "',this,'" + tarefasUsuarioVOAux.getCodigoUsuario() + "','" + tarefasUsuarioVO.getUsuarioCode() + "','" + model + "')\" style=\"cursor: pointer;\">");
						stringBuilder.append(" " + tarefasUsuarioVO.getSituacaoTarefa() + "</span> ");
					}
					else if (tarefasUsuarioVO.getSituacaoTarefa() != null && tarefasUsuarioVO.getSituacaoTarefa().contains("T"))
					{
						stringBuilder.append(" 	<span class=\"badge progress-bar-danger\"  title=\"Trabalhando\" >");
						stringBuilder.append(" " + tarefasUsuarioVO.getSituacaoTarefa() + "</span> ");
					}

					else if (tarefasUsuarioVO.getSituacaoTarefa() != null && tarefasUsuarioVO.getSituacaoTarefa().contains("E") && (tarefasUsuarioVOAux.getUsuario().contains(PortalUtil.getCurrentUser().getFullName()) || !flag))
					{
						stringBuilder.append(" 	<span class=\"badge progress-bar-info\"  title=\"Em espera\" onclick=\"javascript:showSituacao('" + tarefasUsuarioVO.getCodigoTarefa() + "','" + tarefasUsuarioVO.getSituacaoTarefa() + "',this,'" + tarefasUsuarioVOAux.getCodigoUsuario() + "','" + tarefasUsuarioVO.getUsuarioCode() + "','" + model + "')\" style=\"cursor: pointer;\">");
						stringBuilder.append(" " + tarefasUsuarioVO.getSituacaoTarefa() + "</span> ");
					}
					else if (tarefasUsuarioVO.getSituacaoTarefa() != null && tarefasUsuarioVO.getSituacaoTarefa().contains("E"))
					{
						stringBuilder.append(" 	<span class=\"badge progress-bar-info\"  title=\"Em espera\" >");
						stringBuilder.append(" " + tarefasUsuarioVO.getSituacaoTarefa() + "</span> ");
					}

					else if (tarefasUsuarioVO.getSituacaoTarefa() == null && (tarefasUsuarioVOAux.getUsuario().contains(PortalUtil.getCurrentUser().getFullName()) || !flag))
					{
						stringBuilder.append(" 	<span class=\"badge progress-bar-info\"  title=\"Indefinido\" onclick=\"javascript:showSituacao('" + tarefasUsuarioVO.getCodigoTarefa() + "','" + tarefasUsuarioVO.getSituacaoTarefa() + "',this,'" + tarefasUsuarioVOAux.getCodigoUsuario() + "','" + tarefasUsuarioVO.getUsuarioCode() + "','" + tarefasUsuarioVO.getModelo() + "')\" style=\"cursor: pointer;\">");
						stringBuilder.append(" I </span> ");
					}
					else if (tarefasUsuarioVO.getSituacaoTarefa() == null)
					{
						stringBuilder.append(" 	<span class=\"badge progress-bar-info\"  title=\"Indefinido\" >");
						stringBuilder.append(" I </span> ");
					}
					stringBuilder.append(" </li> ");

					count++;
				}
				stringBuilder.append(" </ul> ");
				stringBuilder.append(" <div class=\"panel-footer\"></div></div>");
				//System.out.println(stringBuilder.toString() +"\n Tarefas "+ta);
			}
		}
		catch (Exception e)
		{
			log.error("##### ERRO PESQUISA POR TAREFAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			e.printStackTrace();
		}
		finally
		{
			out.print(stringBuilder.toString());
			log.warn("##### FINALIZAR  PESQUISA POR TAREFAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}

	}

	private void montaTabelaTarefasGrupo(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{

		String wfProcess = request.getParameter("wfProcess");
		String model = request.getParameter("model");

		String grupo = request.getParameter("grupo");
		String solicitante = request.getParameter("solicitante");
		String idGrupo = request.getParameter("idGrupo");
		String grupoSolicitante = request.getParameter("grupoSolicitante");

		model = "18888";
		Boolean bloquear = Boolean.FALSE;
		StringBuilder stringBuilder = null;

		try
		{
			String nomeGrupo = "";
			if (wfProcess != null && !wfProcess.isEmpty() && wfProcess.contains("18317794"))
			{
				nomeGrupo = "sistemas";
			}else if(wfProcess != null && !wfProcess.isEmpty() && wfProcess.contains("639014270")){
			    nomeGrupo = "nexti";
			}else if (wfProcess != null && !wfProcess.isEmpty() && wfProcess.contains("647233104")){
			    nomeGrupo = "Consultoria";
			}
			else
			{
				nomeGrupo = "infra";
			}
			List<TarefasGrupoUsuarioVO> tarefasUsuarioVOAux = null;
			if (grupo != null && !grupo.isEmpty() && !idGrupo.contains("null") && solicitante != null && !solicitante.isEmpty() && !solicitante.contains("null") && idGrupo != null && !idGrupo.isEmpty() && !idGrupo.contains("null") || grupo != null && !grupo.isEmpty() && !idGrupo.contains("null") && grupoSolicitante != null && !grupoSolicitante.isEmpty() && !grupoSolicitante.contains("null") && idGrupo != null && !idGrupo.isEmpty() && !idGrupo.contains("null"))
			{
				String wfProcessAux = "";
				if (wfProcess != null && !wfProcess.isEmpty() && wfProcess.contains("sistemas"))
				{
					wfProcessAux = "18317794";
					nomeGrupo = "sistemas";
				}else if (wfProcess != null && !wfProcess.isEmpty() && wfProcess.contains("nexti"))
				{
					wfProcessAux = "639014270";
					nomeGrupo = "nexti";
				}else if(wfProcess != null && !wfProcess.isEmpty() && wfProcess.contains("consultoria")){
				    wfProcessAux = "647233104";
				    nomeGrupo = "consultoria";   
				}
				else
				{
					wfProcessAux = "18317811";
					nomeGrupo = "infra";
				}

				tarefasUsuarioVOAux = (List<TarefasGrupoUsuarioVO>) pesquisaTodasTarefas(wfProcessAux, model, solicitante, grupoSolicitante);
				bloquear = Boolean.TRUE;
			}
			else
			{
				tarefasUsuarioVOAux = (List<TarefasGrupoUsuarioVO>) pesquisaTodasTarefas(wfProcess, model);
			}

			stringBuilder = new StringBuilder();

			StringBuilder builderAux = new StringBuilder();
			StringBuilder builderCom = new StringBuilder();

			builderAux.append(" <div class=\"btn-group\"> ");
			Boolean isDiretoriaAux = ((PortalUtil.getCurrentUser().getGroup().getCode().contains("Diretoria")) || (PortalUtil.getCurrentUser().getGroup().getCode().contains("Presidência")));
			Boolean flagAux = PortalUtil.getCurrentUser() != null && PortalUtil.getCurrentUser().getNeoId() != 730475369 && PortalUtil.getCurrentUser().getNeoId() != 47133909 && PortalUtil.getCurrentUser() != null && PortalUtil.getCurrentUser().getNeoId() != 598912158 && PortalUtil.getCurrentUser().getNeoId() != 20166 && PortalUtil.getCurrentUser().getNeoId() != 106214471 && !isDiretoriaAux;
			if (flagAux)
				builderAux.append(" <a class=\"btn btn-default btn-sm dropdown-toggle btn-select1\" data-toggle=\"dropdown\" disabled=\"true\">Selecione o solicitante <span class=\"caret\"></span></a>");
			else
				builderAux.append(" <a class=\"btn btn-default btn-sm dropdown-toggle btn-select1\" data-toggle=\"dropdown\" >Selecione o solicitante <span class=\"caret\"></span></a>");

			builderAux.append(" <ul class=\"dropdown-menu scrollable-menu\">");

			TreeSet<String> solicitanteList = new TreeSet<String>();
			for (TarefasGrupoUsuarioVO tarefasGrupoUsuarioVO : tarefasUsuarioVOAux)
			{

				for (TarefasUsuarioVO tarefasUsuarioVO : tarefasGrupoUsuarioVO.getTarefas())
				{
					for (TarefasVO tarefasVO : tarefasUsuarioVO.getTarefas())
					{
						solicitanteList.add(tarefasGrupoUsuarioVO.getId() + ";" + tarefasVO.getSolicitante() + ";" + tarefasVO.getCodigoGrupoSolicitante() + ";" + tarefasGrupoUsuarioVO.getGrupo());
					}
				}
			}

			for (String solicitanteAux : solicitanteList)
			{
				String resultado[] = solicitanteAux.split(";");
				builderAux.append("<li><a id=\"dpSolicitante\" onclick=\"javascript:showSolicitante('" + resultado[0] + "','" + resultado[1] + "','" + resultado[2] + "','" + resultado[3] + "'," + model + ",'" + nomeGrupo + "','null')\" href=\"#\">" + resultado[1] + "</a></li>");
				builderAux.append("  <li class=\"divider\"></li>");
			}

			builderAux.append(" </ul>");
			builderAux.append(" </div>");

			builderCom.append(" <div class=\"btn-group\"> ");
			if (flagAux)
				builderCom.append(" <a class=\"btn btn-default btn-sm dropdown-toggle btn-select2\" data-toggle=\"dropdown\" disabled=\"true\">Selecione o grupo <span class=\"caret\"></span></a>");
			else
				builderCom.append(" <a class=\"btn btn-default btn-sm dropdown-toggle btn-select2\" data-toggle=\"dropdown\" >Selecione o grupo <span class=\"caret\"></span></a>");
			builderCom.append(" <ul class=\"dropdown-menu scrollable-menu\">");

			TreeSet<String> grupoList = new TreeSet<String>();
			for (TarefasGrupoUsuarioVO tarefasGrupoUsuarioVO : tarefasUsuarioVOAux)
			{
				for (TarefasUsuarioVO tarefasUsuarioVO : tarefasGrupoUsuarioVO.getTarefas())
				{
					for (TarefasVO tarefasVO : tarefasUsuarioVO.getTarefas())
					{
						grupoList.add(tarefasGrupoUsuarioVO.getId() + ";" + tarefasVO.getGrupoSolicitante() + ";" + tarefasVO.getCodigoGrupoSolicitante() + ";" + tarefasGrupoUsuarioVO.getGrupo());
					}
				}
			}

			for (String grupoAux : grupoList)
			{
				String resultado[] = grupoAux.split(";");
				builderCom.append("<li><a id=\"dpGrupo\" onclick=\"javascript:showSolicitante('" + resultado[0] + "','null','" + resultado[2] + "','" + resultado[3] + "'," + model + ",'" + nomeGrupo + "','" + resultado[1] + "')\" href=\"#\">" + resultado[1] + "</a></li>");
				builderCom.append("  <li class=\"divider\"></li>");
			}

			builderCom.append(" </ul>");
			builderCom.append(" </div>");
			
			


			for (TarefasGrupoUsuarioVO grupoVo : tarefasUsuarioVOAux)
			{
			    	
				if (flagAux == false){
				    /* Totalizadores */
				    	stringBuilder.append(" <button class=\"btn btn-primary hidden-button\" type=\"button\" data-toggle=\"collapse\" ");
				    	stringBuilder.append(" <span  style=\"font-family: verdana; font-size: 14px;\"><strong>");
				    	stringBuilder.append("Total finalizadas (mês): "+ "</strong></span> <span  class=\"badge progress-bar-success\" style=\"font-family: verdana; font-size: 12px;color:white;\">" + grupoVo.getTotalExecMes()+ "</span>");
				    	stringBuilder.append(" </button> ");
				    	/* Totalizadores */
				}
				// stringBuilder.append(" <div class=\"tab-pane  fade\" style=\"width: 100%; font-family: verdana; font-size: 12px;\"  id="+grupoVo.getId()+"> ");
			    	
			    	
			    	stringBuilder.append(" <button class=\"btn btn-primary hidden-button\" type=\"button\" data-toggle=\"collapse\" ");
				String collapse = "collapse".concat(grupoVo.getId());
				String targetCollapse = "#" + collapse;
				stringBuilder.append(" data-target=" + targetCollapse + " aria-expanded=\"false\" aria-controls=\"collapseExample\"> ");
				stringBuilder.append(" <span  style=\"font-family: verdana; font-size: 14px;\"><strong>");
				stringBuilder.append(grupoVo.getGrupo() + "</strong></span> <span  class=\"badge progress-bar-danger\" style=\"font-family: verdana; font-size: 12px;color:white;\">" + grupoVo.getTotal() + "</span>");

				stringBuilder.append(" </button> ");

				stringBuilder.append(" <div class=\"collapse\" id=" + collapse + ">");
				stringBuilder.append(" <div class=\"well\" style=\"margin: 0px;\"> ");
				//				stringBuilder.append("	Total de tarefas: " + grupoVo.getTotal() + " ");

				stringBuilder.append(builderAux.toString());

				stringBuilder.append(builderCom.toString());

				stringBuilder.append(" <div class=\"btn-group\">");
				stringBuilder.append(" <button type=\"button\" id=\"btnRefresh\" class=\"btn btn-sm btn-success\" onclick=\"reloadPage();\">");
				stringBuilder.append(" <span class=\"glyphicon glyphicon-refresh\" aria-hidden=\"true\"  title=\"Atualizar\" ></span></button>");
				stringBuilder.append(" </div>");
				stringBuilder.append(" </div>");

				stringBuilder.append(" </div> ");

				stringBuilder.append(" </div> ");

				stringBuilder.append(" </div> ");
				Boolean isDiretoria = ((PortalUtil.getCurrentUser().getGroup().getCode().contains("Diretoria")) || (PortalUtil.getCurrentUser().getGroup().getCode().contains("Presidência")));

				stringBuilder.append("	<table class=\"table\" id=" + grupoVo.getId().concat("x") + " style=\"width: 100%; font-family: verdana;\"> ");
				stringBuilder.append("	<tr> ");
				String executor = "";
				NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "TIGESTAOTAREFASCHIP"));
				if (obj != null && obj.getAllUsers() != null && !obj.getAllUsers().isEmpty())
				{
					for (NeoUser usuarioTarefa : obj.getUsers())
					{
						executor += usuarioTarefa.getNeoId();
					}
				}

				for (TarefasUsuarioVO vo : grupoVo.getTarefas())
				{

					stringBuilder.append(" <td style=\"max-width: 200px;\">	 ");

					stringBuilder.append(" <div class=\"panel panel-primary\" id=" + vo.getCodigoUsuario() + "");
					stringBuilder.append(" style=\"float: left; width: 100%; margin-left: 2px; min-height: 800px; max-width: 100%;min-width:180px; overflow-y:auto;padding: 2px\"> ");

					stringBuilder.append(" <div class=\"panel-heading\" > ");

					if (executor != null && !executor.isEmpty() && executor.contains(vo.getCodigoUsuario().toString()))
					{
						stringBuilder.append(" <div align=\"left\"> ");
						stringBuilder.append(" <div class=\"btn-group btn-group-xs\" role=\"group\" aria-label=\"Extra-small button group\"> ");
						stringBuilder.append("  <button type=\"button\" class=\"btn btn-default\" title=\"Tarefa Simples\"  onclick=\"javascript:showAtualizaTabela('" + vo.getCodigoUsuario() + "','" + vo.getCodigoUsuario() + "')\"><span class=\"glyphicon glyphicon-step-backward\" ></span></button> ");
						stringBuilder.append("  <button type=\"button\" class=\"btn btn-default\" title=\"CHIP GPRS\" onclick=\"javascript:showAtualizaTabelaChipGPRS('" + vo.getCodigoUsuario() + "','" + vo.getCodigoUsuario() + "')\"><span class=\"glyphicon glyphicon-step-forward\" ></span></button> ");
						stringBuilder.append(" </div> ");
						stringBuilder.append(" </div> ");
					}
					stringBuilder.append(" <div align=\"right\"> ");
					
					/* Totalizador */	
					
					if (flagAux == false){
					    
					    	stringBuilder.append(" <span  class=\"badge progress-bar-success\" style=\"font-family: verdana; font-size: 9px;color:white;\">" + vo.getTotalExecMes() + "</span> ");
					    	
					}
					
					/* Totalizador */
					stringBuilder.append(" <span  class=\"badge progress-bar-danger\" style=\"font-family: verdana; font-size: 9px;color:white;\">" + vo.getTotal() + "</span></div> ");
					stringBuilder.append(" <span class=\"glyphicon glyphicon-user\"  ></span> ");
					stringBuilder.append(" <span style=\"font-family: verdana; font-size: 9px;\">" + vo.getUsuario() + "</span>");
					stringBuilder.append(" </div> ");

					int count = 0;
					int countPosicao = 0;
					
					Collections.sort((List<TarefasVO>)vo.getTarefas());
					
					stringBuilder.append(" <ul id=\"tarefa-list\" name=\"tarefa-list\" class=\"list-group\" style=\"width: 100%; font-family: verdana; font-size: 10px;\"> ");
					for (TarefasVO tarefasUsuarioVO : vo.getTarefas())
					{
					        countPosicao += 1;
					    	
						stringBuilder.append(" <li class=\"list-group-item list-group-item-default\" data-type=" + tarefasUsuarioVO.getCodigoTarefa() + " > ");

						String tarefaToolTip = tarefasUsuarioVO.getCodigoTarefa() + " - " + tarefasUsuarioVO.getTituloTarefa();
						stringBuilder.append(" " + countPosicao + "º - " + tarefasUsuarioVO.getCodigoTarefa());
						stringBuilder.append("<span class=\"pull-right\" id=\"teste\"> ");
						
						stringBuilder.append(" <button type=\"button\" class=\"btn btn-default btn-xs\" data-toggle=\"modal\" data-target=\"#modalDesativar\" \"> ");
						stringBuilder.append(" <span class=\"glyphicon glyphicon-dashboard\" aria-hidden=\"true\"></span> ");
						stringBuilder.append(" </button> ");
						
						
						stringBuilder.append("</span>");
						stringBuilder.append("</br>  Título: " + tarefasUsuarioVO.getTituloTarefa());
						stringBuilder.append("</br> Solicitante: " + tarefasUsuarioVO.getSolicitante() + "</br>  Abertura: " + tarefasUsuarioVO.getDataAberturaTarefa() + "<br> Última Alteração: " + tarefasUsuarioVO.getDataUltimaAlteracao() + "<br>");
						stringBuilder.append(" <button type=\"button\" class=\"btn btn-default btn-xs\" data-container=\"body\"");
						stringBuilder.append(" data-toggle=\"popover\" data-placement=\"bottom\" title=\"" + tarefaToolTip + "\"");
						stringBuilder.append(" data-content=\"" + tarefasUsuarioVO.getDescricaoTarefa().toString() + "\"> ");
						stringBuilder.append(" <span class=\"glyphicon glyphicon-search\" aria-hidden=\"true\"></span> ");
						stringBuilder.append(" </button> ");

						stringBuilder.append(" <button type=\"button\" class=\"btn btn-default btn-xs\" data-container=\"body\" data-toggle=\"popover\"  ");
						stringBuilder.append(" title=\"Verificar tarefa detalhada\" data-content=\"Clique no botão para verificar!\" ");
						stringBuilder.append(" onclick=\"javascript:showTarefa('" + tarefasUsuarioVO.getWfProcess().toString() + "')\"> ");
						stringBuilder.append(" <span class=\"glyphicon glyphicon-zoom-in\" aria-hidden=\"true\"></span> ");
						stringBuilder.append(" </button> ");

						stringBuilder.append(" <div class=\"btn-group\"> "); 
						Boolean flag = PortalUtil.getCurrentUser() != null && PortalUtil.getCurrentUser().getNeoId() != 730475369 && PortalUtil.getCurrentUser().getNeoId() != 598912158 && PortalUtil.getCurrentUser().getNeoId() != 47133909 && PortalUtil.getCurrentUser().getNeoId() != 20166 && PortalUtil.getCurrentUser().getNeoId() != 106214471 && !isDiretoria;
						if (flag || bloquear)
						{
							stringBuilder.append(" <button class=\"btn btn-default btn-xs dropdown-toggle\" type=\"button\" id=\"dropdownMenu\"  data-toggle=\"dropdown\" aria-expanded=\"false\" disabled=\"true\"> ");

						}
						else
						{
							stringBuilder.append(" <button class=\"btn btn-default btn-xs dropdown-toggle\" type=\"button\" id=\"dropdownMenu\"  data-toggle=\"dropdown\" aria-expanded=\"false\" > ");
						}
						stringBuilder.append(" <span class=\"glyphicon glyphicon-sort-by-attributes\" id=\"dropdown_title\"></span><span class=\"caret\"></span> ");
						stringBuilder.append(" </button> ");
						stringBuilder.append(" <ul class=\"dropdown-menu pull-center\" role=\"menu\"> ");
						for (int i = 1; i <= Integer.parseInt(vo.getTotal()); i++)
						{
							if (flag || bloquear)
							{
								stringBuilder.append(" <li class=\"disabled\"> ");

							}
							else
							{
								stringBuilder.append(" <li> ");
							}

							stringBuilder.append("<a id=\"dpPosicao\" onclick=\"javascript:showPosicao('" + tarefasUsuarioVO.getCodigoTarefa() + "','" + i + "','" + tarefasUsuarioVO.getUsuarioCode() + "',this,'" + vo.getCodigoUsuario() + "','" + model + "')\" href=\"#\">");
							stringBuilder.append(" " + i + "</a></li>");
						}
						stringBuilder.append("  </ul> ");
						stringBuilder.append(" </div> ");

						if (tarefasUsuarioVO.getTipoTarefa() != null && tarefasUsuarioVO.getTipoTarefa().contains("D") && (vo.getUsuario().contains(PortalUtil.getCurrentUser().getFullName()) || !flag))
						{
							stringBuilder.append(" 	<span class=\"badge progress-bar-success\"  title=\"Desenvolvimento\" onclick=\"javascript:showTipo('" + tarefasUsuarioVO.getCodigoTarefa() + "','" + tarefasUsuarioVO.getTipoTarefa() + "',this,'" + vo.getCodigoUsuario() + "','" + tarefasUsuarioVO.getUsuarioCode() + "','" + model + "')\" style=\"cursor: pointer;\">");
							stringBuilder.append(" " + tarefasUsuarioVO.getTipoTarefa() + "</span> ");
						}
						else if (tarefasUsuarioVO.getTipoTarefa() != null && tarefasUsuarioVO.getTipoTarefa().contains("D"))
						{
							stringBuilder.append(" 	<span class=\"badge progress-bar-success\"  title=\"Desenvolvimento\" >");
							stringBuilder.append(" " + tarefasUsuarioVO.getTipoTarefa() + "</span> ");
						}

						else if (tarefasUsuarioVO.getTipoTarefa() != null && tarefasUsuarioVO.getTipoTarefa().contains("S") && (vo.getUsuario().contains(PortalUtil.getCurrentUser().getFullName()) || !flag))
						{
							stringBuilder.append(" 	<span class=\"badge progress-bar-warning\"  title=\"Suporte\" onclick=\"javascript:showTipo('" + tarefasUsuarioVO.getCodigoTarefa() + "','" + tarefasUsuarioVO.getTipoTarefa() + "',this,'" + vo.getCodigoUsuario() + "','" + tarefasUsuarioVO.getUsuarioCode() + "','" + model + "')\" style=\"cursor: pointer;\" >");
							stringBuilder.append(" " + tarefasUsuarioVO.getTipoTarefa() + "</span> ");
						}
						else if (tarefasUsuarioVO.getTipoTarefa() != null && tarefasUsuarioVO.getTipoTarefa().contains("S"))
						{
							stringBuilder.append(" 	<span class=\"badge progress-bar-warning\"  title=\"Suporte\">");
							stringBuilder.append(" " + tarefasUsuarioVO.getTipoTarefa() + "</span> ");
						}

						else if (tarefasUsuarioVO.getTipoTarefa() == null && (vo.getUsuario().contains(PortalUtil.getCurrentUser().getFullName()) || !flag))
						{
							stringBuilder.append(" 	<span class=\"badge progress-bar-info\"  title=\"Indefinido\" onclick=\"javascript:showTipo('" + tarefasUsuarioVO.getCodigoTarefa() + "','" + tarefasUsuarioVO.getTipoTarefa() + "',this,'" + vo.getCodigoUsuario() + "','" + tarefasUsuarioVO.getUsuarioCode() + "','" + model + "')\" style=\"cursor: pointer;\">");
							stringBuilder.append(" I </span> ");
						}
						else if (tarefasUsuarioVO.getTipoTarefa() == null)
						{
							stringBuilder.append(" 	<span class=\"badge progress-bar-info\"  title=\"Indefinido\" >");
							stringBuilder.append(" I </span> ");
						}

						if (tarefasUsuarioVO.getSituacaoTarefa() != null && tarefasUsuarioVO.getSituacaoTarefa().contains("T") && (vo.getUsuario().contains(PortalUtil.getCurrentUser().getFullName()) || !flag))
						{
							stringBuilder.append(" 	<span class=\"badge progress-bar-danger\"  title=\"Trabalhando\" onclick=\"javascript:showSituacao('" + tarefasUsuarioVO.getCodigoTarefa() + "','" + tarefasUsuarioVO.getSituacaoTarefa() + "',this,'" + vo.getCodigoUsuario() + "','" + tarefasUsuarioVO.getUsuarioCode() + "','" + model + "')\" style=\"cursor: pointer;\">");
							stringBuilder.append(" " + tarefasUsuarioVO.getSituacaoTarefa() + "</span> ");
						}
						else if (tarefasUsuarioVO.getSituacaoTarefa() != null && tarefasUsuarioVO.getSituacaoTarefa().contains("T"))
						{
							stringBuilder.append(" 	<span class=\"badge progress-bar-danger\"  title=\"Trabalhando\" >");
							stringBuilder.append(" " + tarefasUsuarioVO.getSituacaoTarefa() + "</span> ");
						}

						else if (tarefasUsuarioVO.getSituacaoTarefa() != null && tarefasUsuarioVO.getSituacaoTarefa().contains("E") && (vo.getUsuario().contains(PortalUtil.getCurrentUser().getFullName()) || !flag))
						{
							stringBuilder.append(" 	<span class=\"badge progress-bar-info\"  title=\"Em espera\" onclick=\"javascript:showSituacao('" + tarefasUsuarioVO.getCodigoTarefa() + "','" + tarefasUsuarioVO.getSituacaoTarefa() + "',this,'" + vo.getCodigoUsuario() + "','" + tarefasUsuarioVO.getUsuarioCode() + "','" + model + "')\" style=\"cursor: pointer;\">");
							stringBuilder.append(" " + tarefasUsuarioVO.getSituacaoTarefa() + "</span> ");
						}
						else if (tarefasUsuarioVO.getSituacaoTarefa() != null && tarefasUsuarioVO.getSituacaoTarefa().contains("E"))
						{
							stringBuilder.append(" 	<span class=\"badge progress-bar-info\"  title=\"Em espera\" >");
							stringBuilder.append(" " + tarefasUsuarioVO.getSituacaoTarefa() + "</span> ");
						}

						else if (tarefasUsuarioVO.getSituacaoTarefa() == null && (vo.getUsuario().contains(PortalUtil.getCurrentUser().getFullName()) || !flag))
						{
							stringBuilder.append(" 	<span class=\"badge progress-bar-info\"  title=\"Indefinido\" onclick=\"javascript:showSituacao('" + tarefasUsuarioVO.getCodigoTarefa() + "','" + tarefasUsuarioVO.getSituacaoTarefa() + "',this,'" + vo.getCodigoUsuario() + "','" + tarefasUsuarioVO.getUsuarioCode() + "','" + model + "')\" style=\"cursor: pointer;\">");
							stringBuilder.append(" I </span> ");
						}
						else if (tarefasUsuarioVO.getSituacaoTarefa() == null)
						{
							stringBuilder.append(" 	<span class=\"badge progress-bar-info\"  title=\"Indefinido\" >");
							stringBuilder.append(" I </span> ");
						}

						stringBuilder.append(" </li> ");

						count++;
					}
					stringBuilder.append(" </ul> ");
					stringBuilder.append(" <div class=\"panel-footer\"></div></div>");

					stringBuilder.append(" </div> ");
					stringBuilder.append(" </td> ");

				}
				stringBuilder.append(" </tr> ");
				stringBuilder.append(" </table> ");

				//stringBuilder.append(" </div> ");
			}
		}
		catch (Exception e)
		{
			log.error("##### ERRO PESQUISA POR TAREFAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			e.printStackTrace();
		}
		finally
		{
			out.print(stringBuilder.toString());

			log.warn("##### FINALIZAR  PESQUISA POR TAREFAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}

	}
	
	/**
	 * Total tarefas realizada por usuário
	 * @param code
	 * @return mapa com tarefas número de tarefas realizadas por usuário
	 */
	private Map<Long, Long> getTotaisGeraisUsuario(String code) {
	    Map<Long, Long> resultado = new HashMap<Long, Long>();
		
		StringBuilder sql = new StringBuilder();
		
		Connection conn = PersistEngine.getConnection("FUSIONPROD");
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		
		try{
			sql.append("select distinct us.neoid as codigoUsuario, COUNT(DISTINCT p.code) AS Contagem ");
			sql.append("from wfProcess p with(nolock) ");
			sql.append("inner join activity a with(nolock) on p.neoId = a.process_neoId  ");
			sql.append("inner join task t with(nolock) on t.activity_neoid = a.neoid  ");
			sql.append("inner join neouser usr with(nolock) on usr.neoid = p.requester_neoid  ");
			sql.append("inner join neouser us with(nolock) on us.neoid = t.user_neoId  ");
			sql.append(" inner join neogroup g with(nolock) on g.neoId = us.group_neoId  ");
			sql.append("inner join neogroup gu with(nolock) on gu.neoId = usr.group_neoId   ");
			sql.append("inner join securityentity se with(nolock) on se.neoid = g.neoid  ");
			sql.append("inner join securityentity see with(nolock) on see.neoid = us.neoid  ");
			sql.append("inner join securityentity seu with(nolock) on seu.neoid = gu.neoid  ");
			sql.append("inner join d_tarefa ta with(nolock) on p.entity_neoid = ta.neoid  ");
			sql.append("inner join d_TarefaRegistroAtividades tas with(nolock)   ");
			sql.append("on tas.neoid = (select min(tar.registroAtividades_neoid) from d_tarefa_registroAtividades tar with(nolock) where ta.neoid = tar.d_tarefa_neoid )  ");
			sql.append(" where us.neoId IN ( " + code + " )");
			sql.append(" and p.processState = 1 ");
			sql.append(" and  p.model_neoId in (select pmaux.neoId from ProcessModel pmaux where pmaux.name like '%G001 - Tarefa Simples%') ");
			sql.append(" and p.saved = 1  ");
			sql.append("   and t.finishDate is not null  ");
			sql.append("  and t.startDate is not null ");
			sql.append("  and p.startDate is not null ");
			sql.append("  and p.finishDate is not null  ");
			sql.append(" and p.finishDate is not null ");
			sql.append(" and YEAR (p.finishDate) = YEAR (GETDATE()) ");
			sql.append("  and MONTH(p.finishDate) = MONTH(GETDATE()) ");
			sql.append(" GROUP BY us.neoid ,see.code , us.fullName  ");

			pstm = conn.prepareStatement(sql.toString());

			rs = pstm.executeQuery();
			
			
			while (rs.next()){
			    
			    Long key = rs.getLong("codigoUsuario");
			    Long value = rs.getLong("Contagem");
			    
			    resultado.put(key, value);
			    
			}
			
			return resultado;
			
		}catch(SQLException e){
		    e.printStackTrace();
		}finally{
		    OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
		
		
		return resultado;
	}
	
	/**
	 * 
	 * @param idGrupo String
	 * @return Mapa com código do usuário e total de tarefas executadas no mês
	 */
	private Map<Long, Long> getTotaisGerais(String idGrupo) {
	Map<Long, Long> resultado = new HashMap<Long, Long>();
	
	StringBuilder sql = new StringBuilder();
	
	Connection conn = PersistEngine.getConnection("FUSIONPROD");
	PreparedStatement pstm = null;
	ResultSet rs = null;
	
	
	try{
		sql.append("select distinct us.neoid as codigoUsuario, COUNT(DISTINCT p.code) AS Contagem ");
		sql.append("from wfProcess p with(nolock) ");
		sql.append("inner join activity a with(nolock) on p.neoId = a.process_neoId  ");
		sql.append("inner join task t with(nolock) on t.activity_neoid = a.neoid  ");
		sql.append("inner join neouser usr with(nolock) on usr.neoid = p.requester_neoid  ");
		sql.append("inner join neouser us with(nolock) on us.neoid = t.user_neoId  ");
		sql.append(" inner join neogroup g with(nolock) on g.neoId = us.group_neoId  ");
		sql.append("inner join neogroup gu with(nolock) on gu.neoId = usr.group_neoId   ");
		sql.append("inner join securityentity se with(nolock) on se.neoid = g.neoid  ");
		sql.append("inner join securityentity see with(nolock) on see.neoid = us.neoid  ");
		sql.append("inner join securityentity seu with(nolock) on seu.neoid = gu.neoid  ");
		sql.append("inner join d_tarefa ta with(nolock) on p.entity_neoid = ta.neoid  ");
		sql.append("inner join d_TarefaRegistroAtividades tas with(nolock)   ");
		sql.append("on tas.neoid = (select min(tar.registroAtividades_neoid) from d_tarefa_registroAtividades tar with(nolock) where ta.neoid = tar.d_tarefa_neoid )  ");
		sql.append(" where g.neoId IN ( " + idGrupo + " )");
		sql.append(" and p.processState = 1 ");
		sql.append(" and  p.model_neoId in (select pmaux.neoId from ProcessModel pmaux where pmaux.name like '%G001 - Tarefa Simples%') ");
		sql.append(" and p.saved = 1  ");
		sql.append("   and t.finishDate is not null  ");
		sql.append("  and t.startDate is not null ");
		sql.append("  and p.startDate is not null ");
		sql.append("  and p.finishDate is not null  ");
		sql.append(" and p.finishDate is not null ");
		sql.append(" and YEAR (p.finishDate) = YEAR (GETDATE()) ");
		sql.append("  and MONTH(p.finishDate) = MONTH(GETDATE()) ");
		sql.append(" GROUP BY us.neoid ,see.code , us.fullName  ");

		pstm = conn.prepareStatement(sql.toString());

		rs = pstm.executeQuery();
		
		
		while (rs.next()){
		    
		    Long key = rs.getLong("codigoUsuario");
		    Long value = rs.getLong("Contagem");
		    
		    resultado.put(key, value);
		    
		}
		
		return resultado;
		
	}catch(SQLException e){
	    e.printStackTrace();
	}finally{
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
	
	
	return resultado;
    }

	@SuppressWarnings("finally")
	private List<TarefasGrupoUsuarioVO> pesquisaTodasTarefas(String group, String model)
	{

		Connection conn = PersistEngine.getConnection("FUSIONPROD");
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		List<TarefasVO> tarefasVOs = null;
		List<TarefasUsuarioVO> tarefasUsuarioVOs = null;
		List<TarefasGrupoUsuarioVO> tarefasGrupoUsuarioVOs = null;
		log.warn("##### INICIAR PESQUISA POR TAREFAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		try{
		    
		    	String replace = "se.code";
		    
			if (group != null && !group.isEmpty() && group.contains("sistemas"))
			{
				group = "18317794";
			}
			else if (group != null && !group.isEmpty() && group.contains("infra"))
			{
				group = "18317811";
			}else if (group != null && !group.isEmpty() && group.contains("nexti"))
			{
				group = "639014270";
			}
			
			if (group.equals("18317794")){
			    group += ",638564840";
			    replace = "Replace(se.code, 'SupervisaoSustentacaoSistemas', 'Coordenação de Sistemas')";
			}
			
			if (group.equals("647233104")){
			    group += ",638338425,638347864";
			    replace = "Replace(Replace(Replace(se.code, 'ContabilidadeOrsitec', 'Consultoria'),'CoordenacaoOrsitec','Consultoria'),'Sins','Consultoria')";
			    System.out.println("Teste");
			}
			
			String where = "";
			sql.append(" select distinct p.neoId as wfprocess, p.code, us.neoid as codigoUsuario,see.code as usuario, ");
			sql.append(" us.fullName,ta.titulo,tas.descricao,p.startDate,t.startDate as ultimaAlteracao,ta.prazo, "+replace+" as grupo, usr.fullName as solicitante, ");
			sql.append(" gu.neoid as codigoGrupoSolicitante,seu.code as grupoSolicitante, usr.neoid as codigoSolicitante ");
			sql.append(" from wfProcess p with(nolock) ");
			sql.append(" inner join activity a with(nolock) on p.neoId = a.process_neoId ");
			sql.append(" inner join task t with(nolock) on t.activity_neoid = a.neoid ");
			sql.append(" inner join neouser usr with(nolock) on usr.neoid = p.requester_neoid  ");
			sql.append(" inner join neouser us with(nolock) on us.neoid = t.user_neoId ");
			sql.append(" inner join neogroup g with(nolock) on g.neoId = us.group_neoId ");
			sql.append(" left join neogroup gu with(nolock) on gu.neoId = usr.group_neoId  ");
			sql.append(" inner join securityentity se with(nolock) on se.neoid = g.neoid ");
			sql.append(" inner join securityentity see with(nolock) on see.neoid = us.neoid ");
			sql.append(" left join securityentity seu with(nolock) on seu.neoid = gu.neoid  ");
			sql.append(" inner join d_tarefa ta with(nolock) on p.entity_neoid = ta.neoid ");
			sql.append(" inner join d_TarefaRegistroAtividades tas with(nolock)  ");
			sql.append(" on tas.neoid = (select min(tar.registroAtividades_neoid) from d_tarefa_registroAtividades tar with(nolock) where ta.neoid = tar.d_tarefa_neoid ) ");
			sql.append(" where g.neoId IN ( " + group + " )");
			sql.append(" and p.processState = 0 ");
			
			if (NeoUtils.safeIsNotNull(model) && !model.isEmpty() && model.equals("18888"))
			{
				where = " and  p.model_neoId in (select pmaux.neoId from ProcessModel pmaux where pmaux.name like '%G001 - Tarefa Simples%') ";
			}
			else if (NeoUtils.safeIsNotNull(model) && !model.isEmpty() && model.equals("186043073"))
			{
				where = " and  p.model_neoId in (select pmaux.neoId from ProcessModel pmaux where pmaux.name like '%C034 - Solicitar Chip GPRS para Posto%') ";
			}
			
			sql.append(where);
			sql.append(" and p.saved = 1 ");
			sql.append(" and t.finishDate is null ");
			sql.append(" and p.startDate is not null ");
			sql.append(" and t.startDate is not null ");
			sql.append(" order by "+replace+",us.fullName asc ");

			pstm = conn.prepareStatement(sql.toString());
			//pstm.setLong(1, Long.parseLong(wfProcess));
			//pstm.setLong(1, Long.parseLong(model));

			rs = pstm.executeQuery();
			tarefasVOs = new ArrayList<TarefasVO>();
			tarefasUsuarioVOs = new ArrayList<TarefasUsuarioVO>();
			tarefasGrupoUsuarioVOs = new ArrayList<TarefasGrupoUsuarioVO>();
			TarefasVO tarefasVO = null;
			TarefasUsuarioVO tarefasUsuarioVO = null;
			TarefasGrupoUsuarioVO tarefasGrupoUsuarioVO = null;
			int cont = 0;
			int total = 0;
			int totalGrupo = 0;
			String nomeAtual = "";
			String grupoAtual = "";
			String nomeUsuario = "";
			Long neoIdUsuario = 0L;
			HtmlToText htmlToText = new HtmlToText();
			
			//TODO COPIAR PARA METODO COM FILTROS 
			/** Tras totais  executados mensais */
			
			Map<Long, Long> mapTotaisTarefas = this.getTotaisGerais(group);
			
			Long totalExecMesGrupo = 0L;
						
			for (Long key : mapTotaisTarefas.keySet()){
			    Long totalMes = mapTotaisTarefas.get(key);
			    totalExecMesGrupo += totalMes;
			}
			
			
			while (rs.next())
			{
			    
			    	//TODO FAZER ADIÇÃO AO GRUPO CORRETO
				tarefasVO = new TarefasVO();

				if (nomeAtual != null && !nomeAtual.equals(rs.getString("fullName")) && cont > 0)
				{

					List<NeoObject> listaTarefas = retornaObjects(nomeUsuario, Long.parseLong(model));
					verificaTarefas(listaTarefas, tarefasVOs, nomeUsuario);
					tarefasUsuarioVO = new TarefasUsuarioVO();
					tarefasUsuarioVO.setCodigoUsuario(neoIdUsuario);
					tarefasUsuarioVO.setUsuario(nomeAtual);
					tarefasUsuarioVO.setTotal(String.valueOf(total));
					
					Long totalExecMesUsuario = mapTotaisTarefas.get(tarefasUsuarioVO.getCodigoUsuario());
					if (totalExecMesUsuario == null){
					    totalExecMesUsuario = 0L;
					}
					tarefasUsuarioVO.setTotalExecMes(totalExecMesUsuario);
					
					tarefasUsuarioVO.setTarefas(tarefasVOs);
					tarefasUsuarioVOs.add(tarefasUsuarioVO);
					if (grupoAtual != null && !grupoAtual.equals(rs.getString("grupo")) && cont > 0)
					{
						tarefasGrupoUsuarioVO = new TarefasGrupoUsuarioVO();
						tarefasGrupoUsuarioVO.setGrupo(grupoAtual);
						tarefasGrupoUsuarioVO.setTotal(totalGrupo);
						if (grupoAtual.contains("Sistemas")){
							tarefasGrupoUsuarioVO.setId("sistemas");
						}else if (grupoAtual.contains("Consultoria")){
						    tarefasGrupoUsuarioVO.setId("Consultoria");
						}
						tarefasGrupoUsuarioVO.setTarefas(tarefasUsuarioVOs);
						tarefasGrupoUsuarioVOs.add(tarefasGrupoUsuarioVO);
						tarefasUsuarioVOs = new ArrayList<TarefasUsuarioVO>();
						totalGrupo = 0;
					}
					tarefasVOs = new ArrayList<TarefasVO>();
					total = 0;

				}
				neoIdUsuario = rs.getLong("codigoUsuario");
				nomeUsuario = rs.getString("usuario");				
				nomeAtual = rs.getString("fullName");
				tarefasVO.setWfProcess(rs.getLong("wfprocess"));
				tarefasVO.setCodigoTarefa(rs.getLong("code"));
				tarefasVO.setNomeCompleto(nomeAtual);
				tarefasVO.setUsuarioCode(nomeUsuario);
				tarefasVO.setTituloTarefa(htmlToText.parse(rs.getString("titulo")));
				tarefasVO.setModelo(Long.parseLong(model));
				tarefasVO.setSolicitante(rs.getString("solicitante"));
				tarefasVO.setCodigoSolicitante(rs.getLong("codigoSolicitante"));
				tarefasVO.setGrupoSolicitante(rs.getString("grupoSolicitante"));
				tarefasVO.setCodigoGrupoSolicitante(rs.getLong("codigoGrupoSolicitante"));

				GregorianCalendar dataAux = new GregorianCalendar();
				dataAux.setTime(rs.getTimestamp("startDate"));
				tarefasVO.setDataAberturaTarefa(NeoDateUtils.safeDateFormat(dataAux, "dd/MM/yyyy"));
				GregorianCalendar dataAux2 = new GregorianCalendar();
				dataAux2.setTime(rs.getTimestamp("prazo"));
				tarefasVO.setPrazoTarefa(NeoDateUtils.safeDateFormat(dataAux2, "dd/MM/yyyy HH:mm"));
				GregorianCalendar dataAux3 = new GregorianCalendar();
				dataAux3.setTime(rs.getTimestamp("ultimaAlteracao"));
				tarefasVO.setDataUltimaAlteracao(NeoDateUtils.safeDateFormat(dataAux3, "dd/MM/yyyy"));
				tarefasVO.setStartDate(dataAux.getTime());
				grupoAtual = rs.getString("grupo");
				
				//Tratativa para grupos de consultores
				
				if (grupoAtual.contains("Sins") || grupoAtual.contains("Orsitec")){
				    grupoAtual = "Consultoria";
				}
				
				String descricao = rs.getString("descricao");
				descricao = htmlToText.parse(descricao);
				descricao = descricao.replaceAll("\\<.*?>", "");
				descricao = descricao.replaceAll("&nbsp;", "");
				descricao = descricao.replaceAll("&amp;", "");
				descricao = descricao.replaceAll("\"", "");
				descricao = descricao.replaceAll("'", "");
				descricao = descricao.replaceAll("=", "");
				tarefasVO.setDescricaoTarefa(descricao + "\n Prazo:" + tarefasVO.getPrazoTarefa());

				tarefasVOs.add(tarefasVO);
				cont++;
				total++;
				totalGrupo++;

			}
			if (cont > 0)
			{
				List<NeoObject> listaTarefas = retornaObjects(nomeUsuario, Long.parseLong(model));
				verificaTarefas(listaTarefas, tarefasVOs, nomeUsuario);
				tarefasUsuarioVO = new TarefasUsuarioVO();
				tarefasUsuarioVO.setCodigoUsuario(neoIdUsuario);
				tarefasUsuarioVO.setUsuario(nomeAtual);
				/** Tras totais por usuário */
				
				Long totalExecMesUsuario = mapTotaisTarefas.get(tarefasUsuarioVO.getCodigoUsuario());
				if (totalExecMesUsuario == null){
				    totalExecMesUsuario = 0L;
				}
				tarefasUsuarioVO.setTotalExecMes(totalExecMesUsuario);
				tarefasUsuarioVO.setTotal(String.valueOf(total));
				tarefasUsuarioVO.setTarefas(tarefasVOs);
				tarefasUsuarioVOs.add(tarefasUsuarioVO);

				tarefasGrupoUsuarioVO = new TarefasGrupoUsuarioVO();
				tarefasGrupoUsuarioVO.setGrupo(grupoAtual);
				tarefasGrupoUsuarioVO.setTotal(totalGrupo);
				
				/** Total de tarefas executadas mês */
				tarefasGrupoUsuarioVO.setTotalExecMes(totalExecMesGrupo);
				if (grupoAtual.contains("Infra")){
					tarefasGrupoUsuarioVO.setId("infra");
				}
				if (grupoAtual.contains("Sistemas")){
					tarefasGrupoUsuarioVO.setId("sistemas");
				}
				if (grupoAtual.contains("Next")){
				    tarefasGrupoUsuarioVO.setId("nexti");
				}
				
				if(grupoAtual.contains("Consultoria")){
				    tarefasGrupoUsuarioVO.setId("Consultoria");
				}
				
				tarefasGrupoUsuarioVO.setTarefas(tarefasUsuarioVOs);
				tarefasGrupoUsuarioVOs.add(tarefasGrupoUsuarioVO);

			}
			log.warn("##### EXECUTAR PESQUISA POR TAREFAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		}
		catch (Exception e)
		{
			log.error("##### ERRO PESQUISA POR TAREFAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			e.printStackTrace();
		}
		finally
		{

			try
			{

				OrsegupsUtils.closeConnection(conn, pstm, rs);
				int countAux = 0;
				for (TarefasGrupoUsuarioVO grupoUsuarioVO : tarefasGrupoUsuarioVOs)
				{
					for (TarefasUsuarioVO tarefasUsuarioVO : grupoUsuarioVO.getTarefas())
					{

						for (TarefasVO tarefasVO : tarefasUsuarioVO.getTarefas())
						{
							if (tarefasVO.getCodigoTarefa() != null)
							{

								QLGroupFilter groupFilter = new QLGroupFilter("AND");
								groupFilter.addFilter(new QLEqualsFilter("tarefa", tarefasVO.getCodigoTarefa().toString()));
								groupFilter.addFilter(new QLEqualsFilter("modelo", Long.parseLong(model)));
								List<NeoObject> tarefasObj = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICONTROLETAREFAS"), groupFilter);
								if (tarefasObj != null && !tarefasObj.isEmpty())
								{
									EntityWrapper entityWrapper = new EntityWrapper(tarefasObj.get(0));
									tarefasVO.setPosicaoTarefa((Long) entityWrapper.findValue("posicao"));
									tarefasVO.setTipoTarefa((String) entityWrapper.findValue("tipo"));
									tarefasVO.setSituacaoTarefa((String) entityWrapper.findValue("situacao"));
									tarefasVO.setModelo((Long) entityWrapper.findValue("modelo"));
								}
							}
						}
						Collections.sort((List<TarefasVO>) tarefasUsuarioVO.getTarefas());
					}

				}

			}
			catch (Exception e)
			{
				log.error("##### ERRO PESQUISA POR TAREFAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
				e.printStackTrace();
			}
			log.warn("##### FINALIZAR  PESQUISA POR TAREFAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			return tarefasGrupoUsuarioVOs;

		}

	}

	private static List<NeoObject> retornaObjects(String code, Long modelo)
	{
		List<NeoObject> tarefasObjs = null;
		try
		{
			if (code != null && !code.isEmpty())
			{
				NeoUser usuario = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", code));
				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				groupFilter.addFilter(new QLEqualsFilter("usuario", usuario));
				groupFilter.addFilter(new QLEqualsFilter("modelo", modelo));
				tarefasObjs = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICONTROLETAREFAS"), groupFilter, -1, -1, " startDate asc ");

				if (tarefasObjs != null && !tarefasObjs.isEmpty())
				{
					Long count = 1L;
					for (NeoObject neoObject2 : tarefasObjs)
					{
						NeoObject neoObject = (NeoObject) neoObject2;

						EntityWrapper tarefaWrapper = new EntityWrapper(neoObject);
						tarefaWrapper.findField("posicao").setValue(count);

						PersistEngine.persist(neoObject);

						count++;
					}

				}
				else
				{
					QLGroupFilter groupFilterAux = new QLGroupFilter("AND");
					groupFilterAux.addFilter(new QLEqualsFilter("usuario", usuario));
					tarefasObjs = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICONTROLETAREFAS"), groupFilterAux, -1, -1, " posicao asc ");
					if (tarefasObjs != null && !tarefasObjs.isEmpty())
					{
						Long count = 1L;
						for (NeoObject neoObjectAux : tarefasObjs)
						{
							NeoObject neoObject = (NeoObject) neoObjectAux;

							EntityWrapper tarefaWrapper = new EntityWrapper(neoObject);
							if (tarefaWrapper.findValue("modelo") == null)
							{
								if (tarefaWrapper.findValue("posicao") == null)
								{
									tarefaWrapper.findField("posicao").setValue(count);
									count++;
								}
								tarefaWrapper.findField("modelo").setValue(modelo);

								PersistEngine.persist(neoObject);

							}

						}
						QLGroupFilter groupFilterFinal = new QLGroupFilter("AND");
						groupFilterFinal.addFilter(new QLEqualsFilter("usuario", usuario));
						groupFilterFinal.addFilter(new QLEqualsFilter("modelo", modelo));
						tarefasObjs = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICONTROLETAREFAS"), groupFilterFinal, -1, -1, " posicao asc ");

					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO AO PESQUISAR TAREFAS : " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));

		}
		finally
		{
			return tarefasObjs;
		}

	}

	private static void verificaTarefas(List<NeoObject> tarefasFila, List<TarefasVO> tarefas, String neoIdUsuario)
	{
		List<NeoObject> tarefasObjs = null;
		List<String> tarefasStr = new ArrayList<String>();
		try
		{
			if (tarefasFila != null && !tarefasFila.isEmpty())
			{

				for (NeoObject tarefaVO : tarefasFila)
				{
					EntityWrapper entityWrapper = new EntityWrapper(tarefaVO);
					if (entityWrapper != null)
					{
						if ((String) entityWrapper.findValue("tarefa") != null)
						{

							tarefasStr.add((String) entityWrapper.findValue("tarefa"));
						}
					}

				}
			}
			if (tarefasFila != null && !tarefasFila.isEmpty() && tarefas != null && !tarefas.isEmpty())
			{
				Collection<NeoObject> lista = new ArrayList<NeoObject>();
				for (NeoObject tarefaVO : tarefasFila)
				{
					boolean flag = false;
					EntityWrapper entityWrapper = new EntityWrapper(tarefaVO);
					if (entityWrapper != null)
					{
						for (TarefasVO tarefaVOs : tarefas)
						{
							if (((String) entityWrapper.findValue("tarefa")).contains(tarefaVOs.getCodigoTarefa().toString()))
							{
								flag = true;

							}
						}
						if (!flag)
							lista.add(tarefaVO);
					}
				}
				PersistEngine.removeObjects(lista);
			}
			if (tarefas != null && !tarefas.isEmpty() && neoIdUsuario != null && !neoIdUsuario.isEmpty())
			{
				NeoUser usuarioResponsavel = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", neoIdUsuario));
				if (usuarioResponsavel != null)
				{
					Long tamanhaLista = 0L;
					for (TarefasVO tarefaVO : tarefas)
					{
						if (tarefasStr == null || tarefasStr.isEmpty() || (usuarioResponsavel != null && !usuarioResponsavel.getCode().isEmpty() && tarefasStr != null && !tarefasStr.isEmpty() && tarefaVO.getModelo() != null && !tarefasStr.contains(tarefaVO.getCodigoTarefa().toString())))
						{

							tarefasStr.add(tarefaVO.getCodigoTarefa().toString());

							tamanhaLista = Long.valueOf(tarefasStr.size());
							NeoObject noPS = AdapterUtils.createNewEntityInstance("TICONTROLETAREFAS");

							EntityWrapper psWrapper = new EntityWrapper(noPS);
							
							GregorianCalendar dataTeste = new GregorianCalendar();
							dataTeste.setTime(tarefaVO.getStartDate());
							
							psWrapper.findField("posicao").setValue(tamanhaLista);
							psWrapper.findField("tarefa").setValue(String.valueOf(tarefaVO.getCodigoTarefa()));
							psWrapper.findField("usuario").setValue(usuarioResponsavel);
							psWrapper.findField("tipo").setValue("S");
							psWrapper.findField("situacao").setValue("E");
							psWrapper.findField("modelo").setValue(tarefaVO.getModelo());
							psWrapper.setValue("startDate", dataTeste);
							PersistEngine.persist(noPS);
							PersistEngine.getEntityManager().flush();

						}
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO AO PESQUISAR TAREFAS : " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));

		}
		finally
		{
			log.warn("##### FINALIZAR PESQUISAR TAREFAS  - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}

	}

	private void montaTabelaChipGPRSUsuario(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{

		String wfProcess = request.getParameter("wfProcess");
		String model = request.getParameter("model");
		String code = request.getParameter("code");
		wfProcess = "18317811";
		model = "186043073";
		StringBuilder stringBuilder = null;

		try
		{
			TarefasUsuarioVO tarefasUsuarioVOAux = (TarefasUsuarioVO) pesquisaTarefas(wfProcess, model, code);
			if (tarefasUsuarioVOAux != null)
			{
				String executor = "";
				NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "TIGESTAOTAREFASCHIP"));
				if (obj != null && obj.getAllUsers() != null && !obj.getAllUsers().isEmpty())
				{
					for (NeoUser usuarioTarefa : obj.getUsers())
					{
						executor += usuarioTarefa.getNeoId();
					}
				}

				if (tarefasUsuarioVOAux != null && tarefasUsuarioVOAux.getCodigoUsuario() == 0L)
				{
					tarefasUsuarioVOAux.setUsuario(" ");
					tarefasUsuarioVOAux.setTotal("0");

				}

				stringBuilder = new StringBuilder();
				stringBuilder.append(" <div class=\"panel-heading\" > ");

				if (executor != null && !executor.isEmpty() && executor.contains(code))
				{
					stringBuilder.append(" <div align=\"left\"> ");
					stringBuilder.append(" <div class=\"btn-group btn-group-xs\" role=\"group\" aria-label=\"Extra-small button group\"> ");
					stringBuilder.append("  <button type=\"button\" class=\"btn btn-default\" title=\"Tarefa Simples\"  onclick=\"javascript:showAtualizaTabela('" + code + "','" + code + "')\"><span class=\"glyphicon glyphicon-step-backward\" ></span></button> ");
					stringBuilder.append("  <button type=\"button\" class=\"btn btn-default\" title=\"CHIP GPRS\"  onclick=\"javascript:showAtualizaTabelaChipGPRS('" + code + "','" + code + "')\"><span class=\"glyphicon glyphicon-step-forward\" ></span></button> ");
					stringBuilder.append(" </div> ");
					stringBuilder.append(" </div> ");
				}

				stringBuilder.append(" <div align=\"right\"> ");
				stringBuilder.append(" <span  class=\"badge progress-bar-danger\" style=\"font-family: verdana; font-size: 9px;color:white;\">" + tarefasUsuarioVOAux.getTotal() + "</span></div> ");
				stringBuilder.append(" <span class=\"glyphicon glyphicon-user\"  ></span> ");
				stringBuilder.append(" <span style=\"font-family: verdana; font-size: 9px;\">" + tarefasUsuarioVOAux.getUsuario() + "</span>");
				stringBuilder.append(" </div> ");
				int count = 0;
				stringBuilder.append(" <ul id=\"tarefa-list\" name=\"tarefa-list\"  class=\"list-group\" style=\"width: 100%; font-family: verdana; font-size: 10px;\"> ");
				for (TarefasVO tarefasUsuarioVO : tarefasUsuarioVOAux.getTarefas())
				{

					//					if (count < 5)
					//					{
					//						stringBuilder.append(" <li class=\"list-group-item list-group-item-danger\" data-type=" + tarefasUsuarioVO.getCodigoTarefa() + "> ");
					//					}
					//					if (count >= 5 && count <= 9)
					//					{
					//						stringBuilder.append(" <li class=\"list-group-item list-group-item-warning\" data-type=" + tarefasUsuarioVO.getCodigoTarefa() + "> ");
					//					}
					//					if (count > 9)
					//					{
					//						stringBuilder.append(" <li class=\"list-group-item list-group-item-success\" data-type=" + tarefasUsuarioVO.getCodigoTarefa() + "> ");
					//					}
					stringBuilder.append(" <li class=\"list-group-item list-group-item-default\" data-type=" + tarefasUsuarioVO.getCodigoTarefa() + "> ");
					Boolean isDiretoria = ((PortalUtil.getCurrentUser().getGroup().getCode().contains("Diretoria")) || (PortalUtil.getCurrentUser().getGroup().getCode().contains("Presidência")));

					String tarefaToolTip = tarefasUsuarioVO.getCodigoTarefa() + " - " + tarefasUsuarioVO.getTituloTarefa();
					stringBuilder.append(" " + tarefasUsuarioVO.getPosicaoTarefa() + "º - " + tarefasUsuarioVO.getCodigoTarefa() + "</br>  Título: " + tarefasUsuarioVO.getTituloTarefa());
					stringBuilder.append("</br> Solicitante: " + tarefasUsuarioVO.getSolicitante() + "</br>  Abertura: " + tarefasUsuarioVO.getDataAberturaTarefa() + "<br> Última Alteração: " + tarefasUsuarioVO.getDataUltimaAlteracao() + "<br>");
					stringBuilder.append(" <button type=\"button\" class=\"btn btn-default btn-xs\" data-container=\"body\"");
					stringBuilder.append(" data-toggle=\"popover\" data-placement=\"bottom\" title=\"" + tarefaToolTip + "\"");
					if (tarefasUsuarioVO.getDescricaoTarefa() == null)
						tarefasUsuarioVO.setDescricaoTarefa(" Vazio.");

					stringBuilder.append(" data-content=\"" + tarefasUsuarioVO.getDescricaoTarefa().toString() + "\"> ");
					stringBuilder.append(" <span class=\"glyphicon glyphicon-search\" aria-hidden=\"true\"></span> ");
					stringBuilder.append(" </button> ");

					stringBuilder.append(" <button type=\"button\" class=\"btn btn-default btn-xs\" data-container=\"body\" data-toggle=\"popover\"  ");
					stringBuilder.append(" title=\"Verificar tarefa detalhada\" data-content=\"Clique no botão para verificar!\" ");
					stringBuilder.append(" onclick=\"javascript:showTarefa('" + tarefasUsuarioVO.getWfProcess().toString() + "')\"> ");
					stringBuilder.append(" <span class=\"glyphicon glyphicon-zoom-in\" aria-hidden=\"true\"></span> ");
					stringBuilder.append(" </button> ");
					stringBuilder.append(" <div class=\"btn-group\"> ");
					Boolean flag = PortalUtil.getCurrentUser() != null  && PortalUtil.getCurrentUser().getNeoId() != 730475369 && PortalUtil.getCurrentUser().getNeoId() != 598912158 && PortalUtil.getCurrentUser().getNeoId() != 47133909 && PortalUtil.getCurrentUser().getNeoId() != 20166 && PortalUtil.getCurrentUser().getNeoId() != 106214471 && !isDiretoria;

					if (flag)
					{
						stringBuilder.append(" <button class=\"btn btn-default btn-xs dropdown-toggle\" type=\"button\" id=\"dropdownMenu\"  data-toggle=\"dropdown\" aria-expanded=\"false\" disabled=\"true\"> ");

					}
					else
					{
						stringBuilder.append(" <button class=\"btn btn-default btn-xs dropdown-toggle\" type=\"button\" id=\"dropdownMenu\"  data-toggle=\"dropdown\" aria-expanded=\"false\" > ");

					}
					stringBuilder.append(" <span class=\"glyphicon glyphicon-sort-by-attributes\" id=\"dropdown_title\"></span><span class=\"caret\"></span> ");
					stringBuilder.append(" </button> ");
					stringBuilder.append(" <ul class=\"dropdown-menu pull-center\" role=\"menu\"> ");
					for (int i = 1; i <= Integer.parseInt(tarefasUsuarioVOAux.getTotal()); i++)
					{
						if (flag)
						{
							stringBuilder.append(" <li class=\"disabled\"> ");

						}
						else
						{
							stringBuilder.append(" <li> ");
						}

						stringBuilder.append(" <a id=\"dpPosicao\" onclick=\"javascript:showPosicao('" + tarefasUsuarioVO.getCodigoTarefa() + "','" + i + "','" + tarefasUsuarioVO.getUsuarioCode() + "',this,'" + tarefasUsuarioVOAux.getCodigoUsuario() + "','" + model + "')\" href=\"#\">");
						stringBuilder.append(" " + i + "</a></li>");
					}

					stringBuilder.append("  </ul> ");
					stringBuilder.append(" </div> ");
					if (tarefasUsuarioVO.getTipoTarefa() != null && tarefasUsuarioVO.getTipoTarefa().contains("D") && (tarefasUsuarioVOAux.getUsuario().contains(PortalUtil.getCurrentUser().getFullName()) || !flag))
					{
						stringBuilder.append(" 	<span class=\"badge progress-bar-success\"  title=\"Desenvolvimento\" onclick=\"javascript:showTipo('" + tarefasUsuarioVO.getCodigoTarefa() + "','" + tarefasUsuarioVO.getTipoTarefa() + "',this,'" + tarefasUsuarioVOAux.getCodigoUsuario() + "','" + tarefasUsuarioVO.getUsuarioCode() + "','" + model + "')\" style=\"cursor: pointer;\">");
						stringBuilder.append(" " + tarefasUsuarioVO.getTipoTarefa() + "</span> ");
					}
					else if (tarefasUsuarioVO.getTipoTarefa() != null && tarefasUsuarioVO.getTipoTarefa().contains("D"))
					{
						stringBuilder.append(" 	<span class=\"badge progress-bar-success\"  title=\"Desenvolvimento\" >");
						stringBuilder.append(" " + tarefasUsuarioVO.getTipoTarefa() + "</span> ");
					}

					else if (tarefasUsuarioVO.getTipoTarefa() != null && tarefasUsuarioVO.getTipoTarefa().contains("S") && (tarefasUsuarioVOAux.getUsuario().equals(PortalUtil.getCurrentUser().getFullName()) || !flag))
					{
						stringBuilder.append(" 	<span class=\"badge progress-bar-warning\"  title=\"Suporte\" onclick=\"javascript:showTipo('" + tarefasUsuarioVO.getCodigoTarefa() + "','" + tarefasUsuarioVO.getTipoTarefa() + "',this,'" + tarefasUsuarioVOAux.getCodigoUsuario() + "','" + tarefasUsuarioVO.getUsuarioCode() + "','" + model + "')\" style=\"cursor: pointer;\" >");
						stringBuilder.append(" " + tarefasUsuarioVO.getTipoTarefa() + "</span> ");
					}
					else if (tarefasUsuarioVO.getTipoTarefa() != null && tarefasUsuarioVO.getTipoTarefa().contains("S"))
					{
						stringBuilder.append(" 	<span class=\"badge progress-bar-warning\"  title=\"Suporte\">");
						stringBuilder.append(" " + tarefasUsuarioVO.getTipoTarefa() + "</span> ");
					}

					else if (tarefasUsuarioVO.getTipoTarefa() == null && (tarefasUsuarioVOAux.getUsuario().contains(PortalUtil.getCurrentUser().getFullName()) || !flag))
					{
						stringBuilder.append(" 	<span class=\"badge progress-bar-info\"  title=\"Indefinido\" onclick=\"javascript:showTipo('" + tarefasUsuarioVO.getCodigoTarefa() + "','" + tarefasUsuarioVO.getTipoTarefa() + "',this,'" + tarefasUsuarioVOAux.getCodigoUsuario() + "','" + tarefasUsuarioVO.getUsuarioCode() + "','" + model + "')\" style=\"cursor: pointer;\">");
						stringBuilder.append(" I </span> ");
					}
					else if (tarefasUsuarioVO.getTipoTarefa() == null)
					{
						stringBuilder.append(" 	<span class=\"badge progress-bar-info\"  title=\"Indefinido\" >");
						stringBuilder.append(" I </span> ");
					}

					if (tarefasUsuarioVO.getSituacaoTarefa() != null && tarefasUsuarioVO.getSituacaoTarefa().contains("T") && (tarefasUsuarioVOAux.getUsuario().contains(PortalUtil.getCurrentUser().getFullName()) || !flag))
					{
						stringBuilder.append(" 	<span class=\"badge progress-bar-danger\"  title=\"Trabalhando\" onclick=\"javascript:showSituacao('" + tarefasUsuarioVO.getCodigoTarefa() + "','" + tarefasUsuarioVO.getSituacaoTarefa() + "',this,'" + tarefasUsuarioVOAux.getCodigoUsuario() + "','" + tarefasUsuarioVO.getUsuarioCode() + "','" + model + "')\" style=\"cursor: pointer;\">");
						stringBuilder.append(" " + tarefasUsuarioVO.getSituacaoTarefa() + "</span> ");
					}
					else if (tarefasUsuarioVO.getSituacaoTarefa() != null && tarefasUsuarioVO.getSituacaoTarefa().contains("T"))
					{
						stringBuilder.append(" 	<span class=\"badge progress-bar-danger\"  title=\"Trabalhando\" >");
						stringBuilder.append(" " + tarefasUsuarioVO.getSituacaoTarefa() + "</span> ");
					}

					else if (tarefasUsuarioVO.getSituacaoTarefa() != null && tarefasUsuarioVO.getSituacaoTarefa().contains("E") && (tarefasUsuarioVOAux.getUsuario().contains(PortalUtil.getCurrentUser().getFullName()) || !flag))
					{
						stringBuilder.append(" 	<span class=\"badge progress-bar-info\"  title=\"Em espera\" onclick=\"javascript:showSituacao('" + tarefasUsuarioVO.getCodigoTarefa() + "','" + tarefasUsuarioVO.getSituacaoTarefa() + "',this,'" + tarefasUsuarioVOAux.getCodigoUsuario() + "','" + tarefasUsuarioVO.getUsuarioCode() + "','" + model + "')\" style=\"cursor: pointer;\">");
						stringBuilder.append(" " + tarefasUsuarioVO.getSituacaoTarefa() + "</span> ");
					}
					else if (tarefasUsuarioVO.getSituacaoTarefa() != null && tarefasUsuarioVO.getSituacaoTarefa().contains("E"))
					{
						stringBuilder.append(" 	<span class=\"badge progress-bar-info\"  title=\"Em espera\" >");
						stringBuilder.append(" " + tarefasUsuarioVO.getSituacaoTarefa() + "</span> ");
					}

					else if (tarefasUsuarioVO.getSituacaoTarefa() == null && (tarefasUsuarioVOAux.getUsuario().contains(PortalUtil.getCurrentUser().getFullName()) || !flag))
					{
						stringBuilder.append(" 	<span class=\"badge progress-bar-info\"  title=\"Indefinido\" onclick=\"javascript:showSituacao('" + tarefasUsuarioVO.getCodigoTarefa() + "','" + tarefasUsuarioVO.getSituacaoTarefa() + "',this,'" + tarefasUsuarioVOAux.getCodigoUsuario() + "','" + tarefasUsuarioVO.getUsuarioCode() + "','" + model + "')\" style=\"cursor: pointer;\">");
						stringBuilder.append(" I </span> ");
					}
					else if (tarefasUsuarioVO.getSituacaoTarefa() == null)
					{
						stringBuilder.append(" 	<span class=\"badge progress-bar-info\"  title=\"Indefinido\" >");
						stringBuilder.append(" I </span> ");
					}
					stringBuilder.append(" </li> ");

					count++;
				}
				stringBuilder.append(" </ul> ");
				stringBuilder.append(" <div class=\"panel-footer\"></div></div>");

			}
		}
		catch (Exception e)
		{
			log.error("##### ERRO PESQUISA POR TAREFAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			e.printStackTrace();
		}
		finally
		{
			out.print(stringBuilder.toString());
			log.warn("##### FINALIZAR  PESQUISA POR TAREFAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}

	}

	@SuppressWarnings("finally")
	private List<TarefasGrupoUsuarioVO> pesquisaTodasTarefas(String group, String model, String solicitante, String grupoSolicitante)
	{
	    	
	    	//TODO METODO COM FILTROS
	    
		Connection conn = PersistEngine.getConnection("FUSIONPROD");
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		List<TarefasVO> tarefasVOs = null;
		List<TarefasUsuarioVO> tarefasUsuarioVOs = null;
		List<TarefasGrupoUsuarioVO> tarefasGrupoUsuarioVOs = null;
		System.out.println();
		log.warn("##### INICIAR PESQUISA POR TAREFAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		try
		{
		    	
			if (group.equals("18317794")){
			    group += ",638564840";
			}
		    
			sql.append(" select p.neoId as wfprocess, p.code, us.neoid as codigoUsuario,see.code as usuario,  ");
			sql.append(" us.fullName,ta.titulo,tas.descricao,p.startDate,t.startDate as ultimaAlteracao,ta.prazo, Replace(se.code, 'SupervisaoSustentacaoSistemas', 'Coordenação de Sistemas') as grupo, usr.fullName as solicitante, ");
			sql.append(" gu.neoid as codigoGrupoSolicitante,seu.code as grupoSolicitante, ");
			sql.append(" usr.neoid as codigoSolicitante  ");
			sql.append(" from wfProcess p with(nolock)   ");
			sql.append(" inner join activity a with(nolock) on p.neoId = a.process_neoId   ");
			sql.append(" inner join task t with(nolock) on t.activity_neoid = a.neoid   ");
			sql.append(" inner join neouser usr with(nolock) on usr.neoid = p.requester_neoid  ");
			sql.append(" inner join neouser us with(nolock) on us.neoid = t.user_neoId   ");
			sql.append(" inner join neogroup g with(nolock) on g.neoId = us.group_neoId  ");
			sql.append(" inner join neogroup gu with(nolock) on gu.neoId = usr.group_neoId  ");
			sql.append(" inner join securityentity se with(nolock) on se.neoid = g.neoid   ");
			sql.append(" inner join securityentity see with(nolock) on see.neoid = us.neoid   ");
			sql.append(" inner join securityentity seu with(nolock) on seu.neoid = gu.neoid  ");
			sql.append(" inner join d_tarefa ta with(nolock) on p.entity_neoid = ta.neoid   ");
			sql.append(" inner join d_TarefaRegistroAtividades tas with(nolock)    ");
			sql.append(" on tas.neoid = (select min(tar.registroAtividades_neoid) from d_tarefa_registroAtividades tar with(nolock) where ta.neoid = tar.d_tarefa_neoid )  ");
			sql.append(" where g.neoId IN ("+group+")  ");
			sql.append(" and p.processState = 0   ");
			String where = "";
			if (NeoUtils.safeIsNotNull(model) && !model.isEmpty() && model.equals("18888"))
			{
				where = " and  p.model_neoId in (select pmaux.neoId from ProcessModel pmaux where pmaux.name like '%G001 - Tarefa Simples%') ";
			}
			else if (NeoUtils.safeIsNotNull(model) && !model.isEmpty() && model.equals("186043073"))
			{
				where = " and  p.model_neoId in (select pmaux.neoId from ProcessModel pmaux where pmaux.name like '%C034 - Solicitar Chip GPRS para Posto%') ";
			}
			
			sql.append(where);
			if (solicitante != null && !solicitante.isEmpty() && !solicitante.contains("null"))
				sql.append(" and usr.fullName LIKE ?");
			if (grupoSolicitante != null && !grupoSolicitante.isEmpty() && !grupoSolicitante.contains("null"))
				sql.append(" and seu.code LIKE ?");
			sql.append(" and p.saved = 1   ");
			sql.append(" and t.finishDate is null   ");
			sql.append(" and t.startDate is not null   ");
			sql.append(" and p.startDate is not null   ");
			sql.append(" order by Replace(se.code, 'SupervisaoSustentacaoSistemas', 'Coordenação de Sistemas'),us.fullName asc  ");

			pstm = conn.prepareStatement(sql.toString());
			//pstm.setLong(1, Long.parseLong(wfProcess));

			if (solicitante != null && !solicitante.isEmpty() && !solicitante.contains("null"))
				pstm.setString(1, solicitante);
			else if (grupoSolicitante != null && !grupoSolicitante.isEmpty() && !grupoSolicitante.contains("null"))
				pstm.setString(1, grupoSolicitante);

			rs = pstm.executeQuery();
			tarefasVOs = new ArrayList<TarefasVO>();
			tarefasUsuarioVOs = new ArrayList<TarefasUsuarioVO>();
			tarefasGrupoUsuarioVOs = new ArrayList<TarefasGrupoUsuarioVO>();
			TarefasVO tarefasVO = null;
			TarefasUsuarioVO tarefasUsuarioVO = null;
			TarefasGrupoUsuarioVO tarefasGrupoUsuarioVO = null;
			int cont = 0;
			int total = 0;
			int totalGrupo = 0;
			String nomeAtual = "";
			String grupoAtual = "";
			String nomeUsuario = "";
			Long neoIdUsuario = 0L;
			HtmlToText htmlToText = new HtmlToText();
			
			
			/** Tras totais  executados mensais */
			
			Map<Long, Long> mapTotaisTarefas = this.getTotaisGerais(group);
			
			Long totalExecMesGrupo = 0L;
			
			for (Long key : mapTotaisTarefas.keySet()){
			    Long totalMes = mapTotaisTarefas.get(key);
			    totalExecMesGrupo += totalMes;
			}
			
			
			
			
			while (rs.next())
			{
				tarefasVO = new TarefasVO();
				if (nomeAtual != null && !nomeAtual.equals(rs.getString("fullName")) && cont > 0)
				{

					//List<NeoObject> listaTarefas = retornaObjects(nomeUsuario, Long.parseLong(model));
					//(listaTarefas, tarefasVOs, nomeUsuario);
					tarefasUsuarioVO = new TarefasUsuarioVO();
					tarefasUsuarioVO.setCodigoUsuario(neoIdUsuario);
					tarefasUsuarioVO.setUsuario(nomeAtual);
					tarefasUsuarioVO.setTotal(String.valueOf(total));
					
					//TODO INSERIR TOTAIS
					
					
					Long totalExecMesUsuario = mapTotaisTarefas.get(tarefasUsuarioVO.getCodigoUsuario());
					if (totalExecMesUsuario == null){
					    totalExecMesUsuario = 0L;
					}
					tarefasUsuarioVO.setTotalExecMes(totalExecMesUsuario);
					
					
					tarefasUsuarioVO.setTarefas(tarefasVOs);
					tarefasUsuarioVOs.add(tarefasUsuarioVO);
					if (grupoAtual != null && !grupoAtual.equals(rs.getString("grupo")) && cont > 0)
					{
						tarefasGrupoUsuarioVO = new TarefasGrupoUsuarioVO();
						tarefasGrupoUsuarioVO.setGrupo(grupoAtual);
						tarefasGrupoUsuarioVO.setTotal(totalGrupo);
						if (grupoAtual.contains("Sistemas"))
							tarefasGrupoUsuarioVO.setId("sistemas");
						tarefasGrupoUsuarioVO.setTarefas(tarefasUsuarioVOs);
						tarefasGrupoUsuarioVOs.add(tarefasGrupoUsuarioVO);
						tarefasUsuarioVOs = new ArrayList<TarefasUsuarioVO>();
						totalGrupo = 0;
					}
					tarefasVOs = new ArrayList<TarefasVO>();
					total = 0;

				}
				neoIdUsuario = rs.getLong("codigoUsuario");
				nomeUsuario = rs.getString("usuario");
				nomeAtual = rs.getString("fullName");
				tarefasVO.setWfProcess(rs.getLong("wfprocess"));
				tarefasVO.setCodigoTarefa(rs.getLong("code"));
				tarefasVO.setNomeCompleto(nomeAtual);
				tarefasVO.setUsuarioCode(nomeUsuario);
				tarefasVO.setTituloTarefa(htmlToText.parse(rs.getString("titulo")));
				tarefasVO.setModelo(Long.parseLong(model));
				tarefasVO.setSolicitante(rs.getString("solicitante"));
				tarefasVO.setCodigoSolicitante(rs.getLong("codigoSolicitante"));
				tarefasVO.setGrupoSolicitante(rs.getString("grupoSolicitante"));
				tarefasVO.setCodigoGrupoSolicitante(rs.getLong("codigoGrupoSolicitante"));

				GregorianCalendar dataAux = new GregorianCalendar();
				dataAux.setTime(rs.getTimestamp("startDate"));
				tarefasVO.setDataAberturaTarefa(NeoDateUtils.safeDateFormat(dataAux, "dd/MM/yyyy"));
				GregorianCalendar dataAux2 = new GregorianCalendar();
				dataAux2.setTime(rs.getTimestamp("prazo"));
				tarefasVO.setPrazoTarefa(NeoDateUtils.safeDateFormat(dataAux2, "dd/MM/yyyy HH:mm"));
				GregorianCalendar dataAux3 = new GregorianCalendar();
				dataAux3.setTime(rs.getTimestamp("ultimaAlteracao"));
				tarefasVO.setDataUltimaAlteracao(NeoDateUtils.safeDateFormat(dataAux3, "dd/MM/yyyy"));
				grupoAtual = rs.getString("grupo");
				String descricao = rs.getString("descricao");
				descricao = htmlToText.parse(descricao);
				descricao = descricao.replaceAll("\\<.*?>", "");
				descricao = descricao.replaceAll("&nbsp;", "");
				descricao = descricao.replaceAll("&amp;", "");
				descricao = descricao.replaceAll("\"", "");
				descricao = descricao.replaceAll("'", "");
				descricao = descricao.replaceAll("=", "");
				tarefasVO.setDescricaoTarefa(descricao + "\n Prazo:" + tarefasVO.getPrazoTarefa());

				tarefasVOs.add(tarefasVO);
				cont++;
				total++;
				totalGrupo++;

			}
			if (cont > 0)
			{
				//List<NeoObject> listaTarefas = retornaObjects(nomeUsuario, Long.parseLong(model));
				//verificaTarefas(listaTarefas, tarefasVOs, nomeUsuario);
				tarefasUsuarioVO = new TarefasUsuarioVO();
				tarefasUsuarioVO.setCodigoUsuario(neoIdUsuario);
				tarefasUsuarioVO.setUsuario(nomeAtual);
				
				Long totalExecMesUsuario = mapTotaisTarefas.get(tarefasUsuarioVO.getCodigoUsuario());
				if (totalExecMesUsuario == null){
				    totalExecMesUsuario = 0L;
				}
				tarefasUsuarioVO.setTotalExecMes(totalExecMesUsuario);
				
				
				tarefasUsuarioVO.setTotal(String.valueOf(total));
				tarefasUsuarioVO.setTarefas(tarefasVOs);
				tarefasUsuarioVOs.add(tarefasUsuarioVO);

				tarefasGrupoUsuarioVO = new TarefasGrupoUsuarioVO();
				tarefasGrupoUsuarioVO.setGrupo(grupoAtual);
				tarefasGrupoUsuarioVO.setTotal(totalGrupo);
				/** Total de tarefas executadas mês */
				tarefasGrupoUsuarioVO.setTotalExecMes(totalExecMesGrupo);
				
				if (grupoAtual.contains("Infra"))
					tarefasGrupoUsuarioVO.setId("infra");
				if (grupoAtual.contains("Sistemas"))
					tarefasGrupoUsuarioVO.setId("sistemas");
				tarefasGrupoUsuarioVO.setTarefas(tarefasUsuarioVOs);
				tarefasGrupoUsuarioVOs.add(tarefasGrupoUsuarioVO);

			}
			log.warn("##### EXECUTAR PESQUISA POR TAREFAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		}
		catch (Exception e)
		{
			log.error("##### ERRO PESQUISA POR TAREFAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			e.printStackTrace();
		}
		finally
		{

			try
			{

				OrsegupsUtils.closeConnection(conn, pstm, rs);
				int countAux = 0;
				for (TarefasGrupoUsuarioVO grupoUsuarioVO : tarefasGrupoUsuarioVOs)
				{
					for (TarefasUsuarioVO tarefasUsuarioVO : grupoUsuarioVO.getTarefas())
					{

						for (TarefasVO tarefasVO : tarefasUsuarioVO.getTarefas())
						{
							if (tarefasVO.getCodigoTarefa() != null)
							{

								QLGroupFilter groupFilter = new QLGroupFilter("AND");
								groupFilter.addFilter(new QLEqualsFilter("tarefa", tarefasVO.getCodigoTarefa().toString()));
								groupFilter.addFilter(new QLEqualsFilter("modelo", Long.parseLong(model)));
								List<NeoObject> tarefasObj = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICONTROLETAREFAS"), groupFilter);
								if (tarefasObj != null && !tarefasObj.isEmpty())
								{
									EntityWrapper entityWrapper = new EntityWrapper(tarefasObj.get(0));
									tarefasVO.setPosicaoTarefa((Long) entityWrapper.findValue("posicao"));
									tarefasVO.setTipoTarefa((String) entityWrapper.findValue("tipo"));
									tarefasVO.setSituacaoTarefa((String) entityWrapper.findValue("situacao"));
									tarefasVO.setModelo((Long) entityWrapper.findValue("modelo"));
								}
							}
						}
						Collections.sort((List<TarefasVO>) tarefasUsuarioVO.getTarefas());
					}

				}

			}
			catch (Exception e)
			{
				log.error("##### ERRO PESQUISA POR TAREFAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
				e.printStackTrace();
			}
			log.warn("##### FINALIZAR  PESQUISA POR TAREFAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			return tarefasGrupoUsuarioVOs;

		}

	}
}