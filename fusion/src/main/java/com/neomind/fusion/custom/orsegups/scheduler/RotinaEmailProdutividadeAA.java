package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.HtmlEmail;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class RotinaEmailProdutividadeAA implements CustomJobAdapter
{

	private static final Log log = LogFactory.getLog(RotinaEmailProdutividadeAA.class);

	@Override
	public void execute(CustomJobContext ctx)
	{
		log.warn("##### INICIAR ROTINA RotinaEmailProdutividadeAA - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try
		{
			StringBuilder builder = new StringBuilder();
			GregorianCalendar calendar = new GregorianCalendar();
			Calendar periodo = Calendar.getInstance();
			if (periodo.get(Calendar.HOUR_OF_DAY) >= 6 && periodo.get(Calendar.HOUR_OF_DAY) < 12)
				calendar.set(Calendar.HOUR, -24);

			String periodoInicial = NeoDateUtils.safeDateFormat(calendar, "yyyy-MM-dd");
			String periodoFinal = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "yyyy-MM-dd");
			String periodoInicialStr = NeoDateUtils.safeDateFormat(calendar, "dd/MM/yyyy");
			String periodoFinalStr = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy");
			if (NeoUtils.safeIsNotNull(periodoInicial) && NeoUtils.safeIsNotNull(periodoFinal))
			{

				if (periodo.get(Calendar.HOUR_OF_DAY) >= 5 && periodo.get(Calendar.HOUR_OF_DAY) < 12)
				{
					periodoInicial += " 17:00";
					periodoFinal += " 08:00";
					periodoInicialStr += " 17:00";
					periodoFinalStr += " 08:00";
				}
				else if (periodo.get(Calendar.HOUR_OF_DAY) > 12 && periodo.get(Calendar.HOUR_OF_DAY) < 22)
				{
					periodoInicial += " 05:00";
					periodoFinal += " 20:00";
					periodoInicialStr += " 05:00";
					periodoFinalStr += " 20:00";
				}

			}

			//Produtividade AAs DIARIA
			builder.append(" SELECT SUBSTRING(vtr.NM_VIATURA, 1, 3) AS REG, vtr.NM_VIATURA AS AIT, SUM( ");
			builder.append(" CASE WHEN DATEDIFF(MINUTE, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL) <= 10 THEN 2.0 ");
			builder.append(" WHEN DATEDIFF(MINUTE, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL) > 10 AND DATEDIFF(MINUTE, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL) <= 15 THEN 1.0 ");
			builder.append(" WHEN DATEDIFF(MINUTE, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL) > 15 AND DATEDIFF(MINUTE, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL) <= 20 THEN 0.5 ");
			builder.append(" ELSE 0.00 END + ");
			builder.append(" CASE WHEN DATEDIFF(MINUTE, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) <= 15 THEN 1.0 ");
			builder.append(" WHEN DATEDIFF(MINUTE, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) > 15 AND DATEDIFF(MINUTE, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) <= 20 THEN 0.5 ");
			builder.append(" WHEN DATEDIFF(MINUTE, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) > 20 AND DATEDIFF(MINUTE, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) <= 25 THEN 0.25 ");
			builder.append(" ELSE 0.00 END ) AS PRD ");
			builder.append(" FROM VIEW_HISTORICO h WITH (NOLOCK) ");
			builder.append(" INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA ");
			builder.append(" WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL ");
			builder.append(" AND h.DT_VIATURA_NO_LOCAL IS NOT NULL ");
			builder.append(" AND h.DT_FECHAMENTO >= '" + periodoInicial + "' ");
			builder.append(" AND h.DT_FECHAMENTO <= '" + periodoFinal + "' ");
			builder.append(" AND DATEDIFF (MINUTE, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) >= 0 ");
			builder.append(" AND h.CD_EVENTO NOT IN ('XAAE', 'XXX8', 'X8', 'XXX7', 'X406', 'XXX2', 'XXX5') ");
			//AND h.CD_HISTORICO IN (303709058, 303709178, 303709240, 303709555 ,303710414 ,303710776, 303711161, 303711522, 303711655 ,303712005, 303712598, 303712668)
			builder.append("GROUP BY SUBSTRING(vtr.NM_VIATURA, 1, 3), vtr.NM_VIATURA ");
			builder.append("ORDER BY 1, 3 DESC ");
			conn = PersistEngine.getConnection("SIGMA90");
			pstm = conn.prepareStatement(builder.toString());
			rs = pstm.executeQuery();
			String returnEmail = "<strong> Relação de produtividade no período, de: </strong>" + periodoInicialStr + "<strong> à </strong>" + periodoFinalStr + " <br>";
			String reg = "";
			Double totalizador = 0.0;
			while (rs.next())
			{
				if (!reg.equals(rs.getString("REG")))
				{
					reg = rs.getString("REG");
					returnEmail += "<strong> -------------------------------------------------------------------------------------------------- </strong>  <br>";
					returnEmail += "<strong> Regional:  </strong>" + rs.getString("REG") + " <br>";
					returnEmail += "<strong> -------------------------------------------------------------------------------------------------- </strong>  <br><br>";
					returnEmail += "<strong> Colaborador &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp   Produtividade  </strong><br>";
					returnEmail += "<strong> -------------------------------------------------------------------------------------------------- </strong>  <br><br>";
				}

				totalizador += rs.getDouble("PRD");
				returnEmail += rs.getString("AIT") + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + String.valueOf(rs.getDouble("PRD")) + " <br>";
				//returnEmail += "<strong> Produtividade:  </strong>" + String.valueOf(rs.getDouble("PRD")) + " <br>";
				returnEmail += "<strong> -------------------------------------------------------------------------------------------------- </strong>  <br><br>";

			}

			returnEmail += "<strong> -------------------------------------------------------------------------------------------------- </strong>  <br><br>";
			returnEmail += "<strong> Total Geral:  </strong>" + String.valueOf(totalizador) + " <br>";
			returnEmail += "<strong> -------------------------------------------------------------------------------------------------- </strong>  <br><br>";
			if (totalizador != 0.0)
			{
				enviaEmailRotina(returnEmail);
			}
			log.warn("##### EXECUTAR ROTINA RotinaEmailProdutividadeAA - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		catch (Exception ex)
		{
			log.error(" ERRO ROTINA RotinaEmailProdutividadeAA " + ex.getMessage());
			System.out.println("[" + key + "] ERRO ROTINA RotinaEmailProdutividadeAA " + ex.getMessage());
			ex.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
			log.warn("##### FINALIZAR ROTINA RotinaEmailProdutividadeAA - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}

	private void enviaEmailRotina(String returnFromAccess)
	{

		try
		{

			MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

			MailSettings mailClone = new MailSettings();
			mailClone.setMinutesInterval(settings.getMinutesInterval());
			mailClone.setFromEMail(settings.getFromEMail());
			mailClone.setFromName(settings.getFromName());
			mailClone.setRenderServer(settings.getRenderServer());
			mailClone.setSecurityFromEmail(settings.getSecurityFromEmail());
			mailClone.setSecurityCopyEmail(settings.getSecurityCopyEmail());
			mailClone.setSmtpSettings(settings.getSmtpSettings());
			mailClone.setPort(settings.getPort());
			mailClone.setSmtpServer(settings.getSmtpServer());
			mailClone.setEnabled(settings.isEnabled());

			mailClone.setFromEMail("fusion@orsegups.com.br");
			mailClone.setFromName("Orsegups Partipações S.A.");
			if (mailClone != null)
			{

				HtmlEmail noUserEmail = new HtmlEmail();
				StringBuilder noUserMsg = new StringBuilder();

				Calendar saudacao = Calendar.getInstance();
				String saudacaoEMail = "";
				if (saudacao.get(Calendar.HOUR_OF_DAY) >= 6 && saudacao.get(Calendar.HOUR_OF_DAY) < 12)
				{
					saudacaoEMail = "Bom dia, ";
				}
				else if (saudacao.get(Calendar.HOUR_OF_DAY) >= 12 && saudacao.get(Calendar.HOUR_OF_DAY) <= 18)
				{
					saudacaoEMail = "Boa tarde, ";
				}
				else
				{
					saudacaoEMail = "Boa noite, ";
				}

				noUserEmail.setCharset("ISO-8859-1");
				noUserEmail.addTo("cm@orsegups.com.br");
				noUserEmail.addCc("giliardi@orsegups.com.br");

				noUserEmail.setSubject("Não Responda - E-mail contendo a produtividade AA. ");

				noUserMsg.append("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></head><body>");
				noUserMsg.append("<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#ffffff\">");
				noUserMsg.append("          <tbody>");
				noUserMsg.append("		    <td align=\"left\" valign=\"top\" >");
				noUserMsg.append("               <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/logoORS.jpg\" >");
				noUserMsg.append("            </td>");
				noUserMsg.append("			<td>");
				noUserMsg.append("			<strong>" + saudacaoEMail + " este e-mail se refere a produtividade AA.</strong><br>");
				noUserMsg.append(returnFromAccess);
				noUserMsg.append("			</td>");
				noUserMsg.append("     </tbody></table>");
				noUserMsg.append("</body></html>");
				noUserEmail.setHtmlMsg(noUserMsg.toString());
				noUserEmail.setContent(noUserMsg.toString(), "text/html");
				mailClone.applyConfig(noUserEmail);

				noUserEmail.send();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO RotinaLigacaoEventos - Data: " + e.getMessage().toString() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		}

	}
}
