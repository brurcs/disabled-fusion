package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class FAPTotalizaItensServicoProdutoAgrupado implements AdapterInterface
{
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		Connection conn = null;
		StringBuilder sqlFapItem = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String mensagem = "";
		
		try
		{
			conn = PersistEngine.getConnection("");
			
			BigDecimal valorTotalProduto = new BigDecimal(0.0);
			BigDecimal valorTotalServico = new BigDecimal(0.0);
			
			sqlFapItem.append(" SELECT ITM.VALOR, TIPITM.DESCRICAO AS TIPOITEM FROM D_FAPAUTORIZACAODEPAGAMENTO FAP                             ");
			sqlFapItem.append(" INNER JOIN D_FAPAUTORIZACAODEPAGAMENTO_ITEMORCAMENTO FAPI ON FAPI.D_FAPAUTORIZACAODEPAGAMENTO_NEOID = FAP.NEOID ");
			sqlFapItem.append(" INNER JOIN D_FAPITEMDEORCAMENTO ITM ON ITM.NEOID = FAPI.ITEMORCAMENTO_NEOID                                     ");
			sqlFapItem.append(" INNER JOIN D_FAPTIPOITEMDEORCAMENTO TIPITM ON TIPITM.NEOID = ITM.TIPOITEM_NEOID                                 ");
			sqlFapItem.append(" WHERE FAP.CODIGOTAREFA = " + processEntity.findValue("wfprocess.code")             							     );
			sqlFapItem.append(" ORDER BY TIPITM.DESCRICAO 																						");

			pstm = conn.prepareStatement(sqlFapItem.toString());
			rs = pstm.executeQuery();

			while (rs.next())
			{
				BigDecimal valorItem = new BigDecimal(0.0);
				valorItem = rs.getBigDecimal("VALOR");
				String tipoItem = rs.getString("TIPOITEM");

				if(tipoItem != null && tipoItem.equals("PRODUTO"))
				{						
					valorTotalProduto = valorTotalProduto.add((BigDecimal)valorItem);	
				}					
				if(tipoItem != null && tipoItem.equals("SERVIÇO")){
					valorTotalServico = valorTotalServico.add((BigDecimal)valorItem);	
				}
			}
			
			OrsegupsUtils.closeConnection(conn, pstm, rs);
			
			processEntity.setValue("valorTotalProduto", valorTotalProduto);
			processEntity.setValue("valorTotalServicos", valorTotalServico);
		}
		catch (WorkflowException e)
		{
			e.printStackTrace();
			throw new WorkflowException(mensagem);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Erro inesperado. Por favor contate a TI!!");
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				return;
			}
		}
	}
}
