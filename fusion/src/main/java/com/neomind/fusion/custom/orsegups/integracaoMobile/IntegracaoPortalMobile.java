package com.neomind.fusion.custom.orsegups.integracaoMobile;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.json.JSONObject;

import com.neomind.fusion.custom.orsegups.integracao.dao.UrlIntegracaoDao;
import com.neomind.fusion.custom.orsegups.integracao.dto.UrlIntegracaoDTO;
import com.neomind.fusion.custom.orsegups.integracaoSlack.SlackNotifcationUtils;
import com.neomind.fusion.custom.orsegups.objRatMobile.ObjRatMobile;

public class IntegracaoPortalMobile {

    public void inserirInformacoesPush(ObjRatMobile obj) {
	//String servico = " http://DSOO09:8081/portalcliente/inserirRegistroRatMobile"; // homolocacao

	System.out.println("###Iniciando na classe IntegracaoPortalMobile - Inicio");

	try {
	    UrlIntegracaoDTO urlIntegracaoDTO = new UrlIntegracaoDao().buscaUrlPorNome("rat");
	    // verifica se o Dao touxe nulo e seta uma url padrao
	    if (urlIntegracaoDTO == null)
		urlIntegracaoDTO = new UrlIntegracaoDTO(
			"http://apps.orsegups.com.br:8070/portalcliente-services/portalcliente/inserirRegistroRatMobile", "499c7a9a40f3bcda2c54471d03f494dc"
		);

	    obj.setLnkFotoLocal(URLEncoder.encode(obj.getLnkFotoLocal(), "UTF-8"));
	    if (obj.getLnkImg() != null && !obj.getLnkImg().equals(""))
		obj.setLnkImg(URLEncoder.encode(obj.getLnkImg(), "UTF-8"));
	    obj.setObservacao(obj.getObservacao() == null ? "" : URLEncoder.encode(obj.getObservacao(), "UTF-8"));
	    obj.setAtendidoPor(obj.getAtendidoPor() == null ? "" : obj.getAtendidoPor());
	    obj.setLnkFotoAit(obj.getLnkFotoAit() == null ? "" : obj.getLnkFotoAit());
	    obj.setResultado(obj.getResultado() == null ? "" : obj.getResultado());
	    obj.setResultadoDoAtendimento(obj.getResultadoDoAtendimento() == null ? "" : obj.getResultadoDoAtendimento());

	    String urlParameters = obj.toStringServico();
	    System.out.println(urlParameters);

	    URL url = new URL(urlIntegracaoDTO.getUrl());
	    HttpURLConnection con = (HttpURLConnection) url.openConnection();
	    con.setRequestMethod("POST");
	    con.setRequestProperty("User-Agent", "Mozilla/5.0");
	    con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
	    con.setRequestProperty("X-Auth-Token", urlIntegracaoDTO.getToken());
	    con.setDoOutput(true);

	    DataOutputStream wr = new DataOutputStream(con.getOutputStream());
	    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(wr, "UTF-8"));
	    writer.write(urlParameters);
	    writer.flush();
	    writer.close();

	    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
	    String inputLine;
	    StringBuffer response = new StringBuffer();

	    while ((inputLine = in.readLine()) != null) {
		response.append(inputLine);
	    }
	    in.close();

	    JSONObject jsonObj = new JSONObject(response.toString());
	    Long status = Long.parseLong(jsonObj.getString("status"));
	    if (status < 100) {
		System.out.println("###Erro ao tentar inserir a rat: " + response.toString());
	    } else {
		System.out.println(jsonObj.getString("msg") + " - " + obj);
		System.out.println("###Sucesso na classe IntegracaoPortalMobile - Fim");
	    }
	} catch (Exception e) {
	    StringWriter erros = new StringWriter();
	    e.printStackTrace(new PrintWriter(erros));

	    System.err.println("###Erro na classe IntegracaoPortalMobile - Inicio");
	    e.printStackTrace();
	    System.err.println("###Erro na classe IntegracaoPortalMobile - Fim");

	    SlackNotifcationUtils.sendNotification(SlackNotifcationUtils.ERROR_NOTIFICATIOIN, "Sistema Fusion Erro na Classe IntegracaoPortalMobile " + erros.toString() + "\n rat: " + obj);

	    try {
		erros.close();
	    } catch (IOException e1) {
		e1.printStackTrace();
	    }
	}
    }

}
