package com.neomind.fusion.custom.orsegups.site.vo;

public class SiteEventosColaboradorVO {
	
	private String codEve;
	private String desEve;
	private String refEve;
	private String valPro;
	private String valDes;
	
	public String getRefEve() {
		return refEve;
	}
	public void setRefEve(String refEve) {
		this.refEve = refEve;
	}
	public String getCodEve() {
		return codEve;
	}
	public void setCodEve(String codEve) {
		this.codEve = codEve;
	}
	public String getDesEve() {
		return desEve;
	}
	public void setDesEve(String desEve) {
		this.desEve = desEve;
	}
	public String getValPro() {
		return valPro;
	}
	public void setValPro(String valPro) {
		this.valPro = valPro;
	}
	public String getValDes() {
		return valDes;
	}
	public void setValDes(String valDes) {
		this.valDes = valDes;
	}
}
