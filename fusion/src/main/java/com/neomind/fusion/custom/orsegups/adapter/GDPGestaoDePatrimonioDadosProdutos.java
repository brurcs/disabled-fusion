package com.neomind.fusion.custom.orsegups.adapter;

import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.util.NeoUtils;

public class GDPGestaoDePatrimonioDadosProdutos implements AdapterInterface 
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{

		List<NeoObject> tramites = (List<NeoObject>) processEntity.findValue("intensTramite");
		
		if(origin != null){
		processEntity.setValue("solicitante", origin.getUser());
		}
		if (NeoUtils.safeIsNotNull(tramites))
		{
			for (NeoObject tramite : tramites)
			{
				EntityWrapper tramiteWrapper = new EntityWrapper(tramite);
				

				NeoObject patrimonio = (NeoObject) tramiteWrapper.findValue("patrimonio");
				EntityWrapper patrimonioWrapper = new EntityWrapper(patrimonio);
				
				
				String descricaoProduto = (String)patrimonioWrapper.findValue("patrimonio.produto.despro");
				String codigoProduto = (String)patrimonioWrapper.findValue("patrimonio.produto.codpro");
				String departamento = (String)patrimonioWrapper.findValue("patrimonio.departamento.descricao");
				String setor = (String)patrimonioWrapper.findValue("patrimonio.setor.descricao");
				
				
				tramiteWrapper.setValue("codigoProduto", codigoProduto);
				tramiteWrapper.setValue("descricaoProduto", descricaoProduto);
				tramiteWrapper.setValue("departamento", departamento);
				tramiteWrapper.setValue("setorOrigem", setor);
				tramiteWrapper.setValue("posto", setor);
			
			}
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}
}
