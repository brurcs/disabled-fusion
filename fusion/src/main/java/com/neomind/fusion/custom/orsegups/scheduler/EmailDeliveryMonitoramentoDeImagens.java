package com.neomind.fusion.custom.orsegups.scheduler;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.HtmlEmail;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.integracaoMobile.IntegracaoPortalMobile;
import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.objRatMobile.ObjRatMobile;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsEmailUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoCalendarUtils;
import com.neomind.util.NeoDateUtils;

/**
 * Rotina de envio de e-mails com imagens do acompanhemto feito pela central
 */

public class EmailDeliveryMonitoramentoDeImagens implements CustomJobAdapter {
    private static final Log log = LogFactory.getLog(EmailDeliveryMonitoramentoDeImagens.class);
    public static final String EVT_IMG = "IMG";

    @SuppressWarnings("unchecked")
    @Override
    public void execute(CustomJobContext arg0) {

	int adicionados = 0;

	log.warn("E-Mail  Monitoramento de imagens Inicio execução em: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));

	Long key = GregorianCalendar.getInstance().getTimeInMillis();

	Set<Integer> empresasNotificadas = new HashSet<Integer>();

	Connection conn = null;
	StringBuilder strSigma = new StringBuilder();
	PreparedStatement pstm = null;
	ResultSet rs = null;

	try {
	    String ultimaExecucaoRotina = ultimaExecucaoRotina();
	    String ultimaExecucaoRotinaAux = ultimaExecucaoRotinaRange();
	    salvaNovasImagens(ultimaExecucaoRotina, ultimaExecucaoRotinaAux);

	    conn = PersistEngine.getConnection("SIGMA90");
	    InstantiableEntityInfo infoHis = (InstantiableEntityInfo) EntityRegister.getEntityInfo("EmailAutomaticoAcompanhamentoImagens");
	    InstantiableEntityInfo infoImagens = (InstantiableEntityInfo) EntityRegister.getEntityInfo("SIGMAImagensEventoAcompanhamento");
	    InstantiableEntityInfo ExcecoesEmail = (InstantiableEntityInfo) EntityRegister.getEntityInfo("CadastroExcecoesEmailsAcompanhamento");

	    InstantiableEntityInfo infoEnvioEmail = (InstantiableEntityInfo) EntityRegister.getEntityInfo("EmailDelivery");

	    strSigma.append(" 	 SELECT c.OBSERVACAO, c.CGCCPF, c.ID_CENTRAL,C.ID_EMPRESA, c.FANTASIA, c.RAZAO, c.ID_CENTRAL, c.PARTICAO, c.ENDERECO, cid.NOME AS NM_CIDADE, bai.NOME AS NM_BAIRRO, h.DT_FECHAMENTO, c.EMAILRESP,");
	    strSigma.append(" 	 REPLACE(REPLACE(REPLACE(REPLACE(hfe.NM_FRASE_EVENTO, 'NÃO LIGAR', ''), 'ROUBO', ''), '()', ''), '  ', ' ') as NM_FRASE_EVENTO, ");
	    strSigma.append(" 	 h.TX_OBSERVACAO_FECHAMENTO, ISNULL(ma.DS_MOTIVO_ALARME, 'N/A - SEM ALTERAÇÃO') as motivo, h.CD_HISTORICO AS CD_HISTORICO, ");
	    strSigma.append(" 	 h.CD_EVENTO, c.CD_CLIENTE,  c.NU_LATITUDE, c.NU_LONGITUDE,h.DT_RECEBIDO, h.CD_CODE ");
	    strSigma.append(" 	 FROM VIEW_HISTORICO h  WITH (NOLOCK)   ");
	    strSigma.append(" 	 INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
	    strSigma.append(" 	 INNER JOIN ROTA r  WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA   ");
	    strSigma.append(" 	 INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE  ");
	    strSigma.append(" 	 INNER JOIN HISTORICO_FRASE_EVENTO hfe WITH (NOLOCK) ON hfe.CD_FRASE_EVENTO = h.CD_FRASE_EVENTO  ");
	    strSigma.append(" 	 LEFT JOIN MOTIVO_ALARME ma WITH (NOLOCK) ON ma.CD_MOTIVO_ALARME = h.CD_MOTIVO_ALARME  ");
	    strSigma.append(" 	 LEFT JOIN dbCIDADE cid WITH (NOLOCK) ON cid.ID_CIDADE = c.ID_CIDADE  ");
	    strSigma.append(" 	 LEFT JOIN dbBAIRRO bai WITH (NOLOCK) ON bai.ID_CIDADE = c.ID_CIDADE AND bai.ID_BAIRRO = c.ID_BAIRRO  ");
	    strSigma.append(" 	 WHERE  c.ctrl_central = 1 AND C.TP_PESSOA != 2 AND (( H.CD_CODE = 'ACP')  OR (H.CD_CODE = 'ACV' ) OR (H.CD_CODE = 'MPI' ) )   ");
	    strSigma.append(" 	 AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
	    strSigma.append(" 	 AND CD_USUARIO_FECHAMENTO <> 9999     ");
	    strSigma.append(" 	 AND NOT EXISTS (SELECT * FROM [CACUPE\\SQL02].Fusion_Producao.dbo.D_EmailAutomaticoAcompanhamentoImagens em WITH (NOLOCK) where em.historico = h.CD_HISTORICO)  ");
	    strSigma.append(" 	 AND ( h.DT_FECHAMENTO > '" + ultimaExecucaoRotina + "' OR  h.DT_FECHAMENTO > '" + ultimaExecucaoRotinaAux + "' ) ");
	    strSigma.append(" 	 AND h.TX_OBSERVACAO_FECHAMENTO IS NOT NULL AND h.TX_OBSERVACAO_FECHAMENTO <> ''  ");
	    strSigma.append("    AND (h.TX_OBSERVACAO_FECHAMENTO NOT LIKE '%#AtualizarCadastro%' AND h.TX_OBSERVACAO_FECHAMENTO NOT LIKE '%#NaoEnviaComunicado%')   ");

	    pstm = conn.prepareStatement(strSigma.toString());

	    rs = pstm.executeQuery();

	    String calendarDDMM = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM");

	    while (rs.next()) {
		boolean clienteComExcecao = OrsegupsEmailUtils.verificarExcecaoCNPJ(rs.getInt("CD_CLIENTE"));

		String observacao = rs.getString("OBSERVACAO") == null ? "" : rs.getString("OBSERVACAO");

		if (!clienteComExcecao) {
		    String cgcCpf = rs.getString("CGCCPF") == null ? "" : rs.getString("CGCCPF").replaceAll("([.\\-/])", "").trim();
		    String fantasia = (rs.getString("FANTASIA") == null ? "" : rs.getString("FANTASIA"));
		    String razao = (rs.getString("RAZAO") == null ? "" : rs.getString("RAZAO"));
		    String particao = (rs.getString("PARTICAO") == null ? "" : rs.getString("PARTICAO"));
		    String endereco = (rs.getString("ENDERECO") == null ? "" : rs.getString("ENDERECO"));
		    String cidade = (rs.getString("NM_CIDADE") == null ? "" : rs.getString("NM_CIDADE"));
		    String bairro = (rs.getString("NM_BAIRRO") == null ? "" : rs.getString("NM_BAIRRO"));
		    Date dtFechamento = rs.getDate("DT_FECHAMENTO");
		    String nomeFraseEvento = (rs.getString("NM_FRASE_EVENTO") == null ? "" : rs.getString("NM_FRASE_EVENTO"));
		    String txObsevacaoFechamento = (rs.getString("TX_OBSERVACAO_FECHAMENTO") == null ? "" : rs.getString("TX_OBSERVACAO_FECHAMENTO"));
		    String motivo = (rs.getString("motivo") == null ? "" : rs.getString("motivo"));
		    String evento = (rs.getString("CD_EVENTO") == null ? "" : rs.getString("CD_EVENTO"));
		    String email = (rs.getString("EMAILRESP") == null ? "" : rs.getString("EMAILRESP"));
		    String dataAtendimento = NeoDateUtils.safeDateFormat(dtFechamento, "dd/MM/yyyy");
		    String horaAtendimento = rs.getString("DT_FECHAMENTO").substring(11, 16);
		    String historico = (rs.getString("CD_HISTORICO") == null ? "" : rs.getString("CD_HISTORICO"));
		    String cdCliente = (rs.getString("CD_CLIENTE") == null ? "" : rs.getString("CD_CLIENTE"));
		    // String central = rs.getString("ID_CENTRAL");

		    log.warn("[IMG] fantasia:" + fantasia + ", razao:" + razao + ", email" + email);

		    if (txObsevacaoFechamento != null && !txObsevacaoFechamento.isEmpty()) {
			String array[] = txObsevacaoFechamento.split("###");
			String textoFormatado = "";
			for (String string : array) {
			    textoFormatado += string + "<br/>";
			}

			if (!textoFormatado.isEmpty()) {
			    textoFormatado = textoFormatado.replaceAll("#", "");
			    textoFormatado = textoFormatado.replaceAll("\\r\\n", "");
			    textoFormatado = textoFormatado.replaceAll(calendarDDMM, "<br/><br/>" + calendarDDMM);
			    // textoFormatado = textoFormatado.replaceAll(calendarDM, "<br/><br/>" + calendarDDMM);

			    textoFormatado = textoFormatado.replaceAll("Ligando para cliente!", "Ligando para cliente ");
			    String aux = "";
			    if (textoFormatado.contains("Ramal :")) {
				int number = textoFormatado.indexOf("Ramal :");
				aux = textoFormatado.substring((number + 7), (number + 12));
				textoFormatado = textoFormatado.replaceAll(aux, "");
			    }
			    textoFormatado = textoFormatado.replaceAll("Ramal:", "");
			    textoFormatado = textoFormatado.replaceAll("Ramal :", "");
			    textoFormatado = textoFormatado.replaceAll("FecharEvento", "");
			    textoFormatado = textoFormatado.replaceAll("EmEspera", "");
			    textoFormatado = textoFormatado.replaceAll("ComAlteracao", "");
			    textoFormatado = textoFormatado.replaceAll("TratarSigma", "");

			    textoFormatado = textoFormatado.replaceAll("AtendimentoRealizado", "");
			    textoFormatado = textoFormatado.replaceAll("SemContato", "");
			    textoFormatado = textoFormatado.replaceAll("AtualizarCadastro", "");
			    textoFormatado = textoFormatado.replaceAll("NaoEnviaComunicado", "");
			    textoFormatado = textoFormatado.replaceAll(": - ", " - ");

			    textoFormatado = textoFormatado.replaceAll("Motivo:", "<br/><br/>");

			    txObsevacaoFechamento = textoFormatado;

			}
		    }

		    boolean flagAtualizadoNAC = observacao.contains("#AC");

		    List<String> emailClie = OrsegupsEmailUtils.validarEmail(email, cdCliente, flagAtualizadoNAC);
		    // List<String> emailClie = new ArrayList<String>();
		    // emailClie.add("mateus.batista@orsegups.com.br");
		    // emailClie.add("william.bastos@orsegups.com.br");

		    int empresa = rs.getInt("ID_EMPRESA");

		    if ((emailClie != null) && (!emailClie.isEmpty())) {
			log.warn("[IMG] email do cliente validado");

			final String tipo = OrsegupsEmailUtils.TIPO_RAT_ACOMPANHAMENTO;

			Map<Integer, String> params = OrsegupsEmailUtils.getConfiguracaoEmpresa(empresa);

			String pasta = null;
			String remetente = null;
			String grupo = null;

			if (params != null) {

			    pasta = params.get(OrsegupsEmailUtils.RAT_PASTA);
			    remetente = params.get(OrsegupsEmailUtils.RAT_REMETENTE);
			    grupo = params.get(OrsegupsEmailUtils.RAT_GRUPO);
			    String neoId = params.get(OrsegupsEmailUtils.RAT_NEOID);
			    Long tipRat = 6L;
			    String ratingToken = DigestUtils.sha256Hex(cgcCpf + Long.toString(tipRat) + Long.toString(new Date().getTime()));

			    for (String emailFor : emailClie) {
				log.warn("[IMG] addTo: " + emailFor);
				StringBuilder noUserMsg = new StringBuilder();
				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				groupFilter.addFilter(new QLEqualsFilter("email", emailFor));

				List<NeoObject> neoObjects = PersistEngine.getObjects(ExcecoesEmail.getEntityClass(), groupFilter);

				if (neoObjects == null || (neoObjects != null && neoObjects.isEmpty())) {
				    noUserMsg.append(OrsegupsEmailUtils.getCabecalhoEmail(tipo, pasta, ratingToken));

				    noUserMsg.append("\n <table width=\"600\" border=\"0\">");
				    noUserMsg.append("\n <tbody>");
				    noUserMsg.append("\n <tr align='center'>");
				    noUserMsg.append("\n <td colspan=\"4\" ><p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Informamos o atendimento ao seguinte evento de seu sistema de segurança:</p>");
				    noUserMsg.append("\n </td> </tr> </br>");
				    noUserMsg.append("\n <tr align='center'> ");
				    noUserMsg.append("\n <td colspan=\"4\" style=\"text-align: center;\"> <p style=\"font-family: 'Verdana';font-weight:normal;font-size:20px;text-align:center;color:#007FFF;font-weight:bold;padding:10px;margin-top:0px;\">ACOMPANHAMENTO DE IMAGENS</p>");
				    noUserMsg.append("\n </td> </tr> </br>");
				    noUserMsg.append("\n </tbody>");
				    noUserMsg.append("\n </table></td>");
				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n <tr align='center'>");
				    noUserMsg.append("\n <td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/separator-02.jpg\" width=\"auto\" height=\"8\" alt=\"\"/></td>");
				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n <tr align='center'> ");
				    noUserMsg.append("\n <td ><h2 style=\"margin:0px 0px 0px 0px;padding-bottom:0px;line-height:14px;font-size:20px;text-align:center;font-family: 'Verdana';font-weight:normal;\">DETALHES SOBRE O ATENDIMENTO</td>");
				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n <tr align='center'>");
				    noUserMsg.append("\n <td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/separator-02.jpg\" width=\"100%\" height=\"8\" alt=\"\"/></td>");
				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n <tr align='center' >");
				    noUserMsg.append("\n <td >");
				    noUserMsg.append("\n <table width=\"600\" border=\"0\" style=\"margin-bottom:-3px;\">");
				    noUserMsg.append("\n <tbody>");
				    noUserMsg.append("\n <tr align='center'>");
				    if (txObsevacaoFechamento != null && !txObsevacaoFechamento.isEmpty()) {
					noUserMsg.append("\n <tr>");
					noUserMsg.append("\n <td width=\"600\">");
					noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align : justify;\"> " + txObsevacaoFechamento + "</p></td>");
					noUserMsg.append("\n </tr><br/>");
				    } else {
					break;
				    }
				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n </tbody>");
				    noUserMsg.append("\n </table>");
				    noUserMsg.append("\n </td>");
				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n <tr align='center'>");
				    noUserMsg.append("\n <td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/separator-02.jpg\" width=\"100%\" height=\"8\" alt=\"\"/></td>");
				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n <tr align='center'> ");
				    noUserMsg.append("\n <td  style=\"padding:5px;\"><h2 style=\"margin:0px 0px 0px 0px;padding-bottom:0px;line-height:14px;font-size:20px;text-align:center;font-family: 'Verdana';font-weight:normal;\">INFORMAÇÕES DO CLIENTE</td>");
				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n <tr align='center'>");
				    noUserMsg.append("\n <td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/separator-02.jpg\" width=\"100%\" height=\"8\" alt=\"\"/></td>");
				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n <tr align='center'>");
				    noUserMsg.append("\n <td><table width=\"600\"  border=\"0\">");
				    noUserMsg.append("\n <tbody>");
				    noUserMsg.append("\n <tr align='center'><br/>");
				    noUserMsg.append("\n <td valign=\"top\">");
				    noUserMsg.append("\n <img  src=\"https://maps.googleapis.com/maps/api/staticmap?center=" + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE")
					    + "&zoom=19&size=500x300&maptype=hybrid&format=png&markers=icon:http://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/maps/images/casa_rat_email.png%7C" + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE") + "&path=color:0xf9dc00|"
					    + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE") + "\" width=\"500\" height=\"300\" alt=\"\"/>");
				    noUserMsg.append("</td>");
				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n <tr align='center' colspan=\"5\" style=\"padding-left:5%;padding-right:5%;\" >");
				    noUserMsg.append("\n <td  width=\"600\" valign=\"top\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align : justify;\"><strong>Local: </strong>  " + razao + "</br>");
				    noUserMsg.append("\n " + endereco + " - " + bairro + " " + cidade + "</p>");
				    noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align : justify;\"><strong>Data: </strong>" + dataAtendimento + " ");
				    noUserMsg.append("\n <strong>Hora: </strong>" + horaAtendimento + "</p>");
				    noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align : justify;\"><strong>Evento: </strong></br> ");
				    noUserMsg.append("\n " + nomeFraseEvento + "</p> ");
				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n <tr align='center'>");
				    noUserMsg.append("\n <td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/separator-02.jpg\" width=\"100%\" height=\"8\" alt=\"\"/></td>");
				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n <tr align='center'> ");
				    noUserMsg.append("\n <td colspan=\"4\" style=\"padding:5px;\"><h2 style=\"margin:0px 0px 0px 0px;padding-bottom:0px;line-height:14px;font-size:20px;text-align:center;font-family: 'Verdana';font-weight:normal;\">IMAGENS DO CLIENTE</td>");
				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n <tr align='center'>");
				    noUserMsg.append("\n <td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/separator-02.jpg\" width=\"100%\" height=\"8\" alt=\"\"/></td>");
				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n <tr align='center'>");
				    noUserMsg.append("\n <br/><table width=\"100%\" border=\"0\" style=\"margin-bottom:-3px;\">");
				    noUserMsg.append("\n <tbody>");

				    List<NeoObject> listaImagens = new ArrayList<NeoObject>();
				    String lisImg = "";

				    int y = 0;

				    QLGroupFilter filter = new QLGroupFilter("AND");
				    filter.addFilter(new QLEqualsFilter("historico", historico));

				    listaImagens = (List<NeoObject>) PersistEngine.getObjects(infoImagens.getEntityClass(), filter, -1, -1, "instante,camera asc");

				    if (listaImagens != null && !listaImagens.isEmpty()) {
					for (NeoObject neoObject : listaImagens) {
					    if (y == 2) {
						noUserMsg.append("\n </tr>");
						noUserMsg.append("\n <tr>");
						y = 0;
					    }
					    EntityWrapper wpneo = new EntityWrapper(neoObject);

					    noUserMsg.append("\n <td valign=\"top\"> ");
					    noUserMsg.append(" <img src=\"https://intranet.orsegups.com.br/fusion/file/download/" + ((NeoFile) wpneo.getValue("imagem")).getNeoId() + "\" height=\"240\" width=\"320\" border=\"0\" alt=\"Imagens\"></td>");
					    y++;
					    lisImg = lisImg + "https://intranet.orsegups.com.br/fusion/file/download/" + ((NeoFile) wpneo.getValue("imagem")).getNeoId() + ";";
					}

				    }
				    try {
					List<String> urls = OrsegupsEmailUtils.getImagensAmazonS3(Integer.valueOf(historico));
					if (urls != null) {
					    for (String url : urls) {
						if (y == 2) {
						    noUserMsg.append("\n </tr>");
						    noUserMsg.append("\n <tr>");
						    y = 0;
						}
						//
						noUserMsg.append("\n <td valign=\"top\"> ");
						noUserMsg.append(" <img src=\"" + url + "\" height=\"240\" width=\"320\" border=\"0\" alt=\"Imagens\"></td>");
						y++;
						lisImg = lisImg + url + ";";
					    }
					}
				    } catch (Exception e) {
					log.error("##ImagensAmazonS3## Erro ao buscar imagens para o evento: " + historico);
					e.printStackTrace();
				    }
				    noUserMsg.append("\n </tr>");

				    if (listaImagens != null && !listaImagens.isEmpty()) {
					noUserMsg.append("\n </tbody>");
					noUserMsg.append("\n </table>");
					noUserMsg.append("\n </tr>");

					noUserMsg.append(OrsegupsEmailUtils.getRodapeEmail(tipo, pasta, grupo, email, remetente, ratingToken));

					NeoObject emailHis = infoHis.createNewInstance();
					EntityWrapper emailHisWp = new EntityWrapper(emailHis);
					adicionados = adicionados + 1;
					emailHisWp.findField("fantasia").setValue(fantasia);
					emailHisWp.findField("razao").setValue(razao);
					emailHisWp.findField("particao").setValue(particao);
					emailHisWp.findField("endereco").setValue(endereco);
					emailHisWp.findField("cidade").setValue(cidade);
					emailHisWp.findField("bairro").setValue(bairro);
					emailHisWp.findField("dataFechamento").setValue(dataAtendimento);
					emailHisWp.findField("horaFechamento").setValue(horaAtendimento);
					emailHisWp.findField("motivoAlarme").setValue(motivo);
					emailHisWp.findField("evento").setValue(evento);
					emailHisWp.findField("enviadoPara").setValue(emailFor);

					emailHisWp.findField("nomeFraseEvento").setValue(nomeFraseEvento);
					emailHisWp.findField("historico").setValue(historico);
					PersistEngine.persist(emailHis);

					
					// TESTE E-MAIL UTILIZANDO RECURSO FUSION
					//String subject = "Relatório de acompanhamento de imagens - " + fantasia + " - " + dataAtendimento+ " " + horaAtendimento;
					//OrsegupsEmailUtils.sendTestEmail(noUserMsg, subject);

					GregorianCalendar dataCad = (GregorianCalendar) GregorianCalendar.getInstance();
					NeoObject emaiEnvio = infoEnvioEmail.createNewInstance();
					EntityWrapper emailEnvioWp = new EntityWrapper(emaiEnvio);
					emailEnvioWp.findField("de").setValue("cm.rat" + remetente);
					emailEnvioWp.findField("para").setValue(emailFor + ";emailautomatico@orsegups.com.br;giliardi@orsegups.com.br");
					emailEnvioWp.findField("assunto").setValue("Relatório de acompanhamento de imagens - " + fantasia + " - " + dataAtendimento + " " + horaAtendimento);
					emailEnvioWp.findField("Mensagem").setValue(noUserMsg.toString());
					emailEnvioWp.findField("datCad").setValue(dataCad);
					PersistEngine.persist(emaiEnvio);

					try {
					    // Montando informações para serviço Mobile - Inicio
					    ObjRatMobile objRat = new ObjRatMobile();
					    //
					    /**
					     * Tipos de Rats para mobile:
					     * RATATENDIMENTOTATICO(0L,
					     * "Atendimento Tático"),
					     * RATDESVIODEHABITO(1L,
					     * "Desvio de hábito");
					     */
					    objRat.setTipRat(tipRat);
					    objRat.setRatingToken(ratingToken);
					    objRat.setInformativo("Informamos o atendimento ao seguinte evento de seu sistema de segurança:");
					    objRat.setEvento(nomeFraseEvento);
					    objRat.setResultado("");
					    objRat.setHashId("Relatório de acompanhamento de imagens - " + fantasia + " - " + dataAtendimento + " " + horaAtendimento);
					    objRat.setResultadoDoAtendimento("ACOMPANHAMENTO DE IMAGENS");
					    objRat.setAtendidoPor("");
					    objRat.setLocal(razao + " - " + endereco + " - " + bairro + " - " + cidade);
					    objRat.setDataAtendimento(dataAtendimento);
					    objRat.setHoraAtendimento(horaAtendimento);
					    if (txObsevacaoFechamento != null && !txObsevacaoFechamento.equals("")) {
						objRat.setObservacao(txObsevacaoFechamento);
					    } else {
						objRat.setObservacao("");
					    }
					    objRat.setLnkImg(lisImg);
					    objRat.setLnkFotoAit("");
					    objRat.setEmpRat(grupo);
					    objRat.setNeoId(neoId);
					    objRat.setLnkFotoLocal("https://maps.googleapis.com/maps/api/staticmap?key=AIzaSyDie-lwDjXfRv0diccYyQtC8ZuGt-P4scs&center=" + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE")
						    + "&zoom=19&size=500x300&maptype=hybrid&format=png&markers=icon:http://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/maps/images/casa_rat_email.png%7C" + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE")
						    + "&path=color:0xf9dc00|" + rs.getString("NU_LATITUDE") + "," + rs.getString("NU_LONGITUDE"));

					    
					    if (!cgcCpf.equals("")) {
						objRat.setCgcCpf(Long.parseLong(cgcCpf));
						IntegracaoPortalMobile integracao = new IntegracaoPortalMobile();
						integracao.inserirInformacoesPush(objRat);
					    }
					} catch (Exception e) {
					    log.error("Erro IntegracaoPortalMobile EmailDeliveryMonitoramentoImagens");
					    e.printStackTrace();
					}

					log.warn("Cliente [IMG]: " + fantasia + ", E-mail: " + emailFor);
				    }
				}
				break;
			    }
			    OrsegupsUtils.sendEmailWhatsAppRAT(Long.valueOf(cdCliente), "Relatório de acompanhamento de imagens - " + fantasia + " - " + dataAtendimento + " " + horaAtendimento, endereco + " - " + bairro + " - " + cidade, dataAtendimento + " " + horaAtendimento);
			} else {
			    if (!empresasNotificadas.contains(empresa)) {
				OrsegupsEmailUtils.enviarNotificacaoEmpresaSemConfig(empresa);
				empresasNotificadas.add(empresa);
			    }
			}

		    } else {
			log.warn("[IMG] Email do cliente invalido vou vazio ");
		    }
		}
	    }
	    log.warn("E-Mail  Monitoramento de imagens Fim execução em: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	} catch (Exception e) {
	    e.printStackTrace();
	    System.out.println("[" + key + "] E-Mail acompanhamento imagem: " + e.getMessage());
	    log.error(" E-Mail acompanhamento imagem: ");
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");

	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

    }

    /**
     * Salva imagens do cliente no sistema fusion
     * 
     * @param ultimaExecucaoRotina
     * @param ultimaExecucaoRotinaAux
     */
    public void salvaNovasImagens(String ultimaExecucaoRotina, String ultimaExecucaoRotinaAux) {
	Connection conn = null;
	StringBuilder strSigma = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	List<EventoVO> eventoVOs = null;

	try {

	    conn = PersistEngine.getConnection("SIGMA90");
	    strSigma = new StringBuilder();
	    strSigma.append(" 	 SELECT h.CD_HISTORICO,c.ID_CENTRAL,C.ID_EMPRESA, c.PARTICAO,h.CD_CODE ,h.DT_RECEBIDO, c.SERVIDORCFTV, c.PORTACFTV, c.USERCFTV, c.SENHACFTV ");
	    strSigma.append(" 	 FROM VIEW_HISTORICO h  WITH (NOLOCK)   ");
	    strSigma.append(" 	 INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
	    strSigma.append(" 	 INNER JOIN ROTA r  WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA   ");
	    strSigma.append(" 	 INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE  ");
	    strSigma.append(" 	 INNER JOIN HISTORICO_FRASE_EVENTO hfe ON hfe.CD_FRASE_EVENTO = h.CD_FRASE_EVENTO  ");
	    strSigma.append(" 	 LEFT JOIN MOTIVO_ALARME ma ON ma.CD_MOTIVO_ALARME = h.CD_MOTIVO_ALARME  ");
	    strSigma.append(" 	 LEFT JOIN dbCIDADE cid ON cid.ID_CIDADE = c.ID_CIDADE  ");
	    strSigma.append(" 	 LEFT JOIN dbBAIRRO bai ON bai.ID_CIDADE = c.ID_CIDADE AND bai.ID_BAIRRO = c.ID_BAIRRO  ");
	    strSigma.append(" 	 WHERE  c.ctrl_central = 1 AND (( H.CD_CODE = 'ACP')  OR (H.CD_CODE = 'ACV' ) OR (H.CD_CODE = 'MPI' ) )   ");
	    strSigma.append(" 	 AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
	    strSigma.append(" 	 AND CD_USUARIO_FECHAMENTO <> 9999     ");
	    strSigma.append(" 	 AND ( h.DT_FECHAMENTO > '" + ultimaExecucaoRotina + "' OR  h.DT_FECHAMENTO > '" + ultimaExecucaoRotinaAux + "' ) ");
	    strSigma.append(" 	 AND h.TX_OBSERVACAO_FECHAMENTO IS NOT NULL AND h.TX_OBSERVACAO_FECHAMENTO <> ''  ");

	    pstm = conn.prepareStatement(strSigma.toString());
	    GregorianCalendar horarioExecucao = new GregorianCalendar(); // Ajustado para registrar o momento exato da Execução
	    rs = pstm.executeQuery();

	    eventoVOs = new ArrayList<EventoVO>();
	    EventoVO eventoVO = null;
	    InstantiableEntityInfo excecoesEventos = (InstantiableEntityInfo) EntityRegister.getEntityInfo("SIGMAImagensEventoAcompanhamento");

	    while (rs.next()) {
		String historico = (rs.getString("CD_HISTORICO") == null ? "" : rs.getString("CD_HISTORICO"));
		String empresa = (rs.getString("ID_EMPRESA") == null ? "" : rs.getString("ID_EMPRESA"));
		String idCentral = (rs.getString("ID_CENTRAL") == null ? "" : rs.getString("ID_CENTRAL"));
		String particao = (rs.getString("PARTICAO") == null ? "" : rs.getString("PARTICAO"));
		Timestamp dtRecebido = rs.getTimestamp("DT_RECEBIDO");
		String dataRecebidoStr = NeoDateUtils.safeDateFormat(dtRecebido, "dd/MM/yyyy HH:mm:ss");
		String cdCode = (rs.getString("CD_CODE") == null ? "" : rs.getString("CD_CODE"));

		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		groupFilter.addFilter(new QLEqualsFilter("historico", historico));

		@SuppressWarnings("unchecked")
		List<NeoObject> neoObjects = PersistEngine.getObjects(excecoesEventos.getEntityClass(), groupFilter);

		if (neoObjects == null || (neoObjects != null && neoObjects.isEmpty())) {
		    eventoVO = new EventoVO();

		    eventoVO.setCodigoHistorico(historico);
		    eventoVO.setEmpresa(empresa);
		    eventoVO.setCodigoCentral(idCentral);
		    eventoVO.setParticao(particao);
		    eventoVO.setCdCode(cdCode);
		    eventoVO.setDataRecebimento(dataRecebidoStr);
		    eventoVO.setServidorCFTV(rs.getString("SERVIDORCFTV") != null ? rs.getString("SERVIDORCFTV") : "");
		    eventoVO.setPortaCFTV(rs.getString("PORTACFTV") != null ? rs.getString("PORTACFTV") : "");
		    eventoVO.setUsuarioCFTV(rs.getString("USERCFTV") != null ? rs.getString("USERCFTV") : "");
		    eventoVO.setSenhaCFTV(rs.getString("SENHACFTV") != null ? rs.getString("SENHACFTV") : "");
		    eventoVOs.add(eventoVO);

		}
	    }
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	    if (eventoVOs != null && !eventoVOs.isEmpty()) {
		for (EventoVO eventoVOAux : eventoVOs) {

		    TreeMap<String, String> treeMap = getCameraCliente(eventoVOAux);

		    List<String> cameras = new ArrayList<String>();

		    if (treeMap != null && !treeMap.isEmpty()) {
			for (Entry<String, String> entry : treeMap.entrySet()) {

			    String camera = "";
			    if (entry != null) {
				camera = String.valueOf(entry.getKey());
				String descricaoCamera = entry.getValue();
				if (eventoVO.getCdCode() != null && eventoVO.getCdCode().equals("ACP") && descricaoCamera != null && !descricaoCamera.contains("[VIP]")) {
				    cameras.add(camera);
				} else if (eventoVO.getCdCode() != null && (eventoVO.getCdCode().equals("ACV") || eventoVO.getCdCode().equals("MPI"))) {
				    if (descricaoCamera != null && descricaoCamera.contains("[VIP]")) {
					cameras.add(0, camera);
				    } else {
					cameras.add(camera);
				    }
				}
			    }
			}

		    }

		    /*
		     * Novo bloco de busca de imagens registradas na tela de
		     * acompanhamento
		     */
		    QLGroupFilter filtroImagens = new QLGroupFilter("AND");
		    filtroImagens.addFilter(new QLEqualsFilter("cdHistorico", Long.valueOf(eventoVOAux.getCodigoHistorico())));

		    @SuppressWarnings("unchecked")
		    List<NeoObject> listaImagensRegistradas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("cmRegistroImagens"), filtroImagens, -1, -1, "data asc");

		    List<GregorianCalendar> listaDatasEvento = null;

		    if (listaImagensRegistradas != null && !listaImagensRegistradas.isEmpty()) {

			listaDatasEvento = new ArrayList<GregorianCalendar>();

			for (NeoObject registro : listaImagensRegistradas) {

			    EntityWrapper wrapperDadosRegistro = new EntityWrapper(registro);

			    GregorianCalendar dataEvento = (GregorianCalendar) wrapperDadosRegistro.findField("data").getValue();

			    listaDatasEvento.add(dataEvento);

			}

		    }

		    if (listaDatasEvento != null && !listaDatasEvento.isEmpty()) {
			for (int i = 0; i < listaDatasEvento.size(); i++) {

			    GregorianCalendar dataEvento = listaDatasEvento.get(i);

			    String dataHora = NeoDateUtils.safeDateFormat(dataEvento, "dd/MM/yyyy HH:mm:ss");
			    String hora = dataHora.substring(10, dataHora.length());
			    hora = hora.trim();
			    hora = hora.replaceAll(":", "-");
			    String data = dataHora.substring(0, 10);
			    data = data.trim();
			    data = data.replaceAll("/", "-");

			    for (String camera : cameras) {

				InstantiableEntityInfo colAit = AdapterUtils.getInstantiableEntityInfo("SigmaImagensEventoAcompanhamento");

				NeoObject noInatUsu = colAit.createNewInstance();
				EntityWrapper tmeWrapper = new EntityWrapper(noInatUsu);
				File file = new File("image2");
				file = retornaImagem(data, hora, camera, eventoVOAux);
				NeoFile newFile = new NeoFile();
				newFile.setName(file.getName());
				newFile.setStorage(NeoStorage.getDefault());
				PersistEngine.persist(newFile);
				PersistEngine.evict(newFile);
				OutputStream outi = newFile.getOutputStream();
				InputStream in = null;
				in = new BufferedInputStream(new FileInputStream(file));
				NeoStorage.copy(in, outi);
				newFile.setOrigin(NeoFile.Origin.TEMPLATE);
				tmeWrapper.findField("historico").setValue(eventoVOAux.getCodigoHistorico());
				tmeWrapper.findField("instante").setValue(Long.valueOf(i));
				tmeWrapper.findField("camera").setValue(camera);
				tmeWrapper.findField("imagem").setValue(newFile);
				tmeWrapper.findField("dataCadastro").setValue(new GregorianCalendar());
				PersistEngine.persist(noInatUsu);
				System.out.println("Gravado imagens para: " + eventoVOAux.getCodigoHistorico() + " da conta: " + eventoVOAux.getCodigoCentral() + " cdCliente: " + eventoVOAux.getCodigoCliente());
			    }

			}
		    } else {
			for (int i = 1; i <= 3; i++) {
			    GregorianCalendar calendar = new GregorianCalendar();
			    calendar = (NeoCalendarUtils.stringToFullDate(eventoVOAux.getDataRecebimento()));

			    if (i == 2)
				calendar.add(Calendar.SECOND, +10);
			    else if (i == 3)
				calendar.add(Calendar.SECOND, +20);

			    System.out.println(i);
			    String dataHora = NeoDateUtils.safeDateFormat(calendar, "dd/MM/yyyy HH:mm:ss");

			    String hora = dataHora.substring(10, dataHora.length());
			    hora = hora.trim();
			    hora = hora.replaceAll(":", "-");
			    String data = dataHora.substring(0, 10);
			    data = data.trim();
			    data = data.replaceAll("/", "-");

			    for (String camera : cameras) {

				InstantiableEntityInfo colAit = AdapterUtils.getInstantiableEntityInfo("SigmaImagensEventoAcompanhamento");

				NeoObject noInatUsu = colAit.createNewInstance();
				EntityWrapper tmeWrapper = new EntityWrapper(noInatUsu);
				File file = new File("image2");
				file = retornaImagem(data, hora, camera, eventoVOAux);
				NeoFile newFile = new NeoFile();
				newFile.setName(file.getName());
				newFile.setStorage(NeoStorage.getDefault());
				PersistEngine.persist(newFile);
				PersistEngine.evict(newFile);
				OutputStream outi = newFile.getOutputStream();
				InputStream in = null;
				in = new BufferedInputStream(new FileInputStream(file));
				NeoStorage.copy(in, outi);
				newFile.setOrigin(NeoFile.Origin.TEMPLATE);
				tmeWrapper.findField("historico").setValue(eventoVOAux.getCodigoHistorico());
				tmeWrapper.findField("instante").setValue(Long.parseLong(String.valueOf(i)));
				tmeWrapper.findField("camera").setValue(camera);
				tmeWrapper.findField("imagem").setValue(newFile);
				tmeWrapper.findField("dataCadastro").setValue(new GregorianCalendar());
				PersistEngine.persist(noInatUsu);

			    }
			}
		    }
		}
	    }

	    inserirFimRotina(horarioExecucao);

	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * Faz requisição para obter imagens gravadas
     * 
     * @param data
     * @param hora
     * @param camera
     * @return File
     */
    private File retornaImagem(String data, String hora, String camera, EventoVO eventoVO) {
	String img = "https://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/cm/imagens/camera-motion_not_found.png";
	String resolucao = "640x480";
	String qualidade = "100";
	String formato = "jpg";
	// 192.168.20.227

	// String prefixo = "http://";
	//
	// if (eventoVO.getPortaCFTV().equals("443")) {
	// prefixo = "https://";
	// }

	// FIXADO IP DEVIDO A PROBLEMA DE SSL HANDSHAKE

	String urlStr = "http://192.168.20.34/player/getimagem.cgi?camera=" + camera + "&resolucao=" + resolucao + "&qualidade=" + qualidade + "&formato=" + formato + "&data=" + data + "&hora=" + hora;
	ByteArrayOutputStream output = null;
	URL url = null;
	URLConnection uc = null;
	InputStream is = null;
	BufferedImage image = null;
	File outputfile = null;
	try {
	    output = new ByteArrayOutputStream();
	    String userpass = eventoVO.getUsuarioCFTV() + ":" + eventoVO.getSenhaCFTV();

	    url = new URL(urlStr);
	    uc = url.openConnection();
	    String basicAuth = "Basic " + new String(new Base64().encode(userpass.getBytes()));
	    uc.setRequestProperty("Authorization", basicAuth);
	    is = uc.getInputStream();
	    image = ImageIO.read(is);
	    outputfile = new File("image2.jpg");
	    if (image != null) {
		ImageIO.write(image, "jpg", outputfile);
	    } else {

		BufferedImage imageAux = null;
		URL urlAux = new URL(img);
		imageAux = ImageIO.read(urlAux);
		ImageIO.write(imageAux, "png", outputfile);

		System.out.println("Imagem de retorno com problema camera=" + camera + "&resolucao=" + resolucao + "&qualidade=" + qualidade + "&formato=" + formato + "&data=" + data + "&hora=" + hora);
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    try {
		if (is != null)
		    is.close();
		if (output != null) {
		    output.flush();
		    output.close();
		}
	    } catch (IOException e) {
		e.printStackTrace();
	    }

	}

	return outputfile;
    }

    /**
     * Retorna nome das câmeras do cliente
     * 
     * @param idCentral
     * @param empresa
     * @return TreeMap
     */
    public TreeMap<String, String> getCameraCliente(EventoVO eventoVO) {

	TreeMap<String, String> treeMap = null;

	String resposta = "";
	try {
	    String userpass = eventoVO.getUsuarioCFTV() + ":" + eventoVO.getSenhaCFTV();
	    // 192.168.20.227
	    // 10.0.20.34

	    // String prefixo = "http://";
	    //
	    // if (eventoVO.getPortaCFTV().equals("443")) {
	    // prefixo = "https://";
	    // }

	    // FIXADO IP DEVIDO A PROBLEMA DE SSL HANDSHAKE

	    URL oracle = new URL("http://192.168.20.34/camerasnomes.cgi?receiver=" + eventoVO.getEmpresa() + "&server=" + eventoVO.getCodigoCentral());
	    URLConnection yc = oracle.openConnection();
	    String basicAuth = "Basic " + new String(new Base64().encode(userpass.getBytes()));
	    yc.setRequestProperty("Authorization", basicAuth);
	    BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
	    String inputLine;
	    while ((inputLine = in.readLine()) != null) {
		resposta = inputLine;
	    }
	    in.close();
	    if (resposta != null && !resposta.isEmpty() && resposta.contains("=")) {
		String array[] = resposta.split("&");
		treeMap = new TreeMap<String, String>();

		for (String valor : array) {
		    if (valor != null && !valor.isEmpty() && valor.contains("=")) {
			String parametros[] = valor.split("=");
			treeMap.put(parametros[0], (String) parametros[1]);
		    }

		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}

	return treeMap;
    }

    private void inserirFimRotina(GregorianCalendar horarioExecucao) {

	try {
	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("nomeRotina", "EmailAcompanhamentoImagens"));

	    List<NeoObject> monitoraAgendador = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);
	    PersistEngine.getEntityManager().flush();

	    if (monitoraAgendador != null && !monitoraAgendador.isEmpty()) {
		NeoObject neoObject = (NeoObject) monitoraAgendador.get(0);

		EntityWrapper monitoraAgendadorWrapper = new EntityWrapper(neoObject);

		monitoraAgendadorWrapper.findField("dataFinalAgendador").setValue(horarioExecucao);

		PersistEngine.persist(neoObject);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}

    }

    private String ultimaExecucaoRotina() {

	String retorno = "";
	try {
	    Collection<NeoObject> monitoraAgendador = null;

	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("nomeRotina", "EmailAcompanhamentoImagens"));

	    monitoraAgendador = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);

	    if (monitoraAgendador != null && !monitoraAgendador.isEmpty()) {
		for (NeoObject neoObject : monitoraAgendador) {
		    EntityWrapper monitoraAgendadorObj = new EntityWrapper(neoObject);

		    GregorianCalendar dataFinalAgendador = (GregorianCalendar) monitoraAgendadorObj.getValue("dataFinalAgendador");

		    retorno = NeoDateUtils.safeDateFormat(dataFinalAgendador, "yyyy-MM-dd HH:mm:ss");
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {

	}

	return retorno;

    }

    private String ultimaExecucaoRotinaRange() {

	String retorno = "";
	try {
	    Collection<NeoObject> monitoraAgendador = null;

	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("nomeRotina", "EmailAcompanhamentoImagens"));

	    monitoraAgendador = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);

	    if (monitoraAgendador != null && !monitoraAgendador.isEmpty()) {
		for (NeoObject neoObject : monitoraAgendador) {
		    EntityWrapper monitoraAgendadorObj = new EntityWrapper(neoObject);

		    GregorianCalendar dataFinalAgendador = (GregorianCalendar) monitoraAgendadorObj.getValue("dataFinalAgendador");

		    GregorianCalendar dataFinalAgendadorAux = (GregorianCalendar) dataFinalAgendador.clone();

		    dataFinalAgendadorAux.add(Calendar.MINUTE, -15);

		    retorno = NeoDateUtils.safeDateFormat(dataFinalAgendadorAux, "yyyy-MM-dd HH:mm:ss");
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {

	}
	return retorno;

    }

}