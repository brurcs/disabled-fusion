package com.neomind.fusion.custom.orsegups.adapter;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class RSCSetaNovoPrazoAtendimento implements AdapterInterface
{
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		GregorianCalendar novoPrazo = new GregorianCalendar();
		NeoUser responsavelAtendimento = (NeoUser) processEntity.findField("responsavelAtendimento").getValue();
		
		if (responsavelAtendimento.getCode().equalsIgnoreCase("marcelo.varela")) {
			//novoPrazo.add(Calendar.HOUR_OF_DAY, +1);
			novoPrazo.add(Calendar.MINUTE, +5);
			processEntity.setValue("prazoAtendimento", novoPrazo);
		}
	}
}