package com.neomind.fusion.custom.orsegups.c023.adapter;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.event.TaskAssignerEvent;
import com.neomind.fusion.workflow.event.TaskAssignerEventListener;
import com.neomind.fusion.workflow.event.TaskFinishEvent;
import com.neomind.fusion.workflow.event.TaskFinishEventListener;
import com.neomind.fusion.workflow.exception.TaskException;

public class RegistraHistorico implements TaskFinishEventListener
{
	private static final Log log = LogFactory.getLog(RegistraHistorico.class);

	@Override
	public void onFinish(TaskFinishEvent event) throws TaskException
	{
		EntityWrapper wrapper = event.getWrapper();
		
		String responsavel = event.getTask().returnResponsible();
		String atividade = event.getActivity().getActivityName();
		GregorianCalendar dataAcao = new GregorianCalendar();
		String descricao = "Tarefa Executada";
		
		if (atividade.contains("Escalada") && !atividade.equalsIgnoreCase("...Reversão de Cancelamento - Iniciativa - Tarefa Escalada"))
		{
			List<NeoObject> listHistoricoAtividades = wrapper.findGenericValue("historicoAtividades");
			if (listHistoricoAtividades != null && listHistoricoAtividades.size() > 0)
			{
				sortNeoId(listHistoricoAtividades);
				
				NeoObject ultimoHistorico = listHistoricoAtividades.get(listHistoricoAtividades.size()-1);
				EntityWrapper wUltimoHistorico = new EntityWrapper(ultimoHistorico);
				
				wUltimoHistorico.setValue("descricao", "Tarefa Escalada");
			}
		}

		NeoObject noRegistroAtividade = AdapterUtils.createNewEntityInstance("c023HistoricoAtividade");
		EntityWrapper wRegistroAtividade = new EntityWrapper(noRegistroAtividade);
		wRegistroAtividade.setValue("dataAcao", dataAcao);
		wRegistroAtividade.setValue("atividade", atividade);
		wRegistroAtividade.setValue("responsavel", responsavel);
		wRegistroAtividade.setValue("descricao", descricao);
		
		wrapper.findField("historicoAtividades").addValue(noRegistroAtividade);
	}
	
	public static void sortNeoId(Collection<? extends NeoObject> list)
	{
		Collections.sort((List<? extends NeoObject>) list, new Comparator<NeoObject>()
		{
			public int compare(NeoObject obj1, NeoObject obj2)
			{
				return ((Long) new EntityWrapper(obj1).findGenericValue("neoId") < (Long) new EntityWrapper(obj2).findGenericValue("neoId")) ? -1 : 1;
			}
		});
	}
}