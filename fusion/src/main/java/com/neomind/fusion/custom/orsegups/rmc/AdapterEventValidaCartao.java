package com.neomind.fusion.custom.orsegups.rmc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.event.TaskStartEvent;
import com.neomind.fusion.workflow.event.TaskStartEventListener;
import com.neomind.fusion.workflow.exception.TaskException;

public class AdapterEventValidaCartao implements TaskStartEventListener {

    boolean temCartao;
    String numerosPostos; 
    String titulo;
    String descricao;
    String usuarioSolicitante;
    String usuarioExecutor;  
    
    NeoPaper papelSolicitante = null;
    NeoPaper papelExecutor = null;

    @Override
    public void onStart(TaskStartEvent event) throws TaskException {
	
	EntityWrapper processEntity = new EntityWrapper(event.getProcess().getEntity());
	String numeroContrato = processEntity.findGenericValue("numeroContrato");
	Long numeroRmc = processEntity.findGenericValue("numeroRMC");
	
	// Validação se há cartão - equipamentos código 205825
	temCartao = false;
	numerosPostos = "";

	Connection connSapiens = null;
	PreparedStatement psSapiens = null;
	ResultSet rsSapiens = null;

	try {
	    connSapiens = PersistEngine.getConnection("SAPIENS");
	    StringBuilder sqlSap = new StringBuilder();
	    sqlSap.append("Select usu_numpos From usu_t160obs Where usu_numctr = " + numeroContrato + " and usu_numrmc = " + numeroRmc + " and usu_numpos is not null Group By usu_numpos ");

	    psSapiens = connSapiens.prepareStatement(sqlSap.toString());
	    rsSapiens = psSapiens.executeQuery();

	    while (rsSapiens.next()) {
		String NumPos = rsSapiens.getString("usu_numpos");

		if (!NumPos.equals("0")) {
		    numerosPostos = NumPos + "," + numerosPostos;
		}
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(connSapiens, psSapiens, rsSapiens);
	}

	String[] listaPosto = numerosPostos.split(",");
	
	connSapiens = null;
	psSapiens = null;
	rsSapiens = null;

	for (int i = 0; i < listaPosto.length; i++) {
	    try {
		connSapiens = PersistEngine.getConnection("SAPIENS");
		StringBuilder sqlSap = new StringBuilder();
		sqlSap.append("Select usu_codpro From usu_t160eqp Where usu_numpos = " + listaPosto[i]);

		psSapiens = connSapiens.prepareStatement(sqlSap.toString());
		rsSapiens = psSapiens.executeQuery();

		while (rsSapiens.next()) {
		    String CodPro = rsSapiens.getString("usu_codpro");

		    if (CodPro.equals("205825")) {
			temCartao = true;
			break;
		    }
		}
	    } catch (Exception e) {
		e.printStackTrace();
	    } finally {
		OrsegupsUtils.closeConnection(connSapiens, psSapiens, rsSapiens);
	    }
	}

	if (temCartao) {
	    
	    IniciarTarefaSimples iniciar = new IniciarTarefaSimples();
	    
	    String nomeCliente = processEntity.findGenericValue("nomeCliente");	       	    
	    titulo = "Cadastro de Cartões - " + nomeCliente + " - " + numeroContrato + "_" + numeroRmc;
	    descricao = "Favor informar o ID dos cartões para o cliente " + nomeCliente + " - " + numeroContrato + "_" + numeroRmc;
	    papelSolicitante = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "solicitanteValidaCartao"));
	    papelExecutor = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "responsavelValidaCartao"));	 

	    for (NeoUser usr : papelSolicitante.getAllUsers()) {
		usuarioSolicitante = usr.getCode();
		break;
	    }

	    for (NeoUser usr : papelExecutor.getAllUsers()) {
		usuarioExecutor = usr.getCode();
		break;
	    }

	    GregorianCalendar prazo = new GregorianCalendar();
	    prazo.add(GregorianCalendar.DAY_OF_MONTH, 1);
	    prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
	    prazo.set(GregorianCalendar.MINUTE, 59);
	    prazo.set(GregorianCalendar.SECOND, 59);
	    prazo.set(GregorianCalendar.MILLISECOND, 59);

	    // Verifica Dia Util
	    while (!OrsegupsUtils.isWorkDay(prazo)) {
		prazo.add(GregorianCalendar.DAY_OF_MONTH, 1);
	    }

	    iniciar.abrirTarefa(usuarioSolicitante, usuarioExecutor, titulo, descricao, "1", "sim", prazo);
	}
    }
}
