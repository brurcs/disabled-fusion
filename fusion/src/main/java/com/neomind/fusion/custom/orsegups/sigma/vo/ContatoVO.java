package com.neomind.fusion.custom.orsegups.sigma.vo;

public class ContatoVO {
	
	private String cdProvidencia;
	private String cdCliente;
	private String idFuncao;
	private String nome;
	private String fone;
	private String nuPrioridade;
	private String nuPrioridadeNivel2;
	
	public String getCdProvidencia() {
		return cdProvidencia;
	}
	public void setCdProvidencia(String cdProvidencia) {
		this.cdProvidencia = cdProvidencia;
	}
	public String getCdCliente() {
		return cdCliente;
	}
	public void setCdCliente(String cdCliente) {
		this.cdCliente = cdCliente;
	}
	public String getIdFuncao() {
		return idFuncao;
	}
	public void setIdFuncao(String idFuncao) {
		this.idFuncao = idFuncao;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getFone() {
		return fone;
	}
	public void setFone(String fone) {
		this.fone = fone;
	}
	public String getNuPrioridade() {
		return nuPrioridade;
	}
	public void setNuPrioridade(String nuPrioridade) {
		this.nuPrioridade = nuPrioridade;
	}
	public String getNuPrioridadeNivel2() {
		return nuPrioridadeNivel2;
	}
	public void setNuPrioridadeNivel2(String nuPrioridadeNivel2) {
		this.nuPrioridadeNivel2 = nuPrioridadeNivel2;
	}
	
	

}
