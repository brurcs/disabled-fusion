package com.neomind.fusion.custom.orsegups.menu;

import java.util.ArrayList;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class MontarSubMenu
{
	public static List<SubMenuVO> listarMenu(String raiz)
	{
		List<SubMenuVO> subs = new ArrayList<SubMenuVO>();

		QLRawFilter raizFilter = new QLRawFilter("menuRaiz = '" +raiz+ "'");
		List<NeoObject> menus = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("MenuTopBarDinamico"), raizFilter);

		for (NeoObject m : menus)
		{
			EntityWrapper menuWrapper = new EntityWrapper(m);
			SubMenuVO sub = new SubMenuVO();

			
			sub.setMenuRaiz(raiz);
			sub.setLabel((String) menuWrapper.findValue("label"));
			sub.setHref((String) menuWrapper.findValue("href"));
			sub.setCss((String) menuWrapper.findValue("css"));
			sub.setIcon((String) menuWrapper.findValue("icon"));

			subs.add(sub);
		}
		return subs;
	}
}