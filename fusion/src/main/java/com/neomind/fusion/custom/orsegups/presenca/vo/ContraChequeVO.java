package com.neomind.fusion.custom.orsegups.presenca.vo;

import java.util.List;

public class ContraChequeVO
{
	private String titCar;
	private String datAdm;
	private String perRef;
	private String txtBan;
	private String txtAge;
	private String txtCon;
	private String salBas;
	private String basFgt;
	private String salIns;
	private String basIrf;
	private String fgtMes;
	private String totVct;
	private String totDsc;
	private String vlrLiq;
	private String msgChq;
	private ColaboradorVO colaborador;
	private List<EventosColaboradorVO> eventosColaborador;
	
	public String getTitCar()
	{
		return titCar;
	}
	public void setTitCar(String titCar)
	{
		this.titCar = titCar;
	}
	public String getDatAdm()
	{
		return datAdm;
	}
	public void setDatAdm(String datAdm)
	{
		this.datAdm = datAdm;
	}
	public String getPerRef()
	{
		return perRef;
	}
	public void setPerRef(String perRef)
	{
		this.perRef = perRef;
	}
	public String getTxtBan()
	{
		return txtBan;
	}
	public void setTxtBan(String txtBan)
	{
		this.txtBan = txtBan;
	}
	public String getTxtAge()
	{
		return txtAge;
	}
	public void setTxtAge(String txtAge)
	{
		this.txtAge = txtAge;
	}
	public String getTxtCon()
	{
		return txtCon;
	}
	public void setTxtCon(String txtCon)
	{
		this.txtCon = txtCon;
	}
	public String getSalBas()
	{
		return salBas;
	}
	public void setSalBas(String salBas)
	{
		this.salBas = salBas;
	}
	public String getBasFgt()
	{
		return basFgt;
	}
	public void setBasFgt(String basFgt)
	{
		this.basFgt = basFgt;
	}
	public String getSalIns()
	{
		return salIns;
	}
	public void setSalIns(String salIns)
	{
		this.salIns = salIns;
	}
	public String getBasIrf()
	{
		return basIrf;
	}
	public void setBasIrf(String basIrf)
	{
		this.basIrf = basIrf;
	}
	public String getFgtMes()
	{
		return fgtMes;
	}
	public void setFgtMes(String fgtMes)
	{
		this.fgtMes = fgtMes;
	}
	public String getTotVct()
	{
		return totVct;
	}
	public void setTotVct(String totVct)
	{
		this.totVct = totVct;
	}
	public String getTotDsc()
	{
		return totDsc;
	}
	public void setTotDsc(String totDsc)
	{
		this.totDsc = totDsc;
	}
	public String getVlrLiq()
	{
		return vlrLiq;
	}
	public void setVlrLiq(String vlrLiq)
	{
		this.vlrLiq = vlrLiq;
	}
	public String getMsgChq()
	{
		return msgChq;
	}
	public void setMsgChq(String msgChq)
	{
		this.msgChq = msgChq;
	}
	public List<EventosColaboradorVO> getEventosColaborador()
	{
		return eventosColaborador;
	}
	public void setEventosColaborador(List<EventosColaboradorVO> eventosColaborador)
	{
		this.eventosColaborador = eventosColaborador;
	}
	public ColaboradorVO getColaborador()
	{
		return colaborador;
	}
	public void setColaborador(ColaboradorVO colaborador)
	{
		this.colaborador = colaborador;
	}
}
