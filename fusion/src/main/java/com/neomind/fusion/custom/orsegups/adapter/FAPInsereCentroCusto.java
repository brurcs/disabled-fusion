package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Query;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.dao.AplicacaoTaticoDAO;
import com.neomind.fusion.custom.orsegups.fap.dao.AplicacaoTecnicoDAO;
import com.neomind.fusion.custom.orsegups.fap.dto.TaticoDTO;
import com.neomind.fusion.custom.orsegups.fap.dto.TecnicoDTO;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class FAPInsereCentroCusto implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity) throws WorkflowException{
		String mensagem = "";

		try
		{
			String placaViatura = (String) processEntity.findValue("viatura.placa");
			Long aplicacaoPagamento = (Long) processEntity.findValue("aplicacaoPagamento.codigo");

			if (aplicacaoPagamento == 1L)
			{
				// Manutenção Frota

				Long codigoCentroCustoViatura = (Long) processEntity.findValue("viatura.codigoCentroCusto");

				if (codigoCentroCustoViatura == null)
				{
					mensagem = "Centro de Custo não associado a Viatura de Placa " + placaViatura + ", por favor informe o Centro de Custo no Cadastro da Viatura";
					throw new WorkflowException(mensagem);
				}

				QLGroupFilter filterCcu = new QLGroupFilter("AND");
				filterCcu.addFilter(new QLEqualsFilter("codccu", codigoCentroCustoViatura.toString()));
				filterCcu.addFilter(new QLEqualsFilter("nivccu", 8));

				List<NeoObject> codigoCentroCusto = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), filterCcu);
				if (codigoCentroCusto != null && !codigoCentroCusto.isEmpty())
				{
					NeoObject noCentroCusto = AdapterUtils.createNewEntityInstance("FAPListaCentroDeCusto");
					EntityWrapper centralWrapper = new EntityWrapper(noCentroCusto);
					centralWrapper.setValue("codigoEmpresa", 1L);
					centralWrapper.setValue("codigoCentroCusto", codigoCentroCustoViatura.toString());
					centralWrapper.setValue("valorRateio", new BigDecimal(0));

					List<NeoObject> listaCentroCusto = new ArrayList<NeoObject>();
					listaCentroCusto.add(noCentroCusto);

					processEntity.setValue("listaCentroDeCusto", listaCentroCusto);
				}
				else
				{
					mensagem = "Centro de Custo não localizado no Sistema Sapiens! Verificar o Centro de Custo associado a Viatura de Placa " + placaViatura;
					throw new WorkflowException(mensagem);
				}

			}
			else if ((aplicacaoPagamento == 2L) || (aplicacaoPagamento == 3L) || (aplicacaoPagamento == 4L) || 
					 (aplicacaoPagamento == 5L) || (aplicacaoPagamento == 6L) || (aplicacaoPagamento == 7L) ||
					 (aplicacaoPagamento == 8L) || (aplicacaoPagamento == 9L))
			{
				// Manutencao Eletronica
				String nomeFonteDados = "SAPIENS";

				String razaoDoClienteSigma = (String) processEntity.findValue("contaSigma.razao");
				Long clienteSigma = (Long) processEntity.findValue("contaSigma.cd_cliente");

				String sql1 = "SELECT USU_T160CVS.USU_CODEMP, USU_T160CVS.USU_CODCCU                         " + 
				              "FROM USU_T160SIG                                                              " + 
						      "INNER JOIN USU_T160CVS ON USU_T160CVS.USU_CODEMP = USU_T160SIG.USU_CODEMP AND " + 
				              "                          USU_T160CVS.USU_CODFIL = USU_T160SIG.USU_CODFIL AND " + 
						      "							 USU_T160CVS.USU_NUMCTR = USU_T160SIG.USU_NUMCTR AND " + 
				              "                          USU_T160CVS.USU_NUMPOS = USU_T160SIG.USU_NUMPOS     " + 
						      "WHERE USU_T160SIG.USU_CODCLI = " + clienteSigma.toString() + 
						      " AND            " + 
						      "USU_T160CVS.USU_SITCVS = 'A'                                                  ";

				Query query1 = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sql1);
				@SuppressWarnings("unchecked")
				Collection<Object> resultList = query1.getResultList();

				if (resultList != null && !resultList.isEmpty())
				{
					List<NeoObject> listaCentroCusto = new ArrayList<NeoObject>();
					for (Object resultSet : resultList)
					{
						if (resultSet != null)
						{
							Object[] result = (Object[]) resultSet;

							NeoObject noCentroCusto = AdapterUtils.createNewEntityInstance("FAPListaCentroDeCusto");
							EntityWrapper centralWrapper = new EntityWrapper(noCentroCusto);
							centralWrapper.setValue("codigoEmpresa", new Long((Short) result[0]));
							centralWrapper.setValue("codigoCentroCusto", (String) result[1]);
							centralWrapper.setValue("valorRateio", new BigDecimal(0));

							listaCentroCusto.add(noCentroCusto);
						}
					}
					processEntity.setValue("listaCentroDeCusto", listaCentroCusto);
				}
				else
				{
					sql1 = sql1.replace("'A'", "'I'");

					Query query2 = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sql1);
					@SuppressWarnings("unchecked")
					Collection<Object> resultList2 = query2.getResultList();

					if (resultList2 != null && !resultList2.isEmpty())
					{
						List<NeoObject> listaCentroCusto = new ArrayList<NeoObject>();
						for (Object resultSet : resultList2)
						{
							if (resultSet != null)
							{
								Object[] result = (Object[]) resultSet;

								NeoObject noCentroCusto = AdapterUtils.createNewEntityInstance("FAPListaCentroDeCusto");
								EntityWrapper centralWrapper = new EntityWrapper(noCentroCusto);
								centralWrapper.setValue("codigoEmpresa", new Long((Short) result[0]));
								centralWrapper.setValue("codigoCentroCusto", (String) result[1]);
								centralWrapper.setValue("valorRateio", new BigDecimal(0));

								listaCentroCusto.add(noCentroCusto);
							}
						}
						processEntity.setValue("listaCentroDeCusto", listaCentroCusto);
					}
					else
					{
						if (aplicacaoPagamento == 4L)
						{
							System.out.println("Processo criado para permitir a inserção dos centros de custos manualmente para os Serviços de Rastreamento sem Associação Sigma X Sapiens");
						}
						else
						{

							boolean isVigzul = false;
							boolean isViaseg = false;

							long[] vigzul = { 10128L, 10129L, 10130L, 10131L, 10132L, 10133L, 10134L, 10135L, 10136L, 10137L, 10138L, 10139L, 10140L, 10141L, 10142L, 10143L, 10144L };

							long idEmpresa = (long) processEntity.findValue("contaSigma.id_empresa");

							for (long id : vigzul)
							{
								if (id == idEmpresa)
								{
									isVigzul = true;
								}
							}
							
							long[] viaseg = { 10157L, 10160L, 10145L, 10146L, 10147L, 10148L, 10149L, 10150L, 10151L, 10152L, 10153L, 10154L, 10155L, 10156L, 10158L, 10159L };

							long idEmpViaseg = (long) processEntity.findValue("contaSigma.id_empresa");

							for (long id : viaseg)
							{
								if (id == idEmpViaseg)
								{
									isViaseg = true;
								}
							}

							/*if (isVigzul)
							{
								String centroCusto = "8692770";

								List<NeoObject> listaCentroCusto = new ArrayList<NeoObject>();
								NeoObject noCentroCusto = AdapterUtils.createNewEntityInstance("FAPListaCentroDeCusto");
								EntityWrapper centralWrapper = new EntityWrapper(noCentroCusto);

								centralWrapper.setValue("codigoEmpresa", 18L);
								centralWrapper.setValue("codigoCentroCusto", centroCusto);
								centralWrapper.setValue("valorRateio", new BigDecimal(0));

								listaCentroCusto.add(noCentroCusto);
								processEntity.setValue("listaCentroDeCusto", listaCentroCusto);

							}
							else*/ 
							if(isViaseg)
							{
								System.out.println("Processo criado para permitir a inserção dos centros de custos manualmente para os Serviços da Empresa ViaSeg sem Associação Sigma X Sapiens");
							}
							else if(!isVigzul)
							{
								System.out.println("Processo criado para permitir a inserção dos centros de custos manualmente para os Serviços da Empresa Vigzul sem Associação Sigma X Sapiens");
							}
							else
							{
								mensagem = "Não foi encontrado nenhum vínculo do Cliente " + razaoDoClienteSigma + " com o Sistema Sapiens! Por favor contatar o NAC - Ramal: 6730";
								throw new WorkflowException(mensagem);
							}
						}
					}
				}
			}
			else if(aplicacaoPagamento == 10){			   	    
			    
			    TecnicoDTO tecDTO = new TecnicoDTO();
			    Long codTecnico = null;
			    List<NeoObject> listaDTO = new ArrayList<>();
			    
			    codTecnico = (Long) processEntity.findField("tecnicoSigma.tecnico.cd_colaborador").getValue();
			    tecDTO.setListaCcu(AplicacaoTecnicoDAO.consultaListaCcu(codTecnico)); 
			    listaDTO = tecDTO.getListaCcu();
			    		    
			    processEntity.findField("listaCentroDeCusto").setValue(listaDTO);
			} 
			else if(aplicacaoPagamento == 11){
			    
			    TaticoDTO taticoDTO = new TaticoDTO();
			    Long codRota = null;		   
			    List<NeoObject> listaDTO = new ArrayList<>();
			    
			    codRota = (Long) processEntity.findField("rotaSigma.rotaSigma.cd_rota").getValue();
			    List<NeoObject> listaCCU = AplicacaoTaticoDAO.consultaListaCcu(codRota);


			    if(NeoUtils.safeIsNotNull(listaCCU)){
				taticoDTO.setListaCcu(listaCCU);
				listaDTO = taticoDTO.getListaCcu();
				processEntity.findField("listaCentroDeCusto").setValue(listaDTO);
			    }		       
			}

		} catch(WorkflowException e){
		    e.printStackTrace();
		    throw (WorkflowException) e;
		}catch (Exception e) {
			e.printStackTrace();
		    throw new WorkflowException("Erro inesperado. Favor contatar a TI.");
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}
}