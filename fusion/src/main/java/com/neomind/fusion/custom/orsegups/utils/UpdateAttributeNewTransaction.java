package com.neomind.fusion.custom.orsegups.utils;

import java.lang.reflect.Method;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.common.NeoRunnable;
import com.neomind.fusion.custom.casvig.adapters.CasvigGestaoUniformesMovimentarEstoque;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;

public class UpdateAttributeNewTransaction extends NeoRunnable
{
	private static final Log log = LogFactory.getLog(UpdateAttributeNewTransaction.class);
	
	private String method;
	
	private Object value;
	
	private Long neoId;
	
	// quando for atualizar valores de um eform (entidade dinamica) setar esse valor para true;
	private Boolean dynamicEntity = false;
	

	public UpdateAttributeNewTransaction(String method, Object value, Long neoId)
	{
		super();
		
		this.method = method;
		
		this.value = value;
		
		this.neoId = neoId;
	}
	
	public UpdateAttributeNewTransaction(String method, Object value, Long neoId, Boolean dynamicEntity)
	{
		super();
		
		this.method = method;
		
		this.value = value;
		
		this.neoId = neoId;
		
		this.dynamicEntity = dynamicEntity;
	}

	@Override
	public void run() throws Exception
	{
		try
		{
			NeoObject neoObject = null;
			
			if(this.neoId != null)
			{
				neoObject = PersistEngine.getNeoObject(this.neoId);
			}
			else
			{
				log.error("NeoId não informado!");
			}
			
			if(neoObject != null)
			{
				if(this.dynamicEntity)
				{
					EntityWrapper entityWrapper = new EntityWrapper(neoObject);
					entityWrapper.setValue(this.method, this.value);
				}
				else
				{
					Method method = neoObject.getClass().getMethod(this.method, this.value.getClass());
					method.invoke(neoObject, this.value);
				}
				PersistEngine.persist(neoObject);
			}
			else
			{
				log.error("NeoObject não encontrado!");
			}
		}catch(Exception e)
		{
			log.error("Erro ao tentar persistir o valor '"+this.value+"' atraves do metodo '"+this.method+"' no objeto de neoId '"+this.neoId+"'");
			e.printStackTrace();
		}
	}
}
