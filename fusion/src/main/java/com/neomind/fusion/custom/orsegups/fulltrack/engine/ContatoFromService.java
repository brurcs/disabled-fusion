package com.neomind.fusion.custom.orsegups.fulltrack.engine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import com.google.gson.Gson;
import com.neomind.fusion.custom.orsegups.fulltrack.bean.FulltrackContato;
import com.neomind.fusion.custom.orsegups.fulltrack.messages.ResponseContato;

public class ContatoFromService {
    
    public static List<FulltrackContato> run(int id) {

   	StringBuilder retorno = new StringBuilder();

   	try {

   	    URL url = new URL("https://ws.fulltrack2.com/contacts/single/id/"+id);
   	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
   	    conn.setRequestMethod("GET");
   	    conn.setRequestProperty("apiKey", "682ee2548d4a73d8fadf72f32c52df790eb72aff");
   	    conn.setRequestProperty("secretKey", "4c02a36eebf652e2b753cb355cb763132259c65e");
   	    conn.setRequestProperty("Content-Type", "application/json");
   	    conn.setRequestProperty("Accept", "application/json");

   	    if (conn.getResponseCode() != 200) {
   		throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
   	    }

   	    BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream()), "UTF-8"));

   	    String output;

   	    while ((output = br.readLine()) != null) {
   		retorno.append(output + "\n");
   	    }

   	    conn.disconnect();

   	} catch (MalformedURLException e) {

   	    e.printStackTrace();

   	} catch (IOException e) {

   	    e.printStackTrace();

   	}

   	System.out.println(retorno.toString());

   	Gson gson = new Gson();
   	ResponseContato result = gson.fromJson(retorno.toString(), ResponseContato.class);
   	
   	return result.getData();
   

       }

}
