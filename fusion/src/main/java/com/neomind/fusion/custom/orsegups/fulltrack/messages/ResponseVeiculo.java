package com.neomind.fusion.custom.orsegups.fulltrack.messages;

import java.util.List;

import com.neomind.fusion.custom.orsegups.fulltrack.bean.FulltrackVeiculo;

public class ResponseVeiculo extends Response{

    private List<FulltrackVeiculo> data;

    public List<FulltrackVeiculo> getData() {
        return data;
    }

    public void setData(List<FulltrackVeiculo> data) {
        this.data = data;
    }
    
    
    
}
