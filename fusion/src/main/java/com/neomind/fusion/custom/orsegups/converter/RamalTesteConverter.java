package com.neomind.fusion.custom.orsegups.converter;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.portal.PortalUtil;

public class RamalTesteConverter extends StringConverter {

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin) {
		String result = "";

		if (field != null && field.getValue() != null) {
			// ramalDestino = (String) field.getValue();
			
			String ramalDestino = "6676";
			EntityWrapper usuarioOrigemWrapper = new EntityWrapper(PortalUtil.getCurrentUser());
			String ramalOrigem = (String) usuarioOrigemWrapper.findValue("ramal");
			
			

			String link = OrsegupsUtils.getCallLink(ramalOrigem, ramalDestino,
					true);

			if (link != null) {
				result = link;
			} else {
				result = super.getHTMLView(field, origin);
			}
		}
		return result;
	}

}
