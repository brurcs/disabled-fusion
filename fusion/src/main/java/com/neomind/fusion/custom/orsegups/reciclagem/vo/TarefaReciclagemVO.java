package com.neomind.fusion.custom.orsegups.reciclagem.vo;

import java.util.GregorianCalendar;

public class TarefaReciclagemVO
{
	public Long neoId;
	public Long numEmp;
	public Long numCad;
	public String nomeFun;
	public Long codReg;
	public String nomReg;
	public String numCpf;
	public GregorianCalendar dtIniCur;
	public GregorianCalendar dtFinCur;
	public String dtIniCurString;
	public String dtFinCurString;
	public String code;
	public String linkEditar;
	public String linkVisualizar;

	public String getLinkVisualizar()
	{
		return linkVisualizar;
	}

	public void setLinkVisualizar(String linkVisualizar)
	{
		this.linkVisualizar = linkVisualizar;
	}

	public String getLinkEditar()
	{
		return linkEditar;
	}

	public void setLinkEditar(String linkEditar)
	{
		this.linkEditar = linkEditar;
	}

	public String getDtIniCurString()
	{
		return dtIniCurString;
	}

	public void setDtIniCurString(String dtIniCurString)
	{
		this.dtIniCurString = dtIniCurString;
	}

	public String getDtFinCurString()
	{
		return dtFinCurString;
	}

	public void setDtFinCurString(String dtFinCurString)
	{
		this.dtFinCurString = dtFinCurString;
	}

	public String getCode()
	{
		return code;
	}

	public void setCode(String code)
	{
		this.code = code;
	}

	public Long getNeoId()
	{
		return neoId;
	}

	public void setNeoId(Long neoId)
	{
		this.neoId = neoId;
	}

	public Long getNumEmp()
	{
		return numEmp;
	}

	public void setNumEmp(Long numEmp)
	{
		this.numEmp = numEmp;
	}

	public Long getNumCad()
	{
		return numCad;
	}

	public void setNumCad(Long numCad)
	{
		this.numCad = numCad;
	}

	public String getNomeFun()
	{
		return nomeFun;
	}

	public void setNomeFun(String nomeFun)
	{
		this.nomeFun = nomeFun;
	}

	public Long getCodReg()
	{
		return codReg;
	}

	public void setCodReg(Long codReg)
	{
		this.codReg = codReg;
	}

	public String getNomReg()
	{
		return nomReg;
	}

	public void setNomReg(String nomReg)
	{
		this.nomReg = nomReg;
	}

	public String getNumCpf()
	{
		return numCpf;
	}

	public void setNumCpf(String numCpf)
	{
		this.numCpf = numCpf;
	}

	public GregorianCalendar getDtIniCur()
	{
		return dtIniCur;
	}

	public void setDtIniCur(GregorianCalendar dtIniCur)
	{
		this.dtIniCur = dtIniCur;
	}

	public GregorianCalendar getDtFinCur()
	{
		return dtFinCur;
	}

	public void setDtFinCur(GregorianCalendar dtFinCur)
	{
		this.dtFinCur = dtFinCur;
	}
}
