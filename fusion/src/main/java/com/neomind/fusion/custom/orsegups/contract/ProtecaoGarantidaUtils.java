package com.neomind.fusion.custom.orsegups.contract;

import java.math.BigDecimal;
import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class ProtecaoGarantidaUtils {
	
	
	public static Long inserePostoProtecaoGarantida(){
		Long retorno = null;
		try{
			
		}catch(Exception e){
			throw new WorkflowException("Erro ao inserir posto de proteção garantida.");
		}
		
		// into usu_t160cvs
		// into obs posto de proteção garantida cadastrado
		// geração de kit
		// geração obs kits inseridos no posto
		// into USU_T160EQP
		// obs de equipamento inserido
		// into usu_t160chk
		// obs de cheklist inserido
		// 
		
		
		
		return retorno;
	}
	
	/**
	 * Copia ISS do posto normal para o posto de PGO
	 * @param postoNormal
	 * @param postoProtecaoGarantida
	 */
	public static void copiaISSProtecaoGarantida(NeoObject postoNormal, NeoObject postoProtecaoGarantida){
		EntityWrapper wPostoNormal = new EntityWrapper(postoNormal); 
		EntityWrapper wPostoProtecaoGarantida = new EntityWrapper(postoProtecaoGarantida);
		
		String valorISS = NeoUtils.safeOutputString(wPostoNormal.findValue("valorISS"));
		ContratoLogUtils.logInfo("valorISS: " + valorISS);
		wPostoProtecaoGarantida.setValue("valorISS", valorISS);
		
		PersistEngine.persist(postoProtecaoGarantida);
		ContratoLogUtils.logInfo("copia encerrada.");
				
	}
	
	public static void atualizaPostoProtecaoGarantida(NeoObject postoNormal, NeoObject postoProtecaoGarantida){
		ContratoLogUtils.logInfo("copiando dados do posto normal para o posto de preoteção garantida");
		EntityWrapper wPostoNormal = new EntityWrapper(postoNormal); 
		EntityWrapper wPostoProtecaoGarantida = new EntityWrapper(postoProtecaoGarantida);
		
		//NeoObject complementoServico = (NeoObject) wPostoNormal.findValue("complementoServico");
		//wPostoProtecaoGarantida.setValue("complementoServico", complementoServico);
		
		String complementoServicoTxt = (String) wPostoNormal.findValue("complementoServicoTxt");
		wPostoProtecaoGarantida.setValue("complementoServicoTxt", complementoServicoTxt);
		
		NeoObject regionalPosto = (NeoObject) wPostoNormal.findValue("regionalPosto");
		wPostoProtecaoGarantida.setValue("regionalPosto", regionalPosto);
		
		Long numContrato = wPostoNormal.findValue("numContrato") != null ? (Long) wPostoNormal.findValue("numContrato") : (Long) null;
		wPostoProtecaoGarantida.setValue("numContrato", numContrato);
		
		GregorianCalendar dataFimFaturamento = (GregorianCalendar) wPostoNormal.findValue("dataFimFaturamento");
		wPostoProtecaoGarantida.setValue("dataFimFaturamento", dataFimFaturamento);
		
		Long qtdeDeFuncionarios = wPostoNormal.findValue("qtdeDeFuncionarios") != null ? (Long) wPostoNormal.findValue("qtdeDeFuncionarios") : (Long) null;
		wPostoProtecaoGarantida.setValue("qtdeDeFuncionarios", qtdeDeFuncionarios);
		
		Long qtdeDeFuncionariosPrevistos = wPostoNormal.findValue("qtdeDeFuncionariosPrevistos") != null ? (Long) wPostoNormal.findValue("qtdeDeFuncionariosPrevistos") : (Long) null;
		wPostoProtecaoGarantida.setValue("qtdeDeFuncionariosPrevistos", qtdeDeFuncionariosPrevistos);
		
		BigDecimal qtdeParaFaturamento = wPostoNormal.findValue("qtdeParaFaturamento") != null ? (BigDecimal) wPostoNormal.findValue("qtdeParaFaturamento") : (BigDecimal) null;
		wPostoProtecaoGarantida.setValue("qtdeParaFaturamento", qtdeParaFaturamento);
		
		//BigDecimal precoUnitarioPosto = wPostoNormal.findValue("precoUnitarioPosto") != null ? (BigDecimal) wPostoNormal.findValue("precoUnitarioPosto") : (BigDecimal) null;  
		//wPostoProtecaoGarantida.setValue("precoUnitarioPosto", precoUnitarioPosto);
		
		/*BigDecimal valorTotalPosto = wPostoNormal.findValue("valorTotalPosto") != null ? (BigDecimal) wPostoNormal.findValue("valorTotalPosto") : (BigDecimal) null;
		wPostoProtecaoGarantida.setValue("valorTotalPosto", valorTotalPosto);
		 */
		/*BigDecimal precoUnitarioPostoConferencia = wPostoNormal.findValue("precoUnitarioPostoConferencia") != null ? (BigDecimal) wPostoNormal.findValue("precoUnitarioPostoConferencia") : (BigDecimal) null;
		wPostoProtecaoGarantida.setValue("precoUnitarioPostoConferencia", precoUnitarioPostoConferencia);*/
		
		NeoObject representante = (NeoObject) wPostoNormal.findValue("representante");
		wPostoProtecaoGarantida.setValue("representante", representante);
		
		//NeoObject centroCusto = (NeoObject) wPostoNormal.findValue("centroCusto");
		NeoObject contaFinanceira = (NeoObject) wPostoNormal.findValue("contaFinanceira");
		wPostoProtecaoGarantida.setValue("contaFinanceira", contaFinanceira);
		
		NeoObject contaContabil = (NeoObject) wPostoNormal.findValue("contaContabil");
		wPostoProtecaoGarantida.setValue("contaContabil", contaContabil);

		NeoObject enderecoPosto = (NeoObject) wPostoNormal.findValue("enderecoPosto");
		wPostoProtecaoGarantida.setValue("enderecoPosto", enderecoPosto);
		
		Boolean endEfetivoIgualPosto = wPostoNormal.findValue("endEfetivoIgualPosto") != null ? (Boolean) wPostoNormal.findValue("endEfetivoIgualPosto") : (Boolean) null;
		wPostoProtecaoGarantida.setValue("endEfetivoIgualPosto", endEfetivoIgualPosto);
		
		String valorISS = NeoUtils.safeOutputString(wPostoNormal.findValue("valorISS"));
		ContratoLogUtils.logInfo("valorISS: " + valorISS);
		wPostoProtecaoGarantida.setValue("valorISS", valorISS);
		
		NeoObject transacao = (NeoObject) wPostoNormal.findValue("transacao");
		wPostoProtecaoGarantida.setValue("transacao", transacao);
		
		NeoObject enderecoEfetivo = (NeoObject) wPostoNormal.findValue("enderecoEfetivo");
		wPostoProtecaoGarantida.setValue("enderecoEfetivo", enderecoEfetivo);
		
		Boolean postoLiberadoFaturamento = wPostoNormal.findValue("postoLiberadoFaturamento") != null ? (Boolean) wPostoNormal.findValue("postoLiberadoFaturamento") : (Boolean) null;
		wPostoProtecaoGarantida.setValue("postoLiberadoFaturamento", postoLiberadoFaturamento);
		
		Boolean suspenderFaturamento = wPostoNormal.findValue("suspenderFaturamento") != null ? (Boolean) wPostoNormal.findValue("suspenderFaturamento") : (Boolean) null;
		wPostoProtecaoGarantida.setValue("suspenderFaturamento", false);
		
		Boolean aprovacaoDirecao = wPostoNormal.findValue("aprovacaoDirecao") != null ? (Boolean) wPostoNormal.findValue("aprovacaoDirecao") : (Boolean) null;
		wPostoProtecaoGarantida.setValue("aprovacaoDirecao", aprovacaoDirecao);
		
		Boolean geraComissaoVendas = wPostoNormal.findValue("geraComissaoVendas") != null ? (Boolean) wPostoNormal.findValue("geraComissaoVendas") : (Boolean) null;
		wPostoProtecaoGarantida.setValue("geraComissaoVendas", geraComissaoVendas);
		
		Boolean geraPagamentoInstalacao = wPostoNormal.findValue("geraPagamentoInstalacao") != null ? (Boolean) wPostoNormal.findValue("geraPagamentoInstalacao") : (Boolean) null;
		wPostoProtecaoGarantida.setValue("geraPagamentoInstalacao", geraPagamentoInstalacao);
		
		NeoObject periodoFaturamento = (NeoObject) wPostoNormal.findValue("periodoFaturamento");
		wPostoProtecaoGarantida.setValue("periodoFaturamento", periodoFaturamento);
		
		//Boolean aberturaOSHabilitacao = wPostoNormal.findValue("aberturaOSHabilitacao") != null ? (Boolean) wPostoNormal.findValue("aberturaOSHabilitacao") : (Boolean) null;
		//wPostoProtecaoGarantida.setValue("aberturaOSHabilitacao", aberturaOSHabilitacao);
		
		NeoObject buscaOperadorCM = (NeoObject) wPostoNormal.findValue("buscaOperadorCM");
		wPostoProtecaoGarantida.setValue("buscaOperadorCM", buscaOperadorCM);
		
		//Boolean aberturaOSInstalacao = wPostoNormal.findValue("aberturaOSInstalacao") != null ? (Boolean) wPostoNormal.findValue("aberturaOSInstalacao") : (Boolean) null;
		//wPostoProtecaoGarantida.setValue("aberturaOSInstalacao", aberturaOSInstalacao);
		
		//Boolean aberturaOSReinstalacao = wPostoNormal.findValue("aberturaOSReinstalacao") != null ? (Boolean) wPostoNormal.findValue("aberturaOSReinstalacao") : (Boolean) null;
		//wPostoProtecaoGarantida.setValue("aberturaOSReinstalacao", aberturaOSReinstalacao);
		
		// aba de vigilanca eletronica vai vir em branco
		NeoObject oVigilanciaEletronica = AdapterUtils.createNewEntityInstance("FGCVigilanciaEletronica");
		EntityWrapper wVigilanciaEletronica = new EntityWrapper(oVigilanciaEletronica);
		wVigilanciaEletronica.setValue("valorInstalacao", new BigDecimal(0L));
		NeoObject parcelas = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FGCMaxParcelas"),
					new QLGroupFilter("AND", new QLEqualsFilter("codigo", 1L))
				);
		wVigilanciaEletronica.setValue("parcelas", parcelas);
		wVigilanciaEletronica.setValue("observacao", "Posto de proteção garantida");
		wVigilanciaEletronica.setValue("responsavel", " ");
		wVigilanciaEletronica.setValue("cpfResponsavel", "0");
		PersistEngine.persist(oVigilanciaEletronica);
		wPostoProtecaoGarantida.setValue("vigilanciaEletronica", oVigilanciaEletronica);
		
		wPostoProtecaoGarantida.setValue("aberturaOSHabilitacao", false);
		wPostoProtecaoGarantida.setValue("aberturaOSInstalacao", false);
		wPostoProtecaoGarantida.setValue("aberturaOSReinstalacao", false);
		
		
		PersistEngine.persist(postoProtecaoGarantida);
		ContratoLogUtils.logInfo("copia encerrada.");
		
		
	}

}
