package com.neomind.fusion.custom.orsegups.mobile;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.neomind.fusion.custom.orsegups.mobile.vo.ClienteVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.datawarehouse.NeoDataSource;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;

/**
 * Essa classe não é mais utilizada. Utilizar a mesma classe do pacote send
 * @author neomind
 *
 */
@Deprecated
public class SendClientes extends HttpServlet   {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	XmlBuilder xml = new XmlBuilder();
	
	public String clientes() throws SQLException{
		String json = null;
		NeoDataSource data = (NeoDataSource) PersistEngine.getObject(NeoDataSource.class,
				new QLEqualsFilter("name", "SAPIENS"));

		Connection con =  OrsegupsUtils.openConnection(data);

		String SQLCliente = " SELECT * FROM USU_INSP"; //ORDER";
		//BY EMPRESA, CLIENTE, LOTACAO ";
		// Prepara o statement
		Statement stm = null;
	
		ArrayList<ClienteVO> list = new ArrayList<ClienteVO>();	
		try {
			stm = (Statement) con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		ResultSet rs = stm.executeQuery(SQLCliente);
		ArrayList<String> teste = new ArrayList<String>();
		
		while(rs.next()){
			
							
				try{
		
				ClienteVO cvo = new ClienteVO();
				cvo.setCodCliente(String.valueOf(rs.getRow()));
				cvo.setCodEmpresa(String.valueOf(rs.getRow()));
				cvo.setCodLotacao(String.valueOf(rs.getRow()));
				cvo.setCodPosto(String.valueOf(rs.getRow()));
				cvo.setNome_cliente(rs.getString("CLIENTE"));
				cvo.setNome_Empresa(rs.getString("EMPRESA"));
				cvo.setNomeLotacao(rs.getString("LOTACAO"));
				cvo.setNomePosto(rs.getString("POSTO"));
				
				list.add(cvo);

				
          
			
      } catch(Exception e ){
    	  System.out.println(e.getMessage());
      }

		
		
		}
		Gson gson = new Gson();
		json = gson.toJson(list);
		
		return json;
		
	}
	
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		
		response.setContentType("application/json");
		 
		 PrintWriter out = response.getWriter();
		
			try {
				out.print(clientes());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
				 }

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		response.setContentType("application/json");
		 
		 PrintWriter out = response.getWriter();
		
			try {
				out.print(clientes());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
				 }
			
			
	}	
	
	
	
	
	
	


