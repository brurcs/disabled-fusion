package com.neomind.fusion.custom.orsegups.processoJuridico.tributario.adapter;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.lang.NumberUtils;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter-IniciarTarefasListaPedidosAdapter

public class IniciarTarefasListaPedidosAdapter implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
	    try{
		List<NeoObject> listaPedidos = null;
		if (String.valueOf(processEntity.getValue("grauProcesso")).contains("1º"))
		{

			listaPedidos = (List<NeoObject>) processEntity.getValue("lisPed");
		}
		else if (String.valueOf(processEntity.getValue("grauProcesso")).contains("2º"))
		{

			listaPedidos = (List<NeoObject>) processEntity.getValue("lisPedSegGrau");
		}
		else if (String.valueOf(processEntity.getValue("grauProcesso")).contains("3º"))
		{

		}

		if (listaPedidos != null && listaPedidos.size() > 0)
		{
			String solicitante = "rosemeire.svenar";
			String executorTar = "";
			String titulo = "";
			String descricao = "";
			GregorianCalendar prazo = null;
			for (NeoObject pedido : listaPedidos)
			{
				EntityWrapper wPedido = new EntityWrapper(pedido);
				List<NeoObject> listaTarefas = (List<NeoObject>) wPedido.findValue("lisTar");
				List<NeoObject> listaTarefasNova = new ArrayList<>();
				if (listaTarefas != null && listaTarefas.size() > 0)
				{
					for (NeoObject tarefa : listaTarefas)
					{
						EntityWrapper wTarefa = new EntityWrapper(tarefa);
						if (wTarefa.findValue("executorTarNov") != null && !String.valueOf(wTarefa.findValue("executorTarNov")).equals(""))
						{
							NeoUser executor = (NeoUser) wTarefa.findValue("executorTarNov");
							executorTar = executor.getCode();
							if (wTarefa.getValue("titulo") == null || String.valueOf(wTarefa.getValue("titulo")).equals(""))
							{
								if (processEntity.getValue("colaborador") != null)
								{
									NeoObject colaborador = (NeoObject) processEntity.getValue("colaborador");
									EntityWrapper wColaborador = new EntityWrapper(colaborador);
									colaborador = (NeoObject) processEntity.getValue("colaborador");
									NeoObject tipPed = (NeoObject) wPedido.findValue("tipoPedido");
									EntityWrapper tipoPedido = new EntityWrapper(tipPed);
									String descricaoTipo = String.valueOf(tipoPedido.findValue("descricao"));

									wTarefa.setValue("titulo", "J002 - " + activity.getCode() + " - " + wColaborador.getValue("numemp") + "/" + wColaborador.getValue("numcad") + " - " + wColaborador.getValue("nomfun") + " - " + descricaoTipo);
									titulo = String.valueOf(wTarefa.getValue("titulo"));
								}
								else
								{
									NeoObject autorNaoColaborador = (NeoObject) processEntity.getValue("autorNaoCol");
									EntityWrapper wAutor = new EntityWrapper(autorNaoColaborador);
									String cpfAut = String.valueOf(wAutor.getValue("cpfAut"));
									String nomAut = String.valueOf(wAutor.getValue("nomAut"));
									wTarefa.setValue("titulo", titulo = cpfAut + " / " + nomAut);
									titulo = String.valueOf(wTarefa.getValue("titulo"));
								}
							}
							else
							{
								titulo = String.valueOf(wTarefa.findValue("titulo"));
							}
							descricao = String.valueOf(wTarefa.findValue("descricao"));
							prazo = (GregorianCalendar) wTarefa.findValue("prazo");
							NeoFile anexo = (NeoFile) wTarefa.findValue("anexo");

							if (wTarefa.findValue("neoIdTarefa") == null)
							{
								IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
								String codeTarefa = iniciarTarefaSimples.abrirTarefaReturnNeoId(solicitante, executorTar, titulo, descricao, "2", "sim", prazo, anexo);
								
								if (NeoUtils.safeIsNotNull(codeTarefa) && !codeTarefa.trim().equals("null"))
								{
									try
									{
									    	wTarefa.setValue("neoIdTarefa", new Long(codeTarefa.trim())); 
									    	PersistEngine.persist(tarefa);									    
									}
									catch (Exception e)
									{
										GregorianCalendar dataAtual = new GregorianCalendar();
										if (!OrsegupsUtils.isWorkDay(prazo))
										{
											e.printStackTrace();
											throw new WorkflowException("Prazo informado deve ter um dia útil! ");
										}
										else if ((prazo.before(dataAtual)) || (prazo.equals(dataAtual)))
										{
											e.printStackTrace();
											throw new WorkflowException("Prazo informado deve ser de pelo menos 1 dia!");
										}
										else
										{
											e.printStackTrace();
											throw new WorkflowException("O usuário executor da tarefa de Convocar Testemunha esta inativo!");
										}
									}
								}
							}
							listaTarefasNova.add(tarefa);
						}
					}
				}
				wPedido.setValue("lisTar", listaTarefasNova);
			}
		}

		NeoObject objRito = (NeoObject) processEntity.getValue("rito");
		if (objRito != null) {
			
			EntityWrapper wRito = new EntityWrapper(objRito);
			String descricao = String.valueOf(wRito.getValue("descricao"));
			if (descricao.equalsIgnoreCase("Ordinário"))
			{
			    processEntity.setValue("countAtas", 2L);
			}
			else
			{
			    processEntity.setValue("countAtas", 1L);
			}
		}
	    }catch(Exception e){
		System.out.println("Erro na classe IniciarTarefasListaPedidosAdapter do fluxo J002.");
		e.printStackTrace();
		throw new WorkflowException(e.getMessage());
	    }
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}
}
