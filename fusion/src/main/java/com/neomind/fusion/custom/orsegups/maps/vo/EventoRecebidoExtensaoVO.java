package com.neomind.fusion.custom.orsegups.maps.vo;

import java.util.Date;

import br.com.segware.sigmaWebServices.webServices.EventoRecebido;

public class EventoRecebidoExtensaoVO {
    
    private String servidorCFTV;
    
    private String portaCFTV;
    
    private String userCFTV;
    
    private String senhaCFTV;
    
    private boolean armada;
    
    private Date dtUltimoArme;
    
    private EventoRecebido evento;
    
    private String code;
    
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getServidorCFTV() {
        return servidorCFTV;
    }

    public void setServidorCFTV(String servidorCFTV) {
        this.servidorCFTV = servidorCFTV;
    }

    public String getPortaCFTV() {
        return portaCFTV;
    }

    public void setPortaCFTV(String portaCFTV) {
        this.portaCFTV = portaCFTV;
    }

    public String getUserCFTV() {
        return userCFTV;
    }

    public void setUserCFTV(String userCFTV) {
        this.userCFTV = userCFTV;
    }

    public String getSenhaCFTV() {
        return senhaCFTV;
    }

    public void setSenhaCFTV(String senhaCFTV) {
        this.senhaCFTV = senhaCFTV;
    }
    
    public boolean isArmada() {
        return armada;
    }

    public void setArmada(boolean armada) {
        this.armada = armada;
    }

    public Date getDtUltimoArme() {
        return dtUltimoArme;
    }

    public void setDtUltimoArme(Date dtUltimoArme) {
        this.dtUltimoArme = dtUltimoArme;
    }

    public EventoRecebido getEvento() {
        return evento;
    }

    public void setEvento(EventoRecebido evento) {
        this.evento = evento;
    }
    
    
    

}
