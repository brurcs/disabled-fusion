package com.neomind.fusion.custom.orsegups.contract;

import java.util.ArrayList;
import java.util.Collection;

public class MinutaDataSource
{
	private Collection<ListaEquipamentosMinutaDataSource> listaEquipamentos1 = new ArrayList<ListaEquipamentosMinutaDataSource>();
	private Collection<ListaEquipamentosMinutaDataSource> listaEquipamentos2 = new ArrayList<ListaEquipamentosMinutaDataSource>();
	private Collection<ListaAnexosMinutaDataSource> listaAnexos = new ArrayList<ListaAnexosMinutaDataSource>();
	
	
	public Collection<ListaEquipamentosMinutaDataSource> getListaEquipamentos1()
	{
		return listaEquipamentos1;
	}
	public void setListaEquipamentos1(Collection<ListaEquipamentosMinutaDataSource> listaEquipamentos1)
	{
		this.listaEquipamentos1 = listaEquipamentos1;
	}
	public Collection<ListaEquipamentosMinutaDataSource> getListaEquipamentos2()
	{
		return listaEquipamentos2;
	}
	public void setListaEquipamentos2(Collection<ListaEquipamentosMinutaDataSource> listaEquipamentos2)
	{
		this.listaEquipamentos2 = listaEquipamentos2;
	}
	public Collection<ListaAnexosMinutaDataSource> getListaAnexos()
	{
		return listaAnexos;
	}
	public void setListaAnexos(Collection<ListaAnexosMinutaDataSource> listaAnexos)
	{
		this.listaAnexos = listaAnexos;
	}
	
}
