package com.neomind.fusion.custom.orsegups.konviva;

import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.konviva.dao.KonvivaUtils;
import com.neomind.fusion.custom.orsegups.konviva.vo.ColaboradorKonvivaVO;
import com.neomind.fusion.custom.orsegups.konviva.vo.RetornoKonvivaVO;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class KonvivaIntegracao implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(KonvivaIntegracao.class);

	@Override
	public void execute(CustomJobContext arg0)
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		try
		{
			System.out.println("##### PROCESSO DE INTEGRAÇÃO COM O SISTEMA KONVIVA INICIADO - Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
			log.warn("##### PROCESSO DE INTEGRAÇÃO COM O SISTEMA KONVIVA INICIADO - Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
			List<ColaboradorKonvivaVO> colaboradores = KonvivaUtils.lstColaboradorKonviva();
			if (colaboradores != null && colaboradores.size() > 0)
			{
				for (ColaboradorKonvivaVO colaborador : colaboradores)
				{
					String cpf = colaborador.getDsCPF();
					while (cpf.length() < 11)
					{
						cpf = "0" + cpf;
					}
					String retorno = jsonFromURLGet("http://orsegups.konviva.com.br/action/api/getUsuarioByCPF/" + cpf);
					System.out.println(retorno);

					if (retorno != null)
					{

						InstantiableEntityInfo usuariosIntegradosKonviva = AdapterUtils.getInstantiableEntityInfo("usuariosIntegradosKonviva");
						NeoObject objUsuariosIntegradosKonviva = usuariosIntegradosKonviva.createNewInstance();
						EntityWrapper wrapperUsuariosIntegradosKonviva = new EntityWrapper(objUsuariosIntegradosKonviva);

						wrapperUsuariosIntegradosKonviva.findField("cpf").setValue(new Long(colaborador.getDsCPF()));

						PersistEngine.persist(objUsuariosIntegradosKonviva);

						Gson gson = new Gson();
						RetornoKonvivaVO rkvo = gson.fromJson(retorno, RetornoKonvivaVO.class);
						if (rkvo != null && rkvo.getMessage() != null)
						{
							InstantiableEntityInfo errosVerificados = AdapterUtils.getInstantiableEntityInfo("ErrosIntegracaoKonviva");
							NeoObject objErrosVerificador = errosVerificados.createNewInstance();
							EntityWrapper wrapperErrosVerificador = new EntityWrapper(objErrosVerificador);

							wrapperErrosVerificador.findField("matricula").setValue(colaborador.getCdMatricula());
							wrapperErrosVerificador.findField("cpf").setValue(colaborador.getDsCPF());
							wrapperErrosVerificador.findField("nome").setValue(colaborador.getNomeFuncionario());
							wrapperErrosVerificador.findField("erro").setValue(rkvo.getMessage());

							PersistEngine.persist(objErrosVerificador);
						}
						else
						{
							String urlIntegracao = "http://orsegups.konviva.com.br/action/api/integrarUsuario";
							String retorno2 = integrar(urlIntegracao, retorno, colaborador);
							System.out.println(retorno2);
						}
					}
				}
			}
			else
			{
				ArrayList<NeoObject> listaUsuarios = (ArrayList<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("usuariosIntegradosKonviva"));
				if (listaUsuarios != null && !listaUsuarios.isEmpty())
				{
					for (NeoObject neo : listaUsuarios)
					{
						EntityWrapper usuario = new EntityWrapper(neo);
						GregorianCalendar dataProcessamento = (GregorianCalendar) usuario.findField("dataProcessamento").getValue();
						if (dataProcessamento != null)
						{
							dataProcessamento.set(Calendar.HOUR_OF_DAY, 0);
							dataProcessamento.set(Calendar.MINUTE, 0);
							dataProcessamento.set(Calendar.SECOND, 0);
							dataProcessamento.set(Calendar.MILLISECOND, 0);

							GregorianCalendar dataAtual = new GregorianCalendar();
							dataAtual.add(Calendar.DAY_OF_MONTH, 1);
							dataAtual.set(Calendar.HOUR_OF_DAY, 0);
							dataAtual.set(Calendar.MINUTE, 0);
							dataAtual.set(Calendar.SECOND, 0);
							dataAtual.set(Calendar.MILLISECOND, 0);

							if (dataAtual.after(dataProcessamento))
							{
								@SuppressWarnings("unchecked")
								ArrayList<NeoObject> clone = (ArrayList<NeoObject>) listaUsuarios.clone();
								PersistEngine.removeObjects(clone);
							}

						}

					}
				}

			}
			log.warn("##### PROCESSO DE INTEGRAÇÃO COM O SISTEMA KONVIVA FINALIZADO - Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
			System.out.println("##### PROCESSO DE INTEGRAÇÃO COM O SISTEMA KONVIVA FINALIZADO - Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		}
		catch (Exception e)
		{
			System.out.println("[" + key + "] Integração Konviva erro no processamento: " + e.getMessage());
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		}
	}

	/**
	 * Metodo para buscar dados do usuário via requisição
	 * 
	 * @param url contendo o cpf do colaborador
	 * @return retorna json com dados do colaborador
	 */
	public static String jsonFromURLGet(String url)
	{
		String jsonResponse = null;
		if (!"".equals(url))
		{
			try
			{
				URL object = new URL(url);
				//Cria uma nova conexão baseada da URL recebida
				HttpURLConnection connection = (HttpURLConnection) object.openConnection();

				//seta os tempos para a conexão expirar
				connection.setReadTimeout(60 * 1000);
				connection.setConnectTimeout(60 * 1000);
				//seta a chave de autorização                  
				//				connection.setRequestProperty("Authorization", "KONVIVA fb3ed04c831ff39f2eec14c5d661dbc3");
				connection.setRequestProperty("Authorization", "KONVIVA 77d3ebd9b8f44ab74615f4814470e8a0");
				//seta o tipo de conteudo para JSON
				connection.setRequestProperty("Content-Type", "application/json");
				//le a resposta da solicitacao de conexão
				int responseCode = connection.getResponseCode();
				if (responseCode == 200)
				{
					InputStream inputStr = connection.getInputStream();
					String encoding = connection.getContentEncoding() == null ? "UTF-8" : connection.getContentEncoding();
					jsonResponse = IOUtils.toString(inputStr, encoding);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();

			}
		}
		return jsonResponse;
	}

	/**
	 * Metodo que faz a integracao com o sistema da Konviva
	 * 
	 * @param url para a efetuar a integracao
	 * @param retorno da pesquisa de usuário contendo os dados do usuário
	 * @return
	 */
	public static String integrar(String url, String retorno, ColaboradorKonvivaVO colaborador)
	{
		String jsonResponse = null;
		if (!"".equals(url))
		{
			try
			{
				URL object = new URL(url);
				//abre uma nova conexão baseada na url recebida
				HttpURLConnection connection = (HttpURLConnection) object.openConnection();

				//indica o tipo de requisição, no caso PUT (uma requisicao de atualizacao)
				connection.setRequestMethod("PUT");
				connection.setDoOutput(true);

				//indica o tempo para a conexao expirar e a chave de segurança
				connection.setReadTimeout(60 * 1000);
				connection.setConnectTimeout(60 * 1000);
				//				connection.setRequestProperty("Authorization", "KONVIVA fb3ed04c831ff39f2eec14c5d661dbc3");
				connection.setRequestProperty("Authorization", "KONVIVA 77d3ebd9b8f44ab74615f4814470e8a0");
				connection.setRequestProperty("Content-Type", "application/json");

				//indica o tipo de parametro a ser passado
				connection.setRequestProperty("Accept", "application/json");

				//abre um novo escritor de parametros
				OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");

				//aqui eu pego o login do colaborador conforme o retorno
				String array[] = retorno.split(",");
				String arrayLogin[] = array[15].split(":");
				String login = arrayLogin[1].replace("\"", "");

				//creio que aqui voce pode alterar a situacao do colaborador conforme vetorh
				String situacao = "ATIVO";
				String cdMatricula = colaborador.getCdMatricula().toString();
				String cdCentroCusto = colaborador.getCdCentroCusto().toString();
				String escala = "";
				String dsCentroCusto = colaborador.getDsCentroCusto();
				String dsfilial = colaborador.getDsFilial();
				String dsPosto = colaborador.getDsPosto();
				String dsRegional = colaborador.getDsRegional();
				String dsSupervidor = colaborador.getDsSupervisor();

				out.write("{\"Login\":\"" + login + "\",\"Situacao\":\"" + situacao + "\",\"CamposExtras\":{ \"Lider_Imediato\":\"" + dsSupervidor + "\", \"Matricula\":\"" + cdMatricula + "\" , \"CENTRO_CURSO\":\"" + cdCentroCusto + "\" , \"ESCALA\" : \"" + escala + "\", \"DESCRICAO_CENTRO_CURSO\" : \"" + dsCentroCusto + "\", \"FILIAL\" : \"" + dsfilial + "\", \"CODIGO_REGIONAL\" : \"" + dsPosto + "\", \"NOME_REGIONAL\" : \"" + dsRegional + "\"}}");
				out.flush();
				out.close();

				int responseCode = connection.getResponseCode();
				if (responseCode == 200)
				{
					InputStream inputStr = connection.getInputStream();
					String encoding = connection.getContentEncoding() == null ? "UTF-8" : connection.getContentEncoding();
					jsonResponse = IOUtils.toString(inputStr, encoding);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return jsonResponse;
	}

}
