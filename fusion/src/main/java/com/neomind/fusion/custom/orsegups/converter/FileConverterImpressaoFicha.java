package com.neomind.fusion.custom.orsegups.converter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.FileConverter;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.portal.PortalUtil;

public class FileConverterImpressaoFicha extends FileConverter
{
	private static final Log log = LogFactory.getLog(FileConverterImpressaoFicha.class);

	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin)
	{

		// Botão para imprimir a ficha - via fluxo ou consulta

		Long idPai = field.getForm().getObjectId();
		if (field.getFieldCode().equals("impressaoFicha"))
		{
			String servlet = PortalUtil.getBaseURL() + "custom/jsp/orsegups/FichaServletImpressao.jsp?idPai=" + idPai;
			String servletFicha = servlet + "&autorizacao=false";
			String servletAut = servlet + "&autorizacao=true";
			
			if(((com.neomind.fusion.entity.dynamic.DynamicEntityInfo) field.getForm().getObject().getInfo()).getEntityClassName().equals("com.neomind.fusion.entity.dyn.FUEProcessoFichaUniformeEPI")){
				
				return "<script>$('label[for*=__impressaoFicha__]').hide();</script><div align='center'><a class='acao' id='imprimirFicha' onClick='javascript:window.open(\"" + servletFicha + "\",\"Imprimir\",\"status=0, resizable=0, scrollbars=NO, width=100, height=75\");' title='Imprimir Ficha'>" + "<img src='imagens/custom/ico_20_dash_imp.gif' alt='Imprimir Ficha' align='absmiddle' />" + "&nbsp;" + "Imprimir Ficha</a></div>" + "&nbsp;" + 
						"<script>$('label[for*=__impressaoAutorizacao__]').hide();</script><div align='center'><a class='acao' id='imprimirAutorizacao' onClick='javascript:window.open(\"" + servletAut + "\",\"Imprimir\",\"status=0, resizable=0, scrollbars=NO, width=100, height=75\");' title='Imprimir Autorização Retirada'>" + "<img src='imagens/custom/ico_20_dash_imp.gif' alt='Imprimir Autorização Retirada' align='absmiddle' />" + "&nbsp;" + "Imprimir Autorização Retirada</a></div>";
			}else{
				return "<script>$('label[for*=__impressaoFicha__]').hide();</script><div align='center'><a class='acao' id='imprimirFicha' onClick='javascript:window.open(\"" + servletFicha + "\",\"Imprimir\",\"status=0, resizable=0, scrollbars=NO, width=100, height=75\");' title='Imprimir Ficha'>" + "<img src='imagens/custom/ico_20_dash_imp.gif' alt='Imprimir Ficha' align='absmiddle' />" + "&nbsp;" + "Imprimir Ficha</a></div>";
			}

		}

		return super.getHTMLInput(field, origin);
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		// Botão para imprimir a ficha - via fluxo ou consulta

		Long idPai = field.getForm().getObjectId();
		if (field.getFieldCode().equals("impressaoFicha"))
		{
			String servlet = PortalUtil.getBaseURL() + "custom/jsp/orsegups/FichaServletImpressao.jsp?idPai=" + idPai;
			String servletFicha = servlet + "&autorizacao=false";
			String servletAut = servlet + "&autorizacao=true";
			
			if(((com.neomind.fusion.entity.dynamic.DynamicEntityInfo) field.getForm().getObject().getInfo()).getEntityClassName().equals("com.neomind.fusion.entity.dyn.FUEProcessoFichaUniformeEPI")){
				return "<script>$('label[for*=__impressaoFicha__]').hide();</script><div align='center'><a class='acao' id='imprimirFicha' onClick='javascript:window.open(\"" + servletFicha + "\",\"Imprimir\",\"status=0, resizable=0, scrollbars=NO, width=100, height=75\");' title='Imprimir Ficha'>" + "<img src='imagens/custom/ico_20_dash_imp.gif' alt='Imprimir Ficha' align='absmiddle' />" + "&nbsp;" + "Imprimir Ficha</a></div>" + "&nbsp;" + 
						"<script>$('label[for*=__impressaoAutorizacao__]').hide();</script><div align='center'><a class='acao' id='imprimirAutorizacao' onClick='javascript:window.open(\"" + servletAut + "\",\"Imprimir\",\"status=0, resizable=0, scrollbars=NO, width=100, height=75\");' title='Imprimir Autorização Retirada'>" + "<img src='imagens/custom/ico_20_dash_imp.gif' alt='Imprimir Autorização Retirada' align='absmiddle' />" + "&nbsp;" + "Imprimir Autorização Retirada</a></div>";
			}else{
				return "<script>$('label[for*=__impressaoFicha__]').hide();</script><div align='center'><a class='acao' id='imprimirFicha' onClick='javascript:window.open(\"" + servletFicha + "\",\"Imprimir\",\"status=0, resizable=0, scrollbars=NO, width=100, height=75\");' title='Imprimir Ficha'>" + "<img src='imagens/custom/ico_20_dash_imp.gif' alt='Imprimir Ficha' align='absmiddle' />" + "&nbsp;" + "Imprimir Ficha</a></div>";
			}
		}

		return super.getHTMLView(field, origin);
	}

}
