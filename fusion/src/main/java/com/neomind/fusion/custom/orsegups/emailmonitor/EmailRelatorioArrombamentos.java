package com.neomind.fusion.custom.orsegups.emailmonitor;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.MultiPartEmail;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsRelatoriosUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class EmailRelatorioArrombamentos implements CustomJobAdapter{

	private static final Log log = LogFactory.getLog(EmailRelatorioArrombamentos.class);

	@SuppressWarnings("unchecked")
	@Override
	public void execute(CustomJobContext ctx){
		try{
			GregorianCalendar dataInicial = new GregorianCalendar();
			dataInicial.add(Calendar.HOUR_OF_DAY, -4);
			GregorianCalendar dataFinal = new GregorianCalendar();

			//buscar lista de destinatários por regional
			List<NeoObject> listRegionais = PersistEngine.getObjects(AdapterUtils.getEntityClass("GCEscritorioRegional"));

			for (NeoObject regional : listRegionais){
				EntityWrapper ewRegional = new EntityWrapper(regional);
				List<NeoPaper> listaPapeis = (List<NeoPaper>) ewRegional.findField("papelResponsaveis").getValue();
				String siglaRegional = (String) ewRegional.findField("siglaRegional").getValue();

				
				ArrayList<String> destinatarios = new ArrayList<>();

				destinatarios.add("rodrigo.fontoura@orsegups.com.br"); 
				destinatarios.add("suelen.machado@orsegups.com.br"); 
				destinatarios.add("deborah.silva@orsegups.com.br");
				for (NeoPaper papel : listaPapeis)
				{
					EntityWrapper ewPapel = new EntityWrapper(papel);
					Set<NeoUser> listaUsuarios = (Set<NeoUser>) ewPapel.findField("users").getValue();
					for (NeoUser usuario : listaUsuarios)
					{
						EntityWrapper ewUsuario = new EntityWrapper(usuario);
						String email = (String) ewUsuario.findField("email").getValue();
						if (email != null && !email.isEmpty() && usuario.isActive() && !destinatarios.contains(email))
						{
							destinatarios.add(email);
							
						}
					}
				}
				if (!destinatarios.isEmpty()){

					File arqRelatorio = OrsegupsRelatoriosUtils.getRelatorioArrombamentos(siglaRegional, dataInicial, dataFinal, null, true);
					File arqRelatorioPGO = OrsegupsRelatoriosUtils.getRelatorioArrombamentosClientePGO(siglaRegional, dataInicial, dataFinal, null, true);
					if (arqRelatorio != null){
					    
					    log.error("RELATÓRIO DE ARROMBAMENTO ARQUIVO GERADO INICIOU ENVIO EMAIL "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));  
					    
						SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");

						String datIni = df.format(dataInicial.getTime());
						String datFim = df.format(dataFinal.getTime());
						
						StringBuilder html = new StringBuilder();
						
						String portal = PortalUtil.getBaseURL();
												
						html.append(" <!DOCTYPE html> ");
						html.append(" <html> ");
						html.append(" <head> ");
						html.append(" <title></title> ");
						html.append("  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=\"iso-8859-1\" /> ");
						html.append(" <link href=\""+portal+"/css/portal-decorator.css\" rel=\"stylesheet\" type=\"text/css\" /> ");
						html.append(" <style TYPE=\"text/css\"> "); 
						html.append(" p,pre,body,table ");   
						html.append(" { ");
						html.append(" font-size:10.0pt; ");
						html.append(" font-family:'Arial'; ");
						html.append(" } ");
						html.append(" </style> ");
						html.append(" </head> ");
						html.append(" <body bgcolor=\"#FFFFFF\"> ");
						html.append(" <table id=\"Table_01\" width=\"550\" border=\"0\" cellpadding=\"0\" ");
						html.append(" cellspacing=\"0\"> ");
						html.append(" <tr> ");
						html.append(" <td class=\"corpo_mail\"> ");
						html.append(" <p class=\"mail_tit\"><mail:title title='Relatório de Arrombamentos' /></p> ");
						html.append(" </td> ");
						html.append(" </tr>	");
						html.append(" <tr> ");
						html.append(" <td class=\"corpo_mail\" align=\"left\"> ");
						html.append(" <p> Em anexo segue o relatório de arrombamentos do período de <strong>"+datIni+"</strong> até <strong>"+datFim+"</strong>.</p> ");
						html.append(" <br>  ");

						html.append(" Orsegups Participações S/A<br/> ");
						html.append(" <a href=\"http://www.orsegups.com.br\">http://www.orsegups.com.br</a> "); 
									
						html.append(" </td> "); 
						html.append(" </tr> ");
							
						html.append(" </table> ");
						html.append(" </body> ");
						html.append(" </html> ");
						
						
						/**
						 * Novo Envio
						 */
						
				
						EmailAttachment attachment = new EmailAttachment();
						attachment.setPath(arqRelatorio.getPath());
						attachment.setDisposition(EmailAttachment.ATTACHMENT);
						attachment.setName("Relatorio_Arrombamentos_" + siglaRegional+".pdf");
					
						MultiPartEmail email = new HtmlEmail();
						email.setHostName("SSOOMX01");
						email.setSmtpPort(25);

						try{

                        			    for (String to : destinatarios) {
                        				email.addTo(to);
                        			    }
                        			    
                        			    email.addCc("emailautomatico@orsegups.com.br");
                        
                        			    email.setFrom("fusion@orsegups.com.br");
                        			    email.setSubject("Relatório de Arrombamentos");
                        			    email.setMsg(html.toString());
                        			    email.attach(attachment);

                        			    email.send();
                        
                        			}
						catch (EmailException e){
							e.printStackTrace();
						}
						
						/**
						 * Fim novo envio
						 */
						
						if (arqRelatorioPGO != null){
						    /**
                        			     * Novo Envio
                        			     */
                        
                        			    attachment = new EmailAttachment();
                        			    attachment.setPath(arqRelatorioPGO.getPath());
                        			    attachment.setDisposition(EmailAttachment.ATTACHMENT);
                        			    attachment.setName("Relatorio_Arrombamentos_" + siglaRegional + ".pdf");
                        
                        			    email = new HtmlEmail();
                        			    email.setHostName("SSOOMX01");
                        			    email.setSmtpPort(25);
                        
                        			    try {
                                            			
                        				email.addCc("emailautomatico@orsegups.com.br");
                        				email.addCc("protecaogarantida@orsegups.com.br");
                        				
                        				email.setFrom("fusion@orsegups.com.br");
                        				email.setSubject("Relatório de Arrombamentos");
                        				email.setMsg(html.toString());
                        				email.attach(attachment);
                        
                        				email.send();
                        
                        			    } catch (EmailException e) {
                        				e.printStackTrace();
                        			    }
                        
                        			    /**
                        			     * Fim novo envio
                        			     */
						}
						log.warn("LOG RELATÓRIO DE ARROMBAMENTO - Destinatários: " + destinatarios+".pdf");
					}
				}else{
				    log.error("RELATÓRIO DE ARROMBAMENTO LISTA DESTINATARIOS VAZIA. "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));  
				}
			}
		}
		catch (Exception ex){
			Long key = GregorianCalendar.getInstance().getTimeInMillis();
			log.error("["+key+"] Erro na rotina de arrombamentos." +ex.getMessage());
			ex.printStackTrace();
			throw new JobException("Erro na rotina, procurar no log por ["+key+"]");
		}
	}

	
}
