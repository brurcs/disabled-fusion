package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.contract.ContratoUtils;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.UserActivity;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoUtils;

public class C035VerificaDebitoAutomaticoAceito implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		System.out.println("[C035] tarefa:["+origin.getCode()+"]  - Verificando Débito automático.");
		Boolean bancoAceitou = NeoUtils.safeBoolean(wrapper.findValue("bancoAceitou"));
		if (wrapper.findValue("bancoAceitou") != null){
			
			NeoObject oContrato = (NeoObject) wrapper.findValue("contrato");
			
			EntityWrapper wContrato = new EntityWrapper(oContrato);

			Long usu_numctr = NeoUtils.safeLong(NeoUtils.safeOutputString(wContrato.findValue("usu_numctr"))); 
			Long usu_codemp = NeoUtils.safeLong(NeoUtils.safeOutputString(wContrato.findValue("usu_codemp")));
			Long usu_codfil = NeoUtils.safeLong(NeoUtils.safeOutputString(wContrato.findValue("usu_codfil")));
			Long usu_codfpg = NeoUtils.safeLong(NeoUtils.safeOutputString(wContrato.findValue("codfpg")));
			
			if (bancoAceitou){
				System.out.println("[C035] tarefa:["+origin.getCode()+"] - Banco Aceitou");
				
				processaDesconto(usu_numctr);
				abrirTarefaSimples(wrapper);
			}else{
				System.out.println("[C035] tarefa:["+origin.getCode()+"] - Banco não aceitou");
				
				
				atualizaContratoParaBoleto(usu_numctr,usu_codemp,usu_codfil,usu_codfpg);
			}
		}
	}

	

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}
	
	
	public void processaDesconto(Long usu_numctr){
		
		String sql = " select usu_numpos, usu_codemp, usu_codfil,  usu_preuni, usu_vlrmtb, (usu_preuni-(usu_preuni*0.05)) usu_preuni_com_desconto,  (usu_vlrmtb-(usu_vlrmtb*0.05)) usu_vlrmtb_com_desconto " +
					 " , usu_codser "+
					 " from usu_t160cvs cvs join E080SER ser on (cvs.usu_codser = ser.codser and cvs.usu_codemp = ser.codemp) where usu_numctr = ?  and usu_sitcvs = 'A' and ser.codfam = 'SER102' ";
		
		Connection conn =  null;
		ResultSet  rs = null;
		Long count = 0L;
		PreparedStatement pst = null;
		try{
			conn = PersistEngine.getConnection("SAPIENS");
			pst = conn.prepareStatement(sql);
			
			pst.setLong(1,usu_numctr);
			
			rs = pst.executeQuery();
			while(rs.next()){
				
				Long usu_numpos = rs.getLong(1);
				Long usu_codemp = rs.getLong(2);
				Long usu_codfil = rs.getLong(3);
				Double usu_preuni = rs.getDouble(4);
				Double usu_preuni_com_desconto = rs.getDouble(5);
				Double usu_vlrmtb = rs.getDouble(6);
				Double usu_vlrmtb_com_desconto = rs.getDouble(7);
				String usu_codser = rs.getString(8);
				
				int contador = updateValorPosto(usu_numctr, usu_numpos, usu_codemp, usu_codfil, usu_preuni_com_desconto, usu_vlrmtb_com_desconto);
				if (contador > 0 ){
					Long usu_usumov = ContratoUtils.retornaCodeUsuarioSapiens(PortalUtil.getCurrentUser().getCode());
					String usu_txtobs = "Processado desconto de 5% referente a campanha de débito automático no posto " + usu_numpos + ", valor antigo:" + usu_vlrmtb + " Valor com Desconto: " + usu_vlrmtb_com_desconto + ".";
					Long seqObs = ContratoUtils.retornaSequenciaObservacaoContrato(usu_codemp, usu_codfil, usu_numctr);
					
					GregorianCalendar usu_datmov = new GregorianCalendar();
					Long usu_hormov =(long) ( (usu_datmov.get(GregorianCalendar.HOUR_OF_DAY) * 60) + usu_datmov.get(GregorianCalendar.MINUTE) ); 
					String usu_codope = "";
					
					ContratoUtils.geraObservacao(usu_codemp, usu_codfil, usu_numctr, seqObs, usu_numpos, usu_codser, "A", usu_txtobs, usu_usumov, usu_datmov, usu_hormov, usu_codope);
					
				}
				
				
			}
			rs.close();
			rs = null;
		}catch(Exception e){
			System.err.println("sql:" + sql);
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			throw new WorkflowException("Erro ao consultar dados de débito do cliente" + e.getMessage());
		}finally{
			try{
				OrsegupsUtils.closeConnection(conn, pst, rs);
			}catch(Exception e){
				e.printStackTrace();
			}
		}

		
	}
	
	private int updateValorPosto(Long usu_numctr, Long usu_numpos, Long usu_codemp, Long usu_codfil, Double preuni, Double vlrmtb){
		String sql = " update usu_t160cvs set usu_preuni = ?, usu_vlrmtb = ? where usu_numctr = ? and usu_numpos = ? and  usu_codemp = ? and  usu_codfil = ?";
		Connection conn =  null;
		ResultSet  rs = null;
		Long count = 0L;
		PreparedStatement pst = null;
		try{
			conn = PersistEngine.getConnection("SAPIENS");
			pst = conn.prepareStatement(sql);
			
			
			pst.setDouble(1, preuni);
			pst.setDouble(2, vlrmtb);
			
			
			pst.setLong(3, usu_numctr);
			pst.setLong(4, usu_numpos);
			pst.setLong(5, usu_codemp);
			pst.setLong(6, usu_codfil);
			
			int contador = pst.executeUpdate();
			return contador;
			
			
		}catch(Exception e){
			System.err.println("sql:" + sql);
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
			throw new WorkflowException("Erro ao atualizar dados de débito do cliente" + e.getMessage());
		}finally{
			try{
				OrsegupsUtils.closeConnection(conn, pst, rs);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
	}
	
	
	private void atualizaContratoParaBoleto(Long usu_numctr, Long usu_codemp, Long usu_codfil, Long usu_codfpg)
	{
		String sql = " update usu_t160ctr set usu_codfpg = ? where usu_numctr = ? and usu_codemp = ? and  usu_codfil = ?";
		Connection conn =  null;
		ResultSet  rs = null;
		Long count = 0L;
		PreparedStatement pst = null;
		try{
			conn = PersistEngine.getConnection("SAPIENS");
			pst = conn.prepareStatement(sql);
			
			
			pst.setLong(1, usu_codfpg);
			
			pst.setLong(2, usu_numctr);
			pst.setLong(3, usu_codemp);
			pst.setLong(4, usu_codfil);
			
			int contador = pst.executeUpdate();
			
			if (contador > 0){
				Long usu_usumov = ContratoUtils.retornaCodeUsuarioSapiens(PortalUtil.getCurrentUser().getCode());
				String usu_txtobs = "Forma de pagamento alterada para boleto automaticamente, pois o banco não aceitou o débito. Obs.: Via Fluxo C035 ";
				Long seqObs = ContratoUtils.retornaSequenciaObservacaoContrato(usu_codemp, usu_codfil, usu_numctr);
				
				GregorianCalendar usu_datmov = new GregorianCalendar();
				Long usu_hormov =(long) ( (usu_datmov.get(GregorianCalendar.HOUR_OF_DAY) * 60) + usu_datmov.get(GregorianCalendar.MINUTE) ); 
				String usu_codope = "";
				
				ContratoUtils.geraObservacao(usu_codemp, usu_codfil, usu_numctr, seqObs, null, null, "A", usu_txtobs, usu_usumov, usu_datmov, usu_hormov, usu_codope);

			}
			
			
			
		}catch(Exception e){
			System.err.println("sql:" + sql);
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
			throw new WorkflowException("Erro ao atualizar contrato para boleto." + e.getMessage());
		}finally{
			try{
				OrsegupsUtils.closeConnection(conn, pst, rs);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
	}
	
	public static void abrirTarefaSimples(EntityWrapper wFormularioC035){
		
		NeoObject oContrato = (NeoObject) wFormularioC035.findValue("contrato");
		EntityWrapper wContrato = new EntityWrapper(oContrato);

		Long usu_numctr = NeoUtils.safeLong(NeoUtils.safeOutputString(wContrato.findValue("usu_numctr"))); 
		Long usu_codemp = NeoUtils.safeLong(NeoUtils.safeOutputString(wContrato.findValue("usu_codemp")));
		Long usu_codfil = NeoUtils.safeLong(NeoUtils.safeOutputString(wContrato.findValue("usu_codfil")));
		Long usu_codfpg = NeoUtils.safeLong(NeoUtils.safeOutputString(wContrato.findValue("codfpg")));
		
		
		String msg = "<p>Verificar/ajustar apontamentos do Contrato <b>"+ usu_numctr + "</b>, pois este recebeu desconto referente à campanha débito automático.</p>";
		
		msg += "<br><br><b>Contrato:</b>" + usu_numctr;
		msg += "<br><b>Empresa:</b>" + usu_codemp;
		msg += "<br><b>Filial:</b>" + usu_codfil;
		
		NeoUser usuarioResponsavel = PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "caroline.wrunski") );
		NeoUser usuarioSolicitante = PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", PortalUtil.getCurrentUser().getCode()) );
		
		GregorianCalendar prazo = new GregorianCalendar();
		prazo.add(GregorianCalendar.DAY_OF_MONTH, 2);
		prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
		prazo.set(GregorianCalendar.MINUTE, 23);
		prazo.set(GregorianCalendar.SECOND, 59);
		
		String titulo = "Verificar/Ajustar Apontamentos - Campanha de Débito Automático";
		
		/**
		 * @author orsegups lucas.avila - Alterado para utilizar a nova arquitetura de processos do Fusion
		 * @date 04/09/2015
		 */
		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		String tarefa = iniciarTarefaSimples.abrirTarefa(usuarioSolicitante.getCode(), usuarioResponsavel.getCode(), titulo, msg, "1", "sim", OrsegupsUtils.getNextWorkDay(prazo));
		System.out.println(tarefa);
	}
	
	
	
	public static Boolean iniciaProcessoTarefaSimples(NeoObject eformProcesso, NeoUser requester, NeoUser responsavel)
	{
		Boolean result = false;

		QLEqualsFilter equal = new QLEqualsFilter("Name", "G001 - Tarefa Simples");
		ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);
		
		WFProcess proc = processModel.startProcess(eformProcesso, true, null, null, null, null, requester);
		proc.setRequester(requester);
		System.out.println("[C035] - Abriu Tarefa simples n. " + proc.getCode());

		proc.setSaved(true);

		// Necessita persistir para que o metodo getOpenActivities() consiga buscar as atividades abertas
		PersistEngine.persist(proc);
		PersistEngine.commit(true);

		/* Finaliza a primeira tarefa */
		Task task = null;

		final List<Activity> acts = proc.getOpenActivities();
		System.out.println("acts: " + acts);
		if (acts != null)
		{
			System.out.println("acts.size(): " + acts.size());
		}
		final Activity activity1 = (Activity) acts.get(0);
		if (activity1 instanceof UserActivity)
		{
			try
			{
				if (((UserActivity) activity1).getTaskList().size() <= 0)
				{
					task = activity1.getModel().getTaskAssigner().assign((UserActivity) activity1, responsavel);
					OrsegupsWorkflowHelper.finishTaskByActivity(activity1);
				}
				else
				{
					task = ((UserActivity) activity1).getTaskList().get(0);
					OrsegupsWorkflowHelper.finishTaskByActivity(activity1);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				System.err.println("Erro ao iniciar a primeira atividade do processo.");
			}
		}
		return result;
	}

	

}
