package com.neomind.fusion.custom.orsegups.utils.novo;

import java.util.GregorianCalendar;

import com.neomind.fusion.engine.runtime.RuntimeEngine;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.security.SecurityEngine;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.TaskInstance;
import com.neomind.fusion.workflow.TaskInstanceActionType;
import com.neomind.fusion.workflow.TaskInstanceHelper;
import com.neomind.fusion.workflow.TaskLog;
import com.neomind.fusion.workflow.TaskLog.TaskLogType;
import com.neomind.fusion.workflow.exception.AssignmentException;
import com.neomind.fusion.workflow.handler.HandlerFactory;
import com.neomind.fusion.workflow.handler.TaskHandler;
import com.neomind.fusion.workflow.task.central.search.TaskCentralIndex;
import com.neomind.fusion.workflow.task.rule.TaskRuleEngine;
import com.neomind.util.NeoUtils;

public class OrsegupsTaskService
{
	public void delegate(long taskId, String userCode, String targetUserCode, String reason)
	{
		NeoUser fromUser = null;
		NeoUser targetUser = null;

		if (!NeoUtils.safeIsNull(userCode))
			fromUser = SecurityEngine.getInstance().getUser(userCode);
		if (!NeoUtils.safeIsNull(targetUserCode))
			targetUser = SecurityEngine.getInstance().getUser(targetUserCode);

		TaskInstance taskInstance = PersistEngine.getObject(TaskInstance.class, taskId);

		delegate(taskInstance, fromUser, targetUser, reason);
	}
	
	public void delegate(TaskInstance taskInstance, NeoUser user, NeoUser targetUser, String reason)
	{
		if (targetUser == null)
			throw new AssignmentException("");

		Task task = taskInstance.getTask();

		TaskCentralIndex.getInstance().delete(taskInstance, true);

		taskInstance.getTask().getLogs().add(createLog(TaskLogType.TRANSFER, reason, user, taskInstance.getOwner()));

		TaskHandler taskHandler = HandlerFactory.TASK.get(taskInstance.getTask());
		Task t = taskHandler.transfer(user, targetUser);

		taskInstance.setOwner(t.getUser());

		// Remove a caixa
		RuntimeEngine.getTaskService().setBox(taskInstance, null);

		taskInstance.setActionType(TaskInstanceActionType.DELEGATED);

		// Remove os marcadores
		taskInstance.getTags().clear();

		// Executa regras
		this.refresh(taskInstance);

		taskInstance.setTask(t);
	}
	
	private TaskLog createLog(TaskLogType type, String reason, NeoUser user, NeoUser owner)
	{
		TaskLog log = new TaskLog();
		log.setAction(type);
		log.setDateLog(new GregorianCalendar());
		log.setDescription(reason);
		log.setUser(user);
		log.setOwner(owner);

		PersistEngine.persist(log);

		return log;
	}
	
	/**
	 * Método para atualizar os dados da tarefa, e executar novamente seus gatilhos.
	 * 
	 * @param taskInstance
	 */
	public void refresh(TaskInstance taskInstance)
	{
		TaskInstanceHelper.update(taskInstance);

		// TODO - Reindexar

		TaskRuleEngine.get().run(taskInstance);
	}
}
