package com.neomind.fusion.custom.casvig.mobile.action;

import java.io.DataOutputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.neomind.fusion.custom.casvig.mobile.CasvigMobileServlet;
import com.neomind.fusion.custom.casvig.mobile.MobileUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.util.NeoUtils;

/**
 * Classe que valida se o login enviado é válido
 * 
 * @author Daniel Henrique Joppi
 * 
 */
@WebServlet(name="ActionLogin", urlPatterns={"/servlet/com.neomind.fusion.custom.casvig.mobile.action.ActionLogin"})
public class ActionLogin extends HttpServlet
{
	public static ActionLogin getInstance()
	{
		return new ActionLogin();
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String msg = null;
		DataOutputStream out = null;

		boolean error = false;
		try
		{
			out = new DataOutputStream(response.getOutputStream());

			boolean flag = false;
			NeoUser user = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", request.getParameter("code"))); // login
			if (user != null)
			{
				String passUser = user.getPassword();
				String passMobile = NeoUtils.encodePassword(MobileUtils.dencodePassword(request.getParameter("owned")));

				if (passUser.equals(passMobile))
				{ // password
					String fullName = new String(MobileUtils.convertToSendMobile(user.getFullName()));
					MobileUtils.sendString(fullName, response);
					CasvigMobileServlet.log.info("Login checked");
				}
				else
					flag = true;
			}
			else
				flag = true;

			if (flag)
			{
				MobileUtils.sendString("ERROR", response);
				CasvigMobileServlet.log.error("Login error ");
			}
			return;
		}
		catch (Exception e)
		{
			error = true;
		}
		finally
		{
			if (out != null)
			{
				try
				{
					out.close();
				}
				catch (IOException ioe)
				{
					ioe.printStackTrace();
					error = true;
				}
			}
			if (error)
			{
				if (msg != null)
					CasvigMobileServlet.log.error(msg);
			}
		}
	}
}
