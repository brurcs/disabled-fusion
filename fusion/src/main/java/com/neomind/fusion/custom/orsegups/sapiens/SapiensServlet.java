package com.neomind.fusion.custom.orsegups.sapiens;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.HtmlEmail;

import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;

/**
 * Servlet implementation class SapiensServlet
 */
public class SapiensServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	private static final String LINK_NOTA = "\\\\fsoofs01\\f$\\Site\\Matadeiro\\";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SapiensServlet()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		this.doRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		this.doRequest(request, response);
	}

	public void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException
	{
		response.setContentType("text/html");
		response.setCharacterEncoding("ISO-8859-1");

		PrintWriter out;
		try
		{
			out = response.getWriter();

			String action = "";

			if (request.getParameter("action") != null)
			{
				action = request.getParameter("action");

			}

			if (action.equalsIgnoreCase("enviaEmailNotaFiscal"))
			{
				this.enviaEmailNotaFiscal(request, response, out);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void enviaEmailNotaFiscal(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{
		String aCodEmp = request.getParameter("aCodEmp");
		String aCodFil = request.getParameter("aCodFil");
		String aLnkNfe = request.getParameter("aLnkNfe");
		String aNumDfs = request.getParameter("aNumDfs");
		String aCodVer = request.getParameter("aCodVer");
		String aCgcPre = request.getParameter("aCgcPre");
		String aNomCli = request.getParameter("aNomCli");

		String aNomEmp = request.getParameter("aNomEmp");

		String aVctPro = request.getParameter("aVctPro");
		String aVlrOri = request.getParameter("aVlrOri");
		String aDesFpg = request.getParameter("aDesFpg");
		String aNumCtr = request.getParameter("aNumCtr");
		String aNumRps = request.getParameter("aNumRps");
		String aNomArq = request.getParameter("aNomArq");
		String aCodSnf = request.getParameter("aCodSnf");
		String aEmaCli = request.getParameter("aEmaCli");
		String aEmaCop = request.getParameter("aEmaCop");
		String aNomArqAux = "";

		String retorno = "OK";
		try
		{
			if (aNomEmp != null && !aNomEmp.isEmpty())
				aNomEmp = new String(aNomEmp.getBytes("ISO-8859-1"), "UTF-8");
			if (aNomCli != null && !aNomCli.isEmpty())
				aNomCli = new String(aNomCli.getBytes("ISO-8859-1"), "UTF-8");
			StringBuilder builderAux = new StringBuilder();
			if (aCodEmp != null && !aCodEmp.isEmpty() && aCodFil != null && !aCodFil.isEmpty())
			{
				if (((!aCodEmp.equals("15")) && (!aCodEmp.equals("18")) && (!aCodEmp.equals("21"))) || ((aCodEmp.equals("21")) && (!aCodFil.equals("2"))) || ((aCodEmp.equals("18")) && (aCodFil.equals("2"))))
				{
					builderAux.append(" Link para a Nota Fiscal Eletrônica:<br> ");
					builderAux.append(" <a href = '" + aLnkNfe + "'>Clique aqui para visualizar a nota fiscal.</a><br>");
					builderAux.append(" Ou ainda, acesse o link e digite os campos abaixo:<br>");
				}

				if ((aCodEmp.equals("17")) || (aCodEmp.equals("19")))
				{
					builderAux.append(" https://e-gov.betha.com.br/e-nota/verificar_autenticidade.faces<br>");
				}

				if ((aCodEmp.equals("15")) || ((aCodEmp.equals("18")) && (aCodFil.equals("1"))))
				{
					builderAux.append(" Para ter acesso a nota fiscal e confirmar a autenticidade do documento fiscal, acesse o link abaixo:<br>");
					builderAux.append(" http://nfse.pmsj.sc.gov.br:90/Nfse/<br>");
					builderAux.append(" No lado direito do site selecione a opção <b>Autenticidade NFS-e</b><br>");
					builderAux.append(" Preencha os seguintes campos:<br>");
					builderAux.append(" Nº. da NFS-e: " + aNumDfs + "<br>");
					builderAux.append(" Código Verificador: " + aCodVer + "<br>");
					builderAux.append(" CPF/CNPJ do Prestador: " + aCgcPre + "<br>");
					builderAux.append(" Clicar em <b>Validar</b> e por fim <b>Download em PDF</b><br><br>");
				}

				if (((aCodEmp.equals("18")) && (aCodFil.equals("2"))) || ((aCodEmp.equals("21")) && (aCodFil.equals("1"))) || ((aCodEmp.equals("22")) && (aCodFil.equals("1"))))
				{
					builderAux.append(" http://isscuritiba.curitiba.pr.gov.br/portalnfse/autenticidade.aspx<br>");
				}

				if ((aCodEmp.equals("21")) && (aCodFil.equals("2")))
				{
					builderAux.append(" Acesse o link e digite os campos abaixo:<br>");
					builderAux.append(" http://www.issnetonline.com.br/cascavel/online/NotaDigital/VerificaAutenticidade.aspx<br>");
					builderAux.append(" Forma de Validação: Normal<br>");
					builderAux.append(" Insc. Mun. Emissor: 3463800<br>");
					builderAux.append(" Série do Doc. Fiscal: NFS-e Nota Eletrônica de Serviços<br>");
					builderAux.append(" Número do Doc. Fiscal: " + aNumDfs + "<br>");
					builderAux.append(" Chave de Identificação: " + aCodVer + "<br>");
					builderAux.append(" Informar o código que aparece na imagem.<br>");
					builderAux.append(" Clicar em Verificar Autenticidade<br>");
					builderAux.append(" Clicar em Visualizar a Nota Fiscal Eletrônica<br><br>");
				}

				if ((aCodEmp.equals("21")) && (aCodFil.equals("3")))
				{
					builderAux.append(" http://registro.ginfes.com.br<br>");
				}

				if (((!aCodEmp.equals("15")) && (!aCodEmp.equals("18")) && (!aCodEmp.equals("21"))) || ((aCodEmp.equals("21")) && (!aCodFil.equals("2"))) || ((aCodEmp.equals("18")) && (aCodFil.equals("2"))))
				{
					builderAux.append("CPF/CNPJ do Prestador: " + aCgcPre + "<br>");
					builderAux.append("Número da NFS-e: " + aNumDfs + "<br>");
					builderAux.append("Código de Verificação: " + aCodVer + "<br><br>");
				}

			}

			MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

			MailSettings mailClone = new MailSettings();
			mailClone.setMinutesInterval(settings.getMinutesInterval());
			mailClone.setFromEMail(settings.getFromEMail());
			mailClone.setFromName(settings.getFromName());
			mailClone.setRenderServer(settings.getRenderServer());
			mailClone.setSecurityFromEmail(settings.getSecurityFromEmail());
			mailClone.setSecurityCopyEmail(settings.getSecurityCopyEmail());
			mailClone.setSmtpSettings(settings.getSmtpSettings());
			mailClone.setPort(settings.getPort());
			mailClone.setSmtpServer(settings.getSmtpServer());
			mailClone.setEnabled(settings.isEnabled());
			mailClone.setFromEMail("suafatura@suafatura.orsegups.com.br");

			mailClone.setFromName("Orsegups Partipações S.A.");
			if (mailClone != null && mailClone.isEnabled())
			{
				HtmlEmail noUserEmail = new HtmlEmail();
				StringBuilder noUserMsg = new StringBuilder();

				noUserEmail.setCharset("ISO-8859-1");

				if (aEmaCli != null && !aEmaCli.isEmpty() && aEmaCli.contains(";"))
				{
					String[] arrayEmail = aEmaCli.split(";");

					for (String emailStr : arrayEmail)
					{
						if (emailStr != null && !emailStr.isEmpty() && validEmail(emailStr))
							noUserEmail.addTo(emailStr);
					}
				}
				else
				{
					if (aEmaCli != null && !aEmaCli.isEmpty() && validEmail(aEmaCli))
						noUserEmail.addTo(aEmaCli);
				}

				if (aEmaCop != null && !aEmaCop.isEmpty() && aEmaCop.contains(";"))
				{
					String[] arrayEmail = aEmaCop.split(";");

					for (String emailStr : arrayEmail)
					{
						if (emailStr != null && !emailStr.isEmpty() && validEmail(emailStr))
							noUserEmail.addBcc(emailStr);
					}
				}
				else
				{
					if (aEmaCop != null && !aEmaCop.isEmpty() && validEmail(aEmaCop))
						noUserEmail.addBcc(aEmaCop);
				}

				noUserEmail.setFrom("suafatura@suafatura.orsegups.com.br");
				noUserEmail.setSubject("[TESTE] Nota Fiscal Eletrônica de Serviços Municipais - Faturamento RPS Nº " + aNumRps + "/" + aCodSnf);

				if (aNomArq != null && !aNomArq.isEmpty() && aNomArq.contains(";"))
				{

					String array[] = aNomArq.split(";");
					for (String caminhoArquivo : array)
					{

						EmailAttachment anexo = new EmailAttachment();
						File arqRelatorio = new File(LINK_NOTA + caminhoArquivo);
						if (arqRelatorio != null)
						{
							anexo.setPath(arqRelatorio.getAbsolutePath());
							anexo.setDisposition(EmailAttachment.ATTACHMENT);
							anexo.setName(arqRelatorio.getName());
							noUserEmail.attach(anexo);
							aNomArqAux += anexo.getName() + "<br>";
						}
					}
				}
				else if (aNomArq != null && !aNomArq.isEmpty() && !aNomArq.contains(";"))
				{
					EmailAttachment anexo = new EmailAttachment();
					File arqRelatorio = new File(LINK_NOTA + aNomArq);

					anexo.setPath(arqRelatorio.getAbsolutePath());
					anexo.setDisposition(EmailAttachment.ATTACHMENT);
					anexo.setName(arqRelatorio.getName());
					noUserEmail.attach(anexo);
					aNomArqAux += anexo.getName() + "<br>";

				}
				
				noUserMsg.append(" <!DOCTYPE html>");
				noUserMsg.append(" <html>");
				noUserMsg.append(" <head>");
				noUserMsg.append(" <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");
				noUserMsg.append(" <link href='http://fonts.googleapis.com/css?family=Share:400,400italic,700' rel='stylesheet' type='text/css'>");
				noUserMsg.append(" </link>");
				noUserMsg.append(" </head>");
				noUserMsg.append(" <body yahoo=\"yahoo\" style=\"font-family:courier\">");
				noUserMsg.append(" <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">");
				noUserMsg.append(" <tr><td>");
				noUserMsg.append(" <table width=\"600\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">");
				noUserMsg.append(" <tr><td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/emailnfe/header.jpg\"></td></tr>");
				noUserMsg.append(" <tr><td><hr></td></tr><tr><td><h1 style=\"font-size:25px;text-align:center;font-family:Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;\">" + aNomEmp + "</h1></td></tr><tr><td><hr></td></tr>");
				noUserMsg.append(" <tr><td><table width=\"100%\" border=\"0\">");
				noUserMsg.append(" <tr><td width=\"20%\" align=\"left\">");
				noUserMsg.append(" <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/emailnfe/cliente.jpg\" alt=\"CLIENTE\"></td>");
				noUserMsg.append(" <td width=\"80%\" align=\"left\" style='font-family:Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif'><b>" + aNomCli + "</b></td></tr>");
				noUserMsg.append(" <tr><td width=\"20%\" align=\"left\"><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/emailnfe/vencimento.jpg\" alt=\"VENCIMENTO\"></td>");
				noUserMsg.append(" <td width=\"80%\" align=\"left\" style='font-family:Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif'>" + aVctPro + "</td></tr>");
				noUserMsg.append(" <tr><td width=\"20%\" align=\"left\"><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/emailnfe/valor.jpg\" alt=\"VALOR\"></td>");
				noUserMsg.append(" <td width=\"80%\" align=\"left\" style='font-family:Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif'>R$" + aVlrOri + "</td></tr>");
				noUserMsg.append(" <tr><td width=\"20%\" align=\"left\">");
				noUserMsg.append(" <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/emailnfe/forma-de-pagamento.jpg\" alt=\"FORMA DE PAGAMENTO\"></td>");
				noUserMsg.append(" <td width=\"80%\" align=\"left\" style='font-family:Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif'>" + aDesFpg + "</td></tr>");
				noUserMsg.append(" <tr><td width=\"20%\" align=\"left\">");
				noUserMsg.append(" <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/emailnfe/contrato.jpg\" alt=\"CONTRATO\"></td>");
				noUserMsg.append(" <td width=\"80%\" align=\"left\" style='font-family:Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif'>" + aNumCtr + "</td></tr></table></td></tr><tr><td><hr></td></tr>");
				noUserMsg.append(" <tr><td style='font-family:Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif'>" + builderAux.toString() + "</td></tr>");
				noUserMsg.append(" <tr><td><hr></td></tr><tr><td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/emailnfe/anexos.jpg\" alt=\"ANEXOS\"></td></tr>");
				noUserMsg.append(" <tr><td style='font-family:Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif'>" + aNomArqAux + "</td></tr>");
				noUserMsg.append(" <tr><td><hr></td></tr><tr><td style='font-family:Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif'><p>");
				noUserMsg.append(" Para facilitar seu acesso a Nota Fiscal Eletrônica de Serviços, estaremos disponibilizando a mesma e o respectivo Boleto bancário");
				noUserMsg.append(" (quando existir) pelo nosso site: http://www.orsegups.com.br no link <b>Portal do Cliente</b>. Para acessá-lo, basta informar o CNPJ/CPF");
				noUserMsg.append(" e a senha que será os quatro últimos algarismos do seu CNPJ/CPF. Além disso, encaminharemos mensalmente por e-mail o link da Prefeitura");
				noUserMsg.append(" com o acesso a Nota Fiscal Eletrônica e o boleto.</p><p>Portanto para seu maior conforto e segurança, solicitamos que V.Sa. atualize seu cadastro");
				noUserMsg.append(" de e-mail junto a nossa Companhia, através do site http://www.orsegups.com.br/contato, selecione o <b>");
				noUserMsg.append(" TIPO DE CONTATO: ATUALIZAÇÃO CADASTRAL</b></p></td></tr><tr><td><hr></td></tr><tr><td align=\"center\">");
				noUserMsg.append(" <a href=\"http://www.orsegups.com.br/contato\">");
				noUserMsg.append(" <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/emailnfe/call-to-action.jpg\" alt=\"CONTATO\"></a>");
				noUserMsg.append(" </td></tr><tr><td><hr></td></tr><tr><td><a href=\"http://www.facebook.com/orsegups\">");
				noUserMsg.append(" <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/emailnfe/facebook.png\" alt=\"FACEBOOK\"></a>");
				noUserMsg.append(" <a href=\"http://www.twitter.com/orsegups\"><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/emailnfe/twitter.png\" alt=\"TWITTER\"></a>");
				noUserMsg.append(" <a href=\"http://www.youtube.com/orsegupssc\"><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/emailnfe/youtube.png\" alt=\"YOUTUBE\"></a>");
				noUserMsg.append(" <br><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/emailnfe/footer.jpg\"></td></tr></table></td></tr></table></body></html>");

				noUserEmail.setHtmlMsg(noUserMsg.toString());

				mailClone.applyConfig(noUserEmail);
				
				noUserEmail.send();

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			retorno = " Erro: " + e.getLocalizedMessage();
		}
		finally
		{
			out.print(retorno);
			out.flush();
			out.close();
		}
	}

	public boolean validEmail(String email)
	{
		Pattern p = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,7}$");
		Matcher m = p.matcher(email);
		if (m.find())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
