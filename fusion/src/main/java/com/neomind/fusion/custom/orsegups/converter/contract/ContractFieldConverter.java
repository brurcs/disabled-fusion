package com.neomind.fusion.custom.orsegups.converter.contract;

import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.BooleanConverter;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class ContractFieldConverter extends BooleanConverter
{

	@SuppressWarnings("unchecked")
	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin)
	{
		QLEqualsFilter item = new QLEqualsFilter("chave", field.getFieldCode());

		List<NeoObject> itensContrato = (List<NeoObject>) PersistEngine.getObjects(
				AdapterUtils.getEntityClass("FGCItensContrato"), item);

		if (itensContrato != null && !itensContrato.isEmpty())
		{
			NeoObject itemContrato = itensContrato.get(0);
			EntityWrapper entityWrapper = new EntityWrapper(itemContrato);
			String htmlValue = null;
			String boxFormatClass = null;
			boxFormatClass = "<style> div.sel select{width:50px;float:left } div.ex { width:auto; padding:1px; margin-right:20px;margin-left:80px}</style>";
			htmlValue = "<script>$('label[for*=_" + field.getFieldCode() + "_]').hide();</script>";
			return  "<div class='sel'>"+ super.getHTMLInput(field, origin) +  htmlValue + boxFormatClass +"</div>"+ "<div class='ex'>" + entityWrapper.findValue("descricao")+"</div>";
			//return super.getHTMLInput(field, origin) + htmlValue + " <span>" + entityWrapper.findGenericValue("descricaoItem") + "</span>";
		}
		else
		{
			return super.getHTMLInput(field, origin);
		}
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		// TODO Auto-generated method stub
		return super.getHTMLView(field, origin);
	}

	public ContractFieldConverter()
	{
		// TODO Auto-generated constructor stub
	}

}
