package com.neomind.fusion.custom.orsegups.presenca.bioPresenca.Engine;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormatter;

import com.neomind.fusion.custom.orsegups.presenca.bioPresenca.DTO.BioPresencaDTO;
import com.neomind.fusion.custom.orsegups.presenca.bioPresenca.DTO.ErroDTO;
import com.neomind.fusion.custom.orsegups.presenca.bioPresenca.DTO.ErroDTOPai;
import com.neomind.fusion.custom.orsegups.presenca.bioPresenca.DTO.ErroPontoDTO;
import com.neomind.fusion.custom.orsegups.presenca.bioPresenca.DTO.RespostaDTO;
import com.neomind.fusion.custom.orsegups.presenca.bioPresenca.Utils.BioPresencaUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;

/**
 * 
 * @author lucas.alison
 *
 */

public class BioPresencaEngine {
    
    public RespostaDTO novaMarcacao(BioPresencaDTO b){
	String URL = "fusion/custom/jsp/orsegups/presenca/presencaViaSNEP.jsp?";
	URL += "numEmp="+b.getEmpresa();
	URL += "&numCad="+b.getMatricula();
	URL += "&acao="+b.getAcao();
	URL += "&fone="+b.getTelefone();
	
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	StringBuilder sql = new StringBuilder();
	Long id = null;
	try {
	    conn = PersistEngine.getConnection("TIDB");
	    
	    sql.append("INSERT INTO BIO_PRESENCA_PONTOS (DATA,MATRICULA,EMPRESA,TIMEZONEDISPOSITIVO) OUTPUT Inserted.ID  VALUES (?,?,?,?)");
	    
	    pstm = conn.prepareStatement(sql.toString());
	    if (b.getTimeZoneDispositivo() != null){
		TimeZone tz2 = TimeZone.getTimeZone(b.getTimeZoneDispositivo());
		TimeZone.setDefault(tz2);
		Calendar ca2 = GregorianCalendar.getInstance(tz2);
		ca2.setTime(b.getData());
		
		pstm.setTimestamp(1, new Timestamp(ca2.getTimeInMillis()));
	    }else{
		pstm.setTimestamp(1, new Timestamp(b.getData().getTime()));
	    }
	    pstm.setLong(2, b.getMatricula());
	    pstm.setLong(3, b.getEmpresa());
	    pstm.setString(4, b.getTimeZoneDispositivo());

	    rs = pstm.executeQuery();

	    while(rs.next()){
		id = rs.getLong("ID");
		URL += "&origem=BIOPRESENCA;"+id;
	    }
	    
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}	
	RespostaDTO resp = BioPresencaUtils.executaURL(URL);
	removerRegistroPontoTIDB(id);
	return resp;
    }
    
    private void removerRegistroPontoTIDB(Long id){
	Connection conn = null;
	PreparedStatement pstm = null;

	StringBuilder sql = new StringBuilder();

	try {
	    conn = PersistEngine.getConnection("TIDB");

	    sql.append("DELETE BIO_PRESENCA_PONTOS WHERE ID = ?");
	    
	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setLong(1, id);

	    pstm.execute();

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}
    }
    
    public void gravarLog(BioPresencaDTO b, RespostaDTO resposta){
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	StringBuilder sql = new StringBuilder();
	try {
	    conn = PersistEngine.getConnection("TIDB");
	    
	    sql.append("INSERT INTO BIO_PRESENCA_LOG (EMPRESA,MATRICULA,ACAO,TELEFONE,DATA,MSG,TIMEZONEDISPOSITIVO,DATASEMTIMEZONE) OUTPUT Inserted.ID  VALUES (?,?,?,?,?,?,?,?)");
	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setLong(1, b.getEmpresa());
	    pstm.setLong(2, b.getMatricula());
	    pstm.setString(3, b.getAcao());
	    pstm.setString(4, b.getTelefone());
	    
	    if (b.getTimeZoneDispositivo() != null){
		TimeZone tz2 = TimeZone.getTimeZone(b.getTimeZoneDispositivo());
		TimeZone.setDefault(tz2);
		Calendar ca2 = GregorianCalendar.getInstance(tz2);
		ca2.setTime(b.getData());
		pstm.setTimestamp(5, new Timestamp(ca2.getTimeInMillis()));
	    }else{
		pstm.setTimestamp(5, new Timestamp(b.getData().getTime()));
	    }
	    pstm.setTimestamp(6, new Timestamp(b.getData().getTime()));
	    String msg = null; 
	    if (resposta != null && resposta.getErros() != null){
		msg = resposta.getMsg()+" - ";
		for (ErroDTOPai erro : resposta.getErros()) {
		    if (erro instanceof ErroPontoDTO){
			ErroPontoDTO err = (ErroPontoDTO) erro;
			msg += err.toString();
		    }else{
			ErroDTO err = (ErroDTO) erro;
			msg += err.toString();
		    }
		}		
	    }
	    pstm.setString(6, msg);
	    pstm.setString(7, b.getTimeZoneDispositivo());
	    rs = pstm.executeQuery();
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}	
    }
    
}
