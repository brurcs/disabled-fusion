package com.neomind.fusion.custom.orsegups.medicaoDeContratos.vo;

import java.util.List;

public class PostosMedicaoVO
{
	public String nomLoc;
	public long numLoc;
	public long numPos;
	public long nVagas;
	public long nColaboradores;
	public long neoId;
	public String obsPosto;
	public String codCcu;
	public String aletas;
	public String imgAlertas;
	public String linkColaboradoresDemitidos;
	public String linkcoberturasPosto;
	public String linkAddColaborador;
	public String linkAddObsPosto;
	public String linkRemoverPosto;
	public String linkVisualizarObsPosto;
	public List<ColaboradorMedicaoVO> listaColaborador;
	public String codEsc;

	public String getLinkRemoverPosto()
	{
		return linkRemoverPosto;
	}

	public void setLinkRemoverPosto(String linkRemoverPosto)
	{
		this.linkRemoverPosto = linkRemoverPosto;
	}

	public String getCodEsc()
	{
		return codEsc;
	}

	public void setCodEsc(String codEsc)
	{
		this.codEsc = codEsc;
	}

	public String getLinkVisualizarObsPosto()
	{
		return linkVisualizarObsPosto;
	}

	public void setLinkVisualizarObsPosto(String linkVisualizarObsPosto)
	{
		this.linkVisualizarObsPosto = linkVisualizarObsPosto;
	}

	public String getLinkAddObsPosto()
	{
		return linkAddObsPosto;
	}

	public void setLinkAddObsPosto(String linkAddObsPosto)
	{
		this.linkAddObsPosto = linkAddObsPosto;
	}

	public String getObsPosto()
	{
		return obsPosto;
	}

	public void setObsPosto(String obsPosto)
	{
		this.obsPosto = obsPosto;
	}

	public String getCodCcu()
	{
		return codCcu;
	}

	public void setCodCcu(String codCcu)
	{
		this.codCcu = codCcu;
	}

	public final String getLinkAddColaborador()
	{
		return linkAddColaborador;
	}

	public final void setLinkAddColaborador(String linkAddColaborador)
	{
		this.linkAddColaborador = linkAddColaborador;
	}

	public List<ColaboradorMedicaoVO> getListaColaborador()
	{
		return listaColaborador;
	}

	public void setListaColaborador(List<ColaboradorMedicaoVO> listaColaborador)
	{
		this.listaColaborador = listaColaborador;
	}

	public String getLinkColaboradoresDemitidos()
	{
		return linkColaboradoresDemitidos;
	}

	public void setLinkColaboradoresDemitidos(String linkColaboradoresDemitidos)
	{
		this.linkColaboradoresDemitidos = linkColaboradoresDemitidos;
	}

	public String getLinkcoberturasPosto()
	{
		return linkcoberturasPosto;
	}

	public void setLinkcoberturasPosto(String linkcoberturasPosto)
	{
		this.linkcoberturasPosto = linkcoberturasPosto;
	}

	public String getImgAlertas()
	{
		return imgAlertas;
	}

	public void setImgAlertas(String imgAlertas)
	{
		this.imgAlertas = imgAlertas;
	}

	public String getAletas()
	{
		return aletas;
	}

	public void setAletas(String aletas)
	{
		this.aletas = aletas;
	}

	public long getNeoId()
	{
		return neoId;
	}

	public void setNeoId(long neoId)
	{
		this.neoId = neoId;
	}

	public String getNomLoc()
	{
		return nomLoc;
	}

	public void setNomLoc(String nomLoc)
	{
		this.nomLoc = nomLoc;
	}

	public long getNumLoc()
	{
		return numLoc;
	}

	public void setNumLoc(long numLoc)
	{
		this.numLoc = numLoc;
	}

	public long getNumPos()
	{
		return numPos;
	}

	public void setNumPos(long numPos)
	{
		this.numPos = numPos;
	}

	public long getnVagas()
	{
		return nVagas;
	}

	public void setnVagas(long nVagas)
	{
		this.nVagas = nVagas;
	}

	public long getnColaboradores()
	{
		return nColaboradores;
	}

	public void setnColaboradores(long nColaboradores)
	{
		this.nColaboradores = nColaboradores;
	}

}
