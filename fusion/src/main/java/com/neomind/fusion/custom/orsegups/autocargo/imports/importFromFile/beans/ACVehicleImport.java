package com.neomind.fusion.custom.orsegups.autocargo.imports.importFromFile.beans;

public class ACVehicleImport {
    
    private int client_id;
    private String client_name;
    private String registration_plate;
    private String renavam;
    private String description;
    private String color;
    private String chassi_number;
    private String manufacturing_year;
    private String brand;
    private String model;
    private String model_year;
    private String tracking_module;
    private String category;
    private String fuel_type;
    private String licensing_year;
    private String licensing_year_expiration_date;
    private String licensing_ipva_expiration_date;
    
    public int getClient_id() {
        return client_id;
    }
    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }
    public String getClient_name() {
        return client_name;
    }
    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }
    public String getRegistration_plate() {
        return registration_plate;
    }
    public void setRegistration_plate(String registration_plate) {
        this.registration_plate = registration_plate;
    }
    public String getRenavam() {
        return renavam;
    }
    public void setRenavam(String renavam) {
        this.renavam = renavam;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public String getChassi_number() {
        return chassi_number;
    }
    public void setChassi_number(String chassi_number) {
        this.chassi_number = chassi_number;
    }
    public String getManufacturing_year() {
        return manufacturing_year;
    }
    public void setManufacturing_year(String manufacturing_year) {
        this.manufacturing_year = manufacturing_year;
    }
    public String getBrand() {
        return brand;
    }
    public void setBrand(String brand) {
        this.brand = brand;
    }
    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }
    public String getModel_year() {
        return model_year;
    }
    public void setModel_year(String model_year) {
        this.model_year = model_year;
    }
    public String getTracking_module() {
        return tracking_module;
    }
    public void setTracking_module(String tracking_module) {
        this.tracking_module = tracking_module;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public String getFuel_type() {
        return fuel_type;
    }
    public void setFuel_type(String fuel_type) {
        this.fuel_type = fuel_type;
    }
    public String getLicensing_year() {
        return licensing_year;
    }
    public void setLicensing_year(String licensing_year) {
        this.licensing_year = licensing_year;
    }
    public String getLicensing_year_expiration_date() {
        return licensing_year_expiration_date;
    }
    public void setLicensing_year_expiration_date(String licensing_year_expiration_date) {
        this.licensing_year_expiration_date = licensing_year_expiration_date;
    }
    public String getLicensing_ipva_expiration_date() {
        return licensing_ipva_expiration_date;
    }
    public void setLicensing_ipva_expiration_date(String licensing_ipva_expiration_date) {
        this.licensing_ipva_expiration_date = licensing_ipva_expiration_date;
    }
    
    

}
