package com.neomind.fusion.custom.orsegups.presenca.bioPresenca.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.map.ObjectMapper;

import com.neomind.fusion.custom.orsegups.presenca.bioPresenca.DTO.ErroDTOPai;
import com.neomind.fusion.custom.orsegups.presenca.bioPresenca.DTO.ErroPontoDTO;
import com.neomind.fusion.custom.orsegups.presenca.bioPresenca.DTO.RespostaDTO;


/**
 * 
 * @author lucas.alison
 *
 */

public class BioPresencaUtils {
    
//    private static final String URL_FUSION = "http://localhost:8080/";
    private static final String URL_FUSION = "https://intranet.orsegups.com.br/";
    private static final Log log = LogFactory.getLog(BioPresencaUtils.class);
    
    public static RespostaDTO executaURL(String URL) {

	StringBuilder retorno = new StringBuilder();
	RespostaDTO respostaDTO = new RespostaDTO();
	List<ErroDTOPai> erros = new ArrayList<ErroDTOPai>();
	try {

	    URL url = new URL(URL_FUSION + URL);
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setRequestMethod("GET");

	    if (conn.getResponseCode() != 200) {
		throw new RuntimeException(String.valueOf(conn.getResponseCode()));
	    }
	    respostaDTO.setStatus(conn.getResponseCode());
	    respostaDTO.setMsg(conn.getResponseMessage());
	    BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream()), "UTF-8"));

	    String output;

	    while ((output = br.readLine()) != null) {
		retorno.append(output + "\n");
	    }
	    conn.disconnect();

	} catch (MalformedURLException e) {
	    e.printStackTrace();
	    respostaDTO.setStatus(500);
	    ErroPontoDTO erroPontoDTO = new ErroPontoDTO();
	    erroPontoDTO.setId("500");
	    erroPontoDTO.setRetorno(e.getMessage());
	    erros.add(erroPontoDTO);
	} catch (IOException e) {
	    e.printStackTrace();
	    respostaDTO.setStatus(500);
	    ErroPontoDTO erroPontoDTO = new ErroPontoDTO();
	    erroPontoDTO.setId("500");
	    erroPontoDTO.setRetorno(e.getMessage());
	    erros.add(erroPontoDTO);
	} catch (Exception e){
	    e.printStackTrace();
	    respostaDTO.setStatus(500);
	    ErroPontoDTO erroPontoDTO = new ErroPontoDTO();
	    erroPontoDTO.setId("500");
	    erroPontoDTO.setRetorno(e.toString());
	    erros.add(erroPontoDTO);
	}
	if (erros.isEmpty()){
	    try{
//	    Gson gson = new Gson();
//	    ErroPontoDTO result = gson.fromJson(retorno.toString(), ErroPontoDTO.class);
		ObjectMapper map = new ObjectMapper();
		ErroPontoDTO result = map.readValue(retorno.toString(), ErroPontoDTO.class);
		if (!result.getRetorno().toUpperCase().contains("OK")
			&& !result.getRetorno().toUpperCase().contains("10")
			&& !result.getRetorno().toUpperCase().contains("8")
			&& !result.getRetorno().toUpperCase().contains("4")
			&& !result.getRetorno().toUpperCase().contains("3")){
		    respostaDTO.setStatus(412);
		    erros.add(result);
		}else{
		    respostaDTO.setStatus(200);
		    respostaDTO.setMsg(result.getRetorno());
		    erros.add(result);
		}
	    }catch(Exception e){
		respostaDTO.setStatus(500);
		ErroPontoDTO erroPontoDTO = new ErroPontoDTO();
		erroPontoDTO.setId("500");
		erroPontoDTO.setRetorno("Erro ao finalizar conexão");
		erros.add(erroPontoDTO); 
		e.printStackTrace();
	    }
	}
	respostaDTO.setErros(erros);
	return respostaDTO;

    }
}
