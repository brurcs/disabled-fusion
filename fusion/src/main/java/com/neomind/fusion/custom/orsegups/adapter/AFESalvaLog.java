package com.neomind.fusion.custom.orsegups.adapter;

import com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class AFESalvaLog implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		String titulo = (String) processEntity.findValue("titulo");
		String centroCusto = titulo.substring(36, titulo.length());
		String txtResposta = (String) processEntity.findValue("resposta");
		String txtLog = "Resposta informada na tarefa de falta de efetivo " + activity.getProcess().getCode() +": " + txtResposta;
		QLPresencaUtils.saveLog(centroCusto, txtLog);
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}
}
