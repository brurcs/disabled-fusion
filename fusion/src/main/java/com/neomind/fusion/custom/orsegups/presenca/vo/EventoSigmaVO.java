package com.neomind.fusion.custom.orsegups.presenca.vo;


public class EventoSigmaVO
{
	private Long codigo;
	private Long cdCliente;
	private String centralParticao;
	private String tipoEvento;
	private String dataRecebido;
	private String dataDeslocamento;
	private String dataLocal;
	private String dataFechamento;
	private String observacaoFechamento;
	private String tempoDeslocamento;
	private String nomeViatura;
	private String placaViatura;
	
	public Long getCodigo()
	{
		return codigo;
	}
	public void setCodigo(Long codigo)
	{
		this.codigo = codigo;
	}
	public Long getCdCliente()
	{
		return cdCliente;
	}
	public void setCdCliente(Long cdCliente)
	{
		this.cdCliente = cdCliente;
	}
	public String getCentralParticao()
	{
		return centralParticao;
	}
	public void setCentralParticao(String centralParticao)
	{
		this.centralParticao = centralParticao;
	}
	public String getDataRecebido()
	{
		return dataRecebido;
	}
	public void setDataRecebido(String dataRecebido)
	{
		this.dataRecebido = dataRecebido;
	}
	public String getDataDeslocamento()
	{
		return dataDeslocamento;
	}
	public void setDataDeslocamento(String dataDeslocamento)
	{
		this.dataDeslocamento = dataDeslocamento;
	}
	public String getDataLocal()
	{
		return dataLocal;
	}
	public void setDataLocal(String dataLocal)
	{
		this.dataLocal = dataLocal;
	}
	public String getObservacaoFechamento()
	{
		return observacaoFechamento;
	}
	public void setObservacaoFechamento(String observacaoFechamento)
	{
		this.observacaoFechamento = observacaoFechamento;
	}
	public String getTipoEvento()
	{
		return tipoEvento;
	}
	public void setTipoEvento(String tipoEvento)
	{
		this.tipoEvento = tipoEvento;
	}
	public String getDataFechamento()
	{
		return dataFechamento;
	}
	public void setDataFechamento(String dataFechamento)
	{
		this.dataFechamento = dataFechamento;
	}
	public String getTempoDeslocamento()
	{
		return tempoDeslocamento;
	}
	public void setTempoDeslocamento(String tempoDeslocamento)
	{
		this.tempoDeslocamento = tempoDeslocamento;
	}
	public String getPlacaViatura()
	{
		return placaViatura;
	}
	public void setPlacaViatura(String placaViatura)
	{
		this.placaViatura = placaViatura;
	}
	public String getNomeViatura()
	{
		return nomeViatura;
	}
	public void setNomeViatura(String nomeViatura)
	{
		this.nomeViatura = nomeViatura;
	}
}
