package com.neomind.fusion.custom.orsegups.camerite.bean;

import java.io.Serializable;

public class CameriteCamera implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    private int id;
    private String title;
    private String sigmaIdCentral;
    private int sigmaEmpresa;
    private String sigmaParticao;
    private String sigmaAuxiliar;
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getSigmaIdCentral() {
        return sigmaIdCentral;
    }
    public void setSigmaIdCentral(String sigmaIdCentral) {
        this.sigmaIdCentral = sigmaIdCentral;
    }
    public int getSigmaEmpresa() {
        return sigmaEmpresa;
    }
    public void setSigmaEmpresa(int sigmaEmpresa) {
        this.sigmaEmpresa = sigmaEmpresa;
    }
    public String getSigmaParticao() {
        return sigmaParticao;
    }
    public void setSigmaParticao(String sigmaParticao) {
        this.sigmaParticao = sigmaParticao;
    }
    public String getSigmaAuxiliar() {
        return sigmaAuxiliar;
    }
    public void setSigmaAuxiliar(String sigmaAuxiliar) {
        this.sigmaAuxiliar = sigmaAuxiliar;
    }
    
    

}
