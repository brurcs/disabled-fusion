package com.neomind.fusion.custom.orsegups.ti.painelMonitores.engine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.neomind.fusion.custom.orsegups.ti.painelMonitores.beans.MonitoresBean;

public class MonitoresEngine {
    
    
    private static final String CPU_USAGE = " cpu get loadpercentage";  
    
    private static final String FREE_MEMORY = " OS get FreePhysicalMemory";
    
    public MonitoresBean getCpuUsage(String hostname){
		
	return this.doAction(CPU_USAGE, hostname);
    }
    
    public MonitoresBean getMemoryUsage(String hostname){
	
	return this.doAction(FREE_MEMORY, hostname);
    }
    
    
    
    private MonitoresBean doAction (String acao, String hostname){
	
	MonitoresBean retorno = new MonitoresBean();
	
	String valor = "";
	
	try {
		Process p = Runtime.getRuntime().exec("wmic /node:"+hostname+acao);

		BufferedReader reader = new BufferedReader(new InputStreamReader(
				p.getInputStream()));

		String line = reader.readLine();
		int lineCount = 1;
		while (line != null) {
		    
		    if(lineCount == 3){
			valor = line;
		    }
		    
		    line = reader.readLine();
		    lineCount ++;
		}
		
		if (acao.equals(CPU_USAGE)){
		    retorno.setCpuUsage(Integer.parseInt(valor.trim()));
		}else if (acao.equals(FREE_MEMORY)){
		    retorno.setMemoryUsage(Integer.parseInt(valor.trim()));
		}
		
		
	
	}catch(IOException e){
	    e.printStackTrace();
	}
	
	return retorno;
    }

}
