package com.neomind.fusion.custom.orsegups.fap.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.dto.HistoricoFapDTO;
import com.neomind.fusion.custom.orsegups.fap.dto.ItemOrcamentoDTO;
import com.neomind.fusion.custom.orsegups.fap.dto.TecnicoDTO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

/**
 * Classe DAO de acesso a dados para FAP's de aplicação Técnico.
 * 
 * @author diogo.silva
 * 
 */
public class AplicacaoTecnicoDAO
{
	/**
	 * Método para consultar o total de contas de CFTV e ALARME associadas a um
	 * técnico em específico. Essa quantidade será multiplicada pelos devidos
	 * valores e resultará no total a ser pago. P.S.: Apenas contas conciliadas
	 * serão tidas como válidas para pagamento.
	 * 
	 * @param codTecnico
	 *            - Objeto Long. É o código do técnico (cd_colaborador) que
	 *            servirá como parâmetro na consulta.
	 * @return tecnicoDTO - Objeto TecnicoDTO contendo as quantidades de conta
	 *         de CFTV e de Alarme.
	 * @throws WorkflowException
	 *             Caso nenhuma conta conciliada seja encontrada.
	 * @throws Exception
	 *             Para exceções generalizadas.
	 */
	public static TecnicoDTO consultaTotalPagamentos(Long codTecnico) throws Exception, WorkflowException
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		Long codigoTecnico = null;
		StringBuilder sql = new StringBuilder();

		String tipoInstalacao = null;
		TecnicoDTO tecnicoDTO = new TecnicoDTO();

		codigoTecnico = codTecnico;

		sql.append("SELECT CD_COLABORADOR, NM_COLABORADOR, Tipo, COUNT(*) AS TOTAL                                                                                                   ");
		sql.append("	FROM (SELECT DISTINCT t.CD_COLABORADOR, t.NM_COLABORADOR, c.ID_EMPRESA, c.ID_CENTRAL,                                                                        ");
		sql.append("	CASE WHEN PARTICAO LIKE '098' OR PARTICAO LIKE '099' THEN 'CFTV' ELSE 'ALARME' END Tipo, DFAP.valorCFTV AS ValorCFTV,                                        ");
		sql.append("			DFAP.valorAlarme AS ValorAlarme, DFAPMO.codigo AS modalidadePag                                                                              ");
		sql.append("	FROM dbCENTRAL c WITH (NOLOCK)             			  																									");
		sql.append("		INNER JOIN COLABORADOR t WITH (NOLOCK) ON t.CD_COLABORADOR = c.CD_TECNICO_RESPONSAVEL                                                                ");
		sql.append("		INNER JOIN [CACUPE\\SQL02].fusion_producao.DBO.X_SIGMACOLABORADOR XCOL WITH (NOLOCK) ON T.CD_COLABORADOR = XCOL.CD_COLABORADOR                       ");
		sql.append("		INNER JOIN [CACUPE\\SQL02].fusion_producao.DBO.D_FAPAplicacaoTecnico DFAP WITH (NOLOCK) ON XCOL.NEOID = DFAP.TECNICO_NEOID                           ");
		sql.append("		INNER JOIN [CACUPE\\SQL02].fusion_producao.DBO.D_FAPTecnicoModalidadePagamento DFAPMO WITH (NOLOCK) ON DFAP.MODALIDADEPAG_NEOID = DFAPMO.NEOID       ");
		sql.append("            WHERE c.CTRL_CENTRAL = 1 AND t.FG_ATIVO_COLABORADOR = 1 AND c.ID_CENTRAL NOT LIKE 'AAA%' AND c.ID_CENTRAL NOT LIKE 'DDDD' AND                        ");
		sql.append("                  c.ID_CENTRAL NOT LIKE 'FFFF' AND ID_CENTRAL NOT LIKE 'R%' AND NM_COLABORADOR LIKE '%- TERC -%'                                                 ");
		sql.append("                  AND EXISTS (select sig.usu_codcli  from [FSOODB04\\SQL02].sapiens.dbo.usu_t160ctr ctr                                                          ");
		sql.append("                    	inner join [FSOODB04\\SQL02].sapiens.dbo.usu_t160cvs cvs on cvs.usu_codemp = ctr.usu_codemp                                          ");
		sql.append("                    	and cvs.usu_codfil = ctr.usu_codfil                                                                                                  ");
		sql.append("						and cvs.usu_numctr = ctr.usu_numctr 										     ");
		sql.append("                        inner join [FSOODB04\\SQL02].sapiens.dbo.usu_t160sig sig on sig.usu_codemp = ctr.usu_codemp                                              ");
		sql.append("                        and sig.usu_codfil = ctr.usu_codfil 												     ");
		sql.append("						and sig.usu_numctr = ctr.usu_numctr 						      				     ");
		sql.append("						and sig.usu_numpos = cvs.usu_numpos ");
		sql.append("						where ((ctr.usu_sitctr = 'A') or (ctr.usu_sitctr = 'I' and ctr.usu_datfim >= getdate())) ");
		sql.append("						and ((cvs.usu_sitcvs = 'A') or (cvs.usu_sitcvs = 'I' and cvs.usu_datfim >= getdate())) ");
		sql.append("						and sig.usu_codcli = CD_CLIENTE))																			          				");
		sql.append("AS base ");
		sql.append("WHERE CD_COLABORADOR =  " + codigoTecnico);
		sql.append("GROUP BY CD_COLABORADOR, NM_COLABORADOR, Tipo ORDER BY  1, 2");
		
		System.out.println("AplicacaoTecnicoDAO.consultaTotalPagamentos Técnico: " + codigoTecnico);

		try
		{
			conn = PersistEngine.getConnection("SIGMA90");
			pstm = conn.prepareStatement(sql.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = pstm.executeQuery();
			if (rs.next())
			{

				tecnicoDTO.setCodTecnico(rs.getLong(1));
				tecnicoDTO.setNomeTecnico(rs.getString(2));

				rs.beforeFirst();

				while (rs.next())
				{
					tipoInstalacao = rs.getString(3);

					if (tipoInstalacao.equalsIgnoreCase("CFTV"))
					{
						tecnicoDTO.setQtdContaCFTV(rs.getInt(4));
					}
					else if (tipoInstalacao.equalsIgnoreCase("ALARME"))
					{
						tecnicoDTO.setQtdContaAlarme(rs.getInt(4));
					}
				}
				return tecnicoDTO;
			}
			else
			{
				throw new WorkflowException("O técnico " + codigoTecnico + " não possui contas vinculadas a ele no SIGMA.");
			}
		}
		catch (WorkflowException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	/**
	 * Método para consultar o fornecedor associado a um técnico em específico.
	 * 
	 * @param codTecnico
	 *            (Long)
	 * @return NeoObject
	 * @throws WorkflowException
	 * @Deprecated - O fornecedor estará no formulário de cadastro do financeiro
	 *             e não mais em um formulário separado.
	 */
	public static NeoObject consultaFornecedor(Long codTecnico) throws Exception, WorkflowException
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		Long tecnico = null;

		tecnico = codTecnico;
		conn = PersistEngine.getConnection("");

		sql.append(" SELECT SIG.cd_colaborador, SIG.neoId, TECFOR.tecnicoo_neoId, FORN.CODFOR, FORN.NOMFOR FROM D_FAPASSOCTECFOR TECFOR ");
		sql.append(" INNER JOIN X_SIGMACOLABORADOR SIG ON TECFOR.TECNICOO_NEOID = SIG.NEOID               ");
		sql.append("INNER JOIN X_ETABFOR TABFOR ON TABFOR.NEOID = TECFOR.FORNECEDOR_NEOID                   ");
		sql.append(" INNER JOIN [FSOODB04\\SQL02].SAPIENS.DBO.E095FOR FORN ON TABFOR.CODFOR = FORN.CODFOR ");
		sql.append(" WHERE sig.cd_colaborador = ? ");
		sql.append(" AND TECFOR.ATIVO  = 1 ");

		try
		{
			pstm = conn.prepareStatement(sql.toString());
			pstm.setLong(1, tecnico);
			rs = pstm.executeQuery();

			if (rs.next())
			{
				QLEqualsFilter fFornecedor = new QLEqualsFilter("codfor", rs.getInt("CODFOR"));
				NeoObject efFornecedor = PersistEngine.getObject(AdapterUtils.getEntityClass("ETABFOR"), fFornecedor);
				if (efFornecedor == null)
				{
					throw new WorkflowException("Fornecedor " + rs.getString("CODFOR") + " - " + rs.getString("NOMFOR") + " não encontrado no Fusion.");
				}
				else
				{
					return efFornecedor;
				}
			}
			else
			{
				throw new WorkflowException("O técnico " + tecnico + " não está associado a nenhum fornecedor. " + "É necessário cadastrá-lo na tela de associação Técnico x Fornecedor.");
			}

		}
		catch (WorkflowException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	/**
	 * Método para consultar a lista de centros de custos de acordo com um
	 * técnico específico.
	 * 
	 * @param codTecnico
	 *            - Objeto Long. Representa o código do técnico (cd_colaborador)
	 *            que será usado como parâmetro na consulta.
	 * @return listaCcu - Objeto List contendo NeoObjects com as informações dos
	 *         centros de custos encontrados.
	 * @throws WorkflowException
	 * @throws Exception
	 *             Para exceções generalizadas.
	 */
	public static List<NeoObject> consultaListaCcu(Long codTecnico) throws Exception, WorkflowException
	{

		StringBuilder sql = new StringBuilder();
		String fonteDados = null;
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		Long codigo = null;
		List<NeoObject> listaCcu = new ArrayList<>();

		fonteDados = "SAPIENS";
		conn = PersistEngine.getConnection(fonteDados);

		codigo = codTecnico;

		sql.append(" SELECT sig.usu_codemp, sig.usu_codfil, sig.usu_numctr,CVS.usu_codccu FROM usu_t160sig SIG                  ");
		sql.append(" INNER JOIN [FSOODB03\\SQL01].Sigma90.dbo.dbCENTRAL c WITH (NOLOCK)                                         ");
		sql.append(" 		ON C.CD_CLIENTE =  SIG.usu_codcli                                                           ");
		sql.append(" 		AND C.ID_EMPRESA = SIG.usu_sigemp                                                           ");
		sql.append(" 		AND C.ID_CENTRAL COLLATE DATABASE_DEFAULT = SIG.usu_sigcon COLLATE DATABASE_DEFAULT         ");
		sql.append(" 		AND C.PARTICAO = SIG.usu_sigpar 						      	    ");
		sql.append(" INNER JOIN USU_T160CVS CVS  WITH (NOLOCK)                                                                  ");
		sql.append(" 		ON SIG.usu_codemp = CVS.usu_codemp                                                          ");
		sql.append(" 		AND SIG.usu_codfil = CVS.usu_codfil                                                         ");
		sql.append(" 		AND SIG.usu_numctr = CVS.usu_numctr                                                         ");
		sql.append("  		AND SIG.usu_numpos = CVS.usu_numpos                                                         ");
		sql.append("  INNER JOIN USU_T160CTR CTR  WITH (NOLOCK)                                                                 ");
		sql.append("                ON CTR.usu_codemp = CVS.usu_codemp                                                          ");
		sql.append("                AND CTR.usu_codfil = CVS.usu_codfil                                                         ");
		sql.append("                AND CTR.usu_numctr = CVS.usu_numctr                                                         ");
		sql.append(" WHERE c.CTRL_CENTRAL = 1                                                                                   ");
		sql.append(" AND c.ID_CENTRAL NOT LIKE 'AAA%'                                                                           ");
		sql.append(" AND c.ID_CENTRAL NOT LIKE 'DDDD'                                                                           ");
		sql.append(" AND c.ID_CENTRAL NOT LIKE 'FFFF'                                                                           ");
		sql.append(" AND ((CVS.usu_sitcvs = 'A') OR (CVS.usu_sitcvs = 'I' AND CVS.usu_datfim >= GETDATE()))                     ");
		sql.append(" AND ((CTR.usu_sitctr = 'A') OR (CTR.usu_sitctr = 'I' AND CTR.usu_datfim >= GETDATE()))                     ");
		sql.append(" AND C.CD_TECNICO_RESPONSAVEL = " + codigo);
		sql.append(" GROUP BY sig.usu_codemp, sig.usu_codfil, sig.usu_numctr,CVS.usu_codccu                                     ");
		sql.append(" ORDER BY SIG.usu_codemp, CVS.usu_codccu                                                                    ");

		try
		{
			pstm = conn.prepareStatement(sql.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = pstm.executeQuery();

			while (rs.next())
			{
				NeoObject efCentroDeCusto = AdapterUtils.getInstantiableEntityInfo("FAPListaCentroDeCusto").createNewInstance();
				EntityWrapper ewCentroDeCusto = new EntityWrapper(efCentroDeCusto);
				Long empresa = rs.getLong(1);
				String codCcu = rs.getString(4);
				ewCentroDeCusto.setValue("codigoEmpresa", empresa);
				ewCentroDeCusto.setValue("codigoCentroCusto", codCcu);

				PersistEngine.persist(efCentroDeCusto);
				listaCcu.add(efCentroDeCusto);
			}
			return listaCcu;

		}
		catch (WorkflowException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	/**
	 * Retorna as tarefas de FAP já abertas para um técnico em específico. Será
	 * utilizada em um converter para mostrar na tarefa atual o histórico de
	 * todos os pagamentos já efetuados para o técnico em questão.
	 * 
	 * @param codigo
	 *            - Objeto Long. Representa o código do técnico (cd_colaborador)
	 *            que será usado como parâmetro na consulta.
	 * @param aplicacao
	 *            - Objeto Long. É o código da aplicação de pagamento que será
	 *            utilizado em um método que consulta os itens de pagamento.
	 * @param neoId
	 *            - Objeto Long. Neoid da FAP de onde o método foi chamado pois
	 *            a mesma deve ser ignorada desta consulta.
	 * @return tecnicoDTO - Objeto TecnicoDTO onde serão armazenados os dados
	 *         das tarefas já abertas (histórico).
	 * @throws Exception
	 *             Para exceções generalizadas.
	 */
	public static TecnicoDTO recuperaHistoricoTecnico(Long codigo, Long aplicacao, Long neoId) throws Exception
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();

		List<HistoricoFapDTO> listaHistoricoFap = new ArrayList<>();
		List<ItemOrcamentoDTO> listaItemOrcamento = null;
		TecnicoDTO tecnicoDTO = new TecnicoDTO();

		Long codigoTecnico = codigo;
		Long wfNeoId = neoId;
		Long codigoAplicacao = aplicacao;

		// TO DO perguntar pro Diogo pq o rotaSigma esta nesse select
		// sql.append(" SELECT WF.code, SIGCOL.cd_colaborador,FAPAUT.dataSolicitacao,FAPAUT.rotaSigma_neoId,wf.neoid AS wfNeoid,  ");
		sql.append(" SELECT WF.code, SIGCOL.cd_colaborador,FAPAUT.dataSolicitacao,wf.neoid AS wfNeoid,  ");
		sql.append(" FAPAUT.tecnicoSigma_neoId, FAPTEC.valorCFTV, FAPTEC.valorAlarme, WF.processState,FAPAUT.NEOID AS fapNeoId ");
		sql.append(" FROM WFPROCESS WF                                                                                         ");
		sql.append(" INNER JOIN D_FAPAutorizacaoDePagamento FAPAUT ON FAPAUT.wfprocess_neoId = WF.neoId                        ");
		sql.append(" INNER JOIN D_FAPAplicacaoTecnico FAPTEC ON FAPAUT.tecnicoSigma_neoId = FAPTEC.neoId                       ");
		sql.append(" INNER JOIN X_SIGMACOLABORADOR SIGCOL ON SIGCOL.neoId = FAPTEC.tecnico_neoId                               ");
		sql.append(" WHERE WF.saved = 1 AND (WF.processState = 0 OR WF.processState = 1)                                       ");
		sql.append(" AND WF.neoId <> " + wfNeoId);
		sql.append(" AND SIGCOL.cd_colaborador = " + codigoTecnico);
		sql.append(" ORDER BY SIGCOL.cd_colaborador                                                                            ");
		// AND FAPAUT.aprovado = 1 colocar novamente depois dos testes.
		try
		{
			conn = PersistEngine.getConnection("");
			pstm = conn.prepareStatement(sql.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = pstm.executeQuery();

			if (rs.next())
			{
				rs.beforeFirst();

				while (rs.next())
				{
					HistoricoFapDTO historicoFap = new HistoricoFapDTO();
					String codigoTarefa = rs.getString("code");
					Date dataSolicitacao = rs.getDate("dataSolicitacao");
					int status = rs.getInt("processState");
					Long fapNeoId = rs.getLong("fapNeoId");
					Long wfId = rs.getLong("wfNeoid");
					// historicoFap.setLaudo(laudo);
					// historicoFap.setRelato(relato);

					historicoFap.setCodigoTarefa(codigoTarefa);
					historicoFap.setDataSolicitacao(dataSolicitacao);
					historicoFap.setNeoIdTarefa(wfId);
					switch (status)
					{
						case 0:
							historicoFap.setStatus("Em andamento");
							break;
						case 1:
							historicoFap.setStatus("Encerrado");
							break;
					}

					// recupera os itens de orcamento da FAP atual
					listaItemOrcamento = AplicacaoTecnicoDAO.consultaItemOrcamento(fapNeoId, codigoAplicacao);

					BigDecimal valorTotal = new BigDecimal(0.00);
					if (NeoUtils.safeIsNotNull(listaItemOrcamento))
					{
						for (ItemOrcamentoDTO item : listaItemOrcamento)
						{
							valorTotal = valorTotal.add(item.getValor());
						}
						// Objeto que representa a FAP atual recebe os itens de
						// orçamento e o valor total
						historicoFap.setItemOrcamento(listaItemOrcamento);
						historicoFap.setValorTotal(valorTotal);
						// Objeto da aplicação recebe a lista de suas
						// respectivas FAP's
						listaHistoricoFap.add(historicoFap);
						tecnicoDTO.setHistorico(listaHistoricoFap);
					}
				}
			}
			return tecnicoDTO;
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	/**
	 * Consulta os itens de orçamento para o histórico de pagamentos.
	 * 
	 * @param neoId
	 *            - Objeto Long. É o neoid da tarefa atual, utilizado como
	 *            parâmetro na consulta.
	 * @param aplicacao
	 *            - Objeto Long. Representa a aplicação de pagamento que também
	 *            servirá de filtro para a consulta.
	 * @return listaItens - Objeto List contendo ItemOrcamentoDTO's onde constam
	 *         as informações dos itens de pagamento.
	 * @throws Exception
	 *             Para exceções generalizadas.
	 */
	public static List<ItemOrcamentoDTO> consultaItemOrcamento(Long neoId, Long aplicacao) throws Exception
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();

		List<ItemOrcamentoDTO> listaItens = new ArrayList<>();
		Long codigoAplicacao = aplicacao;
		Long fapNeoid = neoId;

		sql.append(" SELECT FAP.NEOID, ITEM.DESCRICAO, ITEM.VALOR,TIPORC.descricao FROM D_FAPAutorizacaoDePagamento FAP                ");
		sql.append(" INNER JOIN D_FAPAUTORIZACAODEPAGAMENTO_ITEMORCAMENTO ASSOC ON FAP.neoId = ASSOC.D_FAPAutorizacaoDePagamento_neoId ");
		sql.append(" INNER JOIN D_FAPItemDeOrcamento ITEM ON ASSOC.itemOrcamento_neoId = ITEM.neoId                                    ");
		sql.append(" INNER JOIN D_FAPTIPOITEMDEORCAMENTO TIPORC ON ITEM.TIPOITEM_NEOID = TIPORC.NEOID                                  ");
		sql.append(" WHERE FAP.codigoAplicacaoPagamento = " + codigoAplicacao);
		sql.append(" AND ASSOC.D_FAPAutorizacaoDePagamento_neoId = " + fapNeoid);

		try
		{
			conn = PersistEngine.getConnection("");

			pstm = conn.prepareStatement(sql.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = pstm.executeQuery();

			while (rs.next())
			{
				ItemOrcamentoDTO item = new ItemOrcamentoDTO();
				String descricaoItem = rs.getString(2);
				BigDecimal valorItem = new BigDecimal(rs.getInt(3));
				String tipoOrc = rs.getString(4);

				item.setDescricao(descricaoItem);
				item.setTipo(tipoOrc);
				item.setValor(valorItem);

				listaItens.add(item);
			}
			return listaItens;
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	/**
	 * Consulta e insere as informações das contas no formulário
	 * FapHistoricoContas.
	 * 
	 * @param codigoTecnico
	 *            - Objeto Long. É o código do técnico (cd_colaborador) que será
	 *            utilizado como parâmetro na consulta.
	 * @return listaContas - Objeto List contendo NeoObject's. Trata-se de uma
	 *         lista com os formulários que armazenam as informações das contas.
	 * @throws Exception
	 *             Para exceções generalizadas.
	 * */
	public static List<NeoObject> retornaContas(Long codigoTecnico) throws Exception
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();

		Long codigo = codigoTecnico;
		List<NeoObject> listaContas = new ArrayList<>();

		sql.append(" SELECT DISTINCT C.RAZAO, C.ID_CENTRAL, CASE WHEN PARTICAO LIKE '098' OR PARTICAO LIKE '099' THEN 'CFTV' ELSE 'ALARME' END TIPO ");
		sql.append(" FROM dbCENTRAL C WITH (NOLOCK)   ");
		sql.append(" INNER JOIN COLABORADOR T WITH (NOLOCK) ON T.CD_COLABORADOR = C.CD_TECNICO_RESPONSAVEL ");
		sql.append(" WHERE C.CTRL_CENTRAL = 1 AND T.FG_ATIVO_COLABORADOR = 1   ");
		sql.append(" AND C.ID_CENTRAL NOT LIKE 'AAA%' AND C.ID_CENTRAL NOT LIKE 'DDDD' 	AND C.ID_CENTRAL NOT LIKE 'FFFF' ");
		sql.append(" AND T.NM_COLABORADOR LIKE '%TERC%' AND t.CD_COLABORADOR = ? ");

		try
		{
			conn = PersistEngine.getConnection("SIGMA90");
			pstm = conn.prepareStatement(sql.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			pstm.setLong(1, codigo);
			rs = pstm.executeQuery();

			if (rs.next())
			{
				rs.beforeFirst();

				while (rs.next())
				{
					String razao = rs.getString("RAZAO");
					String central = rs.getString("ID_CENTRAL");
					String tipoConta = rs.getString("TIPO");

					NeoObject wfHistorico = AdapterUtils.getInstantiableEntityInfo("FapHistoricoContas").createNewInstance();
					EntityWrapper ewHistorico = new EntityWrapper(wfHistorico);

					if (NeoUtils.safeIsNotNull(razao, central, tipoConta))
					{
						ewHistorico.setValue("cliente", razao);
						ewHistorico.setValue("central", central);
						ewHistorico.setValue("tipoConta", tipoConta);
					}

					PersistEngine.persist(wfHistorico);

					listaContas.add(wfHistorico);
				}
				return listaContas;
			}
			else
			{
				return null;
			}
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

}
