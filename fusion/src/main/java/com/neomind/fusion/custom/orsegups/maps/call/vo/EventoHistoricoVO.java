package com.neomind.fusion.custom.orsegups.maps.call.vo;


public class EventoHistoricoVO
{
	private String dtRecebido;
	private String cdEvento;
	private String tipoEvento;
	private String dtEspera;
	private String dtVtrDeslocamento;
	private String dtVtrLocal;
	private String dtFechamento;
	private String nuFechamento;
	private String obsFechamento;
	private String prioridade;
	
	public String getDtRecebido()
	{
		return dtRecebido;
	}
	public void setDtRecebido(String dtRecebido)
	{
		this.dtRecebido = dtRecebido;
	}
	public String getCdEvento()
	{
		return cdEvento;
	}
	public void setCdEvento(String cdEvento)
	{
		this.cdEvento = cdEvento;
	}
	public String getTipoEvento()
	{
		return tipoEvento;
	}
	public void setTipoEvento(String tipoEvento)
	{
		this.tipoEvento = tipoEvento;
	}
	public String getDtEspera()
	{
		return dtEspera;
	}
	public void setDtEspera(String dtEspera)
	{
		this.dtEspera = dtEspera;
	}
	public String getDtVtrDeslocamento()
	{
		return dtVtrDeslocamento;
	}
	public void setDtVtrDeslocamento(String dtVtrDeslocamento)
	{
		this.dtVtrDeslocamento = dtVtrDeslocamento;
	}
	public String getDtVtrLocal()
	{
		return dtVtrLocal;
	}
	public void setDtVtrLocal(String dtVtrLocal)
	{
		this.dtVtrLocal = dtVtrLocal;
	}
	public String getDtFechamento()
	{
		return dtFechamento;
	}
	public void setDtFechamento(String dtFechamento)
	{
		this.dtFechamento = dtFechamento;
	}
	public String getNuFechamento()
	{
		return nuFechamento;
	}
	public void setNuFechamento(String nuFechamento)
	{
		this.nuFechamento = nuFechamento;
	}
	public String getObsFechamento()
	{
		return obsFechamento;
	}
	public void setObsFechamento(String obsFechamento)
	{
		this.obsFechamento = obsFechamento;
	}
	public String getPrioridade()
	{
		return prioridade;
	}
	public void setPrioridade(String prioridade)
	{
		this.prioridade = prioridade;
	}
	
	
	
}
