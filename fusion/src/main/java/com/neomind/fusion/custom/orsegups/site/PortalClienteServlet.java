package com.neomind.fusion.custom.orsegups.site;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;


@WebServlet(name="PortalClienteServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.site.PortalClienteServlet"})
public class PortalClienteServlet extends HttpServlet
{
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	public void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException
	{
		response.setContentType("text/html");
		response.setCharacterEncoding("ISO-8859-1");
		response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type");
        response.setHeader("Access-Control-Max-Age", "86400");
        response.setContentType("application/json");
		
		JSONObject retorno = new JSONObject();
		

		PrintWriter out = null;
		try
		{
			out = response.getWriter();
			
			String action = "";
			if (request.getParameter("action") != null)
			{
				action = request.getParameter("action");
				System.out.println("[PortalClienteServlet] - action:"+action);
			}
			
			
			if (action.equalsIgnoreCase("testeNVOX"))
			{
				retorno = this.testeNVOX(request);
			}
			System.out.println("[PortalClienteServlet] - action:"+action+", retorno ->" + retorno);
			out.print(retorno);
			out.flush();
			out.close();
			
			
		}
		catch (IOException e)
		{
			try{
				retorno.put("status",99L);
				retorno.put("msg","Erro ao processar Requisicao.");
				retorno.put("ret",(Object)null);
			}catch(JSONException e1){
				// nunca vai chegar aqui
				e1.printStackTrace();
			}
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private JSONObject testeNVOX(HttpServletRequest request)
	{
		JSONObject retorno = new JSONObject();
		
		JSONObject ret = null;
		try{
			Enumeration<String> reqs = request.getParameterNames();
			
			if (reqs!= null && reqs.hasMoreElements()){
				ret = new JSONObject();
				do{
					String paramName = reqs.nextElement();
					String paramValue = request.getParameter(paramName);
					ret.put(paramName, paramValue);
				}while(reqs.hasMoreElements());
			}
			if (ret != null){
				retorno.put("status", 100L);
				retorno.put("msg", "Sucesso");
				retorno.put("ret", ret);
			}else{
				retorno.put("status", 98L);
				retorno.put("msg", "Nenhuma informação disponvivel");
				retorno.put("ret", (Object)null);
			}
			return retorno;
		}catch(Exception e){
			System.out.println("Erro PortalCLienteServlet");
			e.printStackTrace();
			try{
				retorno.put("status", 98L);
				retorno.put("msg", "Nenhuma informação disponvivel");
				retorno.put("ret", (Object)null);
				return retorno;
			}catch(JSONException e1){
				e1.printStackTrace();
				return null;
			}
		}
		
	}
	
	

}
