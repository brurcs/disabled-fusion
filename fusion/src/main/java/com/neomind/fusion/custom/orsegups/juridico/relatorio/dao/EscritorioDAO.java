package com.neomind.fusion.custom.orsegups.juridico.relatorio.dao;

import java.util.List;

import com.neomind.fusion.custom.orsegups.juridico.relatorio.dto.EscritorioDTO;

public interface EscritorioDAO {

	List<EscritorioDTO> listaTodosEscResp() throws Exception;

}
