package com.neomind.fusion.custom.orsegups.servlets.seniorCommands;

import javax.servlet.http.HttpServletRequest;

public interface Command {

    public String execute(HttpServletRequest request);
    
}
