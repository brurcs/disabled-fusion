package com.neomind.fusion.custom.orsegups.scheduler;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoCalendarUtils;
import com.neomind.util.NeoDateUtils;

public class RotinaImportaImagensEvento implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(RotinaImportaImagensEvento.class);

	@Override
	public void execute(CustomJobContext arg0)
	{
		Connection conn = null;
		StringBuilder strSigma = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		List<EventoVO> eventoVOs = null;
		try
		{
			conn = PersistEngine.getConnection("SIGMA90");
			strSigma = new StringBuilder();
			strSigma.append(" 	 SELECT top 5 h.CD_HISTORICO,c.ID_CENTRAL,C.ID_EMPRESA, c.PARTICAO,h.CD_CODE ,h.DT_RECEBIDO ");
			strSigma.append(" 	 FROM VIEW_HISTORICO h  WITH (NOLOCK)   ");
			strSigma.append(" 	 INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
			strSigma.append(" 	 INNER JOIN ROTA r  WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA   ");
			strSigma.append(" 	 INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE  ");
			strSigma.append(" 	 INNER JOIN HISTORICO_FRASE_EVENTO hfe ON hfe.CD_FRASE_EVENTO = h.CD_FRASE_EVENTO  ");
			strSigma.append(" 	 LEFT JOIN MOTIVO_ALARME ma ON ma.CD_MOTIVO_ALARME = h.CD_MOTIVO_ALARME  ");
			strSigma.append(" 	 LEFT JOIN dbCIDADE cid ON cid.ID_CIDADE = c.ID_CIDADE  ");
			strSigma.append(" 	 LEFT JOIN dbBAIRRO bai ON bai.ID_CIDADE = c.ID_CIDADE AND bai.ID_BAIRRO = c.ID_BAIRRO  ");
			strSigma.append(" 	 WHERE  c.ctrl_central = 1 AND (( H.CD_CODE = 'ACP')  OR (H.CD_CODE = 'ACV' ) )   ");
			strSigma.append(" 	 AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
			strSigma.append(" 	 AND CD_USUARIO_FECHAMENTO <> 9999     ");
			strSigma.append(" 	 AND DT_RECEBIDO >=  DATEADD(MINUTE, -500, GetDate())   ");

			pstm = conn.prepareStatement(strSigma.toString());

			rs = pstm.executeQuery();

			eventoVOs = new ArrayList<EventoVO>();
			EventoVO eventoVO = null;
			InstantiableEntityInfo excecoesEventos = (InstantiableEntityInfo) EntityRegister.getEntityInfo("SIGMAImagensEventoAcompanhamento");

			while (rs.next())
			{
				String historico = (rs.getString("CD_HISTORICO") == null ? "" : rs.getString("CD_HISTORICO"));
				String empresa = (rs.getString("ID_EMPRESA") == null ? "" : rs.getString("ID_EMPRESA"));
				String idCentral = (rs.getString("ID_CENTRAL") == null ? "" : rs.getString("ID_CENTRAL"));
				String particao = (rs.getString("PARTICAO") == null ? "" : rs.getString("PARTICAO"));
				Timestamp dtRecebido = rs.getTimestamp("DT_RECEBIDO");
				String dataRecebidoStr = NeoDateUtils.safeDateFormat(dtRecebido, "dd/MM/yyyy HH:mm:ss");
				String cdCode = (rs.getString("CD_CODE") == null ? "" : rs.getString("CD_CODE"));

				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				groupFilter.addFilter(new QLEqualsFilter("historico", historico));

				@SuppressWarnings("unchecked")
				List<NeoObject> neoObjects = PersistEngine.getObjects(excecoesEventos.getEntityClass(), groupFilter);

				if (neoObjects == null || (neoObjects != null && neoObjects.isEmpty()))
				{
					eventoVO = new EventoVO();

					eventoVO.setCodigoHistorico(historico);
					eventoVO.setEmpresa(empresa);
					eventoVO.setCodigoCentral(idCentral);
					eventoVO.setParticao(particao);
					eventoVO.setCdCode(cdCode);
					eventoVO.setDataRecebimento(dataRecebidoStr);
					eventoVOs.add(eventoVO);
				}
			}
			OrsegupsUtils.closeConnection(conn, pstm, rs);
			if (eventoVOs != null && !eventoVOs.isEmpty())
			{
				for (EventoVO eventoVOAux : eventoVOs)
				{

					TreeMap<Integer, String> treeMap = getCameraCliente(eventoVOAux.getCodigoCentral(), eventoVOAux.getEmpresa());

					List<String> cameras = new ArrayList<String>();
					if (treeMap != null && !treeMap.isEmpty())
					{
						for (Entry<Integer, String> entry : treeMap.entrySet())
						{

							String camera = "";
							if (entry != null)
							{
								camera = String.valueOf(entry.getKey());
								String descricaoCamera = entry.getValue();
								if (eventoVO.getCdCode() != null && eventoVO.getCdCode().equals("ACP") && descricaoCamera != null && !descricaoCamera.contains("[VIP]"))
								{
									cameras.add(camera);
								}
								else if (eventoVO.getCdCode() != null && eventoVO.getCdCode().equals("ACV"))
								{
									if (descricaoCamera != null && descricaoCamera.contains("[VIP]"))
									{
										cameras.add(0, camera);
									}
									else
									{
										cameras.add(camera);
									}
								}
							}
						}

					}

					for (int i = 0; i <= 4; i++)
					{
						GregorianCalendar calendar = new GregorianCalendar();
						calendar = (NeoCalendarUtils.stringToFullDate(eventoVOAux.getDataRecebimento()));
						if (i == 1)
							calendar.add(Calendar.SECOND, +5);
						else if (i == 2)
							calendar.add(Calendar.SECOND, +10);
						else if (i == 3)
							calendar.add(Calendar.SECOND, +20);
						else if (i == 4)
							calendar.add(Calendar.SECOND, +25);

						String dataHora = NeoDateUtils.safeDateFormat(calendar, "dd/MM/yyyy HH:mm:ss");

						String hora = dataHora.substring(10, dataHora.length());
						hora = hora.trim();
						hora = hora.replaceAll(":", "-");
						String data = dataHora.substring(0, 10);
						data = data.trim();
						data = data.replaceAll("/", "-");

						for (String camera : cameras)
						{

							InstantiableEntityInfo colAit = AdapterUtils.getInstantiableEntityInfo("SigmaImagensEventoAcompanhamento");
		
							NeoObject noInatUsu = colAit.createNewInstance();
							EntityWrapper tmeWrapper = new EntityWrapper(noInatUsu);
							File file = new File("image");
							file = retornaImagem(data, hora, camera);
							NeoFile newFile = new NeoFile();
							newFile.setName(file.getName());
							newFile.setStorage(NeoStorage.getDefault());
							PersistEngine.persist(newFile);
							PersistEngine.evict(newFile);
							OutputStream outi = newFile.getOutputStream();
							InputStream in = null;
							in = new BufferedInputStream(new FileInputStream(file));
							NeoStorage.copy(in, outi);
							newFile.setOrigin(NeoFile.Origin.TEMPLATE);
							System.out.println(eventoVOAux.getCodigoHistorico());
							tmeWrapper.findField("historico").setValue(eventoVOAux.getCodigoHistorico());
							tmeWrapper.findField("instante").setValue(Long.parseLong(String.valueOf(i)));
							tmeWrapper.findField("camera").setValue(camera);
							tmeWrapper.findField("imagem").setValue(newFile);
							tmeWrapper.findField("dataCadastro").setValue(new GregorianCalendar());
							PersistEngine.persist(noInatUsu);

						}
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private File retornaImagem(String data, String hora, String camera)
	{
		String resolucao = "640x480";
		String qualidade = "100";
		String formato = "jpg";
		String urlStr = "http://192.168.20.227/player/getimagem.cgi?camera=" + camera + "&resolucao=" + resolucao + "&qualidade=" + qualidade + "&formato=" + formato + "&data=" + data + "&hora=" + hora;
		ByteArrayOutputStream output = null;
		URL url = null;
		URLConnection uc = null;
		InputStream is = null;
		BufferedImage image = null;
		File outputfile = null;
		try
		{
			output = new ByteArrayOutputStream();
			String userpass = "admin:seventh";

			url = new URL(urlStr);
			uc = url.openConnection();
			String basicAuth = "Basic " + new String(new Base64().encode(userpass.getBytes()));
			uc.setRequestProperty("Authorization", basicAuth);
			is = uc.getInputStream();
			image = ImageIO.read(is);
			outputfile = new File("image.jpg");
			ImageIO.write(image, "jpg", outputfile);

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if (is != null)
					is.close();
				if (output != null)
				{
					output.flush();
					output.close();
				}
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}

		}

		return outputfile;
	}

	public TreeMap<Integer, String> getCameraCliente(String idCentral, String empresa)
	{

		TreeMap<Integer, String> treeMap = null;

		String resposta = "";
		try
		{
			
			String userpass = "admin:seventh";
			URL oracle = new URL("http://192.168.20.227/camerasnomes.cgi?receiver=" + empresa + "&server=" + idCentral);
			URLConnection yc = oracle.openConnection();
			String basicAuth = "Basic " + new String(new Base64().encode(userpass.getBytes()));
			yc.setRequestProperty("Authorization", basicAuth);
			BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null)
			{
				resposta = inputLine;
			}
			in.close();
			if (resposta != null && !resposta.isEmpty() && resposta.contains("="))
			{
				String array[] = resposta.split("&");
				treeMap = new TreeMap<Integer, String>();

				for (String valor : array)
				{
					if (valor != null && !valor.isEmpty() && valor.contains("="))
					{
						String parametros[] = valor.split("=");
						treeMap.put(Integer.parseInt(parametros[0]), (String) parametros[1]);
					}

				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return treeMap;
	}

}
