package com.neomind.fusion.custom.orsegups.adapter;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class ChipGPRSRegistroAtividades implements AdapterInterface
{

	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		InstantiableEntityInfo registroAtividade = AdapterUtils.getInstantiableEntityInfo("ChipsGPRSPostoRegistroAtividades");
		NeoObject registro = registroAtividade.createNewInstance();
		EntityWrapper wRegistro = new EntityWrapper(registro);

		wRegistro.findField("responsavel").setValue(origin.getUser());
		wRegistro.findField("descricao").setValue(wrapper.findValue("descricao"));
		wRegistro.findField("dataInicial").setValue(origin.getStartDate());
		wRegistro.findField("dataFinal").setValue(origin.getFinishDate());
		wRegistro.findField("atividade").setValue(origin.getActivityName());
		wRegistro.findField("prazo").setValue(wrapper.findValue("prazo"));
		
		wrapper.setValue("descricao", null);
		wrapper.setValue("acaoTarefa", null);
		
		PersistEngine.persist(registro);
		wrapper.findField("registroAtividades").addValue(registro);
		
	}

	public void back(EntityWrapper wrapper, Activity activity)
	{
	}
}