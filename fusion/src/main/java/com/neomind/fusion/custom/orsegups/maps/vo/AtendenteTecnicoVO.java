package com.neomind.fusion.custom.orsegups.maps.vo;

public class AtendenteTecnicoVO
{
	private String codigoAtendente;
	
	private String nomeAtendente;

	public String getCodigoAtendente()
	{
		return codigoAtendente;
	}

	public void setCodigoAtendente(String codigoAtendente)
	{
		this.codigoAtendente = codigoAtendente;
	}

	public String getNomeAtendente()
	{
		return nomeAtendente;
	}

	public void setNomeAtendente(String nomeAtendente)
	{
		this.nomeAtendente = nomeAtendente;
	}
	
	
}
