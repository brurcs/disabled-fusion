package com.neomind.fusion.custom.orsegups.medicaoContratos;

import java.util.GregorianCalendar;

import org.apache.commons.mail.EmailException;

import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;

public class EnviaEmailOrsegupsMedicaoUtils22 implements CustomJobAdapter

{

	@Override
	public void execute(CustomJobContext ctx)
	{
		System.out.println("[MED] - iniciando job Envia e-mail Para Medicao de contratos diario");
		processaJob(ctx);
	}

	public static void processaJob(CustomJobContext ctx)
	{
		GregorianCalendar datRef = new GregorianCalendar();
		datRef.add(GregorianCalendar.DATE, -1);
		try
		{
			OrsegupsMedicaoUtils.enviaEmail(datRef);
		}
		catch (EmailException e)
		{
			e.printStackTrace();
		}
	}

}
