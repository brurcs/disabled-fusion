package com.neomind.fusion.custom.orsegups.adapter;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class VerificaUsuariResponsavelSAC implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		NeoPaper papel = new NeoPaper();
		processEntity.setValue("possuiPapelSac", Boolean.FALSE);
		papel = OrsegupsUtils.getPaper("Responsável RSC SAC");
		NeoUser usuarioResponsavel = (NeoUser) processEntity.findValue("executivoResponsavel");
		if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
		{
			for (NeoUser user : papel.getUsers())
			{
				if (user.getCode().equals(usuarioResponsavel.getCode()))
				{
					processEntity.setValue("possuiPapelSac", Boolean.TRUE);
					break;
				}
			}
		}
		Boolean conferi = (Boolean) processEntity.findValue("possuiPapelSac");
		System.out.println("possuiPapelSac >>" + conferi);
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}
}
