package com.neomind.fusion.custom.orsegups.adapter;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.emailmonitor.vo.ResultVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.UserActivity;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;

public class RMCIniciaFluxos implements AdapterInterface{
	
	private static final Log log = LogFactory.getLog(RMCIniciaFluxos.class);
	
	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		Collection<NeoObject> listaTarefaSimples = (Collection<NeoObject>) wrapper.findField("listaTarefaSimples").getValue();
		Collection<NeoObject> listaRelatorioReclamacaoCliente = (Collection<NeoObject>) wrapper.findField("listaRelatorioReclamacaoCliente").getValue();
		NeoObject user = PortalUtil.getCurrentUser();
		ResultVO rVO = null;
		if(listaTarefaSimples.size() > 0)
		{
			for(NeoObject tarefaSimples : listaTarefaSimples)
			{
				NeoObject eformTarefaSimples = retornaEFormTarefaSimples(tarefaSimples);
				rVO = OrsegupsUtils.iniciaWorkflow(eformTarefaSimples, "G001 - Tarefa Simples", user, true);
				(new EntityWrapper(tarefaSimples)).findField("codigo").setValue(rVO.getProcessCode());
			}
		}
		
		if(listaRelatorioReclamacaoCliente.size() > 0)
		{
			for(NeoObject relatorioReclamacaoCliente : listaRelatorioReclamacaoCliente)
			{
				NeoObject eformRRC = retornaEFormRRC(relatorioReclamacaoCliente);
				rVO = OrsegupsUtils.iniciaWorkflow(eformRRC, "Q005 - RRC - Relatório de Reclamação de Cliente", user, true);
				(new EntityWrapper(relatorioReclamacaoCliente)).findField("code").setValue(rVO.getProcessCode());
			}
		}
	}
	
	private NeoObject retornaEFormTarefaSimples(NeoObject obj)
	{
		/*
		 * retorna o eform da entidade Tarefa Simples
		 * com os campos preenchidos pelo usuário
		 */
		InstantiableEntityInfo ieiTarefaSimples = AdapterUtils.getInstantiableEntityInfo("Tarefa");
		NeoObject tarefaSimples = ieiTarefaSimples.createNewInstance();
		EntityWrapper wTarefaSimples = new EntityWrapper(tarefaSimples);
		EntityWrapper wObj = new EntityWrapper(obj);
		wTarefaSimples.findField("Solicitante").setValue(wObj.getValue("solicitante"));
		wTarefaSimples.findField("Executor").setValue(wObj.getValue("executor"));
		wTarefaSimples.findField("Titulo").setValue(wObj.getValue("titulo"));
		wTarefaSimples.findField("DescricaoSolicitacao").setValue(wObj.getValue("descricao"));
		wTarefaSimples.findField("AnexoSolicitante").setValue(wObj.getValue("anexo"));
		wTarefaSimples.findField("Prazo").setValue(wObj.getValue("prazo"));
		PersistEngine.persist(tarefaSimples);
		return tarefaSimples;
	}
	
	private NeoObject retornaEFormRRC(NeoObject obj)
	{
		/*
		 * retorna o eform da entidade Relatório de Reclamação do Cliente
		 * com os campos preenchidos pelo usuário
		 */
		InstantiableEntityInfo ieiRRC = AdapterUtils.getInstantiableEntityInfo("RRCRelatorioReclamacaoCliente");
		NeoObject RRC = ieiRRC.createNewInstance();
		EntityWrapper wRRC = new EntityWrapper(RRC);
		EntityWrapper wObj = new EntityWrapper(obj);
		wRRC.findField("clienteSapiens").setValue(wObj.getValue("cliente"));
		wRRC.findField("lotacao").setValue(wObj.getValue("lotacao"));
		wRRC.findField("cidade").setValue(wObj.getValue("cidade"));
		wRRC.findField("email").setValue(wObj.getValue("email"));
		wRRC.findField("telefone").setValue(wObj.getValue("telefone"));
		wRRC.findField("pessoaContato").setValue(wObj.getValue("contato"));
		wRRC.findField("textoReclamacao").setValue(wObj.getValue("motivo"));
		wRRC.findField("dataAbertura").setValue(wObj.getValue("dataAbertura"));
		wRRC.findField("emitente").setValue(wObj.getValue("emitente"));
		PersistEngine.persist(RRC);
		return RRC;
	}
	
	public void back(EntityWrapper wrapper, Activity act)
	{
		
	}

}
