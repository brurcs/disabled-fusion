package com.neomind.fusion.custom.orsegups.helpdeskti.adapters;

import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;



/*
 * @author william.bastos
 * 
 * registra o histórico ao fim de cada atividade
 */
public class GIRegistraHistoricoIteracao implements AdapterInterface{

	private static final Log log = LogFactory.getLog(GIRegistraHistoricoIteracao.class);
	
	@Override
	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		try{
		InstantiableEntityInfo registroAtividade = AdapterUtils.getInstantiableEntityInfo("GIRegistroAtividade");
		NeoObject registro = registroAtividade.createNewInstance();
		EntityWrapper wRegistro = new EntityWrapper(registro);
		String responsavelStr = "";
		String mensagem = "";
			//valores padrão, existentes em toda tarefa de usuário
			if(NeoUtils.safeIsNotNull(origin)){
				wRegistro.findField("usuarioResponsavel").setValue(origin.getUser());
				wRegistro.findField("dataInicial").setValue(origin.getStartDate());
				wRegistro.findField("dataFinal").setValue(origin.getFinishDate());
				System.out.println("Prazo: " + NeoUtils.safeDateFormat(origin.getFinishDate(), "dd/MM/yyyy HH:mm:ss"));
			}
			else{
				
				NeoPaper papel = new NeoPaper();
				papel = OrsegupsUtils.getPaper("sistemaFusionPaper");
				NeoUser usuarioResponsavel = new NeoUser();
				if(papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
				{
					for (NeoUser user : papel.getUsers() )
					{
						usuarioResponsavel = user;
						break;
					}
				}
				wRegistro.findField("usuarioResponsavel").setValue(usuarioResponsavel);
				wRegistro.findField("dataInicial").setValue(new GregorianCalendar());
				wRegistro.findField("dataFinal").setValue(new GregorianCalendar());
			}
			
			
			/*
			 * aqui é feito tratamento indivual para cada tarefa do fluxo
			 */
			//específico da tarefa 'Atender Chamado'
			if(NeoUtils.safeIsNotNull(origin) && origin.getActivityName().equals("Atender Chamado"))
			{
				wRegistro.findField("atividadeGerou").setValue("Atendeu chamado");
				wRegistro.findField("tipoAtividade").setValue(wrapper.findValue("tipoAtividade") != null ? wrapper.findValue("tipoAtividade") : "");
				wRegistro.findField("tempoGasto").setValue(wrapper.findValue("tempoGasto") != null ? wrapper.findValue("tempoGasto") : "");
				wRegistro.findField("descricaoAtividade").setValue(wrapper.findValue("descricaoAtendimento"));
				wrapper.setValue("descricaoAtendimento", "");
				/////////////////////////// Cadastrar serviço Solução ///////////////////////////////////////
				InstantiableEntityInfo solucao = AdapterUtils.getInstantiableEntityInfo("GISolucao"); 
				NeoObject noPS = solucao.createNewInstance();
				EntityWrapper psWrapper = new EntityWrapper(noPS);
				psWrapper.findField("descricaoSolucao").setValue(wrapper.findValue("obsSolucao"));
				PersistEngine.persist(noPS);
				InstantiableEntityInfo servicoSolucao = AdapterUtils.getInstantiableEntityInfo("GIServicoSolucao"); 
				NeoObject noPSS = servicoSolucao.createNewInstance();
				EntityWrapper pssWrapper = new EntityWrapper(noPSS);
				if(wrapper.findValue("classificacaoIncidenteIII") != null && psWrapper.findValue("descricaoSolucao") != null){
				pssWrapper.findField("GICategoriaChamadoNivelTres").setValue(wrapper.findValue("classificacaoIncidenteIII"));
				pssWrapper.findField("GISolucao").setValue(psWrapper.getObject());
				PersistEngine.persist(noPSS);
				
				}
//				else{
//					throw new WorkflowException("Atenção!. Para a existência de uma solução é preciso haver um Serviço de T.I. vinculado.");
//				}
//				
			}
			if(NeoUtils.safeIsNotNull(origin) && origin.getActivityName().equals("Realizar Triagem"))
			{	
				origin.getFinishDate().after(new GregorianCalendar());
				wRegistro.findField("atividadeGerou").setValue("Realizou Triagem");
				wRegistro.findField("descricaoAtividade").setValue(wrapper.findValue("obsTriagem"));
				wrapper.setValue("obsTriagem", "");	
			}
			
			if(NeoUtils.safeIsNotNull(origin) && origin.getActivityName().equals("Realizar Triagem Coordenação"))
			{	
				wRegistro.findField("atividadeGerou").setValue("Realizou Triagem Coordenação");
				wRegistro.findField("descricaoAtividade").setValue(wrapper.findValue("obsTriagem"));
				wrapper.setValue("obsTriagem", "");	
			
			}
			
			if(NeoUtils.safeIsNotNull(origin) && origin.getActivityName().equals("Realizar Triagem Diretoria"))
			{	
				wRegistro.findField("atividadeGerou").setValue("Realizou Triagem Diretoria");
				wRegistro.findField("descricaoAtividade").setValue(wrapper.findValue("obsTriagem"));
				wrapper.setValue("obsTriagem", "");	
			}
			
			if(NeoUtils.safeIsNotNull(origin) && origin.getActivityName().equals("Realizar Triagem Presidência"))
			{	
				wRegistro.findField("atividadeGerou").setValue("Realizou Triagem Presidência");
				wRegistro.findField("descricaoAtividade").setValue(wrapper.findValue("descricaoIncidente"));
				wrapper.setValue("descricaoIncidente", "");	
			}
			
			if(NeoUtils.safeIsNotNull(origin) && origin.getActivityName().equals("Aprovar Chamado TI"))
			{
				wRegistro.findField("atividadeGerou").setValue("Aprovou chamado TI");
				wRegistro.findField("descricaoAtividade").setValue(wrapper.findValue("obsAprovacaoTI"));
				wrapper.setValue("obsAprovacaoTI", "");
			}
			if(NeoUtils.safeIsNotNull(origin) && origin.getActivityName().equals("Avaliar Solicitação de Serviço"))
			{
				wRegistro.findField("atividadeGerou").setValue("Avaliou solicitação de serviço");
				wRegistro.findField("descricaoAtividade").setValue(wrapper.findValue("obsAvaliarSolicitacao"));
				wrapper.setValue("obsAvaliarSolicitacao", "");
			}
			if(NeoUtils.safeIsNotNull(origin) && origin.getActivityName().equals("Abrir Chamado"))
			{
				wRegistro.findField("atividadeGerou").setValue("Abriu chamado");
				wRegistro.findField("descricaoAtividade").setValue(wrapper.findValue("descricaoIncidente"));
				//wRegistro.findField("tipoAtividade").setValue(wrapper.findField("descricaoIncidente").getName());
			}
			if(NeoUtils.safeIsNotNull(origin) && origin.getActivityName().equals("Aprovar Chamado"))
			{
				wRegistro.findField("atividadeGerou").setValue("Aprovou chamado");
				wRegistro.findField("descricaoAtividade").setValue(wrapper.findValue("obsAprovacaoChamado"));
				wrapper.setValue("obsAprovacaoChamado", "");
			}
			if(NeoUtils.safeIsNotNull(origin) && origin.getActivityName().equals("Complementar Chamado"))
			{
				wRegistro.findField("atividadeGerou").setValue("Complementou chamado triagem");
				wRegistro.findField("descricaoAtividade").setValue(wrapper.findValue("descricaoIncidente"));
				wrapper.setValue("obsTriagem", "");
				wrapper.setValue("complementarChamado", false);
				wrapper.setValue("servicoProjeto", false);
			}
			if(NeoUtils.safeIsNotNull(origin) && origin.getActivityName().equals("Complementar Chamado 2"))
			{
				wRegistro.findField("atividadeGerou").setValue("Complementou chamando atendimento");
				wRegistro.findField("descricaoAtividade").setValue(wrapper.findValue("descricaoIncidente"));
				wrapper.setValue("obsTriagem", "");
				wrapper.setValue("complementarChamado", false);
				wrapper.setValue("servicoProjeto", false);
			}
			if(NeoUtils.safeIsNotNull(origin) && origin.getActivityName().equals("Aprovar Chamado Superior"))
			{
				wRegistro.findField("atividadeGerou").setValue("Aprovou chamado nível superior");
				wRegistro.findField("descricaoAtividade").setValue(wrapper.findValue("obsAprovacaoTI"));
				wrapper.setValue("obsAprovacaoTI", "");
			}
		
			if(NeoUtils.safeIsNull(origin)){
				responsavelStr = String.valueOf(wrapper.findField("papelNivelSuperior.code").getValue());
				
				if(responsavelStr.equals("GI")){
					mensagem = "Realizou Triagem POOL Chamados TI";
				} else	if(responsavelStr.equals("GICoordenadorTI")){	
					mensagem = "Realizou Triagem POOL Chamados Coordenadores TI";
				}else if(responsavelStr.equals("GIDiretoriaTI")){	
					mensagem = "Realizou Triagem POOL Chamados Diretoria TI";
				}else if(responsavelStr.equals("GIPresidencia")){	
					mensagem = "Realizou Triagem POOL Chamados Presidência TI";
				}
		

			wRegistro.findField("atividadeGerou").setValue(mensagem);
			if(NeoUtils.safeIsNull(origin))
				mensagem = "Escalou quando não - " + mensagem;
			wRegistro.findField("descricaoAtividade").setValue(mensagem);
			wrapper.setValue("obsTriagem", "");	
		
			}
		//Verificar o método 
		PersistEngine.persist(registro);
		wrapper.findField("historicoAtividade").addValue(registro);
		}
		catch (Exception e)
		{
			log.error(" Fluxo TI - Por favor, contatar o administrador do sistema! "+e.getMessage());
			throw new WorkflowException(" Fluxo TI - Por favor, contatar o administrador do sistema! "+e.getMessage());
		}
	}
	
	@Override
	public void back(EntityWrapper wrapper, Activity activity)
	{
	}
}
