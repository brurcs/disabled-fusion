package com.neomind.fusion.custom.orsegups.konviva.vo;

public class ColaboradorKonvivaVO
{
	Long cdCentroCusto;
	Long cdRegional;
	Long cdMatricula;
	String dsCPF;
	String dsCentroCusto;
	String dsFilial;
	String dsSupervisor;	
	String dsRegional;
	String nomeFuncionario;
	String dsPosto;
	
	public Long getCdCentroCusto()
	{
		return cdCentroCusto;
	}

	public void setCdCentroCusto(Long cdCentroCusto)
	{
		this.cdCentroCusto = cdCentroCusto;
	}

	public Long getCdRegional()
	{
		return cdRegional;
	}

	public void setCdRegional(Long cdRegional)
	{
		this.cdRegional = cdRegional;
	}

	public Long getCdMatricula()
	{
		return cdMatricula;
	}

	public void setCdMatricula(Long cdMatricula)
	{
		this.cdMatricula = cdMatricula;
	}

	public String getDsCPF()
	{
		return dsCPF;
	}

	public void setDsCPF(String dsCPF)
	{
		this.dsCPF = dsCPF;
	}

	public String getDsCentroCusto()
	{
		return dsCentroCusto;
	}

	public void setDsCentroCusto(String dsCentroCusto)
	{
		this.dsCentroCusto = dsCentroCusto;
	}

	public String getDsFilial()
	{
		return dsFilial;
	}

	public void setDsFilial(String dsFilial)
	{
		this.dsFilial = dsFilial;
	}

	public String getDsSupervisor()
	{
		return dsSupervisor;
	}

	public void setDsSupervisor(String dsSupervisor)
	{
		this.dsSupervisor = dsSupervisor;
	}

	public String getDsRegional()
	{
		return dsRegional;
	}

	public void setDsRegional(String dsRegional)
	{
		this.dsRegional = dsRegional;
	}
	
	public String getNomeFuncionario() 
	{
	    return nomeFuncionario;
	}
	public void setNomeFuncionario(String nomeFuncionario) 
	{
	    this.nomeFuncionario = nomeFuncionario;
	}
	
	public String getDsPosto()
	{
		return dsPosto;
	}

	public void setDsPosto(String dsPosto)
	{
		this.dsPosto = dsPosto;
	}

	@Override
	public String toString()
	{
		return "ColaboradorKonvivaVO [cpf=" + dsCPF + ", matricula=" + cdMatricula + ", filial=" + dsFilial + ", centro de custo=" + cdCentroCusto + ", descricao centro de custo=" + dsCentroCusto + ", lider imediato=" + dsSupervisor + ", código regional=" + cdRegional + ", nome regional=" + dsRegional + "]";
	}
}
