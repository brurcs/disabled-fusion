package com.neomind.fusion.custom.orsegups.maps.call.engine;

import java.util.GregorianCalendar;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.maps.vo.ViaturaVO;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.portal.PortalUtil;


public class CallAlertEventSnep 
{
	
	public void alertEvent(ViaturaVO viaturaVO, EventoVO eventoVO)
	{
		Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();
		
		//clearCache();
		//OrsegupsAlertEventEngine.getInstance().alert(eventoVO);
		receiveCall(timeExec, eventoVO.getTelefone());
	}
	
	
	private void receiveCall(Long timeExec ,String telefone)
	{
		NeoObject user = PortalUtil.getCurrentUser(); 
		EntityWrapper ewUser = new EntityWrapper(user);
        String extensionNumber = (String)ewUser.findField("ramal").getValue();
		Long timeExecFim = GregorianCalendar.getInstance().getTimeInMillis() - timeExec;
		timeExecFim = timeExecFim / 1000;
		//OrsegupsAlertEventEngine.getInstance().r(extensionNumber, telefone , CallAlertAitStatus.ATENDIDA, timeExecFim);
	}
	
	private void clearCache()
	{
		OrsegupsAlertEventEngine.getInstance().clearCache();
	}
	
	
	

}
