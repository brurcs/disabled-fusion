package com.neomind.fusion.custom.orsegups.adapter;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class FCIAvaliarPool implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		//verifica a forma de pagamento
		String desfpg = (String) processEntity.findValue("desfpg");

		//referencia ao e-form de papeis
		InstantiableEntityInfo classPapel = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("NeoPaper");

		//determina em qual pool o campo papelRaiaContatoFCI assumira de acordo com a ativida atual
		if (activity.getActivityName().equalsIgnoreCase("Avaliar Pool 1 Ligacao"))
		{ /* 1 LIGAÇÂO */
			if (desfpg.equalsIgnoreCase("Débito em Conta"))
			{
				NeoObject obj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "FCI Primeiro Contato Cobrança Debito em Conta"));
				processEntity.findField("papelRaiaContatoFCI").setValue((NeoPaper) obj);
			}
			else
			{
				NeoObject obj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "FCI Primeiro Contato Cobrança"));
				processEntity.findField("papelRaiaContatoFCI").setValue((NeoPaper) obj);
			}
		}
		else if (activity.getActivityName().equalsIgnoreCase("Avaliar Pool 2 Ligacao"))
		{/* 2 LIGAÇÂO */
			if (desfpg.equalsIgnoreCase("Débito em Conta"))
			{
				NeoObject obj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "FCI Segundo Contato Cobrança Debito em Conta"));
				processEntity.findField("papelRaiaContatoFCI").setValue((NeoPaper) obj);
			}
			else
			{
				NeoObject obj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "FCI Segundo Contato Cobrança"));
				processEntity.findField("papelRaiaContatoFCI").setValue((NeoPaper) obj);
			}
		}
		else if (activity.getActivityName().equalsIgnoreCase("Avaliar Pool 3 Ligacao"))
		{/* 3 LIGAÇÂO */
			if (desfpg.equalsIgnoreCase("Débito em Conta"))
			{

				NeoObject obj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","FCI Terceiro Contato Cobrança Debito em Conta"));
				processEntity.findField("papelRaiaContatoFCI").setValue((NeoPaper) obj);
			}
			else
			{
				NeoObject obj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "FCI Terceiro Contato Cobrança"));
				processEntity.findField("papelRaiaContatoFCI").setValue((NeoPaper) obj);
			}
		}
		else
		{/* 4 LIGAÇÂO */
			if (desfpg.equalsIgnoreCase("Débito em Conta"))
			{
				NeoObject obj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","FCI Quarto Contato Cobrança Debito em Conta"));
				processEntity.findField("papelRaiaContatoFCI").setValue((NeoPaper) obj);
			}
			else
			{
				NeoObject obj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "FCI Quarto Contato Cobrança"));
				processEntity.findField("papelRaiaContatoFCI").setValue((NeoPaper) obj);
			}
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
