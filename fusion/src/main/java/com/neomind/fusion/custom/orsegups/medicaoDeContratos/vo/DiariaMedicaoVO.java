package com.neomind.fusion.custom.orsegups.medicaoDeContratos.vo;

import java.math.BigDecimal;

public class DiariaMedicaoVO
{
	public BigDecimal qtdDiarias;
	public String obsDiaria;
	public String linkEditar;
	public String linkExcluir;
	public long neoId;

	public String getLinkEditar()
	{
		return linkEditar;
	}

	public void setLinkEditar(String linkEditar)
	{
		this.linkEditar = linkEditar;
	}

	public String getLinkExcluir()
	{
		return linkExcluir;
	}

	public void setLinkExcluir(String linkExcluir)
	{
		this.linkExcluir = linkExcluir;
	}

	public BigDecimal getQtdDiarias()
	{
		return qtdDiarias;
	}

	public void setQtdDiarias(BigDecimal qtdDiarias)
	{
		this.qtdDiarias = qtdDiarias;
	}

	public String getObsDiaria()
	{
		return obsDiaria;
	}

	public void setObsDiaria(String obsDiaria)
	{
		this.obsDiaria = obsDiaria;
	}

	public long getNeoId()
	{
		return neoId;
	}

	public void setNeoId(long neoId)
	{
		this.neoId = neoId;
	}

}
