package com.neomind.fusion.custom.orsegups.mobile.vo2;

import java.util.ArrayList;

public class PesquisaVO {
	
	private String neoId;
	private String txtPesquisa;
	private ArrayList<AssuntoVO> assuntos;
	
	public String getTxtPesquisa() {
		return txtPesquisa;
	}
	public void setTxtPesquisa(String txtPesquisa) {
		this.txtPesquisa = txtPesquisa;
	}
	public ArrayList<AssuntoVO> getAssuntos() {
		return assuntos;
	}
	public void setAssuntos(ArrayList<AssuntoVO> assuntos) {
		this.assuntos = assuntos;
	}
	public String getNeoId() {
		return neoId;
	}
	public void setNeoId(String neoId) {
		this.neoId = neoId;
	}
	

}
