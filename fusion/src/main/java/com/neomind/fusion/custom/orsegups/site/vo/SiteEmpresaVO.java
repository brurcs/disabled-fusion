package com.neomind.fusion.custom.orsegups.site.vo;

public class SiteEmpresaVO
{
	private Long codigoEmpresa;
	private Long codigoFilial;
	private String nomeEmpresa;
	
	public Long getCodigoEmpresa()
	{
		return codigoEmpresa;
	}
	public void setCodigoEmpresa(Long codigoEmpresa)
	{
		this.codigoEmpresa = codigoEmpresa;
	}
	public String getNomeEmpresa()
	{
		return nomeEmpresa;
	}
	public void setNomeEmpresa(String nomeEmpresa)
	{
		this.nomeEmpresa = nomeEmpresa;
	}
	public Long getCodigoFilial()
	{
		return codigoFilial;
	}
	public void setCodigoFilial(Long codigoFilial)
	{
		this.codigoFilial = codigoFilial;
	}
	

	
	
}
