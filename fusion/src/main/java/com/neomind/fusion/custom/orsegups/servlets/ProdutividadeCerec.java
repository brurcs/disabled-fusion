package com.neomind.fusion.custom.orsegups.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.ejb.EntityManagerImpl;

import com.neomind.fusion.persist.PersistEngine;

@WebServlet(name="ProdutividadeCerec", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.servlets.ProdutividadeCerec"})
public class ProdutividadeCerec extends HttpServlet
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doWork(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doWork(req, resp);
	}

	private void doWork(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		String parametro = req.getParameter("sql");
		PrintWriter pw = resp.getWriter();
	
	
		//FIXME NEOMIND método connection() não existe mais

//		/* ################################################### */
//		try
//		{
//			
//			
//			EntityManagerImpl impl = (EntityManagerImpl) PersistEngine.getEntityManager();
//			parametro = parametro.toUpperCase();
//			Statement stmt = impl.getSession().connection().createStatement();
//			stmt.execute(parametro);
//
//			ResultSet rs = stmt.getResultSet();
//			pw.println("<table>");
//			while(rs.next()){
//				pw.println("<tr>");
//				//1st col
//				pw.println("<td>");
//				pw.println(rs.getString(1));
//				pw.println("</td>");
//
//
//				pw.println("</tr>");
//			}
//			pw.println("</table>");
//		}
//		catch (SQLException e)
//		{
//				pw.println(e.getMessage());
//		}finally{
//			
//			pw.close();
//		}
	}

}
