package com.neomind.fusion.custom.orsegups.servlets;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Cell;
import jxl.CellType;
import jxl.DateCell;
import jxl.Sheet;
import jxl.Workbook;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

@WebServlet(name="ArmamentoServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.servlets.ArmamentoServlet"})
public class ArmamentoServlet extends HttpServlet
{
	/*http://localhost:8080/fusion/servlet/com.neomind.fusion.custom.orsegups.servlets.ArmamentoServlet?action=importarArmas*/
		
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		this.doRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		this.doRequest(request, response);
	}

	private void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("INÍCIO DA IMPORTAÇÃO DE ARMAS PARA O FUSION");
		String action = "";

		PrintWriter aMsg = response.getWriter();
		
		if (request.getParameter("action") != null)
		{
			action = request.getParameter("action");
		}

		if (action.equalsIgnoreCase("importarArmas"))
		{
			this.importarArmas(aMsg);
		}

		System.out.println("FIM DA IMPORTAÇÃO DE ARMAS PARA O FUSION");
	}

	private void importarArmas(PrintWriter aMSg)
	{
		Date data = null;
		String mensagem = "";
		try
		{
			List<String> lstNumeroArma = new ArrayList<String>();			
			Workbook wbArmas = Workbook.getWorkbook(new File("C:\\Armamento6.xls"));
			Sheet shArmas = wbArmas.getSheet(0);

			int liTroca = shArmas.getRows();
			for (int i = 0; i < liTroca; i++)
			{
				Cell numeroArma = shArmas.getCell(4, i);
				
				if (i != 0 && lstNumeroArma != null && !lstNumeroArma.contains(numeroArma.getContents()))
				{
					lstNumeroArma.add(numeroArma.getContents());
					
					Cell marca = shArmas.getCell(0, i);
					Cell especie = shArmas.getCell(1, i);
					Cell modelo = shArmas.getCell(2, i);
					Cell calibre = shArmas.getCell(3, i);
					Cell validade = shArmas.getCell(5, i);
					if (validade.getType() == CellType.DATE) 
					{ 
					  DateCell dateCell = (DateCell) validade; 
					  data = dateCell.getDate(); 
					} 					
					Cell sinarm = shArmas.getCell(6, i);
					Cell capacidadeTiros = shArmas.getCell(7, i);
					Cell numeroRegistro = shArmas.getCell(8, i);
					Cell empresa = shArmas.getCell(9, i);
					Cell regionalResponsavel = shArmas.getCell(10, i);
					Cell situacao = shArmas.getCell(11, i);
					
					NeoObject oArmamento = AdapterUtils.createNewEntityInstance("ARMArmamento");
					EntityWrapper wrpArmamento = new EntityWrapper(oArmamento);
								
					QLEqualsFilter equalsFilterMarca = new QLEqualsFilter("descricaoMarca", marca.getContents());
					List<NeoObject> lstMarca = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("ARMMarca"), equalsFilterMarca);
					if (lstMarca != null && lstMarca.size() > 0)
					{
						lstMarca.get(0);
					}
					else
					{
						mensagem = "Não encontrado a Marca da Arma para inserção no Fusion!";
					}
					
					QLEqualsFilter equalsFilterEspecie = new QLEqualsFilter("descricaoEspecie", especie.getContents());
					List<NeoObject> lstEspecie = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("ARMEspecie"), equalsFilterEspecie);
					if (lstEspecie != null && lstEspecie.size() > 0)
					{
						lstEspecie.get(0);
					}
					else
					{
						mensagem = "Não encontrado a Espécie da Arma para inserção no Fusion!";
					}
					
					QLEqualsFilter equalsFilterEmpresa = new QLEqualsFilter("codemp", Long.parseLong(empresa.getContents()));
					List<NeoObject> lstEmpresa = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("EEMP"), equalsFilterEmpresa);
					if (lstEmpresa != null && lstEmpresa.size() > 0)
					{
						lstEmpresa.get(0);
					}
					else
					{
						mensagem = "Não encontrado a Empresa da Arma para inserção no Fusion!";
					}
					
					QLEqualsFilter equalsFilterRegional = new QLEqualsFilter("usu_codreg", Long.parseLong(regionalResponsavel.getContents()));
					List<NeoObject> lstRegional = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUREG"), equalsFilterRegional);
					if (lstRegional != null && lstRegional.size() > 0)
					{
						lstRegional.get(0);
					}
					else
					{
						mensagem = "Não encontrado a Regional da Arma para inserção no Fusion!";
					}
					
					QLEqualsFilter equalsFilterSituacao = new QLEqualsFilter("descricaoSituacao", situacao.getContents());
					List<NeoObject> lstSituacao = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("ARMSituacao"), equalsFilterSituacao);
					if (lstSituacao != null && lstSituacao.size() > 0)
					{
						lstSituacao.get(0);
					}
					else
					{
						mensagem = "Não encontrado a Situação da Arma para inserção no Fusion!";
					}

					GregorianCalendar gcValidade = new GregorianCalendar();
					gcValidade.setTime(data);
					
					wrpArmamento.setValue("marca", lstMarca.get(0));
					wrpArmamento.setValue("especie", lstEspecie.get(0));
					wrpArmamento.setValue("modelo", modelo.getContents());
					wrpArmamento.setValue("calibre", calibre.getContents());
					wrpArmamento.setValue("numeroArma", numeroArma.getContents());
					wrpArmamento.setValue("numeroRegistro", numeroRegistro.getContents());
					wrpArmamento.setValue("dataValidade", gcValidade);
					wrpArmamento.setValue("numeroSinarm", sinarm.getContents());
					wrpArmamento.setValue("capacidadeTiro", Long.parseLong(capacidadeTiros.getContents()));
					wrpArmamento.setValue("empresa", lstEmpresa.get(0));
					wrpArmamento.setValue("regionalResponsavel", lstRegional.get(0));
					wrpArmamento.setValue("situacao", lstSituacao.get(0));
					
					PersistEngine.persist(oArmamento);
				}
				else if (i != 0)
				{
					mensagem = "Número da arma " + numeroArma.getContents() + " já inserido no sistema Fusion! Por favor, verifique.";
					System.out.println(mensagem);
				}
			}
		}
		catch (Exception e)
		{
			System.out.println(mensagem);
			e.printStackTrace();
		}
		
		finally
		{
			aMSg.println(mensagem);
		}
	}
}

