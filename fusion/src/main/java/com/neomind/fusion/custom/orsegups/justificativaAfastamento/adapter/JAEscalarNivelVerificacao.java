package com.neomind.fusion.custom.orsegups.justificativaAfastamento.adapter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoGroup;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class JAEscalarNivelVerificacao implements AdapterInterface {

    private static final Log log = LogFactory.getLog(JAEscalarNivelVerificacao.class);

    @Override
    public void start(Task origin, EntityWrapper processEntity, Activity activity) {
	origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
	String erro = "Erro ao obter responsável da tarefa!";
	try {

	    NeoUser responsavel = null;
	    
	    boolean isEscalada = (boolean) processEntity.findField("isEscalada").getValue();
	    
	    NeoUser usuario = null;
	    
	    if (isEscalada){
		usuario = (NeoUser) processEntity.findField("executorEscalado").getValue();
	    }else{
		 usuario = (NeoUser) processEntity.findField("executorVerificar").getValue();
	    }

	   
	    NeoGroup grupo = usuario.getGroup();

	    NeoPaper papelResponsavelGrupo = grupo.getResponsible();

	    NeoUser usuarioResponsavelGrupo = null;

	    if (papelResponsavelGrupo != null && papelResponsavelGrupo.getAllUsers() != null && !papelResponsavelGrupo.getAllUsers().isEmpty()) {
		for (NeoUser user : papelResponsavelGrupo.getUsers()) {
		    usuarioResponsavelGrupo = user;
		    break;
		}
	    }

	    if (usuarioResponsavelGrupo.getGroup().getCode().equals("Presidência")) {
		responsavel = usuarioResponsavelGrupo;
	    } else {
		if (usuario.equals(usuarioResponsavelGrupo)) {
		    NeoGroup upperLevel = usuario.getGroup().getUpperLevel();

		    if (upperLevel != null) {
			NeoPaper upperLevelResp = upperLevel.getResponsible();

			if (upperLevelResp != null && upperLevelResp.getAllUsers() != null && !upperLevelResp.getAllUsers().isEmpty()) {
			    for (NeoUser user : upperLevelResp.getUsers()) {
				responsavel = user;
				break;
			    }
			}

		    } else {
			responsavel = usuarioResponsavelGrupo;
		    }
		} else {
		    responsavel = usuarioResponsavelGrupo;
		}
	    }

	    processEntity.findField("executorEscalado").setValue(responsavel);
	    processEntity.setValue("acao", null);
	    processEntity.setValue("acaoExecutorSubstituto", null);

	    // GregorianCalendar prazo = (GregorianCalendar)
	    // processEntity.findField("prazo").getValue();
	    // prazo.add(Calendar.DAY_OF_MONTH, +2);
	    //
	    // prazo.set(Calendar.HOUR_OF_DAY, 23);
	    // prazo.set(Calendar.MINUTE, 59);
	    // prazo.set(Calendar.SECOND, 59);
	    //
	    // while (!OrsegupsUtils.isWorkDay(prazo)) {
	    // prazo.add(Calendar.DAY_OF_MONTH, +1);
	    // }
	    //
	    // processEntity.findField("prazo").setValue(prazo);

	} catch (Exception e) {
	    log.error(erro);
	    System.out.println(erro);
	    e.printStackTrace();
	    throw new WorkflowException(erro);
	}
    }

    @Override
    public void back(EntityWrapper processEntity, Activity activity) {
	
    }

}
