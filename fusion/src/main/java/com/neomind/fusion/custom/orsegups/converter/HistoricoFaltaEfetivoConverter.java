package com.neomind.fusion.custom.orsegups.converter;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.workflow.ProcessState;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class HistoricoFaltaEfetivoConverter extends StringConverter
{

	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin)
	{
		StringBuilder textoTable = new StringBuilder();
		try
		{
			Long idPai = field.getForm().getObjectId();
			NeoObject tarefa = PersistEngine.getNeoObject(idPai);
			EntityWrapper wrapper = new EntityWrapper(tarefa);
			
			String codigo = (String) wrapper.findValue("wfprocess.code");
			List<NeoObject> tarefaEfetivo = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TarefaFaltaEfetivo"), new QLEqualsFilter("tarefa", codigo));
			
			if (tarefaEfetivo != null && !tarefaEfetivo.isEmpty())
			{
				EntityWrapper wrapperEfetivo = new EntityWrapper(tarefaEfetivo.get(0));
				Long numloc = (Long) wrapperEfetivo.findValue("numloc");
				Long taborg = (Long) wrapperEfetivo.findValue("taborg");
				
				GregorianCalendar dataExecucao = new GregorianCalendar();
				dataExecucao = OrsegupsUtils.getSpecificWorkDay(dataExecucao, -12L);
				
				QLEqualsFilter numlocFilter = new QLEqualsFilter("numloc", numloc);
				QLEqualsFilter taborgFilter = new QLEqualsFilter("taborg", taborg);
				QLOpFilter dataFilter = new QLOpFilter("dataExecucao", ">=", (GregorianCalendar) dataExecucao);
				QLOpFilter tarFilter = new QLOpFilter("tarefa", "<>", codigo);
				QLEqualsFilter processFilter = new QLEqualsFilter("processo", "G004 - Acompanhamento de Falta de Efetivo");
				
				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				groupFilter.addFilter(numlocFilter);
				groupFilter.addFilter(taborgFilter);
				groupFilter.addFilter(dataFilter);
				groupFilter.addFilter(tarFilter);
				groupFilter.addFilter(processFilter);
				
				List<NeoObject> faltasEfetivo = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TarefaFaltaEfetivo"), groupFilter, -1, -1, "dataExecucao");
				
				if (faltasEfetivo != null && !faltasEfetivo.isEmpty())
				{
					textoTable.append("	<table  class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">");
					textoTable.append("		<tr style=\"cursor: auto\">");
					textoTable.append("			<th style=\"cursor: auto\">Processo</th>");
					textoTable.append("			<th style=\"cursor: auto\">Tarefa</th>");
					textoTable.append("			<th style=\"cursor: auto\">Status</th>");
					textoTable.append("			<th style=\"cursor: auto\">Executor</th>");
					textoTable.append("			<th style=\"cursor: auto; white-space: normal\">Resposta</th>");
					textoTable.append("			<th style=\"cursor: auto\">Data Abertura</th>");
					textoTable.append("			<th style=\"cursor: auto\">Data Execução</th>");
					textoTable.append("		</tr>");
					textoTable.append("		<tbody>");	
					
					for (NeoObject obj : faltasEfetivo)
					{
						EntityWrapper wrapperObj = new EntityWrapper(obj);
						String tar = (String) wrapperObj.findValue("tarefa");
						String proc = (String) wrapperObj.findValue("processo");
						
						
						NeoObject tarEfetivo = PersistEngine.getObject(AdapterUtils.getEntityClass("AFEAcompanhamentoFaltaEfetivo"), new QLEqualsFilter("wfprocess.code", tar));
						EntityWrapper tarWrapper = new EntityWrapper(tarEfetivo);
						
						String user = (String) tarWrapper.findValue("executor.fullName");
						String descricao = (String) tarWrapper.findValue("resposta");
						ProcessState state = (ProcessState) tarWrapper.findValue("wfprocess.processState");
						String estado = "";
						
						switch (state.ordinal())
						{
							case 0:
								estado = "Em andamento";
								break;

							case 1:
								estado = "Finalizada";
								break;

							case 2:
								estado = "Cancelada";
								break;
						}
						
						if (descricao == null || descricao.equals("null"))
						{
							descricao = "";
						}
						
						GregorianCalendar dataInicio = (GregorianCalendar) tarWrapper.findValue("wfprocess.startDate");
						GregorianCalendar dataFinal = (GregorianCalendar) tarWrapper.findValue("wfprocess.finishDate");
						String datini = "";
						String datfim = "";
						
						if (dataInicio != null)
						{
							datini = NeoUtils.safeDateFormat(dataInicio);
						}
						
						if (dataFinal != null)
						{
							datfim = NeoUtils.safeDateFormat(dataFinal);
						}
						
						textoTable.append("		<tr>");
						textoTable.append("			<td>" + proc + "</td>");
						textoTable.append("			<td>" + tar + "</td>");
						textoTable.append("			<td>" + estado + "</td>");
						textoTable.append("			<td>" + user + "</td>");
						textoTable.append("			<td style=\"white-space: normal\">" + descricao + "</td>");
						textoTable.append("			<td>" + datini + "</td>");
						textoTable.append("			<td>" + datfim + "</td>");
						
					}
					textoTable.append("			</tr>");
					textoTable.append("		</tbody>");
					textoTable.append("	</table>");
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return textoTable.toString();
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return "";
	}
}