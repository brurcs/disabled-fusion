package com.neomind.fusion.custom.orsegups.contract;

import java.util.ArrayList;
import java.util.Collection;

public class ListaAnexosMinutaDataSource
{
	private String dataAss = "";
	private String nomeCliente = "";
	private String responsavelLocal = "";
	private String enderecoInstalacao = "";
	private String referenciaInstalacao = "";
	private String numeroInstalacao = "";
	private String cpfInstalacao = "";
	private String bairroInstalacao = "";
	private String telefoneInstalacao1 = "";
	private String telefoneInstalacao2 = "";
	private String telefoneInstalacao3 = "";
	private String estadoCidadeInstalacao = "";
	private String nomeResponsavel1 = "";
	private String nomeResponsavel2 = "";
	private String nomeResponsavel3 = "";
	private String cargoResponsavel1 = "";
	private String cargoResponsavel2 = "";
	private String cargoResponsavel3 = "";
	private String telefone1Responsavel1 = "";
	private String telefone1Responsavel2 = "";
	private String telefone1Responsavel3 = "";
	private String telefone2Responsavel1 = "";
	private String telefone2Responsavel2 = "";
	private String telefone2Responsavel3 = "";
	private String numeroPontos = "";
	private String valorInstalacao = "";
	private String observacao = "";
	private String contratanteCPF = "";
	private String nomeExecutivo = "";
	private String numeroPontosTxt = "";
	private String valorInstalacaoTxt = "";
	
	private String dir_croqui = "";
	private String anexoIIAplica = "";
	private String anexoIINaoAplica = "";
	private String anexoIIIAplica = "";
	private String anexoIIINaoAplica = "";
	
	private String printAnexo2;
	private String printAnexo3;
	
	private String dir_listaEquipamentos;
	private Collection<ListaEquipamentosMinutaDataSource> listaEquipamentos1 = new ArrayList<ListaEquipamentosMinutaDataSource>();
	private Collection<ListaEquipamentosMinutaDataSource> listaEquipamentos2 = new ArrayList<ListaEquipamentosMinutaDataSource>();;
	
	
	public String getDataAss()
	{
		return dataAss;
	}
	public void setDataAss(String dataAss)
	{
		this.dataAss = dataAss;
	}
	public String getNomeCliente()
	{
		return nomeCliente;
	}
	public void setNomeCliente(String nomeCliente)
	{
		this.nomeCliente = nomeCliente;
	}
	public String getResponsavelLocal()
	{
		return responsavelLocal;
	}
	public void setResponsavelLocal(String responsavelLocal)
	{
		this.responsavelLocal = responsavelLocal;
	}
	public String getEnderecoInstalacao()
	{
		return enderecoInstalacao;
	}
	public void setEnderecoInstalacao(String enderecoInstalacao)
	{
		this.enderecoInstalacao = enderecoInstalacao;
	}
	public String getReferenciaInstalacao()
	{
		return referenciaInstalacao;
	}
	public void setReferenciaInstalacao(String referenciaInstalacao)
	{
		this.referenciaInstalacao = referenciaInstalacao;
	}
	public String getNumeroInstalacao()
	{
		return numeroInstalacao;
	}
	public void setNumeroInstalacao(String numeroInstalacao)
	{
		this.numeroInstalacao = numeroInstalacao;
	}
	public String getCpfInstalacao()
	{
		return cpfInstalacao;
	}
	public void setCpfInstalacao(String cpfInstalacao)
	{
		this.cpfInstalacao = cpfInstalacao;
	}
	public String getBairroInstalacao()
	{
		return bairroInstalacao;
	}
	public void setBairroInstalacao(String bairroInstalacao)
	{
		this.bairroInstalacao = bairroInstalacao;
	}
	public String getTelefoneInstalacao1()
	{
		return telefoneInstalacao1;
	}
	public void setTelefoneInstalacao1(String telefoneInstalacao1)
	{
		this.telefoneInstalacao1 = telefoneInstalacao1;
	}
	public String getTelefoneInstalacao2()
	{
		return telefoneInstalacao2;
	}
	public void setTelefoneInstalacao2(String telefoneInstalacao2)
	{
		this.telefoneInstalacao2 = telefoneInstalacao2;
	}
	public String getTelefoneInstalacao3()
	{
		return telefoneInstalacao3;
	}
	public void setTelefoneInstalacao3(String telefoneInstalacao3)
	{
		this.telefoneInstalacao3 = telefoneInstalacao3;
	}
	public String getEstadoCidadeInstalacao()
	{
		return estadoCidadeInstalacao;
	}
	public void setEstadoCidadeInstalacao(String estadoCidadeInstalacao)
	{
		this.estadoCidadeInstalacao = estadoCidadeInstalacao;
	}
	public String getNomeResponsavel1()
	{
		return nomeResponsavel1;
	}
	public void setNomeResponsavel1(String nomeResponsavel1)
	{
		this.nomeResponsavel1 = nomeResponsavel1;
	}
	public String getNomeResponsavel2()
	{
		return nomeResponsavel2;
	}
	public void setNomeResponsavel2(String nomeResponsavel2)
	{
		this.nomeResponsavel2 = nomeResponsavel2;
	}
	public String getNomeResponsavel3()
	{
		return nomeResponsavel3;
	}
	public void setNomeResponsavel3(String nomeResponsavel3)
	{
		this.nomeResponsavel3 = nomeResponsavel3;
	}
	public String getCargoResponsavel1()
	{
		return cargoResponsavel1;
	}
	public void setCargoResponsavel1(String cargoResponsavel1)
	{
		this.cargoResponsavel1 = cargoResponsavel1;
	}
	public String getCargoResponsavel2()
	{
		return cargoResponsavel2;
	}
	public void setCargoResponsavel2(String cargoResponsavel2)
	{
		this.cargoResponsavel2 = cargoResponsavel2;
	}
	public String getCargoResponsavel3()
	{
		return cargoResponsavel3;
	}
	public void setCargoResponsavel3(String cargoResponsavel3)
	{
		this.cargoResponsavel3 = cargoResponsavel3;
	}
	public String getTelefone1Responsavel1()
	{
		return telefone1Responsavel1;
	}
	public void setTelefone1Responsavel1(String telefone1Responsavel1)
	{
		this.telefone1Responsavel1 = telefone1Responsavel1;
	}
	public String getTelefone1Responsavel2()
	{
		return telefone1Responsavel2;
	}
	public void setTelefone1Responsavel2(String telefone1Responsavel2)
	{
		this.telefone1Responsavel2 = telefone1Responsavel2;
	}
	public String getTelefone1Responsavel3()
	{
		return telefone1Responsavel3;
	}
	public void setTelefone1Responsavel3(String telefone1Responsavel3)
	{
		this.telefone1Responsavel3 = telefone1Responsavel3;
	}
	public String getTelefone2Responsavel1()
	{
		return telefone2Responsavel1;
	}
	public void setTelefone2Responsavel1(String telefone2Responsavel1)
	{
		this.telefone2Responsavel1 = telefone2Responsavel1;
	}
	public String getTelefone2Responsavel2()
	{
		return telefone2Responsavel2;
	}
	public void setTelefone2Responsavel2(String telefone2Responsavel2)
	{
		this.telefone2Responsavel2 = telefone2Responsavel2;
	}
	public String getTelefone2Responsavel3()
	{
		return telefone2Responsavel3;
	}
	public void setTelefone2Responsavel3(String telefone2Responsavel3)
	{
		this.telefone2Responsavel3 = telefone2Responsavel3;
	}
	public String getNumeroPontos()
	{
		return numeroPontos;
	}
	public void setNumeroPontos(String numeroPontos)
	{
		this.numeroPontos = numeroPontos;
	}
	public String getValorInstalacao()
	{
		return valorInstalacao;
	}
	public void setValorInstalacao(String valorInstalacao)
	{
		this.valorInstalacao = valorInstalacao;
	}
	public String getObservacao()
	{
		return observacao;
	}
	public void setObservacao(String observacao)
	{
		this.observacao = observacao;
	}
	public String getContratanteCPF()
	{
		return contratanteCPF;
	}
	public void setContratanteCPF(String contratanteCPF)
	{
		this.contratanteCPF = contratanteCPF;
	}
	public String getNumeroPontosTxt()
	{
		return numeroPontosTxt;
	}
	public void setNumeroPontosTxt(String numeroPontosTxt)
	{
		this.numeroPontosTxt = numeroPontosTxt;
	}
	public String getValorInstalacaoTxt()
	{
		return valorInstalacaoTxt;
	}
	public void setValorInstalacaoTxt(String valorInstalacaoTxt)
	{
		this.valorInstalacaoTxt = valorInstalacaoTxt;
	}
	public String getNomeExecutivo()
	{
		return nomeExecutivo;
	}
	public void setNomeExecutivo(String nomeExecutivo)
	{
		this.nomeExecutivo = nomeExecutivo;
	}
	public String getDir_croqui()
	{
		return dir_croqui;
	}
	public void setDir_croqui(String dir_croqui)
	{
		this.dir_croqui = dir_croqui;
	}
	public String getAnexoIIAplica()
	{
		return anexoIIAplica;
	}
	public void setAnexoIIAplica(String anexoIIAplica)
	{
		this.anexoIIAplica = anexoIIAplica;
	}
	public String getAnexoIINaoAplica()
	{
		return anexoIINaoAplica;
	}
	public void setAnexoIINaoAplica(String anexoIINaoAplica)
	{
		this.anexoIINaoAplica = anexoIINaoAplica;
	}
	public String getAnexoIIIAplica()
	{
		return anexoIIIAplica;
	}
	public void setAnexoIIIAplica(String anexoIIIAplica)
	{
		this.anexoIIIAplica = anexoIIIAplica;
	}
	public String getAnexoIIINaoAplica()
	{
		return anexoIIINaoAplica;
	}
	public void setAnexoIIINaoAplica(String anexoIIINaoAplica)
	{
		this.anexoIIINaoAplica = anexoIIINaoAplica;
	}
	public String getDir_listaEquipamentos()
	{
		return dir_listaEquipamentos;
	}
	public void setDir_listaEquipamentos(String dir_listaEquipamentos)
	{
		this.dir_listaEquipamentos = dir_listaEquipamentos;
	}
	public Collection<ListaEquipamentosMinutaDataSource> getListaEquipamentos1()
	{
		return listaEquipamentos1;
	}
	public void setListaEquipamentos1(Collection<ListaEquipamentosMinutaDataSource> listaEquipamento1)
	{
		this.listaEquipamentos1 = listaEquipamento1;
	}
	public Collection<ListaEquipamentosMinutaDataSource> getListaEquipamentos2()
	{
		return listaEquipamentos2;
	}
	public void setListaEquipamentos2(Collection<ListaEquipamentosMinutaDataSource> listaEquipamento2)
	{
		this.listaEquipamentos2 = listaEquipamento2;
	}
	public String getPrintAnexo2()
	{
		return printAnexo2;
	}
	public void setPrintAnexo2(String printAnexo2)
	{
		this.printAnexo2 = printAnexo2;
	}
	public String getPrintAnexo3()
	{
		return printAnexo3;
	}
	public void setPrintAnexo3(String printAnexo3)
	{
		this.printAnexo3 = printAnexo3;
	}
}
