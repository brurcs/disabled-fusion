package com.neomind.fusion.custom.orsegups.presenca.vo;


public class HistoricoEscalaPostoVO {
	
	private String usuNumctr;
	private String usuNumpos;
	private String usuDiasem;
	private Long usuHorini;
	private Long usuHorfim;
	private String usuDatalt;
	
	public String getUsuNumctr() {
		return usuNumctr;
	}
	public void setUsuNumctr(String usuNumctr) {
		this.usuNumctr = usuNumctr;
	}
	public String getUsuNumpos() {
		return usuNumpos;
	}
	public void setUsuNumpos(String usuNumpos) {
		this.usuNumpos = usuNumpos;
	}
	public String getUsuDiasem() {
		return usuDiasem;
	}
	public void setUsuDiasem(String usuDiasem) {
		this.usuDiasem = usuDiasem;
	}
	public Long getUsuHorini() {
		return usuHorini;
	}
	public void setUsuHorini(Long usuHorini) {
		this.usuHorini = usuHorini;
	}
	public Long getUsuHorfim() {
		return usuHorfim;
	}
	public void setUsuHorfim(Long usuHorfim) {
		this.usuHorfim = usuHorfim;
	}
	public String getUsuDatalt() {
		return usuDatalt;
	}
	public void setUsuDatalt(String usuDatalt) {
		this.usuDatalt = usuDatalt;
	}
	
	
	@Override
	public String toString()
	{

		String horarioTexto = "";
		String separador = "";

		horarioTexto += separador + getHorario(usuHorini) + "-" + getHorario(usuHorfim);
		separador = " / ";
		
		
		if (horarioTexto.isEmpty())
		{
			horarioTexto = "Sem Escala";
		}
		return horarioTexto;
	}
	
	private static String getHorario(Long horario)
	{
		String horarioString = "";
		
		int hora = horario.intValue() / 60;
		int minuto = horario.intValue() % 60;
		
		String horaString = String.format ("%02d",hora);
		String minutoString = String.format ("%02d",minuto);
		
		horarioString = horaString + ":" + minutoString;
		return horarioString;
	}
	
	
	
	

}
