package com.neomind.fusion.custom.orsegups.mobile.engine;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.mobile.utils.MobileUtils;
import com.neomind.fusion.custom.orsegups.mobile.vo.Inspecao;
import com.neomind.fusion.custom.orsegups.mobile.vo.Resposta;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Task;
import com.neomind.util.NeoUtils;

public class SendReinspection
{

	public List<Inspecao> buildReinspection(HttpServletRequest request, HttpServletResponse response)
	{
		List<Inspecao> lista = null;
		try
		{
			boolean have = false;
			String usuarioMobile = ""; // usuário logado no aplicativo
			NeoUser userM = null;
			
			String siglaRegional = null;
			
			if (  NeoUtils.safeIsNotNull(request.getParameter("user")) ){
				usuarioMobile =  request.getParameter("user");
				userM = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", usuarioMobile ));
			}
			
			if ( NeoUtils.safeIsNotNull(request.getParameter("siglaRegional")) ){
				siglaRegional = request.getParameter("siglaRegional");
			}

			String token = request.getParameter("token");

			NeoUser user = MobileUtils.getUserFromToken(token);

			//QLRawFilter qlRawFilter = new QLRawFilter("sendToMobile = 1 AND activity.process.processState = 0 and task.user.neoId="+user.getNeoId()); // filtra pelo inspetor
			QLRawFilter qlRawFilter = new QLRawFilter("sendToMobile = 1 AND activity.process.processState = 0  and activity.process.requester.neoId = " + userM.getNeoId());

			List<CasvigMobilePool> mobilePool = (List<CasvigMobilePool>) PersistEngine.getObjects(
					CasvigMobilePool.class, qlRawFilter);

			List<CasvigMobilePool> listPool = new ArrayList<CasvigMobilePool>();

			String arrayTasks = NeoUtils.safeOutputString(request.getParameter("tasksIds"));

			for (CasvigMobilePool pool : mobilePool)
			{
				Task task = pool.getTask();

				if (pool.getSendToMobile() && !haveNeoId(arrayTasks, task.getNeoId()))
				{
					listPool.add(pool);

					if (!have)
						have = true;
				}
			}

			if (have)
			{
				lista = buildReinspection(listPool,siglaRegional);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return lista;

	}

	/**
	 * Lista todas regionais se baseando no filtro pela sigla da regional.
	 * 
	 * @param listPool - pool onde estão todas tarefas passiveis de irem para o celular.
	 * @param filtroRegional Sigla da Regional constante no eform IMRegionais. Se for passado null será considerado SEMREG e não filtrará nada.
	 * @return
	 */
	public List<Inspecao> buildReinspection(List<CasvigMobilePool> listPool, String filtroRegional)
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		String siglaRegional = "SEMREG";
		if (filtroRegional != null && !"".equals(filtroRegional)){
			siglaRegional = filtroRegional;
		}
		
		System.out.println("[REINSPECAO]["+key+"]Filtrando Reinspeções para enviar ao dispositivo. Sigla da Regional Filtrada="+siglaRegional);
		
		List<Inspecao> lista = new ArrayList<Inspecao>();

		for (CasvigMobilePool pool : listPool)
		{
			NeoObject entity = pool.getActivity().getProcess().getEntity();
			try
			{
				Inspecao reinspecao = new Inspecao();
				reinspecao.setIsReinspecao(true);

				EntityWrapper wrapper = new EntityWrapper(entity);

				NeoObject objInspetoria = (NeoObject) wrapper.getValue("relatorioInspecao");
				EntityWrapper wrapperResult = new EntityWrapper(objInspetoria);

				Object cod_postoTrabalho = wrapperResult.getValue("codposto");
				Object codCcu = wrapperResult.getValue("codCcu");
				Object nome_empresa = wrapperResult.getValue("nomeempresa");
				Object nome_cliente = wrapperResult.getValue("nomecliente");
				Object nome_lotacao = wrapperResult.getValue("nomelotacao");
				Object nome_postoTrabalho = wrapperResult.getValue("nomeposto");
				Object gestor_cliente = wrapperResult.getValue("gestorcliente");
				Object nome_colaborador = wrapperResult.getValue("colaboradorentrevistado");
				Object funcao_colaborador = wrapperResult.getValue("funccolaborador");
				Object nome_inspetor = wrapperResult.getValue("nomeinspetor");
				Object sigla_regional = wrapperResult.getValue("siglaRegional");

				GregorianCalendar data = (GregorianCalendar) wrapperResult.getValue("datainspecao");
				int mes = data.get(Calendar.MONTH);
				int dia = data.get(Calendar.DAY_OF_MONTH);
				int ano = data.get(Calendar.YEAR);
				Object data_inpesao = dia + "-" + mes + "-" + ano;
				
				
				GregorianCalendar dataRespostaInconsistencia = (GregorianCalendar) wrapper.getValue("dataRespostaInconsistencia");
				int mesResp = dataRespostaInconsistencia.get(Calendar.MONTH);
				int diaResp = dataRespostaInconsistencia.get(Calendar.DAY_OF_MONTH);
				int anoResp = dataRespostaInconsistencia.get(Calendar.YEAR);
				Object objDatarespostaInconsistencia = diaResp + "-" + mesResp + "-" + anoResp;
				
				Object PrazoInc = null;
				if (NeoUtils.safeIsNotNull(wrapper.getValue("prazoAcaoInconsistencia"))){
					GregorianCalendar dataPrazoInc = (GregorianCalendar) wrapper.getValue("prazoAcaoInconsistencia");
					int mesPrazo = dataPrazoInc.get(Calendar.MONTH);
					int diaPrazo = dataPrazoInc.get(Calendar.DAY_OF_MONTH);
					int anoPrazo = dataPrazoInc.get(Calendar.YEAR);
					PrazoInc = diaPrazo + "-" + mesPrazo + "-" + anoPrazo;
				}
				
				
				
				Object descrisao_pesquisa = wrapperResult.getValue("descpesquisa");

				reinspecao.setTaskId("" + pool.getTask().getNeoId());
				//reinspecao.setTaskId("" + pool.getTask().getProcess().getCode()); // codigo da tarefa
				reinspecao.setEmpresa(((NeoUtils.safeIsNull(nome_empresa)) ? "" : nome_empresa.toString()));
				reinspecao.setCliente(((NeoUtils.safeIsNull(nome_cliente)) ? "" : nome_cliente.toString()));
				reinspecao.setLotacao(((NeoUtils.safeIsNull(nome_lotacao)) ? "" : nome_lotacao.toString()));
				reinspecao.setPosto(((NeoUtils.safeIsNull(nome_postoTrabalho)) ? "" : nome_postoTrabalho.toString()));
				reinspecao.setPosto(((NeoUtils.safeIsNull(cod_postoTrabalho)) ? "" : cod_postoTrabalho.toString()));
				reinspecao.setCodCcu(((NeoUtils.safeIsNull(codCcu)) ? "" : codCcu.toString()));
				reinspecao.setGestor(((NeoUtils.safeIsNull(gestor_cliente)) ? "" : gestor_cliente.toString()));
				reinspecao.setColaborador(((NeoUtils.safeIsNull(nome_colaborador)) ? "": nome_colaborador.toString()));
				reinspecao.setFuncaoColaborador(((NeoUtils.safeIsNull(funcao_colaborador)) ? "" : funcao_colaborador.toString()));
				reinspecao.setDataInspecao((NeoUtils.safeIsNull(data_inpesao)) ? "" : data_inpesao.toString());
				reinspecao.setDatarespostaInconsistencia((NeoUtils.safeIsNull(objDatarespostaInconsistencia)) ? "" : objDatarespostaInconsistencia.toString());
				reinspecao.setPrazoAcaoInconsistencia((NeoUtils.safeIsNull(PrazoInc)) ? "" : PrazoInc.toString());
				reinspecao.setSiglaRegional(NeoUtils.safeIsNull(sigla_regional)? "" : sigla_regional.toString());
				
				

				StringBuilder observacao = new StringBuilder("");

				if (pool.getTypeInspection().equals("Reinspeção")
						|| pool.getTypeInspection().equals("Reinspecao"))
				{
					observacao.append("Resposta à Inconsistência : ");
					observacao
							.append((NeoUtils.safeIsNull(wrapper.getValue("respostaInconsistencia"))) ? ""
									: wrapper.getValue("respostaInconsistencia").toString());
				}
				else
				{
					observacao.append("Resposta à Não-conformidade : ");
					observacao
							.append((NeoUtils.safeIsNull(wrapper.getValue("respostaNaoConformidade"))) ? ""
									: wrapper.getValue("respostaNaoConformidade").toString());
				}

				reinspecao.setObs(observacao.toString());

				//xmlResultado.setAttribute("descrisao_pesquisa",
				//(NeoUtils.safeIsNull(descrisao_pesquisa)) ? "" : descrisao_pesquisa.toString());

				List<Resposta> respostas = montaRespostas(pool, wrapper, wrapperResult);
				reinspecao.setRespostas((ArrayList<Resposta>) respostas);
				
				if (reinspecao.getSiglaRegional().equals(siglaRegional) || reinspecao.getSiglaRegional().equals("") || "SEMREG".equals(siglaRegional) || "".equals(siglaRegional) ){ // adiciona as inspeções com a sigla de filtro e todas sem regional definida
					System.out.println("[REINSPECAO]["+key+"] adicionada reinspeção para enviar ->" + reinspecao);
					lista.add(reinspecao);
				}else{
					System.out.println("[REINSPECAO]["+key+"] descartado envio da reinspeção  ->" + reinspecao);
				}
				
				

			}
			catch (Exception npe)
			{
				npe.printStackTrace();
			}
		}

		return lista;

	}

	private ArrayList<Resposta> montaRespostas(CasvigMobilePool pool, EntityWrapper wrapper,
			EntityWrapper wrapperResult)
	{
		ArrayList<Resposta> respostas = new ArrayList<Resposta>();

		List<NeoObject> listObjResp = ((List<NeoObject>) wrapperResult.findValue("neoidResposta"));

		for (NeoObject respQuery : listObjResp)
		{
			Resposta respostax = new Resposta();
			EntityWrapper wrapperResp = new EntityWrapper(respQuery);

			String obsResp = (String) wrapperResp.getValue("respperg");

			String obs = "";
			if (obsResp != null)
			{
				obs = obsResp.substring(obsResp.indexOf("- (OBS) -") + 9, obsResp.length());

			}

			Object desc_perg = wrapperResp.getValue("descperg");

			Object code_perg = wrapperResp.getValue("codeperg");
			Boolean logical = (Boolean) wrapperResp.getValue("booleanValor");
			
			Integer assuntoId =  NeoUtils.safeInteger( NeoUtils.safeOutputString(wrapperResp.getValue("assuntoId")) );
			Integer pesquisaId =  NeoUtils.safeInteger( NeoUtils.safeOutputString( wrapperResp.getValue("pesquisaId")) );
			Integer perguntaId = NeoUtils.safeInteger( NeoUtils.safeOutputString( wrapperResp.getValue("perguntaId")) );
			

			respostax.setPergunta(((NeoUtils.safeIsNull(desc_perg)) ? "" : desc_perg.toString()));
			respostax.setObservacao(((NeoUtils.safeIsNull(obs)) ? "" : obs.toString()));
			
			respostax.setAssunto_id(assuntoId);
			respostax.setPesquisa_id(pesquisaId);
			respostax.setPergunta_id(perguntaId);

			respostas.add(respostax);

		}

		return respostas;
	}

	public static boolean haveNeoId(String tasks, Long neoId)
	{
		String[] split = tasks.split(";");
		String neoidStr = neoId.toString();
		for (String string : split)
		{
			if (string.equalsIgnoreCase(neoidStr))
			{
				return true;
			}
		}
		return false;
	}

}
