package com.neomind.fusion.custom.orsegups.presenca.vo;

public class ContaSigmaVO
{
	private Long cdCliente;
	private String razao;
	private String fantasia;
	private String centralParticao;
	private String central;
	private String particao;
	private String empresa;
	private String linkEventos;
	private String cgccpf;
	private String email;
	
	public String getCgccpf() {
	    return cgccpf;
	}
	public void setCgccpf(String cgccpf) {
	    this.cgccpf = cgccpf;
	}
	public Long getCdCliente()
	{
		return cdCliente;
	}
	public void setCdCliente(Long cdCliente)
	{
		this.cdCliente = cdCliente;
	}
	public String getRazao()
	{
		return razao;
	}
	public void setRazao(String razao)
	{
		this.razao = razao;
	}
	public String getFantasia()
	{
		return fantasia;
	}
	public void setFantasia(String fantasia)
	{
		this.fantasia = fantasia;
	}
	public String getCentralParticao()
	{
		return centralParticao;
	}
	public void setCentralParticao(String centralParticao)
	{
		this.centralParticao = centralParticao;
	}
	public String getCentral()
	{
		return central;
	}
	public void setCentral(String central)
	{
		this.central = central;
	}
	public String getParticao()
	{
		return particao;
	}
	public void setParticao(String particao)
	{
		this.particao = particao;
	}
	public String getEmpresa()
	{
		return empresa;
	}
	public void setEmpresa(String empresa)
	{
		this.empresa = empresa;
	}
	public String getLinkEventos()
	{
		return linkEventos;
	}
	public void setLinkEventos(String linkEventos)
	{
		this.linkEventos = linkEventos;
	}
	public String getEmail() {
	    return email;
	}
	public void setEmail(String email) {
	    this.email = email;
	}
}
