package com.neomind.fusion.custom.orsegups.scheduler;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoUtils;

public class AbreTarefaSimplesDemitirVigilantes implements CustomJobAdapter
{
	@SuppressWarnings("unused")
	private static final Log log = LogFactory.getLog(AbreTarefaSimplesDemitirVigilantes.class);

	@Override
	public void execute(CustomJobContext arg0)
	{
		
		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.AbreTarefaSimplesAgendamentoReciclagens");
		log.warn("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Simples Agendamento Reciclagens - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		try
		{
			for (int i = 0; i < 5; i++)
			{
				String solicitante = "juliano.clemente";
				String executor = "lauro.ramos";
				String titulo = "Demitir vigilante";
				String descricao = " Identificar um vigilante de sua regional para ser demitido.";
				
				GregorianCalendar dataPrazo = new GregorianCalendar();
				dataPrazo.add(Calendar.DAY_OF_MONTH, +5);
				
				dataPrazo = OrsegupsUtils.getNextWorkDay(dataPrazo);
				dataPrazo.set(Calendar.HOUR_OF_DAY, 23);
				dataPrazo.set(Calendar.MINUTE, 59);
				dataPrazo.set(Calendar.SECOND, 59);
				
				IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
				String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", dataPrazo);
				
				log.warn("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Agendamento Reciclagens - Tarefa aberta: "+ tarefa);
				
				Thread.sleep(1000);
			}
		}
		catch (Exception e)
		{

			log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Agendamento Reciclagens");
			e.printStackTrace();
			System.out.println("dd");
		}
		finally
		{
			log.warn("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Simples Agendamento Reciclagens - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}
}