package com.neomind.fusion.custom.orsegups.ti;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;

@WebServlet(name = "ClonarPapelServlet", urlPatterns = { "/servlet/com.neomind.fusion.custom.orsegups.ti.ClonarPapelServlet" })
public class ClonarPapelServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	private static Integer idDestino = 0;
	private static Integer idOrigem = 0;

	public ClonarPapelServlet()
	{
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		String action = request.getParameter("action");

		if (action.equalsIgnoreCase("clonarPapel"))
		{
			PrintWriter out = response.getWriter();
			String destino = request.getParameter("cmb_usuario_destino");
			String origem = request.getParameter("cmb_usuario_origem");

			String retorno = clonarPapeis(origem, destino);
			
			out.print(retorno);
			System.out.println(retorno);
			out.flush();
			out.close();
			
			//request.setAttribute("resposta", retorno);
		}

		if (action.equalsIgnoreCase("getListaUsuarios"))
		{
			ArrayList<String> listaUsuarios = this.getListaUsuarios();
			String json = new Gson().toJson(listaUsuarios);

			response.setContentType("application/json");
			PrintWriter out = response.getWriter();
			out.print(json);
			out.flush();

		}

	}

	private ArrayList<String> getListaUsuarios()
	{

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		ArrayList<String> usuarios = new ArrayList<String>();

		try
		{

			conn = PersistEngine.getConnection("");

			String sql = "SELECT S.name FROM NeoUser U WITH(NOLOCK) " + " INNER JOIN SecurityEntity S WITH(NOLOCK)ON S.neoId = U.neoId " + " WHERE  S.active=1";

			pstm = conn.prepareStatement(sql);

			rs = pstm.executeQuery();

			while (rs.next())
			{
				usuarios.add(rs.getString("name"));
			}

		}
		catch (Exception e)
		{

		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

		return usuarios;
	}

	private String clonarPapeis(String origem, String destino)
	{

		Integer usuarioOrigem = usuarioOrigem(origem);
		Integer usurioDestino = usuarioDestino(destino);

		if ((usuarioOrigem != 0) && (usurioDestino != 0))
		{
			Connection conn = null;

			StringBuilder sqlInsert = new StringBuilder();

			PreparedStatement pstm = null;
			ResultSet rs = null;
			conn = PersistEngine.getConnection("");

			sqlInsert.append("INSERT INTO NeoPaper_users (papers_neoId, users_neoId) ");
			sqlInsert.append("(SELECT DISTINCT PU.papers_neoId, " + usurioDestino + " FROM NeoPaper_users PU WITH(NOLOCK) ");
			sqlInsert.append("WHERE PU.users_neoId = " + usuarioOrigem + " ");
			sqlInsert.append("AND PU.papers_neoId NOT IN ");
			sqlInsert.append("(SELECT papers_neoId FROM NeoPaper_users WITH (NOLOCK) WHERE users_neoId = " + usurioDestino + ")) ");

			try
			{
				pstm = conn.prepareStatement(sqlInsert.toString());
				pstm.executeUpdate();
				int teste = pstm.getFetchSize();
				System.out.println(teste);

			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			finally
			{

				OrsegupsUtils.closeConnection(conn, pstm, rs);

			}
			return "sucesso";
		}
		else
		{
			return "erro";
		}
	}

	private Integer usuarioOrigem(String origem)
	{

		Connection conn = null;

		StringBuilder sql = new StringBuilder();

		PreparedStatement pstm = null;
		ResultSet rs = null;
		conn = PersistEngine.getConnection("");

		sql.append("SELECT NEOID,* FROM NEOUSER WHERE FULLNAME = '" + origem + "'");

		try
		{
			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();

			if (rs.next())
			{
				idOrigem = rs.getInt("NEOID");

			}
			else{
				
				idOrigem = 0;
			}

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{

			OrsegupsUtils.closeConnection(conn, pstm, rs);

		}

		return idOrigem;

	}

	private Integer usuarioDestino(String destino)
	{

		Connection conn = null;

		StringBuilder sql = new StringBuilder();

		PreparedStatement pstm = null;
		ResultSet rs = null;
		conn = PersistEngine.getConnection("");

		sql.append("SELECT NEOID,* FROM NEOUSER WHERE FULLNAME = '" + destino + "'");

		try
		{
			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();

			if (rs.next())
			{
				idDestino = rs.getInt("NEOID");

			}
			else{
				idDestino = 0;
			}

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{

			OrsegupsUtils.closeConnection(conn, pstm, rs);

		}

		return idDestino;

	}

}
