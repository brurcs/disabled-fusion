package com.neomind.fusion.custom.orsegups.juridico.relatorio.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import com.neomind.fusion.custom.orsegups.juridico.relatorio.util.SistemaUtil;

public class Conexao {
	
	private static Logger logger = Logger.getLogger(Conexao.class);

	private Connection con;
	//TODO mudarconexao bancoooooo
	private static String URL = "jdbc:sqlserver://cachoeira\\sql01?useSSL=false";	
	
	private static String usuario = "vetorh";	
	private static String password = "na5ep2St";
	private static String DRIVER = "com.sqlserver.jdbc.Driver";

	public Conexao() throws SQLException, InstantiationException, IllegalAccessException {
		try {

			Class.forName(DRIVER).newInstance();
			con = DriverManager.getConnection(URL, usuario, password);
			logger.debug("Conexao obtida com sucesso");
		} catch (ClassNotFoundException e) {
			throw new SQLException(e.getMessage());

		}
	}
	
	
	public ResultSet consulta(String query) throws SQLException{
		if (con != null){
			Statement stmt = con.createStatement();
			return stmt.executeQuery(query);
		}else{
			return null;
		}
	}
	
	public int update(String query) throws SQLException{
		if (con != null){
			Statement stmt = con.createStatement();
			return stmt.executeUpdate(query);
		}else{
			return 0 ;
		}
	}
	
	public void fechaConexao(){
		if (con!= null){
			try {
				con.close();
			} catch (SQLException e) {
				con = null;
			}
		}
	}
	

	public Connection getCon() {
		return con;
	}
	private static void closeConnection(Connection conn){
		try{
			if(SistemaUtil.isNotNull(conn)){
				conn.close();
			}
		}catch(Exception e){
			logger.error("contexto:",e);
		}
	}
	
	private static void closeConnection(Connection conn, PreparedStatement pstm){
		try{
			if(SistemaUtil.isNotNull(pstm)){
				pstm.close();
			}
				closeConnection(conn);
		}catch(Exception e){
			logger.error("contexto:",e);
		}
	} 
	
	public static void closeConnection(Connection conn, PreparedStatement pstm, ResultSet rs){
		try{
			if(SistemaUtil.isNotNull(rs)){
				rs.close();
			}
			closeConnection(conn, pstm);
		}catch(Exception e){
			logger.error("contexto:",e);
		}
	}
	
}