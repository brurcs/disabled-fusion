package com.neomind.fusion.custom.orsegups.processoJuridico.adapter;

import java.util.Comparator;
import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;

public class ListaAnexoComparator implements Comparator<NeoObject>{
    @Override
    public int compare(NeoObject o1, NeoObject o2) {
	EntityWrapper wO1 = new EntityWrapper(o1);
	EntityWrapper wO2 = new EntityWrapper(o2);
	
	GregorianCalendar data1 = (GregorianCalendar) wO1.getValue("dtAnexo");
	GregorianCalendar data2 = (GregorianCalendar) wO2.getValue("dtAnexo");
	
	Long time1 = data1.getTimeInMillis();
	Long time2 = data2.getTimeInMillis();
	
	if (time1 < time2){
	    return -1;
	}
	if (time1 > time2){
	    return 1;
	}
        return 0;
    }
}

