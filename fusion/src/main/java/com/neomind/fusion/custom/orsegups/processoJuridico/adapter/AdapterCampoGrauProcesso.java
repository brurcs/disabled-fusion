package com.neomind.fusion.custom.orsegups.processoJuridico.adapter;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;

public class AdapterCampoGrauProcesso extends StringConverter
{

	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin)
	{
		
		Long idPai = field.getForm().getObjectId();
		NeoObject pedido = PersistEngine.getNeoObject(idPai);
		EntityWrapper wrapper = new EntityWrapper(pedido);
		if(wrapper.findValue("sitPed") == null){
			wrapper.setValue("sitPed", "1º Grau");
			PersistEngine.persist(pedido);
		}
		return null;
		
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		
		return getHTMLInput(field, origin);
	}
}