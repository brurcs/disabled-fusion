package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.GregorianCalendar;

import javax.persistence.Query;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.datawarehouse.ExternalEntityInfo;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class FCIHumanaRecuperaTitulosAbertos implements AdapterInterface
{
	private static final FCIHumanaRecuperaTitulosAbertos lock = new FCIHumanaRecuperaTitulosAbertos();
	
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		executaIntegracao(processEntity, activity);
	}

	@SuppressWarnings("unchecked")
	public void executaIntegracao(EntityWrapper processEntity, Activity activity)
	{
		synchronized (lock)
		{
			String nomeProcesso = activity.getProcessName();
			String menssagem = "";
	
			String nomeFonteDados = "SAPIENS";
	
			String codemp = processEntity.findValue("contratoSapiens.usu_codemp").toString();
			String codfil = processEntity.findValue("contratoSapiens.usu_codfil").toString();
			String numctr = processEntity.findValue("contratoSapiens.usu_numctr").toString();
			String codcli = processEntity.findValue("contratoSapiens.codcli").toString();
	
			try
			{
	
				System.out.println("Ínicio do Adapter de Buscar Títulos");
				System.out.println("Código Cliente: " + codcli + ", Contrato: " + numctr + ", Código Flial: " + ", " + codfil + ", Código Empresa: " + codemp + ".");
				menssagem = "Tempo Limite de Espera na Conexão com Sapiens Esgotado.";
	
				String sql = "	SELECT E301TCR.CODEMP, E301TCR.CODFIL, E301TCR.CODTPT, E301TCR.NUMTIT, E301TCR.DATEMI, E301TCR.VCTORI, E301TCR.VCTPRO, E301TCR.VLRORI, E301TCR.VLRABE, E140NFV.VLRBSE, E140IDE.NUMDFS, E140ISV.NUMCTR, E140ISV.DATCPT " + "	FROM E301TCR " + "	INNER JOIN E001TNS ON E001TNS.CODEMP = E301TCR.CODEMP AND E001TNS.CODTNS = E301TCR.CODTNS "
						+ "	LEFT JOIN E140NFV ON E140NFV.CODEMP = E301TCR.CODEMP AND E140NFV.CODFIL = E301TCR.CODFIL AND E140NFV.CODSNF = E301TCR.CODSNF AND E140NFV.NUMNFV = E301TCR.NUMNFV " + "	LEFT JOIN E140ISV ON E140ISV.CODEMP = E140NFV.CODEMP AND E140ISV.CODFIL = E140NFV.CODFIL AND E140ISV.CODSNF = E140NFV.CODSNF AND E140ISV.NUMNFV = E140NFV.NUMNFV "
						+ "	LEFT JOIN E140IDE ON E140IDE.CODEMP = E140NFV.CODEMP AND E140IDE.CODFIL = E140NFV.CODFIL AND E140IDE.CODSNF = E140NFV.CODSNF AND E140IDE.NUMNFV = E140NFV.NUMNFV " + "	WHERE E001TNS.LISMOD = 'CRE'                                                               " + "	AND E301TCR.VLRABE > 0                                                                     " + "	AND E301TCR.VCTPRO <= (GETDATE() - 4)                                                      "
						+ "  AND E301TCR.CODTPT <> 'IFA' 															   " + "	AND ((E301TCR.SITTIT >= 'AA' AND E301TCR.SITTIT <= 'AV') OR (E301TCR.SITTIT = 'CE'))       " + "	AND E301TCR.CodCli =   " + codcli + "	AND E140ISV.CodEmp = " + codemp + " AND E140ISV.CodFil = " + codfil + " AND E140ISV.NumCtr = " + numctr
						+ "	GROUP BY E301TCR.CODEMP, E301TCR.CODFIL, E301TCR.CODTPT, E301TCR.NUMTIT, E301TCR.DATEMI, E301TCR.VCTORI, E301TCR.VCTPRO, E301TCR.VLRORI, E301TCR.VLRABE, E140NFV.VLRBSE, E140IDE.NUMDFS, E140ISV.NUMCTR, E140ISV.DATCPT " + "	ORDER BY E301TCR.DATEMI DESC ";
	
				Query query = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sql);
				@SuppressWarnings("unchecked")
				Collection<Object> resultList = query.getResultList();
	
				// Lista de Títulos
				processEntity.findField("listaTitulos").removeValues();
	
				menssagem = "Erro ao Carregar os Títulos ";
				System.out.println(menssagem);
	
				if (resultList != null)
				{
					for (Object resultSet : resultList)
					{
						if (resultSet != null)
						{
							Object[] result = (Object[]) resultSet;
							QLGroupFilter filterTituloSapiens = new QLGroupFilter("AND");
							filterTituloSapiens.addFilter(new QLEqualsFilter("codemp", (Short) result[0]));
							filterTituloSapiens.addFilter(new QLEqualsFilter("codfil", (Short) result[1]));
							filterTituloSapiens.addFilter(new QLEqualsFilter("codtpt", (String) result[2]));
							filterTituloSapiens.addFilter(new QLEqualsFilter("numtit", (String) result[3]));
							NeoObject noTitulo = AdapterUtils.createNewEntityInstance("FCITituloSapiens");
							EntityWrapper tituloFusionWrapper = new EntityWrapper(noTitulo);
							tituloFusionWrapper.setValue("empresa", new Long((Short) result[0]));
							tituloFusionWrapper.setValue("filial", new Long((Short) result[1]));
							tituloFusionWrapper.setValue("tipoTitulo", (String) result[2]);
							tituloFusionWrapper.setValue("numeroTitulo", (String) result[3]);
							Timestamp dataEmissao = (Timestamp) result[4];
							GregorianCalendar gc = new GregorianCalendar();
							gc.setTimeInMillis(dataEmissao.getTime());
							tituloFusionWrapper.setValue("emissao", gc);
	
							Timestamp dataOriginal = (Timestamp) result[5];
							gc = new GregorianCalendar();
							gc.setTimeInMillis(dataOriginal.getTime());
							tituloFusionWrapper.setValue("vencimentoOriginal", gc);
	
							Timestamp dataProrrogado = (Timestamp) result[6];
							gc = new GregorianCalendar();
							gc.setTimeInMillis(dataProrrogado.getTime());
	
							/*
							 * if((activity.getActivityName().equalsIgnoreCase("Adapter Recupera Títulos"))
							 * || (activity.getActivityName().equalsIgnoreCase("Recupera Títulos GP2"))) {
							 * GregorianCalendar dataCorte = new GregorianCalendar();
							 * dataCorte.add(Calendar.DATE,-38);
							 * if (gc.before(dataCorte)) {
							 * processEntity.findField("enviaGuedesPinto").setValue(true);
							 * }
							 * }
							 */
	
							tituloFusionWrapper.setValue("vctoProrrogado", gc);
							tituloFusionWrapper.setValue("valorOriginal", (BigDecimal) result[7]);
							tituloFusionWrapper.setValue("valorAberto", (BigDecimal) result[8]);
							tituloFusionWrapper.setValue("valorBrutoNF", (BigDecimal) result[9]);
							BigInteger sNumero = (BigInteger) result[10];
							if (sNumero != null)
							{
								tituloFusionWrapper.setValue("numeroNFSe", sNumero.toString());
							}
							Integer intCtr = (Integer) result[11];
							if (intCtr != null)
							{
								Long longCtr = new Long(intCtr);
								tituloFusionWrapper.setValue("contrato", longCtr);
							}
							Timestamp competencia = (Timestamp) result[12];
							gc = new GregorianCalendar();
							gc.setTimeInMillis(competencia.getTime());
							tituloFusionWrapper.setValue("competencia", gc);
	
							Collection<NeoObject> listaObs = (Collection<NeoObject>) PersistEngine.getObjects(((ExternalEntityInfo) EntityRegister.getInstance().getCache().getByType("SAPIENSEMOR")).getEntityClass(), filterTituloSapiens, -1, -1, " seqmov ASC");
							tituloFusionWrapper.setValue("listaObservacoes", listaObs);
							PersistEngine.persist(noTitulo);
							processEntity.findField("listaTitulos").addValue(noTitulo);
	
						}
					}
				}
	
				if (nomeProcesso.equalsIgnoreCase(OrsegupsUtils.CANCELAMENTO_CONTRATO_INICIATIVA))
				{
					if (processEntity.findField("listaTitulos").getValues().isEmpty())
					{
						processEntity.findField("existeTituloAberto").setValue(false);
					}
				}
				else
				{
					processEntity.findField("quantidadeTitulos").setValue(new Long(processEntity.findField("listaTitulos").getValues().size()));
				}
	
				menssagem = "Tempo Limite de Espera na Conexão com Sapiens Esgotado";
				System.out.println(menssagem);
	
				String sql2 = "	SELECT E301TCR.CODEMP, E301TCR.CODFIL, E301TCR.CODTPT, E301TCR.NUMTIT, E301TCR.DATEMI, E301TCR.VCTORI, E301TCR.VCTPRO, E301TCR.VLRORI, E301TCR.VLRABE, E140NFV.VLRBSE, E140IDE.NUMDFS, E140ISV.NUMCTR, E140ISV.DATCPT " + "	FROM E301TCR " + "	INNER JOIN E001TNS ON E001TNS.CODEMP = E301TCR.CODEMP AND E001TNS.CODTNS = E301TCR.CODTNS "
						+ "	LEFT JOIN E140NFV ON E140NFV.CODEMP = E301TCR.CODEMP AND E140NFV.CODFIL = E301TCR.CODFIL AND E140NFV.CODSNF = E301TCR.CODSNF AND E140NFV.NUMNFV = E301TCR.NUMNFV " + "	LEFT JOIN E140ISV ON E140ISV.CODEMP = E140NFV.CODEMP AND E140ISV.CODFIL = E140NFV.CODFIL AND E140ISV.CODSNF = E140NFV.CODSNF AND E140ISV.NUMNFV = E140NFV.NUMNFV "
						+ "	LEFT JOIN E140IDE ON E140IDE.CODEMP = E140NFV.CODEMP AND E140IDE.CODFIL = E140NFV.CODFIL AND E140IDE.CODSNF = E140NFV.CODSNF AND E140IDE.NUMNFV = E140NFV.NUMNFV " + "	WHERE E001TNS.LISMOD = 'CRE'                                                               " + "	AND E301TCR.VLRABE > 0                                                                     " + "	AND E301TCR.VCTPRO <= (GETDATE() - 4)                                                      "
						+ "  AND E301TCR.CODTPT <> 'IFA' 															   " + "	AND ((E301TCR.SITTIT >= 'AA' AND E301TCR.SITTIT <= 'AV') OR (E301TCR.SITTIT = 'CE'))       " + "	AND E301TCR.CodCli =   " + codcli + "	AND NOT (E140ISV.CodEmp = " + codemp + " AND E140ISV.CodFil = " + codfil + " AND E140ISV.NumCtr = " + numctr + ")"
						+ "	GROUP BY E301TCR.CODEMP, E301TCR.CODFIL, E301TCR.CODTPT, E301TCR.NUMTIT, E301TCR.DATEMI, E301TCR.VCTORI, E301TCR.VCTPRO, E301TCR.VLRORI, E301TCR.VLRABE, E140NFV.VLRBSE, E140IDE.NUMDFS, E140ISV.NUMCTR, E140ISV.DATCPT  " + "	ORDER BY E301TCR.DATEMI DESC ";
	
				Query query2 = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sql2);
				@SuppressWarnings("unchecked")
				Collection<Object> resultList2 = query2.getResultList();
	
				// Lista de Títulos em aberto de outros contratos
				processEntity.findField("listaDemaisTitulos").removeValues();
	
				if (resultList2 != null)
				{
					for (Object resultSet : resultList2)
					{
						if (resultSet != null)
						{
							Object[] result = (Object[]) resultSet;
							QLGroupFilter filterTituloSapiens = new QLGroupFilter("AND");
							filterTituloSapiens.addFilter(new QLEqualsFilter("codemp", (Short) result[0]));
							filterTituloSapiens.addFilter(new QLEqualsFilter("codfil", (Short) result[1]));
							filterTituloSapiens.addFilter(new QLEqualsFilter("codtpt", (String) result[2]));
							filterTituloSapiens.addFilter(new QLEqualsFilter("numtit", (String) result[3]));
							NeoObject noTitulo = AdapterUtils.createNewEntityInstance("FCITituloSapiens");
							EntityWrapper tituloFusionWrapper = new EntityWrapper(noTitulo);
							tituloFusionWrapper.setValue("empresa", new Long((Short) result[0]));
							tituloFusionWrapper.setValue("filial", new Long((Short) result[1]));
							tituloFusionWrapper.setValue("tipoTitulo", (String) result[2]);
							tituloFusionWrapper.setValue("numeroTitulo", (String) result[3]);
							Timestamp dataEmissao = (Timestamp) result[4];
							GregorianCalendar gc = new GregorianCalendar();
							gc.setTimeInMillis(dataEmissao.getTime());
							tituloFusionWrapper.setValue("emissao", gc);
							Timestamp dataOriginal = (Timestamp) result[5];
							gc = new GregorianCalendar();
							gc.setTimeInMillis(dataOriginal.getTime());
							tituloFusionWrapper.setValue("vencimentoOriginal", gc);
							Timestamp dataProrrogado = (Timestamp) result[6];
							gc = new GregorianCalendar();
							gc.setTimeInMillis(dataProrrogado.getTime());
							tituloFusionWrapper.setValue("vctoProrrogado", gc);
							tituloFusionWrapper.setValue("valorOriginal", (BigDecimal) result[7]);
							tituloFusionWrapper.setValue("valorAberto", (BigDecimal) result[8]);
							tituloFusionWrapper.setValue("valorBrutoNF", (BigDecimal) result[9]);
							BigInteger sNumero = (BigInteger) result[10];
							if (sNumero != null)
							{
								tituloFusionWrapper.setValue("numeroNFSe", sNumero.toString());
							}
							Integer intCtr = (Integer) result[11];
							if (intCtr != null)
							{
								Long longCtr = new Long(intCtr);
								tituloFusionWrapper.setValue("contrato", longCtr);
							}
							Timestamp competencia = (Timestamp) result[12];
							gc = new GregorianCalendar();
							gc.setTimeInMillis(competencia.getTime());
							tituloFusionWrapper.setValue("competencia", gc);
	
							/**
							 * @author orsegups lucas.avila - case sensitive.
							 * @date 08/07/2015
							 */
							Collection<NeoObject> listaObs = (Collection<NeoObject>) PersistEngine.getObjects(((ExternalEntityInfo) EntityRegister.getInstance().getCache().getByType("SAPIENSEMOR")).getEntityClass(), filterTituloSapiens, -1, -1, " seqmov ASC");
							tituloFusionWrapper.setValue("listaObservacoes", listaObs);
							PersistEngine.persist(noTitulo);
							processEntity.findField("listaDemaisTitulos").addValue(noTitulo);
						}
					}
				}
				System.out.println("Fim do Adapter de Buscar Títulos");
				}
				catch (Exception e)
				{
					e.printStackTrace();
					throw new WorkflowException(menssagem);
				}
		}
	}
}