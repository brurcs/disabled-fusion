package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;
import com.neomind.util.URLEncoder;

public class EmailPesquisaSatisfacao implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(EmailPesquisaSatisfacao.class);

	@Override
	public void execute(CustomJobContext arg0)
	{
		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.EmailPesquisaSatisfacao");
		log.warn("##### INICIAR AGENDADOR DE TAREFA: E-mail  pesquisa satisfação de clientes - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
		Connection conSigma = null;
		conSigma = PersistEngine.getConnection("SIGMA90");
		PreparedStatement stSig = null;
		ResultSet rsSig  = null;
		
		try
		{
			GregorianCalendar gDataNew = new GregorianCalendar();
		    GregorianCalendar dataCorte = new GregorianCalendar(gDataNew.get(GregorianCalendar.YEAR), gDataNew.get(GregorianCalendar.MONTH), gDataNew.get(GregorianCalendar.DAY_OF_MONTH));
					
			//GregorianCalendar dataCorte = new GregorianCalendar();
			//dataCorte.add(Calendar.MINUTE, - 10);
			//dataCorte.set(Calendar.HOUR_OF_DAY, 0);
			//dataCorte.set(Calendar.MINUTE, 0);
			//dataCorte.set(Calendar.SECOND, 0); 
			//dataCorte.set(Calendar.MILLISECOND, 0);
				
			dataCorte.set(Calendar.DAY_OF_MONTH,01);
			dataCorte.set(Calendar.MONTH,9);
			dataCorte.set(Calendar.YEAR,2013);
			
			StringBuffer sqlSig = new StringBuffer();
			sqlSig.append("SELECT ef.enviadoPara, ef.idOrdem, ef.responsavel ");
			sqlSig.append(" FROM  dbORDEM os ");
			sqlSig.append(" INNER JOIN  [CACUPE\\SQL02].fusion_producao.DBO.d_OSEmailAutomaticoExibicaoCliente ef ");
			sqlSig.append(" ON  os.ID_ORDEM = ef.idOrdem AND os.ID_INSTALADOR = ef.cdTecnico ");
			sqlSig.append("  INNER JOIN dbCENTRAL ce  ON  ce.CD_CLIENTE = os.CD_CLIENTE  ");
			sqlSig.append("  INNER JOIN EMPRESA em ON  em.CD_EMPRESA = ce.ID_EMPRESA  ");
			//sqlSig.append(" where ef.dataEnvio >= ? and ");
			sqlSig.append(" where CAST(FLOOR( CAST( os.fechamento AS float) ) AS smalldatetime) >= ? and ");
			sqlSig.append(" os.FECHADO = 1 and ");
			sqlSig.append(" os.opfechou <> 11010 and ");
			sqlSig.append(" em.CD_EMPRESA not in (10071,10072,10073,10074,10075,10076,10077,10078,10079,10080,10081,10082,10083,10084, ");
			sqlSig.append(" 10085,10086,10087,10088,10089,10090,10091,10092,10093,10094,10095,10096,10097,10098,10099,10100,10101,10102, ");
			sqlSig.append(" 10103,10104,10105,10106,10107,10108,10109,10110,10111,10112,10113,10114,10115,10116,10117 )");
			sqlSig.append(" and not exists (select ps.idOrdem FROM [CACUPE\\SQL02].fusion_producao.DBO.d_EmailPesquisaSatisfacao ps ");
			sqlSig.append(" where ps.idOrdem = os.id_ordem)");
			//sqlSig.append(" ORDER BY dataEnvio");
			sqlSig.append(" ORDER BY os.fechamento");
			stSig = conSigma.prepareStatement(sqlSig.toString());
			stSig.setString(1, NeoUtils.safeDateFormat(dataCorte, "yyyy-MM-dd"));
//			stSig.setTimestamp(1, new Timestamp(dataCorte.getTimeInMillis()));
			 rsSig = stSig.executeQuery();
			 
			while (rsSig.next())
			{

				String enviadoPara = null; 
				enviadoPara = rsSig.getString("enviadoPara");
				//remove colchete do inicio do email 
				enviadoPara = enviadoPara.replace("[", ""); 
				//remove colchete do final do email 
				enviadoPara = enviadoPara.replace("]", ""); 
				
				Long idOrdem = null;
				idOrdem = rsSig.getLong("idOrdem");
				
				String responsavel = "Cliente";
				String link ="";
				String[] emailCliente = enviadoPara.split(Pattern.quote(","));  
				
		     
		        //emailCliente
		        for(String email : emailCliente){    
		          
				log.warn("##### EXECUTAR AGENDADOR DE TAREFA: E-mail satisfação de clientes - Ordem: "+idOrdem+" - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
				link = "http://spacemail.com.br/spacemail/campanha_pontual/_R8L1Q88888K?nome="+responsavel+"&ordem_de_servico_="+idOrdem+"&_destinatarios="+email;
				 //substitu o espaço em branco por % 
				link = link.replace(" ", "");
		        URL url = new URL(link.trim());
		        URLConnection conn = null;
		       
		        conn = url.openConnection();
				InputStream is = null;
				is = conn.getInputStream();
				is.close();
				InstantiableEntityInfo emailPS = AdapterUtils.getInstantiableEntityInfo("EmailPesquisaSatisfacao"); 
				NeoObject noPS = emailPS.createNewInstance();
				
				EntityWrapper psWrapper = new EntityWrapper(noPS);
				
				psWrapper.findField("responsavel").setValue(responsavel);
				psWrapper.findField("idOrdem").setValue(idOrdem);
				psWrapper.findField("destinatarios").setValue(email);
				PersistEngine.persist(noPS);
				
				
			}
			}
			
		}
		catch (SQLException e)
		{
			log.warn("##### ERRO AGENDADOR DE TAREFA: E-mail satisfação de clientes - ERRO SQLException \n"+e.getMessage()+" - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
			e.printStackTrace();
		}
		catch (MalformedURLException e)
		{
			log.warn("##### ERRO AGENDADOR DE TAREFA: E-mail satisfação de clientes - ERRO MalformedURLException \n"+e.getMessage()+" - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
			e.printStackTrace();
		}
		catch (IOException e)
		{
			log.warn("##### ERRO AGENDADOR DE TAREFA: E-mail satisfação de clientes - ERRO IOException \n"+e.getMessage()+" - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
			e.printStackTrace();
		
		}finally{
			try {
				
				rsSig.close();
				stSig.close();
				conSigma.close();
			} catch (SQLException e) {
				log.warn("##### ERRO AGENDADOR DE TAREFA: E-mail satisfação de clientes - ERRO SQLException \n"+e.getMessage()+" - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));

				e.printStackTrace();
			}
			
			log.warn("##### FINALIZAR AGENDADOR DE TAREFA: E-mail satisfação de clientes - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
		}
			
	}
}