package com.neomind.fusion.custom.orsegups.adapter;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class FCNDefineResponsaveis implements AdapterInterface
{
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{
			System.out.println("Começou a Inserir o Responsável");
			String codReg = processEntity.findField("contratoSapiens.usu_codreg").getValue().toString();

			if (NeoUtils.safeIsNull(codReg))
			{
				throw new WorkflowException("Não foi possível determinar a regional do contrato");
			}
			QLFilter filter = new QLEqualsFilter("codigo", Long.valueOf(codReg));

			NeoObject obj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("FCNresponsabilidade"), filter);

			if (NeoUtils.safeIsNull(obj))
			{
				throw new WorkflowException("Não foi possível determinar a regional do contrato"
					+ " (Objeto não localizado no eform FCNresponsabilidade). Regional: " + codReg.toString());
			}
			EntityWrapper wrapperObj = new EntityWrapper(obj);

			NeoPaper justEquipamento = (NeoPaper) wrapperObj.findValue("responsavelEquipamento");
			NeoPaper gerente = (NeoPaper) wrapperObj.findValue("respGerente");
			NeoPaper coodernador = (NeoPaper) wrapperObj.findValue("respCoordenador");
			NeoPaper analista = (NeoPaper) wrapperObj.findValue("respAnalista");
			NeoPaper comercial = (NeoPaper) wrapperObj.findValue("respComercial");
			NeoPaper superintendencia = (NeoPaper) wrapperObj.findValue("respSuperintendencia");
			NeoPaper diretor = (NeoPaper) wrapperObj.findValue("respDiretor");
			NeoPaper presidencia = (NeoPaper) wrapperObj.findValue("respPresidencia");

			System.out.println("Passou pelos insertes");

			processEntity.setValue("responsavelJustificativaEquipamento", justEquipamento);
			processEntity.setValue("gerenteRegional", gerente);
			processEntity.setValue("coordenadorRegional", coodernador);
			processEntity.setValue("analistaRegional", analista);
			processEntity.setValue("gerenteComercial", comercial);
			processEntity.setValue("superintendencia", superintendencia);
			processEntity.setValue("diretor", diretor);
			processEntity.setValue("presidente", presidencia);

			System.out.println("Terrminou de Inserir o Responsável");
		}
		catch (Exception e)
		{
			if (e instanceof WorkflowException)
			{
				throw (WorkflowException) e;
			}
			else
			{
				System.out.println("workflow C023 - FCN - Cancelamento de Contrato Iniciativa eNEW / C025 - FCN - Cancelamento de Contrato Inadimplência eNEW - Metodo start ");
				throw new WorkflowException("Erro ao Inserir o Responsável" + e);
			}
		}
	}
}
