package com.neomind.fusion.custom.orsegups.fulltrack.engine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import com.google.gson.Gson;
import com.neomind.fusion.custom.orsegups.fulltrack.bean.FulltrackVeiculo;
import com.neomind.fusion.custom.orsegups.fulltrack.messages.ResponseVeiculo;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;

public class CarImport {

    public static void run() {

	StringBuilder retorno = new StringBuilder();

	try {

	    URL url = new URL("https://ws.fulltrack2.com/vehicles/all");
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setRequestMethod("GET");
	    conn.setRequestProperty("apiKey", "682ee2548d4a73d8fadf72f32c52df790eb72aff");
	    conn.setRequestProperty("secretKey", "4c02a36eebf652e2b753cb355cb763132259c65e");
	    conn.setRequestProperty("Content-Type", "application/json");
	    conn.setRequestProperty("Accept", "application/json");

	    if (conn.getResponseCode() != 200) {
		throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
	    }

	    BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream()), "UTF-8"));

	    String output;

	    while ((output = br.readLine()) != null) {
		retorno.append(output + "\n");
	    }

	    conn.disconnect();

	} catch (MalformedURLException e) {

	    e.printStackTrace();

	} catch (IOException e) {

	    e.printStackTrace();

	}

	System.out.println(retorno.toString());

	Gson gson = new Gson();
	ResponseVeiculo result = gson.fromJson(retorno.toString(), ResponseVeiculo.class);

	addToDB(result);

    }

    private static void addToDB(ResponseVeiculo r) {

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();
	
	sql.append(" IF NOT EXISTS (SELECT 1 FROM FULLTRACK_VEICULO V2 WHERE V2.ID=?) ");
	sql.append(" INSERT INTO FULLTRACK_VEICULO VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");

	try {
	    conn = PersistEngine.getConnection("IMPORTACOES");
	    pstm = conn.prepareStatement(sql.toString());
	    
	    for (FulltrackVeiculo v : r.getData() ){
		
		pstm.setInt(1, v.getRas_vei_id());
		
		pstm.setInt(2, v.getRas_vei_id());
		
		if (v.getRas_vei_id_cli() != null && !v.getRas_vei_id_cli().isEmpty()){
		    pstm.setInt(3,  Integer.parseInt(v.getRas_vei_id_cli()));
		}else{
		    pstm.setNull(3, Types.NULL);
		}
		
		pstm.setString(4, v.getRas_vei_placa() != null ? v.getRas_vei_placa() : null );
		pstm.setString(5, v.getRas_vei_veiculo() != null ? v.getRas_vei_veiculo() : null );
		pstm.setString(6, v.getRas_vei_chassi() != null ? v.getRas_vei_chassi() : null );
		pstm.setString(7, v.getRas_vei_ano() != null ? v.getRas_vei_ano() : null );
		pstm.setString(8, v.getRas_vei_cor() != null ? v.getRas_vei_cor() : null );
				
		if (v.getRas_vei_tipo() != null && !v.getRas_vei_tipo().isEmpty()){
		    pstm.setInt(9,  Integer.parseInt(v.getRas_vei_tipo()));
		}else{
		    pstm.setNull(9, Types.NULL);
		}
						
		if (v.getRas_vei_fabricante() != null && !v.getRas_vei_fabricante().isEmpty()){
		    pstm.setInt(10,  Integer.parseInt(v.getRas_vei_fabricante()));
		}else{
		    pstm.setNull(10, Types.NULL);
		}
		
		pstm.setString(11, v.getRas_vei_modelo() != null ? v.getRas_vei_modelo() : null );
		
		if (v.getRas_vei_equipamento() != null && !v.getRas_vei_equipamento().isEmpty()){
		    pstm.setInt(12,   Integer.parseInt(v.getRas_vei_equipamento()));
		}else{
		    pstm.setNull(12, Types.NULL);
		}
		
		pstm.setNull(13, Types.NULL);
		
		pstm.addBatch();
		pstm.clearParameters();
	    }
	    
	    int[] results = pstm.executeBatch();
	    
	    for (int i=0; i< results.length; i++){
		int value = results[i];
		
		if (value < 0) {
		    System.out.println("### Falha ao inserir o veiculo de ID: "+r.getData().get(i).getRas_vei_id());
		}
		
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

    }

}
