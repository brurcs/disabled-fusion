package com.neomind.fusion.custom.orsegups.utils;

import java.math.BigDecimal;

public class FAPAgrupadorVO
{
	private String codigoProcesso;
	private String item;
	private String tipoItem;
	private BigDecimal valorTotal;
	private BigDecimal valorItem;
	
	public String getItem()
	{
		return item;
	}
	public void setItem(String item)
	{
		this.item = item;
	}
	public String getTipoItem()
	{
		return tipoItem;
	}
	public void setTipoItem(String tipoItem)
	{
		this.tipoItem = tipoItem;
	}
	public String getCodigoProcesso()
	{
		return codigoProcesso;
	}
	public void setCodigoProcesso(String codigoProcesso)
	{
		this.codigoProcesso = codigoProcesso;
	}
	public BigDecimal getValorTotal()
	{
		return valorTotal;
	}
	public void setValorTotal(BigDecimal valorTotal)
	{
		this.valorTotal = valorTotal;
	}
	public BigDecimal getValorItem()
	{
		return valorItem;
	}
	public void setValorItem(BigDecimal valorItem)
	{
		this.valorItem = valorItem;
	}
}
