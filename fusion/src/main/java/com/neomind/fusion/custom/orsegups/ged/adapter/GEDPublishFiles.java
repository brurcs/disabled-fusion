package com.neomind.fusion.custom.orsegups.ged.adapter;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.dms.DMSEngine;
import com.neomind.fusion.dms.DocumentStatus;
import com.neomind.fusion.doc.NeoDocument;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.doc.folder.Folder;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.dynamic.DocumentEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class GEDPublishFiles implements AdapterInterface
{

	private Map<String, NeoDocument> documents = new HashMap<String, NeoDocument>();

	@SuppressWarnings("unchecked")
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		NeoObject colaborador = (NeoObject) processEntity.findValue("colaborador");

		Collection<NeoObject> documentosRH = (Collection<NeoObject>) processEntity.findValue("documentos");

		if (colaborador != null && documentosRH != null && documentosRH.size() > 0)
		{
			for (NeoObject documentoRH : documentosRH)
			{
				this.publishDocuments(documentoRH, colaborador);
			}
			
			this.releaseDocuments();
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	private void publishDocuments(NeoObject documentosRH, NeoObject colaborador)
	{
		if (documentosRH != null)
		{
			EntityWrapper documentoRHWrapper = new EntityWrapper(documentosRH);

			Boolean multiplosArquivos = (Boolean) documentoRHWrapper.findValue("multiplosArquivos");

			Collection<NeoFile> neoFiles = new ArrayList<NeoFile>();

			if (multiplosArquivos)
			{
				String path = (String) documentoRHWrapper.findValue("parametrosUpload.caminhoArquivos");

				if (path != null)
				{
					File folder = new File(path);

					if (!folder.exists())
					{
						throw new WorkflowException("Caminho informado não existe!");
					}

					if (folder.isDirectory())
					{
						NeoStorage neoStorage = NeoStorage.getDefault();

						File[] arquivos = folder.listFiles();
						for (File arquivo : arquivos)
						{
							if (arquivo.isFile() && !arquivo.isHidden())
							{
								NeoFile neoFile = neoStorage.copy(arquivo);
								neoFile.setOrigin(NeoFile.Origin.UPLOAD);
								neoFiles.add(neoFile);
							}
						}
					}
					else
					{
						throw new WorkflowException("Caminho informado não corresponde a uma pasta!");
					}
				}
				else
				{
					throw new WorkflowException("Caminho informado inválido!");
				}
			}
			else
			{
				NeoFile neoFile = (NeoFile) documentoRHWrapper.findValue("uploadSingleFile.arquivo");
				neoFiles.add(neoFile);
			}

			if (neoFiles != null && !neoFiles.isEmpty())
			{
				DocumentEntityInfo documentEntityInfo = (DocumentEntityInfo) documentoRHWrapper.findValue("tipoDocumento");
				EntityWrapper colaboradorWrapper = new EntityWrapper(colaborador);

				NeoDocument neoDocument = this.getDocument(documentEntityInfo, colaborador);

				QLEqualsFilter empresaFilter = new QLEqualsFilter("codigo", (Long) colaboradorWrapper.findValue("numemp"));
				NeoObject empresaObj = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("GCEmpresa"), empresaFilter);

				EntityWrapper documentWrapper = new EntityWrapper(neoDocument);
				documentWrapper.setValue("colaboradorExt", colaborador);
				documentWrapper.setValue("nomeColaborador", colaboradorWrapper.findValue("nomfun"));
				documentWrapper.setValue("matricula", colaboradorWrapper.findValue("numcad"));
				documentWrapper.setValue("numeroCPF", colaboradorWrapper.findValue("numcpf"));
				documentWrapper.setValue("empresa", empresaObj);

				Folder folder = this.getDocumentFolder(documentEntityInfo);

				String titulo = montaTituloDocumento(documentWrapper);

				documentWrapper.setValue("title", titulo);

				// adiciona os arquivos novos ao documento
				Collection<NeoObject> arquivosGED = (Collection<NeoObject>) documentWrapper.findValue("arquivos");
				if (arquivosGED == null)
				{
					arquivosGED = new ArrayList<NeoObject>();
				}
				for (NeoFile neoFile : neoFiles)
				{
					NeoObject arquivoGED = AdapterUtils.createNewEntityInstance("arquivoGED");
					if (arquivoGED != null)
					{
						EntityWrapper arquivoGEDWrapper = new EntityWrapper(arquivoGED);
						arquivoGEDWrapper.setValue("arquivo", neoFile);
						if (multiplosArquivos)
						{
							arquivoGEDWrapper.setValue("dia", documentoRHWrapper.findValue("parametrosUpload.dia"));
							arquivoGEDWrapper.setValue("mes", documentoRHWrapper.findValue("parametrosUpload.mes"));
							arquivoGEDWrapper.setValue("ano", documentoRHWrapper.findValue("parametrosUpload.ano"));
						}
						else
						{
							arquivoGEDWrapper.setValue("dia", documentoRHWrapper.findValue("uploadSingleFile.dia"));
							arquivoGEDWrapper.setValue("mes", documentoRHWrapper.findValue("uploadSingleFile.mes"));
							arquivoGEDWrapper.setValue("ano", documentoRHWrapper.findValue("uploadSingleFile.ano"));
						}

						PersistEngine.persist(arquivoGED);

						arquivosGED.add(arquivoGED);
					}
				}
				documentWrapper.setValue("arquivos", arquivosGED);

				neoDocument.setCreator(PortalUtil.getCurrentUser());
				neoDocument.setCreationDate(new GregorianCalendar());
				neoDocument.setFolder(folder);

				PersistEngine.persist(neoDocument);

				// a liberacao do documento e feita pelo mento releasedocuments pois eles devem ser feitos somente no fina da execucao do adapter
				//DMSEngine.getInstance().releaseVersion(neoDocument);
			}
		}
	}

	public static String montaTituloDocumento(EntityWrapper documentWrapper)
	{
		String titulo = "";

		if (documentWrapper.findValue("empresa.nomeEmpresa") != null)
		{
			titulo += documentWrapper.findValue("empresa.nomeEmpresa");
			titulo += " - ";
		}
		if (documentWrapper.findValue("matricula") != null)
		{
			titulo += documentWrapper.findValue("matricula");
			titulo += " - ";
		}
		if (documentWrapper.findValue("nomeColaborador") != null)
		{
			titulo += documentWrapper.findValue("nomeColaborador");
		}
		return titulo;
	}
	
	private void releaseDocuments()
	{
		if(this.documents != null && !this.documents.isEmpty())
		{
			for(String key : this.documents.keySet())
			{
				NeoDocument neoDocument = this.documents.get(key);
				DMSEngine.getInstance().releaseVersion(neoDocument);
			}
		}
			
	}

	private Folder getDocumentFolder(DocumentEntityInfo documentEntityInfo)
	{
		Folder folder = null;

		QLEqualsFilter equalsFilter = new QLEqualsFilter("tipoDocumento.neoId", documentEntityInfo.getNeoId());
		Class clazz = AdapterUtils.getEntityClass("GEDTipoDocumentoPasta");

		NeoObject tipoDocumentoPasta = PersistEngine.getObject(clazz, equalsFilter);

		if (tipoDocumentoPasta != null)
		{
			EntityWrapper tipoDocumentoPastaWrapper = new EntityWrapper(tipoDocumentoPasta);

			folder = (Folder) tipoDocumentoPastaWrapper.findValue("pasta");
		}

		if (folder == null)
		{
			throw new WorkflowException("Pasta não encontrada para o tipo de documento '" + documentEntityInfo.getTitle() + "'. Verificar relacionamento no eform '[GED] Tipo Documento x Pasta'");
		}

		return folder;
	}

	private NeoDocument getDocument(DocumentEntityInfo documentEntityInfo, NeoObject colaborador)
	{
		//busca o documento no mapa primeiro, pois o mesmo documento pode estar sendo alterado mais de uma vez no mesmo adapter
		//neste caso nao criar versao do mesmo documento varias vezes
		NeoDocument neoDocument = this.documents.get(documentEntityInfo.getTypeName());

		if (neoDocument == null)
		{

			QLEqualsFilter colaboradorFilter = new QLEqualsFilter("colaboradorExt", colaborador);
			QLOpFilter opFilterStatus = new QLOpFilter("documentStatus", "<>", DocumentStatus.DELETED.ordinal());

			QLGroupFilter filterDocument = new QLGroupFilter("AND");
			filterDocument.addFilter(opFilterStatus);
			filterDocument.addFilter(colaboradorFilter);

			neoDocument = (NeoDocument) PersistEngine.getNeoObject(AdapterUtils.getEntityClass(documentEntityInfo.getTypeName()), filterDocument);

			if (neoDocument == null)
			{
				neoDocument = (NeoDocument) documentEntityInfo.createNewInstance();
			}
			else
			{
				EntityWrapper colaboradorWrapper = new EntityWrapper(colaborador);

				if (neoDocument.getDocumentStatus() != null && neoDocument.getDocumentStatus().equals(DocumentStatus.APPROVAL))
				{
					throw new WorkflowException("O documento '" + documentEntityInfo.getTitle() + "' do colaborador '" + colaboradorWrapper.findValue("nomcad") + "' está em aprovação!");
				}

				else if (neoDocument.getDocumentStatus() != null && neoDocument.getDocumentStatus().equals(DocumentStatus.EDITION))
				{
					throw new WorkflowException("O documento '" + documentEntityInfo.getTitle() + "' do colaborador '" + colaboradorWrapper.findValue("nomcad") + "' está em edição!");
				}

				neoDocument = DMSEngine.getInstance().createVersion(neoDocument);
			}
			
			this.documents.put(documentEntityInfo.getTypeName(), neoDocument);
		}

		return neoDocument;
	}

}
