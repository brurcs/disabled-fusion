package com.neomind.fusion.custom.orsegups.presenca.vo;

/**
 * Representa apenas a sequencia e hora do ponto.
 * @author danilo.silva
 *
 */
public class HorariosEscalaHorarioVO
{
	private Long seqMar;
	private Long horBat;
	private Long dia;
	private Long tipoHorario;
	
	public Long getSeqMar()
	{
		return seqMar;
	}
	public void setSeqMar(Long seqMar)
	{
		this.seqMar = seqMar;
	}
	public Long getHorBat()
	{
		return horBat;
	}
	public void setHorBat(Long horBat)
	{
		this.horBat = horBat;
	}
	public Long getDia()
	{
		return dia;
	}
	public void setDia(Long dia)
	{
		this.dia = dia;
	}
	
	public Long getTipoHorario() {
	    return tipoHorario;
	}
	public void setTipoHorario(Long tipoHorario) {
	    this.tipoHorario = tipoHorario;
	}
	
	@Override
	public String toString()
	{
		return "HorariosEscalaHorarioVO [seqMar=" + seqMar + ", horBat=" + horBat + ", dia=" + dia + "]";
	}

	
}
