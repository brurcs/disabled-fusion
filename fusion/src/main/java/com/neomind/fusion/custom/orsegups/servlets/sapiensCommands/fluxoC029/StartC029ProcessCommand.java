package com.neomind.fusion.custom.orsegups.servlets.sapiensCommands.fluxoC029;

import javax.servlet.http.HttpServletRequest;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.servlets.sapiensCommands.Command;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoUtils;

public class StartC029ProcessCommand implements Command {

    @Override
    public String execute(HttpServletRequest request) {
	String resultado = null;
	Long codigoEmpresa = null;
	Long codigoFilial = null;
	Long numeroContrato = null;
	NeoUser usuarioSolicitante = null;
	String codigoTarefa = null;
	NeoObject eFormPrincipal = null;

	codigoEmpresa = NeoUtils.safeLong(request.getParameter("empresa"));
	codigoFilial = NeoUtils.safeLong(request.getParameter("filial"));
	numeroContrato = NeoUtils.safeLong(request.getParameter("contrato"));

	NeoObject contrato = C029Utils.retornaObjetoContrato(codigoEmpresa, codigoFilial, numeroContrato);

	try {

	    if (NeoUtils.safeIsNotNull(contrato)) {
		if (!C029Utils.verificaTarefaAberta(contrato)) {

		    final ProcessModel pm = PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("name", "C029 - FCN - Cancelamento de Contrato Humana - Inadimplência"));

		    usuarioSolicitante = C029Utils.retornaUsuarioSolicitante();

		    eFormPrincipal = C029Utils.insereDadosFormularioPrincipal(contrato);

		    codigoTarefa = OrsegupsWorkflowHelper.iniciaProcesso(pm, eFormPrincipal, false, usuarioSolicitante, true, null);
		    resultado = codigoTarefa;
		}
	    }

	} catch (WorkflowException e) {
	    e.printStackTrace();
	    resultado = "#ERRO!" + e.getErrorList().get(0).getI18nMessage();
	} catch (Exception e) {
	    e.printStackTrace();
	    resultado = "#ERRO!" + e.getMessage();
	}

	return resultado;
    }
}
