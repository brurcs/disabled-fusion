package com.neomind.fusion.custom.orsegups.contract;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.Query;

import org.jfree.util.Log;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.adapter.CadastraCentroCusto;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.adapter.casvig.OrsegupsContratoUtils;
import com.neomind.util.NeoUtils;


/**
 * Preferir utilizar a Classe {@link CentroCusto.java} que tem menos acoplamento com os eforms do fusion.
 * 
 *
 */
public class ContratoIntegracaoSapiens
{
	
	private static String FIELDS_E044CCU = "( CodEmp,CodCcu,DesCcu,AbrCcu,CodUsu,TipCcu,IndAgr,CcuPai,AgrTax,MskCcu,ClaCcu,GruCcu,NivCcu,PosCcu,AnaSin,AceRat,CriRat,CtaRed,CtaRcr,CtaFdv,CtaFcr,ClaDes,CodLoc,LocRea, "+
								   "  PlaSeg,TaxIcu,CodTur,USU_CliNom,USU_LotNom,USU_EmpNom,USU_NumCtr,USU_GerNom,indexp,usu_codreg,datalt,horalt,usualt) ";
	
	private static String FIELDS_E043PCM = "( CodMpc,CtaRed,MskGcc,DefGru,ClaCta,DesCta,AbrCta,AnaSin,NatCta,NivCta,ExiRat,ForRat,ModCtb,CtaCtb,CodCcu, "+
										   "  TipCcu,ExiAux,MetCon,datalt,horalt,usualt,datger,horger,usuger,usu_codreg,valini,valfin ) ";
	
	private static ContratoIntegracaoSapiens instance;
	
	private ContratoIntegracaoSapiens()
	{
		
	}
	
	 /**
	  * Método singleton que controla a instancia da classe
	  */
	public static ContratoIntegracaoSapiens getInstance()
	{
		if (instance == null)
		{
			synchronized (ContratoIntegracaoSapiens.class)
			{
				if (instance == null)
			    {
					instance = new ContratoIntegracaoSapiens();
			    }
			}
		}
		return instance;
	}
	
	/**
	 * 
	 * @param form - eform FGCPrincipal
	 * @param centroCustoQuartoNivel - centro de custo do 4 nivel, para cadatrar o 5 e o 6
	 * @param numeroContrato - numero do contrato, para cadatrar o 5 e o 6
	 * @param codEmp - codigo da empresa - cadastrar o agrupador, 5 nível
	 * @param codFil - codigo da filial - cadastrar o agrupador, 5 nível
	 * @param codCli - codigo do cliente - cadastrar o agrupador, 5 nível
	 * @param codigoAgr - codigo do agrupador - cadastrar o agrupador, 5 nível
	 * @param cliente - 4 nível até o 6 nível?
	 * @param posto - é para cadastrar o 7 e o 8 nível?
	 * @param centroCustoPai - apenas 7 nivel
	 * @param aClaCta - apenas 7 nível
	 * @throws Exception 
	 */
	public static void insert(NeoObject form, String centroCustoQuartoNivel, String numeroContrato, String codEmp, String codFil, String codCli, String codigoAgr, boolean cliente) throws Exception
	{
		synchronized (ContratoIntegracaoSapiens.class)
		{
			if(cliente)
			{
				cadastraClienteCentroCusto(form);
			}else
			{
				insereRegistroAgrupador(codEmp, codFil, codCli, codigoAgr);
				
				cadastraContratoCentroCusto(form, centroCustoQuartoNivel, NeoUtils.safeOutputString(numeroContrato));
			}
		}
	}
	
	public static void insert(NeoObject form) throws Exception
	{
		synchronized (ContratoIntegracaoSapiens.class)
		{
			insert(form, "", "", "", "", "", "", true);
		}
	}
	
	/**
	 * Insere Lotacao Apenas
	 * @param form
	 * @param centroCustoPai
	 * @param aClaCta
	 * @return
	 * @throws Exception 
	 */
	public static String insertLotacao(NeoObject form, String centroCustoPai, String aClaCta) throws Exception
	{
		synchronized (ContratoIntegracaoSapiens.class)
		{
			return cadastraLotacaoCentroCusto(form, centroCustoPai, aClaCta, false);
		}
	}
	
	/**
	 * Insere Posto do contrato
	 * @param form
	 * @param centroCustoPai
	 * @param aClaCta
	 * @return
	 * @throws Exception 
	 */
	public static String insertPosto(NeoObject form, String centroCustoPai, String aClaCta) throws Exception
	{
		synchronized (ContratoIntegracaoSapiens.class)
		{
			return cadastraPostoCentroCusto(form, centroCustoPai, aClaCta);
		}
	}
	
	/**
	 * Insere Lotação e Posto
	 * @param form
	 * @param centroCustoPai
	 * @param aClaCta
	 * @return
	 * @throws Exception 
	 */
	public static String insertLotacaoPosto(NeoObject form, String centroCustoPai, String aClaCta) throws Exception
	{
		synchronized (ContratoIntegracaoSapiens.class)
		{
			return cadastraLotacaoCentroCusto(form, centroCustoPai, aClaCta, true);
		}
	}
	
	public static Collection<String> getEmpresasSapiens()
	{
		List<NeoObject> empresas = (List<NeoObject>)PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE070FIL"));
		Collection<String> codigos = new ArrayList<String>();
		for(NeoObject empresa : empresas)
		{
			EntityWrapper w = new EntityWrapper(empresa);
			String codigo = NeoUtils.safeOutputString(w.findValue("codemp"));
			if(!codigos.contains(codigo))
				codigos.add(codigo);
		}
		
		return codigos;
	}
	
	/*
	 * cria o quarto nível de centro de custo, nivel do cliente
	 */
	public static void cadastraClienteCentroCusto(NeoObject form) throws Exception
	{
		EntityWrapper wForm = new EntityWrapper(form);
		String codEmp = NeoUtils.safeOutputString(wForm.findValue("empresa.codemp"));
		String codFil = NeoUtils.safeOutputString(wForm.findValue("empresa.codfil"));
		
		String numeroContrato; 
		
		numeroContrato = NeoUtils.safeOutputString(wForm.findValue("dadosGeraisContrato.numeroContratoSapiens"));
		
		Long tipoAlteracao = 0L;
		if(wForm.findValue("movimentoAlteracaoContrato") != null)
		{
			tipoAlteracao = (Long) wForm.findValue("movimentoAlteracaoContrato.codTipo");
		}
		NeoObject contrato = (NeoObject)wForm.findValue("numContrato");
		if(contrato != null && tipoAlteracao != 1)
		{
			EntityWrapper wContrato = new EntityWrapper(contrato);
			numeroContrato = NeoUtils.safeOutputString(wContrato.findValue("usu_numctr"));
		}
		
		
		if (wForm.findValue("centroCusto3Nivel") != null &&	wForm.findValue("classificacaoCentroCusto4Nivel") != null){
			
			String aCtaRedExistente = NeoUtils.safeOutputString(wForm.findValue("aCtaRed")); 
			
			cadastraContratoCentroCusto(form, aCtaRedExistente, "AGR"+numeroContrato);
			return;
		}
		
		String codEmpCC = "";
		if(codEmp.equals("15")){
		    codEmpCC = "16" + codEmp;
		}else if (codEmp.equals("7")){
		    codEmpCC = "150"+codEmp;
		}else{
		    codEmpCC = "15" + codEmp;
		}
		NeoObject cliente = (NeoObject) wForm.findValue("buscaNomeCliente");
		EntityWrapper wCliente = new EntityWrapper(cliente);
		String codCli = NeoUtils.safeOutputString(wCliente.findValue("codcli"));
		
		/*
		 * codcli é encontrado usando a query
		 * SELECT max(ClaCcu) from E044ccu where codemp = '18' and ClaCcu like '1518%' and nivccu = 4 
		 * Resultado = 151828153
		 * remover os 4 primeiros digitos, e o restante adicionar +1
		 * Ex: 151828153 = 1518 28153 = 28153 + 1 = 28154
		 */
		String codigo = "";
		
		Long nCtaRed = 0L;
		String aClaCta = "";
		String aCtaRed = "";
		
		QLGroupFilter gp = new QLGroupFilter("AND");
		
		gp.addFilter(new QLRawFilter("codemp = "+codEmp+" and nivccu = 4 and claccu like '"+codEmpCC+"%'"));
		Collection<NeoObject> centroCustoEmpresa = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), gp, 0,1, "claccu DESC");
		Long maxclaccu = 0L;
		for(NeoObject cc : centroCustoEmpresa)
		{
			EntityWrapper w = new EntityWrapper(cc);
			Long claccu = NeoUtils.safeLong(NeoUtils.safeOutputString(w.findValue("claccu")));
			if(claccu > maxclaccu)
				maxclaccu = claccu;
		}
		
		{
			String valor = NeoUtils.safeOutputString(maxclaccu);
			valor = valor.substring(4, valor.length());
			long valorL = NeoUtils.safeLong(valor) + 1;
			codigo = NeoUtils.safeOutputString(valorL);
			while(codigo.length() < 5)
				codigo = "0" + codigo;
			
			codigo = codEmpCC + codigo;
		}
		
		gp.clearFilterList();
		
		
		/*
		 * como este é o trecho que deve ser sempre executado, só deixar ele
		 */
		nCtaRed = getProximaChaveE043PCM();
		aCtaRed = NeoUtils.safeOutputString(nCtaRed);
		aClaCta = codigo;
		
		String aNomCli = NeoUtils.safeOutputString(wCliente.findValue("nomcli"));
		
		String aDesCta = aNomCli.substring(0, (aNomCli.length()>80?80:aNomCli.length()));
		String aAbrCta = aNomCli.substring(0, (aNomCli.length()>20?20:aNomCli.length()));
		String dDatHoj = new SimpleDateFormat("yyyy-MM-dd").format(new GregorianCalendar().getTime());
		String aCtaPai = CadastraCentroCusto.getCentroCusto(codEmpCC, 3, codEmp);

		if (OrsegupsContratoUtils.existeClassificacaoPlanoCentroCusto("SAPIENS", aClaCta )){
			throw new Exception("Não foi possível cadastrar o centro de custo no Modelo de Plano. A classificação " + aClaCta + " conflita com outra já inserida. Tente novamente.");
		}
		String query = " INSERT INTO E043PCM "+FIELDS_E043PCM+"  VALUES (203, :chave, '1.2.33.44444.555.66.7777.88', 'X', '"+aClaCta+"', '"+aDesCta+"', '" +
				aAbrCta+"', 'S', 'D', 4, 'N', 0, 0, 0, ':chave', 3, 'N', 0, '1900-12-31', 0, 0, '"+dDatHoj+"', 0, 1, NULL, '1900-12-31', " +
				"'1900-12-31') ";
		aCtaRed = NeoUtils.safeOutputString(OrsegupsContratoUtils.safeInsert("SAPIENS", "SELECT MAX(ctared)+10 FROM E043PCM WHERE codmpc = 203", query, 10, 5000));
		if ("0".equals(aCtaRed)){
			throw new Exception("Não foi possível cadastrar o centro de custo no Modelo de Plano. Tente Novamente.");
		}
		
		Collection<String> empresas = getEmpresasSapiens();
		String aCodccu = "";
		for(String empresa : empresas)
		{
			String query2 = "INSERT INTO E044CCU  "+FIELDS_E044CCU+" VALUES ("+empresa+", "+aCtaRed+", '"+aDesCta.substring(0, (aDesCta.length()>80?80:aDesCta.length()))+"', '"+aAbrCta+"', 0, 3, '', '"+aCtaPai+"', '', " +
					"'1.2.33.44444.555.66.7777.88', '"+aClaCta+"', 1, 4, 5, 'S', 'N', 5, 0, 0, 0, 0, '', '', '', '', 0, 0, " +
					"NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL) ";

			aCodccu = NeoUtils.safeOutputString( OrsegupsContratoUtils.safeInsert("SAPIENS", Long.parseLong(aCtaRed), query2) );
			
			if ("0".equals(aCodccu)){
				try{
					ContratoUtils.deleteCentroCusto(aClaCta);
				}catch(Exception e){
					ContratoLogUtils.logInfo("Erro ao deletar centros de custo invalidos do 4º nivel. clacta=" + aClaCta  + " msg: " + e.getMessage());
					e.printStackTrace();
				}
				throw new Exception("Não foi possível cadastrar o centro de custo(e044ccu). Tente Novamente.");
			}
			
		}
		
		/*
		 * estes 2 campos serão usados caso seja enviado ao executivo revisar a tarefa
		 */
		wForm.setValue("centroCusto3Nivel", aCtaPai);
		wForm.setValue("classificacaoCentroCusto4Nivel", aClaCta);
		wForm.setValue("aClaCta", aClaCta);
		
		gp.clearFilterList();
		
		
		
		/*
		 * validar o cadastro do quinto nível como sendo o agrupador
		 * validar se foi escolhido um agrupador, senão criar um novo
		 */
		
		if(wForm.findValue("codigoAgrupador") != null)
		{
			
		}else
		{
			String codigoAgr = "AGR" + numeroContrato;
			
			boolean possuiAgrupador = NeoUtils.safeBoolean(wForm.findValue("codigoAgruparInserido"));
			if(!possuiAgrupador)
			{
				if(CadastraCentroCustoClienteContrato.insereRegistroAgrupador(codEmp, codFil, codCli, codigoAgr))
				{
					wForm.setValue("codigoAgruparInserido", true);
					atualizaContratoCodigoAgrupador(codEmp, codFil, numeroContrato, codigoAgr);
				}
			}
			
			cadastraContratoCentroCusto(form, aCtaRed, codigoAgr);
			
		}
	}
	
	/*
	 * cria o quinto nível do centro de custo, nível do agrupador
	 */
	private static void cadastraContratoCentroCusto(NeoObject form, String centroCustoPai, String numeroContrato) throws Exception
	{
		
		EntityWrapper wForm = new EntityWrapper(form);
		if (wForm.findValue("aClaCta") != null && wForm.findValue("aCtaRed") != null && wForm.findValue("centroCustoContrato") != null ){
			
			String aClaCtaExistente = (String) wForm.findValue("aClaCta");
			String aCtaRedExistente = (String) wForm.findValue("aCtaRed");
			cadastraSubContratoCentroCusto(form, aCtaRedExistente, aClaCtaExistente, false);
			return;
		}
		
		if(numeroContrato.contains("AGR"))
			numeroContrato = numeroContrato.replace("AGR", "");
		String aDesCta = "AGRUPADOR - " + numeroContrato.substring(0, (numeroContrato.length()>20?20:numeroContrato.length()));;
		String aAbrCta = aDesCta.substring(0, (aDesCta.length()>20?20:aDesCta.length()));
		
		Long nCtaRed = 0L;
		String aClaCta = "";
		String aCtaRed = "";
		String codEmp = "";
		
		QLGroupFilter gp = new QLGroupFilter("AND");
		gp.addFilter(new QLEqualsFilter("codmpc", 203));
		gp.addFilter(new QLEqualsFilter("descta", aDesCta));
		Collection<NeoObject> listaPlanoContas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE043PCM"), gp);
		if(listaPlanoContas.size() > 0)
		{
			for(NeoObject obj : listaPlanoContas)
			{
				EntityWrapper w = new EntityWrapper(obj);
				nCtaRed = NeoUtils.safeLong(NeoUtils.safeOutputString(w.findValue("ctared")));
				aClaCta = NeoUtils.safeOutputString(w.findValue("clacta"));
				aCtaRed = NeoUtils.safeOutputString(w.findValue("ctared"));
			}
		}else
		{
			nCtaRed = getProximaChaveE043PCM();
			aCtaRed = NeoUtils.safeOutputString(nCtaRed);
			
			aClaCta = NeoUtils.safeOutputString(wForm.findValue("aClaCta"));
			if(aClaCta.equals(""))
			{
				codEmp = NeoUtils.safeOutputString(wForm.findValue("empresa.codemp"));
				String codCli = NeoUtils.safeOutputString(wForm.findValue("buscaNomeCliente.codcli"));
				System.out.println("[FLUXO CONTRATOS]- Integracao contrato - cadastraContratoCentroCusto() codEmp: "+ codEmp+ ",codCli: "+ codCli+", nivcc: "+ 4 +", numCtr: "+numeroContrato);
				aClaCta = CadastraCentroCusto.getClassificacaoCentroCusto(codEmp, codCli, 4, numeroContrato); 
				if(aClaCta.equals(""))
				{
					aClaCta = CadastraCentroCusto.getClassificacaoCentroCusto(codEmp, codCli, 4, "");
					if (aClaCta == null || "".equals(aClaCta) ){ // busca nas tarefas do fusion um arvore para reutilizar
						aClaCta = CadastraCentroCusto.getClassificacaoCC4NivelTarefas(codEmp, codCli);
					}
				}
			}
		}
		
		String aCliCcu = aClaCta + "%";
		gp.clearFilterList();
		gp.addFilter(new QLEqualsFilter("codmpc", 203));
		gp.addFilter(new QLEqualsFilter("nivcta", 5));
		gp.addFilter(new QLRawFilter("clacta like '" + aCliCcu +"'"));
		
		listaPlanoContas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE043PCM"), gp);
		int qtd = listaPlanoContas.size() + 1;
		String nQtdCtr = NeoUtils.safeOutputString(qtd);
		while(nQtdCtr.length() < 3)
			nQtdCtr = "0" + nQtdCtr;
		
		String aCtaPai = centroCustoPai;
		aClaCta = aClaCta + nQtdCtr;
		String dDatHoj = new SimpleDateFormat("yyyy-MM-dd").format(new GregorianCalendar().getTime());		
		
		if (OrsegupsContratoUtils.existeClassificacaoPlanoCentroCusto("SAPIENS", aClaCta )){
			throw new Exception("Não foi possível cadastrar o centro de custo no Modelo de Plano. A classificação " + aClaCta + " conflita com outra já inserida. Tente novamente.");
		}
		String query = " INSERT INTO E043PCM "+FIELDS_E043PCM+"  VALUES (203, :chave, '1.2.33.44444.555.66.7777.88', 'X', '"+aClaCta+"', '"+aDesCta+"', '" +
				aAbrCta+"', 'S', 'D', 5, 'N', 0, 0, 0, ':chave', 3, 'N', 0, '1900-12-31', 0, 0, '"+dDatHoj+"', 0, 1, NULL, '1900-12-31', " +
				"'1900-12-31') ";
		aCtaRed = NeoUtils.safeOutputString(OrsegupsContratoUtils.safeInsert("SAPIENS", "SELECT MAX(ctared)+10 FROM E043PCM WHERE codmpc = 203", query, 10, 5000));
		if ("0".equals(aCtaRed) || "".equals(aCtaRed) ){
			throw new Exception("Não foi possível cadastrar o centro de custo no Modelo de Plano. Tente Novamente.");
		}

		Collection<String> empresas = getEmpresasSapiens();
		String aCodccu = "";
		for(String empresa : empresas)
		{
			String query2 = "INSERT INTO E044CCU "+FIELDS_E044CCU+" VALUES ("+empresa+", "+aCtaRed+", '"+aDesCta.substring(0, (aDesCta.length()>80?80:aDesCta.length()))+"', '"+aAbrCta+"', 0, 3, '', '"+aCtaPai+"', '', " +
					"'1.2.33.44444.555.66.7777.88', '"+aClaCta+"', 1, 5, 3, 'S', 'N', 5, 0, 0, 0, 0, '', '', '', '', 0, 0, " +
					"NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL) ";
			aCodccu = NeoUtils.safeOutputString( OrsegupsContratoUtils.safeInsert("SAPIENS", Long.parseLong(aCtaRed), query2) );
			
			if ("0".equals(aCodccu) || "".equals(aCodccu) ){
				try{
					ContratoUtils.deleteCentroCusto(aClaCta);
				}catch(Exception e){
					ContratoLogUtils.logInfo("Erro ao deletar centros de custo invalidos. clacta=" + aClaCta  + " msg: " + e.getMessage());
					e.printStackTrace();
				}
				throw new Exception("Não foi possível cadastrar o centro de custo. Tente Novamente. ");
			}
		}
		
		wForm.setValue("aClaCta", aClaCta);
		wForm.setValue("aCtaRed", aCtaRed);
		wForm.setValue("centroCustoContrato", aCtaRed);
		
		cadastraSubContratoCentroCusto(form, aCtaRed, aClaCta, false);
	}
	
	/*
	 * cadastra o sexto nível do centro de custo, nível do contrato
	 */
	private static void cadastraSubContratoCentroCusto(NeoObject form, String centroCustoPai, String aClaCta, boolean cadastraTodaArvore) throws Exception
	{
		EntityWrapper wForm = new EntityWrapper(form);
		
		
		if (wForm.findValue("aClaCta6") != null && wForm.findValue("centroCusto6") != null){
			String aClaCtaExistente = (String) wForm.findValue("aClaCta6");
			String aCtaRedExistente = (String) wForm.findValue("centroCusto6");
			
			if(cadastraTodaArvore)
				cadastraLotacaoCentroCusto(form, aCtaRedExistente, aClaCtaExistente, cadastraTodaArvore);
			
			return;
		}
		
		Long tipoAlteracao = 0L;
		if(wForm.findValue("movimentoAlteracaoContrato") != null)
		{
			tipoAlteracao = (Long) wForm.findValue("movimentoAlteracaoContrato.codTipo");
		}
		
		String numeroContrato = NeoUtils.safeOutputString(wForm.findValue("numContrato.usu_numctr"));
		if(numeroContrato.equals("") || tipoAlteracao == 1)
			numeroContrato = NeoUtils.safeOutputString(wForm.findValue("dadosGeraisContrato.numeroContratoSapiens"));
		//6 nivel - buscaNomeCliente.codcli
		String codccuPai = NeoUtils.safeOutputString(centroCustoPai);
		Long nCtaRed = 0L;
		String aCtaRed = "";
		
		String aDesCta = "CONTRATO - " + numeroContrato;//+ codccuPai.substring(0, (codccuPai.length()>80?80:codccuPai.length()));;
		String aAbrCta = aDesCta.substring(0, (aDesCta.length()>20?20:aDesCta.length()));
		
		if(aClaCta == null || aClaCta.equals(""))
		{
			String codEmp = NeoUtils.safeOutputString(wForm.findValue("empresa.codemp"));
			String codCli = NeoUtils.safeOutputString(wForm.findValue("buscaNomeCliente.codcli"));
			aClaCta = CadastraCentroCusto.getClassificacaoCentroCusto(codEmp, codCli, 5, numeroContrato);
		}
		
		aClaCta = aClaCta + "01";
		
		nCtaRed = getProximaChaveE043PCM();
		aCtaRed = NeoUtils.safeOutputString(nCtaRed);	
		
		String dDatHoj = new SimpleDateFormat("yyyy-MM-dd").format(new GregorianCalendar().getTime());
		String aCtaPai = codccuPai;		
		
		if (OrsegupsContratoUtils.existeClassificacaoPlanoCentroCusto("SAPIENS", aClaCta )){
			throw new Exception("Não foi possível cadastrar o centro de custo no Modelo de Plano. A classificação " + aClaCta + " conflita com outra já inserida. Tente novamente.");
		}
		String query = " INSERT INTO E043PCM "+FIELDS_E043PCM+"  VALUES (203, :chave, '1.2.33.44444.555.66.7777.88', 'X', '"+aClaCta+"', '"+aDesCta+"', '" +
				aAbrCta+"', 'S', 'D', 6, 'N', 0, 0, 0, ':chave', 3, 'N', 0, '1900-12-31', 0, 0, '"+dDatHoj+"', 0, 1, NULL, '1900-12-31', " +
				"'1900-12-31') ";
		aCtaRed = NeoUtils.safeOutputString(OrsegupsContratoUtils.safeInsert("SAPIENS", "SELECT MAX(ctared)+10 FROM E043PCM WHERE codmpc = 203", query, 10, 5000));
		if ("0".equals(aCtaRed) || "".equals(aCtaRed)){
			throw new Exception("Não foi possível cadastrar o centro de custo no Modelo de Plano. Tente Novamente.");
		}
		
		Collection<String> empresas = getEmpresasSapiens();
		String aCodccu = "";
		for(String empresa : empresas)
		{
			
			String query2 = "INSERT  INTO E044CCU  "+FIELDS_E044CCU+" VALUES ("+empresa+", "+aCtaRed+", '"+ContratoUtils.truncaCampo(aDesCta, 80)+"', '"+aAbrCta+"', 0, 3, '', '"+aCtaPai+"', '', " +
					"'1.2.33.44444.555.66.7777.88', '"+aClaCta+"', 1, 6, 2, 'S', 'N', 5, 0, 0, 0, 0, '', '', '', '', 0, 0, " +
					"NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL) ";
			
			aCodccu = NeoUtils.safeOutputString( OrsegupsContratoUtils.safeInsert("SAPIENS", Long.parseLong(aCtaRed), query2) );
			
			if ("0".equals(aCodccu) || "".equals(aCodccu)){
				try{
					ContratoUtils.deleteCentroCusto(aClaCta);
				}catch(Exception e){
					ContratoLogUtils.logInfo("Erro ao deletar centros de custo invalidos. clacta=" + aClaCta  + " msg: " + e.getMessage());
					e.printStackTrace();
				}
				throw new Exception("Não foi possível cadastrar o centro de custo. Tente Novamente.");
			}
			
		}
		
		wForm.setValue("aClaCta6", aClaCta);
		//wForm.setValue("aCtaRed", aCtaRed);
		wForm.setValue("centroCusto6", aCtaRed);
		
		if(cadastraTodaArvore)
			cadastraLotacaoCentroCusto(form, aCtaRed, aClaCta, cadastraTodaArvore);
	}
	
	/*
	 * cadastra o sétimo nível do centro de custo, nível da lotação
	 */
	private static String cadastraLotacaoCentroCusto(NeoObject form, String centroCustoPai, String aClaCta, boolean cadastraTodaArvore) throws Exception 
	{
		
		String retorno = null;
		EntityWrapper wForm = new EntityWrapper(form);
		Long numCtr = (Long) wForm.findValue("dadosGeraisContrato.numeroContratoSapiens");
								
		String codccuPai = NeoUtils.safeOutputString(centroCustoPai);
		NeoObject cliente = (NeoObject) wForm.findValue("buscaNomeCliente");
		EntityWrapper wCliente = new EntityWrapper(cliente);
		String aNomCli = NeoUtils.safeOutputString(wCliente.findValue("nomcli"));
		
		String aDesCta = aNomCli.substring(0, (aNomCli.length()>80?80:aNomCli.length()));
		String aAbrCta = aNomCli.substring(0, (aNomCli.length()>20?20:aNomCli.length()));
		Long nCtaRed = 0L;
		String aCtaRed = "";
		
		aClaCta = aClaCta + "0001"; 
		// Busca Informações na E043PCM - Plano de Contas
		QLGroupFilter filter = new QLGroupFilter("AND");
		filter.addFilter(new QLEqualsFilter("codmpc", 203));
		filter.addFilter(new QLEqualsFilter("nivcta", 7));
		filter.addFilter(new QLRawFilter("clacta like '" + aClaCta + "'"));
		List<NeoObject> noListaPlanoConta = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE043PCM"), filter);
		if(noListaPlanoConta.size() > 0)
		{
			for(NeoObject noPlanoConta:noListaPlanoConta)
			{
				EntityWrapper ewPlanoConta = new EntityWrapper(noPlanoConta);
				nCtaRed = NeoUtils.safeLong(NeoUtils.safeOutputString(ewPlanoConta.findValue("ctared")));
				aClaCta = NeoUtils.safeOutputString(ewPlanoConta.findValue("clacta"));
				aCtaRed = NeoUtils.safeOutputString(nCtaRed);
			}
		}else
		{
			nCtaRed = getProximaChaveE043PCM();
			aCtaRed = NeoUtils.safeOutputString(nCtaRed);
		}
		
		String aCtaPai = codccuPai;	
		String data = new SimpleDateFormat("yyyy-MM-dd").format(new GregorianCalendar().getTime());
		
		if (OrsegupsContratoUtils.existeClassificacaoPlanoCentroCusto("SAPIENS", aClaCta )){
			throw new Exception("Não foi possível cadastrar o centro de custo no Modelo de Plano. A classificação " + aClaCta + " conflita com outra já inserida. Tente novamente.");
		}
		String query = " INSERT INTO E043PCM "+FIELDS_E043PCM+"  VALUES (203, :chave, '1.2.33.44444.555.66.7777.88', 'X', '"+aClaCta+"', '"+aDesCta+"', '" +
				aAbrCta+"', 'S', 'D', 7, 'N', 0, 0, 0, ':chave', 3, 'N', 0, '1900-12-31', 0, 0, '"+data+"', 0, 1, NULL, '1900-12-31', " +
				"'1900-12-31') ";
		
		aCtaRed = NeoUtils.safeOutputString(OrsegupsContratoUtils.safeInsert("SAPIENS", "SELECT MAX(ctared)+10 FROM E043PCM WHERE codmpc = 203", query, 10, 5000));
		
		ContratoUtils.addCCToRollBack(numCtr, Long.parseLong(aCtaRed));
		
		retorno = aCtaRed;
		if ("0".equals(aCtaRed)){
			try{
				ContratoUtils.deleteCentroCusto(aClaCta);
			}catch(Exception e){
				ContratoLogUtils.logInfo("Erro ao deletar centros de custo invalidos. clacta=" + aClaCta  + " msg: " + e.getMessage());
				e.printStackTrace();
			}
			throw new Exception("Não foi possível cadastrar o centro de custo no Modelo de Plano. Tente Novamente.");
		}
		
		Collection<String> empresas = getEmpresasSapiens();
		
		String aCodccu = "";
		for(String empresa : empresas)
		{
			
			String query2 = "INSERT INTO E044CCU  "+FIELDS_E044CCU+" VALUES ("+empresa+", "+aCtaRed+", '"+ContratoUtils.truncaCampo(aDesCta, 80)+"', '"+aAbrCta+"', 0, 3, '', '"+aCtaPai+"', '', " +
					"'1.2.33.44444.555.66.7777.88', '"+aClaCta+"', 1, 7, 4, 'S', 'N', 5, 0, 0, 0, 0, '', '', '', '', 0, 0, " +
					"NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL) ";
			aCodccu = NeoUtils.safeOutputString( OrsegupsContratoUtils.safeInsert("SAPIENS", Long.parseLong(aCtaRed), query2) );
			
			if ("0".equals(aCodccu)){
				try{
					ContratoUtils.deleteCentroCusto(aClaCta);
				}catch(Exception e){
					ContratoLogUtils.logInfo("Erro ao deletar centros de custo invalidos. clacta=" + aClaCta  + " msg: " + e.getMessage());
					e.printStackTrace();
				}
				throw new Exception("Não foi possível cadastrar o centro de custo. Tente Novamente.");
			}
			
			
		}
		
		
		if ( cadastraTodaArvore ){
			String codccu8 = cadastraPostoCentroCusto(form, aCtaRed, aClaCta);
			if (codccu8!= null){
				retorno = codccu8;
			}
		}else{
			return aCtaRed;
		}
		return retorno;
		
	}
	
	/**
	 * cadastra o último nível do centro de custo, o nível do posto
	 * @param form
	 * @param centroCustoPai
	 * @param aClaCta - classificação do centro de custo pai
	 * @return
	 * @throws Exception 
	 */
	private static String cadastraPostoCentroCusto(NeoObject form, String centroCustoPai, String aClaCta) throws Exception
	{
		String retorno = null;
		EntityWrapper wForm = new EntityWrapper(form);
		String codccuPai = NeoUtils.safeOutputString(centroCustoPai);
		NeoObject cliente = (NeoObject) wForm.findValue("buscaNomeCliente");
		EntityWrapper wCliente = new EntityWrapper(cliente);
		String aNomCli = NeoUtils.safeOutputString(wCliente.findValue("nomcli"));
		Long numCtr = (Long) wForm.findValue("dadosGeraisContrato.numeroContratoSapiens");
		
		// Início do processo para inserção 		
		QLGroupFilter filter = new QLGroupFilter("AND");
		filter.addFilter(new QLEqualsFilter("nivcta", 8));
		filter.addFilter(new QLEqualsFilter("codmpc", 203));
		filter.addFilter(new QLRawFilter("clacta LIKE '" + aClaCta  +"%'"));
		
		List<NeoObject> noListaPlanoConta = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE043PCM"), filter);
		Long nCtaRed = null;
		String aCtaRed = "";
		int qtd = noListaPlanoConta.size();
		String aPosCcu = qtd < 9 ? ("0"+NeoUtils.safeOutputString( qtd + 1 )) : NeoUtils.safeOutputString( qtd + 1 );
		aClaCta = aClaCta + aPosCcu;
		
		/*filter.clearFilterList();
		filter.addFilter(new QLEqualsFilter("codmpc", 203));
		//filter.addFilter(new QLRawFilter("ctared in (SELECT MAX(ctared) FROM E043PCM WHERE codmpc = 203)"));
		noListaPlanoConta = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE043PCM"), filter, 0, 1, "ctared DESC");
		for(NeoObject no : noListaPlanoConta)
		{
			EntityWrapper wNo = new EntityWrapper(no);
			nCtaRed = NeoUtils.safeLong(NeoUtils.safeOutputString(wNo.findValue("ctared"))) + 10;
			aCtaRed = NeoUtils.safeOutputString(nCtaRed);
			retorno = aCtaRed;
		}*/
		
		nCtaRed = getProximaChaveE043PCM();
		aCtaRed = NeoUtils.safeOutputString(nCtaRed);
		
		String aDesCta = "P" + aPosCcu + " - " + aNomCli.substring(0, (aNomCli.length()>80?80:aNomCli.length()));
		String aAbrCta = aDesCta.substring(0, (aDesCta.length()>20?20:aDesCta.length()));
		String aCtaPai = codccuPai;		
		String data = new SimpleDateFormat("yyyy-MM-dd").format(new GregorianCalendar().getTime());
		/*String query = " INSERT INTO E043PCM "+FIELDS_E043PCM+"  VALUES (203, "+nCtaRed+", '1.2.33.44444.555.66.7777.88', 'X', '"+aClaCta+"', '"+aDesCta+"', '" +
						aAbrCta+"', 'A', 'D', 8, 'N', 0, 0, 0, '"+aCtaRed+"', 3, 'N', 0, '1900-12-31', 0, 0, '"+data+"', 0, 1, NULL, '1900-12-31', " +
						"'1900-12-31') ";*/
		
		if (OrsegupsContratoUtils.existeClassificacaoPlanoCentroCusto("SAPIENS", aClaCta )){
			throw new Exception("Não foi possível cadastrar o centro de custo no Modelo de Plano. A classificação " + aClaCta + " conflita com outra já inserida. Tente novamente.");
		}
		String query = " INSERT INTO E043PCM "+FIELDS_E043PCM+"  VALUES (203, :chave, '1.2.33.44444.555.66.7777.88', 'X', '"+aClaCta+"', '"+aDesCta+"', '" +
				aAbrCta+"', 'A', 'D', 8, 'N', 0, 0, 0, ':chave', 3, 'N', 0, '1900-12-31', 0, 0, '"+data+"', 0, 1, NULL, '1900-12-31', " +
				"'1900-12-31') ";
		
		aCtaRed = NeoUtils.safeOutputString(OrsegupsContratoUtils.safeInsert("SAPIENS", "SELECT MAX(ctared)+10 FROM E043PCM WHERE codmpc = 203", query, 10, 5000));
		
		ContratoUtils.addCCToRollBack(numCtr, Long.parseLong(aCtaRed));
		
		retorno = aCtaRed;
		if ("0".equals(aCtaRed)){
			try{
				ContratoUtils.deleteCentroCusto(aClaCta);
			}catch(Exception e){
				ContratoLogUtils.logInfo("Erro ao deletar centros de custo invalidos. clacta=" + aClaCta  + " msg: " + e.getMessage());
				e.printStackTrace();
			}
			ContratoLogUtils.logInfo("Não foi possível cadastrar o centro de custo no Modelo de Plano. Tente Novamente.[numCtr="+numCtr+",aClaCta="+aClaCta+",aNomCli="+aNomCli+"]");
			throw new Exception("Não foi possível cadastrar o centro de custo no Modelo de Plano. Tente Novamente.[numCtr="+numCtr+",aClaCta="+aClaCta+",aNomCli="+aNomCli+"]");
		}
		
		/*
		StringBuilder sql = new StringBuilder();
		sql.append(query);
		Query queryExecute = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		try
		{
			queryExecute.executeUpdate();
		}catch(Exception e)
		{
			throw new Exception("Erro");
		}*/
		
		Collection<String> empresas = getEmpresasSapiens();
		
		String aCodccu = "";
		for(String empresa : empresas)
		{
			
			String query2 = "INSERT INTO E044CCU "+FIELDS_E044CCU+" VALUES ("+empresa+", "+aCtaRed+", '"+ContratoUtils.truncaCampo(aDesCta, 80)+"', '"+aAbrCta+"', 0, 3, '', '"+aCtaPai+"', '', " +
					"'1.2.33.44444.555.66.7777.88', '"+aClaCta+"', 1, 8, 2, 'A', 'S', 5, 0, 0, 0, 0, '', '', '', '', 0, 0, " +
					"NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL) ";
			
			
			aCodccu = NeoUtils.safeOutputString( OrsegupsContratoUtils.safeInsert("SAPIENS", Long.parseLong(aCtaRed), query2) );
			
			if ("0".equals(aCodccu)){
				try{
					ContratoUtils.deleteCentroCusto(aClaCta);
				}catch(Exception e){
					ContratoLogUtils.logInfo("Erro ao deletar centros de custo invalidos. clacta=" + aClaCta  + " msg: " + e.getMessage());
					e.printStackTrace();
				}
				throw new Exception("Não foi possível cadastrar o centro de custo. Tente Novamente.");
			}
			
			/*
			StringBuilder sql2 = new StringBuilder();
			sql2.append(query2);
			queryExecute = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql2.toString());
			try
			{
				queryExecute.executeUpdate();
			}catch(Exception e)
			{
				throw new Exception("Erro ao integrar com o Sapiens. Favor tentar novamente em instantes. Oitavo nível do Centro de Custo");
			}*/
			
			
		}
		
		return retorno;
	}	
	
	private static void atualizaContratoCodigoAgrupador(String codEmp, String codFil, String numCtr,  String codigoAgr) throws Exception {
		StringBuilder query = new StringBuilder();
		query.append(" UPDATE usu_T160CTR SET ");
		query.append("usu_ctragr= '"+codigoAgr+"'");       // int
		query.append(" WHERE usu_codemp = " + codEmp + " and usu_codfil = " + codFil + " and usu_numctr = " + numCtr + " and usu_sitctr = 'A'");

		Query queryExecute = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(query.toString());
		try
		{
			queryExecute.executeUpdate();
		}catch(Exception e)
		{
			System.out.println("[FLUXO CONTRATOS] - ERRO " + query);
			e.printStackTrace();
			throw new Exception("Erro ao integrar com o Sapiens. Favor tentar novamente em instantes. Oitavo nível do Centro de Custo");
		}
		
	}
	
	public static void insereRegistroAgrupador(String codemp, String codfil, String codcli, String codagr)
	{
		
		//UPDATE E044CCU
		String update = "INSERT INTO USU_T160AGR (USU_CTRAGR, USU_CODEMP, USU_CODFIL, USU_CODCLI) VALUES ('" + codagr + "'," + codemp + "," + codfil + "," + codcli + ")";
		StringBuilder sql = new StringBuilder();
		sql.append(update);
		try
		{
			if (!verificaRegistroAgrupador(codemp, codfil, codcli, codagr)){
				Query queryExecute = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
				queryExecute.executeUpdate();
			}
		}catch(Exception e)
		{
			//Violation of PRIMARY KEY
			ContratoLogUtils.logInfo("Agrupador " + codagr +", já existe... " + e.getMessage() + " sql=["+update+"]");
			//se ja existe, n precisa tratar
			//throw new Exception("Erro: já existe agrupador " + codagr );
		}
	}
	
	
	public static boolean verificaRegistroAgrupador(String codemp, String codfil, String codcli, String codagr)
	{
		boolean retorno = false;
		//UPDATE E044CCU
		String sql = "select * from USU_T160AGR where USU_CTRAGR='" + codagr + "' and USU_CODEMP=" + codemp + " and USU_CODFIL=" + codfil + " and USU_CODCLI=" + codcli + " ";
		
		ResultSet rs = null;
		try
		{
			rs = OrsegupsContratoUtils.selectTable(sql,PersistEngine.getConnection("SAPIENS"));
			if(rs != null){
				retorno = true;
			}
			
		}catch(Exception e)
		{
			return false;
		}
		return retorno;
	}
	
	
	
	/**
	 * 
	 * @param eformExterno - eform que será consultado para gerar 
	 * @return
	 */
	private static long getProximaChaveE043PCM()
	{
		long chave = 0;
		QLGroupFilter filter =  new QLGroupFilter("AND");
		filter.addFilter(new QLEqualsFilter("codmpc", 203));
		//filter.addFilter(new QLRawFilter("ctared in (SELECT MAX(ctared) FROM E043PCM WHERE codmpc = 203) "));
		List<NeoObject> noListaPlanoConta = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE043PCM"), filter, 0, 1, "ctared DESC");
		for(NeoObject noPlanoConta:noListaPlanoConta)
		{
			EntityWrapper ewPlanoConta = new EntityWrapper(noPlanoConta);
			chave = NeoUtils.safeLong(NeoUtils.safeOutputString(ewPlanoConta.findValue("ctared"))) + 10;
			//aCtaRed = NeoUtils.safeOutputString(nCtaRed);
		}
		
		return chave;
	}
	
	
	
}
