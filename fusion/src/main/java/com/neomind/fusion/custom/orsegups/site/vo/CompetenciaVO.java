package com.neomind.fusion.custom.orsegups.site.vo;

import java.util.GregorianCalendar;

import com.neomind.util.NeoUtils;

public class CompetenciaVO
{
	Long NumEmp; 
	Long CodCal; 
	Long TipCal; 
	String SitCal; 
	GregorianCalendar PerRef;              
	GregorianCalendar IniApu;              
	GregorianCalendar FimApu;
	
	public Long getNumEmp()
	{
		return NumEmp;
	}
	public void setNumEmp(Long numEmp)
	{
		NumEmp = numEmp;
	}
	public Long getCodCal()
	{
		return CodCal;
	}
	public void setCodCal(Long codCal)
	{
		CodCal = codCal;
	}
	public Long getTipCal()
	{
		return TipCal;
	}
	public void setTipCal(Long tipCal)
	{
		TipCal = tipCal;
	}
	public String getSitCal()
	{
		return SitCal;
	}
	public void setSitCal(String sitCal)
	{
		SitCal = sitCal;
	}
	public GregorianCalendar getPerRef()
	{
		return PerRef;
	}
	public void setPerRef(GregorianCalendar perRef)
	{
		PerRef = perRef;
	}
	public GregorianCalendar getIniApu()
	{
		return IniApu;
	}
	public void setIniApu(GregorianCalendar iniApu)
	{
		IniApu = iniApu;
	}
	public GregorianCalendar getFimApu()
	{
		return FimApu;
	}
	public void setFimApu(GregorianCalendar fimApu)
	{
		FimApu = fimApu;
	}
	@Override
	public String toString()
	{
		return "CompetenciaVO [NumEmp=" + NumEmp + ", CodCal=" + CodCal + ", TipCal=" + TipCal + ", SitCal=" + SitCal + ", PerRef=" + NeoUtils.safeDateFormat(PerRef,"dd/MM/yyyy") + ", IniApu=" +  NeoUtils.safeDateFormat(IniApu,"dd/MM/yyyy") + ", FimApu=" +  NeoUtils.safeDateFormat(FimApu,"dd/MM/yyyy") + "]";
	}
	
	
	
	
	
	
}
