package com.neomind.fusion.custom.orsegups.contract;

import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.contract.vo.DadosDebitoSapiensVO;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.fusion.workflow.simulation.WorkflowService;
import com.neomind.util.NeoUtils;

/**
 * Faz interface entre o fluxo c001 e c035
 * @author danilo.silva
 *
 */
//com.neomind.fusion.custom.orsegups.contract.ContratoC035Interface
public class ContratoC035Interface implements AdapterInterface
{
	
	@Override
	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		Long key = (new GregorianCalendar()).getTimeInMillis();
		ContratoLogUtils.logInfo("["+key+"] - Iniciando Adapter de tarefa C035");
		if (ParametrizacaoFluxoContratos.findParameter("abreTarefaC035").equals("1") ){ // permite o bloqueio dessa funcionalidade via parametrização no eform
			try{
				NeoUser solicitante;
				NeoUser responsavel;
				EntityWrapper wDadosDebito;
				String tarefaC001 = null; 
				Long codemp = null;
				Long codfil = null; 
				Long numctr = null;
				Long codcli = null;
				
				
				NeoUser usuarioSolicitante = PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", PortalUtil.getCurrentUser().getCode() ) );
				NeoUser usuarioResponsavel = PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "camila.silva") );
				
				String numeroContrato = NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numctr"));
				if (numeroContrato != null && !numeroContrato.equals("")){
					numctr = Long.parseLong(numeroContrato);
				}
				
				if(numeroContrato.equals("")){
					numeroContrato = NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.numeroContratoSapiens"));
					numctr = Long.parseLong(numeroContrato);
				}
				
				codemp = (Long) wrapper.findField("empresa.codemp").getValue();
				codfil = (Long) wrapper.findField("empresa.codfil").getValue();
				
				tarefaC001 = origin.getCode();
				
				codcli = NeoUtils.safeLong( NeoUtils.safeOutputString( wrapper.findValue("novoCliente.codigoCliente") ) );
				if (codcli == null ){
					codcli = NeoUtils.safeLong( NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.codcli") ) );
				}
				
				NeoObject oDebitoDados = (NeoObject) wrapper.findValue("dadosGeraisContrato.debitoDados"); 
				
				wDadosDebito = new EntityWrapper(oDebitoDados);
				
				
				//String tarefa = iniciaProcesso(usuarioSolicitante, usuarioResponsavel, wDadosDebito, tarefaC001, codemp, codfil, numctr, codcli);
				NeoObject oRegistroAberturaC035 = AdapterUtils.createNewEntityInstance("FGCFilaAberturaC035");
				EntityWrapper wRegistroAberturaC035 = new EntityWrapper(oRegistroAberturaC035);
				
				
				//ContratoLogUtils.logInfo("[C035]["+key+"] - Tarefa aberta, numero: " + tarefa);
				

				String banco 	= NeoUtils.safeOutputString(wDadosDebito.findValue("banco.codigo"));
				String portador = NeoUtils.safeOutputString(wDadosDebito.findValue("banco.portador"));
				String agencia 	= NeoUtils.safeOutputString(wDadosDebito.findValue("agecia"));
				String conta 	= NeoUtils.safeOutputString(wDadosDebito.findValue("contacorrente"));
				
				wRegistroAberturaC035.setValue("codemp",codemp);		
				wRegistroAberturaC035.setValue("codfil",codfil);		
				wRegistroAberturaC035.setValue("numctr",numctr);		
				wRegistroAberturaC035.setValue("codcli",codcli);		
				wRegistroAberturaC035.setValue("tarefaC001",tarefaC001);
				wRegistroAberturaC035.setValue("abriuTarefa",false);	
				wRegistroAberturaC035.setValue("banco",banco);		
				wRegistroAberturaC035.setValue("portador",portador);	
				wRegistroAberturaC035.setValue("agencia",agencia);		
				wRegistroAberturaC035.setValue("conta",conta);
				wRegistroAberturaC035.setValue("usuarioSolicitante", PortalUtil.getCurrentUser().getCode());
				
				PersistEngine.persist(oRegistroAberturaC035);
				
				
				DadosDebitoSapiensVO debitoVO = new DadosDebitoSapiensVO(portador, banco, agencia, conta);
				ContratoLogUtils.logInfo("["+key+"]-Atualizando dados de débito do cliente : " + debitoVO);
				ContratoUtils.atualizaDadosDebitoSapiens(codcli,codemp,codfil, debitoVO);
				ContratoLogUtils.logInfo("["+key+"]Atualizado com sucesso: " + debitoVO);
				
			}catch(Exception e){
				e.printStackTrace();
				ContratoLogUtils.logInfo("["+key+"] - Erro ao abrir tarefa C035. "+e.getMessage());
			}
		}else{
			ContratoLogUtils.logInfo("Abertura de C035 desabilitada via parametrização do fluxo de contratos");
		}
		
		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Inicia Processo do tipo C035
	 * @param oEformProcesso
	 * @param solicitante
	 * @param responsavel
	 * @return
	 */
	public static String iniciaProcesso( NeoUser solicitante, NeoUser responsavel, EntityWrapper wDadosDebito, String tarefaC001, Long codemp, Long codfil, Long numctr, Long codcli)
	{
		String result = null;
		
		try{
			
			NeoObject oEformProcesso = AdapterUtils.createNewEntityInstance("CDACadastroDebitoAutomatico");
			EntityWrapper wEformProcesso = new EntityWrapper(oEformProcesso);
			
			QLGroupFilter gp = new QLGroupFilter("AND");
			gp.addFilter(new QLEqualsFilter("usu_codemp", codemp));
			gp.addFilter(new QLEqualsFilter("usu_codfil", codfil));
			gp.addFilter(new QLEqualsFilter("usu_numctr", numctr));
			gp.addFilter(new QLEqualsFilter("codcli", codcli));
			
			// vfus_ctrcce
			List<NeoObject> contrato = PersistEngine.getObjects(AdapterUtils.getEntityClass("VFUSCTRSAP"), gp);
			
			for (NeoObject ctr : contrato){
				wEformProcesso.setValue("contrato", ctr);
				System.out.println("Contrato setado: "+numctr);
			}
			
			String banco 	= NeoUtils.safeOutputString(wDadosDebito.findValue("banco.codigo"));
			String portador = NeoUtils.safeOutputString(wDadosDebito.findValue("banco.portador"));
			String agencia 	= NeoUtils.safeOutputString(wDadosDebito.findValue("agecia"));
			String conta 	= NeoUtils.safeOutputString(wDadosDebito.findValue("contacorrente"));
			
			System.out.println("Banco: " + banco + ", Agencia: " + agencia + ", Conta:" +  conta);
			
			//dados sobre o débito
			wEformProcesso.setValue("portador", portador);
			wEformProcesso.setValue("banco", banco);
			wEformProcesso.setValue("agencia", agencia);
			wEformProcesso.setValue("contaCorrente", conta);
			wEformProcesso.setValue("tarefaC001",tarefaC001);
			
			wEformProcesso.setValue("fechouAcordo", true);
			wEformProcesso.setValue("recebeuForm",true);
			wEformProcesso.setValue("negociacao", "Via executivos de vendas pelo fluxo de Contratos - C001. tarefa:"+tarefaC001);
			
			QLGroupFilter gp2 = new QLGroupFilter("AND");
			gp2.addFilter(new QLEqualsFilter("codigo", 1L)); // e-mail
			
			List<NeoObject> formasEnvio = PersistEngine.getObjects(AdapterUtils.getEntityClass("CDAFormaDeEnvio"), gp2);
			
			for (NeoObject envio : formasEnvio){
				wEformProcesso.setValue("formaEnvio", envio);
			}
			
			// abertura do processo
			
			QLEqualsFilter equal = new QLEqualsFilter("Name", "C035 - Cadastro de Débito Automático");
			ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);
			
			/**
			 * @author neomind willian.mews - Alterado para utilizar a nova arquitetura de processos do Fusion
			 * @date 12/03/2015
			 */
			
			final WFProcess processo = WorkflowService.startProcess(processModel, oEformProcesso, false, solicitante);
			
			System.out.println("[C035] + Tarefa " + processo.getCode() + " Aberta via fluxo de contratos.");
			
			/**
			 * FIM ALTERAÇÕES - NEOMIND
			 */
			
			result = processo.getCode();
		}catch(Exception e){
			result = null;
		}
		
		
		return result;
	}
	 

}
