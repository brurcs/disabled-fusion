package com.neomind.fusion.custom.orsegups.contract;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class MapsGoogleUtils {
	
	/*public static void main(String args[]){
		HashMap<String, String> ret = retornaLatitudeLongitude("Rua Gisela 53", "São José", "SC");
		System.out.println("lat, " +ret.get("lat"));
		System.out.println("lng, " +ret.get("lng"));
		HashMap<String, String> ret2 = retornaLatitudeLongitude("Rua Gisela 53", "Sao Jose", "SC");
		System.out.println("lat, " +ret2.get("lat"));
		System.out.println("lng, " +ret2.get("lng"));
		
	}*/
	
	/**
	 * Retorna Latitude e longitude via API do googleMaps. Basta passar o nome da rua - cidade - uf
	 * @param rua
	 * @param cidade
	 * @param uf
	 * @return HashMap<String, String> contendo as coordenadas da localização <b>lat</b> e <b>lng</b>
	 */
	public static HashMap<String, String> retornaLatitudeLongitude(String rua, String cidade, String uf){
		HashMap<String , String> retorno = new HashMap<String, String>();
		
		
		String endereco = rua + ","+cidade+","+uf;
		endereco = endereco.replaceAll("[ ]", "%20");
		URL url;
		try {
			url = new URL("http://maps.google.com/maps/api/geocode/xml?sensor=false&address=" + endereco+ ",brazil");
			URLConnection uc = url.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
			String inputLine;
			//while ((inputLine = in.readLine()) != null) 
			//	System.out.println(inputLine);
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.parse (uc.getInputStream());			
			in.close();	
			
			Element rootElement = doc.getDocumentElement();
			String lat = getString("lat", rootElement);
			String lng = getString("lng", rootElement);
			retorno.put("lat", lat);
			retorno.put("lng", lng);
			
			System.out.println("[FLUXO CONTRATOS]-http://maps.google.com/maps/api/geocode/xml?sensor=false&address=" + endereco+ "-brazil" + "\n>>>  lat:" + lat+ " -  long: " + lng  );
			
			return retorno;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return retorno;
	}
	
	private static String getString(String tagName, Element element) {
		NodeList list = element.getElementsByTagName(tagName);
		if (list != null && list.getLength() > 0) {
			NodeList subList = list.item(0).getChildNodes();

			if (subList != null && subList.getLength() > 0) {
				return subList.item(0).getNodeValue();
			}
		}
		return null;
	}

}
