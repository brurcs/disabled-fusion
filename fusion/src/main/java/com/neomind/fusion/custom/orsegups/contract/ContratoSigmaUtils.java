package com.neomind.fusion.custom.orsegups.contract;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.neomind.fusion.custom.orsegups.contract.vo.ContaSigmaVo;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.adapter.casvig.OrsegupsContratoUtils;
import com.neomind.util.NeoUtils;

public class ContratoSigmaUtils {
	
	/**
	 * Retorna um array de Strings com as contas vinculadas a um posto de um contrato
	 * @param codcliSapiens
	 * @return
	 */
	public static List<ContaSigmaVo> retornaContasSigmaPostoSapiens(String usu_codemp, String usu_codfil, String usu_numctr, String usu_numpos){
		List<ContaSigmaVo> retorno = new ArrayList<ContaSigmaVo>();
		
		String sql ="select usu_sigemp, usu_sigcon, usu_sigpar, usu_codcli from usu_t160sig where usu_numctr = "+usu_numctr+" and usu_t160sig.usu_numpos = "+usu_numpos+" and usu_codemp = "+usu_codemp+" and usu_codfil = "+usu_codfil;
		ResultSet rs = null;
		try{
			rs = OrsegupsContratoUtils.selectTable(sql, PersistEngine.getConnection("SAPIENS"));
			if (rs!= null){
				do{
					ContaSigmaVo conta = new ContaSigmaVo();
					conta.setCdCliente(rs.getString("usu_codcli"));
					conta.setIdEmpresa(rs.getString("usu_sigemp"));
					conta.setIdCentral(rs.getString("usu_sigcon"));
					conta.setParticao (rs.getString("usu_sigpar"));
					retorno.add(conta);
				}while(rs.next());
				rs.close();
				rs = null;
			}
			
			return retorno;
		}catch(Exception e){
			ContratoLogUtils.logInfo("Erro ContratoSigmaUtils.retornaContasSigmaPostoSapiens() " + e.getMessage());
			e.printStackTrace();
			return retorno;
		}
		
	}
	
	/**
	 * Retorna codigo da central, particao e se o controle de partição está ativo ou não na conta do cliente sigma
	 * @param cdClienteSigma - cd_cliente da conta sigma
	 * @return [0] - ID_CENTRAL, [1] - PARTICAO, [2] - FG_CONTROLAR_PARTICAO
	 */
	public static String[] retornaDadosConta(String cdClienteSigma){
		String retorno[] = null;
		
		if (cdClienteSigma != null && !cdClienteSigma.isEmpty()){
			retorno = new String[3];
		
			String sql =" select ID_CENTRAL, PARTICAO, FG_CONTROLAR_PARTICAO from dbcentral where cd_cliente = " + cdClienteSigma ;
			ResultSet rs = null;
			try{
				rs = OrsegupsContratoUtils.selectTable(sql, PersistEngine.getConnection("SIGMA90"));
				if (rs!= null){
					retorno[0] =  rs.getString(1);
					retorno[1] =  rs.getString(2);
					retorno[2] =  rs.getString(3);
				}
				return retorno;
			}catch(Exception e){
				return retorno;
			}
		}
		return retorno;
		
	}
	
	/**
	 * retorna o maior numero de partição existente em uma conta sigma.
	 * @param cdConta - código da conta Ex.: 06C1
	 * @param cftv - true -> procurar nas contas de CFTV, false -> Procurar somente nas contas de monitoramento eletrônico.
	 * @return
	 */
	public static String retornaMaxParticao(String cdConta, boolean cftv){
		String retorno = null;
		
		if (cdConta != null && !cdConta.isEmpty()){
			
			
			String sql;
			
			if (cftv){
				sql = "select max(particao) from dbcentral where id_central= '"+cdConta+"' and cast(particao as integer) >= 98";
			}else{
				sql = "select max(particao) from dbcentral where id_central= '"+cdConta+"' and cast(particao as integer) < 98";
			}
			
			ResultSet rs = null;
			try{
				rs = OrsegupsContratoUtils.selectTable(sql, PersistEngine.getConnection("SIGMA90"));
				if (rs!= null){
					retorno =  rs.getString(1);
					
				}
				return retorno;
			}catch(Exception e){
				return retorno;
			}
		}
		return retorno;
		
	}
	
	/**
	 * preenche os zeros faltantes a esquerda do numero da partição 
	 * @param particao
	 * @return
	 */
	public static String normalizaParticao(Long particao){
		String retorno ="";
		try{
			retorno = NeoUtils.safeOutputString(particao);
			while (retorno.length() < 3 ){
				retorno = "0" + retorno;
			}
			
		}catch(Exception e){
			retorno = "";
		}
		return retorno;
		
	}
	
	public static void main(String args[]){
		System.out.println(normalizaParticao(1L));
		System.out.println(normalizaParticao(99L));
		System.out.println(normalizaParticao(100L));
	}
	
	
	public static List<String> getTodasContasUsadas(Long empSigma){
		List<String> retorno = new ArrayList<String>();
		PreparedStatement pst = null;
		Connection conn = OrsegupsUtils.getConnection("SIGMA90");
		ResultSet rs = null;
		
		StringBuilder query = new StringBuilder();
		
		query.append("SELECT id_central FROM DBCENTRAL WHERE id_empresa = ?  "); 
		
		try{
			pst = conn.prepareStatement(query.toString());
			pst.setLong(1, empSigma);
			
			rs = pst.executeQuery();
			
			while (rs.next()){
				retorno.add(rs.getString("id_central"));
			}
			
			return retorno;
		}catch(Exception e){
			System.out.println("getTodasContasUsadas - Erro ao consultar todas contas sigma utilizadas");
			e.printStackTrace();
			return null;
		}finally{
			OrsegupsUtils.closeConnection(conn, pst, rs);
		}	
	}


	public static List<String> getRangeContasSigma(){
		List<String> retorno = new ArrayList<String>();
		PreparedStatement pst = null;
		Connection conn = OrsegupsUtils.getConnection("");
		ResultSet rs = null;
		
		StringBuilder query = new StringBuilder();
		
		query.append(" SELECT seqCod FROM D_FGCRangeContasSigma "); 
		
		try{
			pst = conn.prepareStatement(query.toString());
			
			rs = pst.executeQuery();
			
			while (rs.next()){
				retorno.add(rs.getString("seqCod"));
			}
			
			return retorno;
		}catch(Exception e){
			System.out.println("getRangeContasSigma - Erro ao consultar range de contas sigma");
			e.printStackTrace();
			return null;
		}finally{
			OrsegupsUtils.closeConnection(conn, pst, rs);
		}	
	}


}
