package com.neomind.fusion.custom.orsegups.presenca.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.util.NeoUtils;


public class EscalaVO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private List<HorarioVO> horarios;
	private Long codigoEscala;
	private Long codigoTurma;
	private Long grupo;
	private String descricao = "";
	private String descricaoHorarios = "";
	private Boolean considerarEscala = Boolean.TRUE;
	private String tipTur;
	
	

	public List<HorarioVO> getHorarios()
	{
		if (considerarEscala)
		{
			return horarios;
		}
		else 
		{
			return new ArrayList<HorarioVO>();
		}
	}

	public void setHorarios(List<HorarioVO> horarios)
	{
		this.horarios = horarios;
	}
	
	

	@Override
	public String toString() {

		String horarioTexto = "";
		String separador = "";

		if(this.horarios != null && considerarEscala){
			for(HorarioVO  hor : horarios)
			{
				GregorianCalendar dataIni = hor.getDataInicial();
				GregorianCalendar dataFim = hor.getDataFinal();

				horarioTexto += separador + NeoUtils.safeDateFormat(dataIni, "HH:mm") + "-" + 
						NeoUtils.safeDateFormat(dataFim, "HH:mm");
				separador = " / ";
			}
		}
		
		if(horarioTexto.isEmpty()){
			horarioTexto = "Sem Escala";
		}
		if (!considerarEscala)
		{
			horarioTexto = "N/A";
		}

		return horarioTexto;
	}
	
	public EscalaVO clone()
	{
		EscalaVO escala = new EscalaVO();
		List<HorarioVO> horarios = new ArrayList<HorarioVO>();
		
		escala.setDescricao(this.getDescricao());
		escala.setConsiderarEscala(this.considerarEscala);
		if (this.horarios != null)
		{
			for (HorarioVO horario : this.horarios)
			{
				horarios.add(horario);
			}
			escala.setHorarios(horarios);
		}
		
		return escala;
	}

	public String getDescricao()
	{
		return descricao;
	}

	public void setDescricao(String descricao)
	{
		this.descricao = descricao;
	}

	public Long getCodigoEscala()
	{
		return codigoEscala;
	}

	public void setCodigoEscala(Long codigoEscala)
	{
		this.codigoEscala = codigoEscala;
	}

	public Long getCodigoTurma()
	{
		return codigoTurma;
	}

	public void setCodigoTurma(Long codigoTurma)
	{
		this.codigoTurma = codigoTurma;
	}

	public Boolean isConsiderarEscala()
	{
		return considerarEscala;
	}

	public void setConsiderarEscala(Boolean considerarEscala)
	{
		this.considerarEscala = considerarEscala;
	}

	public String getDescricaoHorarios()
	{
		return descricaoHorarios;
	}

	public void setDescricaoHorarios(String descricaoHorarios)
	{
		this.descricaoHorarios = descricaoHorarios;
	}

	public Long getGrupo()
	{
		return grupo;
	}

	public void setGrupo(Long grupo)
	{
		this.grupo = grupo;
	}

	public String getTipTur()
	{
		return tipTur;
	}

	public void setTipTur(String tipTur)
	{
		this.tipTur = tipTur;
	}

	
	

	
}