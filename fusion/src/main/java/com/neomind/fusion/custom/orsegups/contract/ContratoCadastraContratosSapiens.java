package com.neomind.fusion.custom.orsegups.contract;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.adapter.CadastraCentroCusto;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.adapter.casvig.GeraDadosIniciais;
import com.neomind.fusion.workflow.adapter.casvig.OrsegupsContratoUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoCalendarUtils;
import com.neomind.util.NeoUtils;

import edu.emory.mathcs.backport.java.util.Arrays;

/**
 * Cadastro dados gerais contratos sapiens
 * 
 * @author daina.souza
 *
 */
public class ContratoCadastraContratosSapiens implements AdapterInterface
{
	String sqlNextKeyContrato = null;
	String sqlNextKeyPosto = null;
	Long tipoAlteracao = 0L;
	String debugline = "";

	/**
	 * Armazena os codigos dos postos para fazer "rollback" caso haja alguma exeption no fusion. assim
	 * evita duplicação de posto
	 */
	private List<String> postosInseridos;
	long numctr;

	private static Long CONTRATO_NOVO_CLIENTE_ELETRONICA = (long) 1; //CLIENTE JÁ EXISTENTE
	private static Long CONTRATO_NOVO_CLIENTE_HUMANAS = (long) 2; //CLIENTE JÁ EXISTENTE
	private static Long CONTRATO_NOVO_ELETRONICA = (long) 3; //TUDO NOVO
	private static Long CONTRATO_NOVO_HUMANAS = (long) 4; //TUDO NOVO
	private static Long ALTERACAO_TERMO_ADITIVO = (long) 5; //JÁ EXISTE NO SAPIENS

	private static String nomeFonteDados = "SAPIENS";

	private static final Log log = LogFactory.getLog(ContratoCadastraContratosSapiens.class);

	@SuppressWarnings("unchecked")
	@Override
	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		String modeloContrato = NeoUtils.safeOutputString(wrapper.findValue("modelo.codigo"));

		Long key = (new GregorianCalendar()).getTimeInMillis();
		String telefone1 = (String) wrapper.findValue("novoCliente.telefone1");
		String telefone2 = (String) wrapper.findValue("novoCliente.telefone2");

		String sUsu_PerReg = "";
		if (NeoUtils.safeBoolean(wrapper.findValue("pertenceRegionalPosto")))
		{
			sUsu_PerReg = "S";
		}
		else
		{
			sUsu_PerReg = "N";
		}

		if (telefone1.length() > 14)
		{
			throw new WorkflowException(
					"Telefone 1 contem mais caracteres que o permitido, por gentileza preencher com no máximo 14 caracteres!");
		}
		else if (telefone2.length() > 14)
		{
			throw new WorkflowException(
					"Telefone 2 contem mais caracteres que o permitido, por gentileza preencher com no máximo 14 caracteres!");
		}

		if (wrapper.findValue("dadosGeraisContrato.representante") == null)
		{
			throw new WorkflowException("Selecione o representante do contrato.");
		}
		if (NeoUtils.safeBoolean(wrapper.findValue("adereProtecaoGarantida")))
		{
			BigDecimal valorProtecaoG = wrapper.findValue("valorProtecaoG") != null
					? (BigDecimal) wrapper.findValue("valorProtecaoG")
					: new BigDecimal("0");

			if (valorProtecaoG.compareTo(new BigDecimal("29")) != 0
					&& valorProtecaoG.compareTo(new BigDecimal("39")) != 0)
			{
				throw new WorkflowException(
						"O valor do seguro proteção garantida, precisa ser 29 ou 39. Em caso de dúvidas, entre em contato com o departamento comercial privado.");
			}
		}

		postosInseridos = new ArrayList<String>();
		numctr = 0L;
		boolean atividadeComercial = origin.getActivityName().equals("Validação Contrato") ? true
				: false;
		boolean atividadeComercialEscala1 = origin.getActivityName()
				.equals("Validar Contrato - Escala 1") ? true : false;
		boolean atividadeComercialEscala2 = origin.getActivityName()
				.equals("Validar Contrato - Escala 2") ? true : false;
		boolean atividadeComercialEscalaFinal = origin.getActivityName()
				.equals("Validar Contrato - Escala Final") ? true : false;

		atividadeComercial = atividadeComercial || atividadeComercialEscala1 || atividadeComercialEscala2
				|| atividadeComercialEscalaFinal;

		tipoAlteracao = (Long) wrapper.findValue("movimentoAlteracaoContrato.codTipo");

		/*
		 * cadastra o cliente novo
		 */
		Long codMov = NeoUtils
				.safeLong(NeoUtils.safeOutputString(wrapper.findValue("movimentoContratoNovo.codTipo")));
		Long tipAlt = 0L;
		if (codMov == 5)
			tipAlt = NeoUtils.safeLong(
					NeoUtils.safeOutputString(wrapper.findValue("movimentoAlteracaoContrato.codTipo")));
		NeoObject novoCliente = (NeoObject) wrapper.findValue("buscaNomeCliente");
		String codCli = "";
		String tipoCli = "";
		if (wrapper.findValue("novoCliente.tipocliente") != null)
			tipoCli = NeoUtils.safeString(
					NeoUtils.safeOutputString(wrapper.findValue("novoCliente.tipocliente.tipo")));

		Long codEmpresa = (Long) wrapper.findField("empresa.codemp").getValue();
		Long codFilial = (Long) wrapper.findField("empresa.codfil").getValue();

		try
		{

			/*
			 * se for cliente novo ou alteração de CNPJ
			 * deve-se cadastrar o novo cliente
			 */
			if ((novoCliente == null && (codMov == 3 || codMov == 6 || codMov == 4))
					|| (novoCliente != null && codMov == 5 && tipAlt == 1))
			{
				if (tipoCli != null && tipoCli.equals("F"))
				{
					NeoObject tipoEmpresa = PersistEngine.getObject(
							AdapterUtils.getEntityClass("FGCClientesTipoEmpresa"),
							new QLEqualsFilter("tipo", (long) 1));
					wrapper.findField("novoCliente.tipoEmpresa").setValue(tipoEmpresa);
				}
				else if (tipoCli != null && tipoCli.equals("J"))
				{
					NeoObject tipoEmpresa = PersistEngine.getObject(
							AdapterUtils.getEntityClass("FGCClientesTipoEmpresa"),
							new QLEqualsFilter("tipo", (long) 1));
					wrapper.findField("novoCliente.tipoEmpresa").setValue(tipoEmpresa);
				}

				NeoObject noCliente = (NeoObject) wrapper.findField("novoCliente").getValue();

				// validação de tamanho de campos
				EntityWrapper wCliente = new EntityWrapper(noCliente);
				try
				{
					ContratoUtils.validaTamanhoCampo("Razão Social",
							NeoUtils.safeOutputString(wCliente.findValue("")), 100);
					ContratoUtils.validaTamanhoCampo("Nome Fantasia",
							NeoUtils.safeOutputString(wCliente.findValue("")), 50);
				}
				catch (Exception e)
				{
					throw new WorkflowException("Erro validação de campos: " + e.getMessage());
				}

				NeoObject noEndereco = wCliente.findGenericValue("enderecoCliente");
				EntityWrapper wEndereco = new EntityWrapper(noEndereco);

				NeoObject noEndCobranca = AdapterUtils
						.createNewEntityInstance("FGCListaEndCobClienteSapiens");
				EntityWrapper wEndCobranca = new EntityWrapper(noEndCobranca);

				wEndCobranca.setValue("cep", wEndereco.findGenericValue("cep"));
				wEndCobranca.setValue("endereco", wEndereco.findGenericValue("endereco"));
				wEndCobranca.setValue("numero", wEndereco.findGenericValue("numero"));
				wEndCobranca.setValue("complemento", wEndereco.findGenericValue("complemento"));
				wEndCobranca.setValue("bairro", wEndereco.findGenericValue("bairro"));
				wEndCobranca.setValue("estadoCidade", wEndereco.findGenericValue("estadoCidade"));
				wEndCobranca.setValue("usarComoDoContrato", true);

				wCliente.findField("endsCob").addValue(noEndCobranca);

				NeoObject noDadosContrato = (NeoObject) wrapper.findField("dadosGeraisContrato")
						.getValue();
				//Chama o SID para alterar a empresa e a filial
				ContractSIDClient.sidChangeEmpFil(NeoUtils.safeOutputString(codEmpresa),
						NeoUtils.safeOutputString(codFilial));

				//Cadastra um novo cliente para a empresa selecionada

				//verifica se já o cliente já foi gravado nessa tarefa

				boolean bypassTravaCadastroCliente = (ParametrizacaoFluxoContratos
						.findParameter("bypassTravaCadastroCliente").equals("1") ? true : false); //manter até a regra estar 100% corrigida
				if (bypassTravaCadastroCliente)
				{
					codCli = ContractSIDClient.sidClient(wrapper, noCliente,
							NeoUtils.safeOutputString(codEmpresa), NeoUtils.safeOutputString(codFilial),
							true);
				}
				else
				{
					System.out.println("[FLUXO CONTRATO] - tarefa codigo: " + activity.getCode());
					QLGroupFilter gp = new QLGroupFilter("AND");
					gp.addFilter(new QLEqualsFilter("tarefa", activity.getCode()));
					NeoObject controleCadCli = PersistEngine
							.getObject(AdapterUtils.getEntityClass("FGCControleCadCli"), gp);
					if (controleCadCli != null)
					{
						System.out.println("[FLUXO CONTRATO] - Cliente econtrado");
						EntityWrapper wObj = new EntityWrapper(controleCadCli);
						codCli = NeoUtils.safeString((String) wObj.findValue("codcli"));
					}
					else
					{
						System.out.println("[FLUXO CONTRATO] - Cliente não econtrado");
						if (!atividadeComercial)
						{
							codCli = ContractSIDClient.sidClient(wrapper, noCliente,
									NeoUtils.safeOutputString(codEmpresa),
									NeoUtils.safeOutputString(codFilial), false);
							NeoObject reg = AdapterUtils.createNewEntityInstance("FGCControleCadCli");
							EntityWrapper wReg = new EntityWrapper(reg);
							wReg.setValue("tarefa", activity.getCode());
							wReg.setValue("codcli", NeoUtils.safeLong(codCli));
							PersistEngine.persist(reg);
						}

					}

					if (atividadeComercial)
					{
						codCli = ContractSIDClient.sidClient(wrapper, noCliente,
								NeoUtils.safeOutputString(codEmpresa),
								NeoUtils.safeOutputString(codFilial), true);
					}
				}

				if (codCli != null)
				{
					NeoObject oDebitoDados = (NeoObject) wrapper
							.findValue("dadosGeraisContrato.debitoDado");
					EntityWrapper wDadosDebito = new EntityWrapper(oDebitoDados);

					String codBan = NeoUtils.safeOutputString(wDadosDebito.findValue("banco.codigo"));
					String portador = NeoUtils
							.safeOutputString(wDadosDebito.findValue("banco.portador"));
					String codAge = NeoUtils.safeOutputString(wDadosDebito.findValue("agecia"));
					String ccbCli = NeoUtils.safeOutputString(wDadosDebito.findValue("contacorrente"));

					ContractSIDClient.sidClientDefinitions(noDadosContrato, codCli, 0, portador,
							NeoUtils.safeOutputString(codEmpresa), NeoUtils.safeOutputString(codFilial),
							codBan, codAge, ccbCli);

					//ContractSIDClient.sidClientDefinitions(noDadosContrato, codCli, 0, , NeoUtils.safeOutputString(codFilial));
					ContractSIDClient.sidClientContact(noCliente, codCli,
							NeoUtils.safeOutputString(codEmpresa), NeoUtils.safeOutputString(codFilial));
				}
				System.out.println("[FLUXO CONTRATOS] - Debug-0");

				Collection<NeoObject> objClienteSapiens = (Collection<NeoObject>) PersistEngine
						.getObjects(AdapterUtils.getEntityClass("SAPIENS_Clientes"),
								new QLEqualsFilter("codcli", NeoUtils.safeLong(codCli)));
				//Collection<NeoObject> objClienteSapiens = (Collection<NeoObject>)PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENS_Clientes"), new QLEqualsFilter("codcli", 30413l));
				if (objClienteSapiens != null && objClienteSapiens.size() > 0)
				{
					for (NeoObject obj : objClienteSapiens)
					{
						wrapper.setValue("buscaNomeCliente", obj);
						wrapper.findField("novoCliente.codigoCliente").setValue(codCli);
					}
				}
				else if (objClienteSapiens == null)
				{
					throw new WorkflowException(
							"Erro ao consultar o cadastro no eform SAPIENS_Clientes para o cliente: "
									+ codCli + " retornou null. Entre em contato com a TI.");
				}
				else
				{
					throw new WorkflowException(
							"Erro ao consultar o cadastro no eform SAPIENS_Clientes para o cliente: "
									+ codCli + ", 0 resultados encontrados. Entre em contato com a TI.");
				}
			}

			if (codCli != null && codCli.equals(""))
				codCli = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.codcli"));

			String codigoAgrupadorContrato = "";
			boolean possuiCodigo = NeoUtils
					.safeBoolean(wrapper.findValue("dadosGeraisContrato.possuiCodigoAgrupador"), false);
			System.out.println("[FLUXO CONTRATOS] - Debug- Atividade Comercial? " + atividadeComercial);
			System.out.println("[FLUXO CONTRATOS] - Debug- possui codigo? " + possuiCodigo);
			if (atividadeComercial && possuiCodigo)
			{
				boolean codigoExistente = NeoUtils.safeBoolean(
						wrapper.findValue("dadosGeraisContrato.utilizarCodigoExistente"), false);
				if (codigoExistente)
				{
					codigoAgrupadorContrato = NeoUtils.safeOutputString(
							wrapper.findValue("dadosGeraisContrato.codigoAgrupador.usu_ctragr"));
				}
				else
				{
					String numeroContrato = NeoUtils
							.safeOutputString(wrapper.findValue("numContrato.usu_numctr"));
					if (numeroContrato != null)
					{
						numctr = Long.parseLong(numeroContrato);
					}

					if (numeroContrato.equals(""))
					{
						numeroContrato = NeoUtils.safeOutputString(
								wrapper.findValue("dadosGeraisContrato.numeroContratoSapiens"));
						numctr = Long.parseLong(numeroContrato);
					}
					codigoAgrupadorContrato = "AGR" + numeroContrato;
				}
			}
			else
			{
				if (!atividadeComercial && validaCodigoAgrupadorContrato(wrapper, codCli))
				{
					wrapper.findField("dadosGeraisContrato.possuiCodigoAgrupador").setValue(true);
				}
				else if (atividadeComercial)
				{
					String numeroContrato = NeoUtils
							.safeOutputString(wrapper.findValue("numContrato.usu_numctr"));
					if (numeroContrato.equals(""))
						numeroContrato = NeoUtils.safeOutputString(
								wrapper.findValue("dadosGeraisContrato.numeroContratoSapiens"));
					numctr = Long.parseLong(numeroContrato);
					codigoAgrupadorContrato = "AGR" + numeroContrato;
				}
			}

			System.out.println("[FLUXO CONTRATOS] - Debug-1");
			if (!atividadeComercial && (codMov > 4 && codMov != 6) && tipAlt != 1)
			{
				System.out.println("[FLUXO CONTRATOS] - Debug-2");
				//se for alteração não precisa atualizar nada no inicio do processo, apenas quando passar pelo comercial
				return;
			}

			/*
			 * se for alteração de CNPJ não deve atualizar nada, visto que o contrato antigo será
			 * inativado
			 * e criado um novo
			 */
			System.out.println("[FLUXO CONTRATOS] - Preparando dados para o cadastro do contrato");
			NeoUser usuarioLogado = PortalUtil.getCurrentUser();
			Long codUsuariosapiens = 0L;
			try
			{
				System.out.println("[FLUXO CONTRATOS] - Debug-3");
				codUsuariosapiens = ContratoUtils.retornaCodeUsuarioSapiens(usuarioLogado.getCode());
				System.out.println("[FLUXO CONTRATOS] - Debug-3.1");
			}
			catch (Exception e)
			{
				System.out.println("[FLUXO CONTRATOS] - Debug-3.2");
				codUsuariosapiens = 1L;
			}
			Boolean cadastraNovoContrato = false;
			Boolean origemContratoDuplicado = false;
			Boolean origemPostoDuplicado = false;

			//Long codEmpresa = (Long) wrapper.findField("empresa.codemp").getValue();
			//Long codFilial = (Long) wrapper.findField("empresa.codfil").getValue();
			Long movimentoContrato = (Long) wrapper.findValue("movimentoContratoNovo.codTipo");
			Long numContrato = null;
			Long numContratoOrigem = null;

			NeoObject oDadosGeraisContrato = (NeoObject) wrapper.findValue("dadosGeraisContrato");
			EntityWrapper wDadosContrato = new EntityWrapper(oDadosGeraisContrato);

			Long usu_codemp = codEmpresa;
			Long usu_codfil = codFilial;

			Long tipoEmpresa = null; //publico ou privado
			Long usu_codcli = null;
			System.out.println("[FLUXO CONTRATOS] - Debug-3.3 " + "\n codUsuariosapiens= "
					+ codUsuariosapiens + "\n movimentoContrato= " + movimentoContrato
					+ "\n codEmpresa= " + codEmpresa + "\n codFilial= " + codFilial);
			//3 e 4
			if (movimentoContrato.equals(CONTRATO_NOVO_ELETRONICA)
					|| movimentoContrato.equals(CONTRATO_NOVO_HUMANAS) || movimentoContrato.equals(6l))
			{
				System.out.println("[FLUXO CONTRATOS] - Debug-4");
				tipoEmpresa = (Long) wrapper.findValue("novoCliente.tipoEmpresa.tipo");
				usu_codcli = (Long) wrapper.findValue("buscaNomeCliente.codcli");
			}
			else
			{
				System.out.println("[FLUXO CONTRATOS] - Debug-5");
				tipoEmpresa = (Long) wrapper.findField("buscaNomeCliente.tipemc").getValue();
				usu_codcli = (Long) wrapper.findField("buscaNomeCliente.codcli").getValue();
			}
			System.out.println("[FLUXO CONTRATOS] - Debug-3.4");
			if (movimentoContrato.equals(ALTERACAO_TERMO_ADITIVO))
			{

				System.out.println("[FLUXO CONTRATOS] - Debug-6");
				Long cliente = (Long) wrapper.findField("buscaNomeCliente.codcli").getValue();

				Long codFil = null;

				NeoObject contratoExt = (NeoObject) wrapper.findField("numContrato").getValue();
				if (contratoExt != null)
				{
					System.out.println("[FLUXO CONTRATOS] - Debug-7");
					EntityWrapper wContrato = new EntityWrapper(contratoExt);
					numContrato = numctr = (Long) wrapper.findField("numContrato.usu_numctr").getValue();

					numContratoOrigem = numContrato;
					codFil = (Long) wContrato.findField("usu_codfil").getValue();

				}

				/*
				 * quando passar pela validação...
				 */
				if (atividadeComercial)
				{
					System.out.println("[FLUXO CONTRATOS] - Debug-8");
					if (tipoAlteracao == 1)
					{
						System.out.println("[FLUXO CONTRATOS] - Debug-9");
						origemContratoDuplicado = true;
						origemPostoDuplicado = true;
					}
					else if (tipoAlteracao == 2)
					{
						System.out.println("[FLUXO CONTRATOS] - Debug-10");
						CadastraCentroCusto.updateRazaoSocial(wrapper);

					}

					if (tipoAlteracao == 7 || tipoAlteracao == 8)
					{
						System.out.println("[FLUXO CONTRATOS] - Debug-11");
						origemContratoDuplicado = true;
						origemPostoDuplicado = true;
					}
				}
			}
			else
			{
				System.out.println("[FLUXO CONTRATOS] - Debug-12");
				origemContratoDuplicado = true;
			}

			//campo controle - se houver erro nessa classe e já tiver cadastrado o contrato - não cadastra novamente.
			Boolean contratoJaCadastrato = wDadosContrato.findValue("contratoCadastratoSapiens") != null
					? (Boolean) wDadosContrato.findValue("contratoCadastratoSapiens")
					: false;
			System.out.println("[FLUXO CONTRATOS] - Debug-3.5 " + "\n origemContratoDuplicado= "
					+ origemContratoDuplicado + "\n contratoJaCadastrato= " + contratoJaCadastrato);
			//Gera um numero de contrato somente nos casos em que nao foi selecionado um contrato já existente
			//ou quando se trata de uma alteração de CNPJ
			if ((origemContratoDuplicado && !contratoJaCadastrato) || (tipAlt == 1))
			{
				System.out.println("[FLUXO CONTRATOS] - Debug-13");
				numContrato = numctr = geraNumeroContratoNovoSapiens(codEmpresa, tipoEmpresa);
				sqlNextKeyContrato = geraSqlNextKeyContrato(codEmpresa, tipoEmpresa);

				cadastraNovoContrato = true;
				if (wDadosContrato.findValue("numeroContratoSapiens") != null)
				{
					System.out.println("[FLUXO CONTRATOS] - Debug-14");
					cadastraNovoContrato = false;
				}
				else
				{
					System.out.println("[FLUXO CONTRATOS] - Debug-15");
					cadastraNovoContrato = true;
				}
			}

			if (contratoJaCadastrato)
			{
				System.out.println("[FLUXO CONTRATOS] - Debug-16");
				numContrato = numctr = (Long) wDadosContrato.findField("numeroContratoSapiens")
						.getValue();
			}

			String usu_emactr = (String) wDadosContrato.findField("emailContato").getValue();
			String usu_clifon = (String) wDadosContrato.findField("foneCOntato").getValue();
			String usu_clicon = (String) wDadosContrato.findField("pessoaContatoComercial").getValue();

			if (usu_clicon != null && usu_clicon.length() > 40)
			{
				throw new WorkflowException(
						"O campo (Nome da Pessoa do Contato Comercial) pode ter no máximo 40 caracteres. (Limitação do Sapiens)");
			}

			GregorianCalendar usu_datemi = (GregorianCalendar) wDadosContrato
					.findField("dataEmissaoContrato").getValue();

			String usu_numofi = (String) wDadosContrato.findField("numeroOficialContrato").getValue();

			Long usu_regctr = NeoUtils.safeLong(
					NeoUtils.safeOutputString(wDadosContrato.findValue("regional.usu_codreg")));
			//Long usu_regctr = NeoUtils.safeLong(NeoUtils.safeOutputString(wDadosContrato.findValue("representante.usu_codreg"))); // não deve vir mais preenchido

			String usu_objctr = (String) wDadosContrato.findField("ObjetoDoContrato").getValue();
			String usu_obscom = (String) wDadosContrato.findField("observacaoComerciais").getValue();
			usu_obscom = usu_obscom != null ? usu_obscom.replaceAll("<br>|<br />|<p>|</p>", "") : "";

			Long usu_codrep = NeoUtils.safeLong(
					NeoUtils.safeOutputString(wDadosContrato.findValue("representante.codrep")));

			String usu_tnsser = NeoUtils.safeOutputString(wDadosContrato.findValue("transacao.codtns"));

			//usu_pervig o cálculo que deve ser feito é simples (Regra = USU_T160CTR.USU_IniVig + ( ( USU_T160CTR.USU_PerVig /12 ) * 365.25 ))
			//Long peridoMaximoVigencia = (long) ((long) inicio_vigencia_contrato.get(GregorianCalendar.MONTH) + ((periodoVigencia / 12) * 365.25));
			NeoObject oUsuPerVigMax = (NeoObject) wDadosContrato.findValue("peridoMaximoVigencia2");
			Long usu_pervig = 0L;
			if (oUsuPerVigMax != null)
			{
				EntityWrapper wUsuPerVigMax = new EntityWrapper(oUsuPerVigMax);
				usu_pervig = (Long) wUsuPerVigMax.findValue("meses");
			}
			ContratoLogUtils.logInfo("[" + key + "]usu_pervig=" + usu_pervig);

			//GregorianCalendar usu_inivig = (GregorianCalendar) wDadosContrato.findField("peridoVigencia").getValue();
			GregorianCalendar usu_inivig = (GregorianCalendar) wDadosContrato.findField("inivig")
					.getValue();

			GregorianCalendar usu_datini = (GregorianCalendar) wDadosContrato
					.findField("inicioFaturamentoContrato").getValue();

			GregorianCalendar usu_datfim = (wDadosContrato.findValue("fimFaturamentoContrato") != null
					? (GregorianCalendar) wDadosContrato.findValue("fimFaturamentoContrato")
					: NeoCalendarUtils.stringToDate("31/12/1900"));

			Long usu_diabas = NeoUtils
					.safeLong(NeoUtils.safeOutputString(wDadosContrato.findValue("diaBaseFatura")));
			if (usu_diabas == null || usu_diabas <= 0)
				usu_diabas = 5L;
			Long usu_diafix = NeoUtils.safeLong(NeoUtils
					.safeOutputString(wDadosContrato.findValue("diaFixoVencimento.diaVencimento")));
			if (usu_diafix == null || usu_diafix <= 0)
				usu_diafix = 5L;
			String usu_codfcr = NeoUtils.safeOutputString(wDadosContrato.findValue("indice.codmoe"));

			String usu_codcpg = NeoUtils
					.safeOutputString(wDadosContrato.findValue("condicaoPagamento.codcpg"));

			Long usu_codfpg = NeoUtils.safeLong(
					NeoUtils.safeOutputString(wDadosContrato.findValue("formaPagamento.codfpg")));

			//String contratoComposto = (Boolean) wDadosContrato.findField("contratoComposto").getValue() ? "S" : "F";

			String usu_eximed = NeoUtils.safeBoolean(wDadosContrato.findValue("exigeMedicao"), false)
					? "S"
					: "N";

			Long usu_prvpos = NeoUtils
					.safeLong(NeoUtils.safeOutputString(wDadosContrato.findValue("numPostosPrevistos")));
			if (usu_prvpos == null)
				usu_prvpos = 1L;
			Long usu_efepos = NeoUtils
					.safeLong(NeoUtils.safeOutputString(wDadosContrato.findValue("numPostosEfetivos")));
			if (usu_efepos == null)
			{
				usu_efepos = usu_prvpos;
				wDadosContrato.setValue("numPostosEfetivos", usu_prvpos);
			}

			//String contratoLiberado = (Boolean) wDadosContrato.findField("contratoLiberado").getValue() ? "S" : "F";

			// Segundo Leonardo.berka será gravado sempre 1 (por Número de Funcionários)
			// No fusion há uma lista que diverge em relação ao sapiens CETipoContrato
			Long usu_tipctr = 1L;//NeoUtils.safeLong(NeoUtils.safeOutputString(wDadosContrato.findValue("tipoDoContrato.codigoProduto")));

			Long usu_serctr = (long) 0;
			if (wDadosContrato.findValue("tipoServicoContrato.usu_serctr") != null)
			{
				usu_serctr = NeoUtils.safeLong(NeoUtils
						.safeOutputString(wDadosContrato.findValue("tipoServicoContrato.usu_serctr")));
			}

			String usu_impuni = ""; //S ou N - indicativo se imprime o preço unitario da nota fiscal - ok
			Long usu_seqcob = new Long(1); //sequencia do endereco de cobranca  
			GregorianCalendar usu_fimvig = NeoCalendarUtils.stringToDate("31/12/1900"); // fim da vigencia do contrato  
			GregorianCalendar usu_datuft = NeoCalendarUtils.stringToDate("31/12/1900"); // data do ultimo faturamento efetuado ??
			Long usu_filnfv = new Long(0); //cod filial nota fiscal saida  
			String usu_codsnf = ""; //serie ultima nota fiscal gerada  
			Long usu_numnfv = new Long(0); //num ultima nota fiscal gerada  
			GregorianCalendar usu_ultrea = NeoCalendarUtils.stringToDate("31/12/1900"); // ultimo reajuste do contrato ?? null em processos novos??
			String usu_sitctr = "A"; //sempre A para novo contrato - ok
			Long usu_codmot = new Long(0);//cod motivo alteracao ?? passar 0 para novos? adicionar no fusion e021mot??
			String usu_obsmot = ""; //obs motivo situacao  
			String usu_obsctr = "Serviços Prestados em suas instalações em "
					+ retornaCidadePostoAtivo(wrapper); //No insert valorizar com mensagem padrão "Serviços Prestados em suas instaçãoes em "xMunicioPrestacao= cidade do posto" postosContrato.enderecoEfetivo.estadoCidade.cidade.nomcid
			String usu_ctrcom = "N"; //S ou N - contrato composto - ok
			Long usu_usuger = codUsuariosapiens; // usuario gerador registro ?? code usuario no fusion??

			GregorianCalendar usu_datger = new GregorianCalendar(); //data geracao registro - data atual?
			Long usu_horger = (long) usu_datger.get(Calendar.HOUR); //hora geracao registro 

			GregorianCalendar usu_datalt = usu_datger; // data alteracao - atual?
			Long usu_horalt = new Long(0); // hora alteracao - atual?
			GregorianCalendar usu_datmot = NeoCalendarUtils.stringToDate("31/12/1900"); //data motivo situacao cliente - data atual??
			Long usu_hormot = new Long(0); //hora motivo situacao cliente - data atual??

			Long usu_usumot = new Long(0); //usuario respons. motivo situacao do cliente - usuario do fusion ??
			Long usu_usucad = codUsuariosapiens; // usuario responsavel pelo cadasrto ?? usuario no fusion??
			GregorianCalendar usu_datcad = usu_datger; // data cadastro - atual?
			Long usu_horcad = usu_horger;// hora cadastro - atual?
			Long usu_usualt = codUsuariosapiens; //usuario respons. alteracao - usuario do fusion ??

			String usu_obsfin = ""; //observacoes financeiro - adicionar no fusion??
			Long usu_dscfin = new Long(0); // % desconto financeiro contrato - adicionar no fusion??

			Long usu_fatanu = new Long(0); //mes para faturamento - adicionar no fusion??
			String usu_ctrcor = "N"; //S ou N - contrato é contesia - ok

			Boolean contratoSuspender = NeoUtils
					.safeBoolean(wDadosContrato.findValue("suspenderFaturamento"), true);
			String usu_susfat = contratoSuspender ? "S" : "N"; //S ou N - suspender faturamento
			String usu_libfat = "N";

			//solicitado para ser padrao N via tarefa simples 666371 
			String usu_libfatContrato = "N"; //NeoUtils.safeBoolean(wDadosContrato.findValue("contratoLiberado"))? "S":"N";

			GregorianCalendar usu_datsus = contratoSuspender ? new GregorianCalendar() : null; //data suspensao 
			Long usu_ususus = contratoSuspender ? codUsuariosapiens : new Long(0); //usuario suspensao
			GregorianCalendar usu_dfimsu = NeoCalendarUtils.stringToDate("31/12/1900");//data fin suspensao
			//if(true)throw new WorkflowException("Me Remova Exception");
			BigDecimal usu_periss = new BigDecimal(0.00); //% do ISS - do contrato é 0
			Long usu_empori = origemContratoDuplicado ? codEmpresa : new Long(0); //empresa contrato de origem
			Long usu_filori = origemContratoDuplicado ? codFilial : new Long(0); //filial contrato de origem

			Long usu_ctrori = origemContratoDuplicado ? numContratoOrigem : new Long(0); //SE DUPLICADO TRAZER O NUM ORIGEM DO CONTRATO DUPLICADO QUE SERÁ INATIVADO

			Long usu_numcgc = new Long(0); // cnpj prefeitura - adicionar no fusion??
			Long usu_coduni = new Long(0); // codigo unidade gestora - adicionar no fusion??

			//campo novo do sapiens - AGRUPADOR DE CONTRATOS
			String usu_ctragr = "";
			if (atividadeComercial)
			{
				usu_ctragr = codigoAgrupadorContrato;
			}

			//campos OBSERVACAO - CONTRATO E POSTO
			String usu_tipobs = "";
			String usu_txtobs = "";
			Long usu_usumov = codUsuariosapiens;
			GregorianCalendar usu_datmov = new GregorianCalendar();
			Long usu_hormov = null;
			String usu_codope = "";

			if (usu_numofi == null)
			{
				usu_numofi = "";
			}
			if (usu_obscom == null)
			{
				usu_obscom = "";
			}

			if (usu_inivig == null)
			{
				usu_inivig = NeoCalendarUtils.stringToDate("31/12/1900");
			}
			if (usu_datini == null)
			{
				usu_datini = NeoCalendarUtils.stringToDate("31/12/1900");
			}
			if (usu_datfim == null)
			{
				usu_datfim = NeoCalendarUtils.stringToDate("31/12/1900");
			}

			String usu_atuaso = "N"; //solicitado por thiago coelho em 13/01/2015 para que fosse fixo N 			

			//CADASTRA O CONTRATO
			System.out.println("[FLUXO CONTRATOS] - Cadastra o contrato? cadastraNovoContrato = "
					+ cadastraNovoContrato + " && !contratoJaCadastrato =" + !contratoJaCadastrato
					+ " -> " + (cadastraNovoContrato && !contratoJaCadastrato));
			if (cadastraNovoContrato && !contratoJaCadastrato)
			{
				System.out.println("[FLUXO CONTRATOS] - Debug-17");
				String veioConcorrenteString = "N";
				String nomeConcorrente = "";
				//preenche o objeto do contrato conforme o modelo de contrato.
				String objetoContrato = wrapper.findValue("modelo.modelo") != null
						? (String) wrapper.findValue("modelo.modelo")
						: "";

				if (objetoContrato != null)
				{
					wrapper.findField("dadosGeraisContrato.ObjetoDoContrato").setValue(objetoContrato);
					usu_objctr = objetoContrato;
				}
				boolean veioConcorrente = (boolean) wrapper
						.findField("dadosGeraisContrato.veioDeConcorrente").getValue();
				if (veioConcorrente)
				{
					veioConcorrenteString = "S";
					nomeConcorrente = (String) wrapper.findField("dadosGeraisContrato.nomeConcorrente")
							.getValue();
				}
				else
				{
					veioConcorrenteString = "N";
				}

				System.out.println("[FLUXO CONTRATOS] - Inserindo contrato no Sapiens para o cliente "
						+ usu_codcli);
				ContratoLogUtils.logInfo(
						"[" + key + "] Contrato Liberado para Faturamento? " + usu_libfatContrato);
				numContrato = numctr = cadastraDadosGeraisContratos_T160CTR(sqlNextKeyContrato,
						usu_impuni, usu_codemp, usu_codfil, numContrato, usu_numofi, usu_objctr,
						usu_datemi, usu_codcli, usu_seqcob, usu_codrep, usu_inivig, usu_fimvig,
						usu_datini, usu_datfim, usu_diabas, usu_diafix, usu_datuft, usu_filnfv,
						usu_codsnf, usu_numnfv, usu_codcpg, usu_ultrea, usu_sitctr, usu_codmot,
						usu_obsmot, usu_obsctr, usu_ctrcom, usu_eximed, usu_prvpos, usu_efepos,
						usu_codfpg, usu_usuger, usu_datger, usu_horger, usu_libfatContrato, usu_pervig,
						usu_usumot, usu_datmot, usu_hormot, usu_usucad, usu_datcad, usu_horcad,
						usu_usualt, usu_datalt, usu_horalt, usu_obscom, usu_obsfin, usu_dscfin,
						usu_tipctr, usu_clicon, usu_clifon, usu_codfcr, usu_fatanu, usu_ctrcor,
						usu_tnsser, usu_regctr, usu_susfat, usu_datsus, usu_ususus, usu_dfimsu,
						usu_periss, usu_emactr, usu_serctr, usu_empori, usu_filori, usu_ctrori,
						usu_numcgc, usu_coduni, usu_ctragr, usu_atuaso, activity, veioConcorrenteString,
						nomeConcorrente);

				if (numContrato == null || (numContrato != null && numContrato == 0))
				{
					throw new WorkflowException("Erro ao inserir contrato. Por favor tente novamente.");
				}

				wDadosContrato.findField("contratoCadastratoSapiens").setValue(true);
				wDadosContrato.findField("numeroContratoSapiens").setValue(numContrato);

				//OBSERVACAO - HISTÓRICO
				Long usu_seqobs = ContratoUtils.retornaSequenciaObservacaoContrato(usu_codemp,
						usu_codfil, numContrato);
				usu_tipobs = "A";
				if (origemContratoDuplicado && numContratoOrigem != null)
					usu_txtobs = "Duplicado a partir do contrato n. " + numContratoOrigem.toString();
				else
					usu_txtobs = "Cadastro do contrato";

				ContratoUtils.geraObservacao(usu_codemp, usu_codfil, numContrato, usu_seqobs, null, null,
						usu_tipobs, usu_txtobs, usu_usumov, usu_datmov, usu_hormov, usu_codope);
			}
			System.out.println("[FLUXO CONTRATOS] - Debug-18");
			if (atividadeComercial)
			{
				System.out.println("[FLUXO CONTRATOS] - Debug-19");
				usu_inivig = (GregorianCalendar) wDadosContrato.findField("inivig").getValue();
				usu_fimvig = (GregorianCalendar) wDadosContrato.findField("fimvig").getValue();

				usu_datini = (GregorianCalendar) wDadosContrato.findField("inicioFaturamentoContrato")
						.getValue();
				usu_datfim = (GregorianCalendar) wDadosContrato.findField("fimFaturamentoContrato")
						.getValue();

				if (usu_inivig == null)
				{
					usu_inivig = NeoCalendarUtils.stringToDate("31/12/1900");
				}
				if (usu_fimvig == null)
				{
					usu_fimvig = NeoCalendarUtils.stringToDate("31/12/1900");
				}
				if (usu_datini == null)
				{
					usu_datini = NeoCalendarUtils.stringToDate("31/12/1900");
				}
				if (usu_datfim == null)
				{
					usu_datfim = NeoCalendarUtils.stringToDate("31/12/1900");
				}

				System.out.println("[FLUXO CONTRATOS]- atualizando Contrato - " + numContrato
						+ ", Empresa " + codEmpresa + ", Filial " + codFilial + ", usu_datfim="
						+ ContratoUtils.retornaDataFormatoSapiens(usu_datfim));

				usu_datsus = new GregorianCalendar();
				if (!contratoSuspender)
				{
					usu_datsus = null;
				}

				if (movimentoContrato.equals(ALTERACAO_TERMO_ADITIVO))
				{ // reunião 21/05/2014 - sempre para alterações deixar contrato e posto liberados para faturamento por padrão.  
					usu_libfat = "S";
				}

				// Endereço de cobrança do contrato.
				Long usu_seqCob = 1L;
				Collection<NeoObject> enderecosCob = (Collection<NeoObject>) wrapper
						.findValue("novoCliente.endsCob");
				System.out.println(
						"[FLUXO CONTRATOS]- atualizando Contrato - Verficando endereço de cobrança à atrelar ao contrato. ");
				if (enderecosCob != null)
				{

					//validação apenas
					for (NeoObject no : enderecosCob)
					{
						EntityWrapper ew = new EntityWrapper(no);
						/*if (NeoUtils.safeOutputString(ew.findValue("seqCob")).trim().equals(""))
						{
							throw new WorkflowException(
									"A Sequencia do endereço de cobrança não pode ser vazia nem 0");
						}*/

					}
					ContratoLogUtils.logInfo("[" + key + "]Gravando endereços de Cobranda ");
					ContractSIDClient.sidClientEnderecoCobranca(NeoUtils.safeOutputString(codEmpresa),
							NeoUtils.safeOutputString(codFilial), NeoUtils.safeOutputString(usu_codcli),
							enderecosCob);

					for (NeoObject no : enderecosCob)
					{
						EntityWrapper ew = new EntityWrapper(no);
						boolean usar = NeoUtils.safeBoolean(ew.findValue("usarComoDoContrato"));

						if (usar)
						{
							usu_seqCob = NeoUtils
									.safeLong(NeoUtils.safeOutputString(ew.findValue("seqCob")));
							break;
						}
					}
				}

				Long usu_codfen = NeoUtils.safeLong(
						NeoUtils.safeOutputString(wDadosContrato.findValue("usucodfen.usu_codfen")));
				Long usu_forpgto = NeoUtils.safeLong(
						NeoUtils.safeOutputString(wDadosContrato.findValue("formaPagamento.codfpg")));
				Long codGre = 0L;
				if ((usu_codemp == 18) && (usu_codfil == 2))
				{
					if ((usu_codfen != 6) && (usu_forpgto == 1))
					{
						codGre = 100L;
					}
					else if ((usu_codfen != 6) && (usu_forpgto != 1))
					{
						codGre = 0L;
					}
					else
					{
						codGre = 0L;
					}
				}

				StringBuilder queryCli = new StringBuilder();

				queryCli.append(
						" UPDATE e085CLI SET codGre = " + codGre + " where codcli = " + usu_codcli);
				Query queryExecuteCli = PersistEngine.getEntityManager("SAPIENS")
						.createNativeQuery(queryCli.toString());

				System.out.print(queryCli.toString());
				try
				{
					queryExecuteCli.executeUpdate();
				}
				catch (Exception e)
				{
					throw new Exception("Erro ao Alterar o Grupo do cliente -> atualizaDadosContrato()");
				}

				ContratoLogUtils.logInfo("[" + key + "] - usu_codfen=" + usu_codfen);
				ContratoLogUtils.logInfo("[" + key
						+ "] - update contrato - Contrato Liberado para Faturamento? " + usu_libfat);
				if (codMov != 5)
				{
					atualizaDadosContrato(codEmpresa, codFilial, numContrato, usu_datemi, usu_numofi,
							usu_regctr, usu_objctr, usu_obscom, usu_codrep, usu_tnsser, usu_inivig,
							usu_fimvig, usu_pervig, usu_datini, usu_datfim, usu_diabas, usu_diafix,
							usu_ctrcom, usu_eximed, usu_prvpos, usu_efepos, usu_libfat, usu_tipctr,
							usu_serctr, usu_datsus, usu_susfat, usu_seqCob, usu_codfen, usu_codcli);
				}
				else
				{
					atualizaDadosContrato(codEmpresa, codFilial, numContrato, usu_seqCob);
				}

				CadastraCentroCusto.atualizaContratoCodigoAgrupador(String.valueOf(codEmpresa),
						String.valueOf(codFilial), String.valueOf(numContrato), "AGR" + numContrato);

				//POSTOS
				NeoObject postoNormal = null;
				List<NeoObject> listaPostosFusion = (List<NeoObject>) wrapper.findField("postosContrato")
						.getValue();
				for (NeoObject postoFusion : listaPostosFusion)
				{
					EntityWrapper wPostoFusion = new EntityWrapper(postoFusion);
					
					Long tipoPosto = wPostoFusion.findGenericValue("tipoPosto.codigo");
					String placa = wPostoFusion.findGenericValue("placa");
					placa = (placa == null || placa.equals("null")) ? "" : placa;

					// copia os dados do posto normal para o posto de proteção garantida
					if (NeoUtils.safeOutputString(wPostoFusion.findValue("tipoPosto.codigo"))
							.equals("1"))
					{
						postoNormal = postoFusion;
					}
					else if (NeoUtils.safeOutputString(wPostoFusion.findValue("tipoPosto.codigo"))
							.equals("2"))
					{
						if (postoNormal != null)
						{
							ProtecaoGarantidaUtils.atualizaPostoProtecaoGarantida(postoNormal,
									postoFusion); // copia os dados do posto normal para o posto de proteção garantida para evitar retrabalho do usuário
						}

					}

					Boolean geraNovoNumeroPosto = false;
					Boolean cadastraNovoPosto = false;

					Boolean gravaObservacaoPosto = false;

					Long numPosto = null;
					Long numPostoOrigem = null;

					boolean origemSapiens = (Boolean) wPostoFusion.findField("origemSapiens").getValue();
					boolean inativarPosto = NeoUtils
							.safeBoolean(wPostoFusion.findValue("duplicarDesativar"));
					String usu_codser = wPostoFusion.findField("complementoServico").getValue() != null
							? (String) wPostoFusion.findField("complementoServico.codser").getValue()
							: "";

					if (origemSapiens)
					{
						//Se origemSapiens = true -> faz alteração

						if (inativarPosto)
						{
							/*
							 * se o posto veio do sapiens, teve alteração e a Análise decidiu desativar e
							 * duplicar
							 * deve-se cadastrar este posto como um novo, e inativar o antigo
							 */
							geraNovoNumeroPosto = true;
							cadastraNovoPosto = true;
							gravaObservacaoPosto = true;
						}
						else
						{
							//se veio do sapiens e não houve a inativação, nada do posto é enviado para o sapiens
							continue;
						}
						numPosto = (Long) wPostoFusion.findField("numPosto").getValue();
						numPostoOrigem = numPosto;
					}
					else
					{
						//Se origemSapiens = false -> cadastra novo posto
						geraNovoNumeroPosto = true;
						cadastraNovoPosto = true;
						inativarPosto = false;
						gravaObservacaoPosto = true;

					}

					if (origemContratoDuplicado)
					{
						geraNovoNumeroPosto = true;

					}
					//ABA MOVIMENTACAO DE POSTOS
					//Validar este ponto, caso a regional seja null estamos informando a regional? 999 - Sem Regiao
					Long usu_regcvs = wPostoFusion.findValue("regionalPosto") != null
							? (Long) wPostoFusion.findField("regionalPosto.usu_codreg").getValue()
							: new Long(999);
					Long usu_qtdfun = (Long) (wPostoFusion.findValue("qtdeDeFuncionarios") != null
							? wPostoFusion.findField("qtdeDeFuncionarios").getValue()
							: 0L);

					//wPostoFusion.findField("qtdeDeFuncionariosPrevistos").getValue(); ??
					BigDecimal usu_qtdcvs = new BigDecimal(1);//(BigDecimal) wPostoFusion.findField("qtdeParaFaturamento").getValue();
					
					Boolean cartaoRFID = wrapper.findGenericValue("dadosGeraisContrato.adereCartaoRFID");
					if (tipoPosto == 3l && cartaoRFID)
					{
						usu_qtdcvs = new BigDecimal((String.valueOf(wrapper.findGenericValue("dadosGeraisContrato.qtCartaoRFID"))));
					}
					
					BigDecimal usu_preuni = (BigDecimal) wPostoFusion.findField("precoUnitarioPosto")
							.getValue();

					// usu_preuni estava sendo gravado zerado no pgo (após atualização do fusion 2.3 para versão 3.2)
					if (usu_preuni == null || (usu_preuni != null && usu_preuni.equals(0)))
					{
						usu_preuni = (BigDecimal) wPostoFusion.findField("valorTotalPosto").getValue();
					}

					//wPostoFusion.findField("precoUnitarioPostoConferencia").getValue(); ??

					//wPostoFusion.findField("valorTotalPosto").getValue();
					//wPostoFusion.findField("representante").getValue(); mesmo do contrato??

					String usu_codccu = wPostoFusion.findField("centroCusto").getValue() != null
							? (String) wPostoFusion.findField("centroCusto.codccu").getValue()
							: null; //String usu_codccu = "8226020";
					ContratoLogUtils.logInfo("[" + key + "]Centro de custo: (centroCusto.codccu) -> "
							+ wPostoFusion.findValue("centroCusto.codccu"));

					//endereco
					NeoObject oEnderecoPosto = (NeoObject) wPostoFusion.findField("enderecoPosto")
							.getValue();
					EntityWrapper wEnderecoPosto = new EntityWrapper(oEnderecoPosto);

					NeoObject oEstadoCidade = (NeoObject) wEnderecoPosto.findField("estadoCidade")
							.getValue();
					EntityWrapper wEstadoCidade = new EntityWrapper(oEstadoCidade);

					String usu_endctr = (String) wEnderecoPosto.findField("endereco").getValue() + " - "
							+ (String) wEnderecoPosto.findValue("complemento");
					if (usu_endctr != null)
					{
						usu_endctr = usu_endctr.replaceAll("'", " ");
					}
					String usu_cplctr = (String) wEnderecoPosto.findValue("numero");
					String cep = wEnderecoPosto.findField("cep").getValue() != null
							? (String) wEnderecoPosto.findField("cep").getValue()
							: "";
					if (cep != null)
					{
						cep = cep.replace("-", "");
					}
					Long usu_cepctr = (Long) NeoUtils.safeLong(cep);
					String usu_baictr = (String) wEnderecoPosto.findField("bairro").getValue();
					//cidade
					String usu_cidctr = (String) wEstadoCidade.findField("cidade.nomcid").getValue();
					String usu_ufsctr = (String) wEstadoCidade.findField("estado.sigufs").getValue();

					// VERIFICA CEP DO POSTO SE IGUAL CEP CIDADE
					if (ParametrizacaoFluxoContratos.findParameter("validaCepPosto") != null
							&& !ParametrizacaoFluxoContratos.findParameter("validaCepPosto").equals("0"))
					{
						verificaCepCidadeCepPosto(usu_cepctr, usu_codemp, Long.parseLong(usu_codser),
								usu_cidctr, usu_ufsctr);
					}

					// VERIFICA REGRAS Tarefa 1324289 
					if (ParametrizacaoFluxoContratos.findParameter("bloqueioCadastroEmpresa") != null
							&& !ParametrizacaoFluxoContratos.findParameter("bloqueioCadastroEmpresa")
									.equals("0"))
					{
						verificaEmpresa(usu_codemp, usu_codfil, usu_cidctr, usu_ufsctr);
					}

					//--				
					NeoObject oEnderecoEfetivo = (NeoObject) wPostoFusion.findField("enderecoEfetivo")
							.getValue();
					EntityWrapper wEnderecoEfetivo = new EntityWrapper(oEnderecoEfetivo);

					NeoObject oEstadoCidadeEfetivo = (NeoObject) wEnderecoEfetivo
							.findField("estadoCidade").getValue();
					EntityWrapper wEstadoCidadeEfetivo = new EntityWrapper(oEstadoCidadeEfetivo);

					String usu_endeft = (String) wEnderecoEfetivo.findField("endereco").getValue();
					if (usu_endeft != null)
					{
						usu_endeft = usu_endeft.replaceAll("'", " ");
					}
					String usu_cpleft = (String) wEnderecoEfetivo.findField("numero").getValue();
					Long usu_cepeft = (Long) NeoUtils.safeLong(
							((String) wEnderecoEfetivo.findField("cep").getValue()).replace("-", ""));

					String usu_baieft = (String) wEnderecoEfetivo.findField("bairro").getValue();
					//cidade
					String usu_cideft = null;

					if (usu_cideft != null)
					{
						usu_cideft = usu_cideft.replace("'", "/'");
					}
					String usu_ufseft = wEstadoCidadeEfetivo.findField("estado").getValue() != null
							? (String) wEstadoCidadeEfetivo.findField("estado.sigufs").getValue()
							: null;

					//--
					BigDecimal usu_vlrva4 = new BigDecimal(0);
					BigDecimal usu_vlrva6 = new BigDecimal(0);
					BigDecimal usu_vlrva8 = new BigDecimal(0);
					BigDecimal usu_vlrvav = new BigDecimal(0);
					BigDecimal usu_vlrvtr = new BigDecimal(0);
					BigDecimal usu_vlrvre = new BigDecimal(0);
					BigDecimal usu_vlrtri = new BigDecimal(0);

					BigDecimal usu_vlrmta = new BigDecimal(0);
					BigDecimal usu_vlrmtb = new BigDecimal(0);
					NeoObject oFormacaoPreco = (NeoObject) wPostoFusion.findField("formacaoPreco")
							.getValue();

					if (oFormacaoPreco != null)
					{
						EntityWrapper wFormacaoPrecoPosto = new EntityWrapper(oFormacaoPreco);

						usu_vlrva4 = (BigDecimal) (wFormacaoPrecoPosto
								.findValue("valeAlimentacao4") == null ? new BigDecimal(0)
										: wFormacaoPrecoPosto.findField("valeAlimentacao4").getValue());
						usu_vlrva6 = (BigDecimal) (wFormacaoPrecoPosto
								.findValue("valeAlimentacao6") == null ? new BigDecimal(0)
										: wFormacaoPrecoPosto.findField("valeAlimentacao6").getValue());
						usu_vlrva8 = (BigDecimal) (wFormacaoPrecoPosto
								.findValue("valeAlimentacao8") == null ? new BigDecimal(0)
										: wFormacaoPrecoPosto.findField("valeAlimentacao8").getValue());
						usu_vlrvav = (BigDecimal) (wFormacaoPrecoPosto.findValue("vigilante") == null
								? new BigDecimal(0)
								: wFormacaoPrecoPosto.findField("vigilante").getValue());
						usu_vlrvtr = (BigDecimal) (wFormacaoPrecoPosto
								.findValue("valeTransporte") == null ? new BigDecimal(0)
										: wFormacaoPrecoPosto.findField("valeTransporte").getValue());
						usu_vlrvre = (BigDecimal) (wFormacaoPrecoPosto.findValue("valeRefeicao") == null
								? new BigDecimal(0)
								: wFormacaoPrecoPosto.findField("valeRefeicao").getValue());
						usu_vlrtri = (BigDecimal) (wFormacaoPrecoPosto.findValue("valorTributos") == null
								? new BigDecimal(0)
								: wFormacaoPrecoPosto.findField("valorTributos").getValue());
						usu_vlrmta = (BigDecimal) (wFormacaoPrecoPosto
								.findValue("valorMontanteA") == null ? new BigDecimal(0)
										: wFormacaoPrecoPosto.findField("valorMontanteA").getValue());
						usu_vlrmtb = (BigDecimal) (wFormacaoPrecoPosto
								.findValue("valorMontanteB") == null ? new BigDecimal(0)
										: wFormacaoPrecoPosto.findField("valorMontanteB").getValue());
						usu_preuni = usu_vlrmtb;
					}
					Long usu_vlrins = new Long(0);
					Long usu_vlradn = new Long(0);

					String usu_sitcvs = NeoUtils
							.safeOutputString(wPostoFusion.findValue("situacaoPosto.situacao"));
					if (usu_sitcvs.equals(""))
						usu_sitcvs = "A";

					Long usu_cepiss = usu_cepctr;

					//					String usu_cplcvs = removerAcentos(NeoUtils.safeOutputString(wPostoFusion.findValue("complementoServicoTxt"))).toUpperCase();
					String usu_cplcvs = removerAcentos(NeoUtils.safeOutputString(
							wPostoFusion.findValue("postosContrato.complementoServico.desser")) + " "
							+ placa).toUpperCase();
					//					if (atividadeComercial){
					//						usu_cplcvs = removerAcentos(NeoUtils.safeOutputString(wPostoFusion.findValue("complementoServicoTxt"))).toUpperCase();
					//					}

					String usu_unimed = NeoUtils
							.safeOutputString(wPostoFusion.findValue("complementoServico.unimed")); //wPosto.findValue("complementoServico") != null ?	(String) wPosto.findValue("complementoServico.unimed") : "";
					ContratoLogUtils.logInfo("[" + key + "]usu_unimed(usu_t160cvs)" + NeoUtils
							.safeOutputString(wPostoFusion.findValue("complementoServico.unimed")));
					/*
					 * String usu_unimed = wPostoFusion.findField("complementoServico").getValue() !=
					 * null ?
					 * (String) wPostoFusion.findField("complementoServico.unimed").getValue() : "";
					 */

					Long usu_qtdlim = new Long(0);
					String usuTipCli = NeoUtils
							.safeOutputString(wrapper.findValue(" buscaNomeCliente.tipcli"));

					//Long usu_periss = new Long(0);
					BigDecimal usu_perirf = new BigDecimal(0.00); //ir - venlir - e001tns fixo 1
					if (usuTipCli.equals("J"))
					{
						usu_perirf = new BigDecimal(1.00);
					}

					//inss if codemp != 1 , 15, 18 e 19 = 11
					BigDecimal usu_perins = codEmpresa == 1 || codEmpresa == 15 || codEmpresa == 19
							? new BigDecimal(11.00)
							: new BigDecimal(0.00);
					BigDecimal usu_perpit = codEmpresa != 4
							? new BigDecimal(0.65).setScale(2, BigDecimal.ROUND_HALF_UP)
							: new BigDecimal(0.00); // pis   /if codemp != 4 = 0.65 else 0
					BigDecimal usu_percsl = codEmpresa != 4
							? new BigDecimal(1.00).setScale(2, BigDecimal.ROUND_HALF_UP)
							: new BigDecimal(0.00); // CSLL  /if codemp != 4 = 1 else 0
					BigDecimal usu_percrt = codEmpresa != 4
							? new BigDecimal(3.00).setScale(2, BigDecimal.ROUND_HALF_UP)
							: new BigDecimal(0.00); //cofins  /if codemp != 4 = 3 else 0
					BigDecimal usu_perour = new BigDecimal(0.00); //outras retencoes

					// estou zerando aqui pois a neomind fez a formula fixa acima para todo tipo de contrato e nos contratos de eletronica não tem nenhum desses impostos.
					//usu_perirf = new  BigDecimal(0);
					usu_perins = new BigDecimal(0);
					usu_perpit = new BigDecimal(0);
					usu_percsl = new BigDecimal(0);
					usu_percrt = new BigDecimal(0);

					if (movimentoContrato.equals(ALTERACAO_TERMO_ADITIVO))
					{

						usu_perirf = new BigDecimal(0);
						usu_perins = new BigDecimal(0);
						usu_perpit = new BigDecimal(0);
						usu_percsl = new BigDecimal(0);
						usu_percrt = new BigDecimal(0);

						ContratoLogUtils.logInfo("[" + key
								+ "] - Termo aditivo, consultando impostos do contrato original ("
								+ numContratoOrigem + ")");
						BigDecimal impostosPostoAntigo[] = ContratoUtils.retornaImpostos(
								NeoUtils.safeOutputString(numContratoOrigem),
								NeoUtils.safeOutputString(numPostoOrigem));

						if (impostosPostoAntigo != null)
						{
							ContratoLogUtils
									.logInfo("[" + key + "] - Termo aditivo, Impostos encontrados.");
							usu_perirf = impostosPostoAntigo[0];
							usu_perins = impostosPostoAntigo[1];
							usu_perpit = impostosPostoAntigo[2];
							usu_percsl = impostosPostoAntigo[3];
							usu_percrt = impostosPostoAntigo[4];
						}
						else
						{
							ContratoLogUtils.logInfo("[" + key
									+ "] - Termo aditivo, Impostos não encontrados para o contrato,posto ("
									+ numContratoOrigem + "," + numPosto + ")");
						}

					}

					Long usu_ctafin = (Long) wPostoFusion.findValue("contaFinanceira.ctafin"); //Informa do número da conta financeira - preenchimento automatico pelo sistema
					Long usu_ctared = (Long) wPostoFusion.findValue("contaContabil.ctared"); //Número do conta contábil inserido ou resgatado - preenchimento automatico pelo sistema

					GregorianCalendar usu_datiniP = wPostoFusion
							.findValue("periodoFaturamento.de") != null
									? (GregorianCalendar) wPostoFusion.findValue("periodoFaturamento.de")
									: NeoCalendarUtils.stringToDate("31/12/1900"); //Será informado pelo executivo
					GregorianCalendar usu_datfimP = wPostoFusion
							.findValue("periodoFaturamento.ate") != null
									? (GregorianCalendar) wPostoFusion
											.findValue("periodoFaturamento.ate")
									: NeoCalendarUtils.stringToDate("31/12/1900"); //No insert valorizar como 31/12/1900 e na consulta busca valor informado

					String usu_tnsserP = (String) (wPostoFusion
							.findGenericValue("transacao.codtns") != null
									? wPostoFusion.findGenericValue("transacao.codtns")
									: wrapper.findGenericValue("dadosGeraisContrato.transacao.codtns")); //Será informado pelo usuário da análise //String usu_tnsserP = "5949A";

					Long usu_codmotP = new Long(0);
					String usu_obsmotP = "";
					String usu_obscvs = wPostoFusion.findValue("periodoFaturamento") != null
							? (String) wPostoFusion.findValue("periodoFaturamento.obs")
							: ""; //deixar em branco, ou será inserida informação pelo usuário

					Long usu_vlrout = new Long(0);
					Long usu_perout = new Long(0);
					Long usu_vlrluc = new Long(0);
					Long usu_isspre = new Long(0);
					Long usu_perpis = new Long(0);
					Long usu_percof = new Long(0);
					Long usu_perenc = new Long(0);
					Long usu_vlrbas = new Long(0);
					Long usu_pertri = new Long(0);
					Long usu_perluc = new Long(0);
					Long usu_ptrluc = new Long(0);
					Long usu_filnfvP = new Long(0);
					String usu_codsnfP = "";
					Long usu_numnfvP = new Long(0);

					String usu_desmon = "1";
					String usu_iteorc = "";

					Long usu_vlrenc = new Long(0);
					Long usu_vlrcal = new Long(0);
					Long usu_orcuni = new Long(0);
					Long usu_orcmat = new Long(0);
					Long usu_orcepi = new Long(0);
					Long usu_orceqp = new Long(0);
					Long usu_orcvtr = new Long(0);
					Long usu_orcvre = new Long(0);

					Long usu_usumotP = codUsuariosapiens;
					Long usu_usucadP = codUsuariosapiens;

					GregorianCalendar usu_datmotP = NeoCalendarUtils.stringToDate("31/12/1900");
					Long usu_hormotP = new Long(0);
					GregorianCalendar usu_datcadP = new GregorianCalendar();
					Long usu_horcadP = (long) usu_datcadP.get(Calendar.HOUR_OF_DAY);
					Long usu_usualtP = codUsuariosapiens;
					GregorianCalendar usu_dataltP = usu_datcadP;
					Long usu_horaltP = usu_horcadP;
					GregorianCalendar usu_ultcpt = NeoCalendarUtils.stringToDate("31/12/1900");
					String usu_calret = "";

					Long usu_prvposP = wPostoFusion.findValue("qtdeDeFuncionariosPrevistos") != null
							? (Long) wPostoFusion.findValue("qtdeDeFuncionariosPrevistos")
							: 0L; //valor informado como executivo de vendas no momento do cadastro

					Boolean suspenderFaturamentoPosto = NeoUtils
							.safeBoolean(wPostoFusion.findValue("suspenderFaturamento"), false);
					String usu_susfatP = suspenderFaturamentoPosto ? "S" : "N";

					//					StringBuilder sql = new StringBuilder();
					//					sql.append("select c.usu_susfat "); 
					//					sql.append("from USU_T160CTR c ");
					//					sql.append("WHERE ");
					//					sql.append("c.usu_codemp = " + wPostoFusion.findValue("codemp"));
					//					sql.append(" and c.usu_codfil = " + wPostoFusion.findValue("codfil"));
					//					sql.append(" and c.usu_numctr = " + wPostoFusion.findValue("numctr"));
					//					
					//					
					//					Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
					//					Object retorno = query.getSingleResult();
					//					
					//					if (retorno != null){
					//						ContratoLogUtils.logInfo("usu_susfat");
					//						if (String.valueOf(retorno).equals("S")){
					//							usu_susfatP = "S";
					//						}
					//					}

					GregorianCalendar usu_dateft = usu_datcadP;

					String usu_sigcon = ""; //nesse momento passar vazio - verificar se após o cadastro no sigma esse campo deve ser populado
					GregorianCalendar usu_datsusP = suspenderFaturamentoPosto ? new GregorianCalendar()
							: null;
					Long usu_usususP = suspenderFaturamentoPosto ? codUsuariosapiens : new Long(0); //Valorizar com código do usuário que efetuou a suspensão - usuário de cadastro
					BigDecimal usu_vlrseg = wPostoFusion.findValue("valorRepasseProtecaoG") != null
							? (BigDecimal) wPostoFusion.findField("vigilanciaEletronica.valorSeguro")
									.getValue()
							: new BigDecimal(0);

					BigDecimal usu_perissP = null;

					if (NeoUtils.safeOutputString(wPostoFusion.findValue("tipoPosto.codigo"))
							.equals("2"))
					{
						usu_vlrseg = wPostoFusion.findValue("valorRepasseProtecaoG") != null
								? (BigDecimal) wPostoFusion.findField("valorRepasseProtecaoG").getValue()
								: new BigDecimal(0);
						//usu_perissP =  wPostoFusion.findValue("valorISS") != null ? (BigDecimal) wPostoFusion.findField("valorISS").getValue() : new BigDecimal(0);
					}
					else
					{
						usu_perissP = retornaPercentualISSPosto(codEmpresa, usu_codser, usu_cepctr);//regra no email usu_cepctr
					}

					ContratoLogUtils.logInfo("[" + key + "]Valor de repasse do seguro " + usu_vlrseg);
					ContratoLogUtils.logInfo("[" + key + "]ISS-Empresa:" + codEmpresa + ",codServico:"
							+ usu_codser + ",cepContrato:" + usu_cepctr);

					Long usu_sigemp = new Long(0);
					Long usu_codrepP = (Long) wPostoFusion.findField("representante.codrep").getValue();
					GregorianCalendar usu_datapr = NeoCalendarUtils.stringToDate("31/12/1900");
					Long usu_usuapr = new Long(0);
					Long usu_posori = origemSapiens ? numPostoOrigem : new Long(0); // SE FOI CRIADO A PARTIR DE UM POSTO QUE FOI INATIVADO POPULAR COM O NUM DO posto 
					String usu_dirapr = "N";
					String usu_gercom = "N";
					String usu_comins = "N";
					String usu_basiss = "";
					String usu_ctrfis = "N";
					GregorianCalendar usu_datfis = NeoCalendarUtils.stringToDate("31/12/1900");
					Long usu_usufis = new Long(0);
					Long usu_tarapr = new Long(0);
					Long usu_codesc = new Long(0);

					GregorianCalendar usu_datass = null; //Preenchido pelo executivo de venda - a assinatura seria a data do contrato assinado na empresa

					GregorianCalendar usu_cptcom = NeoCalendarUtils.stringToDate("31/12/1900");
					Long usu_peresc = new Long(80);

					String usu_dupadt = "";
					if (codMov == 5)
					{
						usu_dupadt = verificaDupAdt(wrapper, wPostoFusion);
					}

					// comentado pois não faz nada
					//verificaCadastrodoPosto(usu_codemp, usu_codfil, numContrato);

					if (geraNovoNumeroPosto)
					{
						numPosto = geraNumeroPostoNovoSapiens();
						sqlNextKeyPosto = "SELECT ISNULL(max(USU_NUMPOS),0)+1 FROM USU_T160CVS";
					}

					// pego o codccu do posto (gravado na atividade CadastraCentroCuso) para gravar no posto 
					String codccuPosto = (String) wPostoFusion.findValue("codccu");
					if (codccuPosto != null && !codccuPosto.equals(""))
					{
						usu_codccu = codccuPosto;
						ContratoLogUtils.logInfo("[" + key + "]Centro de custo: (codccu) -> "
								+ wPostoFusion.findValue("codccu"));
					}

					usu_gercom = wPostoFusion.findValue("geraComissaoVendas") != null
							? (((Boolean) wPostoFusion.findValue("geraComissaoVendas")) ? "S" : "N")
							: "N";

					usu_comins = wPostoFusion.findValue("geraPagamentoInstalacao") != null
							? (((Boolean) wPostoFusion.findValue("geraPagamentoInstalacao")) ? "S" : "N")
							: "N";

					if (/* wPostoFusion.findValue("numPosto") == null */cadastraNovoPosto)
					{
						usu_libfat = NeoUtils.safeBoolean(
								wPostoFusion.findValue("postoLiberadoFaturamento")) == true ? "S" : "N";
						if (movimentoContrato.equals(ALTERACAO_TERMO_ADITIVO))
						{ // reunião 21/05/2014 - sempre para alterações deixar contrato e posto liberados para faturamento por padrão.  
							usu_libfat = "S";
						}
						if ("null".equals(usu_codccu) || usu_codccu == null)
						{
							//throw new WorkflowException("Centro de custo (" + usu_codccu + ") não pode ser vinculado ao posto. Entre em contato com a TI.");
						}

						// copia os dados do posto normal para o posto de proteção garantida

						if (NeoUtils.safeOutputString(wPostoFusion.findValue("tipoPosto.codigo"))
								.equals("2"))
						{
							if (postoNormal != null)
							{
								ProtecaoGarantidaUtils.copiaISSProtecaoGarantida(postoNormal,
										postoFusion);
								EntityWrapper wPostoProtecaoGarantida = new EntityWrapper(postoFusion);
								usu_perissP = (wPostoProtecaoGarantida.getValue("valorISS") != null
										&& !"".equals(wPostoProtecaoGarantida.getValue("valorISS")))
												? new BigDecimal(NeoUtils.safeOutputString(
														wPostoProtecaoGarantida.getValue("valorISS"))
														.replace(",", "."))
												: new BigDecimal("0");
							}
						}
						ContratoLogUtils
								.logInfo("[" + key + "] Posto Liberado para Faturamento? " + usu_libfat);
						numPosto = cadastraNovoPostoT160CVS(sqlNextKeyPosto, usu_libfat, usu_cepiss,
								usu_codemp, usu_codfil, numContrato, numPosto, usu_codser, usu_cplcvs,
								usu_unimed, usu_qtdlim, usu_qtdfun, usu_qtdcvs, usu_preuni, usu_perissP,
								usu_perirf, usu_perins, usu_perpit, usu_percsl, usu_percrt, usu_perour,
								usu_ctafin, usu_ctared, usu_codccu, usu_datiniP, usu_datfimP,
								usu_tnsserP, usu_sitcvs, usu_codmotP, usu_obsmotP, usu_obscvs,
								usu_endctr, usu_cplctr, usu_cepctr, usu_baictr, usu_cidctr, usu_ufsctr,
								usu_endeft, usu_cpleft, usu_cepeft, usu_baieft, usu_cideft, usu_ufseft,
								usu_vlrmta, usu_vlrmtb, usu_vlrva4, usu_vlrva6, usu_vlrva8, usu_vlrvav,
								usu_vlrvtr, usu_vlrvre, usu_vlrins, usu_vlradn, usu_vlrout, usu_perout,
								usu_vlrluc, usu_vlrtri, usu_isspre, usu_perpis, usu_percof, usu_perenc,
								usu_vlrbas, usu_pertri, usu_perluc, usu_ptrluc, usu_filnfvP, usu_codsnfP,
								usu_numnfvP, usu_desmon, usu_iteorc, usu_vlrenc, usu_vlrcal, usu_orcuni,
								usu_orcmat, usu_orcepi, usu_orceqp, usu_orcvtr, usu_orcvre, usu_usumotP,
								usu_datmotP, usu_hormotP, usu_usucadP, usu_datcadP, usu_horcadP,
								usu_usualtP, usu_dataltP, usu_horaltP, usu_ultcpt, usu_calret,
								usu_prvposP, usu_regcvs, usu_susfatP, usu_dateft, usu_sigcon,
								usu_datsusP, usu_vlrseg, usu_usususP, usu_dfimsu, usu_sigemp,
								usu_codrepP, usu_datapr, usu_usuapr, usu_posori, usu_dirapr, usu_gercom,
								usu_comins, usu_basiss, usu_ctrfis, usu_datfis, usu_usufis, usu_tarapr,
								usu_codesc, usu_datass, usu_cptcom, usu_peresc, usu_dupadt, sUsu_PerReg);
						postosInseridos.add(String.valueOf(numPosto));
						
						if (modeloContrato.equals("05"))
						{
							if (tipoPosto != 3l && (placa != null && !placa.isEmpty()))
								inserirDadosRastreamento(usu_codemp, usu_codfil, numContrato, numPosto, placa);
							else
								inserirDadosRastreamento(usu_codemp, usu_codfil, numContrato, numPosto, "aaa1111");
						}
							
						
						OrsegupsContratoUtils.atualizaObsDoContrato(usu_cidctr, usu_codemp, usu_codfil, numContrato);
						/*	PreparedStatement stm = null;
							StringBuilder query = new StringBuilder();
							String usu_obsctrAtu = "Serviços Prestados em suas instalações em " +usu_cidctr;
							String sql = "UPDATE usu_T160CTR SET usu_obsctr= ? WHERE usu_codemp = ? and usu_codfil = ? and usu_numctr = ?";
							Connection connection = null;
							try
							{	
								connection = OrsegupsUtils.getSqlConnection("SAPIENS");
								stm = connection.prepareStatement(sql);
								stm.setString(1,usu_obsctrAtu);
								stm.setLong(2,usu_codemp);
								stm.setLong(3,usu_codfil);
								stm.setLong(4,numContrato);
								int retorno = stm.executeUpdate();		
							}catch (Exception e) {
								e.printStackTrace();
								long chave = GregorianCalendar.getInstance().getTimeInMillis();
								ContratoLogUtils.logInfo("["+chave+"] Erro ao Atualizar Obs do contrato: "+query+" - "+ e.getMessage());
								throw new Exception("["+chave+"] Erro ao Atualizar Obs do contrato: "+query+" - "+e.getMessage());
							}finally
							{
								OrsegupsUtils.closeConnection(connection, stm, null);
							}
							*/

						OrsegupsContratoUtils.atualizaObsDoContrato(usu_cidctr, usu_codemp, usu_codfil,
								numContrato);
						/*
						 * PreparedStatement stm = null;
						 * StringBuilder query = new StringBuilder();
						 * String usu_obsctrAtu = "Serviços Prestados em suas instalações em "
						 * +usu_cidctr;
						 * String sql =
						 * "UPDATE usu_T160CTR SET usu_obsctr= ? WHERE usu_codemp = ? and usu_codfil = ? and usu_numctr = ?"
						 * ;
						 * Connection connection = null;
						 * try
						 * {
						 * connection = OrsegupsUtils.getSqlConnection("SAPIENS");
						 * stm = connection.prepareStatement(sql);
						 * stm.setString(1,usu_obsctrAtu);
						 * stm.setLong(2,usu_codemp);
						 * stm.setLong(3,usu_codfil);
						 * stm.setLong(4,numContrato);
						 * int retorno = stm.executeUpdate();
						 * }catch (Exception e) {
						 * e.printStackTrace();
						 * long chave = GregorianCalendar.getInstance().getTimeInMillis();
						 * ContratoLogUtils.logInfo("["+chave+"] Erro ao Atualizar Obs do contrato: "
						 * +query
						 * +" - "+ e.getMessage());
						 * throw new
						 * Exception("["+chave+"] Erro ao Atualizar Obs do contrato: "+query+" - "
						 * +e.getMessage());
						 * }finally
						 * {
						 * OrsegupsUtils.closeConnection(connection, stm, null);
						 * }
						 */

						if (!inativarPosto)
						{
							wPostoFusion.findField("origemSapiens").setValue(true);
							wPostoFusion.findField("numPosto").setValue(numPosto);
							wPostoFusion.findField("numContrato").setValue(numContrato);
						}
						else
						{
							/*
							 * caso seja uma inativação de posto
							 * deve-se guardar a referencia do posto antigo
							 * isso será usado no momento de atualizar a conta sigma
							 */
							String numeroPostoAntigo = NeoUtils
									.safeOutputString(wPostoFusion.findValue("numPosto"));
							wPostoFusion.findField("numPosto").setValue(numPosto);
							wPostoFusion.findField("numPostoAntigo")
									.setValue(Long.parseLong(numeroPostoAntigo));
						}

						/*
						 * lista de contatos
						 * Collection<NeoObject> listaContato =
						 * wPostoFusion.findField("vigilanciaEletronica.listaContatosPosto").getValues();
						 * int seq = 1;
						 * for(NeoObject contato : listaContato)
						 * {
						 * validaAdicaoContatoPosto(contato, numContrato, numPosto, usu_codemp,
						 * usu_codfil, usu_codser, seq);
						 * seq++;
						 * }
						 */

						PersistEngine.persist(postoFusion);
						PersistEngine.getEntityManager().flush();
					}

					Long usu_seqobs = ContratoUtils.retornaSequenciaObservacaoContrato(usu_codemp,
							usu_codfil, numContrato);

					if (wPostoFusion.findValue("numPosto") == null && gravaObservacaoPosto)
					{
						usu_tipobs = "A";
						if (origemSapiens && numPostoOrigem != null)
						{
							usu_txtobs = "Duplicado a partir do posto n." + numPostoOrigem.toString();
						}
						else
						{
							usu_txtobs = "Cadastro do posto no Sapiens. ";
							if (NeoUtils.safeOutputString(wPostoFusion.findValue("tipoPosto.codigo"))
									.equals("2"))
							{
								usu_txtobs = "Cadastro do posto PGO no Sapiens. ";
							}
						}

						ContratoUtils.geraObservacao(usu_codemp, usu_codfil, numContrato, usu_seqobs,
								numPosto, usu_codser, usu_tipobs, usu_txtobs, usu_usumov, usu_datmov,
								usu_hormov, usu_codope);
					}

					if (NeoUtils.safeOutputString(wPostoFusion.findValue("tipoPosto.codigo"))
							.equals("2"))
					{ // adicionar
						usu_seqobs = ContratoUtils.retornaSequenciaObservacaoContrato(usu_codemp,
								usu_codfil, numContrato);
						usu_txtobs = "Complemento do serviço (Texto NF)= PROTEÇÃO GARANTIDA; Geração da Pagamento de Instalador =  > N; ";
						ContratoUtils.geraObservacao(usu_codemp, usu_codfil, numContrato, usu_seqobs,
								numPosto, usu_codser, usu_tipobs, usu_txtobs, usu_usumov, usu_datmov,
								usu_hormov, "A");
					}

					if (wPostoFusion.findField("equipamentosItens").getValue() != null)
					{

						List<NeoObject> listaKits = (List<NeoObject>) wPostoFusion
								.findField("equipamentosItens.kitsPosto").getValue();

						if (!listaKits.isEmpty())
						{
							for (NeoObject kit : listaKits)
							{
								EntityWrapper wKit = new EntityWrapper(kit);

								Boolean cadastraNovoKit = false;
								Boolean atualizaKit = false;

								Boolean origemSapiensKit = wKit.findField("origemSapiens")
										.getValue() != null
												? (Boolean) wKit.findField("origemSapiens").getValue()
												: false;

								Long usu_codkit = (Long) wKit.findField("kit.usu_codkit").getValue();
								Long usu_qtdkit = (Long) wKit.findField("quantidade").getValue();
								Long usu_codins = (Long) wKit.findField("instaladores.usu_codins")
										.getValue();

								String usu_tipins = wKit.findField("tipoInstalacao").getValue() != null
										? (String) wKit.findField("tipoInstalacao.codigo").getValue()
										: null;

								Long seqKit = null;
								if (origemSapiensKit)
								{
									seqKit = (Long) wKit.findField("seqKit").getValue();
									atualizaKit = true;
								}
								else
								{
									seqKit = retornaSequenciaKit(usu_codemp, usu_codfil, numContrato,
											numPosto, usu_codser);
									cadastraNovoKit = true;
								}

								if (cadastraNovoKit)
								{
									geraRelacaoKitContratoPosto(usu_codemp, usu_codfil, numContrato,
											numPosto, usu_codser, seqKit, usu_codkit, usu_qtdkit,
											usu_codins, usu_tipins);

									wKit.findField("origemSapiens").setValue(true);
									PersistEngine.getEntityManager().flush();

									//OBSERVACAO - HISTÓRICO
									usu_seqobs = ContratoUtils.retornaSequenciaObservacaoContrato(
											usu_codemp, usu_codfil, numContrato);
									usu_tipobs = "A";
									usu_txtobs = "Inserido - Kit: " + usu_codkit + " - Qtd: "
											+ usu_qtdkit;

									ContratoUtils.geraObservacao(usu_codemp, usu_codfil, numContrato,
											usu_seqobs, numPosto, usu_codser, usu_tipobs, usu_txtobs,
											usu_usumov, usu_datmov, usu_hormov, usu_codope);
								}
								if (atualizaKit)
								{
									//TODO UPDATE KIT
								}
							}
						}

						List<NeoObject> listaEquipamentos = (List<NeoObject>) wPostoFusion
								.findField("equipamentosItens.equipamentosPosto").getValue();
						if (!listaEquipamentos.isEmpty())
						{
							for (NeoObject equipamento : listaEquipamentos)
							{
								EntityWrapper wEquip = new EntityWrapper(equipamento);

								Boolean origemSapiensEquipamento = wEquip.findField("origemSapiens")
										.getValue() != null
												? (Boolean) wEquip.findField("origemSapiens").getValue()
												: false;
								Boolean cadastraNovoEquipamento = false;
								Boolean atualizaEquipamento = false;

								Long seqEquip = null;
								Long usu_codfor = 2922196L;
								usu_codfor = 201654L; // thiago orientou a usar o menor numero

								if (origemSapiensEquipamento)
								{
									seqEquip = wEquip.findField("equipamento").getValue() != null
											? (Long) wEquip.findField("equipamento.usu_seqeqp")
													.getValue()
											: new Long(1);
									atualizaEquipamento = true;
								}
								else
								{
									seqEquip = retornaSequenciaEquipamento(usu_codemp, usu_codfil,
											numContrato, numPosto);
									cadastraNovoEquipamento = true;
								}

								String usu_codpro = wEquip.findField("produto").getValue() != null
										? (String) wEquip.findField("produto.codpro").getValue()
										: null;

								String usu_codder = "";
								Long usu_qtdeqp = (Long) wEquip.findField("quantidade").getValue();
								Long usu_codins = wEquip.findField("instaladores").getValue() != null
										? (Long) wEquip.findField("instaladores.usu_codins").getValue()
										: null;
								String usu_tipins = wEquip.findField("tipoInstalacao").getValue() != null
										? (String) wEquip.findField("tipoInstalacao.codigo").getValue()
										: null;

								Long usu_reqeqp = 0L; //rmc 
								Long usu_rmceqp = 0L; //rmc
								Long usu_emppro = 1L; //rmc

								String usu_tippro = "I";
								GregorianCalendar usu_datcadE = new GregorianCalendar();

								Long usu_codkit = 0L;

								Long usu_seqeme = 0L; //sequencia do produto na requisicao- sapiens faz
								Long usu_numoss = 0L; //num os
								GregorianCalendar usu_datoss = NeoCalendarUtils
										.stringToDate("31/12/1900"); //data fechamento os sigma

								Long usu_seqkit = 0L;

								if (wEquip.findValue("codigoKit") != null)
								{
									usu_codkit = Long.parseLong(
											NeoUtils.safeOutputString(wEquip.findValue("codigoKit")),
											10);
									usu_tippro = "K";
									usu_seqkit = retornaSequenciaKit(usu_codemp, usu_codfil, numContrato,
											numPosto, usu_codser);
								}
								Long usu_soleqp = 0L; //rmc
								Long usu_seqsol = 0L; //rmc
								GregorianCalendar usu_datins = (GregorianCalendar) wEquip
										.findField("dataInstalacaoEqp").getValue(); //data instalacao do equipamento - adicionar no fusion - nao obg

								if (usu_datins == null)
								{
									usu_datins = NeoCalendarUtils.stringToDate("31/12/1900");
								}

								if (cadastraNovoEquipamento)
								{
									geraRelacaoEquipamentoContratoPosto(usu_codemp, usu_codfil,
											numContrato, numPosto, usu_codser, seqEquip, usu_codpro,
											usu_codder, usu_qtdeqp, usu_codins, usu_reqeqp, usu_rmceqp,
											usu_emppro, usu_tippro, usu_datcadE, usu_codkit, usu_seqeme,
											usu_numoss, usu_datoss, usu_tipins, usu_seqkit, usu_soleqp,
											usu_seqsol, usu_datins, usu_codfor);

									wEquip.findField("origemSapiens").setValue(true);
									PersistEngine.getEntityManager().flush();

									usu_seqobs = ContratoUtils.retornaSequenciaObservacaoContrato(
											usu_codemp, usu_codfil, numContrato);
									usu_tipobs = "A";
									usu_txtobs = "Inserido Equiapmentos no Posto > : Código: " + seqEquip
											+ " - Qtd: " + usu_qtdeqp + " - Data: "
											+ ContratoUtils.retornaDataFormatada(usu_datcadE)
											+ " - Instalador: " + usu_codins;

									ContratoUtils.geraObservacao(usu_codemp, usu_codfil, numContrato,
											usu_seqobs, numPosto, usu_codser, usu_tipobs, usu_txtobs,
											usu_usumov, usu_datmov, usu_hormov, usu_codope);
								}
								if (atualizaEquipamento)
								{
									//TODO UPDATE EQUIPAMENTO
								}
							}
						}

						List<NeoObject> listaChecklist = (List<NeoObject>) wPostoFusion
								.findField("equipamentosItens.itensChecklist").getValue();
						if (!listaChecklist.isEmpty())
						{
							for (NeoObject cklist : listaChecklist)
							{
								EntityWrapper wNovoChecklistPosto = new EntityWrapper(cklist);

								Long seqCkl = null;
								Boolean cadastraNovoChecklist = false;
								Boolean atualizaChecklist = false;

								//wNovoChecklistPosto.findField("itemChecklist").getValue();

								//se possui seqchk preenchido é origem sapiens
								if (wNovoChecklistPosto.findField("seqchk").getValue() != null)
								{
									seqCkl = (Long) wNovoChecklistPosto.findField("seqchk").getValue();
									atualizaChecklist = true;
								}
								else
								{
									//select * from USU_T160CHK where usu_codemp = 18 and usu_codfil = 1 and usu_numctr = 180046 and usu_numpos = 84031 oRDER BY usu_seqchk DESC
									seqCkl = retornaSequenciaChecklist(usu_codemp, usu_codfil,
											numContrato, numPosto);
									cadastraNovoChecklist = true;
								}

								Long usu_codItm = 0L;
								if (wNovoChecklistPosto.findValue("itemChecklist.usu_coditm") != null)
								{
									usu_codItm = NeoUtils.safeLong(wNovoChecklistPosto
											.findValue("itemChecklist.usu_coditm").toString());
								}

								QLGroupFilter gp = new QLGroupFilter("AND");
								gp.addFilter(new QLRawFilter("usu_coditm = " + usu_codItm));
								NeoObject itemCheckList = (NeoObject) PersistEngine
										.getObject(AdapterUtils.getEntityClass("SAPIENSUSUITM"), gp);
								if (itemCheckList != null)
								{
									EntityWrapper wItemCheckList = new EntityWrapper(itemCheckList);

									String usu_itmchk = "";
									if (wItemCheckList.findField("usu_desitm") != null)
									{
										usu_itmchk = (String) wItemCheckList.findField("usu_desitm")
												.getValue();
									}

									Long usu_qtdchk = (Long) wNovoChecklistPosto.findField("quantidade")
											.getValue();
									GregorianCalendar usu_datchk = (GregorianCalendar) wNovoChecklistPosto
											.findField("prazo").getValue();
									String usu_stachk = ""; //status - c t e - adicionar no fusion?
									Long usu_rmcchk = null; //rmc

									//activity.getProcess().getCode()
									Long usu_tarfus = NeoUtils.safeLong("0"); // numero tarefa fusion

									if (cadastraNovoChecklist)
									{
										geraRelacaoChecklistContratoPosto(usu_codemp, usu_codfil,
												numContrato, numPosto, usu_codser, usu_itmchk,
												usu_qtdchk, usu_datchk, usu_stachk, seqCkl, usu_tarfus,
												usu_rmcchk, usu_codItm);
										//se cadastrou atualiza seq
										wNovoChecklistPosto.findField("seqchk").setValue(seqCkl);
										PersistEngine.getEntityManager().flush();

										//OBSERVACAO - HISTÓRICO
										usu_seqobs = ContratoUtils.retornaSequenciaObservacaoContrato(
												usu_codemp, usu_codfil, numContrato);
										usu_tipobs = "A";
										usu_txtobs = "Cadastrado(s) Item(ns) do Check List= Item: "
												+ usu_itmchk + ";  Quantidade: " + usu_qtdchk
												+ ";  Prazo de Atendimento: "
												+ ContratoUtils.retornaDataFormatada(usu_datchk);
										ContratoUtils.geraObservacao(usu_codemp, usu_codfil, numContrato,
												usu_seqobs, numPosto, usu_codser, usu_tipobs, usu_txtobs,
												usu_usumov, usu_datmov, usu_hormov, usu_codope);
									}
								}

								if (atualizaChecklist)
								{
									//TODO UPDATE CHECKL
								}
							}
						}
					}

					//Contatos
					if (wPostoFusion.findField("vigilanciaEletronica").getValue() != null)
					{
						List<NeoObject> listaContatosPosto = (List<NeoObject>) wPostoFusion
								.findField("vigilanciaEletronica.listaContatosPosto").getValue();
						for (NeoObject oContato : listaContatosPosto)
						{
							EntityWrapper wContato = new EntityWrapper(oContato);

							Long seqCon = null;
							Boolean cadastraNovoContato = false;
							Boolean atualizaContato = false;

							//se possui seqchk preenchido é origem sapiens
							if (wContato.findValue("seqcon") != null)
							{
								seqCon = (Long) wContato.findField("seqcon").getValue();
								atualizaContato = true;
							}
							else
							{
								//select usu_seqcon  from USU_T160CON where usu_codemp = 18 and usu_codfil = 1 and usu_numctr = 334630 and usu_numpos = 159144 oRDER BY usu_seqcon DESC
								seqCon = retornaSequenciaContato(usu_codemp, usu_codfil, numContrato,
										numPosto);
								cadastraNovoContato = true;
							}

							String usu_nomcon = wContato.findField("nomecontato").getValue() != null
									? (String) wContato.findField("nomecontato").getValue()
									: "";
							String usu_foncon1 = wContato.findField("telefone1").getValue() != null
									? (String) wContato.findField("telefone1").getValue()
									: "";
							String usu_foncon2 = wContato.findField("telefone2").getValue() != null
									? (String) wContato.findField("telefone2").getValue()
									: "";
							String usu_funcon = wContato.findField("funcao").getValue() != null
									? (String) wContato.findField("funcao").getValue()
									: "";

							//if(cadastraNovoContato)
							//{
							geraRelacaoContatoContratoPosto(codEmpresa, codFilial, numContrato, numPosto,
									usu_codser, seqCon, usu_nomcon, usu_foncon1, usu_foncon2,
									usu_funcon);

							wContato.findField("seqcon").setValue(seqCon);
							PersistEngine.getEntityManager().flush();

							//OBSERVACAO - HISTÓRICO
							usu_seqobs = ContratoUtils.retornaSequenciaObservacaoContrato(usu_codemp,
									usu_codfil, numContrato);
							usu_tipobs = "A";
							usu_txtobs = "Contatos do Posto= Nome do Contato: " + usu_nomcon
									+ ";  Telefone de contato 1: " + usu_foncon1
									+ ";  Telefone de contato 2: " + usu_foncon2 + " Função: "
									+ usu_funcon;

							ContratoUtils.geraObservacao(usu_codemp, usu_codfil, numContrato, usu_seqobs,
									numPosto, usu_codser, usu_tipobs, usu_txtobs, usu_usumov, usu_datmov,
									usu_hormov, usu_codope);
							//}

							if (atualizaContato)
							{
								//TODO update contato
							}
						}
					}

					// tranferencia de bonificações
					// termo aditivo, onde tem posto com lançamentos na usu_t160cms para competencias futuras
					String numPostoAntigo = NeoUtils
							.safeOutputString(wPostoFusion.findValue("numPostoAntigo"));
					//Pegando data inicio do novo posto para so trazer os apontamentos futuros( apontamentos com inicio >= a data inicio do posto novo);
					/*
					 * GregorianCalendar dtIniPosNov = (GregorianCalendar)
					 * wPostoFusion.findValue("periodoFaturamento.de");
					 */
					if (movimentoContrato.equals(ALTERACAO_TERMO_ADITIVO)
							&& !"".equals(OrsegupsContratoUtils.exiteInstalacaoCompetenciaFutura(
									NeoUtils.safeOutputString(numContrato),
									NeoUtils.safeOutputString(numPostoAntigo))))
					{
						ContratoLogUtils.logInfo("[" + key + "] - contrato=" + numContrato
								+ " numposto= " + numPostoAntigo + "  ==> "
								+ OrsegupsContratoUtils.exiteInstalacaoCompetenciaFutura(
										NeoUtils.safeOutputString(numContrato),
										NeoUtils.safeOutputString(numPostoAntigo)));

						Long usu_numctr = numContrato;
						Long usu_numposAntigo = (Long) wPostoFusion.findValue("numPostoAntigo");
						Long usu_numposNovo = (Long) wPostoFusion.findValue("numPosto");
						String usu_descObs = "";

						GregorianCalendar usu_datGer = new GregorianCalendar();
						Long usu_horGer = (long) usu_datGer.get(Calendar.HOUR_OF_DAY);

						ContratoLogUtils.logInfo("[" + key
								+ "]Iniciando transferencia de lançamentos usu_t160cms cpt futura -> usu_codEmp= "
								+ usu_codemp + "\n usu_codfil= " + usu_codfil + "\n usu_numctr= "
								+ usu_numctr + "\n usu_numposAntigo= " + usu_numposAntigo
								+ "\n usu_numposNovo= " + usu_numposNovo + "\n usu_codccuPostoNovo= "
								+ usu_codccu + "\n usu_usuger= " + codUsuariosapiens + "\n usu_datger= "
								+ usu_datger + "\n usu_horger= " + usu_horGer + "\n usu_descObs= "
								+ usu_descObs);
						//Pegar data de inicio de faturamento do posto novo
						transfereBonificacoesPosto(usu_codemp, usu_codfil, usu_numctr, usu_numposAntigo,
								usu_numposNovo, NeoUtils.safeLong(usu_codccu), codUsuariosapiens,
								usu_datger, usu_horGer, usu_descObs);
					}
					//if (true) throw new WorkflowException("ME REMOVCA EXCEPTION");

				}

				//SAPIENSUSUTCVS
				List<NeoObject> listaPostosExt = GeraDadosIniciais.retornaListaPostos(codEmpresa,
						usu_codfil, numContrato);
				for (NeoObject postoSapiens : listaPostosExt)
				{

					EntityWrapper wPostoSapiens = new EntityWrapper(postoSapiens);
					Long numeroPostoExt = (Long) wPostoSapiens.findField("usu_numpos").getValue();
					boolean contemNoFusion = false;
					for (NeoObject postoFusion1 : listaPostosFusion)
					{
						EntityWrapper wPostoFusion1 = new EntityWrapper(postoFusion1);
						Long numeroPostoFusion = (Long) wPostoFusion1.findField("numPosto").getValue();

						if (numeroPostoExt.equals(numeroPostoFusion))
						{
							contemNoFusion = true;
							break;
						}
					}
				}
			}

			if (atividadeComercial)
			{
				geraApontamentos(wrapper, activity);
				ContratoUtils.addPostosToRollBack(numctr, codEmpresa, codFilial, postosInseridos);
			}
			System.out.println("[FLUXO CONTRATOS] - Debug-99 Chegou ao final do adapter sem problemas");

		}
		catch (Exception e)
		{

			ContratoLogUtils
					.logInfo("[" + key + "] Erro ContratoCadastraContratosSapiens. " + e.getMessage());

			e.printStackTrace();
			if (e instanceof WorkflowException)
			{
				ContratoUtils.rollBackPostos(numctr, codEmpresa, codFilial, postosInseridos);

				ContratoUtils.rollBackCC(numctr, ContratoUtils.retornaListaCCToRollBack(numctr),
						"CADASTROCONTRATOSSAPIENS - " + origin.getCode());
				ContratoUtils.removeListaCCToRollBack(numctr);

				throw (WorkflowException) e;
			}
			else
			{
				ContratoUtils.rollBackPostos(numctr, codEmpresa, codFilial, postosInseridos);

				ContratoUtils.rollBackCC(numctr, ContratoUtils.retornaListaCCToRollBack(numctr),
						"CADASTROCONTRATOSSAPIENS - " + origin.getCode());
				ContratoUtils.removeListaCCToRollBack(numctr);

				System.out.println("[FLUXO CONTRATOS][" + key
						+ "] - Erro ao cadastrar cliente/contrato no Fluxo de contratos."
						+ e.getMessage());
				throw new WorkflowException(
						"[" + key + "] Erro ao cadastrar cliente/contrato no Fluxo de contratos. "
								+ e.getMessage());
			}
		}

		//rollBackPostos(numctr, codEmpresa, codFilial );

		// Armazena os postos inseridos aqui para que o proximo adapter possa removelos em caso de erro.
		// *Gambiarra pra contornar limitação do fusion
		ContratoUtils.addPostosToRollBack(numctr, codEmpresa, codFilial, postosInseridos);
		System.out.println("[FLUXO CONTRATOS] [" + key
				+ "] - Debug-999 Chegou ao final do adapter sem problemas 2");
	}

	private void verificaCepCidadeCepPosto(Long usu_cepctr, Long usu_codEmpresa, Long usu_codServico,
			String usu_cidctr, String usu_ufsctr)
	{

		System.out.println("CONTRATO TESTE ENTROU 1 usu_cepctr=" + usu_cepctr + " usu_codEmpresa="
				+ usu_codEmpresa + " usu_codServico=" + usu_codServico + " usu_cidctr=" + usu_cidctr
				+ " usu_ufsctr=" + usu_ufsctr);
		List<NeoObject> lista = (List<NeoObject>) PersistEngine.getObjects(
				AdapterUtils.getEntityClass("SAPIENSESER"),
				new QLRawFilter(" codser = '" + usu_codServico + "' and codemp = " + usu_codEmpresa));
		for (NeoObject l : lista)
		{
			EntityWrapper ew = new EntityWrapper(l);
			System.out.println("CONTRATO TESTE ENTROU 2 serimp=");
			if (!ew.findGenericValue("serimp").toString().equals("14.01")
					&& !ew.findGenericValue("serimp").toString().equals("14.06"))
			{

				System.out.println("CONTRATO TESTE ENTROU 3");
				StringBuffer query = new StringBuffer();
				query.append(
						"SELECT E008RAI.NomCid, E008RAI.SigUfs FROM E008CEP, E008RAI WHERE E008RAI.CodRai = E008CEP.CodRai AND "
								+ usu_cepctr + " BETWEEN CEPINI AND CEPFIM ORDER BY CEPINI");
				ResultSet rs = OrsegupsContratoUtils.getResultSet(query.toString(), nomeFonteDados);
				if (rs != null)
				{
					try
					{
						String nomCid = rs.getString("NomCid");
						String sigUfs = rs.getString("SigUfs");
						System.out.println(
								"CONTRATO TESTE ENTROU 4 - NomCid= " + nomCid + "SigUfs=" + sigUfs);
						System.out.println("nomCid=" + nomCid + " e usu_cidctr=" + usu_cidctr
								+ " - sigUfs=" + sigUfs + " e usu_ufsctr=" + usu_ufsctr);
						if (!nomCid.equals(usu_cidctr) && !sigUfs.equals(usu_ufsctr))
						{
							System.out.println("CONTRATO TESTE ENTROU 5");
							throw new WorkflowException(
									"Cidade/Estado do Posto diferente da Cidade/Estado do CEP informado! Por favor, ajuste para o CEP correto ou entre em contato com o NAC!");
						}
					}
					catch (SQLException e)
					{
						System.out.println(
								"[FLUXO CONTRATOS] - ERRO: Falha ao verificar se cidade/uf da cidade é igual ao do posto.");
						throw new WorkflowException(
								"ERRO: Falha ao verificar cidade/uf da cidade é igual ao do posto.");
					}
				}
				else
				{
					throw new WorkflowException(
							"Atenção: CEP não encontrado na faixa de CEP cadastrado na tabela de Cidade. Entre em contato com o NAC!");
				}
			}
		}
	}

	private void verificaEmpresa(Long empresa, Long filial, String cidade, String estado)
	{

		System.out.println("CONTRATOCADASTRACONTRATOSAPIENS: empresa=" + empresa + ", filial=" + filial
				+ ", cidade=" + cidade + ", estado=" + estado);

		//MONITORAMENTO
		if (empresa == 18L && filial != 1000 && filial != 1001)
		{
			if (empresa == 18L && estado.equals("SC") && cidade.equals("JOINVILLE") && filial != 3L)
			{
				throw new WorkflowException(
						"O Contrato não pode ser cadastrado. Favor cadastrar o mesmo na filial 3");
			}
			else
			{

				if (empresa == 18L && estado.equals("PR") && filial != 2)
				{
					throw new WorkflowException(
							"O Contrato não pode ser cadastrado. Favor cadastrar o mesmo na filial 2");
				}
				else
				{

					if (empresa == 18L && estado.equals("SC") && !cidade.equals("JOINVILLE")
							&& filial != 1L)
					{
						throw new WorkflowException(
								"O Contrato não pode ser cadastrado. Favor cadastrar o mesmo na filial 1");
					}
					else
					{

						if (empresa == 18L && estado.equals("RS") && cidade.equals("IMBE")
								&& filial != 4)
						{
							throw new WorkflowException(
									"O Contrato não pode ser cadastrado. Favor cadastrar o mesmo na filial 4");
						}
						else
						{

							if (empresa == 18L && estado.equals("SC") && !cidade.equals("JOINVILLE"))
							{
								if (empresa == 18L && !estado.equals("PR"))
								{
									if (empresa == 18L && estado.equals("RS") && !cidade.equals("IMBE"))
									{
										if (filial != 1L)
										{
											throw new WorkflowException(
													"O Contrato não pode ser cadastrado. Favor cadastrar o mesmo na filial 1");
										}
									}
								}
							}
						}
					}
				}
			}
		}

		//ITASEG
		if (empresa == 29L && estado.equals("PR") && filial != 2)
		{
			throw new WorkflowException(
					"O Contrato não pode ser cadastrado. Favor cadastrar o mesmo na filial 2");
		}
		else
		{

			if (empresa == 29L && estado.equals("SC") && filial != 1)
			{
				throw new WorkflowException(
						"O Contrato não pode ser cadastrado. Favor cadastrar o mesmo na filial 1");
			}
		}

		//METROPOLITANA
		if (empresa == 21L && estado.equals("SP") && cidade.equals("REGISTRO") && filial != 3L) {
			throw new WorkflowException("O Contrato não pode ser cadastrado. Favor cadastrar o mesmo na filial 3");
		} else {
			if (empresa == 21L && estado.equals("PR") && cidade.equals("CASCAVEL") && filial != 2L) {
				throw new WorkflowException("O Contrato não pode ser cadastrado. Favor cadastrar o mesmo na filial 2");
			} else {
				if (empresa == 21L && estado.equals("PR") && !cidade.equals("CASCAVEL") && filial != 1 ) {
					throw new WorkflowException(
							"O Contrato não pode ser cadastrado. Favor cadastrar o mesmo na filial 1");
				} else if (empresa == 21L && !estado.equals("PR") && filial != 4) {
					throw new WorkflowException(
							"O Contrato não pode ser cadastrado. Favor cadastrar o mesmo na filial 4");
				} else if (empresa == 21L && estado.equals("SC") && filial != 4) {
					throw new WorkflowException(
							"O Contrato não pode ser cadastrado. Favor cadastrar o mesmo na filial 4");
				}
			}
		}
	}

	/**
	 * Verifica se o valor do posto aumentou para marcar que o posto foi duplicado por aditivo. Regra
	 * Definida com Caroline.wrunsky no dia 26/06/2014
	 * 
	 * @param wrapper
	 * @param wPostoNovo
	 * @return
	 */
	public String verificaDupAdt(EntityWrapper wrapper, EntityWrapper wPostoNovo)
	{
		try
		{
			Long numposto = NeoUtils.safeLong(String.valueOf(wPostoNovo.findValue("numPosto")));
			BigDecimal valorPNovo = new BigDecimal(
					String.valueOf(wPostoNovo.findValue("formacaoPreco.valorMontanteB") == null ? 0
							: wPostoNovo.findValue("formacaoPreco.valorMontanteB")));

			ContratoLogUtils.logInfo("[dupadt] VERIFICA TIPO DE DUPLICACAO DO POSTO - USU_DUPADT");
			List<NeoObject> listaPostosContratoBackup = (List<NeoObject>) wrapper
					.findField("postosContratoBackup").getValue();
			//List<NeoObject> listaPostos = (List<NeoObject>) wrapper.findField("postosContrato").getValue();

			for (int i = 0; i < listaPostosContratoBackup.size(); i++)
			{
				EntityWrapper wPostoAntigo = new EntityWrapper(
						(NeoObject) listaPostosContratoBackup.get(i));
				Long numPostoBkp = NeoUtils.safeLong(String.valueOf(wPostoAntigo.findValue("numPosto")));
				ContratoLogUtils.logInfo("[dupadt]NumPosto = " + numposto + ", NumPostoBkp ="
						+ numPostoBkp + "=" + numposto.equals(numPostoBkp));

				if (numposto.equals(numPostoBkp))
				{
					BigDecimal valorPAntigo = new BigDecimal((String
							.valueOf(wPostoAntigo.findValue("formacaoPreco.valorMontanteB") == null ? 0
									: wPostoAntigo.findValue("formacaoPreco.valorMontanteB"))));
					ContratoLogUtils.logInfo(
							"[dupadt]valorPAntigo->valorPNovo" + valorPAntigo + "/" + valorPNovo);
					if (valorPNovo.compareTo(valorPAntigo) == 1
							&& valorPAntigo.compareTo(new BigDecimal(0)) == 1)
					{
						ContratoLogUtils.logInfo("[dupadt]Retorno S");
						return "S";
					}
				}
				wPostoAntigo = null;
			}
			//listaPostos.clear();
			//listaPostosContratoBackup.clear();
			ContratoLogUtils.logInfo("[dupadt]Retorno N");
			return "N";
		}
		catch (Exception e)
		{
			System.out.println(
					"[FLUXO CONTRATOS] - Não foi possível verificar se o posto é duplicado por aditivo.");
			e.printStackTrace();
			return "";
		}
	}

	public static void validaAdicaoContatoPosto(NeoObject contato, Long numContrato, Long numPosto,
			Long usu_codemp, Long usu_codfil, String usu_codser, int seq)
	{

		/*
		 * fazer validação se o contato existe, caso contrário deve-se cria-lo
		 * tabela USU_T160CON
		 */

		StringBuilder sql = new StringBuilder();
		sql.append("select con.* ");
		sql.append("from USU_T160CON con ");
		sql.append("WHERE ");
		sql.append("con.usu_codemp = " + usu_codemp);
		sql.append("con.usu_codfil = " + usu_codfil);
		sql.append("con.usu_numctr = " + numContrato);
		sql.append("con.usu_numpos = " + numPosto);
		sql.append("con.usu_codser = " + usu_codser);
		sql.append("con.usu_seqcon = " + seq);

		Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		Collection<Object> resultList = query.getResultList();

		if (resultList != null && !resultList.isEmpty())
		{
			//ja existe, não faz nada
		}
		else
		{
			EntityWrapper wContato = new EntityWrapper(contato);
			String nome = NeoUtils.safeOutputString(wContato.findValue("nomecontato"));
			String fone1 = NeoUtils.safeOutputString(wContato.findValue("telefone1"));
			String fone2 = NeoUtils.safeOutputString(wContato.findValue("telefone2"));
			String funcao = NeoUtils.safeOutputString(wContato.findValue("funcao"));
			String update = "INSERT INTO USU_T160CON (usu_codemp,usu_codfil,usu_numctr,usu_numpos,usu_codser,usu_seqcon,usu_nomcon,usu_foncon1,usu_foncon2,usu_funcon) VALUES "
					+ "(" + usu_codemp + ", " + usu_codfil + ", " + numContrato + ", " + numPosto + ", "
					+ usu_codser + ", " + seq + ", " + nome + ", " + fone1 + ", " + fone2 + ", " + funcao
					+ ")";
			sql = new StringBuilder();
			sql.append(update);
			Query queryExecute = PersistEngine.getEntityManager("SIGMA90")
					.createNativeQuery(sql.toString());
			try
			{
				queryExecute.executeUpdate();
			}
			catch (Exception e)
			{
				throw new WorkflowException("Erro");
			}
		}
	}

	public static String removerAcentos(String str)
	{
		if (str == null || str.equals(""))
			return "";

		str = Normalizer.normalize(str, Normalizer.Form.NFD);
		str = str.replaceAll("[^\\p{ASCII}]", "");
		return str;
	}

	public BigDecimal retornaPercentualISSPosto(Long codEmpresa, String usu_codser, Long usu_cepctr)
	{

		StringBuffer query = new StringBuffer();

		query.append("SELECT TOP 1 CEPINI FROM E008CEP WHERE " + usu_cepctr
				+ " BETWEEN CEPINI AND CEPFIM ORDER BY CEPINI ");

		/*
		 * ResultSet rsContagem =
		 * OrsegupsContratoUtils.getResultSet("SELECT COUNT(CEPINI) FROM E008CEP WHERE "
		 * +usu_cepctr+" BETWEEN CEPINI AND CEPFIM", nomeFonteDados);
		 * int faixasCep =0;
		 * if (rsContagem != null){
		 * try {
		 * faixasCep = rsContagem.getInt(1);
		 * rsContagem.close();
		 * if (faixasCep > 1){
		 * throw new WorkflowException(
		 * "ERRO: Impossível determinar o ISS. Foi encontrada mais de uma faixa de CEP para o CEP:"
		 * +usu_cepctr);
		 * }
		 * } catch (SQLException e) {
		 * e.printStackTrace();
		 * return new BigDecimal(0.00);
		 * }
		 * }
		 */

		ResultSet rs = OrsegupsContratoUtils.getResultSet(query.toString(), nomeFonteDados);

		if (rs != null)
		{
			try
			{
				Long CEPINI = rs.getLong("CEPINI");

				StringBuffer querySXC = new StringBuffer();
				querySXC.append(
						"SELECT PERISS FROM E080SXC WHERE CODEMP = " + codEmpresa + " AND CODSER = '"
								+ usu_codser + "' AND CEPINI = " + CEPINI + "ORDER BY DATINI DESC");
				ResultSet rsSXC = OrsegupsContratoUtils.getResultSet(querySXC.toString(),
						nomeFonteDados);

				if (rsSXC != null)
				{
					BigDecimal PERISS = rsSXC.getBigDecimal("PERISS");

					if (PERISS.compareTo(new BigDecimal(0.00)) != 0)
					{
						return PERISS;
					}
					else
					{
						ContratoLogUtils.logInfo(
								"Alterar a Ligação Serviço x CEP com o valor do ISS = 0 para o CEP: "
										+ CEPINI + " \n" + query.toString());
						throw new WorkflowException(
								"Alterar a Ligação Serviço x CEP com o valor do ISS = 0 para o CEP: "
										+ CEPINI);
					}
				}
			}
			catch (SQLException e)
			{
				System.out.println(
						"[FLUXO CONTRATOS] - ERRO: Falha ao verificar ISS para retornaPercentualISSPosto.");
				throw new WorkflowException(
						"ERRO: Falha ao verificar ISS para retornaPercentualISSPosto.");
			}
		}
		else
		{
			throw new WorkflowException("Atenção: Cep não encontrado na busca do ISS do Posto. ");
		}

		return new BigDecimal(0.00);
	}

	private String retornaCidadePostoAtivo(EntityWrapper wrapper)
	{
		String cidade = "";
		if (wrapper.findField("postosContrato").getValue() != null)
		{
			List<NeoObject> listaPostosFusion = (List<NeoObject>) wrapper.findField("postosContrato")
					.getValue();
			for (NeoObject postoFusion : listaPostosFusion)
			{
				if (new EntityWrapper(postoFusion).findField("situacaoPosto") != null)
				{
					String situacao = NeoUtils.safeOutputString(
							new EntityWrapper(postoFusion).findValue("situacaoPosto.situacao"));
					if (situacao.startsWith("A"))
					{
						if (new EntityWrapper(postoFusion).findField("enderecoPosto").getValue() != null)
						{
							String usu_cidctr = NeoUtils.safeOutputString(new EntityWrapper(postoFusion)
									.findValue("enderecoPosto.estadoCidade.cidade.nomcid"));

							cidade = usu_cidctr;
							break;
						}
					}
				}
			}
		}
		return cidade;
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}

	/**
	 * usu_t160ctr
	 * 
	 * @param codemp
	 * 
	 *            Passo 5
	 * @throws Exception
	 */
	private static Long cadastraDadosGeraisContratos_T160CTR(String sqlNextKeyContrato,
			String usu_impuni, Long usu_codemp, Long usu_codfil, Long usu_numctr, String usu_numofi,
			String usu_objctr, GregorianCalendar usu_datemi, Long usu_codcli, Long usu_seqcob,
			Long usu_codrep, GregorianCalendar usu_inivig, GregorianCalendar usu_fimvig,
			GregorianCalendar usu_datini, GregorianCalendar usu_datfim, Long usu_diabas, Long usu_diafix,
			GregorianCalendar usu_datuft, Long usu_filnfv, String usu_codsnf, Long usu_numnfv,
			String usu_codcpg, GregorianCalendar usu_ultrea, String usu_sitctr, Long usu_codmot,
			String usu_obsmot, String usu_obsctr, String usu_ctrcom, String usu_eximed, Long usu_prvpos,
			Long usu_efepos, Long usu_codfpg, Long usu_usuger, GregorianCalendar usu_datger,
			Long usu_horger, String usu_libfat, Long usu_pervig, Long usu_usumot,
			GregorianCalendar usu_datmot, Long usu_hormot, Long usu_usucad, GregorianCalendar usu_datcad,
			Long usu_horcad, Long usu_usualt, GregorianCalendar usu_datalt, Long usu_horalt,
			String usu_obscom, String usu_obsfin, Long usu_dscfin, Long usu_tipctr, String usu_clicon,
			String usu_clifon, String usu_codfcr, Long usu_fatanu, String usu_ctrcor, String usu_tnsser,
			Long usu_regctr, String usu_susfat, GregorianCalendar usu_datsus, Long usu_ususus,
			GregorianCalendar usu_dfimsu, BigDecimal usu_periss, String usu_emactr, Long usu_serctr,
			Long usu_empori, Long usu_filori, Long usu_ctrori, Long usu_numcgc, Long usu_coduni,
			String usu_ctragr, String usu_atuaso, Activity activity, String veioConcorrenteString,
			String nomeConcorrente) throws Exception
	{
		Long retorno = 0L;
		/**
		 * insert into usu_t160ctr
		 * (usu_impuni, usu_codemp, usu_codfil, usu_numctr, usu_numofi, usu_objctr, usu_datemi,
		 * usu_codcli, usu_seqcob,
		 * usu_codrep, usu_inivig, usu_fimvig, usu_datini, usu_datfim, usu_diabas, usu_diafix,
		 * usu_datuft, usu_filnfv,
		 * usu_codsnf, usu_numnfv, usu_codcpg, usu_ultrea, usu_sitctr, usu_codmot, usu_obsmot,
		 * usu_obsctr, usu_ctrcom,
		 * usu_eximed, usu_prvpos, usu_efepos, usu_codfpg, usu_usuger, usu_datger, usu_horger,
		 * usu_libfat, usu_pervig,
		 * usu_usumot, usu_datmot, usu_hormot, usu_usucad, usu_datcad, usu_horcad, usu_usualt,
		 * usu_datalt, usu_horalt,
		 * usu_obscom, usu_obsfin, usu_dscfin, usu_tipctr, usu_clicon, usu_clifon, usu_codfcr,
		 * usu_fatanu, usu_ctrcor,
		 * usu_tnsser, usu_regctr, usu_susfat, usu_datsus, usu_ususus, usu_dfimsu, usu_periss,
		 * usu_emactr, usu_serctr,
		 * usu_empori, usu_filori, usu_ctrori, usu_numcgc, usu_coduni)
		 * values (' ', 18, 1, 337122, ' ', 'SERVICOS DE VIGILANCIA ELETRONICA ', '2013-08-20 00:00:00',
		 * 84731, 0, 13,
		 * '2013-08-20 00:00:00', '1900-12-31 00:00:00', '2013-08-20 00:00:00', '1900-12-31 00:00:00',
		 * 15, 10,
		 * '1900-12-31 00:00:00', 0, ' ', 0, 'PRXMES', '1900-12-31 00:00:00', 'A', 0, ' ', 'Serviços
		 * prestados em suas instalações em BRUSQUE',
		 * 'N', 'N', 1, 1, 1, 587, '2013-08-20 00:00:00', 1048, 'N', 0, 0, '1900-12-31 00:00:00', 0, 587,
		 * '2013-08-20 00:00:00',
		 * 1048, 0, '1900-12-31 00:00:00', 0, 'EQUIPAMENTOS LOCADOS ', ' ', 0.00, 1, 'DRIVISON RAFEL
		 * KOHLER - 06152420903', '4733518800',
		 * '08', 0, ' ', '5949A', 3, 'S', '2013-08-20 00:00:00', 587, '1900-12-31 00:00:00', 0.00, ' ',
		 * 2, 0, 0, 0, 0, 0,'N');
		 */
		Long retornoContrato = 0L;
		retornoContrato = ContratoUtils.retornaContratoToRollBack(usu_codemp, usu_codfil,
				activity.getCode());
		if (retornoContrato == 0L)
		{
			Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);

			StringBuffer query = new StringBuffer();
			query.append("insert into usu_t160ctr "
					+ "(usu_impuni, usu_codemp, usu_codfil, usu_numctr, usu_numofi, usu_objctr, usu_datemi, usu_codcli, usu_seqcob,"
					+ "usu_codrep, usu_inivig, usu_fimvig, usu_datini, usu_datfim, usu_diabas, usu_diafix, usu_datuft, usu_filnfv,"
					+ "usu_codsnf, usu_numnfv, usu_codcpg, usu_ultrea, usu_sitctr, usu_codmot, usu_obsmot, usu_obsctr, usu_ctrcom,"
					+ "usu_eximed, usu_prvpos, usu_efepos, usu_codfpg, usu_usuger, usu_datger, usu_horger, usu_libfat, usu_pervig,"
					+ "usu_usumot, usu_datmot, usu_hormot, usu_usucad, usu_datcad, usu_horcad, usu_usualt, usu_datalt, usu_horalt,"
					+ "usu_obscom, usu_obsfin, usu_dscfin, usu_tipctr, usu_clicon, usu_clifon, usu_codfcr, usu_fatanu, usu_ctrcor,"
					+ "usu_tnsser, usu_regctr, usu_susfat, usu_datsus, usu_ususus, usu_dfimsu, usu_periss, usu_emactr, usu_serctr,"
					+ "usu_empori, usu_filori, usu_ctrori, usu_numcgc, usu_coduni, usu_ctragr, usu_liscon, usu_nomcon, usu_intwin )"); //TODO DESFIXCONTRATOS
			//"usu_empori, usu_filori, usu_ctrori, usu_numcgc, usu_coduni, usu_ctragr )");

			query.append("values('" + usu_impuni + "', " + usu_codemp + ", " + usu_codfil + ", :chave, '"
					+ usu_numofi + "', '" + usu_objctr + "', '"
					+ ContratoUtils.retornaDataFormatoSapiens(usu_datemi) + "', " + usu_codcli + ", "
					+ usu_seqcob + ", " + usu_codrep + ", '"
					+ ContratoUtils.retornaDataFormatoSapiens(usu_inivig) + "', '"
					+ ContratoUtils.retornaDataFormatoSapiens(usu_fimvig) + "', '"
					+ ContratoUtils.retornaDataFormatoSapiens(usu_datini) + "', '"
					+ ContratoUtils.retornaDataFormatoSapiens(usu_datfim) + "', " + usu_diabas + ", "
					+ usu_diafix + ", '" + ContratoUtils.retornaDataFormatoSapiens(usu_datuft) + "', "
					+ usu_filnfv + ", '" + usu_codsnf + "', '" + usu_numnfv + "', '" + usu_codcpg
					+ "', '" + ContratoUtils.retornaDataFormatoSapiens(usu_ultrea) + "', '" + usu_sitctr
					+ "', " + usu_codmot + ", '" + usu_obsmot + "', '" + usu_obsctr + "', '" + usu_ctrcom
					+ "', '" + usu_eximed + "', " + usu_prvpos + ", " + usu_efepos + ", " + usu_codfpg
					+ ", " + usu_usuger + ", '" + ContratoUtils.retornaDataFormatoSapiens(usu_datger)
					+ "', " + usu_horger + ", '" + usu_libfat + "', " + usu_pervig + ", " + usu_usumot
					+ ", '" + ContratoUtils.retornaDataFormatoSapiens(usu_datmot) + "', " + usu_hormot
					+ ", " + usu_usucad + ", '" + ContratoUtils.retornaDataFormatoSapiens(usu_datcad)
					+ "', " + usu_horcad + ", " + usu_usualt + ", '"
					+ ContratoUtils.retornaDataFormatoSapiens(usu_datalt) + "', " + usu_horalt + ", '"
					+ usu_obscom + "', '" + usu_obsfin + "', " + usu_dscfin + ", " + usu_tipctr + ", '"
					+ usu_clicon + "', '" + usu_clifon + "', '" + usu_codfcr + "', " + usu_fatanu + ", '"
					+ usu_ctrcor + "', '" + usu_tnsser + "', " + usu_regctr + ", '" + usu_susfat + "', '"
					+ ContratoUtils.retornaDataFormatoSapiens(usu_datsus) + "', " + usu_ususus + ", '"
					+ ContratoUtils.retornaDataFormatoSapiens(usu_dfimsu) + "', " + usu_periss + ", '"
					+ usu_emactr + "', " + usu_serctr + ", " + usu_empori + ", " + usu_filori + ", "
					+ usu_ctrori + ", " + usu_numcgc + ", " + usu_coduni + ", '" + usu_ctragr + "', '"
					+ veioConcorrenteString + "', '" + nomeConcorrente + "', '0') "); //TODO DESFIXCONTRATOS
			//+", "+ usu_numcgc +", "+ usu_coduni +", '"+ usu_ctragr+"' ) ");

			log.debug("[FLUXO CONTRATOS]-" + query.toString());
			retorno = new Long(OrsegupsContratoUtils.safeInsert("SAPIENS", sqlNextKeyContrato,
					query.toString(), 10, 5000));

			if (retorno == 0)
			{
				System.out.println("[FLUXO CONTRATOS]- Novo Contrato: " + query.toString());
				throw new Exception("Não foi possível Cadastrar o contrato");
			}
			else
			{
				ContratoUtils.addContratoToRollBack(usu_codemp, usu_codfil, activity.getCode(), retorno);
				System.out.println("[FLUXO CONTRATOS]-" + ContratoUtils
						.retornaContratoToRollBack(usu_codemp, usu_codfil, activity.getCode()));
				retornoContrato = ContratoUtils.retornaContratoToRollBack(usu_codemp, usu_codfil,
						activity.getCode());
				Long[] empsAtivas = { 1l, 2l, 4l, 6l, 7l, 8l, 12l, 13l, 15l, 17l, 18l, 19l, 21l, 22l,
						24l, 25l, 26l, 27l, 28l, 29l };
				List<Long> empresasAtivas = Arrays.asList(empsAtivas);
				if (empresasAtivas.contains(usu_codemp))
				{
					if (usu_serctr != null && (usu_serctr.equals(1l) || usu_serctr.equals(4l)
							|| usu_serctr.equals(51l) || usu_serctr.equals(54l)))
					{
						Connection conn = null;
						Statement pstm = null;
						ResultSet rs = null;

						try
						{
							conn = PersistEngine.getConnection("SAPIENS");
							pstm = conn.createStatement();

							StringBuilder sql = new StringBuilder("INSERT INTO USU_T160DOC VALUES ("
									+ usu_codemp + "," + usu_codfil + "," + usu_numctr + ",1,'')");
							pstm.executeUpdate(sql.toString());
							sql = new StringBuilder("INSERT INTO USU_T160DOC VALUES (" + usu_codemp + ","
									+ usu_codfil + "," + usu_numctr + ",2,'')");
							pstm.executeUpdate(sql.toString());
							sql = new StringBuilder("INSERT INTO USU_T160DOC VALUES (" + usu_codemp + ","
									+ usu_codfil + "," + usu_numctr + ",3,'')");
							pstm.executeUpdate(sql.toString());
							sql = new StringBuilder("INSERT INTO USU_T160DOC VALUES (" + usu_codemp + ","
									+ usu_codfil + "," + usu_numctr + ",4,'')");
							pstm.executeUpdate(sql.toString());

						}
						catch (SQLException e)
						{
							e.printStackTrace();
							System.out.println(
									"[FLUXO CONTRATOS]- Erro ao inserir documentos do contrato: "
											+ e.getMessage());
						}
						finally
						{
							OrsegupsUtils.closeConnection(conn, pstm, rs);
						}

					}
				}

			}
			System.out.println("[FLUXO CONTRATOS]- Novo Contrato: " + retorno);
		}
		else
		{
			retorno = ContratoUtils.retornaContratoToRollBack(usu_codemp, usu_codfil,
					activity.getCode());
			;
			System.out.println("[FLUXO CONTRATOS]- A tarefa" + activity.getCode()
					+ " já possui um contrato cadastrado : " + retorno);
		}
		return retorno;
	}

	private static Long cadastraNovoPostoT160CVS(String sqlNextKeyPosto, String usu_libfat,
			Long usu_cepiss, Long usu_codemp, Long usu_codfil, Long usu_numctr, Long usu_numpos,
			String usu_codser, String usu_cplcvs, String usu_unimed, Long usu_qtdlim, Long usu_qtdfun,
			BigDecimal usu_qtdcvs, BigDecimal usu_preuni, BigDecimal usu_periss, BigDecimal usu_perirf,
			BigDecimal usu_perins, BigDecimal usu_perpit, BigDecimal usu_percsl, BigDecimal usu_percrt,
			BigDecimal usu_perour, Long usu_ctafin, Long usu_ctared, String usu_codccu,
			GregorianCalendar usu_datini, GregorianCalendar usu_datfim, String usu_tnsser,
			String usu_sitcvs, Long usu_codmot, String usu_obsmot, String usu_obscvs, String usu_endctr,
			String usu_cplctr, Long usu_cepctr, String usu_baictr, String usu_cidctr, String usu_ufsctr,
			String usu_endeft, String usu_cpleft, Long usu_cepeft, String usu_baieft, String usu_cideft,
			String usu_ufseft, BigDecimal usu_vlrmta, BigDecimal usu_vlrmtb, BigDecimal usu_vlrva4,
			BigDecimal usu_vlrva6, BigDecimal usu_vlrva8, BigDecimal usu_vlrvav, BigDecimal usu_vlrvtr,
			BigDecimal usu_vlrvre, Long usu_vlrins, Long usu_vlradn, Long usu_vlrout, Long usu_perout,
			Long usu_vlrluc, BigDecimal usu_vlrtri, Long usu_isspre, Long usu_perpis, Long usu_percof,
			Long usu_perenc, Long usu_vlrbas, Long usu_pertri, Long usu_perluc, Long usu_ptrluc,
			Long usu_filnfv, String usu_codsnf, Long usu_numnfv, String usu_desmon, String usu_iteorc,
			Long usu_vlrenc, Long usu_vlrcal, Long usu_orcuni, Long usu_orcmat, Long usu_orcepi,
			Long usu_orceqp, Long usu_orcvtr, Long usu_orcvre, Long usu_usumotP,
			GregorianCalendar usu_datmot, Long usu_hormot, Long usu_usucadP,
			GregorianCalendar usu_datcad, Long usu_horcad, Long usu_usualt, GregorianCalendar usu_datalt,
			Long usu_horalt, GregorianCalendar usu_ultcpt, String usu_calret, Long usu_prvposP,
			Long usu_regcvs, String usu_susfat, GregorianCalendar usu_dateft, String usu_sigcon,
			GregorianCalendar usu_datsusP, BigDecimal usu_vlrseg, Long usu_ususus,
			GregorianCalendar usu_dfimsu, Long usu_sigemp, Long usu_codrep, GregorianCalendar usu_datapr,
			Long usu_usuapr, Long usu_posori, String usu_dirapr, String usu_gercom, String usu_comins,
			String usu_basiss, String usu_ctrfis, GregorianCalendar usu_datfis, Long usu_usufis,
			Long usu_tarapr, Long usu_codesc, GregorianCalendar usu_datass, GregorianCalendar usu_cptcom,
			Long usu_peresc, String usu_dupadt, String usu_perreg) throws Exception
	{
		long retorno = 0;
		/**
		 * insert into usu_t160cvs
		 * (usu_libfat, usu_cepiss, usu_codemp, usu_codfil, usu_numctr, usu_numpos, usu_codser,
		 * usu_cplcvs, usu_unimed,
		 * usu_qtdlim, usu_qtdfun, usu_qtdcvs, usu_preuni, usu_periss, usu_perirf, usu_perins,
		 * usu_perpit, usu_percsl,
		 * usu_percrt, usu_perour, usu_ctafin, usu_ctared, usu_codccu, usu_datini, usu_datfim,
		 * usu_tnsser, usu_sitcvs,
		 * usu_codmot, usu_obsmot, usu_obscvs, usu_endctr, usu_cplctr, usu_cepctr, usu_baictr,
		 * usu_cidctr, usu_ufsctr,
		 * usu_endeft, usu_cpleft, usu_cepeft, usu_baieft, usu_cideft, usu_ufseft, usu_vlrmta,
		 * usu_vlrmtb, usu_vlrva4,
		 * usu_vlrva6, usu_vlrva8, usu_vlrvav, usu_vlrvtr, usu_vlrvre, usu_vlrins, usu_vlradn,
		 * usu_vlrout, usu_perout,
		 * usu_vlrluc, usu_vlrtri, usu_isspre, usu_perpis, usu_percof, usu_perenc, usu_vlrbas,
		 * usu_pertri, usu_perluc,
		 * usu_ptrluc, usu_filnfv, usu_codsnf, usu_numnfv, usu_desmon, usu_iteorc, usu_vlrenc,
		 * usu_vlrcal, usu_orcuni,
		 * usu_orcmat, usu_orcepi, usu_orceqp, usu_orcvtr, usu_orcvre, usu_usumot, usu_datmot,
		 * usu_hormot, usu_usucad,
		 * usu_datcad, usu_horcad, usu_usualt, usu_datalt, usu_horalt, usu_ultcpt, usu_calret,
		 * usu_prvpos, usu_regcvs,
		 * usu_susfat, usu_dateft, usu_sigcon, usu_datsus, usu_vlrseg, usu_ususus, usu_dfimsu,
		 * usu_sigemp, usu_codrep,
		 * usu_datapr, usu_usuapr, usu_posori, usu_dirapr, usu_gercom, usu_comins, usu_basiss,
		 * usu_ctrfis, usu_datfis,
		 * usu_usufis, usu_tarapr, usu_codesc, usu_datass, usu_cptcom, usu_peresc, usu_dupadt)
		 * values ('S', 0, 18, 2, 337124, 167059, '9002006', 'SERVICOS DE VIGILANCIA ELETRONICA', 'UN',
		 * 0.00000, 0.00000,
		 * 1.00000, 100.00000, 2.50, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 130, 4060, '7241820',
		 * '2013-08-20 00:00:00',
		 * '1900-12-31 00:00:00', '5949I', 'A', 0, ' ', ' ', 'RUA JOAQUIM DE FREITAS', '176', 81730310,
		 * 'BOQUEIRAO',
		 * 'CURITIBA', 'PR', ' ', ' ', 0, ' ', ' ', ' ', 0.00, 100.00, 0.00, 0.00, 0.00, 0.00, 0.00,
		 * 0.00, 0.00, 0.00,
		 * 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, ' ', 0, 1,
		 * 'Serviços - Vigilância Eletrônica',
		 * 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '1900-12-31 00:00:00', 0, 569, '2013-08-20
		 * 00:00:00', 1059,
		 * 569, '2013-08-20 00:00:00', 1065, '1900-12-31 00:00:00', ' ', 0, 13, 'N', '1900-12-31
		 * 00:00:00', ' ', '1900-12-31 00:00:00',
		 * 0.00, 0, '1900-12-31 00:00:00', 0, 86, '1900-12-31 00:00:00', 0, 0, 'N', 'S', 'N', ' ', 'N',
		 * '1900-12-31 00:00:00', 0, 0,
		 * 0, '1900-12-31 00:00:00', '1900-12-31 00:00:00', 0, ' ');
		 */
		//Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);

		List<Double> impostos = preencheImpostos(usu_ufsctr, usu_tnsser, usu_numctr, usu_codemp,
				usu_codfil, usu_codser);
		//public List<Long> preencheImpostos(String usu_ufsctr,String usu_tnsser,Long usu_numctr,Long usu_codemp, Long usu_codfil, String usu_codser) throws SQLException{
		usu_perpit = new BigDecimal(impostos.get(0), MathContext.DECIMAL32);
		usu_percsl = new BigDecimal(impostos.get(1), MathContext.DECIMAL32);
		usu_percrt = new BigDecimal(impostos.get(2), MathContext.DECIMAL32);
		usu_perins = new BigDecimal(impostos.get(3), MathContext.DECIMAL32);
		usu_perirf = new BigDecimal(impostos.get(4), MathContext.DECIMAL32);

		StringBuffer query = new StringBuffer();

		query.append("insert into usu_t160cvs "
				+ "(usu_libfat, usu_cepiss, usu_codemp, usu_codfil, usu_numctr, usu_numpos, usu_codser, usu_cplcvs, usu_unimed,"
				+ " usu_qtdlim, usu_qtdfun, usu_qtdcvs, usu_preuni, usu_periss, usu_perirf, usu_perins, usu_perpit, usu_percsl,"
				+ " usu_percrt, usu_perour, usu_ctafin, usu_ctared, usu_codccu, usu_datini, usu_datfim, usu_tnsser, usu_sitcvs,"
				+ " usu_codmot, usu_obsmot, usu_obscvs, usu_endctr, usu_cplctr, usu_cepctr, usu_baictr, usu_cidctr, usu_ufsctr,"
				+ " usu_endeft, usu_cpleft, usu_cepeft, usu_baieft, usu_cideft, usu_ufseft, usu_vlrmta, usu_vlrmtb, usu_vlrva4,"
				+ " usu_vlrva6, usu_vlrva8, usu_vlrvav, usu_vlrvtr, usu_vlrvre, usu_vlrins, usu_vlradn, usu_vlrout, usu_perout,"
				+ " usu_vlrluc, usu_vlrtri, usu_isspre, usu_perpis, usu_percof, usu_perenc, usu_vlrbas, usu_pertri, usu_perluc,"
				+ " usu_ptrluc, usu_filnfv, usu_codsnf, usu_numnfv, usu_desmon, usu_iteorc, usu_vlrenc, usu_vlrcal, usu_orcuni,"
				+ " usu_orcmat, usu_orcepi, usu_orceqp, usu_orcvtr, usu_orcvre, usu_usumot, usu_datmot, usu_hormot, usu_usucad,"
				+ " usu_datcad, usu_horcad, usu_usualt, usu_datalt, usu_horalt, usu_ultcpt, usu_calret, usu_prvpos, usu_regcvs,"
				+ " usu_susfat, usu_dateft, usu_sigcon, usu_datsus, usu_vlrseg, usu_ususus, usu_dfimsu, usu_sigemp, usu_codrep,"
				+ " usu_datapr, usu_usuapr, usu_posori, usu_dirapr, usu_gercom, usu_comins, usu_basiss, usu_ctrfis, usu_datfis,"
				+ " usu_usufis, usu_tarapr, usu_codesc, usu_datass, usu_cptcom, usu_peresc, usu_dupadt, usu_posarm, usu_perreg)");
		query.append("\n values ('" + usu_libfat + "', " + usu_cepiss + ", " + usu_codemp + ", "
				+ usu_codfil + ", " + usu_numctr + ", :chave, '" + usu_codser + "', '" + usu_cplcvs
				+ "', '" + usu_unimed + "', " + usu_qtdlim + ", " + usu_qtdfun + ", " + usu_qtdcvs + ", "
				+ usu_preuni + ", " + usu_periss + ", " + usu_perirf + ", " + usu_perins + ", "
				+ usu_perpit + ", " + usu_percsl + ", " + usu_percrt + ", " + usu_perour + ", "
				+ usu_ctafin + ", " + usu_ctared + ", '" + usu_codccu + "', '"
				+ ContratoUtils.retornaDataFormatoSapiens(usu_datini) + "', '"
				+ ContratoUtils.retornaDataFormatoSapiens(usu_datfim) + "', '" + usu_tnsser + "', '"
				+ usu_sitcvs + "', " + usu_codmot + ", '" + usu_obsmot + "', '" + usu_obscvs + "', '"
				+ usu_endctr + "', '" + usu_cplctr + "', " + usu_cepctr + ", '" + usu_baictr + "', '"
				+ usu_cidctr + "', '" + usu_ufsctr + "', '" + usu_endeft + "', '" + usu_cpleft + "', "
				+ usu_cepeft + ", '" + usu_baieft + "', '" + NeoUtils.safeOutputString(usu_cideft)
				+ "', '" + NeoUtils.safeOutputString(usu_ufseft) + "', " + usu_vlrmta + ", " + usu_vlrmtb
				+ ", " + usu_vlrva4 + ", " + usu_vlrva6 + ", " + usu_vlrva8 + ", " + usu_vlrvav + ", "
				+ usu_vlrvtr + ", " + usu_vlrvre + ", " + usu_vlrins + ", " + usu_vlradn + ", "
				+ usu_vlrout + ", " + usu_perout + ", " + usu_vlrluc + ", " + usu_vlrtri + ", "
				+ usu_isspre + ", " + usu_perpis + ", " + usu_percof + ", " + usu_perenc + ", "
				+ usu_vlrbas + ", " + usu_pertri + ", " + usu_perluc + ", " + usu_ptrluc + ", "
				+ usu_filnfv + ", '" + usu_codsnf + "', " + usu_numnfv + ", " + usu_desmon + ", '"
				+ usu_iteorc + "', " + usu_vlrenc + ", " + usu_vlrcal + ", " + usu_orcuni + ", "
				+ usu_orcmat + ", " + usu_orcepi + ", " + usu_orceqp + ", " + usu_orcvtr + ", "
				+ usu_orcvre + ", " + usu_usumotP + ", '"
				+ ContratoUtils.retornaDataFormatoSapiens(usu_datmot) + "', " + usu_hormot + ", "
				+ usu_usucadP + ", '" + ContratoUtils.retornaDataFormatoSapiens(usu_datcad) + "', "
				+ usu_horcad + ", " + usu_usualt + ", '"
				+ ContratoUtils.retornaDataFormatoSapiens(usu_datalt) + "', " + usu_horalt + ", '"
				+ ContratoUtils.retornaDataFormatoSapiens(usu_ultcpt) + "', '" + usu_calret + "', "
				+ usu_prvposP + ", " + usu_regcvs + ", '" + usu_susfat + "', '"
				+ ContratoUtils.retornaDataFormatoSapiens(usu_dateft) + "', '" + usu_sigcon + "', '"
				+ ContratoUtils.retornaDataFormatoSapiens(usu_datsusP) + "', " + usu_vlrseg + ", "
				+ usu_ususus + ", '" + ContratoUtils.retornaDataFormatoSapiens(usu_dfimsu) + "', "
				+ usu_sigemp + ", " + usu_codrep + ", '"
				+ ContratoUtils.retornaDataFormatoSapiens(usu_datapr) + "', " + usu_usuapr + ", "
				+ usu_posori + ", '" + usu_dirapr + "', '" + usu_gercom + "', '" + usu_comins + "', '"
				+ usu_basiss + "', '" + usu_ctrfis + "', '"
				+ ContratoUtils.retornaDataFormatoSapiens(usu_datfis) + "', " + usu_usufis + ", "
				+ usu_tarapr + ", " + usu_codesc + ", '"
				+ ContratoUtils.retornaDataFormatoSapiens(usu_datass) + "', '"
				+ ContratoUtils.retornaDataFormatoSapiens(usu_cptcom) + "', " + usu_peresc + ", '"
				+ usu_dupadt + "', 'N', '" + usu_perreg + "')");
		ContratoLogUtils.logInfo("cadastraNovoPostoT160CVS:" + query.toString());
		retorno = new Long(OrsegupsContratoUtils.safeInsert("SAPIENS", sqlNextKeyPosto, query.toString(),
				10, 5000));
		if (retorno == 0)
		{
			System.out.println("[Fluxo Contratos]" + query.toString());
			throw new Exception("Não foi possível Cadastrar o posto do contrato");
		}
		atualizaTransacaoDoContrato(usu_numctr, usu_codemp, usu_codfil, usu_tnsser);
		return retorno;

	}

	/**
	 * USU_T160CHK
	 * 
	 * @param usu_codemp
	 * @param usu_codfil
	 * @param usu_numctr
	 * @param usu_numpos
	 * @param usu_codser
	 * @param usu_itmchk
	 * @param usu_qtdchk
	 * @param usu_datchk
	 * @param usu_stachk
	 * @param usu_seqchk
	 * @param usu_tarfus
	 * @param usu_rmcchk
	 * @throws Exception
	 */
	private static void geraRelacaoChecklistContratoPosto(Long usu_codemp, Long usu_codfil,
			Long usu_numctr, Long usu_numpos, String usu_codser, String usu_itmchk, Long usu_qtdchk,
			GregorianCalendar usu_datchk, String usu_stachk, Long usu_seqchk, Long usu_tarfus,
			Long usu_rmcchk, Long usu_codItm) throws Exception
	{
		/*
		 * insert into usu_t160chk
		 * (usu_codemp,usu_codfil,usu_numctr,usu_numpos,usu_codser,usu_itmchk,usu_qtdchk
		 * ,usu_datchk,usu_stachk,usu_seqchk,usu_tarfus,usu_rmcchk)
		 * values
		 * (usu_codemp,usu_codfil,usu_numctr,usu_numpos,usu_codser,usu_itmchk,usu_qtdchk,usu_datchk
		 * ,usu_stachk,usu_seqchk,usu_tarfus,usu_rmcchk)
		 */
		Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
		StringBuffer query = new StringBuffer();

		query.append("insert into usu_t160chk "
				+ "(usu_codemp,usu_codfil,usu_numctr,usu_numpos,usu_codser,usu_itmchk,"
				+ "usu_qtdchk,usu_datchk,usu_stachk,usu_seqchk,usu_tarfus,usu_rmcchk,usu_coditm)");
		query.append("values (" + usu_codemp + ", " + usu_codfil + ", " + usu_numctr + ", " + usu_numpos
				+ ", '" + usu_codser + "', '" + usu_itmchk + "', " + usu_qtdchk + ", '"
				+ ContratoUtils.retornaDataFormatoSapiens(usu_datchk) + "', '" + usu_stachk + "', "
				+ usu_seqchk + ", " + usu_tarfus + ", " + usu_rmcchk + ", " + usu_codItm + ")");

		log.debug(query.toString());
		PreparedStatement st = null;
		try
		{
			st = connection.prepareStatement(query.toString());
			st.executeUpdate();
			System.out.println("[FLUXO CONTRATOS]-Contrato: " + usu_numctr + " - Posto: " + usu_numpos
					+ " - Checklist: " + usu_seqchk);
		}

		catch (Exception e)
		{
			if (e instanceof WorkflowException)
			{
				throw (WorkflowException) e;
			}
			else
			{
				System.out.println("[FLUXO CONTRATOS]-Erro ao cadastrar checklist no Fluxo de contratos."
						+ e.getMessage());
				throw new Exception(
						"Erro ao cadastrar checklist no Fluxo de contratos. " + e.getMessage());
			}
		}
		finally
		{
			if (st != null)
			{
				try
				{
					st.close();
					connection.close();
				}
				catch (SQLException e)
				{
					System.out.println("[FLUXO CONTRATOS] - Erro ao fechar o statement");
					e.printStackTrace();
				}
			}
		}

	}

	/**
	 * USU_T160EQP
	 * 
	 * @param usu_codemp
	 * @param usu_codfil
	 * @param usu_numctr
	 * @param usu_numpos
	 * @param usu_codser
	 * @param usu_itmchk
	 * @param usu_qtdchk
	 * @param usu_datchk
	 * @param usu_stachk
	 * @param usu_seqchk
	 * @param usu_tarfus
	 * @param usu_rmcchk
	 * @throws Exception
	 */
	private static void geraRelacaoEquipamentoContratoPosto(Long usu_codemp, Long usu_codfil,
			Long usu_numctr, Long usu_numpos, String usu_codser, Long usu_seqeqp, String usu_codpro,
			String usu_codder, Long usu_qtdeqp, Long usu_codins, Long usu_reqeqp, Long usu_rmceqp,
			Long usu_emppro, String usu_tippro, GregorianCalendar usu_datcad, Long usu_codkit,
			Long usu_seqeme, Long usu_numoss, GregorianCalendar usu_datoss, String usu_tipins,
			Long usu_seqkit, Long usu_soleqp, Long usu_seqsol, GregorianCalendar usu_datins,
			Long usu_codfor) throws Exception
	{
		/*--Inseri relação de equipamentos do contrato/posto                                       
		INSERT INTO USU_T160EQP (usu_codemp,usu_codfil,usu_numctr,usu_numpos,usu_seqeqp,usu_codpro,usu_codder,usu_qtdeqp,usu_codser,
		usu_codins,usu_reqeqp,usu_rmceqp,usu_emppro,usu_tippro,usu_datcad,usu_codkit,usu_seqeme,usu_numoss,usu_datoss,usu_tipins, 
		usu_seqkit, usu_soleqp, usu_seqsol,usu_datins) 
		VALUES (aNewEmp,aNewFil,aCtrBar,aNewPos,aSeqEqp,aCodPro,aCodDer,aQtdEqp,aCodSer, 
		aCodIns,aReqEqp,aRmcEqp,aEmpPro,aTipPro,dDatCad,aCodKit,aSeqEme,aNumOss,dDatOss,atipins,aseqkit,asoleqp,aseqsol,dDatIns)
		 */
		Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
		StringBuffer query = new StringBuffer();

		query.append("INSERT INTO USU_T160EQP "
				+ "(usu_codemp, usu_codfil, usu_numctr, usu_numpos, usu_seqeqp, usu_codpro,usu_codder,usu_qtdeqp,usu_codser,"
				+ "usu_codins,usu_reqeqp,usu_rmceqp,usu_emppro,usu_tippro,usu_datcad,usu_codkit,usu_seqeme,usu_numoss,"
				+ "usu_datoss,usu_tipins,usu_seqkit,usu_soleqp,usu_seqsol,usu_datins, usu_codfor) "); //, usu_codfor

		query.append("values (" + usu_codemp + ", " + usu_codfil + ", " + usu_numctr + ", " + usu_numpos
				+ ", " + usu_seqeqp + ", '" + usu_codpro + "', '" + usu_codder + "'," + usu_qtdeqp
				+ ", '" + usu_codser + "', " + usu_codins + ", " + usu_reqeqp + ", " + usu_rmceqp + ", "
				+ usu_emppro + ", '" + usu_tippro + "', '"
				+ ContratoUtils.retornaDataFormatoSapiens(usu_datcad) + "'," + usu_codkit + ", "
				+ usu_seqeme + ", " + usu_numoss + ", '"
				+ ContratoUtils.retornaDataFormatoSapiens(usu_datoss) + "', '" + usu_tipins + "', "
				+ usu_seqkit + ", " + usu_soleqp + ", " + usu_seqsol + ", '"
				+ ContratoUtils.retornaDataFormatoSapiens(usu_datins) + "'," + usu_codfor + " )"); //+ "',"+usu_codfor

		log.info(query.toString());
		//System.out.println("[FLUXO CONTRATOS]-"+query.toString());
		PreparedStatement st = null;
		try
		{
			st = connection.prepareStatement(query.toString());
			st.executeUpdate();
			//System.out.println("[FLUXO CONTRATOS]-Contrato: "+usu_numctr+" - Posto: " +usu_numpos+ " - Equipamento: "+usu_seqeqp);
		}

		catch (Exception e)
		{
			System.out.println("[FLUXO CONTRATOS]-Contrato: Erro ao inserir Equipamento: " + usu_numctr
					+ " - Posto: " + usu_numpos + " - Equipamento: " + usu_codpro);
			if (e instanceof WorkflowException)
			{
				throw (WorkflowException) e;
			}
			else
			{
				System.out.println(
						"[FLUXO CONTRATOS] - Erro ao cadastrar equipamento no Fluxo de contratos."
								+ e.getMessage());
				throw new Exception(
						"Erro ao cadastrar equipamento no Fluxo de contratos. " + e.getMessage());
			}
		}
		finally
		{
			if (st != null)
			{
				try
				{
					st.close();
					connection.close();
				}
				catch (SQLException e)
				{
					System.out.println("[FLUXO CONTRATOS] - Erro ao fechar o statement");
					e.printStackTrace();
				}
			}
		}
	}

	public static String getTransacao(EntityWrapper contrato, EntityWrapper posto, String codigo)
	{
		String codTrans = "";

		String codEmp = NeoUtils.safeOutputString(contrato.findValue("empresa.codfil"));
		String tipCli = NeoUtils.safeOutputString(contrato.findValue("buscaNomeCliente.tipcli"));
		String ufCli = NeoUtils
				.safeOutputString(posto.findValue("enderecoPosto.estadoCidade.estado.sigufs"));

		if (codEmp.equals("18"))
		{
			if (tipCli.equals("F"))
			{
				if (ufCli.equals("SC"))
				{
					codTrans = "5949F";
				}
				else
				{
					codTrans = "6949F";
				}
			}
			else
			{
				if (ufCli.equals("SC"))
				{
					codTrans = "5949A";
				}
				else
				{
					codTrans = "6949A";
				}
			}

		}
		else
		{
			if (tipCli.equals("F"))
			{
				if (ufCli.equals("SC"))
				{
					codTrans = "5949F";
				}
				else
				{
					codTrans = "6949F";
				}
			}

		}

		if (codTrans.equals(""))
			return codigo;
		else
			return codTrans;
		//return codTrans;
	}

	/**
	 * USU_T160CON
	 * 
	 * @param usu_codemp
	 * @param usu_codfil
	 * @param usu_numctr
	 * @param usu_numpos
	 * @param usu_codser
	 * @param usu_itmchk
	 * @param usu_qtdchk
	 * @param usu_datchk
	 * @param usu_stachk
	 * @param usu_seqchk
	 * @param usu_tarfus
	 * @param usu_rmcchk
	 * @throws Exception
	 */
	private static void geraRelacaoContatoContratoPosto(Long usu_codemp, Long usu_codfil,
			Long usu_numctr, Long usu_numpos, String usu_codser, Long usu_seqcon, String usu_nomcon,
			String usu_foncon1, String usu_foncon2, String usu_funcon) throws Exception
	{
		/*
		 * INSERT INTO USU_T160CON
		 * (USU_CODEMP,USU_CODFIL,USU_NUMCTR,USU_NUMPOS,USU_CODSER,USU_SEQCON,USU_NOMCON
		 * ,USU_FONCON1,USU_FONCON2,USU_FUNCON)
		 * VALUES
		 * (usu_codemp,usu_codfil,usu_numctr,usu_numpos,usu_codser,usu_seqcon,usu_nomcon,usu_foncon1
		 * ,usu_foncon2,usu_funcon)
		 */
		Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
		StringBuffer query = new StringBuffer();

		query.append("INSERT INTO USU_T160CON"
				+ " (USU_CODEMP,USU_CODFIL,USU_NUMCTR,USU_NUMPOS,USU_CODSER,USU_SEQCON,USU_NOMCON,USU_FONCON1,USU_FONCON2,USU_FUNCON) ");

		query.append("VALUES (" + usu_codemp + ", " + usu_codfil + ", " + usu_numctr + ", " + usu_numpos
				+ ", '" + usu_codser + "', " + usu_seqcon + ", '" + usu_nomcon + "', '" + usu_foncon1
				+ "', '" + usu_foncon2 + "', '" + usu_funcon + "')");

		log.debug(query.toString());
		PreparedStatement st = null;
		try
		{
			st = connection.prepareStatement(query.toString());
			st.executeUpdate();
			System.out.println("[FLUXO CONTRATOS]-Contrato: " + usu_numctr + " - Posto: " + usu_numpos
					+ " - Contato: " + usu_seqcon + ", inserido com sucesso.");
		}

		catch (Exception e)
		{
			System.out.println("[FLUXO CONTRATOS] - Erro ao cadastrar equipamento no Fluxo de contratos."
					+ e.getMessage());
			ContratoLogUtils.logInfo(query.toString());
			if (e.getMessage().contains("duplicate"))
			{
				ContratoLogUtils.logInfo("Contato já existe para este contrato(" + usu_numctr
						+ ") e posto(" + usu_numpos + ")");
			}
			else
			{
				if (e instanceof WorkflowException)
				{

					throw (WorkflowException) e;
				}
				else
				{
					throw new Exception(
							"Erro ao cadastrar contato no Fluxo de contratos. " + e.getMessage());
				}
			}
		}
		finally
		{
			if (st != null)
			{
				try
				{
					st.close();
					connection.close();
				}
				catch (SQLException e)
				{
					System.out.println("[FLUXO CONTRATOS] - Erro ao fechar o statement");
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * USU_T160KPO
	 * 
	 * @param usu_codemp
	 * @param usu_codfil
	 * @param usu_numctr
	 * @param usu_numpos
	 * @param usu_codser
	 * @param usu_seqcon
	 * @param usu_nomcon
	 * @param usu_foncon1
	 * @param usu_foncon2
	 * @param usu_funcon
	 * @throws Exception
	 */
	private static void geraRelacaoKitContratoPosto(Long usu_codemp, Long usu_codfil, Long usu_numctr,
			Long usu_numpos, String usu_codser, Long usu_seqkit, Long usu_codkit, Long usu_qtdkit,
			Long usu_codins, String usu_tipins) throws Exception
	{
		/*
		 * INSERT INTO USU_T160KPO
		 * (USU_CODEMP,USU_CODFIL,USU_NUMCTR,USU_NUMPOS,USU_CODSER,USU_SEQKIT,USU_CODKIT
		 * ,USU_QTDKIT,USU_CODINS)
		 * VALUES
		 * (usu_codemp,usu_codfil,usu_numctr,usu_numpos,usu_codser,usu_seqkit,usu_codkit,usu_qtdkit
		 * ,usu_codins)
		 */
		Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
		StringBuffer query = new StringBuffer();

		query.append("INSERT INTO USU_T160KPO"
				+ " (USU_CODEMP,USU_CODFIL,USU_NUMCTR,USU_NUMPOS,USU_CODSER,USU_SEQKIT,USU_CODKIT,USU_QTDKIT,USU_CODINS, USU_TIPINS) ");

		query.append("VALUES (" + usu_codemp + ", " + usu_codfil + ", " + usu_numctr + ", " + usu_numpos
				+ ", '" + usu_codser + "', " + usu_seqkit + ", " + usu_codkit + ", " + usu_qtdkit + ", "
				+ usu_codins + ", '" + usu_tipins + "')");

		log.debug(query.toString());
		PreparedStatement st = null;
		try
		{
			st = connection.prepareStatement(query.toString());
			st.executeUpdate();
			System.out.println("[FLUXO CONTRATOS]-Contrato: " + usu_numctr + " - Posto: " + usu_numpos
					+ " - Kit: " + usu_seqkit);
		}
		catch (Exception e)
		{
			if (e instanceof WorkflowException)
			{
				throw (WorkflowException) e;
			}
			else
			{
				System.out.println(
						"[FLUXO CONTRATOS] - Erro ao cadastrar Relacao Kit Contrato Posto no Fluxo de contratos."
								+ e.getMessage());
				throw new Exception(
						"Erro ao cadastrar Relacao Kit Contrato Posto no Fluxo de contratos. "
								+ e.getMessage());
			}
		}
		finally
		{
			if (st != null)
			{
				try
				{
					st.close();
					connection.close();
				}
				catch (SQLException e)
				{
					System.out.println("[FLUXO CONTRATOS] - Erro ao fechar o statement");
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Copia os lançamentos com competencia futura do posto antigo para o posto novo excluindo do antigo
	 * e lançando uma observação na usu_t160obs do posto antigo.
	 * 
	 * @param usu_codemp
	 * @param usu_codfil
	 * @param usu_numctr
	 * @param usu_numposAntigo
	 * @param usu_numposNovo
	 * @param usu_usuGer
	 * @param usu_datGer
	 * @param usu_horGer
	 * @param usu_descObs
	 * @throws Exception
	 */
	public static void transfereBonificacoesPosto(Long usu_codemp, Long usu_codfil, Long usu_numctr,
			Long usu_numposAntigo, Long usu_numposNovo, Long usu_codccuPostoNovo, Long usu_usuGer,
			GregorianCalendar usu_datGer, Long usu_horGer, String usu_descObs) throws Exception
	{
		Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
		StringBuffer query = new StringBuffer();

		Long seqObs = ContratoUtils.retornaSequenciaObservacaoContrato(usu_codemp, usu_codfil,
				usu_numctr);

		/**
		 * Data de competencia ficticia para evitar problema com o comercial privado quando forem pagar
		 * comissão.
		 */
		GregorianCalendar datcptGenerica = new GregorianCalendar();
		datcptGenerica.set(GregorianCalendar.YEAR, 1901);
		datcptGenerica.set(GregorianCalendar.MONTH, 01);
		datcptGenerica.set(GregorianCalendar.DATE, 01);
		datcptGenerica.set(GregorianCalendar.HOUR, 00);
		datcptGenerica.set(GregorianCalendar.MINUTE, 00);
		datcptGenerica.set(GregorianCalendar.SECOND, 00);

		query.append(" INSERT INTO usu_t160cms ");
		query.append(
				"           (usu_codemp,usu_codfil,usu_numctr,usu_numpos,usu_seqmov,usu_datmov,usu_codser ");
		query.append(
				"           ,usu_cplcvs,usu_unimed,usu_qtdfun,usu_qtdcvs,usu_preuni,usu_periss,usu_perirf ");
		query.append(
				"           ,usu_perins,usu_perpit,usu_percsl,usu_percrt,usu_perour,usu_ctafin,usu_ctared ");
		query.append(
				"           ,usu_codccu,usu_datini,usu_datfim,usu_tnsser,usu_usuger,usu_datger,usu_horger ");
		query.append(
				"           ,usu_filnfv,usu_codsnf,usu_numnfv,usu_datcpt,usu_adcsub,usu_obscms,usu_diatra ");
		query.append(
				"           ,usu_posbon,usu_libapo,usu_tipapo,usu_tarfus,usu_cptcom,usu_jusapo,usu_codlan) ");
		query.append(" SELECT " + usu_codemp + "," + usu_codfil + "," + usu_numctr + "," + usu_numposNovo
				+ ", ( (select max(usu_seqmov) from usu_t160cms)+row_number() over(order by usu_seqMov) ),cm.usu_datmov,cm.usu_codser, ");
		query.append(
				"       cm.usu_cplcvs,cm.usu_unimed,cm.usu_qtdfun,cm.usu_qtdcvs,cm.usu_preuni,cm.usu_periss,cm.usu_perirf, ");
		query.append(
				"       cm.usu_perins,cm.usu_perpit,cm.usu_percsl,cm.usu_percrt,cm.usu_perour,cm.usu_ctafin,cm.usu_ctared, ");
		query.append("       " + usu_codccuPostoNovo + ",cm.usu_datini,cm.usu_datfim,cm.usu_tnsser,"
				+ usu_usuGer + ",'" + ContratoUtils.retornaDataFormatoSapiens(usu_datGer) + "',"
				+ usu_horGer + ", ");
		query.append(
				"       cm.usu_filnfv,cm.usu_codsnf,cm.usu_numnfv,cm.usu_datcpt,cm.usu_adcsub,cm.usu_obscms,cm.usu_diatra, ");
		query.append(
				"       cm.usu_posbon,'D',cm.usu_tipapo,0,cm.usu_cptcom,cm.usu_jusapo,cm.usu_codlan ");
		query.append(" from usu_t160cms cm  ");
		query.append(
				" join usu_t160cvs cv on (cm.usu_numctr = cv.usu_numctr and cm.usu_numpos = cv.usu_numpos )  "); // and cv.usu_sitcvs = 'A'
		query.append(" where 1=1  "); // /*--and cm.usu_adcsub = '+' */
		query.append(" and cm.usu_numctr = " + usu_numctr + " and cm.usu_numpos = " + usu_numposAntigo);
		query.append(
				" and cast (left(CONVERT(varchar(12), cm.usu_datcpt, 12),4)  as numeric  ) > cast (left(CONVERT(varchar(12), getDate(), 12),4)  as numeric  ); ");
		query.append("\n ");
		query.append(" update usu_t160cms set usu_libapo='N' , usu_cptcom = '"
				+ ContratoUtils.retornaDataFormatoSapiens(datcptGenerica) + "' where usu_seqmov in (  ");
		query.append("	select cm.usu_seqmov from usu_t160cms cm  ");
		query.append(
				"	join usu_t160cvs cv on (cm.usu_numctr = cv.usu_numctr and cm.usu_numpos = cv.usu_numpos )  "); // and cv.usu_sitcvs = 'A'
		query.append("	where 1=1   ");/*-- and cm.usu_adcsub = '+'*/
		query.append("  and cm.usu_numctr = " + usu_numctr + " and cm.usu_numpos = " + usu_numposAntigo);
		query.append(
				"	and cast (left(CONVERT(varchar(12), cm.usu_datcpt, 12),4)  as numeric  ) > cast (left(CONVERT(varchar(12),getDate(), 12),4)  as numeric  ) ");
		query.append("); ");
		query.append("\n ");
		query.append(" INSERT INTO usu_t160obs ");
		query.append(
				"   (usu_codemp,usu_codfil,usu_numctr,usu_seqobs,usu_tipobs,usu_txtobs,usu_usumov ");
		query.append("   ,usu_datmov,usu_hormov,usu_numrmc,usu_numpos,usu_codser,usu_codope) ");
		query.append("    ");
		query.append("VALUES ");
		query.append("   (" + usu_codemp + "," + usu_codfil + "," + usu_numctr + "," + seqObs
				+ ",'A','Lançamentos com competências futuras transferidos para o posto novo ("
				+ usu_numposNovo + ")'," + usu_usuGer + " ");
		query.append("   ,'" + ContratoUtils.retornaDataFormatoSapiens(usu_datGer) + "'," + usu_horGer
				+ ",0," + usu_numposAntigo + ",'',''); ");

		ContratoLogUtils.logInfo(
				"transfereBonificacoesPosto usu_numctr=" + usu_numctr + ", SQL -> " + query.toString());

		PreparedStatement st = null;
		try
		{
			st = connection.prepareStatement(query.toString());
			st.executeUpdate();
			ContratoLogUtils.logInfo("Contrato: " + usu_numctr + " - Posto Antigo: " + usu_numposAntigo
					+ ", Posto Novo: " + usu_numposNovo);
		}

		catch (Exception e)
		{
			if (e instanceof WorkflowException)
			{
				throw (WorkflowException) e;
			}
			else
			{
				ContratoLogUtils
						.logInfo("Erro ao cadastrar posto no Fluxo de contratos." + e.getMessage());
				System.out.println("SELECT " + usu_codemp + "," + usu_codfil + "," + usu_numctr + ","
						+ usu_numposNovo
						+ ", ( (select max(usu_seqmov) from usu_t160cms)+row_number() over(order by usu_seqMov) ),cm.usu_datmov,cm.usu_codser,");
				throw new Exception(
						"Erro ao transferir Bonificações de competencias futuras para o novo posto. "
								+ e.getMessage());
			}
		}
		finally
		{
			if (st != null)
			{
				try
				{
					st.close();
					connection.close();
				}
				catch (SQLException e)
				{
					ContratoLogUtils.logInfo(" Erro ao fechar o statement. " + e.getMessage());
					e.printStackTrace();
				}
			}

		}
	}

	/**
	 * Busca no sapiens o ultimo contrato cadastrado e incrementa +1
	 * 
	 * @param codEmp
	 * @param empresaPrivada Eform buscaNomeCliente.tipemc ou novoCliente.tipoEmpresa.tipo - tipemc: 1 =
	 *            privado / 2 = publico
	 * @return
	 */
	public static Long geraNumeroContratoNovoSapiens(Long codEmp, Long tipoEmpresa)
	{
		Long numeroContratoNovo = null;

		if (NeoUtils.safeIsNotNull(codEmp, tipoEmpresa))
		{
			Boolean empresaPublica = false;
			Boolean empresaPrivada = false;

			Long vEmpIni = null;
			Long vEmpFim = null;

			if (tipoEmpresa == 1)
			{
				empresaPrivada = true;
			}
			else
			{
				empresaPublica = true;
			}

			if (empresaPrivada)
			{
				if (codEmp.floatValue() == 4)
				{
					vEmpIni = codEmp * 10000;
					vEmpFim = vEmpIni + 14999;
				}
				else if (codEmp.floatValue() == 18)
				{
					vEmpIni = (long) 305000;
					vEmpFim = vEmpIni + 94999;
				}
				else if (codEmp.floatValue() == 19) // adicionado no dia 26/10/2015 - tarefa 657402 
				{
					vEmpIni = codEmp * 11000;
					vEmpFim = vEmpIni + 99999;
				}
				else if (codEmp.floatValue() == 15)
				{
					vEmpIni = (long) 500001;
					vEmpFim = (long) 515000;
				}
				else
				{
					vEmpIni = (codEmp * 10000) + 5000;
					vEmpFim = vEmpIni + 14999;

				}
			}

			if (empresaPublica)
			{
				if (codEmp.floatValue() == 19)
				{
					vEmpIni = codEmp * 10000;
					vEmpFim = vEmpIni + 99999;
				}
				else if (codEmp.floatValue() == 18)
				{
					vEmpIni = (long) 300000;
					vEmpFim = vEmpIni + 4999;
				}
				else
				{
					vEmpIni = codEmp * 10000;
					vEmpFim = vEmpIni + 4999;
				}
			}

			StringBuffer query = new StringBuffer();

			query.append("SELECT USU_NUMCTR FROM USU_T160CTR WHERE ");
			query.append("USU_NUMCTR >= " + vEmpIni + "AND ");
			query.append("USU_NUMCTR <= " + vEmpFim + "AND ");
			query.append("USU_CODEMP = " + codEmp + " ORDER BY USU_NUMCTR DESC");

			ResultSet rs = OrsegupsContratoUtils.getResultSet(query.toString(), nomeFonteDados);

			if (rs != null)
			{
				try
				{
					Long numeroUltimoContrato = rs.getLong("USU_NUMCTR");
					numeroContratoNovo = numeroUltimoContrato + 1;
				}
				catch (SQLException e)
				{
					System.out.println(
							"[FLUXO CONTRATOS] - ERRO: Falha ao verificar USU_T160CTR para geraNumeroContratoNovoSapiens.");
				}
			}
			else
			{
				numeroContratoNovo = vEmpIni;
			}

		}

		return numeroContratoNovo;
	}

	/**
	 * Gera código sql para buscar a chave do proximo contrato. <br>
	 * Esse sql será passado para a função OrsegupsContratoUtils.safeInsert() <br>
	 * na hora de inserir o contrato
	 * 
	 * @param codEmp - Codigo da Empresa
	 * @param empresaPrivada Eform buscaNomeCliente.tipemc ou novoCliente.tipoEmpresa.tipo - tipemc: 1 =
	 *            privado / 2 = publico
	 * @return
	 */
	public static String geraSqlNextKeyContrato(Long codEmp, Long tipoEmpresa)
	{
		String sql = null;

		if (NeoUtils.safeIsNotNull(codEmp, tipoEmpresa))
		{
			Boolean empresaPublica = false;
			Boolean empresaPrivada = false;

			Long vEmpIni = null;
			Long vEmpFim = null;

			if (tipoEmpresa == 1)
			{
				empresaPrivada = true;
			}
			else
			{
				empresaPublica = true;
			}

			if (empresaPrivada)
			{
				if (codEmp.floatValue() == 4)
				{
					vEmpIni = codEmp * 10000;
					vEmpFim = vEmpIni + 14999;
				}
				else if (codEmp.floatValue() == 18)
				{
					vEmpIni = (long) 305000;
					vEmpFim = vEmpIni + 94999;
				}
				else if (codEmp.floatValue() == 19) // adicionado no dia 26/10/2015 - tarefa 657402 
				{
					//vEmpIni = codEmp * 11000;
					//vEmpFim = vEmpIni + 99999;
					// alterado no dia 14/04/2016 por orientação do Thiado.coelho pois estourou o range de contratos.
					vEmpIni = 555000L;
					vEmpFim = 599999L;

				}
				else if (codEmp.floatValue() == 15)
				{
					vEmpIni = (long) 500001;
					vEmpFim = (long) 549999;

				}
				else if (codEmp.floatValue() == 21)
				{
					vEmpIni = (long) 400000;
					vEmpFim = (long) 500000;
				}
				else
				{
					vEmpIni = (codEmp * 10000) + 5000;
					vEmpFim = vEmpIni + 14999;
				}
			}

			if (empresaPublica)
			{
				if (codEmp.floatValue() == 19)
				{
					vEmpIni = codEmp * 10000;
					vEmpFim = vEmpIni + 99999;
				}
				else if (codEmp.floatValue() == 18)
				{
					vEmpIni = (long) 300000;
					vEmpFim = vEmpIni + 4999;
				}
				else
				{
					vEmpIni = codEmp * 10000;
					vEmpFim = vEmpIni + 4999;
				}
			}

			StringBuffer query = new StringBuffer();

			query.append("SELECT isnull( max(USU_NUMCTR)+1, " + vEmpIni + " ) FROM USU_T160CTR WHERE ");
			query.append("USU_NUMCTR >= " + vEmpIni + " AND ");
			query.append("USU_NUMCTR <= " + vEmpFim + " AND ");
			query.append("USU_CODEMP = " + codEmp);

			sql = query.toString();

		}

		return sql;
	}

	/**
	 * Busca o número do ultimo posto inserido USU_T160CVS e incrementa +1
	 * SELECT USU_NUMPOS FROM USU_T160CVS ORDER BY USU_NUMPOS DESC
	 * 
	 * @return numero novo posto
	 */
	public static Long geraNumeroPostoNovoSapiens()
	{
		Long numeroPostoNovo = null;

		StringBuffer query = new StringBuffer();

		query.append("SELECT ISNULL(max(USU_NUMPOS),0) FROM USU_T160CVS");

		ResultSet rs = OrsegupsContratoUtils.getResultSet(query.toString(), nomeFonteDados);

		if (rs != null)
		{
			try
			{
				Long numeroUltimoPosto = rs.getLong(1);
				numeroPostoNovo = numeroUltimoPosto + 1;
			}
			catch (SQLException e)
			{
				System.out.println(
						"[FLUXO CONTRATOS] - ERRO: Falha ao verificar USU_T160CTR para geraNumeroContratoNovoSapiens.");
			}
		}

		return numeroPostoNovo;
	}

	/**
	 * Busca o número do ultimo equipamento inserida USU_T160EQP e incrementa +1
	 * select usu_seqeqp from USU_T160EQP where usu_codemp = 18 and usu_codfil = 1 and usu_numctr =
	 * 331478 oRDER BY usu_seqeqp DESC
	 * 
	 * @param usu_codemp
	 * @param usu_codfil
	 * @param numContrato
	 * @param numPosto
	 * @return próximo número
	 */
	private Long retornaSequenciaEquipamento(Long usu_codemp, Long usu_codfil, Long numContrato,
			Long numPosto)
	{
		Long numeroSeqNovo = new Long(1);

		StringBuffer query = new StringBuffer();

		query.append("SELECT usu_seqeqp FROM USU_T160EQP " + "where usu_codemp = " + usu_codemp
				+ " and usu_codfil = " + usu_codfil + " and usu_numctr = " + numContrato
				+ " and usu_numpos = " + numPosto + " ORDER BY usu_seqeqp DESC");

		ResultSet rs = OrsegupsContratoUtils.getResultSet(query.toString(), nomeFonteDados);

		if (rs != null)
		{
			try
			{
				Long numeroUltimoEqp = rs.getLong("usu_seqeqp");
				numeroSeqNovo = numeroUltimoEqp + 1;
			}
			catch (SQLException e)
			{
				System.out.println(
						"[FLUXO CONTRATOS] - ERRO: Falha ao verificar USU_T160EQP para retornaSequenciaEquipamento.");
			}
		}

		return numeroSeqNovo;
	}

	/**
	 * select usu_seqkit from USU_T160kpo where usu_codemp = 18 and usu_codfil = 1 and usu_numctr =
	 * 331478 and usu_numpos = 150063 oRDER BY usu_seqkit DESC
	 * 
	 * @param usu_codemp
	 * @param usu_codfil
	 * @param numContrato
	 * @param numPosto
	 * @param usu_codser
	 * @return
	 */
	private Long retornaSequenciaKit(Long usu_codemp, Long usu_codfil, Long numContrato, Long numPosto,
			String usu_codser)
	{
		Long numeroSeqNovo = new Long(1);

		StringBuffer query = new StringBuffer();

		query.append("SELECT usu_seqkit FROM USU_T160kpo " + "where usu_codemp = " + usu_codemp
				+ " and usu_codfil = " + usu_codfil + " and usu_numctr = " + numContrato
				+ " and usu_numpos = " + numPosto + " ORDER BY usu_seqkit DESC");

		ResultSet rs = OrsegupsContratoUtils.getResultSet(query.toString(), nomeFonteDados);

		if (rs != null)
		{
			try
			{
				Long numeroUltimoKit = rs.getLong("usu_seqkit");
				numeroSeqNovo = numeroUltimoKit + 1;
			}
			catch (SQLException e)
			{
				System.out.println(
						"[FLUXO CONTRATOS] - ERRO: Falha ao verificar USU_T160kpo para retornaSequenciaKit.");
			}
		}
		return numeroSeqNovo;
	}

	/**
	 * select * from USU_T160CHK where usu_codemp = 18 and usu_codfil = 1 and usu_numctr = 180046 and
	 * usu_numpos = 84031 oRDER BY usu_seqchk DESC
	 * 
	 * @param usu_codemp
	 * @param usu_codfil
	 * @param numContrato
	 * @param numPosto
	 * @return
	 */
	private Long retornaSequenciaChecklist(Long usu_codemp, Long usu_codfil, Long numContrato,
			Long numPosto)
	{
		Long numeroSeqNovo = new Long(1);

		StringBuffer query = new StringBuffer();

		query.append("SELECT usu_seqchk FROM USU_T160CHK " + "where usu_codemp = " + usu_codemp
				+ " and usu_codfil = " + usu_codfil + " and usu_numctr = " + numContrato
				+ " and usu_numpos = " + numPosto + " ORDER BY usu_seqchk DESC");

		ResultSet rs = OrsegupsContratoUtils.getResultSet(query.toString(), nomeFonteDados);

		if (rs != null)
		{
			try
			{
				Long numeroUltimoKit = rs.getLong("usu_seqkit");
				numeroSeqNovo = numeroUltimoKit + 1;
			}
			catch (SQLException e)
			{
				System.out.println(
						"[FLUXO CONTRATOS] ERRO: Falha ao verificar USU_T160kpo para retornaSequenciaKit.");
			}
		}
		return numeroSeqNovo;

	}

	/**
	 * select usu_seqcon from USU_T160CON where usu_codemp = 18 and usu_codfil = 1 and usu_numctr =
	 * 334630 and usu_numpos = 159144 oRDER BY usu_seqcon DESC
	 * 
	 * @param usu_codemp
	 * @param usu_codfil
	 * @param numContrato
	 * @param numPosto
	 * @return
	 */
	private Long retornaSequenciaContato(Long usu_codemp, Long usu_codfil, Long numContrato,
			Long numPosto)
	{
		Long numeroSeqNovo = new Long(1);

		StringBuffer query = new StringBuffer();

		query.append("SELECT usu_seqcon FROM USU_T160CON " + "where usu_codemp = " + usu_codemp
				+ " and usu_codfil = " + usu_codfil + " and usu_numctr = " + numContrato
				+ " and usu_numpos = " + numPosto + " ORDER BY usu_seqcon DESC");

		ResultSet rs = OrsegupsContratoUtils.getResultSet(query.toString(), nomeFonteDados);

		if (rs != null)
		{
			try
			{
				Long numeroUltimoKit = rs.getLong("usu_seqcon");
				numeroSeqNovo = numeroUltimoKit + 1;
			}
			catch (SQLException e)
			{
				System.out.println(
						"[FLUXO CONTRATOS] - ERRO: Falha ao verificar USU_T160CON para retornaSequenciaContato.");
			}
		}
		return numeroSeqNovo;

	}

	public static boolean validaCodigoAgrupadorContrato(EntityWrapper wrapper, String codCli)
	{
		String codEmp = NeoUtils.safeOutputString(wrapper.findValue("empresa.codemp"));
		String codFil = NeoUtils.safeOutputString(wrapper.findValue("empresa.codfil"));

		StringBuilder sql = new StringBuilder();
		sql.append("select ");
		sql.append("cod.usu_ctragr ");
		sql.append("from USU_T160AGR cod ");

		sql.append("where ");
		sql.append("cod.usu_codemp = " + codEmp);
		sql.append(" and cod.usu_codfil = " + codFil);
		sql.append(" and cod.usu_codcli = " + codCli);

		Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		Collection<Object> resultList = query.getResultList();
		if (resultList != null && !resultList.isEmpty())
		{
			return true;
		}
		else
			return false;
	}

	public static Long verificaCadastrodoPosto(Long usu_codemp, Long usu_codfil, Long numContrato)
	{
		StringBuilder query = new StringBuilder();
		Long numeroPosto = 0L;
		query.append("SELECT usu_numpos FROM USU_T160CVS " + "where usu_codemp = " + usu_codemp
				+ " and usu_codfil = " + usu_codfil + " and usu_numctr = " + numContrato
				+ " and USU_SITCVS = 'A'");

		ResultSet rs = OrsegupsContratoUtils.getResultSet(query.toString(), nomeFonteDados);

		if (rs != null)
		{
			try
			{
				Long numpos = rs.getLong("usu_numpos");
			}
			catch (SQLException e)
			{
				System.out.println(
						"[FLUXO CONTRATOS] - ERRO: Falha ao verificar USU_T160CON para retornaSequenciaContato.");
			}
		}
		return numeroPosto;
	}

	/**
	 * Retorna proxima sequencia da tabela usu_t160CMS
	 * 
	 * @return
	 */
	public static Long verificaApontamentoNextCode()
	{
		StringBuilder query = new StringBuilder();
		Long retorno = 0L;
		query.append("select max(ISNULL(USU_SeqMov,0))+1 from USU_T160CMS");

		ResultSet rs = OrsegupsContratoUtils.getResultSet(query.toString(), nomeFonteDados);

		if (rs != null)
		{
			try
			{
				retorno = rs.getLong(1);
				rs.close();
			}
			catch (SQLException e)
			{
				System.out.println(
						"[FLUXO CONTRATOS] - ERRO: Falha ao verificar USU_T160CMS para verificaApontamentoNextCode .");
				return 0L;
			}
		}
		return retorno;
	}

	/**
	 * Insere apontamento na tabela usu_t160cms para cada posto
	 * 
	 * @param usu_codemp
	 * @param usu_codfil
	 * @param usu_numctr
	 * @param usu_numpos
	 * @param usu_seqmov
	 * @param usu_datmov
	 * @param usu_codser
	 * @param usu_cplcvs
	 * @param usu_unimed
	 * @param usu_qtdfun
	 * @param usu_qtdcvs
	 * @param usu_preuni
	 * @param usu_periss
	 * @param usu_perirf
	 * @param usu_perins
	 * @param usu_perpit
	 * @param usu_percsl
	 * @param usu_percrt
	 * @param usu_perour
	 * @param usu_ctafin
	 * @param usu_ctared
	 * @param usu_codccu
	 * @param usu_datini
	 * @param usu_datfim
	 * @param usu_tnsser
	 * @param usu_usuger
	 * @param usu_datger
	 * @param usu_horger
	 * @param usu_filnfv
	 * @param usu_codsnf
	 * @param usu_numnfv
	 * @param usu_datcpt
	 * @param usu_adcsub
	 * @param usu_obscms
	 * @param usu_diatra
	 * @param usu_posbon
	 * @param usu_libapo
	 * @param usu_tipapo
	 * @param usu_tarfus
	 * @param usu_cptcom
	 * @throws Exception
	 */
	private static void InsereApontamenoUSU_T160CMS(Long usu_codemp, Long usu_codfil, Long usu_numctr,
			Long usu_numpos, Long usu_seqmov, GregorianCalendar usu_datmov, String usu_codser,
			String usu_cplcvs, String usu_unimed, BigDecimal usu_qtdfun, BigDecimal usu_qtdcvs,
			BigDecimal usu_preuni, BigDecimal usu_periss, BigDecimal usu_perirf, BigDecimal usu_perins,
			BigDecimal usu_perpit, BigDecimal usu_percsl, BigDecimal usu_percrt, Long usu_perour,
			Long usu_ctafin, Long usu_ctared, String usu_codccu, GregorianCalendar usu_datini,
			GregorianCalendar usu_datfim, String usu_tnsser, Integer usu_usuger,
			GregorianCalendar usu_datger, Long usu_horger, Long usu_filnfv, String usu_codsnf,
			Long usu_numnfv, GregorianCalendar usu_datcpt, String usu_adcsub, String usu_obscms,
			BigDecimal usu_diatra, String usu_posbon, String usu_libapo, String usu_tipapo,
			Long usu_tarfus, GregorianCalendar usu_cptcom, Long usu_codlan) throws Exception
	{
		/**
		 * INSERT INTO usu_t160cms
		 * (usu_codemp,usu_codfil,usu_numctr,usu_numpos,usu_seqmov,usu_datmov,usu_codser
		 * ,usu_cplcvs,usu_unimed,usu_qtdfun,usu_qtdcvs,usu_preuni,usu_periss,usu_perirf
		 * ,usu_perins,usu_perpit,usu_percsl,usu_percrt,usu_perour,usu_ctafin,usu_ctared
		 * ,usu_codccu,usu_datini,usu_datfim,usu_tnsser,usu_usuger,usu_datger,usu_horger
		 * ,usu_filnfv,usu_codsnf,usu_numnfv,usu_datcpt,usu_adcsub,usu_obscms,usu_diatra
		 * ,usu_posbon,usu_libapo,usu_tipapo,usu_tarfus,usu_cptcom)
		 * VALUES
		 * (
		 * <usu_codemp, smallint,>,<usu_codfil, smallint,>,<usu_numctr, int,>,<usu_numpos,
		 * int,>,<usu_seqmov, int,>,<usu_datmov, datetime,>,<usu_codser, varchar(14),>
		 * ,<usu_cplcvs, varchar(250),>,<usu_unimed, varchar(3),>,<usu_qtdfun,
		 * numeric(14,5),>,<usu_qtdcvs, numeric(14,5),>,<usu_preuni, numeric(14,5),>,<usu_periss,
		 * numeric(4,2),>,<usu_perirf, numeric(4,2),>
		 * ,<usu_perins, numeric(4,2),>,<usu_perpit, numeric(4,2),>,<usu_percsl,
		 * numeric(4,2),>,<usu_percrt, numeric(4,2),>,<usu_perour, numeric(4,2),>,<usu_ctafin,
		 * int,>,<usu_ctared, int,>
		 * ,<usu_codccu, varchar(9),>,<usu_datini, datetime,>,<usu_datfim, datetime,>,<usu_tnsser,
		 * varchar(5),>,<usu_usuger, bigint,>,<usu_datger, datetime,>,<usu_horger, int,>
		 * ,<usu_filnfv, smallint,>,<usu_codsnf, varchar(3),>,<usu_numnfv, int,>,<usu_datcpt,
		 * datetime,>,<usu_adcsub, varchar(1),>,<usu_obscms, varchar(2000),>,<usu_diatra, numeric(14,5),>
		 * ,<usu_posbon, varchar(1),>,<usu_libapo, varchar(1),>,<usu_tipapo, varchar(1),>,<usu_tarfus,
		 * int,>,<usu_cptcom, datetime,>
		 * )
		 */
		Connection connection = OrsegupsUtils.getSqlConnection("SAPIENS");

		StringBuffer query = new StringBuffer();

		query.append("INSERT INTO usu_t160cms "
				+ "(usu_codemp,usu_codfil,usu_numctr,usu_numpos,usu_seqmov,usu_datmov,usu_codser "
				+ ",usu_cplcvs,usu_unimed,usu_qtdfun,usu_qtdcvs,usu_preuni,usu_periss,usu_perirf "
				+ ",usu_perins,usu_perpit,usu_percsl,usu_percrt,usu_perour,usu_ctafin,usu_ctared "
				+ ",usu_codccu,usu_datini,usu_datfim,usu_tnsser,usu_usuger,usu_datger,usu_horger "
				+ ",usu_filnfv,usu_codsnf,usu_numnfv,usu_datcpt,usu_adcsub,usu_obscms,usu_diatra "
				+ ",usu_posbon,usu_libapo,usu_tipapo,usu_tarfus,usu_cptcom, usu_codlan) ");
		query.append(" values ( " + usu_codemp + "," + usu_codfil + "," + usu_numctr + "," + usu_numpos
				+ "," + usu_seqmov + ",'" + ContratoUtils.retornaDataFormatoSapiens(usu_datmov) + "','"
				+ usu_codser + "' " + ",'" + usu_cplcvs + "','" + usu_unimed + "'," + usu_qtdfun + ","
				+ usu_qtdcvs + "," + usu_preuni + "," + usu_periss + "," + usu_perirf + " " + ","
				+ usu_perins + "," + usu_perpit + "," + usu_percsl + "," + usu_percrt + "," + usu_perour
				+ "," + usu_ctafin + "," + usu_ctared + " " + ",'" + usu_codccu + "','"
				+ ContratoUtils.retornaDataFormatoSapiens(usu_datini) + "','"
				+ ContratoUtils.retornaDataFormatoSapiens(usu_datfim) + "','" + usu_tnsser + "',"
				+ usu_usuger + ",'" + ContratoUtils.retornaDataFormatoSapiens(usu_datger) + "',"
				+ usu_horger + " " + "," + usu_filnfv + ",'" + usu_codsnf + "'," + usu_numnfv + ",'"
				+ ContratoUtils.retornaDataFormatoSapiens(usu_datcpt) + "','" + usu_adcsub + "','"
				+ usu_obscms + "'," + usu_diatra + " " + ",'" + usu_posbon + "','" + usu_libapo + "','"
				+ usu_tipapo + "'," + usu_tarfus + ",'"
				+ ContratoUtils.retornaDataFormatoSapiens(usu_cptcom) + "', " + usu_codlan + " ) ");

		ContratoLogUtils.logInfo("apontamento: " + query.toString());
		PreparedStatement st = null;
		try
		{
			st = connection.prepareStatement(query.toString());
			st.executeUpdate();
		}

		catch (Exception e)
		{
			if (e instanceof WorkflowException)
			{
				throw (WorkflowException) e;
			}
			else
			{
				System.out.println(
						"[FLUXO CONTRATOS] - Erro ao inserir apontamento no Fluxo de contratos. " + e);
				throw new Exception(
						"Erro ao inserir apontamento no Fluxo de contratos. " + e.getMessage());
			}
		}
		finally
		{
			if (st != null)
			{
				try
				{
					st.close();
					connection.close();
				}
				catch (SQLException e)
				{
					System.out.println("[FLUXO CONTRATOS] - Erro ao fechar o statement");
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Navega pelos postos gerando os apontamentos
	 * 
	 * @param wrapper
	 * @throws Exception
	 */
	public void geraApontamentos(EntityWrapper wrapper, Activity activity) throws Exception
	{
		GregorianCalendar now = null;
		List<NeoObject> listaPostosFusion = (List<NeoObject>) wrapper.findField("postosContrato")
				.getValue();

		NeoUser user = PortalUtil.getCurrentUser();
		Long idUserSapiens = ContratoUtils.retornaCodeUsuarioSapiens(user.getCode());

		for (NeoObject postoFusion : listaPostosFusion)
		{
			EntityWrapper wPosto = new EntityWrapper(postoFusion);

			// só faz a inserção se o valor de instalação for maior que zero
			if (wPosto.findValue("vigilanciaEletronica.valorInstalacao") != null
					&& ((BigDecimal) wPosto.findValue("vigilanciaEletronica.valorInstalacao"))
							.compareTo(new BigDecimal(0)) > 0)
			{

				now = new GregorianCalendar();

				Long usu_codemp = (Long) wrapper.findField("empresa.codemp").getValue();
				Long usu_codfil = (Long) wrapper.findField("empresa.codfil").getValue();
				Long usu_numctr = (Long) wrapper.findField("dadosGeraisContrato.numeroContratoSapiens")
						.getValue();
				if (usu_numctr == null || usu_numctr == 0)
				{
					usu_numctr = (Long) wrapper.findValue("numContrato.usu_numctr");
				}
				Long usu_numpos = (Long) wPosto.findField("numPosto").getValue();
				Long usu_seqmov = 0L; // nao usado pois é gerado dinamicamente dentro do insert
				GregorianCalendar usu_datmov = new GregorianCalendar();
				String usu_codser = "9002012"; // definido que será sempre este para eletronica
				String usu_codser_obs = (String) wPosto.findValue("complementoServico.codser");
				// alterado via solicitação da tarefa simples 327357
				String usu_cplcvs = "Instalação";//(String) wPosto.findField("complementoServico.desser").getValue();

				String usu_unimed = NeoUtils
						.safeOutputString(wPosto.findValue("complementoServico.unimed")); //wPosto.findValue("complementoServico") != null ?	(String) wPosto.findValue("complementoServico.unimed") : "";
				ContratoLogUtils.logInfo("usu_unimed(usu_t160cms)"
						+ NeoUtils.safeOutputString(wPosto.findValue("complementoServico.unimed")));

				BigDecimal usu_qtdfun = (BigDecimal) (wPosto.findValue("qtdeDeFuncionarios") == null
						? new BigDecimal(0)
						: new BigDecimal((String.valueOf(wPosto.findValue("qtdeDeFuncionarios")))));

				BigDecimal usu_qtdcvs = new BigDecimal(1); // qtde do servico
				
				Boolean cartaoRFID = wrapper.findGenericValue("dadosGeraisContrato.adereCartaoRFID");
				String tipoPosto = NeoUtils.safeOutputString(wPosto.findValue("tipoPosto.codigo"));
				if (tipoPosto.equals("3") && cartaoRFID)
				{
					usu_qtdcvs = new BigDecimal((String.valueOf(wrapper.findGenericValue("dadosGeraisContrato.qtCartaoRFID"))));
				}
				
				BigDecimal usu_preuni = (BigDecimal) wPosto
						.findField("vigilanciaEletronica.valorInstalacao").getValue(); // preco unitario do sevico 
				BigDecimal usu_periss = new BigDecimal(0); // percentual de iss do servico
				//Long usu_perirf = 0L; // percentual do irrf do servico
				//Long usu_perins = 0L; // percntual do inss do servico
				//Long usu_perpit = 0L; // percenutual do pis retido
				//Long usu_percsl = 0L; // percentual do csll do retido
				//Long usu_percrt = 0L; // percentual do Cofins retido
				Long usu_perour = 0L; // percentual de outras retencoes

				Long movimentoContrato = (Long) wrapper.findValue("movimentoContratoNovo.codTipo");
				Long key = GregorianCalendar.getInstance().getTimeInMillis();

				BigDecimal usu_perirf = new BigDecimal(0);
				BigDecimal usu_perins = new BigDecimal(0);
				BigDecimal usu_perpit = new BigDecimal(0);
				BigDecimal usu_percsl = new BigDecimal(0);
				BigDecimal usu_percrt = new BigDecimal(0);

				if (movimentoContrato.equals(ALTERACAO_TERMO_ADITIVO))
				{

					usu_perirf = new BigDecimal(0);
					usu_perins = new BigDecimal(0);
					usu_perpit = new BigDecimal(0);
					usu_percsl = new BigDecimal(0);
					usu_percrt = new BigDecimal(0);

					Long numContratoOrigem = (Long) wrapper.findValue("numContrato.usu_numctr");
					ContratoLogUtils.logInfo(
							"[" + key + "] - Termo aditivo, consultando impostos do contrato original ("
									+ numContratoOrigem + ")");
					BigDecimal impostosPostoAntigo[] = ContratoUtils.retornaImpostos(
							NeoUtils.safeOutputString(numContratoOrigem),
							NeoUtils.safeOutputString(usu_numpos));

					if (impostosPostoAntigo != null)
					{
						ContratoLogUtils.logInfo("[" + key + "] - Termo aditivo, Impostos encontrados.");
						usu_perirf = impostosPostoAntigo[0];
						usu_perins = impostosPostoAntigo[1];
						usu_perpit = impostosPostoAntigo[2];
						usu_percsl = impostosPostoAntigo[3];
						usu_percrt = impostosPostoAntigo[4];
						usu_periss = impostosPostoAntigo[5];
					}
					else
					{
						ContratoLogUtils.logInfo("[" + key
								+ "] - Termo aditivo, Impostos não encontrados para o contrato,posto ("
								+ usu_numctr + "," + usu_numpos + ")");
					}

				}

				Long usu_ctafin = NeoUtils.safeLong(NeoUtils
						.safeOutputString(wPosto.findValue("postosContrato.contaFinanceira.ctafin"))); // conta financeira reduzida
				Long usu_ctared = (Long) wPosto.findField("postosContrato.contaContabil.ctared")
						.getValue(); // conta contabil reduzida
				String usu_codccu = (String) wPosto.findField("postosContrato.codccu").getValue();
				ContratoLogUtils.logInfo("APONTAMENTOS CC do Posto " + usu_numpos + " = " + usu_codccu);
				//GregorianCalendar usu_datini = NeoCalendarUtils.stringToDate("31/12/1900"); // Data do inicio do processamento das parcelas a nivel de item

				GregorianCalendar usu_datini = wPosto.findValue("periodoFaturamento.de") != null
						? (GregorianCalendar) wPosto.findField("periodoFaturamento.de").getValue()
						: NeoCalendarUtils.stringToDate("31/12/1900"); //Será informado pelo executivo

				NeoObject tipoLancamento = (NeoObject) wPosto
						.findValue("periodoFaturamento.tipoLancamentoApontamentos");
				Long usu_codlan = null;
				if (tipoLancamento != null)
				{
					EntityWrapper wTipoLancamento = new EntityWrapper(tipoLancamento);
					usu_codlan = (Long) wTipoLancamento.findValue("usu_codlan");
				}

				GregorianCalendar usu_datfim = NeoCalendarUtils.stringToDate("31/12/1900"); // Data do Fim do processamento das parcelas a nivel de item
				String usu_tnsser = wPosto.findField("transacao.codtns").getValue() != null
						? (String) wPosto.findField("transacao.codtns").getValue()
						: null; //Será informado pelo usuário da análise
				Integer usu_usuger = (idUserSapiens != null ? idUserSapiens.intValue() : (Integer) null); // usuario responsavel pela geracao do registro
				GregorianCalendar usu_datger = now; // data da geracao do registro
				Long usu_horger = NeoUtils.safeLong(ContratoUtils.retornaHora(now)); // hora da geracao
				Long usu_filnfv = 0L; // codigo da filial da NF de saida
				String usu_codsnf = ""; // serie da nf gerada
				Long usu_numnfv = 0L; // numero da nf gerada
				GregorianCalendar usu_datcpt = NeoCalendarUtils.stringToDate("31/12/1900"); // mes e ano da competencia para faturamento
				String usu_adcsub = "+"; //adiciona ou subtrai
				String usu_obscms = (String) wPosto.findField("vigilanciaEletronica.observacao")
						.getValue(); // Obs do movimento
				BigDecimal usu_diatra = new BigDecimal(0); //Dias trabalhados no mes 
				String usu_posbon = "N"; //Postos bonificados (S/N)
				String usu_libapo = "S"; // Liberado apontamnto (D - Dgitado, E - Em analise, S - Aprovado, N - Nao Aprovado) - Quando for mão de obra de instalação deve ir como S 
				String usu_tipapo = "C"; // Tipo de apontamento (M - Medicao, C - Comissionamento)
				Long usu_tarfus = 0L; // é para ir vazio pois é preenchida por outra tarefa do fusion
				GregorianCalendar usu_cptcom = NeoCalendarUtils.stringToDate("31/12/1900"); // Competencia da comissao

				Integer parcelas = 1;
				if (wPosto.findValue("postosContrato.vigilanciaEletronica.parcelas.codigo") != null)
				{
					parcelas = ((Long) wPosto
							.findValue("postosContrato.vigilanciaEletronica.parcelas.codigo"))
									.intValue();
				}

				BigDecimal val_parc = usu_preuni.divide(new BigDecimal(parcelas), 5,
						RoundingMode.HALF_UP);
				String obsComplemento = "";
				//usu_datger
				for (int p = 1; p <= parcelas; p++)
				{
					if (!parcelas.equals(1))
					{
						obsComplemento = ". Parcela " + p + " de " + parcelas.intValue();
					}
					else
					{
						obsComplemento = "";
					}

					usu_datcpt = now;
					usu_datcpt.add(GregorianCalendar.MONTH, 1);
					usu_datcpt.set(GregorianCalendar.DAY_OF_MONTH, 1);
					//

					usu_seqmov = verificaApontamentoNextCode();
					if (usu_seqmov != 0)
					{
						System.out.println("[FLUXO CONTRATOS] - USU_T160CMS -> Contrato: " + usu_numctr
								+ "Cod_Servico: " + usu_codser + ", Transação: " + usu_tnsser
								+ ", ccu_nivel_8: " + usu_codccu + ", Conta Reduzida: " + usu_ctared
								+ " Cta_Fin: " + usu_ctafin + ", cpt: "
								+ ContratoUtils.retornaDataFormatoSapiens(usu_cptcom));
						BigDecimal impostosPosto[] = ContratoUtils.retornaImpostos(
								NeoUtils.safeOutputString(usu_numctr),
								NeoUtils.safeOutputString(usu_numpos));
						usu_perirf = impostosPosto[0];//IRF do posto!
						usu_perins = impostosPosto[1];//INSS do posto!
						usu_perpit = impostosPosto[2];//PIS do posto!
						usu_percsl = impostosPosto[3];//CSL do posto!
						usu_percrt = impostosPosto[4];//COFINS do posto!
						usu_periss = impostosPosto[5];//ISS do posto!

						InsereApontamenoUSU_T160CMS(usu_codemp, usu_codfil, usu_numctr, usu_numpos,
								usu_seqmov, usu_datmov, usu_codser, usu_cplcvs, usu_unimed, usu_qtdfun,
								usu_qtdcvs, val_parc, usu_periss, usu_perirf, usu_perins, usu_perpit,
								usu_percsl, usu_percrt, usu_perour, usu_ctafin, usu_ctared, usu_codccu,
								usu_datini, usu_datfim, usu_tnsser, usu_usuger, usu_datger, usu_horger,
								usu_filnfv, usu_codsnf, usu_numnfv, usu_datcpt, usu_adcsub,
								usu_obscms + obsComplemento, usu_diatra, usu_posbon, usu_libapo,
								usu_tipapo, usu_tarfus, usu_cptcom, usu_codlan);

						//OBSERVACAO - HISTÓRICO
						String usu_txtobs = "lançamento de instalação do posto ";
						Long usu_seqobs = ContratoUtils.retornaSequenciaObservacaoContrato(usu_codemp,
								usu_codfil, usu_numctr);

						Long usu_usumov = new Long(String.valueOf(usu_usuger));
						ContratoUtils.geraObservacao(usu_codemp, usu_codfil, usu_numctr, usu_seqobs,
								usu_numpos, usu_codser_obs, "A", usu_txtobs + obsComplemento, usu_usumov,
								usu_datmov, usu_horger, "");

					}
					else
					{
						throw new Exception("Erro ao buscar sequncia para os novos apontamentos ");
					}

				}

			}

		}
		//if (true) throw new WorkflowException("me remova exception");
	}

	/**
	 * Atualiza no sapiens os dados alterados pelo comercial na validação do contrato (atividade do
	 * comercial).
	 * 
	 * @throws Exception
	 */
	public void atualizaDadosContrato(long codemp, long codfil, long numctr, Long usu_seqCob)
			throws Exception
	{
		StringBuilder query = new StringBuilder();
		query.append(" UPDATE usu_T160CTR SET ");
		query.append(" usu_seqCob= " + usu_seqCob + " "); // int
		query.append(" WHERE usu_codemp = " + codemp + " and usu_codfil = " + codfil
				+ " and usu_numctr = " + numctr + " and usu_sitctr = 'A'");

		Query queryExecute = PersistEngine.getEntityManager("SAPIENS")
				.createNativeQuery(query.toString());
		try
		{
			queryExecute.executeUpdate();
		}
		catch (Exception e)
		{
			ContratoLogUtils.logInfo(
					" Erro ao Atualizar Contrato -> atualizaDadosContrato(codemp, codfil,numctr,seqcob) "
							+ e.getMessage());
			throw new Exception(
					"Erro ao Atualizar Contrato -> atualizaDadosContrato(codemp, codfil,numctr,seqcob) "
							+ e.getMessage());
		}

	}

	/**
	 * Atualiza no sapiens os dados alterados pelo comercial na validação do contrato (atividade do
	 * comercial).
	 * 
	 * @throws Exception
	 */

	public void atualizaDadosContrato(long codemp, long codfil, long numctr,
			GregorianCalendar usu_datemi, String usu_numofi, Long usu_regctr, String usu_objctr,
			String usu_obscom, Long usu_codrep, String usu_tnsser, GregorianCalendar usu_inivig,
			GregorianCalendar usu_fimvig, Long usu_pervig, GregorianCalendar usu_datini,
			GregorianCalendar usu_datfim, Long usu_diabas, Long usu_diafix, String usu_ctrcom,
			String usu_eximed, Long usu_prvpos, Long usu_efepos, String usu_libfat, Long usu_tipctr,
			Long usu_serctr, GregorianCalendar usu_datsus, String usu_susfat, Long usu_seqCob,
			Long usu_codfen, Long usu_codcli) throws Exception
	{
		StringBuilder query = new StringBuilder();
		query.append(" UPDATE usu_T160CTR SET ");
		query.append("usu_datemi= ?,"); // DT
		query.append("usu_numofi= ?,"); // STR
		query.append("usu_regctr= ?,"); // int
		query.append("usu_objctr= ?,"); // STR
		query.append("usu_obscom= ?,"); // STR
		query.append("usu_codrep= ?,"); // int
		query.append("usu_tnsser= ?,"); // STR
		query.append("usu_inivig= ?,"); // DT
		query.append("usu_fimvig= ?,"); // DT
		query.append("usu_pervig= ?,"); // int
		query.append("usu_datini= ?,"); // DT
		query.append("usu_datfim= ?,"); // DT
		query.append("usu_diabas= ?,"); // int
		query.append("usu_diafix= ?,"); // int
		query.append("usu_ctrcom= ?,"); // STR
		query.append("usu_eximed= ?,"); // STR
		query.append("usu_prvpos= ?,"); // int
		query.append("usu_efepos= ?,"); // int
		query.append("usu_libfat= ?,"); // STR
		query.append("usu_tipctr= ?,"); // int
		query.append("usu_serctr= ?,"); // int
		query.append("usu_susfat= ?,");
		query.append("usu_datsus= ?,");
		query.append("usu_seqCob= ?,"); // int
		query.append("usu_codfen= ?"); // int

		query.append(" WHERE usu_codemp = ? and usu_codfil = ? and usu_numctr = ? and usu_sitctr = 'A'");
		PreparedStatement st = null;
		Connection connection = null;

		/*
		 * Query queryExecute =
		 * PersistEngine.getEntityManager("SAPIENS").createNativeQuery(query.toString());
		 * try
		 * {
		 * queryExecute.executeUpdate();
		 * }catch(Exception e)
		 * {
		 * throw new WorkflowException("Erro ao Atualizar Contrato -> atualizaDadosContrato() ");
		 * }
		 */
		try
		{
			String nomeFonteDados = "SAPIENS";

			connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
			st = connection.prepareStatement(query.toString());
			st.setString(1, ContratoUtils.retornaDataFormatoSapiens(usu_datemi));
			st.setString(2, usu_numofi);
			st.setLong(3, usu_regctr);
			st.setString(4, usu_objctr);
			st.setString(5, usu_obscom);
			st.setLong(6, usu_codrep);
			st.setString(7, usu_tnsser);
			st.setString(8, ContratoUtils.retornaDataFormatoSapiens(usu_inivig));
			st.setString(9, ContratoUtils.retornaDataFormatoSapiens(usu_fimvig));
			st.setLong(10, usu_pervig);
			st.setString(11, ContratoUtils.retornaDataFormatoSapiens(usu_datini));
			st.setString(12, ContratoUtils.retornaDataFormatoSapiens(usu_datfim));
			st.setLong(13, usu_diabas);
			st.setLong(14, usu_diafix);
			st.setString(15, usu_ctrcom);
			st.setString(16, usu_eximed);
			st.setLong(17, usu_prvpos);
			st.setLong(18, usu_efepos);
			st.setString(19, usu_libfat);
			st.setLong(20, usu_tipctr);
			st.setLong(21, usu_serctr);
			st.setString(22, usu_susfat);
			st.setString(23, ContratoUtils.retornaDataFormatoSapiens(usu_datsus));
			st.setLong(24, 1l);
			st.setLong(25, usu_codfen);
			st.setLong(26, codemp);
			st.setLong(27, codfil);
			st.setLong(28, numctr);

			st.executeUpdate();

		}
		catch (SQLException e)
		{
			System.out.println("[FLUXO CONTRATOS] - ERRO " + query.toString());
			e.printStackTrace();
			throw new Exception(
					"Erro ao integrar com o Sapiens. Favor tentar novamente em instantes. Oitavo nível do Centro de Custo");
		}
		finally
		{
			OrsegupsUtils.closeConnection(connection, st, null);
			;
		}

		/* INICIO - VALIDAÇÃO TAREFA 555049 - GRUPO DO CLIENTE */

		/*
		 * if ((codemp == 18) && (codfil == 2)) {
		 * if ((usu_diabas != 5) || (usu_diabas != 8)) {
		 * codGre = 0;
		 * } else {
		 * codGre = 100;
		 * }
		 * queryCli.append(" UPDATE e085CLI SET codGre = " + codGre + " where codcli = " + usu_codcli);
		 * Query queryExecuteCli =
		 * PersistEngine.getEntityManager("SAPIENS").createNativeQuery(queryCli.toString());
		 * System.out.print(queryCli.toString());
		 * try {
		 * queryExecuteCli.executeUpdate();
		 * } catch (Exception e) {
		 * throw new Exception("Erro ao Alterar o Grupo do cliente -> atualizaDadosContrato(): " +
		 * e.getMessage());
		 * }
		 * queryCli = new StringBuilder();
		 * }
		 */

		if (usu_codfen == 6)
		{
			if (codemp == 18)
			{
				if (codfil != 2)
				{
					throw new Exception(
							"Forma de Envio igual a 6 só pode ser utilizada na Empresa/Filial (18/2)");
				}
			}
			else
			{
				throw new Exception(
						"Forma de Envio igual a 6 só pode ser utilizada na Empresa/Filial (18/2)");
			}
		}

		/* FIM - VALIDAÇÃO TAREFA 555049 - GRUPO DO CLIENTE */

		//			queryCli.append(" UPDATE e085CLI SET codGre = "+codGre+" where codcli = "+ usu_codcli);
		//			Query queryExecuteCli = PersistEngine.getEntityManager("SAPIENS")
		//					.createNativeQuery(queryCli.toString());
		//
		//			System.out.print(queryCli.toString());
		//
		//			try {
		//				queryExecuteCli.executeUpdate();
		//			} catch (Exception e) {
		//				throw new WorkflowException(
		//						"Erro ao Alterar o Grupo do cliente -> atualizaDadosContrato()");
		//			}
		//			
	}

	/**
	 * Cadastra cliente em operação de alteração de contrato
	 * 
	 * @param wrapper
	 * @return
	 */
	/*
	 * private static String cadastraNovoCliente(EntityWrapper wrapper) {
	 * Long codMov =
	 * NeoUtils.safeLong(NeoUtils.safeOutputString(wrapper.findValue("movimentoContratoNovo.codTipo")));
	 * NeoObject novoCliente = (NeoObject) wrapper.findValue("buscaNomeCliente");
	 * String codCli = null;
	 * Long codCliAntigo = (Long) wrapper.findValue("buscaNomeCliente.codcli");
	 * String tipoCli = "";
	 * if(wrapper.findValue("novoCliente.tipocliente") != null)
	 * tipoCli =
	 * NeoUtils.safeString(NeoUtils.safeOutputString(wrapper.findField("novoCliente.tipocliente.tipo"
	 * ).getValue()));
	 * Long codEmpresa = (Long) wrapper.findField("empresa.codemp").getValue();
	 * Long codFilial = (Long) wrapper.findField("empresa.codfil").getValue();
	 * if(novoCliente != null && (codMov == 5) )
	 * {
	 * if (tipoCli != null && tipoCli.equals("F")){
	 * NeoObject tipoEmpresa =
	 * PersistEngine.getObject(AdapterUtils.getEntityClass("FGCClientesTipoEmpresa"), new
	 * QLEqualsFilter("tipo", (long) 1 ));
	 * wrapper.findField("novoCliente.tipoEmpresa").setValue(tipoEmpresa);
	 * }else if (tipoCli != null && tipoCli.equals("J")){
	 * NeoObject tipoEmpresa =
	 * PersistEngine.getObject(AdapterUtils.getEntityClass("FGCClientesTipoEmpresa"), new
	 * QLEqualsFilter("tipo", (long) 1 ));
	 * wrapper.findField("novoCliente.tipoEmpresa").setValue(tipoEmpresa);
	 * }
	 * NeoObject noCliente = (NeoObject) wrapper.findField("novoCliente").getValue();
	 * NeoObject noDadosContrato = (NeoObject) wrapper.findField("dadosGeraisContrato").getValue();
	 * //Chama o SID para alterar a empresa e a filial
	 * ContractSIDClient.sidChangeEmpFil(NeoUtils.safeOutputString(codEmpresa),
	 * NeoUtils.safeOutputString(codFilial));
	 * //Cadastra um novo cliente para a empresa selecionada
	 * EntityWrapper ewCliente = new EntityWrapper(noCliente);
	 * ewCliente.findField("codigoCliente").setValue((Object) null);
	 * codCli = ContractSIDClient.sidClient(wrapper, noCliente, NeoUtils.safeOutputString(codEmpresa),
	 * NeoUtils.safeOutputString(codFilial),true);
	 * if (codCli != null)
	 * {
	 * ContractSIDClient.sidClientDefinitions(noDadosContrato, codCli, 0,
	 * NeoUtils.safeOutputString(codEmpresa), NeoUtils.safeOutputString(codFilial));
	 * ContractSIDClient.sidClientContact(noCliente, codCli, NeoUtils.safeOutputString(codEmpresa),
	 * NeoUtils.safeOutputString(codFilial));
	 * }
	 * Collection<NeoObject> objClienteSapiens =
	 * (Collection<NeoObject>)PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENS_Clientes"),
	 * new QLEqualsFilter("codcli", NeoUtils.safeLong(codCli)));
	 * if(objClienteSapiens != null && objClienteSapiens.size() > 0)
	 * {
	 * for(NeoObject obj : objClienteSapiens)
	 * {
	 * wrapper.setValue("buscaNomeCliente", obj);
	 * wrapper.findField("novoCliente.codigoCliente").setValue(codCli);
	 * }
	 * }
	 * }
	 * fixCopiaCliente(codCliAntigo, Long.parseLong(codCli));
	 * inativaCliente(codCliAntigo);
	 * return codCli;
	 * }
	 */

	/**
	 * Em casos de duplicação do cadastro do cliente por alteração de CNPJ, copia os dados que o sid não
	 * consegue manipular do cliente antigo para novo
	 * 
	 * @param codCliAntigo
	 * @param CodCliNovo
	 */
	public static void fixCopiaCliente(Long codCliAntigo, Long CodCliNovo)
	{
		String query = " update e085cli  set " + " e085cli.insEnt     = c2.insEnt, "
				+ " e085cli.entCor     = c2.entCor, " + " e085cli.cliFor     = c2.cliFor, "
				+ " e085cli.nenCob     = c2.nenCob, " + " e085cli.natRet     = c2.natRet, "
				+ " e085cli.usu_pesCon = c2.usu_pesCon, " + " e085cli.usu_pescob = c2.usu_pescob, "
				+ " e085cli.usu_cgcmat = c2.usu_cgcmat, " + " e085cli.usu_tipcli = c2.usu_tipcli  "
				+ " from e085cli  inner join e085cli c2 on c2.codcli = " + codCliAntigo
				+ " and e085cli.codcli =" + CodCliNovo;

		Connection connection = OrsegupsUtils.getSqlConnection("SAPIENS");

		PreparedStatement st = null;
		try
		{
			if (query != null && !query.equals(""))
			{
				st = connection.prepareStatement(query);
				System.out.println("[FLUXO CONTRATOS]- fixCopiaCliente->" + st.executeUpdate());
			}
		}

		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Erro ao fixar Copia do cliente " + codCliAntigo + " para "
					+ CodCliNovo + " ." + e.getMessage());
		}
		finally
		{
			if (st != null)
			{
				try
				{
					st.close();
					connection.close();
				}
				catch (SQLException e)
				{
					e.printStackTrace();
				}
			}
		}

	}

	/**
	 * Inativa o cliente na tabela e085cli
	 * 
	 * @param codcli
	 * @throws Exception
	 */
	public static void inativaCliente(Long codCli) throws Exception
	{
		String query = "update e085cli set sitCli='I' where codcli =" + codCli;

		Connection connection = OrsegupsUtils.getSqlConnection("SAPIENS");

		PreparedStatement st = null;
		try
		{
			if (query != null && !query.equals(""))
			{
				st = connection.prepareStatement(query);
				System.out.println("[FLUXO CONTRATOS]- Inativar Cliente - " + st.executeUpdate());
			}
		}

		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception("Erro ao inativar o cliente  " + codCli + ". " + e.getMessage());
		}
		finally
		{
			if (st != null)
			{
				try
				{
					st.close();
					connection.close();
				}
				catch (SQLException e)
				{
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Cadastra nova ávore de centro de custo identica à que o cliente já possuia no cnpj antigo
	 * 
	 * @param wrapper
	 * @return
	 * @throws Exception
	 */
	public static String criarNovoCentroCusto(EntityWrapper wrapper) throws Exception
	{
		String retorno = null;

		ContratoIntegracaoSapiens integrator = ContratoIntegracaoSapiens.getInstance();
		integrator.insert(wrapper.getObject());

		return retorno;
	}

	private Long cadastraNovoContrato(EntityWrapper wrapper, String codCli)
	{
		// TODO Auto-generated method stub
		return null;
	}

	public static List<Double> preencheImpostos(String usu_ufsctr, String usu_tnsser, Long usu_numctr,
			Long usu_codemp, Long usu_codfil, String usu_codser) throws SQLException
	{
		List<Double> impostos = new ArrayList<Double>();

		String sqlContrato = "SELECT usu_codcli FROM USU_T160CTR C WHERE C.USU_NUMCTR = " + usu_numctr;
		ResultSet rs2 = OrsegupsContratoUtils.getResultSet(sqlContrato.toString(), "SAPIENS");

		String sqlCliente = "SELECT sigufs,tipcli from E085CLI where codcli ="
				+ rs2.getLong("usu_codcli");
		ResultSet rs3 = OrsegupsContratoUtils.getResultSet(sqlCliente.toString(), "SAPIENS");

		String ufCLi = rs3.getString("sigufs");
		String tipCli = rs3.getString("tipcli");
		String tipEmc = "1";

		String sql3 = "SELECT codfam FROM e080ser S WHERE S.codemp = " + usu_codemp + " and S.codser = '"
				+ usu_codser + "'";
		ResultSet rs4 = OrsegupsContratoUtils.getResultSet(sql3.toString(), "SAPIENS");

		String codFam = rs4.getString("codfam");

		String ufPosto = usu_ufsctr;

		String denFor = "";
		String transacaoPosto = usu_tnsser;

		//			if(ufCLi.equals("PR") && ufPosto.equals("PR") /*&& cidade.equals("CURITIBA")*/)	denFor = "D"; else denFor = "F";
		//			if( (usu_codemp.equals("21") || usu_codemp.equals("22") || (usu_codemp.equals("18") && usu_codfil.equals("2"))
		//					&& ufCLi.equals("SC") && ufPosto.equals("SC"))){	
		//				
		//			}else if( !usu_codemp.equals("21") && !usu_codemp.equals("22") && ufCLi.equals("SC") && ufPosto.equals("SC") )
		//			{
		//				denFor = "D";
		//				
		//			}else {
		//				denFor = "F";
		//			}
		if ((ufCLi.equals("SC") && ufPosto.equals("SC")) || (ufCLi.equals("PR") && ufPosto.equals("PR")))
		{
			denFor = "D";
		}
		else
		{
			denFor = "F";
		}

		if (denFor.equals(""))
		{
			throw new WorkflowException(
					"Não foi possível verificar se o posto está dentro ou fora do estado selecionado. Verificar junto à TI.");
		}

		String sql = " SELECT usu_zerpit,usu_zercsl,usu_zercrt,usu_zerins,usu_zerirf FROM USU_T001TNS "
				+ " where 1=1 and usu_codemp=" + usu_codemp + " and usu_codfil =" + usu_codfil
				+ " and usu_tipcli ='" + tipCli + "' " + " and usu_tipemc =" + tipEmc
				+ " and usu_denfor ='" + denFor + "' " + " and usu_codtns ='" + transacaoPosto + "' "
				+ " and usu_codfam = '" + codFam + "' ";

		ContratoLogUtils.logInfo("verificaParametroTransacao->" + sql);

		ResultSet rs = OrsegupsContratoUtils.getResultSet(sql.toString(), "SAPIENS");
		if (rs != null)
		{
			if (rs.getString("usu_zerpit").equals("N"))
			{
				impostos.add(0, 0.65D);
			}
			else
			{
				impostos.add(0, 0D);
			}

			if (rs.getString("usu_zercsl").equals("N"))
			{
				impostos.add(1, 1D);
			}
			else
			{
				impostos.add(1, 0D);
			}

			if (rs.getString("usu_zercrt").equals("N"))
			{
				impostos.add(2, 3D);
			}
			else
			{
				impostos.add(2, 0D);
			}

			if (rs.getString("usu_zerins").equals("N"))
			{
				impostos.add(3, 11D);
			}
			else
			{
				impostos.add(3, 0D);
			}

			if (rs.getString("usu_zerirf").equals("N"))
			{
				impostos.add(4, 1D);
			}
			else
			{
				impostos.add(4, 0D);
			}
		}
		else
		{
			impostos.add(0, 0D);
			impostos.add(1, 0D);
			impostos.add(2, 0D);
			impostos.add(3, 0D);
			impostos.add(4, 0D);
		}

		return impostos;

	}

	public static String atualizaTransacaoDoContrato(Long usu_numctr, Long usu_codemp, Long usu_codfil,
			String usu_tnsser) throws Exception
	{
		String retorno = null;
		String query = "update usu_t160ctr set usu_tnsser = " + usu_tnsser + " where usu_numctr = "
				+ usu_numctr + " and usu_codemp = " + usu_codemp + " and usu_codfil = " + usu_codfil;
		Connection connection = OrsegupsUtils.getSqlConnection("SAPIENS");
		PreparedStatement st = null;
		try
		{
			if (query != null && !query.equals("") && !usu_tnsser.equals(""))
			{
				st = connection.prepareStatement(query);
				System.out.println("[FLUXO DE CONTRATOS] Erro ao atualizar o contrato  " + usu_numctr
						+ " empresa: " + usu_codemp + " filial: " + usu_codfil + ". ");
			}
		}

		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception("Erro ao atualizar o contrato  " + usu_numctr + " empresa: " + usu_codemp
					+ " filial: " + usu_codfil + ". " + e.getMessage());
		}
		finally
		{
			if (st != null)
			{
				try
				{
					st.close();
					connection.close();
				}
				catch (SQLException e)
				{
					e.printStackTrace();
				}
			}
		}

		return retorno;
	}
	
	public void inserirDadosRastreamento(Long codEmp, Long codFilial, Long nrContrato, Long nrPosto, String placa) throws Exception
	{
		String insert = "INSERT INTO USU_T160RAS VALUES (" + codEmp + ", " + codFilial + ", " + nrContrato + ", " + nrPosto + ", '" + placa + "', '', '', 0, '', '')";	
		Query queryExecute = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(insert);
		
		try
		{
			queryExecute.executeUpdate();
			System.out.println("[FLUXO CONTRATOS]-Vinculação de Contas SIGMA x SAPIENS criada.");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception("Erro na Inserção na tabela USU_T160RAS do Sapiens");
		}
	}

}
