package com.neomind.fusion.custom.orsegups.maps.servlet.bean;

import com.neomind.fusion.custom.orsegups.maps.vo.TrackEventVO;

public class ReturnFromUpdateMapsPosition {
    
    private String status;
    
    private TrackEventVO retorno;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public TrackEventVO getRetorno() {
        return retorno;
    }

    public void setRetorno(TrackEventVO retorno) {
        this.retorno = retorno;
    }
    
    

}
