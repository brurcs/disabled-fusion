package com.neomind.fusion.custom.orsegups.servlets;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.relatorio.ImpressaoRelatorio;
import com.neomind.fusion.custom.orsegups.relatorio.ImpressaoRelatorioAutorizacaoRetirada;
import com.neomind.fusion.custom.orsegups.serasa.MyTrustManager;
import com.neomind.fusion.custom.orsegups.serasa.RegistroSerasa;
import com.neomind.fusion.custom.orsegups.serasa.RegistroSerasaFactory;
import com.neomind.fusion.custom.orsegups.serasa.SerasaUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsRelatoriosUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.ProcessState;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

@WebServlet(name="OrsegupsServletUtils", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.servlets.OrsegupsServletUtils"})
public class OrsegupsServletUtils extends HttpServlet
{
	private static final long serialVersionUID = -2837035295207229914L;

	private static final Log log = LogFactory.getLog(OrsegupsServletUtils.class);

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String action = request.getParameter("action");
		if (request.getPathInfo() != null)

		{
			String params[] = request.getPathInfo().split("/");
			action = (params.length >= 1) ? params[1] : null;
		}
		if (!NeoUtils.safeIsNull(action))
		{
			if (action.equalsIgnoreCase("doImprimeFicha"))
			{
				this.doImprimeFicha(request, response);
			}
			else if (action.equalsIgnoreCase("doRelatorioArrombamentos"))
			{
				this.doRelatorioArrombamentos(request, response);
			}
			else if (action.equalsIgnoreCase("doPopulaListaTelefonica"))
			{
				this.doPopulaListaTelefonica(request, response);
			}
			else if (action.equalsIgnoreCase("doVerificaRSCRelacionado"))
			{
				this.doVerificaRSCRelacionado(request, response);
			}
			else if (action.equalsIgnoreCase("getClienteSapiens"))
			{
				this.getClienteSapiens(request, response);
			}
			else if (action.equalsIgnoreCase("doImprimeRelatorioProdutividade"))
			{
				this.doImprimeRelatorioProdutividade(request, response);
			}
			else if (action.equalsIgnoreCase("doBuscaDocumento"))
			{
				this.doBuscaDocumento(request, response);
			}
			else if (action.equalsIgnoreCase("doVerificaRRCRelacionado"))
			{
				this.doVerificaRRCRelacionado(request, response);
			}
			else if (action.equalsIgnoreCase("doPopulaRHViaturaColaborador"))
			{
				this.doPopulaRHViaturaColaborador(request, response);
			}
			else if (action.equalsIgnoreCase("doImprimeFichaPorCPF"))
			{
			    	this.doImprimeFichaPorCpf(request, response);
			}
			
		}
	}

	// Gera o PDF da ficha.
	private void doImprimeFicha(HttpServletRequest request, HttpServletResponse response)
	{

		Long idEForm = NeoUtils.safeLong(request.getParameter("idPai"));
		String autorizacao = request.getParameter("autorizacao");

		try
		{
			if (idEForm != null)
			{
				File fileProposta = null;

				//Imprime o relatorio de autorizacao para retirada de EPI
				if (autorizacao.equals("true"))
				{
					NeoObject ficha = PersistEngine.getNeoObject(idEForm);
					EntityWrapper wrapper = new EntityWrapper(ficha);
					ficha = (NeoObject) wrapper.findValue("fichaUniformeEPI");
					fileProposta = ImpressaoRelatorioAutorizacaoRetirada.geraPDF(ficha);
				}
				else
				{
					//Imprime o relatorio de Ficha de EPI
					NeoObject ficha = PersistEngine.getNeoObject(idEForm);
					Boolean imprimeFichaCompleta = true;

					//verifica onde esta o botao de impressao clicado
					if (ficha.getClass().toString().equalsIgnoreCase("class com.neomind.fusion.entity.dyn.FCEFichaUniformeEpi"))
					{
						imprimeFichaCompleta = true;
					}
					else
					{
						EntityWrapper wrapper = new EntityWrapper(ficha);
						ficha = (NeoObject) wrapper.findValue("fichaUniformeEPI");
					}

					fileProposta = ImpressaoRelatorio.geraPDF(ficha, imprimeFichaCompleta);

				}
				OutputStream out = response.getOutputStream();
				try
				{
					String sContentType = null;
					sContentType = "application/pdf";
					response.setContentType(sContentType);
					response.addHeader("content-disposition", "attachment; filename=" + fileProposta.getName());
					//response.setCharacterEncoding("ISO-8859-1" );
					InputStream in = null;
					in = new BufferedInputStream(new FileInputStream(fileProposta));
					if (in != null)
					{
						response.setContentLength((int) in.available());
						int l;
						byte b[] = new byte[1024];
						while ((l = in.read(b, 0, b.length)) != -1)
						{
							out.write(b, 0, l);
						}
						out.flush();
						in.close();
					}
					else
					{
						System.out.println("Trying to download an invalid file: ");
					}
				}
				catch (Exception e)
				{
					System.out.println("Error trying to download file ");
				}
				finally
				{
					out.close();
				}
			}
		}
		catch (Exception e)
		{
			log.error("Erro ao gerar o PDF da ficha!", e);
		}
	}
	
	// Gera o PDF da ficha.
		private void doImprimeFichaPorCpf(HttpServletRequest request, HttpServletResponse response)
		{

			Long idEForm = NeoUtils.safeLong(request.getParameter("idPai"));
			String cpf = request.getParameter("cpf");

			try
			{
				if (idEForm != null)
				{
					File fileProposta = null;

					
					//Imprime o relatorio de Ficha de EPI
					NeoObject ficha = PersistEngine.getNeoObject(idEForm);
					Boolean imprimeFichaCompleta = true;

					//verifica onde esta o botao de impressao clicado
					if (ficha.getClass().toString().equalsIgnoreCase("class com.neomind.fusion.entity.dyn.FCEFichaUniformeEpi"))
					{
						imprimeFichaCompleta = true;
					}
					else
					{
						EntityWrapper wrapper = new EntityWrapper(ficha);
						
					//	cpf = ""+wrapper.findValue("cpfGerarRelatorio");
					}
					

					fileProposta = ImpressaoRelatorio.geraPDFComCPF(cpf);

					
					OutputStream out = response.getOutputStream();
					try
					{
						String sContentType = null;
						sContentType = "application/pdf";
						response.setContentType(sContentType);
						response.addHeader("content-disposition", "attachment; filename=" + fileProposta.getName());
						//response.setCharacterEncoding("ISO-8859-1" );
						InputStream in = null;
						in = new BufferedInputStream(new FileInputStream(fileProposta));
						if (in != null)
						{
							response.setContentLength((int) in.available());
							int l;
							byte b[] = new byte[1024];
							while ((l = in.read(b, 0, b.length)) != -1)
							{
								out.write(b, 0, l);
							}
							out.flush();
							in.close();
						}
						else
						{
							System.out.println("Trying to download an invalid file: ");
						}
					}
					catch (Exception e)
					{
						System.out.println("Error trying to download file ");
					}
					finally
					{
						out.close();
					}
				}
			}
			catch (Exception e)
			{
				log.error("Erro ao gerar o PDF da ficha!", e);
			}
		}

	private void doRelatorioArrombamentos(HttpServletRequest request, HttpServletResponse response)
	{
		String regional = request.getParameter("regional");
		String dataInicial = request.getParameter("dataInicial");
		String dataFinal = request.getParameter("dataFinal");
		String motivo = request.getParameter("motivo");

		String siglaRegional = "";
		if (regional != null && !regional.isEmpty())
		{
			siglaRegional = regional.substring(0, regional.indexOf("-")).trim();
		}

		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		GregorianCalendar dataIni = null;
		GregorianCalendar dataFim = null;
		try
		{
			Date dtIni = df.parse(dataInicial);
			dataIni = new GregorianCalendar();
			dataIni.setTime(dtIni);

			Date dtFim = df.parse(dataFinal);
			dataFim = new GregorianCalendar();
			dataFim.setTime(dtFim);
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}

		Integer mot = 0;

		try
		{
			mot = Integer.parseInt(motivo);
		}
		catch (Exception e)
		{
			mot = 0;
		}

		File arquivoRelatorio = OrsegupsRelatoriosUtils.getRelatorioArrombamentos(siglaRegional, dataIni, dataFim, mot, false);

		try
		{
			OutputStream out = response.getOutputStream();

			try
			{
				String sContentType = null;
				sContentType = "application/pdf";
				response.setContentType(sContentType);
				response.addHeader("content-disposition", "attachment; filename=" + arquivoRelatorio.getName());
				//response.setCharacterEncoding("ISO-8859-1" );
				InputStream in = null;
				in = new BufferedInputStream(new FileInputStream(arquivoRelatorio));
				if (in != null)
				{
					response.setContentLength((int) in.available());
					int l;
					byte b[] = new byte[1024];
					while ((l = in.read(b, 0, b.length)) != -1)
					{
						out.write(b, 0, l);
					}
					out.flush();
					in.close();
				}
				else
				{
					System.out.println("Trying to download an invalid file: ");
				}
			}
			catch (Exception e)
			{
				System.out.println("Error trying to download file ");
			}
			finally
			{
				out.close();
			}
		}
		catch (Exception e)
		{
			log.error("Erro ao gerar o PDF.", e);
		}
	}

	private void doPopulaListaTelefonica(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{

			response.setContentType("text/html");
			response.setCharacterEncoding("UTF-8");
			PrintWriter out = response.getWriter();
			String nomeColaborador = NeoUtils.safeOutputString(request.getParameter("nomeColaborador"));
			if ("".equals(nomeColaborador))
			{
				out.print("");
				return;
			}

			NeoObject colaborador = PersistEngine.getObject(AdapterUtils.getEntityClass("colaboradores"), new QLOpFilter("fullName", "LIKE", nomeColaborador));
			EntityWrapper ewColaborador = new EntityWrapper(colaborador);
			String nome = "";
			String departamento = "";
			String ramal = "";
			String email = "";

			nome = (String) ewColaborador.findField("fullName").getValue();
			departamento = (String) ewColaborador.findField("group.name").getValue();
			ramal = (String) ewColaborador.findField("ramal").getValue();
			email = (String) ewColaborador.findField("email").getValue();
			String dados = "";

			dados += nome + ";" + departamento + ";" + ramal + ";" + email;

			out.print(dados);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void doVerificaRSCRelacionado(HttpServletRequest request, HttpServletResponse response)
	{
		PrintWriter out = null;
		try
		{
			out = response.getWriter();

			response.setContentType("text/html");
			response.setCharacterEncoding("UTF-8");
			String codCli = request.getParameter("codCli");
			String result = "";

			if (codCli != null && !codCli.trim().isEmpty())
			{
				codCli = codCli.replace(".", "");

				QLEqualsFilter savedFilter = new QLEqualsFilter("wfprocess.saved", true);
				////////////// TAREFA 202128 ////////////
				//QLOpFilter processStateFilter = new QLOpFilter("wfprocess.processState","<>", 1);
				///////////////////////////////////////
				QLEqualsFilter clienteFilter = new QLEqualsFilter("clienteSapiens.neoId", Long.valueOf(codCli));

				QLGroupFilter statusGroupFilter = new QLGroupFilter("OR");
				QLEqualsFilter runningFilter = new QLEqualsFilter("wfprocess.processState", ProcessState.running.ordinal());
				//QLEqualsFilter finishedFilter = new QLEqualsFilter("wfprocess.processState", ProcessState.finished.ordinal());

				statusGroupFilter.addFilter(runningFilter);
				//statusGroupFilter.addFilter(finishedFilter);

				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				groupFilter.addFilter(savedFilter);
				groupFilter.addFilter(statusGroupFilter);
				groupFilter.addFilter(clienteFilter);

				System.out.println(groupFilter.toString());

				Class clazzNovo = AdapterUtils.getEntityClass("RSCRelatorioSolicitacaoClienteNovo");
				Collection<NeoObject> formsNovo = PersistEngine.getObjects(clazzNovo, groupFilter);
				
				Class clazz = AdapterUtils.getEntityClass("RSCRelatorioSolicitacaoCliente");
				Collection<NeoObject> forms = PersistEngine.getObjects(clazz, groupFilter);
				
				//RSC modelo novo
				if (formsNovo != null && !formsNovo.isEmpty())
				{
					for (NeoObject form : formsNovo)
					{
						if (form != null)
						{
							EntityWrapper formWrapper = new EntityWrapper(form);
							WFProcess wfProcess = (WFProcess) formWrapper.findValue("wfprocess");
							if (wfProcess != null)
							{

								if (result.equalsIgnoreCase(""))
								{
									result = String.valueOf(wfProcess.getNeoId());
								}
								else
								{
									result += ";" + String.valueOf(wfProcess.getNeoId());
								}
							}
						}
					}
				}
				
				//TODO RSC - Remover depois de 01/05/2017
				if (forms != null && !forms.isEmpty())
				{
					for (NeoObject form : forms)
					{
						if (form != null)
						{
							EntityWrapper formWrapper = new EntityWrapper(form);
							WFProcess wfProcess = (WFProcess) formWrapper.findValue("wfprocess");
							if (wfProcess != null)
							{

								if (result.equalsIgnoreCase(""))
								{
									result = String.valueOf(wfProcess.getNeoId());
								}
								else
								{
									result += ";" + String.valueOf(wfProcess.getNeoId());
								}
							}
						}
					}
				}
			}

			System.out.println("Out: " + result);

			out.print(result);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (out != null)
			{
				out.close();
			}
		}
	}

	private void doVerificaRRCRelacionado(HttpServletRequest request, HttpServletResponse response)
	{
		PrintWriter out = null;
		try
		{
			out = response.getWriter();

			response.setContentType("text/html");
			response.setCharacterEncoding("UTF-8");
			String codCli = request.getParameter("codCli");
			String result = "";

			if (codCli != null && !codCli.trim().isEmpty())
			{
				codCli = codCli.replace(".", "");

				QLEqualsFilter savedFilter = new QLEqualsFilter("wfprocess.saved", true);
				////////////// TAREFA 202128 ////////////
				//QLOpFilter processStateFilter = new QLOpFilter("wfprocess.processState","<>", 1);
				///////////////////////////////////////
				QLEqualsFilter clienteFilter = new QLEqualsFilter("clienteSapiens.codcli", Long.valueOf(codCli));

				QLGroupFilter statusGroupFilter = new QLGroupFilter("OR");
				QLEqualsFilter runningFilter = new QLEqualsFilter("wfprocess.processState", ProcessState.running.ordinal());
				//QLEqualsFilter finishedFilter = new QLEqualsFilter("wfprocess.processState", ProcessState.finished.ordinal());

				statusGroupFilter.addFilter(runningFilter);
				//statusGroupFilter.addFilter(finishedFilter);

				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				groupFilter.addFilter(savedFilter);
				groupFilter.addFilter(statusGroupFilter);
				groupFilter.addFilter(clienteFilter);

				System.out.println(groupFilter.toString());

				Class clazz = AdapterUtils.getEntityClass("RRCRelatorioReclamacaoCliente");
				Collection<NeoObject> forms = PersistEngine.getObjects(clazz, groupFilter);

				if (forms != null && !forms.isEmpty())
				{
					for (NeoObject form : forms)
					{
						if (form != null)
						{
							EntityWrapper formWrapper = new EntityWrapper(form);
							WFProcess wfProcess = (WFProcess) formWrapper.findValue("wfprocess");
							if (wfProcess != null)
							{

								if (result.equalsIgnoreCase(""))
								{
									result = String.valueOf(wfProcess.getNeoId());
								}
								else
								{
									result += ";" + String.valueOf(wfProcess.getNeoId());
								}
							}
						}
					}
				}
			}

			System.out.println("Out: " + result);

			out.print(result);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (out != null)
			{
				out.close();
			}
		}
	}

	private void getClienteSapiens(HttpServletRequest request, HttpServletResponse response)
	{
		PrintWriter out = null;
		try
		{
			out = response.getWriter();

			response.setContentType("text/html");
			response.setCharacterEncoding("UTF-8");
			String codCli = request.getParameter("codCli");
			String result = "";

			if (codCli != null && !codCli.trim().isEmpty())
			{
				codCli = codCli.replace(".", "");

				Class clazz = AdapterUtils.getEntityClass("SAPIENS_Clientes");
				List<NeoObject> clientes = PersistEngine.getObjects(clazz, new QLEqualsFilter("codcli", Long.valueOf(codCli)));
				if (clientes != null && clientes.size() > 0)
				{
					NeoObject cliente = clientes.get(0);
					EntityWrapper clienteWrapper = new EntityWrapper(cliente);

					String apecli = (String) clienteWrapper.findValue("apecli");
					String nomcli = (String) clienteWrapper.findValue("nomcli");
					Long cgccpf = (Long) clienteWrapper.findValue("cgccpf");
					String usu_pescon = (String) clienteWrapper.findValue("usu_pescon");
					String endcli = (String) clienteWrapper.findValue("endcli");
					String cidcli = (String) clienteWrapper.findValue("cidcli");
					String foncli = (String) clienteWrapper.findValue("foncli");
					String foncl2 = (String) clienteWrapper.findValue("foncl2");
					String foncl3 = (String) clienteWrapper.findValue("foncl3");
					String intnet = (String) clienteWrapper.findValue("intnet");

					result = apecli + ";" + nomcli + ";" + cgccpf + ";" + usu_pescon + ";" + endcli + ";" + cidcli + ";" + foncli + ";" + foncl2 + ";" + foncl3 + ";" + intnet;
				}
			}

			out.print(result);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (out != null)
			{
				out.close();
			}
		}
	}

	// Gera o PDF da ficha.
	private void doImprimeRelatorioProdutividade(HttpServletRequest request, HttpServletResponse response)
	{
		//não vou precisar
		Long idEForm = NeoUtils.safeLong(request.getParameter("idPai"));

		try
		{
			File fileProposta = null;

			//Imprime o relatorio de autorizacao para retirada de EPI

			NeoObject ficha = PersistEngine.getNeoObject(idEForm);
			EntityWrapper wrapper = new EntityWrapper(ficha);
			ficha = (NeoObject) wrapper.findValue("fichaUniformeEPI");
			fileProposta = ImpressaoRelatorioAutorizacaoRetirada.geraPDF(ficha);
			OutputStream out = response.getOutputStream();
			try
			{
				String sContentType = null;
				sContentType = "application/pdf";
				response.setContentType(sContentType);
				response.addHeader("content-disposition", "attachment; filename=" + fileProposta.getName());

				InputStream in = null;
				in = new BufferedInputStream(new FileInputStream(fileProposta));
				if (in != null)
				{
					response.setContentLength((int) in.available());
					int l;
					byte b[] = new byte[1024];
					while ((l = in.read(b, 0, b.length)) != -1)
					{
						out.write(b, 0, l);
					}
					out.flush();
					in.close();
				}
				else
				{
					System.out.println("Trying to download an invalid file: ");
				}
			}
			catch (Exception e)
			{
				System.out.println("Error trying to download file ");
			}
			finally
			{
				out.close();
			}

		}
		catch (Exception e)
		{
			log.error("Erro ao gerar o PDF da ficha!", e);
		}
	}

	public void doBuscaDocumento(HttpServletRequest request, HttpServletResponse response)
	{
		String eform = request.getParameter("eform");
		if (eform != null)
		{
			NeoObject eformPai = PersistEngine.getNeoObject(NeoUtils.safeLong(eform));
			EntityWrapper wPai = new EntityWrapper(eformPai);
			System.out.println("caminho: " + wPai.findValue("caminhoArquivos"));
			System.out.println("mes: " + wPai.findValue("mes"));
		}

		try
		{

		}
		catch (Exception e)
		{
			e.printStackTrace();

		}
	}

	public void getContasTecnico()
	{
		log.warn("##### INICIAR CONSULTA QUANT CONTAS TECNICO - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		String returnFromAccess = "";

		try
		{

			conn = PersistEngine.getConnection("SIGMA90");

			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT NM_COLABORADOR, Tipo, COUNT(*) AS TOTAL ");
			sql.append(" FROM ( ");
			sql.append(" SELECT DISTINCT t.NM_COLABORADOR, c.ID_EMPRESA, c.ID_CENTRAL, CASE WHEN PARTICAO LIKE '098' OR PARTICAO LIKE '099' THEN 'CFTV' ELSE 'ALARME' END Tipo ");
			sql.append(" FROM dbCENTRAL c WITH (NOLOCK)  ");
			sql.append(" INNER JOIN COLABORADOR t WITH (NOLOCK) ON t.CD_COLABORADOR = c.CD_TECNICO_RESPONSAVEL ");
			sql.append(" WHERE c.CTRL_CENTRAL = 1  ");
			sql.append(" AND c.ID_CENTRAL NOT LIKE 'AAA%' AND c.ID_CENTRAL NOT LIKE 'DDDD' 	AND c.ID_CENTRAL NOT LIKE 'FFFF' ");
			sql.append(" ) AS base GROUP BY NM_COLABORADOR, Tipo ORDER BY 1, 2 ");

			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();
			while (rs.next())
			{
				String colaborador = rs.getString("NM_COLABORADOR");
				String tipo = rs.getString("Tipo");
				String total = rs.getString("TOTAL");

			}

			log.warn("##### EXECUTAR CONSULTA QUANT CONTAS TECNICO - RETORNO " + returnFromAccess + " Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO CONSULTA QUANT CONTAS TECNICO - Data: " + e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				log.error("##### ERRO ROTINA  GERAR EVENTO XLIG - Data: " + e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			}
			log.warn("##### FINALIZAR ROTINA  GERAR EVENTO XLIG - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}

	private void doPopulaRHViaturaColaborador(HttpServletRequest request, HttpServletResponse response)
	{
		Connection conn = null;
		PreparedStatement preparedStatementHSelect = null;
		ResultSet rsH = null;
		String dados = "";
		
		try
		{

			response.setContentType("text/html");
			response.setCharacterEncoding("UTF-8");
			PrintWriter out = response.getWriter();
			String nmViatura = NeoUtils.safeOutputString(request.getParameter("cdViatura"));
			if ("".equals(nmViatura))
			{
				out.print("");
				return;
			}


			conn = PersistEngine.getConnection("SIGMA90");
			StringBuilder sqlSigma = new StringBuilder();
			sqlSigma.append("  SELECT CD_VIATURA,NM_VIATURA FROM VIATURA WHERE NM_VIATURA LIKE ? ");

			preparedStatementHSelect = conn.prepareStatement(sqlSigma.toString());
			preparedStatementHSelect.setString(1, nmViatura);
			rsH = preparedStatementHSelect.executeQuery();

			if (rsH.next())
			{
				String cdVtr = rsH.getString("CD_VIATURA");
				String nmVtr = rsH.getString("NM_VIATURA");
				dados = cdVtr + ";" + nmVtr;
				
			}
				out.print(dados);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, preparedStatementHSelect, rsH);
		
		}
	}

}
