package com.neomind.fusion.custom.orsegups.rsc;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class RSCEscalarNivelCategoriaDiversos implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(RSCEscalarNivelCategoriaDiversos.class);

	@SuppressWarnings("static-access")
	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		String erro = "Por favor, contatar o administrador do sistema!";

		try
		{
			RSCUtils rscUtils = new RSCUtils();
			NeoUser responsavelExecutor = (NeoUser) wrapper.findValue("responsavelExecutor");
			
			if (origin != null)
			{
				if ((Long) wrapper.findValue("etapaVerificacaoEficacia") == 2L){
				    NeoUser uSolic = null;
				    NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","responsavelPrazoRSCDiversos"));
				    if (obj != null && obj.getAllUsers() != null && !obj.getAllUsers().isEmpty()){
					for (NeoUser user : obj.getUsers()){
					    uSolic = user;
					    break;
					}
				    }
				    wrapper.setValue("responsavelPrazoRSC", uSolic);

				}

				if ((origin.getActivityName().equalsIgnoreCase("Atender Solicitação - Escalada") || origin.getActivityName().equalsIgnoreCase("Atender Solicitação")) 
					&& ((Long) wrapper.findValue("etapaVerificacaoEficacia")) == 1L)
				{
					if (origin.getFinishByUser() == null)
					{
						wrapper.setValue("superiorResponsavelExecutor", rscUtils.retornaSuperiorResponsavelExecutor(responsavelExecutor));
					}
					else
					{
					    NeoUser uSolic = null;
					    NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","responsavelPrazoRSCDiversos"));
					    if (obj != null && obj.getAllUsers() != null && !obj.getAllUsers().isEmpty()){
						for (NeoUser user : obj.getUsers()){
						    uSolic = user;
						    break;
						}
					    }
					    wrapper.setValue("responsavelPrazoRSC", uSolic);
//						NeoUser uSolic = (NeoUser) PersistEngine.getObject(SecurityEntity.class, new QLEqualsFilter("code", "fernanda.maciel"));
//						wrapper.setValue("responsavelPrazoRSC", uSolic);
					}
				}else if (origin.getActivityName().equalsIgnoreCase("Atender Solicitação")){
				    if (origin.getFinishByUser() == null){
					wrapper.setValue("superiorResponsavelExecutor", rscUtils.retornaSuperiorResponsavelExecutor(responsavelExecutor));
				    }else{
					NeoUser uSolic = null;
					NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","responsavelPrazoRSCDiversos"));
					if (obj != null && obj.getAllUsers() != null && !obj.getAllUsers().isEmpty()){
					    for (NeoUser user : obj.getUsers()){
						uSolic = user;
						break;
					    }
					}
					wrapper.setValue("responsavelPrazoRSC", uSolic);
					//						NeoUser uSolic = (NeoUser) PersistEngine.getObject(SecurityEntity.class, new QLEqualsFilter("code", "fernanda.maciel"));
					//						wrapper.setValue("responsavelPrazoRSC", uSolic);
				    }
				}else{
					/*
					 * if (((NeoUser) wrapper.findValue("ultimoUsuario")) == null && ((Long)
					 * wrapper.findValue("etapaVerificacaoEficacia")) == 2L)
					 * {
					 * wrapper.setValue("superiorResponsavelExecutor",
					 * rscUtils.retornaSuperiorResponsavelExecutor(responsavelExecutor));
					 * }
					 * else
					 * {
					 */
					NeoPaper superiorResponsavelExecutor = (NeoPaper) wrapper.findValue("superiorResponsavelExecutor");
					for (NeoUser user : superiorResponsavelExecutor.getAllUsers())
					{
						if (user.getCode().equals("dilmoberger"))
						{
							erro = "Tarefa já escalou para o último nível de hierarquia. Por favor, proceder com a finalização da tarefa!";
							throw new WorkflowException(erro);
						}
						else
						{
							wrapper.setValue("superiorResponsavelExecutor", rscUtils.retornaSuperiorResponsavelExecutor(user));
						}
					}
					//}
				}
			}
		}
		catch (Exception e)
		{
			log.error(erro);
			e.printStackTrace();
			throw new WorkflowException(erro);
		}
	}

	public void back(EntityWrapper wrapper, Activity activity)
	{
	}
}
