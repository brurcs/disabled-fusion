package com.neomind.fusion.custom.orsegups.contract;

import java.util.ArrayList;
import java.util.Collection;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.dynamic.EntityCloner;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class ContratoClonaPostosBackup implements AdapterInterface
{
	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		Collection<NeoObject> postos = wrapper.findField("postosContrato").getValues();
		Collection<NeoObject> postosClonados = new ArrayList<NeoObject>();
		for(NeoObject posto : postos)
		{
			NeoObject postoClonado = EntityCloner.cloneNeoObject(posto);
			PersistEngine.persist(postoClonado);
			postosClonados.add(postoClonado);
		}
		wrapper.findField("postosContratoBackup").getValues().addAll(postosClonados);
		
	}
	
	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}
}
