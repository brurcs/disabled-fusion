package com.neomind.fusion.custom.orsegups.ti.paineljobs.bean;

import java.util.Date;
import java.util.List;

public class PainelJobBean {
    private String nomeJob;
    private String periodicidade;
    private String horarioExecucao;
    private String dataHorarioUltimoExecucao;
    private String dataHorarioProximaExecucao;
    private String adapter;
    private String status;
    private List<String> tarefas;
    private Long idJob;
    private String filtroTarefas;
    private String descricaoJob;
    private String sistema;
    
    public String getNomeJob() {
        return nomeJob;
    }
    public void setNomeJob(String nomeJob) {
        this.nomeJob = nomeJob;
    }
    public String getPeriodicidade() {
        return periodicidade;
    }
    public void setPeriodicidade(String periodicidade) {
        this.periodicidade = periodicidade;
    }
    public String getHorarioExecucao() {
        return horarioExecucao;
    }
    public void setHorarioExecucao(String horarioExecucao) {
        this.horarioExecucao = horarioExecucao;
    }
    public String getDataHorarioUltimoExecucao() {
        return dataHorarioUltimoExecucao;
    }
    public void setDataHorarioUltimoExecucao(String dataHorarioUltimoExecucao) {
        this.dataHorarioUltimoExecucao = dataHorarioUltimoExecucao;
    }
    public String getDataHorarioProximaExecucao() {
        return dataHorarioProximaExecucao;
    }
    public void setDataHorarioProximaExecucao(String dataHorarioProximaExecucao) {
        this.dataHorarioProximaExecucao = dataHorarioProximaExecucao;
    }
    public String getAdapter() {
        return adapter;
    }
    public void setAdapter(String adapter) {
        this.adapter = adapter;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public List<String> getTarefas() {
        return tarefas;
    }
    public void setTarefas(List<String> tarefas) {
        this.tarefas = tarefas;
    }
    public Long getIdJob() {
	return idJob;
    }
    public void setIdJob(Long idJob) {
	this.idJob = idJob;
    }
    public String getFiltroTarefas() {
	return filtroTarefas;
    }
    public void setFiltroTarefas(String filtroTarefas) {
	this.filtroTarefas = filtroTarefas;
    }
    public String getDescricaoJob() {
        return descricaoJob;
    }
    public void setDescricaoJob(String descricaoJob) {
        this.descricaoJob = descricaoJob;
    }
    public String getSistema() {
        return sistema;
    }
    public void setSistema(String sistema) {
        this.sistema = sistema;
    }
    
    
}
