package com.neomind.fusion.custom.orsegups.presenca.vo;

import java.sql.Timestamp;

public class JustificativaFaltaVO
{
	private String numemp; 
	private String numcad; 
	private String nomFun; 
	private Long numcpf; 
	private Long tipcol; 
	private String afainf; 
	private Timestamp datafa; 
	private Long horafa; 
	private Timestamp datter; 
	private Long sitafa; 
	private Long caudem; 
	private Long codreg; 
	private String nomloc; 
	private String lotorn; 
	private String titred; 
	private String nomfis; 
	private String dessit;
	private Long usuCodccu;
	private Long usuCodCid;
	
	public JustificativaFaltaVO(String numemp, String numcad, String nomFun, Long numcpf, Long tipcol, String afainf, Timestamp datafa, Long horafa, Timestamp datter, Long sitafa, Long caudem, Long codreg, String nomloc, String lotorn, String titred, String nomfis, String dessit, Long usuCodccu, Long usuCodCid)
	{
		super();
		this.numemp = numemp;
		this.numcad = numcad;
		this.nomFun = nomFun;
		this.numcpf = numcpf;
		this.tipcol = tipcol;
		this.afainf = afainf;
		this.datafa = datafa;
		this.horafa = horafa;
		this.datter = datter;
		this.sitafa = sitafa;
		this.caudem = caudem;
		this.codreg = codreg;
		this.nomloc = nomloc;
		this.lotorn = lotorn;
		this.titred = titred;
		this.nomfis = nomfis;
		this.dessit = dessit;
		this.usuCodccu = usuCodccu;
		this.usuCodCid = usuCodCid;
	}

	public JustificativaFaltaVO()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	public String getNumemp()
	{
		return numemp;
	}

	public void setNumemp(String numemp)
	{
		this.numemp = numemp;
	}

	public String getNumcad()
	{
		return numcad;
	}

	public void setNumcad(String numcad)
	{
		this.numcad = numcad;
	}

	public String getNomFun()
	{
		return nomFun;
	}

	public void setNomFun(String nomFun)
	{
		this.nomFun = nomFun;
	}

	public Long getNumcpf()
	{
		return numcpf;
	}

	public void setNumcpf(Long numcpf)
	{
		this.numcpf = numcpf;
	}

	public Long getTipcol()
	{
		return tipcol;
	}

	public void setTipcol(Long tipcol)
	{
		this.tipcol = tipcol;
	}

	public String getAfainf()
	{
		return afainf;
	}

	public void setAfainf(String afainf)
	{
		this.afainf = afainf;
	}

	public Timestamp getDatafa()
	{
		return datafa;
	}

	public void setDatafa(Timestamp datafa)
	{
		this.datafa = datafa;
	}

	public Long getHorafa()
	{
		return horafa;
	}

	public void setHorafa(Long horafa)
	{
		this.horafa = horafa;
	}

	public Timestamp getDatter()
	{
		return datter;
	}

	public void setDatter(Timestamp datter)
	{
		this.datter = datter;
	}

	public Long getSitafa()
	{
		return sitafa;
	}

	public void setSitafa(Long sitafa)
	{
		this.sitafa = sitafa;
	}

	public Long getCaudem()
	{
		return caudem;
	}

	public void setCaudem(Long caudem)
	{
		this.caudem = caudem;
	}

	public Long getCodreg()
	{
		return codreg;
	}

	public void setCodreg(Long codreg)
	{
		this.codreg = codreg;
	}

	public String getNomloc()
	{
		return nomloc;
	}

	public void setNomloc(String nomloc)
	{
		this.nomloc = nomloc;
	}

	public String getLotorn()
	{
		return lotorn;
	}

	public void setLotorn(String lotorn)
	{
		this.lotorn = lotorn;
	}

	public String getTitred()
	{
		return titred;
	}

	public void setTitred(String titred)
	{
		this.titred = titred;
	}

	public String getNomfis()
	{
		return nomfis;
	}

	public void setNomfis(String nomfis)
	{
		this.nomfis = nomfis;
	}

	public String getDessit()
	{
		return dessit;
	}

	public void setDessit(String dessit)
	{
		this.dessit = dessit;
	}
	public Long getUsuCodccu()
	{
		return usuCodccu;
	}

	public void setUsuCodccu(Long usuCodccu)
	{
		this.usuCodccu = usuCodccu;
	}
	
	public Long getUsuCodCid()
	{
		return usuCodCid;
	}

	public void setUsuCodCid(Long usuCodCid)
	{
		this.usuCodCid = usuCodCid;
	}

	@Override
	public String toString()
	{
		return "JustificativaFaltaVO [numemp=" + numemp + ", numcad=" + numcad + ", nomFun=" + nomFun + ", numcpf=" + numcpf + ", tipcol=" + tipcol + ", afainf=" + afainf + ", datafa=" + datafa + ", horafa=" + horafa + ", datter=" + datter + ", sitafa=" + sitafa + ", caudem=" + caudem + ", codreg=" + codreg + ", nomloc=" + nomloc + ", lotorn=" + lotorn + ", titred=" + titred + ", nomfis=" + nomfis + ", dessit=" + dessit + ", usuCodccu=" + usuCodccu + ", usuCodCid=" + usuCodCid + "]";
	}
	
	
	
	

}
