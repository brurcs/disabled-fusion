package com.neomind.fusion.custom.orsegups.scheduler;

import java.text.ParseException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import br.com.senior.services.CalculoApuracaoCalcularIn;
import br.com.senior.services.CalculoApuracaoCalcularOut;
import br.com.senior.services.Ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPx;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class RecalcularApuracaoPonto implements CustomJobAdapter
{
	@SuppressWarnings("unused")
	private static final Log log = LogFactory.getLog(RecalcularApuracaoPonto.class);

	@Override
	public void execute(CustomJobContext arg0)
	{
		try
		{
			GregorianCalendar dataAtual = new GregorianCalendar();
			dataAtual.add(Calendar.MONTH, - 1);
			
			String competencia = OrsegupsUtils.getCompetencia(dataAtual);
			GregorianCalendar periodo;
		
			// busca a competência do dia anterior
			periodo = OrsegupsUtils.stringToGregorianCalendar("01/" + competencia, "dd/MM/yyyy");
			
			QLEqualsFilter filtroPeriodo = new QLEqualsFilter("perref",periodo);
			QLEqualsFilter filtroTipo = new QLEqualsFilter("tipcal", 11L);
			
			QLGroupFilter filtroCalculo = new QLGroupFilter("AND");
			filtroCalculo.addFilter(filtroPeriodo);
			filtroCalculo.addFilter(filtroTipo);
			
			List<NeoObject> calculos = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORHRCAL"),filtroCalculo);
			
			Long codcal = 0L;
			
			if (NeoUtils.safeIsNotNull(calculos))
			{
				for (NeoObject cal : calculos)
				{
					CalculoApuracaoCalcularOut calcularOut = new CalculoApuracaoCalcularOut();
					CalculoApuracaoCalcularIn calcularIn = new CalculoApuracaoCalcularIn();
					
					EntityWrapper wrapperCalculo = new EntityWrapper(cal);
					codcal = (Long) wrapperCalculo.findField("codcal").getValue();
					Long numemp = (Long) wrapperCalculo.findField("numemp").getValue();
					
					calcularIn.setNumEmp(numemp.intValue());
					calcularIn.setAbrEmp(numemp.toString());
					calcularIn.setProApu("1");
					calcularIn.setCodCal(codcal.intValue());
					calcularIn.setReaMar("S");
					calcularIn.setDesAce("S");
					
					Ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPx calculoApuracaoPx = new Ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPx();
					calcularOut = calculoApuracaoPx.calcular(OrsegupsUtils.WS_LOGIN, OrsegupsUtils.WS_SENHA, 0, calcularIn);

					String erro = calcularOut.getErroExecucao();

					if (erro != null && !erro.equals(""))
					{
						log.warn("ERRO NO WEBSERVICE RONDA - " + erro);
					}
				}
			}
		}
		catch (ParseException e)
		{
			log.warn("ERRO NO WEBSERVICE RONDA");
			e.printStackTrace();
		}
		catch (Exception e)
		{
			log.warn("ERRO NO WEBSERVICE RONDA");
			e.printStackTrace();
		}
	}
}

