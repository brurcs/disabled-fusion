package com.neomind.fusion.custom.orsegups.mobile.vo;

public class InspecaoClienteCobrancaVO
{

	Long codCli;
	Long codReg;
	String nomReg;
	String nomCli;
	String lotacao;
	Long numCtr;
	Long numPos;
	String codCcu;
	String endCtr;
	String baiCtr;
	String cidCtr;

	public Long getNumCtr()
	{
		return numCtr;
	}

	public void setNumCtr(Long numCtr)
	{
		this.numCtr = numCtr;
	}

	public Long getNumPos()
	{
		return numPos;
	}

	public void setNumPos(Long numPos)
	{
		this.numPos = numPos;
	}

	public String getCodCcu()
	{
		return codCcu;
	}

	public void setCodCcu(String codCcu)
	{
		this.codCcu = codCcu;
	}

	public String getEndCtr()
	{
		return endCtr;
	}

	public void setEndCtr(String endCtr)
	{
		this.endCtr = endCtr;
	}

	public String getBaiCtr()
	{
		return baiCtr;
	}

	public void setBaiCtr(String baiCtr)
	{
		this.baiCtr = baiCtr;
	}

	public String getCidCtr()
	{
		return cidCtr;
	}

	public void setCidCtr(String cidCtr)
	{
		this.cidCtr = cidCtr;
	}

	public InspecaoClienteCobrancaVO()
	{
		super();
	}

	public Long getCodCli()
	{
		return codCli;
	}

	public void setCodCli(Long codCli)
	{
		this.codCli = codCli;
	}

	public Long getCodReg()
	{
		return codReg;
	}

	public void setCodReg(Long codReg)
	{
		this.codReg = codReg;
	}

	public String getNomReg()
	{
		return nomReg;
	}

	public void setNomReg(String nomReg)
	{
		this.nomReg = nomReg;
	}

	public String getNomCli()
	{
		return nomCli;
	}

	public void setNomCli(String nomCli)
	{
		this.nomCli = nomCli;
	}

	public String getLotacao()
	{
		return lotacao;
	}

	public void setLotacao(String lotacao)
	{
		this.lotacao = lotacao;
	}

}
