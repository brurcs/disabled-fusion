package com.neomind.fusion.custom.orsegups.medicaoContratos;

import java.util.GregorianCalendar;

import org.apache.commons.mail.EmailException;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;

public class EnviaEmailOrsegupsMedicaoMensal implements CustomJobAdapter

{

	@Override
	public void execute(CustomJobContext ctx)
	{
		if (OrsegupsUtils.isWorkDay(new GregorianCalendar()))
		{
			System.out.println("[MED] - iniciando job Envia e-mail Para Medicao de contratos Mensal");
			processaJob(ctx);
		}
	}

	public static void processaJob(CustomJobContext ctx)
	{
		GregorianCalendar datRef = new GregorianCalendar();
		datRef.add(GregorianCalendar.MONTH, -1);
		
		String datCpt = "06/2016";
		OrsegupsMedicaoUtilsMensal.inserePostosSemColaborador(datCpt);
	}

}
