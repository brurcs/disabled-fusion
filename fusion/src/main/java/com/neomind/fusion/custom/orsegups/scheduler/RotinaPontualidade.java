package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class RotinaPontualidade implements CustomJobAdapter {

    @SuppressWarnings("unused")
    private static final Log log = LogFactory.getLog(RotinaVarreduraPostosNaoFaturados.class);

    @Override
    public void execute(CustomJobContext arg0) {
	
	Connection conn = PersistEngine.getConnection("FUSIONPROD"); 
	PreparedStatement pstm = null;
	ResultSet rs = null;
	final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.RotinaPontualidade");
	log.warn("##### INICIO AGENDADOR DE TAREFA: RotinaPontualidade - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	System.out.println("##### INICIO AGENDADOR DE TAREFA: RotinaPontualidade - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	Long key = GregorianCalendar.getInstance().getTimeInMillis();
	try
	{	    
		StringBuffer  varname1 = new StringBuffer();
		
		varname1.append("               select distinct ");
		varname1.append("		               fun.numcpf, ");
		varname1.append("		               fun.nomfun, ");
		varname1.append("		               right(replicate('0',2) + convert(VARCHAR,acc.horacc/60),2) as horaF, ");
		varname1.append("		               right(replicate('0',2) + convert(VARCHAR,acc.horacc%60),2) as minutoF, ");
		varname1.append("		               right(replicate('0',2) + convert(VARCHAR,mhr.HorBat/60),2) as horaEF, ");
		varname1.append("		               right(replicate('0',2) + convert(VARCHAR,mhr.HorBat%60),2) as minutoEF, ");
		varname1.append("		               acc.numemp, ");
		varname1.append("		               acc.numcad, ");
		varname1.append("		               hes.tipcol, ");
		varname1.append("		               acc.horacc/60 as hora, ");
		varname1.append("		               acc.horacc%60 as minuto, ");
		varname1.append("		               mhr.HorBat/60 as horaEscala, ");
		varname1.append("		               mhr.HorBat%60 as minutoEscala, ");
		varname1.append("		               acc.horacc, ");
		varname1.append("		               mhr.HorBat, ");
		varname1.append("		               mhr.HorBat-acc.horacc as total, ");
		varname1.append("		               hes.datalt, ");
		varname1.append("		               orn.USU_CodReg, ");
		varname1.append("		               orn.USU_LotOrn, ");
		varname1.append("		               orn.NomLoc, ");
		varname1.append("		               case when (mhr.HorBat-acc.horacc) < -15 then 'SIM' else 'NAO' end as abreTarefa ");
		varname1.append("		          from [FSOODB04\\SQL02].VETORH.dbo.R070ACC acc ");
		varname1.append("		    inner join [FSOODB04\\SQL02].VETORH.dbo.R038HCH hch on acc.numcra = hch.numcra ");
		varname1.append("		    inner join [FSOODB04\\SQL02].VETORH.dbo.R038HES hes on hch.numEmp = hes.numEmp ");
		varname1.append("		    												  and hch.numCad = hes.numCad ");
		varname1.append("		    												  and hch.tipCol = hes.tipCol ");
		varname1.append("		    inner join [FSOODB04\\SQL02].VETORH.dbo.R006ESC esc on hes.codEsc = esc.codEsc ");
		varname1.append("		    inner join [FSOODB04\\SQL02].VETORH.dbo.R006HOR hor on esc.codEsc = hor.codEsc ");
		varname1.append("		    inner join [FSOODB04\\SQL02].VETORH.dbo.R004HOR hor4 on hor.codHor = hor4.codHor ");
		varname1.append("		    inner join [FSOODB04\\SQL02].VETORH.dbo.R004MHR mhr on hor4.codHor = mhr.codHor ");
		varname1.append("		    inner join [FSOODB04\\SQL02].VETORH.dbo.R034FUN fun on hes.NumEmp = fun.NumEmp ");
		varname1.append("		                    								  and hes.TipCol = fun.TipCol ");
		varname1.append("		                    								  and hes.NumCad = fun.NumCad ");
		varname1.append("		                    								  and hes.DatAlt = (SELECT MAX (DATALT) ");
		varname1.append("		                    								  					  FROM [FSOODB04\\SQL02].VETORH.dbo.R038HES TABELA001 ");
		varname1.append("		                    								  					 WHERE TABELA001.NUMEMP = hes.NUMEMP ");
		varname1.append("		                    								  					   AND TABELA001.TIPCOL = hes.TIPCOL ");
		varname1.append("		                    								  					   AND TABELA001.NUMCAD = hes.NUMCAD ");
		varname1.append("		                    								  					   AND TABELA001.DATALT <= GetDate()) ");
		varname1.append("		    inner join [FSOODB04\\SQL02].VETORH.dbo.R016ORN orn WITH (NOLOCK) ON (fun.numloc = orn.numloc) ");
		varname1.append("		         where acc.datacc > DATEADD(Day,-1, getDate()) ");
		varname1.append("		           and acc.horacc = (select min(a.horacc) ");
		varname1.append("		                               from [FSOODB04\\SQL02].VETORH.dbo.R070ACC a ");
		varname1.append("		                              where a.datacc = acc.datacc ");
		varname1.append("		                                and a.numcra = acc.numcra) ");
		varname1.append("                  AND acc.usomar = 2 ");
		varname1.append("		           and mhr.seqMar = 1 ");
		varname1.append("		           and hor.seqReg = 1 ");
		varname1.append("		           and fun.usu_colsde = 'S' ");
		varname1.append("		           and fun.usu_tipAdm = 1 ");
		varname1.append("		           and fun.conRho = 2 ");
		varname1.append("		           and fun.tipCon <> 5 ");
		varname1.append("		           and fun.tipCol = 1 ");
		varname1.append("		           and hes.codEsc <> 3188 ");
		varname1.append("		           and hes.codEsc <> 3228 ");  
		varname1.append("		           and esc.nomEsc not like '%FOL%' ");
		varname1.append("		           and (mhr.HorBat-acc.horacc) < -15 ");
		varname1.append("		           and not exists (select 1 ");
		varname1.append("		                             from [FSOODB04\\SQL02].VETORH.dbo.R066apu apu ");
		varname1.append("		                            where acc.numEmp = apu.numEmp ");
		varname1.append("		                              and acc.tipCol = apu.tipCol ");
		varname1.append("		                              and acc.numCad = apu.numCad ");
		varname1.append("		                              and acc.datAcc = apu.datApu ");
		varname1.append("		                              and apu.HorDat > 9995) ");
		varname1.append("		      order by acc.numemp, acc.numcad, acc.horacc/60, acc.horacc%60");
	    
	    pstm = conn.prepareStatement(varname1.toString());
	    rs = pstm.executeQuery();
	    

	    while (rs.next()) {
	    	
	    	String abreTarefa = rs.getString("abreTarefa");
	    	
	    	if (abreTarefa.equals("SIM")) {
	    		
	    		String nomFun = rs.getString("nomfun");
	    		String tituloTarefa = "Pontualidade - "+ nomFun;
	    		String descricaoTarefa = "<p>Prezado Gestor, justifique o motivo do atraso do colaborador " + nomFun + ", e porque o mesmo foi aceito mesmo assumindo o posto com atraso. </p>";
	    		descricaoTarefa += "Departamento: "+ rs.getString("USU_LotOrn");
	    		descricaoTarefa += "</br>Horário Escala: " + rs.getString("horaEF")+":"+rs.getString("minutoEF")+ "hs | Horário Marcação: <b>"+rs.getString("horaF")+":"+rs.getString("minutoF")+"hs.</b>";
	    		descricaoTarefa += retornaHistorico("%Pontualidade%"+nomFun+"%");
	    		
	    		Long codReg = rs.getLong("USU_CodReg");
	    		String lotacao = "";

	    		if (codReg == 0 && rs.getString("USU_LotOrn").equals("Coordenadoria Indenização Contratual")) {
	    		    lotacao = rs.getString("NomLoc");
	    		} else if (codReg == 1L) {
	    		    lotacao = rs.getString("NomLoc");
	    		} else if (codReg == 13L) {
	    		    lotacao = rs.getString("NomLoc");
	    		} else if (codReg == 0 && rs.getString("NomLoc").equals("Coordenador de Infraestrutura")) {
	    		    lotacao = rs.getString("NomLoc");
	    		} else {
	    		    lotacao = rs.getString("USU_LotOrn");
	    		}
	    		
	    		abrirTarefa(rs.getInt("numemp"), rs.getInt("numcad"), nomFun,  tituloTarefa, descricaoTarefa, codReg, lotacao);
	    	}
	    }
	    
	}catch (Exception e) {
	    log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Validacao OS Chp1");
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
	}
	finally {
	    try {
		rs.close();
		pstm.close();
		conn.close();
	    } catch (SQLException e) {
		e.printStackTrace();
	    }

	    log.warn("##### FIM AGENDADOR DE TAREFA: RotinaPontualidade - - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	    System.out.println("##### FIM AGENDADOR DE TAREFA: RotinaPontualidade - - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	}
    }
    
	private String retornaHistorico(String string) {

		String descricao = "";

		Connection conn2 = PersistEngine.getConnection("FUSIONPROD");
		PreparedStatement pstm2 = null;
		ResultSet rs2 = null;
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		try {
			StringBuffer varname1 = new StringBuffer();

			varname1.append("select p.code, ");
			varname1.append("       Convert(varchar(10), p.startDate,103) as dataAbertura ");
			varname1.append("  from d_tarefa t ");
			varname1.append(" inner join WFProcess p on t.wfprocess_neoId = p.neoId ");
			varname1.append(" where t.titulo like '");
			varname1.append(string);
			varname1.append("' ");
			varname1.append("   and t.neoId > 1072220268 ");
			varname1.append("  order by t.neoId");

			pstm2 = conn2.prepareStatement(varname1.toString());
			rs2 = pstm2.executeQuery();

			descricao += "</br></br>Histórico de Tarefas Simples relacionadas à Pontualidade:</br><table>"
					+ "<tr>"
					+ "<td>Cód.Tarefa</td>"
					+ "<td>Data</td>"
					+ "</tr>";
			int contador = 0;
			
			while (rs2.next()) {
				descricao += "<tr>"
						+ "<td>" + rs2.getInt("code")
						+ "</td>"
						+ "<td>" + rs2.getString("dataAbertura")
						+ "</td>"
						+ "</tr>";	
				contador++;
			}
			descricao += "</table>";
			
			if (contador == 0)
				descricao = "</br></br>Não há registros anteriores de atraso para este colaborador.";

			return descricao;
			
		} catch (Exception e) {
			return "</br></br>Não foi possível recuperar o Histórico";
		} finally {		    
			try {
				rs2.close();
				pstm2.close();
				conn2.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private void abrirTarefa(Integer _numEmp, Integer _numCad, String _nomFun, String _titulo, String _descricaoTarefa, Long _regional, String _lotacao) throws Exception {
	
    	NeoUser usuarioSolicitante = PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "dilmoberger" ) );
		NeoUser usuarioResponsavel = null; //PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "danilo.silva") );
		
		NeoPaper papel = null;
		try {
			papel = getPapelJustificarR022(_regional, _lotacao);
		} catch (Exception e) {
			
		}
		
		if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
		{
			for (NeoUser user : papel.getUsers())
			{
				usuarioResponsavel = user;
				break;
			}
		}
		
		GregorianCalendar prazo = new GregorianCalendar();
		int dia = 0;
		while (dia < 2) {	    	
		    if (OrsegupsUtils.isWorkDay(prazo)) {		
			dia++;
		    }
		    prazo.add(GregorianCalendar.DATE, 1);
		}
		while (!OrsegupsUtils.isWorkDay(prazo)) 
		{
			prazo = OrsegupsUtils.getNextWorkDay(prazo);
		}
		
		prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
		prazo.set(GregorianCalendar.MINUTE, 59);
		prazo.set(GregorianCalendar.SECOND, 59);
		
		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		
		if (usuarioResponsavel == null){
			
			usuarioSolicitante = PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "diogo.silva" ) );	
			usuarioResponsavel = PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "fernanda.martins" ) );
			_titulo = "Corrigir Gestor. "+_titulo;
		    _descricaoTarefa = "Solicitamos informação para ajustar Setor do Colaborador: </br>Colaborador: "+_numEmp+"/"+_numCad+" - "+_nomFun+"</br>Regional: "+_regional+"</br>Lotação: "+_lotacao +"</br>Quem é seu Gestor?";		    
		}
		
		System.out.println(usuarioSolicitante.getCode()+";"+usuarioResponsavel.getCode()+";"+_nomFun+";"+_titulo+";"+_descricaoTarefa+";");
		iniciarTarefaSimples.abrirTarefa(usuarioSolicitante.getCode(), usuarioResponsavel.getCode(), _titulo, _descricaoTarefa, "1", "hadouken", prazo); 
    }
    
    public static NeoPaper getPapelJustificarR022(Long codreg, String lotacao) {
    	EntityWrapper ewResponsavelRegional = null;
    	NeoPaper papelResponsavel = null;
    	NeoObject responsavelRegional = null;
    	NeoUser user = null;

    	if (codreg != 0L) {
    	    responsavelRegional = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("responsaveisRegionais"), new QLEqualsFilter("codreg", codreg));
    	} else {
    	    QLGroupFilter groupFilter = new QLGroupFilter("AND");
    	    groupFilter.addFilter(new QLEqualsFilter("codreg", codreg));
    	    groupFilter.addFilter(new QLEqualsFilter("nomloc", lotacao));
    	    responsavelRegional = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("responsaveisRegionais"), groupFilter);
    	}


    	if (NeoUtils.safeIsNotNull(responsavelRegional)) {

    	    ewResponsavelRegional = new EntityWrapper(responsavelRegional);
    	    papelResponsavel = ewResponsavelRegional.findGenericValue("responsavelR022");
    	    user = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelResponsavel);
    	    
    	} 

    	return papelResponsavel;

        }
}
