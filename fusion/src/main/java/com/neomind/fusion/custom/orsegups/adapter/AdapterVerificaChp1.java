package com.neomind.fusion.custom.orsegups.adapter;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilterIsNull;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class AdapterVerificaChp1 implements AdapterInterface {

    @Override
    public void start(Task origin, EntityWrapper processEntity, Activity activity) {

	GregorianCalendar data = new GregorianCalendar();
	String _iccId = "";
	_iccId = ""+processEntity.getValue("iccId");
	
	if (activity.getActivityName().equals("Inicio")) {
	    System.out.println("Entrou"); 
	} else if (activity.getActivityName().equals("Verifica Código de Barras")) {
	    
	    
	    processEntity.setValue("chp1ConstaEForm", Boolean.FALSE);
	    processEntity.setValue("chp1Utilizado", Boolean.FALSE);
	    if (processEntity.getValue("confirmaIccId") != null) {
		if (!(Boolean)processEntity.getValue("confirmaIccId")) {
		    processEntity.setValue("confirmaIccId", Boolean.FALSE);
		}
	    }
	    processEntity.setValue("msgRedigitaIccId", "Digite novamente o IccId");
	    
	    QLGroupFilter groupFilter = new QLGroupFilter("AND");
	    groupFilter.addFilter(new QLEqualsFilter("iccId", _iccId));
	    NeoObject o = PersistEngine.getObject(AdapterUtils.getEntityClass("Chp1"), groupFilter);
	    
	    if (o != null) {
		
		processEntity.setValue("Chp1", o);
		processEntity.setValue("chp1ConstaEForm", Boolean.TRUE);	
		
		QLGroupFilter groupFilter2 = new QLGroupFilter("AND");
		groupFilter2.addFilter(new QLEqualsFilter("Chp1", o));
		NeoObject o2 = PersistEngine.getObject(AdapterUtils.getEntityClass("Chp1Cliente"), groupFilter2);
		
		if (o2 != null) {
		    processEntity.setValue("chp1Utilizado", Boolean.TRUE);
		}
		processEntity.setValue("dataInstalacao", data);
		    
	    } 

	} else if (activity.getActivityName().equals("Abre Tarefa Simples Cadastro")) {

	    /*
	    String sRazaoSocial = "";
	    sRazaoSocial = processEntity.getValue("razaoSocial").toString() + " - Cadastrar CHIP";
	    processEntity.setValue("razaoSocial", sRazaoSocial);
	    
	    String solicitante = processEntity.getValue("solicitante").toString(); 
	    String executor = OrsegupsUtils.getUserNeoPaper("responsavelCadastroChp1");

	    GregorianCalendar prazo = new GregorianCalendar();
	    prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
	    prazo.set(Calendar.HOUR_OF_DAY, 23);
	    prazo.set(Calendar.MINUTE, 59);
	    prazo.set(Calendar.SECOND, 59);

	    String titulo = "Cadastro de Chip IccID nº: " + processEntity.getValue("iccId").toString();
	    String descricao = "TAREFA AUTOMÁTICA: Efetuar cadastro de CHIP GPRS com IccId nº " + processEntity.getValue("iccId").toString();

	    IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
	    String tarefaSimples = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "hadouken", prazo);
	    */
	    processEntity.setValue("tarefaSimples", "Cadastrar CHIP");
	    processEntity.setValue("abriuTarefa", Boolean.TRUE);

	} else if (activity.getActivityName().equals("Atualiza Data Retirada e Motivo por IccID do Cliente Antigo")) {

	    String _neoId = "";
	    _neoId = processEntity.getValue("neoId").toString();
	    
	    //QLRawFilter rawFilter = new QLRawFilter("Chp1.iccId = '" + _iccId + "' and dataRetirada is null and neoId <> " + _neoId);
	    //List<NeoObject> eventosParaAtualizar = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("Chp1Cliente"), rawFilter);
	    
	    QLGroupFilter groupFilter = new QLGroupFilter("AND");
	    groupFilter.addFilter(new QLEqualsFilter("Chp1.iccId", _iccId));
	    groupFilter.addFilter(new QLFilterIsNull("dataRemocao"));
	    groupFilter.addFilter(new QLOpFilter("neoId", "<>", Long.parseLong(_neoId)));
	    
	    List<NeoObject> eventosParaAtualizar = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("Chp1Cliente"), groupFilter);
	    
	    
	    

	    for (NeoObject no : eventosParaAtualizar) {
		EntityWrapper wp = new EntityWrapper(no);
		wp.findField("dataRemocao").setValue(data);
		wp.findField("motivoRemocao").setValue("Atualização efetuada pelo Sistema.");
	    }

	} else if(activity.getActivityName().equals("Limpa Campo IccId")) {
	    processEntity.setValue("iccId", "");
	    processEntity.setValue("confirmaIccId", Boolean.TRUE);
	}

    }

    @Override
    public void back(EntityWrapper processEntity, Activity activity) {
	// TODO Auto-generated method stub

    }

}
