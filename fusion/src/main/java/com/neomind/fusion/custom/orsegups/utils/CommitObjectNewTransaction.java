package com.neomind.fusion.custom.orsegups.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoRunnable;
import com.neomind.fusion.persist.PersistEngine;

public class CommitObjectNewTransaction extends NeoRunnable
{
	private static final Log log = LogFactory.getLog(CommitObjectNewTransaction.class);
	
	private Object object;
	
	public CommitObjectNewTransaction(Object object)
	{
		super();
		
		this.object = object;
	}
	
	@Override
	public void run() throws Exception
	{
		try
		{
			if(object != null)
			{
				PersistEngine.persist(object);
			}
			else
			{
				log.error("NeoObject não encontrado!");
			}
		}catch(Exception e)
		{
			log.error("Erro ao tentar persistir o objeto de neoId '"+this.object+"'");
			e.printStackTrace();
		}
	}
}
