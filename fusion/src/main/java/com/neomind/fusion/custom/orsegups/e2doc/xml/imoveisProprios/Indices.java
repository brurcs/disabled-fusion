package com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisProprios;

public class Indices {
    
    private String i0;
    private String i1;
    private String i2;
    private String i3;
    private String i4;
    private String i5;
    private String i6;
    private String i7;
    private String i8;
    private String i9;
    private String i10;
    private String i11;
    private String i12;
    private String i13;
    
    public String getI10() {
        return i10;
    }
    public void setI10(String i10) {
        this.i10 = i10;
    }
    public String getI0() {
        return i0;
    }
    public void setI0(String i0) {
        this.i0 = i0;
    }
    public String getI1() {
        return i1;
    }
    public void setI1(String i1) {
        this.i1 = i1;
    }
    public String getI2() {
        return i2;
    }
    public void setI2(String i2) {
        this.i2 = i2;
    }
    public String getI3() {
        return i3;
    }
    public void setI3(String i3) {
        this.i3 = i3;
    }
    public String getI4() {
        return i4;
    }
    public void setI4(String i4) {
        this.i4 = i4;
    }
    public String getI6() {
        return i6;
    }
    public void setI6(String i6) {
        this.i6 = i6;
    }
    public String getI7() {
        return i7;
    }
    public void setI7(String i7) {
        this.i7 = i7;
    }
    public String getI8() {
        return i8;
    }
    public void setI8(String i8) {
        this.i8 = i8;
    }
    public String getI9() {
        return i9;
    }
    public void setI9(String i9) {
        this.i9 = i9;
    }
    public String getI5() {
        return i5;
    }
    public void setI5(String i5) {
        this.i5 = i5;
    }
    public String getI11() {
        return i11;
    }
    public void setI11(String i11) {
        this.i11 = i11;
    }
    public String getI12() {
        return i12;
    }
    public void setI12(String i12) {
        this.i12 = i12;
    }
    public String getI13() {
        return i13;
    }
    public void setI13(String i13) {
        this.i13 = i13;
    }
    
}
