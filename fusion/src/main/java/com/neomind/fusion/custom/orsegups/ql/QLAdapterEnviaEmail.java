package com.neomind.fusion.custom.orsegups.ql;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.json.JSONException;
import org.json.JSONObject;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.integracao.dao.UrlIntegracaoDao;
import com.neomind.fusion.custom.orsegups.integracao.dto.UrlIntegracaoDTO;
import com.neomind.fusion.custom.orsegups.integracaoSlack.SlackNotifcationUtils;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsConnectionUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class QLAdapterEnviaEmail implements AdapterInterface {

	public static final Log log = LogFactory.getLog(QLAdapterEnviaEmail.class);

	private Map<Integer, String> serverInfo = OrsegupsConnectionUtils
			.getServerInfo("SIGMA");

	@Override
	public void start(Task origin, EntityWrapper processEntity,
			Activity activity) {
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		PreparedStatement pstmAux = null;
		ResultSet rsAux = null;
		Boolean enviaComunicado = false;
		String codigoMotivo = "";
		String nomeMotivo = "";

		try {

			if (processEntity.findValue("operacao") != null) {
				/**
				 * NumLoc dos locais que devem ser encaminhado msg da winker.
				 */
				Long numLocWinker = 0L;

				String codigoOperacao = (String) processEntity
						.findValue("operacao.codigo");

				Long codReg = (Long) processEntity
						.findValue("regional.usu_codreg");

				Boolean enviaGerente = (Boolean) processEntity
						.findValue("enviaGerente");

				if (processEntity.findValue("motivo") != null) {
					codigoMotivo = (String) processEntity
							.findValue("motivo.codigo");
					nomeMotivo = (String) processEntity
							.findValue("motivo.motivo");
					enviaComunicado = (Boolean) processEntity
							.findValue("motivo.enviaComunicado");
				}

				Long codOpe = Long.parseLong(codigoOperacao);
				// Solicitacao via tarefa 731836 - if ((codOpe == 3L && codReg
				// != 13L && codReg != 15L && enviaGerente) || (enviaComunicado
				// && (codOpe.equals(2L) || codOpe.equals(7L) ||
				// codOpe.equals(12L) || codOpe.equals(13L) ||
				// codOpe.equals(14L))))
				if ((codOpe == 3L && enviaGerente)
						|| codOpe.equals(15L)
						|| (enviaComunicado && (codOpe.equals(2L)
								|| codOpe.equals(7L) || codOpe.equals(12L)
								|| codOpe.equals(13L) || codOpe.equals(14L)))) {
					conn = PersistEngine.getConnection("VETORH");

					NeoObject colaborador = (NeoObject) processEntity
							.findValue("colaborador");
					EntityWrapper colaboradorWrapper = new EntityWrapper(
							colaborador);
					Long numEmpCob = null;
					Long tipColCob = null;
					Long numCadCob = null;
					String nomfunCod = "";
					String nomeEscala = "";
					String nomFun = "";
					String numCpf = "";
					String nomCar = "";
					String datAlt = "";
					String datFim = "";
					String datRet = "";
					String nomFer = "";
					String nomCarFer = "";
					String escFer = "";
					Long serCtr = 0L;
					Long numEmp = (Long) colaboradorWrapper.findValue("numemp");
					Long tipCol = (Long) colaboradorWrapper.findValue("tipcol");
					Long numCad = (Long) colaboradorWrapper.findValue("numcad");
					Long numcpf = (Long) colaboradorWrapper.findValue("numcpf");

					NeoObject posto = (NeoObject) processEntity
							.findValue("posto.nivel8");

					Long taborg = null;
					Long numloc = null;

					if (posto != null) {
						EntityWrapper postoWrapper = new EntityWrapper(posto);
						taborg = (Long) postoWrapper.findValue("taborg");
						numloc = (Long) postoWrapper.findValue("numloc");
						numLocWinker = (Long) postoWrapper.findValue("numloc");
					}

					if (numLocWinker == 0) {
						numLocWinker = (Long) colaboradorWrapper
								.findValue("numloc");
					}

					GregorianCalendar data = (GregorianCalendar) processEntity
							.getValue("data");
					GregorianCalendar dataZero = new GregorianCalendar(
							data.get(GregorianCalendar.YEAR),
							data.get(GregorianCalendar.MONTH),
							data.get(GregorianCalendar.DAY_OF_MONTH));

					NeoObject colaboradorSubstituir = (NeoObject) processEntity
							.findValue("colaboradorASubstituir");

					if (NeoUtils.safeIsNotNull(colaboradorSubstituir)) {
						EntityWrapper colaboradorSubWrapper = new EntityWrapper(
								colaboradorSubstituir);
						numEmpCob = (Long) colaboradorSubWrapper
								.findValue("numemp");
						tipColCob = (Long) colaboradorSubWrapper
								.findValue("tipcol");
						numCadCob = (Long) colaboradorSubWrapper
								.findValue("numcad");
					}

					if (((numEmpCob != null && tipColCob != null && numCadCob != null) && (numEmpCob != 0L
							&& tipColCob != 0L && numCadCob != 0L))
							|| posto != null) {

						String dat = NeoDateUtils.safeDateFormat(dataZero,
								"dd/MM/yyyy");

						List<String> listaEmails = new ArrayList<String>();
						String emails = "";
						StringBuilder sql = new StringBuilder();
						sql.append("  SELECT cvs.usu_emapre,cli.intnet,ctr.usu_emactr,orn.USU_NOMCLI, orn.NOMLOC, orn.USU_LOTORN, orn.numloc, orn.taborg, ce.emailresp, reg.usu_nomreg, cli.emanfe, pro.EMAIL, cli.cgccpf, ctr.usu_serctr  ");
						sql.append("  FROM R034FUN fun WITH(NOLOCK)  ");
						sql.append("  Inner Join R038HLO hlo WITH(NOLOCK)  On hlo.NumEmp = fun.numemp And hlo.tipcol = fun.tipcol And hlo.numcad = fun.numcad   ");
						sql.append("  And hlo.DatAlt = (Select Max(hlo2.datalt) From R038HLO hlo2 WITH(NOLOCK)  ");
						sql.append("  Where hlo2.numemp = hlo.numemp And hlo2.tipcol = hlo.tipcol And hlo2.numcad = hlo.numcad And hlo2.DatAlt <= GetDate())  ");
						sql.append("  Inner Join R016ORN orn WITH(NOLOCK) On orn.numloc = hlo.NumLoc And orn.taborg = hlo.TabOrg   ");
						sql.append("  Inner Join USU_T200REG reg WITH(NOLOCK) On orn.usu_codreg = reg.usu_codreg  ");
						sql.append("  Inner Join [FSOODB04\\SQL02].Sapiens.dbo.usu_t160cvs cvs WITH(NOLOCK) On cvs.usu_numctr = orn.usu_numctr and cvs.usu_numpos = orn.usu_numpos  ");
						sql.append("  Inner Join [FSOODB04\\SQL02].Sapiens.dbo.usu_t160ctr ctr WITH(NOLOCK) On ctr.usu_numctr = orn.usu_numctr AND ctr.usu_codemp = cvs.usu_codemp AND ctr.usu_codfil = cvs.USU_CODFIL ");
						sql.append("  Inner Join [FSOODB04\\SQL02].Sapiens.dbo.usu_t160sig sig WITH(NOLOCK) On orn.usu_numctr = sig.usu_numctr and orn.usu_numpos = sig.usu_numpos    ");
						sql.append("  Inner Join [FSOODB04\\SQL02].Sapiens.dbo.e085cli cli WITH(NOLOCK) On cli.codcli = ctr.usu_codcli   ");
						sql.append("  Inner Join ["
								+ this.serverInfo
										.get(OrsegupsConnectionUtils.NOME_SERVIDOR)
								+ "\\"
								+ this.serverInfo
										.get(OrsegupsConnectionUtils.INSTANCIA)
								+ "].SIGMA90.dbo.DBCENTRAL ce WITH(NOLOCK) On ce.cd_cliente = sig.usu_codcli    ");
						sql.append("  left Join  ["
								+ this.serverInfo
										.get(OrsegupsConnectionUtils.NOME_SERVIDOR)
								+ "\\"
								+ this.serverInfo
										.get(OrsegupsConnectionUtils.INSTANCIA)
								+ "].SIGMA90.dbo.dbPROVIDENCIA pro on PRO.cd_cliente = CE.cd_cliente ");
						sql.append("  where fun.sitafa <> 7 And orn.usu_sitati = 'S' AND cli.intnet <> 'nf.eletronica@orsegups.com.br' ");

						if ((numEmpCob != null && tipColCob != null && numCadCob != null)
								&& (numEmpCob != 0L && tipColCob != 0L && numCadCob != 0L)) {
							sql.append("  AND fun.numcad = ? AND fun.tipcol = ? AND fun.numemp = ? ");
						} else if (posto != null) {
							sql.append("  AND  orn.numloc = ? And orn.taborg = ? ");
						}

						pstm = conn.prepareStatement(sql.toString());

						if ((numEmpCob != null && tipColCob != null && numCadCob != null)
								&& (numEmpCob != 0L && tipColCob != 0L && numCadCob != 0L)) {
							pstm.setLong(1, numCadCob);
							pstm.setLong(2, tipColCob);
							pstm.setLong(3, numEmpCob);
						} else if (posto != null && numloc != null
								&& taborg != null) {
							pstm.setLong(1, numloc);
							pstm.setLong(2, taborg);
						}

						rs = pstm.executeQuery();

						String infoLocal = "";
						String postoServico = "";
						String regional = "";
						String cnpjCpf = "";
						while (rs.next()) {
							if (serCtr == null || serCtr.equals(0L)) {
								serCtr = rs.getLong("usu_serctr");
							}

							if (cnpjCpf.equals("")) {
								cnpjCpf = rs.getString("cgccpf");
							}

							if (infoLocal != null && infoLocal.isEmpty()) {
								infoLocal = rs.getString("USU_NOMCLI") + " - "
										+ rs.getString("NOMLOC") + " - "
										+ rs.getString("USU_LOTORN");
								postoServico = rs.getString("USU_LOTORN")
										+ " - " + rs.getString("NOMLOC");
							}

							if (rs.getString("usu_emapre") != null
									&& !rs.getString("usu_emapre").isEmpty()
									&& !listaEmails.contains(rs
											.getString("usu_emapre"))) {
								listaEmails.add(rs.getString("usu_emapre"));
							}

							if (rs.getString("intnet") != null
									&& !rs.getString("intnet").isEmpty()
									&& !listaEmails.contains(rs
											.getString("intnet"))) {
								listaEmails.add(rs.getString("intnet"));
							}

							if (rs.getString("usu_emactr") != null
									&& !rs.getString("usu_emactr").isEmpty()
									&& !listaEmails.contains(rs
											.getString("usu_emactr"))) {
								listaEmails.add(rs.getString("usu_emactr"));
							}

							if (rs.getString("emailresp") != null
									&& !rs.getString("emailresp").isEmpty()
									&& !listaEmails.contains(rs
											.getString("emailresp"))) {
								listaEmails.add(rs.getString("emailresp"));
							}

							if (rs.getString("usu_nomreg") != null
									&& !rs.getString("usu_nomreg").isEmpty()
									&& regional.isEmpty()) {
								regional = rs.getString("usu_nomreg");
							}

							if (rs.getString("emanfe") != null
									&& !rs.getString("emanfe").isEmpty()
									&& !listaEmails.contains(rs
											.getString("emanfe"))) {
								listaEmails.add(rs.getString("emanfe"));
							}

							if (rs.getString("EMAIL") != null
									&& !rs.getString("EMAIL").isEmpty()
									&& !listaEmails.contains(rs
											.getString("EMAIL"))) {
								listaEmails.add(rs.getString("EMAIL"));
							}

						}

						OrsegupsUtils.closeConnection(null, pstm, rs);
						// listaEmails.clear();
						// listaEmails.add("lucas.alison@orsegups.com.br");
						if ((listaEmails != null && !listaEmails.isEmpty())
								|| (emails != null && !emails.isEmpty())) {
							if (codOpe != null
									&& (codOpe.equals(3L) || codOpe.equals(2L) || codOpe
											.equals(14L))) {
								StringBuilder sqlAux = new StringBuilder();
								sqlAux.append(" SELECT SUB.NOMFUN AS NOME_SUB, CARSUB.TitCar AS CARGO_SUB, COB.usu_datalt AS DATASUBALT, COB.usu_datfim AS DATASUBFIM, ");
								sqlAux.append(" FER.NOMFUN AS NOME_FER, CARFER.TitCar AS CARGO_FER, ESCFER.NomEsc AS NOME_ESCALA_FER, COB.usu_datalt AS DATAFERALT,COB.usu_datfim AS DATAFERFIM, ");
								sqlAux.append(" COB.usu_datfim + 1 AS DATAFERRET,FER.NUMEMP AS EMPFER, SUB.NUMCPF AS CPF, FER.NUMCPF AS CPF_FER ");
								sqlAux.append(" FROM USU_T038COBFUN AS COB ");
								// //////////COLABORADOR SUBSTITUTO
								sqlAux.append(" JOIN R034FUN AS SUB ");
								sqlAux.append(" ON SUB.NUMCAD =  COB.usu_numcad ");
								sqlAux.append(" AND SUB.NUMEMP = COB.usu_numemp ");
								sqlAux.append(" AND SUB.TIPCOL = COB.usu_tipcol ");
								// /////////COLABORADOR EM FÉRIAS
								sqlAux.append(" JOIN R034FUN AS FER ");
								sqlAux.append(" ON FER.NUMCAD = COB.USU_NUMCADCOB ");
								sqlAux.append(" AND FER.NUMEMP = COB.USU_NUMEMPCOB ");
								sqlAux.append(" AND FER.TIPCOL = COB.USU_TIPCOLCOB ");
								// /////////HISTÓRICOS COLABORADOR SUBSTITUTO
								sqlAux.append(" JOIN R038HCA AS HCASUB ");
								sqlAux.append(" ON HCASUB.NUMEMP = SUB.NUMEMP ");
								sqlAux.append(" AND HCASUB.NUMCAD = SUB.NUMCAD ");
								sqlAux.append(" AND HCASUB.TIPCOL = SUB.TIPCOL ");
								sqlAux.append(" AND HCASUB.DatAlt = (SELECT MAX (DATALT) FROM R038HCA HCAMAXSUB WHERE HCAMAXSUB.NUMEMP = HCASUB.NUMEMP AND HCAMAXSUB.TIPCOL = HCASUB.TIPCOL AND HCAMAXSUB.NUMCAD = HCASUB.NUMCAD AND HCAMAXSUB.DATALT <= getDate()) ");
								// ///////HISTÓRICOS COLABORADOR FÉRIAS
								sqlAux.append(" JOIN R038HCA AS HCAFER ");
								sqlAux.append(" ON HCAFER.NUMEMP = FER.NUMEMP ");
								sqlAux.append(" AND HCAFER.NUMCAD = FER.NUMCAD ");
								sqlAux.append(" AND HCAFER.TIPCOL = FER.TIPCOL ");
								sqlAux.append(" AND HCAFER.DatAlt = (SELECT MAX (DATALT) FROM R038HCA HCAMAX WHERE HCAMAX.NUMEMP = HCAFER.NUMEMP AND HCAMAX.TIPCOL = HCAFER.TIPCOL AND HCAMAX.NUMCAD = HCAFER.NUMCAD AND HCAMAX.DATALT <= getDate()) ");
								sqlAux.append(" JOIN R038HES AS HESFER ");
								sqlAux.append(" ON HESFER.NUMEMP = FER.NUMEMP ");
								sqlAux.append(" AND HESFER.NUMCAD = FER.NUMCAD ");
								sqlAux.append(" AND HESFER.TIPCOL = FER.TIPCOL ");
								sqlAux.append(" AND HESFER.DatAlt = (SELECT MAX (DATALT) FROM R038HES HESMAX WHERE HESMAX.NUMEMP = HESFER.NUMEMP AND HESMAX.TIPCOL = HESFER.TIPCOL AND HESMAX.NUMCAD = HESFER.NUMCAD AND HESMAX.DATALT <= getDate()) ");
								// ///////CARGO COLABORADOR SUBSTITUTO
								sqlAux.append(" JOIN R024CAR AS CARSUB ");
								sqlAux.append(" ON CARSUB.CODCAR = HCASUB.CODCAR ");
								sqlAux.append(" AND CARSUB.ESTCAR = HCASUB.ESTCAR ");
								// /////// COLABORADOR FÉRIAS
								sqlAux.append(" JOIN R024CAR AS CARFER ");
								sqlAux.append(" ON CARFER.CODCAR = HCAFER.CODCAR ");
								sqlAux.append(" AND CARFER.ESTCAR = HCAFER.ESTCAR ");
								// ///////ESCALA COLABORADOR FÉRIAS
								sqlAux.append(" JOIN R006ESC AS ESCFER ");
								sqlAux.append(" ON ESCFER.CODESC = HESFER.CodEsc ");
								// //////PARAMETROS DE ENTRADA
								sqlAux.append(" WHERE ");
								sqlAux.append(" convert(varchar, COB.USU_DATALT, 103) = '"
										+ dat + "' ");
								sqlAux.append(" AND COB.usu_numcad = ? ");
								sqlAux.append(" AND COB.usu_tipcol = ? ");
								sqlAux.append(" AND COB.usu_numemp = ? ");
								sqlAux.append(" AND COB.usu_codmot = ? ");

								pstmAux = conn.prepareStatement(sqlAux
										.toString());

								pstmAux.setLong(1, numCad);
								pstmAux.setLong(2, tipCol);
								pstmAux.setLong(3, numEmp);
								pstmAux.setLong(4, codOpe);

								rsAux = pstmAux.executeQuery();
								String cpfFer = "";

								int flag = 0;
								if (rsAux.next()) {
									if (rsAux.getString("NOME_SUB") != null
											&& !rsAux.getString("NOME_SUB")
													.isEmpty()) {
										nomFun = rsAux.getString("NOME_SUB");
									}

									if (rsAux.getString("CPF") != null
											&& !rsAux.getString("CPF")
													.isEmpty()) {
										numCpf = rsAux.getString("CPF");
									}

									if (rsAux.getString("CARGO_SUB") != null
											&& !rsAux.getString("CARGO_SUB")
													.isEmpty()) {
										nomCar = rsAux.getString("CARGO_SUB");
									}

									if (rsAux.getTimestamp("DATASUBALT") != null) {
										GregorianCalendar gregorianCalendar = new GregorianCalendar();
										gregorianCalendar.setTime(rsAux
												.getTimestamp("DATASUBALT"));
										datAlt = NeoDateUtils
												.safeDateFormat(
														gregorianCalendar,
														"dd/MM/yyyy");
									}

									if (rsAux.getTimestamp("DATASUBFIM") != null) {
										GregorianCalendar gregorianCalendar = new GregorianCalendar();
										gregorianCalendar.setTime(rsAux
												.getTimestamp("DATASUBFIM"));
										datFim = NeoDateUtils
												.safeDateFormat(
														gregorianCalendar,
														"dd/MM/yyyy");
									}

									if (rsAux.getString("NOME_FER") != null
											&& !rsAux.getString("NOME_FER")
													.isEmpty()) {
										nomFer = rsAux.getString("NOME_FER");
									}

									if (rsAux.getString("CARGO_FER") != null
											&& !rsAux.getString("CARGO_FER")
													.isEmpty()) {
										nomCarFer = rsAux
												.getString("CARGO_FER");
									}

									if (rsAux.getString("NOME_ESCALA_FER") != null
											&& !rsAux.getString(
													"NOME_ESCALA_FER")
													.isEmpty()) {
										nomeEscala = rsAux
												.getString("NOME_ESCALA_FER");
									}

									if (rsAux.getTimestamp("DATAFERRET") != null) {
										GregorianCalendar gregorianCalendar = new GregorianCalendar();
										gregorianCalendar.setTime(rsAux
												.getTimestamp("DATAFERRET"));
										datRet = NeoDateUtils
												.safeDateFormat(
														gregorianCalendar,
														"dd/MM/yyyy");
									}

									if (rsAux.getString("CPF_FER") != null
											&& !rsAux.getString("CPF_FER")
													.isEmpty()) {
										cpfFer = rsAux.getString("CPF_FER");
									}

									flag = 1;
								}

								if (flag == 1) {
									String numRg = (String) getRg(
											numEmp.toString(),
											tipCol.toString(),
											numCad.toString());
									enviaEmailCliente(listaEmails, nomFer,
											nomCarFer, nomeEscala, datAlt,
											datFim, datRet,
											numCadCob.toString(),
											numEmpCob.toString(),
											tipColCob.toString(), nomFun,
											numEmp.toString(),
											tipCol.toString(),
											numCad.toString(), codOpe,
											infoLocal, nomeMotivo, nomCar,
											postoServico, numCpf, numRg, null,
											regional, serCtr);
									try {
										String texto = retornaTituloEDescricao(
												nomFer, nomCarFer, nomeEscala,
												datAlt, datFim, datRet,
												numCadCob.toString(),
												numEmpCob.toString(),
												tipColCob.toString(), nomFun,
												numEmp.toString(),
												tipCol.toString(),
												numCad.toString(), codOpe,
												infoLocal, nomeMotivo, nomCar,
												postoServico, numCpf, numRg,
												null, regional);
										String[] descricoes = texto.split(";");

										if (validarNumLoc(numLocWinker)) {
											integracaoWinker(cnpjCpf, cpfFer,
													nomFer, nomCarFer, numCpf,
													nomFun, nomCar,
													descricoes[0],
													descricoes[1], datAlt,
													datFim,
													String.valueOf(codOpe),
													activity.getCode(), false,
													numLocWinker);
										}
									} catch (IOException e1) {

										StringBuilder parametros = new StringBuilder();

										parametros.append("cnpjCpf: " + cnpjCpf
												+ "\n");
										parametros.append("cpfFer: " + cpfFer
												+ "\n");
										parametros.append("nomFer: " + nomFer
												+ "\n");
										parametros.append("nomCarFer: "
												+ nomCarFer + "\n");
										parametros.append("numCpf: " + numCpf
												+ "\n");
										parametros.append("nomFun: " + nomFun
												+ "\n");
										parametros.append("nomCar: " + nomCar
												+ "\n");
										parametros.append("datAlt: " + datAlt
												+ "\n");
										parametros.append("datFim: " + datFim
												+ "\n");
										parametros
												.append("ope: "
														+ String.valueOf(codOpe)
														+ "\n");
										parametros
												.append("activity.getCode(): "
														+ activity.getCode()
														+ "\n");
										parametros.append("numLocWinker: "
												+ numLocWinker + "\n");

										StringWriter erros = new StringWriter();
										e1.printStackTrace(new PrintWriter(
												erros));

										System.err
												.println("###Erro na classe QlAdapterEnviaEmail - Inicio");
										e1.printStackTrace();
										System.err
												.println("###Erro na classe QlAdapterEnviaEmail - Fim");

										SlackNotifcationUtils
												.sendNotification(
														SlackNotifcationUtils.ERROR_NOTIFICATIOIN,
														"Sistema Fusion Erro na Classe QlAdapterEnviaEmail "
																+ erros.toString()
																+ "\n parametros : "
																+ parametros);

										try {
											erros.close();
										} catch (IOException e2) {
											e2.printStackTrace();
										}

									}
								}
							} else if (codOpe != null
									&& (codOpe.equals(7L) || codOpe.equals(12L) || codOpe
											.equals(13L))) {
								nomFer = (String) colaboradorWrapper
										.findValue("nomfun");
								nomCarFer = (String) colaboradorWrapper
										.findValue("titred");
								tipCol = (Long) colaboradorWrapper
										.findValue("tipcol");

								GregorianCalendar dataInicial = (GregorianCalendar) processEntity
										.findValue("data");
								GregorianCalendar dataFinal = (GregorianCalendar) processEntity
										.findValue("dataFinal");
								datAlt = NeoDateUtils.safeDateFormat(
										dataInicial, "dd/MM/yyyy HH:mm");
								datFim = NeoDateUtils.safeDateFormat(dataFinal,
										"dd/MM/yyyy HH:mm");

								Long motivo = Long.parseLong(codigoMotivo);
								NeoObject colaboradorFaltante = null;
								if (motivo != null
										&& (motivo.equals(1L)
												|| motivo.equals(4L)
												|| motivo.equals(6L)
												|| motivo.equals(7L) || motivo
													.equals(9L))) {

									if (motivo.equals(1L)) {
										colaboradorFaltante = (NeoObject) processEntity
												.findValue("colaboradorFaltante");
									} else {
										colaboradorFaltante = (NeoObject) processEntity
												.findValue("colaboradorFaltante2");
									}

									if (NeoUtils
											.safeIsNotNull(colaboradorFaltante)) {
										EntityWrapper colaboradorFaltanteWrapper = new EntityWrapper(
												colaboradorFaltante);
										numEmpCob = (Long) colaboradorFaltanteWrapper
												.findValue("numemp");
										tipColCob = (Long) colaboradorFaltanteWrapper
												.findValue("tipcol");
										numCadCob = (Long) colaboradorFaltanteWrapper
												.findValue("numcad");
										nomFun = (String) colaboradorFaltanteWrapper
												.findValue("nomfun");
										nomCar = (String) colaboradorFaltanteWrapper
												.findValue("titred");
										String numRg = (String) getRg(
												numEmp.toString(),
												tipCol.toString(),
												numCad.toString());
										enviaEmailCliente(listaEmails, nomFer,
												nomCarFer, nomeEscala, datAlt,
												datFim, "",
												numCadCob.toString(),
												numEmpCob.toString(),
												tipColCob.toString(), nomFun,
												numEmp.toString(),
												tipCol.toString(),
												numCad.toString(), codOpe,
												infoLocal, nomeMotivo, nomCar,
												postoServico,
												numcpf.toString(), numRg,
												motivo, regional, serCtr);

									}
								} else {
									enviaEmailCliente(listaEmails, nomFer,
											nomCarFer, nomeEscala, datAlt,
											datFim, "", "", "", "", "",
											numEmp.toString(),
											tipCol.toString(),
											numCad.toString(), codOpe,
											infoLocal, nomeMotivo, nomCar,
											postoServico, "", "",
											Long.parseLong(codigoMotivo),
											regional, serCtr);
								}

								try {
									String cpfSub = "";
									boolean inverter = false;
									if (colaboradorFaltante != null) {
										EntityWrapper colaboradorFaltanteWrapper = new EntityWrapper(
												colaboradorFaltante);
										cpfSub = String
												.valueOf((Long) colaboradorFaltanteWrapper
														.findValue("numcpf"));
										inverter = true;
									} else {
										cpfSub = "";
									}

									String cpf = String
											.valueOf((Long) colaboradorWrapper
													.findValue("numcpf"));
									String numRg = (String) getRg(
											numEmp.toString(),
											tipCol.toString(),
											numCad.toString());
									String texto = "";
									if (NeoUtils
											.safeIsNotNull(colaboradorFaltante)) {
										texto = retornaTituloEDescricao(nomFer,
												nomCarFer, nomeEscala, datAlt,
												datFim, datRet,
												numCadCob.toString(),
												numEmpCob.toString(),
												tipColCob.toString(), nomFun,
												numEmp.toString(),
												tipCol.toString(),
												numCad.toString(), codOpe,
												infoLocal, nomeMotivo, nomCar,
												postoServico, numCpf, numRg,
												null, regional);
									} else {
										texto = retornaTituloEDescricao(nomFer,
												nomCarFer, nomeEscala, datAlt,
												datFim, "", "", "", "", "",
												numEmp.toString(),
												tipCol.toString(),
												numCad.toString(), codOpe,
												infoLocal, nomeMotivo, nomCar,
												postoServico, "", "",
												Long.parseLong(codigoMotivo),
												regional);
									}

									String[] descricoes = texto.split(";");
									if (validarNumLoc(numLocWinker)) {
										integracaoWinker(cnpjCpf, cpf, nomFer,
												nomCarFer, cpfSub, nomFun,
												nomCar, descricoes[0],
												descricoes[1], datAlt, datFim,
												String.valueOf(codOpe),
												activity.getCode(), inverter,
												numLocWinker);
									}
								} catch (IOException e1) {
									e1.printStackTrace();
								}
							} else if (codOpe != null && codOpe.equals(15L)) {
								StringBuilder sqlAux = new StringBuilder();
								sqlAux.append(" SELECT ");
								sqlAux.append(" SUB.NOMFUN AS NOME_SUB, ");
								sqlAux.append(" CARSUB.TitCar AS CARGO_SUB, ");
								sqlAux.append(" FER.NOMFUN AS NOME_FER, ");
								sqlAux.append(" CARFER.TitCar AS CARGO_FER, ");
								sqlAux.append(" ESCFER.NomEsc AS NOME_ESCALA_FER, ");
								sqlAux.append(" FER.NUMEMP AS EMPFER, ");
								sqlAux.append(" SUB.NUMCPF AS CPF, ");
								sqlAux.append(" FER.NUMCPF AS CPF_FER ");
								sqlAux.append(" FROM USU_T038COBFUN AS COB  ");
								sqlAux.append(" JOIN R034FUN AS SUB  ON SUB.NUMCAD =  COB.usu_numcad  AND SUB.NUMEMP = COB.usu_numemp  AND SUB.TIPCOL = COB.usu_tipcol ");
								sqlAux.append(" JOIN R034FUN AS FER  ON FER.NUMCAD = COB.USU_NUMCADCOB  AND FER.NUMEMP = COB.USU_NUMEMPCOB  AND FER.TIPCOL = COB.USU_TIPCOLCOB ");
								sqlAux.append(" JOIN R038HCA AS HCASUB  ON HCASUB.NUMEMP = SUB.NUMEMP  AND HCASUB.NUMCAD = SUB.NUMCAD  AND HCASUB.TIPCOL = SUB.TIPCOL  AND HCASUB.DatAlt = (SELECT MAX (DATALT) FROM R038HCA HCAMAXSUB WHERE HCAMAXSUB.NUMEMP = HCASUB.NUMEMP AND HCAMAXSUB.TIPCOL = HCASUB.TIPCOL AND HCAMAXSUB.NUMCAD = HCASUB.NUMCAD AND HCAMAXSUB.DATALT <= getDate()) ");
								sqlAux.append(" JOIN R038HCA AS HCAFER  ON HCAFER.NUMEMP = FER.NUMEMP  AND HCAFER.NUMCAD = FER.NUMCAD  AND HCAFER.TIPCOL = FER.TIPCOL  AND HCAFER.DatAlt = (SELECT MAX (DATALT) FROM R038HCA HCAMAX WHERE HCAMAX.NUMEMP = HCAFER.NUMEMP AND HCAMAX.TIPCOL = HCAFER.TIPCOL AND HCAMAX.NUMCAD = HCAFER.NUMCAD AND HCAMAX.DATALT <= getDate())  ");
								sqlAux.append(" JOIN R038HES AS HESFER  ON HESFER.NUMEMP = FER.NUMEMP  AND HESFER.NUMCAD = FER.NUMCAD  AND HESFER.TIPCOL = FER.TIPCOL  AND HESFER.DatAlt = (SELECT MAX (DATALT) FROM R038HES HESMAX WHERE HESMAX.NUMEMP = HESFER.NUMEMP AND HESMAX.TIPCOL = HESFER.TIPCOL AND HESMAX.NUMCAD = HESFER.NUMCAD AND HESMAX.DATALT <= getDate())  ");
								sqlAux.append(" JOIN R024CAR AS CARSUB  ON CARSUB.CODCAR = HCASUB.CODCAR  AND CARSUB.ESTCAR = HCASUB.ESTCAR  ");
								sqlAux.append(" JOIN R024CAR AS CARFER  ON CARFER.CODCAR = HCAFER.CODCAR  AND CARFER.ESTCAR = HCAFER.ESTCAR  ");
								sqlAux.append(" JOIN R006ESC AS ESCFER  ON ESCFER.CODESC = HESFER.CodEsc  ");
								sqlAux.append(" WHERE  ");
								sqlAux.append(" convert(varchar, COB.USU_DATALT, 103) = '"
										+ dat + "' ");
								sqlAux.append(" AND COB.usu_numcad = ? ");
								sqlAux.append(" AND COB.usu_tipcol = ? ");
								sqlAux.append(" AND COB.usu_numemp = ? ");
								sqlAux.append(" AND COB.usu_codmot = ? ");

								pstmAux = conn.prepareStatement(sqlAux
										.toString());

								pstmAux.setLong(1, numCad);
								pstmAux.setLong(2, tipCol);
								pstmAux.setLong(3, numEmp);
								pstmAux.setLong(4, codOpe);

								rsAux = pstmAux.executeQuery();
								String cpfFer = "";

								int flag = 0;
								if (rsAux.next()) {
									if (rsAux.getString("NOME_SUB") != null
											&& !rsAux.getString("NOME_SUB")
													.isEmpty()) {
										nomFun = rsAux.getString("NOME_SUB");
									}

									if (rsAux.getString("CPF") != null
											&& !rsAux.getString("CPF")
													.isEmpty()) {
										numCpf = rsAux.getString("CPF");
									}

									if (rsAux.getString("CARGO_SUB") != null
											&& !rsAux.getString("CARGO_SUB")
													.isEmpty()) {
										nomCar = rsAux.getString("CARGO_SUB");
									}

									if (rsAux.getString("NOME_FER") != null
											&& !rsAux.getString("NOME_FER")
													.isEmpty()) {
										nomFer = rsAux.getString("NOME_FER");
									}

									if (rsAux.getString("CARGO_FER") != null
											&& !rsAux.getString("CARGO_FER")
													.isEmpty()) {
										nomCarFer = rsAux
												.getString("CARGO_FER");
									}

									if (rsAux.getString("NOME_ESCALA_FER") != null
											&& !rsAux.getString(
													"NOME_ESCALA_FER")
													.isEmpty()) {
										nomeEscala = rsAux
												.getString("NOME_ESCALA_FER");
									}

									if (rsAux.getString("CPF_FER") != null
											&& !rsAux.getString("CPF_FER")
													.isEmpty()) {
										cpfFer = rsAux.getString("CPF_FER");
									}

									flag = 1;
								}

								if (flag == 1) {
									GregorianCalendar dataInicio = (GregorianCalendar) processEntity
											.getValue("data");
									GregorianCalendar dataFim = (GregorianCalendar) processEntity
											.getValue("dataFinal");
									String datIni = NeoDateUtils
											.safeDateFormat(dataInicio,
													"dd/MM/yyyy");
									String datFinal = NeoDateUtils
											.safeDateFormat(dataFim,
													"dd/MM/yyyy");
									String numRg = (String) getRg(
											numEmp.toString(),
											tipCol.toString(),
											numCad.toString());
									enviaEmailCliente(listaEmails, nomFer,
											nomCarFer, nomeEscala, datIni,
											datFinal, datRet,
											numCadCob.toString(),
											numEmpCob.toString(),
											tipColCob.toString(), nomFun,
											numEmp.toString(),
											tipCol.toString(),
											numCad.toString(), codOpe,
											infoLocal, nomeMotivo, nomCar,
											postoServico, numCpf, numRg, null,
											regional, serCtr);
									try {
										String texto = retornaTituloEDescricao(
												nomFer, nomCarFer, nomeEscala,
												datAlt, datFim, datRet,
												numCadCob.toString(),
												numEmpCob.toString(),
												tipColCob.toString(), nomFun,
												numEmp.toString(),
												tipCol.toString(),
												numCad.toString(), codOpe,
												infoLocal, nomeMotivo, nomCar,
												postoServico, numCpf, numRg,
												null, regional);
										String[] descricoes = texto.split(";");
										if (validarNumLoc(numLocWinker)) {
											integracaoWinker(cnpjCpf, cpfFer,
													nomFer, nomCarFer, numCpf,
													nomFun, nomCar,
													descricoes[0],
													descricoes[1], datAlt,
													datFim,
													String.valueOf(codOpe),
													activity.getCode(), false,
													numLocWinker);
										}
									} catch (IOException e1) {
										e1.printStackTrace();
									}
								}
							}

							OrsegupsUtils.closeConnection(conn, pstmAux, rsAux);

						} else {
							log.error("Erro ao enviar e-mail, não foi encontrado o destinatário.");
						}
					}
				}
			}

		} catch (Exception e) {
			log.error("Erro ao enviar e-mail ao cliente.");

			StringBuilder parametros = new StringBuilder();
			parametros.append("neoid: " + processEntity.findValue("neoId")
					+ "\n");
			parametros.append("operacao: "
					+ processEntity.findValue("operacao") + "\n");
			parametros.append("codop: "
					+ processEntity.findValue("operacao.codigo") + "\n");
			parametros.append("regioanal cod: "
					+ processEntity.findValue("regional.usu_codreg") + "\n");
			parametros.append("motivo codigo: "
					+ processEntity.findValue("motivo.codigo") + "\n");
			parametros.append("motivo motivo: "
					+ processEntity.findValue("motivo.motivo") + "\n");
			parametros.append("motivo comunicado: "
					+ processEntity.findValue("motivo.enviaComunicado") + "\n");
			parametros.append("envia gerente: "
					+ processEntity.findValue("enviaGerente") + "\n");

			StringWriter erros = new StringWriter();
			e.printStackTrace(new PrintWriter(erros));

			System.err
					.println("###Erro na classe QlAdapterEnviaEmail - Inicio");
			e.printStackTrace();
			System.err.println("###Erro na classe QlAdapterEnviaEmail - Fim");

			SlackNotifcationUtils.sendNotification(
					SlackNotifcationUtils.ERROR_NOTIFICATIOIN,
					"Sistema Fusion Erro na Classe QlAdapterEnviaEmail "
							+ erros.toString() + "\n parametros : "
							+ parametros);

			try {
				erros.close();
			} catch (IOException e2) {
				e2.printStackTrace();
			}
		} finally {
			OrsegupsUtils.closeConnection(null, pstm, rs);
			OrsegupsUtils.closeConnection(conn, pstmAux, rsAux);
		}
	}

	public void integracaoWinker(String cnpjCli, String cpf, String nome,
			String cargo, String cpfSub, String nomeSub, String cargoSub,
			String titulo, String mensagem, String datIni, String datFim,
			String tipoOpe, String code, boolean inverter, Long numLoc)
			throws IOException, JSONException {
		System.out.println("#Winker#tarefa" + code
				+ " ---Iniciando integracaoWinker para tarefa: " + code
				+ " Cliente= " + cnpjCli + " cpf do colaborador : " + cpf);
		// String url =
		// "http://dsoo17:8081/winker/integracao/fusion/aviso/cobertura";
		// String url =
		// "http://apps.orsegups.com.br:8280/services/fusion-integracao-winker-aviso-cobertura";
		// String url =
		// "https://apps.orsegups.com.br:8243/services/fusion-integracao-winker-aviso-cobertura";

		UrlIntegracaoDTO urlIntegracaoDTO = new UrlIntegracaoDao()
				.buscaUrlPorNome("winkerCobertura");

		// verifica se o Dao touxe nulo e seta uma url padrao
		if (urlIntegracaoDTO == null)
			new UrlIntegracaoDTO(
					"https://apps.orsegups.com.br:8243/services/winker-integracao-fusion-aviso-cobertura",
					"499c7a9a40f3bcda2c54471d03f494dc");

		String url = urlIntegracaoDTO.getUrl();

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", "Mozilla/5.0");
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		con.setRequestProperty("X-Auth-Token", urlIntegracaoDTO.getToken());
		long dtIni = 0;
		long dtFim = 0;

		if (Long.parseLong(tipoOpe) < 10) {
			tipoOpe = "00" + tipoOpe;
		} else if (Long.parseLong(tipoOpe) < 100) {
			tipoOpe = "0" + tipoOpe;
		}

		try {
			GregorianCalendar dataIni = null;
			if (datIni != null && !datIni.equals("")) {
				dataIni = OrsegupsUtils.stringToGregorianCalendar(datIni,
						"dd/MM/yyyy");
				dtIni = dataIni.getTimeInMillis();
			}

			GregorianCalendar dataFim = null;
			if (datFim != null && !datFim.equals("")) {
				dataFim = OrsegupsUtils.stringToGregorianCalendar(datFim,
						"dd/MM/yyyy");
				dtFim = dataFim.getTimeInMillis();
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

		String urlParameters = "";
		if (cpfSub != null && !cpfSub.isEmpty() && !inverter) {
			urlParameters = "cnpj=" + cnpjCli + "&cpf=" + cpf + "&nome=" + nome
					+ "&cargo=" + cargo + "&cpfSubstituto=" + cpfSub
					+ "&nomeSubstituto=" + nomeSub + "&cargoSubstituto="
					+ cargoSub + "&" + "titulo=" + titulo + "&mensagem="
					+ mensagem + "&datIni=" + dtIni + "&datFim=" + dtFim
					+ "&tipoOperacao=" + tipoOpe + "&numLoc=" + numLoc;
		} else {
			urlParameters = "cnpj=" + cnpjCli + "&cpf=" + cpfSub + "&nome="
					+ nomeSub + "&cargo=" + cargoSub + "&cpfSubstituto=" + cpf
					+ "&nomeSubstituto=" + nome + "&cargoSubstituto=" + cargo
					+ "&" + "titulo=" + titulo + "&mensagem=" + mensagem
					+ "&datIni=" + dtIni + "&datFim=" + dtFim
					+ "&tipoOperacao=" + tipoOpe + "&numLoc=" + numLoc;
		}

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(wr,
				"UTF-8"));

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		StringBuilder sql = new StringBuilder();

		sql.append("insert into integracao_winker (cnpj_cliente,cpf,nome,cargo,cpf_substituto,nome_substituto,cargo_substituto,titulo,mensagem,data_inicio,data_fim,tipo_operacao,num_loc,data_cadastro)");
		sql.append(" values (?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");

		try {
			conn = PersistEngine.getConnection("TIDB");
			pstm = conn.prepareStatement(sql.toString());

			pstm.setString(1, cnpjCli);
			pstm.setString(2, cpf);
			pstm.setString(3, nome);
			pstm.setString(4, cargo);
			pstm.setString(5, cpfSub);
			pstm.setString(6, nomeSub);
			pstm.setString(7, cargoSub);
			pstm.setString(8, titulo);
			pstm.setString(9, mensagem);
			pstm.setTimestamp(10, new Timestamp(dtIni));
			pstm.setTimestamp(11, new Timestamp(dtFim));
			pstm.setString(12, tipoOpe);
			pstm.setLong(13, numLoc);
			pstm.setTimestamp(14,
					new Timestamp(new GregorianCalendar().getTimeInMillis()));

			rs = pstm.executeQuery();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

		writer.write(urlParameters);
		writer.flush();
		writer.close();

		int responseCode = con.getResponseCode();
		System.out.println("#Winker#tarefa" + code
				+ " ---\nSending 'POST' request to URL : " + url);
		System.out.println("#Winker#tarefa" + code + " ---Post parameters : "
				+ urlParameters);
		System.out.println("#Winker#tarefa" + code + " ---Response Code : "
				+ responseCode);
		System.out.println("#Winker#tarefa" + code + " ---inverter : "
				+ inverter);

		BufferedReader in = new BufferedReader(new InputStreamReader(
				con.getInputStream(), "UTF-8"));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		JSONObject jsonObj = new JSONObject(response.toString());
		Long status = Long.parseLong(jsonObj.getString("status"));
		System.out.println("#Winker#tarefa" + code
				+ " ---Retorno Status pela URL " + url + " com os parametros "
				+ urlParameters + " = " + status);
		if (status < 100) {
			abrirTarefaSimplesErroWinker(response.toString(), cnpjCli, url,
					urlParameters, code);
		} else if (status == 101) {
			try {
				StringBuilder corpo = new StringBuilder();
				corpo.append("Houve um problema ao tentar integrar as informações com a winker. \n");
				corpo.append("Segue retorno obtido pelo Winker:<br>");
				corpo.append("<b>JSon: </b>" + response.toString() + "<br>");
				corpo.append("<b>Url: </b>" + url + "<br>");
				corpo.append("<b>Parametros: </b>" + urlParameters + "<br>");
				encaminharEmailIntegracao(corpo);
			} catch (EmailException e) {
				e.printStackTrace();
			}
		}

		System.out.println("#Winker#tarefa" + code
				+ " ---Finalizou integracaoWinker para tarefa: " + code
				+ " -  Cliente= " + cnpjCli + " cpf do colaborador : " + cpf);
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
	}

	public String abrirTarefaSimplesErroWinker(String erro, String cpfCnpj,
			String url, String urlParameters, String codTar) throws IOException {
		String solicitante = "fabio.meier";
		String executor = "luiz.reis";
		String titulo = "Problemas na integração do Winker realizado via fluxo R030 - tarefa: "
				+ codTar;
		StringBuilder descricao = new StringBuilder();
		descricao.append("Segue retorno obtido pelo Winker:<br>");
		descricao.append("<b>JSon: </b>" + erro + "<br>");
		descricao.append("<b>Url: </b>" + url + "<br>");
		descricao.append("<b>Parametros: </b>" + urlParameters + "<br>");

		String textoQueSeraEscrito = descricao.toString();
		BufferedWriter output = null;
		NeoFile neoFile = null;
		try {
			File file = new File("arquivo.txt");
			file.createNewFile();

			FileWriter fw = new FileWriter(file, true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(textoQueSeraEscrito);

			bw.close();
			fw.close();

			neoFile = OrsegupsUtils.criaNeoFile(file);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);

		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		String code = iniciarTarefaSimples.abrirTarefa(solicitante, executor,
				titulo, descricao.toString(), "2", "sim", prazo, neoFile);

		return code;

	}

	public String retornaTituloEDescricao(String nomfun, String sisCar,
			String nomeEscala, String dataInicial, String dataFinal,
			String retorno, String numCadCob, String numEmpCob,
			String tipColCob, String nomfunCod, String numEmp, String tipCol,
			String numCad, Long codOpe, String infoLocal, String nomeMotivo,
			String nomCar, String postoServico, String cpf, String rg,
			Long codMotivo, String regional) {
		StringBuilder texto = new StringBuilder();
		if (codOpe != null && codOpe.equals(3L)) {
			texto.append("Comunicado de Férias Orsegups;");
		}
		// if (codOpe != null && (codOpe.equals(2L) || codOpe.equals(7L) ||
		// codOpe.equals(12L) || codOpe.equals(13L) || codOpe.equals(14L)))
		else {
			texto.append("Comunicado de Cobertura em Seu Posto;");
		}

		if (codOpe != null
				&& (codOpe.equals(3L) || codOpe.equals(2L) || codOpe
						.equals(14L))) {
			texto.append(" <p>   Comunicamos que o colaborador " + nomfun);
			texto.append(", cargo " + sisCar
					+ ", que atua no horário e escala " + nomeEscala
					+ ", neste posto de serviço, ");
		}

		if (codOpe != null
				&& (codOpe.equals(7L) || codOpe.equals(12L) || codOpe
						.equals(13L))) {
			texto.append(" <p> Comunicamos cobertura operacional realizada pelo colaborador  "
					+ nomfun);
			texto.append(" , cargo " + sisCar + ", neste posto de serviço "
					+ postoServico);
			texto.append(", no período de " + dataInicial + "H até "
					+ dataFinal + "H.<br/><br/>");
		}

		if (codOpe != null && codOpe.equals(3L)) {
			texto.append(" estará em gozo de férias no período de "
					+ dataInicial + " até " + dataFinal
					+ ", e retornará as suas ");
			texto.append(" atividades no dia "
					+ retorno
					+ ". Durante esse período está programado para substituí-lo");
			texto.append(" " + nomfunCod + ", devidamente capacitado ");
			texto.append(" para a função.</p> ");
		}

		if (codOpe != null && (codOpe.equals(2L) || codOpe.equals(14L))) {

			texto.append(" será substituído temporariamente no período de "
					+ dataInicial.substring(0, 10) + " até "
					+ dataFinal.substring(0, 10) + ". ");
			texto.append(" Durante esse período está programado para substituí-lo,");
			texto.append(" " + nomfunCod
					+ ", devidamente capacitado para a função.<br/><br/>");
		}

		if (codOpe != null
				&& (codOpe.equals(2L) || codOpe.equals(7L)
						|| codOpe.equals(12L) || codOpe.equals(13L) || codOpe
							.equals(14L))) {
			texto.append(" Motivo da cobertura: <strong>" + nomeMotivo
					+ "</strong>.</p>");
		}

		if (codOpe != null && codOpe.equals(15L)) {
			texto.append(" Comunicamos que o colaborador " + nomfun);
			texto.append(", cargo "
					+ sisCar
					+ ", que atua neste posto de serviço, terá sua escala invertida com o ");
			texto.append("colaborador " + nomfunCod + ".");
		}

		return texto.toString();
	}

	private void enviaEmailCliente(List<String> emailCliente, String nomfun,
			String sisCar, String nomeEscala, String dataInicial,
			String dataFinal, String retorno, String numCadCob,
			String numEmpCob, String tipColCob, String nomfunCod,
			String numEmp, String tipCol, String numCad, Long codOpe,
			String infoLocal, String nomeMotivo, String nomCar,
			String postoServico, String cpf, String rg, Long codMotivo,
			String regional, Long serctr) {

		try {

			MailSettings settings = (MailSettings) PersistEngine
					.reload(FusionRuntime.getInstance().getSettings()
							.getMailSettings());

			MailSettings mailClone = new MailSettings();
			mailClone.setMinutesInterval(settings.getMinutesInterval());
			mailClone.setFromEMail("presenca@orsegups.com.br");
			mailClone.setFromName("Orsegups Participações S.A.");
			mailClone.setRenderServer(settings.getRenderServer());
			mailClone.setSecurityFromEmail(settings.getSecurityFromEmail());
			mailClone.setSecurityCopyEmail(settings.getSecurityCopyEmail());
			mailClone.setSmtpSettings(settings.getSmtpSettings());
			mailClone.setPort(settings.getPort());
			mailClone.setSmtpServer(settings.getSmtpServer());
			mailClone.setEnabled(settings.isEnabled());

			if (mailClone != null) {

				HtmlEmail noUserEmail = new HtmlEmail();
				StringBuilder noUserMsg = new StringBuilder();
				noUserEmail.setCharset("ISO-8859-1");
				for (String email : emailCliente) {
					if (email != null && !email.isEmpty()
							&& email.contains(";")) {
						String[] arrayEmail = email.split(";");

						for (String emailStr : arrayEmail) {
							if (validEmail(emailStr)
									&& validExcessaoEmail(emailStr))
								noUserEmail.addTo(emailStr);
						}
					} else {
						if (validEmail(email) && validExcessaoEmail(email))
							noUserEmail.addTo(email);
					}
				}

				if (noUserEmail.getToAddresses() != null
						&& !noUserEmail.getToAddresses().isEmpty()) {
					noUserEmail.addBcc("emailautomatico@orsegups.com.br");
					
					if (serctr != null && (serctr == 1L || serctr == 4L || serctr == 51L || serctr == 54L)) {
						String emailGerente = "";
						emailGerente = getEmailGerente(regional);
						noUserEmail.addBcc(emailGerente);						
					}

					noUserEmail.setSubject("Presença - " + infoLocal);

					noUserMsg.append(" <!DOCTYPE html>");
					noUserMsg
							.append(" <html xmlns='http://www.w3.org/1999/xhtml'>");
					noUserMsg.append(" <html> ");
					noUserMsg.append(" <head> ");
					noUserMsg
							.append(" <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" /> ");
					noUserMsg.append(" <meta charset=\"utf-8\"> ");
					noUserMsg
							.append(" <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"> ");

					if (codOpe != null && codOpe.equals(3L)) {
						noUserMsg
								.append(" <title>Comunicado de Férias Orsegups</title> ");
					}

					if (codOpe != null
							&& (codOpe.equals(2L) || codOpe.equals(7L)
									|| codOpe.equals(12L) || codOpe.equals(13L) || codOpe
										.equals(14L)) && codMotivo != null
							&& !codMotivo.equals(3L)) {
						noUserMsg
								.append(" <title>Comunicado de Cobertura em Seu Posto</title> ");
					} else if (codOpe != null
							&& (codOpe.equals(2L) || codOpe.equals(7L)
									|| codOpe.equals(12L) || codOpe.equals(13L) || codOpe
										.equals(14L)) && codMotivo != null
							&& codMotivo.equals(3L)) {
						noUserMsg
								.append(" <title>Comunicado de Treinamento</title> ");
					} else if (codOpe != null && codOpe.equals(15L)) {
						noUserMsg
								.append("<title>Comunicado de Inversão de Escala</title>");
					}

					noUserMsg
							.append(" <link rel=\"stylesheet\"	type=\"text/css\" /> ");
					noUserMsg.append(" <style TYPE=\"text/css\"> ");
					noUserMsg.append("   p,pre,body,table ");
					noUserMsg.append("   { ");
					noUserMsg.append("   font-size:10.0pt; ");
					noUserMsg.append("   font-family:'Arial';");
					noUserMsg.append("   } ");
					noUserMsg.append("  </style> ");
					noUserMsg.append("  </head> ");

					noUserMsg.append(" <body bgcolor=\"#FFFFFF\"> ");
					noUserMsg
							.append(" <table width=\"100%\"  cellspacing=\"0\" cellpadding=\"0\"> ");
					noUserMsg.append(" <tbody> ");
					noUserMsg.append(" <tr> ");
					noUserMsg.append(" <td>&nbsp;</td> ");
					noUserMsg.append(" </tr> ");
					noUserMsg.append(" <tr> ");
					noUserMsg.append(" <td> ");
					noUserMsg
							.append(" <table width=\"800\"  align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin-top:10px;\"> ");
					noUserMsg
							.append(" <tbody style=\"background-color:#FFF;padding:10px;\"> ");
					noUserMsg.append(" <tr>  ");
					noUserMsg.append(" <td width=\"800\"> ");
					noUserMsg
							.append(" <table width=\"100%\"  align=\"left\" cellpadding=\"0\" cellspacing=\"0\"> ");
					noUserMsg.append(" <tr> ");
					noUserMsg
							.append(" <td align=\"center\" style=\"font-size: 20px; font-weight: 900; line-height: 1em; color: #333; font-family:monospace;\"> ");
					noUserMsg
							.append(" <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/topo-email-ferias.jpg\" width=\"100%\" height=\"211\" alt=\"\"/></td> ");
					noUserMsg.append(" </tr> ");
					noUserMsg.append(" <tr>  ");

					if (codOpe != null && codOpe.equals(3L)) {
						noUserMsg
								.append(" <td align=\"center\" style=\"font-size: 20px; font-weight: 900; line-height: 1em; color: #333; font-family:monospace;padding:20px;border-bottom:1px dashed #333;\">COMUNICADO DE FÉRIAS</td> ");
					}
					if (codOpe != null
							&& (codOpe.equals(2L) || codOpe.equals(7L)
									|| codOpe.equals(12L) || codOpe.equals(13L) || codOpe
										.equals(14L))) {
						noUserMsg
								.append(" <td align=\"center\" style=\"font-size: 20px; font-weight: 900; line-height: 1em; color: #333; font-family:monospace;padding:20px;border-bottom:1px dashed #333;\">COMUNICADO DE COBERTURA EM SEU POSTO</td> ");
					}
					if (codOpe != null && codOpe.equals(15L)) {
						noUserMsg
								.append(" <td align=\"center\" style=\"font-size: 20px; font-weight: 900; line-height: 1em; color: #333; font-family:monospace;padding:20px;border-bottom:1px dashed #333;\">COMUNICADO DE INVERSÃO DE ESCALA</td> ");
					}

					noUserMsg.append(" </tr>  ");
					noUserMsg.append(" <tr>  ");
					noUserMsg
							.append(" <td style=\"font-size: 0; line-height: 0;\" height=\"20\"><table width=\"96%\" align=\"left\"  cellpadding=\"0\" cellspacing=\"0\">  ");
					noUserMsg.append(" <tr>   ");
					noUserMsg
							.append(" <td style=\"font-size: 0; line-height: 0;\" height=\"20\">&nbsp;</td>  ");
					noUserMsg.append(" </tr>   ");
					noUserMsg.append(" </table></td>  ");
					noUserMsg.append(" </tr>  ");
					noUserMsg.append(" <tr>   ");
					noUserMsg
							.append(" <td align=\"left\" style=\"font-size: 18px; font-style: normal; font-weight: normal; color: #000; line-height: 1.5em; text-align:justify; padding:10px 20px 20px 20px; font-family:monospace;\"><p>Senhor(a) Cliente.</p> ");

					if (codOpe != null
							&& (codOpe.equals(3L) || codOpe.equals(2L) || codOpe
									.equals(14L))) {
						noUserMsg
								.append(" <p>   Comunicamos que o colaborador "
										+ nomfun);
						noUserMsg.append(", cargo " + sisCar
								+ ", que atua no horário e escala "
								+ nomeEscala + ", neste posto de serviço, ");
					}

					if (codOpe != null
							&& (codOpe.equals(7L) || codOpe.equals(12L) || codOpe
									.equals(13L))) {
						noUserMsg
								.append(" <p> Comunicamos cobertura operacional realizada pelo colaborador  "
										+ nomfun);
						noUserMsg.append(" , cargo " + sisCar
								+ ", neste posto de serviço " + postoServico);
						noUserMsg.append(", no período de " + dataInicial
								+ "H até " + dataFinal + "H.<br/><br/>");
					}

					if (codOpe != null && codOpe.equals(3L)) {
						noUserMsg
								.append(" estará em gozo de férias no período de "
										+ dataInicial
										+ " até "
										+ dataFinal
										+ ", e retornará as suas ");
						noUserMsg
								.append(" atividades no dia "
										+ retorno
										+ ". Durante esse período está programado para substituí-lo");
						noUserMsg.append(" " + nomfunCod
								+ ", devidamente capacitado ");
						noUserMsg.append(" para a função.</p> ");
					}

					if (codOpe != null
							&& (codOpe.equals(2L) || codOpe.equals(14L))) {

						noUserMsg
								.append(" será substituído temporariamente no período de "
										+ dataInicial.substring(0, 10)
										+ " até "
										+ dataFinal.substring(0, 10)
										+ ". ");
						noUserMsg
								.append(" Durante esse período está programado para substituí-lo,");
						noUserMsg
								.append(" "
										+ nomfunCod
										+ ", devidamente capacitado para a função.<br/><br/>");
					}

					if (codOpe != null
							&& (codOpe.equals(2L) || codOpe.equals(7L)
									|| codOpe.equals(12L) || codOpe.equals(13L) || codOpe
										.equals(14L))) {
						noUserMsg.append(" Motivo da cobertura: <strong>"
								+ nomeMotivo + "</strong>.</p>");
					}

					if (codOpe != null && codOpe.equals(15L)) {
						noUserMsg
								.append(" <p>   Comunicamos que o colaborador "
										+ nomfun);
						noUserMsg
								.append(", cargo "
										+ sisCar
										+ ", que atua neste posto de serviço, terá sua escala invertida com o ");
						noUserMsg.append("colaborador " + nomfunCod + ".<p>");
					}

					String caminhoFotoStr = "custom/jsp/orsegups/utils/imageFromDB.jsp?numEmp="
							+ numEmpCob
							+ "&tipCol="
							+ tipColCob
							+ "&numCad="
							+ numCadCob + "&regra=Sim";
					String caminhoFoto = "custom/jsp/orsegups/utils/imageFromDB.jsp?numEmp="
							+ numEmp
							+ "&tipCol="
							+ tipCol
							+ "&numCad="
							+ numCad + "&regra=Sim";

					if (codOpe != null
							&& (codOpe.equals(3L) || codOpe.equals(2L)
									|| codOpe.equals(14L) || codOpe.equals(15L))) {
						noUserMsg.append(" <h2>COLABORADOR EFETIVO</h2> ");
						noUserMsg
								.append(" <table width=\"100%\" border=\"0\"> ");
						noUserMsg.append(" <tbody> ");
						noUserMsg.append(" <tr> ");

						noUserMsg
								.append(" <td width=\"26%\" rowspan=\"2\"><img src=\"http://intranet.orsegups.com.br/fusion/"
										+ caminhoFotoStr
										+ "\" alt=\"\" width=\"169\" height=\"226\" alt=\"\"/></td> ");

						noUserMsg.append(" </tr> ");
						noUserMsg.append(" <tr> ");
						noUserMsg.append(" <td valign=\"top\">");
						noUserMsg.append(" <table  border=\"0\"> ");
						noUserMsg.append(" <tbody> ");
						noUserMsg.append(" <tr> ");
						noUserMsg
								.append(" <td width=\"31%\">Colaborador:</td> ");
						noUserMsg.append(" <td width=\"4%\">&nbsp;</td> ");
						noUserMsg.append(" <td width=\"65%\">" + nomfun
								+ "</td> ");
						noUserMsg.append(" </tr> ");
						noUserMsg.append(" <tr> ");
						noUserMsg.append(" <td>Cargo:</td> ");
						noUserMsg.append(" <td>&nbsp;</td> ");
						noUserMsg.append(" <td>" + sisCar + "</td> ");
						noUserMsg.append(" </tr> ");
						noUserMsg.append(" <tr> ");
						noUserMsg.append(" <td>Horário:</td> ");
						noUserMsg.append(" <td>&nbsp;</td> ");
						noUserMsg.append(" <td>" + nomeEscala + "</td> ");
						noUserMsg.append(" </tr> ");
						noUserMsg.append(" <tr> ");
						noUserMsg.append(" <td>Período:</td> ");
						noUserMsg.append(" <td>&nbsp;</td> ");
						noUserMsg.append(" <td>" + dataInicial + " - "
								+ dataFinal + "</td> ");
						noUserMsg.append(" </tr> ");
						if (codOpe != null && codOpe.equals(3L)) {
							noUserMsg.append(" <tr> ");
							noUserMsg.append(" <td>Retorno:</td> ");
							noUserMsg.append(" <td>&nbsp;</td> ");
							noUserMsg.append(" <td>" + retorno + "</td> ");
							noUserMsg.append(" </tr> ");
						}
						noUserMsg.append(" </td></tr> ");
						noUserMsg.append(" </tbody> ");
						noUserMsg.append(" </table> ");
						noUserMsg.append(" </tbody> ");
						noUserMsg.append(" </table> ");
						noUserMsg.append(" <h2>COLABORADOR SUBSTITUTO<br> ");
						noUserMsg.append(" </h2> ");
						noUserMsg
								.append(" <table width=\"100%\" border=\"0\"> ");
						noUserMsg.append(" <tbody> ");
						noUserMsg.append(" <tr> ");

						noUserMsg
								.append(" <td width=\"26%\" rowspan=\"2\"><img src=\"http://intranet.orsegups.com.br/fusion/"
										+ caminhoFoto
										+ "\" alt=\"\" width=\"169\" height=\"226\" alt=\"\"/></td> ");

						noUserMsg.append(" </tr> ");
						noUserMsg.append(" <tr> ");
						noUserMsg.append(" <td valign=\"top\">");
						noUserMsg
								.append(" <table width=\"100%\" border=\"0\"> ");
						noUserMsg.append(" <tbody> ");
						noUserMsg.append(" <tr> ");

						noUserMsg
								.append(" <td width=\"21%\">Colaborador:</td> ");
						noUserMsg.append(" <td width=\"53%\">" + nomfunCod
								+ "</td> ");
						noUserMsg.append(" </tr> ");
						noUserMsg.append(" <tr> ");
						noUserMsg.append(" <td valign=\"top\">CPF:</td> ");
						noUserMsg.append(" <td valign=\"top\">" + cpf
								+ "</td> ");
						noUserMsg.append(" </tr> ");
						noUserMsg.append(" <tr> ");
						noUserMsg.append(" <td valign=\"top\">RG:</td> ");
						noUserMsg
								.append(" <td valign=\"top\">" + rg + "</td> ");
						noUserMsg.append(" </tr> ");
						noUserMsg.append(" <tr> ");
						noUserMsg.append(" <td valign=\"top\">Cargo:</td> ");
						noUserMsg.append(" <td valign=\"top\">" + nomCar
								+ "</td> ");
						noUserMsg.append(" </tr> ");
						noUserMsg.append(" </td></tr> ");
						noUserMsg.append(" </tbody> ");
						noUserMsg.append(" </table> ");
						noUserMsg.append(" </tbody> ");
						noUserMsg.append(" </table> ");
						noUserMsg
								.append(" <p style=\"font-size: 7px;\"> Algumas fotos apresentadas foram fornecidas pelos colaboradores no ato da admissão e podem estar fora do padrão de qualidade desejado, como falta de uniforme, desatualizada, baixa iluminação, enquadramento, etc. Estamos realizando atualização cadastral de todos os colaboradores visando adequação.</p>");
						noUserMsg.append(" <hr /> ");
						noUserMsg.append(" <p>Cordialmente,</p> ");
						noUserMsg.append(" <p>Orsegups Participações S/A</p> ");
						noUserMsg.append(" <p>Regional: " + regional + "</p> ");
						noUserMsg
								.append(" <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/rodape-email-ferias.jpg\" width=\"100%\" height=\"211\" alt=\"\"/></td> ");
						noUserMsg.append(" </tr> ");
						noUserMsg.append(" </table> ");
						noUserMsg.append(" </td></tr> ");
						noUserMsg.append(" </tbody> ");
						noUserMsg.append(" </table></td> ");
						noUserMsg.append(" </tr> ");
						noUserMsg.append(" </tbody> ");
						noUserMsg.append(" </table> ");
						noUserMsg.append(" </body> ");
						noUserMsg.append(" </html> ");
					}

					if (codOpe != null
							&& (codOpe.equals(7L) || codOpe.equals(12L) || codOpe
									.equals(13L))
							&& codMotivo != null
							&& (!codMotivo.equals(1L) && !codMotivo.equals(4L)
									&& !codMotivo.equals(6L)
									&& !codMotivo.equals(7L) && !codMotivo
										.equals(9L))) {
						noUserMsg.append(" <h2>COLABORADOR</h2> ");
						noUserMsg
								.append(" <table width=\"100%\" border=\"0\"> ");
						noUserMsg.append(" <tbody> ");
						noUserMsg.append(" <tr> ");

						noUserMsg
								.append(" <td width=\"26%\" rowspan=\"2\"><img src=\"http://intranet.orsegups.com.br/fusion/"
										+ caminhoFoto
										+ "\" width=\"169\" height=\"226\" alt=\"\"/></td> ");

						noUserMsg.append(" </tr> ");
						noUserMsg.append(" <tr> ");
						noUserMsg.append(" <td valign=\"top\">");
						noUserMsg.append(" <table  border=\"0\"> ");
						noUserMsg.append(" <tbody> ");
						noUserMsg.append(" <tr> ");
						noUserMsg
								.append(" <td width=\"31%\">Colaborador:</td> ");
						noUserMsg.append(" <td width=\"4%\">&nbsp;</td> ");
						noUserMsg.append(" <td width=\"65%\">" + nomfun
								+ "</td> ");
						noUserMsg.append(" </tr> ");
						noUserMsg.append(" <tr> ");
						noUserMsg.append(" <td>Cargo:</td> ");
						noUserMsg.append(" <td>&nbsp;</td> ");
						noUserMsg.append(" <td>" + sisCar + "</td> ");
						noUserMsg.append(" </tr> ");
						noUserMsg.append(" <tr> ");
						noUserMsg.append(" <td><b>Período</b></td> ");
						noUserMsg.append(" <td>&nbsp;</td> ");
						noUserMsg.append(" <td>&nbsp;</td> ");
						noUserMsg.append(" </tr> ");
						noUserMsg.append(" <tr> ");
						noUserMsg.append(" <td>Início dia:</td> ");
						noUserMsg.append(" <td>&nbsp;</td> ");
						noUserMsg.append(" <td>" + dataInicial.substring(0, 10)
								+ ", às " + dataInicial.substring(11, 16)
								+ " hora(s).</td> ");
						noUserMsg.append(" </tr> ");
						noUserMsg.append(" <tr> ");
						noUserMsg.append(" <td>Final dia:</td> ");
						noUserMsg.append(" <td>&nbsp;</td> ");
						noUserMsg.append(" <td>" + dataFinal.substring(0, 10)
								+ ", às " + dataFinal.substring(11, 16)
								+ " hora(s).</td> ");
						noUserMsg.append(" </tr> ");
						noUserMsg.append(" </td></tr> ");
						noUserMsg.append(" </tbody> ");
						noUserMsg.append(" </table> ");
						noUserMsg.append(" </tbody> ");
						noUserMsg.append(" </table> ");
						noUserMsg
								.append(" <p style=\"font-size: 7px;\"> Algumas fotos apresentadas foram fornecidas pelos colaboradores no ato da admissão e podem estar fora do padrão de qualidade desejado, como falta de uniforme, desatualizada, baixa iluminação, enquadramento, etc. Estamos realizando atualização cadastral de todos os colaboradores visando adequação.</p>");
						noUserMsg.append(" <hr /> ");
						noUserMsg.append(" <p>Cordialmente,</p> ");
						noUserMsg.append(" <p>Orsegups Participações S/A</p> ");
						noUserMsg.append(" <p>Regional: " + regional + "</p> ");
						noUserMsg
								.append(" <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/rodape-email-ferias.jpg\" width=\"800\" height=\"211\" alt=\"\"/></td> ");
						noUserMsg.append(" </tr> ");
						noUserMsg.append(" </table> ");
						noUserMsg.append(" </td></tr> ");
						noUserMsg.append(" </tbody> ");
						noUserMsg.append(" </table></td> ");
						noUserMsg.append(" </tr> ");
						noUserMsg.append(" </tbody> ");
						noUserMsg.append(" </table> ");
						noUserMsg.append(" </body> ");
						noUserMsg.append(" </html> ");
					} else if (codOpe != null
							&& (codOpe.equals(7L) || codOpe.equals(12L) || codOpe
									.equals(13L))
							&& codMotivo != null
							&& (codMotivo.equals(1L) || codMotivo.equals(4L)
									|| codMotivo.equals(6L)
									|| codMotivo.equals(7L) || codMotivo
										.equals(9L))) {
						noUserMsg.append(" <h2>COLABORADOR AUSENTE</h2> ");
						noUserMsg
								.append(" <table width=\"100%\" border=\"0\"> ");
						noUserMsg.append(" <tbody> ");
						noUserMsg.append(" <tr> ");

						noUserMsg
								.append(" <td width=\"26%\" rowspan=\"2\"><img src=\"http://intranet.orsegups.com.br/fusion/"
										+ caminhoFotoStr
										+ "\" alt=\"\" width=\"169\" height=\"226\" alt=\"\"/></td> ");

						noUserMsg.append(" </tr> ");
						noUserMsg.append(" <tr> ");
						noUserMsg.append(" <td valign=\"top\">");
						noUserMsg.append(" <table  border=\"0\"> ");
						noUserMsg.append(" <tbody> ");
						noUserMsg.append(" <tr> ");
						noUserMsg
								.append(" <td width=\"31%\">Colaborador:</td> ");
						noUserMsg.append(" <td width=\"4%\">&nbsp;</td> ");
						noUserMsg.append(" <td width=\"65%\">" + nomfunCod
								+ "</td> ");
						noUserMsg.append(" </tr> ");
						noUserMsg.append(" <tr> ");
						noUserMsg.append(" <td>Cargo:</td> ");
						noUserMsg.append(" <td>&nbsp;</td> ");
						noUserMsg.append(" <td>" + nomCar + "</td> ");
						noUserMsg.append(" </tr> ");
						noUserMsg.append(" </td></tr> ");
						noUserMsg.append(" </tbody> ");
						noUserMsg.append(" </table> ");
						noUserMsg.append(" </tbody> ");
						noUserMsg.append(" </table> ");
						noUserMsg.append(" <h2>COLABORADOR SUBSTITUTO<br> ");
						noUserMsg.append(" </h2> ");
						noUserMsg
								.append(" <table width=\"100%\" border=\"0\"> ");
						noUserMsg.append(" <tbody> ");
						noUserMsg.append(" <tr> ");

						noUserMsg
								.append(" <td width=\"26%\" rowspan=\"2\"><img src=\"http://intranet.orsegups.com.br/fusion/"
										+ caminhoFoto
										+ "\" width=\"169\" height=\"226\" alt=\"\"/></td> ");

						noUserMsg.append(" </tr> ");
						noUserMsg.append(" <tr> ");
						noUserMsg.append(" <td valign=\"top\">");
						noUserMsg.append(" <table  border=\"0\"> ");
						noUserMsg.append(" <tbody> ");
						noUserMsg.append(" <tr> ");
						noUserMsg
								.append(" <td width=\"31%\">Colaborador:</td> ");
						noUserMsg.append(" <td width=\"4%\">&nbsp;</td> ");
						noUserMsg.append(" <td width=\"65%\">" + nomfun
								+ "</td> ");
						noUserMsg.append(" </tr> ");
						noUserMsg.append(" <tr> ");
						noUserMsg.append(" <td>Cargo:</td> ");
						noUserMsg.append(" <td>&nbsp;</td> ");
						noUserMsg.append(" <td>" + sisCar + "</td> ");
						noUserMsg.append(" </tr> ");
						noUserMsg.append(" <tr> ");
						noUserMsg.append(" <td><b>Período</b></td> ");
						noUserMsg.append(" <td>&nbsp;</td> ");
						noUserMsg.append(" <td>&nbsp;</td> ");
						// noUserMsg.append(" <td>Início dia: " +
						// dataInicial.substring(0, 10) + ", às " +
						// dataInicial.substring(11, 16) + " hora(s).<br/> ");
						// noUserMsg.append(" Final dia:" +
						// dataFinal.substring(0, 10) + ", às " +
						// dataFinal.substring(11, 16)+ " hora(s).</td> ");
						noUserMsg.append(" </tr> ");
						noUserMsg.append(" <tr> ");
						noUserMsg.append(" <td>Início dia:</td> ");
						noUserMsg.append(" <td>&nbsp;</td> ");
						noUserMsg.append(" <td>" + dataInicial.substring(0, 10)
								+ ", às " + dataInicial.substring(11, 16)
								+ " hora(s).</td> ");
						noUserMsg.append(" </tr> ");
						noUserMsg.append(" <tr> ");
						noUserMsg.append(" <td>Final dia:</td> ");
						noUserMsg.append(" <td>&nbsp;</td> ");
						noUserMsg.append(" <td>" + dataFinal.substring(0, 10)
								+ ", às " + dataFinal.substring(11, 16)
								+ " hora(s).</td> ");
						noUserMsg.append(" </tr> ");
						noUserMsg.append(" </td></tr> ");
						noUserMsg.append(" </tbody> ");
						noUserMsg.append(" </table> ");
						noUserMsg.append(" </tbody> ");
						noUserMsg.append(" </table> ");
						noUserMsg
								.append(" <p style=\"font-size: 7px;\"> Algumas fotos apresentadas foram fornecidas pelos colaboradores no ato da admissão e podem estar fora do padrão de qualidade desejado, como falta de uniforme, desatualizada, baixa iluminação, enquadramento, etc. Estamos realizando atualização cadastral de todos os colaboradores visando adequação.</p>");
						noUserMsg.append(" <hr /> ");
						noUserMsg.append(" <p>Cordialmente,</p> ");
						noUserMsg.append(" <p>Orsegups Participações S/A</p> ");
						noUserMsg.append(" <p>Regional: " + regional + "</p> ");
						noUserMsg
								.append(" <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/rodape-email-ferias.jpg\" width=\"800\" height=\"211\" alt=\"\"/></td> ");
						noUserMsg.append(" </tr> ");
						noUserMsg.append(" </table> ");
						noUserMsg.append(" </td></tr> ");
						noUserMsg.append(" </tbody> ");
						noUserMsg.append(" </table></td> ");
						noUserMsg.append(" </tr> ");
						noUserMsg.append(" </tbody> ");
						noUserMsg.append(" </table> ");
						noUserMsg.append(" </body> ");
						noUserMsg.append(" </html> ");
					}
					Thread.sleep(100);
					noUserEmail.setHtmlMsg(noUserMsg.toString());
					noUserEmail.setContent(noUserMsg.toString(), "text/html");
					mailClone.applyConfig(noUserEmail);

					noUserEmail.send();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("##### ERRO ao enviar comunicado de férias para cliente - Data: "
					+ e.getMessage().toString()
					+ " - Data: "
					+ NeoDateUtils.safeDateFormat(new GregorianCalendar(),
							"dd/MM/yyyy"));

		}

	}

	private String getEmailGerente(String regional) {
		String emailGerente = "presenca@orsegups.com.br";
		
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		
		try {
			conn = PersistEngine.getConnection("SAPIENS");
			
			sql.append("SELECT USU.CODUSU, USU.INTNET, REG.USU_NOMREG ");
			sql.append("FROM USU_T200REG REG ");
			sql.append("INNER JOIN E099USU USU ON REG.USU_GERREG = USU.CODUSU ");
			sql.append("WHERE USU.CODEMP = 1 AND REG.USU_NOMREG LIKE '%" + regional +  "%' COLLATE SQL_LATIN1_GENERAL_CP1_CI_AI ");

			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();
			
			if(rs.next()){
				emailGerente = rs.getString("INTNET");
			} 		
		} catch (Exception e) {
			e.printStackTrace();
			return emailGerente;
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
				
		return emailGerente;
	}

	public boolean validEmail(String email) {
		Pattern p = Pattern
				.compile("^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,7}$");
		Matcher m = p.matcher(email);
		if (m.find()) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validExcessaoEmail(String email) {

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		StringBuilder sql = new StringBuilder();

		sql.append("SELECT 1 FROM D_CadastroExcecoesEmailsPresenca WITH(NOLOCK) WHERE email=?");

		try {
			conn = PersistEngine.getConnection("");
			pstm = conn.prepareStatement(sql.toString());

			pstm.setString(1, email);

			rs = pstm.executeQuery();

			if (rs.next()) {
				return false;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

		return true;

	}

	private Boolean getFoto(String numEmp, String tipCol, String numCad) {
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		Boolean flag = Boolean.FALSE;
		try {
			String nomeFonteDados = "VETORH";
			conn = PersistEngine.getConnection(nomeFonteDados);

			StringBuffer sqlFoto = new StringBuffer();
			sqlFoto.append(" SELECT FotEmp FROM R034FOT WITH(NOLOCK) WHERE NumEmp = "
					+ numEmp
					+ " AND TipCol = "
					+ tipCol
					+ " AND NumCad = "
					+ numCad);
			st = conn.prepareStatement(sqlFoto.toString());
			rs = st.executeQuery();

			if (rs.next()) {
				flag = Boolean.TRUE;
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, st, rs);
			return flag;
		}
	}

	private String getRg(String numEmp, String tipCol, String numCad) {
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		String retorno = "";
		try {
			String nomeFonteDados = "VETORH";
			conn = PersistEngine.getConnection(nomeFonteDados);

			StringBuffer sqlFoto = new StringBuffer();
			sqlFoto.append(" SELECT NUMCID FROM R034CPL WITH(NOLOCK) WHERE NumEmp = "
					+ numEmp
					+ " AND TipCol = "
					+ tipCol
					+ " AND NumCad = "
					+ numCad);
			st = conn.prepareStatement(sqlFoto.toString());
			rs = st.executeQuery();

			if (rs.next()) {
				if (rs.getString("NUMCID") != null
						&& !rs.getString("NUMCID").isEmpty())
					retorno = rs.getString("NUMCID");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, st, rs);
			return retorno;
		}
	}

	public void encaminharEmailIntegracao(StringBuilder corpo)
			throws EmailException {
		MailSettings settings = (MailSettings) PersistEngine
				.reload(FusionRuntime.getInstance().getSettings()
						.getMailSettings());
		StringBuilder msgCorpo = new StringBuilder();
		msgCorpo.append(" <!DOCTYPE html>");
		msgCorpo.append(" <html xmlns='http://www.w3.org/1999/xhtml'>");
		msgCorpo.append(" <html> ");
		msgCorpo.append(" <head> ");
		msgCorpo.append(" <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" /> ");
		msgCorpo.append(" <meta charset=\"utf-8\"> ");
		msgCorpo.append(" <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"> ");
		msgCorpo.append(" </head>");
		msgCorpo.append(corpo);
		msgCorpo.append(" </html>");

		MailSettings mailClone = new MailSettings();
		mailClone.setMinutesInterval(settings.getMinutesInterval());
		mailClone.setFromEMail(settings.getFromEMail());
		mailClone.setFromName(settings.getFromName());
		mailClone.setRenderServer(settings.getRenderServer());
		mailClone.setSecurityFromEmail(settings.getSecurityFromEmail());
		mailClone.setSecurityCopyEmail(settings.getSecurityCopyEmail());
		mailClone.setSmtpSettings(settings.getSmtpSettings());
		mailClone.setPort(settings.getPort());
		mailClone.setSmtpServer(settings.getSmtpServer());
		mailClone.setEnabled(settings.isEnabled());

		mailClone.setFromEMail("fusion@orsegups.com.br");
		mailClone.setFromName("Orsegups Participações S.A.");
		HtmlEmail noUserEmail = new HtmlEmail();
		StringBuilder noUserMsg = new StringBuilder();
		noUserEmail.setCharset("ISO-8859-1");
		noUserEmail.addBcc("jorge.filho@orsegups.com.br");
		noUserEmail.addBcc("danilo.silva@orsegups.com.br");
		noUserEmail.setSubject("Erro ao registrar o R030 na winker");
		noUserMsg.append(corpo.toString());
		noUserEmail.setHtmlMsg(noUserMsg.toString());
		noUserEmail.setContent(noUserMsg.toString(), "text/html");
		mailClone.applyConfig(noUserEmail);

		noUserEmail.send();
	}

	public boolean validarNumLoc(Long numLoc) {
		StringBuilder sql = new StringBuilder();
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		sql.append(" select distinct(orn.usu_cgccpf), orn.usu_codclisap, case when usu_codclisap = 26358 then 'ORSEGUPS SEDE' else cli.apecli end as apecli, ");
		sql.append(" 	cli.EndCli + ', '+ cli.NenCli as usu_endloc, cli.CepCli as usu_endcep,  cli.BaiCli as bairro, cli.CidCli as nomcid, cli.SigUfs as estcid  ");
		sql.append(" ,orn.numloc, cli.usu_nomsin, cli.usu_cpfsin, cli.usu_emasin, cli.usu_fcnsin    ");
		sql.append(" from R016ORN orn  ");
		sql.append(" inner join Sapiens.dbo.e085cli cli on orn.usu_codclisap = cli.codcli  ");
		sql.append("		 inner join usu_t038cvs cvs on cvs.usu_sitcvs = 'S' and cvs.usu_numloc = orn.numloc and usu_seqalt = (select max(x.usu_seqalt) from usu_t038cvs x where x.usu_numloc = orn.numloc) and orn.usu_sitati = 'S' ");
		sql.append(" left join r074cid cid on cid.CodCid = orn.usu_codcid ");
		sql.append("		 where (exists (select 1 from Sapiens.dbo.usu_t160ctr ctr where ctr.usu_codcli = orn.usu_codclisap and ctr.usu_numctr = orn.usu_numctr and ctr.usu_codemp = orn.usu_numemp and  usu_sitctr = 'A' and usu_intwin = 'S') ");
		sql.append("		 and usu_codclisap not in (26358) or (usu_codclisap = 26358  and orn.numloc in (61396, 26151,25345,46498,46503,46495,46501,46500))) and orn.numloc = ? ");
		try {
			conn = OrsegupsUtils.getConnection("VETORH");
			pstm = conn.prepareStatement(sql.toString());
			pstm.setLong(1, numLoc);
			rs = pstm.executeQuery();
			if (rs.next()) {
				return true;
			}
			return false;
		} catch (Exception e) {
			System.out
					.println("Erro no metodo de validar local para encaminhar há winker");
			e.printStackTrace();
			return false;
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
	}
}
