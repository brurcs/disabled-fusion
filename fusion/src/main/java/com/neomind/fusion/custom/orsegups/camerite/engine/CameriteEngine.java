package com.neomind.fusion.custom.orsegups.camerite.engine;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;

import javax.imageio.ImageIO;

import com.google.gson.Gson;
import com.neomind.fusion.custom.orsegups.camerite.bean.CameriteGrupo;
import com.neomind.fusion.custom.orsegups.camerite.bean.CameriteResponse;

public class CameriteEngine {

    public static CameriteGrupo[] getInfoGrupo(String conta, String particao, int empresa) {

	StringBuilder retorno = new StringBuilder();

	try {

	    URL url = new URL("https://orsegups.camerite.com/webapi/v1/groups/sigma?fieldsList[0][idCentral]=" + conta + "&fieldsList[0][empresa]=" + empresa + "&fieldsList[0][particao]=" + particao + "&token=2fc4a5d7-ba85-4b12-bad2-e089d2402995");
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setRequestMethod("GET");
	    conn.setConnectTimeout(5000);
	    conn.setReadTimeout(10000);
	    // conn.setRequestProperty("x-auth-token",
	    // "682ee2548d4a73d8fadf72f32c52df790eb72aff");

	    if (conn.getResponseCode() != 200) {
		// inserirLogErro(historico, conn.getResponseMessage());
		return null;
	    }

	    BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream()), "UTF-8"));

	    String output;

	    while ((output = br.readLine()) != null) {
		retorno.append(output + "\n");
	    }

	    conn.disconnect();

	} catch (MalformedURLException e) {
	    // inserirLogErro(historico, e.getMessage());
	    e.printStackTrace();

	} catch (IOException e) {
	    // inserirLogErro(historico, e.getMessage());
	    e.printStackTrace();

	}

	if (!retorno.toString().isEmpty()) {
	    Gson gson = new Gson();

	    CameriteGrupo[] result = null;
	    try {
		result = (CameriteGrupo[]) gson.fromJson(retorno.toString(), CameriteGrupo[].class);
	    } catch (Exception e) {
		e.printStackTrace();
	    }

	    return result;
	}

	return null;
    }

    public static CameriteResponse getQuickRecord(int id, long time, boolean onlyShot) {

	StringBuilder retorno = new StringBuilder();

	try {

	    URL url = new URL("https://orsegups.camerite.com/webapi/v1/createQuickRecord/" + id + "?time=" + time + "&onlyshot=" + onlyShot + "&token=2fc4a5d7-ba85-4b12-bad2-e089d2402995");
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setRequestMethod("GET");
	    conn.setConnectTimeout(5000);
	    conn.setReadTimeout(10000);
	    // conn.setRequestProperty("x-auth-token",
	    // "682ee2548d4a73d8fadf72f32c52df790eb72aff");

	    if (conn.getResponseCode() != 200) {
		// inserirLogErro(historico, conn.getResponseMessage());
		return null;
	    }

	    BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream()), "UTF-8"));

	    String output;

	    while ((output = br.readLine()) != null) {
		retorno.append(output + "\n");
	    }

	    conn.disconnect();

	} catch (MalformedURLException e) {
	    // inserirLogErro(historico, e.getMessage());
	    e.printStackTrace();

	} catch (IOException e) {
	    // inserirLogErro(historico, e.getMessage());
	    e.printStackTrace();

	}

	if (!retorno.toString().isEmpty()) {
	    Gson gson = new Gson();

	    CameriteResponse result = null;
	    try {
		result = (CameriteResponse) gson.fromJson(retorno.toString(), CameriteResponse.class);
	    } catch (Exception e) {
		e.printStackTrace();
	    }

	    return result;
	}

	return null;
    }

    public static File getFileFromURL(String urlStr) throws Exception {

	ByteArrayOutputStream output = null;
	URL url = null;
	URLConnection uc = null;
	InputStream is = null;
	BufferedImage image = null;
	File outputfile = null;

	int count = 0;

	while (count < 5 && outputfile == null) {
	    try {
		output = new ByteArrayOutputStream();
		// String userpass = "sigma:sigma";

		url = new URL(urlStr);
		
		uc = url.openConnection();
		uc.setConnectTimeout(5000);
		uc.setReadTimeout(10000);

		// String basicAuth = "Basic " + new String(new
		// Base64().encode(userpass.getBytes()));
		// uc.setRequestProperty("Authorization", basicAuth);
		is = uc.getInputStream();

		outputfile = new File(Calendar.getInstance().getTimeInMillis() + ".jpg");

		image = ImageIO.read(is);

		if (image != null) {
		    ImageIO.write(image, "jpg", outputfile);
		}else{
		    return null;
		}

	    } catch (Exception e) {
		e.printStackTrace();
		throw new Exception("[CameriteEngine][getFileFromURL] e:" + e.getMessage());
	    } finally {
		try {
		    if (is != null)
			is.close();
		    if (output != null) {
			output.flush();
			output.close();
		    }
		} catch (IOException e) {
		    e.printStackTrace();
		}

	    }
	}

	return outputfile;

    }

}
