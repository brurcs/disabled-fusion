package com.neomind.fusion.custom.orsegups.converter;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.ProcessState;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class HistoricoRTOciosoConverter extends StringConverter
{

	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin)
	{
		StringBuilder textoTable = new StringBuilder();
		try
		{
			List<NeoObject> listaProcessos = null;
			Long idPai = field.getForm().getObjectId();
			NeoObject tarefa = PersistEngine.getNeoObject(idPai);
			EntityWrapper wrapper = new EntityWrapper(tarefa);
			Long cpf = (Long) wrapper.findValue("cpf");

			if (cpf != null)
			{
				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				QLEqualsFilter savedFilter = new QLEqualsFilter("wfprocess.saved", true);
				QLGroupFilter statusGroupFilter = new QLGroupFilter("OR");
				QLOpFilter eliminateItSelf = new QLOpFilter("wfprocess.neoId", "<>", (Long) wrapper.findValue("wfprocess.neoId"));
				QLEqualsFilter runningFilter = new QLEqualsFilter("wfprocess.processState", ProcessState.running.ordinal());
				QLEqualsFilter finishedFilter = new QLEqualsFilter("wfprocess.processState", ProcessState.finished.ordinal());
				statusGroupFilter.addFilter(runningFilter);
				statusGroupFilter.addFilter(finishedFilter);
				groupFilter.addFilter(savedFilter);
				groupFilter.addFilter(statusGroupFilter);
				groupFilter.addFilter(eliminateItSelf);

				QLEqualsFilter cpfFilter = new QLEqualsFilter("cpf", cpf);
				groupFilter.addFilter(cpfFilter);

				listaProcessos = PersistEngine.getObjects(AdapterUtils.getEntityClass("RTOcioso"), groupFilter,-1,-1,"wfprocess.code");

			}

			if (listaProcessos != null && !listaProcessos.isEmpty())
			{

				textoTable.append("		<table  class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">");
				textoTable.append("			<tr >");
				textoTable.append("				<th style=\"cursor: auto; white-space: normal\">Data da Ociosidade</th>");
				textoTable.append("				<th style=\"cursor: auto; white-space: normal\">Informações</th>");
				textoTable.append("				<th style=\"cursor: auto; white-space: normal\">Código</th>");
				textoTable.append("			</tr>");
				textoTable.append("			<tbody>");
				
				for (NeoObject obj : listaProcessos)
				{
					EntityWrapper wrapperObj = new EntityWrapper(obj);
					String code = (String) wrapperObj.findValue("wfprocess.code");
					GregorianCalendar dataInicial = (GregorianCalendar) wrapperObj.findValue("wfprocess.startDate");
					GregorianCalendar dtIni = (GregorianCalendar) dataInicial.clone();
					dtIni.add(GregorianCalendar.DATE, -1);
					String dataInicialStr = NeoUtils.safeDateFormat(dtIni, "dd/MM/yyyy");
					System.out.println(dataInicialStr);
					List<NeoObject> registroAtividades = (List<NeoObject>) wrapperObj.findValue("registroAtividades");
					
					textoTable.append("		<tr>");
					textoTable.append("			<td style=\"cursor: auto; white-space: normal\">" + dataInicialStr +"</td>");
					textoTable.append("			<td style=\"cursor: auto; white-space: normal\">" );

					if (NeoUtils.safeIsNotNull(registroAtividades) && !registroAtividades.isEmpty())
					{
						textoTable.append("		<table  class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">");
						textoTable.append("			<tr >");
						textoTable.append("				<th style=\"cursor: auto; white-space: normal\">Responsável</th>");
						textoTable.append("				<th style=\"cursor: auto; white-space: normal\">Descrição</th>");
						textoTable.append("				<th style=\"cursor: auto; white-space: normal\">Data da Ação</th>");
						textoTable.append("			</tr>");
						textoTable.append("			<tbody>");
						for (NeoObject objItemOrcamento : registroAtividades)
						{
							EntityWrapper registroWrapper = new EntityWrapper(objItemOrcamento);
							NeoUser responsavelNeoUser = (NeoUser) registroWrapper.findValue("responsavel");
							String responsavel = responsavelNeoUser.getFullName();
							String atividadeAnterior = (String) registroWrapper.findValue("atividadeAnterior");
							String descricao = (String) registroWrapper.findValue("descricao");
							GregorianCalendar dataFinal = (GregorianCalendar) registroWrapper.findValue("dataFinal");

							String dataAcao = NeoUtils.safeDateFormat(dataFinal, "dd/MM/yyyy HH:mm:ss");

							if ((atividadeAnterior != null && atividadeAnterior.isEmpty()) || atividadeAnterior == null)
								atividadeAnterior = "";
							if ((descricao != null && descricao.isEmpty()) || descricao == null)
								descricao = "";

							textoTable.append("		<tr>");
							textoTable.append("			<td style=\"cursor: auto; white-space: normal\">" + responsavel + "</td>");
							textoTable.append("			<td style=\"cursor: auto; white-space: normal\">" + descricao + "</td>");
							textoTable.append("			<td style=\"cursor: auto; white-space: normal\">" + dataAcao + "</td>");
							textoTable.append("		</tr>");
						}
						textoTable.append("		</tbody>");
						textoTable.append("	</table>");
					}
					textoTable.append("	</td>");
					textoTable.append("			<td style=\"cursor: auto; white-space: normal\">" + code + "</td>");
					textoTable.append("		</tr>");
					
				}
				textoTable.append("		</tbody>");
				textoTable.append("	</table>");
				textoTable.append("	<br/>");

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			return textoTable.toString();
		}

	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return this.getHTMLInput(field, origin);
	}
}