package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class AbreTarefaSimplesEstoqueComDivergencia implements CustomJobAdapter {
    
    private static final Log log = LogFactory.getLog(AbreTarefaSimplesEstoqueComDivergencia.class);

    @Override
    public void execute(CustomJobContext ctx) {
	
	Long key = GregorianCalendar.getInstance().getTimeInMillis(); 
	
	// Estabelecendo conexão com a base do Sapiens
	Connection conn = PersistEngine.getConnection("SAPIENS");
	PreparedStatement pstm = null;
	ResultSet rs = null;
	StringBuilder sql = new StringBuilder();
	
	// Definindo o solicitante e validando se o mesmo está ativo
	String solicitante = "";
	NeoUser usuSolicitante = (NeoUser) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoUser"), new QLEqualsFilter("code","giliardi"));
	
	if(usuSolicitante != null && usuSolicitante.isActive()){ 
	    solicitante = usuSolicitante.getCode();
	}
	
	// Definindo o executor e validando se o mesmo está ativo	
	String executor = "";
	NeoUser usuExecutor = (NeoUser) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoUser"), new QLEqualsFilter("code","jairo"));
	
	if(usuExecutor != null && usuExecutor.isActive()){
	   executor = usuExecutor.getCode();
	}
	
	
	// Definindo o prazo da tarefa
	GregorianCalendar prazo = new GregorianCalendar();
	prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
	prazo.set(Calendar.HOUR, 23);
	prazo.set(Calendar.MINUTE, 59);
	prazo.set(Calendar.SECOND,59);
	
	String titulo = "Estoques - Saldo com Divergência";
	String descricao = "Favor verificar os saldos abaixo e providenciar a transferência para o estoque princial </br>";
	String tarefa = "";
	
	
	try{
	    	sql.append(" SELECT EST.CODEMP,CODPRO,CODDER,EST.CODDEP,QTDEST,SITEST "); 
        	sql.append(" FROM E210EST EST ");
        	sql.append(" INNER JOIN E205DEP DEP ON EST.codemp = DEP.CodEmp AND DEP.CodDep = EST.coddep ");
        	sql.append(" WHERE EST.qtdest > 0 AND DEP.SitDep = 'A' AND EST.CODEMP NOT IN (1,30) AND EST.CODDEP <> 'SOO01' ");
        	
        	pstm = conn.prepareStatement(sql.toString());
        	rs = pstm.executeQuery();
        	
        	descricao = descricao + "<table class=\"gridbox\" cellpadding=\"0\" cellspacing=\"0\" style=\"empty-cells: show\">";
        	descricao = descricao + "<thead>";
        	descricao = descricao + "<tr>";
        	descricao = descricao + "<th>Empresa</th>";
        	descricao = descricao + "<th>Depósito</th>";
        	descricao = descricao + "<th>Produto</th>";
        	descricao = descricao + "<th>Derivação</th>";
        	descricao = descricao + "<th>Quantidade em Estoque</th>";
        	descricao = descricao + "</tr>";
        	descricao = descricao + "<thead>";
        	descricao = descricao + "<tbody>";
        	
        	boolean temResultado = false;
        	
        	while(rs.next()){
            	  descricao = descricao + "<tr>";
            	  descricao = descricao + "<td>" + rs.getString("codemp") + "</td>";
            	  descricao = descricao + "<td>" + rs.getString("coddep") + "</td>";
            	  descricao = descricao + "<td>" + rs.getString("codpro") + "</td>";
            	  descricao = descricao + "<td>" + rs.getString("codder") + "</td>";
            	  descricao = descricao + "<td>" + rs.getString("qtdest") + "</td>";
            	  descricao = descricao + "</tr>";
            	  temResultado = true;
        	}
        	
        	descricao = descricao + "</tbody>";
        	descricao = descricao + "</table>";
        	
        	if(temResultado){
        	IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
        	tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);
        	}
	}
	catch(Exception e){
	    log.error("##### ERRO AGENDADOR DE TAREFA: Abre Tarefa Simples Estoques com Divergência");
	    System.out.println("["+key+"] ERRO AGENDADOR DE TAREFA: Abre Tarefa Simples Estoques com Divergência");
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por ["+key+"]");

	}
	finally{    
	    try
	    {
		rs.close();
		pstm.close();
		conn.close();
	    }
	    catch (SQLException e){
		e.printStackTrace();
	    }

	log.warn("##### FIM AGENDADOR DE TAREFA: Abre Tarefa Simples Estoques com Divergência - Tarefa: "+tarefa+" Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
       }	
    }
       
}
