package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;


public class RotinaAbreTarefaSimplesOrdemServico implements CustomJobAdapter {
    
    private static final Log log = LogFactory.getLog(RotinaAbreTarefaSimplesOrdemServico.class);

    public void execute(CustomJobContext arg0) {
	Long key = GregorianCalendar.getInstance().getTimeInMillis();
	try {
	    Connection conn = null;
	    PreparedStatement pstm = null;
	    ResultSet rs = null;

	    StringBuilder sql = new StringBuilder();

	    try {
		conn = PersistEngine.getConnection("TIDB");
		sql.append("SELECT * FROM TAREFA_SIMPLES_OS");
		pstm = conn.prepareStatement(sql.toString());		

		rs = pstm.executeQuery();

		while (rs.next()) {
		    
		    String solicitante = rs.getString("SOLICITANTE");
		    String executor = rs.getString("EXECUTOR");
		    String titulo = rs.getString("TITULO");
		    String descricao = rs.getString("DESCRICAO");
		    Long numeroOS = rs.getLong("NUMEROOS");
		    GregorianCalendar prazo = new GregorianCalendar();
		    prazo.setTime(rs.getTimestamp("PRAZO"));
		    Long id = rs.getLong("ID");
		    
		    IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		    String retorno = "";
		    try {
			 retorno = iniciarTarefaSimples.abrirTarefa(solicitante,executor, titulo, descricao, "1", "sim", prazo);
			 if (retorno != null && (!retorno.trim().contains("Erro"))) {
			    boolean abriuTSOS = gravarAberturaTarefaOS(numeroOS, retorno);
			    if (abriuTSOS) {
				removerRegistroTarefaSimples(id);
			    }
			 }
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		    
		}

	    } catch (Exception e) {
		e.printStackTrace();
	    } finally {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    }
	} catch (Exception e) {
	    log.error("##### ERRO  AGENDADOR DE TAREFA: Abrir Tarefa Simples Ordens de Serviço - ERRO AO GERAR TAREFA");
	    System.out.println("[" + key + "] ##### ERRO  AGENDADOR DE TAREFA: Abrir Tarefa Simples Ordens de Serviço - ERRO AO GERAR TAREFA");
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	}
	
    }    
    
    private boolean gravarAberturaTarefaOS(Long numeroOS, String tarefa){
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	try {
	    conn = PersistEngine.getConnection("");
	    
	    sql.append("UPDATE D_SIGMAOSAtrasadaTarefaSimples SET numeroTarefa = ? WHERE numeroOS = ?");
	    
	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setString(1, tarefa);
	    pstm.setLong(2, numeroOS);

	    return pstm.executeUpdate() > 0;


	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
	return false;
    }
    
    private boolean removerRegistroTarefaSimples(Long id){
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	try {
	    conn = PersistEngine.getConnection("TIDB");
	    
	    sql.append("DELETE TAREFA_SIMPLES_OS WHERE ID = ?");
	    
	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setLong(1, id);

	    return pstm.executeUpdate() > 0;


	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
	return false;
    }
}