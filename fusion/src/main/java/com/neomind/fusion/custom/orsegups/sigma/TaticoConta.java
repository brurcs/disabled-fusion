package com.neomind.fusion.custom.orsegups.sigma;

public class TaticoConta implements Comparable<TaticoConta>
{
	private Integer codRota;
	private String nomeTatico;
	private Integer qtdContas;
	private Double vlrRota;
	private String formaPagamento;
	private String regional; 
	private Integer codModalidade;
	private String descModalidade;
	
	public Integer getCodRota()
	{
		return codRota;
	}
	public void setCodRota(Integer codRota)
	{
		this.codRota = codRota;
	}
	public String getNomeTatico()
	{
		return nomeTatico;
	}
	public void setNomeTatico(String nomeTatico)
	{
		this.nomeTatico = nomeTatico;
	}
	public Integer getQtdContas()
	{
		return qtdContas;
	}
	public void setQtdContas(Integer qtdContas)
	{
		this.qtdContas = qtdContas;
	}
	public Double getVlrRota()
	{
		return vlrRota;
	}
	public void setVlrRota(Double vlrRota)
	{
		this.vlrRota = vlrRota;
	}
	public String getFormaPagamento()
	{
		return formaPagamento;
	}
	public void setFormaPagamento(String formaPagamento)
	{
		this.formaPagamento = formaPagamento;
	}
	public String getRegional()
	{
		return regional;
	}
	public void setRegional(String regional)
	{
		this.regional = regional;
	}
	
	public Integer getCodModalidade()
	{
		return codModalidade;
	}
	public void setCodModalidade(Integer codModalidade)
	{
		this.codModalidade = codModalidade;
	}
	public String getDescModalidade()
	{
		return descModalidade;
	}
	public void setDescModalidade(String descModalidade)
	{
		this.descModalidade = descModalidade;
	}
	@Override
	public int compareTo(TaticoConta o)
	{
		// TODO Auto-generated method stub
		return  -1;
	}
}
