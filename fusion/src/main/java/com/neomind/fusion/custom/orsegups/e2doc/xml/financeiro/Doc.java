package com.neomind.fusion.custom.orsegups.e2doc.xml.financeiro;

public class Doc {
    private String id;
    private int tam;
    private String des;
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public int getTam() {
        return tam;
    }
    public void setTam(int tam) {
        this.tam = tam;
    }
    public String getDes() {
        return des;
    }
    public void setDes(String des) {
        this.des = des;
    }
}
