package com.neomind.fusion.custom.orsegups.e2doc.xml.frota;

import java.util.List;


public class Pasta {
    private Indices indices;
    private List<Doc> doc;
    
    public Indices getIndices() {
        return indices;
    }
    public void setIndices(Indices indices) {
        this.indices = indices;
    }
    public List<Doc> getDoc() {
        return doc;
    }
    public void setDoc(List<Doc> doc) {
        this.doc = doc;
    }

    
    
}
