package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.camerite.bean.CameriteCamera;
import com.neomind.fusion.custom.orsegups.camerite.bean.CameriteGrupo;
import com.neomind.fusion.custom.orsegups.camerite.bean.CameriteResponse;
import com.neomind.fusion.custom.orsegups.camerite.engine.CameriteEngine;
import com.neomind.fusion.custom.orsegups.integracaoMobile.IntegracaoPortalMobile;
import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.objRatMobile.ObjRatMobile;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsEmailUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class EmailDeliveryRondaVirtualVTO implements CustomJobAdapter {
    private static final Log log = LogFactory.getLog(EmailDeliveryRondaVirtualVTO.class);
    @SuppressWarnings("unused")
    private Pattern pattern;
    private final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public static final String EVT_XVID = "XVID";

    private boolean comRegistro = false;

    @SuppressWarnings({ "unchecked", "static-access", "deprecation" })
    @Override
    public void execute(CustomJobContext arg0) {

	int adicionados = 0;

	log.warn("E-Mail XVID Inicio execuÃ§Ã£o em: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));

	Long key = GregorianCalendar.getInstance().getTimeInMillis();

	InstantiableEntityInfo infoHis = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("EmailAutomaticoDesvioDeHabito");
	InstantiableEntityInfo ExcecoesEmail = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("CadastroExcecoesEmails");
	InstantiableEntityInfo infoEnvioEmail = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("EmailDelivery");

	Set<Integer> empresasNotificadas = new HashSet<Integer>();

	Connection conn = null;
	StringBuilder strSigma = new StringBuilder();
	PreparedStatement pstm = null;
	ResultSet rs = null;

	pattern = Pattern.compile(EMAIL_PATTERN);
	String ultimaExecucaoRotina = OrsegupsEmailUtils.ultimaExecucaoRotinaEmail(OrsegupsEmailUtils.MONITOR_EMAIL_DELIVERY_RONDA_VIRTUAL_VTO);

	try {
	    strSigma.append(" 	 SELECT c.CGCCPF, c.OBSERVACAO, c.FANTASIA, c.RAZAO, c.ID_CENTRAL,c.ID_EMPRESA, c.PARTICAO, c.ENDERECO, cid.NOME AS NM_CIDADE, bai.NOME AS NM_BAIRRO, h.DT_FECHAMENTO, c.EMAILRESP, C.CD_GRUPO_CLIENTE, GS.NM_DESCRICAO AS DESCRICAO_GRUPO, ");
	    strSigma.append(" 	 hfe.NM_FRASE_EVENTO as NM_FRASE_EVENTO, ");
	    strSigma.append(" 	 h.TX_OBSERVACAO_FECHAMENTO, ISNULL(ma.DS_MOTIVO_ALARME, 'N/A - SEM ALTERAÃ‡ÃƒO') as motivo, h.CD_HISTORICO AS CD_HISTORICO, ");
	    strSigma.append(" 	 h.CD_EVENTO, c.CD_CLIENTE,  c.NU_LATITUDE, c.NU_LONGITUDE, h.DT_RECEBIDO, c.SERVIDORCFTV, c.PORTACFTV, c.USERCFTV, c.SENHACFTV  ");
	    strSigma.append(" 	  FROM VIEW_HISTORICO h  WITH (NOLOCK)    ");
	    strSigma.append(" 	 INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
	    strSigma.append(" 	 INNER JOIN ROTA r  WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA   ");
	    strSigma.append(" 	 INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE  ");
	    strSigma.append(" 	 INNER JOIN HISTORICO_FRASE_EVENTO hfe ON hfe.CD_FRASE_EVENTO = h.CD_FRASE_EVENTO  ");
	    strSigma.append(" 	 INNER JOIN GRUPO_CLIENTE GS WITH(NOLOCK) ON C.CD_GRUPO_CLIENTE = GS.CD_GRUPO_CLIENTE ");
	    strSigma.append(" 	 LEFT JOIN MOTIVO_ALARME ma ON ma.CD_MOTIVO_ALARME = h.CD_MOTIVO_ALARME  ");
	    strSigma.append(" 	 LEFT JOIN dbCIDADE cid ON cid.ID_CIDADE = c.ID_CIDADE  ");
	    strSigma.append(" 	 LEFT JOIN dbBAIRRO bai ON bai.ID_CIDADE = c.ID_CIDADE AND bai.ID_BAIRRO = c.ID_BAIRRO  ");
	    strSigma.append(" 	 INNER JOIN [CACUPE\\SQL02].fusion_producao.DBO.X_DBGRUPOCLIENTE g WITH(NOLOCK) ON g.cd_grupo_cliente = c.CD_GRUPO_CLIENTE ");
	    strSigma.append(" 	 INNER JOIN [CACUPE\\SQL02].fusion_producao.DBO.D_gruposVTO gvto WITH(NOLOCK) ON gvto.grupoCliente_neoId = g.neoId ");
	    strSigma.append(" 	 WHERE  H.CD_EVENTO = 'XXX7' AND C.TP_PESSOA != 2 ");
	    strSigma.append("    AND h.FG_STATUS = 4 AND C.TP_PESSOA != 2 ");
	    strSigma.append("    AND DT_VIATURA_DESLOCAMENTO IS NULL ");
	    strSigma.append(" 	 AND (H.CD_MOTIVO_ALARME <> 36 or  H.CD_MOTIVO_ALARME is null) ");
	    strSigma.append("    AND LEN(H.TX_OBSERVACAO_FECHAMENTO)>1 ");
	    strSigma.append(" 	 AND ( DT_FECHAMENTO > '" + ultimaExecucaoRotina + "' ) ");

	    conn = PersistEngine.getConnection("SIGMA90");

	    pstm = conn.prepareStatement(strSigma.toString());

	    OrsegupsEmailUtils.inserirFimRotinaEmail(OrsegupsEmailUtils.MONITOR_EMAIL_DELIVERY_RONDA_VIRTUAL_VTO);

	    rs = pstm.executeQuery();

	    String calendarDDMM = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM");
	    // String calendarDM = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "d/M");

	    while (rs.next()) {

		this.comRegistro = false;

		boolean clienteComExcecao = OrsegupsEmailUtils.verificarExcecaoCNPJ(rs.getInt("CD_CLIENTE"));
		String observacao = rs.getString("OBSERVACAO") == null ? "" : rs.getString("OBSERVACAO");

		if (!clienteComExcecao) {
		    String cgcCpfPrincipal = rs.getString("CGCCPF") == null ? "" : rs.getString("CGCCPF").replaceAll("([.\\-/])", "").trim();
		    String fantasia = (rs.getString("FANTASIA") == null ? "" : rs.getString("FANTASIA"));
		    String razao = (rs.getString("RAZAO") == null ? "" : rs.getString("RAZAO"));
		    String particao = (rs.getString("PARTICAO") == null ? "" : rs.getString("PARTICAO"));
		    String endereco = (rs.getString("ENDERECO") == null ? "" : rs.getString("ENDERECO"));
		    String cidade = (rs.getString("NM_CIDADE") == null ? "" : rs.getString("NM_CIDADE"));
		    String bairro = (rs.getString("NM_BAIRRO") == null ? "" : rs.getString("NM_BAIRRO"));
		    Timestamp dtFechamento = rs.getTimestamp("DT_FECHAMENTO");
		    String nomeFraseEvento = (rs.getString("NM_FRASE_EVENTO") == null ? "" : rs.getString("NM_FRASE_EVENTO"));
		    String txObsevacaoFechamento = (rs.getString("TX_OBSERVACAO_FECHAMENTO") == null ? "" : rs.getString("TX_OBSERVACAO_FECHAMENTO"));
		    String motivo = (rs.getString("motivo") == null ? "" : rs.getString("motivo"));
		    String evento = (rs.getString("CD_EVENTO") == null ? "" : rs.getString("CD_EVENTO"));
		    String email = (rs.getString("EMAILRESP") == null ? "" : rs.getString("EMAILRESP"));
		    String cdCliente = (rs.getString("CD_CLIENTE") == null ? "" : rs.getString("CD_CLIENTE"));
		    String dataAtendimento = NeoDateUtils.safeDateFormat(dtFechamento, "dd/MM/yyyy");
		    String horaAtendimento = rs.getString("DT_FECHAMENTO").substring(11, 16);
		    String historico = (rs.getString("CD_HISTORICO") == null ? "" : rs.getString("CD_HISTORICO"));
		    Timestamp dtRecebido = rs.getTimestamp("DT_RECEBIDO");
		    String imgLocalGrupoVTO = "";
		    // String dtRecebidoStr = NeoDateUtils.safeDateFormat(dtRecebido, "dd/MM/yyyy HH:mm:ss");

		    String central = rs.getString("ID_CENTRAL");
		    int empresa = rs.getInt("ID_EMPRESA");
		    int codigoGrupo = rs.getInt("CD_GRUPO_CLIENTE");
		    String grupoCliente = rs.getString("DESCRICAO_GRUPO");

		    log.warn("[XVID] fantasia:" + fantasia + ", razao:" + razao + ", email" + email);

		    EventoVO eventoVO = new EventoVO();

		    eventoVO.setCodigoCentral(central);
		    eventoVO.setEmpresa(String.valueOf(empresa));
		    eventoVO.setDtRecebido(dtRecebido);
		    eventoVO.setCodigoHistorico(historico);
		    eventoVO.setRazao(razao);
		    eventoVO.setServidorCFTV(rs.getString("SERVIDORCFTV") != null ? rs.getString("SERVIDORCFTV") : "");
		    eventoVO.setPortaCFTV(rs.getString("PORTACFTV") != null ? rs.getString("PORTACFTV") : "");
		    eventoVO.setUsuarioCFTV(rs.getString("USERCFTV") != null ? rs.getString("USERCFTV") : "");
		    eventoVO.setSenhaCFTV(rs.getString("SENHACFTV") != null ? rs.getString("SENHACFTV") : "");

		    if (txObsevacaoFechamento != null && !txObsevacaoFechamento.isEmpty()) {
			String array[] = txObsevacaoFechamento.split("###");
			String textoFormatado = "";
			for (String string : array) {
			    textoFormatado += string + "<br/>";
			}
			array = txObsevacaoFechamento.split(" ");
			List<String> respostas = new ArrayList<String>();
			List<String> cameras = new ArrayList<String>();
			int i = 0;
			for (String string : array) {
			    if (string.contains("R:")) {
				String arrayResp[] = string.split("\\n");
				if (!respostas.contains(arrayResp[0])) {
				    respostas.add(arrayResp[0]);
				}
			    }
			    if (string.equals("camera") || string.equals("cÃ¢mera")) {
				if (array.length > i + 1) {
				    String c = string + " " + array[i + 1];
				    if (!c.contains("(") && !c.contains(")") && !c.contains("{") && !c.contains("}") && !c.contains("[") && !c.contains("]")) {
					cameras.add(c);
				    }
				}
			    } else {
				if (string.contains("camera") || string.contains("cÃ¢mera")) {
				    if (string.contains(":")) {
					String arrayCam[] = string.split(":");
					cameras.clear();
					if (arrayCam.length > 1 && array.length > i + 1) {
					    cameras.add(arrayCam[1] + " " + array[i + 1]);
					}
				    } else {
					String arrayCam[] = string.split("\\n");
					for (String s : arrayCam) {
					    if (s.equals("camera") || s.equals("cÃ¢mera")) {
						if (array.length > i + 1) {
						    cameras.add(s + " " + array[i + 1]);
						}
					    }
					}
				    }
				}
			    }
			    i++;
			}

			if (!textoFormatado.isEmpty()) {
			    textoFormatado = textoFormatado.replaceAll("#", "");
			    textoFormatado = textoFormatado.replaceAll("\\r\\n", "");
			    textoFormatado = textoFormatado.replaceAll(";", "");
			    textoFormatado = textoFormatado.replaceAll(calendarDDMM, "<br/><br/>" + calendarDDMM);
			    // textoFormatado = textoFormatado.replaceAll(calendarDM, "<br/><br/>" + calendarDDMM);

			    if (respostas.size() > 0) {
				for (String resposta : respostas) {
				    textoFormatado = textoFormatado.replaceAll(resposta, "<br/>" + resposta + "<br/><br/>");
				}
			    }

			    if (cameras.size() > 0) {
				for (String camera : cameras) {
				    textoFormatado = textoFormatado.replaceAll(camera, "<br/>" + camera);
				}
			    }

			    textoFormatado = textoFormatado.replaceAll("Ligando para cliente!", "Ligando para cliente ");
			    textoFormatado = textoFormatado.replaceAll("Ramal", "a partir do ramal ");

			    textoFormatado = textoFormatado.replaceAll("FecharEvento", " Finalizar evento.");
			    textoFormatado = textoFormatado.replaceAll("EmEspera", " Aguardo deslocamento de um atendente.");
			    textoFormatado = textoFormatado.replaceAll("TratarSigma", "");
			    textoFormatado = textoFormatado.replaceAll("#ComAlteracao", "");

			    textoFormatado = textoFormatado.replaceAll("AtendimentoRealizado", " Finalizar evento.");
			    textoFormatado = textoFormatado.replaceAll("SemContato", " NÃ£o houve sucesso em nenhuma tentativa de contato.");
			    textoFormatado = textoFormatado.replaceAll("AtualizarCadastro", " Atualizar cadastro do cliente.");
			    textoFormatado = textoFormatado.replaceAll("NaoEnviaComunicado", " Comunicado de alarme com a central de monitoramento.");

			    txObsevacaoFechamento = textoFormatado;
			}
		    }
		    boolean flagAtualizadoNAC = observacao.contains("#AC");

		    List<String> emailClie = OrsegupsEmailUtils.validarEmail(email, cdCliente, flagAtualizadoNAC);

		    List<String> emailGrupo = OrsegupsEmailUtils.getEmailsGrupoCliente(codigoGrupo);

		    for (String m : emailGrupo) {
			if (!emailClie.contains(m.toLowerCase().trim())) {
			    emailClie.add(m);
			}
		    }

		    if ((emailClie != null) && (!emailClie.isEmpty())) {
			log.warn("[XVID] email do cliente validado");

			String neoId = null;
			String lisLnk = "";

			final String tipo = OrsegupsEmailUtils.TIPO_RAT_VERIFICACAO;

			Map<Integer, String> params = OrsegupsEmailUtils.getConfiguracaoEmpresa(empresa);

			String pasta = null;
			String remetente = null;
			String grupo = null;
			Long tipRat = 12L;
			Long executionTime = new java.util.Date().getTime();
			String ratingToken = DigestUtils.sha256Hex(cgcCpfPrincipal + Long.toString(tipRat) + Long.toString(executionTime));
			
			if (params != null) {
			    pasta = params.get(OrsegupsEmailUtils.RAT_PASTA);
			    remetente = params.get(OrsegupsEmailUtils.RAT_REMETENTE);
			    grupo = params.get(OrsegupsEmailUtils.RAT_GRUPO);
			    neoId = params.get(OrsegupsEmailUtils.RAT_NEOID);

			    this.salvarNovasImagens(central, particao, empresa, dtFechamento.getTime(), historico);

			    List<String> urls = OrsegupsEmailUtils.getImagensAmazonS3(Integer.valueOf(historico));

			    for (String emailFor : emailClie) {
				// String emailFor = emailClie.get(0);
				log.warn("[XVID] addTo: " + emailFor);
				StringBuilder noUserMsg = new StringBuilder();

				NeoObject emailExce = PersistEngine.getObject(ExcecoesEmail.getEntityClass(), new QLEqualsFilter("email", emailFor));

				if (emailExce == null) {

				    noUserMsg.append(OrsegupsEmailUtils.getCabecalhoEmail(tipo, pasta, ratingToken));

				    noUserMsg.append("\n <table width=\"600\" border=\"0\">");
				    noUserMsg.append("\n <tbody>");
				    noUserMsg.append("\n <tr align='center'>");
				    noUserMsg.append("\n <td colspan=\"6\" style=\"padding-left:1%;padding-right:18%;\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Informamos o atendimento ao seguinte evento de seu sistema de seguranÃ§a:</p>");
				    noUserMsg.append("\n </td></tr> </br>");
				    noUserMsg.append("\n <tr align='center'> ");
				    noUserMsg.append("\n <td colspan=\"6\" style=\"text-align: center;\"> <p style=\"font-family: 'Verdana';font-weight:normal;font-size:20px;text-align:center;color:#007FFF;font-weight:bold;padding:10px;margin-top:0px;\">RONDA VIRTUAL VIZINHANÃ‡A TRANQUILA</p>");
				    noUserMsg.append("\n </td> </tr><br/>");
				    noUserMsg.append("\n <tr align='center'>");
				    noUserMsg.append("\n <td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/separator-02.jpg\" width=\"600\" height=\"8\" alt=\"\"/></td>");
				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n <tr>");
				    noUserMsg.append("\n <tr width=\"600\"> ");
				    noUserMsg
					    .append("\n <td  style=\"padding:5px;\"><h2 style=\"border-bottom:1px solid #CCC;margin:0px 15px 20px 5px;padding-bottom:10px;line-height:14px;font-size:20px;text-align:center;font-family: 'Verdana';font-weight:normal;\">DETALHES SOBRE O ATENDIMENTO</td>");
				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n <td><table width=\"600\" border=\"0\">");
				    noUserMsg.append("\n <tbody>");
				    if (txObsevacaoFechamento != null && !txObsevacaoFechamento.equals("")) {
					noUserMsg.append("\n <tr>");
					noUserMsg.append("\n <td colspan=\"8\">");
					noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align:justify;\"> " + txObsevacaoFechamento + "</p></td>");
					noUserMsg.append("\n </tr>");
				    }
				    noUserMsg.append("\n <tr>");
				    noUserMsg.append("\n <td colspan=\"6\" align=\"left\" valign=\"top\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:14px;\">");

				    noUserMsg.append("<img src=\"");

				    if (codigoGrupo != 0) {
					imgLocalGrupoVTO = OrsegupsEmailUtils.getMapaGrupoClienteSigma(codigoGrupo);
					noUserMsg.append(imgLocalGrupoVTO);
				    } else {
					noUserMsg.append("https://maps.googleapis.com/maps/api/staticmap?zoom=19&size=300x300&maptype=hybrid&format=png" + "&markers=icon:http://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/maps/images/casa_rat_email.png%7C" + rs.getString("NU_LATITUDE") + ","
						+ rs.getString("NU_LONGITUDE"));
				    }

				    noUserMsg
					    .append("\" width=\"300\" height=\"300\" alt=\"\"/></p><p style=\"font-family: 'Verdana';font-weight:normal;font-size:8px;width: 300px;\">* A posiÃ§Ã£o da viatura Ã© baseada em coordenadas fornecidas pelo GPS do veÃ­culo e pode sofrer variaÃ§Ãµes de posicionamento em virtude de alguns fatores tais como: precisÃ£o do equipamento, cobertura, etc.</p></td>");
				    noUserMsg.append("\n <td width=\"10%\"><p><br>");
				    noUserMsg.append("\n </p></td>");
				    noUserMsg.append("\n <td width=\"51%\" align=\"left\" valign=\"top\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Local </br> <strong>" + grupoCliente + "</strong></br>");
				    noUserMsg.append("\n <strong>" + cidade + "</strong></p>");
				    noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Data <br> <strong>" + dataAtendimento + "</strong></br>");
				    noUserMsg.append("\n Hora  </br> <strong>" + horaAtendimento + "</strong></p>");
				    noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Evento </br> ");
				    noUserMsg.append("\n <strong>RONDA VIRTUAL VIZINHANÃ‡A TRANQUILA</strong></p> ");

				    //
				    noUserMsg.append("\n </td></tr>");

				    noUserMsg.append("\n <tr>");
				    noUserMsg.append("\n </tbody>");
				    noUserMsg.append("\n </table>");
				    noUserMsg.append("\n <br/><table width=\"600\" border=\"0\" style=\"margin-bottom:-3px;\">");
				    noUserMsg.append("\n <tbody>");

				    int y = 0;

				    QLGroupFilter filter = new QLGroupFilter("AND");
				    filter.addFilter(new QLEqualsFilter("historico", historico));

				    try {
					if (urls != null) {
					    for (String url : urls) {
						if (y == 2) {
						    noUserMsg.append("\n </tr>");
						    noUserMsg.append("\n <tr>");
						    y = 0;
						}
						//
						noUserMsg.append("\n <td valign=\"top\"> ");
						noUserMsg.append(" <img src=\"" + url + "\" height=\"240\" width=\"320\" border=\"0\" alt=\"Imagens\"></td>");
						y++;
						lisLnk = lisLnk + url + ";";
					    }
					}
				    } catch (Exception e) {
					log.error("##ImagensAmazonS3## Erro ao buscar imagens para o evento: " + historico);
					e.printStackTrace();
				    }

				    noUserMsg.append("\n </tr>");
				    noUserMsg.append("\n </tbody>");
				    noUserMsg.append("\n <br/></table>");

				    // FIM DO BLOCO DE IMAGENS;

				    noUserMsg.append(OrsegupsEmailUtils.getRodapeEmail(tipo, pasta, grupo, grupoCliente, remetente, ratingToken));
				    noUserMsg.append("\n <br><p style=\"font-family: 'Verdana';font-weight:normal;font-size:6px;text-align:justify; margin: 0 27%;\">CD Cliente: " + cdCliente + " Conta: " + central + " PartiÃ§Ã£o: " + particao + " RazÃ£o: " + razao + " CR: " + this.comRegistro
					    + "</p></td>");

				    NeoObject emailHis = infoHis.createNewInstance();
				    EntityWrapper emailHisWp = new EntityWrapper(emailHis);
				    adicionados = adicionados + 1;
				    emailHisWp.findField("fantasia").setValue(fantasia);
				    emailHisWp.findField("razao").setValue(razao);
				    emailHisWp.findField("particao").setValue(particao);
				    emailHisWp.findField("endereco").setValue(endereco);
				    emailHisWp.findField("cidade").setValue(cidade);
				    emailHisWp.findField("bairro").setValue(bairro);
				    emailHisWp.findField("dataFechamento").setValue(dataAtendimento);
				    emailHisWp.findField("horaFechamento").setValue(horaAtendimento);
				    // emailHisWp.findField("nomeViatura").setValue(nomeViatura);
				    // emailHisWp.findField("textoObservacaoFechamentos").setValue(txObsevacaoFechamento);
				    emailHisWp.findField("motivoAlarme").setValue(motivo);
				    emailHisWp.findField("evento").setValue(evento);
				    emailHisWp.findField("enviadoPara").setValue(emailClie.toString());
				    emailHisWp.findField("nomeFraseEvento").setValue(nomeFraseEvento);
				    emailHisWp.findField("historico").setValue(historico);
				    PersistEngine.persist(emailHis);
				    
				    // TESTE E-MAIL UTILIZANDO RECURSO FUSION
				    //String subject = "Relatório de Atendimento XVID - " + fantasia;
				    //OrsegupsEmailUtils.sendTestEmail(noUserMsg, subject);

				    GregorianCalendar dataCad = (GregorianCalendar) GregorianCalendar.getInstance();
				    NeoObject emaiEnvio = infoEnvioEmail.createNewInstance();
				    EntityWrapper emailEnvioWp = new EntityWrapper(emaiEnvio);
				    emailEnvioWp.findField("de").setValue("cm.rat" + remetente);
				    // emailEnvioWp.findField("para").setValue(emailFor +
				    // ";emailautomatico@orsegups.com.br;giliardi@orsegups.com.br;dilmoberger@orsegups.com.br;dirceu.costa@orsegups.com.br");
				    emailEnvioWp.findField("para").setValue(emailFor + ";emailautomatico@orsegups.com.br;copia@orsegups.com.br");
				    emailEnvioWp.findField("assunto").setValue("RelatÃ³rio de Atendimento Ronda Virtual VTO - " + grupoCliente + " - " + dataAtendimento + " " + horaAtendimento);
				    emailEnvioWp.findField("Mensagem").setValue(noUserMsg.toString());
				    emailEnvioWp.findField("datCad").setValue(dataCad);
				    PersistEngine.persist(emaiEnvio);

				    log.warn("Cliente [XVID]: " + fantasia + ", E-mail: " + emailFor);
				}
			    }
			    OrsegupsUtils.sendEmailWhatsAppRAT(Long.valueOf(cdCliente), "RelatÃ³rio de Atendimento Ronda Virtual VTO - " + grupoCliente + " - " + dataAtendimento + " " + horaAtendimento, endereco+" - "+bairro+" - "+cidade, dataAtendimento + " " + horaAtendimento);
			} else {
			    if (!empresasNotificadas.contains(empresa)) {
				OrsegupsEmailUtils.enviarNotificacaoEmpresaSemConfig(empresa);
				empresasNotificadas.add(empresa);
			    }
			}	
			
			try {
			    List<String> GcgsCpfs = OrsegupsEmailUtils.getCgcCpfGrupoCliente(codigoGrupo);
			    
			    // Montando informações para serviço Mobile - Inicio
			    for (String cgcCpf : GcgsCpfs) {
				cgcCpf = cgcCpf.replaceAll("([.\\-/])", "").trim();
				ObjRatMobile objRat = new ObjRatMobile();
				objRat.setTipRat(tipRat);
				objRat.setRatingToken(DigestUtils.sha256Hex(cgcCpf + Long.toString(tipRat) + Long.toString(executionTime)));
				objRat.setInformativo("RONDA VIRTUAL VIZINHANÇA TRANQUILA");
				objRat.setEvento("RONDA VIRTUAL VIZINHANÇA TRANQUILA");
				objRat.setHashId("Relatório de Atendimento Tático - " + cgcCpf + " - " + dataAtendimento + " " + horaAtendimento);
				objRat.setLocal(grupoCliente);
				objRat.setDataAtendimento(dataAtendimento);
				objRat.setHoraAtendimento(horaAtendimento);
				if (txObsevacaoFechamento != null && !txObsevacaoFechamento.equals("")) {
				    objRat.setObservacao(txObsevacaoFechamento);
				} else {
				    objRat.setObservacao("");
				}
				objRat.setEmpRat(grupo);
				objRat.setNeoId(neoId);
				if (!"".equals(imgLocalGrupoVTO)) {
				    objRat.setLnkFotoLocal(imgLocalGrupoVTO);
				} else {
				    objRat.setLnkFotoLocal("https://maps.googleapis.com/maps/api/staticmap?zoom=19&size=300x300&maptype=hybrid&format=png"
								    + "&markers=icon:http://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/maps/images/casa_rat_email.png%7C"
								    + rs.getString("NU_LATITUDE")+ ","+ rs.getString("NU_LONGITUDE"));
				}
				objRat.setLnkImg(lisLnk);
				// Montando informações para serviço Mobile - Fim
				
				if (!cgcCpf.equals("")) {
				    objRat.setCgcCpf(Long.parseLong(cgcCpf));
				    IntegracaoPortalMobile integracao = new IntegracaoPortalMobile();
				    integracao.inserirInformacoesPush(objRat);
				} else {
				    objRat.setCgcCpf(0L);
				}
			    }
				
			} catch (Exception e) {
			    log.error("Erro IntegracaoPortalMobile EmailDeliveryAtendimentoVTO", e);
			}

		    } else {
			log.warn("[XVID] Email do cliente invalido vou vazio ");
		    }
		}
	    }
	    log.warn("E-Mail XVID Fim execuÃ§Ã£o em: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	} catch (Exception e) {
	    log.error("E-Mail XVID erro no processamento:");
	    System.out.println("[" + key + "] E-Mail XVID erro no processamento: " + e.getMessage());
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

    }

    private void salvarNovasImagens(String conta, String particao, int empresa, long dtFechamento, String historico) {

	CameriteGrupo[] grupos = CameriteEngine.getInfoGrupo(conta, particao, empresa);

	if (grupos != null && grupos.length > 0) {

	    CameriteGrupo g = null;

	    for (CameriteGrupo gr : grupos) {
		if (gr.getTitle().contains("VTO") || gr.getTitle().contains("vto")) {
		    g = gr;
		    break;
		}
	    }

	    if (g == null) {
		g = grupos[0];
	    }

	    List<CameriteCamera> cameras = g.getGroupCamera();

	    Set<Integer> idsFiltrados = new HashSet<Integer>();

	    for (CameriteCamera c : cameras) {
		int id = c.getId();

		if (idsFiltrados.contains(id)) {
		    continue;
		} else {
		    idsFiltrados.add(id);
		}

		CameriteResponse r = CameriteEngine.getQuickRecord(id, dtFechamento, true);

		if (r != null && r.getData().getErrorMessage() == null) {
		    String url = r.getData().getShot();

		    try {
			File f = CameriteEngine.getFileFromURL(url);

			if (f != null) {

			    HttpClient httpclient = new DefaultHttpClient();
			    HttpPost httppost = new HttpPost("https://apps2.orsegups.com.br:8080/AtendimentoEventosServices/amazonS3/uploadFiles/" + historico);

			    httppost.setHeader("x-auth-token", "682ee2548d4a73d8fadf72f32c52df790eb72aff");

			    MultipartEntityBuilder builder = MultipartEntityBuilder.create();

			    FileBody fileBody = new FileBody(f);

			    builder.addPart("file", fileBody);

			    HttpEntity multiPartEntity = builder.build();

			    httppost.setEntity(multiPartEntity);

			    HttpResponse response = httpclient.execute(httppost);

			    System.out.println(response.toString());

			}

		    } catch (Exception e) {
			e.printStackTrace();
		    }
		}

	    }
	}

    }

    public static List<String> dias(GregorianCalendar calendar) {
	List<String> cptList = null;
	if (NeoUtils.safeIsNotNull(calendar)) {
	    long milisecInicial = calendar.getTime().getTime();
	    long milisecFinal = new GregorianCalendar().getTime().getTime();
	    long dif = milisecFinal - milisecInicial;

	    long dias = (((dif / 1000) / 60) / 60) / 24;
	    cptList = new ArrayList<String>();
	    Calendar dia = Calendar.getInstance();
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM");
	    for (int i = 0; i <= dias; i++) {
		String diaStr = dateFormat.format(dia.getTime());
		cptList.add(diaStr);
		dia.add(Calendar.DAY_OF_MONTH, -1);
	    }

	}
	return cptList;
    }

}