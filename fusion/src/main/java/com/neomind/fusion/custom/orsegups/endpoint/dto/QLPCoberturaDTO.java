package com.neomind.fusion.custom.orsegups.endpoint.dto;

public class QLPCoberturaDTO {
    
    private String idNexti;
    private String colaborador;
    private String colaboradorASubstituir;
    private long dataInicio;
    private long dataFim;
    private int idMotivo;
    private int idPosto;
    private String observacao;
    private int idHorario;
    
    public String getIdNexti() {
        return idNexti;
    }
    public void setIdNexti(String idNexti) {
        this.idNexti = idNexti;
    }
    public String getColaboradorASubstituir() {
        return colaboradorASubstituir;
    }
    public void setColaboradorASubstituir(String colaboradorASubistituir) {
        this.colaboradorASubstituir = colaboradorASubistituir;
    }
    public int getIdPosto() {
        return idPosto;
    }
    public void setIdPosto(int idPosto) {
        this.idPosto = idPosto;
    }
    public String getColaborador() {
        return colaborador;
    }
    public void setColaborador(String colaborador) {
        this.colaborador = colaborador;
    }
    public long getDataInicio() {
        return dataInicio;
    }
    public void setDataInicio(long dataInicio) {
        this.dataInicio = dataInicio;
    }
    public long getDataFim() {
        return dataFim;
    }
    public void setDataFim(long dataFim) {
        this.dataFim = dataFim;
    }
    public int getIdMotivo() {
        return idMotivo;
    }
    public void setIdMotivo(int idMotivo) {
        this.idMotivo = idMotivo;
    }
    public String getObservacao() {
        return observacao;
    }
    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
    public int getIdHorario() {
	return idHorario;
    }
    public void setIdHorario(int idHorario) {
	this.idHorario = idHorario;
    }
    
    
}
