package com.neomind.fusion.custom.orsegups.escala;

import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.servicos.helpers.WorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.security.NeoGroup;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class OrsegupsEscalaAprova implements AdapterInterface
{

	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		
	    	EntityWrapper pEntity = processEntity; 
		
		try
		{
		    	
		    Task tarefaAnterior = retornaTarefaAnterior(activity.getActivity());
		    
		    String atividadeanterior = tarefaAnterior.getActivityName();
		    
		    NeoUser solicitante = tarefaAnterior.getUser();
		    
		    NeoPaper papelResponsavel = solicitante.getGroup().getResponsible();
		    
		    Integer respSetado = 0;
		    
		    Integer ok = 0;
		    
		    String status = "";
		    
		    Integer position = tarefaAnterior.getActivityName().indexOf(".");
		    
		    String paramExecRaia = tarefaAnterior.getActivityName().substring(0, position);
		    
		    //1 = Solicitar Aprovação || 2 = Devolver para Regional || 3 = Aprovar || 4 = Rejeitar || 5 = Efetuar Integração || 6 = Enviar para Sede
		    Long opcaoAcao = pEntity.findGenericValue("validacao.acao.codigo");
		    
		    Boolean escalado = pEntity.findGenericValue("escalado");
		    
		    NeoUser primeiroUser = null;
		    
		    if(pEntity.findGenericValue("primeiroUser") != null)
			primeiroUser = pEntity.findGenericValue("primeiroUser");
		    
		    if(escalado == false){
			
			if(opcaoAcao == 1 && escalado == false) {
    			
    				NeoUser userAprovador = pEntity.findGenericValue("validacao.userAprovador");
    				
    				primeiroUser = tarefaAnterior.getUser();
    				
    				processEntity.findField("primeiroUser").setValue(primeiroUser);
			    
			    	status = "APROVAÇÃO ";
    			
    				processEntity.findField("exec"+paramExecRaia).setValue(userAprovador);
    				
    				processEntity.findField("usuarioAprovador").setValue(Boolean.TRUE);
    				
    				processEntity.findField("status").setValue(status);
    				
    				if(atividadeanterior.equals("1. Iniciar Admissão")){
    				    
    				    processEntity.findField("tarefaAtual").setValue("1. Iniciar Admissão.");
    				    
    				}
    				
    				pEntity.findField("validacao.userAprovador").setValue(null);
    				
    				pEntity.findField("validacao.observacao").setValue(null);
			
			}else if(opcaoAcao == 3 && escalado == false){
			    
			    status = "";
			    
			    processEntity.findField("exec"+paramExecRaia).setValue(primeiroUser);
			    
			    processEntity.findField("usuarioAprovador").setValue(Boolean.FALSE);
			    
			    processEntity.findField("status").setValue(status);
			    
			    List<NeoObject> listaParaAprovar = processEntity.findGenericValue("validacao.listaCamposAprovacao");
			    
			    List<NeoObject> listaAprovados = processEntity.findGenericValue("validacao.listaCamposAprovados");
			    
			    Integer tamanholistaParaAprovar = listaParaAprovar.size();
			    
			    if(tamanholistaParaAprovar > 0){
				
				for(NeoObject itemListaParaAprovar : listaParaAprovar){
    				
					listaAprovados.add(itemListaParaAprovar);
					
    			    	}
				
				listaParaAprovar.clear();
				
			    }
			    
			    pEntity.findField("validacao.observacao").setValue(null);
			    
			    
			}else if(opcaoAcao == 4 && escalado == false){
			    
			    status = "";
			   
			    processEntity.findField("exec"+paramExecRaia).setValue(primeiroUser);
			    
			    processEntity.findField("usuarioAprovador").setValue(Boolean.FALSE);
			    
			    processEntity.findField("status").setValue(status);
			    
			    pEntity.findField("validacao.observacao").setValue(null);
			    
			}
		    
		    }else if(escalado == true){
        			
        			status = "ESCALADO ";
        			
        			 //Quando papel responsável for do Presidente não sobre pra ninguém e apresenta uma exceção em tela		    
        			    if(papelResponsavel.getNeoId() != 125900l){
        				
        				if(papelResponsavel != null) { 
        					
        					Integer cont = 0;
        					
        					Set<NeoUser> users = papelResponsavel.getAllUsers();
        					
        					//caso o solicitante não faça parte do papel selecionado ele seta o primeiro usuário desse papel responsável
        					if(!users.contains(solicitante)){
        					    
        					    	for (NeoUser user : users){
        							
        							if(cont == 0) {
        							    
        							    status = status + papelResponsavel.getName();
        							    
        							    processEntity.findField("exec"+paramExecRaia).setValue(user);
        							    
        							    processEntity.findField("status").setValue(status);
        							    
        							    cont++;
        							    
        							    respSetado++;
        							    
        							    ok++;
        							    
        							}
        						    
        						}
        					    
        					}
        					
        					Long idSuperior = solicitante.getGroup().getUpperLevel().getNeoId();
        					
        					NeoGroup grupoSuperior = PersistEngine.getObject(NeoGroup.class, new QLRawFilter("neoId = " + idSuperior));
        					
        					NeoPaper papelSuperior = PersistEngine.getObject(NeoPaper.class, new QLRawFilter("neoId = " + idSuperior));
        					
        					//caso solicitante faça parte do papel selecionado ele busca o grupo superior e o primeiro usuário dele
        					if(grupoSuperior != null && respSetado == 0){
        					    
        					    NeoPaper papelRespGroup = grupoSuperior.getResponsible();
        					    
        					    Set<NeoUser> respsPapelRespGroup = papelRespGroup.getAllUsers();
        					    
        					    Integer i = 0;
        					    
        					    for(NeoUser respPapelRespGroup : respsPapelRespGroup){
        						
        						if(i == 0){
        						    
        						    	status = status + grupoSuperior.getName();
        							
        							processEntity.findField("exec"+paramExecRaia).setValue(respPapelRespGroup);
        							
        							processEntity.findField("status").setValue(status);
        							
        							respSetado++;
        							
        							i++;
        							
        							ok++;
        							
        						    }
        						
        					    }
        					
        					//caso solicitante faça parte do papel selecionado e o superior seja um papel ele busca aqui o papel e o 
        					//primeiro usuário dele
        					}else if(papelSuperior != null && respSetado == 0){
        					    
        					    Set<NeoUser> respsPapelResp = papelSuperior.getAllUsers();
        					    
        					    Integer i = 0;
        					    
        					    for(NeoUser respPapel : respsPapelResp){
        						
        						if(i == 0){
        						    
        						    status = status + papelSuperior.getName();
        						    
        						    processEntity.findField("exec"+paramExecRaia).setValue(respPapel);
        						    
        						    processEntity.findField("status").setValue(status);
        							
        						    respSetado++;
        							
        						    i++;
        							
        						    ok++;
        						    
        						}
        						
        					    }
        					    
        					}
        					
        				    }
        				
        			    }
        			    
        			    if(ok == 0){
        				
        				pEntity.findField("escalado").setValue(Boolean.FALSE); 
        				throw new WorkflowException("Não há superior para o usuário: " + solicitante.getFullName());
        				
        			    }
        			
		    }
		    
		pEntity.findField("escalado").setValue(Boolean.FALSE);    
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if ((e instanceof WorkflowException))
			{
				throw ((WorkflowException) e);
			}
			throw new WorkflowException("Erro na execução");
		}
	}


	public void back(EntityWrapper entitywrapper, Activity activity1)
	{
	}
	
	public Task retornaTarefaAnterior(Activity atividade)
	{
		Task tarefaAnterior = null;
		
		GregorianCalendar ultimaDataFim = null;
		
		List<Task> tarefasAnteriores = new WorkflowHelper().getTaskOrigins(atividade);
		
		if (tarefasAnteriores != null && tarefasAnteriores.size() > 0)
		{
			for (Task tarefa : tarefasAnteriores)
			{
				if (ultimaDataFim == null || tarefa.getActivity().getFinishDate().after(ultimaDataFim))
				{
					ultimaDataFim = tarefa.getActivity().getFinishDate();
					
					tarefaAnterior = tarefa;
				}
			}
		}
		
		return tarefaAnterior;
	}
	
	public void finish(Task origin, EntityWrapper processEntity, Activity activity)
	{
	    
	    String teste = "";
	    
	}

}
