package com.neomind.fusion.custom.orsegups.processoJuridico.adapter;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.IniciarTarefaValorRecuperar

public class IniciarTarefaValorRecuperar implements AdapterInterface
{
	@Override
	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
	    try{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		String titulo = "";
		String descricao = "";
		String executor = "rafael";
		String solicitante = "dilmoberger";
		GregorianCalendar prazo = null;
		String descricaoRegistro = "";
		if (processEntity.getValue("colaborador") != null)
		{
			NeoObject colaborador = (NeoObject) processEntity.getValue("colaborador");
			EntityWrapper wColaborador = new EntityWrapper(colaborador);
			colaborador = (NeoObject) processEntity.getValue("colaborador");
			titulo = "J002 - " + activity.getCode() + " - " + wColaborador.getValue("numemp") + "/" + wColaborador.getValue("numcad") + " - " + wColaborador.getValue("nomfun") + " - Valor a Recuperar";
		}
		else
		{
			NeoObject autorNaoColaborador = (NeoObject) processEntity.getValue("autorNaoCol");
			EntityWrapper wAutor = new EntityWrapper(autorNaoColaborador);
			String cpfAut = String.valueOf(wAutor.getValue("cpfAut"));
			String nomAut = String.valueOf(wAutor.getValue("nomAut"));
			titulo = cpfAut + " / " + nomAut;
		}
		descricao = "Informar qual o valor a recuperar referente ao processo jurídico.";
		prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);

		IniciarTarefaSimples tarefaSimples = new IniciarTarefaSimples();
		String code = tarefaSimples.abrirTarefaReturnNeoIdCode(solicitante, executor, titulo, descricao, "1", "Sim", prazo);
		String[] neoIdECode = code.split(";");
		System.out.println("Tarefa aberta : " + neoIdECode[1]);
		descricaoRegistro = "Aberto tarefa simples " + neoIdECode[1] + " para informar valor a recuperar.";
		List<NeoObject> registroAtividade = (List<NeoObject>) processEntity.getValue("j002RegistroHistoricoAtividade");
		String responsavel = origin.returnResponsible();
		GregorianCalendar dataAcao = new GregorianCalendar();
		InstantiableEntityInfo insRegAti = AdapterUtils.getInstantiableEntityInfo("j002RegistroHistoricoAtividade");
		NeoObject objRegAti = insRegAti.createNewInstance();
		EntityWrapper wRegAti = new EntityWrapper(objRegAti);
		wRegAti.setValue("dataAcao", dataAcao);
		wRegAti.setValue("atividade", origin.getActivityName().toString());
		wRegAti.setValue("responsavel", responsavel);
		wRegAti.setValue("descricao", descricaoRegistro);

		PersistEngine.persist(objRegAti);
		registroAtividade.add(objRegAti);
		processEntity.setValue("possuiVlrRecuperar",false);
	    }catch(Exception e){
		System.out.println("Erro na classe IniciarTarefaValorRecuperar do fluxo J002.");
		e.printStackTrace();
		throw new WorkflowException(e.getMessage());
	    }
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
