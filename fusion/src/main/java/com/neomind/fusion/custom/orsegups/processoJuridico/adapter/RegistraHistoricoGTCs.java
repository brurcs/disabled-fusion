package com.neomind.fusion.custom.orsegups.processoJuridico.adapter;

import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.RegistraHistoricoGTCs

public class RegistraHistoricoGTCs implements AdapterInterface
{
	@Override
	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{
			origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
			List<NeoObject> historicoGTCs = (List<NeoObject>) processEntity.findValue("j002HistoricoGTCs");
			// GTC a ser lançada
			List<NeoObject> listaGTCs = (List<NeoObject>) processEntity.findValue("listaGtcsInfo");
			List<NeoObject> listaGTCsLancar = (List<NeoObject>) processEntity.findValue("listaGTCLancar");
			List<NeoObject> listaGTCsComplementar = (List<NeoObject>) processEntity.findValue("lisPgtoComp");
			List<NeoObject> listaGTCAprovar = (List<NeoObject>) processEntity.findValue("listaGTCAprovar");
			boolean isColaborador = (boolean) processEntity.findValue("isColaborador");
			Long codTipoProcesso = processEntity.findGenericValue("tipoProcesso.codigo");

			if (listaGTCsComplementar != null && listaGTCsComplementar.size() > 0)
				listaGTCs.addAll(listaGTCsComplementar);
			
			for (NeoObject objGTC : listaGTCs)
			{
				EntityWrapper wGtc = new EntityWrapper(objGTC);
				// Copiando
				List<NeoObject> ObjParcelas = (List<NeoObject>) wGtc.getValue("parcelas");
				if (ObjParcelas != null && ObjParcelas.size() > 0)
				{
					for (NeoObject parcela : ObjParcelas)
					{
						EntityWrapper wParcela = new EntityWrapper(parcela);
						InstantiableEntityInfo insGTCpgto = AdapterUtils.getInstantiableEntityInfo("GTCGestaoTitulosControladoria");
						NeoObject objGtcPgto = insGTCpgto.createNewInstance();
						EntityWrapper woGtcPgto = new EntityWrapper(objGtcPgto);

						InstantiableEntityInfo insGTCTomarCiencia = AdapterUtils.getInstantiableEntityInfo("GTCGestaoTitulosControladoria");
						NeoObject objGtcPgtoTomarCiencia = insGTCTomarCiencia.createNewInstance();
						EntityWrapper woGtcPgtoTomarCiencia = new EntityWrapper(objGtcPgtoTomarCiencia);
						NeoObject empresa = (NeoObject) processEntity.getValue("empReu");
						NeoObject filial = (NeoObject) processEntity.getValue("filReu");

						if (codTipoProcesso == null || codTipoProcesso == 1)
						{
							// Centro de custo do colaborador
							if (isColaborador)
							{
								NeoObject colaborador = (NeoObject) processEntity.getValue("colaborador");
								EntityWrapper wColaborador = new EntityWrapper(colaborador);

								QLGroupFilter groupFilter = new QLGroupFilter("AND");
								String lCodccu = String.valueOf(wColaborador.getValue("codccu"));
								QLFilter qlCodccu = new QLEqualsFilter("codccu", lCodccu);
								groupFilter.addFilter(qlCodccu);

								List<NeoObject> objsCcu = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), groupFilter);

								if (objsCcu != null && objsCcu.size() > 0)
								{
									woGtcPgto.setValue("centroCusto", objsCcu.get(0));
								}
							}
						}
						else
						{
							// centro de custo informado pelo dpto jurídico

							String lCodccu = processEntity.findGenericValue("codccuInformado").toString();
							QLFilter qlCodccu = new QLEqualsFilter("codccu", lCodccu);

							List<NeoObject> objsCcu = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), qlCodccu);

							if (objsCcu != null && objsCcu.size() > 0)
							{
								woGtcPgto.setValue("centroCusto", objsCcu.get(0));
							}
						}

						// woGtcPgto.setValue("contaFinanceira",
						// wGtc.getValue("contaFinanceira"));

						if (empresa != null)
						{
							QLGroupFilter groupFilter = new QLGroupFilter("AND");
							EntityWrapper wEmpresa = new EntityWrapper(empresa);

							QLFilter qlEmpresa = new QLEqualsFilter("codemp", (Long) wEmpresa.getValue("codemp"));
							QLFilter qlCtaFin = new QLEqualsFilter("ctafin", 26L);
							groupFilter.addFilter(qlEmpresa);
							groupFilter.addFilter(qlCtaFin);

							List<NeoObject> objsCtaFin = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE091PLF"), groupFilter);

							if (objsCtaFin != null && objsCtaFin.size() > 0)
							{
								woGtcPgto.setValue("contaFinanceira", objsCtaFin.get(0));
								woGtcPgtoTomarCiencia.setValue("contaFinanceira", objsCtaFin.get(0));
							}

							QLGroupFilter groupFilterContabil = new QLGroupFilter("AND");
							QLFilter qlEmpresaContabil = new QLEqualsFilter("codemp", (Long) wEmpresa.getValue("codemp"));
							QLFilter qlCtaContabil = null;
							if ((Boolean) wGtc.getValue("isDefinitivo"))
							{
								qlCtaContabil = new QLEqualsFilter("ctared", 4620L);
							}
							else
							{
								qlCtaContabil = new QLEqualsFilter("ctared", 1210L);
							}
							groupFilterContabil.addFilter(qlEmpresaContabil);
							groupFilterContabil.addFilter(qlCtaContabil);

							List<NeoObject> objsCtaContabil = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE045PLA"), groupFilterContabil);

							if (objsCtaFin != null && objsCtaFin.size() > 0)
							{
								woGtcPgto.setValue("contaContabil", objsCtaContabil.get(0));
								woGtcPgtoTomarCiencia.setValue("contaContabil", objsCtaContabil.get(0));
							}
						}

						woGtcPgto.setValue("usuarioResponsavel", wGtc.getValue("usuarioResponsavel"));
						woGtcPgto.setValue("listaLancamento", wGtc.getValue("listaLancamento"));
						// woGtcPgto.setValue("contaFinanceira",
						// wGtc.getValue("contaFinanceira"));
						// woGtcPgto.setValue("contaContabil",
						// wGtc.getValue("contaContabil"));
						woGtcPgto.setValue("empresa", empresa);
						woGtcPgto.setValue("codigoFilial", filial);
						woGtcPgto.setValue("titulo", wGtc.getValue("titulo"));
						woGtcPgto.setValue("tipoTitulo", wGtc.getValue("tipoTitulo"));
						woGtcPgto.setValue("fornecedor", wGtc.getValue("fornecedor"));
						woGtcPgto.setValue("observacao", wGtc.getValue("observacao"));
						woGtcPgto.setValue("valorOriginal", wParcela.getValue("valorParcela"));
						woGtcPgto.setValue("vencimentoProrrogado", wParcela.getValue("vencimento"));
						woGtcPgto.setValue("vencimentoOriginal", wParcela.getValue("vencimento"));
						woGtcPgto.setValue("formaPagamento", wGtc.getValue("formaPagamento"));
						woGtcPgto.setValue("codigoAgencia", wGtc.getValue("codigoAgencia"));
						woGtcPgto.setValue("codigoBanco", wGtc.getValue("codigoBanco"));
						woGtcPgto.setValue("contaBanco", wGtc.getValue("contaBanco"));
						woGtcPgto.setValue("j002Anexo", wGtc.getValue("j002Anexo"));
						woGtcPgto.setValue("j002ClaPgtos", wGtc.getValue("j002ClaPgtos"));
						woGtcPgto.setValue("isDefinitivo", wGtc.getValue("isDefinitivo"));

						woGtcPgtoTomarCiencia.setValue("usuarioResponsavel", wGtc.getValue("usuarioResponsavel"));
						woGtcPgtoTomarCiencia.setValue("listaLancamento", wGtc.getValue("listaLancamento"));
						// woGtcPgtoTomarCiencia.setValue("contaFinanceira",
						// wGtc.getValue("contaFinanceira"));
						// woGtcPgtoTomarCiencia.setValue("contaContabil",
						// wGtc.getValue("contaContabil"));
						woGtcPgtoTomarCiencia.setValue("empresa", empresa);
						woGtcPgtoTomarCiencia.setValue("codigoFilial", filial);
						woGtcPgtoTomarCiencia.setValue("titulo", wGtc.getValue("titulo"));
						woGtcPgtoTomarCiencia.setValue("tipoTitulo", wGtc.getValue("tipoTitulo"));
						woGtcPgtoTomarCiencia.setValue("fornecedor", wGtc.getValue("fornecedor"));
						woGtcPgtoTomarCiencia.setValue("observacao", wGtc.getValue("observacao"));
						woGtcPgtoTomarCiencia.setValue("valorOriginal", wParcela.getValue("valorParcela"));
						woGtcPgtoTomarCiencia.setValue("vencimentoProrrogado", wParcela.getValue("vencimento"));
						woGtcPgtoTomarCiencia.setValue("vencimentoOriginal", wParcela.getValue("vencimento"));
						woGtcPgtoTomarCiencia.setValue("formaPagamento", wGtc.getValue("formaPagamento"));
						woGtcPgtoTomarCiencia.setValue("codigoAgencia", wGtc.getValue("codigoAgencia"));
						woGtcPgtoTomarCiencia.setValue("codigoBanco", wGtc.getValue("codigoBanco"));
						woGtcPgtoTomarCiencia.setValue("contaBanco", wGtc.getValue("contaBanco"));
						woGtcPgtoTomarCiencia.setValue("j002Anexo", wGtc.getValue("j002Anexo"));
						woGtcPgtoTomarCiencia.setValue("j002ClaPgtos", wGtc.getValue("j002ClaPgtos"));
						woGtcPgtoTomarCiencia.setValue("isDefinitivo", wGtc.getValue("isDefinitivo"));

						PersistEngine.persist(objGtcPgto);
						PersistEngine.persist(objGtcPgtoTomarCiencia);
						listaGTCsLancar.add(objGtcPgto);
						listaGTCAprovar.add(objGtcPgtoTomarCiencia);

						EntityWrapper wNovaGTC = new EntityWrapper(objGtcPgto);

						InstantiableEntityInfo insRegAti = AdapterUtils.getInstantiableEntityInfo("j002HistoricoGTCs");
						NeoObject objRegAti = insRegAti.createNewInstance();
						EntityWrapper wRegAti = new EntityWrapper(objRegAti);
						wRegAti.setValue("neoIdGtc", wNovaGTC.getValue("neoId"));
						wRegAti.setValue("StatusGTC", "Pendente de Lançamento");

						PersistEngine.persist(objRegAti);
						historicoGTCs.add(objRegAti);
					}
				}

			}

			if (listaGTCsLancar == null || listaGTCsLancar.size() == 0)
				processEntity.setValue("possuiGTC", false);
			else
				processEntity.setValue("possuiGTC", true);

			processEntity.setValue("j002HistoricoGTCs", historicoGTCs);
			processEntity.setValue("listaGtcsInfo", null);
			processEntity.setValue("temPgo", false);
			processEntity.setValue("lisPgtoComp", null);
			processEntity.setValue("pgtoComplementar", false);
			processEntity.setValue("listaGTCLancar", listaGTCsLancar);
		}
		catch (Exception e)
		{
			System.out.println("Erro na classe RegistraHistoricoGTCs do fluxo J002.");
			e.printStackTrace();
			throw new WorkflowException(e.getMessage());
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
