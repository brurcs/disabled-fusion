package com.neomind.fusion.custom.orsegups.e2doc.controller;

import java.util.HashMap;
import java.util.List;

import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.neomind.fusion.custom.orsegups.e2doc.engine.E2docFinanceiroEngine;
import com.neomind.fusion.custom.orsegups.e2doc.xml.financeiro.Pastas;

@Path(value = "documentosFinanceiro")
public class E2docFinanceiroHandler {
    
	@POST
	@Path("pesquisaFinanceiro")
	@Produces("application/json")
	public Pastas pesquisaFinanceiro(
		@MatrixParam("tiposDocumentos") String tiposDocumentos, @MatrixParam("empresa") String empresa,
		@MatrixParam("competencia") String competencia) {
	    
	    //nome da chave, valor 
	    HashMap<String, String> atributosPesquisa = new HashMap<String, String>();
	    
	    if (tiposDocumentos != null && !tiposDocumentos.trim().isEmpty()){
		atributosPesquisa.put("Tipos de documentos", tiposDocumentos.trim());
	    }
	    
	    if(empresa != null && !empresa.trim().isEmpty()){
		atributosPesquisa.put("Empresa", "%"+empresa.trim()+"%");
	    }
	    
	    if(competencia != null && !competencia.trim().isEmpty()){
		atributosPesquisa.put("Competência", competencia.trim());
	    }
	    

	    	    
	    E2docFinanceiroEngine e2docEngine = new E2docFinanceiroEngine();
	    
	    Pastas pastas = e2docEngine.pesquisaFinanceiro(atributosPesquisa);
	    	    
	    return pastas;

	}
	
	@POST
	@Path("getTiposDocumentos")
	@Produces("application/json")
	public List<String> getTiposDocumentos() {
	    
	    E2docFinanceiroEngine e2docEngine = new E2docFinanceiroEngine();
	    
	    return e2docEngine.getTiposDocumentos();
	    

	}
	
	@POST
	@Path("getEmpresas")
	@Produces("application/json")
	public List<String> getEmpresas() {
	    
	    E2docFinanceiroEngine e2docEngine = new E2docFinanceiroEngine();
	    
	    return e2docEngine.getEmpresas();
	    

	}

}
