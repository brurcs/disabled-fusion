package com.neomind.fusion.custom.orsegups.ql.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;

import com.google.gson.Gson;
import com.neomind.fusion.custom.orsegups.presenca.vo.DocumentoTreeVO;
import com.neomind.fusion.custom.orsegups.utils.QlPresencaArquivoUtils;

@WebServlet(name="QlArquivoServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlArquivoServlet"})
public class QlArquivoServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	EntityManager entityManager;
	EntityTransaction transaction;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	private void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		try
		{
			response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");

			final PrintWriter out = response.getWriter();
			

			String action = "";

			if (request.getParameter("action") != null)
			{
				action = request.getParameter("action");
			}

			if (action.equalsIgnoreCase("listaDocumentosArqiuivo"))
			{
				String numcpf = request.getParameter("cpf");
				String numCad = request.getParameter("numCad");
				String numEmp = request.getParameter("numemp");
				this.listaDocumentosArquivos(numcpf, numCad,numEmp, response);
			}
			out.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void listaDocumentosArquivos(String numcpf, String numCad, String numEmp, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<DocumentoTreeVO> documentos = new ArrayList<DocumentoTreeVO>();
		
		System.out.println("Passou 01");
		
		documentos = QlPresencaArquivoUtils.listaDocumentoTreeArquivo(Long.parseLong(numcpf));
		
		Gson gson = new Gson();
		String jsonDocumentos = gson.toJson(documentos);
		
		out.print(jsonDocumentos);
		out.flush();
		out.close();
	}
}
