package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.File;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.RelatoriosServices;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoUtils;

public class RotinaEmailEventosFiscalSupervisor implements CustomJobAdapter
{

	private static final Log log = LogFactory.getLog(RotinaEmailEventosFiscalSupervisor.class);

	@SuppressWarnings("unchecked")
	@Override
	public void execute(CustomJobContext ctx)
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		try
		{
			Map<String, Object> paramMap = new HashMap<String, Object>();
			String destinatarios = "carlos.faraco@orsegups.com.br;marcos.beckman@orsegups.com.br;supervisor.at.base@orsegups.com.br;rota.ilha@orsegups.com.br;rota.continente@orsegups.com.br;emailautomatico@orsegups.com.br";

			if (!destinatarios.isEmpty())
			{
				File arqRelatorio = RelatoriosServices.gerarRelatorioEventosFiscalSupervisor();
				if (arqRelatorio != null)
				{
					NeoStorage neoStorage = NeoStorage.getDefault();
					NeoFile neoFile = neoStorage.copy(arqRelatorio);
					neoFile.setName("Relatorio_Eventos_Fiscal_Supervisor.pdf");
					List<String> finale = new ArrayList<String>();
					finale.add(((Long) neoFile.getNeoId()).toString());

					paramMap.put("attachList", finale);

					FusionRuntime.getInstance().getMailEngine().sendEMail(destinatarios, "/portal_orsegups/emailRelatorioEventosFiscalSupervisor.jsp", paramMap);

					log.warn("LOG RELATÓRIO DE EVENTOS FISCAL/SUPERVISOR - Destinatários: " + destinatarios);
				}
			}

		}
		catch (Exception ex)
		{
			
			log.error(" ERRO RELATÓRIO DE EVENTOS FISCAL/SUPERVISOR "+ex.getMessage());
			System.out.println("[" + key + "] ERRO RELATÓRIO DE EVENTOS FISCAL/SUPERVISOR "+ex.getMessage());
			ex.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		}
	}
}
