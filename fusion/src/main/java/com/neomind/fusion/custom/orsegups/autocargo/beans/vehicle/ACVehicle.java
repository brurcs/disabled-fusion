package com.neomind.fusion.custom.orsegups.autocargo.beans.vehicle;

public class ACVehicle {
    
    private int client_id;
    private String client_name;
    private String registration_plate;
    private String description;
    private String brand;
    private String model;
    private ACTrackingModule tracking_module;
    private String last_state_timestamp;
    private ACMiscommunicationDuration miscommunication_duration;
    private String location;
    
    public int getClient_id() {
        return client_id;
    }
    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }
    public String getClient_name() {
        return client_name;
    }
    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }
    public String getRegistration_plate() {
        return registration_plate;
    }
    public void setRegistration_plate(String registration_plate) {
        this.registration_plate = registration_plate;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getBrand() {
        return brand;
    }
    public void setBrand(String brand) {
        this.brand = brand;
    }
    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }
    public ACTrackingModule getTracking_module() {
        return tracking_module;
    }
    public void setTracking_module(ACTrackingModule tracking_module) {
        this.tracking_module = tracking_module;
    }
    public String getLast_state_timestamp() {
        return last_state_timestamp;
    }
    public void setLast_state_timestamp(String last_state_timestamp) {
        this.last_state_timestamp = last_state_timestamp;
    }
    public ACMiscommunicationDuration getMiscommunication_duration() {
        return miscommunication_duration;
    }
    public void setMiscommunication_duration(ACMiscommunicationDuration miscommunication_duration) {
        this.miscommunication_duration = miscommunication_duration;
    }
    public String getLocation() {
        return location;
    }
    public void setLocation(String location) {
        this.location = location;
    }
    
    
}
