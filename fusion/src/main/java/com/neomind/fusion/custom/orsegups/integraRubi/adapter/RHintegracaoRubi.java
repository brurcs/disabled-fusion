package com.neomind.fusion.custom.orsegups.integraRubi.adapter;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.event.ActivityEvent;
import com.neomind.fusion.workflow.event.ActivityStartEventListener;
import com.neomind.fusion.workflow.event.TaskAssignerEvent;
import com.neomind.fusion.workflow.event.TaskAssignerEventListener;
import com.neomind.fusion.workflow.event.TaskTransferEvent;
import com.neomind.fusion.workflow.event.TaskTransferEventListener;
import com.neomind.fusion.workflow.exception.ActivityException;
import com.neomind.fusion.workflow.exception.TaskException;

public class RHintegracaoRubi implements AdapterInterface, ActivityStartEventListener, TaskAssignerEventListener, TaskTransferEventListener
{
	private static final Log log = LogFactory.getLog(RHintegracaoRubi.class);

	public void start(Task origin, EntityWrapper processEntity, Activity activity) //throws RemoteException, ServiceException
	{

		String validade = RHIntegracaoAdmissaoUtils.validarNegocio(processEntity);

		/*
		 * if (validade != null)
		 * {
		 * throw new WorkflowException(validade);
		 * }
		 */

		String erro = integrar(origin, processEntity, activity);
		if (erro == null || erro.trim().length() == 0)
		{
			processEntity.findField("validaFoto").setValue(Boolean.TRUE);
			processEntity.findField("errosIntegracao").setValue("");
			processEntity.findField("contErros").setValue(0l);
			processEntity.findField("validaIntegracao").setValue(true);
		}
		else
		{
			processEntity.setValue("errosIntegracao", erro);
			processEntity.setValue("contErros", 1l);
			processEntity.setValue("exec2", origin.getActivity().getInstance().getOwner());
			processEntity.findField("validaIntegracao").setValue(false);
		}
	}

	private String integrar(Task origin, EntityWrapper pEntity, Activity activity)
	{
		String erroGeral = "";
		
		String codeUsu = origin.getActivity().getInstance().getOwner().getCode();
		String chaveCodigo = activity.getProcess().getCode();
		Long numemp = pEntity.findGenericValue("emp.numemp"); //Número Empresa
		Long tipcol = pEntity.findGenericValue("tpColaborador.codigo"); //Tipo de Colaborador
		pEntity.findField("apefun").setValue(RHIntegracaoAdmissaoUtils.getApFun(chaveCodigo));

		//CADASTRO DE DADOS BÁSICOS
		String erroCadastroBasico = RHIntegracaoAdmissaoUtils.efetivaCadastroBase(pEntity, codeUsu, chaveCodigo, numemp);

		if (RHIntegracaoAdmissaoUtils.ocorreuErro(erroCadastroBasico))
			return erroCadastroBasico;

		Integer numcad = RHIntegracaoBanco.resgataNumCad(RHIntegracaoAdmissaoUtils.getApFun(chaveCodigo));

		//CADASTRO COMPLEMENTAR
		String erroCadastroComplementar = RHIntegracaoAdmissaoUtils.efetivarCadastroComplementar(pEntity, chaveCodigo, numcad);

		if (RHIntegracaoAdmissaoUtils.ocorreuErro(erroCadastroComplementar))
			return erroCadastroComplementar;

		//CADASTRO ADICIONAL
		String erroCadastroAdicional = RHIntegracaoAdmissaoUtils.efetivarIntegracaoAdicional(pEntity, chaveCodigo, numcad);

		if (RHIntegracaoAdmissaoUtils.ocorreuErro(erroCadastroAdicional))
			return erroCadastroAdicional;

		//CADASTRO DE DEPENDENTES
		String errocadastrarDependentes = RHIntegracaoAdmissaoUtils.efetivarCadastroDependentes(pEntity, chaveCodigo, numemp, tipcol, numcad);

		if (RHIntegracaoAdmissaoUtils.ocorreuErro(errocadastrarDependentes))
		{
			return errocadastrarDependentes;
		}

		//CADASTRO DE USUARIOS DA SEDE
		if (!RHIntegracaoAdmissaoUtils.isSede(pEntity))
		{
			String StcodFic = pEntity.findGenericValue("documentacao.fichamedica");
			if (StcodFic == null)
			{
				return "ERRO NA INTEGRAÇÃO - Ficha médica não cadastrada";
			}

			String errocadastroASO = RHIntegracaoAdmissaoUtils.cadastrarASO(pEntity, chaveCodigo, numemp);

			if (RHIntegracaoAdmissaoUtils.ocorreuErro(errocadastroASO))
				return errocadastroASO;

			String erroexame = RHIntegracaoAdmissaoUtils.cadastraExame(pEntity, chaveCodigo, numemp);

			if (RHIntegracaoAdmissaoUtils.ocorreuErro(erroexame))
				return erroexame;
		}

		return null;

	}

	public static void insereLogInfo(String texto)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYY hh:mm:ss");
		log.info(texto + " - Data: " + sdf.format(new GregorianCalendar().getTime()));
	}

	@Override
	public void onTransfer(TaskTransferEvent event)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onAssign(TaskAssignerEvent arg0) throws TaskException
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onStart(ActivityEvent arg0) throws ActivityException
	{
		// TODO Auto-generated method stub

	}

}