package com.neomind.fusion.custom.orsegups.mobile.vo;

public class ReinspecaoVO {
	
	private String codposto;
	private String nomeempresa;
	private String nomecliente;
	private String nomelotacao;
	private String nomeposto;
	private String gestorcliente;
	private String colaboradorentrevistado;
	private String funccolaborador;
	private String nomeinspetor;
	private String datainspecao;
	private String descpesquisa;
	private String Observacao;
	private String tipo_inspecao;
	private String cod_task;
	private String mobile_neoId;
	private String cod_postoTrabalho;
	private String nome_empresa;
	private String nome_cliente;
	private String nome_lotacao;
	private String nome_postoTrabalho;
	private String gestor_cliente;
	private String nome_colaborador;
	private String funcao_colaborador;
	private String nome_inspetor;
	private String data_inpesao;
	private String descrisao_pesquisa;
	private String observacao;
	private String desc_perg;
	private String resp_perg;
	private String logical;
	private String code_perg;
	
	public String getCodposto() {
		return codposto;
	}
	public void setCodposto(String codposto) {
		this.codposto = codposto;
	}
	public String getNomeempresa() {
		return nomeempresa;
	}
	public void setNomeempresa(String nomeempresa) {
		this.nomeempresa = nomeempresa;
	}
	public String getNomecliente() {
		return nomecliente;
	}
	public void setNomecliente(String nomecliente) {
		this.nomecliente = nomecliente;
	}
	public String getNomelotacao() {
		return nomelotacao;
	}
	public void setNomelotacao(String nomelotacao) {
		this.nomelotacao = nomelotacao;
	}
	public String getNomeposto() {
		return nomeposto;
	}
	public void setNomeposto(String nomeposto) {
		this.nomeposto = nomeposto;
	}
	public String getGestorcliente() {
		return gestorcliente;
	}
	public void setGestorcliente(String gestorcliente) {
		this.gestorcliente = gestorcliente;
	}
	public String getColaboradorentrevistado() {
		return colaboradorentrevistado;
	}
	public void setColaboradorentrevistado(String colaboradorentrevistado) {
		this.colaboradorentrevistado = colaboradorentrevistado;
	}
	public String getFunccolaborador() {
		return funccolaborador;
	}
	public void setFunccolaborador(String funccolaborador) {
		this.funccolaborador = funccolaborador;
	}
	public String getNomeinspetor() {
		return nomeinspetor;
	}
	public void setNomeinspetor(String nomeinspetor) {
		this.nomeinspetor = nomeinspetor;
	}
	public String getDatainspecao() {
		return datainspecao;
	}
	public void setDatainspecao(String datainspecao) {
		this.datainspecao = datainspecao;
	}
	public String getDescpesquisa() {
		return descpesquisa;
	}
	public void setDescpesquisa(String descpesquisa) {
		this.descpesquisa = descpesquisa;
	}
	
	public String getTipo_inspecao() {
		return tipo_inspecao;
	}
	public void setTipo_inspecao(String tipo_inspecao) {
		this.tipo_inspecao = tipo_inspecao;
	}
	public String getCod_task() {
		return cod_task;
	}
	public void setCod_task(String l) {
		this.cod_task = l;
	}
	public String getMobile_neoId() {
		return mobile_neoId;
	}
	public void setMobile_neoId(String mobile_neoId) {
		this.mobile_neoId = mobile_neoId;
	}
	public String getCod_postoTrabalho() {
		return cod_postoTrabalho;
	}
	public void setCod_postoTrabalho(String cod_postoTrabalho) {
		this.cod_postoTrabalho = cod_postoTrabalho;
	}
	public String getNome_empresa() {
		return nome_empresa;
	}
	public void setNome_empresa(String nome_empresa) {
		this.nome_empresa = nome_empresa;
	}
	public String getNome_cliente() {
		return nome_cliente;
	}
	public void setNome_cliente(String nome_cliente) {
		this.nome_cliente = nome_cliente;
	}
	public String getNome_lotacao() {
		return nome_lotacao;
	}
	public void setNome_lotacao(String nome_lotacao) {
		this.nome_lotacao = nome_lotacao;
	}
	public String getNome_postoTrabalho() {
		return nome_postoTrabalho;
	}
	public void setNome_postoTrabalho(String nome_postoTrabalho) {
		this.nome_postoTrabalho = nome_postoTrabalho;
	}
	public String getGestor_cliente() {
		return gestor_cliente;
	}
	public void setGestor_cliente(String gestor_cliente) {
		this.gestor_cliente = gestor_cliente;
	}
	public String getNome_colaborador() {
		return nome_colaborador;
	}
	public void setNome_colaborador(String nome_colaborador) {
		this.nome_colaborador = nome_colaborador;
	}
	public String getFuncao_colaborador() {
		return funcao_colaborador;
	}
	public void setFuncao_colaborador(String funcao_colaborador) {
		this.funcao_colaborador = funcao_colaborador;
	}
	public String getNome_inspetor() {
		return nome_inspetor;
	}
	public void setNome_inspetor(String nome_inspetor) {
		this.nome_inspetor = nome_inspetor;
	}
	public String getData_inpesao() {
		return data_inpesao;
	}
	public void setData_inpesao(String data_inpesao) {
		this.data_inpesao = data_inpesao;
	}
	public String getDescrisao_pesquisa() {
		return descrisao_pesquisa;
	}
	public void setDescrisao_pesquisa(String descrisao_pesquisa) {
		this.descrisao_pesquisa = descrisao_pesquisa;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getDesc_perg() {
		return desc_perg;
	}
	public void setDesc_perg(String desc_perg) {
		this.desc_perg = desc_perg;
	}
	public String getResp_perg() {
		return resp_perg;
	}
	public void setResp_perg(String resp_perg) {
		this.resp_perg = resp_perg;
	}
	public String getLogical() {
		return logical;
	}
	public void setLogical(String logical) {
		this.logical = logical;
	}
	public String getCode_perg() {
		return code_perg;
	}
	public void setCode_perg(String code_perg) {
		this.code_perg = code_perg;
	}

}
