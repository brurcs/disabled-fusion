package com.neomind.fusion.custom.orsegups.converter;

import java.util.HashMap;

import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.portal.FreeMarkerUtils;

public class OrsegupsMascaraData extends StringConverter {

    @Override
	public String getHTMLInput(final EFormField field, final OriginEnum origin)
	{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("fieldID", field.getFieldId());
		String customHTML = FreeMarkerUtils.parseTemplate(map, "custom/orsegups/formatData.html.ftl");
		return super.getHTMLInput(field, origin) + customHTML;
	}	
}

