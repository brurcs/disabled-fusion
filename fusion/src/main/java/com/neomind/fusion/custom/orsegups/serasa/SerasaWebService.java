package com.neomind.fusion.custom.orsegups.serasa;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.neomind.fusion.workflow.exception.WorkflowException;

public class SerasaWebService {
	
	private String EMAIL = "";
	private String SENHA = "";
	private String DOCUMENTO = "";
	private static String METODO_ConsultaConcentreSERASA = "ConsultaConcentreSERASA";
	private static String URLTESTE 		= "http://www.consultacpf.com/webservices/test-drive/consultacpf.asmx/";
	private static String URLPRODUCAO 	= "http://www.consultacpf.com/webservices/producao/consultacpf.asmx";
	
	public SerasaWebService(String email, String senha, String documento)
	{
		this.setEMAIL(email);
		this.setSENHA(senha);
		this.setDOCUMENTO(documento);
	}

	public String getEMAIL() {
		return EMAIL;
	}

	public void setEMAIL(String eMAIL) {
		EMAIL = eMAIL;
	}

	public String getSENHA() {
		return SENHA;
	}

	public void setSENHA(String sENHA) {
		SENHA = sENHA;
	}

	public String getDOCUMENTO() {
		return DOCUMENTO;
	}

	public void setDOCUMENTO(String cONTA) {
		DOCUMENTO = cONTA;
	}

	public static String getURLTESTE() {
		return URLTESTE;
	}

	public static void setURLTESTE(String uRLTESTE) {
		URLTESTE = uRLTESTE;
	}

	public static String getURLPRODUCAO() {
		return URLPRODUCAO;
	}

	public static void setURLPRODUCAO(String uRLPRODUCAO) {
		URLPRODUCAO = uRLPRODUCAO;
	}
	
	
	
	@SuppressWarnings("deprecation")
	public static void verificaCNPJTeste(String cpfcnpj)
	{
		TrustManager[] trms = new TrustManager[]{new MyTrustManager(null)};  
        SSLContext ssl;  
          
        try{  
            ssl = SSLContext.getInstance("TLS");  
            ssl.init(null, trms, null);  
            SSLContext.setDefault(ssl);  
        } catch (NoSuchAlgorithmException e) {  
            e.printStackTrace();  
        } catch (KeyManagementException e) {  
            e.printStackTrace();  
        }
		try
		{
			
			/*String login = "57693410";
			String senha = "40302010";*/
//			String login = "67038306";
//			String senha = "832777";
			
			String login = "67038325";
			String senha = "ORS10203";
			
			
			//RegistroB49C regB49C = new RegistroB49C("B49C","",cnpj,"F","C","FI","","","S","99","S","INI","A","N","","","","","","N","","","");
			//regB49C.setNUM_DOC3("000004749001937");
			//regB49C.setNUM_DOC3(cnpj);
			//regB49C.setTIPO_PESSOA4("F");
			//regB49C.setCONVERSA11("S");
			//regB49C.setFUNCAO12("INI");
			//regB49C.setQTD_REG10("01");
			
			
			//RegistroP002 regP002 = new RegistroP002("P002","RSPU","","","","","","","","");
			//regP002.setCOD1("RSPU");
			
			//  1     2   3   4   5   6   7   8  9  10  11  12  13  14  15  16  17 18 19 20 21 22  23  24  25 26  27  28
			//  RegistroI001  regI001 = new RegistroI001("I001","00","D","S","N","N","N","N","N","N","N","N","N","N","N","N","","","","","","","N","N","N","","","");
			//RegistroI001  regI001 = new RegistroI001("I001","00","D","S","N","N","N","N","N","N","N","N","N","N","N","N","","","","","","","N","N","N","","","");
			
			RegistroSerasa r = RegistroSerasaFactory.getReg("B49C");
			r.setupValores("B49C","",cpfcnpj,"F","C","FI","","","S","99","S","INI","A","N","","","","","","N","","","");
			RegistroSerasa regP002 = RegistroSerasaFactory.getReg("P002"); 
			regP002.setupValores("P002","RSPU","","","","","","","","");
			RegistroSerasa  regI001 = RegistroSerasaFactory.getReg("I001");
			regI001.setupValores("I001","00","D","S","N","N","N","N","N","N","N","N","N","N","N","N","","","","","","","N","N","N","","","");
			RegistroSerasa regT999 = RegistroSerasaFactory.getReg("T999");
			regT999.setupValores("T999","","","");
			
			String content2 = SerasaUtils.enviaConsultaSerasa(login, senha, r.parseToString()
																			+regP002.parseToString()
																			+regI001.parseToString()
																			+regT999.parseToString()
							,true);
			
			StringBuilder resultado = new StringBuilder();
			resultado.append( cpfcnpj + "  \n" );
			resultado.append(content2+"\n");
			resultado.append("Criticas: \n");
			System.out.println("[FLUXO CONTRATOS]-Resultado: "+content2);
			if (content2.contains("B49C") ){
				if (content2.contains("A900")){
					if (content2.contains("NADA CONSTA")){
						resultado.append("Nada Consta \n");
					}else{
						resultado.append("A900 \n");
					}
				}
				if (content2.contains("B900")){
					
					System.out.println("[FLUXO CONTRATOS]- Erro Serasa " + content2.substring(content2.indexOf("B900"),content2.indexOf("B900")+115));
				}
				if (content2.contains("I10000")){
					// 
					//resultado.append("I100-00 ");
				}
				if (content2.contains("I10099")){
					// 
					//resultado.append("I100-99 ");
				}
				if (content2.contains("I101")){
					// 
					//resultado.append("I101 ");
				}
				if (content2.contains("I105")){
					// Grafias
					resultado.append("Variacoes de grafia para o documento consultado. \n");
				}
				if (content2.contains("I110")){ // 0..n
					
					resultado.append("Possui protestos. \n");
				}
				if (content2.contains("I12000") || content2.contains("I12001") || content2.contains("I12002") || content2.contains("I12003") ){
					resultado.append("Possui acoes judiciais. \n");
				}
				if (content2.contains("I14000") || content2.contains("I14001") || content2.contains("I14002") || content2.contains("I14003") ){
					resultado.append("REFIN (Restricao Financeira). \n");
				}
				if (content2.contains("I16000") || content2.contains("I16001") || content2.contains("I16002") ){
					resultado.append("Cheques sem fundos. \n");
				}
				if (content2.contains("I17000") || content2.contains("I17001") || content2.contains("I17002") ){
					resultado.append("Cheques sem fundos BB . \n");
				}
				if (content2.contains("I22000") || content2.contains("I22001") || content2.contains("I22002") || content2.contains("I22003")){
					resultado.append("PEFIN (Pendencia Financeira). \n");
				}
				
			}else{
				throw new Exception("Erro ao receber retorno do webservice do serasa");
			}
			System.out.println("[FLUXO CONTRATOS]- Retorno Serasa: " + resultado.toString() + "\n---------------");
			
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
		
	public static void main(String args[]){
		
		verificaCNPJTeste("7433981000100");
		/*
		 * CPFS de teste para homologacao 
		verificaCNPJTeste("00002047810");
		
		verificaCNPJTeste("00423343815");
		verificaCNPJTeste("00611034891");
		verificaCNPJTeste("05759917803");
		verificaCNPJTeste("02701580730");
		verificaCNPJTeste("29714354668");
		verificaCNPJTeste("00027562000");
		verificaCNPJTeste("00002364115");
		verificaCNPJTeste("10441918808");
		verificaCNPJTeste("99234793587");
		verificaCNPJTeste("15331928803");
		verificaCNPJTeste("05108701802");
		verificaCNPJTeste("00020223544");
		verificaCNPJTeste("00020223625");
		verificaCNPJTeste("11456458876");
		verificaCNPJTeste("01107156750");
		verificaCNPJTeste("80926835815");
		verificaCNPJTeste("28189620134");
		verificaCNPJTeste("29870348904");
		verificaCNPJTeste("22434070191");
		verificaCNPJTeste("69720010568");
		verificaCNPJTeste("97722694578");
		verificaCNPJTeste("00314927972");
		verificaCNPJTeste("16942931578");
		verificaCNPJTeste("19307762848");
		verificaCNPJTeste("56983479130");
		verificaCNPJTeste("05708855827");
		verificaCNPJTeste("99868989868");
		verificaCNPJTeste("00130082775");
		verificaCNPJTeste("10695052420");
		verificaCNPJTeste("13924338272");
		verificaCNPJTeste("00008889074");
		verificaCNPJTeste("00000000191");
		verificaCNPJTeste("00000000272");
		verificaCNPJTeste("00000000353");
		verificaCNPJTeste("00000000434");
		verificaCNPJTeste("00000000604");
		verificaCNPJTeste("00000000787");
		verificaCNPJTeste("00000000868");
		verificaCNPJTeste("00000000949");
		verificaCNPJTeste("00000001163");
		verificaCNPJTeste("00000001244");
		verificaCNPJTeste("00000001325");
		verificaCNPJTeste("00000001406");
		verificaCNPJTeste("00000001597");
		verificaCNPJTeste("00000001678");
		verificaCNPJTeste("00000001759");
		verificaCNPJTeste("00000001830");
		verificaCNPJTeste("00000001910");
		verificaCNPJTeste("00000002054");
		verificaCNPJTeste("00000002135");
		verificaCNPJTeste("00000002216");
		verificaCNPJTeste("00000002305");
		verificaCNPJTeste("00000002488");
		verificaCNPJTeste("00000002569");*/
	}
}
