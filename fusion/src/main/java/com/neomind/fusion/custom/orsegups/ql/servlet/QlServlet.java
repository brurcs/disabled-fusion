package com.neomind.fusion.custom.orsegups.ql.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.HtmlEmail;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.senior.services.CalculoApuracaoAcertarIn;
import br.com.senior.services.CalculoApuracaoAcertarInTMarDat;
import br.com.senior.services.CalculoApuracaoAcertarOut;
import br.com.senior.services.CalculoApuracaoCalcularIn;
import br.com.senior.services.CalculoApuracaoCalcularOut;
import br.com.senior.services.Ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPx;

import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.presenca.PresencaUtil;
import com.neomind.fusion.custom.orsegups.presenca.vo.AcessoListaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.AcessoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.AfastamentoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.AnotacaoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ApuracaoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ArmaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ChecklistVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ChipGPRSVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ColaboradorVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ContaSigmaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ContaSigmaVinculoHumanaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ContraChequeVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.DocumentoTreeVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EntradaAutorizadaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EquipamentoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EscalaPostoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EscalaRevezamentoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EscalaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EventoSigmaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.FichaEpiVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.FinancasTituloVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.HistoricoCargoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.HistoricoEscalaPostoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.HistoricoEscalaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.HistoricoLocalVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.InspecaoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.JustificativaMarcacaoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.MensagemTIVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ObsHoristaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.PostoSupervisaoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.PostoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.RTOciosoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ReclamacaoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.SolicitacaoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.TarefaFaltaEfetivoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ValeTransporteVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.VisitaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.X8VO;
import com.neomind.fusion.custom.orsegups.ql.QLAdapter;
import com.neomind.fusion.custom.orsegups.site.vo.CompetenciaVO;
import com.neomind.fusion.custom.orsegups.utils.CoberturaComparator;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.engine.runtime.RuntimeEngine;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLField;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

@WebServlet(name = "QlServlet", urlPatterns = { "/servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet" })
public class QlServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	private static final Log log = LogFactory.getLog(QlServlet.class);
	EntityManager entityManager;
	EntityTransaction transaction;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	private void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		try
		{
			response.setContentType("text/plain");
			response.setCharacterEncoding("ISO-8859-1");

			final PrintWriter out = response.getWriter();

			String action = "";

			if (request.getParameter("action") != null)
			{
				action = request.getParameter("action");
				System.out.println("Action qlservlet:" + action);
			}

			if (action.equalsIgnoreCase("listaAfastamentosJ003"))
			{
				String dadosCol = request.getParameter("dadosCol");
				this.retornaAfastamentos(dadosCol, response);
			}

			if (action.equalsIgnoreCase("historicoMedicoJ003"))
			{
				String crmMedico = request.getParameter("crmMedico");
				this.retornaHistoricoCRMMedico(crmMedico, response);
			}

			if (action.equalsIgnoreCase("saveLog"))
			{
				Gson gson = new Gson();
				String texto = gson.fromJson(request.getParameter("texto"), String.class);
				String idposto = request.getParameter("idPosto");
				String idColaborador = request.getParameter("idColaborador");
				String tipo = request.getParameter("tipo");
				this.saveLog(texto, idposto, idColaborador, tipo, out);
			}

			if (action.equalsIgnoreCase("deleteRegistroCobertura"))
			{
				String numEmp = request.getParameter("numEmp");
				String tipCol = request.getParameter("tipCol");
				String numCad = request.getParameter("numCad");
				String dataIni = request.getParameter("dataInicial");
				String horaIni = request.getParameter("horaInicial");
				String numEmpCob = request.getParameter("numEmpCob");
				String tipColCob = request.getParameter("tipColCob");
				String numCadCob = request.getParameter("numCadCob");
				String nextid = request.getParameter("nextid");

				this.deleteRegistroCobertura(numEmp, tipCol, numCad, dataIni, horaIni, numEmpCob, tipColCob, numCadCob, true, "", "", "", "", nextid, request, response);
			}

			if (action.equalsIgnoreCase("insertEventoForcado"))
			{
				String numcra = request.getParameter("numcra");
				String numemp = request.getParameter("numemp");
				String tipcol = request.getParameter("tipcol");
				String numcad = request.getParameter("numcad");
				String numloc = request.getParameter("numloc");
				String taborg = request.getParameter("taborg");
				String diracc = request.getParameter("diracc");
				String datacc = request.getParameter("datacc");
				String horacc = request.getParameter("horacc");
				String minacc = request.getParameter("minacc");
				String tipdir = request.getParameter("tipdir");
				String codjma = request.getParameter("codjma");
				String obsjma = request.getParameter("obsjma");

				this.insertEventoForcado(numcra, numemp, tipcol, numcad, numloc, taborg, diracc, datacc, horacc, minacc, tipdir, codjma, obsjma, request, response);
			}

			if (action.equalsIgnoreCase("insertSituacaoIndefinida"))
			{
				String empresa = request.getParameter("numemp");
				String tipoColaborador = request.getParameter("tipcol");
				String numeroCadastro = request.getParameter("numcad");
				String dataAlteracao = request.getParameter("datafa");
				String escala = request.getParameter("codesc");
				String turma = request.getParameter("codtma");
				String horaInicial = request.getParameter("horini");

				this.insertSituacaoIndefinida(empresa, tipoColaborador, numeroCadastro, dataAlteracao, escala, turma, horaInicial, request, response);
			}

			if (action.equalsIgnoreCase("visualizaImagem"))
			{
				Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();

				String numemp = request.getParameter("numemp");
				String tipcol = request.getParameter("tipcol");
				String numcad = request.getParameter("numcad");

				this.visualizaImagem(numemp, tipcol, numcad, request, response);
				log.warn("visualizaImagem - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
			}

			if (action.equalsIgnoreCase("consultaCobertura"))
			{
				String colaborador = request.getParameter("colaborador");
				String colaboradorSub = request.getParameter("colaboradorSub");
				String posto = request.getParameter("posto");
				String dataInicialDe = request.getParameter("dataInicialDe");
				String dataInicialAte = request.getParameter("dataInicialAte");
				String dataFinalDe = request.getParameter("dataFinalDe");
				String dataFinalAte = request.getParameter("dataFinalAte");

				String codColab = "";
				String codColabSub = "";
				String sNumLoc = "";
				String sTabOrg = "";
				Integer numLoc;
				Short tabOrg;

				if (colaborador != null && !colaborador.isEmpty())
				{
					codColab = colaborador.substring(0, colaborador.indexOf("-")).trim();
				}
				if (colaboradorSub != null && !colaboradorSub.isEmpty())
				{
					codColabSub = colaboradorSub.substring(0, colaboradorSub.indexOf("-")).trim();
				}
				if (posto != null && !posto.isEmpty())
				{
					this.entityManager = PersistEngine.getEntityManager("VETORH");
					//Verifica histórico de colaborador

					String postoCcu = posto.substring(posto.lastIndexOf("-") + 1, posto.length());

					String sqlSearchPosto = " Select numloc, taborg From USU_VORG203NV8 " + "   Where usu_codccu =:postoCcu";

					Query querySqlSearchPosto = this.entityManager.createNativeQuery(sqlSearchPosto);

					querySqlSearchPosto.setParameter("postoCcu", postoCcu.trim());

					Object resultPosto = querySqlSearchPosto.getSingleResult();
					if (NeoUtils.safeIsNotNull(resultPosto))
					{
						Object[] resultado = (Object[]) resultPosto;
						numLoc = (Integer) resultado[0];
						tabOrg = (Short) resultado[1];

						sNumLoc = numLoc.toString();
						sTabOrg = tabOrg.toString();
					}

				}

				this.consultaCoberturas(codColab, codColabSub, sNumLoc, sTabOrg, dataInicialDe, dataInicialAte, dataFinalDe, dataFinalAte, request, response);
			}

			if (action.equalsIgnoreCase("listaObsHorista"))
			{
				String numemp = request.getParameter("numemp");
				String tipcol = request.getParameter("tipcol");
				String numcad = request.getParameter("numcad");
				String numcpf = request.getParameter("numcpf");
				this.listaObsHorista(numemp, tipcol, numcad, numcpf, response);
			}

			if (action.equalsIgnoreCase("salvaObsHorista"))
			{
				String numcad = request.getParameter("numcad");
				String numemp = request.getParameter("numemp");
				String tipcol = request.getParameter("tipcol");
				String obsHor = request.getParameter("obsHorista");
				String numCpf = request.getParameter("numcpf");

				this.salvaObsHorista(numcad, numemp, tipcol, obsHor, numCpf, response);
			}

			if (action.equalsIgnoreCase("removerObsHorista"))
			{
				String numcad = request.getParameter("numcad");
				String numemp = request.getParameter("numemp");
				String tipcol = request.getParameter("tipcol");
				String neoId = request.getParameter("neoId");
				String numCpf = request.getParameter("numcpf");

				this.removerObsHorista(numcad, numemp, tipcol, neoId, numCpf, response);
			}

			if (action.equalsIgnoreCase("listaAnotacoes"))
			{
				String numemp = request.getParameter("numemp");
				String tipcol = request.getParameter("tipcol");
				String numcad = request.getParameter("numcad");
				this.listaAnotacoes(numemp, tipcol, numcad, response);
			}

			if (action.equalsIgnoreCase("listaCoberturas"))
			{
				String numemp = request.getParameter("numemp");
				String tipcol = request.getParameter("tipcol");
				String numcad = request.getParameter("numcad");
				this.listaCoberturas(numemp, tipcol, numcad, response);
			}

			if (action.equalsIgnoreCase("listaDocumentos"))
			{
				String numcpf = request.getParameter("cpf");
				this.listaDocumentos(numcpf, response);
			}

			if (action.equalsIgnoreCase("listaHistoricoLocal"))
			{
				String numemp = request.getParameter("numemp");
				String tipcol = request.getParameter("tipcol");
				String numcad = request.getParameter("numcad");
				this.listaHistoricoLocal(numemp, tipcol, numcad, response);
			}

			if (action.equalsIgnoreCase("listaHistoricoCargo"))
			{
				String numemp = request.getParameter("numemp");
				String tipcol = request.getParameter("tipcol");
				String numcad = request.getParameter("numcad");
				this.listaHistoricoCargo(numemp, tipcol, numcad, response);
			}

			if (action.equalsIgnoreCase("listaHistoricoEscala"))
			{
				String numemp = request.getParameter("numemp");
				String tipcol = request.getParameter("tipcol");
				String numcad = request.getParameter("numcad");
				this.listaHistoricoEscala(numemp, tipcol, numcad, response);
			}

			if (action.equalsIgnoreCase("listaCompetencias"))
			{
				String numcpf = request.getParameter("numcpf");
				this.listaCompetencias(numcpf, response);
			}

			if (action.equalsIgnoreCase("listaCompetenciasApuracao"))
			{
				String numemp = request.getParameter("numemp");
				String tipcol = request.getParameter("tipcol");
				String numcad = request.getParameter("numcad");
				this.listaCompetenciasApuracao(numemp, tipcol, numcad, response);
			}

			if (action.equalsIgnoreCase("listaApuracoes"))
			{
				String numemp = request.getParameter("numemp");
				String tipcol = request.getParameter("tipcol");
				String numcad = request.getParameter("numcad");
				String competencia = request.getParameter("cpt");
				this.listaApuracoes(numemp, tipcol, numcad, competencia, response);
			}

			if (action.equalsIgnoreCase("listaAcessos"))
			{
				String numcpf = request.getParameter("numcpf");
				String competencia = request.getParameter("cpt");
				String exibirExcecao = request.getParameter("exibirExcecao");
				this.listaAcessos(numcpf, competencia, exibirExcecao, response);
			}

			if (action.equalsIgnoreCase("listaCompetenciasFinancas"))
			{
				String numcpf = request.getParameter("numcpf");
				this.listaCompetenciasFinancas(numcpf, response);
			}

			if (action.equalsIgnoreCase("buscaContraCheque"))
			{
				String numcpf = request.getParameter("numcpf");
				String competencia = request.getParameter("cpt");
				this.buscaContraCheque(numcpf, competencia, response);
			}

			if (action.equalsIgnoreCase("pieChartSituacoes"))
			{
				String numemp = request.getParameter("numemp");
				String tipcol = request.getParameter("tipcol");
				String numcad = request.getParameter("numcad");
				String competencia = request.getParameter("cpt");
				this.pieChartSituacoes(numemp, tipcol, numcad, competencia, response);
			}

			/*
			 * if (action.equalsIgnoreCase("listaSituacaoCompetencia"))
			 * {
			 * Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();
			 * String numemp = request.getParameter("numemp");
			 * String tipcol = request.getParameter("tipcol");
			 * String numcad = request.getParameter("numcad");
			 * //this.listaSituacaoCompetencia(numemp, tipcol, numcad, response);
			 * log.warn("listaSituacaoCompetencia - Time spent: " +
			 * (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
			 * }
			 */

			if (action.equalsIgnoreCase("recalcularApuracao"))
			{
				String competencia = request.getParameter("cpt");
				String numemp = request.getParameter("numemp");
				String numcad = request.getParameter("numcad");
				this.recalcularApuracao(competencia, numemp, numcad, response);
			}

			if (action.equalsIgnoreCase("salvarAcerto"))
			{
				String jsonAcesso = request.getParameter("jsonAcesso");
				String datapu = request.getParameter("datapu");
				String numemp = request.getParameter("numemp");
				String tipcol = request.getParameter("tipcol");
				String numcad = request.getParameter("numcad");
				this.salvarAcerto(jsonAcesso, datapu, numemp, tipcol, numcad, response);
			}

			if (action.equalsIgnoreCase("moverMarcacao"))
			{
				String op = request.getParameter("op");
				String datapu = request.getParameter("datapu");
				String datacc = request.getParameter("datacc");
				String horacc = request.getParameter("horacc");
				String seqacc = request.getParameter("seqacc");
				String numemp = request.getParameter("numemp");
				String tipcol = request.getParameter("tipcol");
				String numcad = request.getParameter("numcad");
				this.moverMarcacao(datapu, datacc, horacc, seqacc, numemp, tipcol, numcad, op, response);
			}

			if (action.equalsIgnoreCase("listaEquipamentos"))
			{
				String numctr = request.getParameter("numctr");
				String numpos = request.getParameter("numpos");
				this.listaEquipamentos(numctr, numpos, response);
			}

			if (action.equalsIgnoreCase("listaChecklist"))
			{
				String numctr = request.getParameter("numctr");
				String numpos = request.getParameter("numpos");
				this.listaChecklist(numctr, numpos, response);
			}

			if (action.equalsIgnoreCase("listaContaSigma"))
			{
				String numctr = request.getParameter("numctr");
				String numpos = request.getParameter("numpos");
				this.listaContaSigma(numctr, numpos, response);
			}

			if (action.equalsIgnoreCase("listaEventoX8"))
			{
				String numctr = request.getParameter("numctr");
				String numpos = request.getParameter("numpos");
				this.listaEventoX8(numctr, numpos, response);
			}

			if (action.equalsIgnoreCase("listaEventosSigma"))
			{
				String cdCliente = request.getParameter("cdCliente");
				String dias = request.getParameter("dias");
				String tipoEvento = request.getParameter("tipoEvento");
				this.listaEventosSigma(cdCliente, dias, tipoEvento, response);
			}

			if (action.equalsIgnoreCase("listaChipGPRS"))
			{
				String numctr = request.getParameter("numctr");
				String numpos = request.getParameter("numpos");
				this.listaChipGPRS(numctr, numpos, response);
			}

			if (action.equalsIgnoreCase("listaSolicitacao"))
			{
				String codcli = request.getParameter("codcli");
				this.listaSolicitacao(codcli, response);
			}

			if (action.equalsIgnoreCase("listaReclamacao"))
			{
				String codcli = request.getParameter("codcli");
				this.listaReclamacao(codcli, response);
			}

			if (action.equalsIgnoreCase("listaVisita"))
			{
				String codpai = request.getParameter("codpai");
				this.listaVisita(codpai, response);
			}

			if (action.equalsIgnoreCase("listaCoberturaPosto"))
			{
				String numloc = request.getParameter("numloc");
				String taborg = request.getParameter("taborg");
				this.listaCoberturaPosto(numloc, taborg, response);
			}

			if (action.equalsIgnoreCase("listaJustificativaMarcacao"))
			{
				this.listaJustificativaMarcacao(response);
			}

			if (action.equalsIgnoreCase("insereHE"))
			{
				String numemp = request.getParameter("numemp");
				String tipcol = request.getParameter("tipcol");
				String numcad = request.getParameter("numcad");
				String data = request.getParameter("data");
				this.insereHoraExtra(numemp, tipcol, numcad, data, response);
			}

			if (action.equalsIgnoreCase("removeHE"))
			{
				String numemp = request.getParameter("numemp");
				String tipcol = request.getParameter("tipcol");
				String numcad = request.getParameter("numcad");
				String data = request.getParameter("data");
				this.removeHoraExtra(numemp, tipcol, numcad, data, response);
			}

			if (action.equalsIgnoreCase("validaExcecao"))
			{
				String op = request.getParameter("op");
				String numemp = request.getParameter("numemp");
				String tipcol = request.getParameter("tipcol");
				String numcad = request.getParameter("numcad");
				String codsit = request.getParameter("codsit");
				String data = request.getParameter("data");
				this.validaExcecao(op, numemp, tipcol, numcad, codsit, data, response);
			}

			if (action.equalsIgnoreCase("listaEscalaRevezamento"))
			{
				String codesc = request.getParameter("codesc");
				String data = request.getParameter("data");
				this.listaEscalaRevezamento(codesc, data, response);
			}

			if (action.equalsIgnoreCase("deleteAfastamento"))
			{
				String numemp = request.getParameter("numemp");
				String tipcol = request.getParameter("tipcol");
				String numcad = request.getParameter("numcad");
				String numcpf = request.getParameter("numcpf");
				String datafa = request.getParameter("datafa");

				this.deleteAfastamento(numemp, tipcol, numcad, numcpf, datafa, request, response);
			}

			if (action.equalsIgnoreCase("deleteAcesso"))
			{
				String numcra = request.getParameter("numcra");
				String numcpf = request.getParameter("numcpf");
				String datacc = request.getParameter("datacc");
				String seqacc = request.getParameter("seqacc");
				String exibirExcecao = request.getParameter("exibirExcecao");

				this.deleteAcesso(numcra, numcpf, datacc, seqacc, exibirExcecao, request, response);
			}

			if (action.equalsIgnoreCase("deleteCobertura"))
			{
				String numEmp = request.getParameter("numEmp");
				String tipCol = request.getParameter("tipCol");
				String numCad = request.getParameter("numCad");
				String dataIni = request.getParameter("dataInicial");
				String horaIni = request.getParameter("horaInicial");
				String numEmpCob = request.getParameter("numEmpCob");
				String tipColCob = request.getParameter("tipColCob");
				String numCadCob = request.getParameter("numCadCob");
				String numcpf = request.getParameter("numCpf");
				String descricaoCobertura = request.getParameter("descCob");
				String dataCobFim = request.getParameter("dataCobFim");
				String dataCobInicio = request.getParameter("dataCobInicio");

				this.deleteCobertura(numEmp, tipCol, numCad, dataIni, horaIni, numEmpCob, tipColCob, numCadCob, numcpf, descricaoCobertura, dataCobFim, dataCobInicio, request, response);
			}

			if (action.equalsIgnoreCase("deleteHistoricoEscala"))
			{
				String numEmp = request.getParameter("numemp");
				String tipCol = request.getParameter("tipcol");
				String numCad = request.getParameter("numcad");
				String numCpf = request.getParameter("numcpf");
				String datalt = request.getParameter("datalt");

				this.deleteHistoricoEscala(numEmp, tipCol, numCad, numCpf, datalt, request, response);
			}

			if (action.equalsIgnoreCase("enviaEmailPostoInconsistente"))
			{
				String posto = request.getParameter("posto");

				this.enviaEmailPostoInconsistente(request, response, posto);
			}

			if (action.equalsIgnoreCase("listaValeTransporte"))
			{
				this.listaValeTransporte(request, response);
			}
			if (action.equalsIgnoreCase("saveLiberacaoSite"))
			{
				String codigoCliente = request.getParameter("codigoCliente");
				String usuario = request.getParameter("usuario");
				this.saveLiberacaoSite(codigoCliente, usuario, response);
			}

			if (action.equalsIgnoreCase("controlaASO"))
			{
				String numctr = request.getParameter("numctr");
				String numposto = request.getParameter("numposto");
				String ctraso = request.getParameter("ctraso");
				String numemp = request.getParameter("numemp");
				String numfil = request.getParameter("numfil");
				String usuario = request.getParameter("usuario");
				String codSer = request.getParameter("codser");
				this.controlaASO(numctr, numemp, numfil, numposto, codSer, usuario, ctraso, response);
			}

			if (action.equalsIgnoreCase("listaFinancasPosto"))
			{
				String numctr = request.getParameter("numctr");
				String numposto = request.getParameter("numpos");
				this.listaFinancasPosto(numctr, numposto, response);
			}

			if (action.equalsIgnoreCase("listaX8Colaborador"))
			{
				String numemp = request.getParameter("numemp");
				String numcad = request.getParameter("numcad");
				String strDatIni = request.getParameter("datIni");
				String strDatFim = request.getParameter("datFim");
				String meses = request.getParameter("meses");
				String numcpf = request.getParameter("numcpf");
				String evt = request.getParameter("evt");
				if (meses == null)
				{
					meses = "1";
				}
				if (evt == null)
				{
					evt = "0";
				}

				GregorianCalendar gcDatIni = new GregorianCalendar();
				GregorianCalendar gcDatFim = new GregorianCalendar();
				gcDatIni.add(GregorianCalendar.MONTH, Integer.parseInt(meses) * (-1));

				this.listaX8Colaborador(numemp, numcad, numcpf, gcDatIni, gcDatFim, evt, response);
			}

			if (action.equalsIgnoreCase("listaFichaEpi"))
			{
				String numemp = request.getParameter("numemp");
				String tipcol = request.getParameter("tipcol");
				String numcad = request.getParameter("numcad");
				String numcpf = request.getParameter("numcpf");
				this.listaFichaEpi(numcpf, NeoUtils.safeLong(numcad), NeoUtils.safeLong(numemp), NeoUtils.safeLong(tipcol), response);

			}

			if (action.equalsIgnoreCase("listaDocumentosArqiuivo"))
			{
				String numcpf = request.getParameter("cpf");
				String numCad = request.getParameter("numCad");
				String numEmp = request.getParameter("numemp");
				this.listaDocumentosArquivos(numcpf, numCad, numEmp, response);
			}
			if (action.equalsIgnoreCase("listaInspecoes"))
			{
				String numctr = request.getParameter("numctr");
				String numposto = request.getParameter("numpos");
				this.listaInspecoes(numctr, numposto, response);
			}
			if (action.equalsIgnoreCase("buscaContaSigma"))
			{
				String conta = request.getParameter("conta");
				String filtro = request.getParameter("filtro");
				if (filtro == null)
				{
					filtro = "1";
				}
				if (conta != null)
				{
					buscaContaSigma(filtro, conta, response);
				}
			}
			if (action.equals("setVinculoContaPosto"))
			{
				String cdCliente = request.getParameter("cdCliente");
				String idCentral = request.getParameter("idCentral");
				String numctr = request.getParameter("numctr");
				String numposto = request.getParameter("numpos");
				this.setVinculoContaPosto(numctr, numposto, idCentral, cdCliente, response);

			}
			if (action.equalsIgnoreCase("removerVinculoContaPosto"))
			{
				String cdCliente = request.getParameter("cdCliente");
				String idCentral = request.getParameter("idCentral");
				String numctr = request.getParameter("numctr");
				String numposto = request.getParameter("numpos");
				this.removerVinculoContaPosto(numctr, numposto, idCentral, cdCliente, response);
			}

			if (action.equalsIgnoreCase("listatVinculoContaPosto"))
			{
				String cdCliente = request.getParameter("cdCliente");
				String idCentral = request.getParameter("idCentral");
				String numctr = request.getParameter("numctr");
				String numposto = request.getParameter("numpos");
				this.listatVinculoContaPosto(numctr, numposto, idCentral, cdCliente, response);
			}

			if (action.equalsIgnoreCase("listaArmasPosto"))
			{
				String numctr = request.getParameter("numctr");
				String numposto = request.getParameter("numpos");
				GregorianCalendar dataFiltro = new GregorianCalendar();
				String timeMillis = request.getParameter("timeMillisDataAtual");
				if (timeMillis != null)
				{
					dataFiltro.setTimeInMillis(NeoUtils.safeLong(timeMillis));
				}

				this.listaArmasPosto(numctr, numposto, dataFiltro, response);
			}

			if (action.equalsIgnoreCase("listaOciosidade"))
			{
				String numemp = request.getParameter("numemp");
				String tipcol = request.getParameter("tipcol");
				String numcad = request.getParameter("numcad");
				this.listaOciosidade(numemp, tipcol, numcad, response);
			}

			if (action.equalsIgnoreCase("listaFaltaEfetivo"))
			{
				String codccu = request.getParameter("codccu");
				this.listaFaltaEfetivo(codccu, response);
			}

			if (action.equalsIgnoreCase("listaHistoricoEscalaPosto"))
			{
				String numloc = request.getParameter("numloc");
				String taborg = request.getParameter("taborg");
				this.listaHistoricoEscalaPosto(numloc, taborg, response);
			}

			if (action.equalsIgnoreCase("listaPostosSupervisaoVinculados"))
			{

				String codccu = request.getParameter("codccu");
				if (codccu == null)
				{
					codccu = "0";
				}

				this.listaPostosSupervisaoVinculados(NeoUtils.safeLong(codccu), response);
			}

			if (action.equalsIgnoreCase("salvaVinculoSupervisaoPosto"))
			{
				String codccu = request.getParameter("codccu");
				String numloc = request.getParameter("numloc");
				String usuario = request.getParameter("usuario");
				String ccusup = request.getParameter("ccusup");

				this.salvaVinculoSupervisaoPosto(codccu, numloc, usuario, ccusup, response);
			}

			if (action.equalsIgnoreCase("atualizaPostoSup"))
			{
				String codccu = request.getParameter("codccu");
				String numloc = request.getParameter("numloc");
				String usuario = request.getParameter("usuario");
				String isPostoSup = request.getParameter("isPostoSup");

				this.atualizaPostoSup(codccu, numloc, usuario, isPostoSup, response);
			}

			if (action.equalsIgnoreCase("removerVinculoSupervisaoPosto"))
			{
				String codccu = request.getParameter("codccu");
				String numloc = request.getParameter("numloc");
				String usuario = request.getParameter("usuario");
				String ccusup = request.getParameter("ccusup");

				this.removerVinculoSupervisaoPosto(codccu, numloc, usuario, ccusup, response);
			}

			if (action.equalsIgnoreCase("listaMensagensURA"))
			{
				String numcpf = request.getParameter("numcpf");

				this.listaMensagensURA(NeoUtils.safeLong(numcpf), response);
			}

			if (action.equalsIgnoreCase("cancelarReciclagem"))
			{
				String neoid = request.getParameter("neoId");
				String numcpf = request.getParameter("cpf");
				String dataCurso = request.getParameter("curso");
				WFProcess processo = (WFProcess) PersistEngine.getNeoObject(WFProcess.class, new QLRawFilter("entity_neoId = " + neoid));
				this.cancelarTarefasReciclagem(dataCurso, processo.getCode(), numcpf, response);
			}

			if (action.equalsIgnoreCase("listaAfastamentos"))
			{
				String numemp = request.getParameter("numemp");
				String tipcol = request.getParameter("tipcol");
				String numcad = request.getParameter("numcad");
				this.listaAfastamentos(numemp, tipcol, numcad, response);
			}

			out.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void insertSituacaoIndefinida(String numemp, String tipcol, String numcad, String datafa, String codesc, String codtma, String horini, HttpServletRequest request, HttpServletResponse response) throws IOException
	{

		this.entityManager = PersistEngine.getEntityManager("VETORH");
		this.transaction = this.entityManager.getTransaction();

		final PrintWriter out = response.getWriter();
		Integer result = 0;

		try
		{

			GregorianCalendar dataNula = new GregorianCalendar(1900, GregorianCalendar.DECEMBER, 31);
			GregorianCalendar dataInicioEscala = new GregorianCalendar();
			GregorianCalendar dataFinalEscala = new GregorianCalendar();
			GregorianCalendar dataAlteracao = new GregorianCalendar();
			GregorianCalendar dataAtual = new GregorianCalendar();

			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			Date date = df.parse(datafa);
			dataAlteracao.setTime(date);

			String afastamentoInformado = null;

			//Valida escala para inserir afastamento informado
			
			String datAtuString = NeoDateUtils.safeDateFormat(dataAlteracao,"dd/MM/yyyy");
			
			if(datAtuString.equals("24/12/2016")||datAtuString.equals("25/12/2016") || datAtuString.equals("31/12/2016") || datAtuString.equals("01/01/2017")){
				afastamentoInformado = null;				
			}else if (horini.equals("0"))
			{
				List<EscalaVO> escalas = QLPresencaUtils.getEscalasColaboradores(dataAlteracao, Long.valueOf(codesc), Long.valueOf(codtma), true);
				//List<EscalaVO> escalas = null;

				if (escalas != null && !escalas.isEmpty() && escalas.get(0).getHorarios() != null && escalas.get(0).getHorarios().get(0).getDataInicial() != null)
				{
					dataInicioEscala = escalas.get(0).getHorarios().get(0).getDataInicial();
					long horas = (dataInicioEscala.getTimeInMillis() - dataAtual.getTimeInMillis()) / 3600000;

					if (horas >= 6L)
					{
						afastamentoInformado = "S";
					}
				}
			}
			else
			{
				Integer hora = Integer.valueOf(horini) / 60;
				Integer minuto = Integer.valueOf(horini) % 60;

				GregorianCalendar dataEntrada = OrsegupsUtils.stringToGregorianCalendar(datafa, "dd/MM/yyyy");
				dataEntrada.add(Calendar.HOUR_OF_DAY, hora);
				dataEntrada.add(Calendar.MINUTE, minuto);

				long horas = (dataEntrada.getTimeInMillis() - dataAtual.getTimeInMillis()) / 3600000;

				if (horas >= 6L)
				{
					afastamentoInformado = "S";
				}
			}

			if (!this.transaction.isActive())
			{
				this.transaction.begin();
			}

			StringBuilder admColaborador = new StringBuilder();
			admColaborador.append(" SELECT DATADM FROM R034FUN WHERE numemp = :numemp AND tipcol = :tipcol AND numcad = :numcad ");

			Query queryAdm = this.entityManager.createNativeQuery(admColaborador.toString());
			queryAdm.setParameter("numemp", numemp);
			queryAdm.setParameter("tipcol", tipcol);
			queryAdm.setParameter("numcad", numcad);
			queryAdm.setMaxResults(1);

			Timestamp timeAdm = (Timestamp) queryAdm.getSingleResult();
			GregorianCalendar dataAdm = new GregorianCalendar();
			dataAdm.setTimeInMillis(timeAdm.getTime());

			if (NeoUtils.safeIsNotNull(dataAdm) && dataAlteracao.before(dataAdm))
			{
				result = 1000;
			}
			else
			{
				StringBuilder buscaAfastamento = new StringBuilder();
				buscaAfastamento.append(" SELECT COUNT(*) FROM R038AFA WHERE numemp = :numemp ");
				buscaAfastamento.append(" AND tipcol = :tipcol ");
				buscaAfastamento.append(" AND numcad = :numcad ");
				buscaAfastamento.append(" AND CAST(FLOOR(CAST(datafa AS float)) AS datetime) <= :dataAlteracao ");
				buscaAfastamento.append(" AND (CAST(FLOOR(CAST(datter AS float)) AS datetime) >= :dataAlteracao OR datter = :dataNula) ");

				Query queryBuscaAfa = this.entityManager.createNativeQuery(buscaAfastamento.toString());
				queryBuscaAfa.setParameter("numemp", numemp);
				queryBuscaAfa.setParameter("tipcol", tipcol);
				queryBuscaAfa.setParameter("numcad", numcad);
				queryBuscaAfa.setParameter("dataAlteracao", dataAlteracao);
				queryBuscaAfa.setParameter("dataNula", dataNula);
				queryBuscaAfa.setMaxResults(1);

				Integer count = (Integer) queryBuscaAfa.getSingleResult();

				if (count != null && count > 0)
				{
					result = 100;
				}
				else
				{
					NeoUser user = PortalUtil.getCurrentUser();
					StringBuilder insereAfastamento = new StringBuilder();

					String dataString = NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm");
					String ObsAfa = "Inserido por " + user.getFullName() + " em " + dataString;

					insereAfastamento.append(" Insert Into R038AFA (NumEmp, TipCol, NumCad, DatAfa, HorAfa, DatTer, SitAfa, OriAfa, ObsAfa, StaAtu, usu_codusu, usu_datger, usu_afainf, usu_datmod, usu_hormod) ");
					insereAfastamento.append("  Values (:numemp,:tipcol,:numcad,:dataAlteracao,0,:dataAlteracao,500,0,:ObsAfa,1,389,:dataAtual, :afainf,");
					insereAfastamento.append("		DATEADD(dd, 0, DATEDIFF(dd, 0, :datMod1)), (DATEPART(HOUR, :datMod2) * 60) + DATEPART(MINUTE, :datMod3)) ");

					Query queryInsereAfa = this.entityManager.createNativeQuery(insereAfastamento.toString());

					queryInsereAfa.setParameter("numemp", numemp);
					queryInsereAfa.setParameter("tipcol", tipcol);
					queryInsereAfa.setParameter("numcad", numcad);
					queryInsereAfa.setParameter("dataAlteracao", new Timestamp(dataAlteracao.getTime().getTime()));
					queryInsereAfa.setParameter("dataAlteracao", new Timestamp(dataAlteracao.getTime().getTime()));
					queryInsereAfa.setParameter("ObsAfa", ObsAfa);
					queryInsereAfa.setParameter("dataAtual", new Timestamp(dataAtual.getTime().getTime()));
					queryInsereAfa.setParameter("afainf", afastamentoInformado);
					GregorianCalendar datMod = new GregorianCalendar();
					queryInsereAfa.setParameter("datMod1", new Timestamp(datMod.getTimeInMillis()));
					queryInsereAfa.setParameter("datMod2", new Timestamp(datMod.getTimeInMillis()));
					queryInsereAfa.setParameter("datMod3", new Timestamp(datMod.getTimeInMillis()));

					result = queryInsereAfa.executeUpdate();

					//LOG Colaborador
					String txtLog = "Inserido afastamento em " + datafa;

					StringBuilder buscaCpf = new StringBuilder();
					buscaCpf.append(" SELECT numcpf FROM R034FUN WHERE numemp = :numemp ");
					buscaCpf.append(" AND tipcol = :tipcol ");
					buscaCpf.append(" AND numcad = :numcad ");

					Query queryBuscaCpf = this.entityManager.createNativeQuery(buscaCpf.toString());
					queryBuscaCpf.setParameter("numemp", numemp);
					queryBuscaCpf.setParameter("tipcol", tipcol);
					queryBuscaCpf.setParameter("numcad", numcad);
					queryBuscaCpf.setMaxResults(1);

					BigInteger cpf = (BigInteger) queryBuscaCpf.getSingleResult();
					String cpfString = cpf.toString();

					if (cpfString != null && !cpfString.equals(""))
					{
						this.saveLog(txtLog, null, cpfString, "colaborador", null);
					}

					log.warn("Cadastro Afastamento - NumEmp:" + numemp + " - NumCad: " + numcad + ")");

					this.transaction.commit();
				}
			}
		}
		catch (ParseException e)
		{
			log.warn("Não foi possivel a conversão de data");
			result = 200;
		}
		catch (Exception ex)
		{
			if (this.transaction.isActive())
			{
				this.transaction.rollback();
			}
			ex.printStackTrace();
		}

		out.print(result);
	}

	private void saveLog(String texto, String idposto, String idColaborador, String tipo, PrintWriter out)
	{
		if (tipo != null && tipo.equalsIgnoreCase("posto"))
		{
			QLGroupFilter groupFilter = new QLGroupFilter("AND");
			groupFilter.addFilter(new QLEqualsFilter("codloc", idposto));

			/**
			 * @author orsegups lucas.avila - Classe generica.
			 * @date 08/07/2015
			 */
			Class<? extends NeoObject> clazz = AdapterUtils.getEntityClass("VETORH_USU_Vorg203nv8");

			NeoObject postoObject = PersistEngine.getObject(clazz, groupFilter);

			if (postoObject != null)
			{
				NeoObject logPosto = AdapterUtils.createNewEntityInstance("QLLogPresencaPosto");

				if (logPosto != null)
				{
					EntityWrapper postoWrapper = new EntityWrapper(logPosto);
					postoWrapper.setValue("posto", postoObject);
					postoWrapper.setValue("dataLog", new GregorianCalendar());
					postoWrapper.setValue("textoLog", texto);
					postoWrapper.setValue("usuario", PortalUtil.getCurrentUser());

					PersistEngine.persist(logPosto);
					if (out != null)
					{
						out.print("Log salvo com sucesso.");
					}
					return;
				}
			}
		}
		else if (tipo != null && tipo.equalsIgnoreCase("colaborador"))
		{
			NeoObject log = AdapterUtils.createNewEntityInstance("QLLogPresencaColaborador");

			if (log != null && idColaborador != null)
			{
				texto = texto.replaceAll("'", "´");
				texto = texto.replaceAll("\"", "´");

				EntityWrapper logWrapper = new EntityWrapper(log);
				logWrapper.setValue("cpf", Long.valueOf(idColaborador));
				logWrapper.setValue("dataLog", new GregorianCalendar());
				logWrapper.setValue("textoLog", texto);
				logWrapper.setValue("usuario", PortalUtil.getCurrentUser());

				if (idposto != null)
				{
					QLGroupFilter groupFilter = new QLGroupFilter("AND");
					groupFilter.addFilter(new QLEqualsFilter("codloc", idposto));

					/**
					 * @author orsegups lucas.avila - Classe generica.
					 * @date 08/07/2015
					 */
					Class<? extends NeoObject> clazz = AdapterUtils.getEntityClass("VETORH_USU_Vorg203nv8");

					NeoObject postoObject = PersistEngine.getObject(clazz, groupFilter);

					if (postoObject != null)
					{
						logWrapper.setValue("posto", postoObject);
					}
				}

				PersistEngine.persist(log);

				if (out != null)
				{
					out.print("Log salvo com sucesso.");
				}
				return;
			}
		}

		out.print("Erro ao salvar log.");
	}

	public void insertEventoForcado(String numcra, String numemp, String tipcol, String numcad, String numloc, String taborg, String diracc, String datacc, String horacc, String minacc, String tipdir, String codjma, String obsjma, HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		final PrintWriter out = response.getWriter();
		Integer result = 0;

		if (numcra == null || numcra.equals("") || datacc == null || datacc.equals("") || codjma == null || codjma.equals(""))
		{
			out.print(result);
			return;
		}

		this.entityManager = PersistEngine.getEntityManager("VETORH");
		this.transaction = this.entityManager.getTransaction();
		GregorianCalendar dataHoraAcc = new GregorianCalendar();
		Date date = null;
		String tarefaSimples = null;
		try
		{
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

			date = df.parse(datacc);
			dataHoraAcc.setTime(date);
			dataHoraAcc.set(GregorianCalendar.HOUR, Integer.parseInt(horacc));
			dataHoraAcc.set(GregorianCalendar.MINUTE, Integer.parseInt(minacc));
			dataHoraAcc.set(GregorianCalendar.SECOND, 0);

			Long numlocLong = Long.parseLong(numloc);
			Long taborgLong = Long.parseLong(taborg);

			GregorianCalendar limiteInferior = new GregorianCalendar();
			limiteInferior.add(GregorianCalendar.HOUR_OF_DAY, -72);
			GregorianCalendar limiteSuperior = new GregorianCalendar();

			if ((dataHoraAcc.after(limiteInferior)) && (dataHoraAcc.before(limiteSuperior)))
			{
				if (!this.transaction.isActive())
				{
					this.transaction.begin();
				}

				if (tipdir.equals("A"))
				{
					if (diracc.equals("E"))
					{
						tipdir = QLPresencaUtils.verificaUltimaEntradaPadrao(numcra, dataHoraAcc);
					}
					else
					{
						tipdir = QLPresencaUtils.verificaUltimaEntradaAutorizada(numcra, numlocLong, taborgLong, dataHoraAcc);
					}
				}

				StringBuilder buscaPerEsc = new StringBuilder();
				buscaPerEsc.append(" SELECT orn.usu_peresc, orn.usu_codccu FROM R016ORN orn ");
				
				buscaPerEsc.append(" WHERE orn.numloc = :numloc AND orn.taborg = :taborg ");

				Query queryBuscaPerEsc = this.entityManager.createNativeQuery(buscaPerEsc.toString());
				queryBuscaPerEsc.setParameter("numloc", numloc);
				queryBuscaPerEsc.setParameter("taborg", taborg);
				queryBuscaPerEsc.setMaxResults(1);

				Short peresc = 0;
				String codccu = "";

				Object obj = queryBuscaPerEsc.getSingleResult();
				if (NeoUtils.safeIsNotNull(result))
				{
					Object[] resultado = (Object[]) obj;
					peresc = (Short) resultado[0];
					codccu = (String) resultado[1];
				}

				if (peresc == null || peresc == 0L)
				{
					peresc = 80;
				}

				if (numcra == null || numcra.length() != 12)
				{
					out.print(result);
					return;
				}

				ColaboradorVO colaborador = QLPresencaUtils.getBuscaColaborador(Long.parseLong(numcad), Long.parseLong(numemp), dataHoraAcc);
				EscalaPostoVO escalaPosto = QLPresencaUtils.getEscalaPosto(Long.parseLong(numloc), Long.parseLong(taborg), dataHoraAcc);
				Integer retorno = 0;

				if (tipdir.equals("P"))
				{
					retorno = QLPresencaUtils.validaToleranciaEscala(colaborador, escalaPosto, Long.valueOf(peresc), diracc, dataHoraAcc, tipdir, true);
				}
				else
				{
					List<EntradaAutorizadaVO> entradasAutorizadas = new ArrayList<EntradaAutorizadaVO>();
					entradasAutorizadas = QLPresencaUtils.getEntradasAutorizadasColaborador(colaborador, dataHoraAcc, codccu);

					if (entradasAutorizadas != null && !entradasAutorizadas.isEmpty())
					{
						for (EntradaAutorizadaVO entrada : entradasAutorizadas)
						{
							//Verifica Última Entrada Padrao nas ultimas 12 horas
							String tipoDirecaoAcesso = QLPresencaUtils.verificaUltimaEntradaPadrao(entrada.getColaborador().getNumeroCracha(), dataHoraAcc);

							//Validar Tolerancia e duplicidade
							retorno = QLPresencaUtils.validaToleranciaEscala(entrada.getColaborador(), escalaPosto, Long.valueOf(peresc), diracc, dataHoraAcc, tipoDirecaoAcesso, true);
						}
					}
				}
				System.out.println("["+tarefaSimples+"] feito validação da escala, retorno: "+retorno);
				if (retorno == 3)
				{
				    System.out.println("["+tarefaSimples+"] entrou no if retorno 3: "+retorno);
					result = -1;
				}
				else
				{
				    System.out.println("["+tarefaSimples+"] entrou no else retorno <> 3: "+retorno);
					Long usomar = 2L;

					if (tipdir.equals("L"))
					{
						usomar = 1L;
					}

					StringBuilder sqlRegistro = new StringBuilder();
					sqlRegistro.append(" INSERT INTO R070ACC (NumCra, DatAcc, HorAcc, SeqAcc, TipAcc, CodPlt, DirAcc, OriAcc, UsoMar, CodRlg, FlaAcc, CodBnf, StaRlg, ExcPon, CodDsp, USU_TabOrg, USU_NumLoc, USU_TipDir, USU_PerEsc, USU_CodJma, USU_ObsJma) ");
					sqlRegistro.append(" SELECT :numcra, DATEADD(dd, 0, DATEDIFF(dd, 0, :dataHoraAcc)), (DATEPART(HOUR, :dataHoraAcc) * 60) + DATEPART(MINUTE, :dataHoraAcc), ISNULL(MAX(SeqAcc)+1, 1), 100, 2, :diracc, 'E', :usomar, 2, 0, 0, 1, 'N', 4, :taborg, :numloc, :tipdir, :peresc, :codjma, :obsjma ");
					sqlRegistro.append(" FROM R070ACC WHERE NumCra = :numcra AND DatAcc = DATEADD(dd, 0, DATEDIFF(dd, 0, :dataHoraAcc)) AND HorAcc = (DATEPART(HOUR, :dataHoraAcc) * 60) + DATEPART(MINUTE, :dataHoraAcc) ");

					Query queryInsertEvento = this.entityManager.createNativeQuery(sqlRegistro.toString());
					queryInsertEvento.setParameter("numcra", numcra);
					queryInsertEvento.setParameter("dataHoraAcc", dataHoraAcc);
					queryInsertEvento.setParameter("dataHoraAcc", dataHoraAcc);
					queryInsertEvento.setParameter("dataHoraAcc", dataHoraAcc);
					queryInsertEvento.setParameter("diracc", diracc);
					queryInsertEvento.setParameter("usomar", usomar);
					queryInsertEvento.setParameter("numloc", numloc);
					queryInsertEvento.setParameter("taborg", taborg);
					queryInsertEvento.setParameter("tipdir", tipdir);
					queryInsertEvento.setParameter("peresc", peresc);
					queryInsertEvento.setParameter("codjma", codjma);
					queryInsertEvento.setParameter("obsjma", obsjma);
					queryInsertEvento.setParameter("peresc", peresc);
					queryInsertEvento.setParameter("numcra", numcra);
					queryInsertEvento.setParameter("dataHoraAcc", dataHoraAcc);
					queryInsertEvento.setParameter("dataHoraAcc", dataHoraAcc);
					queryInsertEvento.setParameter("dataHoraAcc", dataHoraAcc);

					result = queryInsertEvento.executeUpdate();
					log.warn("Cadastro Evento Forçado (R070ACC: " + result + ")");

					String txtLog = "";
					String hora = "";
					String min = "";

					NumberFormat nf = NumberFormat.getInstance();
					nf.setMinimumIntegerDigits(2);

					hora = nf.format(Integer.parseInt(horacc));
					min = nf.format(Integer.parseInt(minacc));

					if (diracc.equals("E"))
					{
						txtLog = "Entrada forçada manualmente em " + NeoUtils.safeDateFormat(dataHoraAcc, "dd/MM/yyyy") + " as " + hora + "h" + min;
					}
					else
					{
						txtLog = "Saída forçada manualmente em " + NeoUtils.safeDateFormat(dataHoraAcc, "dd/MM/yyyy") + " as " + hora + "h" + min;
					}

					StringBuilder buscaCpf = new StringBuilder();
					buscaCpf.append(" SELECT fun.NumCpf FROM R034FUN fun ");
					buscaCpf.append(" INNER JOIN R038HCH hch ON hch.NumEmp = fun.NumEmp AND fun.TipCol = hch.TipCol AND fun.NumCad = hch.NumCad AND DATEADD(MINUTE, hch.HorIni, hch.DatIni) = (SELECT MAX(DATEADD(MINUTE, hch2.HorIni, hch2.DatIni)) FROM R038HCH hch2 WHERE hch2.NUMEMP = hch.NUMEMP AND hch2.TIPCOL = hch.TIPCOL AND hch2.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch2.HorIni, hch2.DatIni) <= :datacc) ");
					buscaCpf.append(" WHERE hch.NumCra = :numcra ");

					Query queryBuscaCpf = this.entityManager.createNativeQuery(buscaCpf.toString());
					queryBuscaCpf.setParameter("datacc", dataHoraAcc);
					queryBuscaCpf.setParameter("numcra", numcra);
					queryBuscaCpf.setMaxResults(1);

					BigInteger cpf = (BigInteger) queryBuscaCpf.getSingleResult();
					String cpfString = cpf.toString();
					System.out.println("["+tarefaSimples+"] CPF para salvar log: "+cpfString);
					if (cpfString != null && !cpfString.equals(""))
					{
					    System.out.println("["+tarefaSimples+"] Entrou no if para salvar o log: "+cpfString);
					    this.saveLog(txtLog, null, cpfString, "colaborador", null);
					    //if (tipdir.equals("P") && (codjma.equals("1") || codjma.equals("3") || codjma.equals("4")))
					    if (tipdir.equals("P") && (codjma.equals("4")))
					    {
						if (diracc.equals("S") || diracc.equals("E"))
						{
						    {
							Long classeEscala = retornaClasseEscala(Long.parseLong(numcad), Long.parseLong(numemp));
							if (classeEscala != 8)
							{
							    tarefaSimples = iniciarTarefaSimples(Long.parseLong(numcad), Long.parseLong(numemp), dataHoraAcc);
							}
						    }
						}
					    }
					}

					this.transaction.commit();
				}
			}
		}
		catch (ParseException e)
		{
			log.warn("Não foi possivel a conversão de data");
			e.printStackTrace();
		}
		catch (Exception ex)
		{
			if (this.transaction.isActive())
			{
				this.transaction.rollback();
			}
			ex.printStackTrace();
		}

		out.print(result);

	}

	public void deleteRegistroCobertura(String numEmp, String tipCol, String numCad, String dataInicial, String horaInicial, String numEmpCob, String tipColCob, String numCadCob, boolean imprimeMsg, String numcpf, String descricaoCobertura, String dataCobFim, String dataCobInicio, String nextid, HttpServletRequest request, HttpServletResponse response) throws IOException
	{

		this.entityManager = PersistEngine.getEntityManager("VETORH");
		this.transaction = this.entityManager.getTransaction();
		Integer result = 0;
		final PrintWriter out = response.getWriter();
		GregorianCalendar data = new GregorianCalendar();

		try
		{

			deleteRegistroCoberturaTIDB(numEmp, tipCol, numCad, dataInicial, horaInicial, numEmpCob, tipColCob, numCadCob, imprimeMsg, numcpf, descricaoCobertura, dataCobFim, dataCobInicio, request, response);

			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			Date date = df.parse(dataInicial);
			data.setTime(date);

			if (!this.transaction.isActive())
			{
				this.transaction.begin();
			}

			String deleteCobertura = "DELETE FROM USU_T038CobFun WHERE USU_NumEmp =:numEmp AND USU_TipCol =:tipCol AND USU_NumCad =:numCad AND CONVERT(varchar, USU_DatAlt, 103) =:dataInicial AND USU_HorIni =:horaInicial ";
			if (nextid != null && !nextid.isEmpty()){
			    deleteCobertura += " AND USU_NEXTID =:nextid ";
			}
			Query queryDeleteCobertura = this.entityManager.createNativeQuery(deleteCobertura);
			queryDeleteCobertura.setParameter("numEmp", numEmp);
			queryDeleteCobertura.setParameter("tipCol", tipCol);
			queryDeleteCobertura.setParameter("numCad", numCad);
			queryDeleteCobertura.setParameter("dataInicial", dataInicial);
			queryDeleteCobertura.setParameter("horaInicial", horaInicial);
			if (nextid != null && !nextid.isEmpty()){
			    queryDeleteCobertura.setParameter("nextid", nextid);
			}

			result = queryDeleteCobertura.executeUpdate();
			String debugStr = "[" + GregorianCalendar.getInstance().getTimeInMillis() + "] - numcad=" + numCad + ", numEmp=" + numEmp;
			log.warn("USU_T038CobFun - " + debugStr + " - DELETED: " + result);
			if ((nextid != null && !nextid.isEmpty() && result > 0)||(nextid == null || "".equals(nextid))){
			    //Excluir Autorizacao de Hora Extra
			    String deleteHoraExtra = " DELETE FROM R064EXT WHERE numemp = :numEmp AND tipcol = :tipCol AND numcad = :numCad AND CONVERT(varchar, datIni, 103) = :datIni ";
			    Query deleteHERegisters = this.entityManager.createNativeQuery(deleteHoraExtra);
			    deleteHERegisters.setParameter("numEmp", numEmp);
			    deleteHERegisters.setParameter("tipCol", tipCol);
			    deleteHERegisters.setParameter("numCad", numCad);
			    deleteHERegisters.setParameter("datIni", dataInicial);
			    
			    Integer result2 = deleteHERegisters.executeUpdate();
			    QLAdapter.log.warn("R064EXT  " + debugStr + " - DELETED: " + result2);
			    
			    if (!NeoUtils.safeOutputString(numCadCob).equals(""))
			    {
				//Excluir Afastamento Situacao 500
				String deleteAfastamento = " DELETE FROM R038AFA WHERE NumEmp = :numEmp AND TipCol = :tipCol AND  NumCad = :numCad AND CONVERT(varchar, DatAfa, 103) = :datAfa AND HorAfa = 0 AND SitAfa = 500 ";
				Query deleteAfastamentoRegisters = this.entityManager.createNativeQuery(deleteAfastamento);
				deleteAfastamentoRegisters.setParameter("numEmp", numEmpCob);
				deleteAfastamentoRegisters.setParameter("tipCol", tipColCob);
				deleteAfastamentoRegisters.setParameter("numCad", numCadCob);
				deleteAfastamentoRegisters.setParameter("datAfa", dataInicial);
				
				Integer result3 = deleteAfastamentoRegisters.executeUpdate();
				QLAdapter.log.warn("R038AFA  " + debugStr + " - DELETED: " + result3);
			    }
			    
			    //Excluir Troca de Escala Colaborador 1
			    String deleteTroca = " DELETE FROM R064TES WHERE NumEmp = :numEmp AND TipCol = :tipCol AND NumCad = :numCad AND CONVERT(varchar, datIni, 103) = :datIni ";
			    Query deleteTrocaRegisters = this.entityManager.createNativeQuery(deleteTroca);
			    deleteTrocaRegisters.setParameter("numEmp", numEmp);
			    deleteTrocaRegisters.setParameter("tipCol", tipCol);
			    deleteTrocaRegisters.setParameter("numCad", numCad);
			    deleteTrocaRegisters.setParameter("datIni", dataInicial);
			    
			    Integer result4 = deleteTrocaRegisters.executeUpdate();
			    QLAdapter.log.warn("R064TES  " + debugStr + " - DELETED: " + result4);
			    
			    if (numEmpCob == null || tipColCob == null || numCadCob == null)
			    {
				
				//Excluir Troca de Escala Colaborador 2
				String deleteTroca2 = " DELETE FROM R064TES WHERE NumEmp = :numEmpCob AND TipCol = :tipColCob AND NumCad = :numCadCob AND CONVERT(varchar, datIni, 103) = :datIni ";
				Query deleteTrocaRegisters2 = this.entityManager.createNativeQuery(deleteTroca2);
				deleteTrocaRegisters2.setParameter("numEmpCob", numEmpCob);
				deleteTrocaRegisters2.setParameter("tipColCob", tipColCob);
				deleteTrocaRegisters2.setParameter("numCadCob", numCadCob);
				deleteTrocaRegisters2.setParameter("datIni", dataInicial);
				
				Integer result5 = deleteTrocaRegisters2.executeUpdate();
				QLAdapter.log.warn("R064TES  " + debugStr + " - DELETED: " + result5);
			    }
			    
			    if (numEmpCob != null && tipColCob != null && numCadCob != null && !"null".equals(numEmpCob) && !"null".equals(tipColCob) && !"null".equals(numCadCob)) // adicionado este bloco pois não estava excluindo a programação de troca de escala do 2 colaborador na inversão de escala.
			    {
				//Excluir cobertura do colaborador 2
				String deleteCobertura2 = "DELETE FROM USU_T038CobFun WHERE USU_NumEmp =:numEmp AND USU_TipCol =:tipCol AND USU_NumCad =:numCad AND CONVERT(varchar, USU_DatAlt, 103) =:dataInicial AND USU_HorIni =:horaInicial ";
				
				Query queryDeleteCobertura2 = this.entityManager.createNativeQuery(deleteCobertura2);
				queryDeleteCobertura2.setParameter("numEmp", numEmpCob);
				queryDeleteCobertura2.setParameter("tipCol", tipColCob);
				queryDeleteCobertura2.setParameter("numCad", numCadCob);
				queryDeleteCobertura2.setParameter("dataInicial", dataInicial);
				queryDeleteCobertura2.setParameter("horaInicial", horaInicial);
				
				result = queryDeleteCobertura2.executeUpdate();
				String debugStr2 = "[" + GregorianCalendar.getInstance().getTimeInMillis() + "] - numcad=" + numCadCob + ", numEmp=" + numEmpCob;
				log.warn("USU_T038CobFun - " + debugStr + " - DELETED: " + result);
				
				//Excluir Troca de Escala Colaborador 2
				String deleteTroca2 = " DELETE FROM R064TES WHERE NumEmp = :numEmpCob AND TipCol = :tipColCob AND NumCad = :numCadCob AND CONVERT(varchar, datIni, 103) = :datIni ";
				Query deleteTrocaRegisters2 = this.entityManager.createNativeQuery(deleteTroca2);
				deleteTrocaRegisters2.setParameter("numEmpCob", numEmpCob);
				deleteTrocaRegisters2.setParameter("tipColCob", tipColCob);
				deleteTrocaRegisters2.setParameter("numCadCob", numCadCob);
				deleteTrocaRegisters2.setParameter("datIni", dataInicial);
				
				Integer result5 = deleteTrocaRegisters2.executeUpdate();
				QLAdapter.log.warn("R064TES  " + debugStr + " - DELETED: " + result5);
			    }
			    
			    //Excluir Autorizacao de Rateio
			    String deleteRateio = " DELETE FROM R064RAT WHERE numemp = :numEmp AND tipcol = :tipCol AND numcad = :numCad AND CONVERT(varchar, datIni, 103) = :datIni AND horIni =:horIni ";
			    Query deleteRatRegisters = this.entityManager.createNativeQuery(deleteRateio);
			    deleteRatRegisters.setParameter("numEmp", numEmp);
			    deleteRatRegisters.setParameter("tipCol", tipCol);
			    deleteRatRegisters.setParameter("numCad", numCad);
			    deleteRatRegisters.setParameter("datIni", dataInicial);
			    deleteRatRegisters.setParameter("horIni", horaInicial);
			    
			    Integer result6 = deleteRatRegisters.executeUpdate();
			    QLAdapter.log.warn("R064RAT  " + debugStr + " - DELETED: " + result6);
			    
			    this.transaction.commit();
			    String txt = "";
			    if (!descricaoCobertura.equals(""))
			    {
				txt = "Cobertura do Tipo " + descricaoCobertura + ", no Período de " + dataCobInicio + " até " + dataCobFim + " Excluída";
				this.saveLog(txt, null, numcpf, "colaborador", null);
			    }
			}

		}
		catch (ParseException e)
		{
			log.warn("Não foi possivel a conversão de data");
			e.printStackTrace();
		}
		catch (Exception ex)
		{
			if (this.transaction.isActive())
			{
				this.transaction.rollback();
			}
			ex.printStackTrace();
		}

		if (imprimeMsg)
		{
			out.print(result);
		}
	}

	public void deleteRegistroCoberturaTIDB(String numEmp, String tipCol, String numCad, String dataInicial, String horaInicial, String numEmpCob, String tipColCob, String numCadCob, boolean imprimeMsg, String numcpf, String descricaoCobertura, String dataCobFim, String dataCobInicio, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		EntityManager entityManager1 = PersistEngine.getEntityManager("TIDB");
		try
		{

			//Excluir Autorizacao de Hora Extra
			String deleteHoraExtra = " DELETE FROM R064EXT WHERE numemp = :numEmp AND tipcol = :tipCol AND numcad = :numCad AND CONVERT(varchar, datIni, 103) = :datIni ";
			Query deleteHERegisters = entityManager1.createNativeQuery(deleteHoraExtra);
			deleteHERegisters.setParameter("numEmp", numEmp);
			deleteHERegisters.setParameter("tipCol", tipCol);
			deleteHERegisters.setParameter("numCad", numCad);
			deleteHERegisters.setParameter("datIni", dataInicial);

			Integer result = deleteHERegisters.executeUpdate();
			QLAdapter.log.warn("R064EXT[TIDB] - DELETED: " + result);

		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception(e);
		}

	}

	private void consultaCoberturas(String colaborador, String colaboradorSub, String numLoc, String tabOrg, String dataInicialDe, String dataInicialAte, String dataFinalDe, String dataFinalAte, HttpServletRequest request, HttpServletResponse response)
	{
		PrintWriter out = null;
		try
		{

			List<EntradaAutorizadaVO> entradasAutorizadas = QLPresencaUtils.getListaCoberturas(colaborador, colaboradorSub, numLoc, tabOrg, dataInicialDe, dataInicialAte, dataFinalDe, dataFinalAte);

			Collections.sort(entradasAutorizadas, new CoberturaComparator());

			response.setHeader("Pragma", "no-cache");
			response.setHeader("Cache-Control", "no-cache");
			response.setDateHeader("Expires", 0);
			out = response.getWriter();

			out.println("<fieldset>");
			out.println("<table class='gridbox' cellpadding='0' cellspacing='0' style='empty-cells: show' >");

			out.println("<thead>");

			out.println("<tr>");
			out.println("<th>#</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Cobertura") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Emp.") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Matrícula") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Colaborador") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Cargo") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Período") + "</th>");
			out.println("<th>" + NeoUtils.encodeHTMLValue("Excluir") + "</th>");

			out.println("</thead>");
			out.println("<tbody>");

			int count = 1;
			if (entradasAutorizadas.size() <= 0)
			{
				out.println("<tr>");
				out.println("<td>" + count + "</td>");
				out.println("<td> Não existe cobertura para os dados informados! </td>");
				out.println("<td><br></td>");
				out.println("<td><br></td>");
				out.println("<td><br></td>");
				out.println("<td><br></td>");
				out.println("<td><br></td>");

				out.println("</tr>");
			}
			else
			{
				for (EntradaAutorizadaVO entrada : entradasAutorizadas)
				{
					out.println("<tr>");
					out.println("<td>" + count + "</td>");
					out.println("<td>" + entrada.getDescricaoCobertura() + "</td>");
					out.println("<td>" + entrada.getColaborador().getNumeroEmpresa() + "</td>");
					out.println("<td>" + entrada.getColaborador().getNumeroCadastro() + "</td>");
					out.println("<td>" + entrada.getColaborador().getNomeColaborador() + "</td>");
					out.println("<td>" + entrada.getColaborador().getCargo() + "</td>");
					out.println("<td>" + NeoUtils.safeDateFormat(entrada.getDataInicial(), "dd/MM/yyyy HH:mm") + " a" + NeoUtils.safeDateFormat(entrada.getDataFinal(), "dd/MM/yyyy HH:mm") + "<br></td>");
					out.println("<td>" + entrada.getBtnDelete() + "</td>");
					out.println("</tr>");
					count++;
				}
			}

			out.println("</tbody>");
			out.println("</table>");
			out.println("</fieldset>");
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
	}

	public void visualizaImagem(String numemp, String tipcol, String numcad, HttpServletRequest request, HttpServletResponse response) throws IOException
	{

		String numEmp = request.getParameter("numEmp");
		String tipCol = request.getParameter("tipCol");
		String numCad = request.getParameter("numCad");
		PreparedStatement st = null;
		try
		{
			Connection connVetorh = PersistEngine.getConnection("VETORH");
			byte[] imgData = null;
			StringBuffer sqlFoto = new StringBuffer();
			sqlFoto.append(" SELECT FotEmp FROM R034FOT WHERE NumEmp = " + numEmp + " AND TipCol = " + tipCol + " AND NumCad = " + numCad);
			st = connVetorh.prepareStatement(sqlFoto.toString());
			ResultSet rs = st.executeQuery();
			if (rs.next())
			{
				imgData = rs.getBytes("FotEmp");
			}
			rs.close();
			st.close();
			response.setContentType("image/jpeg");
			response.setContentLength(imgData.length);
			response.getOutputStream().write(imgData);
			response.getOutputStream().flush();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void listaAfastamentos(String numemp, String tipcol, String numcad, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<AfastamentoVO> afastamentos = new ArrayList<AfastamentoVO>();
		afastamentos = QLPresencaUtils.listaAfastamentos(Long.parseLong(numemp), Long.parseLong(tipcol), Long.parseLong(numcad));

		JSONArray jsonAfastamentos = new JSONArray();

		for (AfastamentoVO afa : afastamentos)
		{
			JSONObject jsonAfastamento = new JSONObject();
			jsonAfastamento.put("numemp", afa.getColaborador().getNumeroEmpresa());
			jsonAfastamento.put("tipcol", afa.getColaborador().getTipoColaborador());
			jsonAfastamento.put("numcad", afa.getColaborador().getNumeroCadastro());
			jsonAfastamento.put("datafa", NeoUtils.safeDateFormat(afa.getDataInicio(), "dd/MM/yyyy"));
			jsonAfastamento.put("datter", NeoUtils.safeDateFormat(afa.getDataFinal(), "dd/MM/yyyy"));
			jsonAfastamento.put("obsafa", afa.getObservacao());
			jsonAfastamento.put("dessit", afa.getSituacao());
			jsonAfastamento.put("link", afa.getLink());
			jsonAfastamento.put("excluir", afa.getExcluir());

			jsonAfastamentos.put(jsonAfastamento);
		}
		System.out.println("[" + GregorianCalendar.getInstance().getTimeInMillis() + "] numcad:" + numcad + ", numemp:" + numemp + ", tipcol:" + tipcol + " afastamentos encontrados: " + jsonAfastamentos);
		out.print(jsonAfastamentos);
		out.flush();
		out.close();
	}

	public void listaObsHorista(String numemp, String tipcol, String numcad, String numcpf, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<ObsHoristaVO> observacoesHoristas = new ArrayList<ObsHoristaVO>();
		observacoesHoristas = QLPresencaUtils.listaObsHoristas(Long.parseLong(numemp), Long.parseLong(tipcol), Long.parseLong(numcad), numcpf);

		JSONArray jsonObservacoes = new JSONArray();

		for (ObsHoristaVO observacao : observacoesHoristas)
		{
			JSONObject jsonObservacao = new JSONObject();
			jsonObservacao.put("numEmpHor", observacao.getNumEmpHor());
			jsonObservacao.put("tipcol", observacao.getTipCol());
			jsonObservacao.put("numCadHor", observacao.getNumCadHor());
			jsonObservacao.put("dataRegistro", observacao.getDataRegistro());
			long qtdHorTotal = observacao.getHoraRegistro();
			long qtdhora = qtdHorTotal / 60;
			double qtdmin = ((qtdHorTotal / 60.0) - qtdhora) * 60;
			int horInt = (int) qtdmin;
			String horMIm = qtdhora + ":";
			if (horInt < 10)
			{
				horMIm += "0" + horInt;
			}
			else
			{
				horMIm += horInt;
			}

			jsonObservacao.put("horRegistro", horMIm);
			jsonObservacao.put("obsHorista", observacao.getObservacao());
			jsonObservacao.put("linkExcluir", observacao.getLinkExcluir());
			jsonObservacoes.put(jsonObservacao);
		}
		System.out.println("[" + GregorianCalendar.getInstance().getTimeInMillis() + "] numcad:" + numcad + ", numemp:" + numemp + ", tipcol:" + tipcol + " Observacoes encontradas para o Horista: " + jsonObservacoes);
		out.print(jsonObservacoes);
		out.flush();
		out.close();
	}

	public void listaAnotacoes(String numemp, String tipcol, String numcad, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<AnotacaoVO> anotacaos = new ArrayList<AnotacaoVO>();
		anotacaos = QLPresencaUtils.listaAnotacoes(Long.parseLong(numemp), Long.parseLong(tipcol), Long.parseLong(numcad));

		JSONArray jsonAnotacoes = new JSONArray();

		for (AnotacaoVO ano : anotacaos)
		{
			JSONObject jsonAnotacao = new JSONObject();
			jsonAnotacao.put("numemp", ano.getColaborador().getNumeroEmpresa());
			jsonAnotacao.put("tipcol", ano.getColaborador().getTipoColaborador());
			jsonAnotacao.put("numcad", ano.getColaborador().getNumeroCadastro());
			jsonAnotacao.put("datnot", NeoUtils.safeDateFormat(ano.getData(), "dd/MM/yyyy"));
			jsonAnotacao.put("desnot", ano.getDescricao());
			jsonAnotacao.put("tipnot", ano.getTipoAnotacao());

			jsonAnotacoes.put(jsonAnotacao);
		}
		out.print(jsonAnotacoes);
		out.flush();
		out.close();
	}

	public void listaCoberturas(String numemp, String tipcol, String numcad, HttpServletResponse response) throws JSONException, IOException//foi este dd/MM/yyyy HH:mm
	{
		PrintWriter out = response.getWriter();
		List<EntradaAutorizadaVO> coberturas = new ArrayList<EntradaAutorizadaVO>();
		coberturas = QLPresencaUtils.listaCoberturas(Long.parseLong(numemp), Long.parseLong(tipcol), Long.parseLong(numcad));

		JSONArray jsonCoberturas = new JSONArray();

		for (EntradaAutorizadaVO cob : coberturas)
		{
			JSONObject jsonCobertura = new JSONObject();
			jsonCobertura.put("tipcob", cob.getDescricaoCobertura());

			if (cob.getColaboradorSubstituido().getNomeColaborador() != null)
			{
				jsonCobertura.put("nomcob", cob.getColaboradorSubstituido().getNumeroEmpresa() + "/" + cob.getColaboradorSubstituido().getNumeroCadastro() + " - " + cob.getColaboradorSubstituido().getNomeColaborador());
			}

			jsonCobertura.put("percob", NeoUtils.safeDateFormat(cob.getDataInicial(), "dd/MM/yyyy HH:mm") + " à " + NeoUtils.safeDateFormat(cob.getDataFinal(), "dd/MM/yyyy HH:mm"));
			jsonCobertura.put("esccob", cob.getColaboradorSubstituido().getEscala().getDescricao());
			jsonCobertura.put("loccob", cob.getPosto().getLotacao() + " - " + cob.getPosto().getNomePosto() + " - CC: " + cob.getPosto().getCentroCusto());
			jsonCobertura.put("obscob", cob.getObservacao());
			jsonCobertura.put("excluir", cob.getExcluir());
			if (cob.getDataIniRet() != null && cob.getDataFimRet() != null){
			    jsonCobertura.put("perretcob", NeoUtils.safeDateFormat(cob.getDataIniRet(), "dd/MM/yyyy HH:mm") + " à " + NeoUtils.safeDateFormat(cob.getDataFimRet(), "dd/MM/yyyy HH:mm"));
			}

			jsonCoberturas.put(jsonCobertura);
		}
		out.print(jsonCoberturas);
		out.flush();
		out.close();
	}

	public void listaDocumentos(String numcpf, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<DocumentoTreeVO> documentos = new ArrayList<DocumentoTreeVO>();
		documentos = QLPresencaUtils.listaDocumentoTree(Long.parseLong(numcpf));

		Gson gson = new Gson();
		String jsonDocumentos = gson.toJson(documentos);

		out.print(jsonDocumentos);
		out.flush();
		out.close();
	}

	public void listaHistoricoLocal(String numemp, String tipcol, String numcad, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<HistoricoLocalVO> locais = new ArrayList<HistoricoLocalVO>();
		locais = QLPresencaUtils.listaHistoricoLocal(Long.parseLong(numemp), Long.parseLong(tipcol), Long.parseLong(numcad));

		JSONArray jsonLocais = new JSONArray();

		for (HistoricoLocalVO loc : locais)
		{
			JSONObject jsonLocal = new JSONObject();
			jsonLocal.put("numemp", loc.getColaborador().getNumeroEmpresa());
			jsonLocal.put("tipcol", loc.getColaborador().getTipoColaborador());
			jsonLocal.put("numcad", loc.getColaborador().getNumeroCadastro());
			jsonLocal.put("posto", loc.getPosto().getLotacao() + " - " + loc.getPosto().getNomePosto());
			jsonLocal.put("cc", loc.getPosto().getCentroCusto());
			if (loc.getPosto().getEntradasAutorizadas().size() > 0 && loc.getPosto().getEntradasAutorizadas() != null)
			{
				Iterator i = loc.getPosto().getEntradasAutorizadas().iterator();
				while (i.hasNext())
				{
					EntradaAutorizadaVO entAut = ((EntradaAutorizadaVO) i.next());
					jsonLocal.put("Observacoes", entAut.getObservacao());
				}
			}
			jsonLocal.put("taborg", loc.getPosto().getCodigoOrganograma());
			jsonLocal.put("datalt", NeoUtils.safeDateFormat(loc.getData(), "dd/MM/yyyy"));

			jsonLocais.put(jsonLocal);
		}
		out.print(jsonLocais);
		out.flush();
		out.close();
	}

	public void listaHistoricoCargo(String numemp, String tipcol, String numcad, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<HistoricoCargoVO> cargos = new ArrayList<HistoricoCargoVO>();
		cargos = QLPresencaUtils.listaHistoricoCargo(Long.parseLong(numemp), Long.parseLong(tipcol), Long.parseLong(numcad));

		JSONArray jsonCargos = new JSONArray();

		for (HistoricoCargoVO car : cargos)
		{
			JSONObject jsonCargo = new JSONObject();
			jsonCargo.put("numemp", car.getColaborador().getNumeroEmpresa());
			jsonCargo.put("tipcol", car.getColaborador().getTipoColaborador());
			jsonCargo.put("numcad", car.getColaborador().getNumeroCadastro());
			jsonCargo.put("titcar", car.getCargo());
			jsonCargo.put("datalt", NeoUtils.safeDateFormat(car.getData(), "dd/MM/yyyy"));

			jsonCargos.put(jsonCargo);
		}
		out.print(jsonCargos);
		out.flush();
		out.close();
	}

	public void listaHistoricoEscala(String numemp, String tipcol, String numcad, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<HistoricoEscalaVO> escalas = new ArrayList<HistoricoEscalaVO>();
		escalas = QLPresencaUtils.listaHistoricoEscala(Long.parseLong(numemp), Long.parseLong(tipcol), Long.parseLong(numcad));

		JSONArray jsonEscalas = new JSONArray();

		for (HistoricoEscalaVO esc : escalas)
		{
			JSONObject jsonEscala = new JSONObject();
			jsonEscala.put("numemp", esc.getColaborador().getNumeroEmpresa());
			jsonEscala.put("tipcol", esc.getColaborador().getTipoColaborador());
			jsonEscala.put("numcad", esc.getColaborador().getNumeroCadastro());
			jsonEscala.put("codesc", esc.getEscala().getCodigoEscala());
			jsonEscala.put("desesc", esc.getEscala().getDescricao());
			jsonEscala.put("datalt", NeoUtils.safeDateFormat(esc.getData(), "dd/MM/yyyy"));
			jsonEscala.put("excluir", esc.getExcluir());

			jsonEscalas.put(jsonEscala);
		}
		out.print(jsonEscalas);
		out.flush();
		out.close();
	}

	public void listaCompetencias(String numcpf, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<String> competencias = new ArrayList<String>();
		competencias = QLPresencaUtils.listaCompetencias(Long.parseLong(numcpf));

		JSONArray jsonCompetencias = new JSONArray();

		String selected = "";

		for (String comp : competencias)
		{
			JSONObject jsonCompetencia = new JSONObject();
			jsonCompetencia.put("competencia", comp);

			if (selected.equals(""))
			{
				selected = "selected";
				jsonCompetencia.put(selected, "true");
			}
			jsonCompetencias.put(jsonCompetencia);
		}
		out.print(jsonCompetencias);
		out.flush();
		out.close();
	}

	public void listaCompetenciasApuracao(String numemp, String tipcol, String numcad, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<String> competencias = new ArrayList<String>();
		competencias = QLPresencaUtils.listaCompetenciasApuracao(Long.parseLong(numemp), Long.parseLong(tipcol), Long.parseLong(numcad));

		JSONArray jsonCompetencias = new JSONArray();

		String selected = "";

		for (String comp : competencias)
		{
			JSONObject jsonCompetencia = new JSONObject();
			jsonCompetencia.put("competencia", comp);

			if (selected.equals(""))
			{
				selected = "selected";
				jsonCompetencia.put(selected, "true");
			}
			jsonCompetencias.put(jsonCompetencia);
		}
		out.print(jsonCompetencias);
		out.flush();
		out.close();
	}

	public void listaApuracoes(String numemp, String tipcol, String numcad, String competencia, HttpServletResponse response) throws JSONException, IOException, ParseException
	{
		PrintWriter out = response.getWriter();
		List<ApuracaoVO> apuracoes = new ArrayList<ApuracaoVO>();
		GregorianCalendar perRef = new GregorianCalendar();
		GregorianCalendar dataInicio = new GregorianCalendar();
		GregorianCalendar dataFim = new GregorianCalendar();

		//String competenciaAtual = OrsegupsUtils.getCompetencia(dataInicio);

		List<String> competencias = new ArrayList<String>();
		competencias = QLPresencaUtils.listaCompetencias(null);
		String competenciaAtual = competencias.get(0);

		System.out.println("Competencia Selecionada:" + competencia);
		CompetenciaVO cpt = QLPresencaUtils.getCompetenciaR044Cal(competencia);

		dataInicio = cpt.getIniApu();
		dataFim = cpt.getFimApu();
		//perRef = cpt.getPerRef();

		/*
		 * dataInicio = OrsegupsUtils.getDiaInicioCompetencia(competencia);
		 * dataFim = OrsegupsUtils.getDiaFimCompetencia(competencia);
		 */

		System.out.println("datini datfim->" + NeoUtils.safeDateFormat(dataInicio) + " - " + NeoUtils.safeDateFormat(dataFim));

		if (competencia.equals(competenciaAtual))
		{
			dataFim.add(Calendar.DAY_OF_MONTH, -6);
		}

		apuracoes = QLPresencaUtils.listaApuracoes(Long.parseLong(numemp), Long.parseLong(tipcol), Long.parseLong(numcad), dataInicio, dataFim);

		Gson gson = new Gson();
		String apuracaoJson = gson.toJson(apuracoes);

		out.print(apuracaoJson);
		out.flush();
		out.close();
	}

	public void buscaApuracao(String datapu, String numemp, String tipcol, String numcad, HttpServletResponse response) throws JSONException, IOException, ParseException
	{
		try
		{
			PrintWriter out = response.getWriter();
			List<ApuracaoVO> apuracoes = new ArrayList<ApuracaoVO>();
			GregorianCalendar dataApu = new GregorianCalendar();
			GregorianCalendar dataInicio = new GregorianCalendar();
			GregorianCalendar dataFim = new GregorianCalendar();

			dataApu = OrsegupsUtils.stringToGregorianCalendar(datapu, "dd/MM/yyyy");
			String competencia = OrsegupsUtils.getCompetencia(dataApu);
			dataInicio = OrsegupsUtils.getDiaInicioCompetencia(competencia);
			dataFim = OrsegupsUtils.getDiaFimCompetencia(competencia);

			apuracoes = QLPresencaUtils.listaApuracoes(Long.valueOf(numemp), Long.valueOf(tipcol), Long.valueOf(numcad), dataApu, dataApu);
			apuracoes.get(0).getColaborador().setQdeExcecao(QLPresencaUtils.contaExcecaoApuracao(Long.valueOf(numemp), Long.valueOf(tipcol), Long.valueOf(numcad), dataInicio, dataFim));

			Gson gson = new Gson();
			String apuracaoJson = gson.toJson(apuracoes.get(0));

			out.print(apuracaoJson);
			out.flush();
			out.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	/*
	 * public void listaSituacaoCompetencia(String numemp, String tipcol, String numcad,
	 * HttpServletResponse response) throws JSONException, IOException
	 * {
	 * PrintWriter out = response.getWriter();
	 * Connection connVetorh = PersistEngine.getConnection("VETORH");
	 * JSONArray jsonApuracoes = new JSONArray();
	 * String competencia = "12/2013";
	 * try
	 * {
	 * GregorianCalendar dataInicio = OrsegupsUtils.getDiaInicioCompetencia(competencia);
	 * GregorianCalendar dataFim = OrsegupsUtils.getDiaFimCompetencia(competencia);
	 * StringBuffer sqlSituacao = new StringBuffer();
	 * sqlSituacao.append(" SELECT sit.CodSit, sto.DesSit, SUM(sit.QtdHor) AS TotHor ");
	 * sqlSituacao.append(" FROM R066APU apu ");
	 * sqlSituacao.append(
	 * " INNER JOIN R066SIT sit ON sit.NumEmp = apu.NumEmp AND sit.TipCol = apu.TipCol AND sit.NumCad = apu.NumCad AND sit.DatApu = apu.DatApu "
	 * );
	 * sqlSituacao.append(" INNER JOIN R010SIT sto ON sto.CodSit = sit.CodSit ");
	 * sqlSituacao.append(
	 * " WHERE apu.NumEmp = ? AND apu.TipCol = ? AND apu.NumCad = ? AND apu.DatApu >= ? AND apu.DatApu <= ? "
	 * );
	 * sqlSituacao.append(" GROUP BY sit.CodSit, sto.DesSit ");
	 * sqlSituacao.append(" ORDER BY sit.CodSit ");
	 * PreparedStatement stSituacao = connVetorh.prepareStatement(sqlSituacao.toString());
	 * stSituacao.setLong(1, Long.parseLong(numemp));
	 * stSituacao.setLong(2, Long.parseLong(tipcol));
	 * stSituacao.setLong(3, Long.parseLong(numcad));
	 * stSituacao.setTimestamp(4, new Timestamp(dataInicio.getTimeInMillis()));
	 * stSituacao.setTimestamp(5, new Timestamp(dataFim.getTimeInMillis()));
	 * ResultSet rsSituacao = stSituacao.executeQuery();
	 * //Situacaos
	 * while (rsSituacao.next())
	 * {
	 * JSONObject jsonApuracao = new JSONObject();
	 * jsonApuracao.put("codsit", rsSituacao.getLong("CodSit"));
	 * jsonApuracao.put("dessit", rsSituacao.getString("DesSit"));
	 * jsonApuracao.put("tothor", OrsegupsUtils.getHorario(rsSituacao.getLong("TotHor")));
	 * jsonApuracoes.put(jsonApuracao);
	 * }
	 * rsSituacao.close();
	 * stSituacao.close();
	 * }
	 * catch (Exception e)
	 * {
	 * e.printStackTrace();
	 * }
	 * finally
	 * {
	 * try
	 * {
	 * connVetorh.close();
	 * }
	 * catch (SQLException e)
	 * {
	 * e.printStackTrace();
	 * }
	 * }
	 * out.print(jsonApuracoes);
	 * out.flush();
	 * out.close();
	 * }
	 */

	public void listaAcessos(String numcpf, String competencia, String exibirExcecao, HttpServletResponse response) throws JSONException, IOException, ParseException
	{
		PrintWriter out = response.getWriter();
		List<AcessoVO> acessos = new ArrayList<AcessoVO>();
		GregorianCalendar perRef = new GregorianCalendar();
		GregorianCalendar dataInicio = new GregorianCalendar();
		GregorianCalendar dataFim = new GregorianCalendar();

		if (competencia == null || competencia.equals(""))
		{
			competencia = OrsegupsUtils.getCompetencia(dataInicio);
		}

		//competencia = "01/2013";

		CompetenciaVO cpt = QLPresencaUtils.getCompetenciaR044Cal(competencia);

		dataInicio = cpt.getIniApu();
		dataFim = cpt.getFimApu();
		perRef = cpt.getPerRef();

		acessos = QLPresencaUtils.listaAcessos(Long.parseLong(numcpf), perRef, dataInicio, dataFim, exibirExcecao);

		JSONArray jsonAcessos = new JSONArray();

		for (AcessoVO acc : acessos)
		{
			JSONObject jsonAcesso = new JSONObject();
			jsonAcesso.put("dataAnt", NeoUtils.safeDateFormat(acc.getData()));
			jsonAcesso.put("dataNew", NeoUtils.safeDateFormat(acc.getData()));
			jsonAcesso.put("diaSemana", OrsegupsUtils.getDayOfWeek(acc.getData()));
			jsonAcesso.put("sequencia", acc.getSequencia());
			jsonAcesso.put("direcao", acc.getDirecao());
			jsonAcesso.put("tipoAcesso", acc.getTipoAcesso());
			jsonAcesso.put("tipoDirecao", acc.getTipoDirecao());
			jsonAcesso.put("relogio", acc.getRelogio());
			jsonAcesso.put("numeroLocal", acc.getNumeroLocal());
			jsonAcesso.put("codigoOrganograma", acc.getCodigoOrganograma());
			jsonAcesso.put("permanenciaPosto", acc.getPermanenciaPosto());
			jsonAcesso.put("fone", acc.getFone());
			jsonAcesso.put("justificativaAcesso", acc.getJustificativaAcesso());
			jsonAcesso.put("obsAcesso", acc.getObsAcesso());
			jsonAcesso.put("excluir", acc.getExcluir());
			jsonAcessos.put(jsonAcesso);
		}
		out.print(jsonAcessos);
		out.flush();
		out.close();
	}

	public void listaCompetenciasFinancas(String numcpf, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<String> competencias = new ArrayList<String>();
		competencias = QLPresencaUtils.listaCompetenciasFinacas(Long.parseLong(numcpf));

		JSONArray jsonCompetencias = new JSONArray();

		String selected = "";

		for (String comp : competencias)
		{
			JSONObject jsonCompetencia = new JSONObject();
			jsonCompetencia.put("competencia", comp);

			if (selected.equals(""))
			{
				selected = "selected";
				jsonCompetencia.put(selected, "true");
			}
			jsonCompetencias.put(jsonCompetencia);
		}
		out.print(jsonCompetencias);
		out.flush();
		out.close();
	}

	public void buscaContraCheque(String numcpf, String competencia, HttpServletResponse response) throws JSONException, IOException, ParseException
	{
		PrintWriter out = response.getWriter();
		ContraChequeVO contraCheque = new ContraChequeVO();

		//competencia = "01/2013";

		if (competencia != null && !competencia.equals(""))
		{
			contraCheque = QLPresencaUtils.buscaContraCheque(Long.parseLong(numcpf), OrsegupsUtils.stringToGregorianCalendar("01/" + competencia, "dd/MM/yyyy"));
		}

		Gson gson = new Gson();
		String chqJson = gson.toJson(contraCheque);

		out.print(chqJson);
		out.flush();
		out.close();
	}

	public void pieChartSituacoes(String numemp, String tipcol, String numcad, String competencia, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		Connection connVetorh = PersistEngine.getConnection("VETORH");

		//String competencia = "12/2013";

		StringBuilder txt = new StringBuilder();
		txt.append("[['Descrição', 'Quantidade'],");

		try
		{
			GregorianCalendar dataInicio = OrsegupsUtils.getDiaInicioCompetencia(competencia);
			GregorianCalendar dataFim = OrsegupsUtils.getDiaFimCompetencia(competencia);

			StringBuffer sqlSituacao = new StringBuffer();
			sqlSituacao.append(" SELECT sit.CodSit, sto.DesSit, SUM(sit.QtdHor) AS TotHor ");
			sqlSituacao.append(" FROM R066APU apu ");
			sqlSituacao.append(" INNER JOIN R066SIT sit ON sit.NumEmp = apu.NumEmp AND sit.TipCol = apu.TipCol AND sit.NumCad = apu.NumCad AND sit.DatApu = apu.DatApu ");
			sqlSituacao.append(" INNER JOIN R010SIT sto ON sto.CodSit = sit.CodSit ");
			sqlSituacao.append(" WHERE apu.NumEmp = ? AND apu.TipCol = ? AND apu.NumCad = ? AND apu.DatApu >= ? AND apu.DatApu <= ? ");
			sqlSituacao.append(" GROUP BY sit.CodSit, sto.DesSit ");
			sqlSituacao.append(" ORDER BY sit.CodSit ");

			PreparedStatement stSituacao = connVetorh.prepareStatement(sqlSituacao.toString());
			stSituacao.setLong(1, Long.parseLong(numemp));
			stSituacao.setLong(2, Long.parseLong(tipcol));
			stSituacao.setLong(3, Long.parseLong(numcad));
			stSituacao.setTimestamp(4, new Timestamp(dataInicio.getTimeInMillis()));
			stSituacao.setTimestamp(5, new Timestamp(dataFim.getTimeInMillis()));
			ResultSet rsSituacao = stSituacao.executeQuery();

			//Situacaos
			while (rsSituacao.next())
			{
				txt.append("['");
				txt.append(OrsegupsUtils.getHorario(rsSituacao.getLong("TotHor")) + " - " + rsSituacao.getString("DesSit"));
				txt.append("'");
				txt.append(",");
				txt.append(rsSituacao.getLong("TotHor"));
				txt.append("],");
			}

			txt.append("]");

			rsSituacao.close();
			stSituacao.close();

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				connVetorh.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		out.print(txt.toString());
		out.flush();
		out.close();
	}

	private void salvarAcerto(String jsonAcesso, String datace, String numemp, String tipcol, String numcad, HttpServletResponse response) throws IOException
	{
		try
		{
			Gson gson = new Gson();
			AcessoListaVO acessoLista = gson.fromJson(jsonAcesso, AcessoListaVO.class);
			NeoUser user = PortalUtil.getCurrentUser();

			if (NeoUtils.safeIsNotNull(acessoLista) && NeoUtils.safeIsNotNull(acessoLista.getRows()))
			{
				int linha = 1;

				for (AcessoVO acc : acessoLista.getRows())
				{
					if (acc.getOperacao() != null && !acc.getOperacao().equals(""))
					{
						CalculoApuracaoAcertarOut acertarOut = new CalculoApuracaoAcertarOut();
						CalculoApuracaoAcertarIn acertarIn = new CalculoApuracaoAcertarIn();
						CalculoApuracaoAcertarInTMarDat[] acertarInTMarDat = new CalculoApuracaoAcertarInTMarDat[1];
						CalculoApuracaoAcertarInTMarDat marDat = new CalculoApuracaoAcertarInTMarDat();

						List<JustificativaMarcacaoVO> justificativa = QLPresencaUtils.listaJustificativaMarcacao(acc.getCodigoJustificativa());
						GregorianCalendar dataAtual = new GregorianCalendar();

						marDat.setCodJMa(1);
						marDat.setCodPlt(2);
						marDat.setCodRlg(2);
						marDat.setDatMar(acc.getDataAcesso());
						marDat.setHorMar(acc.getHoraAcesso());
						marDat.setObsJMa(NeoUtils.safeDateFormat(dataAtual) + " - " + user.getCode() + " - " + justificativa.get(0).getDesJma());
						marDat.setOriMar("R");
						marDat.setTipOpe(acc.getOperacao());
						marDat.setUsoMar(2);
						marDat.setDirMar("E");
						marDat.setLinha(linha);

						if (acc.getOperacao().equals("I"))
						{
							marDat.setMarcOri("N");
						}
						else
						{
							marDat.setMarcOri("S");
						}
						acertarInTMarDat[0] = marDat;

						acertarIn.setDatAce(datace);
						acertarIn.setNumEmp(Integer.valueOf(numemp));
						acertarIn.setTipCol(Integer.valueOf(tipcol));
						acertarIn.setExpPes(Integer.valueOf(numcad));
						acertarIn.setTipOpe("AG");
						acertarIn.setTMarDat(acertarInTMarDat);
						Ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPx calculoApuracaoPx = new Ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPx();
						acertarOut = calculoApuracaoPx.acertar(OrsegupsUtils.WS_LOGIN, OrsegupsUtils.WS_SENHA, 0, acertarIn);
						String erro = acertarOut.getErroExecucao();
						if (erro != null && !erro.equals(""))
						{
							log.warn("ERRO NO WEBSERVICE RONDA - " + erro + " - " + acertarOut.getMsgRet());
						}
					}
					linha++;
				}
				this.buscaApuracao(datace, numemp, tipcol, numcad, response);
			}

		}
		catch (RemoteException e)
		{
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void moverMarcacao(String datapu, String datacc, String horacc, String seqacc, String numemp, String tipcol, String numcad, String op, HttpServletResponse response) throws IOException
	{
		try
		{
			GregorianCalendar gDataApu = OrsegupsUtils.stringToGregorianCalendar(datapu, "dd/MM/yyyy");
			GregorianCalendar gDataAcc = OrsegupsUtils.stringToGregorianCalendar(datacc + " " + horacc, "dd/MM/yyyy HH:mm");
			if (op.equals("anterior"))
			{
				gDataApu.add(Calendar.DAY_OF_MONTH, -1);
			}
			else
			{
				gDataApu.add(Calendar.DAY_OF_MONTH, +1);
			}
			String retorno = QLPresencaUtils.moverMarcacao(gDataApu, gDataAcc, horacc, Long.valueOf(seqacc), Long.valueOf(numemp), Long.valueOf(tipcol), Long.valueOf(numcad), op);

			if (!retorno.startsWith("Erro"))
			{
				this.apurarAcerto(datapu, numemp, tipcol, numcad, response);
				this.apurarAcerto(NeoUtils.safeDateFormat(gDataApu, "dd/MM/yyyy"), numemp, tipcol, numcad, response);

				String competencia = OrsegupsUtils.getCompetencia(gDataApu);

				this.listaApuracoes(numemp, tipcol, numcad, competencia, response);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void recalcularApuracao(String competencia, String numemp, String numcad, HttpServletResponse response) throws IOException
	{
		PrintWriter out = response.getWriter();

		try
		{
			CalculoApuracaoCalcularOut calcularOut = new CalculoApuracaoCalcularOut();
			CalculoApuracaoCalcularIn calcularIn = new CalculoApuracaoCalcularIn();

			GregorianCalendar periodo = OrsegupsUtils.stringToGregorianCalendar("01/" + competencia, "dd/MM/yyyy");

			QLEqualsFilter filtroPeriodo = new QLEqualsFilter("perref", periodo);
			QLEqualsFilter filtroEmpresa = new QLEqualsFilter("numemp", Long.valueOf(numemp));
			QLEqualsFilter filtroTipo = new QLEqualsFilter("tipcal", 11L);

			QLGroupFilter filtroCalculo = new QLGroupFilter("AND");
			filtroCalculo.addFilter(filtroPeriodo);
			filtroCalculo.addFilter(filtroEmpresa);
			filtroCalculo.addFilter(filtroTipo);

			List<NeoObject> calculo = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORHRCAL"), filtroCalculo);

			Long codcal = 0L;

			if (NeoUtils.safeIsNotNull(calculo))
			{
				EntityWrapper wrapperCalculo = new EntityWrapper(calculo.get(0));
				codcal = (Long) wrapperCalculo.findField("codcal").getValue();
			}

			calcularIn.setNumEmp(Integer.valueOf(numemp));
			calcularIn.setAbrEmp(numemp);
			calcularIn.setAbrCad(numcad);
			calcularIn.setProApu("1");
			calcularIn.setCodCal(codcal.intValue());
			calcularIn.setReaMar("N");
			calcularIn.setDesAce("S");

			Ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPx calculoApuracaoPx = new Ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPx();
			calcularOut = calculoApuracaoPx.calcular(OrsegupsUtils.WS_LOGIN, OrsegupsUtils.WS_SENHA, 0, calcularIn);

			String erro = calcularOut.getErroExecucao();

			if (erro != null && !erro.equals(""))
			{
				log.warn("ERRO NO WEBSERVICE RONDA - " + erro);
			}
			this.listaApuracoes(numemp, "1", numcad, competencia, response);

		}
		catch (RemoteException e)
		{
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}
	}

	private void apurarAcerto(String datace, String numemp, String tipcol, String numcad, HttpServletResponse response)
	{
		try
		{
			CalculoApuracaoAcertarOut acertarOut = new CalculoApuracaoAcertarOut();
			CalculoApuracaoAcertarIn acertarIn = new CalculoApuracaoAcertarIn();

			acertarIn.setDatAce(datace);
			acertarIn.setNumEmp(Integer.valueOf(numemp));
			acertarIn.setTipCol(Integer.valueOf(tipcol));
			acertarIn.setExpPes(Integer.valueOf(numcad));
			acertarIn.setTipOpe("A");
			Ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPx calculoApuracaoPx = new Ronda_Synccom_senior_g5_rh_hr_calculoApuracaoPx();
			acertarOut = calculoApuracaoPx.acertar(OrsegupsUtils.WS_LOGIN, OrsegupsUtils.WS_SENHA, 0, acertarIn);
			String erro = acertarOut.getErroExecucao();
			if (erro != null && !erro.equals(""))
			{
				log.warn("ERRO NO WEBSERVICE RONDA - " + erro);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void listaEmpresas(HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();

		List<NeoObject> empresas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("EEMP"));

		Long numEmp = 0L;
		String nomEmp = "";

		JSONArray jsonCompetencias = new JSONArray();

		for (NeoObject neoEmpresa : empresas)
		{
			EntityWrapper empresasWrapper = new EntityWrapper(neoEmpresa);
			numEmp = (Long) empresasWrapper.findField("codemp").getValue();
			nomEmp = (String) empresasWrapper.findField("nomemp").getValue();
			boolean valida = (numEmp.intValue() < 20 && !nomEmp.contains("INATIVO"));
			if (valida)
			{
				JSONObject jsonCompetencia = new JSONObject();
				jsonCompetencia.put("codemp", String.valueOf(numEmp));
				jsonCompetencia.put("nomemp", nomEmp);
				jsonCompetencias.put(jsonCompetencia);
			}
		}

		out.print(jsonCompetencias);
		out.flush();
		out.close();
	}

	public void listaInstaladores(HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();

		List<NeoObject> instaladores = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUTINS"));

		Long codIns = 0L;
		String nomIns = "";
		String tipIns = "";

		JSONArray jsonInstaladores = new JSONArray();

		for (NeoObject neoInstaladores : instaladores)
		{
			EntityWrapper instaladoresWrapper = new EntityWrapper(neoInstaladores);
			codIns = (Long) instaladoresWrapper.findField("usu_codins").getValue();
			nomIns = (String) instaladoresWrapper.findField("usu_nomins").getValue();
			tipIns = (String) instaladoresWrapper.findField("usu_instip").getValue();

			JSONObject jsonInstalador = new JSONObject();
			jsonInstalador.put("codins", String.valueOf(codIns));
			jsonInstalador.put("nomins", nomIns);
			jsonInstalador.put("instip", tipIns);
			jsonInstaladores.put(jsonInstalador);

		}

		out.print(jsonInstaladores);
		out.flush();
		out.close();
	}

	private void listaJustificativaMarcacao(HttpServletResponse response) throws IOException
	{
		PrintWriter out = response.getWriter();

		List<JustificativaMarcacaoVO> justificativas = new ArrayList<JustificativaMarcacaoVO>();
		justificativas = QLPresencaUtils.listaJustificativaMarcacao(0L);

		Gson gson = new Gson();
		String justificativaJson = gson.toJson(justificativas);

		out.print(justificativaJson);
		out.flush();
		out.close();
	}

	public void listaEquipamentos(String numctr, String numpos, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<EquipamentoVO> equipamentos = new ArrayList<EquipamentoVO>();
		equipamentos = QLPresencaUtils.listaEquipamentos(Long.parseLong(numctr), Long.parseLong(numpos));

		Gson gson = new Gson();
		String equipamentosJson = gson.toJson(equipamentos);

		out.print(equipamentosJson);
		out.flush();
		out.close();
	}

	public void listaChecklist(String numctr, String numpos, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<ChecklistVO> checklists = new ArrayList<ChecklistVO>();
		checklists = QLPresencaUtils.listaChecklist(Long.parseLong(numctr), Long.parseLong(numpos));

		Gson gson = new Gson();
		String checklistJson = gson.toJson(checklists);

		out.print(checklistJson);
		out.flush();
		out.close();
	}

	public void listaContaSigma(String numctr, String numpos, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<ContaSigmaVO> contas = new ArrayList<ContaSigmaVO>();
		contas = QLPresencaUtils.listaContasSigma(Long.parseLong(numctr), Long.parseLong(numpos));

		Gson gson = new Gson();
		String contaJson = gson.toJson(contas);

		out.print(contaJson);
		out.flush();
		out.close();
	}

	public void listaEventoX8(String numctr, String numpos, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<EventoSigmaVO> eventos = new ArrayList<EventoSigmaVO>();
		eventos = QLPresencaUtils.listaEventoX8(Long.parseLong(numctr), Long.parseLong(numpos));

		Gson gson = new Gson();
		String eventoJson = gson.toJson(eventos);

		out.print(eventoJson);
		out.flush();
		out.close();
	}

	public void listaEventosSigma(String cdCliente, String dias, String tipoEvento, HttpServletResponse response) throws IOException, ParseException
	{
		PrintWriter out = response.getWriter();
		List<EventoSigmaVO> eventos = new ArrayList<EventoSigmaVO>();
		GregorianCalendar dataAtual = new GregorianCalendar();
		eventos = QLPresencaUtils.listaEventosSigma(Long.valueOf(cdCliente), dataAtual, Long.valueOf(dias), tipoEvento);

		Gson gson = new Gson();
		String eventoJson = gson.toJson(eventos);

		out.print(eventoJson);
		out.flush();
		out.close();
	}

	public void listaChipGPRS(String numctr, String numpos, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<ChipGPRSVO> chips = new ArrayList<ChipGPRSVO>();
		chips = QLPresencaUtils.listaChipGPRS(Long.parseLong(numctr), Long.parseLong(numpos));

		Gson gson = new Gson();
		String chipJson = gson.toJson(chips);

		out.print(chipJson);
		out.flush();
		out.close();
	}

	public void listaSolicitacao(String codcli, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<SolicitacaoVO> solicitacoes = new ArrayList<SolicitacaoVO>();
		solicitacoes = QLPresencaUtils.listaSolicitacoes(Long.parseLong(codcli));

		Gson gson = new Gson();
		String solicitacaoJson = gson.toJson(solicitacoes);

		out.print(solicitacaoJson);
		out.flush();
		out.close();
	}

	public void listaReclamacao(String codcli, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<ReclamacaoVO> reclamacoes = new ArrayList<ReclamacaoVO>();
		reclamacoes = QLPresencaUtils.listaReclamacao(Long.parseLong(codcli));

		Gson gson = new Gson();
		String reclamacaoJson = gson.toJson(reclamacoes);

		out.print(reclamacaoJson);
		out.flush();
		out.close();
	}

	public void listaVisita(String codpai, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<VisitaVO> visitas = new ArrayList<VisitaVO>();
		visitas = QLPresencaUtils.listaVisita(codpai);

		Gson gson = new Gson();
		String visitaJson = gson.toJson(visitas);

		out.print(visitaJson);
		out.flush();
		out.close();
	}

	public void listaCoberturaPosto(String numloc, String taborg, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<EntradaAutorizadaVO> coberturas = new ArrayList<EntradaAutorizadaVO>();
		coberturas = QLPresencaUtils.listaCoberturaPosto(Long.parseLong(numloc), Long.parseLong(taborg));

		JSONArray jsonCoberturas = new JSONArray();

		for (EntradaAutorizadaVO cob : coberturas)
		{
			JSONObject jsonCobertura = new JSONObject();
			jsonCobertura.put("tipcob", cob.getDescricaoCobertura());
			jsonCobertura.put("percob", NeoUtils.safeDateFormat(cob.getDataInicial(), "dd/MM/yyyy") + " à " + NeoUtils.safeDateFormat(cob.getDataFinal(), "dd/MM/yyyy"));
			jsonCobertura.put("nomcob", cob.getColaborador().getNumeroEmpresa() + "/" + cob.getColaborador().getNumeroCadastro() + " - " + cob.getColaborador().getNomeColaborador());
			if (cob.getColaboradorSubstituido().getNomeColaborador() != null)
			{
				jsonCobertura.put("nomsub", cob.getColaboradorSubstituido().getNumeroEmpresa() + "/" + cob.getColaboradorSubstituido().getNumeroCadastro() + " - " + cob.getColaboradorSubstituido().getNomeColaborador());
			}
			jsonCobertura.put("obscob", cob.getObservacao());

			jsonCoberturas.put(jsonCobertura);
		}

		out.print(jsonCoberturas);
		out.flush();
		out.close();
	}

	public void insereHoraExtra(String numemp, String tipcol, String numcad, String data, HttpServletResponse response) throws IOException, JSONException, ParseException
	{
		QLPresencaUtils.insereHoraExtra(NeoUtils.safeLong(numemp), NeoUtils.safeLong(tipcol), NeoUtils.safeLong(numcad), data);
		this.apurarAcerto(data, numemp, tipcol, numcad, response);
		this.buscaApuracao(data, numemp, tipcol, numcad, response);
	}

	public void removeHoraExtra(String numemp, String tipcol, String numcad, String data, HttpServletResponse response) throws IOException, JSONException, ParseException
	{
		QLPresencaUtils.removeHoraExtra(NeoUtils.safeLong(numemp), NeoUtils.safeLong(tipcol), NeoUtils.safeLong(numcad), data);
		this.apurarAcerto(data, numemp, tipcol, numcad, response);
		this.buscaApuracao(data, numemp, tipcol, numcad, response);
	}

	public void validaExcecao(String op, String numemp, String tipcol, String numcad, String codsit, String data, HttpServletResponse response) throws IOException, JSONException, ParseException
	{
		this.entityManager = PersistEngine.getEntityManager("VETORH");

		try
		{
			String retorno = QLPresencaUtils.validaExcecao(op, NeoUtils.safeLong(numemp), NeoUtils.safeLong(tipcol), NeoUtils.safeLong(numcad), NeoUtils.safeLong(codsit), data);
			if (!retorno.startsWith("Erro"))
			{
				GregorianCalendar gDataApu = OrsegupsUtils.stringToGregorianCalendar(data, "dd/MM/yyyy");
				String competencia = OrsegupsUtils.getCompetencia(gDataApu);
				String descricaoOp = "";

				if (op.equals("N"))
				{
					descricaoOp = "retornada";
				}
				else if (op.equals("S"))
				{
					descricaoOp = "validada";
				}

				//LOG Colaborador
				String txtLog = "Exceção do colaborador " + numemp + "/" + numcad + " - ";
				BigInteger numcpf = null;
				String nomfun = "";

				StringBuilder buscaCpf = new StringBuilder();
				buscaCpf.append(" SELECT numcpf, nomfun FROM R034FUN WHERE numemp = :numemp ");
				buscaCpf.append(" AND tipcol = :tipcol ");
				buscaCpf.append(" AND numcad = :numcad ");

				Query queryBuscaCpf = this.entityManager.createNativeQuery(buscaCpf.toString());
				queryBuscaCpf.setParameter("numemp", numemp);
				queryBuscaCpf.setParameter("tipcol", tipcol);
				queryBuscaCpf.setParameter("numcad", numcad);

				Object obj = queryBuscaCpf.getSingleResult();
				if (NeoUtils.safeIsNotNull(obj))
				{
					Object[] resultado = (Object[]) obj;
					numcpf = (BigInteger) resultado[0];
					nomfun = (String) resultado[1];
				}

				if (numcpf != null && !numcpf.equals(""))
				{
					txtLog = txtLog + nomfun;
					txtLog = txtLog + " no dia " + data + " ";
					txtLog = txtLog + descricaoOp + ".";
					this.saveLog(txtLog, null, numcpf.toString(), "colaborador", null);
				}
				this.apurarAcerto(data, numemp, tipcol, numcad, response);
				this.listaApuracoes(numemp, tipcol, numcad, competencia, response);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void listaEscalaRevezamento(String codesc, String data, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<EscalaRevezamentoVO> revezamentos = new ArrayList<EscalaRevezamentoVO>();
		revezamentos = QLPresencaUtils.listaEscalaRevezamento(Long.valueOf(codesc), data);

		Gson gson = new Gson();
		String revezamentoJson = gson.toJson(revezamentos);

		out.print(revezamentoJson);
		out.flush();
		out.close();
	}

	public void deleteAfastamento(String numEmp, String tipCol, String numCad, String numCpf, String datAfa, HttpServletRequest request, HttpServletResponse response) throws IOException
	{

		this.entityManager = PersistEngine.getEntityManager("VETORH");
		this.transaction = this.entityManager.getTransaction();
		Integer result = 0;
		final PrintWriter out = response.getWriter();
		GregorianCalendar data = new GregorianCalendar();
		Connection conn = null;
		PreparedStatement pstm = null;
		try
		{
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			Date date = df.parse(datAfa);
			data.setTime(date);

			if (!this.transaction.isActive())
			{
				this.transaction.begin();
			}

			//----------------Inserir afastamento na fila de exclusao---------------
			
			

//			StringBuilder sql = new StringBuilder();
//
//			conn = PersistEngine.getConnection("VETORH");
//			    
//			sql.append(" INSERT INTO USU_T038NEXTI (usu_numemp, usu_tipcol, usu_numcad, usu_datafa, usu_horafa, usu_datter, usu_horter, usu_sitafa, usu_caudem, usu_nextid) ");
//			sql.append(" SELECT AFA.NumEmp, AFA.TipCol, AFA.NumCad, AFA.DatAfa, AFA.HorAfa, AFA.DatTer, AFA.HorTer, AFA.SitAfa, AFA.CauDem, AFA.usu_nextid FROM R038AFA AFA ");
//			sql.append(" WHERE AFA.NumEmp = ? AND AFA.TIPCOL = ? AND AFA.NumCad = ? AND CONVERT(varchar, AFA.DatAfa, 103) = ? ");
//			    
//			pstm = conn.prepareStatement(sql.toString());
//
//			pstm.setLong(1, Long.parseLong(numEmp));
//			pstm.setLong(2, Long.parseLong(tipCol));
//			pstm.setLong(3, Long.parseLong(numCad));
//			pstm.setString(4, datAfa);
//			    
//
//			pstm.executeUpdate();
			
			
			//----------------------------------------------------------------------
			
			String deleteCobertura = "DELETE FROM R038AFA WHERE NumEmp =:numEmp AND TipCol =:tipCol AND NumCad =:numCad AND CONVERT(varchar, DatAfa, 103) =:datAfa ";

			Query queryDeleteCobertura = this.entityManager.createNativeQuery(deleteCobertura);
			queryDeleteCobertura.setParameter("numEmp", numEmp);
			queryDeleteCobertura.setParameter("tipCol", tipCol);
			queryDeleteCobertura.setParameter("numCad", numCad);
			queryDeleteCobertura.setParameter("datAfa", datAfa);

			Long agora = GregorianCalendar.getInstance().getTimeInMillis();
			System.out.println("[" + agora + "] removendo Afastamento");
			result = queryDeleteCobertura.executeUpdate();
			System.out.println("[" + agora + "] tempo gasto removendo " + (GregorianCalendar.getInstance().getTimeInMillis() - agora));
			log.warn("R038AFA - DELETED: " + result);

			String txt = "Afastamento do dia " + datAfa + " excluído";

			Long agora2 = GregorianCalendar.getInstance().getTimeInMillis();
			System.out.println("[" + agora + "] commit");
			this.transaction.commit();
			System.out.println("[" + agora + "] tempo gasto commit " + (GregorianCalendar.getInstance().getTimeInMillis() - agora2));
			
			

			this.saveLog(txt, null, numCpf, "colaborador", null);
			this.listaAfastamentos(numEmp, tipCol, numCad, response);
		}
		catch (Exception ex)
		{
			System.out.println("Erro ao excluir r038afa");
			if (this.transaction.isActive())
			{
				this.transaction.rollback();
			}
			ex.printStackTrace();
		} finally {
		    OrsegupsUtils.closeConnection(conn, pstm, null);
		}
	}

	public void deleteAcesso(String numcra, String numcpf, String datacc, String seqacc, String exibirExcecao, HttpServletRequest request, HttpServletResponse response) throws IOException
	{

		this.entityManager = PersistEngine.getEntityManager("VETORH");
		this.transaction = this.entityManager.getTransaction();
		Integer result = 0;
		GregorianCalendar data = new GregorianCalendar();

		try
		{
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			Date date = df.parse(datacc);
			data.setTime(date);

			String competencia = OrsegupsUtils.getCompetencia(data);
			Integer horacc = Integer.valueOf(datacc.substring(11, 13)) * 60 + Integer.valueOf(datacc.substring(14, 16));

			if (!this.transaction.isActive())
			{
				this.transaction.begin();
			}

			String deleteCobertura = "DELETE FROM R070ACC WHERE NumCra=:numcra AND CONVERT(varchar, DatAcc, 103) =:datacc AND HorAcc=:horacc AND SeqAcc =:seqacc ";

			Query queryDeleteCobertura = this.entityManager.createNativeQuery(deleteCobertura);
			queryDeleteCobertura.setParameter("numcra", numcra);
			queryDeleteCobertura.setParameter("datacc", datacc.substring(0, 10));
			queryDeleteCobertura.setParameter("horacc", horacc);
			queryDeleteCobertura.setParameter("seqacc", seqacc);

			result = queryDeleteCobertura.executeUpdate();
			log.warn("R070ACC - DELETED: " + result);

			String txt = "Acesso do dia " + datacc + " excluído";

			this.transaction.commit();
			this.saveLog(txt, null, numcpf, "colaborador", null);
			this.listaAcessos(numcpf, competencia, exibirExcecao, response);
		}
		catch (Exception ex)
		{
			if (this.transaction.isActive())
			{
				this.transaction.rollback();
			}
			ex.printStackTrace();
		}
	}

	private void deleteCobertura(String numEmp, String tipCol, String numCad, String dataIni, String horaIni, String numEmpCob, String tipColCob, String numCadCob, String numcpf, String descricaoCobertura, String dataCobFim, String dataCobInicio, HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			deleteRegistroCobertura(numEmp, tipCol, numCad, dataIni, horaIni, numEmpCob, tipColCob, numCadCob, false, numcpf, descricaoCobertura, dataCobFim, dataCobInicio, null, request, response);
			listaCoberturas(numEmp, tipCol, numCad, response);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private void deleteHistoricoEscala(String numemp, String tipcol, String numcad, String numcpf, String datalt, HttpServletRequest request, HttpServletResponse response)
	{
		this.entityManager = PersistEngine.getEntityManager("VETORH");
		this.transaction = this.entityManager.getTransaction();
		Integer result = 0;

		try
		{
			if (!this.transaction.isActive())
			{
				this.transaction.begin();
			}

			String deleteCobertura = "DELETE FROM R038HES WHERE NumEmp=:numemp AND TipCol=:tipcol AND NumCad=:numcad AND CONVERT(varchar, DatAlt, 103) =:datalt ";

			Query queryDeleteCobertura = this.entityManager.createNativeQuery(deleteCobertura);
			queryDeleteCobertura.setParameter("numemp", numemp);
			queryDeleteCobertura.setParameter("tipcol", tipcol);
			queryDeleteCobertura.setParameter("numcad", numcad);
			queryDeleteCobertura.setParameter("datalt", datalt);

			result = queryDeleteCobertura.executeUpdate();
			log.warn("R038HES - DELETED: " + result);

			String txt = "Histórico de escala do dia " + datalt + " excluído";

			this.transaction.commit();
			this.saveLog(txt, null, numcpf, "colaborador", null);
			this.listaHistoricoEscala(numemp, tipcol, numcad, response);
		}
		catch (Exception ex)
		{
			if (this.transaction.isActive())
			{
				this.transaction.rollback();
			}
			ex.printStackTrace();
		}
	}

	private void enviaEmailPostoInconsistente(HttpServletRequest request, HttpServletResponse response, String posto)
	{
		String email = request.getParameter("emailTo");
		String isAsseio = request.getParameter("isAsseio");
		if (posto != null && !posto.isEmpty() && email != null && !email.isEmpty())
		{

			try
			{
				email = email.toLowerCase().trim();
				MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

				MailSettings mailClone = new MailSettings();
				mailClone.setMinutesInterval(settings.getMinutesInterval());
				mailClone.setFromEMail(settings.getFromEMail());
				mailClone.setFromName(settings.getFromName());
				mailClone.setRenderServer(settings.getRenderServer());
				mailClone.setSecurityFromEmail(settings.getSecurityFromEmail());
				mailClone.setSecurityCopyEmail(settings.getSecurityCopyEmail());
				mailClone.setSmtpSettings(settings.getSmtpSettings());
				mailClone.setPort(settings.getPort());
				mailClone.setSmtpServer(settings.getSmtpServer());
				mailClone.setEnabled(settings.isEnabled());
				mailClone.setFromEMail("fusion@orsegups.com.br");
				mailClone.setFromName("Orsegups Partipações S.A.");
				if (mailClone != null && posto != null && !posto.isEmpty())
				{
					String args[] = posto.split(";");
					String numPos = args[12];
					String codCcu = args[4];
					String colInconsistentes[] = args[13].split(",");
					String cplTitulo = "";
					String colIncons = "";
					String telefoneColaborador = "";
					String escalaColaborador = "";
					int cont = 0;

					if (!args[13].equals(" "))
					{
						for (String col : colInconsistentes)
						{
							String cadEmp[] = col.split("/");
							ColaboradorVO colaboradorVo = QLPresencaUtils.infoColaboradoresInconsistentes(cadEmp[0], cadEmp[1]);
							telefoneColaborador = telefoneColaborador + colaboradorVo.getTelefone() + " - " + colaboradorVo.getNomeColaborador() + "<br>";
							colIncons = colIncons + colaboradorVo.getNumeroEmpresa() + "/" + colaboradorVo.getNumeroCadastro() + " - " + colaboradorVo.getNomeColaborador() + " <br>";
							escalaColaborador = escalaColaborador + colaboradorVo.getEscala().getDescricao() + " - " + colaboradorVo.getNomeColaborador() + "<br>";
							if (cont == 0)
							{
								cplTitulo = "- " + colIncons;
							}
							if (cont == 1)
							{
								cplTitulo = cplTitulo + " e mais... ";
								cplTitulo = cplTitulo.replace("<br>", "");
							}
							cont++;
						}

					}
					PostoVO postoVo = QLPresencaUtils.buscaDadosPosto(numPos, codCcu);
					args[12] = "";
					if (postoVo.getEndereco() != null && !postoVo.getEndereco().equals(""))
					{
						args[12] = args[12] + postoVo.getEndereco().toUpperCase();
						if (postoVo.getNumero() != null && !postoVo.getNumero().equals("0"))
						{
							args[12] = args[12] + "," + postoVo.getNumero();
						}
					}

					if (postoVo.getBairro() != null && !postoVo.getBairro().equals(""))
					{
						args[12] = args[12] + "," + postoVo.getBairro().toUpperCase();
					}

					if (postoVo.getCidade() != null && !postoVo.getCidade().equals(""))
					{
						args[12] = args[12] + " - " + postoVo.getCidade().toUpperCase();
					}

					if (postoVo.getEstado() != null && !postoVo.getEstado().equals(""))
					{
						args[12] = args[12] + " - " + postoVo.getEstado().toUpperCase();
					}

					HtmlEmail noUserEmail = new HtmlEmail();
					StringBuilder noUserMsg = new StringBuilder();

					Calendar saudacao = Calendar.getInstance();
					String saudacaoEMail = "";
					if (saudacao.get(Calendar.HOUR_OF_DAY) >= 6 && saudacao.get(Calendar.HOUR_OF_DAY) < 12)
					{
						saudacaoEMail = "Bom dia, ";
					}
					else if (saudacao.get(Calendar.HOUR_OF_DAY) >= 12 && saudacao.get(Calendar.HOUR_OF_DAY) <= 18)
					{
						saudacaoEMail = "Boa tarde, ";
					}
					else
					{
						saudacaoEMail = "Boa noite, ";
					}

					String subject = "Inconsistência Presença " + cplTitulo;

					noUserEmail.setCharset("ISO-8859-1");
					noUserEmail.addTo(email);

					String emailCC = "";
					if (NeoUtils.safeIsNotNull(args[10]) && !args[10].equals("null"))
					{
					    if (isAsseio != null && isAsseio.equals("true")){
						if (args[10].equals("9")){
						    noUserEmail.addCc("cm.controle.asseio@orsegups.com.br");
						    noUserEmail.addCc("soraya.raupp@orsegups.com.br");						    
						}else if ("4_11_5_6".contains(args[10])){
						    emailCC = "cm.controle.asseio@orsegups.com.br";
						}else if ("10_7_16_12_2_3".contains(args[10])){
						    emailCC = "soraya.raupp@orsegups.com.br";
						}
					    }else{
						if ("1_4_6_10".contains(args[10]))
						    emailCC = "cm.controle.supervisao@orsegups.com.br";
						else if ("2_3_12_7_16".contains(args[10]))
						    emailCC = "cm.controle.pr3@orsegups.com.br";
						else if ("15_11_14_5_19_20_22_17".contains(args[10]))
						    emailCC = "cm.controle.pr4@orsegups.com.br";
					    }
					}

					if (emailCC != null && !emailCC.isEmpty())
						noUserEmail.addCc(emailCC);

					noUserEmail.setSubject(subject);
					noUserMsg.append("<!doctype html>");
					noUserMsg.append("<html>");
					noUserMsg.append("<head>");
					noUserMsg.append("<meta charset=\"utf-8\">");
					noUserMsg.append("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
					noUserMsg.append("</head>");
					noUserMsg.append("<body>");
					noUserMsg.append("<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#ffffff\">");
					noUserMsg.append("          <tbody>");
					noUserMsg.append("		    <td align=\"left\" valign=\"top\" >");
					noUserMsg.append("               <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/logoORS.jpg\" >");
					noUserMsg.append("            </td>");
					noUserMsg.append("			<td>");
					noUserMsg.append("			<strong>" + saudacaoEMail + " este e-mail se refere a inconsistência em posto.</strong><br><br>");
					noUserMsg.append("			<strong>IDENTIFICAÇÃO</strong><br><br>");
					if (NeoUtils.safeIsNotNull(args[0]) && NeoUtils.safeIsNotNull(args[1]) && !args[0].equals("null") && !args[1].equals("null"))
						noUserMsg.append("			<strong>Posto: </strong> " + args[1] + " - " + args[0] + " <br>");
					if (NeoUtils.safeIsNotNull(colIncons) && !colIncons.equals("null"))
						noUserMsg.append("			<strong>Colaborador(s) inconsistente(s) : </strong> " + colIncons + " <br>");
					noUserMsg.append("<br>");
					noUserMsg.append("			<strong>ESCALAS</strong><br>");
					if (NeoUtils.safeIsNotNull(escalaColaborador) && !escalaColaborador.equals("null"))
						noUserMsg.append("			<strong>Colaborador(es): </strong> " + escalaColaborador + " <br>");
					if (NeoUtils.safeIsNotNull(args[6]) && !args[6].equals("null"))
						noUserMsg.append("			<strong>Posto: </strong> " + args[6] + "<br><br>");
					else
						noUserMsg.append("			<strong>Posto: </strong> Sem Escala   <br>");
					noUserMsg.append("<br>");
					noUserMsg.append("		 <strong>CONTATOS</strong><br>");
					if (NeoUtils.safeIsNotNull(telefoneColaborador) && !telefoneColaborador.equals("null") && !args[13].equals(" "))
						noUserMsg.append("			<strong>Telefone do(s) Colaborador(es): </strong> " + telefoneColaborador + " <br>");
					else if (!args[13].equals(" "))
						noUserMsg.append("			<strong>Telefone do(s) Colaborador(es): </strong>  Não cadastrado <br>");

					if (NeoUtils.safeIsNotNull(args[5]) && !args[5].equals("null"))
						noUserMsg.append("			<strong>Telefone do posto: </strong> " + args[5] + "<br><br>");
					else
						noUserMsg.append("			<strong>Telefone do posto: </strong>  Não cadastrado<br><br>");
					if (NeoUtils.safeIsNotNull(args[12]) && !args[12].equals("null"))
						noUserMsg.append("			<strong>OUTRAS INFORMAÇÕES</strong><br>");
					noUserMsg.append("			<strong>Endereço do posto: </strong> " + args[12] + " <br>");
					if (NeoUtils.safeIsNotNull(args[8]) && !args[8].equals("null"))
						noUserMsg.append("			<strong>Vagas: </strong> " + args[8] + " <br>");
					if (NeoUtils.safeIsNotNull(args[14]) && !args[14].equals("null"))
						noUserMsg.append("			<strong>Efetivo: </strong> " + args[14] + " <br>");
					if (NeoUtils.safeIsNotNull(args[4]) && !args[4].equals("null"))
						noUserMsg.append("			<strong>Centro de Custo: </strong> " + args[4] + " <br>");
					if (NeoUtils.safeIsNotNull(args[7]) && !args[7].equals("null"))
						noUserMsg.append("			<strong>Situação: </strong> " + args[7] + " <br>");
					noUserMsg.append("			</td>");
					noUserMsg.append("     </tbody></table>");
					noUserMsg.append("</body></html>");
					noUserEmail.setHtmlMsg(noUserMsg.toString());
					noUserEmail.setContent(noUserMsg.toString(), "text/html");
					mailClone.applyConfig(noUserEmail);

					noUserEmail.send();
					//OrsegupsUtils.sendEmail2SendGrid(addTo, from, subject, "", noUserMsg.toString());

					Gson gson = new Gson();
					String revezamentoJson = gson.toJson("Sucesso ao enviar e-mail.");
					String cdc = args[4];
					cadastraEmialResponsavelPosto(email, cdc);

					String txtLog = "E-mail enviado para " + emailCC + " referente à inconsistência no posto.";
					if (NeoUtils.safeIsNotNull(args[11]) && !args[11].isEmpty())
						this.saveLog(txtLog, args[11], null, "posto", null);
					PrintWriter out = response.getWriter();
					out.print(revezamentoJson);
					out.flush();
					out.close();
					log.warn("##### SUCESSO AO ENVIAR E-MAIL INCONSISTÊNCIA POSTO INFORMAÇÃO : " + email + " - " + cdc + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				log.error("##### ERRO enviaEmailPostoInconsistente - Data: " + e.getMessage().toString() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

			}
		}

	}

	public void listaValeTransporte(HttpServletRequest request, HttpServletResponse response)
	{

		String numemp = request.getParameter("numemp");
		String tipcol = request.getParameter("tipcol");
		String numcad = request.getParameter("numcad");
		PrintWriter out = null;
		try
		{
			out = response.getWriter();
			List<ValeTransporteVO> vales = new ArrayList<ValeTransporteVO>();
			vales = QLPresencaUtils.listaValeTransporte(Long.parseLong(numemp), Long.parseLong(tipcol), Long.parseLong(numcad));

			JSONArray jsonAfastamentos = new JSONArray();

			for (ValeTransporteVO vt : vales)
			{
				JSONObject jsonValeTransporte = new JSONObject();
				jsonValeTransporte.put("linha", vt.getLinha());
				jsonValeTransporte.put("quantidade", vt.getQuantidade());
				jsonValeTransporte.put("data", vt.getData());

				jsonAfastamentos.put(jsonValeTransporte);
			}
			out.print(jsonAfastamentos);
			out.flush();
			out.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO listaValeTransporte - Data: " + e.getMessage().toString() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}

	}

	/**
	 * libera o cliente para acessar o presenca no site
	 * 
	 * @param codigoCliente
	 * @param usuario
	 */
	public void saveLiberacaoSite(String codigoCliente, String usuario, HttpServletResponse response)
	{
		System.out.println("saveLiberacaoSite");
		PrintWriter out = null;
		JSONObject jsonSituacao;
		try
		{
			response.setContentType("application/json");
			jsonSituacao = new JSONObject();
			out = response.getWriter();
			//presencaSiteLiberado
			QLGroupFilter filtro = new QLGroupFilter("AND");
			filtro.addFilter(new QLEqualsFilter("codigoCliente", codigoCliente));

			List<NeoObject> liberacoes = PersistEngine.getObjects(AdapterUtils.getEntityClass("presencaSiteLiberado"), filtro);
			if (liberacoes != null && !liberacoes.isEmpty())
			{

				NeoObject liberacao = liberacoes.get(0);
				EntityWrapper wLiberacao = new EntityWrapper(liberacao);

				if (NeoUtils.safeBoolean(wLiberacao.findValue("situacao")))
				{
					wLiberacao.setValue("situacao", false);
					jsonSituacao.put("liberado", false);
				}
				else
				{
					wLiberacao.setValue("situacao", true);
					jsonSituacao.put("liberado", true);
				}
				wLiberacao.setValue("usuario", usuario);
				wLiberacao.setValue("data", new GregorianCalendar());
				PersistEngine.persist(liberacao);

			}
			else
			{ // está vazio
				NeoObject liberacao = AdapterUtils.createNewEntityInstance("presencaSiteLiberado");
				EntityWrapper wLiberacao = new EntityWrapper(liberacao);
				//codigoCliente
				wLiberacao.setValue("codigoCliente", codigoCliente);
				//situacao
				wLiberacao.setValue("situacao", true);
				jsonSituacao.put("liberado", true);
				//usuario
				wLiberacao.setValue("usuario", usuario);
				PersistEngine.persist(liberacao);
			}

			out.print(jsonSituacao);
			out.flush();
			out.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO saveLiberacaoSite - Msg: " + e.getMessage().toString() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}

	private void cadastraEmialResponsavelPosto(String email, String centroDeCusto)
	{
		try
		{
			if (email != null && !email.isEmpty() && centroDeCusto != null && !centroDeCusto.isEmpty())
			{
				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				groupFilter.addFilter(new QLEqualsFilter("email", email));
				groupFilter.addFilter(new QLEqualsFilter("centroDeCusto", centroDeCusto));
				List<NeoObject> objs = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("QLEmailResponsavel"), groupFilter);

				if (objs == null || objs.isEmpty())
				{
					NeoObject noPS = AdapterUtils.createNewEntityInstance("QLEmailResponsavel");

					EntityWrapper psWrapper = new EntityWrapper(noPS);

					psWrapper.findField("email").setValue(email);
					psWrapper.findField("centroDeCusto").setValue(centroDeCusto);

					PersistEngine.persist(noPS);

				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO AO PESQUISAR TAREFAS : " + e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));

		}

	}

	private void controlaASO(String numctr, String numemp, String numfil, String numposto, String codSer, String usuario, String controlaASO, HttpServletResponse response)
	{
		PrintWriter out = null;
		int retorno = 0;
		try
		{
			out = response.getWriter();

			int retOp = QLPresencaUtils.atualizaASO(NeoUtils.safeLong(numctr), NeoUtils.safeLong(numemp), NeoUtils.safeLong(numfil), NeoUtils.safeLong(numposto), codSer, usuario, controlaASO);
			out.write(retOp);
			;
		}
		catch (Exception e)
		{
			log.warn("Erro ao atualizar controle de ASO via presença");
			e.printStackTrace();
			out.write(0);
		}

	}

	public void listaFinancasPosto(String numctr, String numpos, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<FinancasTituloVO> financas = new ArrayList<FinancasTituloVO>();
		financas = QLPresencaUtils.listaFinancasPosto(Long.parseLong(numctr), Long.parseLong(numpos));

		JSONArray jsonListaFinancas = new JSONArray();

		for (FinancasTituloVO fin : financas)
		{
			JSONObject jsonFinancas = new JSONObject();
			jsonFinancas.put("numTit", fin.getNumtit());
			jsonFinancas.put("sitTit", fin.getSitTit());
			jsonFinancas.put("vlrBse", fin.getVlrBco());
			jsonFinancas.put("vlrLiq", fin.getVlrOri());
			jsonFinancas.put("vlrAbe", fin.getVlrAbe());
			jsonFinancas.put("vlrPag", fin.getVlrPag());

			jsonFinancas.put("vctPro", fin.getVctPro());
			jsonFinancas.put("datEmi", fin.getDatEmi());
			jsonFinancas.put("datPag", fin.getDatPag());

			jsonListaFinancas.put(jsonFinancas);
		}
		System.out.println("jsonListaFinancas->" + jsonListaFinancas);
		out.print(jsonListaFinancas);
		out.flush();
		out.close();
	}

	public void listaX8Colaborador(String numemp, String numcad, String numcpf, GregorianCalendar datIni, GregorianCalendar datFim, String evt, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<X8VO> eventosX8 = new ArrayList<X8VO>();

		String strValidacao = QLPresencaUtils.validacaoEventosVinculos(Long.parseLong(numcpf), datIni, datFim);

		if ("OK".equals(strValidacao))
		{
			eventosX8 = QLPresencaUtils.listaX8Colabordor(Long.parseLong(numcpf), datIni, datFim, evt);

			JSONArray jsonListaX8 = new JSONArray();

			for (X8VO x8 : eventosX8)
			{
				JSONObject jsonX8 = new JSONObject();
				jsonX8.put("central", x8.getConta());
				jsonX8.put("razao", x8.getRazao());
				jsonX8.put("txObservacaoFechamento", x8.getObservacaoFechamento());
				jsonX8.put("dtRecebido", x8.getDtRecebido());
				jsonX8.put("dtViaturaDeslocamento", x8.getDtViaturaDeslocamento());
				jsonX8.put("dtFechamento", x8.getDtFechamento());
				jsonX8.put("codigo", x8.getCodigo());

				jsonListaX8.put(jsonX8);
			}
			System.out.println("jsonListaX8Colaborador->" + jsonListaX8);
			out.print(jsonListaX8);

		}
		else
		{
			JSONObject jsonErroX8 = new JSONObject();
			jsonErroX8.put("erro", strValidacao);
			System.out.println("jsonErroX8->" + jsonErroX8);
			out.print(jsonErroX8);
		}

		out.flush();
		out.close();
	}

	private void listaFichaEpi(String numcpf, Long numcad, Long numemp, Long tipcol, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<FichaEpiVO> listaEpis = new ArrayList<FichaEpiVO>();

		/*
		 * ArrayList<Long[]> listaColCads = QLPresencaUtils.getEmpresasColaborador(numcpf); // lista
		 * todas empresas em que o colaborador esteve.
		 * if (listaColCads != null){
		 * for(Long[] cad: listaColCads){
		 * //0- numemp, 1- tipcol, 2-numcad
		 * listaEpis.addAll(QLPresencaUtils.listaFichaEpi(cad[2], cad[1], cad[0]));
		 * }
		 * }
		 */
		listaEpis.addAll(QLPresencaUtils.listaFichaEpi(numcpf));

		JSONArray jsonListaEpis = new JSONArray();

		if (listaEpis != null)
		{
			for (FichaEpiVO epi : listaEpis)
			{
				JSONObject jsonEpi = new JSONObject();
				jsonEpi.put("code", epi.getCode());
				//jsonEpi.put("startDate", epi.getStartDate());
				jsonEpi.put("codProduto", epi.getCodigoProduto());
				jsonEpi.put("descProduto", epi.getDescricao());
				jsonEpi.put("derivacaoProduto", epi.getDerivacao());
				jsonEpi.put("dataEntrega", epi.getDataEntrega());
				jsonEpi.put("quantidadeEntregue", epi.getQuantidadeEntrega());
				jsonEpi.put("dataDevolvido", epi.getDataDevolucao());
				jsonEpi.put("quantidadeDevolvida", epi.getQuantidadeDevolucao());
				//jsonEpi.put("link", "<a title=\"Visualizar tarefa\" style=\"cursor:pointer\" onclick=\"javascript:showSolicitacao("+epi.getNeoId()+")\"><img src=\"imagens/icones_final/properties_16x16-trans.png\"></a>");

				jsonListaEpis.put(jsonEpi);
			}
		}
		System.out.println("jsonListaEpis->" + jsonListaEpis);
		out.print(jsonListaEpis);
		out.flush();
		out.close();

	}

	public void listaDocumentosArquivos(String numcpf, String numCad, String numEmp, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<DocumentoTreeVO> documentos = new ArrayList<DocumentoTreeVO>();
		documentos = QLPresencaUtils.listaDocumentoTree(Long.parseLong(numcpf));

		Gson gson = new Gson();
		String jsonDocumentos = gson.toJson(documentos);

		out.print(jsonDocumentos);
		out.flush();
		out.close();
	}

	public void listaInspecoes(String numctr, String numpos, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<InspecaoVO> inspecoes = new ArrayList<InspecaoVO>();
		inspecoes = QLPresencaUtils.listaInspecoes(Long.parseLong(numctr), Long.parseLong(numpos));

		Gson gson = new Gson();
		String jsonInspecoes = gson.toJson(inspecoes);

		out.print(jsonInspecoes);
		out.flush();
		out.close();

	}

	/**
	 * Consulta a conta do sigma pelo código exato da central. Ex.: B409
	 * 
	 * @param conta
	 * @param response
	 * @throws JSONException
	 * @throws IOException
	 */
	public void buscaContaSigma(String filtro, String conta, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<ContaSigmaVO> contas = new ArrayList<ContaSigmaVO>();
		contas = QLPresencaUtils.buscaContaSigma(filtro, conta);

		Gson gson = new Gson();
		String jsonContas = gson.toJson(contas);
		System.out.println(jsonContas);
		out.print(jsonContas);
		out.flush();
		out.close();

	}

	public void setVinculoContaPosto(String numctr, String numposto, String idCentral, String cdCliente, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		ContaSigmaVO conta = new ContaSigmaVO();
		conta = QLPresencaUtils.buscaContaSigmaPorCdCliente(cdCliente);

		List<ContaSigmaVinculoHumanaVO> contasVinculadas = new ArrayList<ContaSigmaVinculoHumanaVO>();

		boolean retorno = QLPresencaUtils.vinculaContaSigmaPostoHumana(numctr, numposto, conta);

		if (retorno)
		{

			contasVinculadas = QLPresencaUtils.listaContasVinculadas(numctr, numposto);

		}

		Gson gson = new Gson();
		String jsonContasVinculadas = gson.toJson(contasVinculadas);
		System.out.println(jsonContasVinculadas);
		out.print(jsonContasVinculadas);
		out.flush();
		out.close();

	}

	public void removerVinculoContaPosto(String numctr, String numposto, String idCentral, String cdCliente, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		ContaSigmaVO conta = new ContaSigmaVO();
		conta = QLPresencaUtils.buscaContaSigmaPorCdCliente(cdCliente);

		List<ContaSigmaVinculoHumanaVO> contasVinculadas = new ArrayList<ContaSigmaVinculoHumanaVO>();

		boolean retorno = QLPresencaUtils.removeContaSigmaPostoHumana(numctr, numposto, conta);

		if (retorno)
		{
			contasVinculadas = QLPresencaUtils.listaContasVinculadas(numctr, numposto);
		}

		Gson gson = new Gson();
		String jsonContasVinculadas = gson.toJson(contasVinculadas);
		System.out.println(jsonContasVinculadas);
		out.print(jsonContasVinculadas);
		out.flush();
		out.close();

	}

	public void listatVinculoContaPosto(String numctr, String numposto, String idCentral, String cdCliente, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();

		List<ContaSigmaVinculoHumanaVO> contasVinculadas = new ArrayList<ContaSigmaVinculoHumanaVO>();

		contasVinculadas = QLPresencaUtils.listaContasVinculadas(numctr, numposto);

		Gson gson = new Gson();
		String jsonContasVinculadas = gson.toJson(contasVinculadas);
		System.out.println(jsonContasVinculadas);
		out.print(jsonContasVinculadas);
		out.flush();
		out.close();

	}

	public void listaArmasPosto(String numctr, String numposto, GregorianCalendar dataFiltro, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();

		List<ArmaVO> armasPosto = new ArrayList<ArmaVO>();

		armasPosto = QLPresencaUtils.listaArmasPosto(numctr, numposto, dataFiltro);

		Gson gson = new Gson();
		String jsonArmas = gson.toJson(armasPosto);
		System.out.println(jsonArmas);
		out.print(jsonArmas);
		out.flush();
		out.close();

	}

	private void listaOciosidade(String numemp, String tipcol, String numcad, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();

		List<RTOciosoVO> listaRtOciosos = new ArrayList<RTOciosoVO>();

		listaRtOciosos = QLPresencaUtils.listaOciosidade(numemp, tipcol, numcad);

		Gson gson = new Gson();
		String jsonRTOcioso = gson.toJson(listaRtOciosos);
		System.out.println(jsonRTOcioso);
		out.print(jsonRTOcioso);
		out.flush();
		out.close();

	}

	public void listaFaltaEfetivo(String codccu, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();

		List<TarefaFaltaEfetivoVO> tarefasFaltaEfetivo = new ArrayList<TarefaFaltaEfetivoVO>();

		tarefasFaltaEfetivo = QLPresencaUtils.listaFaltaEfetivo(codccu);

		Gson gson = new Gson();
		String jsonFaltaEfetivo = gson.toJson(tarefasFaltaEfetivo);
		System.out.println(jsonFaltaEfetivo);
		out.print(jsonFaltaEfetivo);
		out.flush();
		out.close();

	}

	public void listaHistoricoEscalaPosto(String numloc, String taborg, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();

		List<HistoricoEscalaPostoVO> historicoEscala = new ArrayList<HistoricoEscalaPostoVO>();

		historicoEscala = QLPresencaUtils.listaHistoricoEscalaPosto(numloc, taborg);

		Gson gson = new Gson();
		String json = gson.toJson(historicoEscala);
		System.out.println(json);
		out.print(json);
		out.flush();
		out.close();

	}

	/**
	 * Retorna os postos de supervisão de um posto operacional
	 * 
	 * @param codccu -- codigo do centro de custo do posto supervisionado
	 * @param response -- lista de postos de supervisão
	 * @throws JSONException
	 * @throws IOException
	 */
	public void listaPostosSupervisaoVinculados(Long codccu, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();

		List<PostoSupervisaoVO> postos = new ArrayList<PostoSupervisaoVO>();

		postos = QLPresencaUtils.listaPostosSupervisaoVinculados(codccu);

		Gson gson = new Gson();
		String json = gson.toJson(postos);
		System.out.println(json);
		out.print(json);
		out.flush();
		out.close();

	}

	private void salvaVinculoSupervisaoPosto(String codccu, String numloc, String usuario, String ccusup, HttpServletResponse response) throws IOException, JSONException
	{
		PrintWriter out = response.getWriter();

		Boolean retornoOk = false;
		System.out.println(codccu);

		if (QLPresencaUtils.comparaRegionalDoCentroDeCusto(NeoUtils.safeLong(codccu), NeoUtils.safeLong(ccusup)) && !QLPresencaUtils.existeVinculoSupervisaoPosto(codccu, ccusup))
		{ // se for da mesma regional
			retornoOk = QLPresencaUtils.salvaVinculoSupervisaoPosto(codccu, numloc, usuario, ccusup);
		}

		List<PostoSupervisaoVO> postos = new ArrayList<PostoSupervisaoVO>();

		postos = QLPresencaUtils.listaPostosSupervisaoVinculados(NeoUtils.safeLong(codccu));

		Gson gson = new Gson();
		String jsonContasVinculadas = gson.toJson(postos);

		System.out.println(jsonContasVinculadas);
		out.print(jsonContasVinculadas);
		out.flush();
		out.close();

	}

	private void salvaObsHorista(String numcad, String numemp, String tipcol, String obsHorista, String numCpf, HttpServletResponse response) throws IOException, JSONException
	{
		PrintWriter out = response.getWriter();

		Boolean retornoOk = false;
		retornoOk = QLPresencaUtils.salvaObsHorista(numcad, numemp, tipcol, obsHorista);
		if (!retornoOk)
		{
			throw new WorkflowException("Problemas ao inserir a observação!");
		}
		else
		{
			saveLog(obsHorista, null, numCpf, "colaborador", null);
		}
		List<ObsHoristaVO> obssHorista = new ArrayList<ObsHoristaVO>();
		obssHorista = QLPresencaUtils.listaObsHoristas(Long.parseLong(numemp), Long.parseLong(tipcol), Long.parseLong(numcad), numCpf);

		JSONArray jsonObservacoes = new JSONArray();
		for (ObsHoristaVO observacao : obssHorista)
		{
			JSONObject jsonObservacao = new JSONObject();
			jsonObservacao.put("numEmpHor", observacao.getNumEmpHor());
			jsonObservacao.put("tipcol", observacao.getTipCol());
			jsonObservacao.put("numCadHor", observacao.getNumCadHor());
			jsonObservacao.put("dataRegistro", observacao.getDataRegistro());
			long qtdHorTotal = observacao.getHoraRegistro();
			long qtdhora = qtdHorTotal / 60;
			double qtdmin = ((qtdHorTotal / 60.0) - qtdhora) * 60;
			int horInt = (int) qtdmin;
			String horMIm = qtdhora + ":";
			if (horInt < 10)
			{
				horMIm += "0" + horInt;
			}
			else
			{
				horMIm += horInt;
			}
			jsonObservacao.put("horRegistro", horMIm);
			jsonObservacao.put("obsHorista", observacao.getObservacao());
			jsonObservacao.put("linkExcluir", observacao.getLinkExcluir());
			jsonObservacoes.put(jsonObservacao);
		}

		out.print(jsonObservacoes);
		out.flush();
		out.close();

	}

	private void removerVinculoSupervisaoPosto(String codccu, String numloc, String usuario, String ccusup, HttpServletResponse response) throws IOException, JSONException
	{
		PrintWriter out = response.getWriter();

		Boolean retornoOk = false;
		System.out.println(codccu);

		retornoOk = QLPresencaUtils.removerVinculoSupervisaoPosto(codccu, numloc, usuario, ccusup);

		List<PostoSupervisaoVO> postos = new ArrayList<PostoSupervisaoVO>();
		if (retornoOk)
		{
			postos = QLPresencaUtils.listaPostosSupervisaoVinculados(NeoUtils.safeLong(codccu));
		}

		Gson gson = new Gson();
		String jsonContasVinculadas = gson.toJson(postos);

		System.out.println(jsonContasVinculadas);
		out.print(jsonContasVinculadas);
		out.flush();
		out.close();

	}

	private void removerObsHorista(String numcad, String numemp, String tipcol, String neoId, String numCpf, HttpServletResponse response) throws IOException, JSONException
	{
		PrintWriter out = response.getWriter();

		Boolean retornoOk = false;
		System.out.println(neoId);

		retornoOk = QLPresencaUtils.removerObsHorista(neoId);

		List<ObsHoristaVO> observacoes = new ArrayList<ObsHoristaVO>();
		if (retornoOk)
		{
			saveLog("Removido observação do Horista!", null, numCpf, "colaborador", null);
			observacoes = QLPresencaUtils.listaObsHoristas(Long.parseLong(numemp), Long.parseLong(tipcol), Long.parseLong(numcad), numCpf);
		}

		JSONArray jsonObservacoes = new JSONArray();
		for (ObsHoristaVO observacao : observacoes)
		{
			JSONObject jsonObservacao = new JSONObject();
			jsonObservacao.put("numEmpHor", observacao.getNumEmpHor());
			jsonObservacao.put("tipcol", observacao.getTipCol());
			jsonObservacao.put("numCadHor", observacao.getNumCadHor());
			jsonObservacao.put("dataRegistro", observacao.getDataRegistro());
			long qtdHorTotal = observacao.getHoraRegistro();
			long qtdhora = qtdHorTotal / 60;
			double qtdmin = ((qtdHorTotal / 60.0) - qtdhora) * 60;
			int horInt = (int) qtdmin;
			String horMIm = qtdhora + ":";
			if (horInt < 10)
			{
				horMIm += "0" + horInt;
			}
			else
			{
				horMIm += horInt;
			}
			jsonObservacao.put("horRegistro", horMIm);
			jsonObservacao.put("obsHorista", observacao.getObservacao());
			jsonObservacao.put("linkExcluir", observacao.getLinkExcluir());
			jsonObservacoes.put(jsonObservacao);
		}

		out.print(jsonObservacoes);
		out.flush();
		out.close();

	}

	private void atualizaPostoSup(String codccu, String numloc, String usuario, String isPostoSup, HttpServletResponse response) throws IOException, JSONException
	{

		PrintWriter out = response.getWriter();

		Boolean retorno = false;
		System.out.println(codccu);

		retorno = QLPresencaUtils.atualizaPostoSup(codccu, numloc, usuario, isPostoSup);

		Gson gson = new Gson();
		JSONObject jsonret = new JSONObject();
		jsonret.put("ret", retorno);

		//String json = gson.toJson(jsonret);
		System.out.println(jsonret);
		out.print(jsonret);
		out.flush();
		out.close();

	}

	private void listaMensagensURA(Long numcpf, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<MensagemTIVO> mensagens = new ArrayList<MensagemTIVO>();

		mensagens = QLPresencaUtils.listaMensagensURA(numcpf);

		Gson gson = new Gson();
		/*
		 * JSONObject jsonret = new JSONObject();
		 * jsonret.put("ret", mensagens);
		 */

		String jsonRetorno = gson.toJson(mensagens);
		//String json = gson.toJson(jsonret);
		System.out.println(jsonRetorno);
		out.print(jsonRetorno);
		out.flush();
		out.close();
	}

	public String cancelaProcessos(String codes, String model, String motivo)
	{

		String xCodes[] = codes.split(",");
		String strCodes = "'000000'";
		Long code = Long.parseLong(codes);
		if (code < 10)
		{
			codes = "00000" + codes;
		}
		else if (code < 100)
		{
			codes = "0000" + codes;
		}
		else if (code < 1000)
		{
			codes = "000" + codes;
		}
		else if (code < 10000)
		{
			codes = "00" + codes;
		}
		else if (code < 100000)
		{
			codes = "0" + codes;
		}
		strCodes = codes;
		/*
		 * for (String x: xCodes){
		 * strCodes += ",'"+x+"'";
		 * }
		 */
		String query = "";

		query = " select neoId from wfprocess   where model_neoid in (select neoId from processmodel pm  where pm.name = (select distinct(pm1.name) from processmodel pm1 where pm1.name = '" + model + "' ) ) and code in  " + "(" + strCodes + ")";

		EntityManager entity = PersistEngine.getEntityManager();
		Integer cont = 1;
		if (entity != null)
		{
			Query q = entity.createNativeQuery(query);
			List<BigInteger> result = (List<BigInteger>) q.getResultList();

			for (BigInteger i : result)
			{
				Long id = i.longValue();

				/**
				 * FIXME Cancelar tarefas.
				 */
				WFProcess proc = PersistEngine.getObject(WFProcess.class, new QLEqualsFilter("neoId", id));

				if (proc != null)
				{
					RuntimeEngine.getProcessService().cancel(proc, motivo);
					System.out.println("[Tela de cancelamento reciclagem] - " + cont + " : Processo " + proc.getCode());
				}
				else
				{
					System.out.println("[Tela de cancelamento reciclagem] - " + cont + " : ERRO: Processo não localizado!");
				}
				cont++;
			}
			return "Cancelamento Processado com Sucesso!";
		}
		else
		{
			return "Nenhuma tarefa encontrada para cancelamento";
		}
	}

	public String removeRegistroURA(String numCpf, GregorianCalendar dataCurso)
	{
		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		groupFilter.addFilter(new QLEqualsFilter("cpf", Long.parseLong(numCpf)));
		groupFilter.addFilter(new QLEqualsFilter("dataFim", (GregorianCalendar) dataCurso));
		List<NeoObject> objs = PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMensagem"), groupFilter, -1, -1, "dataFim desc");
		if ((objs != null) && (objs.size() > 0))
		{
			PersistEngine.remove(objs.get(0));
			System.out.println("Registro da URA removido com Sucesso!");
			return "Registro da URA removido com Sucesso!";
		}
		else
		{
			System.out.println("Nenhum registro para ser removido da URA");
			return "Nenhum registro para ser removido da URA";
		}

	}

	public void cancelarTarefasReciclagem(String dataCurso, String code, String numcpf, HttpServletResponse response) throws IOException
	{
		PrintWriter out = response.getWriter();
		GregorianCalendar d = new GregorianCalendar();
		String retornoUra = "Nenhum registro para ser removido da URA.";
		if (dataCurso != null && !dataCurso.equals("0"))
		{
			d.setTimeInMillis(Long.parseLong(dataCurso));
			retornoUra = this.removeRegistroURA(numcpf, d);
		}
		Gson gson = new Gson();
		String jsonContasVinculadas = "";
		String retorno = this.cancelaProcessos(code, "RCL - informar reciclagem", "Cancelamento realizado via tela 'tarefas de reciclagem'.");
		if (!retorno.contains("Sucesso"))
		{
			jsonContasVinculadas = gson.toJson("Nenhuma tarefa encontrada para o cancelamento.");
		}
		else
		{
			jsonContasVinculadas = gson.toJson("Tarefa cancelada com sucesso!");
		}
		if (!retornoUra.contains("Nenhum registro para ser removido da URA"))
		{
			jsonContasVinculadas = jsonContasVinculadas + ";" + gson.toJson("Registro da URA removido com Sucesso!");
		}
		else
		{
			jsonContasVinculadas = jsonContasVinculadas + ";" + gson.toJson("Nenhum registro para ser removido da URA.");
		}

		out.print(jsonContasVinculadas);
	}

	public String iniciarTarefaSimples(Long numcad, Long numemp, GregorianCalendar dataHoraAcc)
	{
		String solicitante = "nicolas.silva";
		NeoPaper papel = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "responsavelTarefaEntradaForcada"));
		String executor = "";
		for (NeoUser u : papel.getAllUsers())
		{
			executor = u.getCode();
		}
		StringBuilder buscaPerEsc = new StringBuilder();
		buscaPerEsc.append(" SELECT FUN.NomFun FROM R034FUN FUN ");
		buscaPerEsc.append(" WHERE FUN.NUMCAD = :numcad AND FUN.NUMEMP = :numemp ");

		Query queryBuscaPerEsc = this.entityManager.createNativeQuery(buscaPerEsc.toString());
		queryBuscaPerEsc.setParameter("numcad", numcad);
		queryBuscaPerEsc.setParameter("numemp", numemp);
		queryBuscaPerEsc.setMaxResults(1);
		String nome = "";

		Object obj = queryBuscaPerEsc.getSingleResult();
		if (NeoUtils.safeIsNotNull(obj))
		{
			nome = obj.toString();
		}

		String titulo = "Justificar Ausência de Marcação - " + numemp + "/" + numcad + " - " + nome;
		String descricao = "Identificamos que o colaborador " + numemp + "/" + numcad + " - " + nome + " teve seu acesso do dia " + NeoDateUtils.safeDateFormat(dataHoraAcc, "dd/MM/yyyy") + " hora: " + NeoDateUtils.safeDateFormat(dataHoraAcc, "HH:mm") + " realizado de forma manual aplicado pela Central de Monitoramento. Solicito averiguar o motivo";

		String avanca = "sim";
		GregorianCalendar prazo = new GregorianCalendar();
		prazo.add(GregorianCalendar.DATE, 2);
		prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
		prazo.set(GregorianCalendar.MINUTE, 59);
		prazo.set(GregorianCalendar.SECOND, 59);

		if(!OrsegupsUtils.isWorkDay(prazo)){
		    prazo = OrsegupsUtils.getNextWorkDay(prazo);
		}
		
		IniciarTarefaSimples tarefaSimples = new IniciarTarefaSimples();
		String tarefa = tarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", avanca, prazo);
		System.out.println("Tarefa simples aberta para entrada forçada ===> " + tarefa);
		return tarefa;

	}

	public Long retornaClasseEscala(Long numcad, Long numemp)
	{
		Connection connVetorh = PersistEngine.getConnection("VETORH");
		ResultSet rsColaborador = null;
		PreparedStatement stColaborador = null;
		StringBuffer queryColaborador = new StringBuffer();
		queryColaborador.append(" SELECT esc.claesc ");
		queryColaborador.append(" FROM R034FUN fun  ");
		queryColaborador.append(" LEFT JOIN R038HCH hch WITH (NOLOCK) ON hch.NUMEMP = fun.NumEmp AND fun.TIPCOL = hch.TIPCOL AND fun.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch.HorIni, hch.DatIni) = (SELECT MAX(DATEADD(MINUTE, hch2.HorIni, hch2.DatIni)) FROM R038HCH hch2 WHERE hch2.NUMEMP = hch.NUMEMP AND hch2.TIPCOL = hch.TIPCOL AND hch2.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch2.HorIni, hch2.DatIni) <= getdate())");
		queryColaborador.append(" INNER JOIN R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp AND hlo.TipCol = fun.TipCol AND hlo.NumCad = fun.NumCad AND hlo.DatAlt = (SELECT MAX (DATALT) FROM R038HLO TABELA001 WHERE TABELA001.NUMEMP = hlo.NUMEMP AND TABELA001.TIPCOL = hlo.TIPCOL AND TABELA001.NUMCAD = hlo.NUMCAD AND TABELA001.DATALT <= getdate()) ");
		queryColaborador.append(" INNER JOIN R038HES hes WITH (NOLOCK) ON hes.NumEmp = fun.NumEmp AND hes.TipCol = fun.TipCol AND hes.NumCad = fun.NumCad AND hes.DatAlt = (SELECT MAX (DATALT) FROM R038HES TABELA001 WHERE TABELA001.NUMEMP = hes.NUMEMP AND TABELA001.TIPCOL = hes.TIPCOL AND TABELA001.NUMCAD = hes.NUMCAD AND TABELA001.DATALT <= getdate()) ");
		queryColaborador.append(" INNER JOIN R006ESC esc ON esc.CodEsc = hes.CodEsc ");
		queryColaborador.append(" Where fun.numcad = ? and fun.numemp = ? ");

		try
		{
			stColaborador = connVetorh.prepareStatement(queryColaborador.toString());
			stColaborador.setLong(1, numcad);
			stColaborador.setLong(2, numemp);
			rsColaborador = stColaborador.executeQuery();

			while (rsColaborador.next())
			{
				return rsColaborador.getLong("claesc");
			}
			return 0L;

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connVetorh, stColaborador, rsColaborador);
		}

		return 0L;
	}

	public void retornaAfastamentos(String dadosColaborador, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		String[] dadosCol = dadosColaborador.split(" - ");
		Long numemp = Long.parseLong(dadosCol[0]);
		Long numcad = Long.parseLong(dadosCol[1]);
		String numcpf = "";
		//String nomeCol = String.valueOf(dadosCol[2]);

		Connection connVetorh = PersistEngine.getConnection("VETORH");
		ResultSet rsColaborador = null;
		PreparedStatement stColaborador = null;
		StringBuffer queryColaborador = new StringBuffer();
		queryColaborador.append(" SELECT numcpf ");
		queryColaborador.append(" FROM R034FUN fun WITH(NOLOCK) ");
		queryColaborador.append(" Where fun.numcad = ? and fun.numemp = ? ");
		try
		{
			stColaborador = connVetorh.prepareStatement(queryColaborador.toString());
			stColaborador.setLong(1, numcad);
			stColaborador.setLong(2, numemp);
			rsColaborador = stColaborador.executeQuery();

			if (rsColaborador.next())
			{
				numcpf = rsColaborador.getString("numcpf");
				queryColaborador = new StringBuffer();
				queryColaborador.append(" select a.DatAfa,a.DatTer,a.SitAfa,sit.DesSit from r038afa a WITH(NOLOCK) ");
				queryColaborador.append(" inner join r034fun fun WITH(NOLOCK) on fun.numcad = a.NumCad and fun.numemp = a.NumEmp ");
				queryColaborador.append(" inner join R010SIT sit WITH(NOLOCK) on sit.CodSit = a.SitAfa ");
				queryColaborador.append(" where fun.numcpf = ? and fun.sitAfa <> 7 order by a.datAfa");

				ResultSet rsAfastamento = null;
				PreparedStatement stAfastamento = null;
				stAfastamento = connVetorh.prepareStatement(queryColaborador.toString());
				stAfastamento.setString(1, numcpf);
				rsAfastamento = stAfastamento.executeQuery();

				JSONArray jsonAfastamentos = new JSONArray();
				while (rsAfastamento.next())
				{
					JSONObject jsonAfastamento = new JSONObject();

					GregorianCalendar datAfa = new GregorianCalendar();
					datAfa.setTime(rsAfastamento.getDate("datAfa"));
					jsonAfastamento.put("datAfa", NeoDateUtils.safeDateFormat(datAfa, "dd/MM/yyyy"));
					GregorianCalendar datTer = new GregorianCalendar();
					datTer.setTime(rsAfastamento.getDate("datTer"));
					jsonAfastamento.put("datTer", NeoDateUtils.safeDateFormat(datTer, "dd/MM/yyyy"));
					jsonAfastamento.put("sitAfa", rsAfastamento.getLong("sitAfa"));
					jsonAfastamento.put("desSit", rsAfastamento.getString("desSit"));
					jsonAfastamentos.put(jsonAfastamento);
				}

				out.print(jsonAfastamentos);
				System.out.println(jsonAfastamentos);
				out.flush();
				out.close();
			}

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connVetorh, stColaborador, rsColaborador);
		}

	}

	public void retornaHistoricoCRMMedico(String crmMedico, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		String[] dados = crmMedico.split(" - ");
		if(!dados.equals("")){
			crmMedico = dados[0];
		}
		
		System.out.println("teste");
		
		Connection connFusion = PersistEngine.getConnection("");
		ResultSet rsHistoricoMedico = null;
		PreparedStatement stHistoricoMedico = null;
		StringBuffer queryHistoricoMedico = new StringBuffer();

		queryHistoricoMedico.append(" SELECT w.code,w.neoId,med.nome,med.crm,cid.codigo,cid.descricao,fun.nomfun,fun.numcad,fun.numemp FROM D_jaJustificativaAfastamento ja WITH(NOLOCK)");
		queryHistoricoMedico.append(" inner join WFProcess w WITH(NOLOCK) on w.neoId = ja.wfprocess_neoId ");
		queryHistoricoMedico.append(" inner join D_JAMedico med WITH(NOLOCK) on med.neoId = ja.medico_neoId ");
		queryHistoricoMedico.append(" left join D_jaCID cid WITH(NOLOCK) on cid.neoId = ja.cid_neoId ");
		queryHistoricoMedico.append(" inner join X_VETORHRFUN funFus WITH(NOLOCK) on funFus.neoId = ja.colaborador_neoId ");
		queryHistoricoMedico.append(" inner join [FSOODB04\\SQL02].VETORH.DBO.R034FUN fun WITH(NOLOCK) on fun.numcad = funFus.numcad and fun.numemp = funFus.numemp and fun.tipcol = funFus.tipcol ");
		queryHistoricoMedico.append(" where w.processState <> 2 and med.crm = ? order by w.code");

		try
		{
			stHistoricoMedico = connFusion.prepareStatement(queryHistoricoMedico.toString());
			stHistoricoMedico.setString(1, crmMedico);
			rsHistoricoMedico = stHistoricoMedico.executeQuery();
			JSONArray jsonHistoricoMedico = new JSONArray();

			while (rsHistoricoMedico.next())
			{
				JSONObject jsonRegistroMedico = new JSONObject();
				jsonRegistroMedico.put("nome", rsHistoricoMedico.getString("nome"));
				jsonRegistroMedico.put("CRM", rsHistoricoMedico.getString("crm"));
				jsonRegistroMedico.put("cid", rsHistoricoMedico.getString("codigo") != null ? rsHistoricoMedico.getString("codigo") : "");
				jsonRegistroMedico.put("desSub", rsHistoricoMedico.getString("descricao") != null ? rsHistoricoMedico.getString("descricao") : "Não informado");
				jsonRegistroMedico.put("nomfun", rsHistoricoMedico.getString("nomfun"));
				jsonRegistroMedico.put("numcad", rsHistoricoMedico.getLong("numcad"));
				jsonRegistroMedico.put("numemp", rsHistoricoMedico.getLong("numemp"));
				//StringBuilder link =  new StringBuilder();
				//link.append("<img onclick=\"javascript:viewItemFusion(" + rsHistoricoMedico.getString("neoId") + ");\" neoid=\"" + rsHistoricoMedico.getString("neoId") + "\" id=\"editItem_49873\" class=\"tableIcon\" src=\"imagens/icones_final/properties_16x16-trans.png\" title=\"Visualizar\" align=\"absmiddle\"></a>&nbsp;" + rsHistoricoMedico.getString("code") + "<br>");
				
				long wfProcess = rsHistoricoMedico.getLong("neoId");
				String tarefa = rsHistoricoMedico.getString("code");
				
				String link = "<a href=\"https://intranet.orsegups.com.br/fusion/bpm/workflow_search.jsp?id="+wfProcess+"\" target=\"_blank\"><img title=\"Pesquisar\" src=\"https://intranet.orsegups.com.br/fusion/imagens/icones_final/visualize_blue_16x16-trans.png\" align=\"absmiddle\"> "+tarefa+"</a>";
				
				
				jsonRegistroMedico.put("code", link);
				jsonHistoricoMedico.put(jsonRegistroMedico);
			}

			out.print(jsonHistoricoMedico);
			System.out.println(jsonHistoricoMedico);
			out.flush();
			out.close();

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connFusion, stHistoricoMedico, rsHistoricoMedico);
		}

	}
}
