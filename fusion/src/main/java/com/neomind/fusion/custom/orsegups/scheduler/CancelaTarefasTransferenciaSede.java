package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.SchedulerUtils;
import com.neomind.fusion.custom.orsegups.utils.TransferenciaSedeAdministracaoVO;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoUtils;

public class CancelaTarefasTransferenciaSede implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(CancelaTarefasTransferenciaSede.class);

	public void execute(CustomJobContext arg0)
	{
		Connection conn = null;
		Connection connFusion = null;
		
		StringBuilder sql = new StringBuilder();
		StringBuilder sqlFusion = new StringBuilder();
		
		PreparedStatement pstm = null;
		PreparedStatement pstmFusion = null;
		
		ResultSet rs = null;
		ResultSet rsFusion = null;
		
		log.warn("##### INICIO AGENDADOR DE TAREFA: Cancela Tarefa Simples Transferência Sede - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		SchedulerUtils schedulerUtils = new SchedulerUtils();
		
		try
		{
			/*Verifica lista de colaboradores que devem abrir tarefas no Fusion*/
			conn = PersistEngine.getConnection("VETORH");
			List<TransferenciaSedeAdministracaoVO> transferenciaSedeAdministracaoVOs = new ArrayList<TransferenciaSedeAdministracaoVO>();

			sql.append(" SELECT fun.numemp, fun.tipcol, fun.numcad, fun.numcpf, fun.nomfun, car.titcar FROM R034FUN FUN                                                                                                                    ");
			sql.append(" INNER JOIN R038HLO HLO ON HLO.NUMEMP = FUN.NUMEMP AND                                                                                                                                     ");
			sql.append(" 				          HLO.TIPCOL = FUN.TIPCOL AND                                                                                                                                      ");
			sql.append(" 				          HLO.NUMCAD = FUN.NUMCAD AND                                                                                                                                      ");
			sql.append(" 				          HLO.DATALT = (SELECT MAX(DATALT) FROM R038HLO HLO1 WHERE HLO1.NUMEMP = HLO.NUMEMP AND HLO1.TIPCOL = HLO.TIPCOL AND HLO1.NUMCAD = HLO.NUMCAD)                     ");
			sql.append(" LEFT JOIN R038AFA AFA ON AFA.NUMEMP = FUN.NUMEMP AND                                                                                                                                      ");
			sql.append(" 						  AFA.TIPCOL = FUN.TIPCOL AND                                                                                                                                      ");
			sql.append(" 						  AFA.NUMCAD = FUN.NUMCAD AND                                                                                                                                      ");
			sql.append(" 						  AFA.DATAFA = (SELECT MAX(DATAFA) FROM R038AFA AFA1 WHERE AFA1.NUMEMP = AFA.NUMEMP AND AFA1.TIPCOL = AFA.TIPCOL AND AFA1.NUMCAD = AFA.NUMCAD)                     ");
			sql.append(" INNER JOIN R038HCA HCA ON HCA.NUMEMP = FUN.NUMEMP AND                                                                                                                                     ");
			sql.append(" 				          HCA.TIPCOL = FUN.TIPCOL AND                                                                                                                                      ");
			sql.append(" 				          HCA.NUMCAD = FUN.NUMCAD AND                                                                                                                                      ");
			sql.append(" 				          HCA.DATALT = (SELECT MAX(DATALT) FROM R038HCA HCA1 WHERE HCA1.NUMEMP = HCA.NUMEMP AND HCA1.TIPCOL = HCA.TIPCOL AND HCA1.NUMCAD = HCA.NUMCAD AND HCA1.ESTCAR = 1) ");
			sql.append(" INNER JOIN R024CAR CAR ON HCA.EstCar = CAR.ESTCAR AND HCA.CODCAR = CAR.CODCAR                                                                                                             ");
			sql.append(" WHERE FUN.TIPCOL = 1                                                                                                                                                                      ");
			sql.append("   AND HLO.TABORG = 203	                                                                                                                                                                   ");
			sql.append("   AND HLO.NUMLOC = 27124                                                                                                                                                                  ");
			sql.append("   AND (AFA.SITAFA <> 7 OR AFA.SITAFA IS NULL)                                                                                                                                             ");
			sql.append(" ORDER BY FUN.NUMEMP, FUN.TIPCOL, FUN.NUMCAD																																									   ");

			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();
			TransferenciaSedeAdministracaoVO transferenciaSedeAdministracaoVO = null;
			while (rs.next())
			{
				transferenciaSedeAdministracaoVO = new TransferenciaSedeAdministracaoVO();
				Long numemp = rs.getLong("numemp");
				Long tipcol = rs.getLong("tipcol");
				Long numcad = rs.getLong("numcad");
				Long numcpf = rs.getLong("numcpf");

				transferenciaSedeAdministracaoVO.setNumemp(numemp);
				transferenciaSedeAdministracaoVO.setTipcol(tipcol);
				transferenciaSedeAdministracaoVO.setNumcad(numcad);
				transferenciaSedeAdministracaoVO.setNumcpf(numcpf);

				transferenciaSedeAdministracaoVOs.add(transferenciaSedeAdministracaoVO);
			}
			OrsegupsUtils.closeConnection(conn, pstm, rs);
			
			/*Verifica lista de tarefas abertas no Fusion*/
			connFusion = PersistEngine.getConnection("");
			List<TransferenciaSedeAdministracaoVO> transferenciaSedeColaboradoresVOs = new ArrayList<TransferenciaSedeAdministracaoVO>();

			sqlFusion.append(" SELECT numEmp, tipCol, numCad, numCpf, tarefa FROM D_TAREFATRANSFERENCIACOLABORADOR TTC   	     	");
			sqlFusion.append(" 			WHERE TTC.DATAGERACAO = (SELECT MAX(DATAGERACAO) FROM D_TAREFATRANSFERENCIACOLABORADOR TTC1 ");
			sqlFusion.append(" 			                         WHERE TTC.NUMEMP = TTC1.NUMEMP AND 								");
			sqlFusion.append(" 			                               TTC.TIPCOL = TTC1.TIPCOL AND 								");
			sqlFusion.append(" 			                               TTC.NUMCAD = TTC1.NUMCAD)									");
			sqlFusion.append(" 			ORDER BY TTC.numEmp, TTC.tipCol, TTC.numCad													");

			pstmFusion = connFusion.prepareStatement(sqlFusion.toString());
			rsFusion = pstmFusion.executeQuery();
			TransferenciaSedeAdministracaoVO transferenciaSedeColaboradoresVO = null;
			while (rsFusion.next())
			{
				transferenciaSedeColaboradoresVO = new TransferenciaSedeAdministracaoVO();
				Long numemp = rsFusion.getLong("numEmp");
				Long tipcol = rsFusion.getLong("tipCol");
				Long numcad = rsFusion.getLong("numCad");
				Long numcpf = rsFusion.getLong("numCpf");
				String code = rsFusion.getString("tarefa");

				//0 - Ativo ou 1 - Inativo 
				int situacao = retornaStatusTarefaSimplesTransferenciaSede(code);
				if(situacao == 0L)
				{	
					transferenciaSedeColaboradoresVO.setNumemp(numemp);
					transferenciaSedeColaboradoresVO.setTipcol(tipcol);
					transferenciaSedeColaboradoresVO.setNumcad(numcad);
					transferenciaSedeColaboradoresVO.setNumcpf(numcpf);
					transferenciaSedeColaboradoresVO.setCodtar(code);

					transferenciaSedeColaboradoresVOs.add(transferenciaSedeColaboradoresVO);
				}
			}
			OrsegupsUtils.closeConnection(connFusion, pstmFusion, rsFusion);
			
			for (TransferenciaSedeAdministracaoVO transferenciaSedeAdministracaoVO3 : transferenciaSedeColaboradoresVOs)
			{
				for (TransferenciaSedeAdministracaoVO transferenciaSedeAdministracaoVO2 : transferenciaSedeAdministracaoVOs)
				{
					if(transferenciaSedeAdministracaoVO3.getNumemp().equals(transferenciaSedeAdministracaoVO2.getNumemp()) &&
					   transferenciaSedeAdministracaoVO3.getTipcol().equals(transferenciaSedeAdministracaoVO2.getTipcol()) &&	
					   transferenciaSedeAdministracaoVO3.getNumcad().equals(transferenciaSedeAdministracaoVO2.getNumcad()))
					{
						log.warn("##### MEIO DO AGENDADOR DE TAREFA: Existe uma Tarefa em Aberto para o Colaborador " + transferenciaSedeAdministracaoVO3.getNumemp() + "/" + transferenciaSedeAdministracaoVO3.getNumcad() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
					}
					else
					{
						cancelaTarefaSimplesTransferenciaSede(transferenciaSedeAdministracaoVO3.getCodtar());
					}
				}
			}			
		}
		catch (Exception e)
		{
			log.error("##### ERRO AGENDADOR DE TAREFA: Cancela Tarefa Simples Transferência Sede");
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
			OrsegupsUtils.closeConnection(connFusion, pstmFusion, rsFusion);
			log.warn("##### FIM AGENDADOR DE TAREFA: Cancela Tarefa Simples Transferência Sede - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}

	@SuppressWarnings("finally")
	public int retornaStatusTarefaSimplesTransferenciaSede(String code)
	{
		//0 - Ativo ou 1 - Inativo  
		int situacao = 1;
		
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try
		{
			conn = PersistEngine.getConnection("");

			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT WF.PROCESSSTATE                                                       ");
			sql.append(" FROM WFPROCESS AS WF WITH (NOLOCK)                                           ");
			sql.append(" INNER JOIN D_TAREFA TA WITH (NOLOCK) ON TA.WFPROCESS_NEOID = WF.NEOID        ");
			sql.append(" INNER JOIN PROCESSMODEL AS PM WITH (NOLOCK)  ON PM.NEOID = WF.MODEL_NEOID    ");
			sql.append(" WHERE PM.NEOID = 18888 AND WF.SAVED = 1 AND WF.CODE = '" + code + "'         ");

			//BUSCA-SE REGISTROS NA BASE
			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();
			while (rs.next())
			{
				String status = rs.getString("PROCESSSTATE");
				if (status != null && status.equals("0"))
				{
					situacao = 0;
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (final Exception e)
			{
				e.printStackTrace();
			}
			return situacao;
		}
	}
	
	public static void cancelaTarefaSimplesTransferenciaSede(String codigoProcesso)
	{
		String codigoCancelamento = "cancelaTarefaSimples";
		String motivoCancelamento = "Tarefa cancelada, pois o colaborador não está mais lotado no posto Transferência Sede Administração.";
				
		String resultado = OrsegupsUtils.cancelWorkflowProcess(codigoProcesso, codigoCancelamento, motivoCancelamento);
		log.warn("Cancelar tarefas falta de efetivo: " + resultado + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	}
}

