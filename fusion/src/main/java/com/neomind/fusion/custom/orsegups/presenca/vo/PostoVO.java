package com.neomind.fusion.custom.orsegups.presenca.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils;

public class PostoVO
{
	private ClienteVO cliente;
	private Long codigoOrganograma;
	private String lotacao;
	private String nomePosto;
	private Long numeroLocal;
	private Long numeroPosto;
	private Long numeroContrato;
	private String codigoLocal;
	private String centroCusto;
	private String codigoPai;
	private String servico;
	private String telefone;
	private String numeroContato;
	private String dddContato;
	private EscalaPostoVO escalaPosto;
	private String situacao;
	private Long vagas;
	private Long vagasPresenca;
	private String endereco;
	private String cidade;
	private String estado;
	private String cep;
	private String bairro;
	private String numero;
	private BigDecimal valor;
	private Date dataInicio;
	private Date dataFim;
	private Date dataFimVigencia;
	private String motivoEncerramento;
	private String obsEncerramento;
	private StatusVO status;
	private Long ajusteEscala;
	private String strDataInicio; // criado pois o objeto date não pode ser manipulado via javacript em notação json
	private Collection<ColaboradorVO> colaboradores = new ArrayList();
	private Collection<EntradaAutorizadaVO> entradasAutorizadas = new ArrayList();
	private long usuCodreg;
	
	
	private Long numemp;
	private Long numfil;
	private String codSer;
	
	
	private String usuPossup;
	private Long usuIssup;

	public Long getCodigoOrganograma()
	{
		return this.codigoOrganograma;
	}

	public void setCodigoOrganograma(Long codigoOrganograma)
	{
		this.codigoOrganograma = codigoOrganograma;
	}

	public Long getNumeroLocal()
	{
		return this.numeroLocal;
	}

	public void setNumeroLocal(Long numeroLocal)
	{
		this.numeroLocal = numeroLocal;
	}

	public Long getNumeroPosto()
	{
		return numeroPosto;
	}

	public void setNumeroPosto(Long numeroPosto)
	{
		this.numeroPosto = numeroPosto;
	}

	public Long getNumeroContrato()
	{
		return numeroContrato;
	}

	public void setNumeroContrato(Long numeroContrato)
	{
		this.numeroContrato = numeroContrato;
	}

	public String getCodigoLocal()
	{
		return codigoLocal;
	}

	public void setCodigoLocal(String codigoLocal)
	{
		this.codigoLocal = codigoLocal;
	}

	public String getCentroCusto()
	{
		return this.centroCusto;
	}

	public void setCentroCusto(String centroCusto)
	{
		this.centroCusto = centroCusto;
	}

	public String getServico()
	{
		return this.servico;
	}

	public void setServico(String servico)
	{
		this.servico = servico;
	}

	public String getTelefone()
	{
		return this.telefone;
	}

	public void setTelefone(String telefone)
	{
		this.telefone = telefone;
	}
	
	
	public String getDddContato()
	{
		return this.dddContato;
	}

	public void setDddContato(String dddContato)
	{
		this.dddContato = dddContato;
	}
	
	public String getNumeroContato()
	{
		return this.numeroContato;
	}

	public void setNumeroContato(String numeroContato)
	{
		this.numeroContato = numeroContato;
	}

	public EscalaPostoVO getEscalaPosto()
	{
		return escalaPosto;
	}

	public void setEscalaPosto(EscalaPostoVO escalaPosto)
	{
		this.escalaPosto = escalaPosto;
	}

	public String getSituacao()
	{
		return this.situacao;
	}

	public void setSituacao(String situacao)
	{
		this.situacao = situacao;
	}

	public Long getVagas()
	{
		return this.vagas;
	}

	public void setVagas(Long vagas)
	{
		this.vagas = vagas;
	}

	public String getEndereco()
	{
		return this.endereco;
	}

	public void setEndereco(String endereco)
	{
		this.endereco = endereco;
	}

	public Collection<ColaboradorVO> getColaboradores()
	{
		return this.colaboradores;
	}

	public void setColaboradores(Collection<ColaboradorVO> colaboradores)
	{
		this.colaboradores = colaboradores;
	}

	public Collection<EntradaAutorizadaVO> getEntradasAutorizadas()
	{
		return this.entradasAutorizadas;
	}

	public void setEntradasAutorizadas(Collection<EntradaAutorizadaVO> entradasAutorizadas)
	{
		this.entradasAutorizadas = entradasAutorizadas;
	}

	public String getLotacao()
	{
		return lotacao;
	}

	public void setLotacao(String lotacao)
	{
		this.lotacao = lotacao;
	}

	public String getNomePosto()
	{
		return this.nomePosto;
	}

	public void setNomePosto(String nomePosto)
	{
		this.nomePosto = nomePosto;
	}
	
	public StatusVO getStatus()
	{
		return status;
	}

	public void setStatus(StatusVO status)
	{
		this.status = status;
	}

	public String getCidade()
	{
		return cidade;
	}

	public void setCidade(String cidade)
	{
		this.cidade = cidade;
	}

	public String getEstado()
	{
		return estado;
	}

	public void setEstado(String estado)
	{
		this.estado = estado;
	}

	public BigDecimal getValor()
	{
		return valor;
	}

	public void setValor(BigDecimal valor)
	{
		this.valor = valor;
	}

	public String getCep()
	{
		return cep;
	}

	public void setCep(String cep)
	{
		this.cep = cep;
	}

	public Long getVagasPresenca()
	{
		return vagasPresenca;
	}

	public void setVagasPresenca(Long vagasPresenca)
	{
		this.vagasPresenca = vagasPresenca;
	}

	public Date getDataInicio()
	{
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio)
	{
		this.dataInicio = dataInicio;
	}

	public Date getDataFim()
	{
		return dataFim;
	}

	public void setDataFim(Date dataFim)
	{
		this.dataFim = dataFim;
	}

	public Date getDataFimVigencia()
	{
		return dataFimVigencia;
	}

	public void setDataFimVigencia(Date dataFimVigencia)
	{
		this.dataFimVigencia = dataFimVigencia;
	}

	public String getBairro()
	{
		return bairro;
	}

	public void setBairro(String bairro)
	{
		this.bairro = bairro;
	}

	public String getNumero()
	{
		return numero;
	}

	public void setNumero(String numero)
	{
		this.numero = numero;
	}

	public ClienteVO getCliente()
	{
		return cliente;
	}

	public void setCliente(ClienteVO cliente)
	{
		this.cliente = cliente;
	}

	public String getMotivoEncerramento()
	{
		return motivoEncerramento;
	}

	public void setMotivoEncerramento(String motivoEncerramento)
	{
		this.motivoEncerramento = motivoEncerramento;
	}

	public Long getAjusteEscala()
	{
		return ajusteEscala;
	}

	public void setAjusteEscala(Long ajusteEscala)
	{
		this.ajusteEscala = ajusteEscala;
	}

	public String getCodigoPai()
	{
		return codigoPai;
	}

	public void setCodigoPai(String codigoPai)
	{
		this.codigoPai = codigoPai;
	}

	public String getStrDataInicio()
	{
		return strDataInicio;
	}

	public void setStrDataInicio(String strDataInicio)
	{
		this.strDataInicio = strDataInicio;
	}

	public Long getNumemp() {
		return numemp;
	}

	public void setNumemp(Long numemp) {
		this.numemp = numemp;
	}

	public Long getNumfil() {
		return numfil;
	}

	public void setNumfil(Long numfil) {
		this.numfil = numfil;
	}

	public String getCodSer() {
		return codSer;
	}

	public void setCodSer(String codSer) {
		this.codSer = codSer;
	}

	public String getUsuPossup()
	{
		return usuPossup;
	}

	public void setUsuPossup(String usuPossup)
	{
		this.usuPossup = usuPossup;
	}

	public Long getUsuIssup()
	{
		return usuIssup;
	}

	public void setUsuIssup(Long usuIssup)
	{
		this.usuIssup = usuIssup;
	}

	public long getUsuCodreg()
	{
		return usuCodreg;
	}

	public void setUsuCodreg(long usuCodreg)
	{
		this.usuCodreg = usuCodreg;
	}

	public String getObsEncerramento()
	{
		return obsEncerramento;
	}

	public void setObsEncerramento(String obsEncerramento)
	{
		this.obsEncerramento = obsEncerramento;
	}
	
	
}