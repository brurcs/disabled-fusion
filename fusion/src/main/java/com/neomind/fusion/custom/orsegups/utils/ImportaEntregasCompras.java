package com.neomind.fusion.custom.orsegups.utils;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.datawarehouse.ExternalEntityInfo;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class ImportaEntregasCompras {
	
	private static final Log log = LogFactory.getLog(ImportaEntregasCompras.class);
	
	/*
	 * importa registros de solicitações de compras
	 */
	public static boolean merge(Collection<NeoObject> solCompras, boolean compras)
	{
		try
		{
			//flags para controle de registros lidos
			int size = 0;
			int importados = 0;
			if(solCompras.size() > 0)
			{
				for(NeoObject solCompra : solCompras)
				{
					size++;
					if(log.isDebugEnabled())
					{
						log.debug("["+size+"] - Analisando registro "+size+"/"+solCompras.size());
					}
					EntityWrapper wLanc = new EntityWrapper(solCompra);
					String obs = NeoUtils.safeOutputString(wLanc.findValue("obssol"));
					//produto
					String codpro = NeoUtils.safeOutputString(wLanc.findValue("codpro"));
					//tamanho
					String codder = NeoUtils.safeOutputString(wLanc.findValue("codder"));
					//deposito
					String coddep = NeoUtils.safeOutputString(wLanc.findValue("coddep"));
					//quantidade
					long qtd = ((BigDecimal) wLanc.findValue("qtdsol")).longValue();
					//data
					GregorianCalendar data = (GregorianCalendar) wLanc.findValue("datsol");
					
					//com base na observação extrai o neoId do workflow, que está no texto
					long neoid = getNeoidFromWFP(obs);
					if(neoid == 0)
					{
						//entra aqui em alguns casos dos lançamentos, pois a query está ampla
						if(log.isDebugEnabled())
						{
							log.debug("["+size+"] - Não encontrado o workflow. Código(neoId) = "+neoid+". Obseme analisado = "+obs);
						}
						continue;
					}
						
					
					WFProcess wfp = (WFProcess) PersistEngine.getNeoObject(neoid); 
					if(wfp == null)
					{
						if(log.isDebugEnabled())
						{
							log.debug("["+size+"] - Não encontrado o workflow. Código(neoId) = "+neoid+". Obseme analisado = "+obs);
						}
						continue;
					}
					//pega a entidade do processo EPI
					NeoObject entity = wfp.getEntity();
					EntityWrapper wEntity = new EntityWrapper(entity);
					//busca os registros do histórico, que é o alvo das validações
					Collection<NeoObject> listaSolicitacoesCompra = wEntity.findField("fichaUniformeEPI.historicoEntregas").getValues();
					String numsol = NeoUtils.safeOutputString(wLanc.findValue("numsol"));
					
					//verifica se o registro em questão, do SAPIENS, está neste workflow
					//como o neoid do workflow estava nesse registro, se não existir nele deve ser criado.
					//se existe : atualiza o código sapiens
					//se não existe, cria o registro e adiciona a grid
					if(exist(listaSolicitacoesCompra, codpro, codder, coddep, qtd, numsol, compras))
					{
						if(log.isDebugEnabled())
						{
							log.debug("["+size+"] - Esta solicitação de compra ja está no fusion.");
						}
						continue;
					}else
					{
						importados++;
						if(log.isDebugEnabled())
						{
							log.debug("["+size+"] - IMPORTANDO solicitação de compra. CodigoSapiens = "+numsol+" (Ref. importados = "+importados+").");
						}
						//NumEme\SeqEme 
						
						//se não existe então cria o registro
						NeoUser user = getUser(obs);
						
						NeoObject produto = getProduto(codpro);
						NeoObject deposito = getDeposito(coddep);
						NeoObject tamanho = getTamanho(codder);
						
						//FUEListaEntregas
						NeoObject entrega = AdapterUtils.createNewEntityInstance("FUEListaEntregas");
						EntityWrapper wEntrega = new EntityWrapper(entrega);
						wEntrega.setValue("dataEntrega", data);
						wEntrega.setValue("depositoEntrega", deposito);
						wEntrega.setValue("produto", produto);
						wEntrega.setValue("derivacao", tamanho);
						wEntrega.setValue("quantidadeEntregue", qtd);
						wEntrega.setValue("entregaIntegrada", true);
						wEntrega.setValue("solicitarCompra", true);
						wEntrega.setValue("codigoSapiens", numsol);
						wEntrega.setValue("usuario", user);
						PersistEngine.persist(entrega);
						//após gerar o neoid setar no registro
						wEntrega.setValue("codigoEntrega", NeoUtils.safeOutputString(entrega.getNeoId()));
						PersistEngine.persist(entrega);
						
						wEntity.findField("fichaUniformeEPI.historicoEntregas").addValue(entrega);
					}
					
				}
			}
		}catch(Exception e)
		{
			log.error("Erro ao verificar a importação de solicitações de compras", e);
			return false;
		}
		return true;
	}
	
	/*
	 * importa registros de lançamentos de entregas, 
	 * quase idêntico ao metodo acima
	 */
	public static boolean merge(Collection<NeoObject> lancamentos)
	{
		try
		{
			int size = 0;
			int importados = 0;
			if(lancamentos.size() > 0)
			{
				for(NeoObject lanc : lancamentos)
				{
					size++;
					if(log.isDebugEnabled())
					{
						log.debug("["+size+"] - Analisando registro "+size+"/"+lancamentos.size());
					}
					EntityWrapper wLanc = new EntityWrapper(lanc);
					String obs = NeoUtils.safeOutputString(wLanc.findValue("obseme"));
					//produto
					String codpro = NeoUtils.safeOutputString(wLanc.findValue("codpro"));
					//tamanho
					String codder = NeoUtils.safeOutputString(wLanc.findValue("codder"));
					//deposito
					String coddep = NeoUtils.safeOutputString(wLanc.findValue("coddep"));
					//quantidade
					long qtdeme = ((BigDecimal) wLanc.findValue("qtdeme")).longValue();
					//data
					GregorianCalendar data = (GregorianCalendar) wLanc.findValue("dateme");
					
					long neoid = getNeoidFromWFP(obs);
					if(neoid == 0)
					{
						if(log.isDebugEnabled())
						{
							log.debug("["+size+"] - Não encontrado o workflow. Código(neoId) = "+neoid+". Obseme analisado = "+obs);
						}
						continue;
					}
						
					
					WFProcess wfp = (WFProcess) PersistEngine.getNeoObject(neoid);
					if(wfp == null)
					{
						if(log.isDebugEnabled())
						{
							log.debug("["+size+"] - Não encontrado o workflow. Código(neoId) = "+neoid+". Obseme analisado = "+obs);
						}
						continue;
					}
					NeoObject entity = wfp.getEntity();
					EntityWrapper wEntity = new EntityWrapper(entity);
					Collection<NeoObject> listaEntregas = wEntity.findField("fichaUniformeEPI.historicoEntregas").getValues();
					
					//código sapiens é composto por duas chaves, separadas por uma '/'
					String numeme = NeoUtils.safeOutputString(wLanc.findValue("numeme"));
					String seqeme = NeoUtils.safeOutputString(wLanc.findValue("seqeme"));
					String codigoSapiens = numeme + "/" + seqeme;
					//se existe : atualiza o código sapiens
					//se não existe, cria o registro e adiciona a grid
					if(exist(listaEntregas, codpro, codder, coddep, qtdeme, codigoSapiens, false))
					{
						if(log.isDebugEnabled())
						{
							log.debug("["+size+"] - Esta entrega ja está no fusion.");
						}
						continue;
					}else
					{
						importados++;
						if(log.isDebugEnabled())
						{
							log.debug("["+size+"] - IMPORTANDO entrega. CodigoSapiens = "+codigoSapiens+" (Ref. importados = "+importados+").");
						}
						//NumEme\SeqEme 
						
						NeoUser user = getUser(obs);
						NeoObject produto = getProduto(codpro);
						NeoObject deposito = getDeposito(coddep);
						NeoObject tamanho = getTamanho(codder);
						
						//FUEListaEntregas
						NeoObject entrega = AdapterUtils.createNewEntityInstance("FUEListaEntregas");
						EntityWrapper wEntrega = new EntityWrapper(entrega);
						wEntrega.setValue("dataEntrega", data);
						wEntrega.setValue("depositoEntrega", deposito);
						wEntrega.setValue("produto", produto);
						wEntrega.setValue("derivacao", tamanho);
						wEntrega.setValue("quantidadeEntregue", qtdeme);
						wEntrega.setValue("entregaIntegrada", true);
						wEntrega.setValue("solicitarCompra", false);
						wEntrega.setValue("codigoSapiens", codigoSapiens);
						wEntrega.setValue("usuario", user);
						PersistEngine.persist(entrega);
						//após gerar o neoid setar no registro
						wEntrega.setValue("codigoEntrega", NeoUtils.safeOutputString(entrega.getNeoId()));
						PersistEngine.persist(entrega);
						
						wEntity.findField("fichaUniformeEPI.historicoEntregas").addValue(entrega);
					}
					
				}
			}
		}catch(Exception e)
		{
			log.error("Erro ao verificar a importação de entregas", e);
			return false;
		}
		return true;
	}

	
	public static long getNeoidFromWFP(String s)
	{
		String neoid = "";
		//Integrado via processo R011 - Manutenção Ficha Uniforme e EPI - código 36796777
		
		//usar expressão regular, teste apenas
		if(s.contains("código"))
		{
			int idx = s.lastIndexOf("código") + 6;
			neoid = NeoUtils.safeOutputString(s.substring(idx, idx+9));
			return NeoUtils.safeLong(neoid.trim());
		}else
		{
			return 0;
		}
		
	}
	
	public static NeoUser getUser(String obs)
	{
		String name = getFullName(obs);
		if(name.equals("")) 
			return null;
		NeoUser user = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("fullname", name));
		return user;
	}
	
	public static String getFullName(String obs)
	{
		String name = "";
		if(obs.contains("Integrado por:"))
		{
			int idx = obs.lastIndexOf("Integrado por:");
			name = NeoUtils.safeOutputString(obs.substring(idx + 14, obs.length()));
			return NeoUtils.safeOutputString(name.trim());
		}
		return name;
	}
	
	public static boolean exist(Collection<NeoObject> lista, String codpro, String codder, String coddep, long qtd, String codigoSapiens, Boolean solicitarCompra)
	{
		for(NeoObject obj : lista)
		{
			EntityWrapper w = new EntityWrapper(obj);
			//depositoEntrega, produto, quantidadeEntregue, derivacao
			NeoObject deposito = (NeoObject) w.findValue("depositoEntrega");
			NeoObject produto = (NeoObject) w.findValue("produto");
			NeoObject tamanho = (NeoObject) w.findValue("derivacao");
			long quantidade = NeoUtils.safeLong(NeoUtils.safeOutputString(w.findValue("quantidadeEntregue")));
			
			if(produtoIgual(produto, codpro) && tamanhoIgual(tamanho, codder) && depositoIgual(deposito, coddep) && quantidade == qtd)
			{
				updateCodigoSapiens(obj, codigoSapiens, solicitarCompra);
				return true;
			}
		}
		return false;
	}
	
	public static void updateCodigoSapiens(NeoObject obj, String codigoSapiens, Boolean solicitarCompra)
	{
		EntityWrapper w = new EntityWrapper(obj);
		w.setValue("codigoSapiens", codigoSapiens);
		w.setValue("solicitarCompra", solicitarCompra);
		PersistEngine.persist(obj);
	}
	
	public static boolean produtoIgual(NeoObject produto, String codpro)
	{
		EntityWrapper wProduto = new EntityWrapper(produto);
		String cod = NeoUtils.safeOutputString(wProduto.findValue("codpro"));
		if(cod.equals(codpro))
			return true;
		return false;
	}
	
	public static boolean tamanhoIgual(NeoObject tamanho, String codder)
	{
		if(tamanho == null && (codder == null || codder.trim().equals("") ))
			return true;
		
		if(tamanho == null)
			return false;
		
		EntityWrapper wProduto = new EntityWrapper(tamanho);
		String cod = NeoUtils.safeOutputString(wProduto.findValue("codder"));
		if(cod.equals(codder))
			return true;
		return false;
	}
	
	public static boolean depositoIgual(NeoObject deposito, String coddep)
	{
		EntityWrapper wProduto = new EntityWrapper(deposito);
		String cod = NeoUtils.safeOutputString(wProduto.findValue("depositoSapiens.coddep"));
		if(cod.equals(coddep))
			return true;
		return false;
	}
	
	public static NeoObject getProduto(String codpro)
	{
		NeoObject produto = getFromExternal("SAPIENSFUEPRO", "codpro", codpro);
		return produto;
	}
	
	public static NeoObject getTamanho(String codder)
	{
		NeoObject tamanho = getFromExternal("SAPIENSFUEDER", "codder", codder);
		return tamanho;
	}
	
	public static NeoObject getDeposito(String coddep)
	{
		NeoObject depositoSAPIENS = getFromExternal("SAPIENSFUEDEP", "coddep", coddep);
		NeoObject depositoFUSION = PersistEngine.getObject(AdapterUtils.getEntityClass("GCDepositos"), new QLEqualsFilter("depositoSapiens", depositoSAPIENS));
		return depositoFUSION;
	}
	
	public static NeoObject getFromExternal(String table, String field, String code)
	{
		ExternalEntityInfo z = (ExternalEntityInfo) EntityRegister.getInstance().getCache().getByType(table);
		List<NeoObject> objetos = (List<NeoObject>) PersistEngine.getObjects(z.getEntityClass(), new QLEqualsFilter(field, code));
		if(objetos.size() <=0)
			return null;
		return objetos.get(0);
		
	}
}
