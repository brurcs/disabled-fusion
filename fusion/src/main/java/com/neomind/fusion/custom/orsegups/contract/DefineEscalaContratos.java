package com.neomind.fusion.custom.orsegups.contract;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class DefineEscalaContratos implements AdapterInterface {

    @Override
    public void start(Task origin, EntityWrapper processEntity, Activity activity) {

	String msgErro = null;

	try {
	    Long codigoCliente = (Long) processEntity.findValue("buscaNomeCliente.codcli");

	    Connection conn = PersistEngine.getConnection("SAPIENS");

	    PreparedStatement pstm = null;
	    ResultSet rs = null;

	    StringBuilder sqlConn = new StringBuilder();

	    sqlConn.append(" SELECT TIPEMC FROM E085CLI");
	    sqlConn.append(" WHERE CODCLI = ?");

	    pstm = conn.prepareStatement(sqlConn.toString());
	    pstm.setLong(1, codigoCliente);

	    rs = pstm.executeQuery();

	    while (rs.next()) {

		Long tipoMercado = rs.getLong("tipemc");

		if (tipoMercado == 1L) {

		    // Tipo de Mercado Privado - Setor Responsável: Comercial
		    // Privado

		    NeoPaper papelEscalada = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "FGCResponsavelComercialPrivado"));

		    processEntity.setValue("papelEscala", papelEscalada);

		} else if (tipoMercado == 2L) {

		    // Tipo de Mercado Público - Setor Responsável: Comercial
		    // Público

		    NeoPaper papelEscalada = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "FGCResponsavelComercialPublico"));

		    processEntity.setValue("papelEscala", papelEscalada);

		} else if (tipoMercado == 0L) {

		    // Tipo de Mercado Indefinido - Pool administrado pelo
		    // Comercial Privado & Público

		    NeoPaper papelEscalada = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "FGCResponsavelTipEmcIndefinido"));

		    processEntity.setValue("papelEscala", papelEscalada);

		} else {

		    msgErro = "Tipo de mercado do cliente inválido, ou não preenchido. Verifique o cadastro do cliente " + codigoCliente + "!";
		    throw new WorkflowException(msgErro);

		}
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    if(msgErro == null){
		throw new WorkflowException("Erro na classe: "+ this.getClass() +". Verifique junto ao setor de T.I");
	    } else {
		throw new WorkflowException(msgErro);
	    }
	}
    }

    @Override
    public void back(EntityWrapper processEntity, Activity activity) {

    }

}
