package com.neomind.fusion.custom.orsegups.serasa;

/**
 * Classe responsável por criar instancia do registro com base on layout pre-definido
 * @author danilo.silva
 *
 */
public class RegistroSerasaFactory {
	
	
	
	public static RegistroSerasa getReg(String regId) throws Exception{
		if (regId == null){
			return null;
		}else if (regId.equals("B49C")){
			RegistroSerasa r = new RegistroSerasa("B49C");
			r.setupNomesColunas("FILLER1","FILLER2","NUM DOC","TIPO PESSOA","BASE CONS","MODALIDADE","VLR CONSUL","CENTRO CUST",
					"CODIFICADO","QTD REG","CONVERSA","FUNÇÃO","TP CONSULTA","ATUALIZA","IDENT_TERM","RESCLI","DELTS","COBRA",
					"PASSA","CONS.COLLEC","FILLER","CONSULTANTE","FILLER");
			r.setupTamanhoCampos(4,6,15,1,6,2,7,12,1,2,1,3,1,1,18,10,1,1,1,1,57,15,234);
			r.setupTipos("X","X","N","X","X","X","N","X","X","N","X","X","X","X","X","X","X","X","X","X","X","N","X");
			return r;
		}
		else if (regId.equals("P002")){
			RegistroSerasa r = new RegistroSerasa("P002");
			r.setupNomesColunas("TIPO–REG","COD1","CHAVE1","COD2","CHAVE2","COD3","CHAVE3","COD4","CHAVE4","FILLER");
			r.setupTamanhoCampos(4,4,21,4,21,4,21,4,21,11);
			r.setupTipos("X","X","X","X","X","X","X","X","X","X");
			return r;
		}else if (regId.equals("I001")){
			RegistroSerasa r = new RegistroSerasa("I001");
			r.setupNomesColunas("Tipo_Reg",
					"Subtipo","Filler","Filler","Filler","Filler","Filler","Filler","Filler",
					"Filler","Filler","Filler","Filler","Filler","Filler","Filler","Filler",
					"Filler","Filler","Filler","Filler","Filler","Filler","Filler","Filler",
					"Filler","Filler","Filler");

			r.setupTamanhoCampos(4,2,1,1,1,1,4,1,4,10,1,1,2,1,1,1,2,2,2,2,2,1,1,1,1,17,47,01);
			// tipos oficiais do layout
			//r.setupTipos("X","X","X","X","X","X","X","X","N","N","X","X","N","X","X","X","X","X","X","X","X","X","X","X","X","X","X","X");
			// mas só funciona com os tipos abaixo
			r.setupTipos("X","X","X","X","X","X","X","X","X","X","X","X","X","X","X","X","X","X","X","X","X","X","X","X","X","X","X","X");
			return r;
		}else if (regId.equals("I00199")){
			RegistroSerasa r = new RegistroSerasa("I00199");
			r.setupNomesColunas("Tipo_Reg","Subtipo","Filler","Filler");
			r.setupTamanhoCampos(4,2,3,106);
			r.setupTipos("X","X","X","X");
			return r;
		}else if (regId.equals("A900")){
			RegistroSerasa r = new RegistroSerasa("A900");
			r.setupNomesColunas("TIPO-REG","CÓDIGO","MENS REDUZ","MENS COMPL","FILLER");
			r.setupTamanhoCampos(4,6,32,70,3);
			r.setupTipos("X","N","X","X","X");
			return r;
		}else if (regId.equals("T999")){
			RegistroSerasa r = new RegistroSerasa("T999");
			r.setupNomesColunas("TIPO-REG","CÓDIGO","MENSAGEM","FILLER");
			r.setupTamanhoCampos(4,3,70,38);
			r.setupTipos("X","N","X","X");
			return r;
		}
		else{
			throw new Exception("Registro nao encontrado");
		}
	}

}
