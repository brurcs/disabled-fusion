package com.neomind.fusion.custom.orsegups.adapter.r001v2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.event.TaskFinishEvent;
import com.neomind.fusion.workflow.event.TaskFinishEventListener;
import com.neomind.fusion.workflow.exception.TaskException;

public class VerificarFeriasAtivaRubi implements TaskFinishEventListener {

	@Override
	public void onFinish(TaskFinishEvent event) throws TaskException {
		
		EntityWrapper processEntity = event.getWrapper();
		
		List<NeoObject> registroAtividades = (List<NeoObject>) processEntity.getValue("r001V2RegistroAtividade");
		
		InstantiableEntityInfo insRegAti = AdapterUtils.getInstantiableEntityInfo("r001V2RegistroAtividades");
		NeoObject objRegAti = insRegAti.createNewInstance();
		EntityWrapper wRegAti = new EntityWrapper(objRegAti);
				
		String observacao = "";
		String usuario = "SISTEMA";
		
		if ("sistemaFusion".equals(event.getTask().getFinishByUser().getFullName())) {
			
			observacao = "Avançado automaticamente pelo SISTEMA";
			
		} else {
			
			usuario = event.getTask().getFinishByUser().getFullName();
			observacao = "Avançado manualmente por: " + usuario;
			
		}
		
		if(verificaFeriasAtivasRubi(Integer.parseInt(processEntity.findField("neoId").getValue().toString()))) {
			observacao += " - Férias <b>ATIVAS</b> no RUBI";
			processEntity.findField("feriasAtivaNoRubi").setValue(true);
		} else {
			observacao += " - Férias <b>INATIVAS</b> no RUBI";
			if (processEntity.findField("obsFeriasInativasRubi") != null) {
				observacao += " | Motivo: " + processEntity.getValue("obsFeriasInativasRubi").toString();
			}
			processEntity.findField("feriasAtivaNoRubi").setValue(false); 
		}

		GregorianCalendar dataAcao = new GregorianCalendar();

		wRegAti.setValue("usuario", usuario);
		wRegAti.setValue("dataAcao", dataAcao);
		wRegAti.setValue("descricao", observacao);
		wRegAti.setValue("anexo", null);
		
		PersistEngine.persist(objRegAti);
		registroAtividades.add(objRegAti);

		processEntity.setValue("r001V2RegistroAtividade", registroAtividades);
			
	}
	
	private boolean verificaFeriasAtivasRubi(Integer _neoId) {		
		
		boolean feriasAtivas = true;
		String inifer = null;
		Integer numemp = null;
		Integer numcad = null;
		
		try {
			
			NeoObject formulario = PersistEngine.getObject(AdapterUtils.getEntityClass("r001V2ProgramacaoFerias"), new QLEqualsFilter("neoId", _neoId));
			EntityWrapper wr = new EntityWrapper(formulario);
			
			if (wr.findField("feriasAtivaNoRubi").getValue() != null && (boolean) wr.findField("feriasAtivaNoRubi").getValue() == false) {
				feriasAtivas = false;
			} else {
				Calendar cal = (Calendar) wr.findField("inicioFerias").getValue();
				
				final DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				inifer = df.format(cal.getTime());
				
				NeoObject colaborador = (NeoObject) wr.findField("colaboradorCoberturaFerias").getValue();
				EntityWrapper wColaborador = new EntityWrapper(colaborador);			
				
				numemp = Integer.parseInt(wColaborador.findField("numemp").getValue().toString());
				numcad = Integer.parseInt(wColaborador.findField("numcad").getValue().toString());
				
				//Colaborador
				QLEqualsFilter filterNumemp = new QLEqualsFilter("numemp", numemp);			
				QLEqualsFilter filterNumcad = new QLEqualsFilter("numcad", numcad);
				QLEqualsFilter filterIniFer = new QLEqualsFilter("inifer", cal.getTime());
				
				QLGroupFilter filtro = new QLGroupFilter("AND");
				filtro.addFilter(filterNumemp);			
				filtro.addFilter(filterNumcad);
				filtro.addFilter(filterIniFer);
				
				List<NeoObject> list = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORHR040FEM"), filtro);
				System.out.println("VerificaFeriasAtivaRubi: filtro"+filtro);
				System.out.println("VerificaFeriasAtivaRubi: lista"+list.size());
				if (list.size() == 0) {
					feriasAtivas = false;
				}
			}
			
		} catch (Exception e) {
			System.out.println("ERRO VerificaFeriasAtivaRubi");
			e.printStackTrace();
		}
				
		return feriasAtivas;
	}
}
