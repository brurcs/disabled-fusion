package com.neomind.fusion.custom.orsegups.mainlingcontratos.vo;

public class RegionalVO
{

	private String codigoRegional;
	private String nomeRegional;

	public String getCodigoRegional()
	{
		return codigoRegional;
	}

	public void setCodigoRegional(String codigoRegional)
	{
		this.codigoRegional = codigoRegional;
	}

	public String getNomeRegional()
	{
		return nomeRegional;
	}

	public void setNomeRegional(String nomeRegional)
	{
		this.nomeRegional = nomeRegional;
	}

}
