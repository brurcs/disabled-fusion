package com.neomind.fusion.custom.orsegups.endpoint;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.neomind.fusion.custom.orsegups.endpoint.annotation.Secured;
import com.neomind.fusion.custom.orsegups.endpoint.dto.QLPCoberturaDTO;
import com.neomind.fusion.custom.orsegups.endpoint.dto.QLPTrocaDTO;
import com.neomind.fusion.custom.orsegups.endpoint.dto.RespostaDTO;
import com.neomind.fusion.custom.orsegups.endpoint.dto.TarefaSimplesDTO;
import com.neomind.fusion.custom.orsegups.endpoint.engine.PresencaEngine;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;

@Path("endpoint")
public class OrsegupsRESTService {

    @POST
    @Secured
    @Path("abrirTarefaSimples")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response mySecuredMethod(TarefaSimplesDTO ts) {

	RespostaDTO resposta = new RespostaDTO();

	if (ts != null) {

	    if (ts.getTitulo() != null && !ts.getTitulo().isEmpty() && ts.getDescricao() != null && !ts.getDescricao().isEmpty() && ts.getSolicitante() != null && !ts.getSolicitante().isEmpty() && ts.getExecutor() != null && !ts.getExecutor().isEmpty()) {

		IniciarTarefaSimples tarefaSimples = new IniciarTarefaSimples();

		GregorianCalendar prazo = new GregorianCalendar();

		if (ts.getPrazo() != null && ts.getPrazo() > 0) {
		    prazo = OrsegupsUtils.getSpecificWorkDay(prazo, ts.getPrazo());
		} else {
		    prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
		}

		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);

		String retorno = tarefaSimples.abrirTarefa(ts.getSolicitante(), ts.getExecutor(), ts.getTitulo(), ts.getDescricao(), "1", "sim", prazo);

		if (retorno.contains("Erro")) {
		    resposta.setMensagem(retorno);
		    return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
		} else {
		    resposta.setMensagem("Sucesso");
		    resposta.setTarefa(retorno);
		    return Response.ok(resposta, MediaType.APPLICATION_JSON_TYPE).build();
		}

	    } else {
		resposta.setMensagem("Apenas o campo 'prazo' é opcional!");
		return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
	    }
	} else {
	    resposta.setMensagem("Corpo da requisição não pode ser nulo!");
	    return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
	}

    }

    /**
     * @api {post} /services/endpoint/qlpCoberturaColaborador
     *      qlpCoberturaColaborador
     * @apiDescription Efetua o lançamento de cobertura de colaborador para o
     *                 período informado
     * @apiVersion 1.0.0
     * @apiName qlpCoberturaColaborador
     * @apiGroup QLP
     * @apiHeader {String} Authorization Sua chave de acesso à API.
     * @apiHeader {String} Content-Type =application/json
     * @apiHeaderExample {String} Header-Example: Authorization: "seutoken"
     *                   Content-Type: application/json
     * @apiParam {String} colaborador Colaborador que efetuará a substuição
     * @apiParam {String} colaboradorASubstituir Colaborador que será
     *           substituido
     * @apiParam {Number} dataInicio Tempo em milésimos de segundos referente a
     *           data de inicio da cobertura
     * @apiParam {Number} dataFim Tempo em milésimos de segundos referente a
     *           data de inicio da cobertura
     * @apiParam {Number} idMotivo ID de um dos motivos da cobertura: <br>
     *           1 Reciclagem <br>
     *           2 Falta de Efetivo <br>
     *           3 Treinamento <br>
     *           4 Ausência (a justificar) <br>
     *           5 Folga <br>
     *           6 Atestado <br>
     *           7 Dispensa Sindicato <br>
     *           8 Extensão de horário <br>
     *           9 Suspensão <br>
     *           10 Cobertura SDF <br>
     *           11 Serviço Extra <br>
     *           12 Cobertura de almoço <br>
     *           13 Atraso de rendição <br>
     *           14 Treinamento para cobertura de férias <br>
     *           15 Treinamento para efetivação <br>
     *           16 Inversão de escala <br>
     *           17 Remanejamento <br>
     *           18 Cobertura Semanal <br>
     * @apiParam {String} [observacao] Observação da cobertura
     * @apiParamExample {json} Exemplo Requisição: { "colaborador":
     *                  "19-1-999452", "colaboradorASubstituir": "19-1-999449",
     *                  "dataInicio": 1514462400000, "dataFim": 1514466000000,
     *                  "idMotivo": 2, "observacao": "Teste TI" }
     */
    @POST
    @Secured
    @Path("qlpCoberturaColaborador")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response qlpCoberturaColaborador(QLPCoberturaDTO c) {

	return PresencaEngine.lancarCobertura(c, "0002");

    }

    @DELETE
    @Secured
    @Path("qlpCoberturaColaborador")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response qlpDeleteCoberturaColaborador(QLPCoberturaDTO c) {

	return PresencaEngine.removerCoberturaColaborador(c);

    }

    @POST
    @Secured
    @Path("qlpCoberturaColaboradorFerias")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response qlpCoberturaColaboradorFerias(QLPCoberturaDTO c) {

	return PresencaEngine.lancarCobertura(c, "0003");

    }

    @POST
    @Secured
    @Path("qlpInversaoEscala")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response qlpInversaoEscala(QLPCoberturaDTO c) {

	return PresencaEngine.lancarCobertura(c, "0015");

    }

    @POST
    @Secured
    @Path("qlpCoberturaPosto")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response qlpCoberturaPosto(QLPCoberturaDTO c) {

	return PresencaEngine.lancarCobertura(c, "0007");

    }

    @DELETE
    @Secured
    @Path("qlpCoberturaPosto")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response qlpDeleteCoberturaPosto(QLPCoberturaDTO c) {

	c.setColaboradorASubstituir("");
	return PresencaEngine.removerCoberturaColaborador(c);

    }

    @POST
    @Secured
    @Path("qlpCoberturaPostoHorista")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response qlpCoberturaPostoHorista(QLPCoberturaDTO c) {

	return PresencaEngine.lancarCobertura(c, "0007");

    }

    @DELETE
    @Secured
    @Path("qlpCoberturaPostoHorista")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response qlpDeleteCoberturaPostoHorista(QLPCoberturaDTO c) {

	return PresencaEngine.removerCoberturaColaborador(c);

    }

    @POST
    @Secured
    @Path("qlpCoberturaColaboradorComHoraExtra")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response qlpCoberturaColaboradorComHoraExtra(QLPCoberturaDTO c) {

	return PresencaEngine.lancarCobertura(c, "0014");

    }

    @DELETE
    @Secured
    @Path("qlpCoberturaColaboradorComHoraExtra")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response qlpDeleteCoberturaColaboradorComHoraExtra(QLPCoberturaDTO c) {

	return PresencaEngine.removerCoberturaColaborador(c);

    }

    @POST
    @Secured
    @Path("qlpCoberturaPostoComHoraExtra")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response qlpCoberturaPostoComHoraExtra(QLPCoberturaDTO c) {

	return PresencaEngine.lancarCobertura(c, "0013");

    }

    @DELETE
    @Secured
    @Path("qlpCoberturaPostoComHoraExtra")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response qlpDeleteCoberturaPostoComHoraExtra(QLPCoberturaDTO c) {

	return PresencaEngine.removerCoberturaColaborador(c);

    }

    @POST
    @Secured
    @Path("qlpTrocaHorario")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response qlpCoberturaTrocaHorario(QLPCoberturaDTO c) {

	return PresencaEngine.lancarCobertura(c, "0016");

    }

    @POST
    @Secured
    @Path("qlpTrocaPosto")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response qlpTrocaPosto(QLPTrocaDTO c) {

	return PresencaEngine.lancarTroca(c, "0004");

    }

    @POST
    @Secured
    @Path("qlpTrocaEscala")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response qlpTrocaEscala(QLPTrocaDTO c) {

	return PresencaEngine.lancarTroca(c, "0008");

    }

    @DELETE
    @Secured
    @Path("qlpDeleteTrocaEscala")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response qlpDeleteTrocaEscala(QLPTrocaDTO t) {

	return PresencaEngine.removerTrocaEscala(t);

    }




}
