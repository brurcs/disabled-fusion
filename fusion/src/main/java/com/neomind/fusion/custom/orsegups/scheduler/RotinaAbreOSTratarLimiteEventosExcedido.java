package com.neomind.fusion.custom.orsegups.scheduler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.common.base.Joiner;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.sigma.SigmaUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class RotinaAbreOSTratarLimiteEventosExcedido implements CustomJobAdapter
{

	private static final Log log = LogFactory.getLog(RotinaAbreOSTratarLimiteEventosExcedido.class);

	@SuppressWarnings("unchecked")
	@Override
	public void execute(CustomJobContext arg0)
	{

		log.warn("##### Ínicia Rotina Abre OS para Tratar Limite de Eventos Excedido - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		Long key = GregorianCalendar.getInstance().getTimeInMillis(); 
				
		try
		{
			
			List<NeoObject> limiteEvento = PersistEngine.getObjects(AdapterUtils.getEntityClass("ConfigDeExcessoDeEventosSIGMA"));
			Long limite = 0L;
			Long respPad = 0L;
			String concIdCod = "";
			boolean passou = true;
			
			String defeitosOS = "";
			String tipoEvento = "";
			String nomesClientes = "";
			
			for (NeoObject eventLim : limiteEvento) 
			{
				EntityWrapper wRegistroAtividade = new EntityWrapper(eventLim);
				limite = (Long) wRegistroAtividade.findValue("LimiteDeEventosDiario");
				
				NeoObject responsavelPadrao = (NeoObject) wRegistroAtividade.findValue("tecnicoResponsavelPadrao");
				EntityWrapper wresponsavelPadrao = new EntityWrapper(responsavelPadrao);
				respPad = (Long) wresponsavelPadrao.findValue("tecnicoResponsavelPadrao.cd_colaborador");
				
				List<NeoObject> CUCsIgnoradas = (List<NeoObject>) wRegistroAtividade.findValue("CUCsIgnoradas");
				
				if(CUCsIgnoradas != null && !CUCsIgnoradas.isEmpty()){
				    for (NeoObject cuc : CUCsIgnoradas)
					{
						EntityWrapper cucs = new EntityWrapper(cuc);
						NeoObject objCuc = (NeoObject) cucs.findValue("listaCUCsSigma.CUCsIgnoradas.CUCs");
						EntityWrapper wpObjCuc = new EntityWrapper(objCuc);
						String idCod = (String) wpObjCuc.findValue("id_code");
						if (passou) {
							concIdCod = "'"+idCod+"'";
							passou = false;
						} else {
							concIdCod += ",'"+idCod+"'";
						}
					}
				}
				
				
				List<NeoObject> listaDefeitoOS = (List<NeoObject>) wRegistroAtividade.findValue("defeitoOS");
				
				defeitosOS = this.getListaDefeito(listaDefeitoOS);
				
				List<NeoObject> listaTipoEvento = (List<NeoObject>) wRegistroAtividade.findValue("descricaoTipoEvento");
				
				tipoEvento = this.getListaTipoEvento(listaTipoEvento);
				
				List<NeoObject> listaNomeCliente = (List<NeoObject>) wRegistroAtividade.findValue("nomesClientesIgnorados");
				
				nomesClientes = this.getListaNomesClientes(listaNomeCliente);
										
			}
			
			StringBuilder sql = new StringBuilder();
			
			sql.append(" SELECT r.NM_ROTA, c.CD_TECNICO_RESPONSAVEL, c.PARTICAO, c.ID_CENTRAL, c.ID_EMPRESA,  h.CD_CLIENTE, count(*) as total "); 
			sql.append(" FROM VIEW_HISTORICO h WITH(NOLOCK)"); 
			sql.append(" INNER JOIN dbCENTRAL c WITH(NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
			sql.append(" LEFT JOIN ROTA R WITH(NOLOCK) ON R.CD_ROTA = C.ID_ROTA ");
			sql.append(" WHERE ("+tipoEvento+")" ); 
			if (!concIdCod.equals("")) {
				sql.append(" AND h.CD_CODE NOT IN ("+concIdCod+") "); 
			}
			//sql.append(" AND (H.TX_OBSERVACAO_FECHAMENTO NOT LIKE 'Evento fechado automaticamente. Cliente em manutenção.%' AND H.CD_USUARIO_FECHAMENTO != 9999) ");
			sql.append(" AND h.DT_RECEBIDO >= DATEADD(HOUR, -24, GETDATE()) ");
			sql.append(" AND NOT EXISTS (SELECT * FROM DBCENTRAL C2 WITH(NOLOCK) WHERE C.CD_CLIENTE = C2.CD_CLIENTE AND ("+nomesClientes+") )  ");
			sql.append(" AND (NOT EXISTS ( SELECT * FROM dbORDEM OS WITH(NOLOCK) WHERE (OS.FECHADO = 0 OR OS.FECHAMENTO IS NULL) AND OS.IDOSDEFEITO IN ("+defeitosOS+") AND OS.CD_CLIENTE = H.CD_CLIENTE) ");
			sql.append(" AND  H.DT_RECEBIDO >  (SELECT MAX(OS.ABERTURA) FROM dbORDEM OS WITH(NOLOCK) WHERE (OS.FECHADO = 1 OR OS.FECHAMENTO IS NOT NULL)");
			sql.append(" AND OS.IDOSDEFEITO IN ("+defeitosOS+") AND OS.CD_CLIENTE = H.CD_CLIENTE)");
			sql.append(" OR NOT EXISTS ( SELECT * FROM dbORDEM OS WITH(NOLOCK) WHERE  OS.IDOSDEFEITO IN ("+defeitosOS+") AND OS.CD_CLIENTE = H.CD_CLIENTE)) ");
			sql.append(" GROUP BY  h.CD_CLIENTE,  c.ID_CENTRAL, c.PARTICAO, c.ID_EMPRESA, c.CD_TECNICO_RESPONSAVEL, r.NM_ROTA ");
			sql.append(" HAVING COUNT(*) >= "+limite);

			System.out.println(sql.toString()); 
	
			Query queryExecute = PersistEngine.getEntityManager("SIGMA90").createNativeQuery(sql.toString());
						
			Collection<Object> resultList = queryExecute.getResultList();
			
			GregorianCalendar dataIni = (GregorianCalendar) GregorianCalendar.getInstance();
			dataIni.add(Calendar.HOUR_OF_DAY, -24);
			String dataInicio = NeoDateUtils.safeDateFormat(dataIni, "dd/MM/yyyy HH:mm:ss");
			
			GregorianCalendar dataF = (GregorianCalendar) GregorianCalendar.getInstance();
			String dataFim = NeoDateUtils.safeDateFormat(dataF, "dd/MM/yyyy HH:mm:ss");
			
			for(Object os : resultList) {
				Object[] result = (Object[]) os;
				Integer tecnicoResponsavel = (Integer)result[1];
				if (tecnicoResponsavel == null || tecnicoResponsavel == 0) {
					tecnicoResponsavel = Integer.valueOf(respPad.toString());  
				}
				String particao = (String)result[2];
				String idCentral = (String)result[3];
				int idEmpresa = (Integer)result[4];
				int cdCliente = (Integer)result[5];
				int total = (Integer)result[6];
				String descricao = "Verificar Excesso de eventos transmitidos via linha telefônica nesta conta. \n";
				descricao += "No período de "+dataInicio+" a "+dataFim+" foram recebidos "+total+" eventos que podem causar altos custos na linha telefônica do cliente.";
 				SigmaUtils.getAbrirOSSigma(String.valueOf(cdCliente), particao, String.valueOf(idCentral), String.valueOf(idEmpresa), "10030", "106", String.valueOf(tecnicoResponsavel), descricao);
				//Conta do Sr. Laercio
				//SigmaUtils.getAbrirOSSigma("50931", particao, "B409", "10001", "10011", "106", String.valueOf(tecnicoResponsavel), descricao);
			}
			log.warn("##### FIM da Excussão da Rotina Abre OS para Tratar Limite de Eventos Excedido - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		}
		catch (Exception e)
		{

			log.error("##### AGENDADOR DE TAREFA: Erro ao Abrir OS para Tratar Limite de Eventos Excedido");
			System.out.println("["+key+"] Erro ao Abrir OS para Tratar Limite de Eventos Excedido");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
		}
	}
	
	
    private String getListaNomesClientes(List<NeoObject> listaNomeCliente) {
	String clientes = "";
	
	if (listaNomeCliente != null && !listaNomeCliente.isEmpty()){
	    int i = 0;
	    for (NeoObject cliente : listaNomeCliente){
		EntityWrapper wrapperTipoEvento = new EntityWrapper(cliente);
		String nomeCliente = (String) wrapperTipoEvento.findField("nomeCliente").getValue();
		if (i == 0){
		    clientes += " C2.RAZAO LIKE '%"+nomeCliente+"%'  OR  C2.FANTASIA LIKE '%"+nomeCliente+"%' ";
		}else{
		    clientes += " OR C2.RAZAO LIKE '%"+nomeCliente+"%'  OR  C2.FANTASIA LIKE '%"+nomeCliente+"%' ";
		}
		
		i++;
		
	    }
	}
	
	return clientes;
	}

    private String getListaTipoEvento(List<NeoObject> listaTipoEvento) {
	
	String eventos = "";
	
	if (listaTipoEvento != null && !listaTipoEvento.isEmpty()){
	    int i = 0;
	    for (NeoObject tipoEvento : listaTipoEvento){
		EntityWrapper wrapperTipoEvento = new EntityWrapper(tipoEvento);
		String evento = (String) wrapperTipoEvento.findField("descricaoTipoEvento").getValue();
		if (i == 0){
		    eventos += "h.DS_TIPO_EVENTO_RECEBIDO LIKE '%"+evento+"%' ";
		}else{
		    eventos += " OR h.DS_TIPO_EVENTO_RECEBIDO LIKE '%"+evento+"%'";
		}
		
		i++;
		
	    }
	}
	
	return eventos;
    }

    private String getListaDefeito(List<NeoObject> listaDefeitos) {

	String codigos = "";

	ArrayList<Integer> listaCodDefeito = new ArrayList<Integer>();

	if (listaDefeitos != null && !listaDefeitos.isEmpty()) {
	    for (NeoObject excecao : listaDefeitos) {

		EntityWrapper wrappperExcecao = new EntityWrapper(excecao);

		Long codDefeito = (Long) wrappperExcecao.findField("idosdefeito").getValue();

		listaCodDefeito.add(Integer.valueOf(codDefeito.intValue()));

	    }
	}

	codigos = Joiner.on(",").join(listaCodDefeito);

	return codigos;
    }

}

