package com.neomind.fusion.custom.casvig.mobile.xml.entity;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.casvig.mobile.CasvigMobileServlet;
import com.neomind.fusion.custom.casvig.mobile.MobileUtils;
import com.neomind.fusion.custom.casvig.mobile.entity.CasvigMobileEntityPool;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
//import com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServlet;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.util.NeoUtils;


/**
 * 
 * @author Daniel Henrique Joppi
 * 
 */
public class Reinspection implements MobileXmlInterface
{
	private NeoObject entity;
	private static final Log log = LogFactory.getLog(Reinspection.class);

	public NeoObject createNewInstance(InstantiableEntityInfo entityInfo)
	{
		this.entity = entityInfo.createNewInstance();
		return this.entity;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.neomind.fusion.custom.casvig.mobile.xml.entity.MobileXmlInterface
	 * #setInstance(Document document)
	 */
	public NeoObject setInstance(Document document)
	{
		if (this.entity == null)
			return null;

		boolean notConform = false;
		boolean isReinspection = false;

		String postoTrabalho = "";

		long codTask = 0;

		EntityWrapper objWrapper = new EntityWrapper(this.entity);
		Element elem = document.getDocumentElement(); // Resultados
		if (elem != null && elem.hasChildNodes())
		{
			NodeList nodeList = elem.getChildNodes();
			for (int k = 0; k < nodeList.getLength(); k++)
			{
				Element elemInsp = (Element) nodeList.item(k); // Resultado

				String tipo = String.valueOf(elemInsp.getAttribute("tipo_inspecao")); // valor
																						// tipo_inspecao
				
				if (/*tipo.equals("Reinspe��o") || tipo.equals("Reinspecao") || tipo.equals("Reinspeção") || */ tipo.toLowerCase().startsWith("reinsp"))
				{ // verifica reinspe��o
					isReinspection = true;
				}
				
				log.debug("tipo_inspecao: " + tipo + " isreinspection" + isReinspection );
				System.out.println("tipo_inspecao: " + tipo + " isreinspection" + isReinspection );
				codTask = Long.valueOf(elemInsp.getAttribute("cod_task")); // valor
																			// cod_task

				objWrapper.setValue("mobileNeoId", new BigDecimal(elemInsp.getAttribute("mobile_neoId"))); // valor
																											// mobile_neoId

				objWrapper.setValue("codposto", elemInsp.getAttribute("cod_postoTrabalho")); // valor
																								// cod_postoTrabalho

				objWrapper.setValue("nomeempresa", elemInsp.getAttribute("nome_empresa")); // valor
																							// nome_empresa

				objWrapper.setValue("nomecliente", elemInsp.getAttribute("nome_cliente")); // valor
																							// nome_cliente

				objWrapper.setValue("nomelotacao", elemInsp.getAttribute("nome_lotacao")); // valor
																							// nome_lotacao

				postoTrabalho = elemInsp.getAttribute("nome_postoTrabalho");
				objWrapper.setValue("nomeposto", elemInsp.getAttribute("nome_postoTrabalho")); // valor
																								// nome_postoTrabalho

				objWrapper.setValue("gestorcliente", elemInsp.getAttribute("gestor_cliente")); // valor
																								// gestor_cliente

				objWrapper.setValue("colaboradorentrevistado", elemInsp.getAttribute("nome_colaborador")); // valor
																											// nome_colaborador

				objWrapper.setValue("funccolaborador", elemInsp.getAttribute("funcao_colaborador")); // valor
																										// funcao_colaborador

				objWrapper.setValue("nomeinspetor", elemInsp.getAttribute("nome_inspetor")); // valor
																								// nome_inspetor

				// exemplo: aaaa mm dd
				StringTokenizer dataString = new StringTokenizer(elemInsp.getAttribute("data_inspesao"));
				String ano = dataString.nextToken();
				String mes = dataString.nextToken();
				String dia = dataString.nextToken();
				GregorianCalendar calendar = new GregorianCalendar(Integer.parseInt(ano), Integer.parseInt(mes), Integer.parseInt(dia));
				objWrapper.setValue("datainspecao", calendar); // valor
																// data_inspesao

				objWrapper.setValue("descpesquisa", elemInsp.getAttribute("descrisao_pesquisa")); // valor
																									// descrisao_pesquisa

				// valor numTotalResp

				InstantiableEntityInfo infoEform = (InstantiableEntityInfo) EntityRegister.getCacheInstance().getByType("Resposta");

				notConform = false;
				NodeList nodeListReply = elemInsp.getChildNodes();
				List<NeoObject> list = Collections.synchronizedList(new LinkedList<NeoObject>());
				for (int r = 0; r < nodeListReply.getLength(); r++)
				{
					Element elemReply = (Element) nodeListReply.item(r); // Resposta

					NeoObject findNewObj = infoEform.createNewInstance();
					EntityWrapper findObjWrapper = new EntityWrapper(findNewObj);

					QLGroupFilter groupF = new QLGroupFilter("and");

					findObjWrapper.setValue("descperg", elemReply.getAttribute("desc_perg")); // valor
																								// desc_perg
					groupF.addFilter(MobileUtils.createFilter("descperg", elemReply.getAttribute("desc_perg")));

					findObjWrapper.setValue("respperg", elemReply.getAttribute("resp_perg")); // valor
																								// resp_perg
					groupF.addFilter(MobileUtils.createFilter("respperg", elemReply.getAttribute("resp_perg")));

					Boolean logical = MobileUtils.isConform(elemReply.getAttribute("logical"));
					findObjWrapper.setValue("booleanValor", logical);
					groupF.addFilter(MobileUtils.createFilter("booleanValor", logical));

					Long code_perg = NeoUtils.safeIsNull(elemReply.getAttribute("code_perg")) ? null : new Long(elemReply.getAttribute("code_perg"));
					findObjWrapper.setValue("codeperg", code_perg); // valor
																	// code_perg
					groupF.addFilter(MobileUtils.createFilter("codeperg", code_perg));

					if (logical != null && !logical)
					{ // se detectou n�o conformidade
						if (!notConform)
							notConform = true;
					}

					PersistEngine.persist(findNewObj);
					list.add(findObjWrapper.getObject());
				}

				objWrapper.setValue("neoidResposta", list);
			}
		}

		// finaliza a task
		finishTask(isReinspection, notConform, postoTrabalho, codTask);

		return entity;
	}

	public void finishTask(boolean isReinspection, boolean notConform, String postoTrabalho, long codTask)
	{
		Activity activity = null;
		Task task = null;
		List<CasvigMobileEntityPool> mobilePool = (List<CasvigMobileEntityPool>) PersistEngine.getObjects(CasvigMobileEntityPool.class);
		for (CasvigMobileEntityPool pool : mobilePool)
		{
			if (pool.getTask().getNeoId() == codTask)
			{
				// salva a task e activity
				activity = pool.getActivity();
				task = pool.getTask();

				// remover identificador da reinspe��o
				PersistEngine.removeById(pool.getNeoId());
				break;
			}
		}

		if (task == null)
		{
			log.error("Task not found in CasvigMobilePool");
			return;
		}

		if (task != null)
		{
			// Instancia o E-Form IM_Inspetoria
			NeoObject inspetoriaObj = task.getProcess().getEntity();
			EntityWrapper inspetoriaWrapper = new EntityWrapper(inspetoriaObj);

			PersistEngine.persist(this.entity);
			inspetoriaWrapper.setValue("relatorioInspecao", this.entity);

			if (isReinspection)
			{
				inspetoriaWrapper.setValue("gerouNaoConformidade", new Boolean(notConform));
				PersistEngine.persist(inspetoriaObj);
				log.info("Persist new reinspection" + ((notConform) ? " -> no conformity " : " - OK") + " - " + postoTrabalho);
			}
			else
			{
				inspetoriaWrapper.setValue("gerouReincidencia", new Boolean(notConform));
				PersistEngine.persist(inspetoriaObj);
				log.info("Persist new reinspection" + ((notConform) ? " -> no efficacy " : " - OK") + " - " + postoTrabalho);
			}

		}
		else
		{
			if (isReinspection)
			{
				CasvigMobileServlet.log.error("Duplicate reinspection" + ((notConform) ? " -> no conformity " : " - OK") + " - " + postoTrabalho);
			}
			else
			{
				CasvigMobileServlet.log.error("Duplicate reinspection" + ((notConform) ? " -> no efficacy " : " - OK") + " - " + postoTrabalho);
			}
		}

		// continua workflow
		if (task != null && activity != null)
		{
			OrsegupsWorkflowHelper.finishTaskByActivity(activity);
		}
	}

	/**
	 * Reinspe��o n�o inicia workflow.
	 * 
	 * @return false.
	 */
	public boolean startProcess()
	{
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.neomind.fusion.custom.casvig.mobile.xml.entity.MobileXmlInterface
	 * #finishActivity()
	 */
	public boolean finishActivity()
	{
		try
		{
			return true;
		}
		catch (Exception e)
		{
			return false;
		}

	}
}
