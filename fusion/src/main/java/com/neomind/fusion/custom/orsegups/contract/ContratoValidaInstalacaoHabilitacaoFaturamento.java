package com.neomind.fusion.custom.orsegups.contract;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.adapter.CustomActivityAdapter;
import com.neomind.util.NeoUtils;

public class ContratoValidaInstalacaoHabilitacaoFaturamento implements AdapterInterface{
	
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity act)
	{
		/*
		 * so podera finalizar quando todos os postos estiverem faturados
		 */
		Long movimento = (Long) processEntity.findValue("movimentoContratoNovo.codTipo");
		Long alteracao = 0L;
		if(movimento == 5)
			alteracao = (Long) processEntity.findValue("movimentoAlteracaoContrato.codTipo");
		
		
		Collection<NeoObject> postos = processEntity.findField("postosContrato").getValues();
		int quantidadePostosAbrirOS = 0;
		for(NeoObject posto : postos)
		{
			EntityWrapper wPosto = new EntityWrapper(posto);
			
			boolean abrirOSInstalacao = NeoUtils.safeBoolean(wPosto.findValue("aberturaOSInstalacao"));
			boolean abrirOSReintalacao = NeoUtils.safeBoolean(wPosto.findValue("aberturaOSReinstalacao"));
			boolean abrirOSHabilitacao = NeoUtils.safeBoolean(wPosto.findValue("aberturaOSHabilitacao"));
			if(abrirOSInstalacao || abrirOSReintalacao || abrirOSHabilitacao)
			{
				quantidadePostosAbrirOS++;
			}
		}
		
		
		/*
		 * se for um fluxo de redução de equipamento, deve manter essa tarefa aberta
		 * para que o JOB possa abrir a devida OS
		 */
		if(quantidadePostosAbrirOS > 0 || alteracao == 4)
			((CustomActivityAdapter) act).setKeepOpen(true);
		
		
		
	}
	
	public void back(EntityWrapper w, Activity act)
	{
		
	}
	
	
}
