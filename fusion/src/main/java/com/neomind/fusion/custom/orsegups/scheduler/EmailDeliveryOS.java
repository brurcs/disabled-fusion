package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.HtmlEmail;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;

public class EmailDeliveryOS implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(EmailDeliveryOS.class);
	private Pattern pattern;
	private Matcher matcher;
	private final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	@SuppressWarnings("unchecked")
	@Override
	public void execute(CustomJobContext arg0)
	{
		int adicionados = 0;
		log.warn("E-Mail OS executado em: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		Long key = GregorianCalendar.getInstance().getTimeInMillis();

		Connection conn = null;
		StringBuilder strSigma = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		try
		{
			conn = PersistEngine.getConnection("SIGMA90");
			strSigma = new StringBuilder();

			pattern = Pattern.compile(EMAIL_PATTERN);

			InstantiableEntityInfo infoEnvioEmail = (InstantiableEntityInfo) EntityRegister.getEntityInfo("EmailDelivery");
			InstantiableEntityInfo infoHis = (InstantiableEntityInfo) EntityRegister.getEntityInfo("OSEmailAutomaticoExibicaoCliente");
			InstantiableEntityInfo infoTec = (InstantiableEntityInfo) EntityRegister.getEntityInfo("OSEmailAutomaticoTecnicos");
			InstantiableEntityInfo infoCadTec = (InstantiableEntityInfo) EntityRegister.getEntityInfo("OSEmaiCadastrolAutomaticoTecnicosCM");

			strSigma.append(" 	SELECT he.IDOSHISTORICO, he.ID_ORDEM, c.RESPONSAVEL, c.EMAILRESP, c.RAZAO, c.ENDERECO, bai.NOME AS NOMEBAIRRO, cid.NOME AS NOMECIDADE, col.CD_COLABORADOR, c.CD_CLIENTE, col.NM_COLABORADOR, d.DESCRICAODEFEITO ");
			strSigma.append(" 	FROM OSHISTORICO he ");
			strSigma.append(" 	INNER JOIN dbORDEM os ON os.ID_ORDEM = he.ID_ORDEM ");
			strSigma.append(" 	INNER JOIN dbo.OSDEFEITO d on d.IDOSDEFEITO = os.IDOSDEFEITO ");
			strSigma.append(" 	INNER JOIN COLABORADOR col ON col.CD_COLABORADOR = os.ID_INSTALADOR	");
			strSigma.append(" 	INNER JOIN dbCENTRAL c ON c.CD_CLIENTE = os.CD_CLIENTE ");
			strSigma.append(" 	INNER JOIN dbCIDADE cid ON c.ID_CIDADE = cid.ID_CIDADE ");
			strSigma.append(" 	INNER JOIN dbBAIRRO bai ON c.ID_CIDADE = bai.ID_CIDADE AND c.ID_BAIRRO = bai.ID_BAIRRO ");
			strSigma.append(" 	WHERE DATAINICIOEXECUCAO >= DATEADD(MI, -30, GetDate()) ");
			strSigma.append(" 	AND os.DATAEXECUTADA_MOB IS NULL ");
			strSigma.append(" 	AND c.TP_PESSOA in (0,1) ");
			strSigma.append("   AND col.nm_colaborador not like '%terc%' ");
			strSigma.append("   AND col.nm_colaborador not like 'cm%' ");
			strSigma.append("   AND c.id_empresa not in (10128,10129,10130,10131,10132,10133,10134,10135,10136,10137,10138,10139,10140) ");
			strSigma.append(" 	ORDER BY DATAINICIOEXECUCAO	");

			pstm = conn.prepareStatement(strSigma.toString());

			rs = pstm.executeQuery();

			while (rs.next())
			{
				QLGroupFilter queryGroup = new QLGroupFilter("AND");
				queryGroup.addFilter(new QLEqualsFilter("idOrdem", rs.getString("ID_ORDEM")));
				queryGroup.addFilter(new QLEqualsFilter("cdTecnico", rs.getString("CD_COLABORADOR")));

				NeoObject openOS = PersistEngine.getObject(infoHis.getEntityClass(), queryGroup);
				if (openOS == null)
				{

					List<NeoObject> listaEmails = PersistEngine.getObjects(infoTec.getEntityClass(), new QLEqualsFilter("codigo", rs.getString("CD_COLABORADOR")), -1, -1);

					if (listaEmails != null && !listaEmails.isEmpty())
					{
						EntityWrapper wpneo = new EntityWrapper(listaEmails.get(0));

						String idOrdem = rs.getString("ID_ORDEM");
						String responsavel = (rs.getString("RESPONSAVEL") == null ? "" : rs.getString("RESPONSAVEL"));
						String email = (rs.getString("EMAILRESP") == null ? "" : rs.getString("EMAILRESP"));
						String razao = (rs.getString("RAZAO") == null ? "" : rs.getString("RAZAO"));
						String endereco = (rs.getString("ENDERECO") == null ? "" : rs.getString("ENDERECO"));
						String cidade = (rs.getString("NOMECIDADE") == null ? "" : rs.getString("NOMECIDADE"));
						String bairro = (rs.getString("NOMEBAIRRO") == null ? "" : rs.getString("NOMEBAIRRO"));
						String cdCliente = rs.getString("CD_CLIENTE");
						String matriculaTecnico = (String) wpneo.findValue("matricula");
						String nomeTecnico = wpneo.findValue("nome").toString().toUpperCase();
						String idOsHistorico = rs.getString("IDOSHISTORICO");
						String cdTecnico = rs.getString("CD_COLABORADOR");
						String tipoOS = rs.getString("DESCRICAODEFEITO");

						//ESTA É MINHA ADAPTAÇÃO
						List<String> emailClie = this.validarEmail(email, cdCliente);

						if ((emailClie != null) && (!emailClie.isEmpty()))
						{

							StringBuilder noUserMsg = new StringBuilder();

							for (String emailFor : emailClie)
							{

								noUserMsg.append(" <html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></head><body>");
								noUserMsg.append("  <table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#ffffff\">");
								noUserMsg.append(" <tbody><tr>");
								noUserMsg.append(" <td valign=\"top\" align=\"left\" style=\"font-family:Arial;font-size:11px;font-weight:normal;text-align:left;\">");
								noUserMsg.append(" </td>");
								noUserMsg.append(" </tr>");
								noUserMsg.append(" </tbody></table>");
								noUserMsg.append("  <table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#ffffff\">");
								noUserMsg.append("           <tbody><tr>");
								noUserMsg.append("             <td valign=\"top\" align=\"left\" colspan=\"11\"><img style=\"display:block;\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif\" alt=\"\" width=\"1\" height=\"9\"></td>");
								noUserMsg.append("           </tr>");
								noUserMsg.append("           <tr>");
								noUserMsg.append("             <td align=\"left\" valign=\"middle\" >");
								noUserMsg.append("               <a href=\"http://www.orsegups.com.br\" target=\"_blank\">");
								noUserMsg.append("               <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/logoORS.jpg\" border=\"0\" alt=\"Orsegups Participações S.A.\"></a>");
								noUserMsg.append("             </td>");
								noUserMsg.append("             <td><img style=\"display:block;\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif\" height=\"1\" width=\"30\"></td>");
								noUserMsg.append("             <td valign=\"top\" align=\"left\" style=\"font-family:Arial; font-size:11px;font-weight:normal;\">");
								noUserMsg.append(" 			 <a href=\"http://www.orsegups.com.br/area-do-cliente/login?expirado\" target=\"_blank\" style=\"color:#0078d2; text-decoration:none;\">");
								noUserMsg.append(" 			Portal do Cliente</a>");
								noUserMsg.append(" 			<img style=\"DISPLAY: inline\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif\" alt=\"\" width=\"20\" height=\"1\">");
								noUserMsg.append(" 			<a href=\"http://www.orsegups.com.br/area-do-cliente/login?expirado?acao=Acessar\" target=\"_blank\" style=\"color:#0078d2; text-decoration:none;\">");
								noUserMsg.append(" 			Relatório de Eventos</a>");
								noUserMsg.append(" 			<img style=\"DISPLAY: inline\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif\" alt=\"\" width=\"20\" height=\"1\">");
								noUserMsg.append(" 			<a href=\"http://www.orsegups.com.br/orcamento.html\" target=\"_blank\" style=\"color:#0078d2; text-decoration:none;\">");
								noUserMsg.append(" 			Atualização Cadastral</a> ");
								noUserMsg.append(" 			<img style=\"DISPLAY: inline\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif\" alt=\"\" width=\"20\" height=\"1\">");
								noUserMsg.append(" 			</td>            ");
								noUserMsg.append("           </tr>");
								noUserMsg.append("           <tr>");
								noUserMsg.append("             <td valign=\"top\" align=\"left\" colspan=\"11\"><img style=\"display:block;\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif\" alt=\"\" width=\"1\" height=\"9\"></td>");
								noUserMsg.append("          </tr>");
								noUserMsg.append("      </tbody></table>");
								noUserMsg.append(" <!--- header table ends --->");
								noUserMsg.append(" <!--- main hero image with salutation begins--->");
								noUserMsg.append(" <table align=\"center\" width=\"640\" height=\"448\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
								noUserMsg.append(" 	<tbody><tr>");
								noUserMsg.append(" 		<td align=\"center\" colspan=\"3\" width=\"640\" height=\"78\">");
								noUserMsg.append(" 		<img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/tecnico_a_caminhoX.jpg\" border=\"0\" alt=\"AUTORIZAÇÃO DE ACESSO\">");
								noUserMsg.append(" 			</td>");
								noUserMsg.append(" 	</tr>");
								noUserMsg.append(" 	<tr>	");
								noUserMsg.append(" <td width=\"635\" height=\"226\" bgcolor=\"#FFFFFF\" align=\"center\" valign=\"top\">");
								noUserMsg.append(" <!--- main content table begins--->");
								noUserMsg.append(" <table width=\"635\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\">");
								noUserMsg.append(" <tbody><tr><td colspan=\"3\"><img width=\"1\" height=\"15\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif\" alt=\"\"></td></tr>");
								noUserMsg.append(" <tr><td width=\"10\"></td>");
								noUserMsg.append(" <td align=\"left\" style=\"FONT-WEIGHT: regular; FONT-SIZE: 18px; COLOR: #0298d5; FONT-FAMILY: Helvetica, Arial, sans-serif;\">");
								noUserMsg.append(" <font style=\"FONT-WEIGHT: regular; FONT-SIZE: 18px; COLOR: #0298d5; FONT-FAMILY: Helvetica, Arial, sans-serif;\">Prezado Cliente,</font>");
								noUserMsg.append(" <br>");
								noUserMsg.append(" </td><td width=\"10\"></td>");
								noUserMsg.append(" </tr>");
								noUserMsg.append(" <tr><td width=\"10\"></td>");
								noUserMsg.append(" <td align=\"left\">");
								noUserMsg.append("  <img width=\"10\" height=\"22\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif\" alt=\"\"></td><td width=\"10\"></td>");
								noUserMsg.append(" </tr>");
								noUserMsg.append(" <tr><td width=\"10\"><img width=\"10\" height=\"15\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif\" alt=\"\"></td>");
								noUserMsg.append(" <td style=\"FONT-WEIGHT: regular; FONT-SIZE: 18px; FONT-FAMILY: Helvetica, Arial, sans-serif;\">");
								noUserMsg.append(" Para sua maior segurança, a Orsegups apresenta abaixo o <span style=\"font-weight:bold\">técnico autorizado</span> para o atendimento a Ordem de Serviço número <span style=\"font-weight:bold\">" + idOrdem + "</span> em seu sistema de segurança.<br>");
								noUserMsg.append(" <br>");
								noUserMsg.append(" <div align=\"center\">");
								noUserMsg.append(" <img src=\"http://intranet.orsegups.com.br/fusion/file/download/" + ((NeoFile) wpneo.getValue("fotoTec")).getNeoId() + "\" border=\"0\" alt=\"Técnico Responsável\"></div>");
								noUserMsg.append(" <div align=\"center\" style=\"font-size:14px;;line-height:18px; color:#0298d5; font-weight:bold; font-family:Helvetica, Arial, sans-serif\">" + nomeTecnico + "</div> ");
								noUserMsg.append(" <div align=\"center\" style=\"FONT-WEIGHT: normal; FONT-SIZE: 10px; COLOR: #999999; font-family:Arial;valign:top;\">Matrícula: " + matriculaTecnico + "&nbsp;*</div><br>");
								noUserMsg.append(" <br>");
								noUserMsg.append(" <div align=\"left\" style=\"FONT-WEIGHT: normal; FONT-SIZE: 10px; COLOR: #999999; font-family:Arial;valign:top;\">*&nbsp;Para sua segurança, solicite ao nosso técnico o crachá de identificação.</div>");
								noUserMsg.append(" <br>");
								noUserMsg.append(" O atendimento refere-se ao seu sistema de segurança instalado no endereço abaixo:<br><br>");
								noUserMsg.append(" <strong>Nome/Razão Social: </strong> " + razao + "<br>");
								noUserMsg.append(" <strong>Endereço: </strong>" + endereco + "<br>");
								noUserMsg.append(" <strong>Bairro: </strong>" + bairro + "<br>");
								noUserMsg.append(" <strong>Cidade: </strong>" + cidade);
								noUserMsg.append(" <br>");
								noUserMsg.append(" <strong>Tipo de OS: </strong>" + tipoOS);
								noUserMsg.append(" </td><td width=\"10\"><img width=\"10\" height=\"15\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif\" alt=\"\"></td>");
								noUserMsg.append(" </tr>");
								noUserMsg.append(" </tbody></table>");
								noUserMsg.append(" <!--- main content table ends--->");
								noUserMsg.append(" </td>		");
								noUserMsg.append(" 	</tr>      ");
								noUserMsg.append("     <tr>");
								noUserMsg.append(" <td colspan=\"3\" valign=\"top\" align=\"left\"><img width=\"10\" height=\"22\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif\" alt=\"\">");
								noUserMsg.append(" </td>");
								noUserMsg.append(" </tr>");
								noUserMsg.append("  <tr>");
								noUserMsg.append(" <td colspan=\"4\" valign=\"top\" align=\"left\" style=\"padding-top:10px;\">");
								noUserMsg.append(" <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/line.jpg\" height=\"6\" border=\"0\" width=\"640\"></td>");
								noUserMsg.append(" </tr>");
								noUserMsg.append(" </tbody></table>	");
								noUserMsg.append(" <!--- main hero image with salutation ends--->");
								noUserMsg.append(" <!--- main body begins--->");
								noUserMsg.append(" <!--- main body ends --->");
								noUserMsg.append(" <br>");
								noUserMsg.append(" <!---cross promo icons begin--->");
								noUserMsg.append(" <table width=\"640\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">");
								noUserMsg.append(" <tbody><tr>");
								noUserMsg.append(" <td width=\"5\"><img width=\"9\" height=\"22\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif\" alt=\"\"></td>");
								noUserMsg.append("                             <td valign=\"top\" align=\"right\">");
								noUserMsg.append("                             <a href=\"http://www.facebook.com/orsegups\" target=\"_blank\">");
								noUserMsg.append("                             <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/logo_facebook.jpg\" width=\"22\" height=\"18\" border=\"0\" alt=\"Facebook\"></a>");
								noUserMsg.append("                            <a href=\"http://www.youtube.com/user/orsegupssc\" target=\"_blank\">");
								noUserMsg.append("                             <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/logo_youtube.jpg\" width=\"22\" height=\"18\" border=\"0\" alt=\"YouTube\"></a>");
								noUserMsg.append("                             <a href=\"http://www.twitter.com/orsegups\" target=\"_blank\">");
								noUserMsg.append("                             <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/logo_twitter.jpg\" width=\"22\" height=\"18\" border=\"0\" alt=\"Twitter\"></a>");
								noUserMsg.append("                           </td>");
								noUserMsg.append("                         </tr>");
								noUserMsg.append(" </tbody></table>");
								noUserMsg.append(" <!---cross promo icons end--->");
								noUserMsg.append(" <!--- bottom links begin--->");
								noUserMsg.append(" <br>");
								noUserMsg.append(" <table width=\"640\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">");
								noUserMsg.append(" <tbody><tr>");
								noUserMsg.append(" <td width=\"5\"><img width=\"9\" height=\"22\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif\" alt=\"\"></td>");
								noUserMsg.append(" <td valign=\"top\" align=\"left\" style=\"font-family:Arial; font-size:11px;font-weight:normal;\">");
								noUserMsg.append("  <a href=\"http://www.orsegups.com.br/area-do-cliente/login?expirado\" target=\"_blank\" style=\"color:#0078d2; text-decoration:none;\">");
								noUserMsg.append(" Portal do Cliente</a>");
								noUserMsg.append(" <img style=\"DISPLAY: inline\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif\" alt=\"\" width=\"20\" height=\"1\">");
								noUserMsg.append(" <a href=\"http://www.orsegups.com.br/area-do-cliente/login?expirado\" target=\"_blank\" style=\"color:#0078d2; text-decoration:none;\">");
								noUserMsg.append(" Relatório de Eventos</a>");
								noUserMsg.append(" <img style=\"DISPLAY: inline\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif\" alt=\"\" width=\"20\" height=\"1\">");
								noUserMsg.append(" <a href=\"http://www.orsegups.com.br/orcamento.html\" target=\"_blank\" style=\"color:#0078d2; text-decoration:none;\">");
								noUserMsg.append(" Atualização Cadastral</a> ");
								noUserMsg.append(" <img style=\"DISPLAY: inline\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif\" alt=\"\" width=\"20\" height=\"1\">");
								noUserMsg.append(" </td>");
								noUserMsg.append(" </tr>");
                            								
								noUserMsg.append(" <tr>");
								noUserMsg.append(" <td valign=\"top\" align=\"left\" colspan=\"2\">");
								noUserMsg.append(" <img style=\"display:inline;\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/line.jpg\" height=\"6\" border=\"0\">");
								noUserMsg.append(" </td>");
								noUserMsg.append(" </tr>   ");
								noUserMsg.append(" </tbody></table>");
								noUserMsg.append(" <!--- bottom links end--->");
								noUserMsg.append(" <!---legal begins --->");
								noUserMsg.append(" <table width=\"640\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" bgcolor=\"#ffffff\" align=\"center\" style=\"font-family:Arial\">");
								noUserMsg.append(" <tbody><tr>");
								noUserMsg.append(" <td width=\"5\">");
								noUserMsg.append(" <img width=\"9\" height=\"22\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif\" alt=\"\">");
								noUserMsg.append(" </td>");
								noUserMsg.append(" <td valign=\"top\" style=\"FONT-WEIGHT: normal; FONT-SIZE: 10px; COLOR: #999999; font-family:Arial;valign:top;\">");
								noUserMsg.append(" Este e-mail foi enviado a <span style=\"color:#0078d2; text-decoration:none;\">" + email + "</span> pois você está cadastrado em nosso sistema operacional.<br><br>");
								noUserMsg
										.append(" Este e-mail foi enviado em nome da Orsegups Participações. Teremos satisfação em ajudá-lo com quaisquer dúvidas ou preocupações. Para qualquer esclarecimento desejado, visite <a href=\"http://www.orsegups.com.br/orcamento.html\" style=\"color:#0078d2; text-decoration:none;\" target=\"_blank\">www.orsegups.com.br</a>, no link contato ou envie e-mail para <a href=\"mailto:sac@orsegups.com.br\" style=\"color:#0078d2; text-decoration:none;\" target=\"_blank\">sac@orsegups.com.br</a>.<br><br>");
								noUserMsg.append(" © 2013 Orsegups Participações S.A. Todos os direitos reservados.<br>");
								noUserMsg.append(" <br>");
								noUserMsg
										.append(" Este email e todas as informações ou arquivos contidos nele são exclusivamente para uso confidencial do destinatário. Esta mensagem contém informações confidenciais e pertencentes à Orsegups (como dados corporativos, dos clientes e dos funcionários da Orsegups) que não podem ser lidas, pesquisadas, distribuídas ou utilizadas de outra forma por qualquer outro indivíduo que não seja o destinatário pretendido. Caso tenha recebido este email por engano, por favor, notifique o remetente ");
								noUserMsg.append(" e exclua esta mensagem e seus anexos imediatamente.");
								noUserMsg.append(" </td>");
								noUserMsg.append(" <td width=\"5\">");
								noUserMsg.append(" <img width=\"9\" height=\"22\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif\" alt=\"\">");
								noUserMsg.append(" </td>");
								noUserMsg.append(" </tr>");
								noUserMsg.append(" </tbody></table>");
								noUserMsg.append(" <!--- legal ends --->");
								noUserMsg.append(" <img height=\"1\" width=\"1\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/52\"><img height=\"1\" width=\"1\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/52\">");
								noUserMsg.append(" </body></html>");

								NeoObject emailHis = infoHis.createNewInstance();
								EntityWrapper emailHisWp = new EntityWrapper(emailHis);
								adicionados = adicionados + 1;
								emailHisWp.findField("idOrdem").setValue(idOrdem);
								emailHisWp.findField("responsavel").setValue(responsavel);
								emailHisWp.findField("email").setValue(emailClie.toString());
								emailHisWp.findField("razao").setValue(razao);
								emailHisWp.findField("endereco").setValue(endereco);
								emailHisWp.findField("cidade").setValue(cidade);
								emailHisWp.findField("bairro").setValue(bairro);
								emailHisWp.findField("cdCliente").setValue(cdCliente);
								emailHisWp.findField("enviadoPara").setValue(emailClie.toString());
								emailHisWp.findField("idHistorico").setValue(idOsHistorico);
								emailHisWp.findField("cdTecnico").setValue(cdTecnico);

								PersistEngine.persist(emailHis);
								
								GregorianCalendar dataCad = (GregorianCalendar) GregorianCalendar.getInstance();
								NeoObject emaiEnvio = infoEnvioEmail.createNewInstance();
								EntityWrapper emailEnvioWp = new EntityWrapper(emaiEnvio);
								emailEnvioWp.findField("de").setValue("os@orsegups.com.br");
								emailEnvioWp.findField("para").setValue(emailFor + ";emailautomatico@orsegups.com.br");
								emailEnvioWp.findField("assunto").setValue("Autorização de Acesso - Ordem de Serviço: " + idOrdem);
								emailEnvioWp.findField("Mensagem").setValue(noUserMsg.toString());
								emailEnvioWp.findField("datCad").setValue(dataCad);
								PersistEngine.persist(emaiEnvio);
							}

						}

					}
					else
					{
						//envio email cadastro tecnico

						List<NeoObject> listaEmailsEnviadosCM = PersistEngine.getObjects(infoCadTec.getEntityClass(), new QLEqualsFilter("cdTecnico", rs.getString("CD_COLABORADOR")), -1, -1);

						if (listaEmailsEnviadosCM.isEmpty())
						{
							MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

							MailSettings mailClone = new MailSettings();
							mailClone.setMinutesInterval(settings.getMinutesInterval());
							mailClone.setFromEMail(settings.getFromEMail());
							mailClone.setFromName(settings.getFromName());
							mailClone.setRenderServer(settings.getRenderServer());
							mailClone.setSecurityFromEmail(settings.getSecurityFromEmail());
							mailClone.setSecurityCopyEmail(settings.getSecurityCopyEmail());
							mailClone.setSmtpSettings(settings.getSmtpSettings());
							mailClone.setPort(settings.getPort());
							mailClone.setSmtpServer(settings.getSmtpServer());
							mailClone.setEnabled(settings.isEnabled());

							mailClone.setFromEMail("os@orsegups.com.br");

							mailClone.setFromName("Orsegups Partipações S.A.");
							if (mailClone != null && mailClone.isEnabled())
							{

								HtmlEmail noUserEmail = new HtmlEmail();
								StringBuilder noUserMsg = new StringBuilder();

								Calendar saudacao = Calendar.getInstance();
								String saudacaoEMail = "";
								if (saudacao.get(Calendar.HOUR_OF_DAY) >= 6 && saudacao.get(Calendar.HOUR_OF_DAY) <= 12)
								{
									saudacaoEMail = "Bom dia, ";
								}
								else if (saudacao.get(Calendar.HOUR_OF_DAY) > 12 && saudacao.get(Calendar.HOUR_OF_DAY) <= 18)
								{
									saudacaoEMail = "Boa tarde, ";
								}
								else
								{
									saudacaoEMail = "Boa noite, ";
								}

								noUserEmail.setCharset("UTF-8");
								noUserEmail.addTo("emailautomatico@orsegups.com.br");
								noUserEmail.addTo("cm@orsegups.com.br");

								noUserEmail.setFrom("os@orsegups.com.br");
								noUserEmail.setSubject("Não Responda - Cadastro de Técnico " + rs.getString("NM_COLABORADOR"));

								noUserMsg.append("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></head><body>");
								noUserMsg.append("<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#ffffff\">");
								noUserMsg.append("          <tbody>");
								noUserMsg.append("		    <td align=\"left\" valign=\"top\" >");
								noUserMsg.append("               <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/logoORS.jpg\" >");
								noUserMsg.append("            </td>");
								noUserMsg.append("			<td>");
								noUserMsg.append("			<strong>" + saudacaoEMail + " favor providenciar o cadastro do técnico para execução de ordens de serviço.</strong><br>");
								noUserMsg.append("			<strong>Código: </strong> " + rs.getString("CD_COLABORADOR") + " <br>");
								noUserMsg.append("			<strong>Nome: </strong>" + rs.getString("NM_COLABORADOR") + " <br>");
								noUserMsg.append("			</td>");
								noUserMsg.append("     </tbody></table>");
								noUserMsg.append("</body></html>");
								noUserEmail.setHtmlMsg(noUserMsg.toString());
								noUserEmail.setContent(noUserMsg.toString(), "text/html");
								mailClone.applyConfig(noUserEmail);

								noUserEmail.send();

								NeoObject emailCadTec = infoCadTec.createNewInstance();
								EntityWrapper emailHisWp = new EntityWrapper(emailCadTec);
								emailHisWp.findField("cdTecnico").setValue(rs.getString("CD_COLABORADOR"));
								PersistEngine.persist(emailCadTec);

							}
						}
					}

				}
			}
		}

		catch (Exception e)
		{
			log.error("E-Mail OS erro no processamento:" + e.getMessage());
			System.out.println("[" + key + "] E-Mail OS erro no processamento: " + e.getMessage());
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e2)
			{
				log.error("E-Mail OS erro fechamento de recursos cadastro técnico:" + e2.getMessage());
			}

		}

	}

	public List<String> validarEmail(String emails, String cdCliente)
	{

		List<String> listaDeEmail = new ArrayList<String>();

		emails = emails.replaceAll(",", ";");

		String emailsSigma[] = emails.split(";");

		Connection conn = PersistEngine.getConnection("SIGMA90");
		PreparedStatement pstm = null;
		ResultSet rsSapiens = null;
		ResultSet rs = null;
		try {

		    for (String mail : emailsSigma) {
			matcher = pattern.matcher(mail);

			if (matcher.matches()) {
			    if (!listaDeEmail.contains(mail.toLowerCase().trim())) {
				listaDeEmail.add(mail.toLowerCase().trim());
			    }
			}
		    }

		    String sqlProv = " SELECT email FROM DBPROVIDENCIA WITH(NOLOCK) WHERE cd_cliente = " + Long.valueOf(cdCliente) + " AND  email IS NOT NULL AND email <> ' ' ";

		    pstm = conn.prepareStatement(sqlProv);

		    rs = pstm.executeQuery();

		    while (rs.next()) {

			String emailRs = rs.getString("email");

			if (emailRs != null && !emailRs.isEmpty()) {
			    emailRs = emailRs.replaceAll(",", ";");

			    String emailsProv[] = emailRs.split(";");

			    for (String mail : emailsProv) {
				matcher = pattern.matcher(emailRs);

				if (matcher.matches()) {
				    if (!listaDeEmail.contains(mail.toLowerCase().trim())) {
					listaDeEmail.add(mail.toLowerCase().trim());
				    }
				}
			    }
			}
		    }

		    conn = PersistEngine.getConnection("SAPIENS");

		    if (pstm != null) {
			pstm.close();
		    }

		    String sqlSap = ""
		    + " SELECT DISTINCT cli.IntNet, cli.emanfe, ctr.usu_emactr "
		    + " FROM USU_T160SIG sig WITH(NOLOCK) "
		    + " INNER JOIN USU_T160CTR ctr WITH(NOLOCK) ON  ctr.usu_codemp = sig.usu_codemp and ctr.usu_codfil = sig.usu_codfil and ctr.usu_numctr = sig.usu_numctr "
		    + " inner join dbo.E085CLI cli WITH(NOLOCK) ON CLI.CodCli = CTR.usu_codcli "
		    + " WHERE sig.usu_codcli = " + cdCliente;

		    pstm = conn.prepareStatement(sqlSap);
		    rsSapiens = pstm.executeQuery();

		    while (rsSapiens.next()) {

			String intNet = rsSapiens.getString("IntNet");
			String emanfe = rsSapiens.getString("emanfe");
			String usu_emactr = rsSapiens.getString("usu_emactr");

			if (intNet != null && !intNet.isEmpty()) {
			    intNet = intNet.replaceAll(",", ";");
			    String rsEmails[] = intNet.split(";");

			    for (String mail : rsEmails) {
				matcher = pattern.matcher(mail);
				if (matcher.matches()) {
				    if (!listaDeEmail.contains(mail.toLowerCase().trim())) {
					listaDeEmail.add(mail.toLowerCase().trim());
				    }
				}
			    }
			}

			if (emanfe != null && !emanfe.isEmpty()) {
			    emanfe = emanfe.replaceAll(",", ";");
			    String rsEmails[] = emanfe.split(";");

			    for (String mail : rsEmails) {
				matcher = pattern.matcher(mail);
				if (matcher.matches()) {
				    if (!listaDeEmail.contains(mail.toLowerCase().trim())) {
					listaDeEmail.add(mail.toLowerCase().trim());
				    }
				}
			    }
			}

			if (usu_emactr != null && !usu_emactr.isEmpty()) {
			    usu_emactr = usu_emactr.replaceAll(",", ";");
			    String rsEmails[] = usu_emactr.split(";");

			    for (String mail : rsEmails) {
				matcher = pattern.matcher(mail);
				if (matcher.matches()) {
				    if (!listaDeEmail.contains(mail.toLowerCase().trim())) {
					listaDeEmail.add(mail.toLowerCase().trim());
				    }
				}
			    }
			}
		    }
		} catch (Exception e) {
		    e.printStackTrace();
		    log.error("E-Mail OS erro validação do e-mail: " + e.getMessage());
		} finally {
		    OrsegupsUtils.closeConnection(null, null, rs);
		    OrsegupsUtils.closeConnection(conn, pstm, rsSapiens);
		}
		return listaDeEmail;

	    }

	@SuppressWarnings("unused")
	private void enviaEmailRotinaTeste(String returnFromAccess, String idOrdem)
	{

		try
		{

			MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

			MailSettings mailClone = new MailSettings();
			mailClone.setMinutesInterval(settings.getMinutesInterval());
			mailClone.setFromEMail(settings.getFromEMail());
			mailClone.setFromName(settings.getFromName());
			mailClone.setRenderServer(settings.getRenderServer());
			mailClone.setSecurityFromEmail(settings.getSecurityFromEmail());
			mailClone.setSecurityCopyEmail(settings.getSecurityCopyEmail());
			mailClone.setSmtpSettings(settings.getSmtpSettings());
			mailClone.setPort(settings.getPort());
			mailClone.setSmtpServer(settings.getSmtpServer());
			mailClone.setEnabled(settings.isEnabled());

			mailClone.setFromEMail("fusion@orsegups.com.br");
			mailClone.setFromName("Orsegups Partipações S.A.");
			if (mailClone != null)
			{

				HtmlEmail noUserEmail = new HtmlEmail();

				noUserEmail.setCharset("UTF-8");
				//noUserEmail.addTo("william.bastos@orsegups.com.br");
				//noUserEmail.addTo("mateus.batista@orsegups.com.br");

				noUserEmail.setSubject("Atendimento concluído: " + idOrdem);

				noUserEmail.setHtmlMsg(returnFromAccess);
				noUserEmail.setContent(returnFromAccess, "text/html");
				mailClone.applyConfig(noUserEmail);

				noUserEmail.send();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO RotinaNovosClientesOn Demand - Data: " + e.getMessage().toString() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		}

	}

}