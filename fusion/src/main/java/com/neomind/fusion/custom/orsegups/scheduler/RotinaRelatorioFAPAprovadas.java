package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;

/**
 * Rotina criada para gerar semanalmente uma planilha contendo as FAPs aprovadas na semana passada.
 * Somente FAPs de Aplicação 2 e 5 - Manutenção e Instalação de Alarme e CFTV - Mercado Privado
 * @author herisson.ferreira
 *
 */
public class RotinaRelatorioFAPAprovadas implements CustomJobAdapter {
	private static final Log log = LogFactory.getLog(AbreTarefaRelatorioCancelamentoFerias.class);
	
	@Override
	public void execute(CustomJobContext ctx) {
		
		// Log
		
		log.error("##### INICIO AGENDADOR DE TAREFA: Rotina FAPS APROVADAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		System.out.println("##### INICIO AGENDADOR DE TAREFA: Rotina FAPS APROVADAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy")); 
		
		// Conexão
		
		StringBuilder sql = new StringBuilder();
		Connection conn = PersistEngine.getConnection("");
		ResultSet rs = null;
		PreparedStatement pstm = null;
		
		// Datas
		
		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 1L);
		prazo.set(Calendar.HOUR, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);
		
		GregorianCalendar dataBase = new GregorianCalendar();
		dataBase.add(GregorianCalendar.WEEK_OF_MONTH, -1);
		dataBase.set(GregorianCalendar.DAY_OF_WEEK, 2);
		dataBase.set(GregorianCalendar.HOUR_OF_DAY, 00);
		dataBase.set(GregorianCalendar.MINUTE, 00);
		dataBase.set(GregorianCalendar.SECOND, 00);
		dataBase.set(GregorianCalendar.MILLISECOND, 00);

		String dataInicio = NeoDateUtils.safeDateFormat(dataBase, "yyyy-MM-dd HH:mm:ss");
		String dataInicioAux = NeoDateUtils.safeDateFormat(dataBase, "dd/MM/yyyy");

		dataBase.add(GregorianCalendar.DAY_OF_MONTH, 6);
		dataBase.set(GregorianCalendar.HOUR_OF_DAY, 23);
		dataBase.set(GregorianCalendar.MINUTE, 59);
		dataBase.set(GregorianCalendar.SECOND, 59);
		dataBase.set(GregorianCalendar.MILLISECOND, 59);

		String dataFim = NeoDateUtils.safeDateFormat(dataBase, "yyyy-MM-dd HH:mm:ss");
		String dataFimAux = NeoDateUtils.safeDateFormat(dataBase, "dd/MM/yyyy");
		
		// Dados da Tarefa
			
		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();

		String papelSolicitante = null;
		String papelExecutor = null;
		String usuarioSolicitante = null;
		String usuarioExecutor = null;

		String titulo = null;
		String descricao = null;
		
		papelSolicitante = "SolicitanteRotinaFAPsAprovadas";
		papelExecutor = "ExecutorRotinaFAPsAprovadas";

		usuarioSolicitante = OrsegupsUtils.getUserNeoPaper(papelSolicitante);
		usuarioExecutor = OrsegupsUtils.getUserNeoPaper(papelExecutor);

		titulo = "Relatório de FAPs Aprovadas";
		descricao = "Relatório das FAPs Aprovadas do dia "+dataInicioAux+" ao "+dataFimAux+"."; 
		
		try {
			
			sql.append(" SELECT WFP.CODE AS 'Número da FAP', FAP.dataAprovacao AS 'Data da Aprovação' FROM D_FAPAutorizacaoDePagamento FAP ");
			sql.append(" INNER JOIN D_FAPAplicacaoDoPagamento FAPPGT ");
			sql.append(" ON FAP.aplicacaoPagamento_neoId = FAPPGT.neoId ");
			sql.append(" INNER JOIN WFProcess WFP ");
			sql.append(" ON FAP.wfprocess_neoId = WFP.neoId ");
			sql.append(" WHERE WFP.processState = 1 ");
			sql.append(" AND FAPPGT.codigo IN (2,5)  ");
			sql.append(" AND FAP.DATAAPROVACAO BETWEEN ? AND ? ");
			
			pstm = conn.prepareStatement(sql.toString());
			
			pstm.setString(1, dataInicio);
			pstm.setString(2, dataFim);
			
			rs = pstm.executeQuery();
			
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("FAPs");
			int rownum = 0;
			Cell cell;
			Row row;
			
			Map<Integer, String> mapa = new HashMap<>();

			// Definição das Colunas (Planilha)
			
			row = sheet.createRow(rownum++);
			row.createCell(0).setCellValue("Número da FAP");
			row.createCell(1).setCellValue("Data da Aprovação");

			while(rs.next()){

				mapa.put(0, rs.getString("Número da FAP"));
				mapa.put(1, rs.getString("Data da Aprovação"));
				
				row = sheet.createRow(rownum++);
				
				for(Integer key: mapa.keySet()){
					cell = row.createCell(key);
					cell.setCellValue(mapa.get(key));
				}
				
			}
			
			File file = File.createTempFile("FAPSAPROVADAS_",".xls", null);
			FileOutputStream out = new FileOutputStream(file);
			workbook.write(out);
			out.close();

			NeoFile anexo = OrsegupsUtils.criaNeoFile(file);
			
			iniciarTarefaSimples.abrirTarefa(usuarioSolicitante, usuarioExecutor, titulo, descricao, "1", "sim", prazo, anexo);
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error("##### ERRO AGENDADOR DE TAREFA: Rotina FAPS APROVADAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			System.out.println("##### ERRO AGENDADOR DE TAREFA: Rotina FAPS APROVADAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
			log.error("##### FIM AGENDADOR DE TAREFA: Rotina FAPS APROVADAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			System.out.println("##### FIM AGENDADOR DE TAREFA: Rotina FAPS APROVADAS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		
		
	}

}
