package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.common.base.Joiner;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class RotinaAbreTarefaSimplesEventoXXX8Excessivo implements CustomJobAdapter {
    private static final Log log = LogFactory.getLog(RotinaAbreTarefaSimplesEventoXXX8Excessivo.class);
    private Long key = GregorianCalendar.getInstance().getTimeInMillis();

    @Override
    public void execute(CustomJobContext arg0) {
	Long key = GregorianCalendar.getInstance().getTimeInMillis();
	List<String> regionais = new ArrayList<String>();
	regionais = OrsegupsUtils.listaRegionaisSigla();

	/* Removido Regional Jaraguá do Sul (JGS) Ref tarefa 738353 */

	regionais.remove("JGS");
	regionais.remove("BNU");

	/**
	 * Adicionado lista de exeções de contas
	 * 
	 * @author mateus.batista
	 */
	
	String listaClientes = this.getListaExcecoes();

	int maxC = 0;
	while ((listaClientes == null || listaClientes.isEmpty()) && maxC < 2) {
	    listaClientes = this.getListaExcecoes();
	    maxC++;
	}

	for (int i = 0; i < regionais.size(); i++) {
	    String reg = regionais.get(i);
	    Connection conn = null;
	    StringBuilder sqlEventoExcessivo = new StringBuilder();
	    PreparedStatement pstm = null;
	    ResultSet rs = null;
	    log.warn("##### INICIAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Eventos XXX8 Excessivos - Regional: " + reg + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

	    try {
		conn = PersistEngine.getConnection("SIGMA90");

		sqlEventoExcessivo.append("  SELECT TOP 10 h.CD_CLIENTE, SUBSTRING(r.NM_ROTA, 1, 3) AS sigla_regional, c.ID_CENTRAL, c.PARTICAO, c.FANTASIA,  ");
		sqlEventoExcessivo.append("  c.RAZAO, COUNT(*) AS QtdDsl, GETDATE()-15 as PrimeiraData,GETDATE() as UltimaData,a.USU_REGCTR  ");
		sqlEventoExcessivo.append("  FROM VIEW_HISTORICO h WITH (NOLOCK)   ");
		sqlEventoExcessivo.append("  INNER JOIN dbCENTRAL c  ");
		sqlEventoExcessivo.append("  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE   ");
		sqlEventoExcessivo.append("  INNER JOIN ROTA r  ");
		sqlEventoExcessivo.append("  WITH (NOLOCK) ON  r.CD_ROTA = c.ID_ROTA  ");
		sqlEventoExcessivo.append("  INNER JOIN (SELECT distinct ctr.USU_REGCTR,sig.usu_codcli FROM [FSOODB04\\SQL02].SAPIENS.dbo.USU_T160SIG sig  ");
		sqlEventoExcessivo.append("  INNER JOIN [FSOODB04\\SQL02].SAPIENS.dbo.USU_T160CTR ctr WITH(NOLOCK)  ");
		sqlEventoExcessivo.append("  ON  ctr.usu_codemp = sig.usu_codemp and ctr.usu_codfil = sig.usu_codfil and ctr.usu_numctr = sig.usu_numctr  ");
		sqlEventoExcessivo.append("  WHERE  sig.usu_numctr <> 1   ) a on a.usu_codcli = c.CD_CLIENTE  ");
		sqlEventoExcessivo.append("  WHERE h.DT_RECEBIDO BETWEEN GETDATE()-15 AND GETDATE() AND c.CD_CLIENTE NOT IN("+listaClientes+")  ");
		sqlEventoExcessivo.append("  AND h.CD_EVENTO = 'XXX8'   ");
		sqlEventoExcessivo.append("  AND h.CD_USUARIO_FECHAMENTO <> 9999   ");
		sqlEventoExcessivo.append("  AND c.ID_CENTRAL IN ('AAAA','AAA1','AAA2','AAA3','AAA4')  ");
		sqlEventoExcessivo.append("  AND c.CD_CLIENTE NOT IN (   ");
		sqlEventoExcessivo.append("  SELECT sig.usu_codcli FROM [FSOODB04\\SQL02].SAPIENS.dbo.USU_T160SIG sig  ");
		sqlEventoExcessivo.append("  WHERE sig.usu_codcli = c.CD_CLIENTE AND sig.usu_numctr = 1   ");
		sqlEventoExcessivo.append("  )  ");
		sqlEventoExcessivo.append("  AND SUBSTRING(r.NM_ROTA, 1, 3) = ?   ");
		sqlEventoExcessivo.append("  GROUP BY h.CD_CLIENTE, SUBSTRING(r.NM_ROTA, 1, 3), c.ID_CENTRAL, c.PARTICAO, c.FANTASIA, c.RAZAO ,a.USU_REGCTR ");
		sqlEventoExcessivo.append("  HAVING COUNT(*) >= 10   ");
		sqlEventoExcessivo.append("  ORDER BY 7 DESC  ");

		pstm = conn.prepareStatement(sqlEventoExcessivo.toString());

		pstm.setString(1, reg);

		rs = pstm.executeQuery();

		String executor = "";

		while (rs.next()) {

		    String solicitante = OrsegupsUtils.getUserNeoPaper("G001SolicitanteEventoXXX8Excessivo");
		    String tituloAux = "Tratar Eventos XXX8 em Excesso - " + reg + " - " + rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "]%";
		    String titulo = "Tratar Eventos XXX8 em Excesso - " + reg + " - " + rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "] "+rs.getString("RAZAO");
		    GregorianCalendar prazo = new GregorianCalendar();
		    prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 5L);
		    prazo.set(Calendar.HOUR_OF_DAY, 23);
		    prazo.set(Calendar.MINUTE, 59);
		    prazo.set(Calendar.SECOND, 59);

		    NeoPaper papel = new NeoPaper();

		    Long codReg = OrsegupsUtils.getCodigoRegional(rs.getString("sigla_regional"));
		    int codRegCtr = rs.getInt("USU_REGCTR");
		    // solicitação do Srº Ernesto via tarefa simples 716894

		    if (codReg != null && codReg == 6) {
			papel = OrsegupsUtils.getPapelGerenteRegional(codReg);
		    } else if (codReg != null) {
			if (codReg == 2){// Solicitado via tarefa simples 1145677 
			    papel = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "GerenteERIAISeguranca"));
			}else{
			    if (reg != null && !reg.isEmpty() && reg.equals("SOO")) {
				switch (codRegCtr) {
				case 1:
				    codReg = 1L;
				    break;
				    
				case 9:
				    codReg = 9L;
				    break;
				}
			    }
			    papel = OrsegupsUtils.getPapelCoordenadorRegional(codReg);
			}
		    }
		    if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty()) {
			for (NeoUser user : papel.getUsers()) {
			    executor = user.getCode();
			    break;
			}

			if (executor != null && !executor.isEmpty()) {
			    String descricao = OrsegupsUtils.getHistoricoTarefaSimples(tituloAux);
			    descricao += " Conta : " + rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "] " + " <br>";
			    descricao = descricao + " Razão Social : " + rs.getString("RAZAO") + " <br>";
			    descricao = descricao + " Nome Fantasia : " + rs.getString("FANTASIA") + " <br>";
			    descricao = descricao + " Período : " + NeoDateUtils.safeDateFormat(rs.getDate("PrimeiraData"), "dd/MM/yyyy") + " à " + NeoDateUtils.safeDateFormat(rs.getDate("UltimaData"), "dd/MM/yyyy")
				    + "<br>";
			    descricao = descricao + " Eventos : " + rs.getString("QtdDsl") + "<br>";

			    IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
			    String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);

			    log.warn("##### EXECUTAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Eventos XXX8 Excessivos - Responsavel : " + executor + " Regional: " + reg + " Tarefa: " + tarefa);
			}
		    } else {
			log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Eventos XXX8 Excessivos - " + reg + " - Regra: Gerente responsável não encontrado - Papel " + papel.getName());
			// throw new
			// JobException("Erro no processamento, procurar no log por ["
			// + key + "]");
			continue;
		    }

		}
	    } catch (Exception e) {
		System.out.println("[" + key + "] ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Eventos XXX8 Excessivos - ERRO AO GERAR TAREFA - Regional: " + reg);
		log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Eventos XXX8 Excessivos - ERRO AO GERAR TAREFA - Regional: " + reg);
		e.printStackTrace();
		throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	    } finally {

		OrsegupsUtils.closeConnection(conn, pstm, rs);

		log.warn("##### FINALIZAR  AGENDADOR DE TAREFA: Abrir Tarefa Simples Eventos XXX8 Excessivos e Ordens de Serviço em Excesso - Regional: " + reg + " Data: "
			+ NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	    }
	}
    }
    
    
    /**
     * Retorna lista de exceções cadastradas em [SIGMA] Exceções XX8 Excessivos
     * @return Códigos de clientes (CD_CLIENTE) separados por virgulas
     */
    private String getListaExcecoes() {

	String codigos = "";

	try {
	    List<NeoObject> listaClientes = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("sigmaExcecoesXXX8Excessivos"));

	    ArrayList<Integer> listaCDCliente = new ArrayList<Integer>();

	    if (listaClientes != null && !listaClientes.isEmpty()) {
		for (NeoObject cliente : listaClientes) {

		    EntityWrapper wrappperExcecao = new EntityWrapper(cliente);
		    Long cdCliente = (Long) wrappperExcecao.findField("cliente.cd_cliente").getValue();
		    listaCDCliente.add(Integer.valueOf(cdCliente.intValue()));
		}
	    }
	    codigos = Joiner.on(",").join(listaCDCliente);
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("GravacaoOnDemand - ERRO getListaClientes - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	}
	return codigos;

    }
}
