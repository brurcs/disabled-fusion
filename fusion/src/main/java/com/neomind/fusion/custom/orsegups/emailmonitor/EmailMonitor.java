package com.neomind.fusion.custom.orsegups.emailmonitor;

import java.io.DataOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.FetchProfile;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.HtmlEmail;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.emailmonitor.vo.EmailMonitorVO;
import com.neomind.fusion.custom.orsegups.emailmonitor.vo.EmailVO;
import com.neomind.fusion.custom.orsegups.emailmonitor.vo.ResultVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoUtils;

public class EmailMonitor implements CustomJobAdapter
{

	private static final Log log = LogFactory.getLog(EmailMonitor.class);

	@SuppressWarnings("unchecked")
	@Override
	public void execute(CustomJobContext ctx)
	{
		//busca a lista de registro no eForm
		List<NeoObject> formParam = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("GCParametrosMonitoramentoEmail"));

		if (formParam != null && formParam.size() > 0)
		{
			EntityWrapper ewFormParam = null;

			for (NeoObject no : formParam)
			{
				ewFormParam = new EntityWrapper(no);

				EmailMonitorVO emVO = new EmailMonitorVO();

				if (ewFormParam != null)
				{
					Boolean habilitado = (Boolean) ewFormParam.findValue("habilitado");

					if (habilitado != null && habilitado)
					{
						//popula a VO com os campos do eForm
						emVO.setAutoAdvance((Boolean) ewFormParam.findValue("avancaAtividade"));
						emVO.setCustomAdapter((String) ewFormParam.findValue("eformAdapter"));
						//emVO.setDescription((String) ewFormParam.findValue("descricao"));
						emVO.setEnabled((Boolean) ewFormParam.findValue("habilitado"));
						emVO.setName((String) ewFormParam.findValue("Nome"));
						emVO.setProcess((ProcessModel) ewFormParam.findValue("processo"));
						emVO.setRequester((NeoUser) ewFormParam.findValue("solicitante"));
						emVO.setSenderRequester((Boolean) ewFormParam.findValue("remetenteSeraSolicitante"));
						emVO.setSendReport((Boolean) ewFormParam.findValue("enviarRelatorio"));
						emVO.setStartProcessForEachCC((Boolean) ewFormParam.findValue("iniciarProcessoParaCadaDestinatario"));
						emVO.setMailProvider((String) ewFormParam.findValue("tipoConta"));
						emVO.setMailServer((String) ewFormParam.findValue("servidorEmail"));
						emVO.setUserLogin((String) ewFormParam.findValue("usuarioEmail"));
						emVO.setUserPassword((String) ewFormParam.findValue("senhaEmail"));
						emVO.setMailPort((Long) ewFormParam.findValue("portaEmal"));
						emVO.setMailToMonitoring((String) ewFormParam.findValue("emailMonitorado"));

						//chama a funcao que verifica se ha emails para inicar processos

						//lMailVO.add(eMVO);
						verify(emVO);
					}
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void verify(EmailMonitorVO emailMonitorVO)
	{
		List<ResultVO> resultVOList = new ArrayList<ResultVO>();

		try
		{
			Session session;
			Store store = null;
			Folder folder = null;
			Folder inboxfolder = null;

			Properties props = System.getProperties();
			props.setProperty("mail.pop3s.rsetbeforequit", "true");
			props.setProperty("mail.pop3.rsetbeforequit", "true");

			session = Session.getInstance(props, null);
			//session.setDebug(true);

			String from = null;

			//buscar os campos do eform criado
			String emailprovider = emailMonitorVO.getMailProvider();
			String emailserver = emailMonitorVO.getMailServer();
			String emailuser = emailMonitorVO.getUserLogin();
			String emailpassword = NeoUtils.decodeDecodablePassword(emailMonitorVO.getUserPassword());
			Long emailPort = emailMonitorVO.getMailPort();

			try
			{

				store = session.getStore(emailprovider);
				store.connect(emailserver, emailPort.intValue(), emailuser, emailpassword);
				folder = store.getDefaultFolder();

				if (folder == null)
					throw new Exception("No default folder");

				inboxfolder = folder.getFolder("INBOX");

				if (inboxfolder == null)
					throw new Exception("No INBOX");

				inboxfolder.open(Folder.READ_WRITE);

				Message[] msgs = inboxfolder.getMessages();

				FetchProfile fp = new FetchProfile();
				fp.add("Subject");
				inboxfolder.fetch(msgs, fp);

				for (int j = 0; j < msgs.length; j++)
				{
					NeoUser remetente = null;

					try
					{
						Renderable rm = null;

						if (msgs[j].getContentType().startsWith("text/plain"))
						{
							rm = new RenderablePlainText(msgs[j]);
						}
						else
						{
							rm = new RenderableMessage(msgs[j]);
						}

						DataOutputStream out = null;

						//busca os destinatarios do email
						List<InternetAddress> cc = new ArrayList<InternetAddress>();
						Address[] toArray = msgs[j].getRecipients(Message.RecipientType.TO);
						if (toArray != null)
						{
							for (Address item : toArray)
							{
								cc.add((InternetAddress) item);
							}
						}
						Address[] ccArray = msgs[j].getRecipients(Message.RecipientType.CC);
						if (ccArray != null)
						{
							for (Address item : ccArray)
							{
								cc.add((InternetAddress) item);
							}
						}
						Address[] bccArray = msgs[j].getRecipients(Message.RecipientType.BCC);
						if (bccArray != null)
						{
							for (Address item : bccArray)
							{
								cc.add((InternetAddress) item);
							}
						}

						List<NeoUser> destinatarios = new ArrayList<NeoUser>();
						remetente = this.buscaRemetente(emailMonitorVO, msgs[j]);

						// caso exista rementente inicia logica de inicio de processos
						if (remetente != null)
						{
							for (InternetAddress internetAddress : cc)
							{
								String email = internetAddress.getAddress();

								if (emailMonitorVO.getMailToMonitoring() != null && email != null && !emailMonitorVO.getMailToMonitoring().equalsIgnoreCase(email))
								{
									NeoUser usuario = validaUsuario(email);

									//caso o remetente nao existe adiciona um resultVO para q seja enviado no email ao final do processo
									if (usuario == null)
									{
										ResultVO erVO = new ResultVO();

										erVO.setMessage("E-mail (" + from + ") não relacionado a nenhum usuário cadastrado no Fusion.");
										erVO.setResult(false);
										resultVOList.add(erVO);
									}
									else
									{
										destinatarios.add(usuario);
									}
								}
							}

							String emailFormAdapterImplStr = emailMonitorVO.getCustomAdapter();
							Class<EmailFormAdapter> clazz = (Class<EmailFormAdapter>) Class.forName(emailFormAdapterImplStr);
							EmailFormAdapter emailFormAdapterImpl = clazz.newInstance();

							//caso existam destinatário inicia os processos
							if (destinatarios != null && !destinatarios.isEmpty())
							{
								if (emailMonitorVO.startProcessForEachCC())
								{
									//inicia um processo para cada destinatario
									for (NeoUser neoUser : destinatarios)
									{
										EmailVO emailVO = new EmailVO();
										emailVO.setRemetente(remetente);
										emailVO.getDestinatarios().add(neoUser);
										emailVO.setAssunto(rm.getSubject());
										emailVO.setTextoEmail(rm.getBodytext());

										if (emailFormAdapterImpl != null)
										{
											NeoObject eform = emailFormAdapterImpl.getProcessForm(emailVO);
											ResultVO resultVO = OrsegupsUtils.iniciaWorkflow(eform, emailMonitorVO.getProcess(), remetente, emailMonitorVO.getAutoAdvance());
											resultVO.setDestinatario(neoUser);
											resultVOList.add(resultVO);
										}
										else
										{
											ResultVO resultVO = new ResultVO();
											resultVO.setMessage("Nao foi possivel instanciar a classe " + emailFormAdapterImplStr);
											resultVO.setResult(false);

											resultVOList.add(resultVO);
										}
									}
								}
								else
								{
									//inicia um processo passando todos os destinatarios como parametro para recuperar o form do processo
									EmailVO emailVO = new EmailVO();
									emailVO.setRemetente(remetente);
									emailVO.setDestinatarios(destinatarios);
									emailVO.setAssunto(rm.getSubject());
									emailVO.setTextoEmail(rm.getBodytext());

									if (emailFormAdapterImpl != null)
									{
										NeoObject eform = emailFormAdapterImpl.getProcessForm(emailVO);
										ResultVO resultVO = OrsegupsUtils.iniciaWorkflow(eform, emailMonitorVO.getProcess(), remetente, emailMonitorVO.getAutoAdvance());
										resultVOList.add(resultVO);
									}
									else
									{
										ResultVO resultVO = new ResultVO();
										resultVO.setMessage("Nao foi possivel instanciar a classe " + emailFormAdapterImplStr);
										resultVO.setResult(false);

										resultVOList.add(resultVO);
									}
								}
							}
						}
						else
						{
							ResultVO erVO = new ResultVO();

							erVO.setMessage("E-mail (" + from + ") não relacionado a nenhum usuário cadastrado no Fusion.");
							erVO.setResult(false);
							resultVOList.add(erVO);
						}

						feedback(remetente, resultVOList, emailMonitorVO);
					}
					catch (Exception ex)
					{
						ex.printStackTrace();
						log.error(ex);
						ResultVO erVO = new ResultVO();

						erVO.setMessage(ex.toString());
						erVO.setResult(false);
						resultVOList.add(erVO);

						feedback(remetente, resultVOList);
					}
					finally
					{
						// marca como deletada para não processa novamente o mesmo email
						//msgs[j].setFlag(Flags.Flag.DELETED, true);

						// zera a lista de resultados pois a proxima iteração do loop é referente ao proximo email
						resultVOList = new ArrayList<ResultVO>();
					}
				}
			}
			catch (Exception ex)
			{
				log.error(ex);
				ResultVO erVO = new ResultVO();

				erVO.setMessage(ex.toString());
				erVO.setResult(false);
				resultVOList.add(erVO);
			}
			finally
			{
				try
				{
					if (store != null)
					{
						inboxfolder.close(true);
						store.close();
					}
				}
				catch (MessagingException ex)
				{
					log.error(ex);
					ResultVO erVO = new ResultVO();

					erVO.setMessage(ex.toString());
					erVO.setResult(false);
					resultVOList.add(erVO);
				}
			}
		}
		catch (Exception e)
		{
			ResultVO erVO = new ResultVO();

			erVO.setMessage(e.toString());
			erVO.setResult(false);
			resultVOList.add(erVO);
		}
		feedback(null, resultVOList);
	}

	public void feedback(NeoUser requester, List<ResultVO> resultVOList)
	{
		this.feedback(requester, resultVOList, null);
	}

	public void feedback(NeoUser requester, List<ResultVO> resultVOList, EmailMonitorVO emailMonitorVO)
	{
		if (resultVOList != null && !resultVOList.isEmpty())
		{
			MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

			if (settings != null && settings.isEnabled())
			{
				//Caso não tenha remetente envia email para o administrador do fusion
				if (requester == null)
				{
					requester = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "adm"));
				}

				HtmlEmail noUserEmail = new HtmlEmail();
				StringBuilder noUserMsg = new StringBuilder();
				Boolean sendReport = false;
				if (emailMonitorVO != null)
				{
					sendReport = emailMonitorVO.getSendReport();
				}

				try
				{
					noUserEmail.setCharset("ISO-8859-1");
					noUserEmail.addTo(requester.getEmail());
					noUserEmail.setFrom(settings.getFromEMail());

					noUserEmail.setSubject("Neomind Fusion - Relatório via e-mail");

					noUserMsg.append("<html>");
					noUserMsg.append("<style type=\"text/css\">");
					noUserMsg.append("body { background: #FFFFFF; color:black; padding:10px; font-family:Calibri, Arial, Helvetica, sans-serif; font-size:12px;}");
					noUserMsg.append("p{font-size:12px;}");
					noUserMsg.append("</style>");
					noUserMsg.append("<body>");
					noUserMsg.append("<p>Olá(a)," + requester.getFullName() + "<br /><br />");
					if (emailMonitorVO != null)
					{
						noUserMsg.append("Segue abaixo o relatório de inicio da solicitação " + emailMonitorVO.getProcess().getName() + " via email: <br /><br />");
					}
					else
					{
						noUserMsg.append("Segue abaixo o relatório de inicio de solicitação workflow via email: <br /><br />");
					}

					for (ResultVO resultVO : resultVOList)
					{
						if (!resultVO.getResult())
						{
							noUserMsg.append("Msg: Erro: <b>" + resultVO.getMessage() + "</b><br /><br />");

							if (!sendReport)
							{
								sendReport = true;
							}
						}
						else
						{
							noUserMsg.append("Msg: Solicitação<b> " + resultVO.getProcessCode() + "</b> iniciadada com sucesso");
							String destinatario = null;
							
							if(resultVO.getDestinatario() != null)
							{
								if(resultVO.getDestinatario() != null && resultVO.getDestinatario() instanceof NeoUser)
								{
									destinatario = ((NeoUser) resultVO.getDestinatario()).getFullName();
								}
								else
								{
									destinatario = resultVO.getDestinatario().getName();
								}
							}
							
							if(destinatario != null && !destinatario.isEmpty()){
								noUserMsg.append(" e está sob responsabilidade de " + destinatario + " <br /><br />");
							}
							else
							{
								noUserMsg.append(".<br /><br />");
							}
						}
					}

					noUserMsg.append("</body>");
					noUserMsg.append("</html>");

					noUserEmail.setHtmlMsg(noUserMsg.toString());

					settings.applyConfig(noUserEmail);

					noUserEmail.send();
				}
				catch (Exception excep)
				{
					log.error("Não foi possível enviar e-mail: " + excep);
					excep.printStackTrace();
				}
			}
			else
			{
				log.error("Erro ao enviar email. Verifique as configuracoes de envio de email do Fusion.");
			}
		}
	}

	private NeoUser validaUsuario(String email)
	{
		NeoUser user = null;

		if (email != null)
		{
			user = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("email", email));
		}
		return user;
	}

	private NeoUser buscaRemetente(EmailMonitorVO emailMonitorVO, Message message) throws MessagingException
	{
		NeoUser remetente = null;

		if (message != null && emailMonitorVO != null)
		{
			if (emailMonitorVO.isSenderRequester())
			{
				//busca o remetente
				Address[] fromInternetAddressArray = message.getFrom();
				if (fromInternetAddressArray != null && fromInternetAddressArray.length > 0)
				{
					InternetAddress fromInternetAddress = (InternetAddress) fromInternetAddressArray[0];
					String emailFrom = fromInternetAddress.getAddress();

					remetente = this.validaUsuario(emailFrom);
				}
			}
			else
			{
				remetente = emailMonitorVO.getRequester();
			}
		}

		return remetente;
	}

}
