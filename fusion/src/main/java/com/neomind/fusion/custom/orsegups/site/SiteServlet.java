package com.neomind.fusion.custom.orsegups.site;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.UserWebServiceAuthentication;
import com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServlet;
import com.neomind.fusion.custom.orsegups.rsc.helper.RSCHelper;
import com.neomind.fusion.custom.orsegups.rsc.vo.RSCVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteCidadeVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteTipoContatoVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.fusion.workflow.simulation.WorkflowService;

@WebServlet(name="SiteServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.site.SiteServlet"})
public class SiteServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	private static final Log log = LogFactory.getLog(OrsegupsMapsServlet.class);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp); 
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	private void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/html");
		response.setCharacterEncoding("ISO-8859-1");

		PrintWriter out;
		try
		{
			out = response.getWriter();

			String action = "";

			if (request.getParameter("action") != null)
			{
				action = request.getParameter("action");
			}
			
			if (action.equalsIgnoreCase("ping"))
			{
				out.print(1);
			}
			
			if (action.equalsIgnoreCase("listarTipoContato"))
			{
				this.listarTipoContatoRSC(request, response, out);
			}
			
			if (action.equalsIgnoreCase("listarCidade"))
			{
				this.listarCidade(request, response, out);
			}

			if (action.equalsIgnoreCase("salvarContato"))
			{
				this.salvarContato(request, response, out);
			}
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	
	private void listarTipoContatoRSC(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws JSONException
	{
		//Validar autenticacao MD5
		String key = request.getParameter("key");

		UserWebServiceAuthentication auth = new UserWebServiceAuthentication();
		if (!auth.authentication(key))
		{
			JSONObject jsonAuth = new JSONObject();

			jsonAuth.append("status", 0L);
			jsonAuth.append("msg", "Erro de autenticação");

			out.print(jsonAuth);
			return;
		}

		List<SiteTipoContatoVO> listTipoContato = new ArrayList<SiteTipoContatoVO>();
		List<NeoObject> objs = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("tipoContato"));

		for (NeoObject obj : objs)
		{
			SiteTipoContatoVO tipoContato = new SiteTipoContatoVO();
			EntityWrapper wrapper = new EntityWrapper(obj);
			tipoContato.setTipo((String) wrapper.getValue("tipo"));
			tipoContato.setId((Long) wrapper.getValue("neoId"));
			listTipoContato.add(tipoContato);
		}
		Gson gson = new Gson();
		String tipoContatoJson = gson.toJson(listTipoContato);

		out.print(tipoContatoJson);
	}
	
	private void listarCidade(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws JSONException
	{
		String value = (String) request.getParameter("value");

		//Validar autenticacao MD5
		String key = request.getParameter("key");

		UserWebServiceAuthentication auth = new UserWebServiceAuthentication();
		if (!auth.authentication(key))
		{
			JSONObject jsonAuth = new JSONObject();

			jsonAuth.append("status", 0L);
			jsonAuth.append("msg", "Erro de autenticação");

			out.print(jsonAuth);
			return;
		}

		if (value != null)
		{
			List<SiteCidadeVO> listCidade = new ArrayList<SiteCidadeVO>();
			String valueClear = OrsegupsUtils.removeAccents(value);
			System.out.println(valueClear);
			QLRawFilter filterCidade = new QLRawFilter("nomcid LIKE '%" + valueClear + "%'");
			List<NeoObject> objs = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORH_R074CID"), filterCidade, -1, 10);
			if (objs != null && !objs.isEmpty())
			{
				for (NeoObject obj : objs)
				{
					SiteCidadeVO cidade = new SiteCidadeVO();
					EntityWrapper wrapper = new EntityWrapper(obj);
					cidade.setCodcid((Long) wrapper.getValue("codcid"));
					cidade.setNomcid((String) wrapper.getValue("nomcid"));
					cidade.setEstcid((String) wrapper.getValue("estcid"));
					listCidade.add(cidade);
				}
			}
			Gson gson = new Gson();
			String cidadeJson = gson.toJson(listCidade);

			out.print(cidadeJson);
		}
	}
	
	private void salvarContato(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws JSONException
	{
		try
		{
			//Validar autentificacao MD5
			String key = request.getParameter("key");
	
			UserWebServiceAuthentication auth = new UserWebServiceAuthentication();
			if (!auth.authentication(key))
			{
				JSONObject jsonAuth = new JSONObject();
	
				jsonAuth.append("status", 0L);
				jsonAuth.append("msg", "Erro de autenticação");
	
				out.print(jsonAuth);
				return;
			}
	
			//Busca os valores por Request
			String nome = request.getParameter("nome");
			String email = request.getParameter("email");
			String telefone = request.getParameter("telefone");
			String mensagem = request.getParameter("mensagem");
			String cidade = request.getParameter("cidade"); // atualmente o site nao envia estes dados, nao sei o porque mas ninguem reclamou. Provavelmente alguma solicitacao
			String tipoContato = request.getParameter("tipoContato");
	
			//Busca o Tipo de Contato
			QLEqualsFilter filterTipoContato = new QLEqualsFilter("neoId", Long.valueOf(tipoContato));
			NeoObject tipoContatoObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("tipoContato"), filterTipoContato);
			EntityWrapper tipoContatoWrapper = new EntityWrapper(tipoContatoObj);
			Long idTipoContato = (Long) tipoContatoWrapper.getValue("neoId");
			String descricaoTipoContato = (String) tipoContatoWrapper.getValue("tipo");
	
			//Cria Variaveis para diferenciar RSC de RRC
			String eform = "";
			String workflow = "";
			String fieldTelefone = "";
			String fieldContato = "";
			String fieldMensagem = "";
			String valueMensagem = "";
			String valueSolicitante = "";
			
			GregorianCalendar gcPrazo = new GregorianCalendar();
			gcPrazo.add(GregorianCalendar.DATE, 5);
			gcPrazo.set(GregorianCalendar.HOUR, 23);
			gcPrazo.set(GregorianCalendar.MINUTE, 59);
			gcPrazo.set(GregorianCalendar.SECOND, 59);
	
			//Reclamacao
			if (idTipoContato == 11715151L)
			{
				eform = "RRCRelatorioReclamacaoCliente";
				workflow = "Q005 - RRC - Relatório de Reclamação de Cliente";
				fieldTelefone = "telefone";
				fieldContato = "pessoaContato";
				fieldMensagem = "textoReclamacao";
				valueSolicitante = "SITERESPONSAVELRRC";
			}
			else
			{
				/*eform = "RSCRelatorioSolicitacaoClienteNovo";
				workflow = "C027 - RSC - Relatório de Solicitação de Cliente Novo";
				fieldTelefone = "telefones";
				fieldContato = "contato";
				fieldMensagem = "descricao";
				valueMensagem = "Intenção: " + descricaoTipoContato + " ";
				valueSolicitante = "SITERESPONSAVELRSC";*/
				valueSolicitante = "SITERESPONSAVELRSC";
				
				RSCVO rsc = new RSCVO();
				//rsc.setCodCli(codcli);
				rsc.setOrigemSolicitacao("05");
				rsc.setTipoContato(tipoContato);
				rsc.setNome(nome);
				rsc.setEmail(email);
				rsc.setTelefone(telefone);
				rsc.setMensagem(mensagem);
				//rsc.setCidade(cidade);
				//rsc.setBairro(bairro);
				rsc.setPrazo(gcPrazo);
				
				String tarefa = RSCHelper.abrirRSC(rsc, valueSolicitante,true);
				System.out.println("Tarefa RSC Aberta: "+ tarefa);
				JSONObject jsonCliente = new JSONObject();
				
				jsonCliente.append("status", 1L);
				jsonCliente.append("msg", "Obrigado pelo contato");
				out.print(jsonCliente);
				return;
			}
	
			//Cria Instancia do Eform Principal
			InstantiableEntityInfo infoSolicitacao = AdapterUtils.getInstantiableEntityInfo(eform);
			NeoObject noSolicitacao = infoSolicitacao.createNewInstance();
			EntityWrapper solicitacaoWrapper = new EntityWrapper(noSolicitacao);
	
			//Busca Origem da Solicitacao
			QLEqualsFilter filterOrigem = new QLEqualsFilter("sequencia", "05");
			NeoObject origemObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("RRCOrigem"), filterOrigem);
	
			//Busca Cidade
			//QLEqualsFilter filterCidade = new QLEqualsFilter("codcid", Long.valueOf(cidade));
			//NeoObject cidadeObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("VETORH_R074CID"), filterCidade);
	
			//Seta os valores
			if (idTipoContato == 11715151L){
				solicitacaoWrapper.setValue("prazoAcao",OrsegupsUtils.getNextWorkDay(gcPrazo));
				solicitacaoWrapper.findField("origem").setValue(origemObj);
				solicitacaoWrapper.findField("cidade").setValue(cidade);
				solicitacaoWrapper.findField("email").setValue(email);
				solicitacaoWrapper.findField(fieldContato).setValue(nome);
				solicitacaoWrapper.findField(fieldTelefone).setValue(telefone);
				solicitacaoWrapper.findField(fieldMensagem).setValue(valueMensagem + mensagem);
			}else{
				
			}
			
	
			//Salva o Eform
			PersistEngine.persist(noSolicitacao);
	
			// Cria Instancia do WorkFlow
			QLEqualsFilter equal = new QLEqualsFilter("Name", workflow);
			ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);
	
			//Busca o solicitante
			NeoPaper papel = new NeoPaper();
			String executor = "";
			NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code",valueSolicitante));
			papel = (NeoPaper)obj;
			
			if(papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
			{
				for(NeoUser user : papel.getUsers()){
				executor = user.getCode();
				break;
				}
				
			}else{
				String gestaoCerec = "Gestão CEREC";
				NeoPaper objCerec = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code",gestaoCerec));
				papel = (NeoPaper)objCerec;
				if(papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
				{
					for(NeoUser user : papel.getUsers()){
					executor = user.getCode();
					break;
					}
				}else{
					JSONObject jsonCliente = new JSONObject();
					jsonCliente.append("status", 0L);
					jsonCliente.append("msg", "Usuário ou Gestor Responsável não encontrado!");
					out.print(jsonCliente);
				}
				
			}
			
			NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", executor));
			
			/**
			 * @author neomind willian.mews - Alterado para utilizar a nova arquitetura de processos do Fusion
			 * @date 11/03/2015
			 */
			
			final WFProcess processo = WorkflowService.startProcess(processModel, noSolicitacao, true, solicitante);
			processo.setSaved(true);
			System.out.println("solicitacao aberta site. " + processo.getCode());
			//PersistEngine.getEntityManager().flush();
			try
			{
				new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividade(solicitante,false);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				out.print("Erro #4 - Erro ao avançar a primeira tarefa");
				return;
			}
			
			/**
			 * FIM ALTERAÇÕES - NEOMIND
			 */
			
	
			// Dispara email ao cliente
			Map<String, Object> paramMap = new HashMap<String, Object>();
	
			paramMap.put("tarefa", processo.getCode());
			paramMap.put("nome", nome);
			paramMap.put("tipo", tipoContato);
			paramMap.put("mensagem", mensagem);
	
			FusionRuntime.getInstance().getMailEngine().sendEMail((String) solicitacaoWrapper.findField("email").getValue(), "/portal_orsegups/emailAberturaRSC.jsp", paramMap);
			
			JSONObject jsonCliente = new JSONObject();
			
			jsonCliente.append("status", 1L);
			jsonCliente.append("msg", "Obrigado pelo contato");
			out.print(jsonCliente);
		}
		catch (Exception e)
		{
			JSONObject jsonCliente = new JSONObject();
			
			jsonCliente.append("status", 0L);
			jsonCliente.append("msg", "Erro inesperado");
			out.print(jsonCliente);
			e.printStackTrace();
		}
	}
}
