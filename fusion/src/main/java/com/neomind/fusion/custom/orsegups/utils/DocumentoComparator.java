package com.neomind.fusion.custom.orsegups.utils;
import java.util.Comparator;
import com.neomind.fusion.custom.orsegups.presenca.vo.DocumentoTreeVO;

public class DocumentoComparator  implements Comparator<DocumentoTreeVO> {  
	public int compare(DocumentoTreeVO o1, DocumentoTreeVO o2){  

		return o1.getNome().compareTo(o2.getNome());  
	}  
}