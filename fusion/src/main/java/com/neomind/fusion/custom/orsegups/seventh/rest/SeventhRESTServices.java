package com.neomind.fusion.custom.orsegups.seventh.rest;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.neomind.fusion.custom.orsegups.seventh.SeventhUtils;
import com.neomind.fusion.custom.orsegups.seventh.bean.EventoArmeDesarme;
import com.neomind.fusion.custom.orsegups.seventh.dto.SeventhCameraIDsDTO;
import com.neomind.fusion.custom.orsegups.ti.sigma.engine.SIGMAUtilsEngine;


@Path(value = "seventh")
public class SeventhRESTServices {
    
    private SeventhUtils utils = new SeventhUtils();
    
    @GET
    @Path("getCameraIDs/{cdCliente}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public SeventhCameraIDsDTO getCameraIDs(@PathParam("cdCliente") int cdCliente){
	
	SeventhCameraIDsDTO retorno = new SeventhCameraIDsDTO();
	
	EventoArmeDesarme dados = SIGMAUtilsEngine.getDadosCFTV(cdCliente);
	
	if (dados != null){
	    
	    retorno.setServidor(dados.getServidorCFTV());
	    
	    Map<String, String> cameras = null;
	    
	    try {
		cameras = utils.getCameraCliente(dados);
	    } catch (IOException e) {
		e.printStackTrace();
	    }
	    
	    if (cameras != null){
		for (String key : cameras.keySet()){
		    List<String> ids = retorno.getIds();	    
		    ids.add(key);
		}
	    }
	    
	}
	
	return retorno;
    }

}
