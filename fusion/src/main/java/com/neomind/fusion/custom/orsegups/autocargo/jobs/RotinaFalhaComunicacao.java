package com.neomind.fusion.custom.orsegups.autocargo.jobs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.common.base.Joiner;
import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.autocargo.beans.vehicle.ACVehicle;
import com.neomind.fusion.custom.orsegups.autocargo.beans.vehicle.ACVehicles;
import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.ti.sigma.engine.SIGMAUtilsEngine;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class RotinaFalhaComunicacao implements CustomJobAdapter{ // FalhaComunicacaoVeiculos

    private final Log log = LogFactory.getLog(RotinaFalhaComunicacao.class);

    private Long key = GregorianCalendar.getInstance().getTimeInMillis();

    private SIGMAUtilsEngine engine = new SIGMAUtilsEngine();

    /**
     * Veiculos com falha de comunicação de pelo 12 horas na data de hoje.
     * <tt>Rastreador</tt> é a chave do mapa
     */
    private Map<String, ACVehicle> miscommunication = new HashMap<String, ACVehicle>();

    /**
     * Contas <tt>rastreadores</tt> que possuem eventos R001 abertos
     */
    private Map<String, EventoVO> miscommunicationOpenEvents = new HashMap<String, EventoVO>();

    /**
     * Número total de páginas recebidos da API de consulta de falhas de eventos
     */
    private int totalPageNumber = 1;

    /**
     * Inconsistencias na inserção de eventos
     */
    private List<String> inconsistencias = new ArrayList<String>();

    public void execute(CustomJobContext ctx) {

	for (int i = 0; i < totalPageNumber; i++) {

	    try {
		this.getMiscommunicationFail(i);
	    } catch (RuntimeException e) {
		this.logError(e.getMessage(), "Erro ao acessar API", "Orsegups");
		log.error("RotinaFalhaComunicacao - Erro ao acessar a API Autocargo ACEngineFalhaComunicacao [" + key + "] :");
		e.printStackTrace();
		throw new JobException("Erro ao acessar API de consulta de falha de comunicação Autocargo! Procurar no log por [" + key + "]");
	    }

	    try {
		Thread.sleep(1000);
	    } catch (InterruptedException e) {
		e.printStackTrace();
	    }
	}

	boolean getEvents = this.getMiscommunicationEvent();

	int countTry = 0;

	while (!getEvents && countTry < 2) {
	    getEvents = this.getMiscommunicationEvent();
	    countTry++;
	}

	if (!miscommunication.isEmpty()) {

	    for (String key : miscommunication.keySet()) {

		if (miscommunicationOpenEvents.containsKey(key)) {
		    /* Já possui evento aberto, não faço nada */
		    miscommunicationOpenEvents.remove(key);
		    
		} else {
		    
		    boolean openOS = this.checkOpenOS(key);
		    
		    if (!openOS){
			String retorno = engine.receberEventoByIdentificadorCliente(key, "R001");
			
			if (!retorno.equals("ACK")) {
			    ACVehicle v = miscommunication.get(key);
			    String cliente = "Cliente: "+ v.getClient_name() + "<br/>Veiculo: "+v.getBrand()+" "+v.getModel()+" "
				    + "<br/>Nº Modulo: "+v.getTracking_module().getNumber()+" <br/>Placa: " + v.getRegistration_plate();
			    this.logError("R001", retorno, cliente);
			    this.inconsistencias.add(retorno + ";" + cliente);
			}			
		    }		  
		}
	    }
	}

	/*
	 * Agora na lista miscommunicationOpenEvents restam apenas os eventos
	 * abertos que não estão mais em falha
	 */

	if (!miscommunicationOpenEvents.isEmpty()) {
	    for (String placa : miscommunicationOpenEvents.keySet()) {
		String retorno = engine.receberEventoByIdentificadorCliente(placa, "R002");

		if (!retorno.equals("ACK")) {
		    EventoVO v = miscommunicationOpenEvents.get(placa);
		    String cliente = "Cliente: " + v.getCodigoCentral() + "[" + v.getParticao() + "] " + v.getFantasia() + " Placa: " + placa;
		    this.logError("R002", retorno, cliente);
		    this.inconsistencias.add(retorno + ";" + cliente);
		}
	    }
	}
	if (Calendar.getInstance().get(Calendar.HOUR_OF_DAY) == 8 || Calendar.getInstance().get(Calendar.HOUR_OF_DAY) == 14){
	    this.sendEmail();	    
	}

    }
    
    private boolean checkOpenOS(String placa){
	
	String defeitosOS = this.getDefeitos();
	
	int count = 0;
	
	while (defeitosOS.isEmpty() && count < 2){
	    defeitosOS = this.getDefeitos();
	}
	
	if (!defeitosOS.isEmpty()){
	    StringBuilder sql = new StringBuilder();
	    
	    sql.append(" SELECT ID_ORDEM FROM dbORDEM OS WITH(NOLOCK) "); 
	    sql.append(" INNER JOIN dbCENTRAL C WITH(NOLOCK) ON OS.CD_CLIENTE = C.CD_CLIENTE ");
	    sql.append(" WHERE (OS.FECHADO = 0 OR OS.FECHAMENTO IS NULL)   ");
	    sql.append(" AND C.NM_RASTREADOR ='"+placa+"'");
	    sql.append(" AND OS.IDOSDEFEITO IN ("+defeitosOS+")  ");
	    
	    Connection conn = null;
	    Statement stmt = null;
	    ResultSet rs = null;
	    
	    try{
		
		conn = PersistEngine.getConnection("SIGMA90");
		
		stmt = conn.createStatement();
		
		rs = stmt.executeQuery(sql.toString());
		
		if (rs.next()){
		    return true;
		}
		
		
	    }catch (SQLException e){
		log.error("RotinaFalhaComunicacao Falha ao buscar ordens de serviço abertas");
		e.printStackTrace();
	    }finally{
		OrsegupsUtils.closeConnection(conn, stmt, rs);
	    }
	    
	}
	
	return false;
    }
    
    private String getDefeitos(){
	
	String codigos = "";
	
	List<NeoObject> listaExcecoesDefeito = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("ATCadastroDefeitoOS"));
	
	ArrayList<Integer> listaCodDefeito = new ArrayList<Integer>();

	if (listaExcecoesDefeito != null && !listaExcecoesDefeito.isEmpty()) {
	    for (NeoObject excecao : listaExcecoesDefeito) {
		
		EntityWrapper wrappperExcecao = new EntityWrapper(excecao);
		
		Long codDefeito = (Long) wrappperExcecao.findField("defeito.idosdefeito").getValue();
		
		listaCodDefeito.add(Integer.valueOf(codDefeito.intValue()));	
	
	    }
	}
	
	codigos = Joiner.on(",").join(listaCodDefeito);
	
	return codigos;
    }
    
    /**
     * Popula o set {@link #miscommunicationOpenEvents}
     * 
     * @return <tt>true</tt> se executou com sucesso <tt>false</tt> caso
     *         contrário
     */
    private boolean getMiscommunicationEvent() {

	boolean executouComSucesso = false;

	StringBuilder sql = new StringBuilder();

	sql.append(" SELECT C.NM_RASTREADOR, C.ID_CENTRAL, C.PARTICAO, C.FANTASIA FROM HISTORICO H WITH(NOLOCK) ");
	sql.append(" INNER JOIN dbCENTRAL C WITH(NOLOCK) ON C.CD_CLIENTE = H.CD_CLIENTE ");
	sql.append(" WHERE H.CD_EVENTO = 'R001' ");
	sql.append(" ORDER BY DT_PROCESSADO DESC ");

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	try {

	    conn = PersistEngine.getConnection("SIGMA90");
	    stmt = conn.createStatement();
	    rs = stmt.executeQuery(sql.toString());

	    while (rs.next()) {

		EventoVO novoEvento = new EventoVO();

		novoEvento.setCodigoCentral(rs.getString(2));
		novoEvento.setParticao(rs.getString(3));
		novoEvento.setFantasia(rs.getString(4));

		this.miscommunicationOpenEvents.put(rs.getString(1), novoEvento);
	    }

	    executouComSucesso = true;

	} catch (SQLException e) {
	    log.error("RotinaFalhaComunicacao Falha ao buscar eventos de falha de comunicação abertos");
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	return executouComSucesso;

    }

    /**
     * Popula o mapa {@link #miscommunication}
     * 
     * @param pageNumber
     *            {@link #totalPageNumber} Não é necessário modificar
     */
    private void getMiscommunicationFail(int pageNumber) {

	StringBuilder retorno = new StringBuilder();

	try {

	    URL url = new URL("https://www2.autocargo.com.br/api/integration/v1/miscommunication?page=" + pageNumber + "&miscommunication_control=true&miscommunication_duration_limit=12");
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setRequestMethod("GET");
	    conn.setRequestProperty("Authorization", "Token token=\"115fe7a39c6d1910308c335ba3f845a0f9d6495ee912068b9cf7a90f717f03a5\"");
	    conn.setRequestProperty("Content-Type", "application/json");
	    conn.setRequestProperty("Accept", "application/json");

	    if (conn.getResponseCode() != 200) {
		throw new RuntimeException(String.valueOf(conn.getResponseCode()));
	    }

	    String totalPorPagina = conn.getHeaderField("X-Autocargo-Per-Page");
	    String totalResultados = conn.getHeaderField("X-Autocargo-Total");

	    int total = Integer.parseInt(totalPorPagina);

	    int result = this.roundUp(Integer.parseInt(totalResultados), total);

	    this.totalPageNumber = result / total;

	    BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream()), "UTF-8"));

	    String output;

	    while ((output = br.readLine()) != null) {
		retorno.append(output + "\n");
	    }

	    conn.disconnect();

	} catch (MalformedURLException e) {
	    log.error("RotinaFalhaComunicacao - Falha ao buscar falhas de comunicação via API - URL Mal Formada");
	    e.printStackTrace();

	} catch (IOException e) {
	    log.error("RotinaFalhaComunicacao - Falha ao buscar falhas de comunicação via API - IO Exception");
	    e.printStackTrace();

	}

	Gson gson = new Gson();
	ACVehicles resultList = gson.fromJson(retorno.toString(), ACVehicles.class);

	for (ACVehicle v : resultList.getVehicles()) {
	    miscommunication.put(v.getRegistration_plate(), v);
	}

    }

    private int roundUp(int toRound, int roundTo) {

	if (toRound % roundTo == 0) {
	    return toRound;
	} else {
	    return (roundTo - toRound % roundTo) + toRound;
	}

    }

    /**
     * Gravar de execução na base
     * 
     * @param codigoEvento
     * @param erro
     * @param cliente
     */
    private void logError(String codigoEvento, String erro, String cliente) {

	Connection conn = null;
	PreparedStatement pstm = null;

	String sql = "INSERT INTO FalhaComunicacaoVeiculosLogs (CODIGO_EVENTO, ERRO, CLIENTE, DATA_ERRO) VALUES (?,?,?,?)";

	try {
	    conn = PersistEngine.getConnection("TIDB");

	    pstm = conn.prepareStatement(sql);

	    pstm.setString(1, codigoEvento);
	    pstm.setString(2, erro);
	    pstm.setString(3, cliente);
	    pstm.setTimestamp(4, new Timestamp(Calendar.getInstance().getTimeInMillis()));

	    pstm.executeUpdate();

	} catch (SQLException e) {
	    log.error("RotinaFalhaComunicacao - Falha ao logar falhas de comunicação em TIDB");
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}
	
    }

    /**
     * Encaminha e-mail de informe de inconsistências
     */
    private void sendEmail() {

	if (!this.inconsistencias.isEmpty()) {
	    
	    StringBuilder dados = new StringBuilder();

	    int cont = 0;

	    for (String i : this.inconsistencias) {

		String a[] = i.split(";");

		int resto = (cont) % 2;
		if (resto == 0) {
		    dados.append("<tr style=\"background-color: #d6e9f9\" ><td>" + a[0] + "</td><td>" + a[1] + "</td></tr>");
		} else {
		    dados.append("<tr><td>" + a[0] + "</td><td>" + a[1] + "</td></tr>");
		}

		cont++;
	    }

	    StringBuilder corpoEmail = new StringBuilder();

	    corpoEmail.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
	    corpoEmail.append("<html>");
	    corpoEmail.append("<head>");
	    corpoEmail.append("<meta charset=\"ISO-8859-1\">");
	    corpoEmail.append("<title>Relatório de inconsistências</title>");
	    corpoEmail.append("<style>");
	    corpoEmail.append("h2{");
	    corpoEmail.append("font-weight:bold; color:#303090; letter-spacing:1pt; word-spacing:2pt; font-size:20px; text-align:center; font-family:Verdana; line-height:1;");
	    corpoEmail.append("}");
	    corpoEmail.append(".table-p{");
	    corpoEmail.append("font-family: Verdana; font-weight:normal; font-size:12px; text-align:justify;");
	    corpoEmail.append(" }");
	    corpoEmail.append(".table-info{");
	    corpoEmail.append("table-layout: fixed;");
	    corpoEmail.append("text-align:left;");
	    corpoEmail.append("font-family: Verdana;");
	    corpoEmail.append("}");
	    corpoEmail.append(".table-info td{");
	    corpoEmail.append("overflow-x: hidden;");
	    corpoEmail.append(" }");
	    corpoEmail.append(".table-info thead{");
	    corpoEmail.append("background-color:#303090;");
	    corpoEmail.append("color: white;");
	    corpoEmail.append("font-weight:bold;");
	    corpoEmail.append("	}");
	    corpoEmail.append("</style>");
	    corpoEmail.append("</head>");
	    corpoEmail.append("<body>");
	    corpoEmail.append("<div>");
	    corpoEmail.append("<table width=\"600\" align=\"center\">");
	    corpoEmail.append("<tr><td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/header.jpg\" width=\"600\" height=\"145\"></td></tr>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("<td><h2>Inconsistências de falha de comunicação</h2></td>");
	    corpoEmail.append("<table width=\"600\" align=\"center\" class=\"table-p\"> ");
	    corpoEmail.append("<tr><td>As seguintes inconsistências ocorreram durante a inserção de eventos de falha de comunicação:</td></tr>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("<table width=\"600\" align=\"center\" class=\"table-info\"> ");
	    corpoEmail.append("<thead>");
	    corpoEmail.append("<tr>");
	    corpoEmail.append("<td>Inconsistência</td>");
	    corpoEmail.append("<td>Cliente</td>");
	    corpoEmail.append("</tr>");
	    corpoEmail.append("</thead>");
	    corpoEmail.append("<tbody>");
	    corpoEmail.append("<!-- Recebe dados do método -->");
	    corpoEmail.append(dados);
	    corpoEmail.append("<br>");
	    corpoEmail.append("</tbody>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("<br>");
	    corpoEmail.append("<!-- Rodapé do e-mail -->");
	    corpoEmail.append("<div class=\"rodape\">");
	    corpoEmail.append("<table width=\"600\" align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
	    corpoEmail.append("<tbody>");
	    corpoEmail.append("<tr>");
	    corpoEmail.append("<td width=\"360\" valign=\"bottom\"><img src=\"https://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/footer-1.jpg\"></td>");
	    corpoEmail
		    .append("<td><a href=\"http://www.orsegups.com.br\" target=\"_blank\"><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/footer-2.jpg\" width=\"260\" height=\"120\" alt=\"\"/></a></td>");
	    corpoEmail.append("</tr>");
	    corpoEmail.append("</tbody>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("</div>");
	    corpoEmail.append("</div>");
	    corpoEmail.append("</body>");
	    corpoEmail.append("</html>");

	    String[] addTo = new String[] { "gustavo.freitas@orsegups.com.br" };
	    String[] comCopia = new String[] { "emailautomatico@orsegups.com.br" };
	    String from = "fusion@orsegups.com.br";
	    String subject = "Relatório de Inconsistência de inserção de eventos de falha de comunicação";
	    String html = corpoEmail.toString();

	    OrsegupsUtils.sendEmail2Orsegups(addTo, from, subject, html, comCopia);
	}

    }

}
