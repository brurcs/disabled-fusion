package com.neomind.fusion.custom.orsegups.presenca.vo;

import java.util.GregorianCalendar;

public class HistoricoLocalVO
{
	private PostoVO posto;
	private ColaboradorVO colaborador;
	private GregorianCalendar data;
	
	public PostoVO getPosto()
	{
		return posto;
	}
	public void setPosto(PostoVO posto)
	{
		this.posto = posto;
	}
	public ColaboradorVO getColaborador()
	{
		return colaborador;
	}
	public void setColaborador(ColaboradorVO colaborador)
	{
		this.colaborador = colaborador;
	}
	public GregorianCalendar getData()
	{
		return data;
	}
	public void setData(GregorianCalendar data)
	{
		this.data = data;
	}
	
}
