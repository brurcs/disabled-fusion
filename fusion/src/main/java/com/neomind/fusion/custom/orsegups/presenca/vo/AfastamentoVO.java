package com.neomind.fusion.custom.orsegups.presenca.vo;

import java.util.GregorianCalendar;

public class AfastamentoVO
{
	private ColaboradorVO colaborador;
	private GregorianCalendar dataInicio;
	private GregorianCalendar dataFinal;
	private String situacao;
	private String observacao;
	private String link;
	private String excluir;
	
	public ColaboradorVO getColaborador()
	{
		return colaborador;
	}
	public void setColaborador(ColaboradorVO colaborador)
	{
		this.colaborador = colaborador;
	}
	public GregorianCalendar getDataInicio()
	{
		return dataInicio;
	}
	public void setDataInicio(GregorianCalendar dataInicio)
	{
		this.dataInicio = dataInicio;
	}
	public GregorianCalendar getDataFinal()
	{
		return dataFinal;
	}
	public void setDataFinal(GregorianCalendar dataFinal)
	{
		this.dataFinal = dataFinal;
	}
	public String getSituacao()
	{
		return situacao;
	}
	public void setSituacao(String situacao)
	{
		this.situacao = situacao;
	}
	public String getObservacao()
	{
		return observacao;
	}
	public void setObservacao(String observacao)
	{
		this.observacao = observacao;
	}
	public String getLink()
	{
		return link;
	}
	public void setLink(String link)
	{
		this.link = link;
	}
	public String getExcluir()
	{
		return excluir;
	}
	public void setExcluir(String excluir)
	{
		this.excluir = excluir;
	}
}
