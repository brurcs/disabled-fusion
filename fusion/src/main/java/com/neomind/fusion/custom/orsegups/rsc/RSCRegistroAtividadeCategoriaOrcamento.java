	package com.neomind.fusion.custom.orsegups.rsc;

import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.Days;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;

public class RSCRegistroAtividadeCategoriaOrcamento implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(RSCRegistroAtividadeCategoriaOrcamento.class);

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		String mensagem = "Por favor, contatar o administrador do sistema!";
		WorkflowException exception = null;
		try
		{
			RSCUtils rscUtils = new RSCUtils();
			GregorianCalendar prazoDeadLine = new GregorianCalendar();

			InstantiableEntityInfo registroAtividade = AdapterUtils.getInstantiableEntityInfo("RSCRegistroAtividade");
			NeoObject registro = registroAtividade.createNewInstance();
			EntityWrapper wRegistro = new EntityWrapper(registro);

			wRegistro.findField("responsavel").setValue(origin.getUser());
			wRegistro.findField("dataInicialRSC").setValue(origin.getStartDate());
			wRegistro.findField("dataFinalRSC").setValue(origin.getFinishDate());

			/* Histórico de Atividade do Fluxo C027.001 - RSC - Categoria Diversos */
			if (activity.getProcess().getModel().getName().equals("C027.002 - RSC - Categoria Orçamento"))
			{
				String des = (String) processEntity.findValue("observacao");
				if (origin.getActivityName().equalsIgnoreCase("Agendar Visita - Executivo"))
				{
					if (origin.getFinishByUser() == null)
					{
						prazoDeadLine = rscUtils.retornaPrazoDeadLine("Agendar Visita Escalada - Superior", (GregorianCalendar) processEntity.findValue("prazoAgendarVisita"));
						processEntity.findField("prazoAgendarVisitaEscalada").setValue(prazoDeadLine);

						des = "RSC não atendida pelo colaborador " + origin.getUser().getFullName();
						wRegistro.findField("descricao").setValue(des);
						wRegistro.findField("atividade").setValue("Agendar Visita - Executivo");
						wRegistro.findField("prazo").setValue(prazoDeadLine);

						processEntity.setValue("souPessoaResponsavel", true);
					}
					else
					{
						Boolean souPessoaResponsavel = (Boolean) processEntity.findValue("souPessoaResponsavel");
						if (souPessoaResponsavel)
						{
							GregorianCalendar dataVisita = (GregorianCalendar) processEntity.findValue("dataVisita");
							GregorianCalendar dataAtual = new GregorianCalendar();
							dataAtual.set(GregorianCalendar.HOUR_OF_DAY, 0);
							dataAtual.set(GregorianCalendar.MINUTE, 0);
							dataAtual.set(GregorianCalendar.SECOND, 0);
							dataAtual.set(GregorianCalendar.MILLISECOND, 0);
							System.out.println(NeoDateUtils.safeDateFormat(dataVisita, "dd/MM/yyyy HH:mm:ss"));
							System.out.println(NeoDateUtils.safeDateFormat(dataAtual, "dd/MM/yyyy HH:mm:ss"));
							if (dataVisita.before(dataAtual))
							{
								mensagem = "O campo Data da Visita não pode ser menor que a Data Atual!!";
								throw new WorkflowException(mensagem);
							}

							GregorianCalendar prazoMaximoVisita = rscUtils.addDiasPrazoRSC(new GregorianCalendar(), 7, true);
							if (dataVisita.after(prazoMaximoVisita))
							{
								mensagem = "O campo Data da Visita não pode ser maior do que 7 Dias Úteis!!";
								throw new WorkflowException(mensagem);
							}

							DateTime dtInicio = new DateTime(dataVisita);
							DateTime dtFim = new DateTime(new GregorianCalendar());
							int num = Days.daysBetween(dtInicio, dtFim).getDays();

							if (num >= 0 && num <= 2)
							{
								processEntity.setValue("aguardarInicioDaVisita", false);
								prazoDeadLine = rscUtils.addDiasPrazoRSC(dataVisita, -2, true);
								//System.out.println(NeoDateUtils.safeDateFormat(prazoDeadLine, "dd/MM/yyyy HH:mm:ss"));

								GregorianCalendar dataDeadLineVisitarCliente = rscUtils.addDiasPrazoRSC(prazoDeadLine, 3, true);
								processEntity.setValue("prazoVisitarCliente", dataDeadLineVisitarCliente);
								prazoDeadLine = dataDeadLineVisitarCliente;
								//System.out.println(NeoDateUtils.safeDateFormat(prazoDeadLine, "dd/MM/yyyy HH:mm:ss"));
							}
							else
							{
								prazoDeadLine = rscUtils.addDiasPrazoRSC(dataVisita, -2, true);

								processEntity.setValue("aguardarInicioDaVisita", true);
								processEntity.setValue("prazoAguardandoIniciarVisitaCliente", prazoDeadLine);
								//System.out.println(NeoDateUtils.safeDateFormat((GregorianCalendar) processEntity.findValue("prazoAguardandoIniciarVisitaCliente"), "dd/MM/yyyy HH:mm:ss"));

								GregorianCalendar dataDeadLineVisitarCliente = rscUtils.addDiasPrazoRSC((GregorianCalendar) processEntity.findValue("prazoAguardandoIniciarVisitaCliente"), 3, true);
								processEntity.setValue("prazoVisitarCliente", dataDeadLineVisitarCliente);
								//System.out.println(NeoDateUtils.safeDateFormat((GregorianCalendar) processEntity.findValue("prazoVisitarCliente"), "dd/MM/yyyy HH:mm:ss"));
							}

							des = des + " - Agendado a visita para o dia " + NeoDateUtils.safeDateFormat(dataVisita, "dd/MM/yyyy");									
							wRegistro.findField("atividade").setValue("Agendar Visita - Executivo");
							wRegistro.findField("descricao").setValue(des);
							wRegistro.findField("prazo").setValue(prazoDeadLine);
							
							System.out.println(NeoDateUtils.safeDateFormat((GregorianCalendar) wRegistro.findValue("prazo"), "dd/MM/yyyy HH:mm:ss"));
						}
						else
						{
							des = "Ajustado a tarefa para o colaborador " + processEntity.findValue("usuarioResponsavel.fullName") + " com a seguinte justificativa: " + processEntity.findValue("justifique");
							wRegistro.findField("atividade").setValue("Agendar Visita - Executivo");
							wRegistro.findField("descricao").setValue(des);
							wRegistro.findField("prazo").setValue((GregorianCalendar) processEntity.findValue("prazoAgendarVisita"));

							/* Limpa os campos */
							processEntity.setValue("usuarioResponsavel", null);
							processEntity.setValue("justifique", null);
						}
					}
				}
				else if (origin.getActivityName().equalsIgnoreCase("Aguardando - Iniciar Visita Cliente"))
				{
					wRegistro.findField("atividade").setValue("Aguardando - Iniciar Visita Cliente");
					wRegistro.findField("descricao").setValue(des);
					wRegistro.findField("prazo").setValue((GregorianCalendar) processEntity.findValue("dataVisita"));
				}
				else if (origin.getActivityName().equalsIgnoreCase("Visitar Cliente - Executivo"))
				{
					if (origin.getFinishByUser() == null)
					{
						prazoDeadLine = rscUtils.retornaPrazoDeadLine("Visitar Cliente Escalada - Superior", (GregorianCalendar) processEntity.findValue("prazoVisitarCliente"));
						processEntity.findField("prazoVisitarClienteEscalada").setValue(prazoDeadLine);

						des = "RSC não atendida pelo colaborador " + origin.getUser().getFullName();
						wRegistro.findField("descricao").setValue(des);
						wRegistro.findField("atividade").setValue("Visitar Cliente - Executivo");
						wRegistro.findField("prazo").setValue(prazoDeadLine);
					}
					else
					{
						if (!((Boolean) processEntity.findValue("fechouNegocio")) && ((Long) processEntity.findValue("motivoStatusNegocio.codigoMotivo")) == 5L)
						{
							if(((GregorianCalendar) processEntity.findValue("prazoRespostaOrcamento")) == null)
							{
								prazoDeadLine = rscUtils.retornaPrazoDeadLine("Aprovar Prazo - Resposta Orçamento", new GregorianCalendar());
								processEntity.setValue("prazoRespostaOrcamento", prazoDeadLine);
							}
							else
							{
								prazoDeadLine = (GregorianCalendar) processEntity.findValue("prazoRespostaOrcamento");
							}							
							des = des + " --> Solicitado prazo para a proposta em análise até o dia " + NeoDateUtils.safeDateFormat(((GregorianCalendar) processEntity.findValue("novoPrazoVisitarCliente")), "dd/MM/yyyy");    
						}
						else if(!((Boolean) processEntity.findValue("fechouNegocio")))
						{
							GregorianCalendar prazoTomarCienciaNegocioNaoFechado = rscUtils.retornaPrazoDeadLine("Tomar Ciência - Negócio Não Fechado", new GregorianCalendar());
							processEntity.findField("prazoTomarCienciaNegocioNaoFechado").setValue(prazoTomarCienciaNegocioNaoFechado);
						}

						wRegistro.findField("atividade").setValue("Visitar Cliente - Executivo");
						wRegistro.findField("descricao").setValue(des);
						wRegistro.findField("prazo").setValue(prazoDeadLine);
					}
				}
				else if (origin.getActivityName().equalsIgnoreCase("Agendar Visita Escalada - Superior"))
				{
					if (origin.getFinishByUser() == null)
					{
						des = "RSC não atendida pelo colaborador " + origin.getUser().getFullName();
						wRegistro.findField("descricao").setValue(des);
						wRegistro.findField("atividade").setValue("Agendar Visita Escalada - Superior");
						processEntity.setValue("souPessoaResponsavel", true);

						String papel = (String) processEntity.findValue("superiorResponsavelExecutor.name");
						if (papel.contains("Diretor") || papel.contains("Presidente"))
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Agendar Visita Escalada - Diretoria", (GregorianCalendar) processEntity.findValue("prazoAgendarVisitaEscalada"));
							processEntity.findField("prazoAgendarVisitaEscaladaDiretoria").setValue(prazoDeadLine);
							processEntity.findField("souPessoaResponsavel").setValue(false);
							processEntity.findField("continuarRSC").setValue(true);

							wRegistro.findField("prazo").setValue(prazoDeadLine);
							
							rscUtils.registrarTarefaSimples(processEntity, activity);
						}
						else
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Agendar Visita Escalada - Superior", (GregorianCalendar) processEntity.findValue("prazoAgendarVisitaEscalada"));
							processEntity.findField("prazoAgendarVisitaEscalada").setValue(prazoDeadLine);

							wRegistro.findField("prazo").setValue(prazoDeadLine);
						}
					}
					else
					{
						GregorianCalendar dataVisita = (GregorianCalendar) processEntity.findValue("dataVisita");
						GregorianCalendar dataAtual = new GregorianCalendar();
						dataAtual.set(GregorianCalendar.HOUR_OF_DAY, 0);
						dataAtual.set(GregorianCalendar.MINUTE, 0);
						dataAtual.set(GregorianCalendar.SECOND, 0);
						dataAtual.set(GregorianCalendar.MILLISECOND, 0);
						System.out.println(NeoDateUtils.safeDateFormat(dataVisita, "dd/MM/yyyy HH:mm:ss"));
						System.out.println(NeoDateUtils.safeDateFormat(dataAtual, "dd/MM/yyyy HH:mm:ss"));
						if (dataVisita.before(dataAtual))
						{
							mensagem = "O campo Data da Visita não pode ser menor que a Data Atual!!";
							throw new WorkflowException(mensagem);
						}

						GregorianCalendar prazoMaximoVisita = rscUtils.addDiasPrazoRSC(new GregorianCalendar(), 7, true);
						if (dataVisita.after(prazoMaximoVisita))
						{
							mensagem = "O campo Data da Visita não pode ser maior do que 7 Dias Úteis!!";
							throw new WorkflowException(mensagem);
						}

						DateTime dtInicio = new DateTime(dataVisita);
						DateTime dtFim = new DateTime(new GregorianCalendar());
						int num = Days.daysBetween(dtInicio, dtFim).getDays();

						if (num >= 0 && num <= 2)
						{
							processEntity.setValue("aguardarInicioDaVisita", false);
							prazoDeadLine = rscUtils.addDiasPrazoRSC(dataVisita, -2, true);
							//System.out.println(NeoDateUtils.safeDateFormat(prazoDeadLine, "dd/MM/yyyy HH:mm:ss"));

							GregorianCalendar dataDeadLineVisitarCliente = rscUtils.addDiasPrazoRSC(prazoDeadLine, 3, true);
							processEntity.setValue("prazoVisitarCliente", dataDeadLineVisitarCliente);
							prazoDeadLine = dataDeadLineVisitarCliente;
							System.out.println(NeoDateUtils.safeDateFormat(prazoDeadLine, "dd/MM/yyyy HH:mm:ss"));
						}
						else
						{
							prazoDeadLine = rscUtils.addDiasPrazoRSC(dataVisita, -2, true);

							processEntity.setValue("aguardarInicioDaVisita", true);
							processEntity.setValue("prazoAguardandoIniciarVisitaCliente", prazoDeadLine);
							//System.out.println(NeoDateUtils.safeDateFormat((GregorianCalendar) processEntity.findValue("prazoAguardandoIniciarVisitaCliente"), "dd/MM/yyyy HH:mm:ss"));

							GregorianCalendar dataDeadLineVisitarCliente = rscUtils.addDiasPrazoRSC((GregorianCalendar) processEntity.findValue("prazoAguardandoIniciarVisitaCliente"), 3, true);
							processEntity.setValue("prazoVisitarCliente", dataDeadLineVisitarCliente);
							//System.out.println(NeoDateUtils.safeDateFormat((GregorianCalendar) processEntity.findValue("prazoVisitarCliente"), "dd/MM/yyyy HH:mm:ss"));
						}
						
						des = des + " - Agendado a visita para o dia " + NeoDateUtils.safeDateFormat(dataVisita, "dd/MM/yyyy");									
						wRegistro.findField("atividade").setValue("Agendar Visita Escalada - Superior");
						wRegistro.findField("descricao").setValue(des);
						wRegistro.findField("prazo").setValue(prazoDeadLine);
					}
				}
				else if (origin.getActivityName().equalsIgnoreCase("Agendar Visita Escalada - Diretoria"))
				{
					if (origin.getFinishByUser() == null)
					{
						des = "RSC não atendida pelo colaborador " + origin.getUser().getFullName();
						wRegistro.findField("descricao").setValue(des);
						wRegistro.findField("atividade").setValue("Agendar Visita Escalada - Diretoria");

						prazoDeadLine = rscUtils.retornaPrazoDeadLine("Agendar Visita Escalada - Presidência", (GregorianCalendar) processEntity.findValue("prazoAgendarVisitaEscaladaDiretoria"));
						processEntity.findField("prazoAgendarVisitaEscaladaPresidencia").setValue(prazoDeadLine);

						wRegistro.findField("prazo").setValue(prazoDeadLine);

						processEntity.setValue("souPessoaResponsavel", true);
						
						rscUtils.registrarTarefaSimples(processEntity, activity);
					}
					else
					{
						GregorianCalendar dataVisita = (GregorianCalendar) processEntity.findValue("dataVisita");
						GregorianCalendar dataAtual = new GregorianCalendar();
						dataAtual.set(GregorianCalendar.HOUR_OF_DAY, 0);
						dataAtual.set(GregorianCalendar.MINUTE, 0);
						dataAtual.set(GregorianCalendar.SECOND, 0);
						dataAtual.set(GregorianCalendar.MILLISECOND, 0);
						System.out.println(NeoDateUtils.safeDateFormat(dataVisita, "dd/MM/yyyy HH:mm:ss"));
						System.out.println(NeoDateUtils.safeDateFormat(dataAtual, "dd/MM/yyyy HH:mm:ss"));
						if (dataVisita.before(dataAtual))
						{
							mensagem = "O campo Data da Visita não pode ser menor que a Data Atual!!";
							throw new WorkflowException(mensagem);
						}

						GregorianCalendar prazoMaximoVisita = rscUtils.addDiasPrazoRSC(new GregorianCalendar(), 7, true);
						if (dataVisita.after(prazoMaximoVisita))
						{
							mensagem = "O campo Data da Visita não pode ser maior do que 7 Dias Úteis!!";
							throw new WorkflowException(mensagem);
						}

						DateTime dtInicio = new DateTime(dataVisita);
						DateTime dtFim = new DateTime(new GregorianCalendar());
						int num = Days.daysBetween(dtInicio, dtFim).getDays();

						if (num >= 0 && num <= 2)
						{
							processEntity.setValue("aguardarInicioDaVisita", false);
							prazoDeadLine = rscUtils.addDiasPrazoRSC(dataVisita, -2, true);
							//System.out.println(NeoDateUtils.safeDateFormat(prazoDeadLine, "dd/MM/yyyy HH:mm:ss"));

							GregorianCalendar dataDeadLineVisitarCliente = rscUtils.addDiasPrazoRSC(prazoDeadLine, 3, true);
							processEntity.setValue("prazoVisitarCliente", dataDeadLineVisitarCliente);
							prazoDeadLine = dataDeadLineVisitarCliente;
							System.out.println(NeoDateUtils.safeDateFormat(prazoDeadLine, "dd/MM/yyyy HH:mm:ss"));
						}
						else
						{
							prazoDeadLine = rscUtils.addDiasPrazoRSC(dataVisita, -2, true);

							processEntity.setValue("aguardarInicioDaVisita", true);
							processEntity.setValue("prazoAguardandoIniciarVisitaCliente", prazoDeadLine);
							//System.out.println(NeoDateUtils.safeDateFormat((GregorianCalendar) processEntity.findValue("prazoAguardandoIniciarVisitaCliente"), "dd/MM/yyyy HH:mm:ss"));

							GregorianCalendar dataDeadLineVisitarCliente = rscUtils.addDiasPrazoRSC((GregorianCalendar) processEntity.findValue("prazoAguardandoIniciarVisitaCliente"), 3, true);
							processEntity.setValue("prazoVisitarCliente", dataDeadLineVisitarCliente);
							//System.out.println(NeoDateUtils.safeDateFormat((GregorianCalendar) processEntity.findValue("prazoVisitarCliente"), "dd/MM/yyyy HH:mm:ss"));
						}
						
						des = des + " - Agendado a visita para o dia " + NeoDateUtils.safeDateFormat(dataVisita, "dd/MM/yyyy");									
						wRegistro.findField("atividade").setValue("Agendar Visita Escalada - Diretoria");
						wRegistro.findField("descricao").setValue(des);
						wRegistro.findField("prazo").setValue(prazoDeadLine);
					}
				}
				else if (origin.getActivityName().equalsIgnoreCase("Agendar Visita Escalada - Presidência"))
				{
					GregorianCalendar dataVisita = (GregorianCalendar) processEntity.findValue("dataVisita");
					GregorianCalendar dataAtual = new GregorianCalendar();
					dataAtual.set(GregorianCalendar.HOUR_OF_DAY, 0);
					dataAtual.set(GregorianCalendar.MINUTE, 0);
					dataAtual.set(GregorianCalendar.SECOND, 0);
					dataAtual.set(GregorianCalendar.MILLISECOND, 0);
					System.out.println(NeoDateUtils.safeDateFormat(dataVisita, "dd/MM/yyyy HH:mm:ss"));
					System.out.println(NeoDateUtils.safeDateFormat(dataAtual, "dd/MM/yyyy HH:mm:ss"));
					if (dataVisita.before(dataAtual))
					{
						mensagem = "O campo Data da Visita não pode ser menor que a Data Atual!!";
						throw new WorkflowException(mensagem);
					}

					GregorianCalendar prazoMaximoVisita = rscUtils.addDiasPrazoRSC(new GregorianCalendar(), 7, true);
					if (dataVisita.after(prazoMaximoVisita))
					{
						mensagem = "O campo Data da Visita não pode ser maior do que 7 Dias Úteis!!";
						throw new WorkflowException(mensagem);
					}

					DateTime dtInicio = new DateTime(dataVisita);
					DateTime dtFim = new DateTime(new GregorianCalendar());
					int num = Days.daysBetween(dtInicio, dtFim).getDays();

					if (num >= 0 && num <= 2)
					{
						processEntity.setValue("aguardarInicioDaVisita", false);
						prazoDeadLine = rscUtils.addDiasPrazoRSC(dataVisita, -2, true);
						//System.out.println(NeoDateUtils.safeDateFormat(prazoDeadLine, "dd/MM/yyyy HH:mm:ss"));

						GregorianCalendar dataDeadLineVisitarCliente = rscUtils.addDiasPrazoRSC(prazoDeadLine, 3, true);
						
						dataDeadLineVisitarCliente = rscUtils.retornaPrazoDeadLine("Visitar Cliente - Executivo", new GregorianCalendar());
						processEntity.setValue("prazoVisitarCliente", dataDeadLineVisitarCliente);
						
						processEntity.setValue("prazoVisitarCliente", dataDeadLineVisitarCliente);
						prazoDeadLine = dataDeadLineVisitarCliente;
						System.out.println(NeoDateUtils.safeDateFormat(prazoDeadLine, "dd/MM/yyyy HH:mm:ss"));
					}
					else
					{
						prazoDeadLine = rscUtils.addDiasPrazoRSC(dataVisita, -2, true);

						processEntity.setValue("aguardarInicioDaVisita", true);
						processEntity.setValue("prazoAguardandoIniciarVisitaCliente", prazoDeadLine);
						//System.out.println(NeoDateUtils.safeDateFormat((GregorianCalendar) processEntity.findValue("prazoAguardandoIniciarVisitaCliente"), "dd/MM/yyyy HH:mm:ss"));

						GregorianCalendar dataDeadLineVisitarCliente = rscUtils.addDiasPrazoRSC((GregorianCalendar) processEntity.findValue("prazoAguardandoIniciarVisitaCliente"), 3, true);
						processEntity.setValue("prazoVisitarCliente", dataDeadLineVisitarCliente);
						//System.out.println(NeoDateUtils.safeDateFormat((GregorianCalendar) processEntity.findValue("prazoVisitarCliente"), "dd/MM/yyyy HH:mm:ss"));
					}
					
					des = des + " - Agendado a visita para o dia " + NeoDateUtils.safeDateFormat(dataVisita, "dd/MM/yyyy");									
					wRegistro.findField("atividade").setValue("Agendar Visita Escalada - Presidência");
					wRegistro.findField("descricao").setValue(des);
					wRegistro.findField("prazo").setValue(prazoDeadLine);
				}
				else if (origin.getActivityName().equalsIgnoreCase("Visitar Cliente Escalada - Superior"))
				{
					if (origin.getFinishByUser() == null)
					{
						des = "RSC não atendida pelo colaborador " + origin.getUser().getFullName();
						wRegistro.findField("descricao").setValue(des);
						wRegistro.findField("atividade").setValue("Visitar Cliente Escalada - Superior");

						String papel = (String) processEntity.findValue("superiorResponsavelExecutorVisitar.name");
						if (papel.contains("Diretor") || papel.contains("Presidente"))
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Visitar Cliente Escalada - Diretoria", (GregorianCalendar) processEntity.findValue("prazoVisitarClienteEscalada"));
							System.out.println(NeoDateUtils.safeDateFormat(prazoDeadLine, "dd/MM/yyyy HH:mm:ss"));
							processEntity.findField("prazoVisitarClienteEscaladaDiretoria").setValue(prazoDeadLine);

							wRegistro.findField("prazo").setValue(prazoDeadLine);
							
							rscUtils.registrarTarefaSimples(processEntity, activity);
						}
						else
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Visitar Cliente Escalada - Superior", (GregorianCalendar) processEntity.findValue("prazoVisitarClienteEscalada"));
							processEntity.findField("prazoVisitarClienteEscalada").setValue(prazoDeadLine);
							System.out.println(NeoDateUtils.safeDateFormat((GregorianCalendar) processEntity.findValue("prazoVisitarClienteEscalada"), "dd/MM/yyyy HH:mm:ss"));

							wRegistro.findField("prazo").setValue(prazoDeadLine);
						}
					}
					else
					{
						des = (String) processEntity.findValue("observacao");

						wRegistro.findField("atividade").setValue("Visitar Cliente Escalada - Superior");
						wRegistro.findField("descricao").setValue(des);
						
						prazoDeadLine = rscUtils.retornaPrazoTarefaEficacia(processEntity);
						wRegistro.findField("prazo").setValue(prazoDeadLine);
					}
				}
				else if (origin.getActivityName().equalsIgnoreCase("Visitar Cliente Escalada - Diretoria"))
				{
					if (origin.getFinishByUser() == null)
					{
						des = "RSC não atendida pelo colaborador " + origin.getUser().getFullName();
						wRegistro.findField("descricao").setValue(des);
						wRegistro.findField("atividade").setValue("Visitar Cliente Escalada - Diretoria");

						prazoDeadLine = rscUtils.retornaPrazoDeadLine("Visitar Cliente Escalada - Presidência", (GregorianCalendar) processEntity.findValue("prazoVisitarClienteEscaladaDiretoria"));
						processEntity.findField("prazoVisitarClienteEscaladaPresidencia").setValue(prazoDeadLine);

						wRegistro.findField("prazo").setValue(prazoDeadLine);
						
						rscUtils.registrarTarefaSimples(processEntity, activity);
					}
					else
					{
						des = (String) processEntity.findValue("observacao");

						wRegistro.findField("atividade").setValue("Visitar Cliente Escalada - Diretoria");
						wRegistro.findField("descricao").setValue(des);
						
						prazoDeadLine = rscUtils.retornaPrazoTarefaEficacia(processEntity);
						wRegistro.findField("prazo").setValue(prazoDeadLine);
					}
				}
				else if (origin.getActivityName().equalsIgnoreCase("Visitar Cliente Escalada - Presidência"))
				{
					des = (String) processEntity.findValue("observacao");

					wRegistro.findField("atividade").setValue("Visitar Cliente Escalada - Presidência");
					wRegistro.findField("descricao").setValue(des);
					
					prazoDeadLine = rscUtils.retornaPrazoTarefaEficacia(processEntity);
					wRegistro.findField("prazo").setValue(prazoDeadLine);
				}
				else if (origin.getActivityName().equalsIgnoreCase("Aprovar Prazo - Resposta Orçamento"))
				{
					if (origin.getFinishByUser() == null)
					{
						des = "RSC de Orçamento excedeu o prazo limite de 90 dias entre Superintendência e Executivo, para que seja informado se o negócio foi fechado, pelo colaborador  " + origin.getUser().getFullName();
						wRegistro.findField("descricao").setValue(des);
						wRegistro.findField("atividade").setValue("Aprovar Prazo - Resposta Orçamento");

						prazoDeadLine = rscUtils.retornaPrazoDeadLine("Aprovar Prazo - Resposta Orçamento Diretoria", (GregorianCalendar) processEntity.findValue("prazoRespostaOrcamento"));
						processEntity.findField("prazoRespostaOrcamentoDiretoria").setValue(prazoDeadLine);

						wRegistro.findField("prazo").setValue(prazoDeadLine);
					}
					else
					{
						GregorianCalendar novoPrazoVisitarCliente = (GregorianCalendar) processEntity.findValue("novoPrazoVisitarCliente");

						if ((novoPrazoVisitarCliente.before(new GregorianCalendar())) || (novoPrazoVisitarCliente.equals(new GregorianCalendar())))
						{
							mensagem = "O campo Prazo para Proposta em Análise não pode ser Menor ou Igual a Data Atual!!";
							throw new WorkflowException(mensagem);
						}

						processEntity.setValue("prazoVisitarCliente", rscUtils.atualizaHoraParaFinalDoDia(novoPrazoVisitarCliente));
						
						des = des + " --> O prazo foi ajustado até o dia " + NeoDateUtils.safeDateFormat(((GregorianCalendar) processEntity.findValue("novoPrazoVisitarCliente")),"dd/MM/yyyy");
						wRegistro.findField("descricao").setValue(des);
						wRegistro.findField("atividade").setValue("Aprovar Prazo - Resposta Orçamento");
						wRegistro.findField("prazo").setValue(novoPrazoVisitarCliente);
						
						processEntity.setValue("novoPrazoVisitarCliente", null);
						
					}
				}
				else if (origin.getActivityName().equalsIgnoreCase("Aprovar Prazo - Resposta Orçamento Diretoria"))
				{
					GregorianCalendar novoPrazoRespostaOrcamento = (GregorianCalendar) processEntity.findValue("novoPrazoRespostaOrcamento");
					GregorianCalendar novoPrazoVisitarCliente = (GregorianCalendar) processEntity.findValue("novoPrazoVisitarCliente");

					processEntity.setValue("prazoRespostaOrcamento", rscUtils.atualizaHoraParaFinalDoDia(novoPrazoRespostaOrcamento));
					processEntity.setValue("prazoVisitarCliente", rscUtils.atualizaHoraParaFinalDoDia(novoPrazoVisitarCliente));
					
					des = (String) processEntity.findValue("observacao");
					
					wRegistro.findField("atividade").setValue("Aprovar Prazo - Resposta Orçamento Diretoria");
					wRegistro.findField("descricao").setValue(des);
					wRegistro.findField("prazo").setValue(novoPrazoVisitarCliente);
				}
				else if (origin.getActivityName().equalsIgnoreCase("Aprovar - Negócio Não Fechado"))
				{
					GregorianCalendar novoPrazoVisitarCliente = (GregorianCalendar) processEntity.findValue("prazoVisitarCliente");
					boolean aprovarNegocioNaoFechado = (boolean) processEntity.findGenericValue("aprovarNegocioNaoFechado");
					
					if(!aprovarNegocioNaoFechado){											    
					    if ((novoPrazoVisitarCliente.before(new GregorianCalendar())) || (novoPrazoVisitarCliente.equals(new GregorianCalendar()))){
					    	mensagem = "O campo Prazo para Proposta em Análise não pode ser Menor ou Igual a Data Atual!!";
					    	throw new WorkflowException(mensagem);
					    }
					    else{
					    	processEntity.findField("prazoAgendarVisita").setValue(novoPrazoVisitarCliente);
					    }
					}
				    
				    wRegistro.findField("atividade").setValue("Aprovar - Negócio Não Fechado");	
				    wRegistro.findField("prazo").setValue(processEntity.findValue("prazoVisitarCliente"));
				    
				    des = (String) processEntity.findValue("observacao");						
				    wRegistro.findField("descricao").setValue(des);				    
				    wRegistro.findField("responsavel").setValue(processEntity.findValue("responsavelValidarPrazoRespostaOrcamento"));
						
				}
				else if (origin.getActivityName().equalsIgnoreCase("Tomar Ciência - Negócio Não Fechado"))
				{				    
				    wRegistro.findField("atividade").setValue("Tomar Ciência - Negócio Não Fechado");
				    wRegistro.findField("prazo").setValue(new GregorianCalendar());

					if (origin.getFinishByUser() == null)
					{				
						wRegistro.findField("descricao").setValue("Registro inserido automaticamente. Avançado automaticamente pelo sistema.");						
					}
					else
					{	
						wRegistro.findField("descricao").setValue("Registro inserido automaticamente. Avançado pelo Gestor Comercial.");	
					}										
				}
			}

			processEntity.setValue("observacao", null);
			
			NeoObject rscRegistroSolicitacaoCliente = (NeoObject) processEntity.findValue("RscRelatorioSolicitacaoCliente");
			EntityWrapper rscWrapper = new EntityWrapper(rscRegistroSolicitacaoCliente);

			PersistEngine.persist(registro);
			rscWrapper.findField("registroAtividades").addValue(registro);
		}
		catch (Exception e)
		{
			log.error(mensagem);
			e.printStackTrace();
			throw new WorkflowException(mensagem);
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}

}
