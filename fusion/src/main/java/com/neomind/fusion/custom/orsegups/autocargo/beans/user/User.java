package com.neomind.fusion.custom.orsegups.autocargo.beans.user;

public class User {

    private String username; 
    private String name; 
    private String email; 
    private String role; 
    private Long client_id; 
    private String client_name; 
    private Long sign_in_count; 
    private String sign_in_at;
    
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }
    public Long getClient_id() {
        return client_id;
    }
    public void setClient_id(Long client_id) {
        this.client_id = client_id;
    }
    public String getClient_name() {
        return client_name;
    }
    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }
    public Long getSign_in_count() {
        return sign_in_count;
    }
    public void setSign_in_count(Long sign_in_count) {
        this.sign_in_count = sign_in_count;
    }
    /**
     * Formato da data dd-MM-yyyy HH:mm:ss
     * @return String Data do ultimo login
     */
    public String getSign_in_at() {
        return sign_in_at;
    }
    public void setSign_in_at(String sign_in_at) {
        this.sign_in_at = sign_in_at;
    }
    
    
    
}
