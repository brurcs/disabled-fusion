package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.common.NeoRunnable;
import com.neomind.fusion.custom.orsegups.presenca.vo.ColaboradorVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.JustificativaFaltaVO;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.QLPresencaSupervisaoUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

//com.neomind.fusion.custom.orsegups.scheduler.AbreTarefaSimplesAfastamentoJustificativa
public class AbreTarefaSimplesAfastamentoJustificativa implements CustomJobAdapter
{
	@SuppressWarnings("unused")
	private static final Log log = LogFactory.getLog(AbreTarefaSimplesAfastamentoJustificativa.class);
	
	public StringBuilder logEmail;
	protected JustificativaFaltaVO justificativaFaltaVO;

	
	int contJFN;
	int contJF;
	int contJDS;
	int contJDC;

	int contJFN_ERR;
	int contJF_ERR;
	int contJDS_ERR;
	int contJDC_ERR;
	
	static String cidadesExecao = "";
	

	@Override
	public void execute(CustomJobContext arg0)
	{
		cidadesExecao = recuperCidadesRegra();
		
		contJFN = 0;
		contJF = 0;
		contJDS = 0;
		contJDC = 0;

		contJFN_ERR = 0;
		contJF_ERR = 0;
		contJDS_ERR = 0;
		contJDC_ERR = 0;

		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		boolean houveErro = false;
		
		Connection conn = PersistEngine.getConnection("VETORH");
		StringBuilder sqlFaltasNaoJustificadas = new StringBuilder();
		StringBuilder sqlFaltasJustificadas = new StringBuilder();
		StringBuilder sqlDemissaoSemJustaCausa = new StringBuilder();
		StringBuilder sqlDemissaoComJustaCausa = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		//final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.AbreTarefaSimplesAfastamentoJustificativa");
		System.out.println("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Simples Afastamento Justificativa - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		
		logEmail = new StringBuilder();
		
		logEmail.append("<h1>Tarefas de Justificativa de falta.</h1><br>");
		
		
		
//		logEmail.append("<br>[Faltas nao Justificadas]");
//		try
//		{
//			//Faltas nÃ£o justificadas	
//			sqlFaltasNaoJustificadas.append(" DECLARE @DatRef  Datetime "); 
//			sqlFaltasNaoJustificadas.append(" SELECT @DatRef = DATEADD(dd, 0, DATEDIFF(dd, 0, getdate()))  ");
//			//sqlFaltasNaoJustificadas.append(" SELECT @DatRef = '2015-06-29' ");
//			sqlFaltasNaoJustificadas.append(" SELECT fun.NumEmp, fun.TipCol, fun.NumCad, fun.NumCpf, afa.datafa, afa.horafa, fun.NomFun,orn.USU_CodReg, car.TitRed, orn.USU_LotOrn, orn.NomLoc, afa.DatAfa, afa.DatTer, afa.USU_AfaInf, fis.USU_NomFis, afa.SitAfa, afa.CauDem, orn.usu_codccu, orn.usu_codcid ");
//			sqlFaltasNaoJustificadas.append(" FROM R038AFA afa WITH (NOLOCK) ");
//			sqlFaltasNaoJustificadas.append(" INNER JOIN R034FUN fun WITH (NOLOCK) ON fun.NumEmp = afa.NumEmp AND fun.TipCol = afa.TipCol AND fun.NumCad = afa.NumCad ");
//			sqlFaltasNaoJustificadas.append(" INNER JOIN R024CAR car WITH (NOLOCK) ON car.EstCar = fun.EstCar AND car.CodCar = fun.CodCar ");
//			sqlFaltasNaoJustificadas.append(" INNER JOIN R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp AND hlo.TipCol = fun.TipCol AND hlo.NumCad = fun.NumCad AND hlo.DatAlt = (SELECT MAX (DATALT) FROM R038HLO TABELA001 WHERE TABELA001.NUMEMP = hlo.NUMEMP AND TABELA001.TIPCOL = hlo.TIPCOL AND TABELA001.NUMCAD = hlo.NUMCAD AND TABELA001.DATALT <=  afa.DatAfa) ");
//			sqlFaltasNaoJustificadas.append(" INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.TabOrg = hlo.TabOrg AND orn.NumLoc = hlo.NumLoc ");
//			sqlFaltasNaoJustificadas.append(" INNER JOIN USU_TFisRes fis ON fis.USU_FisRes = fun.usu_fisres ");
//			sqlFaltasNaoJustificadas.append(" WHERE afa.SitAfa = 15  ");
//			sqlFaltasNaoJustificadas.append(" AND NOT EXISTS (SELECT * FROM [CACUPE\\SQL02].fusion_producao.DBO.d_TarefaAfastamentoJustificativa aj WITH (NOLOCK) ");
//			sqlFaltasNaoJustificadas.append(" 			  WHERE aj.numcpf = fun.numcpf AND CAST(floor(CAST(aj.datafa AS FLOAT)) AS datetime) = CAST(floor(CAST(afa.datafa AS FLOAT)) AS datetime) AND aj.horafa = afa.horafa  ) "); // AND isnull(aj.SitAfa, afa.SitAfa) = afa.SitAfa   removido para nÃ£o gerar volume de tarefas
//			
//			sqlFaltasNaoJustificadas.append(" AND afa.DatTer <= @DatRef-1 ");
//			
//			sqlFaltasNaoJustificadas.append(" AND orn.USU_CodReg IN (0,1,2,3,4,5,6,7,9,10,11,12,13,14,15,16)  AND afa.DatTer >= getdate()-40 ");
//			sqlFaltasNaoJustificadas.append(" AND orn.nomloc NOT IN ('Afastados Sede AdministraÃ§Ã£o','TransferÃªncia Sede AdministraÃ§Ã£o','Aviso PrÃ©vio Sede AdministraÃ§Ã£o','SESMT') ");
//
//			pstm = conn.prepareStatement(sqlFaltasNaoJustificadas.toString());
//			rs = pstm.executeQuery();
//
//			while (rs.next())
//			{
//				justificativaFaltaVO = new JustificativaFaltaVO(rs.getString("NumEmp"), rs.getString("NumCad"), rs.getString("NomFun"), rs.getLong("NumCpf"), rs.getLong("TipCol") , rs.getString("USU_AfaInf"),                                             
//				    			   rs.getTimestamp("DatAfa"), rs.getLong("HorAfa"), rs.getTimestamp("DatTer"), rs.getLong("SitAfa"), rs.getLong("CauDem"), rs.getLong("USU_CodReg"), rs.getString("NomLoc"), rs.getString("USU_LotOrn"),  
//				    			   rs.getString("TitRed"), rs.getString("USU_NomFis"),"", rs.getLong("usu_codccu"), rs.getLong("usu_codcid"));
//				System.out.println("Falta nÃ£o justificada " + justificativaFaltaVO);
//				final NeoRunnable work = new NeoRunnable(){
//				       public void run() throws Exception{
//				    	   abreTarefaFaltaNaoJustificada(justificativaFaltaVO.getNumemp(),justificativaFaltaVO.getNumcad(), justificativaFaltaVO.getNomFun(), justificativaFaltaVO.getNumcpf(), justificativaFaltaVO.getTipcol(), justificativaFaltaVO.getAfainf(), 
//				    			   justificativaFaltaVO.getDatafa(), justificativaFaltaVO.getHorafa(), justificativaFaltaVO.getDatter(), justificativaFaltaVO.getSitafa(), justificativaFaltaVO.getCaudem(), justificativaFaltaVO.getCodreg(), justificativaFaltaVO.getNomloc(), justificativaFaltaVO.getLotorn(),
//				    			   justificativaFaltaVO.getTitred(), justificativaFaltaVO.getNomfis(), justificativaFaltaVO.getUsuCodccu(), justificativaFaltaVO.getUsuCodCid()
//		    			   );
//				             
//				       }
//				};
//				try{
//					contJFN++;
//					PersistEngine.managedRun(work);
//				}
//				catch (final Exception e){
//					System.out.println(e.getMessage() + e);
//					e.printStackTrace();
//					logEmail.append("<BR>Erro ao processar managedrun => [Falta nÃ£o justificada] " + justificativaFaltaVO);
//					throw new Exception("Erro ao fazer managedRun");
//				}
//
//				
//			}
//		}catch (Exception e)
//		{
//			logEmail.append( "Erro geral ao processar as aberturas: " + e.getMessage() );
//			System.out.println("##### AGENDADOR DE TAREFA: Erro ao Abrir Tarefa Simples Afastamento Justificativa - Regra: Falta nÃ£o justificada ["+key+"]");
//			e.printStackTrace();
//			logEmail.append("<b>Erro</b>");
//			houveErro =true;
//			
//		}
//		
//		logEmail.append("<br>[Faltas Justificadas]");
//		try
//		{
//			//Faltas justificadas	
//			sqlFaltasJustificadas.append(" DECLARE @DatRef  Datetime "); 
//			sqlFaltasJustificadas.append(" SELECT @DatRef = DATEADD(dd, 0, DATEDIFF(dd, 0, getdate()))  ");
//			//sqlFaltasJustificadas.append(" SELECT @DatRef = '2015-06-29' ");
//			sqlFaltasJustificadas.append(" SELECT fun.NumEmp, fun.TipCol, fun.NumCad, fun.NumCpf, afa.datafa, afa.horafa, fun.NomFun, orn.USU_CodReg, car.TitRed, orn.USU_LotOrn, orn.NomLoc, sit.DesSit, afa.DatAfa, afa.DatTer, fis.USU_NomFis, afa.SitAfa, afa.CauDem, orn.usu_codccu, orn.usu_codcid  ");
//			sqlFaltasJustificadas.append(" FROM R038AFA afa WITH (NOLOCK) ");
//			sqlFaltasJustificadas.append(" INNER JOIN R034FUN fun WITH (NOLOCK) ON fun.NumEmp = afa.NumEmp AND fun.TipCol = afa.TipCol AND fun.NumCad = afa.NumCad ");
//			sqlFaltasJustificadas.append(" INNER JOIN R024CAR car WITH (NOLOCK) ON car.EstCar = fun.EstCar AND car.CodCar = fun.CodCar ");
//			sqlFaltasJustificadas.append(" INNER JOIN R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp AND hlo.TipCol = fun.TipCol AND hlo.NumCad = fun.NumCad AND hlo.DatAlt = (SELECT MAX (DATALT) FROM R038HLO TABELA001 WHERE TABELA001.NUMEMP = hlo.NUMEMP AND TABELA001.TIPCOL = hlo.TIPCOL AND TABELA001.NUMCAD = hlo.NUMCAD AND TABELA001.DATALT <=  afa.DatAfa) ");
//			sqlFaltasJustificadas.append(" INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.TabOrg = hlo.TabOrg AND orn.NumLoc = hlo.NumLoc ");
//			sqlFaltasJustificadas.append(" INNER JOIN R010SIT sit WITH (NOLOCK) ON sit.CodSit = afa.SitAfa ");
//			sqlFaltasJustificadas.append(" INNER JOIN USU_TFisRes fis ON fis.USU_FisRes = fun.usu_fisres ");
//			sqlFaltasJustificadas.append(" WHERE afa.SitAfa NOT IN (2, 7, 16, 20, 68, 500) ");
//			sqlFaltasJustificadas.append(" and NOT EXISTS (SELECT * FROM [CACUPE\\SQL02].fusion_producao.DBO.d_TarefaAfastamentoJustificativa aj WITH (NOLOCK) ");
//			sqlFaltasJustificadas.append(" 			   WHERE aj.numcpf = fun.numcpf AND CAST(floor(CAST(aj.datafa AS FLOAT)) AS datetime) = CAST(floor(CAST(afa.datafa AS FLOAT)) AS datetime) AND aj.horafa = afa.horafa  )  "); // AND isnull(aj.SitAfa, afa.SitAfa) = afa.SitAfa removido para nÃ£o gerar volume de tarefas
//
//			sqlFaltasJustificadas.append(" AND afa.DatTer <= @DatRef-1  ");
//			
//			
//			sqlFaltasJustificadas.append(" AND afa.SitAfa not in (3,4,6,13,113,117) ");
//			sqlFaltasJustificadas.append(" AND orn.USU_CodReg IN (0,1,2,3,4,5,6,7,9,10,11,12,13,14,15,16)  AND afa.DatTer >= getdate()-30   ");
//			sqlFaltasJustificadas.append(" AND orn.nomloc NOT IN ('Afastados Sede AdministraÃ§Ã£o','TransferÃªncia Sede AdministraÃ§Ã£o','Aviso PrÃ©vio Sede AdministraÃ§Ã£o','SESMT') ");
//
//			pstm = conn.prepareStatement(sqlFaltasJustificadas.toString());
//			rs = pstm.executeQuery();
//
//			while (rs.next())
//			{
//				justificativaFaltaVO = new JustificativaFaltaVO(rs.getString("NumEmp"), rs.getString("NumCad"), rs.getString("NomFun"), rs.getLong("NumCpf"), rs.getLong("TipCol") , /*rs.getString("USU_AfaInf"),*/ "",                                             
//		    			   rs.getTimestamp("DatAfa"), rs.getLong("HorAfa"), rs.getTimestamp("DatTer"), rs.getLong("SitAfa"), rs.getLong("CauDem"), rs.getLong("USU_CodReg"), rs.getString("NomLoc"), rs.getString("USU_LotOrn"),  
//		    			   rs.getString("TitRed"), rs.getString("USU_NomFis"), rs.getString("DesSit"), rs.getLong("usu_codccu"), rs.getLong("usu_codcid") );
//				System.out.println("Falta justificada " + justificativaFaltaVO);
//				final NeoRunnable work = new NeoRunnable(){
//				       public void run() throws Exception{
//				    	    abreTarefaFaltaJustificada(justificativaFaltaVO.getNumemp(),justificativaFaltaVO.getNumcad(), justificativaFaltaVO.getNomFun(), justificativaFaltaVO.getNumcpf(), justificativaFaltaVO.getTipcol(), justificativaFaltaVO.getAfainf(), 
//				    			   justificativaFaltaVO.getDatafa(), justificativaFaltaVO.getHorafa(), justificativaFaltaVO.getDatter(), justificativaFaltaVO.getSitafa(), justificativaFaltaVO.getCaudem(), justificativaFaltaVO.getCodreg(), justificativaFaltaVO.getNomloc(), justificativaFaltaVO.getLotorn(),
//				    			   justificativaFaltaVO.getTitred(), justificativaFaltaVO.getNomfis(), justificativaFaltaVO.getDessit(), justificativaFaltaVO.getUsuCodccu(), justificativaFaltaVO.getUsuCodCid()
//		    			   ); 
//				             
//				       }
//				};
//				try{
//					contJF++;
//					PersistEngine.managedRun(work);
//				}
//				catch (final Exception e){
//					System.out.println(e.getMessage() + e);
//					e.printStackTrace();
//					logEmail.append("<BR>Erro ao processar managedrun => [Falta justificada] " + justificativaFaltaVO);
//					throw new Exception("Erro ao fazer managedRun");
//				}
//				
//			}
//		}catch (Exception e)
//		{
//			System.out.println("##### AGENDADOR DE TAREFA: Erro ao Abrir Tarefa Simples Afastamento Justificativa - Enviar justificativa de ausÃªncia ["+key+"]");
//			e.printStackTrace();
//			logEmail.append("<b>Erro</b>");
//			houveErro = true;
//		}
		
		
		logEmail.append("<br>[Desligamento sem justa causa]");
		try
		{
		
			//"Justificar desligamento sem justa causa	
			sqlDemissaoSemJustaCausa.append(" DECLARE @DatRef  Datetime "); 
			sqlDemissaoSemJustaCausa.append(" SELECT @DatRef = DATEADD(dd, 0, DATEDIFF(dd, 0, getdate()))  ");
			//sqlDemissaoSemJustaCausa.append(" SELECT @DatRef = '2015-06-29' ");
			sqlDemissaoSemJustaCausa.append(" SELECT fun.NumEmp, fun.TipCol, fun.NumCad, fun.NumCpf, afa.datafa, afa.horafa, fun.NomFun, orn.USU_CodReg, car.TitRed, orn.USU_LotOrn, orn.NomLoc, afa.DatAfa, fis.USU_NomFis, afa.SitAfa, afa.CauDem, orn.usu_codccu, orn.usu_codcid ");
			sqlDemissaoSemJustaCausa.append(" FROM R038AFA afa WITH (NOLOCK) ");
			sqlDemissaoSemJustaCausa.append(" INNER JOIN R034FUN fun WITH (NOLOCK) ON fun.NumEmp = afa.NumEmp AND fun.TipCol = afa.TipCol AND fun.NumCad = afa.NumCad ");
			sqlDemissaoSemJustaCausa.append(" INNER JOIN R024CAR car WITH (NOLOCK) ON car.EstCar = fun.EstCar AND car.CodCar = fun.CodCar ");
			sqlDemissaoSemJustaCausa.append(" INNER JOIN R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp AND hlo.TipCol = fun.TipCol AND hlo.NumCad = fun.NumCad AND hlo.DatAlt = (SELECT MAX (DATALT) FROM R038HLO TABELA001 WHERE TABELA001.NUMEMP = hlo.NUMEMP AND TABELA001.TIPCOL = hlo.TIPCOL AND TABELA001.NUMCAD = hlo.NUMCAD AND TABELA001.DATALT <=  afa.DatAfa) ");
			sqlDemissaoSemJustaCausa.append(" INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.TabOrg = hlo.TabOrg AND orn.NumLoc = hlo.NumLoc ");
			sqlDemissaoSemJustaCausa.append(" INNER JOIN USU_TFisRes fis ON fis.USU_FisRes = fun.usu_fisres ");
			sqlDemissaoSemJustaCausa.append(" WHERE afa.SitAfa = 7 AND afa.CauDem IN (2, 13) and ");
			sqlDemissaoSemJustaCausa.append(" not exists (select * FROM [CACUPE\\SQL02].fusion_producao.DBO.d_TarefaAfastamentoJustificativa aj WITH (NOLOCK) ");
			sqlDemissaoSemJustaCausa.append(" where aj.numcpf = fun.numcpf and cast(floor(cast(aj.datafa as float)) as datetime) = cast(floor(cast(afa.datafa as float)) as datetime) and aj.horafa = afa.horafa AND isnull(aj.SitAfa, afa.SitAfa) = afa.SitAfa) ");
			sqlDemissaoSemJustaCausa.append(" AND afa.DatAfa <= @DatRef-1  AND afa.DatAfa >= '2017-12-11' ");
			sqlDemissaoSemJustaCausa.append(" AND orn.USU_CodReg in (0,1,2,3,4,5,6,7,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23) AND ((fun.numemp in (27,28,29) AND fun.datadm >= '2017-08-01') or (fun.numemp not in (27,28,29) AND afa.DatAfa >= @DatRef-120)) ");

			pstm = conn.prepareStatement(sqlDemissaoSemJustaCausa.toString());

			rs = pstm.executeQuery();

			while (rs.next())
			{
				
				justificativaFaltaVO = new JustificativaFaltaVO(rs.getString("NumEmp"), rs.getString("NumCad"), rs.getString("NomFun"), rs.getLong("NumCpf"), rs.getLong("TipCol") , /*rs.getString("USU_AfaInf"),*/ "",                                             
		    			   rs.getTimestamp("DatAfa"), rs.getLong("HorAfa"), /* rs.getTimestamp("DatTer") */(Timestamp) null, rs.getLong("SitAfa"), rs.getLong("CauDem"), rs.getLong("USU_CodReg"), rs.getString("NomLoc"), rs.getString("USU_LotOrn"),  
		    			   rs.getString("TitRed"), rs.getString("USU_NomFis"),"", rs.getLong("usu_codccu"), rs.getLong("usu_codcid"));
				System.out.println("Demissao sem justa causa" + justificativaFaltaVO);
				final NeoRunnable work = new NeoRunnable(){
				       public void run() throws Exception{
				    	    abreTarefaDemissaoSemJustaCausa(justificativaFaltaVO.getNumemp(),justificativaFaltaVO.getNumcad(), justificativaFaltaVO.getNomFun(), justificativaFaltaVO.getNumcpf(), justificativaFaltaVO.getTipcol(), justificativaFaltaVO.getAfainf(), 
				    			   justificativaFaltaVO.getDatafa(), justificativaFaltaVO.getHorafa(), justificativaFaltaVO.getDatter(), justificativaFaltaVO.getSitafa(), justificativaFaltaVO.getCaudem(), justificativaFaltaVO.getCodreg(), justificativaFaltaVO.getNomloc(), justificativaFaltaVO.getLotorn(),
				    			   justificativaFaltaVO.getTitred(), justificativaFaltaVO.getNomfis() , justificativaFaltaVO.getUsuCodccu(), justificativaFaltaVO.getUsuCodCid()
		    			   );
				             
				       }
				};
				
				try{
					contJDS++;
					PersistEngine.managedRun(work);
				}
				catch (final Exception e){
					System.out.println(e.getMessage() + e);
					e.printStackTrace();
					logEmail.append("<BR>Erro ao processar managedrun => [Demissao sem justa causa] " + justificativaFaltaVO);
					throw new Exception("Erro ao fazer managedRun");
				}
				
			}
			
		}catch (Exception e)
		{
			System.out.println("##### AGENDADOR DE TAREFA: Erro ao Abrir Tarefa Simples Afastamento Justificativa - Regra: Justificar desligamento sem justa causa ["+key+"]");
			e.printStackTrace();
			logEmail.append("<b>Erro</b>");
			houveErro = true;
		}	
		
		logEmail.append("<br>[Desligamentos com justa causa]");
		try{
			
			//"Justificar desligamento com justa causa		
			sqlDemissaoComJustaCausa.append(" DECLARE @DatRef  Datetime "); 
			sqlDemissaoComJustaCausa.append(" SELECT @DatRef = DATEADD(dd, 0, DATEDIFF(dd, 0, getdate()))  ");
			//sqlDemissaoComJustaCausa.append(" SELECT @DatRef = '2015-06-29' ");
			sqlDemissaoComJustaCausa.append(" SELECT fun.NumEmp, fun.TipCol, fun.NumCad, fun.NumCpf, afa.datafa, afa.horafa, fun.NomFun, orn.USU_CodReg, car.TitRed, orn.USU_LotOrn, orn.NomLoc, afa.DatAfa, fis.USU_NomFis, afa.SitAfa, afa.CauDem, orn.usu_codccu, orn.usu_codcid ");
			sqlDemissaoComJustaCausa.append(" FROM R038AFA afa WITH (NOLOCK) ");
			sqlDemissaoComJustaCausa.append(" INNER JOIN R034FUN fun WITH (NOLOCK) ON fun.NumEmp = afa.NumEmp AND fun.TipCol = afa.TipCol AND fun.NumCad = afa.NumCad ");
			sqlDemissaoComJustaCausa.append(" INNER JOIN R024CAR car WITH (NOLOCK) ON car.EstCar = fun.EstCar AND car.CodCar = fun.CodCar ");
			sqlDemissaoComJustaCausa.append(" INNER JOIN R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp AND hlo.TipCol = fun.TipCol AND hlo.NumCad = fun.NumCad AND hlo.DatAlt = (SELECT MAX (DATALT) FROM R038HLO TABELA001 WHERE TABELA001.NUMEMP = hlo.NUMEMP AND TABELA001.TIPCOL = hlo.TIPCOL AND TABELA001.NUMCAD = hlo.NUMCAD AND TABELA001.DATALT <=  afa.DatAfa) ");
			sqlDemissaoComJustaCausa.append(" INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.TabOrg = hlo.TabOrg AND orn.NumLoc = hlo.NumLoc ");
			sqlDemissaoComJustaCausa.append(" INNER JOIN USU_TFisRes fis ON fis.USU_FisRes = fun.usu_fisres ");
			sqlDemissaoComJustaCausa.append(" WHERE afa.SitAfa = 7 AND afa.CauDem IN (1) and ");
			sqlDemissaoComJustaCausa.append(" not exists (select * FROM [CACUPE\\SQL02].fusion_producao.DBO.d_TarefaAfastamentoJustificativa aj WITH (NOLOCK) ");
			sqlDemissaoComJustaCausa.append(" where aj.sitAfa = 7 and aj.caudem = 1 and aj.numcpf = fun.numcpf and cast(floor(cast(aj.datafa as float)) as datetime) = cast(floor(cast(afa.datafa as float)) as datetime) and aj.horafa = afa.horafa AND isnull(aj.SitAfa, afa.SitAfa) = afa.SitAfa ) ");
			sqlDemissaoComJustaCausa.append(" AND afa.DatAfa <= @DatRef-1 AND afa.DatAfa >= '2017-12-11' ");
			sqlDemissaoComJustaCausa.append(" AND orn.USU_CodReg in (0,1,2,3,4,5,6,7,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23) AND afa.DatAfa >= '2014-10-01' ");

			pstm = conn.prepareStatement(sqlDemissaoComJustaCausa.toString());

			rs = pstm.executeQuery();

			while (rs.next())
			{
				
				justificativaFaltaVO = new JustificativaFaltaVO(rs.getString("NumEmp"), rs.getString("NumCad"), rs.getString("NomFun"), rs.getLong("NumCpf"), rs.getLong("TipCol") , /*rs.getString("USU_AfaInf"),*/ "",                                             
		    			   rs.getTimestamp("DatAfa"), rs.getLong("HorAfa"), /* rs.getTimestamp("DatTer") */(Timestamp) null, rs.getLong("SitAfa"), rs.getLong("CauDem"), rs.getLong("USU_CodReg"), rs.getString("NomLoc"), rs.getString("USU_LotOrn"),  
		    			   rs.getString("TitRed"), rs.getString("USU_NomFis"),"", rs.getLong("usu_codccu"), rs.getLong("usu_codcid"));
				System.out.println("Demissao com Justa causa" + justificativaFaltaVO);
				final NeoRunnable work = new NeoRunnable(){
				       public void run() throws Exception{
				    	    abreTarefaDemissaoComJustaCausa(justificativaFaltaVO.getNumemp(),justificativaFaltaVO.getNumcad(), justificativaFaltaVO.getNomFun(), justificativaFaltaVO.getNumcpf(), justificativaFaltaVO.getTipcol(), justificativaFaltaVO.getAfainf(), 
				    			   justificativaFaltaVO.getDatafa(), justificativaFaltaVO.getHorafa(), justificativaFaltaVO.getDatter(), justificativaFaltaVO.getSitafa(), justificativaFaltaVO.getCaudem(), justificativaFaltaVO.getCodreg(), justificativaFaltaVO.getNomloc(), justificativaFaltaVO.getLotorn(),
				    			   justificativaFaltaVO.getTitred(), justificativaFaltaVO.getNomfis() , justificativaFaltaVO.getUsuCodccu(), justificativaFaltaVO.getUsuCodCid()
		    			   );
				             
				       }
				};
				try{
					contJDC++;
					PersistEngine.managedRun(work);
				}
				catch (final Exception e){
					System.out.println(e.getMessage() + e);
					e.printStackTrace();
					logEmail.append("<BR>Erro ao processar managedrun => [Demissao com Justa causa] " + justificativaFaltaVO);
					throw new Exception("Erro ao fazer managedRun");
				}
				
			}
		}catch (Exception e)
		{
			System.out.println("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Afastamento Justificativa ["+key+"]");
			e.printStackTrace();
			logEmail.append("<b>Erro</b>");
			houveErro = true;
		}
		
		try{
		
			System.out.println("Faltas nao justificadas: " + contJFN +
					"\nFaltas justificadas: " + contJF +
					"\nDemissao com Justa Causa: " + contJDC +
					"\nDemissao sem Justa Causa: " + contJDS 
					);
			
			logEmail.append(("\nFaltas nao justificadas: " + contJFN +
					"\nFaltas justificadas: " + contJF +
					"\nDemissao com Justa Causa: " + contJDC +
					"\nDemissao sem Justa Causa: " + contJDS 
					));
			
			OrsegupsUtils.sendEmail2Orsegups("emailautomatico@orsegups.com.br", "nao.responda@orsegups.com.br", "Justificativa de Afastamentos", "", logEmail.toString());
			//OrsegupsUtils.sendEmail2Orsegups("marcos.santos@orsegups.com.br", "nao.responda@orsegups.com.br", "Justificativa de Afastamentos", "", logEmail.toString());
	
			OrsegupsUtils.closeConnection(conn, pstm, rs);
			System.out.println("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Simples Afastamento Justificativa - - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}catch(Exception e){
			e.printStackTrace();
			throw new JobException("Rotina executou com erros. verificar no log por ["+key+"]");
		}
		
		if (houveErro){
			throw new JobException("Rotina executou com erros. verificar no log por ["+key+"]");
		}
		

	}
	
	
	/*
	 * rs.getString("NumEmp"), rs.getString("NumCad"), rs.getString("NomFun"), rs.getLong("NumCpf"), rs.getLong("TipCol") , rs.getString("USU_AfaInf"),
	 * rs.getDate("DatAfa"), rs.getLong("HorAfa"), rs.getDate("DatTer"), rs.getLong("SitAfa"), rs.getLong("CauDem"), rs.getLong("USU_CodReg"), rs.getString("NomLoc"), rs.getString("USU_LotOrn"),
	 * rs.getString("TitRed"), rs.getString("USU_NomFis")
	 */
	private void abreTarefaFaltaNaoJustificada(String numemp, String numcad, String nomFun, Long numcpf, Long tipcol, String afainf, Timestamp datafa, Long horafa, Timestamp datter, Long sitafa, Long caudem, Long codreg, String nomloc, String lotorn, String titred, String nomfis, Long usuCodccu, Long usuCodcid) throws SQLException, Exception
	{
		Connection conn = null;
		PreparedStatement stCount = null;
		ResultSet rsCount = null;
		
		try{
			conn = PersistEngine.getConnection("VETORH");
			
			logEmail.append("<br/> " + numemp + "/" + numcad + "-" +  nomFun );
			
			String afaInf = afainf;
			
			if (afaInf != null && !afaInf.equals("") && afaInf.equals("S"))
			{
				Long numCpf = numcpf;
				Timestamp dataTermino = datter;
				GregorianCalendar gData = new GregorianCalendar();
				gData.setTimeInMillis(dataTermino.getTime());
				
				
				
				StringBuilder sqlCount = new StringBuilder();
				sqlCount.append(" SELECT SUM(DATEDIFF(DAY, afa.DatAfa, CASE WHEN afa.DatAfa = afa.DatTer THEN DATEADD(dd, 1, afa.DatTer) ELSE afa.DatTer END)) AS count ");
				sqlCount.append(" FROM R038AFA afa ");
				sqlCount.append(" INNER JOIN R034FUN fun ON fun.NumEmp = afa.NumEmp AND fun.TipCol = afa.TipCol AND fun.NumCad = afa.NumCad ");
				sqlCount.append(" WHERE afa.SitAfa = 15 AND fun.NumCpf = ? AND afa.USU_AfaInf = 'S' ");
				sqlCount.append(" AND afa.DatTer >= DATEADD(MM, -12, ?) AND afa.DatTer <= ? ");

				stCount = conn.prepareStatement(sqlCount.toString());
				
				stCount.setLong(1, numCpf);
				stCount.setTimestamp(2, new Timestamp(gData.getTimeInMillis()));
				stCount.setTimestamp(3, new Timestamp(gData.getTimeInMillis()));
				
				rsCount = stCount.executeQuery();

				if (rsCount.next())
				{
					Long count = rsCount.getLong("count");
					if (count == null || count <= 3)
					{
						logEmail.append( "- Obs: tem registros de afastamentos informado no periodo menor que 3" );
						InstantiableEntityInfo tarefaAJ = AdapterUtils.getInstantiableEntityInfo("TarefaAfastamentoJustificativa");
						NeoObject noAJ = tarefaAJ.createNewInstance();
						EntityWrapper ajWrapper = new EntityWrapper(noAJ);

						GregorianCalendar datAfa = new GregorianCalendar();
						datAfa.setTime(datafa);

						ajWrapper.findField("numemp").setValue(NeoUtils.safeLong(numemp));
						ajWrapper.findField("tipcol").setValue(tipcol);
						ajWrapper.findField("numcad").setValue(NeoUtils.safeLong(numcad));
						ajWrapper.findField("numcpf").setValue(numCpf);
						ajWrapper.findField("datafa").setValue(datAfa);
						ajWrapper.findField("horafa").setValue(horafa);
						ajWrapper.findField("tarefa").setValue("Afastamento Informado");
						ajWrapper.findField("SitAfa").setValue(sitafa);
						ajWrapper.findField("CauDem").setValue(caudem);
						
						PersistEngine.persist(noAJ);
						return;
					}else{
						logEmail.append( "- Obs: NAO tem registro de afastamento informado no periodo. [count="+count+"]" );
					}
				}
			}
			//Esse aqui
			String solicitante = "juliano.clemente";
			String titulo = "Falta nÃ£o justificada - " + numemp + "/" + numcad + " - " + nomFun;

			GregorianCalendar prazo = OrsegupsUtils.getNextWorkDay(new GregorianCalendar());
			
			prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 1L);
			prazo.set(Calendar.HOUR_OF_DAY, 23);
			prazo.set(Calendar.MINUTE, 59);
			prazo.set(Calendar.SECOND, 59);
			String dataInicio = NeoDateUtils.safeDateFormat(datafa, "dd/MM/yyyy");
			String dataFim = NeoDateUtils.safeDateFormat(datter, "dd/MM/yyyy");

			Long codReg = codreg;
			String lotacao = "";
			
			if (codReg == 0 && lotorn.equals("Coordenadoria IndenizaÃ§Ã£o Contratual"))
			{
				lotacao = nomloc;
			}
			else if (codReg == 1L)
			{
				lotacao = nomloc;
			}
			else if (codReg == 13L)
			{
				lotacao = nomloc;
			}
			else
			{
				lotacao = lotorn;
			}

			//NeoPaper papel = new NeoPaper();
			String executor = "";

			System.out.println("[DEBUG JUSTIFICATIVA FALTA]" + numemp + "/" + numcad + " - " + nomFun+ " - codreg= " + codReg + " lotacao=" + lotacao);
			if (codReg == 0){
				System.out.println();
			}
			
			/* Alterado em 04/04/2016 para encontrar o supervisor do colaborador dinamicamente */
			System.out.println("[DEBUG JUSTIFICATIVA FALTA]" + numemp + "/" + numcad + " - " + nomFun+ " - codreg= " + codReg + " lotacao=" + lotacao + " cidade=" + usuCodcid + " estÃ¡ na lista de cidades? " + cidadesExecao.contains(NeoUtils.safeOutputString(usuCodcid)) );
					
			if(codReg != 0L && codReg != 9L){
				executor = "gusthavo.costa";
			}else if(codReg == 9L) {
				executor = "delcimara.pedrotti";
			}else{
				ColaboradorVO colab = QLPresencaSupervisaoUtils.retornaSupervisorPosto(NeoUtils.safeLong(numemp), NeoUtils.safeLong(numcad), 1L,usuCodccu);
				
				if (colab != null){
					System.out.println("Colaborador: " + colab);
					executor = QLPresencaSupervisaoUtils.getUsuario(QLPresencaSupervisaoUtils.PP_USUARIO_FUSION, codreg, QLPresencaSupervisaoUtils.recuperaSupervisorExecao(colab.getCpf()),lotacao).getCode();
					System.out.println("responsavel Tarefa: " + colab);
	
				}else{
					
					NeoUser coordenador = QLPresencaSupervisaoUtils.getUsuario(QLPresencaSupervisaoUtils.PP_COORDENADOR_REGIONAL, codreg, null, lotacao);
					
					if (coordenador == null){
						executor = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "juliano.clemente" )).getCode();
					}else{
						executor = coordenador.getCode();
					}
				}
			
			}
			
			/*papel = OrsegupsUtils.getPapelJustificarAfastamento(codReg, lotacao);

			if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
			{
				for (NeoUser user : papel.getUsers())
				{
					executor = user.getCode();
					break;
				}
			}
			else
			{
				System.out.println("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Afastamento Justificativa - Regra: Falta nÃ£o justificada - Papel sem usuÃ¡rio definido - Colaborador: " + numemp + "/" + numcad + " - Data: " + NeoUtils.safeDateFormat(datafa, "dd/MM/yyyy"));
				return;
			}*/

			String descricao = "";
			descricao = " <strong>Colaborador:</strong> " + numemp + "/" + numcad + " - " + nomFun + "<br>";
			descricao = descricao + " <strong>Cargo:</strong> " + titred + "<br>";
			descricao = descricao + " <strong>Posto:</strong> " + lotorn + " - " + nomloc + "<br>";
			descricao = descricao + " <strong>ResponsÃ¡vel:</strong> " + nomfis + "<br>";

			if (dataInicio.equals(dataFim))
			{
				descricao = descricao + " <strong>Data da falta:</strong> " + dataInicio + "<br>";
			}
			else
			{
				descricao = descricao + " <strong>PerÃ­odo da falta:</strong> " + dataInicio + " Ã  " + dataFim + "<br>";
			}

			IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
			String avanca = "sim";
			/*if (executor.equals("juliano.clemente")){
				avanca = "off";
			}*/
			String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", avanca, prazo);
			logEmail.append( " [tarefa:"+tarefa+"] " );
			
			InstantiableEntityInfo tarefaAJ = AdapterUtils.getInstantiableEntityInfo("TarefaAfastamentoJustificativa");
			NeoObject noAJ = tarefaAJ.createNewInstance();
			EntityWrapper ajWrapper = new EntityWrapper(noAJ);

			try{
				GregorianCalendar datAfa = new GregorianCalendar();
				datAfa.setTime(datafa);

				ajWrapper.setValue("numemp", NeoUtils.safeLong(numemp) );
				ajWrapper.setValue("tipcol",tipcol);
				ajWrapper.setValue("numcad", NeoUtils.safeLong(numcad) );
				ajWrapper.setValue("numcpf",numcpf);
				ajWrapper.setValue("datafa",datAfa);
				ajWrapper.setValue("horafa",horafa);
				ajWrapper.setValue("tarefa",tarefa);
				ajWrapper.setValue("SitAfa",sitafa);
				ajWrapper.setValue("CauDem",caudem);
				
				
				PersistEngine.persist(noAJ);
				logEmail.append( " - reg no eform Ok. " );
			}catch(Exception e){
				System.out.println("Regra: Falta nÃ£o justificada - erro ao registrar no eform ");
				e.printStackTrace();
				logEmail.append( " - reg no eform Falhou. " );
			}
			
			System.out.println("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Afastamento Justificativa - Regra: Falta nÃ£o justificada - Responsavel " + executor + " Colaborador: " + numemp + "/" + numcad + " - Data: " + NeoDateUtils.safeDateFormat(datafa, "dd/MM/yyyy"));
		}catch(SQLException se){
			throw se;
		}catch(Exception e){
			throw e;
		}finally{
			OrsegupsUtils.closeConnection(conn, stCount, rsCount);
		}
		
	}

	/*
	 * rs.getString("NumEmp"), rs.getString("NumCad"), rs.getString("NomFun"), rs.getLong("NumCpf"), rs.getLong("TipCol") , rs.getString("USU_AfaInf"),
	 * rs.getDate("DatAfa"), rs.getLong("HorAfa"), rs.getDate("DatTer"), rs.getLong("SitAfa"), rs.getLong("CauDem"), rs.getLong("USU_CodReg"), rs.getString("NomLoc"), rs.getString("USU_LotOrn"),
	 * rs.getString("TitRed"), rs.getString("USU_NomFis"), rs.getString("DesSit")
	 */
	private void abreTarefaFaltaJustificada(String numemp, String numcad, String nomFun, Long numcpf, Long tipcol, String afainf, Timestamp datafa, Long horafa, Timestamp datter, Long sitafa, Long caudem, Long codreg, String nomloc, String lotorn, String titred, String nomfis, String dessit, Long usuCodccu, Long usuCodcid) throws SQLException
	{

		//Long key = GregorianCalendar.getInstance().getTimeInMillis();
		logEmail.append("<br/>" + numemp + "/" + numcad + "-" +  nomFun );
		
		String solicitante = "juliano.clemente";
		String titulo = "Enviar justificativa de ausÃªncia - " + numemp + "/" + numcad + " - " + nomFun;//esse aqui
		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 1L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);
		String dataInicio = NeoDateUtils.safeDateFormat(datafa, "dd/MM/yyyy");
		String dataFim = NeoDateUtils.safeDateFormat(datter, "dd/MM/yyyy");

		Long codReg = codreg;
		String lotacao = "";
		if (codReg == 0 && lotorn.equals("Coordenadoria IndenizaÃ§Ã£o Contratual"))
		{
			lotacao = nomloc;
		}
		else if (codReg == 1L)
		{
			lotacao = nomloc;
		}
		else if (codReg == 13L)
		{
			lotacao = nomloc;
		}
		else
		{
			lotacao = lotorn;
		}

		NeoPaper papel = new NeoPaper();
		String executor = "";


		System.out.println("[DEBUG JUSTIFICATIVA FALTA]" + numemp + "/" + numcad + " - " + nomFun+ " - codreg= " + codReg + " lotacao=" + lotacao + " cidade=" + usuCodcid + " estÃ¡ na lista de cidades? " + cidadesExecao.contains(NeoUtils.safeOutputString(usuCodcid)) );

		/* Alterado em 04/04/2016 para encontrar o supervisor do colaborador dinamicamente */
		/*if (cidadesExecao.contains(NeoUtils.safeOutputString(usuCodcid))){
			executor = "gusthavo.costa";
		}else{
		
			ColaboradorVO colab = QLPresencaSupervisaoUtils.retornaSupervisorPosto(NeoUtils.safeLong(numemp), NeoUtils.safeLong(numcad), 1L,usuCodccu);
			
			if (colab != null){
				System.out.println("Colaborador: " + colab);
				executor = QLPresencaSupervisaoUtils.getUsuario(QLPresencaSupervisaoUtils.PP_USUARIO_FUSION, codreg, QLPresencaSupervisaoUtils.recuperaSupervisorExecao(colab.getCpf()),lotacao).getCode();
				System.out.println("responsavel Tarefa: " + colab);

			}else{
				
				NeoUser coordenador = QLPresencaSupervisaoUtils.getUsuario(QLPresencaSupervisaoUtils.PP_COORDENADOR_REGIONAL, codreg, null, lotacao);
				
				if (coordenador == null){
					executor = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "juliano.clemente" )).getCode();
				}else{
					executor = coordenador.getCode();
				}
			}
		
		}*/
		
		if(codReg != 0L && codReg != 9L){
			executor = "gusthavo.costa";
		}else if(codReg == 9L) {
			executor = "delcimara.pedrotti";
		}else{
			ColaboradorVO colab = QLPresencaSupervisaoUtils.retornaSupervisorPosto(NeoUtils.safeLong(numemp), NeoUtils.safeLong(numcad), 1L,usuCodccu);
			
			if (colab != null){
				System.out.println("Colaborador: " + colab);
				executor = QLPresencaSupervisaoUtils.getUsuario(QLPresencaSupervisaoUtils.PP_USUARIO_FUSION, codreg, QLPresencaSupervisaoUtils.recuperaSupervisorExecao(colab.getCpf()),lotacao).getCode();
				System.out.println("responsavel Tarefa: " + colab);

			}else{
				
				NeoUser coordenador = QLPresencaSupervisaoUtils.getUsuario(QLPresencaSupervisaoUtils.PP_COORDENADOR_REGIONAL, codreg, null, lotacao);
				
				if (coordenador == null){
					executor = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "juliano.clemente" )).getCode();
				}else{
					executor = coordenador.getCode();
				}
			}
		
		}
		
		/*papel = OrsegupsUtils.getPapelJustificarAfastamento(codReg, lotacao);

		if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
		{
			for (NeoUser user : papel.getUsers())
			{
				executor = user.getCode();
				break;
			}
		}
		else
		{
			System.out.println("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Afastamento Justificativa - Regra: Enviar justificativa de ausÃªncia - Papel sem usuÃ¡rio definido - Colaborador: " + numemp + "/" + numcad + " - Data: " + NeoUtils.safeDateFormat(datafa, "dd/MM/yyyy"));
			return;
		}*/

		String descricao = "";
		descricao = " <strong>Colaborador:</strong> " + numemp + "/" + numcad + " - " + nomFun + "<br>";
		descricao = descricao + " <strong>Cargo:</strong> " + titred + "<br>";
		descricao = descricao + " <strong>Posto:</strong> " + lotorn + " - " + nomloc + "<br>";
		descricao = descricao + " <strong>ResponsÃ¡vel:</strong> " + nomfis + "<br>";

		if (dataInicio.equals(dataFim))
		{
			descricao = descricao + " <strong>Data da ausÃªncia:</strong> " + dataInicio + "<br>";
		}
		else
		{
			descricao = descricao + " <strong>PerÃ­odo da ausÃªncia:</strong> " + dataInicio + " Ã  " + dataFim + "<br>";
		}

		descricao = descricao + " <strong>Motivo:</strong> " + dessit + "<br>";

		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		String avanca = "sim";
		/*if (executor.equals("juliano.clemente")){
			avanca = "off";
		}*/
		String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", avanca, prazo);
		logEmail.append( " [tarefa:"+tarefa+"] " );

		InstantiableEntityInfo tarefaAJ = AdapterUtils.getInstantiableEntityInfo("TarefaAfastamentoJustificativa");
		NeoObject noAJ = tarefaAJ.createNewInstance();
		EntityWrapper ajWrapper = new EntityWrapper(noAJ);

		try{
			GregorianCalendar datAfa = new GregorianCalendar();
			datAfa.setTime(datafa);

			ajWrapper.setValue("numemp", NeoUtils.safeLong(numemp) );
			ajWrapper.setValue("tipcol", tipcol);
			ajWrapper.setValue("numcad", NeoUtils.safeLong(numcad) );
			ajWrapper.setValue("numcpf", numcpf);
			ajWrapper.setValue("datafa", datAfa);
			ajWrapper.setValue("horafa", horafa);
			ajWrapper.setValue("tarefa", tarefa);
			ajWrapper.setValue("SitAfa", sitafa);
			ajWrapper.setValue("CauDem", caudem);
			
			PersistEngine.persist(noAJ);
			logEmail.append( " - reg no eform Ok. " );
		}catch(Exception e){
			System.out.println("Regra: Falta justificada - erro ao registrar no eform ");
			e.printStackTrace();
			logEmail.append( " - reg no eform Falhou. " );
		}

		System.out.println("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Afastamento Justificativa - Regra: Enviar justificativa de ausÃªncia - Responsavel " + papel + " Colaborador: " + numemp + "/" + numcad + " - Data: " + NeoDateUtils.safeDateFormat(datafa, "dd/MM/yyyy"));

	}

	/*
	 * rs.getString("NumEmp"), rs.getString("NumCad"), rs.getString("NomFun"), rs.getLong("NumCpf"), rs.getLong("TipCol") , rs.getString("USU_AfaInf"),
	 * rs.getDate("DatAfa"), rs.getLong("HorAfa"), rs.getDate("DatTer"), rs.getLong("SitAfa"), rs.getLong("CauDem"), rs.getLong("USU_CodReg"), rs.getString("NomLoc"), rs.getString("USU_LotOrn"),
	 * rs.getString("TitRed"), rs.getString("USU_NomFis"), rs.getString("DesSit")
	 */
	private void abreTarefaDemissaoSemJustaCausa(String numemp, String numcad, String nomFun, Long numcpf, Long tipcol, String afainf, Timestamp datafa, Long horafa, Timestamp datter, Long sitafa, Long caudem, Long codreg, String nomloc, String lotorn, String titred, String nomfis, Long usuCodccu,  Long usuCodcid) throws SQLException
	{
		logEmail.append("<br/>" + numemp + "/" + numcad + "-" +  nomFun );
		String solicitante = OrsegupsUtils.getUserNeoPaper("G001SolicitanteJustificarDesligamento");
		String titulo = "Justificar desligamento - " + numemp + "/" + numcad + " - " + nomFun;
		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);

		Long codReg = codreg;
		
		String executor = OrsegupsUtils.getUserNeoPaper("G001ExecutorJustificarDesligamento");
		/*	
		String executor = "";
		NeoPaper gerente = new NeoPaper();

		gerente = OrsegupsUtils.getPapelDesligamentoSemJustaCausa(codReg, lotorn);

		if (gerente != null && gerente.getAllUsers() != null && !gerente.getAllUsers().isEmpty())
		{
			for (NeoUser user : gerente.getUsers())
			{
				executor = user.getCode();
				break;
			}
		}
		else
		{
			System.out.println("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Afastamento Justificativa - Regra: DemissÃ£o sem justa causa - Papel sem usuÃ¡rio definido - Colaborador: " + numemp + "/" + numcad + " - Data: " + NeoDateUtils.safeDateFormat(datafa, "dd/MM/yyyy"));
			return;
		}
		*/
		String descricao = "";
		descricao = " <strong>Colaborador:</strong> " + numemp + "/" + numcad + " - " + nomFun + "<br>";
		descricao = descricao + " <strong>Cargo:</strong> " + titred + "<br>";
		descricao = descricao + " <strong>Posto:</strong> " + lotorn + " - " + nomloc + "<br>";
		descricao = descricao + " <strong>Data de desligamento:</strong> " + NeoDateUtils.safeDateFormat( datafa, "dd/MM/yyyy") + "<br>";
		descricao = descricao + " <strong>ResponsÃ¡vel:</strong> " + nomfis + "<br>";

		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		String avanca = "sim";
		/*if (executor.equals("juliano.clemente")){
			avanca = "off";
		}*/
		String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", avanca, prazo);
		logEmail.append( " [tarefa:"+tarefa+"] " );

		InstantiableEntityInfo tarefaAJ = AdapterUtils.getInstantiableEntityInfo("TarefaAfastamentoJustificativa");
		NeoObject noAJ = tarefaAJ.createNewInstance();
		EntityWrapper ajWrapper = new EntityWrapper(noAJ);

		try{
			GregorianCalendar datAfa = new GregorianCalendar();
			datAfa.setTime(datafa);
			
			ajWrapper.setValue("numemp", NeoUtils.safeLong(numemp) );
			ajWrapper.setValue("tipcol", tipcol);
			ajWrapper.setValue("numcad", NeoUtils.safeLong(numcad) );
			ajWrapper.setValue("numcpf", numcpf);
			ajWrapper.setValue("datafa", datAfa);
			ajWrapper.setValue("horafa", horafa);
			ajWrapper.setValue("tarefa", tarefa);
			ajWrapper.setValue("SitAfa", sitafa);
			ajWrapper.setValue("CauDem", caudem);
			
			PersistEngine.persist(noAJ);
			logEmail.append( " - reg no eform Ok. " );
			
		}catch(Exception e){
			System.out.println("Regra: Falta justificada - erro ao registrar no eform ");
			e.printStackTrace();
			logEmail.append( " - reg no eform Falhou. " );
		}

	}

	/*
	 * rs.getString("NumEmp"), rs.getString("NumCad"), rs.getString("NomFun"), rs.getLong("NumCpf"), rs.getLong("TipCol") , rs.getString("USU_AfaInf"),
	 * rs.getDate("DatAfa"), rs.getLong("HorAfa"), rs.getDate("DatTer"), rs.getLong("SitAfa"), rs.getLong("CauDem"), rs.getLong("USU_CodReg"), rs.getString("NomLoc"), rs.getString("USU_LotOrn"),
	 * rs.getString("TitRed"), rs.getString("USU_NomFis"), rs.getString("DesSit")
	 */
	private void abreTarefaDemissaoComJustaCausa(String numemp, String numcad, String nomFun, Long numcpf, Long tipcol, String afainf, Timestamp datafa, Long horafa, Timestamp datter, Long sitafa, Long caudem, Long codreg, String nomloc, String lotorn, String titred, String nomfis, Long usuCodccu, Long usuCodcid) throws SQLException
	{

		logEmail.append("<br/>" + numemp + "/" + numcad + "-" +  nomFun );
		String titulo = "Justificar desligamento com justa causa - " + numemp + "/" + numcad + " - " + nomFun;
		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);				

		NeoPaper papelSolicitante = new NeoPaper();
		NeoPaper papelResponsavel = new NeoPaper();
		String solicitante = "";
		String executor = "";		
		
		//Solicitado alteração para unico executor fixo tarefa 1460283 - 17/10/2018
		papelSolicitante = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "solicitanteTarefaDemissaoJustaCausa"));
		papelResponsavel = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "executorTarefaDemissaoJustaCausa"));	
		
		//papelResponsavel = OrsegupsUtils.getPapelJustaCausa(codReg, lotorn);
		
		if (papelSolicitante != null && papelSolicitante.getAllUsers() != null && !papelSolicitante.getAllUsers().isEmpty()) {
			for (NeoUser user : papelSolicitante.getUsers()) {
			    solicitante = user.getCode();
			    break;
			}
		}
		else {
		    System.out.println("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Afastamento Justificativa - Regra: Demissão com justa causa - Papel " + papelSolicitante.getCode() + " sem usuário definido - Colaborador: " + numemp + "/" + numcad + " - Data: " + NeoDateUtils.safeDateFormat(datafa, "dd/MM/yyyy"));
		    return;
		}
		
		if (papelResponsavel != null && papelResponsavel.getAllUsers() != null && !papelResponsavel.getAllUsers().isEmpty()) {
			for (NeoUser user : papelResponsavel.getUsers()) {
			    executor = user.getCode();
			    break;
			}
		}
		else {
		    System.out.println("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Afastamento Justificativa - Regra: Demissão com justa causa - Papel " + papelResponsavel.getCode() + " sem usuário definido - Colaborador: " + numemp + "/" + numcad + " - Data: " + NeoDateUtils.safeDateFormat(datafa, "dd/MM/yyyy"));
		    return;
		}

		String descricao = "";
		descricao = " <strong>Colaborador:</strong> " + numemp + "/" + numcad + " - " + nomFun + "<br>";
		descricao = descricao + " <strong>Cargo:</strong> " + titred + "<br>";
		descricao = descricao + " <strong>Posto:</strong> " + lotorn + " - " + nomloc + "<br>";
		descricao = descricao + " <strong>Data de desligamento:</strong> " + NeoDateUtils.safeDateFormat(datafa, "dd/MM/yyyy") + "<br>";
		descricao = descricao + " <strong>ResponsÃ¡vel:</strong> " + nomfis + "<br>";
		
		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		String avanca = "0";

		String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", avanca, prazo);
		logEmail.append( " [tarefa:"+tarefa+"] " );

		InstantiableEntityInfo tarefaAJ = AdapterUtils.getInstantiableEntityInfo("TarefaAfastamentoJustificativa");
		NeoObject noAJ = tarefaAJ.createNewInstance();
		EntityWrapper ajWrapper = new EntityWrapper(noAJ);

		try{
			GregorianCalendar datAfa = new GregorianCalendar();
			datAfa.setTime(datafa);

			ajWrapper.setValue("numemp", NeoUtils.safeLong(numemp) );
			ajWrapper.setValue("tipcol", tipcol);
			ajWrapper.setValue("numcad", NeoUtils.safeLong(numcad) );
			ajWrapper.setValue("numcpf", numcpf);
			ajWrapper.setValue("datafa", datAfa);
			ajWrapper.setValue("horafa", horafa);
			ajWrapper.setValue("tarefa", tarefa);
			ajWrapper.setValue("SitAfa", sitafa);
			ajWrapper.setValue("CauDem", caudem);
			
			PersistEngine.persist(noAJ);
			logEmail.append( " - reg no eform Ok. " );
			
		}catch(Exception e){
			System.out.println("Regra: Falta justificada - erro ao registrar no eform ");
			e.printStackTrace();
			logEmail.append( " - reg no eform Falhou. " );
		}

		System.out.println("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Afastamento Justificativa - Regra: Justificar desligamento com justa causa - Responsavel Colaborador: " + numemp + "/" + numcad + " - Data: " + NeoDateUtils.safeDateFormat(datafa, "dd/MM/yyyy"));

	}
	
	public static String recuperCidadesRegra(){
		String retorno = "0";
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		StringBuilder sql = new StringBuilder();
		
		sql.append(" select codcid from D_qlRegraCidadesAfastamentos ");
		
		try{
			con = PersistEngine.getConnection("");
			pst = con.prepareStatement(sql.toString());
			
			rs = pst.executeQuery();
			
			while(rs.next()){
				retorno += ","+rs.getLong("codcid");
			}
			
			return retorno;
		}catch(Exception e){
			System.out.println("Erro recuperCidadesRegra()");
			e.printStackTrace();
			return retorno;
		}finally{
			OrsegupsUtils.closeConnection(con, pst, rs);
		}
	}
	
	public static String retonaFiscalResposanvel(Long numcad, Long numemp){
		StringBuilder sql = new StringBuilder();
	    PreparedStatement pstm = null;
	    Connection conn = PersistEngine.getConnection("VETORH");
	    ResultSet rs = null;
	    String cpfFiscal = null;
		
	    sql.append(" select (case when Fis.USU_NomFis = 'Luciano - Asseio' then 'Luciano Martins da Silva' "); 
	    sql.append(" 			  when Fis.USU_NomFis = 'Antonio - Asseio' then 'Antonio Adilson Araujo dos Santos' ");
	    sql.append(" 			  when Fis.USU_NomFis = 'Geni - Asseio' then 'Geni Stockmann' ");
	    sql.append(" 			  when Fis.USU_NomFis = 'Jonas Hamerski - Asseio' then 'Jonas Hamerski' ");
	    sql.append("          end) as fiscal, ");
	    sql.append(" (select top 1 numcpf from r034fun where nomfun = (case when Fis.USU_NomFis = 'Luciano - Asseio' then 'Luciano Martins da Silva' "); 
   		sql.append(" 														when Fis.USU_NomFis = 'Antonio - Asseio' then 'Antonio Adilson Araujo dos Santos' ");
		sql.append(" 														when Fis.USU_NomFis = 'Geni - Asseio' then 'Geni Stockmann' ");
    	sql.append(" 														when Fis.USU_NomFis = 'Jonas Hamerski - Asseio' then 'Jonas Hamerski' ");
		sql.append(" 													end)) as cpf from USU_TFisRes Fis ");
		sql.append(" inner join r034fun fun on fun.usu_fisres = Fis.USU_FisRes ");
		sql.append(" where numcad = ? and numemp = ? and sitafa <> 7 ");
		sql.append(" order by 1 ");

	    try
		{
			pstm = conn.prepareStatement(sql.toString());
			pstm.setLong(1, numcad);
			pstm.setLong(2, numemp);
			rs = pstm.executeQuery();
			
			while (rs.next())
			{
				cpfFiscal = rs.getString("cpf");
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}finally{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
	    return cpfFiscal;
	}
}
