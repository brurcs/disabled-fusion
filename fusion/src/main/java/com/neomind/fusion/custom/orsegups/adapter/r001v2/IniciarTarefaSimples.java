package com.neomind.fusion.custom.orsegups.adapter.r001v2;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.fusion.workflow.model.ProcessModel;

public class IniciarTarefaSimples implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{
			ProcessModel pm = (ProcessModel) PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("name", "G001 - Tarefa Simples"));
			NeoUser nuSolicitante = processEntity.findGenericValue("tarefaSimples2.Solicitante");
			NeoObject noTarefa = processEntity.findGenericValue("tarefaSimples2");

			WFProcess processo = pm.startProcess(noTarefa, false, null, null, null, null, nuSolicitante);

			try
			{
				new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividade(nuSolicitante, true);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		catch (Exception e)
		{
			System.out.println("Erro na classe IniciarTarefaSimples do fluxo Q004.");
			e.printStackTrace();
			throw new WorkflowException(e.getMessage());
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
