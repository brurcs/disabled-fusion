package com.neomind.fusion.custom.orsegups.justificativaAfastamento.converter;

import java.text.SimpleDateFormat;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.justificativaAfastamento.util.JAUtils;
import com.neomind.fusion.custom.orsegups.justificativaAfastamento.vo.JAHistoricoMedicoBean;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.WFProcess;

public class JAHistoricoCRMConverter extends StringConverter {

    @Override
    protected String getHTMLInput(EFormField field, OriginEnum origin) {

	Long idPai = field.getForm().getObjectId();
	EntityWrapper wrapper = JAUtils.getEformPai(idPai);
	
	WFProcess wfprocess = (WFProcess) wrapper.findField("wfprocess").getValue();
	
	String code = "";
	
	if (wfprocess != null) {

	    String processCode = wfprocess.toString();

	    String arrayCode[] = processCode.split("-");

	    if (arrayCode.length >= 0) {
		
		code = arrayCode[0].trim();
	    }
	}
	
	NeoObject medico = (NeoObject)wrapper.findField("medico").getValue();
	
	StringBuilder html = new StringBuilder();
	
	if (medico != null){
		EntityWrapper wMedico = new EntityWrapper(medico);
		
		String crm = wMedico.findGenericValue("registro.regcon");
		
		List<JAHistoricoMedicoBean> listaHistorico = JAUtils.getListaHistoricoMedico(crm, code);

		html.append("<table class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\" id=\"historicoMedico\">");
		html.append("<tbody>");
		html.append("<th>Tarefa</th>"); 
		html.append("<th>Inicio processo</th>");
		html.append("<th>Status do processo</th>");
		html.append("<th>Empresa</th>");
		html.append("<th>Matricula</th>");
		html.append("<th>Nome</th>");
		html.append("<th>CID</th>");
		
		for (JAHistoricoMedicoBean historico : listaHistorico) {
		    this.htmlAppend(html, historico);
		};
		
		html.append("</tbody>");
		html.append("</table></br>");
	}else{
		html.append("<table class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\" id=\"historicoMedico\">");
		html.append("<tbody>");
		html.append("<th>Tarefa</th>"); 
		html.append("<th>Inicio processo</th>");
		html.append("<th>Status do processo</th>");
		html.append("<th>Empresa</th>");
		html.append("<th>Matricula</th>");
		html.append("<th>Nome</th>");
		html.append("<th>CID</th>");
		html.append("</tbody>");
		html.append("</table></br>");
	    
	}
	


	return html.toString();
    }
    
    private void htmlAppend(StringBuilder html, JAHistoricoMedicoBean historico){
	
	String formatoData = "dd/MM/yyyy";
	
	SimpleDateFormat formatador= new SimpleDateFormat(formatoData);
	
	String linkTarefa = historico.getLink() != null ? historico.getLink() : "";
	
	String statusProcesso = "";
	
	String inicioProcesso = "";
	
	if (historico.getDataProcesso() != null){
	    inicioProcesso = formatador.format(historico.getDataProcesso().getTime());
	    statusProcesso = historico.getStatusProcesso() == 0 ? "Em execução" : "Finalizado";
	}
	
	html.append("<tr>");
	html.append("<td>"+linkTarefa+"</td>");
	html.append("<td>"+inicioProcesso+"</td>");
	html.append("<td>"+statusProcesso+"</td>");
	html.append("<td>"+historico.getNumEmp()+"</td>");
	html.append("<td>"+historico.getNumCad()+ "</td>");
	html.append("<td>"+historico.getNome()+"</td>");
	html.append("<td>"+historico.getCodCid()+"</td>");
	html.append("</tr>");
    }
    

    @Override
    protected String getHTMLView(EFormField field, OriginEnum origin) {
	return getHTMLInput(field, origin);
    }

}
