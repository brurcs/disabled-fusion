package com.neomind.fusion.custom.orsegups.processoJuridico.civel.adapter;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.util.NeoDateUtils;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.ConverterShowHistorico
public class HistoricoPenalidadesConverter extends StringConverter
{
	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin)
	{
		StringBuilder outBuilder = new StringBuilder();

		List<NeoObject> listHistorico = (List<NeoObject>) field.getValue();

		sortNeoId(listHistorico);

		outBuilder.append("<table  class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">");
		outBuilder.append("    <tr style=\"cursor: auto\">");
		outBuilder.append("        <th style=\"cursor: auto\">Descrição</th>");
		outBuilder.append("        <th style=\"cursor: auto; white-space: normal\">Data Recebido</th>");
		outBuilder.append("        <th style=\"cursor: auto\">Data Prazo</th>");
		outBuilder.append("        <th style=\"cursor: auto\">Data Execução</th>");
		outBuilder.append("        <th style=\"cursor: auto\">Percentual Redução Honorário</th>");
		outBuilder.append("    </tr>");
		outBuilder.append("    <tbody>");

		for (NeoObject registro : listHistorico)
		{
			EntityWrapper wRegistro = new EntityWrapper(registro);
			outBuilder.append("        <tr>");
			outBuilder.append("            <td> " + wRegistro.getValue("descricao") + " </td>");
			outBuilder.append("            <td> " + NeoDateUtils.safeDateFormat((GregorianCalendar) wRegistro.findGenericValue("dataRecebido"), "dd/MM/yyyy HH:mm:ss") + " </td>");
			outBuilder.append("            <td> " + NeoDateUtils.safeDateFormat((GregorianCalendar) wRegistro.findGenericValue("dataPrazo"), "dd/MM/yyyy HH:mm:ss") + " </td>");
			outBuilder.append("            <td> " + NeoDateUtils.safeDateFormat((GregorianCalendar) wRegistro.findGenericValue("dataExecutado"), "dd/MM/yyyy HH:mm:ss") + " </td>");
			outBuilder.append("            <td> " + wRegistro.findGenericValue("percentualReducao") + " </td>");
			outBuilder.append("        </tr>");
		}

		outBuilder.append("    </tbody>");
		outBuilder.append("</table>");

		return outBuilder.toString();

	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}

	public static void sortNeoId(Collection<? extends NeoObject> list)
	{
		Collections.sort((List<? extends NeoObject>) list, new Comparator<NeoObject>()
		{

			public int compare(NeoObject obj1, NeoObject obj2)
			{

				return ((Long) new EntityWrapper(obj1).findGenericValue("neoId") < (Long) new EntityWrapper(obj2).findGenericValue("neoId")) ? -1 : 1;
			}
		});
	}
}
