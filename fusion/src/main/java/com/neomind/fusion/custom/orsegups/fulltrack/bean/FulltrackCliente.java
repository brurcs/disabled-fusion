package com.neomind.fusion.custom.orsegups.fulltrack.bean;

public class FulltrackCliente {
    
    private int ras_cli_id;
    private String ras_cli_desc;
    private String ras_cli_razao;
    private String ras_cli_endereco;
    private String ras_cli_bairro;
    private String ras_cli_cep;
    private String ras_cli_uf;
    private String ras_cli_cidade;
    private String ras_cli_cnpj;
    private String ras_cli_tipo;
    private String ras_cli_data_cadastro;
    private String ras_cli_data_alteracao;
    private String ras_cli_liberado;
    
    public int getRas_cli_id() {
        return ras_cli_id;
    }
    public void setRas_cli_id(int ras_cli_id) {
        this.ras_cli_id = ras_cli_id;
    }
    public String getRas_cli_desc() {
        return ras_cli_desc;
    }
    public void setRas_cli_desc(String ras_cli_desc) {
        this.ras_cli_desc = ras_cli_desc;
    }
    public String getRas_cli_razao() {
        return ras_cli_razao;
    }
    public void setRas_cli_razao(String ras_cli_razao) {
        this.ras_cli_razao = ras_cli_razao;
    }
    public String getRas_cli_endereco() {
        return ras_cli_endereco;
    }
    public void setRas_cli_endereco(String ras_cli_endereco) {
        this.ras_cli_endereco = ras_cli_endereco;
    }
    public String getRas_cli_bairro() {
        return ras_cli_bairro;
    }
    public void setRas_cli_bairro(String ras_cli_bairro) {
        this.ras_cli_bairro = ras_cli_bairro;
    }
    public String getRas_cli_cep() {
        return ras_cli_cep;
    }
    public void setRas_cli_cep(String ras_cli_cep) {
        this.ras_cli_cep = ras_cli_cep;
    }
    public String getRas_cli_uf() {
        return ras_cli_uf;
    }
    public void setRas_cli_uf(String ras_cli_uf) {
        this.ras_cli_uf = ras_cli_uf;
    }
    public String getRas_cli_cidade() {
        return ras_cli_cidade;
    }
    public void setRas_cli_cidade(String ras_cli_cidade) {
        this.ras_cli_cidade = ras_cli_cidade;
    }
    public String getRas_cli_cnpj() {
        return ras_cli_cnpj;
    }
    public void setRas_cli_cnpj(String ras_cli_cnpj) {
        this.ras_cli_cnpj = ras_cli_cnpj;
    }
    public String getRas_cli_tipo() {
        return ras_cli_tipo;
    }
    public void setRas_cli_tipo(String ras_cli_tipo) {
        this.ras_cli_tipo = ras_cli_tipo;
    }
    public String getRas_cli_data_cadastro() {
        return ras_cli_data_cadastro;
    }
    public void setRas_cli_data_cadastro(String ras_cli_data_cadastro) {
        this.ras_cli_data_cadastro = ras_cli_data_cadastro;
    }
    public String getRas_cli_data_alteracao() {
        return ras_cli_data_alteracao;
    }
    public void setRas_cli_data_alteracao(String ras_cli_data_alteracao) {
        this.ras_cli_data_alteracao = ras_cli_data_alteracao;
    }
    public String getRas_cli_liberado() {
        return ras_cli_liberado;
    }
    public void setRas_cli_liberado(String ras_cli_liberado) {
        this.ras_cli_liberado = ras_cli_liberado;
    }
    
    

}
