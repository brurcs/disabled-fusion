package com.neomind.fusion.custom.orsegups.presenca.vo;

import java.util.Arrays;
import java.util.Date;

public class ColaboradorVO
{
	private Long numeroEmpresa;
	private String nomeEmpresa;
	private Long tipoColaborador;
	private Long numeroCadastro;
	private String numeroCracha;
	private String nomeColaborador;
	private String cargo;
	private String sistemaCargo;
	private EscalaVO escala;
	private Long codigoEscala;
	private Long turmaEscala;
	private String situacao;
	private String obsSituacao;
	private Boolean controlaAcesso;
	private Boolean acessoBloqueado;	
	private PresencaVO presenca;
	private String sexo;
	private Long cpf;
	private Long numeroLocal;
	private Long codigoOrganograma;
	private String atividadesExecutadas;
	private String centroCusto;
	private Date dataNascimento;
	private Date dataAdmissao;
	private Date dataAfastamento;
	private Date dataDemissao;
	private String afastamento;
	private String fiscal;
	private String matrizResponsabilidade;
	private String causaDemissao;
	private EnderecoVO endereco;
	private String telefone;
	private Long qdeExcecao;
	private Long avosFerias;
	private byte[] foto;
	private String departamento;
	private String strDataNascimento;
	private String strDataAdmissao;
	private EnderecoVO enderecoPosto;
	private String telefone2;
	private String lotacao; 
	
	/**
	 * Ultimo exame ASO
	 */
	private String ultExm;
	/**
	 * Próximo Exame ASO
	 */
	private String proExm;
	/**
	 * Data de reciclagem
	 */
	private String usuDatRec;
	
	/**
	 * Data de reciclagem dacadastrada no ruby pela Luana. Menu personalizadas -> cadastro de cursos
	 */
	private String usuDatfor;
	

	public PresencaVO getPresenca()
	{
		return this.presenca;
	}

	public void setPresenca(PresencaVO presenca)
	{
		this.presenca = presenca;
	}

	public Long getNumeroEmpresa()
	{
		return this.numeroEmpresa;
	}

	public void setNumeroEmpresa(Long numeroEmpresa)
	{
		this.numeroEmpresa = numeroEmpresa;
	}

	public Long getTipoColaborador()
	{
		return this.tipoColaborador;
	}

	public void setTipoColaborador(Long tipoColaborador)
	{
		this.tipoColaborador = tipoColaborador;
	}

	public Long getNumeroCadastro()
	{
		return this.numeroCadastro;
	}

	public void setNumeroCadastro(Long numeroCadastro)
	{
		this.numeroCadastro = numeroCadastro;
	}

	public String getNomeColaborador()
	{
		return this.nomeColaborador;
	}

	public void setNomeColaborador(String nomeColaborador)
	{
		this.nomeColaborador = nomeColaborador;
	}

	public String getCargo()
	{
		return this.cargo;
	}

	public void setCargo(String cargo)
	{
		this.cargo = cargo;
	}

	public EscalaVO getEscala()
	{
		return escala;
	}

	public void setEscala(EscalaVO escala)
	{
		this.escala = escala;
	}

	public Long getCodigoEscala()
	{
		return this.codigoEscala;
	}

	public void setCodigoEscala(Long codigoEscala)
	{
		this.codigoEscala = codigoEscala;
	}

	public Long getTurmaEscala()
	{
		return this.turmaEscala;
	}

	public void setTurmaEscala(Long turmaEscala)
	{
		this.turmaEscala = turmaEscala;
	}

	public String getSituacao()
	{
		return this.situacao;
	}

	public void setSituacao(String situacao)
	{
		this.situacao = situacao;
	}
	
	public Long getCpf()
	{
		return cpf;
	}

	public void setCpf(Long cpf)
	{
		this.cpf = cpf;
	}

	public Boolean isPresente()
	{
		if ((this.presenca != null) && (this.presenca.getEntrada() != null) && (this.presenca.getSaida() == null)) {
			return true;
		} else 
		{
			return false;
		}
	}

	@Override
	public boolean equals(Object obj)
	{
		boolean result = false;
		if (obj instanceof ColaboradorVO) {
			
			result = obj != null && this.getNumeroCadastro() == ((ColaboradorVO) obj).getNumeroCadastro() && this.getTipoColaborador() == ((ColaboradorVO) obj).getTipoColaborador() && this.getNumeroEmpresa() == ((ColaboradorVO) obj).getNumeroEmpresa();
			
		}
		return result;
	}

	public Long getNumeroLocal()
	{
		return numeroLocal;
	}

	public void setNumeroLocal(Long numeroLocal)
	{
		this.numeroLocal = numeroLocal;
	}

	public Long getCodigoOrganograma()
	{
		return codigoOrganograma;
	}

	public void setCodigoOrganograma(Long codigoOrganograma)
	{
		this.codigoOrganograma = codigoOrganograma;
	}

	public String getNumeroCracha()
	{
		return numeroCracha;
	}

	public void setNumeroCracha(String numeroCracha)
	{
		this.numeroCracha = numeroCracha;
	}

	public Boolean isControlaAcesso()
	{
		return controlaAcesso;
	}

	public void setControlaAcesso(Boolean controlaAcesso)
	{
		this.controlaAcesso = controlaAcesso;
	}

	public Boolean isAcessoBloqueado()
	{
		return acessoBloqueado;
	}

	public void setAcessoBloqueado(Boolean acessoBloqueado)
	{
		this.acessoBloqueado = acessoBloqueado;
	}

	public String getObsSituacao()
	{
		return obsSituacao;
	}

	public void setObsSituacao(String obsSituacao)
	{
		this.obsSituacao = obsSituacao;
	}

	public String getAtividadesExecutadas()
	{
		return atividadesExecutadas;
	}

	public void setAtividadesExecutadas(String atividadesExecutadas)
	{
		this.atividadesExecutadas = atividadesExecutadas;
	}

	public String getCentroCusto()
	{
		return centroCusto;
	}

	public void setCentroCusto(String centroCusto)
	{
		this.centroCusto = centroCusto;
	}

	public String getSexo()
	{
		return sexo;
	}

	public void setSexo(String sexo)
	{
		this.sexo = sexo;
	}

	public Date getDataNascimento()
	{
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento)
	{
		this.dataNascimento = dataNascimento;
	}

	public Date getDataAdmissao()
	{
		return dataAdmissao;
	}

	public void setDataAdmissao(Date dataAdmissao)
	{
		this.dataAdmissao = dataAdmissao;
	}

	public Date getDataAfastamento()
	{
		return dataAfastamento;
	}

	public void setDataAfastamento(Date dataAfastamento)
	{
		this.dataAfastamento = dataAfastamento;
	}

	public Date getDataDemissao() {
		return dataDemissao;
	}

	public void setDataDemissao(Date dataDemissao) {
		this.dataDemissao = dataDemissao;
	}

	public String getAfastamento()
	{
		return afastamento;
	}

	public void setAfastamento(String afastamento)
	{
		this.afastamento = afastamento;
	}

	public String getFiscal()
	{
		return fiscal;
	}

	public void setFiscal(String fiscal)
	{
		this.fiscal = fiscal;
	}

	public String getMatrizResponsabilidade()
	{
		return matrizResponsabilidade;
	}

	public void setMatrizResponsabilidade(String matrizResponsabilidade)
	{
		this.matrizResponsabilidade = matrizResponsabilidade;
	}

	public String getCausaDemissao() {
		return causaDemissao;
	}

	public void setCausaDemissao(String causaDemissao) {
		this.causaDemissao = causaDemissao;
	}

	public EnderecoVO getEndereco() {
		return endereco;
	}

	public void setEndereco(EnderecoVO endereco) {
		this.endereco = endereco;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	public Long getQdeExcecao()
	{
		return qdeExcecao;
	}

	public void setQdeExcecao(Long qdeExcecao)
	{
		this.qdeExcecao = qdeExcecao;
	}

	public String getNomeEmpresa()
	{
		return nomeEmpresa;
	}

	public void setNomeEmpresa(String nomeEmpresa)
	{
		this.nomeEmpresa = nomeEmpresa;
	}

	public String getSistemaCargo()
	{
		return sistemaCargo;
	}

	public void setSistemaCargo(String sistemaCargo)
	{
		this.sistemaCargo = sistemaCargo;
	}

	public Long getAvosFerias()
	{
		return avosFerias;
	}

	public void setAvosFerias(Long avosFerias)
	{
		this.avosFerias = avosFerias;
	}

	public String getDepartamento()
	{
		return departamento;
	}

	public void setDepartamento(String departamento)
	{
		this.departamento = departamento;
	}

	public String getStrDataNascimento()
	{
		return strDataNascimento;
	}

	public void setStrDataNascimento(String strDataNascimento)
	{
		this.strDataNascimento = strDataNascimento;
	}

	public String getStrDataAdmissao()
	{
		return strDataAdmissao;
	}

	public void setStrDataAdmissao(String strDataAdmissao)
	{
		this.strDataAdmissao = strDataAdmissao;
	}

	public EnderecoVO getEnderecoPosto() {
		return enderecoPosto;
	}

	public void setEnderecoPosto(EnderecoVO enderecoPosto) {
		this.enderecoPosto = enderecoPosto;
	}

	public String getUltExm() {
		return ultExm;
	}

	public void setUltExm(String ultExm) {
		this.ultExm = ultExm;
	}

	public String getProExm() {
		return proExm;
	}

	public void setProExm(String proExm) {
		this.proExm = proExm;
	}

	public String getUsuDatRec() {
		return usuDatRec;
	}

	public void setUsuDatRec(String usuDatRec) {
		this.usuDatRec = usuDatRec;
	}

	public String getUsuDatfor() {
		return usuDatfor;
	}

	public void setUsuDatfor(String usuDatfor) {
		this.usuDatfor = usuDatfor;
	}
	
	public String getTelefone2()
	{
		return telefone2;
	}

	public void setTelefone2(String telefone2)
	{
		this.telefone2 = telefone2;
	}
	
	

	public String getLotacao()
	{
		return lotacao;
	}

	public void setLotacao(String lotacao)
	{
		this.lotacao = lotacao;
	}

	@Override
	public String toString()
	{
		return "ColaboradorVO [numeroEmpresa=" + numeroEmpresa + ", nomeEmpresa=" + nomeEmpresa + ", tipoColaborador=" + tipoColaborador + ", numeroCadastro=" + numeroCadastro + ", numeroCracha=" + numeroCracha + ", nomeColaborador=" + nomeColaborador + ", cargo=" + cargo + ", sistemaCargo=" + sistemaCargo + ", escala=" + escala + ", codigoEscala=" + codigoEscala + ", turmaEscala=" + turmaEscala + ", situacao=" + situacao + ", obsSituacao=" + obsSituacao + ", controlaAcesso=" + controlaAcesso
				+ ", acessoBloqueado=" + acessoBloqueado + ", presenca=" + presenca + ", sexo=" + sexo + ", cpf=" + cpf + ", numeroLocal=" + numeroLocal + ", codigoOrganograma=" + codigoOrganograma + ", atividadesExecutadas=" + atividadesExecutadas + ", centroCusto=" + centroCusto + ", dataNascimento=" + dataNascimento + ", dataAdmissao=" + dataAdmissao + ", dataAfastamento=" + dataAfastamento + ", dataDemissao=" + dataDemissao + ", afastamento=" + afastamento + ", fiscal=" + fiscal
				+ ", matrizResponsabilidade=" + matrizResponsabilidade + ", causaDemissao=" + causaDemissao + ", endereco=" + endereco + ", telefone=" + telefone + ", qdeExcecao=" + qdeExcecao + ", avosFerias=" + avosFerias + ", foto=" + Arrays.toString(foto) + ", departamento=" + departamento + ", strDataNascimento=" + strDataNascimento + ", strDataAdmissao=" + strDataAdmissao + ", enderecoPosto=" + enderecoPosto + ", telefone2=" + telefone2 + ", lotacao=" + lotacao + ", ultExm=" + ultExm
				+ ", proExm=" + proExm + ", usuDatRec=" + usuDatRec + ", usuDatfor=" + usuDatfor + "]";
	}
	
	

	
	
	
	
	
	
}