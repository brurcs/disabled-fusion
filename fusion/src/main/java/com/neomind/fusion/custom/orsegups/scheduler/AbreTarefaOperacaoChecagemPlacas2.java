package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class AbreTarefaOperacaoChecagemPlacas2 implements CustomJobAdapter
{
    @SuppressWarnings("unused")
    private static final Log log = LogFactory.getLog(AbreTarefaOperacaoChecagemPlacas2.class);

    @Override
    public void execute(CustomJobContext arg0)
    {
	Connection conn = PersistEngine.getConnection("SIGMA90");
	StringBuilder sqlEventos = new StringBuilder();

	PreparedStatement pstm = null;
	ResultSet rs = null;
	final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.AbreTarefaOperacaoChecagemPlacas");
	log.warn("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Operacao Checagem Placas - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	Long key = GregorianCalendar.getInstance().getTimeInMillis(); 

	try
	{
	    //Eventos finalizados (FG_STATUS = 4)			
	    sqlEventos.append(" SELECT h.CD_HISTORICO, h.CD_EVENTO, c.ID_EMPRESA, c.ID_CENTRAL, c.PARTICAO, c.RAZAO, C.FANTASIA, h.TX_OBSERVACAO_FECHAMENTO, (c.ENDERECO + ', ' + bai.NOME + ', ' + cid.NOME + ', ' + cid.ID_ESTADO) AS ENDERECO, v.NM_VIATURA ");
	    sqlEventos.append(" FROM VIEW_HISTORICO h ");
	    sqlEventos.append(" INNER JOIN dbCENTRAL c ON c.CD_CLIENTE = h.CD_CLIENTE ");
	    sqlEventos.append(" INNER JOIN VIATURA v ON v.CD_VIATURA = h.CD_VIATURA ");
	    sqlEventos.append(" LEFT OUTER JOIN dbCIDADE cid ON cid.ID_CIDADE = c.ID_CIDADE ");
	    sqlEventos.append(" LEFT OUTER JOIN dbBAIRRO bai ON bai.ID_CIDADE = c.ID_CIDADE AND bai.ID_BAIRRO = c.ID_BAIRRO ");
	    sqlEventos.append(" WHERE h.FG_STATUS = 4 AND  (h.CD_EVENTO = 'XPL1' OR h.CD_EVENTO = 'XPL2' OR h.CD_EVENTO = 'XPL3') AND CONVERT(DATE, h.DT_FECHAMENTO) = CONVERT(DATE, DATEADD(DAY, -1, GETDATE())) AND h.DT_VIATURA_DESLOCAMENTO IS NOT NULL ");

	    pstm = conn.prepareStatement(sqlEventos.toString());
	    rs = pstm.executeQuery();

	    while (rs.next())
	    {
		Long cdHistorico = rs.getLong("CD_HISTORICO");
		Long empresaConta = rs.getLong("ID_EMPRESA");
		String idCentral = rs.getString("ID_CENTRAL");
		String particao = rs.getString("PARTICAO");
		String razaoSocial = rs.getString("RAZAO");
		String fantasia = rs.getString("FANTASIA");
		String endereco = rs.getString("ENDERECO");
		String colaborador = rs.getString("NM_VIATURA");
		String logViatura = rs.getString("TX_OBSERVACAO_FECHAMENTO");
		if (logViatura == null) {
		    logViatura = "";
		}

		// Abrir Tarefa
		String userSolicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteOperacaoChecagemPlacas"); 
		NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", userSolicitante));
		NeoObject wkfOCP = AdapterUtils.createNewEntityInstance("OperacaoChecagemPlaca");
		final EntityWrapper ewWkfOCP= new EntityWrapper(wkfOCP);

		ewWkfOCP.findField("cdHistorico").setValue(cdHistorico);
		ewWkfOCP.findField("contaSIGMA").setValue(idCentral);
		ewWkfOCP.findField("empresaConta").setValue(empresaConta);
		ewWkfOCP.findField("particao").setValue(particao);
		ewWkfOCP.findField("razaoSocial").setValue(razaoSocial);
		ewWkfOCP.findField("fantasia").setValue(fantasia);
		ewWkfOCP.findField("endereco").setValue(endereco);
		ewWkfOCP.findField("colaborador").setValue(colaborador);
		ewWkfOCP.findField("eventoAGerar").setValue("XPL2");
		int index = logViatura.indexOf("#");
		String resultadoVisita = "";
		if (index > 0) {
		    resultadoVisita = logViatura.substring(0, index);					
		} else {
		    resultadoVisita = logViatura.toString();					
		}
		Boolean visitaOK = Boolean.TRUE;
		if (resultadoVisita.contains("PENDENTE")) {
			visitaOK = Boolean.FALSE;
		}
		ewWkfOCP.findField("visitaOk").setValue(visitaOK);
		ewWkfOCP.findField("resultadoVisita").setValue(resultadoVisita);
		
		PersistEngine.persist(wkfOCP);

		OrsegupsUtils.iniciaWorkflow(wkfOCP, "O038 - Operação Checagem de Placas", solicitante, true, true);

	    }
	} catch (Exception e)
	{

	    log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Checagem Placas");
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
	}
	finally
	{

	    try
	    {
		rs.close();
		pstm.close();
		conn.close();
	    }
	    catch (SQLException e)
	    {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }


	    log.warn("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Checagem Placas - - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	}
    }
}
