package com.neomind.fusion.custom.orsegups.fap.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.neomind.fusion.custom.orsegups.fap.dto.FornecedorDTO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

/**
 * Classe DAO para fluxo de FAP
 * 
 * @author diogo.silva
 *
 */
public class FapDAO {

    /**
     * Recupera os dados bancários do fornecedor em caso de pagamento via
     * depósito.
     * 
     * @param codigoEmpresa
     *            (Long)
     * @param codigoFornecedor
     *            (Long)
     * @return Map<String,String>
     * @throws WorkflowException
     */
    public static FornecedorDTO consultaDadosFornecedor(Long codigoEmpresa, Long codigoFornecedor) throws Exception, WorkflowException {

	FornecedorDTO fornecedorDTO = new FornecedorDTO();
	String agenciaFornecedor = null;
	String contaFornecedor = null;
	String bancoFornecedor = null;
	Long empresa = codigoEmpresa;
	Long fornecedor = codigoFornecedor;

	Connection conn = null;
	PreparedStatement pstm = null;
	StringBuilder sql = new StringBuilder();
	ResultSet rs = null;

	sql.append(" SELECT CODBAN, CODAGE, CCBFOR FROM E095HFO WITH (NOLOCK) WHERE CODFOR = " + fornecedor);
	sql.append(" AND CODEMP = " + empresa);
	sql.append(" AND CODFIL = 1 ");

	try {
	    conn = PersistEngine.getConnection("SAPIENS");

	    pstm = conn.prepareStatement(sql.toString());

	    rs = pstm.executeQuery();

	    if (rs.next()) {

		agenciaFornecedor = rs.getString("CODAGE");
		contaFornecedor = rs.getString("CCBFOR");
		bancoFornecedor = rs.getString("CODBAN");

		if (NeoUtils.safeIsNotNull(agenciaFornecedor, contaFornecedor, bancoFornecedor)) {
		    fornecedorDTO.setAgencia(agenciaFornecedor);
		    fornecedorDTO.setBanco(bancoFornecedor);
		    fornecedorDTO.setConta(contaFornecedor);
		}
	    }

	    return fornecedorDTO;

	} catch (WorkflowException e) {

	    throw new WorkflowException("Não foi possível recuperar os dados do fornecedor " + fornecedor + " na empresa " + empresa + ". Verifique as definições do fornecedor.");

	} catch (Exception e) {

	    throw new Exception(e.getMessage());

	} finally {
	    try {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}
    }

    /**
     * Recupera os dados bancários para pessoa física (fornecedor) do favorecido em caso de
     * pagamento via depósito.
     * 
     * @param codigoEmpresa
     *            (Long)
     * @param codigoFornecedor
     *            (Long)
     * @return Map<String,String>
     * @throws WorkflowException
     */
    public static FornecedorDTO consultaDadosFavorecido(Long codigoEmpresa, Long codigoFornecedor) throws Exception, WorkflowException {
	FornecedorDTO fornecedorDTO = new FornecedorDTO();
	String agenciaFornecedor = null;
	String contaFornecedor = null;
	String bancoFornecedor = null;
	Long empresa = codigoEmpresa;	
	Long fornecedor = codigoFornecedor;

	Connection conn = null;
	PreparedStatement pstm = null;
	StringBuilder sql = new StringBuilder();
	ResultSet rs = null;

	sql.append(" SELECT F.CODBAN AS CODBAN, F.CODAGE AS CODAGE, F.CCBFAV AS CCBFAV FROM E077FAV F WITH (NOLOCK) INNER JOIN E095HFO C ON ");
	sql.append(" F.CODFAV = CONVERT(varchar,C.CODFAV) AND C.CODFOR = " + fornecedor);
	sql.append(" AND C.CODEMP = " + empresa);
	sql.append(" AND C.CODFIL = 1 ");	

	try {
	    conn = PersistEngine.getConnection("SAPIENS");

	    pstm = conn.prepareStatement(sql.toString());

	    rs = pstm.executeQuery();

	    if (rs.next()) {

		agenciaFornecedor = rs.getString("CODAGE");
		contaFornecedor = rs.getString("CCBFAV");
		bancoFornecedor = rs.getString("CODBAN");

		if (NeoUtils.safeIsNotNull(agenciaFornecedor, contaFornecedor, bancoFornecedor)) {
		    fornecedorDTO.setAgencia(agenciaFornecedor);
		    fornecedorDTO.setBanco(bancoFornecedor);
		    fornecedorDTO.setConta(contaFornecedor);
		}
	    }

	    return fornecedorDTO;

	} catch (WorkflowException e) {

	    throw new WorkflowException("Não foi possível recuperar os dados bancários do favorecido associado ao  fornecedor " + fornecedor + ". Verifique as definições do fornecedor.");

	} catch (Exception e) {
	    throw new Exception(e.getMessage());
	} finally {
	    try {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}

    }

    /**
     * Consulta a UF de prestação de serviço
     * 
     * @param cdCliente
     *            (Long)
     * @param razaoCliente
     *            (String)
     * @return String
     * @throws WorkflowException
     */
    public static String consultaEstadoContrato(Long cdCliente, String razaoCliente) throws WorkflowException, Exception {
	Long conta = cdCliente;
	String razao = razaoCliente;
	String estado = null;

	Connection conn = null;
	PreparedStatement pstm = null;
	StringBuilder sql = new StringBuilder();
	ResultSet rs = null;

	sql.append("SELECT USU_T160CVS.USU_UFSCTR ");
	sql.append("FROM USU_T160SIG WITH (NOLOCK)");
	sql.append("INNER JOIN USU_T160CVS WITH (NOLOCK) ON USU_T160CVS.USU_CODEMP = USU_T160SIG.USU_CODEMP AND ");
	sql.append("USU_T160CVS.USU_CODFIL = USU_T160SIG.USU_CODFIL AND                           ");
	sql.append("USU_T160CVS.USU_NUMCTR = USU_T160SIG.USU_NUMCTR AND                           ");
	sql.append("USU_T160CVS.USU_NUMPOS = USU_T160SIG.USU_NUMPOS                               ");
	sql.append("WHERE USU_T160SIG.USU_CODCLI = " + conta);

	try {
	    conn = PersistEngine.getConnection("SAPIENS");
	    pstm = conn.prepareStatement(sql.toString());
	    rs = pstm.executeQuery();

	    if (rs.next()) {
		estado = rs.getString("USU_UFSCTR");

		return estado;
	    } else {
		throw new WorkflowException("Não foi encontrado nenhum vínculo do Cliente " + razao + " com o Sistema Sapiens! Por favor contatar o NAC Interno");
	    }
	} catch (WorkflowException e) {
	    throw (WorkflowException) e;
	} catch (Exception e) {
	    throw new Exception(e.getMessage());
	} finally {
	    try {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}
    }
}
