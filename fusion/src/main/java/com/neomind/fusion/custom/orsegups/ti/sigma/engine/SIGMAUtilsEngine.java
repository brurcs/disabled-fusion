package com.neomind.fusion.custom.orsegups.ti.sigma.engine;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.Node;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.seventh.bean.EventoArmeDesarme;
import com.neomind.fusion.custom.orsegups.ti.sigma.beans.BufferCount;
import com.neomind.fusion.custom.orsegups.ti.sigma.beans.Recepcao;
import com.neomind.fusion.custom.orsegups.ti.sigma.beans.SIGMAEmpresa;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.util.NeoDateUtils;

public class SIGMAUtilsEngine {

    public static final String REGIONAL = "regional";
    public static final String ROTA = "rota";

    private final String sql = "SELECT CD_RECEPCAO,NM_EVENTO,DT_RECEPCAO, NM_RECEPTORA, CD_CENTRAL, NU_PARTICAO FROM RECEPCAO WITH(NOLOCK) " + "WHERE (DATEDIFF(SECOND, DT_RECEPCAO, GETDATE())) >= 180 "
	    + "AND NM_EVENTO != 'ERND' " + "ORDER BY DT_RECEPCAO ";

    public BufferCount getBufferJson() {

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	BufferCount buffer = null;

	try {

	    conn = PersistEngine.getConnection("SIGMA90");
	    stmt = conn.createStatement();

	    rs = stmt.executeQuery(sql);

	    int count = 0;

	    buffer = new BufferCount();

	    while (rs.next()) {

		Recepcao rec = new Recepcao();

		String cdRecepcao = rs.getString("CD_RECEPCAO");
		Date dtRecepcao = rs.getTimestamp("DT_RECEPCAO");
		String nmReceptora = (rs.getString("NM_RECEPTORA") != null) ? rs.getString("NM_RECEPTORA") : "Vazio";
		String cdCentral = (rs.getString("CD_CENTRAL") != null) ? rs.getString("CD_CENTRAL") : "Vazio";
		String nuParticao = (rs.getString("NU_PARTICAO") != null) ? rs.getString("NU_PARTICAO") : "Vazio";
		String cdEvento = (rs.getString("NM_EVENTO") != null) ? rs.getString("NM_EVENTO") : "Vazio";

		String dtRecepcaoFormatada = NeoDateUtils.safeDateFormat(dtRecepcao, "dd/MM/yyyy HH:mm:ss:SSS");

		rec.setCdRecepcao(cdRecepcao);
		rec.setDtRecepcao(dtRecepcao);
		rec.setNmReceptora(nmReceptora);
		rec.setCdCentral(cdCentral);
		rec.setNuParticao(nuParticao);
		rec.setDtRecepcaoFormatada(dtRecepcaoFormatada);
		rec.setCdEvento(cdEvento);

		List<Recepcao> list = buffer.getListaRecepcao();

		list.add(rec);

		count++;
	    }

	    buffer.setCountAtraso(count);

	    buffer.setCount(this.getBufferCount());

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	return buffer;
    }

    public String getBufferTxt() {

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	String retorno = "";

	try {

	    conn = PersistEngine.getConnection("SIGMA90");
	    stmt = conn.createStatement();

	    rs = stmt.executeQuery(sql);

	    int count = 0;

	    while (rs.next()) {
		String cdRecepcao = rs.getString("CD_RECEPCAO");
		Date dtRecepcao = rs.getDate("DT_RECEPCAO");
		String nmReceptora = (rs.getString("NM_RECEPTORA") != null) ? rs.getString("NM_RECEPTORA") : "Vazio";
		String cdCentral = (rs.getString("CD_CENTRAL") != null) ? rs.getString("CD_CENTRAL") : "Vazio";
		String nuParticao = (rs.getString("NU_PARTICAO") != null) ? rs.getString("NU_PARTICAO") : "Vazio";
		String cdEvento = (rs.getString("NM_EVENTO") != null) ? rs.getString("NM_EVENTO") : "Vazio";

		String dtRecepcaoFormatada = NeoDateUtils.safeDateFormat(dtRecepcao, "dd/MM/yyyy HH:mm:ss:SSS");

		retorno += "cd_recepcao=" + cdRecepcao + " cd_evento=" + cdEvento + " dt_recepcao=" + dtRecepcaoFormatada + " nm_receptora=" + nmReceptora + " central=" + cdCentral + " particao=" + nuParticao;

		count++;
	    }

	    retorno = "buffer=" + this.getBufferCount() + "/" + count + " " + retorno;

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	return retorno;
    }

    private int getBufferCount() {

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	int retorno = 0;

	try {

	    String sql = "SELECT COUNT(*) as CONT FROM RECEPCAO WITH(NOLOCK)";

	    conn = PersistEngine.getConnection("SIGMA90");
	    stmt = conn.createStatement();

	    rs = stmt.executeQuery(sql);

	    while (rs.next()) {
		retorno = rs.getInt("CONT");
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	return retorno;

    }

    public List<SIGMAEmpresa> getEmpresas() {

	List<SIGMAEmpresa> retorno = null;

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	String sql = "SELECT CD_EMPRESA AS EMPRESA, NM_RAZAO_SOCIAL AS RAZAO FROM EMPRESA WITH(NOLOCK)";

	try {

	    retorno = new ArrayList<>();

	    conn = PersistEngine.getConnection("SIGMA90");

	    stmt = conn.createStatement();

	    rs = stmt.executeQuery(sql);

	    while (rs.next()) {

		SIGMAEmpresa novaEmpresa = new SIGMAEmpresa();

		novaEmpresa.setCodigo(rs.getInt("EMPRESA"));
		novaEmpresa.setRazao(rs.getString("RAZAO"));

		retorno.add(novaEmpresa);

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	return retorno;
    }

    /**
     * Retorna a lista de eventos abertos conforme parametros
     * 
     * @param eventos
     *            código dos eventos a serem selecionados.
     *            <P>
     *            Devem estar separados por virgulas e cercados por aspas
     *            simples conforme exemplo:
     * 
     *            <pre>
     * 'E130','E131','E132'
     * </pre>
     * @param cdCliente
     *            Código do cliente <code>CD_CLIENTE</code>
     * @return Lista dos eventos abertos
     */
    public List<EventoVO> getEventosAbertos(String eventos, int cdCliente) {

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	List<EventoVO> retorno = null;

	String sqlEventos = "SELECT DT_RECEBIDO, CD_EVENTO, DS_TIPO_EVENTO_RECEBIDO FROM HISTORICO WITH(NOLOCK) WHERE CD_CLIENTE=" + cdCliente + " AND CD_EVENTO IN (" + eventos + ") ORDER BY DT_RECEBIDO ASC";

	try {

	    conn = PersistEngine.getConnection("SIGMA90");
	    stmt = conn.createStatement();

	    rs = stmt.executeQuery(sqlEventos);

	    retorno = new ArrayList<>();

	    while (rs.next()) {

		EventoVO novoEvento = new EventoVO();

		GregorianCalendar dtRecebido = new GregorianCalendar();
		dtRecebido.setTime(rs.getTimestamp("DT_RECEBIDO"));

		// NeoDateUtils.safeDateFormat(new GregorianCalendar(),
		// "dd/MM/yyyy")
		novoEvento.setDataRecebimento(NeoDateUtils.safeDateFormat(dtRecebido, "dd/MM/yyyy HH:mm:ss"));
		novoEvento.setCodigoEvento(rs.getString("CD_EVENTO"));
		novoEvento.setResposta(rs.getString("DS_TIPO_EVENTO_RECEBIDO"));

		retorno.add(novoEvento);

	    }

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	return retorno;
    }

    /**
     * Retorna nome da viatura no sistema Sigma
     * 
     * @param cdViatura
     * @return nomeViatura, nunca nulo
     */
    public String getNomeViaturaSigma(String cdViatura) {
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	String retorno = "";
	try {
	    conn = PersistEngine.getConnection("SIGMA90");
	    StringBuilder selecHtSQL = new StringBuilder();
	    selecHtSQL.append(" SELECT V.NM_VIATURA ");
	    selecHtSQL.append(" FROM VIATURA V WITH (NOLOCK) ");
	    selecHtSQL.append(" WHERE  V.CD_VIATURA = ? ");

	    pstm = conn.prepareStatement(selecHtSQL.toString());
	    pstm.setString(1, cdViatura);
	    rs = pstm.executeQuery();

	    if (rs.next()) {
		retorno = rs.getString("NM_VIATURA");
	    }

	} catch (Exception e) {
	    e.printStackTrace();

	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);

	}

	return retorno;
    }

    /**
     * Retorno regional e rota para o nome de viatura fornecido
     * 
     * @param nomeViatura
     * @return Map<String,String> com as chaves incluidas nas constantes na
     *         classe pai deste metodo
     */
    public Map<String, String> getRegionalRota(String viatura) {

	Map<String, String> retorno = new HashMap<String, String>();

	if (viatura != null && !viatura.isEmpty()) {

	    String arrayViatura[] = viatura.split("-");
	    if (arrayViatura.length > 1) {
		retorno.put(SIGMAUtilsEngine.REGIONAL, arrayViatura[0]);
		if (viatura.contains("[")) {
		    retorno.put(SIGMAUtilsEngine.ROTA, arrayViatura[1] + "-" + arrayViatura[2]);
		} else {
		    retorno.put(SIGMAUtilsEngine.ROTA, arrayViatura[1]);
		}

	    }

	}

	return retorno;
    }

    /**
     * Insere um evento via API utilizando como parametro de indentificação o
     * campo <tt>Rastreador</tt> também chamado de identificador.
     * 
     * @param identificador
     *            valor do campo <tt>Rastreador</tt>
     * @param codigoEvento
     *            codigo do evento a ser inserido.
     * @return mensagem da API conforme abaixo:
     * 
     * <table border='1' cellpadding='3'>
     * <tr><td>ACK </td><td> Successful operation</td></tr>
     * <tr><td>ERRO_DATABASE </td><td> An unexpected error occurred while processing the event.</td></tr>
     * <tr><td>ERRO_CENTRAL_DUPLICATE </td><td> Two or more clients were found with the provided identification ( codigo ).</td></tr>
     * <tr><td>ERRO_CENTRAL_NOT_FOUND </td><td> Client not found.</td></tr>
     * <tr><td>ERRO_TYPE_INTEGRATION_INVALID </td><td> Invalid value for tipoIntegracao</td></tr>
     * <tr><td>ERRO_EVENT_INVALID </td><td> Invalid value for codigo</td></tr>
     * <tr><td>ERRO_TRACKER_DUPLICATE </td><td> Two or more clients were found with the provided identification. Occurs when tipoIntegracao = 1 and identificadorCliente is duplicated.</td></tr>
     * <tr><td>ERRO_BUTTON_DUPLICATE </td><td> Two or more clients were found with the provided identification. Occurs when tipoIntegracao = 2 and identificadorCliente is duplicated.</td></tr>
     * <tr><td>ERRO_PROTOCOL_INVALID </td><td> Invalid value for protocolo</td></tr>
     * 
     * </table>
     * 
     * @author mateus.batista
     */
    public String receberEventoByIdentificadorCliente(String identificador, String codigoEvento) {
	String retorno = "";
	try {
	    SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
	    SOAPConnection soapConnection = soapConnectionFactory.createConnection();

	    // Send SOAP Message to SOAP Server
	    String url = "http://201.47.43.181:8080/SigmaWebServices/ReceptorEventosWebService?wsdl";
	    SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(identificador, codigoEvento), url);

	    // Process the SOAP Response
	    retorno = printSOAPResponse(soapResponse);

	    soapConnection.close();
	} catch (Exception e) {
	    System.err.println("Error occurred while sending SOAP Request to Server");
	    e.printStackTrace();
	}

	return retorno;
    }
    
    /**
     * Insere um evento via API utilizando como parametro de indentificação os
     * campos <tt>ID_CENTRAL</tt> , <tt>PARTICAO</tt> , <tt>ID_EMPRESA</tt>
     * 
     * @param idCentral   valor do campo <tt>ID_CENTRAL</tt>
     * @param empresa   valor do campo <tt>ID_EMPRESA</tt>
     * @param particao   valor do campo <tt>PARTICAO</tt>
     * @param codigoEvento    codigo do evento a ser inserido.
     * @return mensagem da API conforme abaixo:
     * 
     * <table border='1' cellpadding='3'>
     * <tr><td>ACK </td><td> Successful operation</td></tr>
     * <tr><td>ERRO_DATABASE </td><td> An unexpected error occurred while processing the event.</td></tr>
     * <tr><td>ERRO_CENTRAL_DUPLICATE </td><td> Two or more clients were found with the provided identification ( codigo ).</td></tr>
     * <tr><td>ERRO_CENTRAL_NOT_FOUND </td><td> Client not found.</td></tr>
     * <tr><td>ERRO_TYPE_INTEGRATION_INVALID </td><td> Invalid value for tipoIntegracao</td></tr>
     * <tr><td>ERRO_EVENT_INVALID </td><td> Invalid value for codigo</td></tr>
     * <tr><td>ERRO_TRACKER_DUPLICATE </td><td> Two or more clients were found with the provided identification. Occurs when tipoIntegracao = 1 and identificadorCliente is duplicated.</td></tr>
     * <tr><td>ERRO_BUTTON_DUPLICATE </td><td> Two or more clients were found with the provided identification. Occurs when tipoIntegracao = 2 and identificadorCliente is duplicated.</td></tr>
     * <tr><td>ERRO_PROTOCOL_INVALID </td><td> Invalid value for protocolo</td></tr>
     * 
     * </table>
     * 
     * @author mateus.batista
     */
    public String receberEventoByIdentificadorCliente(String idCentral,String empresa, String particao, String codigoEvento) {
	String retorno = "";
	try {
	    SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
	    SOAPConnection soapConnection = soapConnectionFactory.createConnection();

	    // Send SOAP Message to SOAP Server
	    String url = "http://201.47.43.181:8080/SigmaWebServices/ReceptorEventosWebService?wsdl";
	    SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(idCentral,empresa,particao,codigoEvento), url);

	    // Process the SOAP Response
	    retorno = printSOAPResponse(soapResponse);

	    soapConnection.close();
	} catch (Exception e) {
	    System.err.println("Error occurred while sending SOAP Request to Server");
	    e.printStackTrace();
	}

	return retorno;
    }
    
    /**
     * Insere um evento via API utilizando como parametro de indentificação os
     * campos <tt>ID_CENTRAL</tt> , <tt>PARTICAO</tt> , <tt>ID_EMPRESA</tt>
     * 
     * @param idCentral   valor do campo <tt>ID_CENTRAL</tt>
     * @param empresa   valor do campo <tt>ID_EMPRESA</tt>
     * @param particao   valor do campo <tt>PARTICAO</tt>
     * @param codigoEvento    codigo do evento a ser inserido.
     * @param receptora identificação da receptora
     * @return mensagem da API conforme abaixo:
     * 
     * <table border='1' cellpadding='3'>
     * <tr><td>ACK </td><td> Successful operation</td></tr>
     * <tr><td>ERRO_DATABASE </td><td> An unexpected error occurred while processing the event.</td></tr>
     * <tr><td>ERRO_CENTRAL_DUPLICATE </td><td> Two or more clients were found with the provided identification ( codigo ).</td></tr>
     * <tr><td>ERRO_CENTRAL_NOT_FOUND </td><td> Client not found.</td></tr>
     * <tr><td>ERRO_TYPE_INTEGRATION_INVALID </td><td> Invalid value for tipoIntegracao</td></tr>
     * <tr><td>ERRO_EVENT_INVALID </td><td> Invalid value for codigo</td></tr>
     * <tr><td>ERRO_TRACKER_DUPLICATE </td><td> Two or more clients were found with the provided identification. Occurs when tipoIntegracao = 1 and identificadorCliente is duplicated.</td></tr>
     * <tr><td>ERRO_BUTTON_DUPLICATE </td><td> Two or more clients were found with the provided identification. Occurs when tipoIntegracao = 2 and identificadorCliente is duplicated.</td></tr>
     * <tr><td>ERRO_PROTOCOL_INVALID </td><td> Invalid value for protocolo</td></tr>
     * 
     * </table>
     * 
     * @author mateus.batista
     */
    public String receberEventoByIdentificadorCliente(String idCentral,String empresa, String particao, String codigoEvento, String receptora) {
	String retorno = "";
	try {
	    SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
	    SOAPConnection soapConnection = soapConnectionFactory.createConnection();

	    // Send SOAP Message to SOAP Server
	    String url = "http://201.47.43.181:8080/SigmaWebServices/ReceptorEventosWebService?wsdl";
	    SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(idCentral,empresa,particao,codigoEvento, receptora), url);

	    // Process the SOAP Response
	    retorno = printSOAPResponse(soapResponse);

	    soapConnection.close();
	} catch (Exception e) {
	    System.err.println("Error occurred while sending SOAP Request to Server");
	    e.printStackTrace();
	}

	return retorno;
    }
    
    private SOAPMessage createSOAPRequest(String idCentral,String empresa, String particao, String cdEvento) throws Exception {
	MessageFactory messageFactory = MessageFactory.newInstance();
	SOAPMessage soapMessage = messageFactory.createMessage();
	SOAPPart soapPart = soapMessage.getSOAPPart();

	String serverURI = "http://webServices.sigmaWebServices.segware.com.br/";

	// SOAP Envelope
	SOAPEnvelope envelope = soapPart.getEnvelope();
	envelope.addNamespaceDeclaration("web", serverURI);
	envelope.setPrefix("x");

	// SOAP Body
	SOAPBody soapBody = envelope.getBody();
	soapBody.setPrefix("x");

	SOAPHeader soapHeader = envelope.getHeader();
	soapHeader.setPrefix("x");

	SOAPElement receberEvento = soapBody.addChildElement("receberEvento", "web");
	SOAPElement evento = receberEvento.addChildElement("evento");

	SOAPElement codigoEvento = evento.addChildElement("codigo");
	codigoEvento.addTextNode(cdEvento);

	SOAPElement descricaoReceptora = evento.addChildElement("descricaoReceptora");
	descricaoReceptora.addTextNode("Geração Automática");

	SOAPElement eIdCentral = evento.addChildElement("idCentral");
	eIdCentral.addTextNode(idCentral);
	
	SOAPElement eEmpresa = evento.addChildElement("empresa");
	eEmpresa.addTextNode(empresa);
	
	SOAPElement eParticao = evento.addChildElement("particao");
	eParticao.addTextNode(particao);
	
	SOAPElement protocolo = evento.addChildElement("protocolo");
	protocolo.addTextNode("2");

	soapMessage.saveChanges();

	return soapMessage;
    }
    
    private SOAPMessage createSOAPRequest(String idCentral,String empresa, String particao, String cdEvento, String receptora) throws Exception {
	MessageFactory messageFactory = MessageFactory.newInstance();
	SOAPMessage soapMessage = messageFactory.createMessage();
	SOAPPart soapPart = soapMessage.getSOAPPart();

	String serverURI = "http://webServices.sigmaWebServices.segware.com.br/";

	// SOAP Envelope
	SOAPEnvelope envelope = soapPart.getEnvelope();
	envelope.addNamespaceDeclaration("web", serverURI);
	envelope.setPrefix("x");

	// SOAP Body
	SOAPBody soapBody = envelope.getBody();
	soapBody.setPrefix("x");

	SOAPHeader soapHeader = envelope.getHeader();
	soapHeader.setPrefix("x");

	SOAPElement receberEvento = soapBody.addChildElement("receberEvento", "web");
	SOAPElement evento = receberEvento.addChildElement("evento");

	SOAPElement codigoEvento = evento.addChildElement("codigo");
	codigoEvento.addTextNode(cdEvento);

	SOAPElement descricaoReceptora = evento.addChildElement("descricaoReceptora");
	descricaoReceptora.addTextNode(receptora.toUpperCase().trim());

	SOAPElement eIdCentral = evento.addChildElement("idCentral");
	eIdCentral.addTextNode(idCentral);
	
	SOAPElement eEmpresa = evento.addChildElement("empresa");
	eEmpresa.addTextNode(empresa);
	
	SOAPElement eParticao = evento.addChildElement("particao");
	eParticao.addTextNode(particao);
	
	SOAPElement protocolo = evento.addChildElement("protocolo");
	protocolo.addTextNode("2");

	soapMessage.saveChanges();

	return soapMessage;
    }

    private SOAPMessage createSOAPRequest(String identificador, String cdEvento) throws Exception {
	MessageFactory messageFactory = MessageFactory.newInstance();
	SOAPMessage soapMessage = messageFactory.createMessage();
	SOAPPart soapPart = soapMessage.getSOAPPart();

	String serverURI = "http://webServices.sigmaWebServices.segware.com.br/";

	// SOAP Envelope
	SOAPEnvelope envelope = soapPart.getEnvelope();
	envelope.addNamespaceDeclaration("web", serverURI);
	envelope.setPrefix("x");

	// SOAP Body
	SOAPBody soapBody = envelope.getBody();
	soapBody.setPrefix("x");

	SOAPHeader soapHeader = envelope.getHeader();
	soapHeader.setPrefix("x");

	SOAPElement receberEvento = soapBody.addChildElement("receberEvento", "web");
	SOAPElement evento = receberEvento.addChildElement("evento");

	SOAPElement codigoEvento = evento.addChildElement("codigo");
	codigoEvento.addTextNode(cdEvento);

	SOAPElement descricaoReceptora = evento.addChildElement("descricaoReceptora");
	descricaoReceptora.addTextNode("Geração Automática");

	SOAPElement identificadorCliente = evento.addChildElement("identificadorCliente");
	identificadorCliente.addTextNode(identificador);

	SOAPElement protocolo = evento.addChildElement("protocolo");
	protocolo.addTextNode("2");

	SOAPElement tipoIntegracao = evento.addChildElement("tipoIntegracao");
	tipoIntegracao.addTextNode("1");

	soapMessage.saveChanges();

	return soapMessage;
    }

    private String printSOAPResponse(SOAPMessage soapResponse) throws Exception {

	String resposta = "";
	
	if (soapResponse != null) {
	    Iterator<?> itr = soapResponse.getSOAPBody().getChildElements();

	    while (itr.hasNext()) {
		Node node = (Node) itr.next();

		if (node.getNodeType() == Node.ELEMENT_NODE) {
		    Element ele = (Element) node;

		    switch (ele.getNodeName()) {
		    case "ns2:receberEventoResponse":
			resposta = ele.getTextContent();
			break;
		    }
		}
	    }

	}
	
	return resposta;
	
    }
    
    
    public void normalizarCoordenadasCentrais() {

	Connection conn = null;
	Statement st = null;
	ResultSet rs = null;
	
	List<String> result = new ArrayList<String>();
	
	try {
	    StringBuilder sql = new StringBuilder();
	    sql.append(" SELECT CASE  ");
	    sql.append(" 		WHEN (c.NU_LATITUDE IS NULL OR c.NU_LONGITUDE IS NULL) THEN 1  ");
	    sql.append(" 	   END AS COD_INC,		                                                                                                                                             ");
	    sql.append(" 		 c.NU_LATITUDE, c.NU_LONGITUDE, cid.USU_NU_LATITUDE AS LATCID, cid.USU_NU_LONGITUDE AS LNGCID, cid.NU_RAIO * cid.NU_MARGEM AS RANGE, dbo.LatLonRadiusDistance(c.NU_LATITUDE, c.NU_LONGITUDE, cid.USU_NU_LATITUDE, cid.USU_NU_LONGITUDE)*1.6 AS DISTANCE,  ");
	    sql.append(" 		 c.DT_CADASTRO_CENTRAL, CD_CLIENTE, ID_CENTRAL, ID_EMPRESA, PARTICAO, RAZAO, FANTASIA, ENDERECO, REFERENCIA, bai.NOME AS NOMEBAIRRO, cid.NOME AS NOMECIDADE, cid.ID_ESTADO,    ");
	    sql.append(" CASE WHEN ID_EMPRESA IN (10128,10129,10130,10131,10132,10133,10134,10135,10136,10137,10138,10139,10140) THEN 'V' ELSE 'O' END AS EMP  ");
	    sql.append(" FROM dbCENTRAL c WITH (NOLOCK)                                                                                       ");
	    sql.append(" LEFT OUTER JOIN dbCIDADE cid WITH (NOLOCK) ON cid.ID_CIDADE = c.ID_CIDADE                                                          ");
	    sql.append(" LEFT OUTER JOIN dbBAIRRO bai ON c.ID_BAIRRO = bai.ID_BAIRRO AND c.ID_CIDADE = bai.ID_CIDADE ");
	    sql.append(" WHERE c.CTRL_CENTRAL = 1 													    ");
	    sql.append(" AND c.ID_RAMO <> 10191 ");
	    sql.append(" AND (     														    ");
	    sql.append(" 		c.NU_LATITUDE IS NULL           ");
	    sql.append(" 		OR c.NU_LONGITUDE IS NULL   	 ");
	    sql.append(" 	) ");
	    sql.append("  AND c.ID_EMPRESA NOT IN (10127,10075,10129) ");
	    sql.append(" ORDER BY cid.NOME ");
	    
	    conn = PersistEngine.getConnection("SIGMA90");
	    
	    st = conn.createStatement();
	    
	    rs = st.executeQuery(sql.toString());
	    
	    while (rs.next()){
		result.add(rs.getString("CD_CLIENTE"));
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, st, rs);
	}
	
	for (String r : result){
	    this.atualizarPosicao(r);
	}

    }

    private boolean atualizarPosicao(String cdCliente) {
	Connection conn = null;
	PreparedStatement st = null;
	ResultSet rs = null;
	try {

	    conn = PersistEngine.getConnection("SIGMA90");

	    StringBuffer sql1 = new StringBuffer();
	    sql1.append("SELECT c.CD_CLIENTE, c.ENDERECO, bai.NOME AS NOMEBAIRRO, cid.NOME AS NOMECIDADE, uf.NOME AS NOMEUF, cid.ID_ESTADO, c.NU_LATITUDE, c.NU_LONGITUDE ");
	    sql1.append("FROM dbCENTRAL c WITH(NOLOCK)");
	    sql1.append("INNER JOIN dbCIDADE cid WITH(NOLOCK) ON c.ID_CIDADE = cid.ID_CIDADE ");
	    sql1.append("INNER JOIN dbBAIRRO bai WITH(NOLOCK) ON c.ID_BAIRRO = bai.ID_BAIRRO AND c.ID_CIDADE = bai.ID_CIDADE ");
	    sql1.append("INNER JOIN dbESTADO uf WITH(NOLOCK) ON cid.ID_ESTADO = uf.ID_ESTADO ");
	    sql1.append("WHERE c.CD_CLIENTE = " + cdCliente + " ");
	    sql1.append("ORDER BY c.CD_CLIENTE ");

	    st = conn.prepareStatement(sql1.toString());
	    rs = st.executeQuery();

	    while (rs.next()) {
		String lat = null;
		String lng = null;

		String end = rs.getString("ENDERECO");
		String nomcid = rs.getString("NOMECIDADE");
		String uf = rs.getString("NOMEUF");

		end = end.replaceAll("[ãâàáä]", "a").replaceAll("[êèéë]", "e").replaceAll("[îìíï]", "i").replaceAll("[õôòóö]", "o").replaceAll("[ûúùü]", "u").replaceAll("[ÃÂÀÁÄ]", "A").replaceAll("[ÊÈÉË]", "E").replaceAll("[ÎÌÍÏ]", "I").replaceAll("[ÕÔÒÓÖ]", "O").replaceAll("[ÛÙÚÜ]", "U")
			.replace('ç', 'c').replace('Ç', 'C').replace('ñ', 'n').replace('Ñ', 'N').replaceAll("!", "").replaceAll("\\[\\´\\`\\?!\\@\\#\\$\\%\\¨\\*", " ").replaceAll("\\(\\)\\=\\{\\}\\[\\]\\~\\^\\]", " ").replaceAll("[\\.\\;\\-\\_\\+\\'\\ª\\º\\:\\;\\/]", " ");

		nomcid = nomcid.replaceAll("[ãâàáä]", "a").replaceAll("[êèéë]", "e").replaceAll("[îìíï]", "i").replaceAll("[õôòóö]", "o").replaceAll("[ûúùü]", "u").replaceAll("[ÃÂÀÁÄ]", "A").replaceAll("[ÊÈÉË]", "E").replaceAll("[ÎÌÍÏ]", "I").replaceAll("[ÕÔÒÓÖ]", "O").replaceAll("[ÛÙÚÜ]", "U")
			.replace('ç', 'c').replace('Ç', 'C').replace('ñ', 'n').replace('Ñ', 'N').replaceAll("!", "").replaceAll("\\[\\´\\`\\?!\\@\\#\\$\\%\\¨\\*", " ").replaceAll("\\(\\)\\=\\{\\}\\[\\]\\~\\^\\]", " ").replaceAll("[\\.\\;\\-\\_\\+\\'\\ª\\º\\:\\;\\/]", " ");

		uf = uf.replaceAll("[ãâàáä]", "a").replaceAll("[êèéë]", "e").replaceAll("[îìíï]", "i").replaceAll("[õôòóö]", "o").replaceAll("[ûúùü]", "u").replaceAll("[ÃÂÀÁÄ]", "A").replaceAll("[ÊÈÉË]", "E").replaceAll("[ÎÌÍÏ]", "I").replaceAll("[ÕÔÒÓÖ]", "O").replaceAll("[ÛÙÚÜ]", "U")
			.replace('ç', 'c').replace('Ç', 'C').replace('ñ', 'n').replace('Ñ', 'N').replaceAll("!", "").replaceAll("\\[\\´\\`\\?!\\@\\#\\$\\%\\¨\\*", " ").replaceAll("\\(\\)\\=\\{\\}\\[\\]\\~\\^\\]", " ").replaceAll("[\\.\\;\\-\\_\\+\\'\\ª\\º\\:\\;\\/]", " ");
		String sURL = end + " - " + nomcid + " - " + uf;
		String encodedURL = URLEncoder.encode(sURL.toString(), "UTF-8");
		System.out.println(end + "," + nomcid + "," + uf);
		System.out.println(encodedURL);
		URL url = new URL("http://maps.google.com/maps/api/geocode/xml?sensor=false&address=" + encodedURL);
		URLConnection uc = url.openConnection();
		BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(uc.getInputStream());
		in.close();

		Element rootElement = doc.getDocumentElement();
		lat = getString("lat", rootElement);
		lng = getString("lng", rootElement);

		if (lat != null && !lat.isEmpty()) {
		    StringBuffer sql2 = new StringBuffer();
		    sql2.append("UPDATE dbCENTRAL SET NU_LATITUDE = ?, NU_LONGITUDE = ?, DT_ULTIMA_LOCALIZACAO = GETDATE() WHERE CD_CLIENTE = ? ");
		    PreparedStatement st2 = conn.prepareStatement(sql2.toString());
		    st2.setString(1, lat);
		    st2.setString(2, lng);
		    st2.setLong(3, rs.getLong("CD_CLIENTE"));
		    int r = st2.executeUpdate();

		    if (r > 0) {
			return true;
		    }

		}

	    }

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, st, rs);
	}

	return false;
    }

    private final String getString(String tagName, Element element) {
	NodeList list = element.getElementsByTagName(tagName);
	if (list != null && list.getLength() > 0) {
	    NodeList subList = list.item(0).getChildNodes();

	    if (subList != null && subList.getLength() > 0) {
		return subList.item(0).getNodeValue();
	    }
	}
	return null;
    }

    
    /**
     * Retorna ultimos eventos de Arme e Desarme
     * @param cdCliente CD_CLIENTE
     * @return List de EventoArmeDesarme com dois objetos; um arme; um desarme ou nulo caso o CD_CLIENTE não retorne resultados
     */
    public static List<EventoArmeDesarme> getUltimoArmeDesarme(int cdCliente) {

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	sql.append(" DECLARE @CD_CLIENTE INT; ");

	sql.append(" SET @CD_CLIENTE = ?; ");

	sql.append("SELECT 'ARME' AS ACAO, DT_RECEBIDO FROM HISTORICO_ARME ");
	sql.append("WHERE CD_CLIENTE=@CD_CLIENTE  ");
	sql.append("AND CD_HISTORICO_ARME = (SELECT MAX(CD_HISTORICO_ARME) FROM HISTORICO_ARME WHERE CD_CLIENTE=@CD_CLIENTE) ");
	sql.append("UNION ");
	sql.append("SELECT 'DESARME' AS ACAO, DT_RECEBIDO FROM HISTORICO_DESARME ");
	sql.append(" WHERE CD_CLIENTE=@CD_CLIENTE  ");
	sql.append("AND CD_HISTORICO_DESARME = (SELECT MAX(CD_HISTORICO_DESARME) FROM HISTORICO_DESARME WHERE CD_CLIENTE=@CD_CLIENTE) ");
	sql.append("ORDER BY DT_RECEBIDO DESC ");
	
	List<EventoArmeDesarme> lista = new ArrayList<EventoArmeDesarme>();

	try {
	    conn = PersistEngine.getConnection("SIGMA90");
	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setInt(1, cdCliente);

	    rs = pstm.executeQuery();

	    while (rs.next()) {
		EventoArmeDesarme evento = new EventoArmeDesarme();
		evento.setOperacao(rs.getString(1));
		evento.setDtProcessado(rs.getTimestamp(2));
		lista.add(evento);
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
	
	if (lista.size() > 0){
	    return lista;
	}else{
	    return null;	    
	}

    }
    
    public static EventoArmeDesarme getDadosCFTV(int cdCliente){
	
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	sql.append(" SELECT C.ID_CENTRAL, C.ID_EMPRESA, C.SERVIDORCFTV, C.PORTACFTV, C.USERCFTV, C.SENHACFTV ");
	sql.append(" FROM dbCENTRAL C WITH(NOLOCK) ");
	sql.append(" WHERE C.CD_CLIENTE=? ");
	
	EventoArmeDesarme r = null;
	
	try {
	    conn = PersistEngine.getConnection("SIGMA90");
	    pstm = conn.prepareStatement(sql.toString());
	    
	    pstm.setInt(1, cdCliente);

	    rs = pstm.executeQuery();

	    if (rs.next()) {
		r = new EventoArmeDesarme();
		r.setIdCentral(rs.getString(1));
		r.setIdEmpresa(rs.getString(2));
		r.setServidorCFTV(rs.getString(3));
		r.setPortaCFTV(rs.getString(4));
		r.setUsuarioCFTV(rs.getString(5));
		r.setSenhaCFTV(rs.getString(6));
		
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
	
	return r;
    }
    
}
