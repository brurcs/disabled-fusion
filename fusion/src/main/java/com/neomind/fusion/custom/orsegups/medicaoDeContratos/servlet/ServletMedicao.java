package com.neomind.fusion.custom.orsegups.medicaoDeContratos.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.medicaoContratos.OrsegupsMedicaoUtilsMensal;
import com.neomind.fusion.custom.orsegups.medicaoDeContratos.vo.CoberturaPostoMedicaoVO;
import com.neomind.fusion.custom.orsegups.medicaoDeContratos.vo.ColaboradoresDemitidosPostosMedicaoVO;
import com.neomind.fusion.custom.orsegups.medicaoDeContratos.vo.ConsideracoesMedicaoVO;
import com.neomind.fusion.custom.orsegups.medicaoDeContratos.vo.DiariaMedicaoVO;
import com.neomind.fusion.custom.orsegups.medicaoDeContratos.vo.MedicaoVO;
import com.neomind.fusion.custom.orsegups.medicaoDeContratos.vo.PostosMedicaoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EntradaAutorizadaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EventoSigmaVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils;
import com.neomind.fusion.engine.runtime.RuntimeEngine;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

@WebServlet(name = "ServletMedicao", urlPatterns = { "/servlet/com.neomind.fusion.custom.orsegups.medicaoDeContratos.servlet.ServletMedicao" })
public class ServletMedicao extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	private static final Log log = LogFactory.getLog(ServletMedicao.class);
	EntityManager entityManager;
	EntityTransaction transaction;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	private void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		try
		{
			response.setContentType("text/plain");
			response.setCharacterEncoding("ISO-8859-1");

			final PrintWriter out = response.getWriter();

			String action = "";

			action = request.getParameter("action");

			if (action.equalsIgnoreCase("listaMedicoes"))
			{
				String empresa = request.getParameter("empresa");
				String competencia = request.getParameter("competencia");
				String cliente = request.getParameter("cliente");
				String nOficialContrato = request.getParameter("noContrato");
				String cbTodos = request.getParameter("cbTodos");
				String cbVagas = request.getParameter("cbVagas");
				String cbFaltas = request.getParameter("cbFalta");
				String cbDemit = request.getParameter("cbDemit");
				String cbOK = request.getParameter("cbOk");
				String regionalPesquisa = request.getParameter("regionaisPesquisa");
				regionalPesquisa = "1,9";
				
				this.listaMedicoes(empresa, competencia, cliente, nOficialContrato, cbTodos, cbVagas, cbFaltas, cbDemit, cbOK, regionalPesquisa, response);
			}

			if (action.equalsIgnoreCase("listaPostos"))
			{
				String competencia = request.getParameter("competencia");
				String cliente = request.getParameter("cliente");
				String nomeColaborador = request.getParameter("nomeColaborador");
				String numeroPosto = request.getParameter("numeroPosto");
				String cbTodos = request.getParameter("cbTodos");
				String cbVagas = request.getParameter("cbVagas");
				String cbFaltas = request.getParameter("cbFalta");
				String cbDemit = request.getParameter("cbDemit");
				String cbOK = request.getParameter("cbOk");
				String numOfi = request.getParameter("numOfi");

				this.listaPostos(competencia, cliente, nomeColaborador, numeroPosto, cbTodos, cbVagas, cbFaltas, cbDemit, cbOK, numOfi, response);
			}

			if (action.equalsIgnoreCase("listaObservacoes"))
			{
				String competencia = request.getParameter("competencia");
				String numcad = request.getParameter("numcad");
				String nomfun = request.getParameter("nomfun");

				this.listaObservacoes(competencia, numcad, nomfun, response);
			}

			if (action.equalsIgnoreCase("listaCoberturaPosto"))
			{
				String numloc = request.getParameter("numloc");
				this.listaCoberturasPosto(numloc, response);
			}

			if (action.equalsIgnoreCase("listaColaboradoresDemitidosDoPosto"))
			{
				String competencia = request.getParameter("competencia");
				String numPos = request.getParameter("numPos");
				this.listaColaboradoresDemitidosDoPosto(competencia, numPos, response);
			}

			if (action.equalsIgnoreCase("removerObservacao"))
			{
				String neoId = request.getParameter("neoId");
				String competencia = request.getParameter("competencia");
				String numcad = request.getParameter("numcad");
				String nomfun = request.getParameter("nomfun");

				if (this.removerObservacao(neoId))
				{
					this.listaObservacoes(competencia, numcad, nomfun, response);
				}
			}

			if (action.equalsIgnoreCase("removerColaborador"))
			{
				String neoId = request.getParameter("neoId");
				String competencia = request.getParameter("competencia");
				String cliente = request.getParameter("cliente");
				String numOfi = request.getParameter("numOfi");

				if (this.removerColaborador(neoId))
				{
					this.listaPostos(competencia, cliente, null, null, null, null, null, null, null, numOfi, response);
				}
			}

			if (action.equalsIgnoreCase("salvarObservacao"))
			{
				String competencia = request.getParameter("competencia");
				String numcad = request.getParameter("numcad");
				String nomfun = request.getParameter("nomfun");
				String observacao = request.getParameter("observacao");
				String isFalta = request.getParameter("isFalta");
				String neoId = request.getParameter("neoId");
				String horEx = request.getParameter("horEx");
				String minEx = request.getParameter("minEx");
				String horMed = request.getParameter("horTra");
				String minMed = request.getParameter("minTra");
				String descMed = request.getParameter("descMed");

				if (this.salvarObservacao(competencia, numcad, nomfun, observacao, isFalta, neoId, horEx, minEx, horMed, minMed, descMed))
				{
					this.listaObservacoes(competencia, numcad, nomfun, response);
				}

			}
			if (action.equalsIgnoreCase("salvarObservacaoPosto"))
			{
				String obsPosto = request.getParameter("observacao");
				String neoIdPosto = request.getParameter("neoId");
				this.salvarObservacaoPosto(neoIdPosto, obsPosto);

			}

			if (action.equalsIgnoreCase("editarObservacao"))
			{
				String neoId = request.getParameter("neoId");
				this.editarObservacao(neoId, response);
			}

			if (action.equalsIgnoreCase("editarDiaria"))
			{
				String neoId = request.getParameter("neoId");
				this.editarDiarias(neoId, response);
			}

			if (action.equalsIgnoreCase("removerDiaria"))
			{
				String neoId = request.getParameter("neoId");
				String cliente = request.getParameter("cliente");
				String periodo = request.getParameter("periodo");
				if (this.removerDiaria(neoId))
				{
					this.listaDiarias(cliente, periodo, response);
				}
			}

			if (action.equalsIgnoreCase("listaDiarias"))
			{
				String cliente = request.getParameter("cliente");
				String periodo = request.getParameter("competencia");
				this.listaDiarias(cliente, periodo, response);
			}

			if (action.equalsIgnoreCase("salvarColaborador"))
			{
				String campoCargaHorPos = request.getParameter("campoCargaHorPos");
				String campoFuncao = request.getParameter("campoFuncao");
				String campoMatricula = request.getParameter("campoMatricula");
				String campoNome = request.getParameter("campoNome");
				String campoCentroCusto = request.getParameter("campoCentroCusto");
				String campoDatAdm = request.getParameter("campoDatAdm");
				String campoDescMed = request.getParameter("campoDescMed");
				String campoSituacao = request.getParameter("campoSituacao");
				String campoHorTra = request.getParameter("campoHorTra");
				String campoMinTra = request.getParameter("campoMinTra");
				String campoHorEx = request.getParameter("campoHorEx");
				String campoMinEx = request.getParameter("campoMinEx");
				String neoIdPosto = request.getParameter("neoIdPosto");
				String campoLocal = request.getParameter("campoLocal");

				this.salvarColaborador(campoCargaHorPos, campoFuncao, campoMatricula, campoNome, campoCentroCusto, campoDatAdm, campoDescMed, campoSituacao, campoHorTra, campoMinTra, campoHorEx, campoMinEx, neoIdPosto, campoLocal);

			}

			if (action.equalsIgnoreCase("listaEventoX8"))
			{
				String numPos = request.getParameter("numpos");
				String numLoc = request.getParameter("numloc");
				listaEventoX8(numPos, numLoc, response);
			}

			if (action.equalsIgnoreCase("salvarDiaria"))
			{
				String cliente = request.getParameter("cliente");
				String obsDiaria = request.getParameter("obsDia");
				String qtdDiarias = request.getParameter("qtdDia");
				String competencia = request.getParameter("competencia");
				String neoIdDiaria = request.getParameter("neoIdDiaria");
				salvarDiaria(qtdDiarias, obsDiaria, cliente, competencia, neoIdDiaria, response);
			}

			if (action.equalsIgnoreCase("cadastrarPosto"))
			{
				String codCcu = request.getParameter("codCcu");
				String periodo = request.getParameter("periodo");
				String cliente = request.getParameter("cliente");

				cadastrarPosto(codCcu, periodo, cliente, response);
			}

			if (action.equalsIgnoreCase("removerPosto"))
			{
				String neoIdPosto = request.getParameter("neoId");
				if (!removerPosto(neoIdPosto))
				{
					out.print("erro!");
				}
			}

			out.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void salvarDiaria(String qtdDiarias, String obsDiaria, String cliente, String competencia, String neoIdDiaria, HttpServletResponse response) throws IOException
	{
		PrintWriter out = response.getWriter();
		if (OrsegupsMedicaoUtilsMensal.salvarDiaria(cliente, competencia, qtdDiarias, obsDiaria, neoIdDiaria))
		{
			List<DiariaMedicaoVO> diarias = OrsegupsMedicaoUtilsMensal.listaDeDiarias(cliente, competencia);
			Gson gson = new Gson();
			String eventoJson = gson.toJson(diarias);
			out.print(eventoJson);
			out.flush();
			out.close();
		}
	}

	public void listaEventoX8(String numPos, String numLoc, HttpServletResponse response) throws IOException
	{
		PrintWriter out = response.getWriter();
		long numctr = OrsegupsMedicaoUtilsMensal.retornaContrato(Long.parseLong(numPos), Long.parseLong(numLoc));
		List<EventoSigmaVO> eventos = new ArrayList<EventoSigmaVO>();
		eventos = QLPresencaUtils.listaEventoX8(numctr, Long.parseLong(numPos));

		Gson gson = new Gson();
		String eventoJson = gson.toJson(eventos);

		out.print(eventoJson);
		out.flush();
		out.close();
	}

	public void listaDiarias(String cliente, String periodo, HttpServletResponse response) throws IOException
	{
		PrintWriter out = response.getWriter();

		List<DiariaMedicaoVO> diarias = new ArrayList<DiariaMedicaoVO>();
		diarias = OrsegupsMedicaoUtilsMensal.listaDeDiarias(cliente, periodo);
		Gson gson = new Gson();
		String diariasJson = gson.toJson(diarias);
		System.out.println(diariasJson);
		out.print(diariasJson);
		out.flush();
		out.close();
	}

	private boolean salvarObservacao(String competencia, String numcad, String nomfun, String observacao, String isFalta, String neoId, String horEx, String minEx, String horMed, String minMed, String descMed)
	{
		boolean retorno = OrsegupsMedicaoUtilsMensal.salvarObservacao(competencia, numcad, nomfun, observacao, isFalta, neoId, horEx, minEx, horMed, minMed, descMed);
		return retorno;
	}

	private boolean salvarObservacaoPosto(String neoIdPosto, String obsPosto)
	{
		boolean retorno = OrsegupsMedicaoUtilsMensal.salvarObservacaoPosto(neoIdPosto, obsPosto);
		return retorno;
	}

	public void salvarColaborador(String campoCargaHorPos, String campoFuncao, String campoMatricula, String campoNome, String campoCentroCusto, String campoDatAdm, String campoDescMed, String campoSituacao, String campoHorTra, String campoMinTra, String campoHorEx, String campoMinEx, String neoIdPosto, String campoLocal) throws ParseException
	{
		OrsegupsMedicaoUtilsMensal.salvarColaborador(campoCargaHorPos, campoFuncao, campoMatricula, campoNome, campoCentroCusto, campoDatAdm, campoDescMed, campoSituacao, campoHorTra, campoMinTra, campoHorEx, campoMinEx, neoIdPosto, campoLocal);
	}

	private void editarObservacao(String neoId, HttpServletResponse response) throws IOException
	{
		PrintWriter out = response.getWriter();
		ConsideracoesMedicaoVO consideracao = OrsegupsMedicaoUtilsMensal.editarObservacao(neoId);
		Gson gson = new Gson();
		String observacaoJson = gson.toJson(consideracao);
		System.out.println(observacaoJson);
		out.print(observacaoJson);
		out.flush();
		out.close();
	}

	private void editarDiarias(String neoId, HttpServletResponse response) throws IOException
	{
		PrintWriter out = response.getWriter();
		DiariaMedicaoVO diaria = OrsegupsMedicaoUtilsMensal.editarDiarias(neoId);
		Gson gson = new Gson();
		String observacaoJson = gson.toJson(diaria);
		System.out.println(observacaoJson);
		out.print(observacaoJson);
		out.flush();
		out.close();
	}

	private boolean removerColaborador(String neoId)
	{
		boolean retorno = OrsegupsMedicaoUtilsMensal.removerColaborador(neoId);
		return retorno;
	}

	private boolean removerDiaria(String neoId)
	{
		boolean retorno = OrsegupsMedicaoUtilsMensal.removerDiaria(neoId);
		return retorno;
	}

	private boolean removerObservacao(String neoId)
	{
		boolean retorno = OrsegupsMedicaoUtilsMensal.removerObservacao(neoId);
		return retorno;
	}

	private void listaMedicoes(String empresa, String competencia, String cliente, String nOficialContrato, String cbTodos, String cbVagas, String cbFaltas, String cbDemit, String cbOK, String regionalPesquisa, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<MedicaoVO> medicoes = new ArrayList<MedicaoVO>();
		medicoes = OrsegupsMedicaoUtilsMensal.listaMedicoes(empresa, competencia, cliente, cbTodos, cbVagas, cbFaltas, cbDemit, cbOK, nOficialContrato, regionalPesquisa);
		Gson gson = new Gson();
		String colaboradorJson = gson.toJson(medicoes);
		System.out.println(colaboradorJson);
		out.print(colaboradorJson);
		out.flush();
		out.close();
	}

	private void listaObservacoes(String competencia, String numcad, String nomFun, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<ConsideracoesMedicaoVO> consideracoes = new ArrayList<ConsideracoesMedicaoVO>();
		consideracoes = OrsegupsMedicaoUtilsMensal.listaObservacoes(competencia, numcad, nomFun);
		Gson gson = new Gson();
		String colaboradorJson = gson.toJson(consideracoes);
		out.print(colaboradorJson);
		out.flush();
		out.close();
	}

	private void listaCoberturasPosto(String numloc, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<EntradaAutorizadaVO> coberturas = new ArrayList<EntradaAutorizadaVO>();
		coberturas = QLPresencaUtils.listaCoberturaPosto(Long.parseLong(numloc), 203L);

		JSONArray jsonCoberturas = new JSONArray();

		for (EntradaAutorizadaVO cob : coberturas)
		{
			JSONObject jsonCobertura = new JSONObject();
			jsonCobertura.put("tipcob", cob.getDescricaoCobertura());
			jsonCobertura.put("percob", NeoUtils.safeDateFormat(cob.getDataInicial(), "dd/MM/yyyy") + " à " + NeoUtils.safeDateFormat(cob.getDataFinal(), "dd/MM/yyyy"));
			jsonCobertura.put("nomcob", cob.getColaborador().getNumeroEmpresa() + "/" + cob.getColaborador().getNumeroCadastro() + " - " + cob.getColaborador().getNomeColaborador());
			if (cob.getColaboradorSubstituido().getNomeColaborador() != null)
			{
				jsonCobertura.put("nomsub", cob.getColaboradorSubstituido().getNumeroEmpresa() + "/" + cob.getColaboradorSubstituido().getNumeroCadastro() + " - " + cob.getColaboradorSubstituido().getNomeColaborador());
			}
			jsonCobertura.put("obscob", cob.getObservacao());

			jsonCoberturas.put(jsonCobertura);
		}
		System.out.println(jsonCoberturas);
		out.print(jsonCoberturas);
		out.flush();
		out.close();
	}

	private void listaColaboradoresDemitidosDoPosto(String competencia, String numPos, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<ColaboradoresDemitidosPostosMedicaoVO> colDems = new ArrayList<ColaboradoresDemitidosPostosMedicaoVO>();
		colDems = OrsegupsMedicaoUtilsMensal.listaColaboradoresDemitidosDoPosto(competencia, numPos);
		Gson gson = new Gson();
		String coberturaJson = gson.toJson(colDems);
		out.print(coberturaJson);
		out.flush();
		out.close();
	}

	private void listaPostos(String competencia, String cliente, String nomeColaborador, String numeroPosto, String cbTodos, String cbVagas, String cbFaltas, String cbDemit, String cbOK, String numOfi, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		List<PostosMedicaoVO> postos = new ArrayList<PostosMedicaoVO>();
		postos = OrsegupsMedicaoUtilsMensal.listaPostos(competencia, cliente, nomeColaborador, numeroPosto, cbTodos, cbVagas, cbFaltas, cbDemit, cbOK, numOfi);
		Gson gson = new Gson();
		String colaboradorJson = gson.toJson(postos);
		out.print(colaboradorJson);
		out.flush();
		out.close();
	}

	public String cancelaProcessos(String codes, String model, String motivo)
	{

		String xCodes[] = codes.split(",");
		String strCodes = "'000000'";
		Long code = Long.parseLong(codes);
		if (code < 10)
		{
			codes = "00000" + codes;
		}
		else if (code < 100)
		{
			codes = "0000" + codes;
		}
		else if (code < 1000)
		{
			codes = "000" + codes;
		}
		else if (code < 10000)
		{
			codes = "00" + codes;
		}
		else if (code < 100000)
		{
			codes = "0" + codes;
		}
		strCodes = codes;
		/*
		 * for (String x: xCodes){
		 * strCodes += ",'"+x+"'";
		 * }
		 */
		String query = "";

		query = " select neoId from wfprocess   where model_neoid in (select neoId from processmodel pm  where pm.name = (select distinct(pm1.name) from processmodel pm1 where pm1.name = '" + model + "' ) ) and code in  " + "(" + strCodes + ")";

		EntityManager entity = PersistEngine.getEntityManager();
		Integer cont = 1;
		if (entity != null)
		{
			Query q = entity.createNativeQuery(query);
			List<BigInteger> result = (List<BigInteger>) q.getResultList();

			for (BigInteger i : result)
			{
				Long id = i.longValue();

				/**
				 * FIXME Cancelar tarefas.
				 */
				WFProcess proc = PersistEngine.getObject(WFProcess.class, new QLEqualsFilter("neoId", id));

				if (proc != null)
				{
					RuntimeEngine.getProcessService().cancel(proc, motivo);
					System.out.println("[Tela de cancelamento reciclagem] - " + cont + " : Processo " + proc.getCode());
				}
				else
				{
					System.out.println("[Tela de cancelamento reciclagem] - " + cont + " : ERRO: Processo não localizado!");
				}
				cont++;
			}
			return "Cancelamento Processado com Sucesso!";
		}
		else
		{
			return "Nenhuma tarefa encontrada para cancelamento";
		}
	}

	public String removeRegistroURA(String numCpf, GregorianCalendar dataCurso)
	{
		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		groupFilter.addFilter(new QLEqualsFilter("cpf", Long.parseLong(numCpf)));
		groupFilter.addFilter(new QLEqualsFilter("dataFim", (GregorianCalendar) dataCurso));
		List<NeoObject> objs = PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMensagem"), groupFilter, -1, -1, "dataFim desc");
		if ((objs != null) && (objs.size() > 0))
		{
			PersistEngine.remove(objs.get(0));
			System.out.println("Registro da URA removido com Sucesso!");
			return "Registro da URA removido com Sucesso!";
		}
		else
		{
			System.out.println("Nenhum registro para ser removido da URA");
			return "Nenhum registro para ser removido da URA";
		}

	}

	public void cadastrarPosto(String codCcu, String periodo, String cliente, HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();
		String retorno = OrsegupsMedicaoUtilsMensal.cadastrarPosto(codCcu, periodo, cliente);
		JSONObject jSonMensagem = new JSONObject();
		if (!retorno.contains("sucesso"))
		{
			jSonMensagem.put("mensagemErro", retorno);
			out.print(jSonMensagem);
			out.flush();
			out.close();
		}
		else
		{
			jSonMensagem.put("mensagemSucesso", retorno);
			out.print(jSonMensagem);
			out.flush();
			out.close();
		}
	}

	public boolean removerPosto(String neoId)
	{
		return OrsegupsMedicaoUtilsMensal.removerPosto(neoId);
	}

}
