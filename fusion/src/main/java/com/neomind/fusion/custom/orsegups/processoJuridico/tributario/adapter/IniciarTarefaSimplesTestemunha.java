package com.neomind.fusion.custom.orsegups.processoJuridico.tributario.adapter;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.IniciarTarefaSimplesTestemunha

public class IniciarTarefaSimplesTestemunha implements AdapterInterface
{
	@Override
	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
	    try{		
		Integer iTipoProcesso = 0;
		NeoObject tipoProcesso = (NeoObject) processEntity.getValue("tipoProcesso");
    	    	if (tipoProcesso != null) {
    	    	    EntityWrapper wTipoProcesso = new EntityWrapper(tipoProcesso);
    	    	    iTipoProcesso = Integer.parseInt(wTipoProcesso.getValue("codigo").toString());
    	    	}
    	    	
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		String titulo = "";
		String descricao = "";
		String executor = "";
		String solicitante = "rosemeire.svenar";
		GregorianCalendar prazo = null;
		String descricaoRegistro = "";

		List<NeoObject> listaOqueFazerHistorico = (List<NeoObject>) processEntity.findValue("j002HistoricoOqueFazer");
		boolean convocarTest = (boolean) processEntity.getValue("convocarTestemunha");
		if (convocarTest)
		{
		    if (iTipoProcesso == 3) {
			    titulo = "J002 - Tributário" + activity.getCode() + " - Convocar Testemunha";
			} else {
			if (processEntity.getValue("colaborador") != null)
			{
				NeoObject colaborador = (NeoObject) processEntity.getValue("colaborador");
				EntityWrapper wColaborador = new EntityWrapper(colaborador);
				colaborador = (NeoObject) processEntity.getValue("colaborador");
				titulo = "J002 - " + activity.getCode() + " - " + wColaborador.getValue("numemp") + "/" + wColaborador.getValue("numcad") + " - " + wColaborador.getValue("nomfun") + " - Convocar Testemunha";
			}
			else
			{
				NeoObject autorNaoColaborador = (NeoObject) processEntity.getValue("autorNaoCol");
				EntityWrapper wAutor = new EntityWrapper(autorNaoColaborador);
				String cpfAut = String.valueOf(wAutor.getValue("cpfAut"));
				String nomAut = String.valueOf(wAutor.getValue("nomAut"));
				titulo = cpfAut + " / " + nomAut;
			}
		    }

			processEntity.setValue("tituloTarefaSimplesTestemunha", titulo);

			descricao = String.valueOf(processEntity.findValue("corpoTarefaSimplesTestemunha"));

			prazo = (GregorianCalendar) processEntity.findValue("prazoTarefaSimplesTestemunha");

			NeoFile file = (NeoFile) processEntity.findValue("anexoTarefaSimplesTestemunha");

			NeoUser neoExecutor = (NeoUser) processEntity.findValue("executorTarefaSimplesTestemuna");
			executor = neoExecutor.getCode();

			IniciarTarefaSimples tarefaSimples = new IniciarTarefaSimples();
			String code = tarefaSimples.abrirTarefaReturnNeoIdCode(solicitante, executor, titulo, descricao, "1", "Sim", prazo, file);
			try
			{
				String[] neoIdECode = code.split(";");
				System.out.println("Tarefa aberta : " + neoIdECode[1]);
				descricaoRegistro = "Aberto tarefa simples " + neoIdECode[1] + " para convocar a testemunha";
				InstantiableEntityInfo insRegAti = AdapterUtils.getInstantiableEntityInfo("j002HistoricoOqueFazer");
				NeoObject objRegAti = insRegAti.createNewInstance();
				EntityWrapper wRegAti = new EntityWrapper(objRegAti);
				wRegAti.setValue("executorTar", executor);
				wRegAti.setValue("titulo", titulo);
				wRegAti.setValue("descricao", descricao);
				wRegAti.setValue("prazo", prazo);
				wRegAti.setValue("anexo", file);
				wRegAti.setValue("solicitante", solicitante);
				wRegAti.setValue("neoIdTarefa", Long.parseLong(neoIdECode[0]));
				PersistEngine.persist(objRegAti);
				listaOqueFazerHistorico.add(objRegAti);
			}
			catch (Exception e)
			{
				GregorianCalendar dataAtual = new GregorianCalendar();
				if (!OrsegupsUtils.isWorkDay(prazo))
				{
					e.printStackTrace();
					throw new WorkflowException("Prazo informado deve ter um dia útil! ");
				}
				else if ((prazo.before(dataAtual)) || (prazo.equals(dataAtual)))
				{
					e.printStackTrace();
					throw new WorkflowException("Prazo informado deve ser de pelo menos 1 dia!");
				}
				else
				{
					e.printStackTrace();
					throw new WorkflowException("O usuário executor da tarefa de Convocar Testemunha esta inativo!");
				}
			}
		}
		else
		{
			descricaoRegistro = "Não foi realizado a convocação.";
		}
		List<NeoObject> registroAtividade = (List<NeoObject>) processEntity.getValue("j002RegistroHistoricoAtividade");
		String responsavel = origin.returnResponsible();
		GregorianCalendar dataAcao = new GregorianCalendar();
		InstantiableEntityInfo insRegAti = AdapterUtils.getInstantiableEntityInfo("j002RegistroHistoricoAtividade");
		NeoObject objRegAti = insRegAti.createNewInstance();
		EntityWrapper wRegAti = new EntityWrapper(objRegAti);
		wRegAti.setValue("dataAcao", dataAcao);
		wRegAti.setValue("atividade", origin.getActivityName().toString());
		wRegAti.setValue("responsavel", responsavel);
		wRegAti.setValue("descricao", descricaoRegistro);

		PersistEngine.persist(objRegAti);
		registroAtividade.add(objRegAti);

		processEntity.setValue("j002RegistroHistoricoAtividade", registroAtividade);
		processEntity.setValue("convocarTestemunha", false);
		processEntity.setValue("tituloTarefaSimplesTestemunha", "");
		processEntity.setValue("corpoTarefaSimplesTestemunha", "");
		processEntity.setValue("prazoTarefaSimplesTestemunha", null);
		processEntity.setValue("anexoTarefaSimplesTestemunha", null);
		processEntity.setValue("executorTarefaSimplesTestemuna", null);
		processEntity.setValue("j002HistoricoOqueFazer", listaOqueFazerHistorico);
	    }catch(Exception e){
		System.out.println("Erro na classe IniciarTarefaSimplesTestemunha do fluxo J002.");
		e.printStackTrace();
		throw new WorkflowException(e.getMessage());
	    }
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
