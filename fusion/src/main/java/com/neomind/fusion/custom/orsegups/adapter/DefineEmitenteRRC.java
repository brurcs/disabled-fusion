package com.neomind.fusion.custom.orsegups.adapter;

import com.neomind.fusion.custom.orsegups.rsc.helper.RSCHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class DefineEmitenteRRC implements AdapterInterface{

    @Override
    public void start(Task origin, EntityWrapper processEntity, Activity activity) {
	// TODO Auto-generated method stub
	
	String verificaEmitente;
	verificaEmitente = processEntity.findField("emitente").getValueAsString();
	
	if(verificaEmitente == null || verificaEmitente.isEmpty()){
	    //Busca o solicitante
	    NeoPaper papel = new NeoPaper();
	    NeoUser emitente = null;
	    String usuario = "";
	    String papelResponsavelRRC = "";
	    NeoPaper obj = null;
	    
	
	    try{
		papelResponsavelRRC = "SITERESPONSAVELRRC";
		obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", papelResponsavelRRC));
		papel = (NeoPaper) obj;

		if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
		{
		    for (NeoUser user : papel.getUsers())
		    {
			usuario = user.getCode();
			break;
		    }

		}

		emitente = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", usuario));
		
		processEntity.findField("emitente").setValue(emitente);

	    }catch(Exception e){
		System.out.println("Erro ao consultar Papel: " + papelResponsavelRRC + "Adapter: DefineEmitenteRRC");
	    }						
	}
    }

    @Override
    public void back(EntityWrapper processEntity, Activity activity) {

	throw new WorkflowException("Você não pode recusar esta tarefa.");
	
    }

}
