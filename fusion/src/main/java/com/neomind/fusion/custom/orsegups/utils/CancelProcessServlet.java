package com.neomind.fusion.custom.orsegups.utils;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name="CancelProcessServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.utils.CancelProcessServlet"})
public class CancelProcessServlet extends HttpServlet
{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}
	
	private void doRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		String codigoProcesso = req.getParameter("codigoProcesso");
		String codigoCancelamento = req.getParameter("codigoCancelamento");
		String motivoCancelamento = req.getParameter("motivoCancelamento");
		
		String result = OrsegupsUtils.cancelWorkflowProcess(codigoProcesso, codigoCancelamento, motivoCancelamento);
		
		final PrintWriter out = resp.getWriter();
		out.print(result);
		out.close();
	}
}
