package com.neomind.fusion.custom.orsegups.adapter.r001v2;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.e2doc.E2docIndexacaoBean;
import com.neomind.fusion.custom.orsegups.e2doc.E2docUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class EnviarParaGEDV2 implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{
			NeoFile arquivo = (NeoFile) processEntity.findValue("anexoComprovanteBancario");
			
			if (arquivo == null){
				arquivo = (NeoFile) processEntity.findValue("anexoReciboFeriasAssinado");
			}
			
			if (arquivo != null){
				
				GregorianCalendar dataPagamento = (GregorianCalendar) processEntity.getValue("dataPagamentoFerias");
				if (dataPagamento == null)
					dataPagamento = new GregorianCalendar();
				NeoObject colaborador = (NeoObject) processEntity.getValue("primeiraSugestao");
				EntityWrapper wColaborador = new EntityWrapper(colaborador);
				String nomFun = wColaborador.findField("nomfun").getValue().toString();
				String numCad = wColaborador.findField("numcad").getValue().toString();
				String nomEmp = wColaborador.findField("nomemp").getValue().toString();
				String numCpf = wColaborador.findField("numcpf").getValue().toString();
				
				E2docIndexacaoBean e2docIndexacaoBean = new E2docIndexacaoBean();
				
				e2docIndexacaoBean.setEmpresa(nomEmp); 
				e2docIndexacaoBean.setMatricula(numCad);
			    e2docIndexacaoBean.setColaborador(nomFun);
			    e2docIndexacaoBean.setCpf(Long.parseLong(numCpf));
			    e2docIndexacaoBean.setTipoDocumento("FERIAS");
			    e2docIndexacaoBean.setClasse("outros");
			    e2docIndexacaoBean.setData(dataPagamento.getTime());
			    e2docIndexacaoBean.setObservacao(String.valueOf(Calendar.getInstance().getTimeInMillis()));
			    e2docIndexacaoBean.setTipo("i");
			    e2docIndexacaoBean.setModelo("documentação rh");
			    e2docIndexacaoBean.setFormato(arquivo.getSufix());
			    e2docIndexacaoBean.setDocumento(arquivo.getBytes());
			    
			    E2docUtils.indexarDocumento(e2docIndexacaoBean);
			    
			    // Salvar Historico - Registro de Atividades
			    List<NeoObject> registroAtividades = (List<NeoObject>) processEntity.getValue("r001V2RegistroAtividade");		
				
				InstantiableEntityInfo insRegAti = AdapterUtils.getInstantiableEntityInfo("r001V2RegistroAtividades");
				NeoObject objRegAti = insRegAti.createNewInstance();
				EntityWrapper wRegAti = new EntityWrapper(objRegAti);
				
				String observacao = "Enviado para o GED.";
				String usuario = "SISTEMA";

				GregorianCalendar dataAcao = new GregorianCalendar();

				wRegAti.setValue("usuario", usuario);
				wRegAti.setValue("dataAcao", dataAcao);
				wRegAti.setValue("descricao", observacao);
				wRegAti.setValue("anexo", null);
				
				PersistEngine.persist(objRegAti);
				registroAtividades.add(objRegAti);

				processEntity.setValue("r001V2RegistroAtividade", registroAtividades);
				
			} else {
				throw new WorkflowException("Problema ao enviar para o GED. Não consta para este processo o Comprovante Bancário ou Recibo de Férias Assinado!");
			}
			
			
		}
		catch (Exception e)
		{
			System.out.println("Erro na classe IniciarTarefaSimples no Envio para o GED");
			e.printStackTrace();
			throw new WorkflowException(e.getMessage());
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
