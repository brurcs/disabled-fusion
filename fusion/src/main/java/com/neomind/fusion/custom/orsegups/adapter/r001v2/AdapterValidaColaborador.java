package com.neomind.fusion.custom.orsegups.adapter.r001v2;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.presenca.vo.ColaboradorVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EscalaVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.event.TaskFinishEvent;
import com.neomind.fusion.workflow.event.TaskFinishEventListener;
import com.neomind.fusion.workflow.exception.TaskException;
import com.neomind.util.NeoUtils;

public class AdapterValidaColaborador implements TaskFinishEventListener
{

	@Override
	public void onFinish(TaskFinishEvent event) throws TaskException
	{
		EntityWrapper processEntity = event.getWrapper();
		
		Boolean iniViaProAut = (Boolean) processEntity.getValue("iniciadoProcessoAutomatico");
		if (iniViaProAut != null && !iniViaProAut)
		{
			NeoObject colaboradorFerias = (NeoObject) processEntity.findField("colaboradorCoberturaFerias").getValue();
			EntityWrapper wColaborador = new EntityWrapper(colaboradorFerias);
			processEntity.setValue("numempFer", Long.parseLong(String.valueOf(wColaborador.getValue("numemp"))));
			processEntity.setValue("numcadFer", Long.parseLong(String.valueOf(wColaborador.getValue("numcad"))));
			processEntity.setValue("nomfunFer", String.valueOf(wColaborador.getValue("nomfun")));
			processEntity.setValue("codccuFer", String.valueOf(wColaborador.getValue("codccu")));
			processEntity.setValue("titcarFer", String.valueOf(wColaborador.getValue("titred")));

			Connection conn = PersistEngine.getConnection("VETORH");
			StringBuilder sql = new StringBuilder();
			PreparedStatement pstm = null;
			ResultSet rs = null;

			try
			{
				sql.append(" Select top 1 fun.numemp, fun.numcad, fun.nomfun, fun.numcpf, reg.USU_CodReg, reg.USU_NomReg, car.TitRed, ");
				sql.append(" fis.USU_NomFis,cpl.endrua,cpl.endcpl,cpl.endnum,bai.NomBai,cid.NomCid, orn.usu_codccu, orn.usu_lotorn From  R034FUN fun ");
				sql.append("Inner Join R038HLO hlo On hlo.NumEmp = fun.numemp And hlo.tipcol = fun.tipcol And hlo.numcad = fun.numcad And hlo.DatAlt = (Select Max(hlo2.datalt) From R038HLO hlo2 Where hlo2.numemp = hlo.numemp And hlo2.tipcol = hlo.tipcol And hlo2.numcad = hlo.numcad And hlo2.DatAlt <= getdate()) ");
				sql.append("Inner Join R016ORN orn On orn.taborg = hlo.TabOrg And orn.NumLoc = hlo.NumLoc  ");
				sql.append("Inner join USU_T200REG reg On reg.USU_CodReg = orn.usu_codreg  ");
				sql.append("Inner Join R038HCA hca On hca.NumEmp = fun.numemp And hca.tipcol = fun.tipcol And hca.numcad = fun.numcad And hca.DatAlt = (Select Max(hca2.datalt) From R038HCA hca2 Where hca2.numemp = hca.numemp And hca2.tipcol = hca.tipcol And hca2.numcad = hca.numcad And hca2.DatAlt <= getdate()) ");
				sql.append("Inner Join R024CAR car On car.CodCar = fun.codcar And car.EstCar = fun.estcar  ");
				sql.append("Inner JOIN USU_TFisRes fis ON fis.USU_FisRes = fun.usu_fisres ");
				sql.append("Inner join r034cpl cpl on cpl.numcad = fun.numcad and cpl.numemp = fun.numemp and cpl.tipcol = fun.tipcol ");
				sql.append("Inner join R074BAI bai on bai.CodBai = cpl.codbai and bai.CodCid = cpl.codcid ");
				sql.append("Inner join R074CID cid on cid.CodCid = cpl.codcid ");
				sql.append("Where fun.numcad = ? and fun.numemp = ?");
				//			sql.append(" And not exists (select * FROM [CACUPE\\SQL02].fusion_producao.DBO.d_TarefaFeriasProgramadas fp WITH (NOLOCK) ");
				//			sql.append("  			  	 where fp.numcpf = fun.numcpf and cast(floor(cast(fp.datalt as float)) as datetime) = cast(floor(cast(cob.usu_datalt as float)) as datetime) and fp.horini = cob.usu_horini) ");
				pstm = conn.prepareStatement(sql.toString());
				pstm.setLong(1, Long.parseLong(String.valueOf(processEntity.getValue("numcadFer"))));
				pstm.setLong(2, Long.parseLong(String.valueOf(processEntity.getValue("numempFer"))));
				rs = pstm.executeQuery();

				while (rs.next())
				{
					processEntity.setValue("lotacaoFer", rs.getString("usu_lotorn"));
					processEntity.setValue("codregFer", rs.getLong("USU_CodReg"));
					processEntity.setValue("nomregFer", rs.getString("USU_NomReg"));

					GregorianCalendar dataAtual = new GregorianCalendar();
					NeoFile anexo = null;

					if (rs.getLong("numemp") == 1 || rs.getLong("numemp") == 15 || rs.getLong("numemp") == 17 || rs.getLong("numemp") == 18 || rs.getLong("numemp") == 19 || rs.getLong("numemp") == 21)
					{
						anexo = OrsegupsUtils.criaNeoFile(new File("\\\\fsoofs01\\Vetorh\\Relatorios\\Avos de Ferias\\Regionais\\FPPF204COL_Vigilancia_Regional_" + rs.getLong("USU_CodReg") + "_Data_" + NeoUtils.safeDateFormat(dataAtual, "dd-MM-yyyy") + ".pdf"));
					}
					else if (rs.getLong("numemp") == 2 || rs.getLong("numemp") == 6 || rs.getLong("numemp") == 7 || rs.getLong("numemp") == 8 || rs.getLong("numemp") == 22)
					{
						anexo = OrsegupsUtils.criaNeoFile(new File("\\\\fsoofs01\\Vetorh\\Relatorios\\Avos de Ferias\\Regionais\\FPPF204COL_Asseio_Regional_" + rs.getLong("USU_CodReg") + "_Data_" + NeoUtils.safeDateFormat(dataAtual, "dd-MM-yyyy") + ".pdf"));
					}

					if (anexo != null)
					{
						processEntity.setValue("relatorioAvosDeFerias", anexo);
					}
					
					ColaboradorVO colaborador = new ColaboradorVO();
					colaborador.setEscala(new EscalaVO());
					colaborador.setEscala(QLPresencaUtils.getEscalaColaborador(rs.getLong("numcad"), rs.getLong("numemp"), 1L));
					processEntity.setValue("escalaFer", colaborador.getEscala().getDescricao() + "/ T: " + colaborador.getEscala().getCodigoTurma());
					processEntity.setValue("endFer", rs.getString("endrua") + ", Complemento: " + rs.getString("endcpl") + ", Nº: " + rs.getString("endnum"));
					processEntity.setValue("endFer", processEntity.findField("endFer").getValue() + " Bairro: " + rs.getString("NomBai") + " - Cidade: " + rs.getString("NomCid"));
					/*
					GregorianCalendar dataFim = (GregorianCalendar) processEntity.getValue("dataFimCob");
					String dataCor = NeoUtils.safeDateFormat(dataFim, "yyyy-MM-dd");
					HistoricoLocalVO local = new HistoricoLocalVO();
					local.setPosto(new PostoVO());
					local = QLPresencaUtils.listaHistoricoLocal(rs.getLong("numemp"), 1L, rs.getLong("numcad"), dataCor);
					*/
					
				}
				

			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}

		}
		
		InstantiableEntityInfo insRegAti = AdapterUtils.getInstantiableEntityInfo("r001V2RegistroAtividades");
		NeoObject objRegAti = insRegAti.createNewInstance();
		EntityWrapper wRegAti = new EntityWrapper(objRegAti);
		
		String observacao = "";
		String usuario = "SISTEMA";
		if ( processEntity.getValue("iniciadoProcessoAutomatico") != null && (boolean) processEntity.getValue("iniciadoProcessoAutomatico")) {
			observacao = "Iniciado via Rotina FUSION.";
		} else {
			observacao = "Iniciado manualmente.";
			usuario = event.getTask().getFinishByUser().getFullName();
		}

		GregorianCalendar dataAcao = new GregorianCalendar();

		wRegAti.setValue("usuario", usuario);
		wRegAti.setValue("dataAcao", dataAcao);
		wRegAti.setValue("descricao", observacao);
		wRegAti.setValue("anexo", null);
		
		processEntity.findField("r001V2RegistroAtividade").addValue(objRegAti);

	}

}
