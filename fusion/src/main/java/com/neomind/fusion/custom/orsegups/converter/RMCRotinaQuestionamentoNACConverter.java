package com.neomind.fusion.custom.orsegups.converter;

import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class RMCRotinaQuestionamentoNACConverter extends StringConverter
{
	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin)
	{		
		List<NeoObject> lstRotinaQuestionamento = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("RMCRotinaQuestionamentoNACPosVenda"));
		StringBuilder textoTable = new StringBuilder();
		
		if (NeoUtils.safeIsNotNull(lstRotinaQuestionamento) && !lstRotinaQuestionamento.isEmpty())
		{
			textoTable.append("<div style=\"float:left;\">");
			textoTable.append("<label>");			
			textoTable.append("</label>");
			//style=\"margin: 0px; width: 358px; height: 132px;\" 
			
			
			String descricao = "";			
			for (NeoObject obj : lstRotinaQuestionamento)
			{
				EntityWrapper registroWrapper = new EntityWrapper(obj);
				textoTable.append("<label style=\"margin: 0px; width: 358px; height: 132px;\">");
				descricao = (String) registroWrapper.findValue("descricaoRotinaQuestionamentoNAC");
				textoTable.append(NeoUtils.encodeHTMLValue(descricao));
				textoTable.append("</label>");
			}			
			textoTable.append("</div>");
		}
		
		return textoTable.toString();
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}
}
