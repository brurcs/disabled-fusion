package com.neomind.fusion.custom.orsegups.e2doc.engine;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import com.neomind.fusion.custom.orsegups.e2doc.xml.juridico.Pastas;

public class E2docJuridicoV2Engine {
    
    public synchronized Pastas pesquisaJuridicoV2(HashMap<String, String> atributosPesquisa) {

	String pesquisa = "<modelo>jurídico</modelo>";

	int numIndice = 0;
	for (Entry<String, String> entry : atributosPesquisa.entrySet()) {

	    String nomeAtributo = entry.getKey();
	    String valorAtributo = entry.getValue();

	    pesquisa += "<indice" + numIndice + ">" + nomeAtributo + "</indice" + numIndice + "><valor" + numIndice + ">" + valorAtributo + "</valor" + numIndice + ">";
	    numIndice++;

	}

	E2docEngine.verificaValidadeKey("RH");

	String retorno = E2docEngine.pesquisarUsuarioWebServiceDocGET2(pesquisa, "RH");

	retorno = retorno.replaceAll("&", " ");

	Pastas pastas = null;

	if (!retorno.contains("erro")) {
	    pastas = this.tratarRetornoDocumentosJuridicoV2(retorno);
	}
	
	E2docEngine.removeDocumentosTemporarios();
	
	return pastas;
    }
    
    private Pastas tratarRetornoDocumentosJuridicoV2(String retorno) {

	Pastas pastas = null;

	try {
	    JAXBContext jaxbContext = JAXBContext.newInstance(Pastas.class);
	    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	    StreamSource streamSource = new StreamSource(new StringReader(retorno));
	    JAXBElement<Pastas> je = jaxbUnmarshaller.unmarshal(streamSource, Pastas.class);

	    pastas = (Pastas) je.getValue();

	} catch (JAXBException e) {
	    e.printStackTrace();
	}

	return pastas;

    }
    

}
