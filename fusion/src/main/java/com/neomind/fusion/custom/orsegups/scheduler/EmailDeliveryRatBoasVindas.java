package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.MultiPartEmail;
import org.jfree.util.Log;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsEmailUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;

public class EmailDeliveryRatBoasVindas  implements CustomJobAdapter {

	@Override
	public void execute(CustomJobContext ctx) {
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		MailSettings settings = PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());
		String ultimaExecucaoRotina = OrsegupsEmailUtils.ultimaExecucaoRotinaEmail(OrsegupsEmailUtils.MONITOR_EMAIL_RAT_BOAS_VINDAS);
		
		try {
			conn = PersistEngine.getConnection("SIGMA90");

			sql.append(" SELECT c.emailresp, COALESCE(c.responsavel,c.razao,c.fantasia) as NOME, c.razao FROM dbORDEM OS " );
			sql.append(" INNER JOIN dbCENTRAL C ON OS.CD_CLIENTE = C.CD_CLIENTE ");
			sql.append(" WHERE ");
			sql.append(" OS.IDOSDEFEITO IN (134,184)");
			sql.append(" AND OS.FECHAMENTO BETWEEN '" + ultimaExecucaoRotina + "' AND GETDATE() ");
			sql.append(" GROUP BY C.EMAILRESP, C.RESPONSAVEL, C.RAZAO, C.FANTASIA ");

			pstm = conn.prepareStatement(sql.toString());

			OrsegupsEmailUtils.inserirFimRotinaEmail(OrsegupsEmailUtils.MONITOR_EMAIL_RAT_BOAS_VINDAS);
			
			rs = pstm.executeQuery();
					
			while(rs.next()) {

				String emailCliente = rs.getString("EMAILRESP");
				String nomeCliente = rs.getString("NOME");
				
				if(nomeCliente.isEmpty()){
					nomeCliente = rs.getString("RAZAO");
				}
				
				MultiPartEmail email = new HtmlEmail();								
				StringBuilder mensagem = new StringBuilder();
			
				mensagem.append("<table width='100%' bgcolor='#fff' align='center'><tr><td width='100%' bgcolor='#fff' align='center'>");
				mensagem.append("<div style='width: 800;'><img src='https://s3.amazonaws.com/nexti-mkt/newsletters/space.gif' width='1' height='20' style='display: block;'></div>");
				mensagem.append("<table id='Tabela_01' width='800' height='1300' border='0' cellpadding='0' cellspacing='0'><tr><td><img src='http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/boas_vindas/topo.png' width='800' height='221' usemap='#MapCabecalho' style='display: block;' border='0'></td></tr>");
				mensagem.append("<tr><td><div style='font-family: \"Encode Sans\", sans-serif; font-size: 30px; text-align: left; margin-left: 80px; width: 680px;'>"+nomeCliente+"</div></td></tr>");
				mensagem.append("<tr><td><img src='http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/boas_vindas/corpo.png' width='800' height='793' usemap='#MapStores' style='display: block;' border='0'></td></tr>");
				mensagem.append("<tr><td><img src='http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/boas_vindas/rodape.png' width='800' height='230' usemap='#MapRodape' style='display: block;' border='0'></td></tr></table>");
				mensagem.append("</tr></table>");
				mensagem.append("<map name='MapCabecalho'><area shape='rect' coords='685,11,788,136' href='https://www.orsegups.com.br'></map>");
				mensagem.append("<map name='MapStores'><area shape='rect' coords='129,472,351,538' href='https://play.google.com/store/apps/details?id=orsegups.com.br.portaldocliente&hl=pt_BR'><area shape='rect' coords='375,472,597,538' href='https://itunes.apple.com/br/app/portal-orsegups/id1251485372?mt=8'></map>");
				mensagem.append("<map name='MapRodape'>");
				mensagem.append("<area shape='rect' coords='130,38,370,93' href='https://www.orsegups.com.br'>");
				mensagem.append("<area shape='rect' coords='494,94,650,114' href='https://www.orsegups.com.br'>");
				mensagem.append("<area shape='rect' coords='668,94,687,114' href='https://www.youtube.com/user/orsegupssc'>");
				mensagem.append("<area shape='rect' coords='691,94,713,114' href='https://www.twitter.com/orsegups'>");
				mensagem.append("<area shape='rect' coords='717,94,736,114' href='https://www.facebook.com/orsegups'>");
				mensagem.append("<area shape='rect' coords='741,94,762,114' href='https://www.instagram.com/orsegups/?hl=pt-br'>");
				mensagem.append("</map>");


				
				// Envio de e-mail utilizando recurso do Fusion
//				String subject = "Relatório de Boas Vindas";
//				OrsegupsEmailUtils.sendTestEmail(mensagem, subject);
				
				email.addTo(emailCliente);				
				email.addBcc("emailautomatico@orsegups.com.br");
				email.setSubject("Bem vindo a Orsegups!");
				email.setMsg(mensagem.toString());
				email.setFrom(settings.getFromEMail(),settings.getFromName());
				email.setSmtpPort(settings.getPort());
				email.setHostName(settings.getSmtpServer());
				email.send();

			}

		} catch (Exception e) {		
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

	}

}
