package com.neomind.fusion.custom.orsegups.presenca;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;



public class JsonHttpUtil
{

	public static JsonObject readFromJson(String url, String params) throws Exception{
		
			String charset = "UTF8";  // Or in Java 7 and later, use the constant: java.nio.charset.StandardCharsets.UTF_8.name()
			
			
			
		
		    BufferedReader rd  = null;
		    StringBuilder sb = new StringBuilder();
		    String line = null;
		    HttpURLConnection connection = null;
		    try {
		    	//JsonObject job = JsonHttpUtil.readFromJson("http://localhost:8081/OrsegupsBryBridge/assina?act=assinaEnvialEmail");
		    	 System.out.println(url+'?'+params);
		         URL urlObj = new URL(url+'?'+params);
		         connection = (HttpURLConnection) urlObj.openConnection();
		         connection.setRequestMethod("GET");
		         connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401"); 
		         connection.connect();
		         rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		          sb = new StringBuilder();

		          while ((line = rd.readLine()) != null)
		          {
		              sb.append(line + '\n');
		          }
		          System.out.println(sb.toString());
		          
		          JsonObject job = new JsonParser().parse(sb.toString()).getAsJsonObject();
		          		          
		          return job;
		     } catch (MalformedURLException e) {
		         e.printStackTrace();
		         throw new Exception("001-Erro de formação de URL",e);
		     } catch (ProtocolException e) {
		         e.printStackTrace();
		         throw new Exception("002-Erro de protocolo",e);
		     } catch (IOException e) {
		         e.printStackTrace();
		         throw new Exception("003-Falha de comunicação com o servidor",e);
		     }


	}

}
