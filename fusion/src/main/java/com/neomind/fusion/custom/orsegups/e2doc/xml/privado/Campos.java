package com.neomind.fusion.custom.orsegups.e2doc.xml.privado;

public class Campos {
    
    private String i9;
    private String i10;
    private String i11;
    private String i13;
    private String i14;
    private String i71;
    private String i72;
    
    public String getI9() {
        return i9;
    }
    public void setI9(String i9) {
        this.i9 = i9;
    }
    public String getI10() {
        return i10;
    }
    public void setI10(String i10) {
        this.i10 = i10;
    }
    public String getI11() {
        return i11;
    }
    public void setI11(String i11) {
        this.i11 = i11;
    }
    public String getI13() {
        return i13;
    }
    public void setI13(String i13) {
        this.i13 = i13;
    }
    public String getI14() {
        return i14;
    }
    public void setI14(String i14) {
        this.i14 = i14;
    }
    public String getI71() {
	return i71;
    }
    public void setI71(String i71) {
	this.i71 = i71;
    }
    public String getI72() {
	return i72;
    }
    public void setI72(String i72) {
	this.i72 = i72;
    }
    
    
}
