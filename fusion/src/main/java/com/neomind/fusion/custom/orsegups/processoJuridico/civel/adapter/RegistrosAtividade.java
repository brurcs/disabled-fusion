package com.neomind.fusion.custom.orsegups.processoJuridico.civel.adapter;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.event.TaskAssignerEvent;
import com.neomind.fusion.workflow.event.TaskAssignerEventListener;
import com.neomind.fusion.workflow.event.TaskFinishEvent;
import com.neomind.fusion.workflow.event.TaskFinishEventListener;
import com.neomind.fusion.workflow.exception.TaskException;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;


public class RegistrosAtividade implements TaskAssignerEventListener, TaskFinishEventListener
{
	@SuppressWarnings("unchecked")
	@Override
	public void onFinish(TaskFinishEvent event) throws TaskException
	{
		EntityWrapper processEntity = event.getWrapper();

		String mensagemErro = "Erro!";

		Task origin = event.getTask();
		if (origin == null)
			return;

		try
		{
			List<NeoObject> registroAtividade = processEntity.findGenericValue("historicoAtividades");
			List<NeoObject> registroAtividadeArquivo = processEntity.findGenericValue("historicoArquivos");
			List<NeoObject> listaGTCComprovantePgto = processEntity.findGenericValue("ListGTCComprovante");
			List<NeoObject> listaGTCLancar = processEntity.findGenericValue("listaPagamentos");
			List<NeoObject> listaPenalidades = processEntity.findGenericValue("historicoPenalidades");

			String responsavel = origin.returnResponsible();
			String atividade = origin.getActivityName();
			GregorianCalendar dataAcao = new GregorianCalendar();
			Boolean documentacaoCompleta = (Boolean) (processEntity.findGenericValue("documentacaoCompleta") == null ? false : processEntity.findGenericValue("documentacaoCompleta"));
			Boolean avancaPorJOB = (Boolean) (processEntity.findGenericValue("avancaPorJOB") == null ? false : processEntity.findGenericValue("avancaPorJOB"));
			
			processEntity.setValue("atividadeOrigemPenalidade", atividade);

			NeoObject noRegistroAtividade = AdapterUtils.createNewEntityInstance("j002RegistroHistoricoAtividade");
			EntityWrapper wRegistroAtividade = new EntityWrapper(noRegistroAtividade);
			wRegistroAtividade.setValue("dataAcao", dataAcao);
			wRegistroAtividade.setValue("atividade", atividade);
			wRegistroAtividade.setValue("responsavel", responsavel);

			// validar dias uteis para o vencimento.
			for (NeoObject gtcLancar : listaGTCLancar)
			{
				EntityWrapper wLancar = new EntityWrapper(gtcLancar);
				List<NeoObject> listaParcelas = (List<NeoObject>) wLancar.getValue("parcelas");

				for (NeoObject parcela : listaParcelas)
				{
					EntityWrapper wParcela = new EntityWrapper(parcela);
					GregorianCalendar datVen = (GregorianCalendar) wParcela.getValue("vencimento");

					if (!OrsegupsUtils.isWorkDay(datVen))
					{
						if (mensagemErro.equals("Erro!"))
							mensagemErro = "<br>";

						mensagemErro = mensagemErro + "A data " + NeoDateUtils.safeDateFormat(datVen, "dd/MM/yyyy") + ", referente há parcela " + wParcela.getValue("nParcela") + " com valor " + wParcela.getValue("valorParcela") + " não é dia útil.<br>";
					}
				}
			}

			if (!mensagemErro.equals("") && !mensagemErro.equals("Erro!"))
			{
				throw new WorkflowException(mensagemErro);
			}

			if (atividade.contains("Abrir Processo"))
			{
				String descricao = processEntity.findGenericValue("obsProcesso");
				wRegistroAtividade.setValue("descricao", descricao);

				processEntity.setValue("obsProcesso", "");

				System.out.println("Abrir Processo");
			}
			else if (atividade.contains("Validar Documentação"))
			{
				String descricao = processEntity.findGenericValue("obsProcesso");

				if (descricao.equals(""))
					descricao = "Mensagem Automática: Aprovado documentação";

				wRegistroAtividade.setValue("descricao", descricao);

				processEntity.setValue("obsProcesso", "");

				if (documentacaoCompleta)
				{
					inserirHistoricoPenalidade(processEntity, 10, "Elaborar Petição");
					
					GregorianCalendar gcPrazo = adicionarDiasUteis(new GregorianCalendar(), 10);
					processEntity.setValue("prazo", gcPrazo);
				}

				System.out.println("Validar Documentação");
			}
			else if (atividade.contains("Elaborar Petição"))
			{
				String descricao = processEntity.findGenericValue("obsProcesso");
				processEntity.setValue("obsProcesso", "");
				
				if (descricao.equals(""))
					wRegistroAtividade.setValue("descricao", "Mensagem Automática: Elaborou a Petição");
				else
					wRegistroAtividade.setValue("descricao", descricao);

				if (listaPenalidades != null && listaPenalidades.size() > 0)
					if (avancaPorJOB)
						gerarPenalidade(listaPenalidades, new BigDecimal(1), atividade);
					else
						gerarPenalidade(listaPenalidades, new BigDecimal(0), atividade);
				

				List<NeoObject> listAnexosPeticao = processEntity.findGenericValue("anexosPeticao");
				for (NeoObject noAnexo : listAnexosPeticao)
				{
					EntityWrapper wAnexo = new EntityWrapper(noAnexo);
					
					NeoObject noRegistroArquivo = AdapterUtils.createNewEntityInstance("j002RegistroArquivos");
					EntityWrapper wRegistroArquivo = new EntityWrapper(noRegistroArquivo);

					wRegistroArquivo.setValue("dtAnexo", new GregorianCalendar());
					wRegistroArquivo.setValue("titulo", String.valueOf(wAnexo.getValue("titulo")));
					wRegistroArquivo.setValue("respAnexo", responsavel);
					wRegistroArquivo.setValue("anexo", (NeoFile) wAnexo.getValue("arquivo"));

					registroAtividadeArquivo.add(noRegistroArquivo);
				}
				
				processEntity.setValue("anexosPeticao", null);
			}
			else if (atividade.contains("Complementar Guia"))
			{
				String descricao = processEntity.findGenericValue("obsProcesso");

				wRegistroAtividade.setValue("descricao", descricao);

				processEntity.setValue("obsProcesso", "");
				System.out.println("Complementar Guia");
			}
			else if (atividade.contains("Finalizar Processo jurídico"))
			{
				String descricao = String.valueOf(processEntity.getValue("obsFinalizarProcesso"));
				wRegistroAtividade.setValue("descricao", descricao);
				System.out.println("Finalizar Processo jurídico");
			}
			else if (atividade.contains("Considerações finais sobre o processo"))
			{
				String descricao = String.valueOf(processEntity.getValue("obsFinalProcesso"));
				wRegistroAtividade.setValue("descricao", descricao);
				System.out.println("Considerações finais sobre o processo");
			}
			else if (atividade.contains("Lançar GTCs"))
			{
				String descricao = "GTC(s) lançada(s) com sucesso! Aguardando comprovante(s) de pagamento.";
				wRegistroAtividade.setValue("descricao", descricao);
				System.out.println("Lançar GTCs");
			}
			else if (atividade.contains("Aprovar Pagamento"))
			{
				String descricao = processEntity.findGenericValue("obsProcesso");
				processEntity.setValue("obsProcesso", "");
				
				if (descricao.equals(""))
					wRegistroAtividade.setValue("descricao", "Mensagem Automática: Pagamento Aprovado");
				else
					wRegistroAtividade.setValue("descricao", descricao);
			}
			else if (atividade.contains("Tomar Ciência Pagamento"))
			{
				wRegistroAtividade.setValue("descricao", "Mensagem Automática: Presidência - Tomar Ciência - Pagamento");
				System.out.println("Tomar Ciência - Pagamento");
			}
			else if (atividade.contains("Inserir Dados do Processo"))
			{
				inserirHistoricoPenalidade(processEntity, 90, "Movimentar Processo");
				
				GregorianCalendar gcPrazo = adicionarDiasUteis(new GregorianCalendar(), 90);
				processEntity.setValue("prazo", gcPrazo);

				if (listaPenalidades != null && listaPenalidades.size() > 0)
					if (avancaPorJOB)
						gerarPenalidade(listaPenalidades, new BigDecimal(0.10), atividade);
					else
						gerarPenalidade(listaPenalidades, new BigDecimal(0), atividade);

				wRegistroAtividade.setValue("descricao", "Mensagem Automática: Escritório de Advocacia - Inserir Dados do Processo");
			}
			else if (atividade.contains("Aprovar Defesa"))
			{

				boolean isTributario = false;
				NeoObject tipoProcesso = (NeoObject) processEntity.getValue("tipoProcesso");
				if (tipoProcesso != null)
				{
					EntityWrapper wTipoProcesso = new EntityWrapper(tipoProcesso);
					isTributario = (boolean) (Integer.parseInt(wTipoProcesso.getValue("codigo").toString()) == 3) ? true : false;
				}
				if (!isTributario)
				{
					String descricao = String.valueOf(processEntity.getValue("obsDefApr"));
					wRegistroAtividade.setValue("descricao", descricao);
					processEntity.setValue("obsDefApr", "");
					System.out.println("Aprovar Defesa");
				}
			}

			else if (atividade.contains("Peticionar a Defesa"))
			{
				String descricao = String.valueOf(processEntity.getValue("observacao"));
				wRegistroAtividade.setValue("descricao", descricao);
				processEntity.setValue("observacao", "");
				processEntity.setValue("listaAcaoPro", null);
				System.out.println("Peticionar a Defesa");

			}
			else if (atividade.contains("Movimentar Processo"))
			{
				QLRawFilter raw = new QLRawFilter("codccu = " + processEntity.findGenericValue("centroCusto"));
				NeoObject sapiensCCU = PersistEngine.getObject(AdapterUtils.getEntityClass("SAPIENSE044CCU"), raw);
				if (sapiensCCU == null)
				{
					mensagemErro = "Centro de Custo é Inválido";
					//throw new WorkflowException(mensagemErro);
				}
				else
				{
					EntityWrapper e = new EntityWrapper(sapiensCCU);
					// System.out.println(e.findValue("acerat"));
					if (e.findValue("acerat").equals("N"))
					{
						mensagemErro = "Centro de Custo está Inativo";
						throw new WorkflowException(mensagemErro);
					}
					else
					{
						if (e.findValue("acerat").equals("S"))
						{

							String descricao = String.valueOf(processEntity.getValue("obsProcesso"));
							wRegistroAtividade.setValue("descricao", descricao);
							processEntity.setValue("obsProcesso", "");
							
							NeoObject acao = processEntity.findGenericValue("acao");
							EntityWrapper wAcao = new EntityWrapper(acao);
							wRegistroAtividade.setValue("atividade", atividade + " - <b>Ação : " + wAcao.getValue("acao") + " </b>");
						}
					}
				}

				if (listaPenalidades != null && listaPenalidades.size() > 0)
					if (avancaPorJOB)
						gerarPenalidade(listaPenalidades, new BigDecimal(0.10), atividade);
					else
						gerarPenalidade(listaPenalidades, new BigDecimal(0), atividade);
			}
			else if (atividade.contains("Enviar Comprovante de Pagamento"))
			{
				for (NeoObject comprovantes : listaGTCComprovantePgto)
				{
					EntityWrapper wComprovante = new EntityWrapper(comprovantes);
					if (wComprovante.getValue("comprovante") == null)
					{
						mensagemErro = "É Preciso anexar o comprovante de pagamento em todas as GTCs";
						throw new WorkflowException(mensagemErro);
					}
					else
					{
						NeoFile file = (NeoFile) wComprovante.getValue("comprovante");
						file.setName(file.getName().replaceAll(",", "."));
					}
				}
				for (NeoObject comprovantes : listaGTCComprovantePgto)
				{
					EntityWrapper wComprovante = new EntityWrapper(comprovantes);
					
					QLGroupFilter groupFilter = new QLGroupFilter("AND");
					groupFilter.addFilter(new QLEqualsFilter("neoIdGtc", (Long) wComprovante.findGenericValue("neoId")));

					List<NeoObject> objsHistorico = PersistEngine.getObjects(AdapterUtils.getEntityClass("j002HistoricoGTCs"), groupFilter);
					if (objsHistorico != null && objsHistorico.size() > 0)
					{
						EntityWrapper wRegistroHistorico = new EntityWrapper(objsHistorico.get(0));
						wRegistroHistorico.setValue("StatusGTC", "Finalizado");
						PersistEngine.persist(objsHistorico.get(0));
					}
					
					NeoFile file = (NeoFile) wComprovante.getValue("comprovante");
					
					NeoObject objReArq = AdapterUtils.createNewEntityInstance("j002RegistroArquivos");
					EntityWrapper wRegArq = new EntityWrapper(objReArq);

					wRegArq.setValue("dtAnexo", new GregorianCalendar());
					wRegArq.setValue("titulo", file.getName());
					wRegArq.setValue("respAnexo", responsavel);
					wRegArq.setValue("anexo", file);
					
					registroAtividadeArquivo.add(objReArq);
				}
				
				String descricao = "Anexado comprovante de pagamento na(s) GTC(s).";
				wRegistroAtividade.setValue("descricao", descricao);
				
				processEntity.setValue("ListGTCComprovante", null);

				inserirHistoricoPenalidade(processEntity, 5, "Inserir Dados do Processo");
				
				GregorianCalendar gcPrazo = adicionarDiasUteis(new GregorianCalendar(), 5);
				processEntity.setValue("prazo", gcPrazo);
			}

			registroAtividade.add(noRegistroAtividade);

			// Historico de arquivos
			List<NeoObject> arquivos = processEntity.findGenericValue("listaArquivos");
			if (arquivos != null && !((atividade.equalsIgnoreCase("Abrir Processo") || (atividade.equalsIgnoreCase("Validar Documentação") && !documentacaoCompleta))))
			{
				if (atividade.equalsIgnoreCase("Validar Documentação"))
					responsavel = origin.getProcess().getRequester().getName();

				for (NeoObject arquivo : arquivos)
				{
					EntityWrapper wArquivo = new EntityWrapper(arquivo);
					
					NeoObject objReArq = AdapterUtils.createNewEntityInstance("j002RegistroArquivos");
					EntityWrapper wRegArq = new EntityWrapper(objReArq);

					wRegArq.setValue("dtAnexo", new GregorianCalendar());
					wRegArq.setValue("titulo", String.valueOf(wArquivo.getValue("titulo")));
					wRegArq.setValue("respAnexo", responsavel);
					wRegArq.setValue("anexo", (NeoFile) wArquivo.getValue("arquivo"));

					registroAtividadeArquivo.add(objReArq);
				}

				processEntity.setValue("listaArquivos", null);
			}
		}
		catch (Exception e)
		{
			System.out.println("Erro na classe RegistrosAtividade do fluxo J004.");
			e.printStackTrace();
			throw new WorkflowException(e.getMessage() + "#####" + mensagemErro);
		}
	}

	@Override
	public void onAssign(TaskAssignerEvent event) throws TaskException
	{
		String atividade = event.getActivity().getModel().getName();

		EntityWrapper processEntity = event.getWrapper();
		processEntity.setValue("avancaPorJOB", false);
		processEntity.setValue("atividadeAtual", atividade);
	}

	private void gerarPenalidade(List<NeoObject> listaPenalidades, BigDecimal percentual, String atividade)
	{
		//ordena os registros pelo neoId para buscar o ultimo registro da atividade
		Collections.sort(listaPenalidades, new Comparator<NeoObject>()
		{
			@Override
			public int compare(NeoObject o1, NeoObject o2)
			{
				return o1.getNeoId() > o2.getNeoId() ? -1 : 1;
			}
		});

		for (NeoObject noRegistroPenalidade : listaPenalidades)
		{
			EntityWrapper wRegistroPenalidade = new EntityWrapper(noRegistroPenalidade);
			if (wRegistroPenalidade.findGenericValue("descricao").equals(atividade))
			{
				wRegistroPenalidade.setValue("dataExecutado", new GregorianCalendar());
				wRegistroPenalidade.setValue("percentualReducao", percentual);

				return;
			}
		}
	}

	private void inserirHistoricoPenalidade(EntityWrapper processEntity, Integer prazo, String descricao)
	{
		GregorianCalendar gcToday = new GregorianCalendar();
		GregorianCalendar gcDataPrazo = new GregorianCalendar();
		
		gcDataPrazo = adicionarDiasUteis(gcDataPrazo, prazo);

		NeoObject noRegistroPenalidade = AdapterUtils.createNewEntityInstance("j003HistoricoPenalidades");
		EntityWrapper wRegistroPenalidade = new EntityWrapper(noRegistroPenalidade);

		wRegistroPenalidade.setValue("dataRecebido", gcToday);
		wRegistroPenalidade.setValue("dataPrazo", gcDataPrazo);
		wRegistroPenalidade.setValue("descricao", descricao);
		wRegistroPenalidade.setValue("percentualReducao", new BigDecimal(0));

		processEntity.findField("historicoPenalidades").addValue(noRegistroPenalidade);
	}
	
	private GregorianCalendar adicionarDiasUteis(GregorianCalendar date, Integer dias)
	{
		for (int i = 0; i < dias;)
		{
			date.add(Calendar.DAY_OF_MONTH, 1);
			if (OrsegupsUtils.isWorkDayCalendarioJuridico(date))
				i++;
		}
		
		return date;
	}

}
