package com.neomind.fusion.custom.orsegups.medicaoContratos;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class GeraApontamentoNoSapiens

{
	public static String GerarApontamentos(String perRefString, String codEmpString, String numCtrString) throws ParseException
	{
		GregorianCalendar perRef = new GregorianCalendar();
		perRef = OrsegupsUtils.stringToGregorianCalendar(perRefString, "dd/MM/yyyy");
		GregorianCalendar cptAtu = (GregorianCalendar) perRef.clone();
		perRefString = NeoDateUtils.safeDateFormat(cptAtu, "yyyy-MM");
		perRefString += "-01";
		int contRegistro = 0;
		Long codEmpFilter = Long.parseLong(codEmpString);
		QLEqualsFilter periodo = new QLEqualsFilter("periodo", NeoDateUtils.safeDateFormat(perRef, "MM/yyyy"));
		QLEqualsFilter empresa = new QLEqualsFilter("codEmpresaContratada", codEmpFilter);
		QLEqualsFilter geradoApo = new QLEqualsFilter("gerCms", Boolean.FALSE);
		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		if (!numCtrString.isEmpty() && !numCtrString.equals("0"))
		{
			QLRawFilter contrato = new QLRawFilter("contrato in (" + numCtrString + ")");
			groupFilter.addFilter(contrato);
		}

		groupFilter.addFilter(periodo);
		groupFilter.addFilter(empresa);
		groupFilter.addFilter(geradoApo);

		List<NeoObject> objs = PersistEngine.getObjects(AdapterUtils.getEntityClass("MEDMedicaoDeContratos"), groupFilter);
		String retornoString = "";
		for (NeoObject neoObject : objs)
		{
			EntityWrapper medWrapper = new EntityWrapper(neoObject);
			List<NeoObject> postos = (List) medWrapper.findField("listaPostos").getValues();
			long numCtr = (long) medWrapper.findField("contrato").getValue();
			long codEmp = (long) medWrapper.findField("codEmpresaContratada").getValue();
			Long neoIdCtr = (long) medWrapper.findField("neoId").getValue();
			//Preenche os valores dos contratos

			//Verifica as diarias contidas em cada contrato.
			List<NeoObject> diarias = null;
			if (medWrapper.findField("listaDiarias").getValue() != null)
			{
				diarias = (List<NeoObject>) medWrapper.findField("listaDiarias").getValue();
				if (diarias.size() < 1)
				{
					diarias = null;
				}
			}
			//Inserir as diarias.
			if (diarias != null)
			{
				for (NeoObject diaria : diarias)
				{
					EntityWrapper diariaWrapper = new EntityWrapper(diaria);
					BigDecimal qtdDiarias = (BigDecimal) diariaWrapper.findField("qtdDiarias").getValue();
					double qtdDiaria = Double.parseDouble(qtdDiarias.toString());
					String obsDia = (String) diariaWrapper.findField("obsDiaria").getValue();
					int qtdExecucoes = geraDiaria(numCtr, codEmp, qtdDiaria, obsDia);
					if (qtdExecucoes > 0)
					{
						Long neoIdDia = (Long) diariaWrapper.findField("neoId").getValue();
						registraGeracaoApontamentoDiaria(neoIdDia);
						contRegistro++;
					}
					else
					{
						retornoString += "\n\nNenhum posto de diária encontrado para o contrato " + numCtr;
					}
				}
			}

			for (NeoObject posto : postos)
			{
				EntityWrapper postoWrapper = new EntityWrapper(posto);
				List<NeoObject> colObjs = (List<NeoObject>) postoWrapper.findField("listaColaboradores").getValues();
				long qtdVagas = (long) postoWrapper.findField("nVagas").getValue();
				long qtdColab = (long) postoWrapper.findField("nColaboradores").getValue();
				long numPos = (long) postoWrapper.findField("numPos").getValue();

				if (qtdVagas > qtdColab)
				{
					//Chamar metodo para inserir bonificação
				}

				for (NeoObject neoObject2 : colObjs)
				{
					Long contFaltas = 0L;
					EntityWrapper colWrapper = new EntityWrapper(neoObject2);
					Boolean gerCms = (Boolean) colWrapper.findField("gerCms").getValue();

					if (!gerCms)
					{
						String contFaltasString = (String) colWrapper.findField("descMed").getValue();
						contFaltas = Long.parseLong(contFaltasString.replace(" Dia(s)", "").trim());
						String codCcu = (String) colWrapper.findField("codCcu").getValue();
						Connection conn = null;
						PreparedStatement pstm = null;
						ResultSet rs = null;
						StringBuilder sql = new StringBuilder();
						GregorianCalendar datAtu = new GregorianCalendar();
						String horGer = NeoDateUtils.safeDateFormat(datAtu, "HH");
						Long minGer = Long.parseLong(NeoDateUtils.safeDateFormat(datAtu, "mm"));
						Long nHorGer = Long.parseLong(horGer);
						nHorGer = (nHorGer * 60) + minGer;
						Long neoId = (Long) colWrapper.findField("neoId").getValue();
						//HE
						long horEX = 0;
						long minEX = 0;
						if (colWrapper.findField("horEx").getValue() != null)
						{
							horEX = (long) colWrapper.findField("horEx").getValue();
						}
						if (colWrapper.findField("minEx").getValue() != null)
						{

							minEX = (long) colWrapper.findField("minEx").getValue();
						}

						if (horEX > 0 || minEX > 0)
						{
							contRegistro += geraApontamentoHE(numCtr, codEmp, horEX, minEX);
						}
						if (contFaltas < 30)
						{
							try
							{
								conn = PersistEngine.getConnection("SAPIENS");
								sql.append(" INSERT INTO usu_t160cms");
								sql.append("  (usu_codemp,usu_codfil,usu_numctr,usu_numpos,usu_seqmov,usu_datmov,usu_codser,usu_cplcvs,usu_unimed,usu_qtdfun,");
								sql.append("  usu_qtdcvs,usu_preuni,usu_periss,usu_perirf,usu_perins,usu_perpit,usu_percsl,usu_percrt,usu_perour,");
								sql.append("  usu_ctafin,usu_ctared,usu_codccu,usu_datini,usu_datfim,usu_tnsser,usu_usuger,usu_datger,usu_horger,");
								sql.append("  usu_filnfv,usu_codsnf,usu_numnfv,usu_datcpt,usu_adcsub,usu_obscms,usu_diatra,usu_posbon,usu_libapo,");
								sql.append("  usu_tipapo,usu_tarfus,usu_cptcom,usu_jusapo)");
								sql.append(" SELECT TOP 1 usu_codemp,usu_codfil,usu_numctr,usu_numpos,isnull((SELECT MAX(USU_SEQMOV) FROM usu_t160cms),0)+1 AS USU_SEQMOV,REPLACE(CONVERT(varchar,GETDATE(),111),'/','-')+' 00:00:000' AS USU_DATMOV,");
								sql.append("  usu_codser,usu_cplcvs,usu_unimed,usu_qtdfun,1,usu_preuni,usu_periss,usu_perirf,usu_perins,usu_perpit,usu_percsl,usu_percrt,usu_perour,usu_ctafin,usu_ctared,usu_codccu,");
								sql.append("  usu_datini,usu_datfim,usu_tnsser,1166,REPLACE(CONVERT(varchar,GETDATE(),111),'/','-')+' 00:00:000' AS USU_DATGER," + nHorGer + ",usu_filnfv,usu_codsnf,usu_numnfv,'" + perRefString + "','+',");
								sql.append("  'Contrato por medição'," + contFaltas + ",'N','D','M','','1900-12-31 00:00:00.000','S' ");
								sql.append(" FROM USU_T160CVS P WHERE P.USU_NUMCTR = " + numCtr + " AND P.USU_CODEMP = " + codEmp + " AND P.USU_NUMPOS = " + numPos + " AND (P.USU_SITCVS = 'A' OR P.USU_SITCVS = 'I' AND P.USU_DATFIM >= " + perRefString + ")");
								sql.append(" ORDER BY usu_qtdfun DESC");
								pstm = conn.prepareStatement(sql.toString());
								int retorno = pstm.executeUpdate();
								if (retorno > 0)
								{
									contRegistro++;
									if (!registraGeracaoApontamento(neoId))
									{
										System.out.println("Para o contrato " + numCtr + " empresa " + codEmp + " posto " + numPos + " não foi atualizado o eform do colaborador");
									}
									else
									{
										//HT
										long horTra = 0;
										long minTra = 0;
										String carHorPos = String.valueOf(colWrapper.findField("cargaHorPos").getValue());
										if (colWrapper.findField("horMed").getValue() != null)
										{
											horTra = (long) colWrapper.findField("horMed").getValue();
										}
										if (colWrapper.findField("minMed").getValue() != null)
										{

											minTra = (long) colWrapper.findField("minMed").getValue();
										}

										if (horTra > 0 || minTra > 0)
										{
											validaCargaHorariaApontamentoHorasFaltante(numCtr, numPos, codEmp, perRefString, horTra, minTra, carHorPos);
										}

									}
								}
								else
								{
									retornoString += "\n\nPara o contrato " + numCtr + " empresa " + codEmp + " posto " + numPos + " não houve apontamentos inseridos";
								}
							}
							catch (Exception e)
							{
								retornoString = "Não foi possível inserir o(s) apontamento(s) no sapiens, por gentileza entrar em contato com o administrador do sistema.";
								e.printStackTrace();
							}
							finally
							{
								OrsegupsUtils.closeConnection(conn, pstm, rs);
							}
						}
						else
						{
							if (!registraGeracaoApontamento(neoId))
							{
								System.out.println("Para o contrato " + numCtr + " empresa " + codEmp + " CCU " + codCcu + " não foi atualizado o eform do colaborador");
							}
						}
					}
				}
			}
			registraGeracaoApontamentoContrato(neoIdCtr);

		}

		if (contRegistro == 0)
		{
			retornoString += "\n\nNão há apontamentos a serem lançados na competência " + NeoDateUtils.safeDateFormat(cptAtu, "MM/yyyy") + " para a Empresa: " + codEmpString + ".";
		}

		if (!retornoString.contains("Para o contrato") && !retornoString.contains("Nenhum posto de diária encontrado para o contrato") && !retornoString.contains("Não há apontamentos a serem lançados na competência"))
		{
			defineValoresContratos(cptAtu, codEmpFilter);
			retornoString = "Apontamentos gerados com sucesso!";
		}
		else
		{
			if (!retornoString.contains("Não há apontamentos a serem lançados na competência"))
			{
				defineValoresContratos(cptAtu, codEmpFilter);
				retornoString += "\n\nDemais apontamentos gerados com sucesso!";
			}
		}
		return retornoString;
	}

	public static boolean registraGeracaoApontamento(Long neoId)
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		try
		{
			conn = PersistEngine.getConnection("");
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE D_MEDColaboradores SET gerCms = ? where neoId = ?");
			pstm = conn.prepareStatement(sql.toString());
			pstm.setLong(1, 1);
			pstm.setLong(2, neoId);
			int retorno = pstm.executeUpdate();
			if (retorno == 0)
			{
				return false;
			}
			else
			{
				return true;
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

		return false;
	}

	public static boolean registraGeracaoApontamentoContrato(Long neoId)
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		try
		{
			conn = PersistEngine.getConnection("");
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE D_MEDMedicaoDeContratos SET gerCms = ? where neoId = ?");
			pstm = conn.prepareStatement(sql.toString());
			pstm.setLong(1, 1);
			pstm.setLong(2, neoId);
			int retorno = pstm.executeUpdate();
			if (retorno == 0)
			{
				return false;
			}
			else
			{
				return true;
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

		return false;
	}

	public static boolean registraGeracaoApontamentoDiaria(Long neoId)
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		try
		{
			conn = PersistEngine.getConnection("");
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE D_MEDDiarias SET gerCmsDia = ? where neoId = ?");
			pstm = conn.prepareStatement(sql.toString());
			pstm.setLong(1, 1);
			pstm.setLong(2, neoId);
			int retorno = pstm.executeUpdate();
			if (retorno == 0)
			{
				return false;
			}
			else
			{
				return true;
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

		return false;
	}

	public static void geraBonificacao(long numCtr, long numPos, long codEmp, long numBon)
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		GregorianCalendar datCpt = new GregorianCalendar();
		datCpt.set(GregorianCalendar.DATE, 1);
		String datCptString = NeoDateUtils.safeDateFormat(datCpt, "yyyy-MM-dd");
		String horGer = NeoDateUtils.safeDateFormat(datCpt, "HH");
		Long minGer = Long.parseLong(NeoDateUtils.safeDateFormat(datCpt, "mm"));
		Long nHorGer = Long.parseLong(horGer);
		nHorGer = (nHorGer * 60) + minGer;
		try
		{
			conn = PersistEngine.getConnection("SAPIENS");

			sql.append(" INSERT INTO usu_t160cms");
			sql.append("  (usu_codemp,usu_codfil,usu_numctr,usu_numpos,usu_seqmov,usu_datmov,usu_codser,usu_cplcvs,usu_unimed,usu_qtdfun,");
			sql.append("  usu_qtdcvs,usu_preuni,usu_periss,usu_perirf,usu_perins,usu_perpit,usu_percsl,usu_percrt,usu_perour,");
			sql.append("  usu_ctafin,usu_ctared,usu_codccu,usu_datini,usu_datfim,usu_tnsser,usu_usuger,usu_datger,usu_horger,");
			sql.append("  usu_filnfv,usu_codsnf,usu_numnfv,usu_datcpt,usu_adcsub,usu_obscms,usu_diatra,usu_posbon,usu_libapo,");
			sql.append("  usu_tipapo,usu_tarfus,usu_cptcom,usu_jusapo)");
			sql.append(" SELECT TOP 1 usu_codemp,usu_codfil,usu_numctr,usu_numpos,(SELECT MAX(USU_SEQMOV) FROM usu_t160cms)+1 AS USU_SEQMOV,REPLACE(CONVERT(varchar,GETDATE(),111),'/','-')+' 00:00:000' AS USU_DATMOV,");
			sql.append("  usu_codser,usu_cplcvs,usu_unimed,usu_qtdfun,usu_qtdcvs,usu_preuni,usu_periss,usu_perirf,usu_perins,usu_perpit,usu_percsl,usu_percrt,usu_perour,usu_ctafin,usu_ctared,usu_codccu,");
			sql.append("  usu_datini,usu_datfim,usu_tnsser,1166,REPLACE(CONVERT(varchar,GETDATE(),111),'/','-')+' 00:00:000' AS USU_DATGER," + nHorGer + ",usu_filnfv,usu_codsnf,usu_numnfv,'" + datCptString + "','+',");
			sql.append("  'Contrato por medição'," + 30 + ",'N','D','M','','1900-12-31 00:00:00.000','S' ");
			sql.append(" FROM USU_T160CVS P WHERE P.USU_NUMCTR = " + numCtr + " AND P.USU_CODEMP = " + codEmp + " AND P.USU_NUMPOS = " + numPos + " AND (P.USU_SITCVS = 'A' OR P.USU_SITCVS = 'I' AND P.USU_DATFIM >= " + datCptString + ")");
			sql.append(" ORDER BY usu_qtdfun DESC");
			for (int i = 0; i < numBon; i++)
			{
				pstm.addBatch();
			}
			pstm = conn.prepareStatement(sql.toString());
			pstm.executeBatch();
		}
		catch (SQLException e)
		{
			System.out.println("[MED] - ###ERRO - não foi realizado a inserção da bonificação referente ao contrato:" + numCtr + " - posto: " + numPos + "");
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
	}

	public static int geraApontamentoHE(long numCtr, long codEmp, long qtdHorEx, long qtdMinEX)
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		GregorianCalendar datCpt = new GregorianCalendar();
		datCpt.set(GregorianCalendar.DATE, 1);
		datCpt.set(GregorianCalendar.MONTH, -1);
		String datCptString = NeoDateUtils.safeDateFormat(datCpt, "yyyy-MM-dd");
		String horGer = NeoDateUtils.safeDateFormat(datCpt, "HH");
		Long minGer = Long.parseLong(NeoDateUtils.safeDateFormat(datCpt, "mm"));
		Long nHorGer = Long.parseLong(horGer);
		nHorGer = (nHorGer * 60) + minGer;
		double qtdHor50 = 0;
		double qtdHor100 = 0;
		BigDecimal qtdHoras = new BigDecimal(0);
		int qtdExecucoes = 0;
		double qtdMinExtra = qtdMinEX / 100.00;
		qtdHorEx = qtdHorEx - 1;
		//Verificar se existe mais de um posto para as dias, caso existe, o de menor valor são para horas de 50% (até 40hs), o de maior valor para 100%
		int qtdPostos = qtdPostosServicoExtra(numCtr, datCpt);

		if (qtdPostos == 0)
		{
			String ordem = "ASC";
			if (qtdHorEx + qtdMinExtra > 40)
			{
				qtdHor50 = 40;
				qtdHor100 = qtdHorEx - 40;
			}
			else
			{
				qtdHor50 = qtdHorEx + (qtdMinEX / 60.0);
			}

			qtdHoras = new BigDecimal(qtdHor50).setScale(3, BigDecimal.ROUND_HALF_UP);
			for (int i = 0; i < 2; i++)
			{
				if (qtdHoras.compareTo(BigDecimal.ZERO) > 0)
				{
					try
					{
						conn = PersistEngine.getConnection("SAPIENS");

						sql.append(" INSERT INTO usu_t160cms");
						sql.append("  (usu_codemp,usu_codfil,usu_numctr,usu_numpos,usu_seqmov,usu_datmov,usu_codser,usu_cplcvs,usu_unimed,usu_qtdfun,");
						sql.append("  usu_qtdcvs,usu_preuni,usu_periss,usu_perirf,usu_perins,usu_perpit,usu_percsl,usu_percrt,usu_perour,");
						sql.append("  usu_ctafin,usu_ctared,usu_codccu,usu_datini,usu_datfim,usu_tnsser,usu_usuger,usu_datger,usu_horger,");
						sql.append("  usu_filnfv,usu_codsnf,usu_numnfv,usu_datcpt,usu_adcsub,usu_obscms,usu_diatra,usu_posbon,usu_libapo,");
						sql.append("  usu_tipapo,usu_tarfus,usu_cptcom,usu_jusapo)");
						sql.append(" SELECT TOP 1 usu_codemp,usu_codfil,usu_numctr,usu_numpos,(SELECT MAX(USU_SEQMOV) FROM usu_t160cms)+1 AS USU_SEQMOV,REPLACE(CONVERT(varchar,GETDATE(),111),'/','-')+' 00:00:000' AS USU_DATMOV,");
						sql.append("  usu_codser,usu_cplcvs,usu_unimed,usu_qtdfun," + qtdHoras + ",usu_preuni,usu_periss,usu_perirf,usu_perins,usu_perpit,usu_percsl,usu_percrt,usu_perour,usu_ctafin,usu_ctared,usu_codccu,");
						sql.append("  usu_datini,usu_datfim,usu_tnsser,1166,REPLACE(CONVERT(varchar,GETDATE(),111),'/','-')+' 00:00:000' AS USU_DATGER," + nHorGer + ",usu_filnfv,usu_codsnf,usu_numnfv,'" + datCptString + "','+',");
						sql.append("  'Contrato por medição', 30,'N','D','M','','1900-12-31 00:00:00.000','S' ");
						sql.append(" FROM USU_T160CVS P WHERE P.USU_NUMCTR = " + numCtr + " AND P.USU_CODEMP = " + codEmp + " AND (P.USU_SITCVS = 'A' OR P.USU_SITCVS = 'I' AND P.USU_DATFIM >= " + datCptString + ")");
						sql.append(" and P.USU_CPLCVS LIKE '%SERVICO EXTRA%'");
						sql.append(" ORDER BY usu_preuni " + ordem);
						pstm = conn.prepareStatement(sql.toString());
						qtdExecucoes += pstm.executeUpdate();
					}
					catch (SQLException e)
					{
						System.out.println("[MED] - ###ERRO - NÃO FOI POSSIVEL REALIZAR A INCLUSÃO DO APONTAMENTO NO POSTO DE SERVIÇO EXTRA : " + numCtr);
						e.printStackTrace();
					}
					finally
					{
						OrsegupsUtils.closeConnection(conn, pstm, rs);
					}
				}
				ordem = "DESC";
				qtdHoras = new BigDecimal(qtdHor100);
			}
		}
		else if (qtdPostos == 1)
		{
			qtdHoras = new BigDecimal(qtdHorEx + (qtdMinEX / 60.0));
			try
			{
				conn = PersistEngine.getConnection("SAPIENS");

				sql.append(" INSERT INTO usu_t160cms");
				sql.append("  (usu_codemp,usu_codfil,usu_numctr,usu_numpos,usu_seqmov,usu_datmov,usu_codser,usu_cplcvs,usu_unimed,usu_qtdfun,");
				sql.append("  usu_qtdcvs,usu_preuni,usu_periss,usu_perirf,usu_perins,usu_perpit,usu_percsl,usu_percrt,usu_perour,");
				sql.append("  usu_ctafin,usu_ctared,usu_codccu,usu_datini,usu_datfim,usu_tnsser,usu_usuger,usu_datger,usu_horger,");
				sql.append("  usu_filnfv,usu_codsnf,usu_numnfv,usu_datcpt,usu_adcsub,usu_obscms,usu_diatra,usu_posbon,usu_libapo,");
				sql.append("  usu_tipapo,usu_tarfus,usu_cptcom,usu_jusapo)");
				sql.append(" SELECT TOP 1 usu_codemp,usu_codfil,usu_numctr,usu_numpos,(SELECT MAX(USU_SEQMOV) FROM usu_t160cms)+1 AS USU_SEQMOV,REPLACE(CONVERT(varchar,GETDATE(),111),'/','-')+' 00:00:000' AS USU_DATMOV,");
				sql.append("  usu_codser,usu_cplcvs,usu_unimed,usu_qtdfun," + qtdHoras + ",usu_preuni,usu_periss,usu_perirf,usu_perins,usu_perpit,usu_percsl,usu_percrt,usu_perour,usu_ctafin,usu_ctared,usu_codccu,");
				sql.append("  usu_datini,usu_datfim,usu_tnsser,1166,REPLACE(CONVERT(varchar,GETDATE(),111),'/','-')+' 00:00:000' AS USU_DATGER," + nHorGer + ",usu_filnfv,usu_codsnf,usu_numnfv,'" + datCptString + "','+',");
				sql.append("  'Contrato por medição', 30,'N','D','M','','1900-12-31 00:00:00.000','S' ");
				sql.append(" FROM USU_T160CVS P WHERE P.USU_NUMCTR = " + numCtr + " AND P.USU_CODEMP = " + codEmp + " AND (P.USU_SITCVS = 'A' OR P.USU_SITCVS = 'I' AND P.USU_DATFIM >= " + datCptString + ")");
				sql.append(" and P.USU_CPLCVS LIKE '%SERVICO EXTRA%'");
				sql.append(" ORDER BY usu_preuni");
				pstm = conn.prepareStatement(sql.toString());
				qtdExecucoes += pstm.executeUpdate();
			}
			catch (SQLException e)
			{
				System.out.println("[MED] - ###ERRO - Não foi possivel incluir a HE no contrato " + numCtr);
				e.printStackTrace();
			}
			finally
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
		}
		else
		{
			System.out.println("[MED] - ###Nenhum posto de Serviço extra encontrato para o contrato " + numCtr);
		}
		return qtdExecucoes;
	}

	public static int qtdPostosServicoExtra(long numCtr, GregorianCalendar datCpt)
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		int qtdPostos = 0;

		try
		{
			conn = PersistEngine.getConnection("SAPIENS");
			sql.append("SELECT count(*) as qtdPostos FROM USU_T160CVS WHERE USU_NUMCTR = ? AND USU_CPLCVS LIKE '%SERVICO EXTRA%' AND ((USU_SITCVS = 'A') OR (USU_SITCVS = 'I' AND USU_DATFIM >= ?))");
			pstm = conn.prepareStatement(sql.toString());
			pstm.setLong(1, numCtr);
			pstm.setDate(2, new Date(datCpt.getTimeInMillis()));
			rs = pstm.executeQuery();
			while (rs.next())
			{
				qtdPostos = rs.getInt("qtdPostos");
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

		return qtdPostos;
	}

	public static void defineValoresContratos(GregorianCalendar datRef, long numEmp)
	{
		String periodoStr = NeoDateUtils.safeDateFormat(datRef, "MM/yyyy");
		QLEqualsFilter periodo = new QLEqualsFilter("periodo", periodoStr);
		QLEqualsFilter empresa = new QLEqualsFilter("codEmpresaContratada", numEmp);
		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		groupFilter.addFilter(periodo);
		groupFilter.addFilter(empresa);

		List<NeoObject> objsMed = PersistEngine.getObjects(AdapterUtils.getEntityClass("MEDMedicaoDeContratos"), groupFilter);

		for (NeoObject objMed : objsMed)
		{
			EntityWrapper medWrapper = new EntityWrapper(objMed);
			long contrato = (long) medWrapper.findField("contrato").getValue();
			BigDecimal vlrPar = new BigDecimal(retornaValorParcialDoContrato(contrato, NeoDateUtils.safeDateFormat(datRef, "yyyy-MM-dd")));
			BigDecimal vlrCtr = new BigDecimal(retornaValorDoContrato(contrato, NeoDateUtils.safeDateFormat(datRef, "yyyy-MM-dd")));
			medWrapper.setValue("vlrPar", vlrPar);
			medWrapper.setValue("vlrCtr", vlrCtr);
			PersistEngine.persist(objMed);
		}

	}

	public static double retornaValorParcialDoContrato(long numctr, String datRef)
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		double vlrPar = 0;
		try
		{
			conn = PersistEngine.getConnection("SAPIENS");
			sql.append("SELECT CAST(ISNULL((SELECT SUM(USU_PREUNI*USU_QTDCVS) FROM USU_T160CVS WHERE USU_NUMCTR = ? AND ( (USU_SITCVS = 'A') OR (USU_SITCVS = 'I' AND USU_DATFIM >= ?))),0) ");
			sql.append("  -  ");
			sql.append("  ISNULL((SELECT SUM((USU_PREUNI*USU_QTDCVS)/30*USU_DIATRA) FROM USU_T160CMS  ");
			sql.append("								  WHERE USU_NUMCTR = ? AND USU_DATCPT = ? ");
			sql.append("								  AND USU_ADCSUB = '-' ),0)  ");
			sql.append("  -  ");
			sql.append("  ISNULL((SELECT SUM(((USU_QTDCVS * USU_PREUNI)/30)*(30-USU_DIATRA)) FROM USU_T160CMS WHERE  USU_NUMCTR = ? AND USU_DATCPT = ? ");
			sql.append("								  AND USU_ADCSUB = '+' ),0) AS MONEY) AS VLRPAR");
			pstm = conn.prepareStatement(sql.toString());
			pstm.setLong(1, numctr);
			pstm.setString(2, datRef);
			pstm.setLong(3, numctr);
			pstm.setString(4, datRef);
			pstm.setLong(5, numctr);
			pstm.setString(6, datRef);

			rs = pstm.executeQuery();
			while (rs.next())
			{
				vlrPar = rs.getInt("VLRPAR");
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

		return vlrPar;
	}

	public static double retornaValorDoContrato(long numctr, String datRef)
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		double vlrCtr = 0;
		try
		{
			conn = PersistEngine.getConnection("SAPIENS");
			sql.append(" select isnull(CAST((SELECT SUM(USU_PREUNI*USU_QTDCVS) FROM USU_T160CVS C ");
			sql.append(" WHERE C.USU_NUMCTR = ? AND ((USU_SITCVS = 'A') OR (USU_SITCVS = 'I' AND USU_DATFIM >= ?)))");
			sql.append(" AS MONEY),0)AS VLRCTR");
			pstm = conn.prepareStatement(sql.toString());
			pstm.setLong(1, numctr);
			pstm.setString(2, datRef);
			rs = pstm.executeQuery();
			while (rs.next())
			{
				vlrCtr = rs.getInt("VLRCTR");
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

		return vlrCtr;
	}

	public static void validaCargaHorariaApontamentoHorasFaltante(long contrato, long posto, long empresa, String datCpt, long horTra, long minTra, String carHorPos)
	{
		int posFin = carHorPos.indexOf("horas") - 1;
		int carHor = Integer.parseInt(carHorPos.substring(0, posFin));
		long horMes = 0;
		if (carHor == 4)
		{
			horMes = 120;
		}
		else if (carHor == 6)
		{
			horMes = 180;
		}
		else if (carHor == 8)
		{
			horMes = 220;
		}

		if (horMes > 0)
		{
			insereApontamentoHorasFaltantes(contrato, posto, empresa, datCpt, horTra, minTra, horMes, carHor);
		}
		else
		{
			System.out.println("Para o contrato " + contrato + " posto " + posto + " não foi inserido o as horas trabalhadas devido sua carga horaria: " + carHorPos);
		}
	}

	public static void insereApontamentoHorasFaltantes(long numCtr, long posto, long codEmp, String datCpt, long horTra, long minTra, long horMes, int carHor)
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		StringBuilder sql = new StringBuilder();
		conn = PersistEngine.getConnection("SAPIENS");
		String obsApo = "Desconto pelo motivo de  ";

		//		if (minTra > 0 && horTra > 0)
		//		{
		//			horTra++;
		//		}
		//Quantidade de horas faltantes.
		double qtdHor = horTra;
		//		if (qtdHor <= 1)
		//		{
		//			qtdHor = 0;
		//		}
		//		else
		//		{
		//			qtdHor = qtdHor - 1;
		//		}
		if (minTra > 0)
		{
			//Hora + minuto faltante
			qtdHor = qtdHor + ((minTra) / 60.0);
		}

		if (qtdHor > 0 && carHor != horTra)
		{
			obsApo += horTra + "h(s) ";
		}

		if (horTra > 0 && minTra > 0 && carHor != horTra)
		{
			obsApo += " e ";
		}

		if (minTra > 0)
		{
			obsApo += minTra + "min ";
		}

		obsApo += " Trabalhados(s)";

		//Altera para BigDecimal para limitar as casas apos a virgula para 2.
		BigDecimal qtdHorApo = new BigDecimal(qtdHor).setScale(2, BigDecimal.ROUND_HALF_UP);

		//Gera hora e minuto do cadastramento - INICIO
		GregorianCalendar datAtu = new GregorianCalendar();
		String horGer = NeoDateUtils.safeDateFormat(datAtu, "HH");
		Long minGer = Long.parseLong(NeoDateUtils.safeDateFormat(datAtu, "mm"));
		Long nHorGer = Long.parseLong(horGer);
		nHorGer = (nHorGer * 60) + minGer;
		//Gera hora e minuto do cadastramento - FIM

		//Formula para calcular o valor de uma hora e multiplicar o retorno para ter o valor de acordo com as horas faltantes (qtdHorApo). Depois, é incluido esta condição no sql a baixo.
		String calculoHorasMensal = "(usu_preuni/" + horMes + ")*" + qtdHorApo;
		if (qtdHor > 0)
		{
			try
			{
				sql.append(" INSERT INTO usu_t160cms");
				sql.append("  (usu_codemp,usu_codfil,usu_numctr,usu_numpos,usu_seqmov,usu_datmov,usu_codser,usu_cplcvs,usu_unimed,usu_qtdfun,");
				sql.append("  usu_qtdcvs,usu_preuni,usu_periss,usu_perirf,usu_perins,usu_perpit,usu_percsl,usu_percrt,usu_perour,");
				sql.append("  usu_ctafin,usu_ctared,usu_codccu,usu_datini,usu_datfim,usu_tnsser,usu_usuger,usu_datger,usu_horger,");
				sql.append("  usu_filnfv,usu_codsnf,usu_numnfv,usu_datcpt,usu_adcsub,usu_obscms,usu_diatra,usu_posbon,usu_libapo,");
				sql.append("  usu_tipapo,usu_tarfus,usu_cptcom,usu_jusapo)");
				sql.append(" SELECT TOP 1 usu_codemp,usu_codfil,usu_numctr,usu_numpos,(SELECT MAX(USU_SEQMOV) FROM usu_t160cms)+1 AS USU_SEQMOV,REPLACE(CONVERT(varchar,GETDATE(),111),'/','-')+' 00:00:000' AS USU_DATMOV,");
				sql.append("  usu_codser,usu_cplcvs,usu_unimed,usu_qtdfun,1," + calculoHorasMensal + ",usu_periss,usu_perirf,usu_perins,usu_perpit,usu_percsl,usu_percrt,usu_perour,usu_ctafin,usu_ctared,usu_codccu,");
				sql.append("  usu_datini,usu_datfim,usu_tnsser,1166,REPLACE(CONVERT(varchar,GETDATE(),111),'/','-')+' 00:00:000' AS USU_DATGER," + nHorGer + ",usu_filnfv,usu_codsnf,usu_numnfv,'" + datCpt + "','+',");
				sql.append("  '" + obsApo + "', 30,'N','D','M','','1900-12-31 00:00:00.000','S' ");
				sql.append(" FROM USU_T160CVS P WHERE P.USU_NUMCTR = " + numCtr + " AND P.USU_NUMPOS = " + posto + " AND P.USU_CODEMP = " + codEmp + " AND (P.USU_SITCVS = 'A' OR P.USU_SITCVS = 'I' AND P.USU_DATFIM >= '" + datCpt + "')");
				sql.append(" ORDER BY usu_preuni");

				pstm = conn.prepareStatement(sql.toString());
				pstm.executeUpdate();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
			finally
			{
				OrsegupsUtils.closeConnection(conn, pstm, null);
			}
		}
	}

	public static int geraDiaria(long numCtr, long codEmp, double qtdDiaria, String obsDia)
	{
		Connection conn = null;
		PreparedStatement pstm = null;
		StringBuilder sql = new StringBuilder();
		//Gera hora e minuto do cadastramento - INICIO
		GregorianCalendar datAtu = new GregorianCalendar();
		String horGer = NeoDateUtils.safeDateFormat(datAtu, "HH");
		Long minGer = Long.parseLong(NeoDateUtils.safeDateFormat(datAtu, "mm"));
		Long nHorGer = Long.parseLong(horGer);
		nHorGer = (nHorGer * 60) + minGer;
		//Gera hora e minuto do cadastramento - FIM
		GregorianCalendar datCpt = new GregorianCalendar();
		datCpt.set(GregorianCalendar.DATE, 1);
		datCpt.set(GregorianCalendar.MONTH, -1);
		String datCptString = NeoDateUtils.safeDateFormat(datCpt, "yyyy-MM-dd");
		int execucao = 0;
		qtdDiaria = qtdDiaria - 1;

		if (qtdDiaria > 0)
		{
			conn = PersistEngine.getConnection("SAPIENS");
			try
			{
				sql.append(" INSERT INTO usu_t160cms");
				sql.append("  (usu_codemp,usu_codfil,usu_numctr,usu_numpos,usu_seqmov,usu_datmov,usu_codser,usu_cplcvs,usu_unimed,usu_qtdfun,");
				sql.append("  usu_qtdcvs,usu_preuni,usu_periss,usu_perirf,usu_perins,usu_perpit,usu_percsl,usu_percrt,usu_perour,");
				sql.append("  usu_ctafin,usu_ctared,usu_codccu,usu_datini,usu_datfim,usu_tnsser,usu_usuger,usu_datger,usu_horger,");
				sql.append("  usu_filnfv,usu_codsnf,usu_numnfv,usu_datcpt,usu_adcsub,usu_obscms,usu_diatra,usu_posbon,usu_libapo,");
				sql.append("  usu_tipapo,usu_tarfus,usu_cptcom,usu_jusapo)");
				sql.append(" SELECT TOP 1 usu_codemp,usu_codfil,usu_numctr,usu_numpos,(SELECT MAX(USU_SEQMOV) FROM usu_t160cms)+1 AS USU_SEQMOV,REPLACE(CONVERT(varchar,GETDATE(),111),'/','-')+' 00:00:000' AS USU_DATMOV,");
				sql.append("  usu_codser,usu_cplcvs,usu_unimed,usu_qtdfun," + qtdDiaria + ",usu_preuni,usu_periss,usu_perirf,usu_perins,usu_perpit,usu_percsl,usu_percrt,usu_perour,usu_ctafin,usu_ctared,usu_codccu,");
				sql.append("  usu_datini,usu_datfim,usu_tnsser,1166,REPLACE(CONVERT(varchar,GETDATE(),111),'/','-')+' 00:00:000' AS USU_DATGER," + nHorGer + ",usu_filnfv,usu_codsnf,usu_numnfv,'" + datCptString + "','+',");
				sql.append("  'Contrato por medição', 30,'N','D','M','','1900-12-31 00:00:00.000','S' ");
				sql.append(" FROM USU_T160CVS P WHERE P.USU_NUMCTR = " + numCtr + " AND P.USU_CODEMP = " + codEmp + " AND (P.USU_SITCVS = 'A' OR P.USU_SITCVS = 'I' AND P.USU_DATFIM >= '" + datCptString + "')");
				sql.append(" AND P.USU_CODSER = '9008013MTA' ");
				sql.append(" ORDER BY usu_preuni");

				pstm = conn.prepareStatement(sql.toString());
				execucao = pstm.executeUpdate();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
			finally
			{
				OrsegupsUtils.closeConnection(conn, pstm, null);
			}
		}
		return execucao;
	}
}
