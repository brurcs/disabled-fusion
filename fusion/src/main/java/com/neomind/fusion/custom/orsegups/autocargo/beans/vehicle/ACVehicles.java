package com.neomind.fusion.custom.orsegups.autocargo.beans.vehicle;

import java.util.List;

public class ACVehicles {
    
    private List<ACVehicle> vehicles;

    public List<ACVehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<ACVehicle> vehicles) {
        this.vehicles = vehicles;
    }
    
    
    
}
