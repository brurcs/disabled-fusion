package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class ChipGPRSAtualizaPostoEstoque implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{
			String nomeFonteDados = "SAPIENS";
	
			Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
			
			Long codEmp = (Long) processEntity.findValue("postoSapiens.usu_codemp");
			Long codFil = (Long) processEntity.findValue("postoSapiens.usu_codfil");
			Long numCtr = (Long) processEntity.findValue("postoSapiens.usu_numctr");
			Long numPos = (Long) processEntity.findValue("postoSapiens.usu_numpos");
			String codSer = (String) processEntity.findValue("postoSapiens.usu_codser");
			
			Collection<NeoObject> chips = processEntity.findField("chipsGPRSSubstituir").getValues();//chipsGPRSSubstituir.ChipGPRSPosto
			if (chips != null && chips.size() > 0) {
				if (chips.size()>=1 && chips.size() <= 1) {
					for (NeoObject chip : chips)
					{
						EntityWrapper wrapperLista = new EntityWrapper(chip);
						//Chip que esta no posto com defeito
						NeoObject chipPosto = (NeoObject) wrapperLista.findValue("chipsGPRSSubstituir.ChipGPRSPosto");
						EntityWrapper wrapperChipPosto = new EntityWrapper(chipPosto);
						String numTelPosto = (String) wrapperChipPosto.findField("numeroTelefone").getValue();
						String iccIDPosto = (String) wrapperChipPosto.findField("iccID").getValue();
						NeoObject planoDadosPosto = (NeoObject) wrapperChipPosto.findValue("planoDados");
						EntityWrapper planoWrapper = new EntityWrapper(planoDadosPosto);
						NeoObject operadoraPlanoPosto = (NeoObject) planoWrapper.findValue("operadora");
						EntityWrapper opeWrapperPosto = new EntityWrapper(operadoraPlanoPosto);
						String nomeOperadoraPosto = (String) opeWrapperPosto.findField("nome").getValue();
						
						//Desativa Chip Com Defeito
						QLEqualsFilter filtroDefeito = new QLEqualsFilter("iccID",iccIDPosto);
						List<NeoObject> chipDefeito = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TELECOMChipsGPRS"),filtroDefeito);
						if (chipDefeito != null && chipDefeito.size() > 0) {
							NeoObject objChipPostoDefeito =  chipDefeito.get(0);
							EntityWrapper wrapperChipPostoDefeito = new EntityWrapper(objChipPostoDefeito);
							wrapperChipPostoDefeito.setValue("ativo", Boolean.FALSE);
							PersistEngine.commit(true);
						}
						
						//Chip que esta no Estoque
						NeoObject chipEstoque = (NeoObject) wrapperLista.findValue("chipsGPRSSubstituir.ChipGPRSEstoque");
						EntityWrapper wrapperChipEstoque = new EntityWrapper(chipEstoque);
						String numTelEstoque = (String) wrapperChipEstoque.findField("numeroTelefone").getValue();
						String iccIDEstoque = (String) wrapperChipEstoque.findField("iccID").getValue();
						NeoObject planoDadosEstoque = (NeoObject) wrapperChipEstoque.findValue("planoDados");
						EntityWrapper planoWrapperEstoque = new EntityWrapper(planoDadosEstoque);
						NeoObject operadoraPlanoEstoque = (NeoObject) planoWrapperEstoque.findValue("operadora");
						EntityWrapper opeWrapperEstoque = new EntityWrapper(operadoraPlanoEstoque);
						String nomeOperadoraEstoque = (String) opeWrapperEstoque.findField("nome").getValue();
						
						StringBuffer sqlSelect = new StringBuffer();
						sqlSelect.append(" SELECT * FROM USU_T160CHP ");
						sqlSelect.append(" WHERE usu_codemp = ? AND usu_codfil = ? AND usu_numctr = ? AND usu_numpos = ? AND usu_iccid = ? AND usu_codser = ? and usu_sitchp = ? ");
						
						PreparedStatement stSelect = connection.prepareStatement(sqlSelect.toString());
						stSelect.setLong(1, codEmp);
						stSelect.setLong(2, codFil);
						stSelect.setLong(3, numCtr);
						stSelect.setLong(4, numPos);
						stSelect.setString(5, iccIDPosto);
						stSelect.setString(6, codSer);
						stSelect.setString(7, "A");
						ResultSet rs = stSelect.executeQuery();
						
						if(rs.next()) {
							StringBuffer sql = new StringBuffer();
							sql.append(" INSERT INTO USU_T160CHP ");
							sql.append(" (usu_codemp,usu_codfil,usu_numctr,usu_numpos,usu_numtel,usu_iccid,usu_codser,usu_codope,usu_sitchp) ");
							sql.append(" VALUES ");
							sql.append(" (?,?,?,?,?,?,?,?,?) ");
						
							PreparedStatement st = connection.prepareStatement(sql.toString());
							st.setLong(1, codEmp);
							st.setLong(2, codFil);
							st.setLong(3, numCtr);
							st.setLong(4, numPos);
							st.setString(5, numTelEstoque);
							st.setString(6, iccIDEstoque);
							st.setString(7, codSer);
							st.setString(8, nomeOperadoraEstoque);
							st.setString(9, "A");
							System.out.println("Chip: SELECT usu_iccid FROM USU_T160CHP  WHERE usu_codemp = "+codEmp+" AND usu_codfil = "+codFil+" AND usu_numctr = "+numCtr+" AND usu_numpos = "+numPos+" AND usu_codser = "+codSer);
							st.executeUpdate();
							
							sql = new StringBuffer();
							sql.append(" UPDATE dbo.usu_t160chp SET usu_sitchp = ?, USU_OpeSub = ? ");
							sql.append(" WHERE  usu_codemp = ? AND usu_codfil = ? AND usu_numctr = ? AND usu_numpos = ? AND usu_iccid = ? AND usu_codser = ? and usu_sitchp = ? ");
						
							st = connection.prepareStatement(sql.toString());
							
							st.setString(1, "S");
							st.setString(2, nomeOperadoraEstoque);
							st.setLong(3, codEmp);
							st.setLong(4, codFil);
							st.setLong(5, numCtr);
							st.setLong(6, numPos);
							st.setString(7, iccIDPosto);
							st.setString(8, codSer);
							st.setString(9, "A");
							st.executeUpdate();
						}
					}
				} else {
					throw new WorkflowException("Só é Permitido Castrar um ChipGPRS por Vez!");
				}
			} else {
				throw new WorkflowException("Favor Incluir um Chip para Cadastro!");
			}
		}
		catch (WorkflowException e)
		{
			e.printStackTrace();
			throw new WorkflowException("Não foi possivel atualizar as informação no Sapiens.");
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			throw new WorkflowException("Não foi possivel atualizar as informação no Sapiens.");
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

}
