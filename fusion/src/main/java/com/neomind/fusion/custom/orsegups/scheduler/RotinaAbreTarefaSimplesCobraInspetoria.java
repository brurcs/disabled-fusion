package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;


//com.neomind.fusion.custom.orsegups.scheduler.RotinaAbreTarefaSimplesCobraInspetoria
public class RotinaAbreTarefaSimplesCobraInspetoria implements CustomJobAdapter
{
	
	@Override
	public void execute(CustomJobContext ctx)
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		
		Connection conn = PersistEngine.getConnection("");
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.RotinaAbreTarefaSimplesCobraInspetoria");
		log.warn("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Simples Cobrança Inspetoria - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		
		try
		{
			
			GregorianCalendar gc = new GregorianCalendar();
			gc.add(GregorianCalendar.DATE, -2);
			gc.set(GregorianCalendar.HOUR, 0);
			gc.set(GregorianCalendar.MINUTE, 0);
			gc.set(GregorianCalendar.SECOND, 0);
			gc.set(GregorianCalendar.MILLISECOND, 0);
			
			GregorianCalendar prazo = OrsegupsUtils.getNextWorkDay(new GregorianCalendar());
			
			prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
			prazo.set(Calendar.HOUR_OF_DAY, 23);
			prazo.set(Calendar.MINUTE, 59);
			prazo.set(Calendar.SECOND, 59);
			
			HashMap<String, Long> usuariosComTarefa = retornaContagemInspecoes(gc); // lista todos colaboradores que fizeram tarefas de inspeção na data especificada.
			
			//NeoUser usuarioSolicitante = PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "beatriz.malmann" ) ); 
			String solicitante = OrsegupsUtils.getUserNeoPaper("G001SolicitanteCobrancaInspetoria");
					
			ArrayList<NeoObject> usuariosCobranca = (ArrayList<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("IMCobrancaInspecoes"));
			
			GregorianCalendar dataAtual = new GregorianCalendar();
			dataAtual.set(GregorianCalendar.HOUR_OF_DAY, 0);
			dataAtual.set(GregorianCalendar.MINUTE, 0);
			dataAtual.set(GregorianCalendar.SECOND, 0);
			
			for(NeoObject obj : usuariosCobranca){
				EntityWrapper wObj = new EntityWrapper(obj);
				
				String login = NeoUtils.safeOutputString(wObj.findValue("login"));
				GregorianCalendar datPausaIni = (GregorianCalendar) wObj.findValue("datPausaIni");
				GregorianCalendar datPausaFim = (GregorianCalendar) wObj.findValue("datPausaFim");
				
				boolean abrirTarefa = true;
				if (datPausaIni != null && datPausaFim != null){
					abrirTarefa =  (dataAtual.before(datPausaIni)  || dataAtual.after(datPausaFim)) ;
				}
				
				if (!usuariosComTarefa.containsKey(login) && abrirTarefa  ){ // abrir tarefa se não houver historico de inspetorias nos ultimos dias e se não houver pause de abertura (férias, suspensão, afastamento, etc...)
					String titulo = login + " - Justificar falta de inspeção após o dia " + NeoUtils.safeDateFormat(gc);
					String descricao = "Identificamos que após a data " + NeoUtils.safeDateFormat(gc) + ", não houve nenhuma tarefa de inspetoria lançada para a sua pessoa. Por qual motivo?";
					
					IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
					Long timeIni = GregorianCalendar.getInstance().getTimeInMillis();
					
					if (!temCobrancaEmAberto(login)) {// se não tem cobrança em aberto, abre uma nova cobraca
						String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, login, titulo, descricao, "1", "sim", prazo);
						registraAberturaTarefa(login,tarefa);
						System.out.println("tempo Abertura tarefa:"+tarefa+" - " + login +" " + (GregorianCalendar.getInstance().getTimeInMillis() - timeIni) + "ms");
					}
					
					/*InstantiableEntityInfo tarefaAJ = AdapterUtils.getInstantiableEntityInfo("TarefaInspetoriaJustificativa");
					NeoObject noAJ = tarefaAJ.createNewInstance();
					EntityWrapper ajWrapper = new EntityWrapper(noAJ);

					try{
						ajWrapper.setValue("dataCad", new GregorianCalendar() );
						ajWrapper.setValue("tarefa", tarefa);
						ajWrapper.setValue("supervisor", login );
						
						PersistEngine.persist(noAJ);
					}catch(Exception e){
						e.printStackTrace();
					}*/
				}
			}
			
			
		}catch (Exception e){
			log.error("##### AGENDADOR DE TAREFA: Erro Abrir Tarefa Simples Cobrança Inspetoria ["+key+"]" + e.getMessage());
			e.printStackTrace();
			throw new JobException("Erro ao processar job, procurar no log por:"+key);
		}finally{

			OrsegupsUtils.closeConnection(conn, pstm, rs);

			log.warn("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Simples Cobrança Inspetoria - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		
	}
	
	public HashMap<String, Long> retornaContagemInspecoes(GregorianCalendar data){
		HashMap<String, Long> retorno = new HashMap<String, Long>();
		Connection conn = PersistEngine.getConnection("");
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.RotinaAbreTarefaSimplesCobraInspetoria");
		log.warn("[TS INSPETORIA] - Verificando usuarios que possuem tarefas de inspeção na data " + NeoUtils.safeDateFormat(data));

		try
		{
			sql.append(" select su.code, count(wf.neoId) qtde from dbo.WFProcess wf with (nolock) ");
			sql.append(" inner join dbo.D_IMInspetoria i with (nolock) on i.neoId = wf.entity_neoId ");
			sql.append(" inner join dbo.NeoUser nu with (nolock) on wf.requester_neoId = nu.neoId ");
			sql.append(" inner join dbo.SecurityEntity su with (nolock) on (nu.neoId = su.neoId) ");
			sql.append(" where wf.processState != 2 and DATEADD(dd, DATEDIFF(dd, 0, wf.startDate), 0) >= DATEADD(dd, DATEDIFF(dd, 0, ? ), 0) ");
			sql.append(" group by su.code ");
			
			pstm = conn.prepareStatement(sql.toString());
			pstm.setDate(1, new Date(data.getTimeInMillis()));
			
			rs = pstm.executeQuery();
			while (rs.next()){
				retorno.put(rs.getString("code"), rs.getLong("qtde"));
			}
			
			return retorno;
		}catch (Exception e){
			log.error("[TS INSPETORIA] - Erro ao consultar quantidade de tarefas dos colaboradores." + e.getMessage());
			e.printStackTrace();
			return retorno;
		}finally{

			OrsegupsUtils.closeConnection(conn, pstm, rs);

			log.warn("[TS INSPETORIA] - Quantidade de tarefas consultada com exito. ");
		}
		
		
	}
	
	/**
	 * Preinicializa o eform de cobrança de tarefas de inspetoria.
	 */
	public static void initEformCobranca(){
		
		StringBuilder sql = new StringBuilder();
		
		sql.append(" select se.name, fun.nomfun, numemp, numcad, code from dbo.NeoPaper_users "); 
		sql.append(" inner join dbo.SecurityEntity se on  se.neoId = NeoPaper_users.users_neoId ");
		sql.append(" left  join [FSOODB04\\sql02].vetorh.dbo.r034fun fun on nomfun = se.name "); //--like replace(('%'+code+'%'),'.','%') 
		sql.append(" where NeoPaper_users.papers_neoId = ( ");
		sql.append("         select np.neoID from neoPaper np inner join securityentity sp on np.neoid = sp.neoid where sp.code like 'Inspetoria Móbile%' ");
		sql.append(" ) ");
		sql.append(" and se.active = 1 ");
		sql.append(" and fun.sitafa <> 7 ");
		
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try{
			con = PersistEngine.getConnection("");
			pst = con.prepareStatement(sql.toString());
			
			rs = pst.executeQuery();
			while (rs.next()){
				
				NeoObject obj = AdapterUtils.createNewEntityInstance("IMCobrancaInspecoes");
				EntityWrapper wObj = new EntityWrapper(obj);
				wObj.setValue("numemp", rs.getLong("numemp"));
				wObj.setValue("numcad", rs.getLong("numcad"));
				wObj.setValue("login",  rs.getString("code"));
				PersistEngine.persist(obj);
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			OrsegupsUtils.closeConnection(con, pst, rs);
		}
		
	}
	
	public static void registraAberturaTarefa(String login, String tarefa){
		
		NeoObject oHistoricoAbertura = AdapterUtils.createNewEntityInstance("IMHistoricoCobranca");
		EntityWrapper wHistoricoAbertura = new EntityWrapper(oHistoricoAbertura);
		
		wHistoricoAbertura.setValue("login", login);
		wHistoricoAbertura.setValue("tarefa", tarefa);
		
		PersistEngine.persist(oHistoricoAbertura);
		
	}
	
	public static void main(String[] args){
		initEformCobranca();
	}
	
	//NeoPaper presencaSupervisao = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","presencaSupervisao"));
	
	/**
	 * Verifica se há tarefas em aberto
	 * @param login
	 * @return
	 * @throws Exception
	 */
	private static boolean temCobrancaEmAberto(String login) throws Exception{
		boolean retorno = false;
		
		String sql = 	" select * from wfprocess wf "+ 
						" inner join D_tarefa tarefa on wf.entity_neoId = tarefa.neoID "+
						" inner join D_IMHistoricoCobranca hc on wf.code = hc.tarefa "+
						" where wf.processState = 0 and hc.login = ? ";
						
		Connection con = OrsegupsUtils.getConnection("");
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try{
			pstm = con.prepareStatement(sql);
			pstm.setString(1, login);
			rs = pstm.executeQuery();
			
			if (rs.next()){
				retorno = true;
			}else{
				retorno = false;
			}
			
			return retorno;
		}catch(Exception e){
			throw e;
		}finally{
			OrsegupsUtils.closeConnection(con, pstm, rs);
		}
		
	}
	
}
