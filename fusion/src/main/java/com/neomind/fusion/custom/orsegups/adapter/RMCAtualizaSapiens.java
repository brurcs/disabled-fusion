package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class RMCAtualizaSapiens implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		String nomeFonteDados = "SAPIENS";

		Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);

		// valores a serem atualizados no update
		Boolean aprovado = (Boolean) processEntity.findValue("aprovado");
		String aprovadoSapeins = "N";
		if (aprovado != null && aprovado == true)
		{
			aprovadoSapeins = "S";
		}
		GregorianCalendar dataAtual = new GregorianCalendar();
		String codigoProcesso = activity.getProcess().getCode();
		
		// valores utilizados na condicao where
		Long codigoEmpresa = (Long) processEntity.findValue("empresa.codigo");
		String numeroContrato = (String) processEntity.findValue("numeroContrato");
		Long filial = (Long) processEntity.findValue("filial");
		String numerosPostos = (String) processEntity.findValue("numerosPostos");
		String[] nrPostos = numerosPostos.split(",");

		StringBuffer sql = new StringBuffer();
		sql.append("UPDATE USU_T160CVS ");
		sql.append("SET USU_DIRAPR = ?, ");
		sql.append("USU_DATAPR = ?, ");
		sql.append("USU_TARAPR = ? ");
		sql.append("WHERE USU_CODEMP = ? ");
		sql.append("AND USU_CODFIL = ? ");
		sql.append("AND USU_NUMCTR = ? ");
		sql.append("AND USU_NUMPOS IN (");
		
		//workarraound para utilizar IN com preparedStatement
		Boolean first = true;
		for(String nrPosto:nrPostos){
			if(first){
				sql.append("?");
				first = false;
			}
			else{
				sql.append(",?");
			}
		}
		sql.append(")");
		
		try
		{
			PreparedStatement st = connection.prepareStatement(sql.toString());
			//UPDATE
			st.setString(1, aprovadoSapeins);
			st.setDate(2, new Date(dataAtual.getTimeInMillis()));
			st.setString(3, codigoProcesso);
			//WHERE
			st.setLong(4, codigoEmpresa);
			st.setLong(5, filial);
			st.setString(6, numeroContrato);
			
			//workarraound para utilizar IN com preparedStatement
			Integer index = 7;
			for(String nrPosto:nrPostos){
				st.setString(index, nrPosto);
				index++;
			}
			//Log.warn("UPDATE SAPIENS: "+st.toString());
			st.executeUpdate();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			throw new WorkflowException("Não foi possivel atualizar as informação no Sapiens.");
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

}
