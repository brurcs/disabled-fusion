package com.neomind.fusion.custom.orsegups.utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SimpleTimeZone;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.swing.text.MaskFormatter;

import org.apache.axis.encoding.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.MultiPartEmail;
import org.apache.velocity.VelocityContext;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.emailmonitor.vo.ResultVO;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.datawarehouse.NeoDataSource;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.eform.EFormExceptionError;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.engine.StackedFormException;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.i18n.I18nMessage;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.portal.VelocityUtils;
import com.neomind.fusion.security.NeoGroup;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.security.SecurityEntity;
import com.neomind.fusion.workflow.ProcessState;
import com.neomind.fusion.workflow.WFEngine;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.agenda.Holiday;
import com.neomind.fusion.workflow.event.WorkflowEventListener;
import com.neomind.fusion.workflow.event.WorkflowValidateCancelEvent;
import com.neomind.fusion.workflow.event.WorkflowValidateCancelEventListener;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.fusion.workflow.simulation.WorkflowService;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;
import com.sendgrid.SendGrid;
import com.sendgrid.SendGridException;

public class OrsegupsUtils
{
	private static final Log log = LogFactory.getLog(OrsegupsUtils.class);

	public static final int NIVEL_PRESIDENCIA = 1000;

	public static final int NIVEL_DIRETORIA = 800;

	
	public static final int NIVEL_SUPERINTENDENCIA = 600;

	public static final int NIVEL_GERENCIA = 500;

	public static final int NIVEL_COORDENACAO = 300;

	public static final String COBRANCA_INADIMPLENCIA = "C026 - FCI - Cobrança Inadimplência";
	public static final String COBRANCA_HUMANA_INADIMPLENCIA = "C028 - FCI - Humana Inadimplência";
	public static final String CANCELAMENTO_CONTRATO_INICIATIVA = "C023 - FCN - Cancelamento de Contrato Iniciativa eNEW";
	public static final String CANCELAMENTO_CONTRATO_INADIMPLENCIA = "C025 - FCN - Cancelamento de Contrato Inadimplência eNEW";
	public static final String CANCELAMENTO_CONTRATO_HUMANA_INADIMPLENCIA = "C029 - FCN - Cancelamento de Contrato Humana - Inadimplência";
	public static final String URL_PRODUCAO = "https://intranet.orsegups.com.br";
	public static final String URL_HOMOLOGACAO = "http://localhost";
	public static final String URL_AVALIACAO = "https://www.orsegups.com.br/rat/avaliacao";

	public static final String WS_LOGIN = "wssenior";
	public static final String WS_SENHA = "Wss4321!";
	public static final String BOLAO_PASSWORD = "bolaozaum2010";

	public static final String SENDGRID_USERNAME = "orsegups";
	public static final String SENDGRID_PASSWORD = "Ors1234!";
	
	
	 /**
	  * Retorna um histórico com as 10 últimas tarefas simples contendo o valor de título informado (campo <code>titulo</code> no formulário <code>Tarefa</code>).<br><br>
	  * <strong>Atenção: Você deve encaminhar os caracteres de percentual <code>%</code> necessários à pesquisa.</strong>
	  * @author mateus.batista
	  * @since 23/01/2018
	  * @param titulo Título ao qual deseja-se pesquisar uma tarefa simples, contendo ou não o carecter percentual <code>%</code>.
	  * @return Se forem encontradas tarefas, retorna o html da tabela resultado semelhante ao modelo abaixo: <br><br>
	     * <strong>Histórico da conta:</strong>
	     * <br>
	     * <table cellpadding="0" cellspacing="3" border="0"
             *        style="text-align: left; width: 66%;">
             *   <tbody>
             *     <tr>
             *       <th style="vertical-align: top; background-color: rgb(204, 204, 255);
             *           text-align: center;">Data<br>
             *       </th>
             *       <th style="vertical-align: top; background-color: rgb(204, 204, 255);
             *           text-align: center;">Tarefa<br>
             *       </th>
             *     </tr>
             *     <tr>
             *       <td style="vertical-align: middle;">
             *              19/08/2016
             *       </td>
             *       <td style="vertical-align: middle;">
             *              O. 20160819
             *       </td>
             *     </tr>
             *     <tr>
             *       <td style="vertical-align: middle; background-color: rgb(238, 238, 255);">
             *              08/10/2017
             *       </td>
             *       <td style="vertical-align: middle; background-color: rgb(238, 238, 255);">
             *             O. 20171008
             *       </td>
             *     </tr>
             *   </tbody>
             * </table>
             * <br>
             * se nenhum resultado for encontrado retorna o texto:<br><br><strong>Histórico da conta:</strong> Não possui histórico.
             * <br><br> nunca nulo.
	  */
        public static String getHistoricoTarefaSimples(String titulo) {
    
            	titulo = titulo.replace("[", "\\[");
            
        	StringBuilder tarefas = new StringBuilder();
        	StringBuilder retorno = new StringBuilder();
        
        	retorno.append("<strong>Histórico da conta:</strong> ");
        
        	Connection conn = null;
        	PreparedStatement pstm = null;
        	ResultSet rs = null;
        
        	StringBuilder sql = new StringBuilder();
        
        	sql.append(" SELECT TOP 10 P.code, P.neoId, P.startDate FROM D_Tarefa T WITH(NOLOCK) ");
        	sql.append(" INNER JOIN WFProcess P WITH(NOLOCK) ON P.neoId = T.wfprocess_neoId ");
        	sql.append(" WHERE titulo LIKE ? ESCAPE '\\' ");
        	sql.append(" ORDER BY T.neoId DESC ");
        
        	boolean possuiHistorico = false;
        
        	try {
        	    conn = PersistEngine.getConnection("");
        	    pstm = conn.prepareStatement(sql.toString());
        
        	    pstm.setString(1, titulo.trim());
        
        	    rs = pstm.executeQuery();
        
        	    while (rs.next()) {
        
        		possuiHistorico = true;
        
        		String code = rs.getString("code");
        		String neoId = rs.getString("neoId");
        		Date data = rs.getDate("startDate");
        		
        		tarefas.append("<tr>");
        		tarefas.append("<td>"+NeoDateUtils.safeDateFormat(new Date(data.getTime()), "dd/MM/yyyy")+"</td>");
        		tarefas.append("<td><a href=\"https://intranet.orsegups.com.br/fusion/bpm/workflow_search.jsp?id=" + neoId + "\" target=\"_blank\"><img title=\"Pesquisar\" ");
        		tarefas.append("src=\"https://intranet.orsegups.com.br/fusion/imagens/icones_final/visualize_blue_16x16-trans.png\" align=\"absmiddle\"> " + code + "</a></td>");
        		tarefas.append("</tr>");
        
        	    }
        
        	} catch (SQLException e) {
        	    e.printStackTrace();
        	} finally {
        	    OrsegupsUtils.closeConnection(conn, pstm, rs);
        	}
        
        	if (possuiHistorico) {
        	    retorno.append("<br>");
        	    retorno.append(" <table class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\"> ");
        	    retorno.append(" <tr style=\"text-align: left;\"> ");
        	    retorno.append(" <th>Data</th><th>Tarefa</th>");
        	    retorno.append(" </tr> ");
        	    retorno.append(tarefas);
        	    retorno.append("</table> ");
        	}else{        	    
        	    retorno.append(" Não possui histórico");
        	}
        
        	retorno.append("<br>");
        
        	return retorno.toString();
        }

	/**
	 * Verifica se o usuario pertencen ao nivel informado
	 * de acordo com o cadastro de niveis do Fusion
	 * 
	 * @param NeoUser Usuario do qual se prentende verificar o nivel
	 * @return Boolean result Verdadeiro/Falso
	 */
	public static Boolean checkGroupLevel(NeoUser usuario, int level) throws Exception
	{
		Boolean result = false;

		if (usuario == null)
		{
			throw new Exception("Não foi possível obter o usuário.");
		}
		else if (usuario.getGroup() == null)
		{
			throw new Exception("Não foi possível obter o grupo do usuário.");
		}
		else if (usuario.getGroup().getGroupLevel() == level)
		{
			result = true;
		}

		return result;
	}

	/**
	 * Verifica de o grupo e do nivel informado
	 * de acordo com o cadastro de niveis do Fusion
	 * 
	 * @param NeoGroup grupo Grupo do qual se prentende verificar o nivel
	 * @return Boolean result Verdadeiro/Falso
	 */
	public static Boolean checkGroupLevel(NeoGroup grupo, int level) throws Exception
	{
		Boolean result = false;

		if (grupo == null)
		{
			throw new Exception("Não foi possível obter o grupo.");
		}
		else if (grupo.getGroupLevel() == level)
		{
			result = true;
		}

		return result;
	}

	/**
	 * Busca o superior imediato (nivel acima) do colaborador
	 * de acordo com o cadastro de niveis de grupos do Fusion
	 * 
	 * @param NeoUser usuario Usuario do qual se prentende buscar o papel do superior
	 * @param int nivel Nivel do superior, variaveis estaticas da classe OrsegupsUtil
	 * @return NeoPaper Papel do superiror reponsavel do usuario
	 */
	public static NeoPaper getSuperior(NeoUser usuario) throws Exception
	{
		NeoPaper papelSuperior = null;
		NeoGroup grupoUsuario = null;
		Integer nivelGrupo;

		if (NeoUtils.safeIsNull(usuario))
		{
			throw new Exception("Não foi possível obter o usuário.");
		}

		grupoUsuario = usuario.getGroup();
		if (NeoUtils.safeIsNull(grupoUsuario))
		{
			throw new Exception("Não foi possível obter o grupo do usuário.");
		}

		nivelGrupo = grupoUsuario.getGroupLevel();

		// Se o solicitante for o responsavel pelo departamento
		if (nivelGrupo == NIVEL_PRESIDENCIA)
		{
			papelSuperior = grupoUsuario.getResponsible();
		}
		else
		{
			while ((grupoUsuario.getGroupLevel() < nivelGrupo))
			{
				grupoUsuario = grupoUsuario.getUpperLevel();
				if (NeoUtils.safeIsNull(grupoUsuario))
				{
					throw new Exception("Não foi possível obter a hierarquia de grupo do usuário.");
				}
			}
			papelSuperior = grupoUsuario.getResponsible();
		}
		if (NeoUtils.safeIsNull(papelSuperior))
		{
			throw new Exception("Não foi possível determinar o papel superior. ");
		}

		return papelSuperior;
	}

	/**
	 * Busca o superior do nivel especificado do colaborador
	 * de acordo com o cadastro de niveis de grupos do Fusion
	 * 
	 * @param NeoUser usuario Usuario do qual se prentende buscar o papel do superior
	 * @param int nivel Nivel do superior, variaveis estaticas da classe OrsegupsUtil
	 * @return NeoPaper Papel do superiror reponsavel do usuario
	 */
	public static NeoPaper getSuperior(NeoUser usuario, int nivel) throws Exception
	{
		NeoPaper papelSuperior = null;
		NeoGroup grupoUsuario = null;

		if (NeoUtils.safeIsNull(usuario))
		{
			throw new Exception("Não foi possível obter o usuário.");
		}

		grupoUsuario = usuario.getGroup();
		if (NeoUtils.safeIsNull(grupoUsuario))
		{
			throw new Exception("Não foi possível obter o grupo do usuário.");
		}

		if (grupoUsuario.getGroupLevel() >= nivel)
		{
			throw new Exception("Grupo do usuário é igual ou maior do que o solicitado.");
		}
		else
		{
			while (grupoUsuario.getGroupLevel() < nivel)
			{
				grupoUsuario = grupoUsuario.getUpperLevel();
			}

			papelSuperior = grupoUsuario.getResponsible();
		}

		return papelSuperior;
	}

	/**
	 * Retorna o objeto Connection do banco de dados externo baseado nas fontes de dados cadastradas no
	 * Fusion
	 * 
	 * @param String dataSource Nome da fonte de dados cadsatrada no Fusion
	 * @return Connection objeto de conexão com o banco
	 */
	public static Connection getSqlConnection(String dataSource)
	{
		Connection connection = null;

		connection = PersistEngine.getConnection(dataSource);

		return connection;
	}

	/**
	 * Inicia o workflow de Tarefa Simples e retorna <b>true</b> se o workflow foi iniciado com suscesso
	 * ou <b>false</b> caso ocorra algum erro.
	 * 
	 * @param NeoObject eformProcesso Eform do processo de tarefa simples ja com seus valores peenchidos
	 * @param NeoUser responsavel Usuario responsãvel pelo workflow que estã sendo iniciado
	 * @return Boolean true se for iniciado corretamente e false caso ocorra algum erro
	 */
	public static Boolean iniciaProcessoTarefaSimples(NeoObject eformProcesso, NeoUser responsavel)
	{
		Boolean result = false;

		QLEqualsFilter equal = new QLEqualsFilter("Name", "G001 - Tarefa Simples");
		ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);
		try{
			OrsegupsWorkflowHelper.iniciaProcesso(processModel, eformProcesso, true, responsavel);
			result = true;
			return result;
		}catch(Exception e){
			result = false;
			return result;
		}

		//WFProcess proc = processModel.startProcess(eformProcesso, true, null, null, null, null, responsavel);

		//proc.setSaved(true);

		// Necessita persistir para que o metodo getOpenActivities() consiga buscar as atividades abertas
		//PersistEngine.persist(proc);
		//PersistEngine.commit(true);

		/* Finaliza a primeira tarefa */
		/*Task task = null;

		final List<Activity> acts = proc.getOpenActivities();
		System.out.println("acts: " + acts);
		if (acts != null)
		{
			System.out.println("acts.size(): " + acts.size());
		}
		final Activity activity1 = (Activity) acts.get(0);
		if (activity1 instanceof UserActivity)
		{
			try
			{
				if (((UserActivity) activity1).getTaskList().size() <= 0)
				{
					task = activity1.getModel().getTaskAssigner().assign((UserActivity) activity1, responsavel);
					OrsegupsWorkflowHelper.finishTaskByActivity(activity1);
				}
				else
				{
					task = ((UserActivity) activity1).getTaskList().get(0);
					OrsegupsWorkflowHelper.finishTaskByActivity(activity1);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				log.error("Erro ao iniciar a primeira atividade do processo.");
			}
		}*/
		
	}

	public static ResultVO iniciaWorkflow(NeoObject eform, String nomeProcesso, NeoObject solicitante, boolean avancaAtividade)
	{
		return iniciaWorkflow(eform, nomeProcesso, solicitante, avancaAtividade, true);
	}

	public static ResultVO iniciaWorkflow(NeoObject eform, String nomeProcesso, NeoObject solicitante, boolean avancaAtividade, boolean saved)
	{
		QLGroupFilter gf = new QLGroupFilter("OR");
		gf.addFilter(new QLEqualsFilter("name", nomeProcesso));
		gf.addFilter(new QLEqualsFilter("title", nomeProcesso));
		List<ProcessModel> pms = (List<ProcessModel>) PersistEngine.getObjects(ProcessModel.class, gf,-1,-1,"neoId DESC");
		ProcessModel pm = null;
		if (pms != null){
		    pm = pms.get(0);
		}
		return iniciaWorkflow(eform, pm, solicitante, avancaAtividade, saved);
	}

	public static ResultVO iniciaWorkflow(NeoObject eform, ProcessModel modeloProcesso, NeoObject solicitante, Boolean avancaAtividade)
	{
		return iniciaWorkflow(eform, modeloProcesso, solicitante, avancaAtividade, true);
	}

	public static ResultVO iniciaWorkflow(NeoObject eform, ProcessModel modeloProcesso, NeoObject solicitante, Boolean avancaAtividade, boolean saved)
	{
		ResultVO resultVO = new ResultVO();
		resultVO.setResult(true);
		//valida os parametros passados para o metodo
		if (eform == null)
		{
			resultVO.setResult(false);
			resultVO.appendMessage("Formulário do processo não definido.");
			log.error("Formulário do processo não definido.");
		}
		if (modeloProcesso == null)
		{
			resultVO.setResult(false);
			resultVO.appendMessage("Processo não definido.");
			log.error("Processo não definido.");
		}
		if (solicitante == null)
		{
			resultVO.setResult(false);
			resultVO.appendMessage("Solicitante do processo não definido.");
		}
		if (!resultVO.getResult())
		{
			return resultVO;
		}

		if (modeloProcesso.getProcessType().equals(ProcessModel.ProcessType.USER) && !modeloProcesso.returnUserCanStart((NeoUser) solicitante, false))
		{
			resultVO.setResult(false);
			resultVO.setMessage("Usuário '" + ((NeoUser) solicitante).getFullName() + "' sem permissão para iniciar o processo '" + modeloProcesso.getName() + "'.");
			return resultVO;
		}
		else
		{
			
			/*String tarefa = null;
			
			try{
				tarefa = OrsegupsWorkflowHelper.iniciaProcesso(modeloProcesso, eform, true, (NeoUser) solicitante, avancaAtividade, null);
				resultVO.setResult(true);
				resultVO.setProcessNeoId(OrsegupsProcessUtils.findProcessNeoID(modeloProcesso.getName(), tarefa).intValue());
				resultVO.setProcessCode(tarefa);
				
				QLEqualsFilter equal = new QLEqualsFilter("neoId", resultVO.getProcessNeoId());
				WFProcess processo = (WFProcess) PersistEngine.getObject(WFProcess.class, equal);
				
				resultVO.setWfProcess(processo);
				return resultVO;
			}catch(Exception e){
				e.printStackTrace();

				resultVO.setResult(false);
				resultVO.setMessage(e.toString());
				return resultVO;
			}*/
			
			try
			{
				/**
				 * @author neomind willian.mews - Alterado para utilizar a nova arquitetura de processos
				 *         do Fusion
				 * @date 12/03/2015
				 */

				final WFProcess processo = WorkflowService.startProcess(modeloProcesso, eform, true, (NeoUser) solicitante);

				if (avancaAtividade)
				{
					try
					{
						new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividade((NeoUser) solicitante);
					}
					catch (Exception e)
					{
						e.printStackTrace();

						resultVO.setResult(true);
						resultVO.setProcessNeoId((int) processo.getNeoId());
						resultVO.setProcessCode(processo.getCode());

						resultVO.setMessage("Erro ao avançar a primeira atividade do processo. Tarefa salva na primeira atividade.");

						if (e instanceof WorkflowException)
						{
							if (((WorkflowException) e).getErrorList().size() > 0)
							{
								resultVO.setMessage(resultVO.getMessage() + "\n " + ((WorkflowException) e).getErrorList().get(0).getMessage());
							}
						}

						return resultVO;
					}
				}

				/**
				 * FIM ALTERAÃ‡Ã•ES - NEOMIND
				 */

				resultVO.setResult(true);
				resultVO.setProcessNeoId((int) processo.getNeoId());
				resultVO.setProcessCode(processo.getCode());
				resultVO.setWfProcess(processo);
			}
			catch (Exception e)
			{
				e.printStackTrace();

				resultVO.setResult(false);
				resultVO.setMessage(e.toString());
				return resultVO;
			}

			return resultVO;
		}
				
				
				
	}

	/**
	 * Realiza o cancelamento de um processo
	 * 
	 * @param String codigoProcesso Code que do processo que se deseja cancelar
	 * @param String codigoCancelamento Codigo referente ao registro do eform [GC] Parâmetros
	 *            Cancelamento
	 * @param String motivoCancelamento Texto contendo o motivo do cancelamento do processo
	 * @return String contendo 'OK' caso o processo tenha sido cancelado corretamente ou o texto com o
	 *         erro que tenha ocorrido
	 */
	@SuppressWarnings("unchecked")
	public static String cancelWorkflowProcess(String codigoProcesso, String codigoCancelamento, String motivoCancelamento)
	{
		String result = "OK";

		QLEqualsFilter parametrosFilter = new QLEqualsFilter("codigo", codigoCancelamento);

		NeoObject parametrosCancelamento = PersistEngine.getObject(AdapterUtils.getEntityClass("ParametrosCancelamento"), parametrosFilter);
		if (parametrosCancelamento != null)
		{
			EntityWrapper parametrosCancelamentoWrapper = new EntityWrapper(parametrosCancelamento);

			String classeValidacao = (String) parametrosCancelamentoWrapper.findValue("classeValidacao");
			String motivoCancelamentoPadrao = (String) parametrosCancelamentoWrapper.findValue("textoPadraoCancelamento");
			ProcessModel processModel = (ProcessModel) parametrosCancelamentoWrapper.findValue("Processo");

			QLEqualsFilter codigoFilter = new QLEqualsFilter("code", codigoProcesso);
			QLEqualsFilter processoFilter = new QLEqualsFilter("model", processModel);

			QLGroupFilter groupFilter = new QLGroupFilter("AND");
			groupFilter.addFilter(codigoFilter);
			groupFilter.addFilter(processoFilter);

			WFProcess wfProcess = PersistEngine.getObject(WFProcess.class, groupFilter);

			if (wfProcess != null)
			{
				if (wfProcess.getProcessState() != ProcessState.running)
				{
					return "O processo '" + processModel.getName() + "' de número '" + codigoProcesso + "' não está em execução.";
				}

				try
				{
					if (classeValidacao != null && !classeValidacao.isEmpty())
					{
						Class<WorkflowEventListener> clazz = (Class<WorkflowEventListener>) Class.forName(classeValidacao);
						WorkflowEventListener listener = clazz.newInstance();
						if (listener instanceof WorkflowValidateCancelEventListener)
						{
							WorkflowValidateCancelEventListener validateCancelEventListener = (WorkflowValidateCancelEventListener) listener;
							WorkflowValidateCancelEvent workflowCanCancelEvent = new WorkflowValidateCancelEvent();
							workflowCanCancelEvent.setProcess(wfProcess.getProcess());
							EntityWrapper processWrapper = new EntityWrapper(wfProcess.getProcessEntity());
							workflowCanCancelEvent.setWrapper(processWrapper);
							validateCancelEventListener.validateCancel(workflowCanCancelEvent);
						}
					}
				}
				catch (WorkflowException wex)
				{
					//throw wex;
					if (wex.getErrorList() != null && wex.getErrorList().size() > 0)
					{
						return wex.getErrorList().get(0).toString();
					}
					else
					{
						return "";
					}
				}
				catch (Exception ex)
				{
					if (log.isErrorEnabled())
					{
						log.error("Erro ao disparar evento de validação de cancelamento do processo. (WorkflowValidateCancelEventListener)", ex);
					}
				}

				WFEngine wfEngine = WFEngine.getInstance();

				String motivo = "";

				if (motivoCancelamentoPadrao != null && !motivoCancelamentoPadrao.isEmpty())
				{
					motivo = motivoCancelamentoPadrao;
				}

				if (motivoCancelamento != null && !motivoCancelamento.isEmpty())
				{
					motivo += " " + motivoCancelamento;
				}

				wfEngine.cancelProcess(wfProcess, motivo);

			}
			else
			{
				return "Não foi possível encontrar o processo '" + processModel.getName() + "' de número '" + codigoProcesso + "'.";
			}
		}
		else
		{
			return "Não foi possível encontrar os parâmetros de cancelamento com o codigo '" + codigoCancelamento + "'.";
		}

		return result;
	}

	/**
	 * Converte o objeto TimeStamp retornado de um resultSet e transforma em GregorianCalendar
	 * 
	 * @param Timestamp timestamp Data do tipo Timestamp
	 * @return GregorianCalendar data convertida ou null
	 */
	public static GregorianCalendar getGregorianCalendarFromTimeStamp(Timestamp timestamp)
	{
		GregorianCalendar calendar = new GregorianCalendar();
		if (timestamp != null)
		{
			calendar.setTimeInMillis(timestamp.getTime());
			return calendar;
		}
		return null;
	}

	/**
	 * Formata o objeto do tipo TimeStamp para uma String no formato 31/12/2012 18:57
	 * 
	 * @param Timestamp timestamp Data do tipo Timestamp
	 * @return String formatada ou null
	 */
	public static String getFormatedTimeStamp(Timestamp timestamp)
	{
		String result = "";

		GregorianCalendar calendar = getGregorianCalendarFromTimeStamp(timestamp);

		result = NeoUtils.safeDateFormat(calendar);

		return result;
	}

	public static String getFormatedTimeStampDate(Timestamp timestamp)
	{
		String result = "";

		GregorianCalendar calendar = getGregorianCalendarFromTimeStamp(timestamp);

		result = NeoUtils.safeDateFormat(calendar, "dd/MM/yyyy");

		return result;
	}

	public static String getCallLink(String numeroOrigem, String numeroDestino, Boolean validaRamal)
	{
		String link = null;
		
		if(numeroDestino != null){
			numeroDestino = numeroDestino.replaceAll("[^0-9]", "");
			
			if (numeroDestino.startsWith("0")){
			    numeroDestino = numeroDestino.substring(1);
			}
	
			
			Boolean faixaOk = true;
			if (validaRamal)
			{
				faixaOk = OrsegupsUtils.validaFaixaRamaisTelefone(numeroDestino);
			}
	
			if (numeroDestino != null && !numeroDestino.isEmpty() && !numeroDestino.equalsIgnoreCase("null") && numeroOrigem != null && !numeroOrigem.isEmpty() && !numeroOrigem.equalsIgnoreCase("null") && faixaOk)
			{
	
				StringBuffer html = new StringBuffer();
	
				html.append("	result = callSync('" + PortalUtil.getBaseURL() + "servlet/com.neomind.fusion.custom.orsegups.servlets.RamalServlet?ramaOrigem=" + numeroOrigem + "&ramalDestino=" + numeroDestino + "');");
				html.append("	alert(result);");
	
				link = "<span style='cursor:pointer;text-decoration: underline;' onclick=\"" + html.toString() + "\">" + numeroDestino + "</a></span>";
			}
		}
		return link;
	}

	public static Boolean validaFaixaRamaisTelefone(String ramalDestino)
	{
		Boolean result = false;
		
		if (ramalDestino.startsWith("0")){
		    ramalDestino = ramalDestino.substring(1);
		}

		if (NeoUtils.safeIsNotNull(ramalDestino) && ramalDestino.trim().length() == 4)
		{
			Long ramal = Long.valueOf(ramalDestino.trim());

			if ((ramal >= 2200 && ramal <= 3099) || (ramal >= 3200 && ramal <= 3299) || (ramal >= 6600 && ramal <= 6999))
			{
				result = true;
			}
		}
				
		if (NeoUtils.safeIsNotNull(ramalDestino) && ( ramalDestino.trim().length() == 10 || ramalDestino.trim().length() == 11 )){
			result = true;
		}
		
		return result;
	}

	public static GregorianCalendar getNextWorkDay(GregorianCalendar date)
	{
		GregorianCalendar g = (GregorianCalendar) date.clone();
		boolean ok = false;

		g.add(Calendar.DAY_OF_MONTH, 1);

		while (!ok)
		{
			//String vencimentoStr = NeoUtils.safeDateFormat(g, "dd/MM/yyyy");
			//System.out.println("getNextWorkDay: " + vencimentoStr);
			if (!isWorkDay(g))
			{
				g.add(Calendar.DAY_OF_MONTH, 1);
			}
			else
			{
				ok = true;
			}
		}
		return g;
	}

	public static GregorianCalendar getPrevWorkDay(GregorianCalendar date)
	{
		GregorianCalendar g = (GregorianCalendar) date.clone();
		boolean ok = false;

		g.add(Calendar.DAY_OF_MONTH, -1);

		while (!ok)
		{
			//String vencimentoStr = NeoUtils.safeDateFormat(g, "dd/MM/yyyy");
			//System.out.println("getNextWorkDay: " + vencimentoStr);
			if (!isWorkDay(g))
			{
				g.add(Calendar.DAY_OF_MONTH, -1);
			}
			else
			{
				ok = true;
			}
		}
		return g;
	}

	@SuppressWarnings("unchecked")
	public static boolean isWorkDay(GregorianCalendar date)
	{
		if (date.get(GregorianCalendar.DAY_OF_WEEK) == GregorianCalendar.SATURDAY || date.get(GregorianCalendar.DAY_OF_WEEK) == GregorianCalendar.SUNDAY)
		{
			return false;
		}

		GregorianCalendar start = new GregorianCalendar();
		start.setTimeInMillis(date.getTimeInMillis());
		start.set(GregorianCalendar.HOUR_OF_DAY, 0);
		start.set(GregorianCalendar.MINUTE, 0);
		start.set(GregorianCalendar.SECOND, 0);
		start.set(GregorianCalendar.MILLISECOND, 0);

		//String startStr = NeoUtils.safeDateFormat(start, "dd/MM/yyyy");
		//System.out.println("isWorkDay start: " + startStr);

		QLEqualsFilter filter = new QLEqualsFilter("date", start);
		List<Holiday> lista = new ArrayList<Holiday>();
		lista = PersistEngine.getObjects(Holiday.class, filter);

		return lista.size() == 0;
	}
	
	public static GregorianCalendar getNextWorkDayCalendarioJuridico(GregorianCalendar date)
	{
		GregorianCalendar g = (GregorianCalendar) date.clone();
		boolean ok = false;

		g.add(Calendar.DAY_OF_MONTH, 1);

		while (!ok)
		{
			//String vencimentoStr = NeoUtils.safeDateFormat(g, "dd/MM/yyyy");
			//System.out.println("getNextWorkDay: " + vencimentoStr);
			if (!isWorkDayCalendarioJuridico(g))
			{
				g.add(Calendar.DAY_OF_MONTH, 1);
			}
			else
			{
				ok = true;
			}
		}
		return g;
	}

	public static GregorianCalendar getPrevWorkDayCalendarioJuridico(GregorianCalendar date)
	{
		GregorianCalendar g = (GregorianCalendar) date.clone();
		boolean ok = false;

		g.add(Calendar.DAY_OF_MONTH, -1);

		while (!ok)
		{
			//String vencimentoStr = NeoUtils.safeDateFormat(g, "dd/MM/yyyy");
			//System.out.println("getNextWorkDay: " + vencimentoStr);
			if (!isWorkDayCalendarioJuridico(g))
			{
				g.add(Calendar.DAY_OF_MONTH, -1);
			}
			else
			{
				ok = true;
			}
		}
		return g;
	}
	
	@SuppressWarnings("unchecked")
	public static boolean isWorkDayCalendarioJuridico(GregorianCalendar date)
	{		
	    if ((date.get(GregorianCalendar.DAY_OF_MONTH) >= 20  && date.get(GregorianCalendar.MONTH) == 11) && (date.get(GregorianCalendar.DAY_OF_YEAR) <= 20 && date.get(GregorianCalendar.MONTH) == 0))
		return false;
	    else
		return isWorkDay(date);
	}

	/**
	 * Verificar se é apenas feriado, desconsidera sabado e domingo
	 * 
	 * @param date
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static boolean isHoliday(GregorianCalendar date)
	{
		GregorianCalendar start = new GregorianCalendar();
		start.setTimeInMillis(date.getTimeInMillis());
		start.set(GregorianCalendar.HOUR_OF_DAY, 0);
		start.set(GregorianCalendar.MINUTE, 0);
		start.set(GregorianCalendar.SECOND, 0);
		start.set(GregorianCalendar.MILLISECOND, 0);

		//String startStr = NeoUtils.safeDateFormat(start, "dd/MM/yyyy");
		//System.out.println("isWorkDay start: " + startStr);

		QLEqualsFilter filter = new QLEqualsFilter("date", start);
		List<Holiday> lista = new ArrayList<Holiday>();
		lista = PersistEngine.getObjects(Holiday.class, filter);

		return lista.size() == 0;
	}

	public static GregorianCalendar getSpecificWorkDay(GregorianCalendar baseDate, Long workDay)
	{
		GregorianCalendar vencimento = (GregorianCalendar) baseDate.clone();
		vencimento.set(GregorianCalendar.HOUR_OF_DAY, 0);
		vencimento.set(GregorianCalendar.MINUTE, 0);
		vencimento.set(GregorianCalendar.SECOND, 0);
		vencimento.set(GregorianCalendar.MILLISECOND, 0);

		Long cont = workDay;

		if (workDay < 0L)
		{
			for (int x = 0; x > cont; x--)
			{
				vencimento = getPrevWorkDay(vencimento);
			}
		}
		else
		{
			for (int x = 0; x < cont; x++)
			{
				vencimento = getNextWorkDay(vencimento);
			}
		}
		return vencimento;
	}
	
	/**
	 * 
	 * @param baseDate - Data base para cálculo
	 * @param workDay - número de dias a serem adicionados a data base
	 * @return - data com dias adicionados
	 * 
	 * O método irá somar o número de dias definidos em workDay na data definida na baseDate, esses dias serão somados levando em consideração o horário atual.
	 */
	public static GregorianCalendar getSpecificWorkDay24Hr(GregorianCalendar baseDate, Long workDay)
	{
		GregorianCalendar vencimento = (GregorianCalendar) baseDate.clone();
		
		Long cont = workDay;

		if (workDay < 0L)
		{
			for (int x = 0; x > cont; x--)
			{
				vencimento = getPrevWorkDay(vencimento);
			}
		}
		else
		{
			for (int x = 0; x < cont; x++)
			{
				vencimento = getNextWorkDay(vencimento);
			}
		}
		return vencimento;
	}

	public static GregorianCalendar addDays(GregorianCalendar baseDate, Integer days, Boolean zeroToHours)
	{
		GregorianCalendar newDate = (GregorianCalendar) baseDate.clone();
		if (zeroToHours)
		{
			newDate.set(GregorianCalendar.HOUR_OF_DAY, 0);
			newDate.set(GregorianCalendar.MINUTE, 0);
			newDate.set(GregorianCalendar.SECOND, 0);
			newDate.set(GregorianCalendar.MILLISECOND, 0);
		}
		newDate.add(GregorianCalendar.DATE, days);

		return newDate;
	}

	public static String getSiglaRegional(Long codigo)
	{
		Map<Long, String> regionais = new HashMap<Long, String>();
		regionais.put(1L, "SOO");
		regionais.put(2L, "IAI");
		regionais.put(3L, "BQE");
		regionais.put(4L, "BNU");
		regionais.put(5L, "JLE");
		regionais.put(6L, "LGS");
		regionais.put(7L, "CUA");
		regionais.put(8L, "GPR");
		regionais.put(9L, "ACL");
		regionais.put(10L, "CCO");
		regionais.put(11L, "RSL");
		regionais.put(12L, "JGS");
		regionais.put(13L, "CTA Norte");
		regionais.put(14L, "CSC");
		regionais.put(15L, "CTA Sul");
		regionais.put(16L, "TRO");

		return regionais.get(codigo);
	}

	public static Map<Long, String> getListaRegional()
	{
		Map<Long, String> regionais = new HashMap<Long, String>();
		regionais.put(1L, "São José");
		regionais.put(2L, "Itajaí");
		regionais.put(3L, "Brusque");
		regionais.put(4L, "Blumenau");
		regionais.put(5L, "Joinville");
		regionais.put(6L, "Lages");
		regionais.put(7L, "Criciúma");
		regionais.put(8L, "Gaspar");
		regionais.put(9L, "Asseio");
		regionais.put(10L, "Chapecó");
		regionais.put(11L, "Rio do Sul");
		regionais.put(12L, "Jaraguá do Sul");
		regionais.put(13L, "Curitiba Norte");
		regionais.put(14L, "Cascavel");
		regionais.put(15L, "Curitiba Sul");
		regionais.put(16L, "Tubarão");

		return regionais;
	}

	public static Boolean checkPaperPermission(String paperCode)
	{
		String[] codes = paperCode.split(",");

		return checkPermission(codes, NeoPaper.class);
	}

	public static Boolean checkGroupPermission(String groupCode)
	{
		String[] codes = groupCode.split(",");

		return checkPermission(codes, NeoGroup.class);
	}

	/**
	 * @author orsegups lucas.avila - Classe generica.
	 * @date 08/07/2015
	 */
	private static Boolean checkPermission(String[] codes, Class<? extends NeoObject> clazz)
	{
		NeoUser neoUser = PortalUtil.getCurrentUser();
		if (neoUser.isAdm())
		{
			return true;
		}

		Boolean access = false;

		if (neoUser != null)
		{
			for (String code : codes)
			{
				QLEqualsFilter equalsFilter = new QLEqualsFilter("code", code.trim());

				/**
				 * @author orsegups lucas.avila - Classe generica.
				 * @date 08/07/2015
				 */
				SecurityEntity securityEntity = (SecurityEntity) PersistEngine.getObject(clazz, equalsFilter);

				if (securityEntity != null && securityEntity.contains(neoUser))
				{
					access = true;
					break;
				}
			}
		}

		return access;
	}

	public static GregorianCalendar atribuiHora(GregorianCalendar data, int hora, int minuto, int segundo, int milisegundo)
	{
		if (data == null)
		{
			return null;
		}
		else
		{
			GregorianCalendar dataRetorno = (GregorianCalendar) data.clone();

			dataRetorno.set(Calendar.HOUR_OF_DAY, hora);
			dataRetorno.set(Calendar.MINUTE, minuto);
			dataRetorno.set(Calendar.SECOND, segundo);
			dataRetorno.set(Calendar.MILLISECOND, milisegundo);
			return dataRetorno;
		}
	}

	public static GregorianCalendar atribuiHora(GregorianCalendar data)
	{
		if (data == null)
		{
			return null;
		}
		else
		{
			GregorianCalendar dataRetorno = (GregorianCalendar) data.clone();

			dataRetorno.set(Calendar.HOUR_OF_DAY, 0);
			dataRetorno.set(Calendar.MINUTE, 0);
			dataRetorno.set(Calendar.SECOND, 0);
			dataRetorno.set(Calendar.MILLISECOND, 0);
			return dataRetorno;
		}
	}

	public static GregorianCalendar atribuiData(GregorianCalendar data, GregorianCalendar outraData)
	{
		if (data == null || outraData == null)
		{
			return null;
		}
		else
		{
			GregorianCalendar dataRetorno = (GregorianCalendar) data.clone();

			dataRetorno.set(Calendar.DATE, outraData.get(Calendar.DATE));
			dataRetorno.set(Calendar.MONTH, outraData.get(Calendar.MONTH));
			dataRetorno.set(Calendar.YEAR, outraData.get(Calendar.YEAR));
			return dataRetorno;
		}
	}

	// Executa um VM em algum converter (overloaded)
	public static String rodaVm(String pathVM, String neoIdObjeto, String valueTxtId, String valueId)
	{

		String servletPath = PortalUtil.getBaseURL();

		VelocityContext context = new VelocityContext();
		context.put("servletPath", servletPath);
		context.put("neoIdObj", neoIdObjeto);
		context.put("valueTxtId", valueTxtId);
		context.put("valueId", valueId);

		return VelocityUtils.runTemplate(pathVM, context);

	}

	public static String formatarString(String texto, String mascara) throws ParseException
	{
		MaskFormatter mf = new MaskFormatter(mascara);
		mf.setValueContainsLiteralCharacters(false);
		return mf.valueToString(texto);
	}

	/**
	 * @author orsegups lucas.avila - alterado Clob Hibernate.
	 * @date 08/07/2015
	 */
	public static String getStringFromClob(Clob clob)
	{
		String result = "";

		if (clob != null)
		{
			BufferedReader bf = null;
			try
			{
				bf = new BufferedReader(clob.getCharacterStream());

				String linha = null;

				while ((linha = bf.readLine()) != null)
				{
					result = result + linha;
				}
			}
			catch (Exception e)
			{
				log.error("Erro ao transformar Clob para String!");
				e.printStackTrace();
			}
			finally
			{
				if (bf != null)
				{
					try
					{
						bf.close();
					}
					catch (IOException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}

		return result;
	}

	/**
	 * Método que retorna o instalador padrão: Código: 11257 - SOO - terc - Ana Paula Conrad
	 * 
	 * @return
	 */
	public static NeoObject getInstaladorSigmaPadrao()
	{
		NeoObject colaborador = null;

		QLEqualsFilter colaboradorFilter = new QLEqualsFilter("cd_colaborador", 89428);
		List<NeoObject> colaboradores = PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMACOLABORADOR"), colaboradorFilter);

		if (colaboradores != null && !colaboradores.isEmpty())
		{
			colaborador = colaboradores.get(0);
		}
		return colaborador;
	}

	/**
	 * Busca o instalador (tabela colaborador do Sigma) a aprtir de uma conta (tabela dbCentral do Sigma)
	 * 
	 * @param NeoObject conta - objeto do eform externo SIGMACENTRAL
	 * @return NeoObject colaborador - objeto do eform externo SIGMACOLABORADOR
	 */
	@SuppressWarnings("unchecked")
	public static NeoObject getInstaladorSigmafromConta(NeoObject conta)
	{
		NeoObject colaborador = null;

		if (conta != null)
		{
			EntityWrapper contaWrapper = new EntityWrapper(conta);

			Long idRota = (Long) contaWrapper.findValue("id_rota");

			QLEqualsFilter rotaFilter = new QLEqualsFilter("cd_rota", idRota);
			List<NeoObject> rotas = PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMAROTA"), rotaFilter);

			if (rotas != null && !rotas.isEmpty())
			{
				EntityWrapper rotaWrapper = new EntityWrapper(rotas.get(0));
				String nmRota = (String) rotaWrapper.findValue("nm_rota");
				Long cdColaborador = null;

				//TODO - remover do fonte e colocar em um eform dinamico para que seja possivel trocar os responsaveis sem alterar o codigo
				if (nmRota.startsWith("RSL") || nmRota.startsWith("TRO") || nmRota.startsWith("CUA") || nmRota.startsWith("BNU") || nmRota.startsWith("BQE") || nmRota.startsWith("GPR") || nmRota.startsWith("TRO"))
				{
					cdColaborador = 89645L; // CM - HENVELY FATIMA DA SILVA
				}
				else if (nmRota.startsWith("LGS") || nmRota.startsWith("CCO") || nmRota.startsWith("JGS") || nmRota.startsWith("JLE"))
				{
					cdColaborador = 89644L; // CM - GIANI DUARTE PERICO
				}
				else if (nmRota.startsWith("IAI"))
				{
					cdColaborador = 105147L; // CM - FABIANE DE MATOS
				}
				else if (nmRota.startsWith("SOO"))
				{
					cdColaborador = 33561L; // CM - DGECY TRUPEL
				}
				else if (nmRota.startsWith("CTA"))
				{
					cdColaborador = 105170L; // CM - RUBIANE CURCIO CORREA
				}
				else
				{
					cdColaborador = 89616L; // CM - DAYANE RAQUEL VARELLA
				}

				if (cdColaborador != null)
				{
					QLEqualsFilter colaboradorFilter = new QLEqualsFilter("cd_colaborador", cdColaborador);
					List<NeoObject> colaboradores = PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMACOLABORADOR"), colaboradorFilter);

					if (colaboradores != null && !colaboradores.isEmpty())
					{
						colaborador = colaboradores.get(0);
					}
				}
			}
		}

		return colaborador;
	}

	/**
	 * Busca o defeito (tabela OSDEFEITO do Sigma) padrão para abertura de OS no Sigma
	 * 
	 * @return NeoObject defeito - objeto do eform externo SIGMAOSDEFEITO
	 */
	@SuppressWarnings("unchecked")
	public static NeoObject getOsDefeitoPadraoSigma(NeoObject conta)
	{
		NeoObject defeito = null;

		QLEqualsFilter defeitoFilter = new QLEqualsFilter("idosdefeito", 632L); // A CLASSIFICAR
		List<NeoObject> defeitos = PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMAOSDEFEITO"), defeitoFilter);

		if (defeitos != null && !defeitos.isEmpty())
		{
			defeito = defeitos.get(0);
		}

		return defeito;
	}

	/**
	 * Converte String em GregorianCalendar
	 * 
	 * @param value
	 * @param format
	 * @return
	 * @throws ParseException
	 */
	public static GregorianCalendar stringToGregorianCalendar(String value, String format) throws ParseException
	{
		GregorianCalendar data = new GregorianCalendar();
		SimpleDateFormat df = new SimpleDateFormat(format);

		Date d = df.parse(value);
		data.setTime(d);

		return data;
	}

	/**
	 * Remove acentos de uma determinada string
	 * 
	 * @param str
	 * @return String sem acento
	 */
	public static String removeAccents(String str)
	{
		str = Normalizer.normalize(str, Normalizer.Form.NFD);
		str = str.replaceAll("[^\\p{ASCII}]", "");
		return str;
	}

	/**
	 * Remove apenas números
	 * 
	 * @param str
	 * @return
	 */
	public static String GetOnlyNumbers(String str)
	{
		String somenteNumeros = Pattern.compile("[^0-9]").matcher(str).replaceAll("");
		return somenteNumeros;
	}

	/**
	 * Retorna o dia da semana
	 * 
	 * @param data
	 * @return String do dia
	 */
	public static String getDayOfWeek(GregorianCalendar data)
	{
		String[] diaSemana = new String[] { "Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado" };

		return diaSemana[data.get(GregorianCalendar.DAY_OF_WEEK) - 1];
	}

	/**
	 * Retorna a competencia no formato MM/yyyy
	 * 
	 * @param data
	 * @return
	 */
	public static String getCompetencia(GregorianCalendar data)
	{
		String competencia = "";
		GregorianCalendar newData = (GregorianCalendar) data.clone();

		if (newData.get(GregorianCalendar.DAY_OF_MONTH) > 15)
		{
			newData.add(Calendar.MONTH, +1);
			competencia = NeoUtils.safeDateFormat(newData, "MM/yyyy");
		}
		else
		{
			competencia = NeoUtils.safeDateFormat(newData, "MM/yyyy");
		}

		return competencia;
	}

	/**
	 * Retorna a competencia no formato MM/yyyy
	 * 
	 * @param data
	 * @return
	 */
	public static String getCompetenciaApuracao(GregorianCalendar data)
	{
		String competencia = "";
		GregorianCalendar newData = (GregorianCalendar) data.clone();

		if (newData.get(GregorianCalendar.DAY_OF_MONTH) > 20)
		{
			newData.add(Calendar.MONTH, +1);
			competencia = NeoUtils.safeDateFormat(newData, "MM/yyyy");
		}
		else
		{
			competencia = NeoUtils.safeDateFormat(newData, "MM/yyyy");
		}

		return competencia;
	}

	/**
	 * Retorna o primeiro dia da competencia
	 * 
	 * @param data
	 * @return
	 * @throws ParseException
	 */
	public static GregorianCalendar getDiaInicioCompetencia(String data) throws ParseException
	{
		String competencia = "16/" + data;
		GregorianCalendar gData = stringToGregorianCalendar(competencia, "dd/MM/yyyy");
		gData.add(Calendar.MONTH, -1);

		return gData;
	}

	/**
	 * Retorna o ultimo dia da competencia
	 * 
	 * @param data
	 * @return
	 * @throws ParseException
	 */
	public static GregorianCalendar getDiaFimCompetencia(String data) throws ParseException
	{
		String competencia = "15/" + data;
		GregorianCalendar gData = stringToGregorianCalendar(competencia, "dd/MM/yyyy");

		gData.set(Calendar.HOUR_OF_DAY, 23);
		gData.set(Calendar.MINUTE, 59);
		gData.set(Calendar.SECOND, 59);

		return gData;
	}

	public static String getHorario(Long horario)
	{
		String horarioString = "";

		int hora = horario.intValue() / 60;
		int minuto = horario.intValue() % 60;

		String horaString = String.format("%03d", hora);
		String minutoString = String.format("%02d", minuto);

		horarioString = horaString + ":" + minutoString;
		return horarioString;
	}

	public static String getHorarioBanco(Long horario)
	{
		String horarioString = "";

		int hora = horario.intValue() / 60;
		int minuto = horario.intValue() % 60;
		if (minuto < 0)
		{
			minuto = minuto * (-1);
		}

		double valor = (minuto * 100) / 60.0;
		int valor2 = (int) Math.round(valor);

		if (valor < 1)
		{
			minuto = valor2;
		}
		else
		{
			valor2 = minuto;
		}

		String horaString = String.format("%03d", hora);
		String minutoString = String.format("%02d", minuto);
		if (horario < 0 && hora == 0)
		{
			horaString = "-" + horaString;
		}

		horarioString = horaString + ":" + minutoString;
		return horarioString;
	}

	public static NeoPaper getPapelGerenteRegional(Long codreg) throws WorkflowException
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 0:
				nomePapel = "Gerente Comercial Mercado Privado";
				break;
			case 1:
				nomePapel = "Gerente de Segurança";
				break;
			case 2:
				nomePapel = "Gerente Escritorio Regional Balneário Camboriu";
				break;
			case 3:
				nomePapel = "Gerente Escritorio Regional Brusque";
				break;
			case 4:
				nomePapel = "Gerente Escritorio Regional Blumenal";
				break;
			case 5:
				nomePapel = "Coordenador Escritorio Regional JLE";
				break;
			case 6:
				nomePapel = "Gerente Escritorio Regional Lages";
				break;
			case 7:
				nomePapel = "Gerente Escritório Regional CUA";
				break;
			case 8:
				nomePapel = "Gerente Escritorio Regional Gaspar";
				break;
			case 9:
				nomePapel = "Gerente de Asseio Conserv. e Limpeza";
				break;
			case 10:
				nomePapel = "Gerente Escritorio Regional Chapeco";
				break;
			case 11:
				nomePapel = "Gerente Escritório Regional Rio do Sul";
				break;
			case 12:
				nomePapel = "Gerente Escritório Regional Jaraguá do Sul";
				break;
			case 13:
				nomePapel = "Gerente Escritório Regional Curitiba";
				break;
			case 14:
				nomePapel = "Gerente Escritorio Regional Cascavel";
				break;
			case 15:
				nomePapel = "Gerente Escritório Regional Curitiba Sul";
				break;
			case 16:
				nomePapel = "Gerente Escritório Regional Tubarão";
				break;
			case 17:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
			case 18:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
			case 19:
				nomePapel = "GerenteEscritorioRegionalCampinas";
				break;
			case 20:
				nomePapel = "GerenteEscritorioRegionalGoiania";
				break;
			case 21:
				nomePapel = "GerenteEscritorioRegionalGoiania";
				break;
			case 22:
				nomePapel = "GerenteEscritorioRegionalSaoJoseDoRioPreto";
				break;
			case 23:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;

		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		if (obj != null && obj.getAllUsers() != null && !obj.getAllUsers().isEmpty())
		{
			return obj;
		}
		else
		{
			throw new WorkflowException("Gerente responsável não localizado!");
		}
	}
	
	public static NeoPaper getPapelRankingAIT(Long codreg)
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 1:
				nomePapel = "AITRankingSOO";
				break;
			case 2:
				nomePapel = "AITRankingIAI";
				break;
			case 3:
				nomePapel = "AITRankingBQE";
				break;
			case 4:
				nomePapel = "AITRankingBNU";
				break;
			case 5:
				nomePapel = "AITRankingJLE";
				break;
			case 6:
				nomePapel = "AITRankingLGS";
				break;
			case 7:
				nomePapel = "AITRankingCUA";
				break;
			case 8:
				nomePapel = "AITRankingGPR";
				break;
			case 10:
				nomePapel = "AITRankingCCO";
				break;
			case 11:
				nomePapel = "AITRankingRSL";
				break;
			case 12:
				nomePapel = "AITRankingJGS";
				break;
			case 13:
				nomePapel = "AITRankingCTA";
				break;
			case 14:
				nomePapel = "AITRankingCSC";
				break;
			case 16:
				nomePapel = "AITRankingTRO";
				break;
			default:
				nomePapel = "AtualizarAcessosSistemas";
				break;

		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		return obj;
	}
	
	
	public static NeoPaper getPapelRankingHoristas(Long codreg)
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 1:
				nomePapel = "HoristasRankingSOO";
				break;
			case 2:
				nomePapel = "HoristasRankingIAI";
				break;
			case 3:
				nomePapel = "HoristasRankingBQE";
				break;
			case 4:
				nomePapel = "HoristasRankingBNU";
				break;
			case 5:
				nomePapel = "HoristasRankingJLE";
				break;
			case 6:
				nomePapel = "HoristasRankingLGS";
				break;
			case 7:
				nomePapel = "HoristasRankingCUA";
				break;
			case 8:
				nomePapel = "HoristasRankingGPR";
				break;
			case 10:
				nomePapel = "HoristasRankingCCO";
				break;
			case 11:
				nomePapel = "HoristasRankingRSL";
				break;
			case 12:
				nomePapel = "HoristasRankingJGS";
				break;
			case 13:
				nomePapel = "HoristasRankingCTA";
				break;
			case 14:
				nomePapel = "HoristasRankingCSC";
				break;
			case 16:
				nomePapel = "HoristasRankingTRO";
				break;
			default:
				nomePapel = "AtualizarAcessosSistemas";
				break;

		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		return obj;
	}
	
	
	
	public static NeoPaper getPapelCoordenadorRegional(Long codreg)
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 1:
				nomePapel = "Coordenador de Segurança";
				break;
			case 2:
				nomePapel = "Coordenador Escritorio Regional IAI";
				break;
			case 3:
				nomePapel = "Coordenador Escritorio Regional BQE";
				break;
			case 4:
				nomePapel = "Coordenador Escritorio Regional BNU";
				break;
			case 5:
				nomePapel = "Coordenador Escritorio Regional JLE";
				break;
			case 6:
				nomePapel = "Coordenador Escritorio Regional LGS";
				break;
			case 7:
				nomePapel = "CoordenadorEscritorioRegionalCUA";
				break;
			case 8:
				nomePapel = "Coordenador Escritório Regional Gaspar";
				break;
			case 9:
				nomePapel = "Coordenador de Asseio Conserv. e Limpeza";
				break;
			case 10:
				nomePapel = "Coordenador Escritório Regional CCO";
				break;
			case 11:
				nomePapel = "Coordenador Escritório Regional RSL";
				break;
			case 12:
				nomePapel = "Coordenador Escritório Regional JGS";
				break;
			case 13:
				nomePapel = "Coordenador Escritório Regional CTA";
				break;
			case 14:
				nomePapel = "Coordenador Escritório Regional CSC";
				break;
			case 15:
				nomePapel = "Coordenador Escritório Regional CTA Sul";
				break;
			case 16:
				nomePapel = "Coordenador Escritório Regional TRO";
				break;
			case 17:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
			case 18:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
			case 19:
				nomePapel = "GerenteEscritorioRegionalCampinas";
				break;
			case 20:
				nomePapel = "GerenteEscritorioRegionalGoiania";
				break;
			case 21:
				nomePapel = "GerenteEscritorioRegionalGoiania";
				break;
			case 22:
				nomePapel = "GerenteEscritorioRegionalSaoJoseDoRioPreto";
				break;
			case 23:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
			default:
				nomePapel = "AtualizarAcessosSistemas";
				break;

		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		return obj;
	}

	public static NeoPaper getPapelCoordenadorRegional(Long codreg, String lotacao)
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 0:
				if (lotacao != null && lotacao.equals("Coordenadoria NAC"))
				{
					nomePapel = "Supervisor CEREC";
				}
				
				else if (lotacao != null && lotacao.equals("Coordenadoria de Suprimentos"))
				{
					nomePapel = "Coordenador de Suprimentos";
				}
				
				else if (lotacao != null && lotacao.equals("Gerência de Rastreamento e Gestão de Frota"))
				{
					nomePapel = "Gerente Rastreamento e Gestão Frota";
				}
				
				else if (lotacao != null && lotacao.equals("Assistente Indenização Contratual"))
				{
					nomePapel = "Coordenador de Ressarcimento";
				}
				else if (lotacao != null && lotacao.equals("Auxiliar Administrativo - Jurídico"))
				{
					nomePapel = "Assessor Jurídico";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Controladoria"))
				{
					nomePapel = "Gerente Controladoria";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Comercial Administrativa"))
				{
					nomePapel = "Supervisor Comercial Privado";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Comercial Publico"))
				{
					nomePapel = "Coordenador Comercial";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria da Qualidade"))
				{
					nomePapel = "Responsável Justificar Afastamento Qualidade";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Financeira"))
				{
					nomePapel = "Coordenador Financeiro";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Compras"))
				{
					nomePapel = "Coordenador Administrativo";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Administrativa"))
				{
					nomePapel = "Coordenador Administrativo";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Almoxarifado de Eletrônica"))
				{
					nomePapel = "Coordenador Almoxarifado de Eletrônica Sede";
				}
				else if (lotacao != null && lotacao.equals("Comunicação e Marketing"))
				{
					nomePapel = "Responsável Marketing";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Sistemas da Informaçao"))
				{
					nomePapel = "Coordenador de Sistemas";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Infraestrutura"))
				{
					nomePapel = "Supervisor de Infraestrutura de TI";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Monitoramento"))
				{
					nomePapel = "Gerente de Monitoramento";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Recursos Humanos"))
				{
					nomePapel = "Coordenador de RH";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Departamento Pessoal"))
				{
					nomePapel = "Coordenador de RH";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de RH Estratégico"))
				{
					nomePapel = "Supervisor RH Estratégico";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de RH RSA"))
				{
					nomePapel = "Coordenador de Recrutamento e Seleção";
				}
				
				else if (lotacao != null && lotacao.equals("Nexti"))
				{
					nomePapel = "Coordenador de Sistemas";
					
				} else if (lotacao != null && lotacao.equals("Coordenadoria Departamento Presença")) {
					
				    nomePapel = "Gerente Projeto Presença";
				    
				} else if (lotacao != null && lotacao.equals("Coordenadoria RH Operacional")) {
					
				    nomePapel = "Gerente Projeto Presença";
				    
				} else if (lotacao != null && lotacao.equals("NAC Interno")) {
				    
				    nomePapel = "Gerente NAC Interno";
				    
				} else if (lotacao != null && lotacao.equals("Coordenadoria Portaria Remota")){ 
				    
				    nomePapel = "Gerente PRO";
				
				} else if (lotacao != null && lotacao.equals("Coordenadoria NAC Externo")){ 
				    
				    nomePapel = "Gerente CEREC";
				
				} else if (lotacao != null && lotacao.equals("Recuperação de Crédito")){
				    
				    nomePapel = "Gerente de Recuperação de Crédito";
				
				} else if (lotacao != null && lotacao.equals("Coordenadoria NAC Interno")){
				    
				    nomePapel = "Gerente NAC Interno";
				    
				} else if (lotacao != null && lotacao.equals("4B2G/NEXTI")){
				  
				    nomePapel = "ResponsavelDesenvolvimentoNexti";
				    
				} else if (lotacao != null && lotacao.equals("Coordenadoria Executiva de Negócios")){
				
				    nomePapel = "Coordenador Comercial Privado Vendas";
				    
				} else {
				
				    
					nomePapel = "AtualizarAcessosSistemas";
				}

				break;
			case 1:
			    if(lotacao != null && lotacao.equals("Coordenadoria de Segurança Operacional")){
				nomePapel = "Coordenador de Segurança";
			    } else {
				nomePapel = "Coordenador de Vigilância Eletrônica";
			    }
			    	break;
			case 2:
				nomePapel = "Coordenador Escritorio Regional IAI";
				break;
			case 3:
				nomePapel = "Coordenador Escritorio Regional BQE";
				break;
			case 4:
				nomePapel = "Coordenador Escritorio Regional BNU";
				break;
			case 5:
				nomePapel = "Responsável Tratar OS em excesso";
				break;
			case 6:
				nomePapel = "Coordenador Escritorio Regional LGS";
				break;
			case 7:
				nomePapel = "CoordenadorEscritorioRegionalCUA";
				break;
			case 8:
				nomePapel = "Coordenador Escritório Regional Gaspar";
				break;
			case 9:
				nomePapel = "Coordenador de Asseio Conserv. e Limpeza";
				break;
			case 10:
				nomePapel = "Coordenador Escritório Regional CCO";
				break;
			case 11:
				nomePapel = "Coordenador Escritório Regional RSL";
				break;
			case 12:
				nomePapel = "Coordenador Escritório Regional JGS";
				break;
			case 13:
				nomePapel = "Coordenador Escritório Regional CTA";
				break;
			case 14:
				nomePapel = "Coordenador Escritório Regional CSC";
				break;
			case 15:
				nomePapel = "Coordenador Escritório Regional CTA Sul";
				break;
			case 16:
				nomePapel = "Coordenador Escritório Regional TRO";
				break;
			case 17:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
			case 18:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
			case 19:
				nomePapel = "GerenteEscritorioRegionalCampinas";
				break;
			case 20:
				nomePapel = "GerenteEscritorioRegionalGoiania";
				break;
			case 21:
				nomePapel = "GerenteEscritorioRegionalGoiania";
				break;
			case 22:
				nomePapel = "GerenteEscritorioRegionalSaoJoseDoRioPreto";
				break;
			case 23:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
			default:
				nomePapel = "AtualizarAcessosSistemas";
				break;

		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		return obj;
	}

	public static NeoPaper getPapelJustificarAfastamento(Long codreg, String lotacao)
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 0:
				if (lotacao != null && lotacao.equals("Coordenadoria NAC"))
				{
					nomePapel = "Supervisor CEREC";
				}
				else if (lotacao != null && lotacao.equals("Assistente Indenização Contratual"))
				{
					nomePapel = "Coordenador de Ressarcimento";
				}
				else if (lotacao != null && lotacao.equals("Auxiliar Administrativo - Jurídico"))
				{
					nomePapel = "Assessor Jurídico";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Controladoria"))
				{
					nomePapel = "Gerente Controladoria";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Comercial Administrativa"))
				{
					nomePapel = "Supervisor Comercial Privado";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Comercial Publico"))
				{
					nomePapel = "Coordenador Comercial";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria da Qualidade"))
				{
					nomePapel = "Responsável Justificar Afastamento Qualidade";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Financeira"))
				{
					nomePapel = "Coordenador Financeiro";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Compras"))
				{
					nomePapel = "Coordenador Administrativo";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Administrativa"))
				{
					nomePapel = "Coordenador Administrativo";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Almoxarifado de Eletrônica"))
				{
					nomePapel = "Coordenador Almoxarifado de Eletrônica Sede";
				}
				else if (lotacao != null && lotacao.equals("Comunicação e Marketing"))
				{
					nomePapel = "Responsável Marketing";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Sistemas da Informaçao"))
				{
					nomePapel = "Coordenador de Sistemas";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Infraestrutura"))
				{
					nomePapel = "Supervisor de Infraestrutura de TI";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Monitoramento"))
				{
					nomePapel = "Justificar Afastamento CM";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Recursos Humanos"))
				{
					nomePapel = "Coordenador de RH";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Departamento Pessoal"))
				{
					nomePapel = "Coordenador de RH";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de RH Estratégico"))
				{
					nomePapel = "Gerente RH Estratégico";
				}

				else if (lotacao != null && lotacao.equals("Coordenadoria de RH RSA"))
				{
					nomePapel = "Gerente RH Estratégico";
				}
                    				
                    	    	else if (lotacao != null && lotacao.equals("Coordenadoria Portaria Remota")) 
                    	    	{
                    	    	    	nomePapel = "gerentePortariaRemota";
                    	    	}

				else
				{
					nomePapel = "Justificar Afastamento CM";
				}

				break;

			case 1:
				if (lotacao != null && lotacao.startsWith("Técnico de Eletrônica") || lotacao.startsWith("Supervisor Técnico de Eletrônica") || lotacao.startsWith("Técnicos de Eletrônica") || lotacao.startsWith("Agente de Inspeção Técnica") || lotacao != null && lotacao.startsWith("Auxiliar Técnico de Eletrônica") || lotacao != null && lotacao.startsWith("Cobertura de Férias - Segurança Eletrônica") || lotacao != null && lotacao.startsWith("Auxiliar Administrativo Eletrônica")
						|| lotacao != null && lotacao.startsWith("Supervisor de Segurança Eletrônica") || lotacao != null && lotacao.startsWith("Aviso Prévio - Segurança Eletrônica"))
				{
					nomePapel = "Justificar Afastamento Vigilância Eletrônica";
				}
				else
				{
					nomePapel = "Justificar Afastamento Segurança";
				}
				break;
			case 2:
				nomePapel = "Justificar Afastamento Escritorio Regional IAI";
				break;
			case 3:
				nomePapel = "Justificar Afastamento Escritorio Regional BQE";
				break;
			case 4:
				nomePapel = "Justificar Afastamento Escritorio Regional BNU";
				break;
			case 5:
				nomePapel = "Justificar Afastamento Escritorio Regional JLE";
				break;
			case 6:
				nomePapel = "Justificar Afastamento Escritorio Regional LGS";
				break;
			case 7:
				nomePapel = "Justificar Afastamento Escritorio Regional CUA";
				break;
			case 8:
				nomePapel = "Justificar Afastamento Escritorio Regional GPR";
				break;
			case 9:
				nomePapel = "Justificar Afastamento de Asseio Conserv. e Limpeza";
				break;
			case 10:
				nomePapel = "Justificar Afastamento Escritorio Regional CCO";
				break;
			case 11:
				nomePapel = "Justificar Afastamento Escritorio Regional RSL";
				break;
			case 12:
				nomePapel = "Justificar Afastamento Escritorio Regional JGS";
				break;
			case 13:
				//Técnicos de eletrônica CTA
				if (lotacao.startsWith("Técnico") && lotacao.contains("Eletrônica") && lotacao.endsWith("CTA"))
				{
					nomePapel = "Justificar Afastamento Técnicos CTA";
				}
				//Táticos CTA
				else if (lotacao.startsWith("ASE") && lotacao.contains("ROTA") && lotacao.endsWith("CTA"))
				{
					nomePapel = "Justificar Afastamento Táticos CTA";
				}
				else
				//Outros
				{
					nomePapel = "Gerente Escritório Regional Curitiba Sul";
				}
				break;
			case 14:
				nomePapel = "Justificar Afastamento Escritorio Regional CSC";
				break;
			case 15:
				nomePapel = "Justificar Afastamento Escritorio Regional CTA Sul";
				break;
			case 16:
				nomePapel = "Justificar Afastamento Escritorio Regional TRO";
				break;

		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		return obj;
	}

	public static NeoPaper getPapelComunicadoFerias(Long codreg)
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 1:
				nomePapel = "Imprimir Comunicado de Ferias Segurança";
				break;
			case 2:
				nomePapel = "Imprimir Comunicado de Ferias Escritorio Regional IAI";
				break;
			case 3:
				nomePapel = "Imprimir Comunicado de Ferias Escritorio Regional BQE";
				break;
			case 4:
				nomePapel = "Imprimir Comunicado de Ferias Escritorio Regional BNU";
				break;
			case 5:
				nomePapel = "Imprimir Comunicado de Ferias Escritorio Regional JLE";
				break;
			case 6:
				nomePapel = "Imprimir Comunicado de Ferias Escritorio Regional LGS";
				break;
			case 7:
				nomePapel = "Imprimir Comunicado de Ferias Escritorio Regional TRO";
				break;
			case 8:
				nomePapel = "Imprimir Comunicado de Ferias Escritorio Regional GPR";
				break;
			case 9:
				nomePapel = "Imprimir Comunicado de Ferias de Asseio Conserv. e Limpeza";
				break;
			case 10:
				nomePapel = "Imprimir Comunicado de Ferias Escritorio Regional CCO";
				break;
			case 11:
				nomePapel = "Imprimir Comunicado de Ferias Escritorio Regional RSL";
				break;
			case 12:
				nomePapel = "Imprimir Comunicado de Ferias Escritorio Regional JGS";
				break;
			case 13:
				nomePapel = "Imprimir Comunicado de Ferias Escritorio Regional CTA";
				break;
			case 14:
				nomePapel = "Imprimir Comunicado de Ferias Escritorio Regional CSC";
				break;
			case 15:
				nomePapel = "Imprimir Comunicado de Ferias Escritorio Regional CTA Sul";
				break;
			case 16:
				nomePapel = "Imprimir Comunicado de Ferias Escritorio Regional TRO";
			case 17:
				nomePapel = "ImprimirComunicadodeFeriasEscritorio Regional NHO";
			case 18:
				nomePapel = "ImprimirComunicadodeFeriasEscritorioRegionalTRI";

		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		return obj;
	}

	public static NeoPaper getPapelLancamentoFerias(Long codreg)
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 0:
				nomePapel = "Conceder Ferias SEDE";
				break;
			case 1:
				nomePapel = "Conceder Ferias Segurança";
				break;
			case 2:
				nomePapel = "Conceder Ferias Escritorio Regional IAI";
				break;
			case 3:
				nomePapel = "Conceder Ferias Escritorio Regional BQE";
				break;
			case 4:
				nomePapel = "Conceder Ferias Escritorio Regional BNU";
				break;
			case 5:
				nomePapel = "Conceder Ferias Escritorio Regional JLE";
				break;
			case 6:
				nomePapel = "Conceder Ferias Escritorio Regional LGS";
				break;
			case 7:
				nomePapel = "Conceder Ferias Escritorio Regional TRO";
				break;
			case 8:
				nomePapel = "Conceder Ferias Escritorio Regional GPR";
				break;
			case 9:
				nomePapel = "Conceder Ferias de Asseio Conserv. e Limpeza";
				break;
			case 10:
				nomePapel = "Conceder Ferias Escritorio Regional CCO";
				break;
			case 11:
				nomePapel = "Conceder Ferias Escritorio Regional RSL";
				break;
			case 12:
				nomePapel = "Conceder Ferias Escritorio Regional JGS";
				break;
			case 13:
				nomePapel = "Conceder Ferias Escritorio Regional CTA";
				break;
			case 14:
				nomePapel = "Conceder Ferias Escritorio Regional CSC";
				break;
			case 15:
				nomePapel = "Conceder Ferias Escritorio Regional CTA Sul";
				break;
			case 16:
				nomePapel = "Conceder Ferias Escritorio Regional TRO";
				break;

		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		return obj;
	}

	/**
	 * Baseado no papel de lançamento de férias
	 * 
	 * @param codreg
	 * @return
	 * */
	public static NeoPaper getPapelRTOcioso(Long codreg, String lotacao)
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 0:
				if (lotacao != null && lotacao.equals("Coordenadoria NAC"))
				{
					nomePapel = "Coordenador CEREC";
				}
				else if (lotacao != null && lotacao.equals("Assistente Indenização Contratual"))
				{
					nomePapel = "Gerente Comercial Mercado Público"; // o correto seria samara.fontoura, mas como está afastada 
				}
				else if (lotacao != null && lotacao.equals("Auxiliar Administrativo - Jurídico"))
				{
					nomePapel = "Assessor Jurídico";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Controladoria"))
				{
					nomePapel = "Gerente Controladoria";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Comercial Administrativa"))
				{
					nomePapel = "Coordenador CEREC";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Comercial Publico"))
				{
					nomePapel = "Gerente Comercial Mercado Público";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria da Qualidade"))
				{
					nomePapel = "Responsável Justificar Afastamento Qualidade";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Financeira"))
				{
					nomePapel = "Gerente Financeiro";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Compras"))
				{
					nomePapel = "Gerente Administrativo";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Administrativa"))
				{
					nomePapel = "Gerente Administrativo";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Almoxarifado de Eletrônica"))
				{
					nomePapel = "Gerente Administrativo";
				}
				else if (lotacao != null && lotacao.equals("Comunicação e Marketing"))
				{
					nomePapel = "Responsável Marketing";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Sistemas da Informaçao"))
				{
					nomePapel = "Coordenador de Sistemas";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Infraestrutura"))
				{
					nomePapel = "Supervisor de Infraestrutura de TI";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Monitoramento"))
				{
					nomePapel = "Gerente de Monitoramento";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Recursos Humanos"))
				{
					nomePapel = "Gerente de RH";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Departamento Pessoal"))
				{
					nomePapel = "Gerente de RH";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de RH Estratégico"))
				{
					nomePapel = "Gerente RH Estratégico";
				}

				else if (lotacao != null && lotacao.equals("Coordenadoria de RH RSA"))
				{
					nomePapel = "Gerente RH Estratégico";
				}

				break;

			case 1:
				if (lotacao != null && lotacao.startsWith("Técnico de Eletrônica") || lotacao.startsWith("Supervisor Técnico de Eletrônica") || lotacao.startsWith("Técnicos de Eletrônica") || lotacao.startsWith("Agente de Inspeção Técnica") || lotacao != null && lotacao.startsWith("Auxiliar Técnico de Eletrônica") || lotacao != null && lotacao.startsWith("Cobertura de Férias - Segurança Eletrônica") || lotacao != null && lotacao.startsWith("Auxiliar Administrativo Eletrônica")
						|| lotacao != null && lotacao.startsWith("Supervisor de Segurança Eletrônica") || lotacao != null && lotacao.startsWith("Aviso Prévio - Segurança Eletrônica")
						|| lotacao != null && lotacao.startsWith("Vigilância Eletrônica") || lotacao != null && lotacao.startsWith("Supervisão de Eletrônica")
						|| lotacao != null && lotacao.startsWith("Inspetor da Qualidade Eletrônica") || lotacao != null && lotacao.startsWith("Horistas Dep. Eletrônica")
						|| lotacao != null && lotacao.startsWith("Cob. Férias Dep. Eletrônica") || lotacao != null && lotacao.startsWith("Assistente Eletrônica")
						|| lotacao != null && lotacao.startsWith("OPERACIONAL ELETRONICA"))
				{
					nomePapel = "Gerente de Vigilância Eletrônica";
				}
				else
				{
					nomePapel = "Gerente de Segurança";
				}
				break;
			case 2:
				nomePapel = "Gerente Escritorio Regional Balneário Camboriu";
				break;
			case 3:
				nomePapel = "Gerente Escritorio Regional Brusque";
				break;
			case 4:
				nomePapel = "Gerente Escritorio Regional Blumenal";
				break;
			case 5:
				nomePapel = "Gerente Escritorio Regional Joinville";
				break;
			case 6:
				nomePapel = "Gerente Escritorio Regional Lages";
				break;
			case 7:
				nomePapel = "Gerente Escritório Regional CUA";
				break;
			case 8:
				nomePapel = "Gerente Escritorio Regional Gaspar";
				break;
			case 9:
				nomePapel = "Gerente de Asseio Conserv. e Limpeza";
				break;
			case 10:
				nomePapel = "Gerente Escritorio Regional Chapeco";
				break;
			case 11:
				nomePapel = "Gerente Escritório Regional Rio do Sul";
				break;
			case 12:
				nomePapel = "Gerente Escritório Regional Jaraguá do Sul";
				break;
			case 13:
				//Técnicos de eletrônica CTA
				if (lotacao.startsWith("Técnico") && lotacao.contains("Eletrônica") && lotacao.endsWith("CTA"))
				{
					nomePapel = "Gerente de Segurança Eletrônica CTA";
				}
				//Táticos CTA
				else if (lotacao.startsWith("ASE") && lotacao.contains("ROTA") && lotacao.endsWith("CTA"))
				{
					nomePapel = "Gerente de Segurança Eletrônica CTA";
				}
				else
				//Outros
				{
					nomePapel = "Gerente Escritório Regional Curitiba Sul";
				}
				break;
			case 14:
				nomePapel = "Gerente Escritorio Regional Cascavel";
				break;
			case 15:
				nomePapel = "Justificar Afastamento Escritorio Regional CTA Sul";
				break;
			case 16:
				nomePapel = "Gerente Escritório Regional Tubarão";
				break;
			case 17:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
			case 18:
				nomePapel = "SupervisorEscritorioRegionalNovoHamburgo";
				break;
			case 19:
				nomePapel = "GerenteEscritorioRegionalCampinas";
				break;
			case 20:
				nomePapel = "GerenteEscritorioRegionalGoiania";
				break;
			case 21:
				nomePapel = "GerenteEscritorioRegionalGoiania";
				break;
			case 22:
				nomePapel = "GerenteEscritorioRegionalSaoJoseDoRioPreto";
				break;
			case 23:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;

		}

		if (nomePapel.equals(""))
		{ // vai para o juliano tratar
			nomePapel = "Responsável Justificar Afastamento Qualidade";
		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		return obj;
	}

	public static NeoPaper getPapelTratarEventosExcesso(Long codreg)
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 1:
				nomePapel = "Tratar Eventos Excesso Vigilancia Eletronica";
				break;
			case 2:
				nomePapel = "Tratar Eventos Excesso Escritorio Regional IAI";
				break;
			case 3:
				nomePapel = "Tratar Eventos Excesso Escritorio Regional BQE";
				break;
			case 4:
				nomePapel = "Tratar Eventos Excesso Escritorio Regional BNU";
				break;
			case 5:
				nomePapel = "Tratar Eventos Excesso Escritorio Regional JLE";
				break;
			case 6:
				nomePapel = "Tratar Eventos Excesso Escritorio Regional LGS";
				break;
			case 7:
				nomePapel = "Tratar Eventos Excesso Escritorio Regional TRO";
				break;
			case 8:
				nomePapel = "Tratar Eventos Excesso Escritorio Regional GPR";
				break;
			case 9:
				nomePapel = "Tratar Eventos Excesso de Asseio Conserv. e Limpeza";
				break;
			case 10:
				nomePapel = "Tratar Eventos Excesso Escritorio Regional CCO";
				break;
			case 11:
				nomePapel = "Tratar Eventos Excesso Escritorio Regional RSL";
				break;
			case 12:
				nomePapel = "Tratar Eventos Excesso Escritorio Regional JGS";
				break;
			case 13:
				nomePapel = "Tratar Eventos Excesso Escritorio Regional CTA";
				break;
			case 14:
				nomePapel = "Tratar Eventos Excesso Escritorio Regional CSC";
				break;
			case 15:
				nomePapel = "Tratar Eventos Excesso Escritorio Regional CTA Sul";
				break;
			case 16:
				nomePapel = "Tratar Eventos Excesso Escritorio Regional TRO";
				break;
		}
		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		return obj;
	}

	public static NeoPaper getPapelTratarDeslocamentosExcesso(Long codreg)
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 1:
				nomePapel = "Tratar Eventos Excesso Vigilancia Eletronica";
				break;
			case 2:
				nomePapel = "Tratar Eventos Excesso Escritorio Regional IAI";
				break;
			case 3:
				nomePapel = "Tratar Eventos Excesso Escritorio Regional BQE";
				break;
			case 4:
				nomePapel = "Tratar Eventos Excesso Escritorio Regional BNU";
				break;
			case 5:
				nomePapel = "Tratar Eventos Excesso Escritorio Regional JLE";
				break;
			case 6:
				nomePapel = "Tratar Eventos Excesso Escritorio Regional LGS";
				break;
			case 7:
				nomePapel = "TratarEventosExcessoEscritorioRegionalCUA";
				break;
			case 8:
				nomePapel = "Tratar Eventos Excesso Escritorio Regional GPR";
				break;
			case 9:
				nomePapel = "Tratar Eventos Excesso de Asseio Conserv. e Limpeza";
				break;
			case 10:
				nomePapel = "Tratar Eventos Excesso Escritorio Regional CCO";
				break;
			case 11:
				nomePapel = "Tratar Eventos Excesso Escritorio Regional RSL";
				break;
			case 12:
				nomePapel = "Tratar Eventos Excesso Escritorio Regional JGS";
				break;
			case 13:
				nomePapel = "Tratar Deslocamentos excessivo Escritorio Regional CTA";//este
				break;
			case 14:
				nomePapel = "Tratar Eventos Excesso Escritorio Regional CSC";
				break;
			case 15:
				nomePapel = "Tratar Eventos Excesso Escritorio Regional CTA Sul";
				break;
			case 16:
				nomePapel = "Tratar Eventos Excesso Escritorio Regional TRO";
				break;
			case 17:
			    	nomePapel = "TratarEventosExcessoEscritorioRegionalNHO";
			    	break;
			case 18:
			    	nomePapel = "TratarEventosExcessoEscritorioRegionalTRI";
			    	break;
			case 20:
			    	nomePapel = "TratarEventosExcessoEscritorioRegionalGNA";
			    	break;
			case 21:
			    	nomePapel = "TratarEventosExcessoEscritorioRegionalPMJ";
			    	break;
			case 22:
			    	nomePapel = "TratarEventosExcessoEscritorioRegionalSRR";
			    	break;
		}
		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		return obj;
	}

	public static NeoPaper getPapelCSEConfirmarCobranca(Long codreg)
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 1:
				nomePapel = "CSE Confirmar Cobrança Segurança";
				break;
			case 2:
				nomePapel = "CSE Confirmar Cobrança Escritorio Regional IAI";
				break;
			case 3:
				nomePapel = "CSE Confirmar Cobrança Escritorio Regional BQE";
				break;
			case 4:
				nomePapel = "CSE Confirmar Cobrança Escritorio Regional BNU";
				break;
			case 5:
				nomePapel = "CSE Confirmar Cobrança Escritorio Regional JLE";
				break;
			case 6:
				nomePapel = "CSE Confirmar Cobrança Escritorio Regional LGS";
				break;
			case 7:
				nomePapel = "CSE Confirmar Cobrança Escritorio Regional CUA";
				break;
			case 8:
				nomePapel = "CSE Confirmar Cobrança Escritorio Regional GPR";
				break;
			case 9:
				nomePapel = "CSE Confirmar Cobrança de Asseio Conserv. e Limpeza";
				break;
			case 10:
				nomePapel = "CSE Confirmar Cobrança Escritorio Regional CCO";
				break;
			case 11:
				nomePapel = "CSE Confirmar Cobrança Escritorio Regional RSL";
				break;
			case 12:
				nomePapel = "CSE Confirmar Cobrança Escritorio Regional JGS";
				break;
			case 13:
				nomePapel = "CSE Confirmar Cobrança Escritorio Regional CTA";
				break;
			case 14:
				nomePapel = "CSE Confirmar Cobrança Escritorio Regional CSC";
				break;
			case 15:
				nomePapel = "CSE Confirmar Cobrança Escritorio Regional CTA Sul";
				break;
			case 16:
				nomePapel = "CSE Confirmar Cobrança Escritorio Regional TRO";
				break; 
			case 17:
				nomePapel = "CSEConfirmarCobrancaEscritorioRegionalNHO";
				break;
			case 18:
				nomePapel = "CSEConfirmarCobrancaEscritorioRegionalTRI";
				break;
			case 19:
				nomePapel = "CSEConfirmarCobrancaEscritorioRegionalCAS";
				break; 
			case 20:
				nomePapel = "CSEConfirmarCobrancaEscritorioRegionalGNA";
				break; 
			case 21:
				nomePapel = "CSEConfirmarCobrancaEscritorioRegionalPMJ";
				break; 
			case 22:
				nomePapel = "CSEConfirmarCobrancaEscritorioRegionalSRR";
				break;
		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		return obj;
	}

	public static NeoPaper getPapelFeriasProgramada(Long codreg)
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 1:
				nomePapel = "Ferias Programada Segurança";
				break;
			case 2:
				nomePapel = "Ferias Programada Escritorio Regional IAI";
				break;
			case 3:
				nomePapel = "Ferias Programada Escritorio Regional BQE";
				break;
			case 4:
				nomePapel = "Ferias Programada Escritorio Regional BNU";
				break;
			case 5:
				nomePapel = "Ferias Programada Escritorio Regional JLE";
				break;
			case 6:
				nomePapel = "Ferias Programada Escritorio Regional LGS";
				break;
			case 7:
				nomePapel = "Ferias Programada Escritorio Regional CUA";
				break;
			case 8:
				nomePapel = "Ferias Programada Escritorio Regional GPR";
				break;
			case 9:
				nomePapel = "Ferias Programada Asseio Conserv. e Limpeza";
				break;
			case 10:
				nomePapel = "Ferias Programada Escritorio Regional CCO";
				break;
			case 11:
				nomePapel = "Ferias Programada Escritorio Regional RSL";
				break;
			case 12:
				nomePapel = "Ferias Programada Escritorio Regional JGS";
				break;
			case 13:
				nomePapel = "Ferias Programada Escritorio Regional CTA";
				break;
			case 14:
				nomePapel = "Ferias Programada Escritorio Regional CSC";
				break;
			case 15:
				nomePapel = "Ferias Programada Escritorio Regional CTA Sul";
				break;
			case 16:
				nomePapel = "Ferias Programada Escritorio Regional TRO";
				break;

		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		return obj;
	}

	public static NeoPaper getPapelFaltaEfetivo(Long codreg, String lotacao)
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 0:
				if (lotacao != null && lotacao.equals("Coordenadoria NAC"))
				{
					nomePapel = "Supervisor CEREC";
				}
				else if (lotacao != null && lotacao.equals("Assistente Indenização Contratual"))
				{
					nomePapel = "Coordenador de Ressarcimento";
				}
				else if (lotacao != null && lotacao.equals("Auxiliar Administrativo - Jurídico"))
				{
					nomePapel = "Assessor Jurídico";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Controladoria"))
				{
					nomePapel = "Gerente Controladoria";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Comercial Administrativa"))
				{
					nomePapel = "Supervisor Comercial Privado";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Comercial Publico"))
				{
					nomePapel = "Coordenador Comercial";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria da Qualidade"))
				{
					nomePapel = "Responsável Justificar Afastamento Qualidade";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Financeira"))
				{
					nomePapel = "Coordenador Financeiro";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Compras"))
				{
					nomePapel = "Coordenador Administrativo";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Administrativa"))
				{
					nomePapel = "Coordenador Administrativo";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Almoxarifado de Eletrônica"))
				{
					nomePapel = "Coordenador Almoxarifado de Eletrônica Sede";
				}
				else if (lotacao != null && lotacao.equals("Comunicação e Marketing"))
				{
					nomePapel = "Responsável Marketing";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Sistemas da Informaçao"))
				{
					nomePapel = "Coordenador de Sistemas";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Infraestrutura"))
				{
					nomePapel = "Supervisor de Infraestrutura de TI";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Monitoramento"))
				{
					nomePapel = "Justificar Afastamento CM";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Recursos Humanos"))
				{
					nomePapel = "Coordenador de RH";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Departamento Pessoal"))
				{
					nomePapel = "Coordenador de RH";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de RH Estratégico"))
				{
					nomePapel = "Gerente RH Estratégico";
				}

				else if (lotacao != null && lotacao.equals("Coordenadoria de RH RSA"))
				{
					nomePapel = "Gerente RH Estratégico";
				}

				break;
			case 1:
				if (lotacao != null && lotacao.startsWith("Agente de Inspeção Técnica") || lotacao.startsWith("Técnicos de Eletrônica") || lotacao.contains("Eletrônica"))
				{
					nomePapel = "Falta de Efetivo Segurança Eletrônica";
				}
				else
				{
					nomePapel = "Falta de Efetivo Segurança";
				}
				break;
			case 2:
				nomePapel = "Falta de Efetivo Escritorio Regional IAI";
				break;
			case 3:
				nomePapel = "Falta de Efetivo Escritorio Regional BQE";
				break;
			case 4:
				nomePapel = "Falta de Efetivo Escritorio Regional BNU";
				break;
			case 5:
				nomePapel = "Falta de Efetivo Escritorio Regional JLE";
				break;
			case 6:
				nomePapel = "Falta de Efetivo Escritorio Regional LGS";
				break;
			case 7:
				nomePapel = "Falta de Efetivo Escritorio Regional CUA";
				break;
			case 8:
				nomePapel = "Falta de Efetivo Escritorio Regional GPR";
				break;
			case 9:
				nomePapel = "Falta de Efetivo Asseio Conserv. e Limpeza";
				break;
			case 10:
				nomePapel = "Falta de Efetivo Escritorio Regional CCO";
				break;
			case 11:
				nomePapel = "Falta de Efetivo Escritorio Regional RSL";
				break;
			case 12:
				nomePapel = "Falta de Efetivo Escritorio Regional JGS";
				break;
			case 13:
				if (lotacao != null && lotacao.startsWith("ASE"))
				{
					nomePapel = "Falta de Efetivo Segurança Eletrônica CTA";
				}
				else
				{
					nomePapel = "Falta de Efetivo Escritorio Regional CTA";
				}
				break;
			case 14:
				nomePapel = "Falta de Efetivo Escritorio Regional CSC";
				break;
			case 15:
				nomePapel = "Falta de Efetivo Escritorio Regional CTA Sul";
				break;
			case 16:
				nomePapel = "Falta de Efetivo Escritorio Regional TRO";
				break;
			case 17:
				nomePapel = "FaltaEfetivoEscritorioRegionalNHO";
				break;
			case 19:
				nomePapel = "FaltaEfetivoEscritorioRegionalCAS";
				break;
			case 20:
				nomePapel = "FaltaEfetivoEscritorioRegionalGNA";
				break;
			case 22:
				nomePapel = "FaltaEfetivoEscritorioRegionalSRR";
				break;
		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		return obj;
	}

	public static NeoPaper getPapelJustaCausa(Long codreg, String lotacao)
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
		case 0:
		    if (lotacao != null && lotacao.equals("Coordenadoria NAC"))
		    {
			nomePapel = "Coordenador CEREC";
		    }
		    else if (lotacao != null && lotacao.equals("Assistente Indenização Contratual"))
		    {
			nomePapel = "Gerente Comercial Mercado Público"; // o correto seria samara.fontoura, mas como está afastada 
		    }
		    else if (lotacao != null && lotacao.equals("Auxiliar Administrativo - Jurídico"))
		    {
			nomePapel = "Assessor Jurídico";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria de Controladoria"))
		    {
			nomePapel = "Gerente Controladoria";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria Comercial Administrativa"))
		    {
			nomePapel = "Coordenador CEREC";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria Comercial Publico"))
		    {
			nomePapel = "Gerente Comercial Mercado Público";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria da Qualidade"))
		    {
			nomePapel = "Responsável Justificar Afastamento Qualidade";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria Financeira"))
		    {
			nomePapel = "Gerente Financeiro";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria de Compras"))
		    {
			nomePapel = "Gerente Administrativo";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria Administrativa"))
		    {
			nomePapel = "Gerente Administrativo";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria Almoxarifado de Eletrônica"))
		    {
			nomePapel = "Gerente Administrativo";
		    }
		    else if (lotacao != null && lotacao.equals("Comunicação e Marketing"))
		    {
			nomePapel = "Responsável Marketing";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria de Sistemas da Informaçao"))
		    {
			nomePapel = "Coordenador de Sistemas";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria de Infraestrutura"))
		    {
			nomePapel = "Supervisor de Infraestrutura de TI";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria de Monitoramento"))
		    {
			nomePapel = "justaCausaCentralMonitoramento";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria de Recursos Humanos"))
		    {
			nomePapel = "Gerente de RH";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria de Departamento Pessoal"))
		    {
			nomePapel = "Gerente de RH";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria de RH Estratégico"))
		    {
			nomePapel = "Gerente RH Estratégico";
		    }

		    else if (lotacao != null && lotacao.equals("Coordenadoria de RH RSA"))
		    {
			nomePapel = "Gerente RH Estratégico";
		    } 
		    
		    else if (lotacao != null && lotacao.equals("Coordenadoria Portaria Remota"))
		    { 		    
			nomePapel = "gerentePortariaRemota";		
		    }

		    break;

		case 1:
		    nomePapel = "Justa Causa Segurança";
		    break;
		case 2:
		    nomePapel = "Justa Causa Regional IAI";
		    break;
		case 3:
		    nomePapel = "Justa Causa Regional BQE";
		    break;
		case 4:
		    nomePapel = "Justa Causa Regional BNU";
		    break;
		case 5:
		    nomePapel = "Justa Causa Regional JLE";
		    break;
		case 6:
		    nomePapel = "Justa Causa Regional LGS";
		    break;
		case 7:
		    nomePapel = "Justa Causa Regional TRO";
		    break;
		case 8:
		    nomePapel = "Justa Causa Regional GPR";
		    break;
		case 9:
		    nomePapel = "Justa Causa Asseio Conserv. e Limpeza";
		    break;
		case 10:
		    nomePapel = "Justa Causa Escritorio Regional CCO";
		    break;
		case 11:
		    nomePapel = "Justa Causa Escritorio Regional RSL";
		    break;
		case 12:
		    nomePapel = "Justa Causa Escritorio Regional JGS";
		    break;
		case 13:
		    nomePapel = "Justa Causa Escritorio Regional CTA";
		    break;
		case 14:
		    nomePapel = "Justa Causa Escritorio Regional CSC";
		    break;
		case 15:
		    nomePapel = "Justa Causa Escritorio Regional CTA Sul";
		    break;
		case 16:
		    nomePapel = "Justa Causa Regional TRO";
		    break;
		case 17:
		    nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
		    break;
		case 18:
		    nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
		    break;
		case 19:
		    nomePapel = "GerenteEscritorioRegionalCampinas";
		    break;
		case 20:
		    nomePapel = "GerenteEscritorioRegionalGoiania";
		    break;
		case 21:
		    nomePapel = "GerenteEscritorioRegionalGoiania";
		    break;
		case 22:
		    nomePapel = "GerenteEscritorioRegionalSaoJoseDoRioPreto";
		    break;
		case 23:
		    nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
		    break;
		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		return obj;
	}

	public static NeoPaper getPapelAgendamentoReciclagem(Long codreg)
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 1:
				nomePapel = "Agendamento Reciclagem Segurança";
				break;
			case 2:
				nomePapel = "Agendamento Reciclagem Regional IAI";
				break;
			case 3:
				nomePapel = "Agendamento Reciclagem Regional BQE";
				break;
			case 4:
				nomePapel = "Agendamento Reciclagem Regional BNU";
				break;
			case 5:
				nomePapel = "Agendamento Reciclagem Regional JLE";
				break;
			case 6:
				nomePapel = "Agendamento Reciclagem Regional LGS";
				break;
			case 7:
				nomePapel = "Agendamento Reciclagem Regional CUA";
				break;
			case 8:
				nomePapel = "Agendamento Reciclagem Regional GPR";
				break;
			case 9:
				nomePapel = "Agendamento Reciclagem Asseio Conserv. e Limpeza";
				break;
			case 10:
				nomePapel = "Agendamento Reciclagem Escritorio Regional CCO";
				break;
			case 11:
				nomePapel = "Agendamento Reciclagem Escritorio Regional RSL";
				break;
			case 12:
				nomePapel = "Agendamento Reciclagem Escritorio Regional JGS";
				break;
			case 13:
				nomePapel = "Agendamento Reciclagem Escritorio Regional CTA";
				break;
			case 14:
				nomePapel = "Agendamento Reciclagem Escritorio Regional CSC";
				break;
			case 15:
				nomePapel = "Agendamento Reciclagem Escritorio Regional CTA Sul";
				break;
			case 16:
				nomePapel = "Agendamento Reciclagem Regional TRO";
				break;
		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		return obj;
	}

	public static String stringHexa(byte[] bytes)
	{
		StringBuilder s = new StringBuilder();
		for (int i = 0; i < bytes.length; i++)
		{
			int parteAlta = ((bytes[i] >> 4) & 0xf) << 4;
			int parteBaixa = bytes[i] & 0xf;
			if (parteAlta == 0)
			{
				s.append('0');
			}
			s.append(Integer.toHexString(parteAlta | parteBaixa));
		}
		return s.toString();
	}

	/**
	 * Metodo que pega a hora correta contando como horario de verao
	 * 
	 * @return Retorna a data e hora corretas com o horario de verao
	 * @version 1.0
	 * @author Lucas
	 */
	public GregorianCalendar HorarioVerao()
	{

		// Cria uma TIME ZONE correspondente ao horário de Brasília  
		SimpleTimeZone pdt = new SimpleTimeZone(-3 * 60 * 60 * 1000, "GMT-3:00");

		// Seta as regras para o horário de verão Brasileiro começando no primeiro domingo após o dia primeiro  
		pdt.setStartRule(Calendar.OCTOBER, 1, Calendar.SUNDAY, 0);

		// Terminando no último domingo do mês de Fevereiro  
		pdt.setEndRule(Calendar.FEBRUARY, -1, Calendar.SUNDAY, 0);

		// Instanciando um GregorianCalendar com com a timezone de BSB  
		GregorianCalendar dataHoje = new GregorianCalendar(pdt);

		return dataHoje;
	}

	/**
	 * Retorna Mapa contendo as siglas das regionais (exceto reginal 9, SOO) e seus respectivos códigos
	 * 
	 * @version 1.0
	 * @param Nenhum
	 * @return Map <String, Long> Siglas das regionais, Código das regionais
	 * @author Mateus
	 */
	public static Map<String, Long> getRegionais()
	{

		Map<String, Long> regionais = new HashMap<String, Long>();
		regionais.put("SOO", 1L);
		regionais.put("IAI", 2L);
		regionais.put("BQE", 3L);
		regionais.put("BNU", 4L);
		regionais.put("JLE", 5L);
		regionais.put("LGS", 6L);
		regionais.put("CUA", 7L);
		regionais.put("GPR", 8L);
		//regionais.put("SOO",9L);
		regionais.put("CCO", 10L);
		regionais.put("RSL", 11L);
		regionais.put("JGS", 12L);
		regionais.put("CTA", 13L);
		regionais.put("CSC", 14L);
		regionais.put("TRO", 16L);
		
		regionais.put("NHO", 17L);
		regionais.put("TRI", 18L);
		regionais.put("CAS", 19L);
		regionais.put("GNA", 20L);
		regionais.put("PMJ", 21L);
		regionais.put("SRR", 22L);
		regionais.put("XLN", 23L);

		return regionais;
	}

	/**
	 * Map contendo todas as siglas Regionais
	 * 
	 * @return Retorna codigo referente a regional
	 * @version 1.0
	 * @author Willliam
	 */
	public static Long getCodigoRegional(String sigla)
	{
		Map<String, Long> regionais = new HashMap<String, Long>();
		regionais.put("SOO", 1L);
		regionais.put("IAI", 2L);
		regionais.put("BQE", 3L);
		regionais.put("BNU", 4L);
		regionais.put("JLE", 5L);
		regionais.put("LGS", 6L);
		regionais.put("CUA", 7L);
		regionais.put("GPR", 8L);
		//regionais.put("SOO",9L);
		regionais.put("CCO", 10L);
		regionais.put("RSL", 11L);
		regionais.put("JGS", 12L);
		regionais.put("CTA", 13L);
		regionais.put("CSC", 14L);
		regionais.put("TRO", 16L);
		
		regionais.put("NHO", 17L);
		regionais.put("TRI", 18L);
		regionais.put("CAS", 19L);
		regionais.put("GNA", 20L);
		regionais.put("PMJ", 21L);
		regionais.put("SRR", 22L);
		regionais.put("XLN", 23L);

		return regionais.get(sigla);
	}

	// Retorna NeoPaper
	public static NeoPaper getPaper(String paper)
	{
		NeoPaper obj = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", paper));
		if (obj != null && obj.getAllUsers() != null && !obj.getAllUsers().isEmpty())
		{
			return obj;
		}
		else
		{
			throw new WorkflowException("O Papel não possui usuário!");
		}
	}

	// Retorna NeoGroup
	public static NeoGroup getGroup(String group)
	{
		NeoGroup obj = (NeoGroup) PersistEngine.getObject(NeoGroup.class, new QLEqualsFilter("code", group));
		if (obj != null && obj.getAllUsers() != null && !obj.getAllUsers().isEmpty())
		{
			return obj;
		}
		else
		{
			throw new WorkflowException("O Grupo não possui usuário!");
		}

	}
	
	// Retorna NeoUser codigo
        public static String getUserNeoPaper(NeoPaper papel) {
        	String executor = null;
        	if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty()) {
        	    for (NeoUser user : papel.getUsers()) {
        		executor = user.getCode();
        		break;
        	    }
        	}
        
        	return executor;
        }

	// Retorna NeoUser codigo
	public static String getUserNeoPaper(String paper)
	{
		NeoPaper papel = new NeoPaper();
		papel = getPaper(paper);
		String executor = null;
		if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
		{
			for (NeoUser user : papel.getUsers())
			{
				executor = user.getCode();
				break;
			}
		}

		return executor;
	}

	// Retorna NeoUser codigo
	public static String getUserNeoGroup(String group)
	{
		NeoGroup grupo = new NeoGroup();
		grupo = getGroup(group);
		String executor = null;
		if (grupo != null && grupo.getAllUsers() != null && !grupo.getAllUsers().isEmpty())
		{
			for (NeoUser user : grupo.getUsers())
			{
				executor = user.getCode();
				break;
			}
		}

		return executor;
	}

	public static String getLink(NeoFile file)
	{
		if (file != null)
		{
			StringBuffer html = new StringBuffer();
			String function = getOpenFunction(file.getNeoId());

			if (file.getSufix().toLowerCase().contains("pdf"))
			{
				html.append("<span style='cursor:pointer;' onclick=\"" + function + "\">");
				html.append("	<img src='imagens/icones_final/document_search_16x16-trans.png' align='absmiddle'> ");
				html.append("</span>");
			}
			else
			{
				html.append("<span style=\"cursor:pointer;\" onclick=\"window.location=' " + PortalUtil.getBaseURL() + "file/download/" + file.getNeoId() + "'; ");
				html.append("	return false;\"><img src=\"imagens/icones_final/document_search_16x16-trans.png\" align=\"absmiddle\" alt=" + file.getName() + "> ");
				html.append("</span>");
			}

			return html.toString();
		}
		else
		{
			return "";
		}
	}

	public static String getOpenFunction(Long fileNeoId)
	{
		StringBuffer html = new StringBuffer();

		html.append(" mask = callSync('" + PortalUtil.getBaseURL() + "servlet/com.neomind.fusion.custom.orsegups.ged.servlet.ShowAsPdfServlet?neoId=" + fileNeoId + "');");
		html.append(" if(mask != null && mask != '')");
		html.append(" {");
		html.append(" 	  window.open('" + PortalUtil.getBaseURL() + "ged/pdfViewer.jsp?code='+mask);");
		html.append("     return false;");
		html.append(" }");

		return html.toString();
	}

	public static List<String> listaRegionaisSigla()
	{
		List<String> regionais = new ArrayList<String>();
		regionais.add("IAI");
		regionais.add("BQE");
		regionais.add("BNU");
		regionais.add("JLE");
		regionais.add("LGS");
		regionais.add("CUA");
		regionais.add("GPR");
		regionais.add("SOO");
		regionais.add("CCO");
		regionais.add("RSL");
		regionais.add("JGS");
		regionais.add("CTA");
		regionais.add("TRO");
		return regionais;
	}

	private static void closeConnection(Connection conn)
	{
		try
		{
			if (NeoUtils.safeIsNotNull(conn))
				conn.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private static void closeConnection(Connection conn, PreparedStatement pstm)
	{
		try
		{
			if (NeoUtils.safeIsNotNull(pstm))
				pstm.close();
			closeConnection(conn);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private static void closeConnection(Connection conn, Statement stmt)
	{
		try
		{
			if (stmt != null)
			{
				stmt.close();
			}
			closeConnection(conn);
		}
		catch (Exception e)
		{

		}
	}

	public static void closeConnection(Connection conn, Statement stmt, ResultSet rs)
	{
		try
		{
			if (rs != null)
			{
				rs.close();
			}
			closeConnection(conn, stmt);
		}
		catch (Exception e)
		{

		}
	}

	public static void closeConnection(Connection conn, PreparedStatement pstm, ResultSet rs)
	{
		try
		{
			if (NeoUtils.safeIsNotNull(rs))
				rs.close();
			closeConnection(conn, pstm);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static String getDuration(Long _currentTimeMiles)
	{
		int x = 0;
		int seconds = 0;
		int minutes = 0;
		int hours = 0;
		int days = 0;
		int month = 0;
		int year = 0;

		try
		{
			x = (int) (_currentTimeMiles / 1000);
			seconds = x;
			if (seconds > 59)
			{
				minutes = seconds / 60;
				if (minutes > 59)
				{
					hours = minutes / 60;
					if (hours > 23)
					{
						days = hours / 24;
						if (days > 30)
						{
							month = days / 30;
							if (month > 11)
							{
								year = month / 12;
								return year + "A " + month % 12 + "M " + days % 30 + "D " + hours % 24 + "h " + minutes % 60 + "m " + seconds % 60 + "s";
							}
							else
							{
								return month % 12 + "M " + days % 30 + "D " + hours % 24 + "h " + minutes % 60 + "m " + seconds % 60 + "s";
							}
						}
						else
						{
							return days % 30 + "D " + hours % 24 + "h " + minutes % 60 + "m " + seconds % 60 + "s";
						}
					}
					else
					{
						return hours % 24 + "H " + minutes % 60 + "m " + seconds % 60 + "s";
					}
				}
				else
				{
					return minutes % 60 + "m " + seconds % 60 + "s";
				}
			}
			else
			{
				return seconds % 60 + "s";
			}
		}
		catch (Exception e)
		{

		}
		return "";
	}

	public static String crypto(boolean dir, String sPassword, String sText) throws Exception
	{
		String authToken = "";
		byte[] c = null;
		byte[] password = sPassword.getBytes();
		byte[] text = sText.getBytes();

		try
		{
			DESKeySpec keySpec = new DESKeySpec(password);
			SecretKeyFactory factory = SecretKeyFactory.getInstance("DES");
			Cipher cipher = Cipher.getInstance("DES");
			cipher.init(dir ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE, factory.generateSecret(keySpec));

			c = cipher.doFinal(text);

			authToken = new Base64().encode(c);
			authToken = authToken = URLEncoder.encode(authToken, "ISO-8859-1");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return authToken;
	}

	@Deprecated
	public static void sendEmail2SendGrid(String addTo, String from, String subject, String text, String html, String nameFile, File file)
	{
		SendGrid sendgrid = new SendGrid(SENDGRID_USERNAME, SENDGRID_PASSWORD);
		try
		{

			SendGrid.Email email = new SendGrid.Email();
			email.addTo(addTo);
			email.setFrom(from);
			email.setSubject(subject);
			email.setText(text);
			email.setHtml(html);

			if (nameFile != null && file != null)
			{
				email.addAttachment(nameFile, file);
			}
			SendGrid.Response response = sendgrid.send(email);
			System.out.println("SendGrid: " + response.getStatus());
			System.out.println("SendGrid: " + response.getMessage());
		}
		catch (SendGridException e)
		{
			e.printStackTrace();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public static void sendEmail2Orsegups(String addTo, String from, String subject, String text, String html)
	{
		HtmlEmail email = new HtmlEmail();
		email.setHostName("SSOOMX01");
		email.setSmtpPort(25);
		/*
		 * email.setHostName("192.168.20.15");
		 * email.setSmtpPort(587);
		 */
		try
		{
			email.addTo(addTo);
			email.setFrom(from);
			email.setSubject(subject);
			email.setHtmlMsg(html);
			email.send();
		}
		catch (EmailException e)
		{
			e.printStackTrace();
		}
	}

	public static NeoPaper getPapelJustificarR022(Long codreg, String lotacao)
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 0:
				if (lotacao != null && lotacao.equals("Coordenadoria NAC"))
				{
					nomePapel = "Coordenador CEREC";
				}
				else if (lotacao != null && lotacao.equals("Assistente Indenização Contratual"))
				{
					nomePapel = "Gerente Comercial Mercado Público"; // o correto seria samara.fontoura, mas como está afastada 
				}
				else if (lotacao != null && lotacao.equals("Auxiliar Administrativo - Jurídico"))
				{
					nomePapel = "Assessor Jurídico";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Controladoria"))
				{
					nomePapel = "Gerente Controladoria";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Comercial Administrativa"))
				{
					nomePapel = "Coordenador CEREC";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Comercial Publico"))
				{
					nomePapel = "Gerente Comercial Mercado Público";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria da Qualidade"))
				{
					nomePapel = "Responsável Justificar Afastamento Qualidade";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Financeira"))
				{
					nomePapel = "Gerente Financeiro";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Compras"))
				{
					nomePapel = "Gerente Administrativo";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Administrativa"))
				{
					nomePapel = "Gerente Administrativo";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Almoxarifado de Eletrônica"))
				{
					nomePapel = "Gerente Administrativo";
				}
				else if (lotacao != null && lotacao.equals("Comunicação e Marketing"))
				{
					nomePapel = "Responsável Marketing";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Sistemas da Informaçao"))
				{
					nomePapel = "Coordenador de Sistemas";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Infraestrutura"))
				{
					nomePapel = "Supervisor de Infraestrutura de TI";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Monitoramento"))
				{
					nomePapel = "Gerente de Monitoramento";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Recursos Humanos"))
				{
					nomePapel = "Gerente de RH";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Departamento Pessoal"))
				{
					nomePapel = "Gerente de RH";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de RH Estratégico"))
				{
					nomePapel = "Gerente RH Estratégico";
				}

				else if (lotacao != null && lotacao.equals("Coordenadoria de RH RSA"))
				{
					nomePapel = "Gerente RH Estratégico";
				}

				break;

			case 1:
				if (lotacao != null && lotacao.startsWith("Técnico de Eletrônica") || lotacao.startsWith("Supervisor Técnico de Eletrônica") || lotacao.startsWith("Técnicos de Eletrônica") || lotacao.startsWith("Agente de Inspeção Técnica") || lotacao != null && lotacao.startsWith("Auxiliar Técnico de Eletrônica") || lotacao != null && lotacao.startsWith("Cobertura de Férias - Segurança Eletrônica") || lotacao != null && lotacao.startsWith("Auxiliar Administrativo Eletrônica")
						|| lotacao != null && lotacao.startsWith("Supervisor de Segurança Eletrônica") || lotacao != null && lotacao.startsWith("Aviso Prévio - Segurança Eletrônica"))
				{
					nomePapel = "Justificar Afastamento Vigilância Eletrônica";
				}
				else
				{
					nomePapel = "Justificar Afastamento Segurança";
				}
				break;
			case 2:
				nomePapel = "Gerente Escritorio Regional Balneário Camboriu";
				break;
			case 3:
				nomePapel = "Gerente Escritorio Regional Brusque";
				break;
			case 4:
				nomePapel = "Gerente Escritorio Regional Blumenal";
				break;
			case 5:
				nomePapel = "Gerente Escritorio Regional Joinville";
				break;
			case 6:
				nomePapel = "Gerente Escritorio Regional Lages";
				break;
			case 7:
				nomePapel = "Gerente Escritório Regional Tubarão";
				break;
			case 8:
				nomePapel = "Gerente Escritorio Regional Gaspar";
				break;
			case 9:
				nomePapel = "Gerente de Asseio Conserv. e Limpeza";
				break;
			case 10:
				nomePapel = "Gerente Escritorio Regional Chapeco";
				break;
			case 11:
				nomePapel = "Gerente Escritório Regional Rio do Sul";
				break;
			case 12:
				nomePapel = "Gerente Escritório Regional Jaraguá do Sul";
				break;
			case 13:
				//Técnicos de eletrônica CTA
				if (lotacao.startsWith("Técnico") && lotacao.contains("Eletrônica") && lotacao.endsWith("CTA"))
				{
					nomePapel = "Gerente de Segurança Eletrônica CTA";
				}
				//Táticos CTA
				else if (lotacao.startsWith("ASE") && lotacao.contains("ROTA") && lotacao.endsWith("CTA"))
				{
					nomePapel = "Gerente de Segurança Eletrônica CTA";
				}
				else
				//Outros
				{
					nomePapel = "Gerente Escritório Regional Curitiba Sul";
				}
				break;
			case 14:
				nomePapel = "Gerente Escritório Regional Curitiba";
				break;
			case 15:
				nomePapel = "Gerente Escritório Regional Curitiba";
				break;
			case 16:
				nomePapel = "Gerente Escritório Regional Tubarão";
				break;
			case 17:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
			case 18:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
			case 19:
				nomePapel = "GerenteEscritorioRegionalCampinas";
				break;
			case 20:
				nomePapel = "GerenteEscritorioRegionalGoiania";
				break;
			case 21:
				nomePapel = "GerenteEscritorioRegionalGoiania";
				break;
			case 22:
				nomePapel = "GerenteEscritorioRegionalSaoJoseDoRioPreto";
				break;
			case 23:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
			case 24:
				nomePapel = "FCCGerenteEscritorioRegionalPMJ";
				break;

		}

		if (nomePapel.equals(""))
		{ // vai para o juliano tratar
			nomePapel = "Responsável Justificar Afastamento Qualidade";
		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		return obj;
	}

	public static NeoPaper getPapelRetornaEquipamento(Long codreg)
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 1:
				nomePapel = "Responsável Devolução dos Equipamentos para Estoque SOO";
				break;
			case 2:
				nomePapel = "Responsável Devolução dos Equipamentos para Estoque IAI";
				break;
			case 3:
				nomePapel = "Responsável Devolução dos Equipamentos para Estoque BQE";
				break;
			case 4:
				nomePapel = "Responsável Devolução dos Equipamentos para Estoque BNU";
				break;
			case 5:
				nomePapel = "Responsável Devolução dos Equipamentos para Estoque JLE";
				break;
			case 6:
				nomePapel = "Responsável Devolução dos Equipamentos para Estoque LGS";
				break;
			case 7:
				nomePapel = "Responsável Devolução dos Equipamentos para Estoque CUA";
				break;
			case 8:
				nomePapel = "Responsável Devolução dos Equipamentos para Estoque Gaspar";
				break;
			case 9:
				nomePapel = "Responsável Devolução dos Equipamentos para Estoque ACL";
				break;
			case 10:
				nomePapel = "Responsável Devolução dos Equipamentos para Estoque CCO";
				break;
			case 11:
				nomePapel = "Responsável Devolução dos Equipamentos para Estoque RSL";
				break;
			case 12:
				nomePapel = "Responsável Devolução dos Equipamentos para Estoque JGS";
				break;
			case 13:
				nomePapel = "Responsável Devolução dos Equipamentos para Estoque CTA";
				break;
			case 14:
				nomePapel = "Responsável Devolução dos Equipamentos para Estoque CSC";
				break;
			case 15:
				nomePapel = "Responsável Devolução dos Equipamentos para Estoque CTA Sul";
				break;
			case 16:
				nomePapel = "Responsável Devolução dos Equipamentos para Estoque TRO";
				break;
			case 17:
				nomePapel = "responsavelDevolucaodosEquipamentosparaEstoqueNHO";
				break;
			case 18:
				nomePapel = "responsavelDevolucaodosEquipamentosparaEstoqueTRI";
				break;
			case 19:
				nomePapel = "responsavelDevolucaodosEquipamentosparaEstoqueCAS";
				break;
			case 20:
				nomePapel = "responsavelDevolucaodosEquipamentosparaEstoqueGNA";
				break;
			case 21:
				nomePapel = "responsavelDevolucaodosEquipamentosparaEstoquePMJ";
				break;
			case 22:
				nomePapel = "responsavelDevolucaodosEquipamentosparaEstoqueSRR";
				break;
			case 23:
				nomePapel = "responsavelDevolucaodosEquipamentosparaEstoqueXLN";
				break;
				
				
		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		return obj;
	}

	public static NeoPaper getPapelGerenteRegional(Long codreg, String lotacao) throws WorkflowException
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 0:
				if (lotacao != null && lotacao.equals("Coordenadoria NAC"))
				{
					nomePapel = "Diretor de Tecnologia da Informação";
				}
				
				else if (lotacao != null && lotacao.equals("Coordenadoria de Suprimentos"))
				{
					nomePapel = "Coordenador de Suprimentos";
				}
				
				else if (lotacao != null && lotacao.equals("Gerência de Rastreamento e Gestão de Frota"))
				{
					nomePapel = "Gerente Rastreamento e Gestão Frota";
				}
				
				else if (lotacao != null && lotacao.equals("Assistente Indenização Contratual"))
				{
					nomePapel = "Diretor de Planejamento";
				}
				else if (lotacao != null && lotacao.equals("Auxiliar Administrativo - Jurídico"))
				{
					nomePapel = "Assessor Jurídico";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Controladoria"))
				{
					nomePapel = "Gerente Controladoria";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Comercial Administrativa"))
				{
					nomePapel = "Diretor de Tecnologia da Informação";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Comercial Publico"))
				{
					nomePapel = "Gerente Comercial Mercado Público";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria da Qualidade"))
				{
					nomePapel = "Diretor de Planejamento";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Financeira"))
				{
					nomePapel = "Gerente Financeiro";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Compras"))
				{
					nomePapel = "Gerente Administrativo";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Administrativa"))
				{
					nomePapel = "Gerente Administrativo";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Almoxarifado de Eletrônica"))
				{
					nomePapel = "Gerente Administrativo";
				}
				else if (lotacao != null && lotacao.equals("Comunicação e Marketing"))
				{
					nomePapel = "Gerente RH Estratégico";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Sistemas da Informaçao"))
				{
					nomePapel = "Diretor de Tecnologia da Informação";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Infraestrutura"))
				{
					nomePapel = "Diretor de Tecnologia da Informação";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Monitoramento"))
				{
					nomePapel = "Gerente de Monitoramento";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Recursos Humanos"))
				{
					nomePapel = "Gerente de RH";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Departamento Pessoal"))
				{
					nomePapel = "Gerente de RH";
				}				
				else if (lotacao != null && lotacao.equals("Coordenadoria de RH Estratégico"))
				{
					nomePapel = "Gerente RH Estratégico";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de RH RSA"))
				{
					nomePapel = "Gerente RH Estratégico";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria NAC Externo"))
				{
				    	nomePapel = "Gerente CEREC";
				}
				else if (lotacao != null && lotacao.equals("Recuperação de Crédito"))
				{
				    	nomePapel = "Gerente de Recuperação de Crédito";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria NAC Interno"))
				{
				    	nomePapel = "Gerente NAC Interno";
				}
				else if (lotacao != null && lotacao.equals("4B2G/NEXTI"))
				{
				    	nomePapel = "DiretorDeNegociosNexti";
				}
				break;
			case 1:
				if (lotacao != null && lotacao.startsWith("Agente de Inspeção Técnica") || lotacao.startsWith("Técnicos de Eletrônica")
					|| lotacao.contains("Dep. Eletrônica") || lotacao.contains("Segurança Eletrônica") || lotacao.contains("Administrativo Eletrônica")
					|| lotacao.contains("Supervisor Técnico de Eletrônica") || lotacao.contains("Técnico de Eletrônica")
					|| lotacao.contains("Supervisor de Segurança Eletrônica"))
				{
					nomePapel = "Gerente de Vigilância Eletrônica";
				}
				else
				{
					nomePapel = "Gerente de Segurança";
				}
				break;
			case 2:
				nomePapel = "Gerente Escritorio Regional Balneário Camboriu";
				break;
			case 3:
				nomePapel = "Gerente Escritorio Regional Brusque";
				break;
			case 4:
				nomePapel = "Gerente Escritorio Regional Blumenal";
				break;
			case 5:
				nomePapel = "Gerente Escritorio Regional Joinville";
				break;
			case 6:
				nomePapel = "Gerente Escritorio Regional Lages";
				break;
			case 7:
				nomePapel = "Gerente Escritório Regional Tubarão";
				break;
			case 8:
				nomePapel = "Gerente Escritorio Regional Gaspar";
				break;
			case 9:
				nomePapel = "Gerente de Asseio Conserv. e Limpeza";
				break;
			case 10:
				nomePapel = "Gerente Escritorio Regional Chapeco";
				break;
			case 11:
				nomePapel = "Gerente Escritório Regional Rio do Sul";
				break;
			case 12:
				nomePapel = "Gerente Escritório Regional Jaraguá do Sul";
				break;
			case 13:
				/*if (lotacao != null && lotacao.startsWith("ASE"))
				{
					nomePapel = "Gerente de Segurança Eletrônica CTA";
				}
				else
				{
					nomePapel = "Gerente Escritório Regional Curitiba";
				}
				*/
				//Técnicos de eletrônica CTA
				if (lotacao.startsWith("Técnico") && lotacao.contains("Eletrônica") && lotacao.endsWith("CTA"))
				{
					nomePapel = "Gerente de Segurança Eletrônica CTA";
				}
				//Táticos CTA
				else if (lotacao.startsWith("ASE") && lotacao.contains("ROTA") && lotacao.endsWith("CTA"))
				{
					nomePapel = "Gerente de Segurança Eletrônica CTA";
				}
				else
				//Outros
				{
					nomePapel = "Gerente Escritório Regional Curitiba Sul";
				}
				break;
			case 14:
				nomePapel = "Gerente Escritorio Regional Cascavel";
				break;
			case 15:
				nomePapel = "Gerente Escritório Regional Curitiba Sul";
				break;
			case 16:
				nomePapel = "Gerente Escritório Regional Tubarão";
				break;
			case 17:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
			case 18:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
			case 19:
				nomePapel = "GerenteEscritorioRegionalCampinas";
				break;
			case 20:
				nomePapel = "GerenteEscritorioRegionalGoiania";
				break;
			case 21:
				nomePapel = "GerenteEscritorioRegionalGoiania";
				break;
			case 22:
				nomePapel = "GerenteEscritorioRegionalSaoJoseDoRioPreto";
				break;
			case 23:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
				
			default:
				nomePapel = "AtualizarAcessosSistemas";
				break;

		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		if (obj != null && obj.getAllUsers() != null && !obj.getAllUsers().isEmpty())
		{
			return obj;
		}
		else
		{
			throw new WorkflowException("Gerente responsável não localizado!");
		}
	}

	public static NeoPaper getPapelTOP10Marcacoes(Long codreg, String lotacao)
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 0:
				if (lotacao != null && lotacao.equals("Coordenadoria NAC"))
				{
					nomePapel = "Coordenador CEREC";
				}
				else if (lotacao != null && lotacao.equals("Assistente Indenização Contratual"))
				{
					nomePapel = "Gerente Comercial Mercado Público"; // o correto seria samara.fontoura, mas como está afastada 
				}
				else if (lotacao != null && lotacao.equals("Auxiliar Administrativo - Jurídico"))
				{
					nomePapel = "Assessor Jurídico";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Controladoria"))
				{
					nomePapel = "Gerente Controladoria";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Comercial Administrativa"))
				{
					nomePapel = "Coordenador CEREC";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Comercial Publico"))
				{
					nomePapel = "Gerente Comercial Mercado Público";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria da Qualidade"))
				{
					nomePapel = "Responsável Justificar Afastamento Qualidade";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Financeira"))
				{
					nomePapel = "Gerente Financeiro";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Compras"))
				{
					nomePapel = "Gerente Administrativo";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Administrativa"))
				{
					nomePapel = "Gerente Administrativo";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Almoxarifado de Eletrônica"))
				{
					nomePapel = "Gerente Administrativo";
				}
				else if (lotacao != null && lotacao.equals("Comunicação e Marketing"))
				{
					nomePapel = "Responsável Marketing";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Sistemas da Informaçao"))
				{
					nomePapel = "Coordenador de Sistemas";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Infraestrutura"))
				{
					nomePapel = "Supervisor de Infraestrutura de TI";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Monitoramento"))
				{
					nomePapel = "Gerente de Monitoramento";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Recursos Humanos"))
				{
					nomePapel = "Gerente de RH";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Departamento Pessoal"))
				{
					nomePapel = "Gerente de RH";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de RH Estratégico"))
				{
					nomePapel = "Gerente RH Estratégico";
				}

				else if (lotacao != null && lotacao.equals("Coordenadoria de RH RSA"))
				{
					nomePapel = "Gerente RH Estratégico";
				}

				break;

			case 1:
				if (lotacao != null && lotacao.startsWith("Técnico de Eletrônica") || lotacao.startsWith("Supervisor Técnico de Eletrônica") || lotacao.startsWith("Técnicos de Eletrônica") || lotacao.startsWith("Agente de Inspeção Técnica") || lotacao != null && lotacao.startsWith("Auxiliar Técnico de Eletrônica") || lotacao != null && lotacao.startsWith("Cobertura de Férias - Segurança Eletrônica") || lotacao != null && lotacao.startsWith("Auxiliar Administrativo Eletrônica")
						|| lotacao != null && lotacao.startsWith("Supervisor de Segurança Eletrônica") || lotacao != null && lotacao.startsWith("Aviso Prévio - Segurança Eletrônica"))
				{
					nomePapel = "Gerente de Vigilância Eletrônica";
				}
				else
				{
					nomePapel = "Coordenador de Segurança";
				}
				break;
			case 2:
				nomePapel = "Supervisor Operacional Senior";
				break;
			case 3:
				nomePapel = "Coordenador Escritorio Regional BQE";
				break;
			case 4:
				nomePapel = "Coordenador Escritorio Regional BNU";
				break;
			case 5:
				nomePapel = "Coordenador Escritorio Regional JLE";
				break;
			case 6:
				nomePapel = "Coordenador Escritorio Regional LGS";
				break;
			case 7:
				nomePapel = "Coordenador Operacional CUA";
				break;
			case 8:
				nomePapel = "Gerente Escritorio Regional Gaspar";
				break;
			case 9:
				nomePapel = "responsaveltop10marcacoesasseio";
				break;
			case 10:
				nomePapel = "Justificar Afastamento Escritorio Regional CCO";
				break;
			case 11:
				nomePapel = "Coordenador Escritório Regional RSL";
				break;
			case 12:
				nomePapel = "Coordenador Escritório Regional JGS";
				break;
			case 13:
				//Técnicos de eletrônica CTA
				if (lotacao.startsWith("Técnico") && lotacao.contains("Eletrônica") && lotacao.endsWith("CTA"))
				{
					nomePapel = "Justificar Afastamento Escritorio Regional CTA";
				}
				//Táticos CTA
				else if (lotacao.startsWith("ASE") && lotacao.contains("ROTA") && lotacao.endsWith("CTA"))
				{
					nomePapel = "Justificar Afastamento Escritorio Regional CTA";
				}
				else
				//Outros
				{
					nomePapel = "Gerente Escritório Regional Curitiba Sul";
				}
				break;
			case 14:
				nomePapel = "Responsável Justificativa Equipamento CSC";
				break;
			case 15:
				nomePapel = "Justificar Afastamento Escritorio Regional CTA Sul";
				break;
			case 16:
				nomePapel = "Coordenador Operacional TRO";
				break;
			case 17:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
			case 18:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
			case 19:
				nomePapel = "GerenteEscritorioRegionalCampinas";
				break;
			case 20:
				nomePapel = "GerenteEscritorioRegionalGoiania";
				break;
			case 21:
				nomePapel = "GerenteEscritorioRegionalGoiania";
				break;
			case 22:
				nomePapel = "GerenteEscritorioRegionalSaoJoseDoRioPreto";
				break;
			case 23:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;

		}

		if (nomePapel.equals(""))
		{ // vai para o juliano tratar
			nomePapel = "Responsável Justificar Afastamento Qualidade";
		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		return obj;
	}

	public static void sendEmail2Orsegups(String addTo, String from, String subject, String text, String html, String replyto, String comCopia, String copiaOculta)
	{
		HtmlEmail email = new HtmlEmail();
		email.setHostName("SSOOMX01");
		email.setSmtpPort(25);
		/*
		 * email.setHostName("192.168.20.15");
		 * email.setSmtpPort(587);
		 */
		try
		{
			email.addTo(addTo);
			if (replyto != null)
			{
				email.addReplyTo(replyto);
			}
			if (comCopia != null)
			{
				/* Envia em cópia */
				email.addCc(comCopia);
			}
			if (copiaOculta != null)
			{
				/* Envia em cópia oculta */
				email.addBcc(copiaOculta);
			}
			email.setFrom(from);
			email.setSubject(subject);
			email.setHtmlMsg(html);
			email.send();
		}
		catch (EmailException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Envio de emails, não trata valores nulos ou vazios!
	 * 
	 * @param addTo array simples de Strings
	 * @param from String
	 * @param subject String
	 * @param html String
	 * @param comCopia array simples de Strings
	 * @author mateus.batista
	 * @since 29/04/2016
	 */
	public static void sendEmail2Orsegups(String[] addTo, String from, String subject, String html, String[] comCopia)
	{

		HtmlEmail email = new HtmlEmail();
		email.setHostName("SSOOMX01");
		email.setSmtpPort(25);

		try
		{

			for (String to : addTo)
			{
				email.addTo(to);
			}

			for (String cc : comCopia)
			{
				email.addCc(cc);
			}

			email.setFrom(from);
			email.setSubject(subject);
			email.setHtmlMsg(html);
			email.send();

		}
		catch (EmailException e)
		{
			e.printStackTrace();
		}
	}

	public static void sendEmail2Orsegups(String addTo, String from, String subject, String text, String html, String replyto, List<String> comCopia, List<String> copiaOculta)
	{
		HtmlEmail email = new HtmlEmail();
		email.setHostName("SSOOMX01");
		email.setSmtpPort(25);
		try
		{
			email.addTo(addTo);
			if (replyto != null)
			{
				email.addReplyTo(replyto);
			}
			if (comCopia != null && !comCopia.isEmpty())
			{
				for (String comCopiaMail : comCopia)
				{
					/* Envia em cópia */
					email.addCc(comCopiaMail);
					System.out.println("");
				}
			}
			if (copiaOculta != null && !copiaOculta.isEmpty())
			{
				for (String copiaOcultaMail : copiaOculta)
				{
					/* Envia em cópia oculta */
					email.addBcc(copiaOcultaMail);
				}
			}
			email.setFrom(from);
			email.setSubject(subject);
			email.setHtmlMsg(html);
			email.send();
		}
		catch (EmailException e)
		{
			e.printStackTrace();
		}
	}

	public static NeoPaper getPapelDesligamentoSemJustaCausa(Long codreg, String lotacao) throws WorkflowException
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
		case 0:
		    if (lotacao != null && lotacao.equals("Coordenadoria NAC"))
		    {
			nomePapel = "Coordenador CEREC";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria NAC Externo"))
		    {
			nomePapel = "CoordenadorNACExterno";
		    }
		    else if (lotacao != null && lotacao.equals("Assistente Indenização Contratual"))
		    {
			nomePapel = "Gerente Comercial Mercado Público"; // o correto seria samara.fontoura, mas como está afastada 
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria Indenização Contratual"))
		    {
			nomePapel = "Supervisor CEREC"; 
		    }
		    else if (lotacao != null && lotacao.equals("Auxiliar Administrativo - Jurídico"))
		    {
			nomePapel = "Assessor Jurídico";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria de Controladoria"))
		    {
			nomePapel = "Gerente Controladoria";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria Comercial Administrativa"))
		    {
			nomePapel = "Coordenador CEREC";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria Comercial Publico"))
		    {
			nomePapel = "Gerente Comercial Mercado Público";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria da Qualidade"))
		    {
			nomePapel = "Responsável Justificar Afastamento Qualidade";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria Financeira"))
		    {
			nomePapel = "Gerente Financeiro";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria de Compras"))
		    {
			nomePapel = "Gerente Administrativo";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria Administrativa"))
		    {
			nomePapel = "Gerente Administrativo";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria Almoxarifado de Eletrônica"))
		    {
			nomePapel = "Gerente Administrativo";
		    }
		    else if (lotacao != null && lotacao.equals("Comunicação e Marketing"))
		    {
			nomePapel = "Responsável Marketing";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria de Sistemas da Informaçao"))
		    {
			nomePapel = "Coordenador de Sistemas";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria de Infraestrutura"))
		    {
			nomePapel = "Supervisor de Infraestrutura de TI";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria de Monitoramento"))
		    {
			nomePapel = "semJustaCausaCentralMonitoramento2";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria de Recursos Humanos"))
		    {
			nomePapel = "Gerente de RH";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria de Departamento Pessoal"))
		    {
			nomePapel = "Gerente de RH";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria de RH Estratégico"))
		    {
			nomePapel = "Responsável Marketing";
		    }

		    else if (lotacao != null && lotacao.equals("Coordenadoria de RH RSA"))
		    {
			nomePapel = "Responsável Marketing";
		    }
		    else if (lotacao != null && lotacao.equals("Gerente de Portaria Remota"))
		    {
			nomePapel = "GerentePortariaRemota";
		    }
		    else if (lotacao != null && lotacao.equals("Gerente Comercial Publico"))
		    {
			nomePapel = "Supervisor empresa cobrança";
		    }
		    else if (lotacao != null && lotacao.equals("WINKER/REDE DOMUS"))
		    {
			nomePapel = "gerenteWinker";
		    }

		    break;

		    //			case 0:
		    //				nomePapel = "Gerente Comercial Mercado Privado";
		    //				break;
		case 1:
		    nomePapel = "Gerente de Segurança";
		    break;
		case 2:
		    nomePapel = "Gerente Escritorio Regional Balneário Camboriu";
		    break;
		case 3:
		    nomePapel = "Gerente Escritorio Regional Brusque";
		    break;
		case 4:
		    nomePapel = "Gerente Escritorio Regional Blumenal";
		    break;
		case 5:
		    nomePapel = "Gerente Escritorio Regional Joinville";
		    break;
		case 6:
		    nomePapel = "Gerente Escritorio Regional Lages";
		    break;
		case 7:
		    nomePapel = "Gerente Escritório Regional Tubarão";
		    break;
		case 8:
		    nomePapel = "Gerente Escritorio Regional Gaspar";
		    break;
		case 9:
		    nomePapel = "Gerente de Asseio Conserv. e Limpeza";
		    break;
		case 10:
		    nomePapel = "Gerente Escritorio Regional Chapeco";
		    break;
		case 11:
		    nomePapel = "Gerente Escritório Regional Rio do Sul";
		    break;
		case 12:
		    nomePapel = "Gerente Escritório Regional Jaraguá do Sul";
		    break;
		case 13:
		    nomePapel = "Coordenador Escritório Regional CTA";
		    break;
		case 14:
		    nomePapel = "Gerente Escritorio Regional Cascavel";
		    break;
		case 15:
		    nomePapel = "Coordenador Escritório Regional CTA Sul";
		    break;
		case 16:
		    nomePapel = "Gerente Escritório Regional Tubarão";
		    break;
		case 17:
		    nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
		    break;
		case 18:
		    nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
		    break;
		case 19:
		    nomePapel = "GerenteEscritorioRegionalCampinas";
		    break;
		case 20:
		    nomePapel = "GerenteEscritorioRegionalGoiania";
		    break;
		case 21:
		    nomePapel = "GerenteEscritorioRegionalGoiania";
		    break;
		case 22:
		    nomePapel = "GerenteEscritorioRegionalSaoJoseDoRioPreto";
		    break;
		case 23:
		    nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
		    break;

		}
		
		if (nomePapel.equals("")) {
		    nomePapel = "semJustaCausaCentralMonitoramento";
		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		if (obj != null && obj.getAllUsers() != null && !obj.getAllUsers().isEmpty())
		{
			return obj;
		}
		else
		{
			throw new WorkflowException("Gerente responsável não localizado!");
		}
	}
	
	public static NeoPaper getPapelResponsavelIntraInterJornada(Long codreg, String lotacao) throws WorkflowException
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
		case 0:
		    if (lotacao != null && lotacao.equals("Coordenadoria NAC"))
		    {
			nomePapel = "Coordenador CEREC";
		    }
		    else if (lotacao != null && lotacao.equals("Assistente Indenização Contratual"))
		    {
			nomePapel = "Gerente Comercial Mercado Público"; // o correto seria samara.fontoura, mas como está afastada 
		    }
		    else if (lotacao != null && lotacao.equals("Auxiliar Administrativo - Jurídico"))
		    {
			nomePapel = "Assessor Jurídico";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria de Controladoria"))
		    {
			nomePapel = "Gerente Controladoria";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria Comercial Administrativa"))
		    {
			nomePapel = "Coordenador CEREC";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria Comercial Publico"))
		    {
			nomePapel = "Gerente Comercial Mercado Público";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria da Qualidade"))
		    {
			nomePapel = "Responsável Justificar Afastamento Qualidade";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria Financeira"))
		    {
			nomePapel = "Gerente Financeiro";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria de Compras"))
		    {
			nomePapel = "Gerente Administrativo";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria Administrativa"))
		    {
			nomePapel = "Gerente Administrativo";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria Almoxarifado de Eletrônica"))
		    {
			nomePapel = "Gerente Administrativo";
		    }
		    else if (lotacao != null && lotacao.equals("Comunicação e Marketing"))
		    {
			nomePapel = "Responsável Marketing";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria de Sistemas da Informaçao"))
		    {
			nomePapel = "Coordenador de Sistemas";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria de Infraestrutura"))
		    {
			nomePapel = "Supervisor de Infraestrutura de TI";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria de Monitoramento"))
		    {
			nomePapel = "semJustaCausaCentralMonitoramento";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria de Recursos Humanos"))
		    {
			nomePapel = "Gerente de RH";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria de Departamento Pessoal"))
		    {
			nomePapel = "Gerente de RH";
		    }
		    else if (lotacao != null && lotacao.equals("Coordenadoria de RH Estratégico"))
		    {
			nomePapel = "Responsável Marketing";
		    }

		    else if (lotacao != null && lotacao.equals("Coordenadoria de RH RSA"))
		    {
			nomePapel = "Responsável Marketing";
		    }

		    break;

		    //			case 0:
		    //				nomePapel = "Gerente Comercial Mercado Privado";
		    //				break;
		case 1:
		    nomePapel = "Gerente de Segurança";
		    break;
		case 2:
		    nomePapel = "Gerente Escritorio Regional Balneário Camboriu";
		    break;
		case 3:
		    nomePapel = "Gerente Escritorio Regional Brusque";
		    break;
		case 4:
		    nomePapel = "Gerente Escritorio Regional Blumenal";
		    break;
		case 5:
		    nomePapel = "Gerente Escritorio Regional Joinville";
		    break;
		case 6:
		    nomePapel = "Gerente Escritorio Regional Lages";
		    break;
		case 7:
		    nomePapel = "Gerente Escritório Regional Tubarão";
		    break;
		case 8:
		    nomePapel = "Gerente Escritorio Regional Gaspar";
		    break;
		case 9:
		    nomePapel = "Gerente de Asseio Conserv. e Limpeza";
		    break;
		case 10:
		    nomePapel = "Gerente Escritorio Regional Chapeco";
		    break;
		case 11:
		    nomePapel = "Gerente Escritório Regional Rio do Sul";
		    break;
		case 12:
		    nomePapel = "Gerente Escritório Regional Jaraguá do Sul";
		    break;
		case 13:
		    nomePapel = "Coordenador Escritório Regional CTA";
		    break;
		case 14:
		    nomePapel = "Gerente Escritorio Regional Cascavel";
		    break;
		case 15:
		    nomePapel = "Coordenador Escritório Regional CTA Sul";
		    break;
		case 16:
		    nomePapel = "Gerente Escritório Regional Tubarão";
		    break;
		case 17:
		    nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
		    break;
		case 18:
		    nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
		    break;
		case 19:
		    nomePapel = "GerenteEscritorioRegionalCampinas";
		    break;
		case 20:
		    nomePapel = "GerenteEscritorioRegionalGoiania";
		    break;
		case 21:
		    nomePapel = "GerenteEscritorioRegionalGoiania";
		    break;
		case 22:
		    nomePapel = "GerenteEscritorioRegionalSaoJoseDoRioPreto";
		    break;
		case 23:
		    nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
		    break;

		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		if (obj != null && obj.getAllUsers() != null && !obj.getAllUsers().isEmpty())
		{
			return obj;
		}
		else
		{
			throw new WorkflowException("Gerente responsável não localizado!");
		}
	}
	
	

	public static NeoPaper getPapelResponsavelASO(Long codreg, String lotacao)
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 1:
				nomePapel = "Responsavel ASO - SOO";
				break;
			case 2:
				nomePapel = "Responsavel ASO - IAI";
				break;
			case 3:
				nomePapel = "Responsavel ASO - BQE";
				break;
			case 4:
				nomePapel = "Responsavel ASO - BNU";
				break;
			case 5:
				nomePapel = "Responsavel ASO - JLE";
				break;
			case 6:
				nomePapel = "Responsavel ASO - LGS";
				break;
			case 7:
				nomePapel = "Responsavel ASO - CUA";
				break;
			case 8:
				nomePapel = "Responsavel ASO - GPR";
				break;
			case 9:
				nomePapel = "Responsavel ASO - ACL";
				break;
			case 10:
				nomePapel = "Responsavel ASO - CCO";
				break;
			case 11:
				nomePapel = "Responsavel ASO - RSL";
				break;
			case 12:
				nomePapel = "Responsavel ASO - JGS";
				break;
			case 13:
				nomePapel = "Responsavel ASO - CTA";
				break;
			case 14:
				nomePapel = "Responsavel ASO - CSC";
				break;
			case 15:
				nomePapel = "Responsavel ASO - CTA-Sul";
				break;
			case 16:
				nomePapel = "Responsavel ASO - TRO";
		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		return obj;
	}

	public static NeoPaper getPapelResponsavelASOSede(Long codreg, String lotacao)
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{

			case 0:
				nomePapel = "ResponsavelASOADM";
				break;

			case 1:
				if (lotacao.equalsIgnoreCase("Gerência de Segurança Eletrônica"))
				{
					nomePapel = "Gerente de Vigilância Eletrônica";
				}
				else
				{
					nomePapel = "Gerente de Segurança";
				}
				break;

			case 2:
				nomePapel = "Gerente Escritorio Regional Balneário Camboriu";
				break;
			case 3:
				nomePapel = "Gerente Escritorio Regional Brusque";
				break;
			case 4:
				nomePapel = "Gerente Escritorio Regional Blumenal";
				break;
			case 5:
				nomePapel = "Gerente Escritorio Regional Joinville";
				break;
			case 6:
				nomePapel = "Gerente Escritorio Regional Lages";
				break;
			case 7:
				nomePapel = "Gerente Escritório Regional CUA";
				break;
			case 8:
				nomePapel = "Gerente Escritorio Regional Gaspar";
				break;
			case 9:
				nomePapel = "Gerente de Asseio Conserv. e Limpeza";
				break;
			case 10:
				nomePapel = "Gerente Escritorio Regional Chapeco";
				break;
			case 11:
				nomePapel = "Gerente Escritório Regional Rio do Sul";
				break;
			case 12:
				nomePapel = "Gerente Escritório Regional Jaraguá do Sul";
				break;
			case 13:
				//Técnicos de eletrônica CTA
				if (lotacao.startsWith("Técnico") && lotacao.contains("Eletrônica") && lotacao.endsWith("CTA"))
				{
					nomePapel = "Gerente de Segurança Eletrônica CTA";
				}
				//Táticos CTA
				else if (lotacao.startsWith("ASE") && lotacao.contains("ROTA") && lotacao.endsWith("CTA"))
				{
					nomePapel = "Gerente de Segurança Eletrônica CTA";
				}
				else
				//Outros
				{
					nomePapel = "Gerente Escritório Regional Curitiba Sul";
				}

				break;
			case 14:
				nomePapel = "Gerente Escritorio Regional Cascavel";
				break;
			case 15:
				nomePapel = "Gerente Escritório Regional Curitiba";
				break;
			case 16:
				nomePapel = "Gerente Escritório Regional Tubarão";
				break;
			case 17:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
			case 18:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
			case 19:
				nomePapel = "GerenteEscritorioRegionalCampinas";
				break;
			case 20:
				nomePapel = "GerenteEscritorioRegionalGoiania";
				break;
			case 21:
				nomePapel = "GerenteEscritorioRegionalGoiania";
				break;
			case 22:
				nomePapel = "GerenteEscritorioRegionalSaoJoseDoRioPreto";
				break;
			case 23:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;

		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		return obj;
	}

	public static NeoPaper getPapelResponsavelCancelarNF(Long codEmp)
	{
		String nomePapel = "";
		nomePapel = "Responsaveis Cancelar NF";
		//		switch (codEmp.intValue())
		//		{
		//			case 1:
		//				nomePapel = "Responsavel Cancelar NF Casvig";
		//				break;
		//			case 2:
		//				nomePapel = "Responsavel Cancelar NF SLC";
		//				break;
		//			case 6:
		//				nomePapel = "Responsavel Cancelar NF Proserv";
		//				break;
		//			case 7:
		//				nomePapel = "Responsavel Cancelar NF Orsegups Limpeza";
		//				break;
		//			case 8:
		//				nomePapel = "Responsavel Cancelar NF Profiser";
		//				break;
		//			case 15:
		//				nomePapel = "Responsavel Cancelar Orsegups Princesa";
		//				break;
		//			case 17:
		//				nomePapel = "Responsavel Cancelar NF Orsegups Visal";
		//				break;
		//			case 18:
		//				nomePapel = "Responsavel Cancelar NF Orsegups Monitoramento";
		//				break;
		//			case 19:
		//				nomePapel = "Responsavel Cancelar NF Back";
		//				break;
		//			case 21:
		//				nomePapel = "Responsavel Cancelar NF Metropolitana";
		//				break;
		//			case 22:
		//				nomePapel = "Responsavel Cancelar NF Objetiva";
		//				break;
		//			default:
		//				nomePapel = "Responsavel Cancelar NF Demais Empresas";// caso não encontra a empresa irá para Charlot
		//		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		return obj;
	}

	/**
	 * Realiza o cancelamento de um processo
	 * 
	 * @param String codigoProcesso Code que do processo que se deseja cancelar
	 * @param String codigoCancelamento Codigo referente ao registro do eform [GC] Parâmetros
	 *            Cancelamento
	 * @param String motivoCancelamento Texto contendo o motivo do cancelamento do processo
	 * @return String contendo 'OK' caso o processo tenha sido cancelado corretamente ou o texto com o
	 *         erro que tenha ocorrido
	 */
	@SuppressWarnings("unchecked")
	public static String cancelWorkflowProcessNeoId(String codigoProcesso, String codigoCancelamento, String motivoCancelamento, Long neoId)
	{
		String result = "OK";

		QLEqualsFilter parametrosFilter = new QLEqualsFilter("codigo", codigoCancelamento);

		NeoObject parametrosCancelamento = PersistEngine.getObject(AdapterUtils.getEntityClass("ParametrosCancelamento"), parametrosFilter);
		if (parametrosCancelamento != null)
		{
			EntityWrapper parametrosCancelamentoWrapper = new EntityWrapper(parametrosCancelamento);

			String classeValidacao = (String) parametrosCancelamentoWrapper.findValue("classeValidacao");
			String motivoCancelamentoPadrao = (String) parametrosCancelamentoWrapper.findValue("textoPadraoCancelamento");
			ProcessModel processModel = (ProcessModel) parametrosCancelamentoWrapper.findValue("Processo");

			QLEqualsFilter codigoFilter = new QLEqualsFilter("code", codigoProcesso);
			QLEqualsFilter processoFilter = new QLEqualsFilter("model", processModel);
			QLEqualsFilter neoidFilter = new QLEqualsFilter("neoId", neoId);

			QLGroupFilter groupFilter = new QLGroupFilter("AND");
			groupFilter.addFilter(codigoFilter);
			groupFilter.addFilter(processoFilter);
			groupFilter.addFilter(neoidFilter);

			WFProcess wfProcess = PersistEngine.getObject(WFProcess.class, groupFilter);

			if (wfProcess != null)
			{
				if (wfProcess.getProcessState() != ProcessState.running)
				{
					return "O processo '" + processModel.getName() + "' de número '" + codigoProcesso + "' não está em execução.";
				}

				try
				{
					if (classeValidacao != null && !classeValidacao.isEmpty())
					{
						Class<WorkflowEventListener> clazz = (Class<WorkflowEventListener>) Class.forName(classeValidacao);
						WorkflowEventListener listener = clazz.newInstance();
						if (listener instanceof WorkflowValidateCancelEventListener)
						{
							WorkflowValidateCancelEventListener validateCancelEventListener = (WorkflowValidateCancelEventListener) listener;
							WorkflowValidateCancelEvent workflowCanCancelEvent = new WorkflowValidateCancelEvent();
							workflowCanCancelEvent.setProcess(wfProcess.getProcess());
							EntityWrapper processWrapper = new EntityWrapper(wfProcess.getProcessEntity());
							workflowCanCancelEvent.setWrapper(processWrapper);
							validateCancelEventListener.validateCancel(workflowCanCancelEvent);
						}
					}
				}
				catch (WorkflowException wex)
				{
					//throw wex;
					if (wex.getErrorList() != null && wex.getErrorList().size() > 0)
					{
						return wex.getErrorList().get(0).toString();
					}
					else
					{
						return "";
					}
				}
				catch (Exception ex)
				{
					if (log.isErrorEnabled())
					{
						log.error("Erro ao disparar evento de validação de cancelamento do processo. (WorkflowValidateCancelEventListener)", ex);
					}
				}

				WFEngine wfEngine = WFEngine.getInstance();

				String motivo = "";

				if (motivoCancelamentoPadrao != null && !motivoCancelamentoPadrao.isEmpty())
				{
					motivo = motivoCancelamentoPadrao;
				}

				if (motivoCancelamento != null && !motivoCancelamento.isEmpty())
				{
					motivo += " " + motivoCancelamento;
				}

				wfEngine.cancelProcess(wfProcess, motivo);

			}
			else
			{
				return "Não foi possível encontrar o processo '" + processModel.getName() + "' de número '" + codigoProcesso + "'.";
			}
		}
		else
		{
			return "Não foi possível encontrar os parâmetros de cancelamento com o codigo '" + codigoCancelamento + "'.";
		}

		return result;
	}

	/**
	 * Método para pegar uma conexão com o banco utilizando o PersistEngine do fusion. <br>
	 * Caso a tentativa falhe, outras 4 tentativas serão feitas com intervalo de 1 segundo entre cada
	 * tentativa
	 * 
	 * @param fonteDados - fontes de dados configuradas no fusion. Ex.: "SAPIENS", "VETORH", "" (para
	 *            utilizar uma conexão com o banco de dados interno do fusion), etc.
	 * @return
	 */
	public static Connection getConnection(String fonteDados)
	{
		Connection retorno = null;

		for (int i = 0; i < 5; i++)
		{
			try
			{
				retorno = PersistEngine.getConnection(fonteDados);
				if (retorno == null)
				{
					throw new Exception("Tentativa de conexão com a fonte " + fonteDados + " falhou. (Conexão nula)");
				}
				return retorno;
			}
			catch (Exception e)
			{
				log.error("Tentativa de conexão com a fonte " + fonteDados + " falhou [" + (i + 1) + " de 5]. (Conexão nula)");
				try
				{
					Thread.sleep(1000);
				}
				catch (InterruptedException e1)
				{
					e1.printStackTrace();
				}
			}
		}
		log.error("OrsegupsUtils.getConnection(str) - Máximo de tentativas de conexão com a fonte de dados " + fonteDados + " excedido.");
		return null;

	}

	public static NeoPaper getPapelFotoAdmissao(Long codreg, String lotacao)
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 0:
				if (lotacao != null && lotacao.equals("Coordenadoria NAC"))
				{
					nomePapel = "Supervisor CEREC";
				}
				else if (lotacao != null && lotacao.equals("Assistente Indenização Contratual"))
				{
					nomePapel = "Coordenador de Ressarcimento";
				}
				else if (lotacao != null && lotacao.equals("Auxiliar Administrativo - Jurídico"))
				{
					nomePapel = "Assessor Jurídico";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Controladoria"))
				{
					nomePapel = "Gerente Controladoria";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Comercial Administrativa"))
				{
					nomePapel = "Supervisor Comercial Privado";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Comercial Publico"))
				{
					nomePapel = "Coordenador Comercial";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria da Qualidade"))
				{
					nomePapel = "Responsável Justificar Afastamento Qualidade";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Financeira"))
				{
					nomePapel = "Coordenador Financeiro";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Compras"))
				{
					nomePapel = "Coordenador Administrativo";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Administrativa"))
				{
					nomePapel = "Coordenador Administrativo";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria Almoxarifado de Eletrônica"))
				{
					nomePapel = "Coordenador Almoxarifado de Eletrônica Sede";
				}
				else if (lotacao != null && lotacao.equals("Comunicação e Marketing"))
				{
					nomePapel = "Responsável Marketing";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Sistemas da Informaçao"))
				{
					nomePapel = "Coordenador de Sistemas";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Infraestrutura"))
				{
					nomePapel = "Supervisor de Infraestrutura de TI";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Monitoramento"))
				{
					nomePapel = "ResponsavelFotoAdmissaoCM";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Recursos Humanos"))
				{
					nomePapel = "Coordenador de RH";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de Departamento Pessoal"))
				{
					nomePapel = "Coordenador de RH";
				}
				else if (lotacao != null && lotacao.equals("Coordenadoria de RH Estratégico"))
				{
					nomePapel = "Gerente RH Estratégico";
				}

				else if (lotacao != null && lotacao.equals("Coordenadoria de RH RSA"))
				{
					nomePapel = "Gerente RH Estratégico";
				}

				break;

			case 1:
				if (lotacao != null && lotacao.startsWith("Técnico de Eletrônica") || lotacao.startsWith("Supervisor Técnico de Eletrônica") || lotacao.startsWith("Técnicos de Eletrônica") || lotacao.startsWith("Agente de Inspeção Técnica") || lotacao != null && lotacao.startsWith("Auxiliar Técnico de Eletrônica") || lotacao != null && lotacao.startsWith("Cobertura de Férias - Segurança Eletrônica") || lotacao != null && lotacao.startsWith("Auxiliar Administrativo Eletrônica")
						|| lotacao != null && lotacao.startsWith("Supervisor de Segurança Eletrônica") || lotacao != null && lotacao.startsWith("Aviso Prévio - Segurança Eletrônica"))
				{
					nomePapel = "ResponsavelFotoAdmissaoVigilanciaEletronica";
				}
				else
				{
					nomePapel = "ResponsavelFotoAdmissaoSeguranca";
				}
				break;
			case 2:
				nomePapel = "ResponsavelFotoAdmissaoEscritorioRegionalIAI";
				break;
			case 3:
				nomePapel = "ResponsavelFotoAdmissaoEscritorioRegionalBQE";
				break;
			case 4:
				nomePapel = "ResponsavelFotoAdmissaoEscritorioRegionalBNU";
				break;
			case 5:
				nomePapel = "ResponsavelFotoAdmissaoEscritorioRegionalJLE";
				break;
			case 6:
				nomePapel = "ResponsavelFotoAdmissaoEscritorioRegionalLGS";
				break;
			case 7:
				nomePapel = "ResponsavelFotoAdmissaoEscritorioRegionalCUA";
				break;
			case 8:
				nomePapel = "ResponsavelFotoAdmissaoEscritorioRegionalGPR";
				break;
			case 9:
				nomePapel = "ResponsavelFotoAdmissaoAsseioConserveLimpeza";
				break;
			case 10:
				nomePapel = "ResponsavelFotoAdmissaoEscritorioRegionalCCO";
				break;
			case 11:
				nomePapel = "ResponsavelFotoAdmissaoEscritorioRegionalRSL";
				break;
			case 12:
				nomePapel = "ResponsavelFotoAdmissaoEscritorioRegionalJGS";
				break;
			case 13:
				//Técnicos de eletrônica CTA
				if (lotacao.startsWith("Técnico") && lotacao.contains("Eletrônica") && lotacao.endsWith("CTA"))
				{
					nomePapel = "ResponsavelFotoAdmissaoTecnicosCTA";
				}
				//Táticos CTA
				else if (lotacao.startsWith("ASE") && lotacao.contains("ROTA") && lotacao.endsWith("CTA"))
				{
					nomePapel = "ResponsavelFotoAdmissaoTaticosCTA";
				}
				else
				//Outros
				{
					nomePapel = "ResponsavelFotoAdmissaoEscritorioRegionalCTA";
				}
				break;
			case 14:
				nomePapel = "ResponsavelFotoAdmissaoEscritorioRegionalCSC";
				break;
			case 15:
				nomePapel = "ResponsavelFotoAdmissaoEscritorioRegionalCTASUL";
				break;
			case 16:
				nomePapel = "ResponsavelFotoAdmissaoEscritorioRegionalTRO";
				break;

		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		return obj;
	}

	public static NeoPaper getPapelTratarOSExcesso(Long codreg)
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 1:
				nomePapel = "TratarOSExcessoVigilanciaEletronica";
				break;
			case 2:
				nomePapel = "TratarOSExcessoEscritorioRegionalIAI";
				break;
			case 3:
				nomePapel = "TratarOSExcessoEscritorioRegionalBQE";
				break;
			case 4:
				nomePapel = "TratarOSExcessoEscritorioRegionalBNU";
				break;
			case 5:
				nomePapel = "TratarOSExcessoEscritorioRegionalJLE";
				break;
			case 6:
				nomePapel = "TratarOSExcessoEscritorioRegionalLGS";
				break;
			case 7:
				nomePapel = "TratarOSExcessoEscritorioRegionalCUA";
				break;
			case 8:
				nomePapel = "TratarOSExcessoEscritorioRegionalGPR";
				break;
			case 9:
				nomePapel = "TratarOSExcessoAsseioConserveLimpeza";
				break;
			case 10:
				nomePapel = "TratarOSExcessoEscritorioRegionalCCO";
				break;
			case 11:
				nomePapel = "TratarOSExcessoEscritorioRegionalRSL";
				break;
			case 12:
				nomePapel = "TratarOSExcessoEscritorioRegionalJGS";
				break;
			case 13:
				nomePapel = "TratarOSExcessoEscritorioRegionalCTA";
				break;
			case 14:
				nomePapel = "TratarOSExcessoEscritorioRegionalCSC";
				break;
			case 15:
				nomePapel = "TratarOSExcessoEscritorioRegionalCTASul";
				break;
			case 16:
				nomePapel = "TratarOSExcessoEscritorioRegionalTRO";
				break;
		}
		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		return obj;
	}

	public static NeoPaper getPapelGerenteRegional(Long codreg, Long serctr) throws WorkflowException
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 0:
				nomePapel = "Gerente Comercial Mercado Privado";
				break;
			case 1:
        			if (serctr == 1L || serctr == 4L || serctr == 51L || serctr == 52L){ //Humana
        			    nomePapel = "Gerente de Segurança";
        			} else { // Eletrônica
        				nomePapel = "Gerente de Vigilância Eletrônica";
                                }
				break;
			case 2:
				nomePapel = "Gerente Escritorio Regional Balneário Camboriu";
				break;
			case 3:
				nomePapel = "Gerente Escritorio Regional Brusque";
				break;
			case 4:
				nomePapel = "Gerente Escritorio Regional Blumenal";
				break;
			case 5:
				nomePapel = "Gerente Escritorio Regional Joinville";
				break;
			case 6:
				nomePapel = "Gerente Escritorio Regional Lages";
				break;
			case 7:
				nomePapel = "Gerente Escritório Regional CUA";
				break;
			case 8:
				nomePapel = "Gerente Escritorio Regional Gaspar";
				break;
			case 9:
				nomePapel = "Gerente de Asseio Conserv. e Limpeza";
				break;
			case 10:
				nomePapel = "Gerente Escritorio Regional Chapeco";
				break;
			case 11:
				nomePapel = "Gerente Escritório Regional Rio do Sul";
				break;
			case 12:
				nomePapel = "Gerente Escritório Regional Jaraguá do Sul";
				break;
			case 13:
				nomePapel = "Gerente Escritório Regional Curitiba";
				break;
			case 14:
				nomePapel = "Gerente Escritorio Regional Cascavel";
				break;
			case 15:
				nomePapel = "Gerente Escritório Regional Curitiba Sul";
				break;
			case 16:
				nomePapel = "Gerente Escritório Regional Tubarão";
				break;
			case 17:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
			case 18:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
			case 19:
				nomePapel = "GerenteEscritorioRegionalCampinas";
				break;
			case 20:
				nomePapel = "GerenteEscritorioRegionalGoiania";
				break;
			case 21:
				nomePapel = "GerenteEscritorioRegionalGoiania";
				break;
			case 22:
				nomePapel = "GerenteEscritorioRegionalSaoJoseDoRioPreto";
				break;
			case 23:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;

		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		if (obj != null && obj.getAllUsers() != null && !obj.getAllUsers().isEmpty())
		{
			return obj;
		}
		else
		{
			throw new WorkflowException("Gerente responsável não localizado!");
		}
	}

	/*
	 * FIXME Este metodo foi originalmente removido da 3.2.2 por nao atender
	 * requisitos minimos de seguranca e performance, foi provisoriamente direcionado
	 * para o utilitario da Orsegups, porem deve ser substituido por uma conexao
	 * atraves do Hibernate.
	 */
	public static Connection openConnection(NeoDataSource data)
	{
		AnnotationConfiguration config = new AnnotationConfiguration();
		config.setProperty("hibernate.connection.driver_class", data.getDriverClass());
		config.setProperty("hibernate.connection.username", data.getLogin());
		config.setProperty("hibernate.connection.password", NeoUtils.decodeDecodablePassword(data.getPassword()));
		config.setProperty("hibernate.connection.url", data.getConnectionURL());
		config.setProperty("hibernate.max_fetch_depth", "1");
		config.setProperty("hibernate.dialect", data.getHibernateDialect());
		config.setProperty("hibernate.hbm2ddl.auto", data.isGenerateSchema() ? "update" : "none");
		config.setProperty("hibernate.query.jpaql_strict_compliance", "false");

		data.addCustomProperties(config);

		if (log.isDebugEnabled())
		{
			log.debug("driver class: " + data.getDriverClass());
			log.debug("login: " + data.getLogin());
			log.debug("connection url: " + data.getConnectionURL());
			log.debug("dialect: " + data.getHibernateDialect());
		}

		try
		{
			ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(config.getProperties()).buildServiceRegistry();

			SessionFactoryImpl session = (SessionFactoryImpl) config.buildSessionFactory(serviceRegistry);

			return session.getConnectionProvider().getConnection();
		}
		catch (SQLException sqle)
		{
			StackedFormException e = new StackedFormException();
			e.addError(new EFormExceptionError(new I18nMessage("DBConnectionFailed", sqle.getMessage()), sqle));
			throw e;
		}
	}

	/**
	 * Recebe um arquivo File e vincula ao Fusion retornando um NeoFile
	 * 
	 * @param file (Qualquer arquivo desejado, desde que esteja salvo em algum lugar)
	 * @return Retorna um NeoFile para ser manipulado
	 */
	public static NeoFile criaNeoFile(File file)
	{
		NeoFile newFile = new NeoFile();
		newFile.setName(file.getName());
		newFile.setStorage(NeoStorage.getDefault());
		PersistEngine.persist(newFile);
		//PersistEngine.evict(newFile);
		OutputStream outi = newFile.getOutputStream();
		InputStream in = null;
		try
		{
			in = new BufferedInputStream(new FileInputStream(file));
			NeoStorage.copy(in, outi);
			newFile.setOrigin(NeoFile.Origin.TEMPLATE);
			return newFile;
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public static NeoPaper getPapelTratarOsEmAtraso(Long codreg)
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 1:
				nomePapel = "TratarOSAtrasoVigilanciaEletronica";
				break;
			case 2:
				nomePapel = "TratarOSAtrasoEscritorioRegionalIAI";
				break;
			case 3:
				nomePapel = "TratarOSAtrasoEscritorioRegionalBQE";
				break;
			case 4:
				nomePapel = "TratarOSAtrasoEscritorioRegionalBNU";
				break;
			case 5:
				nomePapel = "TratarOSAtrasoEscritorioRegionalJLE";
				break;
			case 6:
				nomePapel = "TratarOSAtrasoEscritorioRegionalLGS";
				break;
			case 7:
				nomePapel = "TratarOSAtrasoEscritorioRegionalCUA";
				break;
			case 8:
				nomePapel = "TratarOSAtrasoEscritorioRegionalGPR";
				break;
			case 9:
				nomePapel = "TratarOSAtrasodeAsseioConserv.eLimpeza";
				break;
			case 10:
				nomePapel = "TratarOSAtrasoEscritorioRegionalCCO";
				break;
			case 11:
				nomePapel = "TratarOSAtrasoEscritorioRegionalRSL";
				break;
			case 12:
				nomePapel = "TratarOSAtrasoEscritorioRegionalJGS";
				break;
			case 13:
				nomePapel = "TratarOSAtrasoEscritorioRegionalCTA";
				break;
			case 14:
				nomePapel = "TratarOSAtrasoEscritorioRegionalCSC";
				break;
			case 15:
				nomePapel = "TratarOSAtrasoEscritorioRegionalCTASul";
				break;
			case 16:
				nomePapel = "TratarOSAtrasoEscritorioRegionalTRO";
				break;
			case 17:
				nomePapel = "TratarOSAtrasoEscritorioRegionalNHO";
				break;
			case 18:
				nomePapel = "TratarOSAtrasoEscritorioRegionalTRI";
				break;
			case 19:
				nomePapel = "TratarOSAtrasoEscritorioRegionalCAS";
				break;
			case 20:
				nomePapel = "TratarOSAtrasoEscritorioRegionalGNA";
				break;
			case 21:
				nomePapel = "TratarOSAtrasoEscritorioRegionalPMJ";
				break;
			case 22:
				nomePapel = "TratarOSAtrasoEscritorioRegionalSRR";
				break;
			case 23:
				nomePapel = "TratarOSAtrasoEscritorioRegionalXLN";
				break;
		}
		System.out.println(nomePapel);
		NeoPaper obj = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", nomePapel));
		return obj;
	}

	public static List<NeoObject> retornaTarefasDeReciclagemAtivas(String tarefa, Long numcad, Long numemp)
	{
		QLEqualsFilter processoFilter = new QLEqualsFilter("wfprocess.processState", ProcessState.running.ordinal());
		QLEqualsFilter savedFilter = new QLEqualsFilter("wfprocess.saved", true);

		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		groupFilter.addFilter(processoFilter);
		groupFilter.addFilter(savedFilter);

		QLEqualsFilter qlNumcad = null;
		if (numcad != null && numcad != 0L)
		{
			qlNumcad = new QLEqualsFilter("numcad", tarefa);
			groupFilter.addFilter(qlNumcad);
		}

		QLEqualsFilter qlNumEmp = null;
		if (numemp != null && numemp != 0L)
		{
			qlNumEmp = new QLEqualsFilter("numemp", numemp);
			groupFilter.addFilter(qlNumEmp);
		}

		List<NeoObject> objs = PersistEngine.getObjects(AdapterUtils.getEntityClass("TAREFASDERECICLAGEM"), groupFilter, -1, -1, "Wfprocess.code");

		return objs;
	}

	public static NeoPaper getPapelColaboradorSemFoto(Long codreg, String lotacao)
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 0:
				nomePapel = "ResponsavelColaboradorSemFotoSOO";
				break;
			case 1:
				nomePapel = "ResponsavelColaboradorSemFotoSeguranca";
				break;
			case 2:
				nomePapel = "ResponsavelColaboradorSemFotoEscritorioRegionalIAI";
				break;
			case 3:
				nomePapel = "ResponsavelColaboradorSemFotoEscritorioRegionalBQE";
				break;
			case 4:
				nomePapel = "ResponsavelColaboradorSemFotoEscritorioRegionalBNU";
				break;
			case 5:
				nomePapel = "ResponsavelColaboradorSemFotoEscritorioRegionalJLE";
				break;
			case 6:
				nomePapel = "ResponsavelColaboradorSemFotoEscritorioRegionalLGS";
				break;
			case 7:
				nomePapel = "ResponsavelColaboradorSemFotoEscritorioRegionalCUA";
				break;
			case 8:
				nomePapel = "ResponsavelColaboradorSemFotoEscritorioRegionalGPR";
				break;
			case 9:
				nomePapel = "ResponsavelColaboradorSemFotoAsseioConserveLimpeza";
				break;
			case 10:
				nomePapel = "ResponsavelColaboradorSemFotoEscritorioRegionalCCO";
				break;
			case 11:
				nomePapel = "ResponsavelColaboradorSemFotoEscritorioRegionalRSL";
				break;
			case 12:
				nomePapel = "ResponsavelColaboradorSemFotoEscritorioRegionalJGS";
				break;
			case 13:
				nomePapel = "ResponsavelColaboradorSemFotoEscritorioRegionalCTA";
				break;
			case 14:
				nomePapel = "ResponsavelColaboradorSemFotoEscritorioRegionalCSC";
				break;
			case 15:
				nomePapel = "ResponsavelColaboradorSemFotoEscritorioRegionalCTA";
				break;
			case 16:
				nomePapel = "ResponsavelColaboradorSemFotoEscritorioRegionalTRO";
				break;

		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		return obj;
	}

	public static NeoPaper getPapelGerenteRegionalProdutividadeAit(Long codreg) throws WorkflowException
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 0:
				nomePapel = "Gerente Comercial Mercado Privado";
				break;
			case 1:
				nomePapel = "Gerente de Vigilância Eletrônica";
				break;
			case 2:
				nomePapel = "Gerente Escritorio Regional Balneário Camboriu";
				break;
			case 3:
				nomePapel = "Gerente Escritorio Regional Brusque";
				break;
			case 4:
				nomePapel = "Gerente Escritorio Regional Blumenal";
				break;
			case 5:
				nomePapel = "Gerente Escritorio Regional Joinville";
				break;
			case 6:
				nomePapel = "Gerente Escritorio Regional Lages";
				break;
			case 7:
				nomePapel = "Gerente Escritório Regional CUA";
				break;
			case 8:
				nomePapel = "Gerente Escritorio Regional Gaspar";
				break;
			case 9:
				nomePapel = "Gerente de Asseio Conserv. e Limpeza";
				break;
			case 10:
				nomePapel = "Gerente Escritorio Regional Chapeco";
				break;
			case 11:
				nomePapel = "Gerente Escritório Regional Rio do Sul";
				break;
			case 12:
				nomePapel = "Gerente Escritório Regional Jaraguá do Sul";
				break;
			case 13:
				nomePapel = "GerenteProdutividadeCTA";
				break;
			case 14:
				nomePapel = "Gerente Escritorio Regional Cascavel";
				break;
			case 15:
				nomePapel = "Gerente Escritório Regional Curitiba Sul";
				break;
			case 16:
				nomePapel = "Gerente Escritório Regional Tubarão";
				break;
			case 17:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
			case 18:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
			case 19:
				nomePapel = "GerenteEscritorioRegionalCampinas";
				break;
			case 20:
				nomePapel = "GerenteEscritorioRegionalGoiania";
				break;
			case 21:
				nomePapel = "GerenteEscritorioRegionalGoiania";
				break;
			case 22:
				nomePapel = "GerenteEscritorioRegionalSaoJoseDoRioPreto";
				break;
			case 23:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
				
				

		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		if (obj != null && obj.getAllUsers() != null && !obj.getAllUsers().isEmpty())

		{
			return obj;
		}

		else
		{
			throw new WorkflowException("Gerente responsável não localizado!");
		}
	}

	/**
	 * Substitui caracteres acentuados por caracteres normais
	 * 
	 * @param string fonte
	 * @return String ajustada
	 */
	public static String retiraCaracteresAcentuados(String stringFonte)
	{
		String passa = stringFonte;
		passa = passa.replaceAll("[ÂÀÁÄÃ]", "A");
		passa = passa.replaceAll("[âãàáä]", "a");
		passa = passa.replaceAll("[ÊÈÉË]", "E");
		passa = passa.replaceAll("[êèéë]", "e");
		passa = passa.replaceAll("ÎÍÌÏ", "I");
		passa = passa.replaceAll("îíìï", "i");
		passa = passa.replaceAll("[ÔÕÒÓÖ]", "O");
		passa = passa.replaceAll("[ôõòóö]", "o");
		passa = passa.replaceAll("[ÛÙÚÜ]", "U");
		passa = passa.replaceAll("[ûúùü]", "u");
		passa = passa.replaceAll("Ç", "C");
		passa = passa.replaceAll("ç", "c");
		passa = passa.replaceAll("[ýÿ]", "y");
		passa = passa.replaceAll("Ý", "Y");
		passa = passa.replaceAll("ñ", "n");
		passa = passa.replaceAll("Ñ", "N");
		//	  passa = passa.replaceAll("[-+=*&amp;%$#@!_]", "");
		//	  passa = passa.replaceAll("['\"]", "");
		//	  passa = passa.replaceAll("[<>()\\{\\}]", "");
		//	  passa = passa.replaceAll("['\\\\.,()|/]", "");
		//	  passa = passa.replaceAll("[^!-ÿ]{1}[^ -ÿ]{0,}[^!-ÿ]{1}|[^!-ÿ]{1}", "");
		return passa;
	}
	
	public static NeoPaper getPapelResponsavelJ002ReciboFerias(Long codreg, String lotacao)
	{
		String nomePapel = "";

		switch (codreg.intValue())
		{
    			case 0 :
    			    	nomePapel = "R001ResponsavelReciboFeriasSOOS";
    			    	break;
			case 1:
				nomePapel = "j002ResponsavelReciboFeriasSOO";
				break;
			case 2:
				nomePapel = "j002ResponsavelReciboFeriasIAI";
				break;
			case 3:
				nomePapel = "j002ResponsavelReciboFeriasBQE";
				break;
			case 4:
				nomePapel = "j002ResponsavelReciboFeriasBNU";
				break;
			case 5:
				nomePapel = "j002ResponsavelReciboFeriasJLE";
				break;
			case 6:
				nomePapel = "j002ResponsavelReciboFeriasLGS";
				break;
			case 7:
				nomePapel = "j002ResponsavelReciboFeriasCUA";
				break;
			case 8:
				nomePapel = "j002ResponsavelReciboFeriasGPR";
				break;
			case 9:
				nomePapel = "j002ResponsavelReciboFeriasACL";
				break;
			case 10:
				nomePapel = "j002ResponsavelReciboFeriasCCO";
				break;
			case 11:
				nomePapel = "j002ResponsavelReciboFeriasRSL";
				break;
			case 12:	
				nomePapel = "j002ResponsavelReciboFeriasJGS";
				break;
			case 13:	
				nomePapel = "j002ResponsavelReciboFeriasCTA";
				break;
			case 14:	
				nomePapel = "j002ResponsavelReciboFeriasCSC";
				break;
			case 15:	
				nomePapel = "j002ResponsavelReciboFeriasCTA";
				break;
			case 16:	
				nomePapel = "j002ResponsavelReciboFeriasTRO";
				break;
			case 17:
			    	nomePapel = "j002ResponsavelReciboFeriasNHO";
			    	break;
			case 18:
			    	nomePapel = "j002ResponsavelReciboFeriasTRI";
			    	break;
			case 19:
			    	nomePapel = "j002ResponsavelReciboFeriasCAS";
			    	break;
			case 20:
			    	nomePapel = "j002ResponsavelReciboFeriasGNA";
			    	break;
			case 21:
			    	nomePapel = "j002ResponsavelReciboFeriasGNA"; //Regional Palmas - Destino Goiania
			    	break;
			case 22:
			    	nomePapel = "j002ResponsavelReciboFeriasSRR";
			    	break;
			case 23:
			    	nomePapel = "j002ResponsavelReciboFeriasTRI"; //Regional Xangrilá - Destino Tramandaí
			    	break;
		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		if (obj != null && obj.getAllUsers() != null && !obj.getAllUsers().isEmpty()) {
			return obj;
		}
		else {
			throw new WorkflowException("Responsável pela regional não localizado!");
		}
	}
	
	public static NeoPaper getPapelGerenteRegionalPorNegocio(Long codreg, Long idNegocio) throws WorkflowException
	{
		/*Tipo do Negócio 
		 * 1 - Eletrônica
		 * 2 - Humana
		 */
		
		String nomePapel = "";

		switch (codreg.intValue())
		{
			case 0:
				nomePapel = "Gerente Comercial Mercado Privado";
				break;
			case 1:
				if(idNegocio == 1L)
				{
					nomePapel = "Gerente de Vigilância Eletrônica";
				}
				else if (idNegocio == 2L)
				{
					nomePapel = "Gerente de Segurança";
				}
				break;
			case 2:
				nomePapel = "Gerente Escritorio Regional Balneário Camboriu";
				break;
			case 3:
				nomePapel = "Gerente Escritorio Regional Brusque";
				break;
			case 4:
				nomePapel = "Gerente Escritorio Regional Blumenal";
				break;
			case 5:
				nomePapel = "Gerente Escritorio Regional Joinville";
				break;
			case 6:
				nomePapel = "Gerente Escritorio Regional Lages";
				break;
			case 7:
				nomePapel = "Gerente Escritório Regional CUA";
				break;
			case 8:
				nomePapel = "Gerente Escritorio Regional Gaspar";
				break;
			case 9:
				nomePapel = "Gerente de Asseio Conserv. e Limpeza";
				break;
			case 10:
				nomePapel = "Gerente Escritorio Regional Chapeco";
				break;
			case 11:
				nomePapel = "Gerente Escritório Regional Rio do Sul";
				break;
			case 12:
				nomePapel = "Gerente Escritório Regional Jaraguá do Sul";
				break;
			case 13:
				if(idNegocio == 1L)
				{
					nomePapel = "Gerente Escritório Regional Curitiba";
				}
				else if(idNegocio == 2L)
				{
					nomePapel = "Gerente Operacional Curitiba";
				}
				break;
			case 14:
				nomePapel = "Gerente Escritorio Regional Cascavel";
				break;
			case 15:
				nomePapel = "Gerente Escritório Regional Curitiba Sul";
				break;
			case 16:
				nomePapel = "Gerente Escritório Regional Tubarão";
				break;
			case 17:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
			case 18:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;
			case 19:
				nomePapel = "GerenteEscritorioRegionalCampinas";
				break;
			case 20:
				nomePapel = "GerenteEscritorioRegionalGoiania";
				break;
			case 21:
				nomePapel = "GerenteEscritorioRegionalGoiania";
				break;
			case 22:
				nomePapel = "GerenteEscritorioRegionalSaoJoseDoRioPreto";
				break;
			case 23:
				nomePapel = "GerenteEscritorioRegionalNovoHamburgo";
				break;

		}

		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", nomePapel));
		if (obj != null && obj.getAllUsers() != null && !obj.getAllUsers().isEmpty())
		{
			return obj;
		}
		else
		{
			throw new WorkflowException("Gerente responsável não localizado!");
		}
	}
	

	public static void sendEmailWhatsAppRAT(Long cdCliente, String assunto, String endereco, String data){
	    if (rotinaAtiva("RATWHATS")){
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		StringBuilder sql = new StringBuilder();
		Map<String, String> contatos = new HashMap<>();
		String cnpj = null;
		try {
		    conn = PersistEngine.getConnection("SIGMA90");
		    
		    sql.append(" SELECT P.NOME,P.FONE1,P.FONE2, C.CGCCPF FROM dbCENTRAL C ");
		    sql.append(" INNER JOIN DBPROVIDENCIA P ON P.CD_CLIENTE = C.CD_CLIENTE "); 
		    sql.append(" WHERE C.CD_CLIENTE = ? ");
		    
		    pstm = conn.prepareStatement(sql.toString());
		    
		    pstm.setLong(1, cdCliente);
		    
		    rs = pstm.executeQuery();		
		    while (rs.next()) {
			String nome = rs.getString("nome");
			String fone1 = rs.getString("fone1");
			String fone2 = rs.getString("fone2");
			cnpj = rs.getString("CGCCPF");
			if (nome != null){
			    fone1 = preparaNumeroWhats(fone1);
			    fone2 = preparaNumeroWhats(fone2);
			    if (fone1 != null){
				contatos.put(fone1, nome);
			    }
			    if (fone2 != null){
				contatos.put(fone2, nome);
			    }
			}
		    }
		    
		} catch (SQLException e) {
		    e.printStackTrace();
		} finally {
		    OrsegupsUtils.closeConnection(conn, pstm, rs);
		}	    
		if (contatos.size() > 0){
		    try{
			for (String key : contatos.keySet()){
//			    if (!mensagemJaEnviada(key)){
			    if (!mensagemEnviada(key)){
				String link = montarLink(contatos.get(key), data, endereco, key);
				String link2 = montarLinkMensagem2(key);
				String linkResposta = montarLinkResposta(contatos.get(key), key);
				String html = "<a href=\""+link+"\">Mensagem 1</a><br>";
				html += "<strong>Mensagem 2</strong><br>"+link2+"<br><br>";
				html += "<strong>Anexar GIF</strong><br><br>";
				html += "<strong>Resposta Canal:</strong><br>"+linkResposta+"<br><br>";
				sendEmailWhatsApp(html, assunto, key, cnpj);
			    }
			}
		    }catch(Exception e){
			e.printStackTrace();
		    }
		}
	    }
	}
	
	private static boolean mensagemJaEnviada(String telefone){
	    boolean retorno = false;
	    Connection conn = null;
	    PreparedStatement pstm = null;
	    ResultSet rs = null;

	    StringBuilder sql = new StringBuilder();

	    try {
		conn = PersistEngine.getConnection("TIDB");
		
		sql.append(" SELECT TOP 1 ID FROM MensagensEnviadasRatWhats WHERE TELEFONE = ? AND (USUARIO_ATIVO IS NOT NULL OR USUARIO_ATIVO = 1) ");
		
		pstm = conn.prepareStatement(sql.toString());

		pstm.setString(1, telefone);

		rs = pstm.executeQuery();

		while (rs.next()) {
		    retorno = true;
		}

	    } catch (SQLException e) {
		e.printStackTrace();
	    } finally {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    }
	    return retorno;
	}
	
	private static boolean mensagemEnviada(String telefone){
	    boolean retorno = false;
	    Connection conn = null;
	    PreparedStatement pstm = null;
	    ResultSet rs = null;

	    StringBuilder sql = new StringBuilder();

	    try {
		conn = PersistEngine.getConnection("TIDB");
		
		sql.append(" SELECT TOP 1 ID FROM MensagensEnviadasRatWhats WHERE TELEFONE = ?");
		
		pstm = conn.prepareStatement(sql.toString());

		pstm.setString(1, telefone);

		rs = pstm.executeQuery();

		while (rs.next()) {
		    retorno = true;
		}

	    } catch (SQLException e) {
		e.printStackTrace();
	    } finally {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    }
	    return retorno;
	}
	
	private static void gravarTelefoneJaEnviado(String telefone, String cnpj){
	    Connection conn = null;
	    PreparedStatement pstm = null;
	    ResultSet rs = null;

	    StringBuilder sql = new StringBuilder();

	    try {
		conn = PersistEngine.getConnection("TIDB");
		sql.append(" INSERT INTO MensagensEnviadasRatWhats (TELEFONE, CNPJ, QTDE_ENVIO) VALUES (?,?,1) ");
		pstm = conn.prepareStatement(sql.toString());

		pstm.setString(1, telefone);
		pstm.setString(2, cnpj);
		pstm.execute();

	    } catch (SQLException e) {
		e.printStackTrace();
	    } finally {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    }
	}
	
	private static void atualizarTelefoneJaEnviado(String telefone, String cnpj){
	    Connection conn = null;
	    PreparedStatement pstm = null;
	    ResultSet rs = null;

	    StringBuilder sql = new StringBuilder();

	    try {
		conn = PersistEngine.getConnection("TIDB");
		sql.append(" UPDATE MensagensEnviadasRatWhats SET CNPJ = ?, QTDE_ENVIO = QTDE_ENVIO+1 WHERE TELEFONE = ? AND (USUARIO_ATIVO IS NULL OR USUARIO_ATIVO = 0) ");
		pstm = conn.prepareStatement(sql.toString());

		pstm.setString(1, cnpj);
		pstm.setString(2, telefone);
		pstm.execute();

	    } catch (SQLException e) {
		e.printStackTrace();
	    } finally {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    }
	}
	
	private static boolean rotinaAtiva(String rotina){
	    boolean retorno = false;
	    Connection conn = null;
	    PreparedStatement pstm = null;
	    ResultSet rs = null;

	    StringBuilder sql = new StringBuilder();

	    try {
		conn = PersistEngine.getConnection("TIDB");
		
		sql.append(" SELECT 1 FROM CONTROLE_EXECUCAO WHERE ROTINA = ? AND ATIVO = 1 ");
		
		pstm = conn.prepareStatement(sql.toString());

		pstm.setString(1, rotina);

		rs = pstm.executeQuery();

		while (rs.next()) {
		    retorno = true;
		}

	    } catch (SQLException e) {
		e.printStackTrace();
	    } finally {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    }
	    return retorno;
	}
	
	private static String preparaNumeroWhats(String numero) {
            String numeroOriginal = numero;
            String numeroPreparado = null;
            if (numeroOriginal != null) {
                // Considera apenas os numeros
                numeroOriginal = numeroOriginal.replaceAll("\\D+","");
                // Obtem as ultimas 9 posicoes
                if (numeroOriginal.length() >= 9) {
                    int start = numeroOriginal.length() - 9;                   
                    String numeroNovo = numeroOriginal.substring(start);
                    // Obtem DDD
                    String ddd = numeroOriginal.substring(0, start);
                    if (ddd.length() >= 2) {
                        start = ddd.length() - 2;                      
                        ddd = ddd.substring(start);
                        if ( (ddd.compareTo("11") >= 0 ) && (ddd.compareTo("99") <= 0) ) {
                            numeroPreparado = "55" + ddd + numeroNovo;
                        }
                    }
                }
            }
            return numeroPreparado;
	}
	
	private static String montarLink(String nome, String data, String endereco, String fone){
	    String retorno = null;
	    String[] name = nome.split(" ");
	    String msg = "Olá "+name[0]+", aqui é o Edson Heinz, Gerente da Central de Monitoramento da Orsegups.\n";
	    msg += "Estou entrando em contato pois em "+data+" atendemos um alarme no seu patrimônio no endereço "+endereco+".";
	    
	    try {
		retorno = "https://api.whatsapp.com/send?phone="+fone+"&text="+msg;
		URL url= new URL(retorno);
		URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
		retorno = uri.toASCIIString();
	    } catch (Exception e) {
		e.printStackTrace();
	    }	    	    
	    return retorno;
	}
	
	private static String montarLinkMensagem2(String fone){
	    String msg = "\nVocê sabia que é possível acompanhar em tempo real estes atendimentos, utilizando nosso aplicativo? Veja abaixo como é fácil...\n\n";
	    msg += "Baixe nosso aplicativo: https://www.orsegups.com.br/appportal";
	    
	    return msg;
	}
	
	private static String montarLinkResposta(String nome, String fone){
	    String msg = "Olá, este número de WhatsApp é utilizado apenas para divulgação do aplicativo. Para outras solicitações, favor utilizar o WhatsApp ORSEGUPS, disponível dentro do App Portal: https://www.orsegups.com.br/appportal";
	    
	    return msg;
	}
	
	private static void sendEmailWhatsApp(String link,String assunto, String telefone, String cnpj){
	    try {

		MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

		MailSettings mailClone = new MailSettings();
		mailClone.setMinutesInterval(settings.getMinutesInterval());
		mailClone.setFromEMail(settings.getFromEMail());
		mailClone.setFromName(settings.getFromName());
		mailClone.setRenderServer(settings.getRenderServer());
		mailClone.setSecurityFromEmail(settings.getSecurityFromEmail());
		mailClone.setSecurityCopyEmail(settings.getSecurityCopyEmail());
		mailClone.setSmtpSettings(settings.getSmtpSettings());
		mailClone.setPort(settings.getPort());
		mailClone.setSmtpServer(settings.getSmtpServer());
		mailClone.setEnabled(settings.isEnabled());	

		mailClone.setFromEMail("cm.rat@orsegups.com.br");
		mailClone.setFromName("Orsegups Participações S.A.");

		if (mailClone != null) {
		    

		    HtmlEmail noUserEmail = new HtmlEmail();
		    StringBuilder noUserMsg = new StringBuilder();
		    noUserEmail.setCharset("ISO-8859-9");

		    noUserEmail.addTo("rat-whats@orsegups.com.br");
		    noUserEmail.addBcc("lucas.alison@orsegups.com.br");
//		    noUserEmail.addBcc("emailautomatico@orsegups.com.br");
		    noUserEmail.setSubject("RAT WHATS - "+assunto);
		    noUserMsg.append(link);
		    noUserEmail.setHtmlMsg(noUserMsg.toString());
		    noUserEmail.setContent(noUserMsg.toString(), "text/html");
		    mailClone.applyConfig(noUserEmail);

		    noUserEmail.send();
		    if (mensagemEnviada(telefone)){
			atualizarTelefoneJaEnviado(telefone, cnpj);
		    }else{
			gravarTelefoneJaEnviado(telefone, cnpj);
		    }
		}

	    } catch (Exception e) {
		e.printStackTrace();
		log.error("##### ERRO ao enviar email sobre whatsapp - Data: " + e.getMessage().toString() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

	    }
	}
	
	public static String formataTempo(int elapsed){
	    int ss = elapsed % 60;
	    elapsed /= 60;
	    int min = elapsed % 60;
	    elapsed /= 60;
	    int hh = elapsed % 24;
	    return strzero(hh) + ":" + strzero(min) + ":" + strzero(ss);
	}
  	private static String strzero(int n){
	    if(n < 10)
	      return "0" + String.valueOf(n);
	    return String.valueOf(n);
	}
  	
  	public static NeoUser retornaPrimeiroUsuarioNoPapel(NeoPaper papel){
  	    NeoUser userReturn = null;
  	    
  	    Set<NeoUser> setUsers = papel.getAllUsers();
  	    for(NeoUser user : setUsers) {
  		userReturn = user;
  	    }
  	    
  	    return userReturn;
  	    
  	} 	
}
