package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class RotinaAbreTarefaSimplesOrdemServicoEmAtrasoPor5DiasNovo implements CustomJobAdapter{

    @Override
    public void execute(CustomJobContext ctx) {
	
	Long key = GregorianCalendar.getInstance().getTimeInMillis();
	System.out.println("### INICIOU JOB RotinaAbreTarefaSimplesOrdemServicoEmAtrasoPor5DiasNovo");
	
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	
	try {
	    
	    conn = PersistEngine.getConnection("");
	 
	    StringBuffer  varname1 = new StringBuffer();
	    
	    varname1.append("                 SELECT distinct CONVERT(VARCHAR(MAX),'OS aberta a mais de 05 dias úteis sem execução e agendamento') AS TIPOOS, ");
	    varname1.append("	    	             SUBSTRING(col.NM_COLABORADOR,0,4) as REGIONAL, ");
	    varname1.append("	    		     col.NM_COLABORADOR, ");
	    varname1.append("	    		     def.DESCRICAODEFEITO, ");
	    varname1.append("	    		     os.ID_ORDEM, ");
	    varname1.append("	    		     os.ABERTURA, ");
	    varname1.append("	    		     CONVERT(VARCHAR(MAX),os.DEFEITO) AS DEFEITO, ");
	    varname1.append("	    		     os.ID_INSTALADOR, ");
	    varname1.append("	    		     c.ID_EMPRESA, ");
	    varname1.append("	    		     c.ID_CENTRAL, ");
	    varname1.append("	    		     c.PARTICAO , ");
	    varname1.append("	    		     c.CD_CLIENTE, ");
	    varname1.append("	    		     c.FANTASIA, ");
	    varname1.append("	    		     c.RAZAO, ");
	    varname1.append("	    		     c.ENDERECO, ");
	    varname1.append("	    		     bai.NOME AS NOMEBAIRRO, ");
	    varname1.append("	    		     cid.NOME AS NOMECIDADE, ");
	    varname1.append("	    		     cid.ID_ESTADO, ");
	    varname1.append("	    		     os.FECHAMENTO, ");
	    varname1.append("	    		     os.FECHADO, ");
	    varname1.append("	    		     osh.DATAINICIOEXECUCAO, ");
	    varname1.append("	    		     osh.DATAFIMEXECUCAO, ");
	    varname1.append("	    		     CONVERT(VARCHAR(MAX),os.EXECUTADO) AS EXECUTADO, ");
	    varname1.append("	    		     sol.NM_DESCRICAO, ");
	    varname1.append("	    		     CD_COLABORADOR, ");
	    varname1.append("	    		     mp.NM_DESCRICAO AS NOMEMOTIVOPAUSA, ");
	    varname1.append("	    		     p.TX_OBSERVACAO, ");
	    varname1.append("	    		     p.CD_MOTIVO_PAUSA, ");
	    varname1.append("	    		     os.DATAAGENDADA, ");
	    varname1.append("	    		     p.DT_PAUSA ");
	    varname1.append("	    		FROM [FSOODB03\\SQL01].SIGMA90.dbo.dbORDEM os WITH (NOLOCK) ");
	    varname1.append("	      INNER JOIN [FSOODB03\\SQL01].SIGMA90.dbo.COLABORADOR col WITH (NOLOCK) ON col.CD_COLABORADOR = os.ID_INSTALADOR ");
	    varname1.append("	      INNER JOIN [FSOODB03\\SQL01].SIGMA90.dbo.OS_SOLICITANTE sol WITH (NOLOCK) ON sol.CD_OS_SOLICITANTE = os.CD_OS_SOLICITANTE ");
	    varname1.append("	      INNER JOIN [FSOODB03\\SQL01].SIGMA90.dbo.OSDEFEITO def WITH (NOLOCK) ON def.IDOSDEFEITO = os.IDOSDEFEITO ");
	    varname1.append("	      INNER JOIN [FSOODB03\\SQL01].SIGMA90.dbo.dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = os.CD_CLIENTE ");
	    varname1.append("	       LEFT JOIN [FSOODB03\\SQL01].SIGMA90.dbo.dbBAIRRO bai WITH (NOLOCK) ON bai.ID_BAIRRO = c.ID_BAIRRO AND bai.ID_CIDADE = c.ID_CIDADE ");
	    varname1.append("	       LEFT JOIN [FSOODB03\\SQL01].SIGMA90.dbo.dbCIDADE cid WITH (NOLOCK) ON cid.ID_CIDADE = c.ID_CIDADE ");
	    varname1.append("	       LEFT JOIN [FSOODB03\\SQL01].SIGMA90.dbo.OSHISTORICO osh WITH (NOLOCK) ON osh.ID_ORDEM = os.ID_ORDEM AND osh.IDOSHISTORICO = (SELECT MIN(IDOSHISTORICO) FROM [FSOODB03\\SQL01].SIGMA90.dbo.OSHISTORICO WHERE ID_ORDEM = os.ID_ORDEM) ");
	    varname1.append("	       LEFT JOIN [FSOODB03\\SQL01].SIGMA90.dbo.ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p ON p.CD_ORDEM_SERVICO = os.ID_ORDEM AND p.CD_ORDEM_SERVICO_MOTIVO_PAUSA = (SELECT MAX(p2.CD_ORDEM_SERVICO_MOTIVO_PAUSA) FROM [FSOODB03\\SQL01].SIGMA90.dbo.ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p2 WHERE p2.CD_ORDEM_SERVICO = p.CD_ORDEM_SERVICO ) ");
	    varname1.append("	       LEFT JOIN [FSOODB03\\SQL01].SIGMA90.dbo.MOTIVO_PAUSA mp ON mp.CD_MOTIVO_PAUSA = p.CD_MOTIVO_PAUSA ");
	    varname1.append("	      INNER JOIN [FSOODB04\\SQL02].Sapiens.dbo.USU_T160SIG sig WITH (NOLOCK) ON sig.usu_codcli = c.CD_CLIENTE ");
	    varname1.append("	      INNER JOIN [FSOODB04\\SQL02].Sapiens.dbo.USU_T160CVS cvs WITH (NOLOCK) ON cvs.usu_codemp = sig.usu_codemp AND cvs.usu_numctr = sig.usu_numctr AND cvs.usu_numpos = sig.usu_numpos ");
	    varname1.append("	      INNER JOIN [FSOODB04\\SQL02].Sapiens.dbo.USU_T160CTR ctr WITH (NOLOCK) ON ctr.usu_codemp = cvs.usu_codemp and ctr.usu_codfil = cvs.usu_codfil and ctr.usu_numctr = cvs.usu_numctr ");
	    varname1.append("	      INNER JOIN [FSOODB04\\SQL02].Sapiens.dbo.E085CLI cli WITH (NOLOCK) ON CLI.CodCli = CTR.usu_codcli ");
	    varname1.append("	           WHERE col.FG_ATIVO_COLABORADOR = 1 ");
	    varname1.append("	    		 AND os.ABERTURA > DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE() - 365), 0) ");
	    varname1.append("	    		 AND NOT EXISTS (SELECT 1 ");
	    varname1.append("	                               FROM D_OSSigmaColaboradores os ");
	    varname1.append("	                         INNER JOIN X_SIGMACOLABORADOR x on os.colaborador_neoId = x.neoId ");
	    varname1.append("	                         INNER JOIN [FSOODB03\\SQL01].SIGMA90.DBO.COLABORADOR sc on x.cd_colaborador=sc.cd_colaborador ");
	    varname1.append("	        	                                                                   and col.NM_COLABORADOR=sc.nm_colaborador) ");
	    varname1.append("	    		 AND col.NM_COLABORADOR NOT LIKE '%KHRONOS%' ");
	    varname1.append("	    		 AND col.NM_COLABORADOR NOT LIKE '%QUATENUS%' ");
	    varname1.append("	    		 AND def.DESCRICAODEFEITO NOT LIKE ('OS-RETIRAR EQUIPAMENTO') ");
	    varname1.append("	    		 AND def.DESCRICAODEFEITO NOT LIKE ('OS-DESABILITAR MONITORAMENT') ");
	    varname1.append("	    		 AND os.FECHADO = 0 ");
	    varname1.append("	    		 AND GETDATE() > DATEADD(DAY, DATEDIFF(DAY, 0, os.ABERTURA + 6), 0) ");
	    varname1.append("	    		 AND (os.FECHADO = 0 AND GETDATE() >   DATEADD(DAY, DATEDIFF(DAY, 0, os.ABERTURA + 6), 0) AND (osh.DATAINICIOEXECUCAO IS NULL AND os.DATAAGENDADA IS NULL)) ");
	    varname1.append("	    		 AND osh.DATAINICIOEXECUCAO IS NULL ");
	    varname1.append("	    		 AND os.DATAAGENDADA IS NULL ");
	    varname1.append("	    		 AND cli.TipEmc !=2 ");
	    varname1.append("	    		 AND os.DEFEITO not like'%ethernet.#FOTOCHIP#%' ");
	    varname1.append("	             AND NOT EXISTS(SELECT 1 ");
	    varname1.append("	                              FROM D_SIGMAOSAtrasadaTarefaSimples ds ");
	    varname1.append("	                             WHERE ds.numeroOS = os.ID_ORDEM) ");
	    varname1.append("	        ORDER BY 1, col.NM_COLABORADOR, os.FECHAMENTO DESC, sol.NM_DESCRICAO, os.DATAAGENDADA, os.ABERTURA");
	    
	    pstm = conn.prepareStatement(varname1.toString());
	    rs = pstm.executeQuery();
	    
	    while (rs.next()) {
		
		boolean isRastreamento = false;
		
		String solicitante = "";
		String papelSolicitante = "";
		String papelExecutor = "";
		String tipoOS = rs.getString("TIPOOS");
		String regional = rs.getString("REGIONAL");
		String idOrdem = rs.getString("ID_ORDEM");
		String nmColaborador = rs.getString("NM_COLABORADOR");
		String central = rs.getString("ID_CENTRAL");
		String particao =  rs.getString("PARTICAO");
		String razao = rs.getString("RAZAO");
		String defeito = rs.getString("DEFEITO");
		Integer instalador = rs.getInt("ID_INSTALADOR");
		Integer empresa = rs.getInt("ID_EMPRESA");
		String estadoPosto = rs.getString("ID_ESTADO");
				
		
		String titulo = tipoOS + " - OS: " + idOrdem + " - TEC: " + nmColaborador;
		String tituloAux = "%TEC: " + nmColaborador;

		GregorianCalendar dataInicio = new GregorianCalendar();
		GregorianCalendar dataFim = new GregorianCalendar();

		GregorianCalendar prazo = new GregorianCalendar();
		
		dataInicio.setTime(rs.getTimestamp("ABERTURA"));
		dataFim = OrsegupsUtils.getSpecificWorkDay(dataInicio, 5L);
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
		
		
		papelSolicitante = "SolicitanteTarefaOSEmAtraso5Dias";
		solicitante = OrsegupsUtils.getUserNeoPaper(papelSolicitante);
		
		
		if (dataFim.after(new GregorianCalendar())) {			
		    continue;
		}
		
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);

		String descricao = OrsegupsUtils.getHistoricoTarefaSimples(tituloAux);
		descricao += "<strong><u>Acompanhar, " + tipoOS + ":</u></strong><br><br>";
		descricao = descricao + "<strong>Número OS:</strong> " + idOrdem + " <br>";
		descricao = descricao + " <strong>Cliente :</strong> " + central + "[" + particao + "] " + razao + " <br>";
		descricao = descricao + " <strong>Texto OS :</strong> " + defeito + "<br>";
		descricao = descricao + " <strong>Instalador :</strong> " + nmColaborador + "<br>";
		
		NeoPaper papel = new NeoPaper();

		int idInstalador = instalador;

		int idEmpresa = empresa;
		
		Long lReg = 0L;
		
		switch(regional){
		    case "SOO":
			lReg = 1L;
		    break;
		    case "IAI":
			lReg = 2L;
			break;
		    case "BQE":
			lReg = 3L;
			break;
		    case "BNU":
			lReg = 4L;
			break;
		    case "JLE":
			lReg = 5L;
			break;
		    case "LGS":
			lReg = 6L;
			break;
		    case "CUA":
			lReg = 7L;
			break;
		    case "GPR":
			lReg = 8L;
			break;
		    case "CCO":
			lReg = 10L;
			break;
		    case "RSL":
			lReg = 11L;
			break;
		    case "JGS":
			lReg = 12L;
			break;
		    case "CTA":
			lReg = 13L;
			break;
		    case "CSC":
			lReg = 14L;
			break;
		    case "TRO":
			lReg = 16L;
			break;
		    case "NHO":
			lReg = 17L;
			break;
		    case "TRI":
			lReg = 18L;
			break;
		    case "CAS":
			lReg = 19L;
			break;
		    case "GNA":
			lReg = 20L;
			break;
		    case "PMJ":
			lReg = 21L;
			break;
		    case "SRR":
			lReg = 22L;
			break;
		    case "XLN":
			lReg = 23L;
			break;
		    
		}	
		
		papelExecutor = "ExecutorTarefaOSEmAtraso5Dias";
		String executor = OrsegupsUtils.getUserNeoPaper(papelExecutor);

		if (idInstalador == 89642) { 
		    papel = OrsegupsUtils.getPaper("TratarOSSupervisorJLE");
		} else if (idEmpresa == 10127 || idEmpresa == 10075 || central.startsWith("R")) {
		    solicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteOSAtrasoRastreamento");
		    papel = OrsegupsUtils.getPaper("TratarOSAtrasoRastreamento");
		    isRastreamento = true;
		} else if (lReg != null && !lReg.equals(13L)) {
		    if (nmColaborador != null && !nmColaborador.isEmpty() && nmColaborador.contains("TEC") && nmColaborador.contains("ITH")) {
			papel = OrsegupsUtils.getPaper("TratarOSAtrasoEscritorioRegionalITH");
		    } else {			
			papel = OrsegupsUtils.getPapelTratarOsEmAtraso(lReg);
		    }
		} else if (lReg != null && lReg.equals(13L)) {
		    if (nmColaborador != null && !nmColaborador.isEmpty() && nmColaborador.contains("CORP")) {
			    papel = OrsegupsUtils.getPaper("TratarOSAtrasoEscritorioRegionalCTACORP");
			} else {
			    papel = OrsegupsUtils.getPapelTratarOsEmAtraso(lReg);

			}
		}
		
		// TODO CLIENTES CORPORATIVOS - TAREFA 1409491
		if (estadoPosto != null && razao != null && isRastreamento == false) {
		    if (estadoPosto.equals("BA") || estadoPosto.equals("TO") || estadoPosto.equals("GO") || estadoPosto.equals("BA") || estadoPosto.equals("AL") || estadoPosto.equals("CE") || estadoPosto.equals("MA") || estadoPosto.equals("PA") || estadoPosto.equals("PB")
			    || estadoPosto.equals("PE") || estadoPosto.equals("PI")) {

			if (razao.contains("RAIA DROGASIL") || razao.contains("LOJAS RIACHUELO") || razao.contains("BK BRASIL") || razao.contains("SHERWIN WILLIAMS")) {
			    papel = OrsegupsUtils.getPaper("TratarOSAtrasoClienteCorporativo");
			}
		    }
		}

		InstantiableEntityInfo osInfo = AdapterUtils.getInstantiableEntityInfo("SIGMAOSAtrasadaTarefaSimples");
		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples(); 
		if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty()) {
		    for (NeoUser user : papel.getUsers()) {
			executor = user.getCode();
			break;
		    }

		} else {
		    System.out.println("##### RotinaAbreTarefaSimplesOrdemServicoEmAtrasoPor5DiasNovo - Papel sem usuário definido - Regional: " + regional + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		
		String retorno = "";		
		
		retorno = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);
		//retorno = iniciarTarefaSimples.abrirTarefa("lucas.alison", "lucas.alison", titulo, descricao, "1", "sim", prazo);
		System.out.println("solicitante:"+solicitante+", executor:"+executor+", titulo="+titulo+" prazo:"+prazo); 
		
		if (retorno != null && (!retorno.trim().contains("Erro"))) {
		    Long nuOS = Long.parseLong(idOrdem);
		    NeoObject objOS = osInfo.createNewInstance();
		    EntityWrapper wrpOS = new EntityWrapper(objOS);
		    wrpOS.findField("numeroOS").setValue(nuOS);
		    wrpOS.findField("numeroTarefa").setValue(Long.parseLong(retorno));
		    wrpOS.findField("dataCadastro").setValue(new GregorianCalendar());

		    PersistEngine.persist(objOS);

		} else {
		    System.out.println(" Erro ao criar tarefa Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	    }
	    
	    System.out.println("### FINALIZOU JOB RotinaAbreTarefaSimplesOrdemServicoEmAtrasoPor5DiasNovo");
	    
	} catch (Exception e) {
	    System.out.println("### ERRO no JOB RotinaAbreTarefaSimplesOrdemServicoEmAtrasoPor5DiasNovo");
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	}finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);

	}
    }
}
