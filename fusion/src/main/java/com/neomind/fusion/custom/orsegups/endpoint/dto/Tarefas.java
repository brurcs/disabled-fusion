package com.neomind.fusion.custom.orsegups.endpoint.dto;

import java.util.Date;
import java.util.List;

/**
 * Classe que contém os campos necessários para a criação de uma tarefa do
 * cftv-inteligente
 * 
 * @author jaime.gomes
 *
 */
public class Tarefas {

    private String cdCliente;
    private Date dataCriacao;
    private String numTarefa;
    private List<Imagens> imagens;
    

    /**
     * @return the cdCliente
     */
    public String getCdCliente() {
	return cdCliente;
    }

    /**
     * @param cdCliente
     *            the cdCliente to set
     */
    public void setCdCliente(String cdCliente) {
	this.cdCliente = cdCliente;
    }

    /**
     * @return the numTarefa
     */
    public String getNumTarefa() {
	return numTarefa;
    }

    /**
     * @param numTarefa
     *            the numTarefa to set
     */
    public void setNumTarefa(String numTarefa) {
	this.numTarefa = numTarefa;
    }

    /**
     * @return the imagens
     */
    public List<Imagens> getImagens() {
	return imagens;
    }

    /**
     * @param imagens
     *            the imagens to set
     */
    public void setImagens(List<Imagens> imagens) {
	this.imagens = imagens;
    }

    /**
     * @return the dataCriacao
     */
    public Date getDataCriacao() {
	return dataCriacao;
    }

    /**
     * @param dataCriacao
     *            the dataCriacao to set
     */
    public void setDataCriacao(Date dataCriacao) {
	this.dataCriacao = dataCriacao;
    }

}
