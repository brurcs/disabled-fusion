package com.neomind.fusion.custom.orsegups.teste;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoCalendarUtils;

public class DefineObservacaoAdapter implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		//Busca de valores em formulário
		Long codigo = (Long) processEntity.findValue("Codigo");
		String descricao = (String) processEntity.findValue("Descricao");
		
		String posto = (String) processEntity.findValue("centroDeCusto.nivel8.nomloc");
		GregorianCalendar  dataAtual = new GregorianCalendar();
		String dataFormatada = NeoCalendarUtils.dateToString(dataAtual);
		String produtoObs = codigo + " - " + descricao + " - " + posto + " - " + dataFormatada;

		//Setando valores
		processEntity.setValue("observacao", produtoObs);

		//Eform Sistema
		String texto = (String) processEntity.findValue("Login");
		QLEqualsFilter filtroLogin = new QLEqualsFilter("code", texto);
		NeoUser user = PersistEngine.getObject(NeoUser.class, filtroLogin);
		processEntity.setValue("Responsavel", user);

		//Eform Dinamico
		EntityWrapper userWrapper = new EntityWrapper(user);
		Long codigoRegional = (Long) userWrapper.findValue("escritorioRegional.codigo");
		QLEqualsFilter filtroRegional = new QLEqualsFilter("codigo", codigoRegional);
		NeoObject regional = PersistEngine.getObject(AdapterUtils.getEntityClass("GCEscritorioRegional"), filtroRegional);
		processEntity.setValue("Regional", regional);
		
		//Eform Externo
		Long codigoPais = (Long) processEntity.findValue("Pais.codpai");
		String codigoEstado = (String) processEntity.findValue("Estado.codest");
		
		QLEqualsFilter filtroPais = new QLEqualsFilter("codpai",codigoPais);
		QLEqualsFilter filtroEstado = new QLEqualsFilter("codest", codigoEstado);
		
		QLGroupFilter filtroCidade = new QLGroupFilter("AND");
		filtroCidade.addFilter(filtroPais);
		filtroCidade.addFilter(filtroEstado);
		
		List<NeoObject> cidades = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORH_R074CID"),filtroCidade);
		//Setando valores
		processEntity.setValue("Cidade", cidades);
		
		GregorianCalendar gc = new GregorianCalendar();
		
		QLOpFilter datefilterini = new QLOpFilter("startDate", ">=", (GregorianCalendar) gc);
		QLOpFilter datefilterfim = new QLOpFilter("startDate", "<=", (GregorianCalendar) gc);
		  
		QLGroupFilter filtroMnc = new QLGroupFilter("AND");
		
		filtroMnc.addFilter(datefilterini);
		filtroMnc.addFilter(datefilterfim);
		  
		List<NeoObject> listMnc = PersistEngine.getObjects(AdapterUtils.getEntityClass("MNCProcessoMovimentoNumerarioCobertura"), filtroMnc);  
		
		QLRawFilter rawFilter = new QLRawFilter("pais.codpai = " + codigoPais + " and estado.codest = " + codigoEstado);
		List<NeoObject> cidades2 = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORH_R074CID"),rawFilter);
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}

}
