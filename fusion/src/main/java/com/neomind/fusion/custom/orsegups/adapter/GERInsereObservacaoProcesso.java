package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class GERInsereObservacaoProcesso implements AdapterInterface
{

	private static final Log log = LogFactory.getLog(GERInsereObservacaoProcesso.class);


	public void back(EntityWrapper entitywrapper, Activity activity1)
	{
	}

	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		List<NeoObject> listaTitulos = (List<NeoObject>) processEntity.findField("listaTitulos").getValue();
		
		if (listaTitulos != null && !listaTitulos.isEmpty()) {			
			try	{
				GregorianCalendar dataAux = new GregorianCalendar();
				GregorianCalendar data = new GregorianCalendar( dataAux.get(GregorianCalendar.YEAR), dataAux.get(GregorianCalendar.MONTH), dataAux.get(GregorianCalendar.DAY_OF_MONTH));
				Time hora = new Time(dataAux.get(GregorianCalendar.HOUR_OF_DAY), dataAux.get(GregorianCalendar.MINUTE), dataAux.get(GregorianCalendar.MILLISECOND));
				Long horaInt = (hora.getHours() * 60L ) + hora.getMinutes();
				
				String texto = "Iniciado o Processo " + origin.getProcessName() + " - Tarefa: " + origin.getCode();
				
				Long codigoUsuarioSapiens = 100L; //sapienssid
				
				Long ocorrencia = 22L; //ABERTURA DE PROCESSO
				insereObservacaoTituloSapiens(listaTitulos, data, horaInt, texto, codigoUsuarioSapiens, ocorrencia);
			} catch (WorkflowException we)	{
				throw we;
			} catch (Exception e) {
				e.printStackTrace();
				throw new WorkflowException(e.getMessage());
			}						
		}
	}

	@SuppressWarnings("unchecked")
	public void insereObservacaoTituloSapiens(List<NeoObject> listaTitulos, GregorianCalendar data, Long hora, String texto, Long usuario, Long ocorrencia) throws Exception
	{
		for (NeoObject titulo : listaTitulos) {
			EntityWrapper wrapper = new EntityWrapper(titulo);
			Long empresa = (Long) wrapper.findValue("empresa");
			Long filial = (Long) wrapper.findValue("filial");
			String numeroTitulo = (String) wrapper.findValue("numeroTitulo");
			String tipoTitulo = (String) wrapper.findValue("tipoTitulo");
			Integer sequencial = 1;
			
			String nomeFonteDados = "SAPIENS";
			
			String sqlSelect = "SELECT (CASE WHEN max(seqmov) > 0 THEN MAX(seqmov)+1 ELSE 1 END) FROM E301MOR WHERE codemp = :codemp AND codfil = :codfil AND numtit = :numtit AND codtpt = :codtpt ";
			
			Query query = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sqlSelect);
			query.setParameter("codemp", empresa);
			query.setParameter("codfil", filial);
			query.setParameter("numtit", numeroTitulo);
			query.setParameter("codtpt", tipoTitulo);
			List<Object> resultList = query.getResultList();
			
			if(resultList != null && !resultList.isEmpty())	{
				sequencial = (Integer)resultList.get(0);
			}	
			
			Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
			
			String sql = "INSERT INTO E301MOR (CodEmp, CodFil, NumTit, CodTpt, SeqMov, TipObs, ObsTit, UsuMov, DatMov, HorMov, USU_CodOcr) ";
			sql += "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
			PreparedStatement st = null;
			try
			{	
				st = connection.prepareStatement(sql);
				st.setLong(1, empresa);
				st.setLong(2, filial);
				st.setString(3, numeroTitulo);
				st.setString(4, tipoTitulo);
				st.setInt(5, sequencial);
				st.setString(6, "A");
				st.setString(7, texto);
				st.setLong(8, usuario);
				st.setTimestamp(9, new Timestamp(data.getTimeInMillis()));
				st.setLong(10, hora);
				st.setLong(11, ocorrencia);
				
				st.executeUpdate();
			}
			catch (SQLException e)
			{
				throw e;
			}
			finally
			{
				if (st != null)
				{
					try
					{
						st.close();
						connection.close();
					}
					catch (SQLException e)
					{
						log.error("Erro ao fechar o statement");
						e.printStackTrace();
						throw e;
					}
				}
			}

		}

	}
}
