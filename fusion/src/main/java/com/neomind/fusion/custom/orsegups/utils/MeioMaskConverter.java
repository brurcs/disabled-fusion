package com.neomind.fusion.custom.orsegups.utils;

import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.DefaultConverter;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.util.NeoUtils;

/**
 * MeioMaskConverter - Permite utilizar o plugin MeioMask em campos do tipo texto.
 * Implementar validação (Valor Incorreto)
 * @author Otávio
 *
 */
public class MeioMaskConverter extends DefaultConverter{

	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin) 
	{
		StringBuilder outBuilder = new StringBuilder();
		
		String mask = field.getFieldInfo().getMask();
		
		if(mask.contains("mask:"))
			mask = "{" + (mask) + "}";
		else
			mask = "'" + (mask) + "'";
		
		outBuilder.append("<script type='text/javascript'>");
		outBuilder.append("$(document).ready(function() {");
		outBuilder.append("$('#" + field.getFieldId() + "').setMask(" + mask + ");");
		outBuilder.append("});");
		outBuilder.append("</script>");
		
		outBuilder.append(super.getHTMLInput(field, origin));
		
		return outBuilder.toString();
	}
	
	@Override
	// otavio.campos - Pode suportar qualquer tipo de campo, mas a mascara tem que ser retirada do valor aqui.
	public Object getValueFromHTMLString(EFormField field, Object value)
	{
		String valor = NeoUtils.safeOutputString(value).replace("(", "");
		valor = valor.replace(")", "");
		valor = valor.replace(".", "");
		valor = valor.replace("-", "");
		valor = valor.replace(" ", "");
		valor = valor.replace("/", "");
		return valor;
	}
	
}
