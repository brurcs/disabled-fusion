package com.neomind.fusion.custom.orsegups.processoJuridico.adapter;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.ConverterShowHistoricoGTC
public class ConverterShowHistoricoGTC extends StringConverter
{
	@Override
	@SuppressWarnings("unchecked")
	public String getHTMLInput(EFormField field, OriginEnum origin)
	{
		StringBuilder outBuilder = new StringBuilder();
		NeoObject objPrincipal = field.getForm().getObject();
		EntityWrapper wPrincipal = new EntityWrapper(objPrincipal);

		List<NeoObject> historicoTCR = (List<NeoObject>) wPrincipal.getValue("j002HistoricoGTCs");

		if (historicoTCR != null && historicoTCR.size() > 0)
		{
			outBuilder.append("	<table  class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">");
			outBuilder.append("			<tr style=\"cursor: auto\">");
			outBuilder.append("				<th style=\"cursor: auto; white-space: normal\">Tipo Pagamento</th>");
			outBuilder.append("				<th style=\"cursor: auto\">Valor do Título</th>");
			outBuilder.append("				<th style=\"cursor: auto\">Data Vencimento</th>");
			outBuilder.append("				<th style=\"cursor: auto\">Anexo</th>");
			outBuilder.append("				<th style=\"cursor: auto\">Comprovante Pgto</th>");
			outBuilder.append("				<th style=\"cursor: auto\">Situação</th>");
			outBuilder.append("			</tr>");
			outBuilder.append("		<tbody>");

			for (NeoObject registro : historicoTCR)
			{
				EntityWrapper wRegistro = new EntityWrapper(registro);

				String status = String.valueOf(wRegistro.getValue("StatusGTC"));
				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				long lNeoId = Long.parseLong(String.valueOf(wRegistro.getValue("neoIdGtc")));
				QLFilter qlNeoId = new QLEqualsFilter("neoId", lNeoId);
				groupFilter.addFilter(qlNeoId);

				List<NeoObject> objsHistorico = PersistEngine.getObjects(AdapterUtils.getEntityClass("GTCGestaoTitulosControladoria"), groupFilter);
				if (objsHistorico != null && objsHistorico.size() > 0){
				    
				    EntityWrapper wRegistroHistorico = new EntityWrapper(objsHistorico.get(0));

				    NeoObject obsClass = (NeoObject) wRegistroHistorico.getValue("j002ClaPgtos");
				    if (obsClass != null)
				    {
					EntityWrapper wClaPgto = new EntityWrapper(obsClass);

					outBuilder.append("		<tr>");

					if (wClaPgto.getValue("descricao") != null)
					{
					    outBuilder.append("			<td> " + wClaPgto.getValue("descricao") + " </td>");
					}
					else
					{
					    outBuilder.append("			<td> Outros </td>");
					}
				    }
				    else
				    {
					outBuilder.append("			<td> Outros </td>");
				    }

				    if (wRegistroHistorico.getValue("valorOriginal") != null)
				    {
					outBuilder.append("			<td> R$ " + wRegistroHistorico.getValue("valorOriginal") + " </td>");
				    }
				    else
				    {
					outBuilder.append("			<td> R$ 0.00 </td>");
				    }

				    GregorianCalendar vencimento = (GregorianCalendar) wRegistroHistorico.getValue("vencimentoOriginal");

				    if (vencimento != null)
				    {
					outBuilder.append("			<td> " + NeoDateUtils.safeDateFormat(vencimento, "dd/MM/yyyy") + "</td>");
				    }
				    else
				    {
					outBuilder.append("			<td> </td>");
				    }

				    if (wRegistroHistorico.getValue("j002Anexo") != null)
				    {
					NeoFile file = (NeoFile) wRegistroHistorico.getValue("j002Anexo");
					outBuilder.append("	<td><a style=\"cursor:pointer;\" target='_blank' href=\"" + PortalUtil.getBaseURL() + "file/download/" + file.getNeoId() + "\"'; ");
					outBuilder.append("			><img src=\"imagens/icones_final/document_search_16x16-trans.png\" align=\"absmiddle\" alt=" + file.getName() + "> ");
					outBuilder.append("			" + file.getName());
					outBuilder.append("</a></td>");
				    }
				    else
				    {
					outBuilder.append("			<td> </td>");
				    }

				    if (wRegistroHistorico.getValue("comprovante") != null)
				    {
					NeoFile file = (NeoFile) wRegistroHistorico.getValue("comprovante");
					outBuilder.append("	<td><a style=\"cursor:pointer;\" target='_blank' href=\"" + PortalUtil.getBaseURL() + "file/download/" + file.getNeoId() + "\"'; ");
					outBuilder.append("			><img src=\"imagens/icones_final/document_search_16x16-trans.png\" align=\"absmiddle\" alt=" + file.getName() + "> ");
					outBuilder.append("			" + file.getName());
					outBuilder.append("</a></td>");
				    }
				    else
				    {
					outBuilder.append("			<td> </td>");
				    }
				    outBuilder.append("			<td> " + status + "</td>");

				    outBuilder.append("		</tr>");
				}
			}

			outBuilder.append("		</tbody>");
			outBuilder.append("	</table>");
		}

		return outBuilder.toString();

	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}
}
