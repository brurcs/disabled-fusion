package com.neomind.fusion.custom.casvig.adapters;

import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.entity.dynamic.EntityCloner;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.ProcessState;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class CasvigGestaoUniformes implements AdapterInterface
{

	private static final Log log = LogFactory.getLog(CasvigGestaoUniformes.class);

	public void back(EntityWrapper entitywrapper, Activity activity1)
	{
	}

	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		log.warn("INICIAR PROCEDIMENTO GESTÃO DE UNIFORMES -------  CasvigGestaoUniformes!");

		NeoObject colaborador = (NeoObject) processEntity.findField("colaborador").getValue();

		InstantiableEntityInfo infoFichaUniforme = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("FCEFichaUniformeEpi");
		log.warn("Verificando se existe ficha de uniforme cadastrada para colaborador");

		NeoObject ficha = (NeoObject) PersistEngine.getObject(infoFichaUniforme.getEntityClass(), new QLEqualsFilter("colaboradorRH", colaborador));
		log.warn("Verificacao de ficha de uniforme para colaborador concluida");

		Boolean inclusaoFicha = false;
		
		if (ficha != null)
		{
			System.out.println("Ficha != Null");

			QLEqualsFilter fichaFilter = new QLEqualsFilter("fichaUniformeEPI", ficha);
			QLEqualsFilter processoFilter = new QLEqualsFilter("wfprocess.processState", ProcessState.running.ordinal());
			QLEqualsFilter savedFilter = new QLEqualsFilter("wfprocess.saved", true);
			
			
			QLGroupFilter groupFilter = new QLGroupFilter("AND");
			groupFilter.addFilter(fichaFilter);
			groupFilter.addFilter(processoFilter);
			groupFilter.addFilter(savedFilter);
			
			List<NeoObject> eformsProcesso = PersistEngine.getObjects(AdapterUtils.getEntityClass("FUEProcessoFichaUniformeEPI"), groupFilter);
			
			if(eformsProcesso != null && eformsProcesso.size() > 0){
				NeoObject eformEPIObject = eformsProcesso.get(0);
				EntityWrapper eformEPIWrapper = new EntityWrapper(eformEPIObject);
				
				String processCode = (String) eformEPIWrapper.findValue("wfprocess.code");
				String requester = (String)eformEPIWrapper.findValue("wfprocess.requester.fullName");
				String erro = "Já existe a solicitação número '"+processCode+" aberta pelo colaborador '"+requester+"' do processo de Manutenção Ficha Uniforme e EPI para este colaborador. Finalize esta solicitação antes de abrir uma nova.";
				throw new WorkflowException(erro);
			}
			
			processEntity.setValue("controleFicha", "fluxo1");
			

			EntityWrapper ewFicha = new EntityWrapper(ficha);
			ewFicha.setValue("DevolucaoTotal", false);
			ewFicha.setValue("descontoTotalFolha", false);
			

			processEntity.findField("fichaUniformeEPI").setValue(ficha);

			defineHistorico(processEntity);

			log.info("Seguir fluxo 1: Colaborador com Ficha de Uniforme");

		}
		else
		{
			System.out.println("Ficha == Null");

			processEntity.setValue("controleFicha", "fluxo2");
			NeoObject noFicha = (NeoObject) infoFichaUniforme.createNewInstance();
			EntityWrapper ewFicha = new EntityWrapper(noFicha);
			ewFicha.findField("colaboradorRH").setValue(colaborador);
			ewFicha.setValue("DevolucaoTotal", false);
			processEntity.findField("fichaUniformeEPI").setValue(noFicha);

			inclusaoFicha = true;
			log.info("Seguir fluxo 2: Colaborador sem Ficha de Uniforme");
		}
		
		processEntity.setValue("inclusaoFicha", inclusaoFicha);

		NeoObject definicaoDepositos = null;
		NeoUser solicitante = PortalUtil.getCurrentUser();
		
		// valida os depositos de entrega e devolucao do usuario logado - 
		//busca primeiro no eform  FUELigacaoUsuarioDeposito caso nao ache nele busca o deposito padrao da regional a qual o usuario pertence 
		QLEqualsFilter equalsFilter = new QLEqualsFilter("usuarioFusion", solicitante);
		List<NeoObject> ligacoesUsuarioDeposito = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("FUELigacaoUsuarioDeposito"), equalsFilter);
		if(ligacoesUsuarioDeposito != null && ligacoesUsuarioDeposito.size() > 0)
		{
			NeoObject ligacaoUsuarioDeposito = ligacoesUsuarioDeposito.get(0);
			EntityWrapper ligacaoUsuarioDepositoWrapper = new EntityWrapper(ligacaoUsuarioDeposito);
			if(ligacaoUsuarioDepositoWrapper != null){
				definicaoDepositos = (NeoObject) ligacaoUsuarioDepositoWrapper.findValue("definicaoDeposito");
			}
		}else{
			EntityWrapper solicitanteWrapper = new EntityWrapper(solicitante);
			definicaoDepositos = (NeoObject) solicitanteWrapper.findValue("escritorioRegional.DefinicaoDepositoEntregaDevolucao");
		}

		if (definicaoDepositos != null)
		{
			EntityWrapper definicaoDepositosEW = new EntityWrapper(definicaoDepositos);

			NeoObject depositoEntrega = (NeoObject) definicaoDepositosEW.findValue("DepositoEntrega");
			NeoObject depositoDevolucao = (NeoObject) definicaoDepositosEW.findValue("DepositoDevolucao");

			NeoObject fichaUniformeEPI = (NeoObject) processEntity.findValue("fichaUniformeEPI");
			EntityWrapper fichaUniformeEPIEW = new EntityWrapper(fichaUniformeEPI);

			fichaUniformeEPIEW.setValue("DepositoEntrega", depositoEntrega);
			fichaUniformeEPIEW.setValue("DepositoDevolucao", depositoDevolucao);
		}else{
			String erro = "Depósitos de entrega/devolução da regional do usuário "+solicitante.getFullName()+ "não cadastrados.";
			throw new WorkflowException(erro);
		}
		
		// valida se o usuario pode realizar uma entrega ou devolucao sem integrar com o sapiens
		Boolean permiteNaoIntegrar = false;
		QLEqualsFilter filter = new QLEqualsFilter("Usuario", solicitante);
		Collection<NeoObject> permiteNaoIntegrarList = PersistEngine.getObjects(AdapterUtils.getEntityClass("FUEPermissaodeNaoIntegrar"), filter);
		
		if(permiteNaoIntegrarList != null && permiteNaoIntegrarList.size() > 0)
		{
			permiteNaoIntegrar = true;
		}
		
		processEntity.setValue("permiteNaoIntegrar", permiteNaoIntegrar);
		processEntity.setValue("IntegraComSapiens", true);
		processEntity.setValue("erro", false);
		
		log.warn("FINALIZAR PROCEDIMENTO GESTÃO DE UNIFORMES -------  CasvigGestaoUniformes!");
	}

	public void defineHistorico(EntityWrapper processEntity)
	{
		//Se já existe histórico é porque itens já foram persistidos no Sapiens, neste caso copia integralmente os itens de histórico para 
		//a lista de Entregas

		List<NeoObject> listaEntregasEmAberto = (List<NeoObject>) processEntity.findField("fichaUniformeEPI.listaEntregas").getValue();
		List<NeoObject> listaEntregasFinalizadas = (List<NeoObject>) processEntity.findField("fichaUniformeEPI.listaEntregasFinalizadas").getValue();
		List<NeoObject> listaHistorico = (List<NeoObject>) processEntity.findField("fichaUniformeEPI.historicoEntregas").getValue();

		//Se histórico não é vazio, deve ser clonado para a lista de entregas
		if (listaHistorico != null && !listaHistorico.isEmpty())
		{
			/* Limpa a lista de entregas em aberto */
			if (listaEntregasEmAberto != null)
			{
				int tamanho = listaEntregasEmAberto.size();
				for (int i = 1; i < tamanho + 1; i++)
				{
					NeoObject entrega = listaEntregasEmAberto.get(tamanho - i);
					processEntity.findField("fichaUniformeEPI.listaEntregas").removeValue(entrega);
				}
			}

			/* Limpa a lista de entregas finalizadas */
			if (listaEntregasFinalizadas != null)
			{
				int tamanho = listaEntregasFinalizadas.size();
				for (int i = 1; i < tamanho + 1; i++)
				{
					NeoObject entrega = listaEntregasFinalizadas.get(tamanho - i);
					processEntity.findField("fichaUniformeEPI.listaEntregasFinalizadas").removeValue(entrega);
				}
			}

			/* Copia o histórico para a lista de entregas */
			for (NeoObject entregaHistorico : listaHistorico)
			{
				EntityWrapper wrapperEntrega = new EntityWrapper(entregaHistorico);
				InstantiableEntityInfo infoListaEntregas = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("FUEListaEntregas");
				NeoObject cloneNoEntrega = infoListaEntregas.createNewInstance();

				EntityWrapper wrapperCloneEntrega = new EntityWrapper(cloneNoEntrega);

				/* Define os valores padrões que não são clonados */
				wrapperCloneEntrega.findField("depositoEntrega").setValue(wrapperEntrega.findField("depositoEntrega").getValue());
				wrapperCloneEntrega.findField("produto").setValue(wrapperEntrega.findField("produto").getValue());
				wrapperCloneEntrega.findField("derivacao").setValue(wrapperEntrega.findField("derivacao").getValue());
				wrapperCloneEntrega.findField("observacao").setValue(wrapperEntrega.findField("observacao").getValue().toString());
				wrapperCloneEntrega.findField("quantidadeEntregue").setValue((Long) wrapperEntrega.findField("quantidadeEntregue").getValue());
				wrapperCloneEntrega.findField("codigoEntrega").setValue(wrapperEntrega.findField("codigoEntrega").getValue());
				wrapperCloneEntrega.findField("dataEntrega").setValue(wrapperEntrega.findField("dataEntrega").getValue());
				wrapperCloneEntrega.findField("usuario").setValue(wrapperEntrega.findField("usuario").getValue());
				wrapperCloneEntrega.findField("solicitarCompra").setValue(wrapperEntrega.findValue("solicitarCompra"));
				wrapperCloneEntrega.findField("codigoSapiens").setValue(wrapperEntrega.findValue("codigoSapiens"));

				List<NeoObject> listaDevolucoes = (List<NeoObject>) wrapperEntrega.findField("listaDevolucoes").getValue();

				Long entregue = (Long) wrapperEntrega.findField("quantidadeEntregue").getValue();
				Long devolvido = 0L;

				for (NeoObject devolucao : listaDevolucoes)
				{
					NeoObject cloneDevolucao = EntityCloner.cloneNeoObject(devolucao);

					EntityWrapper devolucaoWrapper = new EntityWrapper(devolucao);

					devolucaoWrapper.findField("exibeRelatorio").setValue(false);
					
					if (devolucaoWrapper.findValue("quantidadeDevolucao") != null)
					{
						devolvido += (Long) devolucaoWrapper.findValue("quantidadeDevolucao");
					}

					wrapperCloneEntrega.findField("listaDevolucoes").addValue(cloneDevolucao);
				}
				wrapperCloneEntrega.findField("entregaIntegrada").setValue(true);
				wrapperCloneEntrega.findField("exibeRelatorio").setValue(false);
				//wrapperCloneEntrega.setValue("solicitarCompra", false);

				if (devolvido < entregue)
				{
					wrapperCloneEntrega.setValue("descontoTotalItemFolha", false);
					wrapperCloneEntrega.setValue("DevolucaoTotal", false);
					
					processEntity.findField("fichaUniformeEPI.listaEntregas").addValue(cloneNoEntrega);
				}
				else
				{
					processEntity.findField("fichaUniformeEPI.listaEntregasFinalizadas").addValue(cloneNoEntrega);
				}
			}
		}
	}
}
