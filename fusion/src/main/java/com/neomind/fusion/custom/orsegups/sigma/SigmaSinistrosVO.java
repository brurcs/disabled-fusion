package com.neomind.fusion.custom.orsegups.sigma;

import java.io.Serializable;
import java.util.Date;


public class SigmaSinistrosVO implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String codigo;
	private String empresaNomeFantasia;
	private String nomeCidade;
	private String nomeCentral;
	private String centralRazaoSocial;
	private String responsavelCentral;
	private Date dataSinistro;
	private Date dataDeslocamentoViatura;
	private Date dataViaturaLocal;
	private String placaViatura;
	private String centralArmada;
	private String obsFechamento;
	private String obsGerente;
	private String regional;
	private String codigoEvento;
	private String fraseEvento;
	private String cgccpf;
	private String endereco;
	private String telefone;
	private String isClientePGO;
	
	public String getCodigo()
	{
		return codigo;
	}
	public void setCodigo(String codigo)
	{
		this.codigo = codigo;
	}
	public String getEmpresaNomeFantasia()
	{
		return empresaNomeFantasia;
	}
	public void setEmpresaNomeFantasia(String empresaNomeFantasia)
	{
		this.empresaNomeFantasia = empresaNomeFantasia;
	}
	public String getNomeCidade()
	{
		return nomeCidade;
	}
	public void setNomeCidade(String nomeCidade)
	{
		this.nomeCidade = nomeCidade;
	}
	public String getNomeCentral()
	{
		return nomeCentral;
	}
	public void setNomeCentral(String nomeCentral)
	{
		this.nomeCentral = nomeCentral;
	}
	public String getCentralRazaoSocial()
	{
		return centralRazaoSocial;
	}
	public void setCentralRazaoSocial(String centralRazaoSocial)
	{
		this.centralRazaoSocial = centralRazaoSocial;
	}
	public String getResponsavelCentral()
	{
		return responsavelCentral;
	}
	public void setResponsavelCentral(String responsavelCentral)
	{
		this.responsavelCentral = responsavelCentral;
	}
	public Date getDataSinistro()
	{
		return dataSinistro;
	}
	public void setDataSinistro(Date dataSinistro)
	{
		this.dataSinistro = dataSinistro;
	}
	public Date getDataDeslocamentoViatura()
	{
		return dataDeslocamentoViatura;
	}
	public void setDataDeslocamentoViatura(Date dataDeslocamentoViatura)
	{
		this.dataDeslocamentoViatura = dataDeslocamentoViatura;
	}
	public Date getDataViaturaLocal()
	{
		return dataViaturaLocal;
	}
	public void setDataViaturaLocal(Date dataViaturaLocal)
	{
		this.dataViaturaLocal = dataViaturaLocal;
	}
	public String getPlacaViatura()
	{
		return placaViatura;
	}
	public void setPlacaViatura(String placaViatura)
	{
		this.placaViatura = placaViatura;
	}
	public String getCentralArmada()
	{
		return centralArmada;
	}
	public void setCentralArmada(String centralArmada)
	{
		this.centralArmada = centralArmada;
	}
	public String getObsFechamento()
	{
		return obsFechamento;
	}
	public void setObsFechamento(String obsFechamento)
	{
		this.obsFechamento = obsFechamento;
	}
	public String getRegional()
	{
		return regional;
	}
	public void setRegional(String regional)
	{
		this.regional = regional;
	}
	public String getCodigoEvento()
	{
		return codigoEvento;
	}
	public void setCodigoEvento(String codigoEvento)
	{
		this.codigoEvento = codigoEvento;
	}
	public String getObsGerente()
	{
		return obsGerente;
	}
	public void setObsGerente(String obsGerente)
	{
		this.obsGerente = obsGerente;
	}
	public String getFraseEvento() {
	    return fraseEvento;
	}
	public void setFraseEvento(String fraseEvento) {
	    this.fraseEvento = fraseEvento;
	}
	public String getCgccpf() {
	    return cgccpf;
	}
	public void setCgccpf(String cgccpf) {
	    this.cgccpf = cgccpf;
	}
	public String getEndereco() {
	    return endereco;
	}
	public void setEndereco(String endereco) {
	    this.endereco = endereco;
	}
	public String getTelefone() {
	    return telefone;
	}
	public void setTelefone(String telefone) {
	    this.telefone = telefone;
	}
	public String getIsClientePGO() {
	    return isClientePGO;
	}
	public void setIsClientePGO(String isClientePGO) {
	    this.isClientePGO = isClientePGO;
	}
}