package com.neomind.fusion.custom.orsegups.ti.cancelamentoTarefa.engine;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.common.base.Joiner;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.e2doc.engine.E2docEngine;
import com.neomind.fusion.custom.orsegups.ti.paineljobs.bean.PainelJobBean;
import com.neomind.fusion.custom.orsegups.ti.paineljobs.bean.PainelJobErrosBean;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.runtime.RuntimeEngine;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;


public class CancelamentoTarefaContratoEngine {

    private static final Log log = LogFactory.getLog(E2docEngine.class);
    
    public CancelamentoTarefaContratoEngine() {
    }

    public String cancelaProcessos(String code, String motivo){
//	String xCodes[] = codes.split(",");
//	String strCodes = "'000000'";
//	
//	for (String x: xCodes){
//		strCodes += ",'"+x+"'";
//	}
	
	EntityManager entity = PersistEngine.getEntityManager();
	Integer cont = 1;
	if(entity != null)
	{
        	String query = "SELECT MAX(neoId) FROM ProcessModel WITH(NOLOCK) WHERE name = 'Fluxo Contratos'";
        	
        	Query q = entity.createNativeQuery(query);
        	List<BigInteger> model = (List<BigInteger>) q.getResultList();
        	
        	query = " select neoId from wfprocess   where model_neoid in (select neoId from processmodel pm  where pm.name = (select distinct(pm1.name) from processmodel pm1 where pm1.neoId = "+model.get(0)+" ) ) and code in  " 
        	+ "("+code+")";
	
	
		q = entity.createNativeQuery(query);
		List<BigInteger> result = (List<BigInteger>) q.getResultList();
		if (result != null && result.size() > 0){
		    for (BigInteger i : result)
		    {
			Long id = i.longValue();
			
			/**
			 * FIXME Cancelar tarefas.
			 */
			WFProcess proc = PersistEngine.getObject(WFProcess.class, new QLEqualsFilter("neoId", id));
			
			if (proc != null)
			{
			    RuntimeEngine.getProcessService().cancel(proc, motivo);
			    System.out.println("[Cancelar Tarefa] - " + cont + " : Processo " + proc.getCode());
			}
			else
			{
			    System.out.println("[Cancelar Tarefa] - " + cont + " : ERRO: Processo não localizado!");
			}
			cont++;
		    }
		    return "Cancelamento Processado com Sucesso!";
		}else{
		    return "Não foi encontrado nenhum processo para as tarefas informadas!";
		}
	}else{
		return "Nenhuma tarefa encontrada para cancelamento";
	}
    }
    
    public String buscarTarefa(String code){
	String retorno = "";
	QLGroupFilter groupFilter = new QLGroupFilter("AND");
	groupFilter.addFilter(new QLEqualsFilter("wfprocess.code", code));
	NeoObject o = PersistEngine.getObject(AdapterUtils.getEntityClass("FGCPrincipal"), groupFilter);
	if (o != null){
	    EntityWrapper wo = new EntityWrapper(o);
	    NeoObject neo = (NeoObject) wo.getValue("buscaNomeCliente");
	    if (neo != null){
		EntityWrapper wNeo = new EntityWrapper(neo);
		retorno = (String) wNeo.getValue("nomcli");
	    }
	}
	
	
	return retorno;
    }
}
