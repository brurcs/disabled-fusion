package com.neomind.fusion.custom.orsegups.medicaoDeContratos.vo;

public class ColaboradorMedicaoVO
{
	public String cargaHorPos;
	public String datAdm;
	public String descMed;
	public String funcao;
	public String local;
	public String nomeColaborador;
	public long neoId;
	public long numCadColaborador;
	public String codCcu;
	public boolean gerCms;
	public long horEx;
	public long minEx;
	public long horMed;
	public long minMed;
	public String alertas;
	public String imgAlertas;
	public String linkConsideracoes;
	public String situacao;
	public String linkRemoveColaborador;
	public String nomEsc;

	public String getNomEsc()
	{
		return nomEsc;
	}

	public void setNomEsc(String nomEsc)
	{
		this.nomEsc = nomEsc;
	}

	public final String getLinkRemoveColaborador()
	{
		return linkRemoveColaborador;
	}

	public final void setLinkRemoveColaborador(String linkRemoveColaborador)
	{
		this.linkRemoveColaborador = linkRemoveColaborador;
	}

	public String getSituacao()
	{
		return situacao;
	}

	public void setSituacao(String situacao)
	{
		this.situacao = situacao;
	}

	public String getLinkConsideracoes()
	{
		return linkConsideracoes;
	}

	public void setLinkConsideracoes(String linkConsideracoes)
	{
		this.linkConsideracoes = linkConsideracoes;
	}

	public String getImgAlertas()
	{
		return imgAlertas;
	}

	public void setImgAlertas(String imgAlertas)
	{
		this.imgAlertas = imgAlertas;
	}

	public String getCargaHorPos()
	{
		return cargaHorPos;
	}

	public void setCargaHorPos(String cargaHorPos)
	{
		this.cargaHorPos = cargaHorPos;
	}

	public String getDatAdm()
	{
		return datAdm;
	}

	public void setDatAdm(String datAdm)
	{
		this.datAdm = datAdm;
	}

	public String getDescMed()
	{
		return descMed;
	}

	public void setDescMed(String descMed)
	{
		this.descMed = descMed;
	}

	public String getFuncao()
	{
		return funcao;
	}

	public void setFuncao(String funcao)
	{
		this.funcao = funcao;
	}

	public String getLocal()
	{
		return local;
	}

	public void setLocal(String local)
	{
		this.local = local;
	}

	public String getNomeColaborador()
	{
		return nomeColaborador;
	}

	public void setNomeColaborador(String nomeColaborador)
	{
		this.nomeColaborador = nomeColaborador;
	}

	public long getNeoId()
	{
		return neoId;
	}

	public void setNeoId(long neoId)
	{
		this.neoId = neoId;
	}

	public long getNumCadColaborador()
	{
		return numCadColaborador;
	}

	public void setNumCadColaborador(long numCadColaborador)
	{
		this.numCadColaborador = numCadColaborador;
	}

	public String getCodCcu()
	{
		return codCcu;
	}

	public void setCodCcu(String codCcu)
	{
		this.codCcu = codCcu;
	}

	public boolean isGerCms()
	{
		return gerCms;
	}

	public void setGerCms(boolean gerCms)
	{
		this.gerCms = gerCms;
	}

	public long getHorEx()
	{
		return horEx;
	}

	public void setHorEx(long horEx)
	{
		this.horEx = horEx;
	}

	public long getMinEx()
	{
		return minEx;
	}

	public void setMinEx(long minEx)
	{
		this.minEx = minEx;
	}

	public long getHorMed()
	{
		return horMed;
	}

	public void setHorMed(long horMed)
	{
		this.horMed = horMed;
	}

	public long getMinMed()
	{
		return minMed;
	}

	public void setMinMed(long minMed)
	{
		this.minMed = minMed;
	}

	public String getAlertas()
	{
		return alertas;
	}

	public void setAlertas(String alertas)
	{
		this.alertas = alertas;
	}

}
