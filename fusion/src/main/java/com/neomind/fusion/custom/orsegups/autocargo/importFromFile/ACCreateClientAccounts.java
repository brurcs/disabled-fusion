package com.neomind.fusion.custom.orsegups.autocargo.importFromFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.neomind.fusion.custom.orsegups.autocargo.importFromFile.beans.ACCliente;
import com.neomind.fusion.custom.orsegups.autocargo.importFromFile.messages.SIGMAClientInsertError;
import com.neomind.fusion.custom.orsegups.autocargo.importFromFile.messages.SIGMAClientInsertSucess;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;

public class ACCreateClientAccounts {


    public void createClients() {

	String sql = "SELECT * FROM CELTEC_CLIENTE WITH(NOLOCK) WHERE CD_CLIENTE IS NULL ";

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	List<ACCliente> clientes = new ArrayList<ACCliente>();

	try {
	    conn = PersistEngine.getConnection("IMPORTACOES");

	    stmt = conn.createStatement();

	    rs = stmt.executeQuery(sql);

	    while (rs.next()) {
		ACCliente novoCliente = new ACCliente();

		novoCliente.setId(rs.getInt(1));
		novoCliente.setIdLegado(rs.getInt(2));
		novoCliente.setOperadora(rs.getString(3));
		novoCliente.setNome(rs.getString(4));
		novoCliente.setTelefone(rs.getString(5) != null ? rs.getString(5) : "");
		novoCliente.setCep(rs.getString(6) != null ? rs.getString(6) : "");
		novoCliente.setRua(rs.getString(7) != null ? rs.getString(7) : "");
		novoCliente.setNumero(rs.getString(8) != null ? rs.getString(8) : "");
		novoCliente.setComplemento(rs.getString(9) != null ? rs.getString(9) : "");
		novoCliente.setBairro(rs.getString(10) != null ? rs.getString(10) : "");
		novoCliente.setCidade(rs.getString(11) != null ? rs.getString(11) : "");
		novoCliente.setEstado(rs.getString(12) != null ? rs.getString(12) : "");
		clientes.add(novoCliente);
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	this.clientInsert(clientes);

    }

    private void clientInsert(List<ACCliente> clientes) {

	int numeroConta = 1365;

	for (ACCliente c : clientes) {

	    StringBuilder retorno = new StringBuilder();
	    
	    String conta = "R" + numeroConta;

	    try {

		StringBuilder params = new StringBuilder();

		params.append("HTTP://192.168.20.218:8080/SigmaWebServices/ClientInsert?");
		params.append("companyId=10001");
		params.append("&account=");
		params.append(conta);
		params.append("&partition=000");
		
		String nome = c.getNome().trim();
		
		if (nome.length()> 50){
		    nome = nome.substring(0, 50);
		}
		
		params.append("&corporationName=");
		params.append(URLEncoder.encode(nome, "UTF-8"));
		params.append("&tradeName=");
		params.append(URLEncoder.encode(nome, "UTF-8"));

		if (c.getTelefone() != null && !c.getTelefone().isEmpty()) {
		    String fones[] = c.getTelefone().split(";");

		    String fone1 = fones[0].trim();
		    
		    if (fone1.length() > 14){
			fone1 = fone1.substring(1,14);
		    }
		    
		    params.append("&phone1=");
		    params.append(URLEncoder.encode(fone1, "UTF-8"));

		    if (fones.length > 1) {
			
			String fone2 = fones[1].trim();
			
			if (fone2.length() > 14){
			    fone2 = fone2.substring(1,14);
			}
			
			params.append("&phone2=");
			params.append(URLEncoder.encode(fone2, "UTF-8"));
		    }

		}

		if (c.getCep() != null && !c.getCep().isEmpty()) {
		    params.append("&zipCode=");
		    params.append(c.getCep());

		}

		if (c.getRua() != null && !c.getRua().isEmpty()) {
		    params.append("&address=");

		    String endereco = c.getRua();

		    if (c.getNumero() != null && !c.getNumero().isEmpty()) {
			endereco += " " + c.getNumero();
		    }

		    if (c.getComplemento() != null && !c.getComplemento().isEmpty()) {
			endereco += " " + c.getComplemento();
		    }

		    params.append(URLEncoder.encode(endereco, "UTF-8"));
		}

		params.append("&stateId=");
		params.append(c.getEstado());

		int codigoCidade = this.getCodigoCidade(c.getEstado(), c.getCidade());

		if (codigoCidade != 0) {
		    params.append("&cityId=");
		    params.append(codigoCidade);

		    int codigoBairro = this.getCodigoBairro(codigoCidade, c.getBairro());

		    if (codigoBairro != 0) {
			params.append("&districtId=");
			params.append(codigoBairro);
		    }else{
			params.append("&districtId=10346");
		    }

		} else {
		    params.append("&cityId=10075");
		    params.append("&districtId=10346");
		}

		params.append("&complement=");
		params.append(c.getIdLegado());

		params.append("&route1Id=86007");
		params.append("&installerId=89427");
		params.append("&sellerId=89427");
		params.append("&branchActivityId=10001");
		params.append("&controlPartition=1");
		params.append("&active=1");
		params.append("&unifyPartition=0");

		System.out.println(params.toString());

		URL url = new URL(params.toString());
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");

		System.out.println(conn.getResponseCode());
		if (conn.getResponseCode() != 200) {
		    this.logErrorClient("Failed : HTTP error code : " + conn.getResponseCode(), c.getId());
		    continue;
		} else {
		    BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

		    String output;

		    while ((output = br.readLine()) != null) {
			retorno.append(output);
		    }

		    conn.disconnect();

		    String resposta = retorno.toString();

		    Gson gson = new Gson();

		    SIGMAClientInsertError erro = null;
		    SIGMAClientInsertSucess sucesso = null;
		    if (resposta.contains("errors")) {
			erro = gson.fromJson(resposta, SIGMAClientInsertError.class);
			
			StringBuilder msg = new StringBuilder();
			
			HashMap<String, String> erros = erro.getErrors();
			
			for (String key : erros.keySet()){
			    msg.append("Campo: "+key+" Problema: "+erros.get(key)+ " ");
			}
			
			int update = this.logErrorClient(msg.toString(), c.getId());
			
			int count = 0;
			
			while (update < 1 && count < 2){
			    update = this.logErrorClient(msg.toString(), c.getId());
			    count ++;
			}
			
			System.out.println("Problema! "+msg.toString());
			
		    } else {
			sucesso = gson.fromJson(resposta, SIGMAClientInsertSucess.class);
			
			System.out.println("Sucesso!");
			
			int update = this.setCdCliente(c.getId(), sucesso.getEntityId());
			
			int count = 0;
			
			while (update < 1 && count < 2){
			    update = this.setCdCliente(c.getId(), sucesso.getEntityId());
			    count ++;
			}
			
			numeroConta++;
			
		    }
		}

	    } catch (MalformedURLException e) {

		e.printStackTrace();

	    } catch (IOException e) {

		e.printStackTrace();

	    }

	}

    }

    private int getCodigoCidade(String estado, String cidade) {

	String sql = "SELECT C.ID_CIDADE FROM dbCIDADE C WITH(NOLOCK) WHERE C.NOME=? AND C.ID_ESTADO=? ";

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	try {
	    conn = PersistEngine.getConnection("SIGMA90");
	    pstm = conn.prepareStatement(sql);
	    pstm.setString(1, cidade);
	    pstm.setString(2, estado);

	    rs = pstm.executeQuery();

	    if (rs.next()) {
		return rs.getInt(1);
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

	return 0;
    }

    private int getCodigoBairro(int codigoCidade, String bairro) {

	String sql = "SELECT B.ID_BAIRRO FROM dbBAIRRO B WITH(NOLOCK) WHERE B.ID_CIDADE = ? AND B.NOME = ? ";

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	try {
	    conn = PersistEngine.getConnection("SIGMA90");
	    pstm = conn.prepareStatement(sql);
	    pstm.setInt(1, codigoCidade);
	    pstm.setString(2, bairro);

	    rs = pstm.executeQuery();

	    if (rs.next()) {
		return rs.getInt(1);
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

	return 0;
    }
    
    private int logErrorClient(String msg, int idCliente){
	
	String sql = "UPDATE CELTEC_CLIENTE SET ERROS = ? WHERE ID_CLIENTE = ?";
	
	Connection conn = null;
	PreparedStatement pstm = null;

	try {
	    conn = PersistEngine.getConnection("IMPORTACOES");
	    pstm = conn.prepareStatement(sql);
	    pstm.setString(1, msg);
	    pstm.setInt(2, idCliente);

	    return pstm.executeUpdate();

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}
	
	return 0;

    }
    
    private int setCdCliente (int idCliente, int cdCliente){
	
	String sql = "UPDATE CELTEC_CLIENTE SET CD_CLIENTE = ? WHERE ID_CLIENTE = ?";
	
	Connection conn = null;
	PreparedStatement pstm = null;

	try {
	    conn = PersistEngine.getConnection("IMPORTACOES");
	    pstm = conn.prepareStatement(sql);
	    pstm.setInt(1, cdCliente);
	    pstm.setInt(2, idCliente);

	    return pstm.executeUpdate();

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}
	
	return 0;
	
    }

}
