package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;

public class AbrirOSTrocaCHIP_SRR implements CustomJobAdapter {
    
    @Override
    public void execute(CustomJobContext ctx) {
	
	Connection conn = PersistEngine.getConnection("SIGMA90"); 
	PreparedStatement pstm = null;
	ResultSet rs = null;
	final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.AbrirOSTrocaCHIP_SRR");
	log.warn("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Validacao OS Chp1 - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	System.out.println("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Validacao OS Chp1 AbrirOSTrocaCHIP_SRR - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	Long key = GregorianCalendar.getInstance().getTimeInMillis();
	try
	{	    
	    StringBuffer  varname1 = new StringBuffer();
	    
	    varname1.append("select top 50 ");
	    varname1.append("      cen.CD_TECNICO_RESPONSAVEL, ");
	    varname1.append("      cen.ctrl_central, ");
	    varname1.append("      tabelao.*, ");
	    varname1.append("      cid.nome as cidade, ");
	    varname1.append("      bai.nome as bairro ");
	    varname1.append("from ( ");
	    varname1.append("select ");
	    varname1.append("       clienteGPRS.*, ");
	    varname1.append("       (select top 1 MIN(CD_CLIENTE) ");
	    varname1.append("          from [FSOODB03\\SQL01].SIGMA90.dbo.dbCentral c ");
	    varname1.append("         where c.ID_EMPRESA = clienteGPRS.ID_EMPRESA ");
	    varname1.append("           and c.ID_CENTRAL = clienteGPRS.ID_CENTRAL ");
	    varname1.append("           and c.PARTICAO not like '09%' ");
	    varname1.append("           and c.ctrl_central = 1 )as Cliente, ");
	    varname1.append("       (select top 1 c.razao ");
	    varname1.append("          from [FSOODB03\\SQL01].SIGMA90.dbo.dbCentral c ");
	    varname1.append("         where c.ID_EMPRESA = clienteGPRS.ID_EMPRESA ");
	    varname1.append("           and c.ID_CENTRAL = clienteGPRS.ID_CENTRAL) as razao, ");
	    varname1.append("       (select COUNT(1) ");
	    varname1.append("          from [FSOODB03\\SQL01].SIGMA90.dbo.dbCentral ccc ");
	    varname1.append("    inner join [FSOODB03\\SQL01].SIGMA90.dbo.dbOrdem o ");
	    varname1.append("               on ccc.cd_cliente = o.cd_cliente ");
	    varname1.append("         where ccc.ID_EMPRESA = clienteGPRS.ID_EMPRESA ");
	    varname1.append("           and ccc.ID_CENTRAL = clienteGPRS.ID_CENTRAL ");
	    varname1.append("           and o.DEFEITO like '%#FOTOCHIP#%') as QtdeOSGeradasComFotoChip, ");
	    varname1.append("       (select COUNT(1) ");
	    varname1.append("          from [FSOODB03\\SQL01].SIGMA90.dbo.dbCentral ccc ");
	    varname1.append("    inner join [FSOODB03\\SQL01].SIGMA90.dbo.dbOrdem o ");
	    varname1.append("               on ccc.cd_cliente = o.cd_cliente ");
	    varname1.append("         where ccc.ID_EMPRESA = clienteGPRS.ID_EMPRESA ");
	    varname1.append("           and ccc.ID_CENTRAL = clienteGPRS.ID_CENTRAL ");
	    varname1.append("           and o.DEFEITO like '%#FOTOCHIP#%' ");
	    varname1.append("           and o.FECHADO = 0) as QtdeOSComFotoChipAberta, ");
	    varname1.append("       (select top 1 SUBSTRING(ro.NM_ROTA,0,4) ");
	    varname1.append("          from [FSOODB03\\SQL01].SIGMA90.dbo.dbCentral ccc ");
	    varname1.append("	inner join [FSOODB03\\SQL01].SIGMA90.dbo.ROTA ro on ccc.ID_ROTA = ro.CD_ROTA ");
	    varname1.append("	     where ccc.ID_EMPRESA = clienteGPRS.ID_EMPRESA ");
	    varname1.append("           and ccc.ID_CENTRAL = clienteGPRS.ID_CENTRAL) as Regional, ");
	    varname1.append("       b.* ");
	    varname1.append("     from ( ");
	    varname1.append("      select * ");
	    varname1.append("        from ( ");
	    varname1.append("          select ID_EMPRESA, ");
	    varname1.append("                 ID_CENTRAL ");
	    varname1.append("          from [FSOODB03\\SQL01].SIGMA90.dbo.dbCentral ce WITH(NOLOCK) ");
	    varname1.append("         where ce.ctrl_central = 1 ");
	    varname1.append("           and ce.STLIBERACAOIMEI = 1 ");
	    varname1.append("           and ce.id_central not like 'DDD%' ");
	    varname1.append("                 and ce.id_central not like 'FFF%' ");
	    varname1.append("                 and ce.id_central not like 'AA%' ");
	    varname1.append("                 and ce.id_central not like 'R%' ");
	    varname1.append("                    ");
	    varname1.append("       union ");
	    varname1.append("        select ID_EMPRESA, ");
	    varname1.append("                 ID_CENTRAL ");
	    varname1.append("          from [FSOODB03\\SQL01].SIGMA90.DBO.dbCENTRAL WITH(NOLOCK) ");
	    varname1.append("         where celular = 1 ");
	    varname1.append("           and ctrl_central = 1 ");
	    varname1.append("           and ncelular = 'GPRS' ");
	    varname1.append("           and id_central not like 'DDD%' ");
	    varname1.append("           and id_central not like 'FFF%' ");
	    varname1.append("           and id_central not like 'AA%' ");
	    varname1.append("           and id_central not like 'R%' ) as clientes) as clienteGPRS ");
	    varname1.append("            ");
	    varname1.append("           left join [CACUPE\\SQL02].Fusion_Producao.dbo.D_Chp1Cliente b on b.empresaConta = clienteGPRS.ID_EMPRESA ");
	    varname1.append("                                    and b.contaSIGMA = clienteGPRS.ID_CENTRAL collate Latin1_General_CI_AS ");
	    varname1.append("                                    and ((b.visitaValidada = 1 and b.dataRemocao is null) or (b.visitaValidada is null and b.dataRemocao is null)) ");
	    varname1.append("        where b.neoId is null ");
	    varname1.append("        ) as tabelao ");
	    varname1.append("        inner join [FSOODB03\\SQL01].SIGMA90.DBO.dbCentral cen on cen.CD_CLIENTE = tabelao.Cliente ");
	    varname1.append("        inner join [FSOODB03\\SQL01].SIGMA90.DBO.dbBairro bai on cen.ID_BAIRRO = bai.ID_BAIRRO ");
	    varname1.append("        inner join [FSOODB03\\SQL01].SIGMA90.DBO.dbCidade cid on bai.ID_CIDADE = cid.ID_CIDADE ");
	    varname1.append("        where tabelao.ID_EMPRESA <> 10175 ");
	    varname1.append("          and tabelao.Regional like 'SRR' ");
	    varname1.append("          and tabelao.QtdeOSComFotoChipAberta = 0 ");
	    varname1.append("          and cen.CD_TECNICO_RESPONSAVEL is not null ");
	    varname1.append("        order by cid.nome,bai.nome");
	    
	    pstm = conn.prepareStatement(varname1.toString());

	    rs = pstm.executeQuery();
	    
	    Integer colaboradorAtual = 0;
	    Integer contador = 1;

	    while (rs.next()) {
		Integer cdCliente = rs.getInt("Cliente");
		Integer idEmpresa = rs.getInt("ID_EMPRESA");
		String idCentral = rs.getString("ID_CENTRAL");
		String razao = rs.getString("razao");
		Integer ctrlCentral = rs.getInt("ctrl_central");
		Integer cdColaborador = rs.getInt("CD_TECNICO_RESPONSAVEL");
		
		if ((colaboradorAtual.equals(0)) || (colaboradorAtual.equals(cdColaborador)) && contador <= 1100) {
		    abrirOrdemServico(cdCliente, cdColaborador);
		    //System.out.println("<tr><td>"+contador+"</td><td>"+cdColaborador+"</td><td>"+razao+"</td></tr>");
		    System.out.println(contador+";"+cdCliente+";"+idEmpresa+";"+idCentral+";"+razao+";"+cdColaborador+";"+ctrlCentral);
		    contador++;
		    colaboradorAtual = cdColaborador;		    
		} else {
		    if (colaboradorAtual.equals(cdColaborador)) {
			contador++;
		    } else {
			contador = 1;
			abrirOrdemServico(cdCliente, cdColaborador);
			//System.out.println("<tr style=\"background-color:powderblue;\"><td>"+contador+"</td><td>"+cdColaborador+"</td><td>"+razao+"</td></tr>");
			System.out.println(contador+";"+cdCliente+";"+idEmpresa+";"+idCentral+";"+razao+";"+cdColaborador+";"+ctrlCentral);
			contador++;
			colaboradorAtual = cdColaborador;
		    }		    
		}						
	    }
	} catch (Exception e) {
	    System.out.println(e);
	}
    }
    
    private void abrirOrdemServico(int cdCliente, int cdTecnico){

   	Connection conn = null;
   	PreparedStatement pstm = null;
   	ResultSet rs = null;

   	StringBuilder sql = new StringBuilder();
   	sql.append(" INSERT INTO dbORDEM ");
   	sql.append(" (CD_CLIENTE, ID_INSTALADOR, ABERTURA,  DEFEITO, OPABRIU, IDOSDEFEITO, FG_EMAIL_ENVIADO, CD_OS_SOLICITANTE, FG_TODAS_PARTICOES_EM_MANUTENCAO,  EXECUTADO,	TEMPOEXECUCAOPREVISTO )");
   	sql.append(" VALUES(?,          ?,        GETDATE(), ?,       ?,       ?,           ?,                ?,                 ?,					?,		1 )");

   	try {
   	    conn = PersistEngine.getConnection("SIGMA90");
   	    pstm = conn.prepareStatement(sql.toString());

   	    pstm.setInt(1, cdCliente);
   	    pstm.setInt(2, 118617);
   	    pstm.setString(3, "Registrar a foto do Chip GPRS da central. Havendo a necessidade de troca(Valide no link: http://chip.legal), retirar fotos dos 2 Chips(Novo e Antigo), marcar um X no Chip "
   		    + "antigo para diferenciar. SRR. Clientes Ethernet, retirar 1 foto da conexão ethernet.#FOTOCHIP#");
   	    pstm.setInt(4, 11010); //FUSION
   	    pstm.setInt(5, 1019);
   	    pstm.setInt(6, 0); //FALSO	    
   	    pstm.setInt(7, 10011); //5-DEMAIS ORDENS DE SERVIÇO
   	    pstm.setInt(8, 0); //FALSO
   	    pstm.setString(9, "");

   	    pstm.executeUpdate();

   	} catch (SQLException e) {
   	    e.printStackTrace();
   	} finally {
   	    OrsegupsUtils.closeConnection(conn, pstm, rs);
   	}
    }    
}
