package com.neomind.fusion.custom.orsegups.autocargo.beans.vehicle;

public class ACTrackingModule {
    
    private long number;
    private String  model;
    
    public long getNumber() {
        return number;
    }
    public void setNumber(long number) {
        this.number = number;
    }
    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }
    
}
