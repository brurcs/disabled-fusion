package com.neomind.fusion.custom.orsegups.utils.db;

public class ConexaoTipo
{
	String nome;
	String host; 
	String port; 
	String db; 
	String user; 
	String passwd; 
	String instance; 
	String tipoBanco;
	
	public String getNome()
	{
		return nome;
	}
	public void setNome(String nome)
	{
		this.nome = nome;
	}
	public String getHost()
	{
		return host;
	}
	public void setHost(String host)
	{
		this.host = host;
	}
	public String getPort()
	{
		return port;
	}
	public void setPort(String port)
	{
		this.port = port;
	}
	public String getDb()
	{
		return db;
	}
	public void setDb(String db)
	{
		this.db = db;
	}
	public String getUser()
	{
		return user;
	}
	public void setUser(String user)
	{
		this.user = user;
	}
	public String getPasswd()
	{
		return passwd;
	}
	public void setPasswd(String passwd)
	{
		this.passwd = passwd;
	}
	public String getInstance()
	{
		return instance;
	}
	public void setInstance(String instance)
	{
		this.instance = instance;
	}
	public String getTipoBanco()
	{
		return tipoBanco;
	}
	public void setTipoBanco(String tipoBanco)
	{
		this.tipoBanco = tipoBanco;
	}
	
	public ConexaoTipo(String nome, String host, String port, String db, String user, String passwd, String instance, String tipoBanco)
	{
		super();
		this.nome = nome;
		this.host = host;
		this.port = port;
		this.db = db;
		this.user = user;
		this.passwd = passwd;
		this.instance = instance;
		this.tipoBanco = tipoBanco;
	}
	
	
	
	
}
