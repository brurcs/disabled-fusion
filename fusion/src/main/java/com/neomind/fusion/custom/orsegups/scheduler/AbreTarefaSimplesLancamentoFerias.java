package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;

public class AbreTarefaSimplesLancamentoFerias implements CustomJobAdapter {

    @SuppressWarnings("unused")
    private static final Log log = LogFactory.getLog(AbreTarefaSimplesLancamentoFerias.class);

    @Override
    public void execute(CustomJobContext arg0) {
	
	Connection conn = PersistEngine.getConnection("VETORH"); 
	PreparedStatement pstm = null;
	ResultSet rs = null;
	final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.AbreTarefaSimplesLancamentoFerias");
	log.warn("##### INICIO AGENDADOR DE TAREFA: AbreTarefaSimplesLancamentoFerias - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	System.out.println("##### INICIO AGENDADOR DE TAREFA: AbreTarefaSimplesLancamentoFerias - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	Long key = GregorianCalendar.getInstance().getTimeInMillis();
	try
	{	    
	    StringBuffer  varname1 = new StringBuffer();
	    
	    varname1.append("SELECT per.NumEmp, ");
	    varname1.append("               fun.codfil, ");
	    varname1.append("               fun.tipcol, ");
	    varname1.append("               per.NumCad, ");
	    varname1.append("               fun.nomfun, ");
	    varname1.append("               orn.usu_codreg, ");
	    varname1.append("               reg.USU_NomReg as nomReg, ");
	    varname1.append("               SUM(per.AvoFer) AS AvoFer, ");
	    varname1.append("               per2.FimPer, ");
	    varname1.append("               CONVERT(varchar(20),per2.FimPer,103) as FimPerFormatada ");
	    varname1.append("		  FROM R040PER per ");
	    varname1.append("	INNER JOIN R034FUN fun on fun.numemp = per.numemp ");
	    varname1.append("	                      AND fun.tipcol = per.tipcol ");
	    varname1.append("	                      AND fun.numcad = per.numcad ");
	    varname1.append("	INNER JOIN R016ORN orn ON orn.TabOrg = fun.TabOrg ");
	    varname1.append("	                      AND orn.NumLoc = fun.NumLoc ");
	    varname1.append("	INNER JOIN usu_t200reg reg on orn.usu_codreg = reg.USU_CodReg ");
	    varname1.append("	 LEFT JOIN R038AFA afa WITH (NOLOCK) ON (afa.NumEmp = fun.NumEmp ");
	    varname1.append("	                                    AND afa.TipCol = fun.TipCol ");
	    varname1.append("	                                    AND afa.NumCad = fun.NumCad ");
	    varname1.append("	                                    AND afa.DatAfa = (SELECT MAX (afa2.DatAfa) ");
	    varname1.append("	                                                        FROM R038AFA afa2 ");
	    varname1.append("	                                                       WHERE afa2.NUMEMP = afa.NUMEMP ");
	    varname1.append("	                                                         AND afa2.TIPCOL = afa.TIPCOL ");
	    varname1.append("	                                                         AND afa2.NUMCAD = afa.NUMCAD ");
	    varname1.append("	                                                         AND afa2.DATAFA <= GETDATE() ");
	    varname1.append("	                                                         AND (afa2.DatTer = '1900-12-31 00:00:00' OR afa2.DatTer >= cast(floor(cast(GETDATE() as float)) as datetime)))) ");
	    varname1.append("	 LEFT JOIN R010SIT sit ON sit.CodSit = afa.SitAfa ");
	    varname1.append("	 LEFT JOIN R040PER per2 ON per2.NumCad = per.NumCad ");
	    varname1.append("	                       AND per2.NumEmp = per.NumEmp ");
	    varname1.append("	                       AND per2.TipCol = per.TipCol ");
	    varname1.append("	                       AND per2.SitPer = 0 ");
	    varname1.append("	                       AND per2.IniPer >= '2000-01-01' ");
	    varname1.append("	                       AND per2.IniPer <= getdate() ");
	    varname1.append("	                       AND per2.IniPer = (SELECT MAX (per3.IniPer) ");
	    varname1.append("	                                            FROM R040PER per3 ");
	    varname1.append("	                                           WHERE per3.NumEmp = per.NumEmp ");
	    varname1.append("	                                             AND per3.TipCol = per.TipCol ");
	    varname1.append("	                                             AND per3.NumCad = per.NumCad ");
	    varname1.append("	                                             AND per3.SitPer = 0 ");
	    varname1.append("	                                             AND per3.IniPer >= '2000-01-01' ");
	    varname1.append("	                                             AND per3.IniPer <= GETDATE()) ");
	    varname1.append("		 WHERE per.SitPer = 0 ");
	    varname1.append("		   AND fun.tipcol = 1 ");
	    varname1.append("		   AND per.iniper >= '2000-01-01' ");
	    varname1.append("		   AND per.iniper <= GETDATE() ");
	    varname1.append("		   AND fun.NumEmp NOT IN (3, 5, 11, 12, 13, 14, 999) ");
	    varname1.append("		   AND (afa.SitAfa IS NULL ");
	    varname1.append("		    OR afa.SitAfa NOT IN (3,4,7,8,74) ");
	    varname1.append("		    OR (afa.SitAfa = 7 ");
	    varname1.append("		    AND afa.DatAfa > cast(floor(cast(GETDATE() as float)) as datetime))) ");
	    varname1.append("		   AND NOT EXISTS (SELECT * ");
	    varname1.append("		                     FROM [CACUPE\\SQL02].fusion_Producao.DBO.d_TarefaLancamentoFerias tlf WITH (NOLOCK) ");
	    varname1.append("			 				WHERE tlf.numemp = fun.NumEmp ");
	    varname1.append("			 				  AND tlf.tipcol = fun.TipCol ");
	    varname1.append("			 				  AND tlf.numcad = fun.NumCad ");
	    varname1.append("			 				  AND cast(floor(cast(tlf.fimper as float)) as datetime) = cast(floor(cast(per.FimPer as float)) as datetime)) ");
	    varname1.append("	  GROUP BY per.NumEmp, ");
	    varname1.append("	           fun.codfil, ");
	    varname1.append("	           fun.tipcol, ");
	    varname1.append("	           fun.nomfun, ");
	    varname1.append("	           per.NumCad, ");
	    varname1.append("	           orn.usu_codreg, ");
	    varname1.append("	           reg.USU_NomReg, ");
	    varname1.append("	           afa.SitAfa, ");
	    varname1.append("	           sit.DesSit, ");
	    varname1.append("	           per2.FimPer, ");
	    varname1.append("	           CONVERT(varchar(20),per2.FimPer,103) ");
	    varname1.append("	    HAVING SUM(per.AvoFer) >= 21 ");
	    varname1.append("	  ORDER BY SUM(per.AvoFer) DESC");
	    
	    pstm = conn.prepareStatement(varname1.toString(),ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
	    rs = pstm.executeQuery();	

	    while (rs.next())
	    {
		String titulo = rs.getString("nomReg") + " - " + rs.getString("AvoFer") + " AVOS - " + rs.getString("NumCad") + " - " + rs.getString("NomFun");
		//titulo = URLEncoder.encode(titulo, "ISO-8859-1");
		
		String codEmpresa = "";
		codEmpresa = ""+rs.getInt("NumEmp");
		
		String codFilial = "";
		codFilial = ""+rs.getInt("CodFil");
		
		String descricaoTarefa = "";
		descricaoTarefa =  rs.getString("AvoFer")+ " AVOS -" + rs.getString("AvoFer") + " AVOS -" + rs.getString("NumCad") + " - " + rs.getString("NomFun") + " deverá sair de férias até " + rs.getString("FimPerFormatada") + ".";
		
		
		abrirTarefa(codEmpresa, codFilial, titulo, descricaoTarefa);
	    }
	    
	}catch (Exception e) {
	    log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Lancameno Ferias");
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
	}
	finally {
	    try {
		rs.close();
		pstm.close();
		conn.close();
	    } catch (SQLException e) {
		e.printStackTrace();
	    }

	    log.warn("##### FIM AGENDADOR DE TAREFA: AbreTarefaSimplesLancamentoFerias - - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	    System.out.println("##### FIM AGENDADOR DE TAREFA: AbreTarefaSimplesLancamentoFerias - - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	}
    }
    
    /**
     * Metodo de abertura de tarefas
     * 
     * @param codEmp
     * @param codFil
     * @param numCtr
     * @param numPos
     * @param numeroOS
     * @return numero da Tarefa
     */
    private void abrirTarefa(String _codEmp, String _codFil, String _titulo, String _descricaoTarefa) throws Exception {
	
	String solicitante = "beatriz.malmann";
	String executor = "joseane.nascimento";
	//String executor = "lucas.alison";
	
	GregorianCalendar prazo = new GregorianCalendar();
	int dia = 0;
	while (dia < 2) {	    	
	    if (OrsegupsUtils.isWorkDay(prazo)) {		
		dia++;
	    }
	    prazo.add(GregorianCalendar.DATE, 1);
	}
	while (!OrsegupsUtils.isWorkDay(prazo)) 
	{
		prazo = OrsegupsUtils.getNextWorkDay(prazo);
	}
	
	prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
	prazo.set(GregorianCalendar.MINUTE, 59);
	prazo.set(GregorianCalendar.SECOND, 59);

	IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
	
	iniciarTarefaSimples.abrirTarefa(solicitante, executor, _titulo, _descricaoTarefa, "1", "hadouken", prazo); 
    }
}
