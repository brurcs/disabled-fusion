package com.neomind.fusion.custom.orsegups.mobile.vo;





public class PesquisaVO {
	
private String cod_pesq;
private String desc_pesq;
private String cod_assun;
private String desc_assun;
private String desc_perg;
private String tip_resposta;
private String cod_perg;	 
private String siglaRegional;
	
	
	public String getCod_assun() {
		return cod_assun;
	}
	public void setCod_assun(String cod_assun) {
		this.cod_assun = cod_assun;
	}
	public String getDesc_assun() {
		return desc_assun;
	}
	public void setDesc_assun(String desc_assun) {
		this.desc_assun = desc_assun;
	}
	public String getDesc_perg() {
		return desc_perg;
	}
	public void setDesc_perg(String desc_perg) {
		this.desc_perg = desc_perg;
	}
	public String getTip_resposta() {
		return tip_resposta;
	}
	public void setTip_resposta(String tip_resposta) {
		this.tip_resposta = tip_resposta;
	}
	public String getCod_perg() {
		return cod_perg;
	}
	public void setCod_perg(String cod_perg) {
		this.cod_perg = cod_perg;
	}


	public String getCod_pesq() {
		return cod_pesq;
	}
	public void setCod_pesq(String cod_pesq) {
		this.cod_pesq = cod_pesq;
	}
	public String getDesc_pesq() {
		return desc_pesq;
	}
	public void setDesc_pesq(String desc_pesq) {
		this.desc_pesq = desc_pesq;
	}
	 public String getSiglaRegional()
	{
		return siglaRegional;
	}
	public void setSiglaRegional(String siglaRegional)
	{
		this.siglaRegional = siglaRegional;
	}
	
	@Override
	    public String toString() {
	        return "Pesquisas{" +
	                "des_pesq=" + desc_pesq +
	                ", cod_pesq='" + cod_pesq + '\'' +
	                 
	                '}';
	    }

}
