package com.neomind.fusion.custom.orsegups.justificativaAfastamento.converter;

import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.justificativaAfastamento.util.JAUtils;
import com.neomind.fusion.custom.orsegups.presenca.vo.AnotacaoVO;
import com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.util.NeoDateUtils;

public class JAHistoricoAnotacoesConverter extends StringConverter {

    @Override
    protected String getHTMLInput(EFormField field, OriginEnum origin) {
	
	
	
	Long idPai = field.getForm().getObjectId();
	EntityWrapper wrapper = JAUtils.getEformPai(idPai);
	
	NeoObject col = (NeoObject)wrapper.findField("colaborador").getValue();
	
	EntityWrapper wCol= new EntityWrapper(col);
	
	long numemp = (long) wCol.findField("numemp").getValue();
	
	long numcad = (long) wCol.findField("numcad").getValue();
	
	List<AnotacaoVO> anotacoes =  QLPresencaUtils.listaAnotacoes(numemp, 1L, numcad);
	
	StringBuilder html = new StringBuilder();
	
	html.append("<table class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\" id=\"tableHistoricoAnotacoes\">"); 
	html.append("<tbody>"); 
	html.append("<th>Data da anotação</th>"); 
	html.append("<th>Tipo anotação</th>");
	html.append("<th>Descrição</th>"); 
	
	if (anotacoes != null && !anotacoes.isEmpty()){
	    for (AnotacaoVO anotacao : anotacoes){
		this.htmlAppend(html, anotacao);
	    }
	}
	
	
	html.append("</tbody>");
	html.append("</table></br>");
	
	
	return html.toString();

    }

    private void htmlAppend(StringBuilder html, AnotacaoVO anotacao) {

	html.append("<tr>");
	html.append("<td>" + NeoDateUtils.safeDateFormat(anotacao.getData(), "dd/MM/yyyy") + "</td>");
	html.append("<td>" + anotacao.getTipoAnotacao() + "</td>");
	html.append("<td>" + anotacao.getDescricao() + "</td>");
	html.append("</tr>");
    }

    @Override
    protected String getHTMLView(EFormField field, OriginEnum origin) {
	return getHTMLInput(field, origin);
    }

}
