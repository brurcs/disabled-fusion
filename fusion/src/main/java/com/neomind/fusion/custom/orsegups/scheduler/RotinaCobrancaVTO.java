package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsConnectionUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class RotinaCobrancaVTO implements CustomJobAdapter {

    final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.RotinaCobrancaOS");
    private Map<Integer, String> serverInfo = OrsegupsConnectionUtils.getServerInfo("SIGMA");

    public void execute(CustomJobContext ctx) {

	Long key = GregorianCalendar.getInstance().getTimeInMillis();

	log.warn("INICIAR AGENDADOR DE TAREFA: RotinaCobrancaOS -- Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	try {

	    conn = PersistEngine.getConnection("SAPIENS");

	    sql.append(" SELECT cvs.usu_codemp, cvs.usu_codfil, cvs.usu_numctr, cvs.usu_numpos, cli.codcli,cvs.usu_sitcvs, cvs.usu_codser, cvs.usu_periss, cvs.usu_perirf, cvs.usu_perins, cvs.usu_perpit, cvs.usu_percsl, ");
	    sql.append(" cvs.usu_percrt, cvs.usu_perour, cvs.usu_ctafin, cvs.usu_ctared, cvs.usu_codccu, cvs.usu_datini, cvs.usu_tnsser,   ");
	    sql.append(" H.CD_HISTORICO, H.DT_RECEBIDO, CEN.CD_CLIENTE, CEN.ID_CENTRAL, CEN.PARTICAO, CEN.RAZAO  ");
	    sql.append(" FROM USU_T160SIG sig WITH (NOLOCK) ");
	    sql.append(" INNER JOIN USU_T160CVS cvs WITH (NOLOCK) ");
	    sql.append(" ON cvs.usu_codemp = sig.usu_codemp ");
	    sql.append(" AND cvs.usu_codfil = sig.usu_codfil AND cvs.usu_numctr = sig.usu_numctr AND cvs.usu_numpos = sig.usu_numpos ");
	    sql.append(" INNER JOIN USU_T160CTR CTR WITH(NOLOCK) ");
	    sql.append(" ON cvs.usu_codemp = ctr.usu_codemp ");
	    sql.append(" AND cvs.usu_codfil = ctr.usu_codfil AND cvs.usu_numctr = ctr.usu_numctr ");
	    sql.append(" INNER JOIN E085CLI cli WITH(NOLOCK) ON CTR.usu_codcli = cli.CodCli ");
	    sql.append(" INNER JOIN E080SER ser WITH(NOLOCK) ON cvs.usu_codemp = ser.CodEmp   ");
	    sql.append(" AND ser.CodSer = cvs.usu_codser   ");
	    sql.append(" INNER JOIN [" + this.serverInfo.get(OrsegupsConnectionUtils.NOME_SERVIDOR) + "\\" + this.serverInfo.get(OrsegupsConnectionUtils.INSTANCIA) + "].SIGMA90.dbo.dbCENTRAL CEN WITH(NOLOCK) ON sig.usu_codcli = CEN.CD_CLIENTE ");
	    sql.append(" INNER JOIN [" + this.serverInfo.get(OrsegupsConnectionUtils.NOME_SERVIDOR) + "\\" + this.serverInfo.get(OrsegupsConnectionUtils.INSTANCIA) + "].SIGMA90.dbo.VIEW_HISTORICO H WITH(NOLOCK) ON H.CD_CLIENTE = CEN.CD_CLIENTE ");
	    sql.append(" WHERE ");
	    sql.append(" ((cvs.usu_sitcvs = 'A') OR (cvs.usu_sitcvs = 'I' and cvs.usu_datfim >= GETDATE())) ");
	    sql.append(" AND cli.TipEmc = 1 ");
	    sql.append(" AND H.DT_FECHAMENTO >= ? ");
	    sql.append("  AND DT_FECHAMENTO <= ? ");
	    sql.append(" AND H.CD_EVENTO='AVTO' ");
//	    sql.append(" AND EXISTS  ");
//	    sql.append(" (SELECT CD_HISTORICO FROM [FSOODB03\\SQL01].SIGMA90.dbo.VIEW_HISTORICO H2 WITH(NOLOCK) "); 
//	    sql.append(" WHERE H2.DT_RECEBIDO = H.DT_RECEBIDO "); 
//	    sql.append(" AND H2.CD_EVENTO='V120' AND H2.DT_VIATURA_NO_LOCAL IS NOT NULL ");
//	    sql.append(" AND H2.TX_OBSERVACAO_FECHAMENTO NOT LIKE '%Fechado automaticamente%' ");
//	    sql.append(" ) ");
	    sql.append(" AND SIG.USU_NUMPOS IN (SELECT MAX (sig2.usu_numpos) FROM usu_t160sig AS sig2 ");
	    sql.append(" inner join usu_t160cvs cvs2 on cvs2.usu_codemp = sig2.usu_codemp and cvs2.usu_codfil = sig2.usu_codfil and cvs2.usu_numctr = sig2.usu_numctr and cvs2.usu_numpos = sig2.usu_numpos ");
	    sql.append(" WHERE sig2.usu_codemp = sig.usu_codemp AND sig2.usu_codfil = sig.usu_codfil AND sig2.usu_numctr = sig.usu_numctr ");
	    sql.append(" and ((cvs2.usu_sitcvs = 'A') OR (cvs2.usu_sitcvs = 'I' and cvs2.usu_datfim >= GETDATE()))) ");
	    sql.append(" GROUP BY cvs.usu_codemp, cvs.usu_codfil, cvs.usu_numctr, cvs.usu_numpos, cli.CodCli,sig.usu_codcli,cvs.usu_sitcvs, cvs.usu_codser, cvs.usu_periss, cvs.usu_perirf, cvs.usu_perins, cvs.usu_perpit, cvs.usu_percsl, ");
	    sql.append(" cvs.usu_percrt, cvs.usu_perour, cvs.usu_ctafin, cvs.usu_ctared, cvs.usu_codccu, cvs.usu_datini, cvs.usu_tnsser, ");
	    sql.append(" H.CD_HISTORICO, H.DT_RECEBIDO, CEN.CD_CLIENTE, CEN.ID_CENTRAL, CEN.PARTICAO, CEN.RAZAO  ");
	    sql.append(" ORDER BY H.CD_HISTORICO ");

	    pstm = conn.prepareStatement(sql.toString());

	    GregorianCalendar dataInicio = new GregorianCalendar();
	    dataInicio.add(Calendar.DAY_OF_MONTH, -1); // -1
	    dataInicio.set(Calendar.HOUR_OF_DAY, 0);
	    dataInicio.set(Calendar.MINUTE, 0);
	    dataInicio.set(Calendar.SECOND, 0);
	    dataInicio.set(Calendar.MILLISECOND, 0);

	    GregorianCalendar dataFim = new GregorianCalendar();
	    dataFim.set(Calendar.HOUR_OF_DAY, 0);
	    dataFim.set(Calendar.MINUTE, 0);
	    dataFim.set(Calendar.SECOND, 0);
	    dataFim.set(Calendar.MILLISECOND, 0);

	    pstm.setTimestamp(1, new Timestamp(dataInicio.getTimeInMillis()));
	    pstm.setTimestamp(2, new Timestamp(dataFim.getTimeInMillis()));

	    rs = pstm.executeQuery();

	    /** Datas usadas a abertura de tarefas e apontamentos */

	    GregorianCalendar datSis = new GregorianCalendar();
	    GregorianCalendar dat1900 = new GregorianCalendar(1900, 11, 31);

	    GregorianCalendar datSisHorarioZerado = new GregorianCalendar();
	    datSisHorarioZerado.set(Calendar.HOUR_OF_DAY, 0);
	    datSisHorarioZerado.set(Calendar.MINUTE, 0);
	    datSisHorarioZerado.set(Calendar.SECOND, 0);
	    datSisHorarioZerado.set(Calendar.MILLISECOND, 0);

	    GregorianCalendar dataCompetencia = new GregorianCalendar();
	    dataCompetencia.add(Calendar.DAY_OF_MONTH, -1);
	    dataCompetencia.add(Calendar.MONTH, +1);
	    dataCompetencia.set(Calendar.DAY_OF_MONTH, 1);
	    dataCompetencia.set(Calendar.HOUR_OF_DAY, 0);
	    dataCompetencia.set(Calendar.MINUTE, 0);
	    dataCompetencia.set(Calendar.SECOND, 0);
	    dataCompetencia.set(Calendar.MILLISECOND, 0);

	    /**
	     * Lista que guardará os dados para o envio de e-mail ao fim da
	     * rotina
	     */

	    StringBuilder dadosApontamentoEmail = new StringBuilder();
	    StringBuilder dadosTarefaEmail = new StringBuilder();

	    /**
	     * Mandinga para colorir as linhas da tabela, visto que clientes 
	     * e-mails Microsoft processam pouco CSS
	     */
	    int contApontamentos = 0;

	    while (rs.next()) {

		Long codigoCliente = rs.getLong("CD_CLIENTE");
		Long historicoEvento = rs.getLong("CD_HISTORICO");
		
		String detalhesEvento = "Conta: "+rs.getString("ID_CENTRAL")+"["+rs.getString("PARTICAO")+"] "+rs.getString("RAZAO")+" Evento: AVTO Recebido em: "+NeoDateUtils.safeDateFormat(rs.getTimestamp("DT_RECEBIDO"), "dd/MM/yyyy HH:mm:ss");

		/** GERAR APONTAMENTO OU TAREFA */

		Long codEmp = rs.getLong("usu_codemp");
		Long codFil = rs.getLong("usu_codfil");
		Long numCtr = rs.getLong("usu_numctr");
		Long numPos = rs.getLong("usu_numpos");
		//Long codSerPosto = rs.getLong("usu_codser");
		
		

		/** VERIFICA HISTÓRICO DE ORDENS DE SERVIÇO */
		boolean historico = verificaHistorico(codigoCliente);
		
		String observacao = "";
		String tarefa = "";

		if (historico) {
		    
		    
		    if (rs.getString("usu_sitcvs").equals("I")) {
			tarefa = this.abrirTarefa(codEmp, codFil, numCtr, numPos, detalhesEvento);
			observacao = "Inserido via Fusion: Gerado cobrança(tarefa "+tarefa+"). Empresa " + codEmp + " Filial " + codFil + " Contrato " + numCtr 
				+ " Posto " + numPos;
			
		    }else{
			String codSer = "9002010";
			String cplCvs = "Serviço Extra - Vigilância Eletrônica - Atendimento VTO";
			Float perIss = rs.getFloat("usu_periss");
			Float perIrf = rs.getFloat("usu_perirf");
			Float perIns = rs.getFloat("usu_perins");
			Float perPit = rs.getFloat("usu_perpit");
			Float perCsl = rs.getFloat("usu_percsl");
			Float perCrt = rs.getFloat("usu_percrt");
			Float perOur = rs.getFloat("usu_perour");
			Long ctaFin = rs.getLong("usu_ctafin");
			Long ctaRed = rs.getLong("usu_ctared");
			String codCcu = rs.getString("usu_codccu");
			Date datInn = rs.getDate("usu_datini");
			String tnsSer = rs.getString("usu_tnsser");
			String obsCms = "Cobrança de Atendimento VTO (VTO) ";
			Float preUni;
			
			preUni = 50f;
			
			Long seqMov = this.getSeqMov();
			
			while (seqMov.equals(0L)) {
			    seqMov = this.getSeqMov();
			}
			observacao = "Inserido via Fusion: Gerado cobrança(apontamento). Empresa " + codEmp + " Filial " + codFil + " Contrato " + numCtr + " Posto " + numPos;
			this.gravarApontamento(codEmp, codFil, numCtr, numPos, seqMov, datSisHorarioZerado, codSer, cplCvs, preUni, perIss, perIrf, perIns, perPit, perCsl, perCrt, perOur, ctaFin, ctaRed, codCcu, datInn, dat1900, tnsSer, datSis, dataCompetencia, obsCms);			
		    }
		}else{
		    observacao = "Inserido via Fusion: Primeira cobrança(apontamento) do mês. Empresa " + codEmp + " Filial " + codFil + " Contrato " + numCtr + " Posto " + numPos;		    
		}

		this.gravarHistorico(codigoCliente, historicoEvento,detalhesEvento, tarefa, observacao);

		contApontamentos++;
		int resto = (contApontamentos) % 2;
		if (resto == 0) {
		    dadosApontamentoEmail.append("<tr style=\"background-color: #d6e9f9\" ><td>" + detalhesEvento + "</td><td>" + codEmp + "</td><td>" + codFil + "</td><td>" + numCtr + "</td><td>" + numPos + "</td></tr>");
		} else {
		    dadosApontamentoEmail.append("<tr><td>" + detalhesEvento + "</td><td>" + codEmp + "</td><td>" + codFil + "</td><td>" + numCtr + "</td><td>" + numPos + "</td></tr>");
		}

	    }

	    /** Envio de e-mail */

	    StringBuilder corpoEmail = new StringBuilder();

	    corpoEmail.append("<!DOCTYPE html>");
	    corpoEmail.append("<html>");
	    corpoEmail.append("<head>");
	    corpoEmail.append("<meta charset=\"UTF-8\">");
	    corpoEmail.append("<title>Relatório de cobrança VTO</title>");
	    corpoEmail.append("<style>");
	    corpoEmail.append("h2{");
	    corpoEmail.append("font-weight:bold; color:#303090; letter-spacing:1pt; word-spacing:2pt; font-size:20px; text-align:center; font-family:Verdana; line-height:1;");
	    corpoEmail.append("}");
	    corpoEmail.append(".table-p{");
	    corpoEmail.append("font-family: Verdana; font-weight:normal; font-size:12px; text-align:justify;");
	    corpoEmail.append(" }");
	    corpoEmail.append(".table-info{");
	    corpoEmail.append("table-layout: fixed;");
	    corpoEmail.append("text-align:left;");
	    corpoEmail.append("font-family: Verdana;");
	    corpoEmail.append("}");
	    corpoEmail.append(".table-info td{");
	    corpoEmail.append("overflow-x: hidden;");
	    corpoEmail.append(" }");
	    corpoEmail.append(".table-info thead{");
	    corpoEmail.append("background-color:#303090;");
	    corpoEmail.append("color: white;");
	    corpoEmail.append("font-weight:bold;");
	    corpoEmail.append("	}");
	    corpoEmail.append("</style>");
	    corpoEmail.append("</head>");
	    corpoEmail.append("<body>");
	    corpoEmail.append("<div>");
	    corpoEmail.append("<table width=\"600\" align=\"center\">");
	    corpoEmail.append("<tr><td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/header.jpg\" width=\"600\" height=\"145\"></td></tr>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("<td><h2>Relatório de Cobrança de eventos VTO</h2></td>");
	    corpoEmail.append("<table width=\"600\" align=\"center\" class=\"table-p\"> ");
	    corpoEmail.append("<tr><td>A(s) OS(s) abaixo gerou(ram) apontamento(s) conforme listagem:</td></tr>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("<table width=\"600\" align=\"center\" class=\"table-info\"> ");
	    corpoEmail.append("<thead>");
	    corpoEmail.append("<tr>");
	    corpoEmail.append("<td>OS</td>");
	    corpoEmail.append("<td>Empresa</td>");
	    corpoEmail.append("<td>Filial</td>");
	    corpoEmail.append("<td>Contrato</td>");
	    corpoEmail.append("<td>Posto</td>");
	    corpoEmail.append("</tr>");
	    corpoEmail.append("</thead>");
	    corpoEmail.append("<tbody>");
	    corpoEmail.append("<!-- Recebe dados do método -->");
	    corpoEmail.append(dadosApontamentoEmail);
	    corpoEmail.append("<br>");
	    corpoEmail.append("</tbody>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("<table width=\"600\" align=\"center\" class=\"table-p\"> ");
	    corpoEmail.append("<tr><td>A(s) OS(s) abaixo gerou(ram) tarefa(s), devido a não possuir(em) posto ativo para cobrança ou cliente ter exceção de cobrança, conforme listagem:</td></tr>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("<table width=\"600\" align=\"center\" class=\"table-info\">");
	    corpoEmail.append("<thead>");
	    corpoEmail.append("<tr>");
	    corpoEmail.append("<td>OS</td>");
	    corpoEmail.append("<td>Empresa</td>");
	    corpoEmail.append("<td>Filial</td>");
	    corpoEmail.append("<td>Contrato</td>");
	    corpoEmail.append("<td>Posto</td>");
	    corpoEmail.append("</tr>");
	    corpoEmail.append("</thead>");
	    corpoEmail.append("<tbody>");
	    corpoEmail.append("<!-- Recebe dados do método -->");
	    corpoEmail.append(dadosTarefaEmail);
	    corpoEmail.append("</tbody>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("<br>");
	    corpoEmail.append("<!-- Rodapé do e-mail -->");
	    corpoEmail.append("<div class=\"rodape\">");
	    corpoEmail.append("<table width=\"600\" align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
	    corpoEmail.append("<tbody>");
	    corpoEmail.append("<tr>");
	    corpoEmail.append("<td width=\"360\" valign=\"bottom\"><img src=\"https://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/footer-1.jpg\"></td>");
	    corpoEmail.append("<td><a href=\"http://www.orsegups.com.br\" target=\"_blank\"><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/footer-2.jpg\" width=\"260\" height=\"120\" alt=\"\"/></a></td>");
	    corpoEmail.append("</tr>");
	    corpoEmail.append("</tbody>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("</div>");
	    corpoEmail.append("</div>");
	    corpoEmail.append("</body>");
	    corpoEmail.append("</html>");

	    String[] addTo = new String[] { "caroline.wrunski@orsegups.com.br", "bruno.brasil@orsegups.com.br" };
	    String[] comCopia = new String[] { "fernanda.maciel@orsegups.com.br", "emailautomatico@orsegups.com.br" };
//	    String[] addTo = new String[] { "mateus.batista@orsegups.com.br" };
//	    String[] comCopia = new String[] { "lucas.alison@orsegups.com.br" };
	    String from = "fusion@orsegups.com.br";
	    String subject = "Relatório de cobrança VTO";
	    String html = corpoEmail.toString();

	    OrsegupsUtils.sendEmail2Orsegups(addTo, from, subject, html, comCopia);

	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("ERRO AGENDADOR DE TAREFA: RotinaCobrancaOS - ERRO AO EXECUTAR ROTINA -- Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

    }

    private boolean verificaHistorico(Long codigoCliente) {

	boolean resultado = false;
	
	GregorianCalendar hoje = new GregorianCalendar();
	hoje.add(Calendar.DAY_OF_MONTH, -1);
	
	String competencia = (hoje.get(Calendar.MONTH)+1)+"/"+hoje.get(Calendar.YEAR);
	
	QLGroupFilter filter = new QLGroupFilter("AND");
	filter.addFilter(new QLEqualsFilter("codigoCliente", codigoCliente));
	filter.addFilter(new QLEqualsFilter("competencia", competencia));

	List<NeoObject> listaHistorico = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("VTOHistoricoCobranca"), filter);

	if (listaHistorico != null && !listaHistorico.isEmpty()) {

	    resultado = true;

	}

	return resultado;
    }

    /**
     * Metodo de abertura de tarefas
     * 
     * @param codEmp
     * @param codFil
     * @param numCtr
     * @param numPos
     * @param numeroOS
     * @return numero da Tarefa
     */
    private String abrirTarefa(Long codEmp, Long codFil, Long numCtr, Long numPos, String detalhesEvento) {
	String tarefa = "";

	String solicitante = "fernanda.maciel";

	String executor = OrsegupsUtils.getUserNeoPaper("ResponsavelCobrancaOS");

	GregorianCalendar prazo = new GregorianCalendar();
	prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
	prazo.set(Calendar.HOUR_OF_DAY, 23);
	prazo.set(Calendar.MINUTE, 59);
	prazo.set(Calendar.SECOND, 59);

	String titulo = "Apontamento/Cobrança referente ao evento " + detalhesEvento;

	String descricao = "Contrato: " + numCtr + " Empresa: " + codEmp + " Filial: " + codFil + " Posto: " + numPos + " ,necessita de apontamento/cobrança referente a Ordem de Serviço número: <br>"
	+ detalhesEvento;

	IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();

	tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "hadouken", prazo);

	return tarefa;
    }

    /**
     * Grava histórico da cobrança
     * 
     * @param codigoCliente
     * @param detalhesEvento
     * @param tarefa
     * @param observacao
     */
    private void gravarHistorico(Long codigoCliente, Long historicoEvento,String detalhesEvento, String tarefa, String observacao) {
	InstantiableEntityInfo historico = AdapterUtils.getInstantiableEntityInfo("VTOHistoricoCobranca");
	NeoObject objHistorico = historico.createNewInstance();
	EntityWrapper wrapperHistorico = new EntityWrapper(objHistorico);

	wrapperHistorico.findField("codigoCliente").setValue(codigoCliente);
	wrapperHistorico.findField("historicoEvento").setValue(historicoEvento);
	wrapperHistorico.findField("detalhesEvento").setValue(detalhesEvento);
	wrapperHistorico.findField("observacao").setValue(observacao);
	
	GregorianCalendar hoje = new GregorianCalendar();
	hoje.add(Calendar.DAY_OF_MONTH, -1);
	
	String competencia = (hoje.get(Calendar.MONTH)+1)+"/"+hoje.get(Calendar.YEAR);
	
	wrapperHistorico.findField("competencia").setValue(competencia);
	if (tarefa != null && !tarefa.isEmpty()) {
	    wrapperHistorico.findField("tarefa").setValue(Long.valueOf(tarefa));
	}

	PersistEngine.persist(objHistorico);
    }

    /**
     * Retorna o número o maior número sequencial de movimento
     * 
     * @return long seqMov
     */
    private Long getSeqMov() {
	Long seqMov = 0L;

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	try {
	    conn = PersistEngine.getConnection("SAPIENS");

	    StringBuilder sqlSeqMov = new StringBuilder();
	    sqlSeqMov.append("SELECT MAX(ISNULL(USU_SeqMov,0))+1 AS seqMov FROM USU_T160CMS");

	    pstm = conn.prepareStatement(sqlSeqMov.toString());

	    rs = pstm.executeQuery();

	    if (rs.next()) {
		seqMov = rs.getLong("seqMov");
		return seqMov;
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

	return seqMov;
    }

    /**
     * Gravar apontamento para cobrança da OS
     * 
     * @param codEmp
     * @param codFil
     * @param numCtr
     * @param numPos
     * @param seqMov
     * @param datSisHorarioZerado
     * @param codSer
     * @param cplCvs
     * @param preUni
     * @param perIss
     * @param perIrf
     * @param perIns
     * @param perPit
     * @param perCsl
     * @param perCrt
     * @param perOur
     * @param ctaFin
     * @param ctaRed
     * @param codCcu
     * @param datInn
     * @param dat1900
     * @param tnsSer
     * @param datSis
     * @param dataCompetencia
     * @param obsCms
     */
    private void gravarApontamento(Long codEmp, Long codFil, Long numCtr, Long numPos, Long seqMov, GregorianCalendar datSisHorarioZerado, String codSer, String cplCvs, Float preUni, Float perIss, Float perIrf, Float perIns, Float perPit, Float perCsl, Float perCrt, Float perOur, Long ctaFin,
	    Long ctaRed, String codCcu, Date datInn, GregorianCalendar dat1900, String tnsSer, GregorianCalendar datSis, GregorianCalendar dataCompetencia, String obsCms) {

	Connection conn = null;
	PreparedStatement pstm = null;

	try {

	    conn = PersistEngine.getConnection("SAPIENS");
	    StringBuffer sqlApontamento = new StringBuffer();

	    sqlApontamento.append(" INSERT INTO USU_T160CMS VALUES (?,?,?,?,?,?,?,?,'UN',0,1,?,?,?,?,?,?,?,?,?,?,?,?,?,?,100,?,?,0,'',0,?,'+',?,30,'N','S','M',0,?,'N',4) ");

	    pstm = conn.prepareStatement(sqlApontamento.toString());

	    pstm.setLong(1, codEmp);
	    pstm.setLong(2, codFil);
	    pstm.setLong(3, numCtr);
	    pstm.setLong(4, numPos);
	    pstm.setLong(5, seqMov);

	    pstm.setTimestamp(6, new Timestamp(datSisHorarioZerado.getTimeInMillis()));
	    pstm.setString(7, codSer);
	    pstm.setString(8, cplCvs);
	    pstm.setFloat(9, preUni);
	    pstm.setFloat(10, perIss);
	    pstm.setFloat(11, perIrf);
	    pstm.setFloat(12, perIns);
	    pstm.setFloat(13, perPit);
	    pstm.setFloat(14, perCsl);
	    pstm.setFloat(15, perCrt);
	    pstm.setFloat(16, perOur);
	    pstm.setLong(17, ctaFin);
	    pstm.setLong(18, ctaRed);
	    pstm.setString(19, codCcu);
	    pstm.setTimestamp(20, new Timestamp(datInn.getTime()));
	    pstm.setTimestamp(21, new Timestamp(dat1900.getTimeInMillis()));
	    pstm.setString(22, tnsSer);
	    pstm.setTimestamp(23, new Timestamp(datSisHorarioZerado.getTimeInMillis()));
	    pstm.setLong(24, datSis.get(Calendar.HOUR_OF_DAY) * 60 + datSis.get(Calendar.MINUTE));
	    pstm.setTimestamp(25, new Timestamp(dataCompetencia.getTimeInMillis()));
	    pstm.setString(26, obsCms);
	    pstm.setTimestamp(27, new Timestamp(dat1900.getTimeInMillis()));

	    pstm.execute();
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);

	}
    }

}
