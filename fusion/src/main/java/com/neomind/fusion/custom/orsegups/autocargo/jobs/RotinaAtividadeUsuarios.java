package com.neomind.fusion.custom.orsegups.autocargo.jobs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.autocargo.beans.user.User;
import com.neomind.fusion.custom.orsegups.autocargo.beans.user.Users;
import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.ti.sigma.engine.SIGMAUtilsEngine;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class RotinaAtividadeUsuarios implements CustomJobAdapter { // FalhaComunicacaoVeiculos

    private final Log log = LogFactory.getLog(RotinaAtividadeUsuarios.class);

    private Long key = GregorianCalendar.getInstance().getTimeInMillis();

    private SIGMAUtilsEngine engine = new SIGMAUtilsEngine();

    /**
     * Clientes que não utilizam o sistema a mais de 45 dias.
     * <tt>Razão social</tt> é a chave do mapa
     */
    private Map<String, User> activities = new HashMap<String, User>();

    /**
     * Contas <tt></tt> que possuem eventos R003 abertos <br>
     * Chave: nome Fantasia
     */
    private Map<String, EventoVO> activitiesOpenEvents = new HashMap<String, EventoVO>();

    /**
     * Número total de páginas recebidos da API de consulta de falhas de eventos
     */
    private int totalPageNumber = 1;

    /**
     * Inconsistencias na inserção de eventos
     */
    private List<String> inconsistencias = new ArrayList<String>();

    //SOLICITADO ATRAVES DA TAREFA SIMPLES 1260261 
    private final String[] contasCorportavias = {"R1321","R1043","R1222","R1373","R1016","R1368","R1017","R1369","R1366","R1022","R1372","R1048","R951","R1583","R1455","R1433","R1147","R930"};
    
    public void execute(CustomJobContext ctx) {
	
		
	for (int i = 0; i < totalPageNumber; i++) {

	    try {
		this.getActivities(i);
	    } catch (RuntimeException e) {
		this.logError(e.getMessage(), "Erro ao acessar API", "Orsegups");
		log.error("Erro ao acessar a API Autocargo ACEngineAtividadeUsuarios [" + key + "] :");
		e.printStackTrace();
		throw new JobException("Erro ao acessar API de consulta de atividade de usuários Autocargo! Procurar no log por [" + key + "]");
	    }

	    try {
		Thread.sleep(1000);
	    } catch (InterruptedException e) {
		e.printStackTrace();
	    }
	}

	boolean getEvents = this.getActivitiesOpenEvents();

	int countTry = 0;

	while (!getEvents && countTry < 2) {
	    getEvents = this.getActivitiesOpenEvents();
	    countTry++;
	}

	if (!this.activities.isEmpty()) {
	    for (String key : activities.keySet()) {
		
		if (activities.get(key).getSign_in_count() == 0L) {
		    continue;
		} else if (this.isClienteExcecao(activities.get(key).getUsername())) {
		    continue;
		} else if (activitiesOpenEvents.containsKey(key)) {
		    /* Já possui evento aberto, não faço nada */
		    activitiesOpenEvents.remove(key);
		} else {

		    EventoVO vo = this.getDadosConta(key);

		    String retorno = "Cliente não encontrado";

		    User u = activities.get(key);
		    String cliente = "Cliente: " + u.getClient_name();

		    String detalhes = " <br/>Usuário: ";

		    if (u.getUsername() != null && !u.getUsername().isEmpty()) {
			detalhes += u.getUsername();
		    } else {
			detalhes += "Não informado";
		    }

		    String ultimoLogin = " <br/>Ultimo login: ";

		    if (u.getSign_in_at() != null && !u.getSign_in_at().isEmpty()) {
			ultimoLogin += u.getSign_in_at();
		    } else {
			ultimoLogin += "Não informado";
		    }

		    if (vo != null) {
			
			String conta = vo.getCodigoCentral();
			boolean contaCorporativa = false;
			
			for (String c : contasCorportavias) {
			    if (conta.equalsIgnoreCase(c)){
				contaCorporativa = true;
				break;
			    }
			}
			
			if (!contaCorporativa){
			    continue;
			}
			
			if (vo.getTipoPessoa() == 0){
			    retorno = engine.receberEventoByIdentificadorCliente(vo.getCodigoCentral(), vo.getEmpresa(), vo.getParticao(), "R003");			   
			}else{
			    retorno = "Cliente não é pessoa jurídica";
			}
		    }

		    if (retorno.equals("ACK")) {
			boolean ok = this.adicionarLogEvento(vo.getCodigoCliente(), detalhes + ultimoLogin);

			int count = 0;

			while (!ok && count < 2) {
			    ok = this.adicionarLogEvento(vo.getCodigoCliente(), detalhes + ultimoLogin);
			    count++;
			}

		    } else {
			this.logError("R003", retorno, cliente);
			if (!retorno.equals("Cliente não é pessoa jurídica")){
			    this.inconsistencias.add(retorno + ";" + cliente + detalhes + ultimoLogin);			    
			}
		    }

		}
	    }
	}

	this.sendEmail();

    }

    private boolean isClienteExcecao(String username){
	
	List<NeoObject> lista = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("AutocargoExcecaoClientes"), new QLEqualsFilter("nomeUsuario", username));
	
	if (lista != null && !lista.isEmpty()) {

	    GregorianCalendar agora = new GregorianCalendar();
	    
	    EntityWrapper wrappperExcecao = new EntityWrapper(lista.get(0));

	    GregorianCalendar novaVerificacao = (GregorianCalendar) wrappperExcecao.findField("novaVerificacao").getValue();
	    
	    if (!agora.after(novaVerificacao)){
		return true;		
	    }

	}
	
	return false;
    }
    
    private boolean adicionarLogEvento(String codigoCliente, String detalhes) {
	
	String sql = " UPDATE HISTORICO SET TX_OBSERVACAO_FECHAMENTO = ? WHERE CD_EVENTO='R003'  AND CD_CLIENTE= ? ";

	Connection conn = null;
	PreparedStatement ps = null;

	try {

	    conn = PersistEngine.getConnection("SIGMA90");
	    ps = conn.prepareStatement(sql);
	    
	    ps.setString(1, detalhes);
	    ps.setInt(2, Integer.parseInt(codigoCliente));
	    
	    int rs = ps.executeUpdate();

	    if (rs > 0) {
		return true;
	    }

	    

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, ps, null);
	}

	
	return false;
    }

    /**
     * Popula o set {@link #activitiesOpenEvents}
     * 
     * @return <tt>true</tt> se executou com sucesso <tt>false</tt> caso
     *         contrário
     */
    private boolean getActivitiesOpenEvents() {

	boolean executouComSucesso = false;

	StringBuilder sql = new StringBuilder();

	sql.append(" SELECT C.NM_RASTREADOR, C.ID_CENTRAL, C.PARTICAO, C.RAZAO, C.CD_CLIENTE FROM HISTORICO H WITH(NOLOCK) ");
	sql.append(" INNER JOIN dbCENTRAL C WITH(NOLOCK) ON C.CD_CLIENTE = H.CD_CLIENTE ");
	sql.append(" WHERE H.CD_EVENTO = 'R003' ");
	sql.append(" ORDER BY DT_PROCESSADO DESC ");

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	try {

	    conn = PersistEngine.getConnection("SIGMA90");
	    stmt = conn.createStatement();
	    rs = stmt.executeQuery(sql.toString());

	    while (rs.next()) {

		EventoVO novoEvento = new EventoVO();

		novoEvento.setCodigoCentral(rs.getString(2));
		novoEvento.setParticao(rs.getString(3));
		novoEvento.setRazao(rs.getString(4));

		this.activitiesOpenEvents.put(rs.getString(4), novoEvento);
	    }

	    executouComSucesso = true;

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	return executouComSucesso;

    }

    /**
     * Popula o mapa {@link #miscommunication}
     * 
     * @param pageNumber
     *            {@link #totalPageNumber} Não é necessário modificar
     */
    private void getActivities(int pageNumber) {

	StringBuilder retorno = new StringBuilder();

	try {

	    URL url = new URL("https://www2.autocargo.com.br/api/integration/v1/user_activities?days_ago=45&page="+pageNumber);
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setRequestMethod("GET");
	    conn.setRequestProperty("Authorization", "Token token=\"115fe7a39c6d1910308c335ba3f845a0f9d6495ee912068b9cf7a90f717f03a5\"");
	    conn.setRequestProperty("Content-Type", "application/json");
	    conn.setRequestProperty("Accept", "application/json");

	    if (conn.getResponseCode() != 200) {
		throw new RuntimeException(String.valueOf(conn.getResponseCode()));
	    }

	    String totalPorPagina = conn.getHeaderField("X-Autocargo-Per-Page");
	    String totalResultados = conn.getHeaderField("X-Autocargo-Total");

	    int total = Integer.parseInt(totalPorPagina);

	    int result = this.roundUp(Integer.parseInt(totalResultados), total);

	    this.totalPageNumber = result / total;

	    BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream()), "UTF-8"));

	    String output;

	    while ((output = br.readLine()) != null) {
		retorno.append(output + "\n");
	    }

	    conn.disconnect();

	} catch (MalformedURLException e) {

	    e.printStackTrace();

	} catch (IOException e) {

	    e.printStackTrace();

	}

	System.out.println(retorno.toString());
	Gson gson = new Gson();
	Users resultList = gson.fromJson(retorno.toString(), Users.class);

	for (User u : resultList.getUsers()) {
	    if (u.getClient_name() != null && !u.getClient_name().equalsIgnoreCase("null")){
		activities.put(u.getClient_name(), u);		
	    }
	}

    }

    private int roundUp(int toRound, int roundTo) {

	if (toRound % roundTo == 0) {
	    return toRound;
	} else {
	    return (roundTo - toRound % roundTo) + toRound;
	}

    }

    private EventoVO getDadosConta(String razao) {

	Connection conn = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	sql.append(" SELECT ID_CENTRAL, PARTICAO, ID_EMPRESA, CD_CLIENTE, TP_PESSOA FROM dbCENTRAL WITH(NOLOCK) ");
	sql.append(" WHERE RAZAO = ? ");
	sql.append(" AND (PARTICAO='000' OR PARTICAO='001') ");
	sql.append(" AND ID_EMPRESA IN (10075, 10129, 10127, 10144) ");
	sql.append(" ORDER BY ID_CENTRAL, PARTICAO ");

	EventoVO retorno = null;

	try {
	    conn = PersistEngine.getConnection("SIGMA90");

	    ps = conn.prepareStatement(sql.toString());

	    ps.setString(1, razao);

	    rs = ps.executeQuery();

	    if (rs.next()) {
		retorno = new EventoVO();

		retorno.setCodigoCentral(rs.getString(1));
		retorno.setParticao(rs.getString(2));
		retorno.setEmpresa(rs.getString(3));
		retorno.setCodigoCliente(rs.getString(4));
		retorno.setTipoPessoa(rs.getInt(5));
		
		return retorno;

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, ps, rs);
	}

	return retorno;

    }

    private void logError(String codigoEvento, String erro, String cliente) {

	Connection conn = null;
	PreparedStatement pstm = null;

	String sql = "INSERT INTO AtividadeUsuariosLogs (CODIGO_EVENTO, ERRO, CLIENTE, DATA_ERRO) VALUES (?,?,?,?)";

	try {
	    conn = PersistEngine.getConnection("TIDB");

	    pstm = conn.prepareStatement(sql);

	    pstm.setString(1, codigoEvento);
	    pstm.setString(2, erro);
	    pstm.setString(3, cliente);
	    pstm.setTimestamp(4, new Timestamp(Calendar.getInstance().getTimeInMillis()));

	    pstm.executeUpdate();

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}

    }

    /**
     * Encaminha e-mail de informe de inconsistências
     */
    private void sendEmail() {

	if (!this.inconsistencias.isEmpty()) {

	    StringBuilder dados = new StringBuilder();

	    int cont = 0;

	    for (String i : this.inconsistencias) {

		String a[] = i.split(";");

		int resto = (cont) % 2;
		if (resto == 0) {
		    dados.append("<tr style=\"background-color: #d6e9f9\" ><td>" + a[0] + "</td><td>" + a[1] + "</td></tr>");
		} else {
		    dados.append("<tr><td>" + a[0] + "</td><td>" + a[1] + "</td></tr>");
		}

		cont++;
	    }

	    StringBuilder corpoEmail = new StringBuilder();

	    corpoEmail.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
	    corpoEmail.append("<html>");
	    corpoEmail.append("<head>");
	    corpoEmail.append("<meta charset=\"ISO-8859-1\">");
	    corpoEmail.append("<title>Relatório de inconsistências</title>");
	    corpoEmail.append("<style>");
	    corpoEmail.append("h2{");
	    corpoEmail.append("font-weight:bold; color:#303090; letter-spacing:1pt; word-spacing:2pt; font-size:20px; text-align:center; font-family:Verdana; line-height:1;");
	    corpoEmail.append("}");
	    corpoEmail.append(".table-p{");
	    corpoEmail.append("font-family: Verdana; font-weight:normal; font-size:12px; text-align:justify;");
	    corpoEmail.append(" }");
	    corpoEmail.append(".table-info{");
	    corpoEmail.append("table-layout: fixed;");
	    corpoEmail.append("text-align:left;");
	    corpoEmail.append("font-family: Verdana;");
	    corpoEmail.append("}");
	    corpoEmail.append(".table-info td{");
	    corpoEmail.append("overflow-x: hidden;");
	    corpoEmail.append(" }");
	    corpoEmail.append(".table-info thead{");
	    corpoEmail.append("background-color:#303090;");
	    corpoEmail.append("color: white;");
	    corpoEmail.append("font-weight:bold;");
	    corpoEmail.append("	}");
	    corpoEmail.append("</style>");
	    corpoEmail.append("</head>");
	    corpoEmail.append("<body>");
	    corpoEmail.append("<div>");
	    corpoEmail.append("<table width=\"600\" align=\"center\">");
	    corpoEmail.append("<tr><td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/header.jpg\" width=\"600\" height=\"145\"></td></tr>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("<td><h2>Inconsistências de não utilização de sistema</h2></td>");
	    corpoEmail.append("<table width=\"600\" align=\"center\" class=\"table-p\"> ");
	    corpoEmail.append("<tr><td>As seguintes inconsistências ocorreram durante a inserção de eventos de não utilização de sistema:</td></tr>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("<table width=\"600\" align=\"center\" class=\"table-info\"> ");
	    corpoEmail.append("<thead>");
	    corpoEmail.append("<tr>");
	    corpoEmail.append("<td>Inconsistência</td>");
	    corpoEmail.append("<td>Cliente</td>");
	    corpoEmail.append("</tr>");
	    corpoEmail.append("</thead>");
	    corpoEmail.append("<tbody>");
	    corpoEmail.append("<!-- Recebe dados do método -->");
	    corpoEmail.append(dados);
	    corpoEmail.append("<br>");
	    corpoEmail.append("</tbody>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("<br>");
	    corpoEmail.append("<!-- Rodapé do e-mail -->");
	    corpoEmail.append("<div class=\"rodape\">");
	    corpoEmail.append("<table width=\"600\" align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
	    corpoEmail.append("<tbody>");
	    corpoEmail.append("<tr>");
	    corpoEmail.append("<td width=\"360\" valign=\"bottom\"><img src=\"https://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/footer-1.jpg\"></td>");
	    corpoEmail
		    .append("<td><a href=\"http://www.orsegups.com.br\" target=\"_blank\"><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/footer-2.jpg\" width=\"260\" height=\"120\" alt=\"\"/></a></td>");
	    corpoEmail.append("</tr>");
	    corpoEmail.append("</tbody>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("</div>");
	    corpoEmail.append("</div>");
	    corpoEmail.append("</body>");
	    corpoEmail.append("</html>");

	    String[] addTo = new String[] { "gustavo.freitas@orsegups.com.br" };
	    String[] comCopia = new String[] { "emailautomatico@orsegups.com.br" };
	    String from = "fusion@orsegups.com.br";
	    String subject = "Relatório de Inconsistência de inserção de eventos de não utilização de sistema";
	    String html = corpoEmail.toString();

	    OrsegupsUtils.sendEmail2Orsegups(addTo, from, subject, html, comCopia);

	}

    }

}
