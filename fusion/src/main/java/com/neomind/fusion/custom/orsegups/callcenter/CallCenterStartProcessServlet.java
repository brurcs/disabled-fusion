package com.neomind.fusion.custom.orsegups.callcenter;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.emailmonitor.vo.ResultVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.TaskInstance;
import com.neomind.fusion.workflow.TaskInstanceHelper;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

@WebServlet(name="CallCenterStartProcessServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.callcenter.CallCenterStartProcessServlet"}, asyncSupported=true)
public class CallCenterStartProcessServlet extends HttpServlet{
    	

    	private static final long serialVersionUID = 1L;

	// for Preflight
        @Override
        protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	setAccessControlHeaders(resp);
    	resp.setStatus(HttpServletResponse.SC_OK);
        }
        
        private void setAccessControlHeaders(HttpServletResponse resp) {
            resp.setHeader("Access-Control-Allow-Origin", "*");
            resp.setHeader("Access-Control-Allow-Methods", "GET");
            resp.setHeader("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept, X-AUTH-TOKEN, Authorization");
        }

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{	
	    	setAccessControlHeaders(resp);
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	private void doRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		resp.setContentType("text/plain");
		resp.setCharacterEncoding("ISO-8859-1");

		String processType = req.getParameter("processType");
		String codigoCliente = req.getParameter("codigoCliente");
		String requesterCallCenter = req.getParameter("requesterCallCenter");
		
		String result = "0;Erro ao iniciar processo!";
	
		NeoUser requester = null;
		
		if (requesterCallCenter != null){
		    requester = (NeoUser) PersistEngine.getNeoObject(NeoUser.class, new QLEqualsFilter("code", requesterCallCenter));;
		}else{
		    requester =  PortalUtil.getCurrentUser();
		}
		if (requester == null){
			requester = (NeoUser) PersistEngine.getNeoObject(NeoUser.class, new QLEqualsFilter("code", "sistemaFusion"));
		}

		String processName = "";
		String eformTitle = "";
		NeoObject eform = null;
		EntityWrapper eformWrapper = null;

		if (processType != null && processType.equalsIgnoreCase("RRC"))
		{
			processName = "Q005 - RRC - Relatório de Reclamação de Cliente";
			eformTitle = "RRCRelatorioReclamacaoCliente";
			eform = AdapterUtils.createNewEntityInstance(eformTitle);

			eformWrapper = new EntityWrapper(eform);

			//define dados formulario
			this.setClienteSapiens(codigoCliente, eformWrapper);
			this.setOrigem(eformWrapper);

		}
		else if (processType != null && processType.equalsIgnoreCase("RSC"))
		{
			processName = "C027 - RSC - Relatório de Solicitação de Cliente Novo";
			eformTitle = "RSCRelatorioSolicitacaoClienteNovo";
			eform = AdapterUtils.createNewEntityInstance(eformTitle);

			eformWrapper = new EntityWrapper(eform);
			
			//define dados formulario
			this.setClienteSapiens(codigoCliente, eformWrapper);
			this.setOrigem(eformWrapper);
		}
		else if (processType != null && processType.equalsIgnoreCase("OsSigma"))
		{
			processName = "Abertura OS Sigma";
			eformTitle = "AberturaOsSigma";
			eform = AdapterUtils.createNewEntityInstance(eformTitle);

			eformWrapper = new EntityWrapper(eform);
			this.setSigmaOSData(codigoCliente, eformWrapper);
		}
		else
		{
			result = "0;<b>Não foi possível iniciar o processo! - (" + processType + ")</b>";
		}

		if (processName != null && !processName.isEmpty() && eform != null)
		{
			

			ResultVO resultVO = OrsegupsUtils.iniciaWorkflow(eform, processName, requester, false, false);

			if (resultVO != null && !resultVO.getResult())
			{
				result = "0;<b>" + resultVO.getMessage().replaceAll("\\n", "<br>") + "</b>";
			}
			else
			{
				Set<Activity> acts = resultVO.getWfProcess().getTaskSet();
				TaskInstance tarefaInicial = null;
				Task tarefaAlvo = null;
				for(Activity a : acts)
				{
					tarefaInicial = TaskInstanceHelper.findTaskInstance(a);
					
					if(tarefaInicial == null)
					{
						List<Task> tarefas = PersistEngine.getObjects(Task.class, new QLEqualsFilter("activity.neoId", a.getNeoId()));
						for(Task t : tarefas)
						{
							tarefaInicial = TaskInstanceHelper.findTaskInstance(t);
							tarefaAlvo = t;
							if(tarefaInicial == null)
							{
								//tarefaInicial = TaskInstanceHelper.assign(t);
							}
						}

					}
				}
				
				String linkAbrir = "";

				if (tarefaInicial != null)
				{
					///Task?type=Task&showSummaryBtn=true&edit=true&readOnly=false&taskInstanceId=80696&openSummary=false
					//linkAbrir = PortalUtil.getBaseURL() + "portal/render/Task?type=Task&id=TASK_ID&edit=true&select=true&root=true&form=true&full=true&mutable=false&toolbar=true";
					linkAbrir = PortalUtil.getBaseURL() + "portal/render/Task?type=Task&showSummaryBtn=true&edit=true&readOnly=false&taskInstanceId=TASKINSTANCE_ID&openSummary=false";
					linkAbrir = linkAbrir.replaceAll("TASKINSTANCE_ID", String.valueOf(tarefaInicial.getId()));
				}else if(tarefaAlvo != null)
				{
					linkAbrir = PortalUtil.getBaseURL() + "portal/render/Task?type=Task&id=TASK_ID&edit=true&select=true&root=true&form=true&full=true&mutable=false&toolbar=true";
					linkAbrir = linkAbrir.replaceAll("TASK_ID", String.valueOf(tarefaAlvo.getNeoId()));
				}

				if (linkAbrir != null && !linkAbrir.isEmpty())
				{
					//result = "<iframe class='work_col' id='processIframe' align='top'scrolling='no' frameborder='0' style='left: 0px; top: 0px;' src='" + linkAbrir + "'></iframe";
					result = "1;"+linkAbrir;
				}
			}
		}

		final PrintWriter out = resp.getWriter();
		out.print(result);
		out.close();
	}

	private void setClienteSapiens(String codigoCliente, EntityWrapper entityWrapper)
	{
		//define o cliente SAPIENS dentro do formulari odo processo
		if (codigoCliente != null && !codigoCliente.isEmpty())
		{
			Long codcli = Long.valueOf(codigoCliente);

			List<NeoObject> clientesSapiens = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENS_Clientes"), new QLEqualsFilter("codcli", codcli));
			if (clientesSapiens != null && !clientesSapiens.isEmpty())
			{
				NeoObject clienteSapiens = clientesSapiens.get(0);

				entityWrapper.setValue("clienteSapiens", clienteSapiens);
			}
		}
	}

	private void setOrigem(EntityWrapper entityWrapper)
	{
		List<NeoObject> origemList = PersistEngine.getObjects(AdapterUtils.getEntityClass("RRCOrigem"), new QLEqualsFilter("sequencia", "01"));
		if (origemList != null && !origemList.isEmpty())
		{
			NeoObject origem = origemList.get(0);

			entityWrapper.setValue("origem", origem);
		}
	}
	
	private void setSigmaOSData(String codigoCliente, EntityWrapper entityWrapper)
	{
		if (codigoCliente != null && !codigoCliente.isEmpty())
		{
			Long cdCliente = Long.valueOf(codigoCliente);
			
			List<NeoObject> contasSigma = PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMACENTRAL"), new QLEqualsFilter("cd_cliente", cdCliente));
			if(contasSigma != null && !contasSigma.isEmpty())
			{
				try{
				entityWrapper.setValue("Conta", contasSigma.get(0));
				
				
				//EMBALADO CONTA DO CLIENTE
				
				EntityWrapper wrapperConta = new EntityWrapper(contasSigma.get(0));
				
				Long colEquip = (Long) wrapperConta.findField("cd_tecnico_responsavel").getValue();
								
				NeoObject colaborador = null;

				QLEqualsFilter colaboradorFilter = new QLEqualsFilter("cd_colaborador", colEquip);
				List<NeoObject> colaboradores = PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMACOLABORADOR"), colaboradorFilter);

				if (colaboradores != null && !colaboradores.isEmpty())
				{
					colaborador = colaboradores.get(0);
				}
				
				//entityWrapper.setValue("TecnicoResponsavel", OrsegupsUtils.getInstaladorSigmaPadrao());
				entityWrapper.setValue("TecnicoResponsavel", colaborador);
				entityWrapper.setValue("Defeito", OrsegupsUtils.getOsDefeitoPadraoSigma(contasSigma.get(0)));
				}catch(Exception e){
					e.printStackTrace();
					System.out.println("Erro CallCenterStartProcessServlet: "+e.getMessage());
				}
			}
		}
		
		QLEqualsFilter osSolicitanteFilter = new QLEqualsFilter("cd_os_solicitante", 10030L); // 3-SOLICITADO PELO CLIENTE
		List<NeoObject> osSolicitantes = PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMAODSOLICITANTE"), osSolicitanteFilter);
		if(osSolicitantes != null && !osSolicitantes.isEmpty())
		{
			entityWrapper.setValue("Solicitante", osSolicitantes.get(0));
		}
		
	}
}
