package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class RESTarefaRegistroConversar implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(TarefaRegistroAtividades.class);

	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		try
		{	NeoUser usuarioResponsavel = origin.getUser();
			if (activity.getName().equals("Historico Conversa Aprovar Presidente")) {
				InstantiableEntityInfo registroAtividade = AdapterUtils.getInstantiableEntityInfo("ResRegistroAtividadesPresidencia");
				NeoObject registro = registroAtividade.createNewInstance();
				EntityWrapper wRegistro = new EntityWrapper(registro);
				
				wRegistro.findField("responsavel").setValue(usuarioResponsavel);
				wRegistro.findField("descricao").setValue(wrapper.findValue("justificativaNaoParecer"));
				wRegistro.findField("dataFinal").setValue(new GregorianCalendar());
				PersistEngine.persist(registro);
				wrapper.findField("registroAtividades").addValue(registro);
				
				wrapper.setValue("justificativaNaoParecer", null);
				wrapper.setValue("desabilitaCampoObs", Boolean.FALSE);
				
				
			} else {
				if (activity.getName().equals("Historico Conversa Resposta negativa Presidência Ressarcimento")) {
					InstantiableEntityInfo registroAtividade = AdapterUtils.getInstantiableEntityInfo("ResRegistroAtividadesPresidencia");
					NeoObject registro = registroAtividade.createNewInstance();
					EntityWrapper wRegistro = new EntityWrapper(registro);
					
					wRegistro.findField("responsavel").setValue(usuarioResponsavel);
					wRegistro.findField("descricao").setValue(wrapper.findValue("observacaoPresidenteJuridico"));
					wRegistro.findField("dataFinal").setValue(new GregorianCalendar());
					PersistEngine.persist(registro);
					wrapper.findField("registroAtividades").addValue(registro);
					
					wrapper.setValue("observacaoPresidenteJuridico", null);
					wrapper.setValue("desabilitaCampoObs", Boolean.TRUE);
				} 
			}
		}
		catch (Exception e)
		{
			log.error("Por favor, contatar o administrador do sistema!");
			throw new WorkflowException("Erro ao Gravar Observação!");
		}
	}

	public void back(EntityWrapper wrapper, Activity activity)
	{
	}
}