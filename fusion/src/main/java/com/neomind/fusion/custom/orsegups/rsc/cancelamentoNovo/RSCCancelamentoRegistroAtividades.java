package com.neomind.fusion.custom.orsegups.rsc.cancelamentoNovo;

import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

/**
 * 
 * @author herisson.ferreira
 *
 */
public class RSCCancelamentoRegistroAtividades implements AdapterInterface {

	@Override
	public void start(Task origin, EntityWrapper processEntity,
			Activity activity) {

		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);

		try {
			InstantiableEntityInfo registroAtividade = AdapterUtils.getInstantiableEntityInfo("RSCRegistroAtividade");
			NeoObject registro = registroAtividade.createNewInstance();
			EntityWrapper wRegistro = new EntityWrapper(registro);

			/* Histórico de Atividade do Fluxo C027.006 - RSC - Categoria Cancelamento NOVO */
			if (activity.getProcess().getModel().getName().equals("C027.006 - RSC - Categoria Cancelamento NOVO"))
			{

				String textoDescricao = "";
				NeoUser usuarioResponsavel = origin.getUser();
				
				if(origin.getActivityName().equalsIgnoreCase("Tentativa de Reversão do Cancelamento"))
				{

					Boolean tratadoViaWhatsApp = processEntity.findGenericValue("tratadoWhatsApp");
					textoDescricao = processEntity.findGenericValue("justificarTratativa");
					
					if(tratadoViaWhatsApp)
					{
						wRegistro.findField("descricao").setValue("");	
					}
					else
					{
						wRegistro.findField("descricao").setValue(textoDescricao);
					}
					
					wRegistro.findField("atividade").setValue("Tentativa de Reversão do Cancelamento");
					wRegistro.findField("responsavel").setValue(usuarioResponsavel);
					wRegistro.findField("dataFinalRSC").setValue(new GregorianCalendar());
					wRegistro.findField("prazo").setValue(origin.getDueDate());
					

				}
				else if(origin.getActivityName().equalsIgnoreCase("Enviar Justificativa ao Gestor"))
				{

					wRegistro.findField("atividade").setValue("Enviar Justificativa ao Gestor");
					wRegistro.findField("descricao").setValue(textoDescricao);
					wRegistro.findField("responsavel").setValue(usuarioResponsavel);
					wRegistro.findField("dataFinalRSC").setValue(new GregorianCalendar());
					wRegistro.findField("prazo").setValue(origin.getDueDate());			    

				}
				else if(origin.getActivityName().equalsIgnoreCase("Aprovar Justificativa"))
				{

					textoDescricao = processEntity.findGenericValue("observacao");

					wRegistro.findField("atividade").setValue("Aprovar Justificativa");
					wRegistro.findField("descricao").setValue(textoDescricao);
					wRegistro.findField("responsavel").setValue(usuarioResponsavel);
					wRegistro.findField("dataFinalRSC").setValue(new GregorianCalendar());
					wRegistro.findField("prazo").setValue(origin.getDueDate());			    

				}	
			}

			PersistEngine.persist(registro);
			processEntity.findField("registroAtividades").addValue(registro);

			processEntity.findField("justificarTratativa").setValue("");
			processEntity.findField("observacao").setValue("");

		} catch (Exception e) {
			e.printStackTrace();
			throw new WorkflowException("Erro na classe: "+this.getClass()+". Verifique junto ao setor de TI!");
		}

	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity) {

	}

}
