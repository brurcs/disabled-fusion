package com.neomind.fusion.custom.orsegups.servlets.fap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;
import java.sql.Clob;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.neomind.fusion.custom.orsegups.utils.SqlQueryUtil;
import com.neomind.fusion.custom.orsegups.vo.JSONHistContasVO;
import com.neomind.fusion.custom.orsegups.vo.TableHeaderVO;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.util.NeoUtils;

@WebServlet(name = "ServletConsultaHistoricoContas", urlPatterns = { "/servlet/ConsultaHistoricoContas" })
public class ServletConsultaHistoricoContas extends HttpServlet 
{
    private static final long serialVersionUID = 1L;
    private static final Log log = LogFactory.getLog(ServletConsultaHistoricoContas.class);

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
	consultaProcesso(request, response);
    }

    @SuppressWarnings("unused")
    private void consultaProcesso(HttpServletRequest request, HttpServletResponse response) throws IOException 
    {
	String jsonResponse = null;

	String jsonRequestStr = request.getParameter("form");

	if (NeoUtils.safeIsNull(jsonRequestStr)) 
	{
	    log.error("Parâmetro form ou type são nulos.");
	    response.sendError(500);

	    return;
	}

	JSONObject jsonRequest = null;

	try 
	{
	    jsonRequest = new JSONObject(jsonRequestStr);
	} 
	catch (JSONException e2) 
	{
	    log.error("Erro os obter json com os dados do form.");
	    response.sendError(500);
	    e2.printStackTrace();

	    return;
	}

	try 
	{

	    jsonResponse = getHistContas(jsonRequest);

	} 
	catch (Exception e) 
	{
	    e.printStackTrace();
	}

	try 
	{
	    jsonResponse= new String(jsonResponse.getBytes("UTF-8"), "UTF-8");
	} 
	catch (UnsupportedEncodingException e) 
	{
	}

	try 
	{
	    response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");

	    PrintWriter out = response.getWriter();

	    if (NeoUtils.safeIsNotNull(out) && NeoUtils.safeIsNotNull(jsonResponse)) 
	    {
		out.append(jsonResponse);
	    }
	} 
	catch (IOException e) 
	{
	    e.printStackTrace();
	}
    }

    public static GregorianCalendar dateConverter(String strData, boolean isINI) 
    {
	java.util.Date date;
	GregorianCalendar calendar = new GregorianCalendar();

	if(isINI) 
	{
	    strData +=" 00:00:00.000";
	} 
	else 
	{
	    strData +=" 23:59:59.999";
	}

	DateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss.SSS");

	try 
	{
	    date = format.parse(strData);
	    calendar.setTime(date);
	} 
	catch (ParseException e1) 
	{
	    e1.printStackTrace();
	}

	return calendar;
    }

    private String getString(Object data) 
    {
	if (data instanceof Clob) 
	{
	    StringBuilder sb = new StringBuilder();

	    try 
	    {
		Reader reader = ((Clob) data).getCharacterStream();
		BufferedReader br = new BufferedReader(reader);

		String line;

		while (null != (line = br.readLine())) 
		{
		    sb.append(line);
		}

		br.close();
	    } 
	    catch (SQLException e) 
	    {
		// handle this exception
	    } 
	    catch (IOException e) 
	    {
		// handle this exception
	    }

	    return sb.toString();
	} 
	else if (data instanceof String)
	    return (String) data;
	else if (data instanceof BigInteger || data instanceof Integer)
	    return data.toString();
	else if (data instanceof Timestamp)
	    return new SimpleDateFormat("dd/MM/yyyy").format(data);
	else
	    return null;
    }

    private String getHistContas(JSONObject data) throws JSONException 
    {
	Map<String, Object> params = new HashMap<String, Object>();

	String cliente = data.has("cliente") ? data.getString("cliente") : null;
	String nomnumrota = data.has("nomnumrota") ? data.getString("nomnumrota") : null;
	String codigo = data.has("codigo") ? data.getString("codigo") : null;
	String tipo = data.has("tipo") ? data.getString("tipo") : null;
	String central = data.has("central") ? data.getString("central") : null;
	String situacaoFap = data.has("situacaoFap") ? data.getString("situacaoFap") : null;
	String competenciaIni = data.has("competenciaIni") ? data.getString("competenciaIni") : null;
	String competenciaFin = data.has("competenciaFin") ? data.getString("competenciaFin") : null;

	Timestamp timestampCompetenciaINI = null;
	Timestamp timestampCompetenciaFIM = null;

	if (NeoUtils.safeIsNotNull(cliente)) 
	{
	    params.put("cliente", cliente);
	}
	if (NeoUtils.safeIsNotNull(nomnumrota)) 
	{
	    params.put("nomnumrota", nomnumrota);
	}
	if (NeoUtils.safeIsNotNull(codigo)) 
	{
	    params.put("codigo", codigo);
	}
	if (NeoUtils.safeIsNotNull(tipo)) 
	{
	    params.put("tipo", tipo);
	}
	if (NeoUtils.safeIsNotNull(central)) {
	    params.put("central", central);
	}
	if (NeoUtils.safeIsNotNull(situacaoFap) && !situacaoFap.equals("-1")) 
	{
	    params.put("situacaoFap", situacaoFap);
	}

	if (NeoUtils.safeIsNotNull(competenciaIni)) 
	{
	    timestampCompetenciaINI = new Timestamp(dateConverter(competenciaIni, true).getTimeInMillis());
	    params.put("hasCompetenciaINI", true);
	    params.put("competenciaIni", timestampCompetenciaINI);
	}
	if (NeoUtils.safeIsNotNull(competenciaFin)) 
	{
	    timestampCompetenciaFIM = new Timestamp(dateConverter(competenciaFin, false).getTimeInMillis());
	    params.put("hasCompetenciaFIM", true);
	    params.put("competenciaFin", timestampCompetenciaFIM);
	}

	SqlQueryUtil sqlUtil = new SqlQueryUtil("UTF-8");

	String consulta = sqlUtil.getQuery("consultaHistContas/consultaHistContas.sql.vm", params);

	Query query = PersistEngine.getEntityManager().createNativeQuery(consulta);

	List<TableHeaderVO> header = new ArrayList<TableHeaderVO>();
	header.add(new TableHeaderVO(0, "central", "Central"));
	header.add(new TableHeaderVO(1, "cliente", "Cliente"));
	header.add(new TableHeaderVO(2, "codigo", "Código Tarefa"));
	header.add(new TableHeaderVO(3, "tipo", "Tipo"));
	header.add(new TableHeaderVO(4, "situacao", "Situação"));
	header.add(new TableHeaderVO(5, "competenciaPagamento", "Competência"));
	header.add(new TableHeaderVO(6, "rota", "Rota"));
	header.add(new TableHeaderVO(7, "tarefa", "Tarefa"));

	List<JSONHistContasVO> rows = new ArrayList<JSONHistContasVO>();

	@SuppressWarnings("unchecked")
	List<Object[]> resultList = (List<Object[]>) query.getResultList();

	for (Object[] item : resultList) 
	{
	    JSONHistContasVO row = new JSONHistContasVO(getString(item[0]), getString(item[1]), getString(item[2]),
		    getString(item[3]), getString(item[4]), getString(item[5]), getString(item[6]), 
		    getString(item[7]));

	    rows.add(row);
	}

	Map<String, Object> response = new HashMap<String, Object>();
	response.put("header", header);
	response.put("rows", rows);

	String responseJSON = (new Gson()).toJson(response);

	return responseJSON;		
    }

    void concatRows(Object original, Object toContat) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchMethodException 
    {
	Class clazz = original.getClass();

	Field[] attributes = clazz.getDeclaredFields();

	for(Field attr : attributes) 
	{
	    if (attr != null)
	    {
		String val1 = (PropertyUtils.getSimpleProperty(original, attr.getName()) != null) ? PropertyUtils.getSimpleProperty(original, attr.getName()).toString() : "";
		String val2 = (PropertyUtils.getSimpleProperty(toContat, attr.getName()) != null) ? PropertyUtils.getSimpleProperty(toContat, attr.getName()).toString() : "";

		if(!val1.equals(val2)) 
		{
		    val1 = val1.concat("\n ").concat(val2);
		    PropertyUtils.setSimpleProperty(original, attr.getName(), val1);
		}
	    }
	}
    }
}