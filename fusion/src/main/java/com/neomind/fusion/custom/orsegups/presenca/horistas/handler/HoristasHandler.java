package com.neomind.fusion.custom.orsegups.presenca.horistas.handler;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.neomind.fusion.custom.orsegups.presenca.horistas.beans.Horista;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoGroup;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class HoristasHandler {

    public List<Horista> getRankingHorista(String regional, int mes, int ano) {

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	
	List<Horista> horistas  = null;
		
	long codRegional  = 0;
	
	if (!regional.equals("GERAL")){
	    codRegional = OrsegupsUtils.getCodigoRegional(regional);
	}

	try {
	    
	    StringBuffer  varname1 = new StringBuffer();
	    
	    varname1.append("SELECT     fun.numemp, ");
	    varname1.append("           fun.numcad, ");
	    varname1.append("           fun.tipcol, ");
	    varname1.append("           orn.usu_codccu , ");
	    varname1.append("           fun.nomfun, ");
	    varname1.append("           ISNULL((select SUM(SP.QtdHor)/60 ");
	    varname1.append("	                 from R066SIT SP ");
	    varname1.append("	                where (YEAR(SP.DatApu) = ");
	    varname1.append(ano);
	    varname1.append("	                  AND MONTH(sp.DatApu) = ");
	    varname1.append(mes);
	    varname1.append(" )	                  AND SP.CodSit IN (1,2,11,14,51,117,311,312) ");
	    varname1.append("	    	          AND SP.NumCad = fun.numcad ");
	    varname1.append("	                  AND SP.NumEmp = fun.numemp),0) AS HORAS, ");
	    varname1.append("	       ISNULL((select SUM(SP.QtdHor)%60 ");
	    varname1.append("	                 from R066SIT SP ");
	    varname1.append("	                where (YEAR(SP.DatApu) = ");
	    varname1.append(ano);
	    varname1.append("	                  AND MONTH(sp.DatApu) = ");
	    varname1.append(mes);	    
	    varname1.append(" )	                  AND SP.CodSit IN (1,2,11,14,51,117,311,312) ");
	    varname1.append("	    	          AND SP.NumCad = fun.numcad ");
	    varname1.append("	                  AND SP.NumEmp = fun.numemp) ,0)AS MINUTOS ");
	    varname1.append("      FROM r034fun FUN WITH(nolock) ");
	    varname1.append("     INNER JOIN r038hes hes WITH (nolock) ");
	    varname1.append("             ON hes.numemp = fun.numemp ");
	    varname1.append("            AND hes.tipcol = fun.tipcol ");
	    varname1.append("            AND hes.numcad = fun.numcad ");
	    varname1.append("     INNER JOIN r038hlo hlo WITH (nolock) ");
	    varname1.append("             ON hlo.numemp = fun.numemp ");
	    varname1.append("            AND hlo.tipcol = fun.tipcol ");
	    varname1.append("            AND hlo.numcad = fun.numcad ");
	    varname1.append("            AND hlo.datalt = (SELECT Max (datalt) ");
	    varname1.append("                                FROM r038hlo TABELA001 ");
	    varname1.append("                               WHERE tabela001.numemp = hlo.numemp ");
	    varname1.append("                                 AND tabela001.tipcol = hlo.tipcol ");
	    varname1.append("                                 AND tabela001.numcad = hlo.numcad ");
	    varname1.append("                                 AND tabela001.datalt <= Getdate()) ");
	    varname1.append("     INNER JOIN r016orn orn WITH (nolock) ");
	    varname1.append("             ON orn.numloc = hlo.numloc ");
	    varname1.append("            AND orn.taborg = 203 ");
	    varname1.append("     INNER JOIN usu_t038cvs cvs WITH (nolock) ");
	    varname1.append("             ON cvs.usu_numloc = orn.numloc ");
	    varname1.append("            AND cvs.usu_taborg = orn.taborg ");
	    varname1.append("            AND cvs.usu_seqalt = (SELECT Max(c2.usu_seqalt) ");
	    varname1.append("                                    FROM usu_t038cvs c2 ");
	    varname1.append("                                   WHERE c2.usu_taborg = cvs.usu_taborg ");
	    varname1.append("                                     AND c2.usu_numloc = cvs.usu_numloc ");
	    varname1.append("                                     AND c2.usu_datalt <= Getdate()) ");
	    varname1.append("     INNER JOIN r006esc esc ");
	    varname1.append("             ON esc.codesc = hes.codesc ");
	    varname1.append("            AND claesc = 8 ");
	    varname1.append("     WHERE ((fun.sitafa != 7) OR (fun.sitafa = 7 AND fun.datafa >= Getdate())) ");
	    varname1.append("       AND fun.numemp <= 22 ");
	    
	    if (!regional.equals("GERAL")) {
		varname1.append("       AND cvs.usu_codreg = ");
		varname1.append(codRegional);	
	    }
	   	     
	    varname1.append("       AND hes.datalt = (SELECT Max (datalt) ");
	    varname1.append("                           FROM r038hes tabela001 ");
	    varname1.append("                          WHERE tabela001.numemp = hes.numemp ");
	    varname1.append("                            AND tabela001.tipcol = hes.tipcol ");
	    varname1.append("                            AND tabela001.numcad = hes.numcad ");
	    varname1.append("                            AND tabela001.datalt <= Getdate()) ");
	    varname1.append("     group BY fun.numemp, ");
	    varname1.append("              fun.numcad, ");
	    varname1.append("              fun.nomfun, ");
	    varname1.append("              cvs.usu_codreg, ");
	    varname1.append("              fun.tipcol, ");
	    varname1.append("              orn.usu_codccu ");
	    varname1.append("     ORDER BY HORAS DESC, ");
	    varname1.append("	          MINUTOS DESC, ");
	    varname1.append("	          nomfun, ");
	    varname1.append("              numemp, ");
	    varname1.append("              numcad");
	        
	    conn = PersistEngine.getConnection("VETORH");

	    stmt = conn.createStatement();

	    rs = stmt.executeQuery(varname1.toString());
	    
	    horistas = new ArrayList<Horista>();
	    
	    while (rs.next()) {
		
		Horista novoHorista = new Horista();
		
		novoHorista.setMatricula(rs.getInt("numcad"));		
		novoHorista.setEmpresa(rs.getInt("numemp"));		
		novoHorista.setNome(rs.getString("nomfun"));				
		novoHorista.setValorHoras(Double.parseDouble(rs.getString("HORAS")+"."+rs.getString("MINUTOS")));
		novoHorista.setTipoColaborador(rs.getInt("tipcol"));
		novoHorista.setCentroCusto(rs.getInt("usu_codccu"));
		
		String hora = rs.getString("HORAS");
		
		if(hora.length() == 1){
		    hora = "0"+hora;
		}
				
		String minuto = rs.getString("MINUTOS");
		
		if(minuto.length() == 1){
		    minuto = "0"+minuto;
		}
		
		novoHorista.setTextoHoras(hora+":"+minuto);
		
		horistas.add(novoHorista);
		
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	return horistas;
    }
    
    
    
    public Map<String, Boolean> getPermissaoExibicao(){
	
	Map<String, Boolean> retorno = new HashMap<>();
	
	NeoUser usuarioLogado = PortalUtil.getCurrentUser();	
	
	NeoGroup grupo = usuarioLogado.getGroup();

	String nomeGrupo = grupo.getName();
		
	
	//USUARIOS QUE POSSUEM ACESSO A TODAS AS FILIAIS
	if(nomeGrupo.contains("Presidência") || nomeGrupo.contains("Diretoria") || nomeGrupo.equalsIgnoreCase("Coordenação de Sistemas") || nomeGrupo.contains("Superintendência") ){
	    for (String key : OrsegupsUtils.getRegionais().keySet()){
		retorno.put(key, true);
	    }
	    retorno.put("GERAL", true);
	    return retorno;
	}else {
	    
	    //VERIFICA SE USUARIO NÃO TEM ACESSO TOTAL
	    
	    NeoPaper acessoTotal = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "HoristasRankingTodos"));
	    
	    if (acessoTotal != null) {
		Set<NeoUser> usuariosAcessoTotal = acessoTotal.getUsers();

		for (NeoUser user : usuariosAcessoTotal) {
		    if (user.equals(usuarioLogado)) {
			for (String key : OrsegupsUtils.getRegionais().keySet()){
				retorno.put(key, true);
			}
			retorno.put("GERAL", true);			
			return retorno;
		    }
		}

	    }
	    
	    
	    Map<String, Long> regionais = OrsegupsUtils.getRegionais();
	    
	    for (String key : regionais.keySet()){
				
		//VERIFICA GERENTES DE ELETRONICA DA SEDE
		
		if (regionais.get(key) == 1){
		      			    	
			if(usuarioLogado.getGroup().getName().equalsIgnoreCase("Gerência de Vigilância Eletrônica")){
			    retorno.put(key, true);
			}			
		}
		
		//VERIFICA GERENTES REGIONAIS
		NeoPaper papel = OrsegupsUtils.getPapelGerenteRegional(regionais.get(key));
		
		if (papel != null) {
		    Set<NeoUser> usuariosPapel = papel.getUsers();

		    for (NeoUser user : usuariosPapel) {
			if (user.equals(usuarioLogado)) {
			    retorno.put(key, true);
			}
		    }
		}
		

		papel = null;
				
		//VERIFICA COORDENADORES REGIONAIS
		    
		papel = OrsegupsUtils.getPapelCoordenadorRegional(regionais.get(key));
		
		if (papel != null) {
		    Set<NeoUser> usuariosPapel = papel.getUsers();

		    for (NeoUser user : usuariosPapel) {
			if (user.equals(usuarioLogado)) {
			    retorno.put(key, true);
			}
		    }
		}
		
		//VERIFICA PAPEIS ESPECIFICOS DO RANKING
		
		papel = null;
		
		papel = OrsegupsUtils.getPapelRankingHoristas(regionais.get(key));
		
		if (papel != null) {
		    Set<NeoUser> usuariosPapel = papel.getUsers();

		    for (NeoUser user : usuariosPapel) {
			if (user.equals(usuarioLogado)) {
			    retorno.put(key, true);
			}
		    }
		}		
	    }	    
	}
		
	return retorno;
    }
    
    

}
