package com.neomind.fusion.custom.orsegups.converter;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.workflow.WFProcess;

public class CFTVInteligenteGaleriaImagensConverter extends StringConverter {

    @SuppressWarnings("unchecked")
    @Override
    protected String getHTMLInput(EFormField field, OriginEnum origin) {

	String result = super.getHTMLInput(field, origin);

	WFProcess wfProcess = PersistEngine.getObject(WFProcess.class, new QLRawFilter("entity_neoId = " + field.getForm().getObject().getNeoId()));

	EntityWrapper processEntityWrapper = new EntityWrapper(field.getForm().getObject());

	NeoObject contaSigma = (NeoObject) processEntityWrapper.findValue("ContaSigma");
	EntityWrapper wContaSigma = new EntityWrapper(contaSigma);

	StringBuilder link = new StringBuilder();
	link.append("http://localhost:8080/#!/galeriaImagens/" + wfProcess.getCode() + "/" + wContaSigma.findValue("cd_cliente"));

	String button = "&nbsp&nbsp&nbsp<a href=\"" + link + "\" target=\"_blank\">VISUALIZAR</a>";

	return result + button;
    }
}
