package com.neomind.fusion.custom.orsegups.helpdeskti.adapters;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class GIDefinePrazoEscalado implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(GIDefinePrazoEscalado.class);
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity) 
	{
		try{
			
			GregorianCalendar prazo = (GregorianCalendar) processEntity.findValue("prazo");
			GregorianCalendar novoPrazo  = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
			processEntity.setValue("prazo", novoPrazo);
		
		}
		catch (Exception e)
		{
			log.error(" Fluxo TI - Define Prazo Escalado - Por favor, contatar o administrador do sistema! " + e.getMessage().toString());
			throw new WorkflowException(" Fluxo TI - Define Prazo Escalado - Por favor, contatar o administrador do sistema! " + e.getMessage().toString());
		}
	}
	
	@Override
	public void back(EntityWrapper arg0, Activity arg1) 
	{
		// TODO Auto-generated method stub
		
	}
	
}
