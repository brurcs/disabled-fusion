package com.neomind.fusion.custom.orsegups.presenca.vo;

public class EscalaRevezamentoVO
{
	private String data;
	private String diaSemana;
	private String descricao;
	private String turma1;
	private String turma2;
	private String turma3;
	private String turma4;
	private String turma5;
	private String turma6;
	private String turma7;
	private String turma8;
	
	public String getData()
	{
		return data;
	}
	public void setData(String data)
	{
		this.data = data;
	}
	public String getDiaSemana()
	{
		return diaSemana;
	}
	public void setDiaSemana(String diaSemana)
	{
		this.diaSemana = diaSemana;
	}
	public String getDescricao()
	{
		return descricao;
	}
	public void setDescricao(String descricao)
	{
		this.descricao = descricao;
	}
	public String getTurma1()
	{
		return turma1;
	}
	public void setTurma1(String turma1)
	{
		this.turma1 = turma1;
	}
	public String getTurma2()
	{
		return turma2;
	}
	public void setTurma2(String turma2)
	{
		this.turma2 = turma2;
	}
	public String getTurma3()
	{
		return turma3;
	}
	public void setTurma3(String turma3)
	{
		this.turma3 = turma3;
	}
	public String getTurma4()
	{
		return turma4;
	}
	public void setTurma4(String turma4)
	{
		this.turma4 = turma4;
	}
	public String getTurma5()
	{
		return turma5;
	}
	public void setTurma5(String turma5)
	{
		this.turma5 = turma5;
	}
	public String getTurma6()
	{
		return turma6;
	}
	public void setTurma6(String turma6)
	{
		this.turma6 = turma6;
	}
	public String getTurma7()
	{
		return turma7;
	}
	public void setTurma7(String turma7)
	{
		this.turma7 = turma7;
	}
	public String getTurma8()
	{
		return turma8;
	}
	public void setTurma8(String turma8)
	{
		this.turma8 = turma8;
	}
}
