package com.neomind.fusion.custom.orsegups.integraRubi.adapter;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.util.NeoUtils;

public class RHIntegracaoBanco
{
	private static final Log log = LogFactory.getLog(RHIntegracaoBanco.class);

	public static Boolean insereDadosEstagio(EntityWrapper wForm, Integer NumCad, Integer nrEmpresa)
	{
		Boolean result = false;

		try
		{
			if (NumCad != null)
			{
				BigDecimal valorBolsa = wForm.findGenericValue("estagiario.valBol");
				Long tipoCol = wForm.findGenericValue("tpColaborador.codigo");
				String codNatEstagio = wForm.findGenericValue("estagiario.naturezaEstagio.codigo");
				Long codNivelEstagio = wForm.findGenericValue("estagiario.nivelEstagio.codigo");
				Long codInstEnsino = wForm.findGenericValue("estagiario.insEns.codoem");
				Long codAgenteIntegracao = wForm.findGenericValue("estagiario.ageInt.codoem");
				Long codEmpresaSupervisor = wForm.findGenericValue("estagiario.empCoo.numemp");
				Long codTipoSupervisor = wForm.findGenericValue("estagiario.tipCoo.codigo");
				Long codSupervisor = wForm.findGenericValue("estagiario.numCoo.numcad");
				Long cpfSupervisor = wForm.findGenericValue("estagiario.cpfCoo");
				GregorianCalendar dtPrevistaTermino = wForm.findGenericValue("estagiario.preTer");
				String dtPrevistaStr = NeoUtils.safeOutputString(dtPrevistaTermino != null ? new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(dtPrevistaTermino.getTime()) : null);
				String nomeSupervisor = wForm.findGenericValue("estagiario.nomCoo");
				String areaAtuacao = wForm.findGenericValue("estagiario.areAtu");
				String apoliceSeguro = wForm.findGenericValue("estagiario.apoSeg");

				String consulta = " select NumCad from  R034ETG where NumEmp = " + nrEmpresa + " and TipCol = " + tipoCol + " and NumCad  = " + NumCad;

				Integer nr = (Integer) selectColumn(consulta, "NumCad");

				//Registro ainda não criado
				if (nr == null || nr.intValue() == 0)
				{
					String query = "insert into R034ETG (NumEmp, TipCol, NumCad, natetg, nivetg, areatu, aposeg, valbol, preter, insens, ageint, empcoo, tipcoo, numcoo, nomcoo, cpfcoo) " + "values (" + nrEmpresa + ", " + tipoCol + ", " + NumCad + ", '" + codNatEstagio + "', " + codNivelEstagio + ", '" + areaAtuacao + "', '" + apoliceSeguro + "', " + valorBolsa + ", '" + dtPrevistaStr + "', " + codInstEnsino + ", " + codAgenteIntegracao + ", " + codEmpresaSupervisor + ", " + codTipoSupervisor
							+ ", " + codSupervisor + ", '" + nomeSupervisor + "', '" + cpfSupervisor + "')";

					log.info(nrEmpresa + " - " + NumCad + " - " + query);

					result = executeQuery(query);
				}
				else
				{
					//registro já existe
					result = true;
				}
			}
		}
		catch (Exception e)
		{
			log.warn("ERRO NO MÉTODO: \"insereEstagiario\"");
			log.info("ERRO NO MÉTODO: \"insereEstagiario\"");
			e.printStackTrace();
			result = Boolean.FALSE;
		}

		return result;
	}

	public static Boolean insereHistoricoCracha(Integer NumCad, Integer NumEmp, String DatIni)
	{

		Boolean result = false;

		try
		{
			String NumCadS = NumCad.toString();
			String Zeros = "";
			Integer contNumCad = 8 - NumCadS.length();

			for (Integer i = 0; i < contNumCad; i++)
				Zeros = Zeros + "0";

			NumCadS = Zeros + NumCadS;

			String NumEmpS = NumEmp.toString();

			if (NumEmpS.length() == 1)
				NumEmpS = "0" + NumEmpS;

			String NumCra = "10" + NumEmpS + NumCadS;

			log.info("Cont NumCra - " + NumCad + ": " + NumCra.length());

			String consulta = " select NumCad from  R038HCH where  TipCra = 1 and NumEmp = " + NumEmp + " and TipCol = 1 and NumCad =" + NumCad;
			Integer nr = (Integer) selectColumn(consulta, "NumCad");

			//Registro ainda não criado
			if (nr == null || nr.intValue() == 0)
			{
				String query = " insert into R038HCH (TipCra, NumEmp, TipCol, NumCad, DatIni, NumCra, DatFim, StaAtu, StaAcc, " + "HorIni, ViaCra, HorFim, RecEPr) " + "values (1, " + NumEmp + ", 1, " + NumCad + ", '" + DatIni + "', " + NumCra + ", '1900-12-31', 1, 1, " + "0, 0, 0, '')";

				log.info("Query inserção histórico do crachá: " + query);

				result = executeQuery(query);
			}
			else
				result = true;

		}
		catch (Exception e)
		{

			log.warn("ERRO NO MÉTODO: \"insereHistoricoCracha\"");
			log.info("ERRO NO MÉTODO: \"insereHistoricoCracha\"");
			e.printStackTrace();
			result = Boolean.FALSE;

		}

		return true;

	}

	@SuppressWarnings("deprecation")
	public static boolean executeQuery(String query)
	{
		Connection connSapiens = null;
		PreparedStatement pstData = null;
		Boolean retorno = false;
		
		log.info(query);

		try
		{
			connSapiens = PersistEngine.getConnection("VETORH");
			pstData = connSapiens.prepareStatement(query);
			int ret = pstData.executeUpdate();

			if (ret == 0)
				retorno = Boolean.FALSE;
			else
				retorno = Boolean.TRUE;
		}
		catch (Exception e)
		{
			log.warn("ERRO NO MÉTODO: \"insereEventoHorista\"");
			log.info("ERRO NO MÉTODO: \"insereEventoHorista\"");
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connSapiens, pstData, null);
		}

		return retorno;
	}

	public static boolean insereEventoHorista(Integer NumCad, Integer NumEmp)
	{
		String consulta = " select NumCad from  r044fix where NumEmp = " + NumEmp + " and TipCol = 1 and NumCad  = " + NumCad + " and CodEve = ";
		String consulta1 = consulta + "1";
		String consulta2 = consulta + "2";

		Integer nr = (Integer) selectColumn(consulta1, "NumCad");
		boolean sucesso = false;
		if (nr != null && nr > 0)
		{
			//se já foi cadastrado, não precisa cadastrar novamente
			sucesso = true;
		}
		else
		{
			String insert1 = " insert into r044fix (NumEmp, TipCol, NumCad, TabEve, CodEve, CodRat, DatIni, DatFim) " + "values (" + NumEmp + ", 1, " + NumCad + ", 941, 1, 0, '1900-12-31 00:00:00.0', '1900-12-31 00:00:00.0')";
			sucesso = executeQuery(insert1);
		}

		if (sucesso)
		{
			nr = (Integer) selectColumn(consulta2, "NumCad");
			if (nr != null && nr > 0)
			{
				//se já foi cadastrado, não precisa cadastrar novamente
				sucesso = true;
			}
			else
			{
				String insert2 = " insert into r044fix (NumEmp, TipCol, NumCad, TabEve, CodEve, CodRat, DatIni, DatFim) " + "values (" + NumEmp + ", 1, " + NumCad + ", 941, 2, 0, '1900-12-31 00:00:00.0', '1900-12-31 00:00:00.0')";
				sucesso = executeQuery(insert2);
			}
		}

		return sucesso;
	}

	@SuppressWarnings("deprecation")
	public static Object selectColumn(String query, String campo)
	{
		Connection connSapiens = null;
		PreparedStatement pstmt = null;
		Object nr = null;
		
		log.info(query);

		try
		{
			connSapiens = PersistEngine.getConnection("VETORH");
			pstmt = connSapiens.prepareStatement(query);

			ResultSet rs = pstmt.executeQuery();

			if (rs.next())
				nr = rs.getObject(campo);
			else
			{
				log.warn("NÃO EXISTE REGISTRO");
				return 0;
			}
		}
		catch (Exception e)
		{
			log.warn("");
			e.printStackTrace();
			return null;
		}
		finally
		{
			OrsegupsUtils.closeConnection(connSapiens, pstmt, null);
		}

		return nr;
	}

	public static Integer getFichaMedica(Integer nrCadastro, Integer nrEmpresa)
	{
		try
		{
			String query = " select codfic from R110FIC where numemp = " + nrEmpresa + " and numcad = " + nrCadastro;

			Integer nrFicha = (Integer) selectColumn(query, "codfic");

			return nrFicha;
		}
		catch (Exception e)
		{
			log.warn("");
			e.printStackTrace();
			return null;
		}
	}

	public static Boolean insereDadosVigilante(Integer NumCad, Integer NumEmp, String DtFormacao, String DtReciclagem, String OrgaoVig, String NumDrt, String DtValidadeCnv, String NumCnv, Boolean ExtTrans, Boolean ExtSegPes, Boolean cadastroCNV, Long codoem)
	{
		if (codoem == null)
			codoem = 0l;

		if (DtReciclagem == null)
			DtReciclagem = "1900-12-31";

		String USU_ExtTraVal = null;
		String USU_ExtSegPes = null;

		if (ExtSegPes)
			USU_ExtSegPes = "S";
		else
			USU_ExtSegPes = "N";

		if (ExtTrans)
			USU_ExtTraVal = "S";
		else
			USU_ExtTraVal = "N";

		String queryConsulta = " select USU_NumCad from USU_TCadCur where USU_NumEmp = " + NumEmp + " and USU_NumCad = " + NumCad;

		Integer qtd = (Integer) selectColumn(queryConsulta, "USU_NumCad");

		if (qtd == null || qtd.intValue() == 0)
		{
			String query = null;

			try
			{

				if (cadastroCNV)
					query = " INSERT INTO USU_TCadCur (USU_NumEmp, USU_TipCol, USU_NumCad, USU_OrgExp, USU_NumDip, USU_NumDrt, USU_ExtTraVal, " + "USU_ExtSegPes, USU_DatFor, USU_DatRec, USU_DatValCnv, USU_NuCaNaVi, USU_NumOem)" + "VALUES (" + NumEmp + ", 1, " + NumCad + ", '" + OrgaoVig + "', '" + NumDrt + "', '" + NumDrt + "', '" + USU_ExtTraVal + "', '" + USU_ExtSegPes + "', '" + DtFormacao + "', '" + DtReciclagem + "', '" + DtValidadeCnv + "', '" + NumCnv + "', " + codoem + ")";
				else
					query = " INSERT INTO USU_TCadCur (USU_NumEmp, USU_TipCol, USU_NumCad, USU_OrgExp, USU_NumDip, USU_NumDrt, USU_ExtTraVal, " + "USU_ExtSegPes, USU_DatFor, USU_DatRec, USU_NumOem)" + "VALUES (" + NumEmp + ", 1, " + NumCad + ", '" + OrgaoVig + "', '" + NumDrt + "', '" + NumDrt + "', '" + USU_ExtTraVal + "', '" + USU_ExtSegPes + "', '" + DtFormacao + "', '" + DtReciclagem + "', " + codoem + ")";

				log.info(NumEmp + " - " + NumCad + " - Query: " + query);

				return executeQuery(query);

			}
			catch (Exception e)
			{
				log.warn("ERRO NO MÉTODO: \"insereDadosVigilante\"");
				log.info("ERRO NO MÉTODO: \"insereDadosVigilante\"");
				e.printStackTrace();
				return Boolean.FALSE;
			}
		}
		else
			return true;

	}

	public static Boolean inserePersonalizados(Integer NumCad, Integer USU_TipAdm, String USU_ColSde, String USU_CsgBrc, String USU_ValCom, Integer USU_DepEmp, Integer USU_FisRes, String USU_NomCra)
	{
		boolean retorno = false;

		try
		{
			String query = " update r034fun set usu_tipadm = " + USU_TipAdm + ", usu_colsde = '" + USU_ColSde + "', usu_csgbrc = '" + USU_CsgBrc + "', usu_valcom = '" + USU_ValCom + "', usu_depemp = " + USU_DepEmp + ", usu_fisres = " + USU_FisRes + ", usu_nomcra = '" + USU_NomCra + "'  where numcad = " + NumCad;

			if (NumCad != null)
				return executeQuery(query);

		}
		catch (Exception e)
		{
			log.warn("ERRO NO MÉTODO: \"inserePersonalizados\"");
			log.info("ERRO NO MÉTODO: \"inserePersonalizados\"");
			e.printStackTrace();
			retorno = Boolean.FALSE;
		}

		return retorno;
	}

	public static Boolean insereFotoCracha(Integer numcad, Integer numemp, String fotemp)
	{
		try
		{
			String queryConsulta = " select NumCad from R034FOT where NumEmp = " + numemp + " and NumCad = " + numcad;

			Integer qtd = (Integer) selectColumn(queryConsulta, "NumCad");

			if (qtd == null || qtd.intValue() == 0)
			{
				String query = " INSERT INTO R034FOT (NumEmp, TipCol, NumCad, FotEmp) SELECT " + numemp + ", 1, " + numcad + "," + "BulkColumn FROM OPENROWSET( Bulk '" + fotemp + "', SINGLE_BLOB) AS BLOB";

				log.info("queryFoto: " + query);

				return executeQuery(query);

			}
			else
				return true;
		}
		catch (Exception e)
		{
			log.warn("ERRO NO MÉTODO: \"inserePersonalizados\"");
			e.printStackTrace();
			return Boolean.FALSE;
		}

	}

	public static Boolean resgataSolicitante(String codeUsu, Integer numcad)
	{
		boolean retorno = false;

		if (codeUsu.equals("thays.santos"))
			codeUsu = "thays.passos";

		Long codusu = null;

		try
		{
			String query = " select codusu from r999usu where nomusu like '" + codeUsu + "' ";

			codusu = (Long) selectColumn(query, "codusu");
			if (codusu != null)
			{
				String query2 = " update r034fun set usu_codusu = " + codusu + " where numcad = " + numcad;
				return executeQuery(query2);
			}
			else
				retorno = Boolean.FALSE;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			retorno = false;
		}

		return retorno;

	}

	public static Boolean insereSelecao(Integer numcad, Integer numemp, Integer tipcol)
	{
		try
		{
			String consulta = " select numcad from  r034sel where numemp =" + numemp + " and tipcol = " + tipcol + " and numcad = " + numcad;
			Integer nr = (Integer) selectColumn(consulta, "numcad");

			//Registro ainda não criado
			if (nr == null || nr.intValue() == 0)
			{
				String query = " insert into r034sel (numemp, tipcol, numcad, codsel) values (" + numemp + ", " + tipcol + ", " + numcad + ", 2)";

				return executeQuery(query);
			}
			else
				return true;
		}
		catch (Exception e)
		{
			log.warn("ERRO NO MÉTODO: \"insereSelecao\"");
			e.printStackTrace();
			return Boolean.FALSE;
		}
	}

	public static Boolean insereHistoricoDispensaPonto(EntityWrapper wForm, Integer nrCadastro, Integer nrEmpresa)
	{
		try
		{
			Long tipoCol = wForm.findGenericValue("tpColaborador.codigo");
			String consulta = " select numcad from  R038NOT where numemp =" + nrEmpresa + " and tipcol = " + tipoCol + " and numcad = " + nrCadastro;
			Integer nr = (Integer) selectColumn(consulta, "numcad");

			//Registro ainda não criado
			if (nr == null || nr.intValue() == 0)
			{
				GregorianCalendar dtAdmissao = wForm.findGenericValue("dtAdmissao");
				String dtAdmissaoStr = NeoUtils.safeOutputString(dtAdmissao != null ? new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(dtAdmissao.getTime()) : null);

				String query = "insert into R038NOT (NumEmp, TipCol, NumCad, DatNot, SeqNot, TipNot) " + "values (" + nrEmpresa + ", " + tipoCol + ", " + nrCadastro + ", '" + dtAdmissaoStr + "', 1, 11)";

				return executeQuery(query);
			}
			else
				return true;

		}
		catch (Exception e)
		{
			log.warn("ERRO NO MÉTODO: \"insereHistoricoDispensaPonto\"");
			log.info("ERRO NO MÉTODO: \"insereHistoricoDispensaPonto\"");
			e.printStackTrace();
			return false;
		}

	}

	public static Integer resgataNumCad(String chave, Long numemp)
	{
		try
		{
			return (Integer) selectColumn(" select numcad from r034fun where numemp = '" + numemp + "' and apefun like '" + chave + "' ", "numcad");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public static Boolean insereListaVT(Integer NumCad, Integer numEmp, Integer codEmpVT, Double numVT)
	{
		try
		{
			if (numVT == null)
				return false;

			if (codEmpVT != null && codEmpVT == 10)
				return validaVT(NumCad, numEmp, numVT, "usu_matjotur");
			else if (codEmpVT != null && codEmpVT == 20)
				return validaVT(NumCad, numEmp, numVT, "usu_matbiguacu");
			else if (codEmpVT != null && codEmpVT == 30)
				return validaVT(NumCad, numEmp, numVT, "usu_matestrela");
			else if (codEmpVT != null && codEmpVT == 40)
				return validaVT(NumCad, numEmp, numVT, "usu_matimp");
			else if (codEmpVT != null && codEmpVT == 50)
				return validaVT(NumCad, numEmp, numVT, "usu_matpasrap");
			else if (codEmpVT != null && codEmpVT == 110)
				return validaVT(NumCad, numEmp, numVT, "usu_codsic");
			else
				return Boolean.FALSE;
		}
		catch (Exception e)
		{
			log.warn("ERRO NO MÉTODO: \"inserePersonalizados\"");
			e.printStackTrace();
			return Boolean.FALSE;
		}
	}

	private static Boolean validaVT(Integer NumCad, Integer numEmp, Double numVT, String campo)
	{
//		String consulta = " select numcad from  r034fun where " + campo + " =" + numVT + "and numcad = " + NumCad;
//		Integer nr = (Integer) selectColumn(consulta, "numcad");
//
//		//Registro ainda não criado
//		if (nr == null || nr.intValue() == 0)
//		{
//			String query = " update r034fun set " + campo + "  = " + numVT + " where numcad = " + NumCad;
//			return executeQuery(query);
//		}
//		else
//			return true;
		
		String query = " update r034fun set " + campo + "  = " + numVT + " where numcad = " + NumCad + " and numemp = " + numEmp;
		return executeQuery(query);
	}

	public static Integer numAte(Long numemp, Long codmed)
	{
		Integer numAte = null;

		try
		{
			StringBuffer varname1 = new StringBuffer();
			varname1.append("  SELECT top 1 numate + 1 AS numate ");
			varname1.append("    FROM r110mam AS A ");
			varname1.append("   WHERE A.numemp = ");
			varname1.append(numemp);
			varname1.append("     AND A.codate = ");
			varname1.append(codmed);
			varname1.append("     AND NOT EXISTS (SELECT * ");
			varname1.append("                       FROM r110mam AS B ");
			varname1.append("                      WHERE B.numemp = ");
			varname1.append(numemp);
			varname1.append("                        AND B.codate = ");
			varname1.append(codmed);
			varname1.append("                        AND B.numate = A.numate + 1) ");
			varname1.append("     AND numate < (SELECT MAX(numate) FROM r110mam) ");
			varname1.append("order by 1");

			numAte = (Integer) selectColumn(varname1.toString(), "numate");
			if (numAte == null)
				numAte = 1;
		}
		catch (Exception e)
		{
			log.warn("");
			e.printStackTrace();
			numAte = 0;
		}
		return numAte;
	}
}
