package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;

public class RotinaAlterarInicioFaturamentoPublicoPrivado implements CustomJobAdapter
{
	@Override
	public void execute(CustomJobContext arg0)
	{
		Connection connSig = PersistEngine.getConnection("SIGMA90");
		Connection connSap = PersistEngine.getConnection("SAPIENS");
		StringBuilder sql = new StringBuilder();
		ResultSet rs = null;
 		PreparedStatement psSig = null;
 		PreparedStatement psSap = null;
		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.RotinaAlterarInicioFaturamento");
		log.warn("##### INICIO AGENDADOR DE TAREFA: Rotina Alterar Inicio Faturamento - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		
		Long key = GregorianCalendar.getInstance().getTimeInMillis(); 
		
		Integer tipemc = 4;
		
		try
		{
			sql.append(" select ord.ID_ORDEM,cen.CD_CLIENTE,cen.ID_CENTRAL,cen.PARTICAO,ctr.usu_codemp,ctr.usu_codfil,ctr.usu_numctr,pos.usu_numpos,pos.usu_codser,ctr.usu_datini as datIniCtr,pos.usu_datini as datIniPos,ord.FECHAMENTO as FECHAMENTO,cli.tipemc     ");
			sql.append(" from [FSOODB03\\SQL01].SIGMA90.dbo.dbORDEM ord                                                                                                                                                                                                ");
			sql.append(" inner join [FSOODB03\\SQL01].SIGMA90.dbo.dbCENTRAL cen on cen.CD_CLIENTE = ord.CD_CLIENTE 																																                       ");
			sql.append(" inner join [fsoodb04\\sql02].sapiens.dbo.usu_t160sig sig on sig.usu_codcli = ord.cd_cliente 																		 							                                               ");
			sql.append(" inner join [fsoodb04\\sql02].sapiens.dbo.usu_t160cvs pos on pos.usu_codemp = sig.usu_codemp and pos.usu_codfil = sig.usu_codfil and pos.usu_numctr = sig.usu_numctr and pos.usu_numpos = sig.usu_numpos 						               "); 
			sql.append(" inner join [fsoodb04\\sql02].sapiens.dbo.usu_t160ctr ctr on ctr.usu_codemp = pos.usu_codemp and ctr.usu_codfil = pos.usu_codfil and ctr.usu_numctr = pos.usu_numctr 							                                               ");
			sql.append(" inner join [fsoodb04\\sql02].sapiens.dbo.E085CLI cli on cli.codcli = ctr.usu_codcli                                                                                                                                                           ");
			sql.append(" where ord.IDOSDEFEITO in (134,178,184)                                                                                                                                                                                                        ");
		    sql.append(" and ord.FECHAMENTO >= '2017-08-19'                                                                                                                                                                                                            ");
			sql.append(" and (ord.FECHADO = 1 and cen.FG_ATIVO = 1 and cen.CTRL_CENTRAL = 1)																			                                                                                               ");
			sql.append(" and ((ctr.usu_susfat = 'S') OR (pos.usu_susfat = 'S'))                                                            			                                                                                                                   ");
			sql.append(" and (ctr.usu_sitctr = 'A' or (ctr.usu_sitctr = 'I' and ctr.usu_datfim > getdate())) 			                                                                                                                                               ");
			sql.append(" and (pos.usu_sitcvs = 'A' or (pos.usu_sitcvs = 'I' and pos.usu_datfim > getdate())) 			                                                                                                                                               ");
			sql.append(" and pos.usu_datini <= cast(floor(cast(ord.FECHAMENTO as float)) as datetime) 					                                                                                                                                               ");
			sql.append(" group by ord.ID_ORDEM,cen.CD_CLIENTE,cen.ID_CENTRAL,cen.PARTICAO,ctr.usu_codemp,ctr.usu_codfil,ctr.usu_numctr,pos.usu_numpos,pos.usu_codser,ctr.usu_datini,pos.usu_datini,ord.FECHAMENTO,cli.tipemc                                           ");
			sql.append(" UNION ALL                                                                                                                             															                                               ");
			sql.append(" select ord.ID_ORDEM,cen.CD_CLIENTE,cen.ID_CENTRAL,cen.PARTICAO,ctr.usu_codemp,ctr.usu_codfil,ctr.usu_numctr,pos.usu_numpos,pos.usu_codser, ctr.usu_datini as datIniCtr,pos.usu_datini as datIniPos,p.DT_PAUSA as FECHAMENTO,cli.tipemc        ");
			sql.append(" from [FSOODB03\\SQL01].SIGMA90.dbo.dbORDEM ord                                                                                                                                                                                                ");
			sql.append(" inner join [FSOODB03\\SQL01].SIGMA90.dbo.dbCENTRAL cen on cen.CD_CLIENTE = ord.CD_CLIENTE                                                                                                                                                     ");
			sql.append(" inner join [FSOODB03\\SQL01].SIGMA90.dbo.ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p on p.CD_ORDEM_SERVICO = ord.ID_ORDEM AND p.CD_ORDEM_SERVICO_MOTIVO_PAUSA = (SELECT MAX(p2.CD_ORDEM_SERVICO_MOTIVO_PAUSA) FROM                                     ");
			sql.append(" [FSOODB03\\SQL01].SIGMA90.dbo.ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p2 WHERE p2.CD_ORDEM_SERVICO = p.CD_ORDEM_SERVICO)                                                                                                                             ");
			sql.append(" inner join [FSOODB04\\SQL02].sapiens.dbo.usu_t160sig sig on sig.usu_codcli = ord.cd_cliente                                                                                                                                                   ");
			sql.append(" inner join [FSOODB04\\SQL02].sapiens.dbo.usu_t160cvs pos on pos.usu_codemp = sig.usu_codemp and pos.usu_codfil = sig.usu_codfil and pos.usu_numctr = sig.usu_numctr and pos.usu_numpos = sig.usu_numpos                                       ");
			sql.append(" inner join [FSOODB04\\SQL02].sapiens.dbo.usu_t160ctr ctr on ctr.usu_codemp = pos.usu_codemp and ctr.usu_codfil = pos.usu_codfil and ctr.usu_numctr = pos.usu_numctr                                                                           ");
			sql.append(" inner join [FSOODB04\\SQL02].sapiens.dbo.E085CLI cli on cli.codcli = ctr.usu_codcli                                                                                                                                                           ");
			sql.append(" where ord.IDOSDEFEITO in (134,178,184)                                                                                                                                                                                                        ");
			sql.append(" and ord.FECHAMENTO >= '2017-08-19'                                                                                                                                                                                                            ");
			sql.append(" and (ord.FECHADO = 3 and cen.FG_ATIVO = 1 and cen.CTRL_CENTRAL = 1)                                                                                                                                                                           ");
			sql.append(" and ((ctr.usu_susfat = 'S') or (pos.usu_susfat = 'S'))                                                                                                                                                                                        ");
			sql.append(" and (ctr.usu_sitctr = 'A' or (ctr.usu_sitctr = 'I' and ctr.usu_datfim > getdate()))                                                                                                                                                           ");
			sql.append(" and (pos.usu_sitcvs = 'A' or (pos.usu_sitcvs = 'I' and pos.usu_datfim > getdate()))                                                                                                                                                           ");
			sql.append(" and pos.usu_datini <= cast(floor(cast(p.DT_PAUSA as float)) as datetime)                                                                                                                                                                      ");
			sql.append(" group by ord.ID_ORDEM,cen.CD_CLIENTE,cen.ID_CENTRAL,cen.PARTICAO,ctr.usu_codemp,ctr.usu_codfil,ctr.usu_numctr,pos.usu_numpos,pos.usu_codser,ctr.usu_datini,pos.usu_datini,p.DT_PAUSA,cli.tipemc                                               ");
			sql.append(" order by 5,6,7,8                                                                                                                                                                                                                              ");
			
			
			psSig = connSig.prepareStatement(sql.toString());
			rs = psSig.executeQuery();
			Integer numCtrAnt = 0;
			
			StringBuilder htmlFimPrivado = new StringBuilder();
			StringBuilder htmlFimPublico = new StringBuilder();
			StringBuilder htmlPos = new StringBuilder();
			StringBuilder htmlPosPrivado = new StringBuilder();
			StringBuilder htmlPosPublico = new StringBuilder();
			
			htmlFimPrivado.append("\n <!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\"> ");
			htmlFimPrivado.append("\n <html> ");
			htmlFimPrivado.append("\n 		<head> ");
			htmlFimPrivado.append("\n 			<meta charset=\"ISO-8859-1\"> ");
			htmlFimPrivado.append("\n 			<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"> ");
			htmlFimPrivado.append("\n 		</head> ");
			htmlFimPrivado.append("\n 		<body yahoo=\"yahoo\"> ");
			
			htmlFimPrivado.append("\n 		Prezado(a), ");
			htmlFimPrivado.append("\n 		<br> ");
			htmlFimPrivado.append("\n 		Segue abaixo listagem de contratos e postos que foram ajustados a data inicio do faturamento conforme fechamento de OS: ");
			htmlFimPrivado.append("\n 		<br> ");
			htmlFimPrivado.append("\n 		<br> ");
			
			htmlFimPublico.append("\n <!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\"> ");
			htmlFimPublico.append("\n <html> ");
			htmlFimPublico.append("\n 		<head> ");
			htmlFimPublico.append("\n 			<meta charset=\"ISO-8859-1\"> ");
			htmlFimPublico.append("\n 			<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"> ");
			htmlFimPublico.append("\n 		</head> ");
			htmlFimPublico.append("\n 		<body yahoo=\"yahoo\"> ");
			
			htmlFimPublico.append("\n 		Prezado(a), ");
			htmlFimPublico.append("\n 		<br> ");
			htmlFimPublico.append("\n 		Segue abaixo listagem de contratos e postos que foram ajustados a data inicio do faturamento conforme fechamento de OS: ");
			htmlFimPublico.append("\n 		<br> ");
			htmlFimPublico.append("\n 		<br> ");
			
			htmlPos.append("\n 			<table width=\"100%\"cellspacing=\"0\" cellpadding=\"2\" border=\"1\"> ");
			htmlPos.append("\n 					<tr> ");
			htmlPos.append("\n 						<td colspan=\"10\"align=\"center\">");
			htmlPos.append("\n 							<strong>Lista de Postos</strong> ");
			htmlPos.append("\n 						</td> ");
			htmlPos.append("\n 					</tr> ");
			htmlPos.append("\n 					<tr> ");
			htmlPos.append("\n 						<td align=\"center\"> ");
			htmlPos.append("\n 							<strong>OS</strong> ");
			htmlPos.append("\n 						</td> ");
			htmlPos.append("\n 						<td align=\"center\"> ");
			htmlPos.append("\n 							<strong>Conta Sigma</strong> ");
			htmlPos.append("\n 						</td> ");
			htmlPos.append("\n 						<td align=\"center\"> ");
			htmlPos.append("\n 							<strong>Empresa</strong> ");
			htmlPos.append("\n 						</td> ");
			htmlPos.append("\n 						<td align=\"center\"> ");
			htmlPos.append("\n 							<strong>Filial</strong> ");
			htmlPos.append("\n 						</td> ");
			htmlPos.append("\n 						<td align=\"center\"> ");
			htmlPos.append("\n 							<strong>Contrato</strong> ");
			htmlPos.append("\n 						</td> ");
			htmlPos.append("\n 						<td align=\"center\"> ");
			htmlPos.append("\n 							<strong>Data inicio Contrato</strong> ");
			htmlPos.append("\n 						</td> ");
			htmlPos.append("\n 						<td align=\"center\"> ");
			htmlPos.append("\n 							<strong>Posto</strong> ");
			htmlPos.append("\n 						</td> ");
			htmlPos.append("\n 						<td align=\"center\"> ");
			htmlPos.append("\n 							<strong>Data inicio Posto</strong> ");
			htmlPos.append("\n 						</td> ");
			htmlPos.append("\n 						<td align=\"center\"> ");
			htmlPos.append("\n 							<strong>Data inicio ajustada de ambos</strong>  ");
			htmlPos.append("\n 						</td> ");
			htmlPos.append("\n 					</tr>");
			
			htmlFimPrivado.append(htmlPosPrivado);
			htmlFimPrivado.append(htmlPos);
			htmlFimPublico.append(htmlPosPublico);
			htmlFimPublico.append(htmlPos);
			
			
			StringBuilder sqlSap = new StringBuilder();
			
			while (rs.next())
			{
				Integer idOS = rs.getInt("ID_ORDEM");
				String idCentral = rs.getString("ID_CENTRAL");
				String particao = rs.getString("PARTICAO");
				Integer codEmp = rs.getInt("usu_codemp");
				Integer codFil = rs.getInt("usu_codfil");
				Integer numCtr = rs.getInt("usu_numctr");
				Integer numPos = rs.getInt("usu_numpos");
				Date datIniCtr = rs.getDate("datIniCtr");
				Date datIniPos = rs.getDate("datIniPos");
				Date datFimOs = rs.getDate("FECHAMENTO");
				tipemc = rs.getInt("tipemc");
				
				String codSer = rs.getString("usu_codser");
				
				if (codSer != null && codSer.equals("9002010")){
				    sqlSap.append(" Update USU_T160CVS Set usu_susfat = 'N', usu_libfat = 'S' Where usu_codemp = " + codEmp + " And usu_codfil = " + codFil + " And usu_numctr = " + numCtr + " And usu_numpos = " + numPos + "; ");
				}else{
				    sqlSap.append(" Update USU_T160CVS Set usu_datini = '" + NeoDateUtils.safeDateFormat(datFimOs, "yyyy-MM-dd") + "', usu_susfat = 'N', usu_libfat = 'S' Where usu_codemp = " + codEmp + " And usu_codfil = " + codFil + " And usu_numctr = " + numCtr + " And usu_numpos = " + numPos + "; ");
				}
				
				htmlPos = new StringBuilder();
				
				htmlPos.append("\n 					<tr> ");
				htmlPos.append("\n 						<td> ");
				htmlPos.append("\n 							"+idOS);
				htmlPos.append("\n 						</td> ");
				htmlPos.append("\n 						<td> ");
				htmlPos.append("\n 							"+idCentral+"["+particao+"]");
				htmlPos.append("\n 						</td> ");
				htmlPos.append("\n 						<td> ");
				htmlPos.append("\n 							"+codEmp);
				htmlPos.append("\n 						</td> ");
				htmlPos.append("\n 						<td> ");
				htmlPos.append("\n 							"+codFil);
				htmlPos.append("\n 						</td> ");
				htmlPos.append("\n 						<td> ");
				htmlPos.append("\n 							"+numCtr);
				htmlPos.append("\n 						</td> ");
				htmlPos.append("\n 						<td> ");
				htmlPos.append("\n 							"+NeoDateUtils.safeDateFormat(datIniCtr, "dd/MM/yyyy"));
				htmlPos.append("\n 						</td> ");
				htmlPos.append("\n 						<td> ");
				htmlPos.append("\n 							"+numPos);
				htmlPos.append("\n 						</td> ");
				htmlPos.append("\n 						<td> ");
				htmlPos.append("\n 							"+NeoDateUtils.safeDateFormat(datIniPos, "dd/MM/yyyy"));
				htmlPos.append("\n 						</td> ");
				htmlPos.append("\n 						<td> ");
				htmlPos.append("\n 							"+NeoDateUtils.safeDateFormat(datFimOs, "dd/MM/yyyy"));
				htmlPos.append("\n 						</td> ");
				htmlPos.append("\n 					</tr>");
				
				if (numCtrAnt == 0 || !numCtrAnt.toString().equals(numCtr.toString()))
				{
					sqlSap.append(" Update USU_T160CTR Set usu_datini = '" + NeoDateUtils.safeDateFormat(datFimOs, "yyyy-MM-dd") + "', usu_susfat = 'N', usu_libfat = 'S' Where usu_codemp = " + codEmp + " And usu_codfil = " + codFil + " And usu_numctr = " + numCtr + " And usu_susfat = 'S'; ");
				}
				numCtrAnt = numCtr;
				
				switch (tipemc) {
					case 0:
						htmlPosPrivado.append(htmlPos.toString());
						htmlPosPublico.append(htmlPos.toString());
						break;
					case 1:
						htmlPosPrivado.append(htmlPos.toString());
						break;
					case 2:
						htmlPosPublico.append(htmlPos.toString());
						break;
					default:
						break;
				}
			}
			if (!sqlSap.toString().isEmpty()){
				System.out.println(sqlSap);
			    /*connSap.prepareStatement(sqlSap.toString());
			    psSap = connSap.prepareStatement(sqlSap.toString());
			    psSap.executeUpdate();*/
			}
			htmlFimPrivado.append(htmlPosPrivado);
			
			htmlFimPrivado.append("\n 			</table> ");
			htmlFimPrivado.append("\n 		</body> ");
			htmlFimPrivado.append("\n </html> ");
			
			htmlFimPublico.append(htmlPosPublico);
			
			htmlFimPublico.append("\n 			</table> ");
			htmlFimPublico.append("\n 		</body> ");
			htmlFimPublico.append("\n </html> ");
			
			System.out.println(htmlFimPrivado.toString());
			System.out.println(htmlFimPublico.toString());
			
			
			//String emailPrivado = "liberacaofaturamentoprivado@orsegups.com.br";
			//String emailPublico = "liberacaofaturamentopublico@orsegups.com.br";
			String emailPrivado = "carlos.silva@orsegups.com.br";
			String emailPublico = "carlos.silva@orsegups.com.br";
			
			OrsegupsUtils.sendEmail2Orsegups(emailPrivado, "fusion@orsegups.com.br", "[SAPIENS] Alteração da data inicio do contrato e posto (Privado)", "", htmlFimPrivado.toString());
			OrsegupsUtils.sendEmail2Orsegups(emailPublico, "fusion@orsegups.com.br", "[SAPIENS] Alteração da data inicio do contrato e posto (Público)", "", htmlFimPublico.toString());
			
						
		}
		catch (Exception e)
		{
			log.error("##### AGENDADOR DE TAREFA: Rotina Faturamento On Demand");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
		}
		finally
		{

			try
			{
				rs.close();
				if (psSap != null){
				    psSap.close();
				}
				psSig.close();
				connSap.close();
				connSig.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
	}
}