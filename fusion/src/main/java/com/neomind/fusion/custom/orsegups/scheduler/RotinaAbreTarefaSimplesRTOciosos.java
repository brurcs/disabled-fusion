package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.UserActivity;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoUtils;

public class RotinaAbreTarefaSimplesRTOciosos implements CustomJobAdapter {

	@Override
	public void execute(CustomJobContext ctx) {
		System.out.println("#### Iniciando Processo de Abertura de Tarefas de RTs Ociosos####");
		GregorianCalendar dataAtual = new GregorianCalendar();
		dataAtual.add(GregorianCalendar.MONTH, 1);
		dataAtual.set(GregorianCalendar.DATE, 1);
		System.out.println(NeoUtils.safeDateFormat(dataAtual, "dd/MM/yyyy HH:mm"));
		
		StringBuilder sql = new StringBuilder();
		
		sql.append(" ");
		
		/*sql.append(" SELECT  getdate()-4,hch.NumCra,fun.NumEmp, fun.TipCol, fun.NumCad, fun.NomFun, fun.NumCpf, orn.numloc, orn.NomLoc, orn.USU_LotOrn, orn.usu_codreg, "); 
		sql.append(" esc.NomEsc, hes.CodEsc, hes.CodTma, cvs.USU_NumLoc, ISNULL(afa.SitAfa, 1) AS SitAfa, ISNULL(sit.DesSit, 'Trabalhando') AS DesSit, ISNULL(sit.usu_blopre, 'N') AS BloPre, "); 
		sql.append(" afa.DatAfa AS dataAfa, afa.DatTer, car.titred, orn.nomloc, fun.codccu ");
		sql.append(" FROM R034FUN fun   ");
		sql.append(" LEFT JOIN R038HCH hch WITH (NOLOCK) ON hch.NUMEMP = fun.NumEmp AND fun.TIPCOL = hch.TIPCOL "); 
		sql.append("                                                                AND fun.NUMCAD = hch.NUMCAD "); 
		sql.append("                                                                AND DATEADD(MINUTE, hch.HorIni, hch.DatIni) = (SELECT MAX(DATEADD(MINUTE, hch2.HorIni, hch2.DatIni)) "); 
		sql.append("                                                                                                                 FROM R038HCH hch2 "); 
		sql.append("                                                                                                                 WHERE hch2.NUMEMP = hch.NUMEMP "); 
		sql.append("                                                                                                                 AND hch2.TIPCOL = hch.TIPCOL "); 
		sql.append("                                                                                                                 AND hch2.NUMCAD = hch.NUMCAD "); 
		sql.append("                                                                                                                 AND DATEADD(MINUTE, hch2.HorIni, hch2.DatIni) <= getdate()-1 ");
		sql.append("                                                                                                               ) ");
		sql.append(" INNER JOIN R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp AND hlo.TipCol = fun.TipCol ");     
		sql.append("                                                                 AND hlo.NumCad = fun.NumCad "); 
		sql.append("                                                                 AND hlo.DatAlt = (SELECT MAX (DATALT) "); 
		sql.append("                                                                                    FROM R038HLO TABELA001  ");
		sql.append("                                                                                    WHERE TABELA001.NUMEMP = hlo.NUMEMP "); 
		sql.append("                                                                                    AND TABELA001.TIPCOL = hlo.TIPCOL "); 
		sql.append("                                                                                    AND TABELA001.NUMCAD = hlo.NUMCAD "); 
		sql.append("                                                                                    AND TABELA001.DATALT <= getdate()-1 ");
		sql.append("                                                                                  ) "); 
		sql.append(" left JOIN R038HES hes WITH (NOLOCK) ON hes.NumEmp = fun.NumEmp AND hes.TipCol = fun.TipCol "); 
		sql.append("                                                                 AND hes.NumCad = fun.NumCad "); 
		sql.append("                                                                 AND hes.DatAlt = (SELECT MAX (DATALT) "); 
		sql.append("                                                                                     FROM R038HES TABELA001 "); 
		sql.append("                                                                                     WHERE TABELA001.NUMEMP = hes.NUMEMP "); 
		sql.append("                                                                                     AND TABELA001.TIPCOL = hes.TIPCOL "); 
		sql.append("                                                                                     AND TABELA001.NUMCAD = hes.NUMCAD  ");
		sql.append("                                                                                     AND TABELA001.DATALT <= getdate()-1 ");
		sql.append("                                                                                  ) "); 
		sql.append(" INNER JOIN R006ESC esc ON esc.CodEsc = hes.CodEsc "); 
		sql.append(" INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.NumLoc = hlo.NumLoc and orn.TabOrg = 203 "); 
		sql.append(" INNER JOIN USU_T038CVS cvs WITH (NOLOCK) ON cvs.USU_NumLoc = orn.NumLoc AND cvs.USU_TabOrg = orn.TabOrg AND cvs.USU_SeqAlt = (Select MAX(c2.usu_seqalt) "); 
		sql.append("                                                                                                                                 FROM USU_T038CVS c2 "); 
		sql.append("                                                                                                                                 Where c2.usu_taborg = cvs.usu_taborg "); 
		sql.append("                                                                                                                                 And c2.usu_numloc = cvs.usu_numloc "); 
		sql.append("                                                                                                                                 And c2.usu_datalt <= getdate()-1) "); 
		sql.append(" left JOIN USU_TFisRes fis ON fis.USU_FisRes = fun.usu_fisres "); 
		sql.append(" LEFT JOIN R038AFA afa WITH (NOLOCK) ON (afa.NumEmp = fun.NumEmp AND afa.TipCol = fun.TipCol AND afa.NumCad = fun.NumCad AND afa.DatAfa = (SELECT MAX (DATAFA) "); 
		sql.append("                                                                                                                                             FROM R038AFA TABELA001 "); 
		sql.append("                                                                                                                                             WHERE TABELA001.NUMEMP = afa.NUMEMP "); 
		sql.append("                                                                                                                                             AND TABELA001.TIPCOL = afa.TIPCOL "); 
		sql.append("                                                                                                                                             AND TABELA001.NUMCAD = afa.NUMCAD "); 
		sql.append("                                                                                                                                             AND TABELA001.DATAFA <= getdate()-1 "); 
		sql.append("                                                                                                                                             AND (TABELA001.DatTer = '1900-12-31 00:00:00' "); 
		sql.append("                                                                                                                                             OR TABELA001.DatTer >= cast(floor(cast(getdate()-1 as float)) as datetime))) ");
		sql.append("                                                                                                                                           ) "); 
		sql.append(" LEFT JOIN R010SIT sit ON sit.CodSit = afa.SitAfa ");
		sql.append(" LEFT JOIN R038HCA hca WITH (NOLOCK) ON hca.NumEmp = fun.NumEmp AND hca.TipCol = fun.TipCol AND hca.NumCad = fun.NumCad AND hca.DatAlt = (SELECT MAX (DATALT) FROM R038HCA TABELA001 WHERE TABELA001.NUMEMP = hca.NUMEMP AND TABELA001.TIPCOL = hca.TIPCOL AND TABELA001.NUMCAD = hca.NUMCAD AND TABELA001.DATALT <= getdate()-1) ");
		sql.append(" LEFT JOIN R024CAR car ON car.EstCar = hca.EstCar AND car.CodCar = hca.CodCar ");
		sql.append(" where orn.NomLoc like '%Reserva%' ");
		sql.append(" and fun.DatAdm <= getdate()-1 ");
		sql.append(" and fun.tipcol = 1 ");
		sql.append(" and ( afa.SitAfa IS NULL ) ");
		sql.append(" and not exists (select 1 from  USU_T038COBFUN cob where  usu_numemp = fun.numemp and usu_tipcol= fun.tipcol and usu_numcad =fun.numcad and getdate()-1 between usu_datinccob and  usu_datfim) ");
		sql.append(" and exists (select acc.DatAcc from R070ACC  acc where numemp = fun.numemp and tipcol = fun.tipcol and acc.Numcad = fun.numcad and acc.usu_numloc = orn.numloc  and acc.datacc = dateadd(d, datediff(d,0, getdate()-1), 0)) ");
		sql.append(" order by orn.NomLoc, fun.NomFun ");*/
		
		
		 
		 
		//sql.append(" SELECT @DatRef = '2015-05-08'  ");
		sql.append(" DECLARE @DatRef  Datetime ");
		sql.append("  SELECT @DatRef = dateadd(d, datediff(d,0, getdate()-1), 0) ");  
		sql.append("  SELECT DISTINCT hch.NumCra, fun.NumEmp, fun.TipCol, fun.NumCad, fun.NomFun, fun.NumCpf, orn.NumLoc, orn.NomLoc, orn.USU_LotOrn, orn.USU_CodReg, esc.NomEsc, ");   
		sql.append("  hes.CodEsc, hes.CodTma, ISNULL(afa.SitAfa, 1) AS SitAfa, ISNULL(sit.DesSit, 'Trabalhando') AS DesSit, ISNULL(sit.USU_BloPre, 'N') AS BloPre, ");
		sql.append("  afa.DatAfa AS DataAfa, afa.DatTer, car.TitRed, orn.NomLoc, fun.CodCcu, DATEADD(MINUTE, ace.HorAcc, ace.DatAcc), DATEADD(MINUTE, acs.HorAcc, acs.DatAcc), @DatRef, reg.usu_nomreg  ");  
		sql.append("  FROM R034FUN fun WITH (NOLOCK) ");  
		sql.append("  LEFT JOIN R038HCH hch WITH (NOLOCK) ON hch.NUMEMP = fun.NumEmp AND fun.TIPCOL = hch.TIPCOL ");   
		sql.append("                                                                AND fun.NUMCAD = hch.NUMCAD "); 
		sql.append("                                                                AND DATEADD(MINUTE, hch.HorIni, hch.DatIni) = (SELECT MAX(DATEADD(MINUTE, hch2.HorIni, hch2.DatIni)) ");   
		sql.append("                                                                                                                 FROM R038HCH hch2 ");   
		sql.append("                                                                                                                 WHERE hch2.NUMEMP = hch.NUMEMP ");   
		sql.append("                                                                                                                 AND hch2.TIPCOL = hch.TIPCOL ");   
		sql.append("                                                                                                                 AND hch2.NUMCAD = hch.NUMCAD    ");
		sql.append("                                                                                                                 AND DATEADD(MINUTE, hch2.HorIni, hch2.DatIni) <= @DatRef ");
		sql.append("																											  )	");	
		sql.append("  INNER JOIN R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp AND hlo.TipCol = fun.TipCol   ");
		sql.append("                                                                 AND hlo.NumCad = fun.NumCad   ");
		sql.append("                                                                 AND hlo.DatAlt = (SELECT MAX (DATALT) ");  
		sql.append("                                                                                    FROM R038HLO TABELA001    ");
		sql.append("                                                                                    WHERE TABELA001.NUMEMP = hlo.NUMEMP ");  
		sql.append("                                                                                    AND TABELA001.TIPCOL = hlo.TIPCOL ");  
		sql.append("                                                                                    AND TABELA001.NUMCAD = hlo.NUMCAD ");  
		sql.append("                                                                                    AND TABELA001.DATALT <= @DatRef ");  
		sql.append("                                                                                  ) ");  
		sql.append("  LEFT  JOIN R038HES hes WITH (NOLOCK) ON hes.NumEmp = fun.NumEmp AND hes.TipCol = fun.TipCol ");  
		sql.append("                                                                 AND hes.NumCad = fun.NumCad ");  
		sql.append("                                                                 AND hes.DatAlt = (SELECT MAX (DATALT) ");  
		sql.append("                                                                                     FROM R038HES TABELA001 ");  
		sql.append("                                                                                     WHERE TABELA001.NUMEMP = hes.NUMEMP ");  
		sql.append("                                                                                     AND TABELA001.TIPCOL = hes.TIPCOL   ");
		sql.append("                                                                                     AND TABELA001.NUMCAD = hes.NUMCAD    ");
		sql.append("                                                                                     AND TABELA001.DATALT <= @DatRef   ");
		sql.append("                                                                                  )   ");
		sql.append("  LEFT JOIN USU_TFisRes fis WITH (NOLOCK) ON fis.USU_FisRes = fun.usu_fisres   ");
		sql.append("  LEFT JOIN R038AFA afa WITH (NOLOCK) ON (afa.NumEmp = fun.NumEmp AND afa.TipCol = fun.TipCol AND afa.NumCad = fun.NumCad AND afa.DatAfa = (SELECT MAX (DATAFA) ");  
		sql.append("                                                                                                                                             FROM R038AFA TABELA001   ");
		sql.append("                                                                                                                                             WHERE TABELA001.NUMEMP = afa.NUMEMP ");  
		sql.append("                                                                                                                                             AND TABELA001.TIPCOL = afa.TIPCOL   ");
		sql.append("                                                                                                                                             AND TABELA001.NUMCAD = afa.NUMCAD   ");
		sql.append("                                                                                                                                             AND TABELA001.DATAFA <= getdate()-1   ");
		sql.append("                                                                                                                                             AND (TABELA001.DatTer = '1900-12-31 00:00:00' ");  
		sql.append("                                                                                                                                             OR TABELA001.DatTer >= cast(floor(cast(@DatRef as float)) as datetime))) ");  
		sql.append("																											  								)	");
		sql.append("  LEFT JOIN R010SIT sit WITH (NOLOCK) ON sit.CodSit = afa.SitAfa   ");
		sql.append("  LEFT JOIN R038HCA hca WITH (NOLOCK) ON hca.NumEmp = fun.NumEmp AND hca.TipCol = fun.TipCol AND hca.NumCad = fun.NumCad AND hca.DatAlt = (SELECT MAX (DATALT) FROM R038HCA TABELA001 WHERE TABELA001.NUMEMP = hca.NUMEMP AND TABELA001.TIPCOL = hca.TIPCOL AND TABELA001.NUMCAD = hca.NUMCAD AND TABELA001.DATALT <= @DatRef) ");  
		sql.append("  LEFT JOIN R024CAR car WITH (NOLOCK) ON car.EstCar = hca.EstCar AND car.CodCar = hca.CodCar   ");
		sql.append("  INNER JOIN R006ESC esc WITH (NOLOCK) ON esc.CodEsc = hes.CodEsc   ");
		sql.append("  INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.NumLoc = hlo.NumLoc AND orn.TabOrg = hlo.TabOrg ");
		sql.append("  INNER JOIN usu_t200reg reg on orn.usu_codreg = reg.USU_CodReg ");
		sql.append("  INNER JOIN R070ACC acs WITH (NOLOCK) ON acs.TipAcc = 100 AND acs.NumCra = hch.NumCra and acs.USU_NumLoc = orn.NumLoc AND acs.DirAcc = 'S' AND acs.DatAcc = @DatRef AND DATEADD(SECOND, acs.SeqAcc, DATEADD(MINUTE, acs.HorAcc, acs.DatAcc)) = (SELECT MAX(DATEADD(SECOND, acs2.SeqAcc, DATEADD(MINUTE, acs2.HorAcc, acs2.DatAcc))) FROM R070ACC acs2 WITH (NOLOCK) WHERE acs2.TipAcc = 100 AND acs2.NumCra = acs.NumCra AND acs2.USU_NumLoc = acs.USU_NumLoc AND acs2.DatAcc = acs.DatAcc AND acs2.DirAcc = acs.DirAcc) "); 
		sql.append("  INNER JOIN R070ACC ace WITH (NOLOCK) ON ace.TipAcc = 100 AND ace.NumCra = hch.NumCra AND ace.USU_NumLoc = orn.NumLoc AND ace.DirAcc = 'E' AND DATEADD(SECOND, ace.SeqAcc, DATEADD(MINUTE, ace.HorAcc, ace.DatAcc)) = (SELECT MAX(DATEADD(SECOND, ace2.SeqAcc, DATEADD(MINUTE, ace2.HorAcc, ace2.DatAcc))) FROM R070ACC ace2 WITH (NOLOCK) WHERE ace2.TipAcc = 100 AND ace2.NumCra = ace.NumCra AND ace2.USU_NumLoc = ace.USU_NumLoc AND ace2.DirAcc = ace.DirAcc AND DATEADD(MINUTE, ace2.HorAcc, ace2.DatAcc) < DATEADD(MINUTE, acs.HorAcc, acs.DatAcc) ) "); 
		sql.append("  WHERE (orn.NomLoc like '%Reserva%' OR orn.NomLoc like '%Cobertura%' OR orn.NomLoc like '%aviso%pr%vio%' OR orn.NomLoc like '%afastado%' OR orn.NomLoc like '%FIS COB%') "); 
		sql.append("  and orn.nomloc not like '%almoço%' ");
		sql.append("  AND orn.USU_NumCtr = 1 AND fun.TipCol = 1 ");  
		sql.append("  AND NOT ( ");
		sql.append("  EXISTS (SELECT 1 FROM USU_T038COBFUN cob WITH (NOLOCK), USU_T010MoCo mc WITH (NOLOCK) WHERE mc.USU_CodMot = cob.USU_CodMot AND mc.USU_TipCob IN ('D', 'R') AND cob.USU_NumEmp = fun.NumEmp AND cob.USU_TipCol= fun.TipCol AND cob.USU_NumCad = fun.NumCad and cob.USU_DatAlt >= DATEADD(MINUTE, ace.HorAcc, ace.DatAcc) AND cob.USU_DatAlt <= DATEADD(MINUTE, acs.HorAcc, acs.DatAcc)) ");  // cobertura entre um 
	    sql.append("     OR ");
	    sql.append("  EXISTS (SELECT 1 FROM USU_T038COBFUN cob WITH (NOLOCK), USU_T010MoCo mc WITH (NOLOCK) WHERE mc.USU_CodMot = cob.USU_CodMot AND mc.USU_TipCob IN ('D', 'R') AND cob.USU_NumEmp = fun.NumEmp AND cob.USU_TipCol= fun.TipCol AND cob.USU_NumCad = fun.NumCad and DATEADD(MINUTE, acs.HorAcc, acs.DatAcc) >= cob.usu_datalt AND  DATEADD(MINUTE, acs.HorAcc, acs.DatAcc) <= cob.usu_datfim ) ");      
		/*
		sql.append("       EXISTS (SELECT 1 FROM USU_T038COBFUN cob WITH (NOLOCK), USU_T010MoCo mc WITH (NOLOCK) WHERE mc.USU_CodMot = cob.USU_CodMot AND mc.USU_TipCob IN ('D', 'R') AND cob.USU_NumEmp = fun.NumEmp AND cob.USU_TipCol= fun.TipCol AND cob.USU_NumCad = fun.NumCad and cob.USU_DatAlt BETWEEN DATEADD(MINUTE, ace.HorAcc, ace.DatAcc) AND DATEADD(MINUTE, acs.HorAcc, acs.DatAcc)) "); // cobertura entre um 
		sql.append("       OR ");
		sql.append("       EXISTS (SELECT 1 FROM USU_T038COBFUN cob WITH (NOLOCK), USU_T010MoCo mc WITH (NOLOCK) WHERE mc.USU_CodMot = cob.USU_CodMot AND mc.USU_TipCob IN ('D', 'R') AND cob.USU_NumEmp = fun.NumEmp AND cob.USU_TipCol= fun.TipCol AND cob.USU_NumCad = fun.NumCad and DATEADD(SECOND, acs.SeqAcc, DATEADD(MINUTE, acs.HorAcc, acs.DatAcc)) BETWEEN cob.usu_datalt AND cob.usu_datfim ) ");
		*/     
		sql.append("       ) ");
		sql.append("  order by orn.NomLoc, fun.NomFun ");
		  
		System.out.println("sql=" + sql.toString()); 

		
		Connection connection = OrsegupsUtils.getSqlConnection("VETORH") ;
		ResultSet rs = null;
		try
		{
			PreparedStatement pst = connection.prepareStatement(sql.toString());
			
			//pst.setDate(1, new Date(dataAtual.getTimeInMillis()));
			//pst.setDate(2, new Date(dataAtual.getTimeInMillis()));
			rs = pst.executeQuery();
			
			while (rs.next()){
				
				
					
				Long codReg = rs.getLong("usu_codreg");
				String lotacao = "";
				
				if (codReg == 0 && rs.getString("USU_LotOrn").equals("Coordenadoria Indenização Contratual"))
				{
					lotacao = rs.getString("NomLoc");
				}
				else if (codReg == 1L)
				{
					lotacao = rs.getString("NomLoc");
				}
				else if (codReg == 13L)
				{
					lotacao = rs.getString("NomLoc");
				}
				else
				{
					lotacao = rs.getString("USU_LotOrn");
				}
				
				System.out.println("[OCIOSO RT] - Abrir tarefa -> Funcionario: " + rs.getLong("numcad") + " - " + rs.getString("nomfun") + " codReg="+codReg+", lotacao"+lotacao );
				abrirTarefaSimples(rs.getLong("NumEmp"),rs.getLong("numcad"), rs.getString("nomfun"),codReg,rs.getString("usu_nomreg"),lotacao, rs.getString("titred"), rs.getString("nomloc"), rs.getString("codccu"));
					
				
			}
			rs.close();
			System.out.println("#### Finalizando Processo de Abertura de Tarefas de RTs Ociosos ####");
		}
		catch (SQLException e)
		{
			System.out.println(sql.toString());
			e.printStackTrace();
		}
		
	}
	
	public static void abrirTarefaSimples(Long numemp, Long numcad, String nomfun, Long codReg, String nomreg, String lotacao, String cargo, String nomloc, String centroCusto ){
		
		
		GregorianCalendar cpt = new GregorianCalendar();

		GregorianCalendar gc = new GregorianCalendar();
		gc.add(GregorianCalendar.DATE, -1);
		
		String msg = ""; //"<p>Colaborador ocioso lotado em RT:"+numemp + " - " +numcad+" - "+nomfun+".</p>";
		msg = "<p>Identificamos que o colaborador "+numemp+"/"+numcad+" - "+nomfun+", "+cargo+", lotado no posto "
				+ nomloc + " - "+centroCusto+", não realizou nenhuma cobertura no dia "+NeoUtils.safeDateFormat(gc,"dd/MM/yyyy")+", restando portanto ocioso.</p>"
				+"<p> Solicitamos informar o motivo.</p>";
		
		
		
		NeoObject oTarefa = AdapterUtils.createNewEntityInstance("Tarefa");
		
		final EntityWrapper wTarefa= new EntityWrapper(oTarefa);
		
		NeoUser usuarioSolicitante = PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "dilmoberger" /*"beatriz.malmann"*/) );
		NeoUser usuarioResponsavel = null;//PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "danilo.silva") );
		
		
		NeoPaper papel = /*OrsegupsUtils.getPapelJustificarR022(codReg, lotacao); */ OrsegupsUtils.getPapelRTOcioso(codReg, lotacao);

		if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
		{
			for (NeoUser user : papel.getUsers())
			{
				usuarioResponsavel = user;
				System.out.println("getPapel->"+user.getCode());
				break;
			}
		}
		
		
		
		GregorianCalendar prazo = new GregorianCalendar();
		prazo.add(GregorianCalendar.DAY_OF_MONTH, 1);
		prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
		prazo.set(GregorianCalendar.MINUTE, 23);
		prazo.set(GregorianCalendar.SECOND, 59);
		
		
		
		wTarefa.findField("Solicitante").setValue(usuarioSolicitante);
		wTarefa.findField("Executor").setValue(usuarioResponsavel);
		wTarefa.findField("Titulo").setValue("Ocioso em Reserva Técnica - ["+ nomreg + "] "+ numcad+ "-" + nomfun);
		wTarefa.findField("DescricaoSolicitacao").setValue(msg);
		wTarefa.findField("Prazo").setValue(OrsegupsUtils.getNextWorkDay(prazo));
		wTarefa.findField("dataSolicitacao").setValue(new GregorianCalendar());
		
		PersistEngine.persist(oTarefa);
		
		iniciaProcessoTarefaSimples(oTarefa, usuarioSolicitante, usuarioResponsavel);
	}

	public static Boolean iniciaProcessoTarefaSimples(NeoObject eformProcesso, NeoUser requester, NeoUser responsavel)
	{
		Boolean result = false;
	
		QLEqualsFilter equal = new QLEqualsFilter("Name", "G001 - Tarefa Simples");
		ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);
		
		WFProcess proc = processModel.startProcess(eformProcesso, true, null, null, null, null, requester);
		proc.setRequester(requester);
		System.out.println("[OCIOSO RT] - Abriu Tarefa simples n. " + proc.getCode());
	
		proc.setSaved(true);
	
		// Necessita persistir para que o metodo getOpenActivities() consiga buscar as atividades abertas
		PersistEngine.persist(proc);
		PersistEngine.commit(true);
	
		/* Finaliza a primeira tarefa */
		Task task = null;
	
		final List<Activity> acts = proc.getOpenActivities();
		System.out.println("acts: " + acts);
		if (acts != null)
		{
			System.out.println("acts.size(): " + acts.size());
		}
		final Activity activity1 = (Activity) acts.get(0);
		if (activity1 instanceof UserActivity)
		{
			try
			{
				if (((UserActivity) activity1).getTaskList().size() <= 0)
				{
					task = activity1.getModel().getTaskAssigner().assign((UserActivity) activity1, responsavel);
					OrsegupsWorkflowHelper.finishTaskByActivity(activity1);
				}
				else
				{
					task = ((UserActivity) activity1).getTaskList().get(0);
					OrsegupsWorkflowHelper.finishTaskByActivity(activity1);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				System.err.println("Erro ao iniciar a primeira atividade do processo.");
			}
		}
		return result;
	}

}
