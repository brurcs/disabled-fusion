package com.neomind.fusion.custom.orsegups.converter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;

public class OCPListaFotosChipOSConverter extends StringConverter
{
	private static final Log log = LogFactory.getLog(OCPListaFotosConverter.class);
	
	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin)
	{
		StringBuffer galeria = new StringBuffer();
		
		EntityWrapper processEntityWrapper = new EntityWrapper(field.getForm().getObject());
		Long idOrdem = (Long) processEntityWrapper.findValue("idOrdem");
		if (idOrdem != null) {	
			Connection conn = OrsegupsUtils.getSqlConnection("SIGMA90");

			StringBuffer sqlIdFotos = new StringBuffer();
			sqlIdFotos.append(" select CD_OS_IMAGEM from OS_IMAGEM where CD_ORDEM_SERVICO = " + idOrdem);		
			try {
				PreparedStatement st = conn.prepareStatement(sqlIdFotos.toString());
				ResultSet rs = st.executeQuery();
											
				sqlIdFotos.append("<div class=\"sidebar-image-gallery\">");
				while (rs.next()) {
					Long cdOsImagem = rs.getLong("CD_OS_IMAGEM");
					galeria.append("<a target=\"_blank\" href=\"/fusion/custom/jsp/orsegups/utils/fotoSIGMAFromDB.jsp?cdOsImagem="+cdOsImagem+ "\"><img width=\"10%\" height=\"10%\" border=\"5\" src=\"/fusion/custom/jsp/orsegups/utils/fotoSIGMAFromDB.jsp?cdOsImagem="+cdOsImagem+"\" width=\"\" height=\"\" alt=\"\"></a>&nbsp;");
				}
				sqlIdFotos.append("</div>");
				rs.close();
				st.close();

			} catch (Exception e) {
				e.printStackTrace();	
			}				
			
			//galeria.append("<input type=\"button\" class=\"input_button\" id=\"bt_import\" value=\""+cdHistorico+"\" onClick = \"javascript:void();\"/>");				
		}
		
		return galeria.toString();
	}
	
	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}
}
