package com.neomind.fusion.custom.orsegups.servicos.helpers;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.engine.runtime.RuntimeEngine;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilterIsNotNull;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.Task.Situation;
import com.neomind.fusion.workflow.TaskInstance;
import com.neomind.fusion.workflow.TaskInstanceHelper;
import com.neomind.fusion.workflow.TaskStatus;
import com.neomind.fusion.workflow.UserActivity;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.WorkFlowVersionControl;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.fusion.workflow.handler.ActivityHandler;
import com.neomind.fusion.workflow.handler.HandlerFactory;
import com.neomind.fusion.workflow.model.ActivityModel;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.fusion.workflow.model.ProcessModelProxy;
import com.neomind.fusion.workflow.service.PeopleAssignmentsService;
import com.neomind.fusion.workflow.service.TaskService;
import com.neomind.fusion.workflow.service.TaskServiceImpl;
import com.neomind.fusion.workflow.simulation.WorkflowService;

@SuppressWarnings({ "deprecation", "unchecked" })
public class WorkflowHelper
{
	private final WFProcess processo;

	public WorkflowHelper()
	{
		super();
		this.processo = null;
	}

	public WorkflowHelper(WFProcess processo)
	{
		super();
		this.processo = processo;
	}

	/**
	 * Método que avançaa a primeira atividade do processo.
	 * 
	 * <pre>
	 * Caso seja necessário, o sistema assumirá a primeira atividade em nome do usuário e avançará.
	 * </pre>
	 * 
	 * @param usuario
	 *            {@link NeoUser}
	 */
	public void avancaPrimeiraAtividade(NeoUser usuario, boolean isAvanca)
	{
		List<Activity> atividadesAbertas = processo.getOpenActivities();

		if (atividadesAbertas != null && !atividadesAbertas.isEmpty())
		{
			Activity primeiraAtividade = atividadesAbertas.get(0);

			if (primeiraAtividade instanceof UserActivity && primeiraAtividade.getFinishDate() == null)
			{
				UserActivity atividadeUsuario = (UserActivity) primeiraAtividade;

				TaskInstance taskInstance = atividadeUsuario.getInstance();

				//para assumir a atividade em pool
				primeiraAtividade.getActivity().getTaskAssigner()
						.assign((UserActivity) atividadeUsuario, (NeoUser) usuario, true);

				if (isAvanca)
				{
					//para avançar a primeira atividade
					RuntimeEngine.getTaskService().complete(taskInstance, usuario);
				}

			}
		}
	}

	public void avancaPrimeiraAtividade(NeoUser usuario)
	{
		this.avancaPrimeiraAtividade(usuario, true);
	}

	/**
	 * @author orsegups lucas.avila - Novo método.
	 * @date 08/07/2015
	 */

	/**
	 * Método que avança a primeira tarefa do processo, quando assign = true
	 * ou seja, a primeira atividade é de usuário não POOL
	 * 
	 * @param usuario
	 *            {@link NeoUser}
	 */
	public void avancaPrimeiraTarefa(NeoUser usuario)
	{
		Set<Activity> acts = processo.getTaskSet();
		List<Task> openTasks = new ArrayList<Task>();
		for (Activity a : acts)
		{
			if (a instanceof UserActivity && a.getFinishDate() == null)
			{
				openTasks.addAll(((UserActivity) a).getTaskList());
			}
		}

		for (Task t : openTasks)
		{
			TaskInstanceHelper.assign(t);
			PersistEngine.getEntityManager().flush();
			TaskInstance taskInstance = TaskInstanceHelper.findTaskInstance(t);
			RuntimeEngine.getTaskService().complete(taskInstance, usuario);
		}
	}

	public WFProcess iniciaProcesso(ProcessModel pm, NeoObject entity, boolean assign, NeoUser user)
	{
		WFProcess processo = WorkflowService.startProcess(pm, entity, false, null);
		processo.setRequester(user);
		return processo;
	}

	public WFProcess iniciaProcesso(String processName, boolean assign, NeoUser user,
			boolean avancarPrimeiraTarefa)
	{
		return iniciaProcesso(getCurrentModelByProcessName(processName), null, assign, user,
				avancarPrimeiraTarefa);
	}

	public WFProcess iniciaProcesso(String processName, NeoObject entity, boolean assign, NeoUser user,
			boolean avancarPrimeiraTarefa)
	{
		return iniciaProcesso(getCurrentModelByProcessName(processName), entity, assign, user,
				avancarPrimeiraTarefa);
	}

	public ProcessModel getCurrentModelByProcessName(String processName)
	{
		ProcessModel modelo = null;

		ProcessModelProxy proxy = PersistEngine.getNeoObject(ProcessModelProxy.class,
				new QLEqualsFilter("name", processName));
		if (proxy != null)
		{
			WorkFlowVersionControl versionControl = proxy.getVersionControl();

			if (versionControl != null)
			{
				modelo = versionControl.getCurrent();
			}
		}
		return modelo;
	}

	public WFProcess iniciaProcesso(ProcessModel pm, NeoObject entity, boolean assign, NeoUser user,
			boolean avancarPrimeiraTarefa)
	{
		WFProcess processo = null;
		if (entity != null)
		{
			processo = WorkflowService.startProcess(pm, entity, false, null);
		}
		else
		{
			processo = WorkflowService.startProcess(pm, assign, user);
		}
		processo = PersistEngine.reload(processo);
		processo.setSaved(true);
		processo.setRequester(user);
		if (processo != null)
		{
			//alteração - INICIO
			TaskInstance instancia = null;
			Activity atividade = null;
			if (getAllOpenActivity(processo).size() > 0)
			{
				atividade = getAllOpenActivity(processo).get(0).getActivity();
				instancia = TaskInstanceHelper.findTaskInstance(atividade);
			}
			else
			{
				List<TaskInstance> list = PersistEngine.getObjects(TaskInstance.class,
						new QLEqualsFilter("activity.process", processo));
				instancia = list.get(0);
			}
			//alteração - FIM

			//avancarPrimeiraTarefa(user);
			//TaskInstance instancia = TaskInstanceHelper.findTaskInstance(atividade);

			if (instancia == null && atividade != null && atividade instanceof UserActivity)
			{
				UserActivity atividadeUsuario = (UserActivity) atividade;
				TaskInstance instance = TaskInstanceHelper.create(atividade,
						PeopleAssignmentsService.getAssignments(atividadeUsuario));

				instancia = instance;

				if (avancarPrimeiraTarefa)
					TaskInstanceHelper.complete(instance.getTask());
			}
			else
			{

				if (avancarPrimeiraTarefa)
				{
					new WorkflowHelper(processo).avancaPrimeiraAtividade(user, true);
				}
			}

			if (assign && instancia.getTask() == null && atividade instanceof UserActivity){
				PersistEngine.getEntityManager().flush();
				RuntimeEngine.getTaskService().claim(instancia, PortalUtil.getCurrentUser());
			}
		}

		return processo;
	}

	public String iniciaProcessoNeoIdCode(ProcessModel pm, NeoObject entity, boolean assign,
			NeoUser user, boolean avancarPrimeiraTarefa)
	{
		WFProcess processo = pm.startProcess(entity, false, null);
		processo.setSaved(true);
		processo.setRequester(user);
		if (processo != null)
		{
			//alteração - INICIO
			TaskInstance instancia = null;
			Activity atividade = null;
			if (getAllOpenActivity(processo).size() > 0)
			{
				atividade = getAllOpenActivity(processo).get(0).getActivity();
				instancia = TaskInstanceHelper.findTaskInstance(atividade);
			}
			else
			{
				List<TaskInstance> list = PersistEngine.getObjects(TaskInstance.class,
						new QLEqualsFilter("activity.process", processo));
				instancia = list.get(0);
			}
			//alteraÃ§Ã£o - FIM

			//avancarPrimeiraTarefa(user);
			//TaskInstance instancia = TaskInstanceHelper.findTaskInstance(atividade);

			if (instancia == null && atividade != null && atividade instanceof UserActivity)
			{
				UserActivity atividadeUsuario = (UserActivity) atividade;
				TaskInstance instance = TaskInstanceHelper.create(atividade,
						PeopleAssignmentsService.getAssignments(atividadeUsuario));

				if (avancarPrimeiraTarefa)
					TaskInstanceHelper.complete(instance.getTask());
			}
			else
			{
				UserActivity atividadeUsuario = (UserActivity) instancia.getActivity();
				Task tarefaCriada = atividadeUsuario.getTaskAssigner().assign(atividadeUsuario, user);

				if (avancarPrimeiraTarefa)
				{
					TaskInstance instanciaTarefa = TaskInstanceHelper.assign(tarefaCriada);
					RuntimeEngine.getTaskService().complete(instanciaTarefa, user);
					//TaskInstanceHelper.complete(tarefaCriada);
				}
			}
		}

		return processo.getNeoId() + ";" + processo.getCode();
	}

	public List<Activity> getAllOpenActivity()
	{
		return getAllOpenActivity(this.processo);
	}

	public List<Activity> getAllOpenActivity(WFProcess process)
	{
		Set<Activity> acts = process.getTaskSet();

		List<Activity> openActs = new ArrayList<Activity>();
		for (Activity a : acts)
		{
			if (a.getFinishDate() == null)
			{
				openActs.add(a);
			}
		}

		return openActs;
	}

	//Finaliza tarefa a partir de uma activity
	public void finishTaskByActivity(Activity act)
	{
		TaskInstance instance = act.getInstance();

		//Caso nao tenha taskinstance trata-se de inconsistencia
		if (instance != null && instance.getOwner() != null)
		{
			RuntimeEngine.getTaskService().complete(instance, instance.getOwner());
		}
		else
		{
			//Trata-se de activity
			if (instance != null)
			{
				//Finaliza Tarefas que estao inconsistentes
				List<Task> tasks = PersistEngine.getObjects(Task.class, new QLEqualsFilter(
						"activity.neoId", act.getNeoId()));
				for (Task t : tasks)
				{
					//Caso tarefa esteja aberta
					if (t.getFinishDate() == null)
					{
						HandlerFactory.TASK.get(t).finish();
					}
				}
			}
		}
	}

	public List<Task> getTaskOrigins(Activity activity)
	{
		if (activity.getIncoming() != null)
		{
			for (Activity a : activity.getIncoming())
			{
				if (a instanceof UserActivity)
				{
					QLGroupFilter groupFilter = new QLGroupFilter("AND");
					groupFilter.addFilter(new QLEqualsFilter("activity.neoId", a.getNeoId()));
					groupFilter.addFilter(new QLFilterIsNotNull("finishDate"));

					List<Task> taskList = PersistEngine.getObjects(Task.class, groupFilter, -1, -1,
							" finishDate asc ");

					if (taskList != null && !taskList.isEmpty())
						return taskList;
					else
						return null;
				}
			}

			for (Activity a : activity.getIncoming())
			{
				return getTaskOrigins(a);
			}
		}

		return null;
	}

	public Task getTaskOrigin(Activity activity)
	{
		List<Task> taskList = getTaskOrigins(activity);
		if (taskList != null && !taskList.isEmpty())
			return taskList.get(0);
		else
			return null;
	}

	public void avancaPrimeiraAtividadePool(NeoUser usuario, boolean isAvanca)
	{
		List<Activity> atividadesAbertas = processo.getOpenActivities();

		if (atividadesAbertas != null && !atividadesAbertas.isEmpty())
		{
			Activity primeiraAtividade = atividadesAbertas.get(0);

			if (primeiraAtividade instanceof UserActivity && primeiraAtividade.getFinishDate() == null)
			{
				UserActivity atividadeUsuario = (UserActivity) primeiraAtividade;

				TaskInstance taskInstance = atividadeUsuario.getInstance();

				if (isAvanca)
				{
					//para avançar a primeira atividade
					RuntimeEngine.getTaskService().complete(taskInstance, usuario);
				}

			}
		}
	}

	public void iniciaAtividade(Activity startActivity, Activity fromActivity)
	{
		Set<Activity> fromActivitySet = new HashSet<Activity>();
		fromActivitySet.add(fromActivity);
		iniciaAtividade(startActivity, fromActivitySet);
	}

	public void iniciaAtividade(Activity startActivity, Set<Activity> fromActivity)
	{
		((ActivityHandler) HandlerFactory.ACTIVITY.get(startActivity)).start(fromActivity);
	}

	public void finalizaAtividade(Activity activity)
	{
		TaskInstance instance = TaskInstanceHelper.findTaskInstance(activity);
		Exception exception = null;
		if (instance != null)
		{
			try
			{
				if (instance.getStatus() == TaskStatus.READY)
					RuntimeEngine.getTaskService().claim(instance, PortalUtil.getCurrentUser());
				
				RuntimeEngine.getTaskService().complete(instance, PortalUtil.getCurrentUser());
				PersistEngine.getEntityManager().flush();
			}
			catch (Exception ex)
			{
				String message = ex.getMessage() != null ? ex.getMessage() : "";
				exception = new WorkflowException(message);
			}
		}

		if (exception != null)
		{
			try
			{
				HandlerFactory.ACTIVITY.get(activity).finish();
			}
			catch (Exception ex)
			{
				String message = ex.getMessage() != null ? ex.getMessage() : "";
				throw new WorkflowException(message);
			}
		}
	}
	
	public void avancaAtividade(Activity activity)
	{
		TaskInstance instance = TaskInstanceHelper.findTaskInstance(activity);
		 
		Exception exception = null;
		if (instance != null)
		{
			try
			{
				if (instance.getStatus() == TaskStatus.READY){
					
					RuntimeEngine.getTaskService().claim(instance, PortalUtil.getCurrentUser());
					
				}
					
				
				RuntimeEngine.getTaskService().complete(instance, PortalUtil.getCurrentUser());
				PersistEngine.getEntityManager().flush();
			}
			catch (Exception ex)
			{
				String message = ex.getMessage() != null ? ex.getMessage() : "";
				exception = new WorkflowException(message);
			}
		}

		if (exception != null)
		{
			try
			{
				HandlerFactory.ACTIVITY.get(activity).finish();
			}
			catch (Exception ex)
			{
				String message = ex.getMessage() != null ? ex.getMessage() : "";
				throw new WorkflowException(message);
			}
		}
	}
	
	public void fechaAtividade(Activity activity)
	{
		TaskInstance instance = TaskInstanceHelper.findTaskInstance(activity);
		Exception exception = null;
		if (instance != null)
		{
			try
			{
				GregorianCalendar finishDate = new GregorianCalendar();
				
				activity.setFinishDate(finishDate);
				
				if (instance.getTask() != null)
				{
					instance.getTask().setFinishByUser(PortalUtil.getCurrentUser());
					instance.getTask().setFinishDate(finishDate);
					instance.getTask().setSituation(Situation.DONE);
				}
				else
				{
					TaskInstanceHelper.remove(activity);
				}
			}
			catch (Exception ex)
			{
				String message = ex.getMessage() != null ? ex.getMessage() : "";
				exception = new WorkflowException(message);
			}
		}
	}

	public void finalizaTarefa(Task task)
	{
		TaskInstance instance = task.getInstance();
		RuntimeEngine.getTaskService().complete(instance, PortalUtil.getCurrentUser());
		PersistEngine.getEntityManager().flush();
	}

	public void backTask(Task task, String reason)
	{
		TaskService taskService = new TaskServiceImpl();
		taskService.refuse(task.getInstance(), reason);
	}

	public Activity createNextActivity(String activityName, WFProcess wfprocess)
	{
		QLGroupFilter qlf = new QLGroupFilter("AND");
		qlf.addFilter(new QLEqualsFilter("name", activityName));
		qlf.addFilter(new QLEqualsFilter("processModel.neoId", wfprocess.getModel().getNeoId()));

		ActivityModel model = PersistEngine.getObject(ActivityModel.class, qlf);

		return model.buildActivity(wfprocess);
	}

	public Task getFirstTask()
	{

		return getFirstTask(this.processo);
	}

	public Task getFirstTask(WFProcess processo)
	{
		Task firstTask = null;

		List<Task> taskList = processo.getAllPendingTaskList();
		if (taskList.size() == 0)
		{
			List<Activity> activityList = processo.getOpenActivities();
			for (Activity activity : activityList)
			{
				if (activity instanceof UserActivity)
				{
					taskList = ((UserActivity) activity).getTaskList();
					if (taskList.size() > 0)
					{
						break;
					}
				}
			}
		}

		if (taskList.size() > 0)
		{
			firstTask = taskList.get(0);
		}

		return firstTask;
	}

	public List<Task> getAllPendingTaskList()
	{
		List<Task> pendingTasks = new ArrayList<Task>();
		List<Activity> openActivities = this.getAllOpenActivity(this.processo);

		for (Activity activity : openActivities)
		{
			TaskInstance instance = TaskInstanceHelper.findTaskInstance(activity);
			if (instance.getTask() != null)
				pendingTasks.add(instance.getTask());
		}
		return pendingTasks;
	}
}
