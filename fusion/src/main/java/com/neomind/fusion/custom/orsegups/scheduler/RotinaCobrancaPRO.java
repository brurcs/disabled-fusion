package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.common.base.Joiner;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class RotinaCobrancaPRO implements CustomJobAdapter {

    final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.RotinaCobrancaPRO");

    public void execute(CustomJobContext ctx) {
	
	Long key = GregorianCalendar.getInstance().getTimeInMillis();
	
	log.warn("INICIAR AGENDADOR DE TAREFA: RotinaCobrancaPRO -- Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	
	StringBuffer  varname1 = new StringBuffer();
	
	try {

	    conn = PersistEngine.getConnection("SAPIENS");
	    	    
	    varname1.append("SELECT ");
	    varname1.append("	           cvs.usu_codemp, ");
	    varname1.append("	           cvs.usu_codfil, ");
	    varname1.append("	           cvs.usu_numctr, ");
	    varname1.append("	           cvs.usu_numpos, ");
	    varname1.append("	           cli.codcli, ");
	    varname1.append("	           cvs.usu_sitcvs, ");
	    varname1.append("	           cvs.usu_codser, ");
	    varname1.append("	           cvs.usu_periss, ");
	    varname1.append("	           cvs.usu_perirf, ");
	    varname1.append("	           cvs.usu_perins, ");
	    varname1.append("	           cvs.usu_perpit, ");
	    varname1.append("	           cvs.usu_percsl, ");
	    varname1.append("	    	    cvs.usu_percrt, ");
	    varname1.append("	    	    cvs.usu_perour, ");
	    varname1.append("	    	    cvs.usu_ctafin, ");
	    varname1.append("	    	    cvs.usu_ctared, ");
	    varname1.append("	    	    cvs.usu_codccu, ");
	    varname1.append("	    	    cvs.usu_datini, ");
	    varname1.append("	    	    cvs.usu_tnsser, ");
	    varname1.append("	    	    sig.usu_codcli, ");
	    varname1.append("	    	    EVENTO.CD_HISTORICO, ");
	    varname1.append("	    	    EVENTO.CD_EVENTO, ");
	    varname1.append("	    	    EVENTO.DT_FECHAMENTO ");
	    varname1.append("	      FROM [FSOODB04\\SQL02].Sapiens.dbo.usu_t160sig sig WITH (nolock) ");
	    varname1.append("	     INNER JOIN [FSOODB04\\SQL02].Sapiens.dbo.usu_t160cvs cvs WITH (nolock) ");
	    varname1.append("	             ON cvs.usu_codemp = sig.usu_codemp ");
	    varname1.append("	            AND cvs.usu_codfil = sig.usu_codfil ");
	    varname1.append("	            AND cvs.usu_numctr = sig.usu_numctr ");
	    varname1.append("	            AND cvs.usu_numpos = sig.usu_numpos ");
	    varname1.append("	     INNER JOIN [FSOODB04\\SQL02].Sapiens.dbo.usu_t160ctr CTR WITH(nolock) ");
	    varname1.append("	             ON cvs.usu_codemp = ctr.usu_codemp ");
	    varname1.append("	            AND cvs.usu_codfil = ctr.usu_codfil ");
	    varname1.append("	            AND cvs.usu_numctr = ctr.usu_numctr ");
	    varname1.append("	     INNER JOIN [FSOODB04\\SQL02].Sapiens.dbo.e085cli cli WITH(nolock) ");
	    varname1.append("	             ON CTR.usu_codcli = cli.codcli ");
	    varname1.append("	     INNER JOIN [FSOODB04\\SQL02].Sapiens.dbo.e080ser ser WITH(nolock) ");
	    varname1.append("	             ON cvs.usu_codemp = ser.codemp ");
	    varname1.append("	            AND ser.codser = cvs.usu_codser ");
	    varname1.append("	     INNER JOIN ");
	    varname1.append("( ");
	    varname1.append("	SELECT HE.CD_HISTORICO, ");
	    varname1.append("	       HE.CD_EVENTO, ");
	    varname1.append("	       HE.DT_FECHAMENTO, ");
	    varname1.append("	       HE.CD_CLIENTE ");
	    varname1.append("      FROM [FSOODB03\\SQL01].SIGMA90.dbo.dbCENTRAL CEN WITH(nolock) ");
	    varname1.append("INNER JOIN [FSOODB03\\SQL01].SIGMA90.dbo.view_historico HE WITH(nolock) ON CEN.cd_cliente = HE.cd_cliente ");
	    varname1.append("       AND HE.TX_OBSERVACAO_FECHAMENTO LIKE '%#COBRAR%' ");
	    varname1.append("       AND HE.DT_FECHAMENTO > DATEADD(DAY,-1,GETDATE()) ");
	    varname1.append(") as EVENTO on sig.usu_codcli = EVENTO.CD_CLIENTE ");
	    varname1.append("	      WHERE ((cvs.usu_sitcvs = 'A' ) OR (cvs.usu_sitcvs = 'I' AND cvs.usu_datfim >= ? )) ");
	    varname1.append("	        AND cli.tipemc = 1 ");
	    varname1.append("	        AND sig.usu_codemp in (15,17,18,19,21) ");
	    varname1.append("	        AND ser.codfam = 'SER102'");
	  
	    pstm = conn.prepareStatement(varname1.toString());
	
	    GregorianCalendar dataInicio = new GregorianCalendar();
	    dataInicio.add(Calendar.DAY_OF_MONTH, -1); //-1
	    dataInicio.set(Calendar.HOUR_OF_DAY, 0);
	    dataInicio.set(Calendar.MINUTE, 0);
	    dataInicio.set(Calendar.SECOND, 0);
	    dataInicio.set(Calendar.MILLISECOND, 0);
	    
	    GregorianCalendar dataFim = new GregorianCalendar();
	    dataFim.set(Calendar.HOUR_OF_DAY, 0);
	    dataFim.set(Calendar.MINUTE, 0);
	    dataFim.set(Calendar.SECOND, 0);
	    dataFim.set(Calendar.MILLISECOND, 0);
	    
	    pstm.setTimestamp(1, new Timestamp(dataFim.getTimeInMillis()));
//	    pstm.setTimestamp(2, new Timestamp(dataInicio.getTimeInMillis()));
//	    pstm.setTimestamp(3, new Timestamp(dataFim.getTimeInMillis()));
//	    pstm.setTimestamp(4, new Timestamp(dataFim.getTimeInMillis()));

	    rs = pstm.executeQuery();
	    
	   /** Datas usadas a abertura de tarefas e apontamentos */

	    GregorianCalendar datSis = new GregorianCalendar();
	    GregorianCalendar dat1900 = new GregorianCalendar(1900, 11, 31);
	    
	    GregorianCalendar datSisHorarioZerado = new GregorianCalendar();
	    datSisHorarioZerado.set(Calendar.HOUR_OF_DAY, 0);
	    datSisHorarioZerado.set(Calendar.MINUTE, 0);
	    datSisHorarioZerado.set(Calendar.SECOND, 0);
	    datSisHorarioZerado.set(Calendar.MILLISECOND, 0);
	    
	    GregorianCalendar dataCompetencia = new GregorianCalendar();
	    dataCompetencia.add(Calendar.DAY_OF_MONTH, -1);
	    dataCompetencia.add(Calendar.MONTH, +1);
	    dataCompetencia.set(Calendar.DAY_OF_MONTH, 1);
	    dataCompetencia.set(Calendar.HOUR_OF_DAY, 0);
	    dataCompetencia.set(Calendar.MINUTE, 0);
	    dataCompetencia.set(Calendar.SECOND, 0);
	    dataCompetencia.set(Calendar.MILLISECOND, 0);

	    
	    /** Lista que guardará os dados para o envio de e-mail ao fim da rotina */
	    
	    StringBuilder dadosApontamentoEmail = new StringBuilder();
	    StringBuilder dadosTarefaEmail = new StringBuilder();
	    
	    /** Mandinga para colorir as linhas da tabela, visto que clientes de e-mails Microsoft processam pouco CSS */
	    int contApontamentos = 0;
	    int contTarefas = 0;
	    
	    while (rs.next()) {
		
		Long codCliente  = rs.getLong("codcli");	
		
		Boolean cobrar = true;
		
		/** VERIFICA CADASTRO DE EXCEÇÃO DE CLIENTE **/
		
		List<NeoObject> listaExcecoes = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CadastroExcecoesCobrancaDeslocamentoPRO"),new QLEqualsFilter("clienteSapiens.codcli", codCliente));

		if (listaExcecoes != null && !listaExcecoes.isEmpty()) {		    
		  
		    for (NeoObject objExcecao : listaExcecoes) {
			
			cobrar = false;
		    }
		}
		
		/**GERAR APONTAMENTO OU TAREFA*/
		
		Long codEmp = rs.getLong("usu_codemp");
		Long codFil = rs.getLong("usu_codfil");
		Long numCtr = rs.getLong("usu_numctr");
		Long numPos = rs.getLong("usu_numpos");
		String codSerPosto = rs.getString("usu_codser");
		Long cdHistorico = rs.getLong("CD_HISTORICO");
		Long cdEvento = rs.getLong("CD_EVENTO");
		String dataFechamento = rs.getString("dataAtend");
		
		if (cobrar) {
		    
		    String codSer = "9002010";
			String cplCvs = "Deslocamento PRO (Empresa: " + codEmp + ")";
			Float perIss = rs.getFloat("usu_periss");
			Float perIrf = rs.getFloat("usu_perirf");
			Float perIns = rs.getFloat("usu_perins");
			Float perPit = rs.getFloat("usu_perpit");
			Float perCsl = rs.getFloat("usu_percsl");
			Float perCrt = rs.getFloat("usu_percrt");
			Float perOur = rs.getFloat("usu_perour");
			Long ctaFin = rs.getLong("usu_ctafin");
			Long ctaRed = rs.getLong("usu_ctared");
			String codCcu = rs.getString("usu_codccu");
			Date datInn = rs.getDate("usu_datini");
			String tnsSer = rs.getString("usu_tnsser");
			String obsCms = "Cobrança de Deslocamento PRO (Empresa: " + codEmp + ", CD Historico: " + cdHistorico + ", CD Evento: "+ cdEvento +", Data Fechamento: "+ dataFechamento +")";
			Float preUni;

			preUni = 50f;			

			Long seqMov = this.getSeqMov();

			while (seqMov.equals(0L)) {
			    seqMov=this.getSeqMov();
			}

			this.gravarApontamento(codEmp, codFil, numCtr, numPos, seqMov, datSisHorarioZerado, codSer, cplCvs, preUni, perIss, perIrf, perIns, perPit, perCsl, perCrt, perOur, ctaFin, ctaRed, codCcu, datInn,
				dat1900, tnsSer, datSis, dataCompetencia, obsCms);

			String observacao = "Inserido via Fusion: Gerado cobrança(apontamento) para deslocamento PRO. Empresa " + codEmp + " Filial " + codFil + " Contrato " + numCtr
				+ " Posto " + numPos;
//			this.gravarHistorico(codCliente, codEmp, null, null, observacao);
			
			contApontamentos ++;
			int resto = (contApontamentos) % 2;
			if (resto == 0){
			    dadosApontamentoEmail.append("<tr style=\"background-color: #d6e9f9\" ><td>"+codEmp+"</td><td>"+codFil+"</td><td>"+numCtr+"</td><td>"+numPos+"</td></tr>");
			}else{
			    dadosApontamentoEmail.append("<tr><td>"+codEmp+"</td><td>"+codFil+"</td><td>"+numCtr+"</td><td>"+numPos+"</td></tr>");
			}
		} else { 
		    String tarefa = this.abrirTarefa(codEmp, codFil, numCtr, numPos);
		    String observacao = "Inserido via Fusion: Gerado tarefa para deslocamento PRO. Empresa " + codEmp + " Filial " + codFil + " Contrato " + numCtr 
			    + " Posto " + numPos;
//		    this.gravarHistorico(codCliente, codEmp, tarefa, cobrar, observacao);
		    contTarefas ++;
		    
		    int resto = (contTarefas) % 2;
		    if (resto == 0) {
			dadosTarefaEmail.append("<tr style=\"background-color: #d6e9f9\" ><td>" + codEmp + "</td><td>" + codFil + "</td><td>" + numCtr + "</td><td>" + numPos
				+ "</td></tr>");
		    } else {
			dadosTarefaEmail.append("<tr><td>" + codEmp + "</td><td>" + codFil + "</td><td>" + numCtr + "</td><td>" + numPos + "</td></tr>");
		    }
		}
	    }
	    
	    
	    /** Envio de e-mail */
	    	    
	    StringBuilder corpoEmail = new StringBuilder();
	 
	    corpoEmail.append("<!DOCTYPE html>");
	    corpoEmail.append("<html>");
	    corpoEmail.append("<head>");
	    corpoEmail.append("<meta charset=\"UTF-8\">");
	    corpoEmail.append("<title>Relatório de cobrança de Deslocamento (PRO)</title>");
	    corpoEmail.append("<style>");
	    corpoEmail.append("h2{");
	    corpoEmail.append("font-weight:bold; color:#303090; letter-spacing:1pt; word-spacing:2pt; font-size:20px; text-align:center; font-family:Verdana; line-height:1;");
	    corpoEmail.append("}");
	    corpoEmail.append(".table-p{");
	    corpoEmail.append("font-family: Verdana; font-weight:normal; font-size:12px; text-align:justify;");
	    corpoEmail.append(" }");
	    corpoEmail.append(".table-info{");
	    corpoEmail.append("table-layout: fixed;");
	    corpoEmail.append("text-align:left;");
	    corpoEmail.append("font-family: Verdana;");
	    corpoEmail.append("}");
	    corpoEmail.append(".table-info td{");
	    corpoEmail.append("overflow-x: hidden;");
	    corpoEmail.append(" }");
	    corpoEmail.append(".table-info thead{");
	    corpoEmail.append("background-color:#303090;");
	    corpoEmail.append("color: white;");
	    corpoEmail.append("font-weight:bold;");
	    corpoEmail.append("	}");
	    corpoEmail.append("</style>");
	    corpoEmail.append("</head>");
	    corpoEmail.append("<body>");
	    corpoEmail.append("<div>");
	    corpoEmail.append("<table width=\"600\" align=\"center\">");
	    corpoEmail.append("<tr><td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/header.jpg\" width=\"600\" height=\"145\"></td></tr>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("<td><h2>Relatório de Cobrança de Deslocamento PRO</h2></td>");
	    corpoEmail.append("<table width=\"600\" align=\"center\" class=\"table-p\"> ");
	    corpoEmail.append("<tr><td>O(s) Deslocamento(s) abaixo(s) citado(s) gerou(ram) apontamento(s):</td></tr>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("<table width=\"600\" align=\"center\" class=\"table-info\"> ");
	    corpoEmail.append("<thead>");
	    corpoEmail.append("<tr>");
	    corpoEmail.append("<td>Empresa</td>");
	    corpoEmail.append("<td>Filial</td>");
	    corpoEmail.append("<td>Contrato</td>");
	    corpoEmail.append("<td>Posto</td>");
	    corpoEmail.append("</tr>");
	    corpoEmail.append("</thead>");
	    corpoEmail.append("<tbody>");
	    corpoEmail.append("<!-- Recebe dados do método -->");
	    corpoEmail.append(dadosApontamentoEmail);
	    corpoEmail.append("<br>");
	    corpoEmail.append("</tbody>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("<table width=\"600\" align=\"center\" class=\"table-p\"> ");
	    corpoEmail.append("<tr><td>O(s) Deslocamento(s) abaixo gerou(ram) tarefa(s), devido a não possuir(em) posto ativo para cobrança ou cliente ter exceção de cobrança, conforme listagem:</td></tr>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("<table width=\"600\" align=\"center\" class=\"table-info\">");
	    corpoEmail.append("<thead>");
	    corpoEmail.append("<tr>");
	    corpoEmail.append("<td>Empresa</td>");
	    corpoEmail.append("<td>Filial</td>");
	    corpoEmail.append("<td>Contrato</td>");
	    corpoEmail.append("<td>Posto</td>");
	    corpoEmail.append("</tr>");
	    corpoEmail.append("</thead>");
	    corpoEmail.append("<tbody>");
	    corpoEmail.append("<!-- Recebe dados do método -->");
	    corpoEmail.append(dadosTarefaEmail);
	    corpoEmail.append("</tbody>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("<br>");
	    corpoEmail.append("<!-- Rodapé do e-mail -->");
	    corpoEmail.append("<div class=\"rodape\">");
	    corpoEmail.append("<table width=\"600\" align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
	    corpoEmail.append("<tbody>");
	    corpoEmail.append("<tr>");
	    corpoEmail.append("<td width=\"360\" valign=\"bottom\"><img src=\"https://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/footer-1.jpg\"></td>");
	    corpoEmail.append("<td><a href=\"http://www.orsegups.com.br\" target=\"_blank\"><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/footer-2.jpg\" width=\"260\" height=\"120\" alt=\"\"/></a></td>");
	    corpoEmail.append("</tr>");
	    corpoEmail.append("</tbody>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("</div>");
	    corpoEmail.append("</div>");
	    corpoEmail.append("</body>");
	    corpoEmail.append("</html>");
	    
	    String[] addTo = new String[] { "caroline.wrunski@orsegups.com.br" };
	    String[] comCopia = new String[] { "carlos.silva@orsegups.com.br" };	    
	    String from = "fusion@orsegups.com.br";
	    String subject ="Relatório de Cobrança de deslocamento (PRO)";
	    String html = corpoEmail.toString();
	    
	    OrsegupsUtils.sendEmail2Orsegups(addTo, from, subject, html, comCopia);
	    
	    
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("ERRO AGENDADOR DE TAREFA: RotinaCobrancaDeslocamentoPRO - ERRO AO EXECUTAR ROTINA -- Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

    }
  
    /**
     * Metodo de abertura de tarefas
     * 
     * @param codEmp
     * @param codFil
     * @param numCtr
     * @param numPos
     * @param numeroOS
     * @return numero da Tarefa
     */
    private String abrirTarefa(Long codEmp, Long codFil, Long numCtr, Long numPos) throws Exception{
	String tarefa = "";

	String solicitante = "fernanda.maciel";
	
	String executor = OrsegupsUtils.getUserNeoPaper("ResponsavelCobrancaPRO");

	GregorianCalendar prazo = new GregorianCalendar();
	prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
	prazo.set(Calendar.HOUR_OF_DAY, 23);
	prazo.set(Calendar.MINUTE, 59);
	prazo.set(Calendar.SECOND, 59);

	String titulo = "Apontamento/Cobrança referente a Deslocamento PRO: Empresa(" + codEmp +")";

	String descricao = "Contrato: " + numCtr + " Empresa: " + codEmp + " Filial: " + codFil + " Posto: " + numPos + " ,necessita de apontamento/cobrança referente a Deslocamento. Empresa(" + codEmp +")";

	IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();

	tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "hadouken", prazo);

	return tarefa;
    }
    
    /**
     * Retorna o número o maior número sequencial de movimento
     * @return long seqMov
     */
    private Long getSeqMov(){
	Long seqMov = 0L;
	
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	
	
	try {
	    conn = PersistEngine.getConnection("SAPIENS");
	    
	    StringBuilder sqlSeqMov = new StringBuilder();
	    sqlSeqMov.append("SELECT MAX(ISNULL(USU_SeqMov,0))+1 AS seqMov FROM USU_T160CMS");

	    pstm = conn.prepareStatement(sqlSeqMov.toString());

	    rs = pstm.executeQuery();

	    if (rs.next()) {
		seqMov = rs.getLong("seqMov");
		return seqMov;
	    }

	}catch (SQLException e){
	    e.printStackTrace();
	}finally{
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
	
	return seqMov;
    }
    
    
    /**
     * Gravar apontamento para cobrança da OS
     * @param codEmp
     * @param codFil
     * @param numCtr
     * @param numPos
     * @param seqMov
     * @param datSisHorarioZerado
     * @param codSer
     * @param cplCvs
     * @param preUni
     * @param perIss
     * @param perIrf
     * @param perIns
     * @param perPit
     * @param perCsl
     * @param perCrt
     * @param perOur
     * @param ctaFin
     * @param ctaRed
     * @param codCcu
     * @param datInn
     * @param dat1900
     * @param tnsSer
     * @param datSis
     * @param dataCompetencia
     * @param obsCms
     */
    private void gravarApontamento(Long codEmp, Long codFil, Long numCtr,Long numPos, Long seqMov, GregorianCalendar datSisHorarioZerado, String codSer, String cplCvs, Float preUni, Float perIss, 
	    Float perIrf, Float perIns, Float perPit, Float perCsl, Float perCrt, Float perOur, Long ctaFin, Long ctaRed, String codCcu, Date datInn, GregorianCalendar dat1900,
	    String tnsSer, GregorianCalendar datSis, GregorianCalendar dataCompetencia, String obsCms) throws Exception{
	
	Connection conn = null;
	PreparedStatement pstm = null;
	
	try {

	    conn = PersistEngine.getConnection("SAPIENS");
	    StringBuffer sqlApontamento = new StringBuffer();

//	    sqlApontamento.append(" INSERT INTO USU_T160CMS VALUES (?,?,?,?,?,?,?,?,'UN',0,1,?,?,?,?,?,?,?,?,?,?,?,?,?,?,100,?,?,0,'',0,?,'+',?,30,'N','S','M',0,?,'N') ");
	    sqlApontamento.append(" INSERT INTO [FSOODB04\\SQL02].Sapiens.dbo.USU_T160CMS (");
	    sqlApontamento.append(" USU_CodEmp, "); // 1
	    sqlApontamento.append(" USU_CodFil, "); // 2
	    sqlApontamento.append(" USU_NumCtr, "); // 3
	    sqlApontamento.append(" USU_NumPos, "); // 4
	    sqlApontamento.append(" USU_SeqMov, "); // 5
	    sqlApontamento.append(" USU_DatMov, "); // 6
	    sqlApontamento.append(" USU_CodSer, "); // 7
	    sqlApontamento.append(" USU_CplCvs, "); // 8
	    sqlApontamento.append(" USU_UniMed, "); // 9
	    sqlApontamento.append(" USU_QtdFun, "); // 10
	    sqlApontamento.append(" USU_QtdCvs, "); // 11
	    sqlApontamento.append(" USU_PreUni, "); // 12
	    sqlApontamento.append(" USU_PerIss, "); // 13
	    sqlApontamento.append(" USU_PerIrf, "); // 14
	    sqlApontamento.append(" USU_PerIns, "); // 15
	    sqlApontamento.append(" USU_PerPit, "); // 16
	    sqlApontamento.append(" USU_PerCsl, "); // 17
	    sqlApontamento.append(" USU_PerCrt, "); // 18
	    sqlApontamento.append(" USU_PerOur, "); // 19
	    sqlApontamento.append(" USU_CtaFin, "); // 20
	    sqlApontamento.append(" USU_CtaRed, "); // 21
	    sqlApontamento.append(" USU_CodCcu, "); // 22
	    sqlApontamento.append(" USU_Datini, "); // 23 
	    sqlApontamento.append(" USU_Datfim, "); // 24
	    sqlApontamento.append(" USU_TnsSer, "); // 25
	    sqlApontamento.append(" USU_UsuGer, "); // 26
	    sqlApontamento.append(" USU_DatGer, "); // 27
	    sqlApontamento.append(" USU_HorGer, "); // 28
	    sqlApontamento.append(" USU_FilNfv, "); // 29
	    sqlApontamento.append(" USU_CodSnf, "); // 30
	    sqlApontamento.append(" USU_NumNfv, "); // 31
	    sqlApontamento.append(" USU_DatCpt, "); // 32
	    sqlApontamento.append(" USU_AdcSub, "); // 33
	    sqlApontamento.append(" USU_ObsCms, "); // 34
	    sqlApontamento.append(" USU_DiaTra, "); // 35
	    sqlApontamento.append(" USU_PosBon, "); // 36
	    sqlApontamento.append(" USU_LibApo, "); // 37
	    sqlApontamento.append(" USU_TipApo, "); // 38
	    sqlApontamento.append(" USU_TarFus, "); // 39
	    sqlApontamento.append(" USU_CptCom, "); // 40
	    sqlApontamento.append(" USU_JusApo, "); // 41
	    sqlApontamento.append(" USU_CodLan  "); // 42
	    sqlApontamento.append(" ) VALUES ( ");
	    sqlApontamento.append("?,"); // 1
	    sqlApontamento.append("?,"); // 2
	    sqlApontamento.append("?,"); // 3
	    sqlApontamento.append("?,"); // 4
	    sqlApontamento.append("?,"); // 5
	    sqlApontamento.append("?,"); // 6
	    sqlApontamento.append("?,"); // 7
	    sqlApontamento.append("?,"); // 8
	    sqlApontamento.append("?,"); // 9
	    sqlApontamento.append("?,"); // 10
	    sqlApontamento.append("?,"); // 11
	    sqlApontamento.append("?,"); // 12
	    sqlApontamento.append("?,"); // 13
	    sqlApontamento.append("?,"); // 14
	    sqlApontamento.append("?,"); // 15
	    sqlApontamento.append("?,"); // 16
	    sqlApontamento.append("?,"); // 17
	    sqlApontamento.append("?,"); // 18
	    sqlApontamento.append("?,"); // 19
	    sqlApontamento.append("?,"); // 20
	    sqlApontamento.append("?,"); // 21
	    sqlApontamento.append("?,"); // 22
	    sqlApontamento.append("?,"); // 23
	    sqlApontamento.append("?,"); // 24
	    sqlApontamento.append("?,"); // 25
	    sqlApontamento.append("?,"); // 26
	    sqlApontamento.append("?,"); // 27
	    sqlApontamento.append("?,"); // 28
	    sqlApontamento.append("?,"); // 29
	    sqlApontamento.append("?,"); // 30
	    sqlApontamento.append("?,"); // 31
	    sqlApontamento.append("?,"); // 32
	    sqlApontamento.append("?,"); // 33
	    sqlApontamento.append("?,"); // 34
	    sqlApontamento.append("?,"); // 35
	    sqlApontamento.append("?,"); // 36
	    sqlApontamento.append("?,"); // 37
	    sqlApontamento.append("?,"); // 38
	    sqlApontamento.append("?,"); // 39
	    sqlApontamento.append("?,"); // 40
	    sqlApontamento.append("?,"); // 41
	    sqlApontamento.append("?) "); // 42

	    pstm = conn.prepareStatement(sqlApontamento.toString());

	    pstm.setLong(1, codEmp);
	    pstm.setLong(2, codFil);
	    pstm.setLong(3, numCtr);
	    pstm.setLong(4, numPos);
	    pstm.setLong(5, seqMov);
	    pstm.setTimestamp(6, new Timestamp(datSisHorarioZerado.getTimeInMillis()));
	    pstm.setString(7, codSer);
	    pstm.setString(8, cplCvs);	    
	    pstm.setString(9, "UN");
	    pstm.setInt(10, 0);
	    pstm.setInt(11, 1);	    
	    pstm.setFloat(12, preUni);
	    pstm.setFloat(13, perIss);
	    pstm.setFloat(14, perIrf);
	    pstm.setFloat(15, perIns);
	    pstm.setFloat(16, perPit);
	    pstm.setFloat(17, perCsl);
	    pstm.setFloat(18, perCrt);
	    pstm.setFloat(19, perOur);
	    pstm.setLong(20, ctaFin);
	    pstm.setLong(21, ctaRed);
	    pstm.setString(22, codCcu);
	    pstm.setTimestamp(23, new Timestamp(datInn.getTime()));
	    pstm.setTimestamp(24, new Timestamp(dat1900.getTimeInMillis()));
	    pstm.setString(25, tnsSer);	    
	    pstm.setInt(26, 100);  
	    pstm.setTimestamp(27, new Timestamp(datSisHorarioZerado.getTimeInMillis()));
	    pstm.setLong(28, datSis.get(Calendar.HOUR_OF_DAY) * 60 + datSis.get(Calendar.MINUTE));	    
	    pstm.setInt(29, 0);
	    pstm.setString(30, "\'\'");
	    pstm.setInt(31, 0);	    
	    pstm.setTimestamp(32, new Timestamp(dataCompetencia.getTimeInMillis()));	    
	    pstm.setString(33, "+");	    
	    pstm.setString(34, obsCms);	    
	    pstm.setInt(35, 30);
	    pstm.setString(36, "N");
	    pstm.setString(37, "S");
	    pstm.setString(38, "M");
	    pstm.setInt(39, 0);	
	    pstm.setTimestamp(40, new Timestamp(dat1900.getTimeInMillis()));	    
	    pstm.setString(41, "N");
	    pstm.setInt(42, 4);

	    pstm.execute();
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);

	}
    }
       
    
  
}
