package com.neomind.fusion.custom.orsegups.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import com.neomind.fusion.persist.PersistEngine;


/**
 * 
 * @author lucas.alison
 *
 */

public class QLBioPresencaUtils {
    public static GregorianCalendar getHorarioPontoColaboradorBioPresenca(Long matricula, Long empresa, Long id){
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	
	StringBuilder sql = new StringBuilder();
	GregorianCalendar retorno = null;
	try {
	    conn = PersistEngine.getConnection("TIDB");
	    
	    sql.append(" SELECT DATA, TIMEZONEDISPOSITIVO FROM BIO_PRESENCA_PONTOS ");
	    sql.append(" WHERE ID = ? ");
	    sql.append(" AND MATRICULA = ? ");
	    sql.append(" AND EMPRESA = ? ");
		
	    pstm = conn.prepareStatement(sql.toString());
	    
	    pstm.setLong(1, id);
	    pstm.setLong(2, matricula);
	    pstm.setLong(3, empresa);

	    rs = pstm.executeQuery();

	    while (rs.next()) {
		String timeZone = rs.getString("TIMEZONEDISPOSITIVO");
		if (timeZone != null){
//		    TimeZone tz2 = TimeZone.getTimeZone(timeZone.trim());
//		    TimeZone.setDefault(tz2);		    
//		    retorno = (GregorianCalendar) GregorianCalendar.getInstance(tz2);
//		    Timestamp data = rs.getTimestamp("DATA");

//		    retorno = new GregorianCalendar();
//		    retorno.setTimeZone(TimeZone.getTimeZone(timeZone));
//		    retorno.setTime(rs.getTimestamp("DATA"));
//		    
		    
		    TimeZone tzLocal = TimeZone.getTimeZone(timeZone.trim());
			
		    retorno = new GregorianCalendar();
		    retorno.setTimeZone(tzLocal);
		    retorno.setTime(rs.getTimestamp("DATA"));
		    
//		    retorno.setTime(data);
		   
		}else{
		    Timestamp data = rs.getTimestamp("DATA");
		    retorno = new GregorianCalendar();
		    retorno.setTime(data);
		}
		
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
	return retorno;
    }
    
    public static GregorianCalendar getHorarioPontoColaboradorBioPresencaTeste(Long matricula, Long empresa, Long id){
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	
	StringBuilder sql = new StringBuilder();
	GregorianCalendar retorno = null;
	try {
	    conn = PersistEngine.getConnection("TIDB");
	    
	    sql.append(" SELECT DATA, TIMEZONEDISPOSITIVO FROM BIO_PRESENCA_LOG ");
	    sql.append(" WHERE ID = ? ");
	    sql.append(" AND MATRICULA = ? ");
	    sql.append(" AND EMPRESA = ? ");
		
	    pstm = conn.prepareStatement(sql.toString());
	    
	    pstm.setLong(1, id);
	    pstm.setLong(2, matricula);
	    pstm.setLong(3, empresa);

	    rs = pstm.executeQuery();

	    while (rs.next()) {
		String timeZone = rs.getString("TIMEZONEDISPOSITIVO");
		if (timeZone != null){
		    TimeZone tz2 = TimeZone.getTimeZone(timeZone.trim());
		    TimeZone.setDefault(tz2);		    
		    retorno = (GregorianCalendar) GregorianCalendar.getInstance(tz2);
		    Timestamp data = rs.getTimestamp("DATA");
		    retorno.setTime(data);
		}else{
		    Timestamp data = rs.getTimestamp("DATA");
		    retorno = new GregorianCalendar();
		    retorno.setTime(data);
		}
		
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
	return retorno;
    }
}
