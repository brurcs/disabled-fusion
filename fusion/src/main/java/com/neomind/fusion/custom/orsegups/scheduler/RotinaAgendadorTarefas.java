package com.neomind.fusion.custom.orsegups.scheduler;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class RotinaAgendadorTarefas implements CustomJobAdapter
{

	private static final Log log = LogFactory.getLog(RotinaAgendadorTarefas.class);

	private Long key = GregorianCalendar.getInstance().getTimeInMillis();

	public void execute(CustomJobContext ctx)
	{

		log.warn("INICIADO EXECUÇÃO ROTINA AGENDADOR TAREFAS -- Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		QLGroupFilter filter = new QLGroupFilter("AND");
		filter.addFilter(new QLEqualsFilter("abreTarefa", Boolean.TRUE));

		List<NeoObject> listaDadosTarefa = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIAgendadorTarefas"), filter);

		GregorianCalendar dataSistema = new GregorianCalendar();

		dataSistema.set(Calendar.HOUR_OF_DAY, 0);
		dataSistema.set(Calendar.MINUTE, 0);
		dataSistema.set(Calendar.SECOND, 0);
		dataSistema.set(Calendar.MILLISECOND, 0);
		

		for (NeoObject dadosTarefa : listaDadosTarefa)
		{

			EntityWrapper wrapperDadosTarefa = new EntityWrapper(dadosTarefa);
			GregorianCalendar ultimaExecucao = null;
			GregorianCalendar iniciarExecucao = (GregorianCalendar) wrapperDadosTarefa.findField("iniciarExecucao").getValue();

			if (iniciarExecucao == null)
			{

				if (wrapperDadosTarefa.findField("ultimaExecucao").getValue() == null)
				{
					ultimaExecucao = new GregorianCalendar();
					ultimaExecucao.add(Calendar.DAY_OF_MONTH, -1);

				}
				else
				{
					ultimaExecucao = (GregorianCalendar) wrapperDadosTarefa.findField("ultimaExecucao").getValue();
					ultimaExecucao.set(Calendar.HOUR_OF_DAY, 0);
					ultimaExecucao.set(Calendar.MINUTE, 0);
					ultimaExecucao.set(Calendar.SECOND, 0);
					ultimaExecucao.set(Calendar.MILLISECOND, 0);
				}

			}
			else
			{
				if (wrapperDadosTarefa.findField("ultimaExecucao").getValue() == null)
				{
					ultimaExecucao = (GregorianCalendar) iniciarExecucao.clone();

				}
				else
				{
					ultimaExecucao = (GregorianCalendar) wrapperDadosTarefa.findField("ultimaExecucao").getValue();
				}
			}

			if (dataSistema.after(ultimaExecucao))
			{

				long idItervalo = (long) wrapperDadosTarefa.findField("intervalo.idIntervalo").getValue();

				if (idItervalo == 1)
				{
					int diaSemana = dataSistema.get(Calendar.DAY_OF_WEEK);
					long diaSemanaTarefa = (long) wrapperDadosTarefa.findField("diaDaSemana.idDiaSemana").getValue();

					if (diaSemana == diaSemanaTarefa)
					{
						this.abrirTarefas(wrapperDadosTarefa);
						wrapperDadosTarefa.findField("ultimaExecucao").setValue(dataSistema);
						PersistEngine.persist(dadosTarefa);
					}

				}
				else if (idItervalo == 2)
				{

					int diaMes = dataSistema.get(Calendar.DAY_OF_MONTH);

					long diaMesTarefa = (long) wrapperDadosTarefa.findField("diaDoMes").getValue();

					if (diaMes == diaMesTarefa || (diaMes == dataSistema.getActualMaximum(Calendar.DAY_OF_MONTH) && diaMesTarefa > diaMes))
					{
						this.abrirTarefas(wrapperDadosTarefa);
						wrapperDadosTarefa.findField("ultimaExecucao").setValue(dataSistema);
						PersistEngine.persist(dadosTarefa);
					}

				}

				else if (idItervalo == 4)
				{

					ultimaExecucao.add(Calendar.MONTH, 2);
					wrapperDadosTarefa.findField("ultimaExecucao").setValue(ultimaExecucao);
					this.abrirTarefas(wrapperDadosTarefa);
					PersistEngine.persist(dadosTarefa);
				}

				else
				{
					this.abrirTarefas(wrapperDadosTarefa);
					wrapperDadosTarefa.findField("ultimaExecucao").setValue(dataSistema);
					PersistEngine.persist(dadosTarefa);
				}
			}
		}
	}

	private void abrirTarefas(EntityWrapper wrapperDadosTarefa)
	{

		int numTarefa = 0;
		String tarefas = "";

		Long neoIdSolicitante = (Long) wrapperDadosTarefa.findField("solicitante.neoId").getValue();
		NeoUser objSolicitante = (NeoUser) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoUser"), new QLEqualsFilter("neoId", neoIdSolicitante));
		String solicitante = objSolicitante.getCode();

		@SuppressWarnings("unchecked")
		List<NeoUser> listaUsuarios = (List<NeoUser>) wrapperDadosTarefa.findField("executores").getValue();

		if (listaUsuarios.isEmpty())
		{
			List<NeoPaper> listaPapeis = (List<NeoPaper>) wrapperDadosTarefa.findValue("papelExecutor");
			if (listaPapeis != null && !listaPapeis.isEmpty())
			{
				for (NeoPaper papelExecutor : listaPapeis)
				{
					if (papelExecutor != null && papelExecutor.getAllUsers() != null && !papelExecutor.getAllUsers().isEmpty())
					{
						for (NeoUser user : papelExecutor.getUsers())
						{
							listaUsuarios.add(user);
							break;
						}
					}
				}
			}
		}
		
		if(listaUsuarios != null && !listaUsuarios.isEmpty())
		{
			for (NeoUser usuario : listaUsuarios)
			{

				String executor = usuario.getCode();

				String titulo = (String) wrapperDadosTarefa.findField("titulo").getValue();

				String descricao = (String) wrapperDadosTarefa.findField("descricao").getValue();

				GregorianCalendar prazo = new GregorianCalendar();

				Long diasPrazo = (Long) wrapperDadosTarefa.findField("diasPrazo").getValue();
				prazo = OrsegupsUtils.getSpecificWorkDay(prazo, diasPrazo);
				prazo.set(Calendar.HOUR_OF_DAY, 23);
				prazo.set(Calendar.MINUTE, 59);
				prazo.set(Calendar.SECOND, 59);

				Long numExecucoes = (Long) wrapperDadosTarefa.findField("numeroDeTarefas").getValue();

				NeoFile anexo = (NeoFile) wrapperDadosTarefa.findField("anexo").getValue();

				IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();

				for (int i = 0; i < numExecucoes; i++)
				{
					String tarefa = "";
					if (anexo != null)
					{
						tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "hadouken", prazo, anexo);
					}
					else
					{
						tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "hadouken", prazo);
					}

					if (tarefa != null && !tarefa.isEmpty())
					{
						numTarefa++;
						tarefas += tarefa + " ";
					}
					else
					{
						log.error("ERRO ROTINA AGENDADOR TAREFA (ABRIRTAREFAS()) ERRO AO ABRIR TAREFAS");
						throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
					}
				}

			}
		}

		log.info("ABERTO TAREFAS VIA ROTINA AGENDADOR TAREFAS - Numero de tarefas: " + numTarefa + " [" + tarefas + "]");

	}

}
