package com.neomind.fusion.custom.orsegups.ql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.engine.runtime.RuntimeEngine;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class QLAdapterCancelaTarefaLotarColaborador implements AdapterInterface
{
	public static final Log log = LogFactory.getLog(QLAdapter.class);

	public static Boolean validation = true;
	public static Collection<String> erros = new ArrayList<String>();

	public static EntityManager entityManager;
	EntityTransaction transaction;

	/*
	 * 2 - Posto deve possuir um centro de custo.
	 * 3 - Para ser utilizado o posto deve estar ativo.
	 * 4 - Posto destino deve possuir uma filial ligada.
	 * 5 - Posto deve possuir vaga disponível.
	 * 6 - Posto deve ser da mesma empresa do colaborador.
	 * 7 - Para cobertura de férias o substituído deverá estar com situação férias no período da
	 * operação.
	 * 8 - O colaborador não pode possuir afastamento no período da operação (afastamento que abre vaga).
	 * 9 - O colaborador não pode possuir restrição no cliente.
	 */

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		this.validation = true;
		this.erros = new ArrayList<String>();
		try
		{
			if (processEntity.findValue("operacao") != null)
			{
				String codigoOperacao = (String) processEntity.findValue("operacao.codigo");
				if (codigoOperacao.equals("0004"))
				{
					PreparedStatement pstm = null;
					Connection conn = null;
					ResultSet rs = null;
					NeoObject colaborador = (NeoObject) processEntity.findValue("colaborador");
					EntityWrapper colaboradorWrapper = new EntityWrapper(colaborador);
					Long numEmp = (Long) colaboradorWrapper.findValue("numemp");
					Long tipCol = (Long) colaboradorWrapper.findValue("tipcol");
					Long numCad = (Long) colaboradorWrapper.findValue("numcad");
					conn = PersistEngine.getConnection("VETORH");

					StringBuilder sql = new StringBuilder();
					sql.append("select hlo1.numLoc,hlo1.tabOrg ");
					sql.append("from R038HLO hlo1 where numemp = ? ");
					sql.append("and tipcol = ? ");
					sql.append("and numcad = ? ");
					sql.append("and datalt = (select max(datalt)");
					sql.append(" from R038HLO hlo2 where hlo2.numcad = hlo1.NumCad and");
					sql.append(" hlo2.numemp = hlo1.NumEmp and");
					sql.append(" hlo2.tipcol = hlo1.TipCol )");
					pstm = conn.prepareStatement(sql.toString());

					if ((numEmp != null && tipCol != null && numCad != null) && (numEmp != 0L && tipCol != 0L && numCad != 0L))
					{
						pstm.setLong(1, numEmp);
						pstm.setLong(2, tipCol);
						pstm.setLong(3, numCad);
					}
					rs = pstm.executeQuery();
					while (rs.next())
					{
						Long numLoc = rs.getLong("numLoc");
						Long tabOrg = rs.getLong("tabOrg");
						if (numLoc == 27124 && tabOrg == 203)
						{
							String titulo = "'%LOTAR COLABORADOR " + numEmp + "/" + numCad + "%'";
							QLRawFilter filtroTarefa = new QLRawFilter("titulo like " + titulo + " and wfprocess.processState = 0");
							List<NeoObject> tarefas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("Tarefa"), filtroTarefa);

							if (NeoUtils.safeIsNotNull(tarefas))
							{
								for (NeoObject obj : tarefas)
								{
									EntityWrapper wTarefa = new EntityWrapper(obj);
									WFProcess wf = (WFProcess) wTarefa.findValue("wfprocess");
									RuntimeEngine.getProcessService().cancel(wf, "Tarefa finalizada devido a vinculação do colaborado ao posto!");
								}
							}

						}

					}
				}
			}

		}
		catch (Exception e)
		{
			if (this.transaction.isActive())
			{
				this.transaction.rollback();
			}

			e.printStackTrace();

			if (e instanceof WorkflowException)
			{
				throw (WorkflowException) e;
			}
			else
			{
				throw new WorkflowException("Erro realizar cancelamento de tarefa");
			}
		}
		finally
		{
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}
}