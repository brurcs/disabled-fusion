package com.neomind.fusion.custom.orsegups.scheduler;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class RotinaAbreTarefaAnaliseMKTDigital implements CustomJobAdapter {

    private static final Log log = LogFactory.getLog(RotinaAbreTarefaAnaliseMKTDigital.class);
    
    public void execute(CustomJobContext ctx) {
	
	Long key = GregorianCalendar.getInstance().getTimeInMillis();
	
	log.warn("INICIAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Analise MKT Digital -- Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	
	String solicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteAnaliseMKTDigital");
	String executor = OrsegupsUtils.getUserNeoPaper("ExecutorAnaliseMKTDigital");
	
	String titulo = "Analise do MKT Digital - Sistema RD Station";
	
	String descricao = "Enviar analise do envio dos e-mails MKTS da semana anterior <br>" 
            	+"Leades Enviados<br>"
            	+"Leades não enviados<br>"
            	+"Leades recebidos<br>"
            	+"Leades entregues com sucesso<br>"
            	+"Analise do Motivos dos não renviados<br>";
	
	GregorianCalendar prazo = new GregorianCalendar();
	prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 1L);
	prazo.set(Calendar.HOUR_OF_DAY, 23);
	prazo.set(Calendar.MINUTE, 59);
	prazo.set(Calendar.SECOND, 59);
	
	IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
	String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "hadouken", prazo);	

	if (tarefa != null && !tarefa.isEmpty()){
	    log.info("Aberto Tarefa Simples Analise MKT Digital - Numero: "+tarefa);
	}else{
	    log.error("ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Analise MKT Digital - ERRO AO GERAR TAREFA");
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	}
    }
}
