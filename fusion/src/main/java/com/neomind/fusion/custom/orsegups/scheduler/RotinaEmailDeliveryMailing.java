package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Map;

import javax.activation.DataSource;
import javax.activation.FileDataSource;

import org.apache.commons.mail.HtmlEmail;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;

import com.neomind.fusion.custom.orsegups.presenca.vo.ContaSigmaVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsConnectionUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;

public class RotinaEmailDeliveryMailing implements CustomJobAdapter {

    private ArrayList<ContaSigmaVO> clienteSigma = new ArrayList<ContaSigmaVO>();

    private ArrayList<ContaSigmaVO> clienteSapiens = new ArrayList<ContaSigmaVO>();

    private HashSet<String> emailClienteGeral = new HashSet<String>();
    
    private Map<Integer, String> serverInfo = OrsegupsConnectionUtils.getServerInfo("SIGMA");

    public enum Origem {
	SIGMA, SAPIENS, GERAL
    };

    public void execute(CustomJobContext ctx) {

	Connection conn = null;

	StringBuilder sql = new StringBuilder();
	PreparedStatement pstm = null;
	ResultSet rs = null;

	try {

	    sql.append("SELECT DISTINCT CL.NomCli AS RAZAO,CL.ApeCli AS FANTASIA, CL.IntNet AS EMAIL, usu_emactr AS EMAIL2, cl.emanfe AS EMAIL3, 'SAPIENS' AS ORIGEM ");
	    sql.append("FROM USU_T160CVS P WITH(NOLOCK) ");
	    sql.append("JOIN USU_T200REG R WITH(NOLOCK) ON R.USU_CODREG=P.USU_REGCVS ");
	    sql.append("JOIN USU_T160CTR C WITH(NOLOCK)ON P.USU_NUMCTR = C.USU_NUMCTR AND P.USU_CODEMP = C.USU_CODEMP AND P.USU_CODFIL = C.USU_CODFIL ");
	    sql.append("JOIN E085CLI CL WITH(NOLOCK) ON CL.CODCLI=C.USU_CODCLI ");
	    sql.append("WHERE ((c.USU_SITCtr = 'A') OR (c.USU_SITCtr = 'I' AND P.USU_DATFIM >= GETDATE())) ");
	    sql.append("AND CL.IntNet IS NOT NULL  ");
	    sql.append("AND LEN(CL.IntNet) > 1 ");
	    sql.append("AND CL.IntNet NOT LIKE '%nf.eletronica@orsegups.com.br%' ");
	    sql.append("AND CL.IntNet NOT LIKE '%faturamento@orsegups.com.br%' ");
	    sql.append("UNION ");
	    sql.append(" SELECT DISTINCT C.RAZAO COLLATE Latin1_General_CI_AS, C.FANTASIA COLLATE Latin1_General_CI_AS, C.EMAILRESP COLLATE Latin1_General_CI_AS AS EMAIL, P.EMAIL COLLATE Latin1_General_CI_AS AS EMAIL2, C.EMAILRELAUTO COLLATE Latin1_General_CI_AS AS EMAIL3, 'SIGMA' AS ORIGEM   ");
	    sql.append("FROM ["+this.serverInfo.get(OrsegupsConnectionUtils.NOME_SERVIDOR)+"\\"+this.serverInfo.get(OrsegupsConnectionUtils.INSTANCIA)+"].SIGMA90.dbo.dbCENTRAL C WITH(NOLOCK) ");
	    sql.append("INNER JOIN ["+this.serverInfo.get(OrsegupsConnectionUtils.NOME_SERVIDOR)+"\\"+this.serverInfo.get(OrsegupsConnectionUtils.INSTANCIA)+"].SIGMA90.dbo.dbPROVIDENCIA P WITH(NOLOCK) ON P.CD_CLIENTE = C.CD_CLIENTE ");
	    sql.append("WHERE C.EMAILRESP IS NOT NULL ");
	    sql.append("AND LEN(C.EMAILRESP) > 1 ");
	    sql.append("AND CTRL_CENTRAL = 1 ");
	    sql.append("AND C.RAZAO NOT LIKE('%DILMO%') ");
	    sql.append("AND C.RAZAO NOT LIKE ('%TESTE%') ");
	    sql.append("AND C.EMAILRESP NOT LIKE '%nf.eletronica@orsegups.com.br%' ");
	    sql.append("AND C.EMAILRESP NOT LIKE '%faturamento@orsegups.com.br%' ");
	    sql.append("ORDER BY RAZAO ");

	    conn = PersistEngine.getConnection("SAPIENS");
	    pstm = conn.prepareStatement(sql.toString());
	    rs = pstm.executeQuery();

	    while (rs.next()) {

		String email = rs.getString("EMAIL").trim();

		String email2 = rs.getString("EMAIL2") == null || rs.getString("EMAIL2").trim().isEmpty() ? "" : rs.getString("EMAIL2").trim();

		String email3 = rs.getString("EMAIL3") == null || rs.getString("EMAIL3").trim().isEmpty() ? "" : rs.getString("EMAIL3").trim();

		String razao = rs.getString("RAZAO");

		String fantasia = rs.getString("FANTASIA");

		String origem = rs.getString("ORIGEM");

		this.fazAMagica(email, email2, email3, razao, fantasia, origem);

	    }
	    
	    GregorianCalendar data = new GregorianCalendar();
	    
	    StringBuilder corpoEmail = new StringBuilder();
		 
	    corpoEmail.append("<!DOCTYPE html>");
	    corpoEmail.append("<html>");
	    corpoEmail.append("<head>");
	    corpoEmail.append("<meta charset=\"UTF-8\">");
	    corpoEmail.append("<title>Relatório de cobrança de OS</title>");
	    corpoEmail.append("<style>");
	    corpoEmail.append("h2{");
	    corpoEmail.append("font-weight:bold; color:#303090; letter-spacing:1pt; word-spacing:2pt; font-size:20px; text-align:center; font-family:Verdana; line-height:1;");
	    corpoEmail.append("}");
	    corpoEmail.append(".table-p{");
	    corpoEmail.append("font-family: Verdana; font-weight:normal; font-size:12px; text-align:justify;");
	    corpoEmail.append(" }");
	    corpoEmail.append(".table-info{");
	    corpoEmail.append("table-layout: fixed;");
	    corpoEmail.append("text-align:left;");
	    corpoEmail.append("font-family: Verdana;");
	    corpoEmail.append("}");
	    corpoEmail.append(".table-info td{");
	    corpoEmail.append("overflow-x: hidden;");
	    corpoEmail.append(" }");
	    corpoEmail.append(".table-info thead{");
	    corpoEmail.append("background-color:#303090;");
	    corpoEmail.append("color: white;");
	    corpoEmail.append("font-weight:bold;");
	    corpoEmail.append("	}");
	    corpoEmail.append("</style>");
	    corpoEmail.append("</head>");
	    corpoEmail.append("<body>");
	    corpoEmail.append("<div>");
	    corpoEmail.append("<table width=\"600\" align=\"center\">");
	    corpoEmail.append("<tr><td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/header.jpg\" width=\"600\" height=\"145\"></td></tr>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("<td><h2>Mailing</h2></td>");
	    corpoEmail.append("<table width=\"600\" align=\"center\" class=\"table-p\"> ");
	    corpoEmail.append("<tr><td>Segue em anexo Mailing, divido em três arquivos: Mailing Sigma, Mailing Sapiens e Maling Geral que inclui Sigma + Sapiens.</td></tr>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("<br>");
	    corpoEmail.append("<!-- Rodapé do e-mail -->");
	    corpoEmail.append("<div class=\"rodape\">");
	    corpoEmail.append("<table width=\"600\" align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
	    corpoEmail.append("<tbody>");
	    corpoEmail.append("<tr>");
	    corpoEmail.append("<td width=\"360\" valign=\"bottom\"><img src=\"https://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/footer-1.jpg\"></td>");
	    corpoEmail.append("<td><a href=\"http://www.orsegups.com.br\" target=\"_blank\"><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/footer-2.jpg\" width=\"260\" height=\"120\" alt=\"\"/></a></td>");
	    corpoEmail.append("</tr>");
	    corpoEmail.append("</tbody>");
	    corpoEmail.append("</table>");
	    corpoEmail.append("</div>");
	    corpoEmail.append("</div>");
	    corpoEmail.append("</body>");
	    corpoEmail.append("</html>");

	    int ano = data.get(Calendar.YEAR);
	    int mes = data.get(Calendar.MONTH) + 1;

	    String mesFormatado = String.format("%02d", mes);

	    String arquivo1 = this.gerarArquivo(Origem.SIGMA);
	    String arquivo2 = this.gerarArquivo(Origem.SAPIENS);
	    String arquivo3 = this.gerarArquivo(Origem.GERAL);
	        
	    HtmlEmail email = new HtmlEmail();
	    email.setHostName("SSOOMX01");
	    email.setSmtpPort(25);
	    
	    email.addTo("rodrigo.fontoura@orsegups.com.br");
	    email.addTo("suelen.machado@orsegups.com.br");
	    email.addTo("deborah.silva@orsegups.com.br");
	    email.addTo("maikon.vargas@orsegups.com.br");
	    email.addCc("emailautomatico@orsegups.com.br");
	    email.setFrom("fusion@orsegups.com.br");
	    email.setSubject("Mailing "+mesFormatado+"/"+ano);
	    email.setHtmlMsg(corpoEmail.toString());
	   
	    DataSource source1 = new FileDataSource(arquivo1);
	    DataSource source2 = new FileDataSource(arquivo2);
	    DataSource source3 = new FileDataSource(arquivo3);
	    
	    email.attach(source1, "Mailing-Sigma.xls", "Mailing-Sigma");
	    email.attach(source2, "Mailing-Sapiens.xls", "Mailing-Sapiens");
	    email.attach(source3, "Mailing-Geral.xls", "Mailing-Geral");
	    email.send();

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

    }

    private String gerarArquivo(Origem origem) {
	
	String nomeArquivo = "";
	
	try {

	    HSSFWorkbook workbook = new HSSFWorkbook();
	    HSSFSheet abaMailing = workbook.createSheet("Mailing");

	    CellStyle style = workbook.createCellStyle();
	    Font font = workbook.createFont();
	    font.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    style.setFont(font);

	    Row row = abaMailing.createRow(0);

	    Cell cellDescEmail = row.createCell(0);
	    cellDescEmail.setCellStyle(style);
	    cellDescEmail.setCellValue("E-mail");

	    Cell cellDescCliente = row.createCell(1);
	    cellDescCliente.setCellStyle(style);
	    cellDescCliente.setCellValue("Razão Social");

	    Cell cellDescFantasia = row.createCell(2);
	    cellDescFantasia.setCellStyle(style);
	    cellDescFantasia.setCellValue("Fantasia");

	    FileOutputStream arquivo = null;

	    GregorianCalendar data = new GregorianCalendar();

	    int ano = data.get(Calendar.YEAR);
	    int mes = data.get(Calendar.MONTH) + 1;

	    String mesFormatado = String.format("%02d", mes);

	    nomeArquivo = "";
	    
	    
	    int rowNum = 1;
	    Row linha = null;

	    if (origem == Origem.SIGMA) {
		nomeArquivo = NeoStorage.getDefault().getPath() + "/report/Mailing-Sigma-" + mesFormatado + "-" + ano + ".xls";

		for (ContaSigmaVO conta : clienteSigma) {

		    linha = abaMailing.createRow(rowNum++);

		    Cell cellEmail = linha.createCell(0);
		    cellEmail.setCellValue(conta.getEmail());

		    Cell cellRazao = linha.createCell(1);
		    cellRazao.setCellValue(conta.getRazao());

		    Cell cellFantasia = linha.createCell(2);
		    cellFantasia.setCellValue(conta.getFantasia());

		}

	    } else if (origem == Origem.SAPIENS) {
		nomeArquivo = NeoStorage.getDefault().getPath() + "/report/Mailing-Sapiens-" + mesFormatado + "-" + ano + ".xls";

		for (ContaSigmaVO conta : clienteSapiens) {

		    linha = abaMailing.createRow(rowNum++);

		    Cell cellEmail = linha.createCell(0);
		    cellEmail.setCellValue(conta.getEmail());

		    Cell cellRazao = linha.createCell(1);
		    cellRazao.setCellValue(conta.getRazao());

		    Cell cellFantasia = linha.createCell(2);
		    cellFantasia.setCellValue(conta.getFantasia());

		}
	    } else if (origem == Origem.GERAL) {
		nomeArquivo = NeoStorage.getDefault().getPath() + "/report/Mailing-Geral-" + mesFormatado + "-" + ano + ".xls";

		for (ContaSigmaVO conta : clienteSigma) {

		    linha = abaMailing.createRow(rowNum++);

		    Cell cellEmail = linha.createCell(0);
		    cellEmail.setCellValue(conta.getEmail());

		    Cell cellRazao = linha.createCell(1);
		    cellRazao.setCellValue(conta.getRazao());

		    Cell cellFantasia = linha.createCell(2);
		    cellFantasia.setCellValue(conta.getFantasia());

		}

		for (ContaSigmaVO conta : clienteSapiens) {

		    linha = abaMailing.createRow(rowNum++);

		    Cell cellEmail = linha.createCell(0);
		    cellEmail.setCellValue(conta.getEmail());

		    Cell cellRazao = linha.createCell(1);
		    cellRazao.setCellValue(conta.getRazao());

		    Cell cellFantasia = linha.createCell(2);
		    cellFantasia.setCellValue(conta.getFantasia());

		}

	    }

	    for (int i = 0; i < 3; i++) {
		abaMailing.autoSizeColumn(i);
	    }

	    arquivo = new FileOutputStream(new File(nomeArquivo));
	    workbook.write(arquivo);
	    arquivo.close();
	    
	    return nomeArquivo;
	    
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    System.out.println("Arquivo não encontrado!");
	} catch (IOException e) {
	    e.printStackTrace();
	    System.out.println("Erro na edição do arquivo!");
	}
	
	return nomeArquivo;
    }

    private void fazAMagica(String email, String email2, String email3, String razao, String fantasia, String origem) {
	String emails[] = null;

	if (email.contains(";")) {
	    emails = email.split(";");

	    for (String em : emails) {
		if (!emailClienteGeral.contains(em) && em.contains("@")) {
		    emailClienteGeral.add(em);

		    if (origem.equalsIgnoreCase("SIGMA")) {
			ContaSigmaVO novaConta = new ContaSigmaVO();
			novaConta.setRazao(razao);
			novaConta.setFantasia(fantasia);
			novaConta.setEmail(em);
			clienteSigma.add(novaConta);

		    } else if (origem.equalsIgnoreCase("SAPIENS")) {
			ContaSigmaVO novaConta = new ContaSigmaVO(); // Sim o mesmo VO, não me julge
			novaConta.setRazao(razao);
			novaConta.setFantasia(fantasia);
			novaConta.setEmail(em);
			clienteSapiens.add(novaConta);
		    }
		}
	    }

	} else if (email.contains(",")) {
	    emails = email.split(",");

	    for (String em : emails) {
		if (!emailClienteGeral.contains(em) && em.contains("@")) {
		    emailClienteGeral.add(em);

		    if (origem.equalsIgnoreCase("SIGMA")) {
			ContaSigmaVO novaConta = new ContaSigmaVO();
			novaConta.setRazao(razao);
			novaConta.setFantasia(fantasia);
			novaConta.setEmail(em);
			clienteSigma.add(novaConta);

		    } else if (origem.equalsIgnoreCase("SAPIENS")) {
			ContaSigmaVO novaConta = new ContaSigmaVO();
			novaConta.setRazao(razao);
			novaConta.setFantasia(fantasia);
			novaConta.setEmail(em);
			clienteSapiens.add(novaConta);
		    }

		}

	    }

	} else {
	    if (!emailClienteGeral.contains(email) && email.contains("@")) {
		emailClienteGeral.add(email);

		if (origem.equalsIgnoreCase("SIGMA")) {
		    ContaSigmaVO novaConta = new ContaSigmaVO();
		    novaConta.setRazao(razao);
		    novaConta.setFantasia(fantasia);
		    novaConta.setEmail(email);
		    clienteSigma.add(novaConta);

		} else if (origem.equalsIgnoreCase("SAPIENS")) {
		    ContaSigmaVO novaConta = new ContaSigmaVO();
		    novaConta.setRazao(razao);
		    novaConta.setFantasia(fantasia);
		    novaConta.setEmail(email);
		    clienteSapiens.add(novaConta);
		}

	    }

	}

	if (email2.contains(";")) {
	    emails = email2.split(";");

	    for (String em : emails) {
		if (!emailClienteGeral.contains(em) && em.contains("@")) {
		    emailClienteGeral.add(em);

		    if (origem.equalsIgnoreCase("SIGMA")) {
			ContaSigmaVO novaConta = new ContaSigmaVO();
			novaConta.setRazao(razao);
			novaConta.setFantasia(fantasia);
			novaConta.setEmail(em);
			clienteSigma.add(novaConta);

		    } else if (origem.equalsIgnoreCase("SAPIENS")) {
			ContaSigmaVO novaConta = new ContaSigmaVO(); 
			novaConta.setRazao(razao);
			novaConta.setFantasia(fantasia);
			novaConta.setEmail(em);
			clienteSapiens.add(novaConta);
		    }
		}
	    }

	} else if (email2.contains(",")) {
	    emails = email2.split(",");

	    for (String em : emails) {
		if (!emailClienteGeral.contains(em) && em.contains("@")) {
		    emailClienteGeral.add(em);

		    if (origem.equalsIgnoreCase("SIGMA")) {
			ContaSigmaVO novaConta = new ContaSigmaVO();
			novaConta.setRazao(razao);
			novaConta.setFantasia(fantasia);
			novaConta.setEmail(em);
			clienteSigma.add(novaConta);

		    } else if (origem.equalsIgnoreCase("SAPIENS")) {
			ContaSigmaVO novaConta = new ContaSigmaVO();
			novaConta.setRazao(razao);
			novaConta.setFantasia(fantasia);
			novaConta.setEmail(em);
			clienteSapiens.add(novaConta);
		    }

		}

	    }

	} else {
	    if (!emailClienteGeral.contains(email2) && email2.contains("@")) {
		emailClienteGeral.add(email2);

		if (origem.equalsIgnoreCase("SIGMA")) {
		    ContaSigmaVO novaConta = new ContaSigmaVO();
		    novaConta.setRazao(razao);
		    novaConta.setFantasia(fantasia);
		    novaConta.setEmail(email2);
		    clienteSigma.add(novaConta);

		} else if (origem.equalsIgnoreCase("SAPIENS")) {
		    ContaSigmaVO novaConta = new ContaSigmaVO();
		    novaConta.setRazao(razao);
		    novaConta.setFantasia(fantasia);
		    novaConta.setEmail(email2);
		    clienteSapiens.add(novaConta);
		}

	    }

	}

	if (email3.contains(";")) {
	    emails = email3.split(";");

	    for (String em : emails) {
		if (!emailClienteGeral.contains(em) && em.contains("@")) {
		    emailClienteGeral.add(em);

		    if (origem.equalsIgnoreCase("SIGMA")) {
			ContaSigmaVO novaConta = new ContaSigmaVO();
			novaConta.setRazao(razao);
			novaConta.setFantasia(fantasia);
			novaConta.setEmail(em);
			clienteSigma.add(novaConta);

		    } else if (origem.equalsIgnoreCase("SAPIENS")) {
			ContaSigmaVO novaConta = new ContaSigmaVO(); 
			novaConta.setRazao(razao);
			novaConta.setFantasia(fantasia);
			novaConta.setEmail(em);
			clienteSapiens.add(novaConta);
		    }
		}
	    }

	} else if (email3.contains(",")) {
	    emails = email3.split(",");

	    for (String em : emails) {
		if (!emailClienteGeral.contains(em) && em.contains("@")) {
		    emailClienteGeral.add(em);

		    if (origem.equalsIgnoreCase("SIGMA")) {
			ContaSigmaVO novaConta = new ContaSigmaVO();
			novaConta.setRazao(razao);
			novaConta.setFantasia(fantasia);
			novaConta.setEmail(em);
			clienteSigma.add(novaConta);

		    } else if (origem.equalsIgnoreCase("SAPIENS")) {
			ContaSigmaVO novaConta = new ContaSigmaVO();
			novaConta.setRazao(razao);
			novaConta.setFantasia(fantasia);
			novaConta.setEmail(em);
			clienteSapiens.add(novaConta);
		    }

		}

	    }

	} else {
	    if (!emailClienteGeral.contains(email3) && email3.contains("@")) {
		emailClienteGeral.add(email3);

		if (origem.equalsIgnoreCase("SIGMA")) {
		    ContaSigmaVO novaConta = new ContaSigmaVO();
		    novaConta.setRazao(razao);
		    novaConta.setFantasia(fantasia);
		    novaConta.setEmail(email3);
		    clienteSigma.add(novaConta);

		} else if (origem.equalsIgnoreCase("SAPIENS")) {
		    ContaSigmaVO novaConta = new ContaSigmaVO();
		    novaConta.setRazao(razao);
		    novaConta.setFantasia(fantasia);
		    novaConta.setEmail(email3);
		    clienteSapiens.add(novaConta);
		}
	    }
	}
    }

}
