package com.neomind.fusion.custom.orsegups.camerite.bean;

import java.io.Serializable;

public class CameriteUser implements Serializable{
    

    private static final long serialVersionUID = -8191378790697712774L;
    
    private String name;
    private String email;
    private String admin;
    private String phone;
    private String rg;
    private String cpf;
    private String cep;
    private String state;
    private String city;
    private String district;
    private String street;
    private String streetNumber;
    private String complement;
    private String observation;
    private String sigmaAuxiliar;
    private Boolean canUsePanic;
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getAdmin() {
        return admin;
    }
    public void setAdmin(String admin) {
        this.admin = admin;
    }
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getRg() {
        return rg;
    }
    public void setRg(String rg) {
        this.rg = rg;
    }
    public String getCpf() {
        return cpf;
    }
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    public String getCep() {
        return cep;
    }
    public void setCep(String cep) {
        this.cep = cep;
    }
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getDistrict() {
        return district;
    }
    public void setDistrict(String district) {
        this.district = district;
    }
    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }
    public String getStreetNumber() {
        return streetNumber;
    }
    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }
    public String getComplement() {
        return complement;
    }
    public void setComplement(String complement) {
        this.complement = complement;
    }
    public String getObservation() {
        return observation;
    }
    public void setObservation(String observation) {
        this.observation = observation;
    }
    public String getSigmaAuxiliar() {
        return sigmaAuxiliar;
    }
    public void setSigmaAuxiliar(String sigmaAuxiliar) {
        this.sigmaAuxiliar = sigmaAuxiliar;
    }
    public Boolean getCanUsePanic() {
        return canUsePanic;
    }
    public void setCanUsePanic(Boolean canUsePanic) {
        this.canUsePanic = canUsePanic;
    }
    
    

}
