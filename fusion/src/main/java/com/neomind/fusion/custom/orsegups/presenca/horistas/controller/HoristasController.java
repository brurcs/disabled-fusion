package com.neomind.fusion.custom.orsegups.presenca.horistas.controller;

import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.presenca.horistas.beans.Horista;
import com.neomind.fusion.custom.orsegups.presenca.horistas.handler.HoristasHandler;
import com.neomind.util.NeoDateUtils;

@Path(value = "horistas")
public class HoristasController {

    private static final Log log = LogFactory.getLog(HoristasController.class);
    
    
    private HoristasHandler handler = new HoristasHandler();

    @GET
    @Path("ranking/{regional}/{mes}/{ano}")
    @Produces("application/json")
    public List<Horista> getRanking(@PathParam("regional") String regional, @PathParam("mes") int mes, @PathParam("ano") int ano) {
	log.warn("Executando consulta de ranking " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	
	return handler.getRankingHorista(regional, mes, ano);

    }
    
    @GET
    @Path("getPermissaoExibicao")
    @Produces("application/json")
    public Map<String, Boolean> getPermissaoExibicao() {
	log.warn("Executando consulta permissão exibição " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));

	return handler.getPermissaoExibicao();

    }
    

}
