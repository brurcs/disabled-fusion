package com.neomind.fusion.custom.orsegups.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.neomind.fusion.persist.PersistEngine;

public class OrsegupsConnectionUtils {

    /**
     * Nome do sistema a ser solicitado
     */
    public static final Integer SISTEMA = 1;

    /**
     * Nome do servidor que hospeda o banco de dados
     */
    public static final Integer NOME_SERVIDOR = 2;
    /**
     * Instancia do servidor SQL Server
     */
    public static final Integer INSTANCIA = 3;

    /**
     * IP do servidor SQL Server
     */
    public static final Integer IP_SERVIDOR = 4;

    /**
     * Observação vinculada ao cadastro
     */
    public static final Integer OBSERVACAO = 5;

    /**
     * Retorna um mapa com as informações do servidor de banco de dados do
     * sistema solicitado conforme cadastrado em
     * <tt>FSOODB04\SQL02 TIDB LINKED_SERVERS</tt> As informações podem ser
     * recuperadas pelas chaves constantes incluidas nesta classe conforme
     * abaixo:
     * 
     * <pre>
     * {@link #SISTEMA} 
     * {@link #NOME_SERVIDOR} 
     * {@link #INSTANCIA} 
     * {@link #IP_SERVIDOR} 
     * {@link #OBSERVACAO}
     * </pre>
     * 
     * @param sistema
     *            <code>String</code> nome do sistema à obter as informações do  servidor de banco de dados
     * @return <code>HashMap</code> incluindo as informações solicitadas; 
     *         <code>null</code> se nenhuma informação for encontrada para o servidor
     */
    public static Map<Integer, String> getServerInfo(String sistema) {

	if (sistema != null && !sistema.isEmpty()) {


	    Map<Integer, String> resultado = new HashMap<Integer, String>();
	    Connection conn = null;
	    PreparedStatement pstm = null;
	    ResultSet rs = null;
	    try {
		
		conn = PersistEngine.getConnection("TIDB");

		String sql = " SELECT SISTEMA, NOME_SERVIDOR, INSTANCIA, IP_SERVIDOR, OBSERVACAO FROM LINKED_SERVERS WITH(NOLOCK) WHERE SISTEMA=? ";


		pstm = conn.prepareStatement(sql);

		pstm.setString(1, sistema);

		rs = pstm.executeQuery();

		if (rs.next()) {
		    
		    resultado.put(SISTEMA, rs.getString(1));
		    resultado.put(NOME_SERVIDOR, rs.getString(2));
		    resultado.put(INSTANCIA, rs.getString(3) != null ? rs.getString(3) : "" );
		    resultado.put(IP_SERVIDOR, rs.getString(4) != null ? rs.getString(4) : "" );
		    resultado.put(OBSERVACAO, rs.getString(5) != null ? rs.getString(5) : "" );
		    
		    return resultado;
		}

	    } catch (SQLException e) {
		e.printStackTrace();
	    } catch (Exception e) {
		e.printStackTrace();
	    } finally {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    }
	}

	return null;
    }

}
