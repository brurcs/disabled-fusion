package com.neomind.fusion.custom.orsegups.endpoint.dto;

import java.util.List;

/**
 * Classe que contém os campos necessários para a criação de um tarefa do
 * cftv-inteligente
 * 
 * @author jaime.gomes
 *
 */
public class Imagens {

    private String idCamera;
    private List<Imagens> listReferencias;
    private byte[] bytesImagem;
    private String nomeImagem;
    private String descricaoProblema;
    private List<String> labels;
    private String pathImagem;

    /**
     * @return the idCamera
     */
    public String getIdCamera() {
	return idCamera;
    }

    /**
     * @param idCamera
     *            the idCamera to set
     */
    public void setIdCamera(String idCamera) {
	this.idCamera = idCamera;
    }

    /**
     * @return the listReferencias
     */
    public List<Imagens> getListReferencias() {
	return listReferencias;
    }

    /**
     * @param listReferencias
     *            the listReferencias to set
     */
    public void setListReferencias(List<Imagens> listReferencias) {
	this.listReferencias = listReferencias;
    }

    /**
     * @return the bytesImagem
     */
    public byte[] getBytesImagem() {
	return bytesImagem;
    }

    /**
     * @param bytesImagem
     *            the bytesImagem to set
     */
    public void setBytesImagem(byte[] bytesImagem) {
	this.bytesImagem = bytesImagem;
    }

    /**
     * @return the nomeImagem
     */
    public String getnomeImagem() {
	return nomeImagem;
    }

    /**
     * @param nomeImagem
     *            the nomeImagem to set
     */
    public void setnomeImagem(String nomeImagem) {
	this.nomeImagem = nomeImagem;
    }

    /**
     * @return the descricaoProblema
     */
    public String getDescricaoProblema() {
	return descricaoProblema;
    }

    /**
     * @param descricaoProblema
     *            the descricaoProblema to set
     */
    public void setDescricaoProblema(String descricaoProblema) {
	this.descricaoProblema = descricaoProblema;
    }

    /**
     * @return the labels
     */
    public List<String> getLabels() {
	return labels;
    }

    /**
     * @param labels
     *            the labels to set
     */
    public void setLabels(List<String> labels) {
	this.labels = labels;
    }

    /**
     * @return the nomeImagem
     */
    public String getNomeImagem() {
        return nomeImagem;
    }

    /**
     * @param nomeImagem the nomeImagem to set
     */
    public void setNomeImagem(String nomeImagem) {
        this.nomeImagem = nomeImagem;
    }

    /**
     * @return the pathImagem
     */
    public String getPathImagem() {
        return pathImagem;
    }

    /**
     * @param pathImagem the pathImagem to set
     */
    public void setPathImagem(String pathImagem) {
        this.pathImagem = pathImagem;
    }
    
    

}
