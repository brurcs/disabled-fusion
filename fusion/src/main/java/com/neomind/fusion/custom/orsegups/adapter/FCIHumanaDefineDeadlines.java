package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class FCIHumanaDefineDeadlines implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{		
			GregorianCalendar startDate = (GregorianCalendar) activity.getProcess().getStartDate();
			startDate.set(GregorianCalendar.HOUR_OF_DAY, 0);
			startDate.set(GregorianCalendar.MINUTE, 0);
			startDate.set(GregorianCalendar.SECOND, 0);
			startDate.set(GregorianCalendar.MILLISECOND, 0);
			
			// Deadline 3 dias
			GregorianCalendar deadDate = (GregorianCalendar) startDate.clone();
			deadDate = OrsegupsUtils.addDays(startDate, 3, true);
			processEntity.setValue("dataDeadPrimeiro", deadDate);
			// Deadline 4 dias
			deadDate = OrsegupsUtils.addDays(startDate, 4, true);
			processEntity.setValue("dataDeadSegundo", deadDate);
			// Deadline 8 dias
			deadDate = OrsegupsUtils.addDays(startDate, 8, true);
			processEntity.setValue("dataDeadTerceiro", deadDate);
			// Deadline 9 dias
			deadDate = OrsegupsUtils.addDays(startDate, 9, true);
			processEntity.setValue("dataDeadQuarto", deadDate);
			// Deadline 16 dias
			deadDate = OrsegupsUtils.addDays(startDate, 16, true);
			processEntity.setValue("dataDeadQuinto", deadDate);
			// Deadline 17 dias
			deadDate = OrsegupsUtils.addDays(startDate, 17, true);
			processEntity.setValue("dataDeadSexto", deadDate);
			// Deadline 26 dias
			deadDate = OrsegupsUtils.addDays(startDate, 26, true);
			processEntity.setValue("dataDeadSetimo", deadDate);
			// Deadline 31 dias
			deadDate = OrsegupsUtils.addDays(startDate, 31, true);
			processEntity.setValue("dataDeadOitavo", deadDate);
			// Deadline 61 dias
			deadDate = OrsegupsUtils.addDays(startDate, 61, true);
			processEntity.setValue("dataDeadNono", deadDate);
			
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}
	
}
