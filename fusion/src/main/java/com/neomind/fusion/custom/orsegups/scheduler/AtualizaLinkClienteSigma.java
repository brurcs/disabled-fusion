package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoUtils;

public class AtualizaLinkClienteSigma implements CustomJobAdapter {

	private static final Log log = LogFactory.getLog(AtualizaLinkClienteSigma.class);

	@Override
	public void execute(CustomJobContext arg0) {

		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		
		Connection conn = PersistEngine.getConnection("SIGMA90");
		StringBuilder sqlUpdateLinkSigmaCliente = new StringBuilder();
		PreparedStatement pstm = null;
		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.AtualizaLinkClienteSigma");
		log.warn("##### INICIO UPDATE SIGMA90: Inicia update no link das contas dos clientes do Sigma - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		try {

			sqlUpdateLinkSigmaCliente.append("UPDATE dbCENTRAL SET NM_PAGINA_WEB = '" + OrsegupsUtils.URL_PRODUCAO + "/fusion/custom/jsp/orsegups/sigma/telaSumarioDesktop.jsp?cd_cliente=' + CONVERT(VARCHAR, CD_CLIENTE) WHERE (NM_PAGINA_WEB IS NULL OR NM_PAGINA_WEB = ' ')");
			pstm = conn.prepareStatement(sqlUpdateLinkSigmaCliente.toString());
			pstm.executeUpdate();
			
		} catch (Exception e) {
			log.error("##### ERRO AGENDADOR DE TAREFA: Atualiza Link do Sistema Sigma (Tabela DbCentral)");
			System.out.println("["+key+"] ##### ERRO AGENDADOR DE TAREFA: Atualiza Link do Sistema Sigma (Tabela DbCentral)");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
		} finally {
			try {
				pstm.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			log.warn("##### FIM DO UPDATE SIGMA90: Inicia update no link das contas dos clientes do Sigma - Data: "	+ NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
		}
	}
}
