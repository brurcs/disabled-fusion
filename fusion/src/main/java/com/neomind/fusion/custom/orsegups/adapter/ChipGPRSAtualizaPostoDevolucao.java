package com.neomind.fusion.custom.orsegups.adapter;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.FieldWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;	
import java.util.Collection;

public class ChipGPRSAtualizaPostoDevolucao
  implements AdapterInterface
{
  public void start(Task origin, EntityWrapper processEntity, Activity activity)
  {
    try
    {
      String nomeFonteDados = "SAPIENS";
      
      Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
      
      Long codEmp = (Long)processEntity.findValue("postoSapiens.usu_codemp");
      Long codFil = (Long)processEntity.findValue("postoSapiens.usu_codfil");
      Long numCtr = (Long)processEntity.findValue("postoSapiens.usu_numctr");
      Long numPos = (Long)processEntity.findValue("postoSapiens.usu_numpos");
      String codSer = (String)processEntity.findValue("postoSapiens.usu_codser");
      
      Collection<NeoObject> chips = processEntity.findField("chipsGPRS").getValues();
      if ((chips != null) && (chips.size() > 0)) {
        for (NeoObject chip : chips)
        {
          EntityWrapper wrapperLista = new EntityWrapper(chip);
          String iccID = (String)wrapperLista.findField("iccID").getValue();
          
          StringBuffer sqlSelect = new StringBuffer();
          sqlSelect.append(" SELECT * FROM USU_T160CHP ");
          sqlSelect.append(" WHERE usu_codemp = ? AND usu_codfil = ? AND usu_numctr = ? AND usu_numpos = ? AND usu_iccid = ? AND usu_codser = ? and usu_sitchp = ? ");
          
          PreparedStatement stSelect = connection.prepareStatement(sqlSelect.toString());
          stSelect.setLong(1, codEmp.longValue());
          stSelect.setLong(2, codFil.longValue());
          stSelect.setLong(3, numCtr.longValue());
          stSelect.setLong(4, numPos.longValue());
          stSelect.setString(5, iccID);
          stSelect.setString(6, codSer);
          stSelect.setString(7, "A");
          
          ResultSet rs = stSelect.executeQuery();
          if (rs.next())
          {
            StringBuffer sql = new StringBuffer();
            sql.append(" UPDATE dbo.usu_t160chp SET usu_sitchp = ?");
            sql.append(" WHERE usu_codemp = ? AND usu_codfil = ? AND usu_numctr = ? AND usu_numpos = ?  AND usu_iccid = ? AND usu_codser = ?");
            
            PreparedStatement st = connection.prepareStatement(sql.toString());
            st.setString(1, "D");
            st.setLong(2, codEmp.longValue());
            st.setLong(3, codFil.longValue());
            st.setLong(4, numCtr.longValue());
            st.setLong(5, numPos.longValue());
            st.setString(6, iccID);
            st.setString(7, codSer);
            
            st.executeUpdate();
          }
        }
      }/* else {
        throw new WorkflowException("Favor Incluir um Chip Com defeito!");
      }*/
    }
    catch (SQLException e)
    {
      e.printStackTrace();
      throw new WorkflowException("Não foi possivel atualizar as informação no Sapiens.");
    }
  }
  
  public void back(EntityWrapper processEntity, Activity activity) {}
}
