package com.neomind.fusion.custom.orsegups.servlets;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoRunnable;
import com.neomind.fusion.entity.EntityInfo;
import com.neomind.fusion.entity.FieldInfo;
import com.neomind.fusion.entity.FieldViewRules;
import com.neomind.fusion.image.DocumentConverterEngine;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.Job;
import com.neomind.fusion.scheduler.quartz.JobsRegister;

/**
 * Servlet implementation class 
 */
@WebServlet(name = "ServletInicializacao", urlPatterns = { "/servlet/servletInicializacao" }, asyncSupported=true)
public class ServletInicializacaoRegras extends HttpServlet
{
	public static final Log log = LogFactory.getLog(ServletInicializacaoRegras.class);
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletInicializacaoRegras()
	{
		super();
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void init() throws ServletException {
		
		List<Job> jobs = (List<Job>) PersistEngine.getObjects(Job.class);
		
		if (jobs != null && !jobs.isEmpty()){
			for (Job job : jobs){
				pauseJob(job);
			}
		}
		
	}
	
	private void pauseJob(Job job)
	{
		job.setEnabled(false);
		JobsRegister.pause(job);
	}
}
