package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.File;
import java.util.GregorianCalendar;

import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.MultiPartEmail;

import com.neomind.fusion.custom.orsegups.ti.RelatorioValidacaoOSChip;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;


public class EnviaEmailRelatorioValidacaoOSChp1 implements CustomJobAdapter
{
 
    @Override
    public void execute(CustomJobContext ctx)
    {
	System.out.println("Inicio para gerar relatorio EnviaEmailRelatorioValidacaoOSChp1");
	processaJob(ctx);
	
    }


    public static void processaJob(CustomJobContext ctx){
	try {
	    gerarRelatorio();
	} catch (Exception e) {
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por [ EnviaEmailRelatorioValidacaoOSChp1 ]");
	}
    }
    
    private static void gerarRelatorio() throws Exception {
	
	RelatorioValidacaoOSChip RelatorioValidacaoOSChip = new RelatorioValidacaoOSChip();
	File file = com.neomind.fusion.custom.orsegups.ti.RelatorioValidacaoOSChip.geraPdf();

	MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

	EmailAttachment attachment = new EmailAttachment();
	attachment.setPath(file.getAbsolutePath());
	attachment.setDisposition(EmailAttachment.ATTACHMENT);
	attachment.setDescription("Relatório de Validações");
	attachment.setName(file.getName());

	MultiPartEmail email = new MultiPartEmail();
	email.addTo("laercio.leite@orsegups.com.br");		    
	email.addCc("fabio.meier@orsegups.com.br");
	email.addBcc("carlos.silva@orsegups.com.br");
	email.setSubject("Relatório de Validações");
	email.setMsg("Segue em anexo relatório de Validações");
	email.attach(attachment);

	email.setFrom(settings.getFromEMail(),settings.getFromName());
	email.setSmtpPort(settings.getPort());
	email.setHostName(settings.getSmtpServer());
	email.send();
    }

}