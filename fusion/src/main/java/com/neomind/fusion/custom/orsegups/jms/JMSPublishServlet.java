package com.neomind.fusion.custom.orsegups.jms;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.callcenter.OrsegupsIncommingCallServlet;

@WebServlet(name="JMSPublishServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.jms.JMSPublishServlet"})
public class JMSPublishServlet extends HttpServlet {

	private static final Log log = LogFactory.getLog(JMSPublishServlet.class);
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.doRequest(req, resp);
	}

	protected void doRequest(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String message = req.getParameter("message");
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss:SS");
		
		try
		{
			NeoJMSPublisher.getInstance().sendCallCenterMessage("Send: "+dateFormat.format(new Date())+"\n"+message, "Cassiano");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error(e.getMessage());
		}
		
		
	}

}
