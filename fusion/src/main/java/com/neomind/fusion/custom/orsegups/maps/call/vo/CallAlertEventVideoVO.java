package com.neomind.fusion.custom.orsegups.maps.call.vo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.neomind.fusion.custom.orsegups.callcenter.ProvidenciaVO;
import com.neomind.fusion.custom.orsegups.maps.call.engine.OrsegupsAlertEventVideoEngine;
import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.maps.vo.ViaturaVO;
import com.neomind.fusion.custom.orsegups.seventh.bean.EventoArmeDesarme;
import com.neomind.fusion.custom.orsegups.ti.sigma.engine.SIGMAUtilsEngine;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class CallAlertEventVideoVO {
    private final String type = "CallAlertEventVideoVO";

    private ViaturaVO callViaturaVO;

    private EventoVO callEventoVO;

    private LinkedHashSet<ProvidenciaVO> numerosChamar;

    private String ultimoNumeroChamado;

    private Integer ultimoNumero;

    private LinkedHashSet<String> numerosJaChamados = new LinkedHashSet<String>();

    private String ramalOperador;

    private String infoWindow;

    private String ultimaCameraVisualizada;

    private String dataEventoVideo;

    private String dataVideo;

    private String horaVideo;

    private String camerasCliente;

    public String getDataVideo() {
	return dataVideo;
    }

    public void setDataVideo(String dataVideo) {
	this.dataVideo = dataVideo;
    }

    public String getHoraVideo() {
	return horaVideo;
    }

    public void setHoraVideo(String horaVideo) {
	this.horaVideo = horaVideo;
    }

    private Integer minutosEvento;

    public ViaturaVO getCallViaturaVO() {
	return callViaturaVO;
    }

    public void setCallViaturaVO(ViaturaVO callViaturaVO) {
	this.callViaturaVO = callViaturaVO;
    }

    public EventoVO getCallEventoVO() {
	return callEventoVO;
    }

    public void setCallEventoVO(EventoVO callEventoVO) {
	this.callEventoVO = callEventoVO;
    }

    public LinkedHashSet<ProvidenciaVO> getNumerosChamar() {
	return numerosChamar;
    }

    public void setNumerosChamar(LinkedHashSet<ProvidenciaVO> numerosChamar) {
	this.numerosChamar = numerosChamar;
    }

    public LinkedHashSet<String> getNumerosJaChamados() {
	return numerosJaChamados;
    }

    public void setNumerosJaChamados(LinkedHashSet<String> numerosJaChamados) {
	this.numerosJaChamados = numerosJaChamados;
    }

    public String getRamalOperador() {
	return ramalOperador;
    }

    public void setRamalOperador(String ramalOperador) {
	this.ramalOperador = ramalOperador;
    }

    public String getUltimoNumeroChamado() {
	return ultimoNumeroChamado;
    }

    public void setUltimoNumeroChamado(String ultimoNumeroChamado) {
	this.ultimoNumeroChamado = ultimoNumeroChamado;
    }

    public String getType() {
	return type;
    }

    public String getInfoWindow() {
	return infoWindow;
    }

    public void setInfoWindow(String infoWindow) {
	this.infoWindow = infoWindow;
    }

    public Integer getUltimoNumero() {
	return ultimoNumero;
    }

    public void setUltimoNumero(Integer ultimoNumero) {
	this.ultimoNumero = ultimoNumero;
    }

    public String getUltimaCameraVisualizada() {
	return ultimaCameraVisualizada;
    }

    public void setUltimaCameraVisualizada(String ultimaCameraVisualizada) {
	this.ultimaCameraVisualizada = ultimaCameraVisualizada;
    }

    public void updateInfoWindow() {
	StringBuilder contentString = new StringBuilder();
	try {

	    List<String> cameras = new ArrayList<String>();
	    TreeMap<String, String> treeMap = new TreeMap<String, String>();
	    String cliente = "<div class='panel-title'><span class='glyphicon glyphicon-user' aria-hidden='true' ></span> Cliente</div>";

	    contentString.append(" <div class=\"easyui-layout\" style=\"width:100%;height:750px;\"> ");
	    contentString.append(" <div id=\"p\" data-options=\"region:'west',split:true,collapsed:true\" title=\"" + cliente + "\" style=\"width:50%;padding:2px;margin:0px;\"  data-options=\"fit:true\"> ");
	    contentString.append(" <div class=\"well well-sm bg-success\" ");
	    contentString.append(" 		style=\"font-family: verdana; font-size: 10px; width: 100%; margin: 1px;padding: 2px;\"> ");
	    contentString.append(" <div align=\"left\" style=\"float: left; width: 50%; height: 20px;\"> ");
	    contentString.append(" <span class=\"label label-default\" ");
	    contentString.append(" style=\"cursor: pointer; font-family: verdana; font-size: 11px; font-weight: bold;\"> ");
	    contentString.append(" <span class=\"glyphicon glyphicon-info-sign\" aria-hidden=\"true\"> ");

	    Date dateAux = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(this.callEventoVO.getDataRecebimento());

	    String dataRecebimentoStr = NeoDateUtils.safeDateFormat(dateAux, "yyyy-MM-dd HH:mm:ss");

	    this.setDataEventoVideo(dataRecebimentoStr);

	    int count = 0;
	    for (ProvidenciaVO providenciaVO : this.numerosChamar) {

		if (count == 0) {

		    contentString.append(" <span id=\"descNomeEvento\" style=\"font-family: verdana; font-size: 11px; font-weight: bold;\"> " + providenciaVO.getNome());
		    contentString.append(" </span> ");
		    contentString.append(" </span> ");
		    contentString.append(" </span> ");
		    contentString.append(" </div> ");

		    contentString.append(" <br/><div align=\"right\"  style=\"float: left; width: 50%;height:20px;\">	");
		    contentString.append(" <span class=\"label label-danger\"><span class=\"glyphicon glyphicon-time\" aria-hidden=\"true\">");
		    contentString.append(" <span id=\"clock\"  style=\"font-family: verdana; font-size: 11px;font-weight: bold;\"></span> </span> </span>");
		    contentString.append(" </div>");
		    contentString.append(" <br>");

		    contentString.append(" <div class=\"span1\" style=\"margin:0px;\"> ");

		    // contentString.append(" <button type=\"button\" onclick=\"showInfoCliente('"
		    // + this.callEventoVO.getCodigoCliente() + "','" +
		    // this.callEventoVO.getCodigoHistorico() +
		    // "')\" title=\"Informações adicionais\" class=\"btn btn-danger btn-sm btn3d\"><span class=\"glyphicon glyphicon-list\"></span></button> ");
		    contentString.append(" <button type=\"button\" class=\"btn3d btn btn-white btn-sm\" title=\"Data de recebimento do evento\"><span class=\"glyphicon glyphicon-tag\"></span>"
			    + this.callEventoVO.getDataRecebimento() + "</button> ");
		    contentString.append(" </div>");

		}
		count++;

	    }
	    contentString.append(" </div>");
	    contentString.append(" 	<div class=\"well well-sm\" ");
	    contentString.append(" 			style=\"font-family: verdana; font-size: 10px; display: table; width: 100%; padding: 2px 2px;margin: 0px;\"> ");
	    contentString.append(" 			<h6 style=\"text-decoration: underline;\"> ");
	    contentString.append(" <span class=\"glyphicon glyphicon-alert\" aria-hidden=\"true\"></span> ");
	    contentString.append(" Dados Conta ");
	    contentString.append(" </h6> ");
	    contentString.append(" <div style=\"float: left;\"> ");
	    contentString.append(" <p> ");
	    contentString.append(" <strong>Conta/Partição: </strong><span id=\"codigoCentral\" class=\"label label-danger\"  ");
	    contentString.append(" style=\"font-family: verdana; font-size: 11px; font-weight: bold;\">  ");
	    contentString.append(" </span>  ");
	    contentString.append(" </p>   ");
	    contentString.append(" 	<p>  ");
	    contentString.append(" 	<strong>Razão Social: </strong><span id=\"razaoSocialSigma\"></span> ");
	    contentString.append(" 	</p> ");
	    contentString.append(" 	<p> ");
	    contentString.append(" 	<strong>Fantasia: </strong><span id=\"fantasiaSigma\"></span> ");
	    contentString.append(" 	</p> ");
	    contentString.append(" <p>    ");
	    contentString.append(" <strong>Rota: </strong><span id=\"rota\"></span> ");
	    contentString.append(" </p>  ");
	    contentString.append(" <p>  ");
	    contentString.append(" <strong>CEP: </strong><span id=\"cepSigma\"></span>  ");
	    contentString.append(" </p>  ");
	    contentString.append(" <p>  ");
	    contentString.append(" <strong>Endereço: </strong><span id=\"logradouro\"></span>  ");
	    contentString.append(" </p>  ");
	    contentString.append(" <p>   ");
	    contentString.append(" <strong>Bairro: </strong><span id=\"bairroSigma\"></span> ");
	    contentString.append(" </p>  ");
	    contentString.append(" <p>  ");
	    contentString.append(" <strong>Cidade - Estado: </strong><span id=\"cidadeSigma\"></span> - <span id=\"ufSigma\">  ");
	    contentString.append(" </p> ");
	    contentString.append(" <p>  ");
	    contentString.append(" <strong>Responsável: </strong><span id=\"responsavelCentral\"></span> ");
	    contentString.append(" </p>  ");
	    contentString.append(" <p>  ");
	    contentString.append(" <strong>Telefone 1: </strong> <span id=\"telefoneCentral1\" class=\"label label-primary\" ");
	    contentString.append(" style=\"cursor: pointer; font-family: verdana; font-size: 11px; font-weight: bold;\"> ");
	    contentString.append(" 	<span class=\"glyphicon glyphicon-phone-alt\" aria-hidden=\"true\"> ");
	    contentString.append(" <span style=\"font-family: verdana; font-size: 11px; font-weight: bold;\"> ");
	    contentString.append(" </span></span> ");
	    contentString.append(" </p> ");
	    contentString.append(" <p> ");
	    contentString.append(" <strong>Email: </strong><span id=\"emailCentral\"></span> ");
	    contentString.append(" </p> ");
	    contentString.append(" <p> ");
	    contentString.append(" <strong>Telefone 2: </strong> <span id=\"telefoneCentral2\" class=\"label label-primary\"  ");
	    contentString.append(" style=\"cursor: pointer; font-family: verdana; font-size: 11px; font-weight: bold;\">  ");
	    contentString.append(" <span class=\"glyphicon glyphicon-phone-alt\" aria-hidden=\"true\">  ");
	    contentString.append(" <span style=\"font-family: verdana; font-size: 11px; font-weight: bold;\">  ");
	    contentString.append(" </span></span> ");
	    contentString.append(" </p>  ");
	    contentString.append(" <p>  ");
	    contentString.append(" <strong>Pergunta: </strong><span id=\"pergunta\" class=\"label label-danger\" style=\"font-family: verdana; font-size: 11px; font-weight: bold;\">  ");
	    contentString.append(" </span>  ");
	    contentString.append(" </p>  ");
	    contentString.append(" <p> ");
	    contentString.append(" <strong>Resposta: </strong><span id=\"resposta\" class=\"label label-danger\" ");
	    contentString.append(" style=\"font-family: verdana; font-size: 11px; font-weight: bold;\"> ");
	    contentString.append(" </span>  ");
	    contentString.append(" </p> ");
	    contentString.append(" <p> ");
	    contentString.append(" <strong>Coação: </strong><span id=\"coacao\" class=\"label label-danger\" ");
	    contentString.append(" style=\"font-family: verdana; font-size: 11px; font-weight: bold;\"> ");
	    contentString.append(" </span>  ");	    
	    contentString.append(" </p> ");
	    
	    
	    //TODO NOVO
	    
	    List<EventoArmeDesarme> eventos = SIGMAUtilsEngine.getUltimoArmeDesarme(Integer.parseInt(this.getCallEventoVO().getCodigoCliente()));
	    
	    if (eventos != null){
		    contentString.append(" <p> ");
		    contentString.append(" <strong>Último arme/desarme: </strong><span id=\"armeDesarme\" > <br>");
		    
		    for (int i=0; i<eventos.size(); i++){
			contentString.append(eventos.get(i).getOperacao()+ " "+ NeoDateUtils.safeDateFormat(new Date(eventos.get(i).getDtProcessado().getTime()), "dd/MM/yyyy HH:mm:ss"));
			contentString.append("<br>");
		    }
		    contentString.append(" </span>  ");
		    contentString.append(" </p> ");
	    }
	    
	    contentString.append(" <div style='width:650px;height:290px;font-family: Calibri,verdana,helvetica,arial,sans-serif;font-size:12px;margin:0px;padding:0px;' > ");
	    contentString.append(" <ul class='nav nav-tabs' id='mainTabs' role='tablist' > ");
	    contentString.append(" <li class='active'> ");
	    contentString.append(" <a   href='#contatos' role='tab' data-toggle='tab'> ");
	    contentString.append(" <p class='text-center'> <span  style='font-family: verdana; font-size: 10px;'>  Contatos </span></p> ");
	    contentString.append(" </a> ");
	    contentString.append(" </li> ");
	    contentString.append(" <li > ");
	    contentString.append(" <a href='#eventos' role='tab' data-toggle='tab'> ");
	    contentString.append(" <p class='text-center'> <span  style='font-family: verdana; font-size: 10px;'>  Eventos </span></p>  ");
	    contentString.append(" </a>  ");
	    contentString.append(" </li> ");
	    contentString.append(" <li>  ");
	    contentString.append(" <a href='#log' role='tab' data-toggle='tab'>  ");
	    contentString.append(" <p class='text-center'><span  style='font-family: verdana; font-size: 10px;'> Log </span></p>  ");
	    contentString.append(" </a>  ");
	    contentString.append(" </li> ");
	    contentString.append(" <li> ");
	    contentString.append(" <a href='#anotacoes' role='tab' data-toggle='tab'>  ");
	    contentString.append(" <p class='text-center'><span  style='font-family: verdana; font-size: 10px;'> Anotações </span></p> ");
	    contentString.append(" </a> ");
	    contentString.append(" </li> ");
	    contentString.append(" <li> ");
	    contentString.append(" <a href='#usuarios' role='tab' data-toggle='tab'> ");
	    contentString.append(" <p class='text-center'><span  style='font-family: verdana; font-size: 10px;'> Usuários </span></p>  ");
	    contentString.append(" </a>  ");
	    contentString.append(" </li> ");
	    contentString.append(" </ul> ");
	    contentString.append("  <!-- Tab panes --> ");
	    contentString.append("  <div class='tab-content' id='tab' style='width: 100%; font-family: verdana; font-size: 10px;'>   ");
	    contentString.append("  <div class='tab-pane fade active in'  style='width: 100%;max-height:199px; font-family: verdana; font-size: 12px;padding: 10px 10px;overflow:auto;' id='contatos'> ");
	    contentString.append("  <div  style='margin:0px;height:auto;'> ");
	    contentString.append("  <table class='table table-striped table-bordered' id='dgProvidencia'  style='width: 100%;max-height:220px; font-family: verdana; font-size: 11px;'> ");
	    contentString.append("  <thead >  ");
	    contentString.append("  <tr data-sortable='true' style='width: 100%; font-family: verdana;padding: 10px 10px;font-size: 10px;'>  ");
	    contentString.append("  <th data-sortable='true'>Providência</th>  ");
	    contentString.append("  <th data-sortable='true'>Nome</th> ");
	    contentString.append("  <th data-sortable='true'>Telefone 1</th> ");
	    contentString.append("  <th data-sortable='true'>Telefone 2</th> ");
	    contentString.append("  <th data-sortable='true'>Email</th> ");
	    contentString.append("  <th data-sortable='true'>Propriedade</th> ");
	    contentString.append("  </tr> ");
	    contentString.append("  </thead> ");
	    contentString.append("  <tbody  style='width: 100%;;max-height:99px; font-family: verdana;padding: 2px 2px;font-size: 10px;overflow: auto;'> ");
	    contentString.append("  <tr style='width: 100%; font-family: verdana;padding: 10px 10px;font-size: 10px;'> ");
	    contentString.append(" <td style='white-space: normal'> </td> ");
	    contentString.append(" <td style='white-space: normal'> </td> ");
	    contentString.append(" <td style='white-space: normal'> </td> ");
	    contentString.append(" <td style='white-space: normal'> </td> ");
	    contentString.append(" <td style='white-space: normal; text-align: right'> </td> ");
	    contentString.append(" <td style='white-space: normal; text-align: right'> </td> ");
	    contentString.append(" </tr>  ");
	    contentString.append(" <tr style='width: 100%; font-family: verdana;padding: 10px 10px;font-size: 10px;'>  ");
	    contentString.append(" <td style='white-space: normal'> </td> ");
	    contentString.append(" <td style='white-space: normal'> </td> ");
	    contentString.append(" <td style='white-space: normal'> </td> ");
	    contentString.append(" <td style='white-space: normal'> </td> ");
	    contentString.append(" <td style='white-space: normal; text-align: right'> </td> ");
	    contentString.append(" <td style='white-space: normal; text-align: right'> </td> ");
	    contentString.append("  </tr> ");
	    contentString.append(" <tr style='width: 100%; font-family: verdana;padding: 10px 10px;font-size: 10px;'> ");
	    contentString.append(" <td style='white-space: normal'> </td> ");
	    contentString.append(" <td style='white-space: normal'> </td> ");
	    contentString.append(" <td style='white-space: normal'> </td> ");
	    contentString.append(" <td style='white-space: normal'> </td> ");
	    contentString.append(" <td style='white-space: normal; text-align: right'> </td> ");
	    contentString.append(" <td style='white-space: normal; text-align: right'> </td> ");
	    contentString.append("  </tr>  ");
	    contentString.append("  </tbody>  ");
	    contentString.append("  </table>  ");
	    contentString.append("  </div>	  ");
	    contentString.append("  </div>  ");
	    contentString.append("  <div class='tab-pane  fade' align='left' style='width: 100%;max-height:245px; font-family: verdana; font-size: 12px;padding: 5px 5px;overflow:auto;'  id='eventos'>  ");
	    contentString.append("  Pesquisar: <div class='btn-group'>  ");
	    contentString.append("   <button class='btn btn-default btn-xs dropdown-toggle' type='button' id='dropdownMenu'  data-toggle='dropdown' aria-expanded='false' >  ");
	    contentString.append("    <span class='glyphicon glyphicon-sort-by-attributes' id='dropdown_title'></span><span class='caret'></span>  ");
	    contentString.append("  </button>  ");
	    contentString.append("  <ul class='dropdown-menu pull-center' role='menu'>   ");
	    contentString.append("  <li> <a id='dpPosicao' onclick='javascript:showHistoricoEventos(" + this.getCallEventoVO().getCodigoCliente() + ",1 )' href='#'>1 dia</a></li> ");
	    contentString.append("  <li> <a id='dpPosicao' onclick='javascript:showHistoricoEventos(" + this.getCallEventoVO().getCodigoCliente() + ",3)' href='#'>3 dias</a></li> ");
	    contentString.append("  <li> <a id='dpPosicao' onclick='javascript:showHistoricoEventos(" + this.getCallEventoVO().getCodigoCliente() + ",5)' href='#'>5 dias</a></li> ");
	    contentString.append("  <li> <a id='dpPosicao' onclick='javascript:showHistoricoEventos(" + this.getCallEventoVO().getCodigoCliente() + ",7)' href='#'>7 dias</a></li> ");
	    contentString.append("  <li> <a id='dpPosicao' onclick='javascript:showHistoricoEventos(" + this.getCallEventoVO().getCodigoCliente() + ",10)' href='#'>10 dias</a></li> ");
	    contentString.append("  <li> <a id='dpPosicao' onclick='javascript:showHistoricoEventos(" + this.getCallEventoVO().getCodigoCliente() + ",20)' href='#'> 20 dias</a></li> ");
	    contentString.append("  <li> <a id='dpPosicao' onclick='javascript:showHistoricoEventos(" + this.getCallEventoVO().getCodigoCliente() + ",30)' href='#'> 30 dias</a></li> ");
	    contentString.append("  </ul> ");
	    contentString.append("  </div> ");
	    contentString.append("  <div  id='dgHistorico' style='width: 100%;overflow:auto;' > ");
	    contentString.append("  <table class='table table-bordered table-striped' style='width: 100%; font-family: verdana; font-size: 11px;overflow:auto;' disabled='disabled'>  ");
	    contentString.append("  <thead >  ");
	    contentString.append("  <tr data-sortable='true' style='width: 100%; font-family: verdana;padding: 2px 2px;font-size: 10px;'> ");
	    contentString.append("  <th data-sortable='true'>Prioridade</th>  ");
	    contentString.append("  <th data-sortable='true'>Recebido</th> ");
	    contentString.append("  <th data-sortable='true'>Evento</th> ");
	    contentString.append("  <th data-sortable='true'>Aux 2</th> ");
	    contentString.append("  <th data-sortable='true'>Descrição</th> ");
	    contentString.append("  <th data-sortable='true'>Em Espera</th> ");
	    contentString.append("  <th data-sortable='true'>Deslocou</th> ");
	    contentString.append("  <th data-sortable='true'>No Local</th> ");
	    contentString.append("  <th data-sortable='true'>Fechado</th> ");
	    contentString.append("  <th data-sortable='true'>Obs</th> ");
	    contentString.append("  </tr> ");
	    contentString.append("  </thead> ");
	    contentString.append("  <tbody  style='width: 100%;max-height:99px; font-family: verdana;padding: 2px 2px;font-size: 10px;overflow: auto;'> ");
	    contentString.append("   <tr style='width: 100%; font-family: verdana;padding: 5px 5px;font-size: 10px;'> ");
	    contentString.append("   <td style='white-space: normal'> </td> ");
	    contentString.append("   <td style='white-space: normal'> </td> ");
	    contentString.append("   <td style='white-space: normal'> </td> ");
	    contentString.append("   <td style='white-space: normal'> </td> ");
	    contentString.append("   <td style='white-space: normal'> </td> ");
	    contentString.append("   <td style='white-space: normal'> </td> ");
	    contentString.append("   <td style='white-space: normal'> </td> ");
	    contentString.append("   <td style='white-space: normal'> </td> ");
	    contentString.append("   <td style='white-space: normal; text-align: right'> </td> ");
	    contentString.append("   <td style='white-space: normal; text-align: right'> </td> ");
	    contentString.append("   </tr> ");
	    contentString.append("   <tr style='width: 100%; font-family: verdana;padding: 10px 10px;font-size: 10px;'> ");
	    contentString.append("   <td style='white-space: normal'> </td> ");
	    contentString.append("   <td style='white-space: normal'> </td> ");
	    contentString.append("   <td style='white-space: normal'> </td> ");
	    contentString.append("   <td style='white-space: normal'> </td> ");
	    contentString.append("   <td style='white-space: normal'> </td> ");
	    contentString.append("   <td style='white-space: normal'> </td> ");
	    contentString.append("   <td style='white-space: normal'> </td> ");
	    contentString.append("   <td style='white-space: normal'> </td> ");
	    contentString.append("   <td style='white-space: normal; text-align: right'> </td> ");
	    contentString.append("   <td style='white-space: normal; text-align: right'> </td> ");
	    contentString.append("   </tr> ");
	    contentString.append("   <tr style='width: 100%; font-family: verdana;padding: 10px 10px;font-size: 10px;'> ");
	    contentString.append("   <td style='white-space: normal'> </td> ");
	    contentString.append("   <td style='white-space: normal'> </td> ");
	    contentString.append("   <td style='white-space: normal'> </td> ");
	    contentString.append("   <td style='white-space: normal'> </td> ");
	    contentString.append("   <td style='white-space: normal'> </td> ");
	    contentString.append("   <td style='white-space: normal'> </td> ");
	    contentString.append("   <td style='white-space: normal'> </td>  ");
	    contentString.append("   <td style='white-space: normal'> </td>  ");
	    contentString.append("   <td style='white-space: normal; text-align: right'> </td>  ");
	    contentString.append("   <td style='white-space: normal; text-align: right'> </td>  ");
	    contentString.append("   </tr>  ");
	    contentString.append("   </tbody>  ");
	    contentString.append("   </table>  ");
	    contentString.append("   </div>  ");
	    contentString.append("   </div>  ");
	    contentString.append("   <div class='tab-pane  fade'  style='width: 100%;max-height:245px; font-family: verdana; font-size: 12px;padding: 5px 5px;overflow:auto;'  id='log'>  ");
	    contentString.append("   <form class='form-horizontal'>  ");
	    contentString.append("   <div class='form-group' style='margin:0px;' >  ");
	    contentString.append("   <label for='inputEmail3' class='col-sm-1 control-label' style='font-family: verdana; font-size: 11px;'>Obs: </label>  ");
	    contentString.append("   <div class='col-sm-10'>  ");
	    contentString
		    .append("   <textarea class='form-control' id='taLog' style='overflow:auto;resize:none;width:99%;height:140px;font-family: Arial;font-size: 9pt;margin:0px;' rows='4' col='100' style='font-family: verdana; font-size: 11px;'></textarea>  ");
	    contentString.append("   </div>  ");
	    contentString.append("   </div>  ");
	    contentString.append("   <div class='form-group' style='margin:2px;' align='center'>   ");
	    contentString.append("   <div class='col-sm-offset-2 col-sm-10' style='margin:0px;width:99%;'>  ");
	    contentString.append("   <button type='button' class='btn btn-primary btn-xs' title='Deslocado Atendente' onclick='adicionaLog(\"Deslocado Atendente\")'>1</button>  ");
	    contentString
		    .append("   <button type='button' class='btn btn-primary btn-xs' title='Atendente no local realizando a verificação.' onclick='adicionaLog(\"Atendente no local realizando a verificação.\")'>2</button>  ");
	    contentString
		    .append("   <button type='button' class='btn btn-primary btn-xs' title='Verificação de imagens no último minuto -  câmera 1() câmera 2() câmera 3() câmera 4().' onclick='adicionaLog(\"Verificação de imagens no último minuto -  câmera 1() câmera 2() câmera 3() câmera 4().\")'>3</button>  ");
	    contentString
		    .append("   <button type='button' class='btn btn-primary btn-xs' title='Verificação de imagens nos últimos dois minutos-  câmera 1() câmera 2() câmera 3() câmera 4().' onclick='adicionaLog(\"Verificação de imagens nos últimos dois minutos-  câmera 1() câmera 2() câmera 3() câmera 4().\")'>4</button>  ");
	    contentString
		    .append("   <button type='button' class='btn btn-primary btn-xs' title='Local com ordem de serviço aberta, aguardando conclusão.' onclick='adicionaLog(\"Local com ordem de serviço aberta, aguardando conclusão.\")'>5</button>   ");
	    contentString
		    .append("   <button type='button' class='btn btn-primary btn-xs' title='Verificação de imagens nos últimos três minutos-  câmera 1() câmera 2() câmera 3() câmera 4().' onclick='adicionaLog(\"Verificação de imagens nos últimos três minutos-  câmera 1() câmera 2() câmera 3() câmera 4().\")'>6</button>   ");
	    contentString
		    .append("   <button type='button' class='btn btn-primary btn-xs' title='Verificação de imagens nos últimos cinco minutos-  câmera 1() câmera 2() câmera 3() câmera 4()' onclick='adicionaLog(\"Verificação de imagens nos últimos cinco minutos-  câmera 1() câmera 2() câmera 3() câmera 4().\")'>7</button>  ");
	    contentString
		    .append("   <button type='button' class='btn btn-primary btn-xs' title='Feito contato com o(a) Sr.(a) , o(a) qual informou estar tudo bem no local. Confirmada palavra-chave.' onclick='adicionaLog(\"Feito contato com o(a) Sr.(a) , o(a) qual informou estar tudo bem no local. Confirmada palavra-chave.\")'>8</button>  ");
	    contentString.append("   <button type='button' class='btn btn-primary btn-xs' title='Contato Telefônico do local sem exito.' onclick='adicionaLog(\"Contato Telefônico do local sem exito.\")'>9</button>  ");
	    contentString
		    .append("   <button type='button' class='btn btn-primary btn-xs' title='Verificação de imagens nos últimos quinze minutos-  câmera 1() câmera 2() câmera 3() câmera 4().' onclick='adicionaLog(\"Verificação de imagens nos últimos quinze minutos-  câmera 1() câmera 2() câmera 3() câmera 4().\")'>10</button>  ");
	    contentString
		    .append("   <button type='button' class='btn btn-primary btn-xs' title='Ronda virtual realizada com sucesso conexão ok.' onclick='adicionaLog(\"Ronda virtual realizada com sucesso conexão ok.\")'>11</button>  ");
	    contentString
		    .append("   <button type='button' class='btn btn-primary btn-xs' title='Verificação de imagens nos últimos trinta minutos-  câmera 1() câmera 2() câmera 3() câmera 4().' onclick='adicionaLog(\"Verificação de imagens nos últimos trinta minutos-  câmera 1() câmera 2() câmera 3() câmera 4().\")'>12</button>  ");
	    contentString
		    .append("   <button type='button' class='btn btn-primary btn-xs' title='Verificação  de Imagens na última hora-  câmera 1() câmera 2() câmera 3() câmera 4().' onclick='adicionaLog(\"Verificação  de Imagens na última hora-  câmera 1() câmera 2() câmera 3() câmera 4().\")'>13</button>  ");
	    contentString.append("   <button type='button' class='btn btn-primary btn-xs' title='Imagens com problemas na verificação.' onclick='adicionaLog(\"Imagens com problemas na verificação.\")'>14</button>   ");
	    contentString
		    .append("   <button type='button' class='btn btn-primary btn-xs' title='Vistoria feita no local sem alteração.:' onclick='adicionaLog(\"Vistoria feita no local sem alteração.:\")'>15</button>   ");
	    contentString.append("   </p>  ");
	    contentString.append("   <button type='button' class='btn btn-primary btn-xs' title='Sucesso' onclick='adicionaLog(\"Ligação realizada com sucesso.\")'>Sucesso</button>   ");
	    contentString.append("   <button type='button' class='btn btn-primary btn-xs' title='Falha' onclick='adicionaLog(\"Ligação falha.\")'>Falha</button>   ");
	    contentString.append("   <button type='button' class='btn btn-primary btn-xs' title='Não atende' onclick='adicionaLog(\"Tentativa realizada mas não atende.\")'>Não atende</button>   ");
	    contentString.append("   <button type='button' class='btn btn-primary btn-xs' title='Ocupado' onclick='adicionaLog(\"Tentativa realizada mas ocupado.\")'>Ocupado</button>   ");
	    contentString.append("   <button type='button' class='btn btn-primary btn-xs' title='Caixa postal' onclick='adicionaLog(\"Tentativa realizada mas caixa postal.\")'>Caixa postal</button>   ");
	    contentString.append("   <button type='button' class='btn btn-primary btn-xs' title='Não Existe'  onclick='adicionaLog(\"Tentativa realizada mas numero não Existe.\")'>Não Existe</button>   ");
	    contentString.append("   </p>  ");
	    contentString.append("   <button type='button' class='btn btn-primary btn-xs' title=\"Salvar Log\"  onclick='salvaLogEventoPopUp()'>Salvar</button>   ");
	    contentString.append("   <button type='button' class='btn btn-primary btn-xs'  title=\"Limpar Log\" onclick='limpaLog()'>Limpar</button>   ");
	    contentString.append("   </p>  ");
	    contentString.append("   </div>   ");
	    contentString.append("   </div>   ");
	    contentString.append("   </form>   ");
	    contentString.append("   </div>  ");
	    contentString.append("   <div class='tab-pane  fade' style='width: 100%;max-height:220px; font-family: verdana; font-size: 12px;padding: 10px 10px;overflow:auto;'  id='anotacoes'>  ");
	    contentString.append("   <table class='table table-bordered table-striped' id='containerAnotacoes' style='width: 100%; font-family: verdana;padding: 10px 10px;min-width:200px;' disabled='disabled'>  ");
	    contentString.append("   <tr>  ");
	    contentString
		    .append("   <td> <span><legend><b><span  style='font-family: verdana; font-size: 12px;'>Anotações temporárias.</span></b></legend></span> <textarea readonly id='taObsTemp' class='disabled'   style='overflow:auto;resize:none;width:99%;height:120px;font-family: Arial;font-size: 9pt;margin:0px;' rows='4' col='100' style='font-family: verdana; font-size: 11px;'> </textarea> </td>  ");
	    contentString
		    .append("   <td> <span><legend><b><span  style='font-family: verdana; font-size: 12px;'>Providências a serem tomadas.</span></b></legend></span><textarea readonly class='disabled'  id='taObsProvidencia' style='overflow:auto;resize:none;width:99%;height:120px;font-family: Arial;font-size: 9pt;margin:0px;' rows='4' col='100' style='font-family: verdana; font-size: 11px;'> </textarea> </td>  ");
	    contentString.append("   </tr>  ");
	    contentString.append("   </table>  ");
	    contentString.append("   </div>  ");
	    contentString.append("   <div class='tab-pane  fade' style='width: 100%;max-height:220px; font-family: verdana; font-size: 12px;padding: 10px 10px;overflow:auto;'  id='usuarios'>  ");
	    contentString.append("   <div  id='dgUsuarios'   style='margin:0px;height:auto;'>  ");
	    contentString.append("   <table class='table table-striped table-bordered' id='dgUsuarios'  style='width: 100%;max-height:199px; font-family: verdana; font-size: 11px;'>  ");
	    contentString.append("   <thead >   ");
	    contentString.append("   <tr data-sortable='true' style='width: 100%; font-family: verdana;padding: 10px 10px;font-size: 10px;'> ");
	    contentString.append("   <th data-sortable='true'>Código</th>  ");
	    contentString.append("   <th data-sortable='true'>Nome</th>  ");
	    contentString.append("   <th data-sortable='true'>Rg</th>  ");
	    contentString.append("   <th data-sortable='true'>Cpf</th>  ");
	    contentString.append("   <th data-sortable='true'>Senha</th>  ");
	    contentString.append("   <th data-sortable='true'>Telefone</th>  ");
	    contentString.append("   <th data-sortable='true'>Obs</th>  ");
	    contentString.append("   </tr>  ");
	    contentString.append("   </thead>  ");
	    contentString.append("   <tbody  style='width: 100%;;max-height:99px; font-family: verdana;padding: 2px 2px;font-size: 10px;overflow: auto;'>  ");
	    contentString.append("   <tr style='width: 100%; font-family: verdana;padding: 10px 10px;font-size: 10px;'>  ");
	    contentString.append("   <td style='white-space: normal'> </td>  ");
	    contentString.append("   <td style='white-space: normal'> </td>  ");
	    contentString.append("   <td style='white-space: normal'> </td>  ");
	    contentString.append("   <td style='white-space: normal'> </td>  ");
	    contentString.append("   <td style='white-space: normal; text-align: right'> </td>  ");
	    contentString.append("   <td style='white-space: normal; text-align: right'> </td>  ");
	    contentString.append("   <td style='white-space: normal; text-align: right'> </td>  ");
	    contentString.append("   </tr>  ");
	    contentString.append("   <tr style='width: 100%; font-family: verdana;padding: 10px 10px;font-size: 10px;'>  ");
	    contentString.append("   <td style='white-space: normal'> </td>  ");
	    contentString.append("   <td style='white-space: normal'> </td>  ");
	    contentString.append("   <td style='white-space: normal'> </td>  ");
	    contentString.append("   <td style='white-space: normal'> </td>  ");
	    contentString.append("   <td style='white-space: normal; text-align: right'> </td>  ");
	    contentString.append("   <td style='white-space: normal; text-align: right'> </td>  ");
	    contentString.append("   <td style='white-space: normal; text-align: right'> </td>  ");
	    contentString.append("   </tr>  ");
	    contentString.append("   <tr style='width: 100%; font-family: verdana;padding: 10px 10px;font-size: 10px;'>  ");
	    contentString.append("   <td style='white-space: normal'> </td>  ");
	    contentString.append("   <td style='white-space: normal'> </td>  ");
	    contentString.append("   <td style='white-space: normal'> </td>  ");
	    contentString.append("   <td style='white-space: normal'> </td>  ");
	    contentString.append("   <td style='white-space: normal; text-align: right'> </td>  ");
	    contentString.append("   <td style='white-space: normal; text-align: right'> </td>  ");
	    contentString.append("   <td style='white-space: normal; text-align: right'> </td>  ");
	    contentString.append("   </tr>  ");
	    contentString.append("   </tbody>  ");
	    contentString.append("   </table> ");
	    contentString.append("   </div> ");
	    contentString.append("   </div> ");
	    contentString.append("   </div> ");
	    contentString.append(" </div> ");
	    contentString.append(" </div> ");
	    contentString.append(" </div> ");

	    contentString.append(" </div> ");

	    String descInfo = "1 – A tratativa de monitoramento de imagens, busca apenas por eventos onde a CUC seja igual a ACP ou ACV, para todos os clientes que possuírem "
		    + "este protocolo personalizado."
		    + "<br/> 2 – Sobre as CUC’S."
		    + "<br/> •	ACP: CUC para acompanhamento normal traz apenas as câmeras externas do cliente."
		    + "<br/> •	ACV. CUC para acompanhamento VIP, a mesma vai trazer câmeras internas do cliente em primeiro lugar mais as câmeras externas na sequência, sendo que este tipo de CUC vai surgir caso exista um evento de alta prioridade."
		    + "<br/> 3 – Imagens."
		    + "<br/> •	A data de recebimento do evento menos 30 segundos será a data inicial(INSTANTE ZERO) da visualização de imagens do cliente."
		    + "<br> 4 – Registro de imagens."
		    + "<br/> •	O botão Capturar imagens irá guardar as imagens do evento em atendimento. As informações apresentadas referem-se à data e hora das imagens (D-Guard) e são atualizadas dinamicamente conforme as imagens são alteradas. OBS: O horário está sincronizado com o D-Guard, podendo existir divergência do horário apresentado pelo DVR do cliente.";

	    StringBuilder stringBuilder = new StringBuilder();
	    stringBuilder.append(" <button type=\"button\" class=\"btn btn-danger btn-xs\" data-container=\"body\"");
	    stringBuilder.append(" data-toggle=\"popover\" data-html=\"true\" data-placement=\"bottom\" title=\"Sobre a tratativa de monitoramento\"");
	    stringBuilder.append(" data-content=\"" + descInfo + "\"> ");
	    stringBuilder.append(" <span class=\"glyphicon glyphicon-info-sign\" aria-hidden=\"true\"></span> ");
	    stringBuilder.append(" </button> ");

	    String cameraStr = "<div class='panel-title'  style='vertical-align: middle;' >" + NeoUtils.encodeHTMLValue(stringBuilder.toString())
		    + " <span class='glyphicon glyphicon-facetime-video' aria-hidden='true' ></span> Cameras - " + this.getCallEventoVO().getFantasia()
		    + " <span style='display:none' class='duration' id='statusDiv'>00:00</span></div>";

	    String layoutStr = "<div class='panel-title'><span class='glyphicon glyphicon-th-large' aria-hidden='true' ></span> Layout </div>";
	    contentString.append(" <div data-options=\"region:'east',collapsed:true\" title=\"" + layoutStr + "\" style=\"width:100px;\"> ");
	    contentString.append(" <div style=\"padding:5px 0;width:99%;\" align=\"center\"> ");
	    contentString.append(" <p><button type=\"button\" onClick=\"javascrit:trocarLayoutCameras('" + this.getCallEventoVO().getCodigoCentral() + "','" + this.getCallEventoVO().getEmpresa()
		    + "',1)\"  class=\"btn btn-primary btn3d\"><span class=\"glyphicon glyphicon-stop\"></span></button> </p> ");
	    contentString.append(" <p><button type=\"button\" onClick=\"javascrit:trocarLayoutCameras('" + this.getCallEventoVO().getCodigoCentral() + "','" + this.getCallEventoVO().getEmpresa()
		    + "',2)\"  class=\"btn btn-primary btn3d\"><span class=\"glyphicon glyphicon-th-large\"></span></button> </p> ");
	    contentString.append(" <p><button type=\"button\" onClick=\"javascrit:trocarLayoutCameras('" + this.getCallEventoVO().getCodigoCentral() + "','" + this.getCallEventoVO().getEmpresa()
		    + "',3)\" class=\"btn btn-primary btn3d\"><span class=\"glyphicon glyphicon-th\"></span></button> </p> ");
	    contentString.append(" </div> ");
	    contentString.append(" </div> ");
	    contentString.append(" <div data-options=\"region:'center'\" title=\"" + cameraStr + "\"> <span id='clock' style='font-family: verdana; font-size: 11px;font-weight: bold;'></span>");
	    contentString.append(" <div id=\"gridCameras\"class=\"container-fluid\" align=\"center\" style=\"margin: 2px;padding: 2px;\"> ");
	    contentString.append(" <ul class=\"list-unstyled video-list-thumbs row\" style=\"width: 100%;\"> ");

	    int countVideos = 0;
	    String idCentral = this.getCallEventoVO().getCodigoCentral();
	    String empresa = this.getCallEventoVO().getEmpresa();
	    treeMap = OrsegupsAlertEventVideoEngine.getInstance().getCameraCliente(idCentral, empresa, this.getCallEventoVO());

	    if (treeMap != null && !treeMap.isEmpty()) {
		for (Entry<String, String> entry : treeMap.entrySet()) {
		    String camera = "";
		    if (entry != null) {
			camera = String.valueOf(entry.getKey());
			String descricaoCamera = entry.getValue();
			if (descricaoCamera != null && !descricaoCamera.contains("[VIP]")) {
			    cameras.add(camera);
			} else if (this.getCallEventoVO().getCdCode() != null && this.getCallEventoVO().getCdCode().equals("ACV")) {
			    if (descricaoCamera != null && descricaoCamera.contains("[VIP]")) {
				cameras.add(0, camera);
			    } else {
				cameras.add(camera);
			    }
			}
		    }
		}
	    }

	    // DATA RECEBIMENTO
	    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	    Date dateAuxRec = sdf.parse(this.getCallEventoVO().getDataRecebimento());
	    GregorianCalendar calendar = new GregorianCalendar();
	    calendar.setTime(dateAuxRec);
	    calendar.set(Calendar.SECOND, -30);
	    this.setDataEventoVideo(NeoDateUtils.safeDateFormat(calendar, "dd/MM/yyyy HH:mm:ss"));
	    String dataHora = this.getDataEventoVideo();

	    String hora = dataHora.substring(10, dataHora.length());
	    hora = hora.trim();
	    hora = hora.replaceAll(":", "-");
	    String data = dataHora.substring(0, 10);
	    data = data.trim();
	    data = data.replaceAll("/", "-");

	    String pattern = "dd-MM-yyyy HH-mm-ss";
	    SimpleDateFormat format = new SimpleDateFormat(pattern);

	    Date date = format.parse(data + " " + hora);

	    GregorianCalendar horario = new GregorianCalendar();
	    horario.setTime(date);
	    int minutos = horario.get(Calendar.HOUR_OF_DAY) * 60 + horario.get(Calendar.MINUTE);
	    this.setMinutosEvento(minutos);

	    if (cameras != null && !cameras.isEmpty()) {
		
		/** Ajuste layout */
		
		if (cameras.contains("48")){
		    cameras.remove("48");
		    cameras.add("48");
		}
		
		if (cameras.contains("89")){
		    cameras.remove("89");
		    cameras.add(1, "89");		    
		}
		
		if(cameras.contains("9")){
		    cameras.remove("9");
		    cameras.add(2, "9"); 
		}

		if(cameras.contains("58")){
		    cameras.remove("58");
		    cameras.add("58");
		}
		
		this.setDataVideo(data);
		this.setHoraVideo(hora);
		String camerasConcat = "";
		for (String camera : cameras) {
		    if (countVideos > 0)
			camerasConcat += ";";

		    camerasConcat += camera;

		    if (countVideos == 0) {

			this.setUltimaCameraVisualizada(camera);
			// String ultimaCamera = cameras.get(cameras - 1);

			contentString.append(" <li class=\"col-xs-8 col-sm-6\" style=\"padding: 0px;margin:0px;\"> ");
			contentString.append(" <a id=\"videoPrincipal\" > ");
			contentString.append(" <div class=\"embed-responsive embed-responsive-16by9\"   >  ");
			contentString.append(" <img style=\"width: 100%;height:100%;\" class=\"embed-responsive-item\"  id=\"" + camera + "\" name=\"" + camera + "\"   alt=\"" + camera + "\"></img>");
			contentString.append(" </div> ");
			contentString.append(" <span class=\"duration\" id=\"sessionDiv\" style=\"visibility:collapse\">00:00</span>  ");
			contentString.append(" </a> ");
			contentString.append(" </li> ");

		    } else if (countVideos == 1) {

			contentString.append(" <li class=\"col-xs-8 col-sm-6\" style=\"padding: 0px;margin:0px;\"> ");
			contentString.append(" <a title=\"Monitoramento de imagens\"> ");
			contentString.append(" <div id=\"videoPrincipal\"  class=\"embed-responsive embed-responsive-16by9\"   >  ");
			contentString.append(" <img style=\"width: 100%;height:100%;\" class=\"embed-responsive-item\"     id=\"" + camera + "\" name=\"" + camera + "\"   alt=\"" + camera + "\"></img>");
			contentString.append(" </div> ");
			contentString.append(" </a> ");
			contentString.append(" </li> ");
		    }
		    // /criar contador para 3 posições
		    if (countVideos >= 2 && countVideos < 4) {
			
			if (this.getCallEventoVO().getCodigoEvento().equals("XPLE") || this.getCallEventoVO().getCodigoEvento().equals("XPE2")) {
			    contentString.append(" <li class=\"col-xs-8 col-sm-6\" style=\"padding: 0px;margin:0px;\">  ");
			    contentString.append(" <a title=\"Monitoramento de imagens\">  ");
			    contentString.append(" <div class=\"embed-responsive embed-responsive-16by9\">  ");
			    contentString.append(" <img  class=\"embed-responsive-item\"  id=\"" + camera + "\" name=\""  + camera + "\"   alt=\"" + camera + "\"></img>");
			    contentString.append(" </div> ");
			    contentString.append(" <!-- <h2>Monitoramento de imagens</h2> --> ");
			    contentString.append(" </a> ");
			    contentString.append(" </li> ");	
			}else{
			    contentString.append(" <li class=\"col-xs-8 col-sm-6\" style=\"padding: 0px;margin:0px;\">  ");
			    contentString.append(" <a title=\"Monitoramento de imagens\">  ");
			    contentString.append(" <div class=\"embed-responsive embed-responsive-16by9\">  ");
			    contentString.append(" <img  class=\"embed-responsive-item\" onClick=\"javascrit:trocarCamerasCliente('" + idCentral + "','" + empresa + "','" + camera + "')\"   id=\"" + camera + "\" name=\""
				    + camera + "\"   alt=\"" + camera + "\"></img>");
			    contentString.append(" </div> ");
			    contentString.append(" <!-- <h2>Monitoramento de imagens</h2> --> ");
			    contentString.append(" </a> ");
			    contentString.append(" </li> ");			    
			}
		    } else if (countVideos >= 4) {
			
			if (this.getCallEventoVO().getCodigoEvento().equals("XPLE") || this.getCallEventoVO().getCodigoEvento().equals("XPE2")) {
			    contentString.append(" <li class=\"col-lg-3 col-md-4 col-sm-4 col-xs-6\" style=\"padding: 0px;margin:0px;\">  ");
			    contentString.append(" <a title=\"Monitoramento de imagens\">  ");
			    contentString.append(" <div class=\"embed-responsive embed-responsive-16by9\">  ");
			    contentString.append(" <img  class=\"embed-responsive-item\" src=\"" + PortalUtil.getBaseURL() + "custom/jsp/orsegups/seventh/mjpegStreamDGuardImagem.jsp?camera="
				    + camera + "&data=" + data + "&hora=" + hora + "&qualidade=80&formato=jpg\"   id=\"" + camera + "\" name=\"" + camera + "\"   alt=\"" + camera + "\"></img>");
			    contentString.append(" </div> ");
			    contentString.append(" <!-- <h2>Monitoramento de imagens</h2> --> ");
			    contentString.append(" </a> ");
			    contentString.append(" </li> ");
			} else {
			    contentString.append(" <li class=\"col-lg-3 col-md-4 col-sm-4 col-xs-6\" style=\"padding: 0px;margin:0px;\">  ");
			    contentString.append(" <a title=\"Monitoramento de imagens\">  ");
			    contentString.append(" <div class=\"embed-responsive embed-responsive-16by9\">  ");
			    contentString.append(" <img  class=\"embed-responsive-item\" onClick=\"javascrit:trocarCamerasCliente('" + idCentral + "','" + empresa + "','" + camera + "')\"     src=\"" + PortalUtil.getBaseURL() + "custom/jsp/orsegups/seventh/mjpegStreamDGuardImagem.jsp?camera="
				    + camera + "&data=" + data + "&hora=" + hora + "&qualidade=80&formato=jpg\"   id=\"" + camera + "\" name=\"" + camera + "\"   alt=\"" + camera + "\"></img>");
			    contentString.append(" </div> ");
			    contentString.append(" <!-- <h2>Monitoramento de imagens</h2> --> ");
			    contentString.append(" </a> ");
			    contentString.append(" </li> ");
			}
			

		    }

		    countVideos++;
		}
		this.setCamerasCliente(camerasConcat);
		contentString.append(" </ul> ");
	    }
	    contentString.append(" 	</div> ");
	    contentString.append(" 	</div> ");
	    contentString.append(" 	</div> ");
	    contentString.append("  <div style=\"width: 98%;\" align=\"right\" > ");
	    contentString.append(" <div class=\"row\">  ");
	    contentString.append(" <div class=\"col-xs-12 col-sm-6 col-lg-9\" align=\"left\">  ");
	    contentString.append(" <button type=\"button\" class=\"btn btn-danger btn-xs btn3d\" id=\"btnStop\" onclick=\"stopCamNow()\" > ");
	    contentString.append(" <span class=\"glyphicon glyphicon-stop\"   ></span></button> ");
	    contentString.append(" <button type=\"button\" class=\"btn btn-danger btn-xs btn3d\" id=\"btnPlayStop\"> ");
	    contentString.append(" <span class=\"glyphicon glyphicon-play\"  onclick=\"playAllCam()\" ></span></button> ");
	    contentString.append(" <button type=\"button\" class=\"btn btn-danger btn-xs btn3d\" id=\"btnSpeed\"  > ");
	    contentString.append(" <span class=\"glyphicon glyphicon-fast-forward\"  onclick=\"speedCam()\" ></span></button> ");
	    contentString.append(" <input type=\"number\" min=\"1\" max=\"10\" value=\"9\" id=\"numSpeed\" style=\"width:30px;\" > </input> ");
	    contentString.append(" <button type=\"button\" class=\"btn btn-danger btn-xs btn3d\" id=\"btnLive\" title=\"Imagem ao vivo\" > ");
	    contentString.append(" <span class=\"glyphicon glyphicon-expand\"  onclick=\"searchCamLive()\" ></span></button> ");

	    contentString.append(" <button type=\"button\" class=\"btn btn-danger btn-xs btn3d\" id=\"btnLiveWindow\" title=\"Imagem ao vivo window\" > ");
	    contentString.append(" <span class=\"glyphicon glyphicon-camera\"  onclick=\"showCamerasPopUp()\" ></span></button> ");

	    contentString.append(" <button type=\"button\" class=\"btn btn-primary btn-xs btn3d\" id=\"btn5n\" title=\"-5min\" onclick=\"playAllCamTime('-300')\" >-5min</button>");
	    contentString.append(" <button type=\"button\" class=\"btn btn-primary btn-xs btn3d\" id=\"btn3n\" title=\"-3min\" onclick=\"playAllCamTime('-180')\" >-3min</button>");
	    contentString.append(" <button type=\"button\" class=\"btn btn-primary btn-xs btn3d\" id=\"btn2n\" title=\"-2min\" onclick=\"playAllCamTime('-120')\" >-2min</button>");
	    contentString.append(" <button type=\"button\" class=\"btn btn-primary btn-xs btn3d\" id=\"btn1n\" title=\"-1min\" onclick=\"playAllCamTime('-60')\" >-1min</button>");
	    contentString.append(" <button type=\"button\" class=\"btn btn-primary btn-xs btn3d\" id=\"btn1n\" title=\"-30s\" onclick=\"playAllCamTime('-30')\" >-30s</button>");
	    contentString.append(" <button type=\"button\" class=\"btn btn-primary btn-xs btn3d\" id=\"btn1n\" title=\"-10s\" onclick=\"playAllCamTime('-10')\" >-10s</button>");
	    contentString.append(" <button type=\"button\" class=\"btn btn-warning  btn-xs btn3d\" id=\"btn1p\" title=\"Instante Zero\" onclick=\"playAllCamTime('0')\" >Momento zero</button>");
	    contentString.append(" <button type=\"button\" class=\"btn btn-primary btn-xs btn3d\" id=\"btn1n\" title=\"10s\" onclick=\"playAllCamTime('10')\" >10s</button>");
	    contentString.append(" <button type=\"button\" class=\"btn btn-primary btn-xs btn3d\" id=\"btn1n\" title=\"30s\" onclick=\"playAllCamTime('30')\" >30s</button>");
	    contentString.append(" <button type=\"button\" class=\"btn btn-primary btn-xs btn3d\" id=\"btn1p\" title=\"1min\" onclick=\"playAllCamTime('60')\" > 1min</button>");
	    contentString.append(" <button type=\"button\" class=\"btn btn-primary btn-xs btn3d\" id=\"btn2p\" title=\"2min\" onclick=\"playAllCamTime('120')\" > 2min</button>");
	    contentString.append(" <button type=\"button\" class=\"btn btn-primary btn-xs btn3d\" id=\"btn3p\" title=\"3min\" onclick=\"playAllCamTime('180')\"> 3min</button>");
	    contentString.append(" <button type=\"button\" class=\"btn btn-primary btn-xs btn3d\" id=\"btn5p\" title=\"5min\" onclick=\"playAllCamTime('300')\" > 5min</button>");

	    contentString.append(" </div>  ");
	    contentString.append(" <div class=\"col-xs-3 col-lg-3\">  ");
	    contentString.append(" <button type=\"button\"  title=\"Fechar evento sigma\" onclick=\"eventCallHandler('" + this.callEventoVO.getCodigoHistorico()
		    + "', true,true,false)\" class=\"btn btn-danger btn3d\"><span class=\"glyphicon glyphicon-off\"></span> Fechar</button>  ");
	    contentString.append(" <button type=\"button\" title=\"Colocar evento em espera sigma\" onclick=\"eventCallHandler('" + this.callEventoVO.getCodigoHistorico()
		    + "', true,false,false)\" class=\"btn btn-warning btn3d\"><span class=\"glyphicon glyphicon-warning-sign\"></span> Deslocar</button>  ");
	    contentString.append(" <button type=\"button\" title=\"Colocar evento em espera sigma com alteração \" onclick=\"eventCallHandler('" + this.callEventoVO.getCodigoHistorico()
		    + "', true,false,true)\" class=\"btn btn-warning btn3d\"><span class=\"glyphicon glyphicon-warning-sign\"></span> Alt</button>  ");
	    contentString.append(" </div>  ");
	    contentString.append(" <div  class=\"divRegistrarImagens\" align=\"left\"  style=\"margin-left: 17px;\"> ");
	    contentString.append(" <hr>");
	    contentString.append(" Data: <input type=\"date\" id=\"datePicker\"  class=\"date-input\" value=\"\" readonly> ");
	    contentString.append(" Hora: <input id=\"timePicker\" type=\"time\" step=\"1\" class=\"date-input\" value=\"\" readonly> ");
	    contentString.append(" <button type=\"button\" id=\"btnRegistrarImagem\" onclick=\"registrarImagens()\"  class=\"btn btn-primary btn3d \"> <span class=\"glyphicon glyphicon-picture\"></span> Capturar imagens </button> ");
	    contentString.append(" <button type=\"button\" onclick=\"abrirOS("+this.getCallEventoVO().getCodigoCliente()+")\"  class=\"btn btn-success btn3d \"> <span class=\"glyphicon glyphicon-wrench\"></span> Abrir OS </button> ");
	    
	    contentString.append(" <button type=\"button\" style=\"margin-right: 15px;\"  title=\"Tratar sigma\" onclick=\"eventCallHandler('" + this.callEventoVO.getCodigoHistorico()
		    + "', false,true,false)\" class=\"btn btn-primary btn3d pull-right\"><span class=\"glyphicon glyphicon-share-alt\"></span> Tratar Sigma</button>  ");
	    
	    contentString.append(" <button type=\"button\"  onclick=\"salvaLogEventoPopUpFechar('NaoEnviaComunicado')\" class=\"btn btn-danger pull-right btn3d \"> <span class=\"glyphicon glyphicon-ban-circle\"></span> Não enviar </button> ");
	    
	    contentString.append(" <span class=\"duration\" id=\"testeHora\"></span>");
	    contentString.append(" </div> ");
	    contentString.append(" </div>  ");
	    contentString.append(" </div>  ");
	    contentString.append(" </div>  ");
	    contentString
		    .append(" <div id=\"modalInfo\" class=\"easyui-window\" title=\"Informações do cliente\" data-options=\"modal:false,closed:true,resizable:false,collapsible:true,minimizable:false,maximizable:false\" style=\"width:850px;height:360px;font-family: Calibri,verdana,helvetica,arial,sans-serif;font-size:12px;z-index: 5000\">   ");
	    contentString.append(" </div>  ");

	    contentString.append(" </div>  ");
	    
	    
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    this.infoWindow = contentString.toString();
	}

    }

    public String getDataEventoVideo() {
	return dataEventoVideo;
    }

    public void setDataEventoVideo(String dataEventoVideo) {
	this.dataEventoVideo = dataEventoVideo;
    }

    public Integer getMinutosEvento() {
	return minutosEvento;
    }

    public void setMinutosEvento(Integer minutosEvento) {
	this.minutosEvento = minutosEvento;
    }

    public String getCamerasCliente() {
	return camerasCliente;
    }

    public void setCamerasCliente(String camerasCliente) {
	this.camerasCliente = camerasCliente;
    }

}
