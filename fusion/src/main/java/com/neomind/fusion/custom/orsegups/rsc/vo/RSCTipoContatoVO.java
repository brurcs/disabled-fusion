package com.neomind.fusion.custom.orsegups.rsc.vo;

public class RSCTipoContatoVO
{
	private String id;
	private String descricao;
	
	public RSCTipoContatoVO(String id, String descricao)
	{
		super();
		this.id = id;
		this.descricao = descricao;
	}
	
	public String getId()
	{
		return id;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	public String getDescricao()
	{
		return descricao;
	}
	public void setDescricao(String descricao)
	{
		this.descricao = descricao;
	}
	
	@Override
	public String toString()
	{
		return "TipoContatoVO [id=" + id + ", descricao=" + descricao + "]";
	}
	
	
}
