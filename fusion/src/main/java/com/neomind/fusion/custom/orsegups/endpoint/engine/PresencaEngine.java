package com.neomind.fusion.custom.orsegups.endpoint.engine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.endpoint.dto.QLPCoberturaDTO;
import com.neomind.fusion.custom.orsegups.endpoint.dto.QLPTrocaDTO;
import com.neomind.fusion.custom.orsegups.endpoint.dto.RespostaDTO;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoDateUtils;

public class PresencaEngine {

    @SuppressWarnings({ "deprecation" })
    public static Response lancarCobertura(QLPCoberturaDTO c, String operacao) {

	RespostaDTO resposta = new RespostaDTO();
	try{
	    if (c != null) {
		
		boolean motivoObritorio = false;
		
		boolean postoObrigatorio = false;
		
		boolean horarioObrigatorio = false;
		
		if (operacao != null && (operacao.equals("0002") || operacao.equals("0007") || operacao.equals("0002") || operacao.equals("0012") || operacao.equals("0013") || operacao.equals("0014"))) {
		    if (c.getIdMotivo() == 0) {
			resposta.setMensagem("O campo 'idMotivo' é obrigatório para esta operação!");
			return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
		    } else {
			motivoObritorio = true;
		    }
		}
		
		if (operacao != null && (operacao.equals("0004") || operacao.equals("0005") || operacao.equals("0007") || operacao.equals("0009") || operacao.equals("0012") || operacao.equals("0013"))) {
		    if (c.getIdPosto() == 0) {
			resposta.setMensagem("O campo 'idPosto' é obrigatório para esta operação!");
			return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
		    } else {
			postoObrigatorio = true;
		    }
		}
		
		if (operacao != null && operacao.equals("0016")){
		    if (c.getIdHorario() == 0){
			resposta.setMensagem("O campo 'idHorario' é obrigatório para esta operação!");
			return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
		    } else {
			horarioObrigatorio = true;
		    }
		}
		
		if (operacao != null && (operacao.equals("0002")|| operacao.equals("0007")|| operacao.equals("0013")|| operacao.equals("0014"))){
		    if (c.getDataInicio() > c.getDataFim()){
			c.setDataFim(c.getDataFim()+(1000*60*60*24));
		    }
		}
		
		if (c.getColaborador() != null && !c.getColaborador().isEmpty() && c.getDataInicio() != 0 && c.getDataFim() != 0) {
		    
		    String colaborador = c.getColaborador();
		    
		    String[] arrayC = colaborador.split("-");
		    
		    QLGroupFilter groupFilter = new QLGroupFilter("AND");
		    groupFilter.addFilter(new QLEqualsFilter("numemp", Long.parseLong(arrayC[0])));
		    groupFilter.addFilter(new QLEqualsFilter("tipcol", Long.parseLong(arrayC[1])));
		    groupFilter.addFilter(new QLEqualsFilter("numcad", Long.parseLong(arrayC[2])));
		    NeoObject col = PersistEngine.getObject(AdapterUtils.getEntityClass("VETORHUSUFUNFUSION"), groupFilter);
		    
		    if (col == null) {
			resposta.setMensagem("Nenhum colaborador encontrado para o external id informado!");
			return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
		    }
		    
		    EntityWrapper wCol = new EntityWrapper(col);
		    
		    long codigoRegional = (long) wCol.findValue("usu_codreg");
		    
		    NeoObject reg = PersistEngine.getObject(AdapterUtils.getEntityClass("SAPIENSUSUREG"), new QLEqualsFilter("usu_codreg", codigoRegional));
		    
		    NeoObject op = PersistEngine.getObject(AdapterUtils.getEntityClass("QLOperacoes"), new QLEqualsFilter("codigo", operacao));
		    
		    NeoObject colASubs = null;
		    
		    if (!postoObrigatorio && !horarioObrigatorio) {
			if (c.getColaboradorASubstituir() != null && !c.getColaboradorASubstituir().isEmpty()) {
			    String colaboradorSubstituto = c.getColaboradorASubstituir();
			    String[] arrayCS = colaboradorSubstituto.split("-");
			    
			    QLGroupFilter groupFilterColSubs = new QLGroupFilter("AND");
			    groupFilterColSubs.addFilter(new QLEqualsFilter("numemp", Long.parseLong(arrayCS[0])));
			    groupFilterColSubs.addFilter(new QLEqualsFilter("tipcol", Long.parseLong(arrayCS[1])));
			    groupFilterColSubs.addFilter(new QLEqualsFilter("numcad", Long.parseLong(arrayCS[2])));
			    
			    colASubs = PersistEngine.getObject(AdapterUtils.getEntityClass("VETORHUSUFUNFUSION"), groupFilterColSubs);
			    
			    if (colASubs == null) {
				resposta.setMensagem("Nenhum colaborador a substituir encontrado para o external id informado!");
				return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
			    }
			} else {
			    resposta.setMensagem("O campo 'colaboradorASubstituir' é obrigatório para esta operação!");
			    return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
			}
		    }
		    
		    
//		TimeZone tzLocal = TimeZone.getTimeZone("UTC");
		    int aux = 0;
		    if (operacao != null && (operacao.equals("0016") || operacao.equals("0007") || operacao.equals("0012") || operacao.equals("0013"))){
			aux = 2*60*1000;
		    }
		    GregorianCalendar dataInicio = new GregorianCalendar();
//		dataInicio.setTimeZone(tzLocal);
		    dataInicio.setTimeInMillis(c.getDataInicio()+aux);
		    
		    GregorianCalendar dataFim = new GregorianCalendar();
//		dataFim.setTimeZone(tzLocal);
		    dataFim.setTimeInMillis(c.getDataFim()+aux);
		    
		    if (operacao != null && operacao.equals("0003") && dataFim.get(Calendar.HOUR_OF_DAY) == 0){
			dataFim.set(Calendar.HOUR_OF_DAY, 23);
			dataFim.set(Calendar.MINUTE, 59);
			dataFim.set(Calendar.SECOND, 59);
		    }
		    
		    NeoObject pos = null;
		    
		    if (postoObrigatorio) {
			NeoObject nv8 = PersistEngine.getObject(AdapterUtils.getEntityClass("VETORH_USU_Vorg203nv8"), new QLEqualsFilter("numloc", c.getIdPosto()));
			// GCNiveis
			
			if (nv8 == null) {
			    resposta.setMensagem("Nenhum posto encontrado para o id informado!");
			    return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
			} else {
			    pos = PersistEngine.getObject(AdapterUtils.getEntityClass("GCNiveis"), new QLEqualsFilter("nivel8", nv8));
			    if (pos == null){
				pos = getGCNiveis(nv8);
			    }
			}
			
		    }
		    NeoObject hor = null;
		    if (horarioObrigatorio){
			hor = PersistEngine.getObject(AdapterUtils.getEntityClass("VETORH_R004HOR"), new QLEqualsFilter("codhor", c.getIdHorario()));
			if (hor == null) {
			    resposta.setMensagem("Nenhum horário encontrado para o id informado!");
			    return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
			}
		    }
		    
		    NeoObject mt = null;
		    
		    if (motivoObritorio) {
			String motivo = "";
			
			if (c.getIdMotivo() < 10) {
			    motivo = "000" + c.getIdMotivo();
			} else if (c.getIdMotivo() < 100) {
			    motivo = "00" + c.getIdMotivo();
			} else {
			    motivo = "0" + c.getIdMotivo();
			}
			
			mt = PersistEngine.getObject(AdapterUtils.getEntityClass("QLMotivo"), new QLEqualsFilter("codigo", motivo));
			
			if (mt == null) {
			    resposta.setMensagem("Nenhum motivo encontrado para o 'idMotivo' informado!");
			    return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
			}
			
		    }
		    
		    InstantiableEntityInfo insQLP = AdapterUtils.getInstantiableEntityInfo("QLExecucao");
		    NeoObject objQLP = insQLP.createNewInstance();
		    
		    EntityWrapper wQLP = new EntityWrapper(objQLP);
		    
		    wQLP.setValue("regional", reg);
		    wQLP.setValue("operacao", op);
		    wQLP.setValue("colaborador", col);
		    wQLP.setValue("colaboradorASubstituir", colASubs);
		    wQLP.setValue("data", dataInicio);
		    wQLP.setValue("dataFinal", dataFim);
		    
		    wQLP.setValue("motivo", mt);
		    wQLP.setValue("posto", pos);
		    wQLP.setValue("horario", hor);
		    
		    wQLP.setValue("isProcessoAutomatico", true);
		    
		    wQLP.setValue("nextid", c.getIdNexti());
		    
		    String obs = "Operação iniciada via Nexti. ";
		    
		    if (c.getObservacao() != null) {
			obs += c.getObservacao();
		    }
		    
		    if (obs.length() > 2000) {
			wQLP.setValue("observacao", obs.substring(0, 1999));
		    } else {
			wQLP.setValue("observacao", obs);
		    }
		    
		    // Inicio - Start processo
		    QLEqualsFilter equal = new QLEqualsFilter("Name", "R030 - QLP - Quadro de Lotação Presença");
		    ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);
		    
		    // TODO ALTERAR PARA NICOLAS.SILVA
		    NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "beatriz.malmann"));
		    
		    PortalUtil.setCurrentUser(solicitante);
		    
		    final WFProcess processo = processModel.startProcess(objQLP, false, null, null, null, null, solicitante);
		    processo.setSaved(true);
		    PersistEngine.persist(processo);
		    
		    PersistEngine.commit(true);
		    
		    try {
		    	new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividadePresencaEngine(solicitante, true);
		    } catch (Exception e) {
		    	e.printStackTrace();
			
		    	resposta.setMensagem("Erro ao avançar a primeira atividade do processo. Tarefa salva na primeira atividade.");

		    	if (e instanceof WorkflowException)
		    	{
		    		if (((WorkflowException) e).getErrorList().size() > 0)
		    		{
		    			resposta.setMensagem(resposta.getMensagem() + "\n " + ((WorkflowException) e).getErrorList().get(0).getI18nMessage());
		    			System.out.println("#PresencaEngine "+((WorkflowException) e).getErrorList().get(0).getI18nMessage());
		    		}
		    	}

		    	return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
		    }
		    // FIM - Start processo
		    
		    String retorno = processo.getCode();
		    ;
		    
		    if (retorno.contains("Erro")) {
			resposta.setMensagem(retorno);
			return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
		    } else {
			resposta.setMensagem("Sucesso");
			resposta.setTarefa(retorno);
			return Response.ok(resposta, MediaType.APPLICATION_JSON_TYPE).build();
		    }
		    
		} else {
		    resposta.setMensagem("Os campos 'colaborador','dataInicio' e 'dataFim' são obrigatórios!");
		    return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
		}
	    } else {
		resposta.setMensagem("Corpo da requisição não pode ser nulo!");
		return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
	    }
	} catch (Exception e){
	    resposta.setMensagem("Message: "+e.getMessage());
	    e.printStackTrace();
	    return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
	}

    }

    public static Response removerCoberturaColaborador(QLPCoberturaDTO c) {

	RespostaDTO retorno = new RespostaDTO();

	String col[] = c.getColaborador().split("-");

	String numEmp = col[0];
	String tipCol = col[1];
	String numCad = col[2];
	
	if (c.getColaboradorASubstituir() == null || c.getColaboradorASubstituir().isEmpty()){
	    c.setColaboradorASubstituir(" - - - ");
	}
	String colCob[] = c.getColaboradorASubstituir().split("-");

	String numEmpCob = colCob[0].trim();
	String tipColCob = colCob[1].trim();
	String numCadCob = colCob[2].trim();

//	TimeZone tzLocal = TimeZone.getTimeZone("UTC");
	
	GregorianCalendar data = new GregorianCalendar();
//	data.setTimeZone(tzLocal);
	data.setTimeInMillis(c.getDataInicio());

	String dataInicial = NeoDateUtils.safeDateFormat(data, "dd/MM/yyyy");

	int horaInicial = data.get(Calendar.HOUR_OF_DAY) * 60;
	horaInicial += data.get(Calendar.MINUTE);

	String urlString = PortalUtil.getBaseURL() + "servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet?action=deleteRegistroCobertura&numEmp=" + numEmp + "&tipCol=" + tipCol + "&numCad=" + numCad + "&dataInicial=" + dataInicial + "&horaInicial=" + horaInicial + "&numEmpCob="
		+ numEmpCob + "&tipColCob=" + tipColCob + "&numCadCob=" + numCadCob;

	String resposta = "";
	BufferedReader in = null;

	try {

	    URL url = new URL(urlString);
	    URLConnection conn = url.openConnection();

	    in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	    String inputLine;

	    while ((inputLine = in.readLine()) != null) {
		resposta = inputLine;
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    if (in != null) {
		try {
		    in.close();
		} catch (IOException e) {
		    e.printStackTrace();
		}
	    }
	}

	if (resposta.contains("Erro")) {
	    retorno.setMensagem("Erro! Tente novamente.");
	    return Response.status(Response.Status.BAD_REQUEST).entity(retorno).type(MediaType.APPLICATION_JSON_TYPE).build();
	} else {
	    retorno.setMensagem("Sucesso");
	    return Response.status(Response.Status.OK).entity(retorno).type(MediaType.APPLICATION_JSON_TYPE).build();
	}

    }
    
    public static Response removerTrocaEscala(QLPTrocaDTO troca) {

   	RespostaDTO retorno = new RespostaDTO();

   	String col[] = troca.getColaborador().split("-");

   	String numEmp = col[0];
   	String tipCol = col[1];
   	String numCad = col[2];
   	
   	QLGroupFilter groupFilter = new QLGroupFilter("AND");
	groupFilter.addFilter(new QLEqualsFilter("numemp", Long.parseLong(numEmp)));
	groupFilter.addFilter(new QLEqualsFilter("tipcol", Long.parseLong(tipCol)));
	groupFilter.addFilter(new QLEqualsFilter("numcad", Long.parseLong(numCad)));
	NeoObject colaborador = PersistEngine.getObject(AdapterUtils.getEntityClass("VETORHUSUFUNFUSION"), groupFilter);

	if (colaborador == null) {
	    retorno.setMensagem("Nenhum colaborador encontrado para o external id informado!");
	    return Response.status(Response.Status.BAD_REQUEST).entity(retorno).type(MediaType.APPLICATION_JSON_TYPE).build();
	}

	EntityWrapper wCol = new EntityWrapper(colaborador);

	long numCpf = (long) wCol.findValue("numcpf");
   	
   	GregorianCalendar data = new GregorianCalendar();
   	data.setTimeInMillis(troca.getData());

   	String dataInicial = NeoDateUtils.safeDateFormat(data, "dd/MM/yyyy");

   	String urlString = PortalUtil.getBaseURL() + "servlet/com.neomind.fusion.custom.orsegups.ql.servlet.QlServlet?action=deleteHistoricoEscala&numemp="+numEmp+"&tipcol="+tipCol+"&numcad="+numCad+"&numcpf="+numCpf+"&datalt="+dataInicial;

   	String resposta = "";
   	BufferedReader in = null;

   	try {

   	    URL url = new URL(urlString);
   	    URLConnection conn = url.openConnection();

   	    in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
   	    String inputLine;

   	    while ((inputLine = in.readLine()) != null) {
   		resposta = inputLine;
   	    }
   	} catch (Exception e) {
   	    e.printStackTrace();
   	} finally {
   	    if (in != null) {
   		try {
   		    in.close();
   		} catch (IOException e) {
   		    e.printStackTrace();
   		}
   	    }
   	}

   	if (resposta.contains("Erro")) {
   	    retorno.setMensagem("Erro! Tente novamente.");
   	    return Response.status(Response.Status.BAD_REQUEST).entity(retorno).type(MediaType.APPLICATION_JSON_TYPE).build();
   	} else {
   	    retorno.setMensagem("Sucesso");
   	    return Response.status(Response.Status.OK).entity(retorno).type(MediaType.APPLICATION_JSON_TYPE).build();
   	}

       }
    
    @SuppressWarnings({ "deprecation" })
    public static Response lancarTroca(QLPTrocaDTO c, String operacao) {

	RespostaDTO resposta = new RespostaDTO();

	if (c != null) {

	    boolean postoObrigatorio = false;
	    
	    boolean escalaTurmaObrigatorios = false;
	    
	    if (operacao != null && (operacao.equals("0004"))) {
		if (c.getIdPosto() == 0) {
		    resposta.setMensagem("O campo 'idPosto' é obrigatório para esta operação!");
		    return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
		} else {
		    postoObrigatorio = true;
		}
	    }
	    
	    if (operacao != null && (operacao.equals("0008"))) {
		if (c.getIdEscala() == 0 || c.getIdTurma() == 0) {
		    resposta.setMensagem("Os campos 'idEscala' e 'idTurma' são obrigatórios para esta operação!");
		    return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
		} else {
		    escalaTurmaObrigatorios = true;
		}
	    }
	    
	    if (c.getColaborador() != null && !c.getColaborador().isEmpty() && c.getData() != 0) {

		String colaborador = c.getColaborador();

		String[] arrayC = colaborador.split("-");

		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		groupFilter.addFilter(new QLEqualsFilter("numemp", Long.parseLong(arrayC[0])));
		groupFilter.addFilter(new QLEqualsFilter("tipcol", Long.parseLong(arrayC[1])));
		groupFilter.addFilter(new QLEqualsFilter("numcad", Long.parseLong(arrayC[2])));
		NeoObject col = PersistEngine.getObject(AdapterUtils.getEntityClass("VETORHUSUFUNFUSION"), groupFilter);

		if (col == null) {
		    resposta.setMensagem("Nenhum colaborador encontrado para o external id informado!");
		    return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
		}

		EntityWrapper wCol = new EntityWrapper(col);

		long codigoRegional = (long) wCol.findValue("usu_codreg");

		NeoObject reg = PersistEngine.getObject(AdapterUtils.getEntityClass("SAPIENSUSUREG"), new QLEqualsFilter("usu_codreg", codigoRegional));

		NeoObject op = PersistEngine.getObject(AdapterUtils.getEntityClass("QLOperacoes"), new QLEqualsFilter("codigo", operacao));

		NeoObject colASubs = null;

//		TimeZone tzLocal = TimeZone.getTimeZone("UTC");
		GregorianCalendar dataInicio = new GregorianCalendar();
//		dataInicio.setTimeZone(tzLocal);
		dataInicio.setTimeInMillis(c.getData());
		

		NeoObject pos = null;

		if (postoObrigatorio) {
		    NeoObject nv8 = PersistEngine.getObject(AdapterUtils.getEntityClass("VETORH_USU_Vorg203nv8"), new QLEqualsFilter("numloc", c.getIdPosto()));
		    // GCNiveis

		    if (nv8 == null) {
			resposta.setMensagem("Nenhum posto encontrado para o id informado!");
			return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
		    } else {
			pos = PersistEngine.getObject(AdapterUtils.getEntityClass("GCNiveis"), new QLEqualsFilter("nivel8", nv8));
			if (pos == null){
			    pos = getGCNiveis(nv8);
			}
		    }

		}
		
		NeoObject esc = null;
		if (escalaTurmaObrigatorios){
		    NeoObject escExt = PersistEngine.getObject(AdapterUtils.getEntityClass("VETORH_R006ESC"), new QLEqualsFilter("codesc", c.getIdEscala()));
		    if (escExt == null){
			resposta.setMensagem("Nenhuma escala encontrada para o id informado!");
			return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
		    }
		    NeoObject turExt = PersistEngine.getObject(AdapterUtils.getEntityClass("VETORHRTMA"), new QLEqualsFilter("codtma", c.getIdTurma()));
		    if (turExt == null){
			resposta.setMensagem("Nenhuma turma encontrada para o id informado!");
			return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
		    }
		    InstantiableEntityInfo insEsc = AdapterUtils.getInstantiableEntityInfo("QLEscala");
		    esc = insEsc.createNewInstance();

		    EntityWrapper wEsc = new EntityWrapper(esc);
		    wEsc.setValue("escalaHor", escExt);
		    wEsc.setValue("turma", turExt);
		}

		InstantiableEntityInfo insQLP = AdapterUtils.getInstantiableEntityInfo("QLExecucao");
		NeoObject objQLP = insQLP.createNewInstance();

		EntityWrapper wQLP = new EntityWrapper(objQLP);

		wQLP.setValue("regional", reg);
		wQLP.setValue("operacao", op);
		wQLP.setValue("colaborador", col);
		wQLP.setValue("colaboradorASubstituir", colASubs);
		wQLP.setValue("data", dataInicio);

		wQLP.setValue("posto", pos);
		wQLP.setValue("escala", esc);

		wQLP.setValue("isProcessoAutomatico", true);
		
		wQLP.setValue("nextid", c.getIdNexti());

		String obs = "Operação iniciada via Nexti. ";

		if (c.getObservacao() != null) {
		    obs += c.getObservacao();
		}

		if (obs.length() > 2000) {
		    wQLP.setValue("observacao", obs.substring(0, 1999));
		} else {
		    wQLP.setValue("observacao", obs);
		}

		// Inicio - Start processo
		QLEqualsFilter equal = new QLEqualsFilter("Name", "R030 - QLP - Quadro de Lotação Presença");
		ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);

		NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "beatriz.malmann"));

		PortalUtil.setCurrentUser(solicitante);

		final WFProcess processo = processModel.startProcess(objQLP, false, null, null, null, null, solicitante);
		processo.setSaved(true);
		PersistEngine.persist(processo);

		PersistEngine.commit(true);

		try {
		    new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividadePresencaEngine(solicitante, true);
		} catch (Exception e) {
		    e.printStackTrace();
		}
		// FIM - Start processo

		String retorno = processo.getCode();

		if (retorno.contains("Erro")) {
		    resposta.setMensagem(retorno);
		    return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
		} else {
		    resposta.setMensagem("Sucesso");
		    resposta.setTarefa(retorno);
		    return Response.ok(resposta, MediaType.APPLICATION_JSON_TYPE).build();
		}

	    } else {
		resposta.setMensagem("Os campos 'colaborador' e 'data' são obrigatórios!");
		return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
	    }
	} else {
	    resposta.setMensagem("Corpo da requisição não pode ser nulo!");
	    return Response.status(Response.Status.BAD_REQUEST).entity(resposta).type(MediaType.APPLICATION_JSON_TYPE).build();
	}
	

    }
    
    public static NeoObject getGCNiveis(NeoObject nv8){
	NeoObject nv7 = getNivel(nv8, "VETORH_USU_Vorg203nv7");
	if (nv7 == null){
	    return nv7;
	}
	NeoObject nv6 = getNivel(nv7, "VETORH_USU_Vorg203nv6");
	if (nv6 == null){
	    return nv6;
	}
	NeoObject nv5 = getNivel(nv6, "VETORH_USU_Vorg203nv5");
	if (nv5 == null){
	    return nv5;
	}
	NeoObject nv4 = getNivel(nv5, "VETORH_USU_Vorg203nv4");
	if (nv4 == null){
	    return nv4;
	}
	NeoObject nv3 = getNivel(nv4, "VETORH_USU_Vorg203nv3");
	if (nv3 == null){
	    return nv3;
	}
	NeoObject nv2 = getNivel(nv3, "VETORH_USU_Vorg203nv2");
	if (nv2 == null){
	    return nv2;
	}
	
	InstantiableEntityInfo insGC = AdapterUtils.getInstantiableEntityInfo("GCNiveis");
	NeoObject gcNiveis = insGC.createNewInstance();;
	EntityWrapper wGcNiveis = new EntityWrapper(gcNiveis);
	wGcNiveis.setValue("nivel8", nv8);
	wGcNiveis.setValue("nivel7", nv7);
	wGcNiveis.setValue("nivel6", nv6);
	wGcNiveis.setValue("nivel5", nv5);
	wGcNiveis.setValue("nivel4", nv4);
	wGcNiveis.setValue("nivel3", nv3);
	wGcNiveis.setValue("nivel2", nv2);
	return gcNiveis;
    }
    
    private static NeoObject getNivel(NeoObject nv, String tabela){
	EntityWrapper wrapper = new EntityWrapper(nv);
	if (wrapper != null){
	    String codPai = (String) wrapper.findValue("codpai");
	    if (codPai != null){
		return PersistEngine.getObject(AdapterUtils.getEntityClass(tabela), new QLEqualsFilter("codnivel", codPai));
	    }
	}
	return null;
    }
    
}
