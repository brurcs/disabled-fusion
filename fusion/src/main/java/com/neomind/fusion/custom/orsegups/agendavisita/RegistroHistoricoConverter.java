package com.neomind.fusion.custom.orsegups.agendavisita;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.util.NeoUtils;

//com.neomind.fusion.custom.orsegups.agendavisita.RegistroHistoricoConverter
public class RegistroHistoricoConverter extends StringConverter {
	
	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin)
	{
		
		Long idPai = field.getForm().getObjectId();
		NeoObject tarefa = PersistEngine.getNeoObject(idPai);
		EntityWrapper wrapper = new EntityWrapper(tarefa);
		
		List<NeoObject> registrosHistorico = (List<NeoObject>) wrapper.findValue("registroHistorico");
		StringBuilder textoTable = new StringBuilder();
		
		if (NeoUtils.safeIsNotNull(registrosHistorico) && !registrosHistorico.isEmpty())
		{
			textoTable.append("		<table  class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"70%\">");
			textoTable.append("			<tr style=\"cursor: auto\">");
			textoTable.append("				<th style=\"cursor: auto\">Data</th>");
			textoTable.append("				<th style=\"cursor: auto\">Usuário</th>");
			textoTable.append("				<th style=\"cursor: auto\">Ação</th>");
			textoTable.append("				<th style=\"cursor: auto; white-space: normal\">Descrição</th>");
			textoTable.append("			</tr>");
			textoTable.append("			<tbody>");	
			
			for (NeoObject obj : registrosHistorico)
			{
				EntityWrapper registroWrapper = new EntityWrapper(obj);
				GregorianCalendar data = (GregorianCalendar) registroWrapper.findValue("dataRegistro");
				String usuario = (String) registroWrapper.findValue("usuario");
				String acao = (String) registroWrapper.findValue("acao");
				String descricao = (String) registroWrapper.findValue("obs2");
				
				textoTable.append("		<tr>");
				textoTable.append("			<td>" + NeoUtils.safeDateFormat(data, "dd/MM/yyyy") + "</td>");
				textoTable.append("			<td>" + usuario + "</td>");
				textoTable.append("			<td>" + acao + "</td>");
				textoTable.append("			<td style=\"white-space: normal\">" + descricao + "</td>");
				textoTable.append("		</tr>");
			}
			
			textoTable.append("			</tbody>");
			textoTable.append("		</table>");
		}
		
		return textoTable.toString();
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}
}
