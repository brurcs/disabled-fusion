package com.neomind.fusion.custom.orsegups.adapter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.site.vo.SiteClienteVO;
import com.neomind.fusion.doc.NeoDocument;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class SiteSalvaArquivosCliente implements AdapterInterface
{

	private Map<String, NeoDocument> documents = new HashMap<String, NeoDocument>();

	@SuppressWarnings("unchecked")
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{

		NeoObject empresa = (NeoObject) processEntity.findValue("empresa");
		NeoObject filial = (NeoObject) processEntity.findValue("filial");
		boolean clienteNull= false;
		Collection<NeoObject> clientes = null;
		Collection<NeoObject> documentos = null;
		clientes = (Collection<NeoObject>) processEntity.findValue("clientes");
		documentos = (Collection<NeoObject>) processEntity.findValue("documentos");
		if (NeoUtils.safeIsNull(clientes) || clientes.isEmpty())
		{
			clienteNull = true;
			processEntity.findField("clientes").setValue(listaClientesEmpresa(empresa,filial));
			clientes = (Collection<NeoObject>) processEntity.findValue("clientes");

		}
		if (clientes != null && clientes.size() > 0)
		{
			for (NeoObject cliente : clientes)
			{
				

				if (documentos != null && documentos.size() > 0)
				{
					for (NeoObject documento : documentos)
					{
						try
						{

							this.publishDocuments(documento, cliente, clienteNull);
						}
						catch (IOException e)
						{

							e.printStackTrace();
							throw new WorkflowException("Erro durante a publicação do arquivo! " + e.getMessage());

						}
					}

				}
				if(clienteNull)
					break;
			}
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	private void publishDocuments(NeoObject documentos, NeoObject cliente , boolean clienteNull) throws IOException
	{
		String pasta = null;
		if (documentos != null)
		{
			EntityWrapper documentoWrapper = new EntityWrapper(documentos);

			Collection<File> arqFiles = new ArrayList<File>();
			String path = null;
			path = (String) documentoWrapper.findValue("siteParametrosUploadCliente.caminhoArquivos");

			if (path != null)
			{
				File folder = new File(path);

				if (!folder.exists())
				{
					throw new WorkflowException("Caminho informado não existe!");
				}

				if (folder.isDirectory())
				{
					DecimalFormat df = new DecimalFormat("00");

					EntityWrapper clienteWrapper = new EntityWrapper(cliente);

					String empresaStr = df.format(clienteWrapper.findValue("usu_codemp"));
					String filialStr = String.valueOf(clienteWrapper.findValue("usu_codfil"));
					Long cli = (Long) clienteWrapper.findValue("cgccpf");

					String cgcCpfStr = null;
//					if (cli <= 11)
//						cgcCpfStr = String.format("%011d", cli);
//					else if (cli > 11)
//						cgcCpfStr = String.format("%014d", cli);
					cgcCpfStr = String.valueOf(cli);
					String clienteStr = String.valueOf(clienteWrapper.findValue("codcli"));
					String tipDoc = (String) documentoWrapper.findValue("tipoDocumentoCliente.decricaoTipoDocumento");

					String mes = df.format(documentoWrapper.findValue("siteParametrosUploadCliente.mesCom"));
					String ano = String.valueOf(documentoWrapper.findValue("siteParametrosUploadCliente.anoCom"));
					String arq = null;
					if(!clienteNull){
					pasta = File.separator + empresaStr + File.separator + filialStr + File.separator + cgcCpfStr + File.separator + tipDoc + File.separator + mes.concat(ano);
					
					}else{
					pasta = File.separator + empresaStr + File.separator + filialStr + File.separator + "TODAS" + File.separator + tipDoc + File.separator + mes.concat(ano);
					
					}
					arq = tipDoc.concat("_") + cgcCpfStr.concat("_") + mes.concat(ano);
					File diretorio = new File("X:\\Site\\Orsegups" + pasta);
					File[] arquivos = folder.listFiles();
				
					for (File arquivo : arquivos)
					{

						if (arquivo.isFile() && !arquivo.isHidden())
						{
							String timeMills = String.valueOf(new GregorianCalendar().getTimeInMillis());
							File arquivoNovo = new File(diretorio, arq.concat("_"+String.valueOf(timeMills)).concat(".pdf"));
							File source = arquivo;
							File destination = diretorio;

							if (!destination.exists())
							{
								destination.mkdirs();
							}
							if (!arquivoNovo.exists())
							{
								arquivoNovo.createNewFile();
							}

							FileChannel sourceChannel = null;
							FileChannel destinationChannel = null;

							try
							{
								sourceChannel = new FileInputStream(source).getChannel();
								destinationChannel = new FileOutputStream(arquivoNovo).getChannel();
								sourceChannel.transferTo(0, sourceChannel.size(), destinationChannel);
								//documentoWrapper.findField("siteParametrosUploadCliente.caminhoArquivos").setValue(arquivoNovo.getAbsolutePath());
							}
							catch (Exception e)
							{
								e.printStackTrace();
								throw new WorkflowException("Erro ao executar caminho e destino do arquivo! " + e.getMessage());
							}
							finally
							{
								if (sourceChannel != null && sourceChannel.isOpen())
									sourceChannel.close();
								if (destinationChannel != null && destinationChannel.isOpen())
									destinationChannel.close();
								//PersistEngine.persist(documentos);
							}

						}
						

					}
				}
				else
				{
					throw new WorkflowException("Caminho informado não corresponde a uma pasta!");
				}
			}
			else
			{
				throw new WorkflowException("Caminho informado inválido!");
			}
		}

	}
	// Retirar metodo!!!
	private Collection<NeoObject> listaClientesEmpresa(NeoObject empresa, NeoObject filial)
	{

		EntityWrapper empresaWrapper = new EntityWrapper(empresa);
		EntityWrapper filialWrapper = new EntityWrapper(filial);
		Long emp = (Long) empresaWrapper.findValue("codemp");
		Long fil = (Long)filialWrapper.findValue("codfil");
		QLGroupFilter filter = new QLGroupFilter("AND");
		filter.addFilter(new QLEqualsFilter("usu_codemp", emp));
		filter.addFilter(new QLEqualsFilter("usu_codfil", fil));

		//Busca Cliente
		List<NeoObject> cliensteListObj = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUVCLIEMP"), filter);

		return cliensteListObj;
	}

	// retornar o documento para o metodo acima e só então usar PersistEngine.persist(documentos);

}
