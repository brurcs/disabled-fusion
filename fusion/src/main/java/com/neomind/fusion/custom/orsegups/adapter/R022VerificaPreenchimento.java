package com.neomind.fusion.custom.orsegups.adapter;

import java.util.Collection;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;
//com.neomind.fusion.custom.orsegups.adapter.R022VerificaPreenchimento
public class R022VerificaPreenchimento implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		try{
			if (NeoUtils.safeOutputString(wrapper.findValue("txtobs")).equals("") || NeoUtils.safeOutputString(wrapper.findValue("txtobs")).trim().equals("Por qual motivo este funcionário fez horas extras?")){
				throw new WorkflowException("Favor preencher o motivo das horas extras.");
			}
		}catch(Exception e){
			throw new WorkflowException("Erro na validação de Preenchimento. msg: "+e);
		}
		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub
		
	}


}
