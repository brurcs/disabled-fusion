package com.neomind.fusion.custom.orsegups.fulltrack.bean;


public class FulltrackVeiculo {
    
    private int ras_vei_id;
    private String ras_vei_id_cli;
    private String ras_vei_placa;
    private String ras_vei_veiculo;
    private String ras_vei_chassi;
    private String ras_vei_ano;
    private String ras_vei_cor;
    private String ras_vei_tipo;
    private String ras_vei_fabricante;
    private String ras_vei_modelo;
    private String ras_vei_combustivel;
    private String ras_vei_consumo;
    private String ras_vei_velocidade_limite;
    private String ras_vei_odometro;
    private String ras_vei_data_cadastro;
    private String ras_vei_data_ult_alt;
    private String ras_vei_equipamento;
    private String ras_vei_vin;
    
    public int getRas_vei_id() {
        return ras_vei_id;
    }
    public void setRas_vei_id(int ras_vei_id) {
        this.ras_vei_id = ras_vei_id;
    }
    public String getRas_vei_id_cli() {
        return ras_vei_id_cli;
    }
    public void setRas_vei_id_cli(String ras_vei_id_cli) {
        this.ras_vei_id_cli = ras_vei_id_cli;
    }
    public String getRas_vei_placa() {
        return ras_vei_placa;
    }
    public void setRas_vei_placa(String ras_vei_placa) {
        this.ras_vei_placa = ras_vei_placa;
    }
    public String getRas_vei_veiculo() {
        return ras_vei_veiculo;
    }
    public void setRas_vei_veiculo(String ras_vei_veiculo) {
        this.ras_vei_veiculo = ras_vei_veiculo;
    }
    public String getRas_vei_chassi() {
        return ras_vei_chassi;
    }
    public void setRas_vei_chassi(String ras_vei_chassi) {
        this.ras_vei_chassi = ras_vei_chassi;
    }
    public String getRas_vei_ano() {
        return ras_vei_ano;
    }
    public void setRas_vei_ano(String ras_vei_ano) {
        this.ras_vei_ano = ras_vei_ano;
    }
    public String getRas_vei_cor() {
        return ras_vei_cor;
    }
    public void setRas_vei_cor(String ras_vei_cor) {
        this.ras_vei_cor = ras_vei_cor;
    }

    public String getRas_vei_modelo() {
        return ras_vei_modelo;
    }
    public void setRas_vei_modelo(String ras_vei_modelo) {
        this.ras_vei_modelo = ras_vei_modelo;
    }
 
    public String getRas_vei_tipo() {
        return ras_vei_tipo;
    }
    public void setRas_vei_tipo(String ras_vei_tipo) {
        this.ras_vei_tipo = ras_vei_tipo;
    }
    public String getRas_vei_fabricante() {
        return ras_vei_fabricante;
    }
    public void setRas_vei_fabricante(String ras_vei_fabricante) {
        this.ras_vei_fabricante = ras_vei_fabricante;
    }
    public String getRas_vei_combustivel() {
        return ras_vei_combustivel;
    }
    public void setRas_vei_combustivel(String ras_vei_combustivel) {
        this.ras_vei_combustivel = ras_vei_combustivel;
    }
    public String getRas_vei_consumo() {
        return ras_vei_consumo;
    }
    public void setRas_vei_consumo(String ras_vei_consumo) {
        this.ras_vei_consumo = ras_vei_consumo;
    }
    public String getRas_vei_velocidade_limite() {
        return ras_vei_velocidade_limite;
    }
    public void setRas_vei_velocidade_limite(String ras_vei_velocidade_limite) {
        this.ras_vei_velocidade_limite = ras_vei_velocidade_limite;
    }
    public String getRas_vei_odometro() {
        return ras_vei_odometro;
    }
    public void setRas_vei_odometro(String ras_vei_odometro) {
        this.ras_vei_odometro = ras_vei_odometro;
    }
    public String getRas_vei_data_cadastro() {
        return ras_vei_data_cadastro;
    }
    public void setRas_vei_data_cadastro(String ras_vei_data_cadastro) {
        this.ras_vei_data_cadastro = ras_vei_data_cadastro;
    }
    public String getRas_vei_data_ult_alt() {
        return ras_vei_data_ult_alt;
    }
    public void setRas_vei_data_ult_alt(String ras_vei_data_ult_alt) {
        this.ras_vei_data_ult_alt = ras_vei_data_ult_alt;
    }
    public String getRas_vei_equipamento() {
        return ras_vei_equipamento;
    }
    public void setRas_vei_equipamento(String ras_vei_equipamento) {
        this.ras_vei_equipamento = ras_vei_equipamento;
    }
    public String getRas_vei_vin() {
        return ras_vei_vin;
    }
    public void setRas_vei_vin(String ras_vei_vin) {
        this.ras_vei_vin = ras_vei_vin;
    }
    
    

}
