package com.neomind.fusion.custom.orsegups.adapter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class TarefaSimplesEscalarNivel implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(TarefaSimplesEscalarNivel.class);

	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		String erro = "Por favor, contatar o administrador do sistema!";
		WorkflowException exception = null;
		try
		{
			if(origin.getUser().getGroup().getUpperLevel().getResponsible() != null)
			{
				wrapper.setValue("ResponsavelExecutor", origin.getUser().getGroup().getUpperLevel().getResponsible());
			}
			else
			{
				erro = "Não foi possível definir o Responsável Superior do Executor (" + origin.getUser() + ") da tarefa: " + origin.getCode();
				log.error(erro);
			}
		}
		catch(Exception e)
		{
			log.error(erro);
			e.printStackTrace();
			throw new WorkflowException(erro);
		}
	}

	public void back(EntityWrapper wrapper, Activity activity)
	{
	}
}
