package com.neomind.fusion.custom.orsegups.presenca.vo;

public class ContaSigmaVinculoHumanaVO {
	
	String numctr;
	String numposto;
	String cdCliente;
	String idCentral;
	String linkExcluir;
	
	public String getNumctr() {
		return numctr;
	}
	public void setNumctr(String numctr) {
		this.numctr = numctr;
	}
	public String getNumposto() {
		return numposto;
	}
	public void setNumposto(String numposto) {
		this.numposto = numposto;
	}
	public String getCdCliente() {
		return cdCliente;
	}
	public void setCdCliente(String cdCliente) {
		this.cdCliente = cdCliente;
	}
	public String getIdCentral() {
		return idCentral;
	}
	public void setIdCentral(String idCentral) {
		this.idCentral = idCentral;
	}
	public String getLinkExcluir() {
		return linkExcluir;
	}
	public void setLinkExcluir(String linkExcluir) {
		this.linkExcluir = linkExcluir;
	}
	
	
	

}
