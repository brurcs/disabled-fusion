package com.neomind.fusion.custom.orsegups.presenca.vo;

public class StatusVO
{
	private String codigo;
	private String descricao;
	private String status;
	
	public String getCodigo()
	{
		return codigo;
	}
	public void setCodigo(String codigo)
	{
		this.codigo = codigo;
	}
	public String getDescricao()
	{
		return descricao;
	}
	public void setDescricao(String descricao)
	{
		this.descricao = descricao;
	}
	
	public String getImageTag() {
		return "<a title='(" + this.getCodigo() + ") " + this.getDescricao() + "' class='easyui-tooltip'><img src='imagens/custom/" + this.getStatus() + ".png' /></a>";
	}
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
	public void setStatusOnline() {
		this.setStatus("online");
	}
	public void setStatusOffline() {
		this.setStatus("offline");
	}
	public void setStatusNa() {
		this.setStatus("na");
	}
	public boolean isStatusOnline() {
		return this.getStatus().equals("online");
	}
	public boolean isStatusOffline() {
		return this.getStatus().equals("offline");
	}
	public boolean isStatusNa() {
		return this.getStatus().equals("na");
	}
	public boolean isStatusCobertura() {
		return this.getStatus().equals("cobertura");
	}
	public void setStatusCobertura() {
		this.setStatus("cobertura");
	}
	@Override
	public String toString()
	{
		return "StatusVO [codigo=" + codigo + ", descricao=" + descricao + ", status=" + status + "]";
	}

}