package com.neomind.fusion.custom.orsegups.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

@WebServlet(name="ListJsonServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.servlets.ListJsonServlet"})
public class ListJsonServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	private static final Log log = LogFactory.getLog(ListJsonServlet.class);
	EntityManager entityManager;
	EntityTransaction transaction;

	private void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		try
		{
			response.setContentType("text/plain");
			response.setCharacterEncoding("ISO-8859-1");

			final PrintWriter out = response.getWriter();
			Gson gson = new Gson();
			String action = "";
			if (request.getParameter("action") != null)
			{
				action = request.getParameter("action");
			}
			if (action.contains("listaEmpresas"))
			{
				this.listaEmpresas(response);
			}
			if (action.contains("listaInstaladores"))
			{
				this.listaInstaladores(response);
			}
			if (action.contains("listaColaboradoresCerec"))
			{
				this.listaColaboradoresCerec(response);
			}
			out.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void listaEmpresas(HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();

		List<NeoObject> empresas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("EEMP"));

		Long numEmp = 0L;
		String nomEmp = "";

		JSONArray jsonCompetencias = new JSONArray();

		for (NeoObject neoEmpresa : empresas)
		{
			EntityWrapper empresasWrapper = new EntityWrapper(neoEmpresa);
			numEmp = (Long) empresasWrapper.findField("codemp").getValue();
			nomEmp = (String) empresasWrapper.findField("nomemp").getValue();
			boolean valida = (numEmp.intValue() < 30 && !nomEmp.contains("INATIVO"));
			if (valida)
			{
				JSONObject jsonCompetencia = new JSONObject();
				jsonCompetencia.put("codemp", String.valueOf(numEmp));
				jsonCompetencia.put("nomemp", nomEmp);
				jsonCompetencias.put(jsonCompetencia);
			}
		}

		out.print(jsonCompetencias);
		out.flush();
		out.close();
	}

	public void listaInstaladores(HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();

		List<NeoObject> instaladores = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUTINS"));

		Long codIns = 0L;
		String nomIns = "";
		String tipIns = "";

		JSONArray jsonInstaladores = new JSONArray();

		for (NeoObject neoInstaladores : instaladores)
		{
			EntityWrapper instaladoresWrapper = new EntityWrapper(neoInstaladores);
			codIns = (Long) instaladoresWrapper.findField("usu_codins").getValue();
			nomIns = (String) instaladoresWrapper.findField("usu_nomins").getValue();
			tipIns = (String) instaladoresWrapper.findField("usu_instip").getValue();

			JSONObject jsonInstalador = new JSONObject();
			jsonInstalador.put("codins", String.valueOf(codIns));
			jsonInstalador.put("nomins", nomIns);
			jsonInstalador.put("instip", tipIns);
			jsonInstaladores.put(jsonInstalador);

		}
		out.print(jsonInstaladores);
		out.flush();
		out.close();
	}

	public void listaColaboradoresCerec(HttpServletResponse response) throws JSONException, IOException
	{
		PrintWriter out = response.getWriter();

		NeoUser userLogado = PortalUtil.getCurrentUser();
		String filtro = "";

		if (userLogado.getGroup().getCode().equals("Execução RSC"))
		{
			filtro = "group.code in ('Execução RSC')";
		}
		else if (userLogado.getGroup().getCode().equals("Cobrança"))
		{
			filtro = "group.code in ('Cobrança')";
		}
		else if (userLogado.getGroup().getCode().equals("Call Center"))
		{
			filtro = "group.code in ('Call Center')";
		}
		else if (userLogado.getGroup().getCode().equals("Call Center 2"))
		{
			filtro = "group.code in ('Call Center 2')";
		}
		else if(userLogado.getGroup().getCode().equals("OuvidoriaGrupo"))
		{
		    	filtro = "group.code in ('OuvidoriaGrupo')";
		}
		else if(userLogado.getGroup().getCode().equals("SupervisaoDeCadastro"))
		{
		    	filtro = "group.code in ('SupervisaoDeCadastro')";
		}
		else
		{
			filtro = "group.code in ('Execução RSC','Cobrança', 'Call Center', 'Call Center 2','OuvidoriaGrupo','SupervisaoDeCadastro')";
		}

		List<NeoUser> users = (List<NeoUser>) PersistEngine.getObjects(NeoUser.class, new QLRawFilter(filtro), -1, -1, "fullName");

		JSONArray jsonUsers = new JSONArray();

		for (NeoUser user : users)
		{
			EntityWrapper userWrapper = new EntityWrapper(user);
			String nome = (String) userWrapper.findValue("fullName");
			JSONObject jsonUser = new JSONObject();
			jsonUser.put("fullname", nome);
			jsonUsers.put(jsonUser);
		}
		out.print(jsonUsers);
		out.flush();
		out.close();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

}
