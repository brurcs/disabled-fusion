package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsConnectionUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoUtils;

public class RotinaRTOciosos implements CustomJobAdapter {

	private static final Log log = LogFactory.getLog(RotinaRTOciosos.class);
	//private Map<Integer, String> serverInfo = OrsegupsConnectionUtils.getServerInfo("SIGMA");
	
	@Override
	public void execute(CustomJobContext ctx) {
		log.warn("#### Iniciando Processo de Abertura de Tarefas de RTs Ociosos####");
		Long key = GregorianCalendar.getInstance().getTimeInMillis(); 
		GregorianCalendar dataAtual = new GregorianCalendar();
		dataAtual.add(GregorianCalendar.MONTH, 1);
		dataAtual.set(GregorianCalendar.DATE, 1);
		log.warn(NeoUtils.safeDateFormat(dataAtual, "dd/MM/yyyy HH:mm"));
		
		StringBuilder sql = new StringBuilder();
		
		sql.append(" ");
		 
		
		sql.append(" DECLARE @DatRef  Datetime ");
		sql.append("  SELECT @DatRef = dateadd(d, datediff(d,0, getdate()-1), 0) ");  
		sql.append("  SELECT DISTINCT hch.NumCra, fun.NumEmp, fun.TipCol, fun.NumCad, fun.NomFun, fun.NumCpf, orn.NumLoc, orn.NomLoc, orn.USU_LotOrn, orn.USU_CodReg, esc.NomEsc, ");   
		sql.append("  hes.CodEsc, hes.CodTma, ISNULL(afa.SitAfa, 1) AS SitAfa, ISNULL(sit.DesSit, 'Trabalhando') AS DesSit, ISNULL(sit.USU_BloPre, 'N') AS BloPre, ");
		sql.append("  afa.DatAfa AS DataAfa, afa.DatTer, car.TitRed, orn.NomLoc, fun.CodCcu, DATEADD(MINUTE, ace.HorAcc, ace.DatAcc), DATEADD(MINUTE, acs.HorAcc, acs.DatAcc), @DatRef, reg.usu_nomreg,  ");  
		sql.append("  org.TabOrg, ace.datacc, datediff (hh, DATEADD(MINUTE, ace.HorAcc, ace.DatAcc) , DATEADD(MINUTE, acs.HorAcc, acs.DatAcc)  ) as horasOciosas ");
		sql.append("  FROM R034FUN fun WITH (NOLOCK) ");  
		sql.append("  LEFT JOIN R038HCH hch WITH (NOLOCK) ON hch.NUMEMP = fun.NumEmp AND fun.TIPCOL = hch.TIPCOL ");   
		sql.append("                                                                AND fun.NUMCAD = hch.NUMCAD "); 
		sql.append("                                                                AND DATEADD(MINUTE, hch.HorIni, hch.DatIni) = (SELECT MAX(DATEADD(MINUTE, hch2.HorIni, hch2.DatIni)) ");   
		sql.append("                                                                                                                 FROM R038HCH hch2 ");   
		sql.append("                                                                                                                 WHERE hch2.NUMEMP = hch.NUMEMP ");   
		sql.append("                                                                                                                 AND hch2.TIPCOL = hch.TIPCOL ");   
		sql.append("                                                                                                                 AND hch2.NUMCAD = hch.NUMCAD    ");
		sql.append("                                                                                                                 AND DATEADD(MINUTE, hch2.HorIni, hch2.DatIni) <= @DatRef ");
		sql.append("																											  )	");	
		sql.append("  INNER JOIN R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp AND hlo.TipCol = fun.TipCol   ");
		sql.append("                                                                 AND hlo.NumCad = fun.NumCad   ");
		sql.append("                                                                 AND hlo.DatAlt = (SELECT MAX (DATALT) ");  
		sql.append("                                                                                    FROM R038HLO TABELA001    ");
		sql.append("                                                                                    WHERE TABELA001.NUMEMP = hlo.NUMEMP ");  
		sql.append("                                                                                    AND TABELA001.TIPCOL = hlo.TIPCOL ");  
		sql.append("                                                                                    AND TABELA001.NUMCAD = hlo.NUMCAD ");  
		sql.append("                                                                                    AND TABELA001.DATALT <= @DatRef ");  
		sql.append("                                                                                  ) ");  
		sql.append("  LEFT  JOIN R038HES hes WITH (NOLOCK) ON hes.NumEmp = fun.NumEmp AND hes.TipCol = fun.TipCol ");  
		sql.append("                                                                 AND hes.NumCad = fun.NumCad ");  
		sql.append("                                                                 AND hes.DatAlt = (SELECT MAX (DATALT) ");  
		sql.append("                                                                                     FROM R038HES TABELA001 ");  
		sql.append("                                                                                     WHERE TABELA001.NUMEMP = hes.NUMEMP ");  
		sql.append("                                                                                     AND TABELA001.TIPCOL = hes.TIPCOL   ");
		sql.append("                                                                                     AND TABELA001.NUMCAD = hes.NUMCAD    ");
		sql.append("                                                                                     AND TABELA001.DATALT <= @DatRef   ");
		sql.append("                                                                                  )   ");
		sql.append("  LEFT JOIN USU_TFisRes fis WITH (NOLOCK) ON fis.USU_FisRes = fun.usu_fisres   ");
		sql.append("  LEFT JOIN R038AFA afa WITH (NOLOCK) ON (afa.NumEmp = fun.NumEmp AND afa.TipCol = fun.TipCol AND afa.NumCad = fun.NumCad AND afa.DatAfa = (SELECT MAX (DATAFA) ");  
		sql.append("                                                                                                                                             FROM R038AFA TABELA001   ");
		sql.append("                                                                                                                                             WHERE TABELA001.NUMEMP = afa.NUMEMP ");  
		sql.append("                                                                                                                                             AND TABELA001.TIPCOL = afa.TIPCOL   ");
		sql.append("                                                                                                                                             AND TABELA001.NUMCAD = afa.NUMCAD   ");
		sql.append("                                                                                                                                             AND TABELA001.DATAFA <= @DatRef   ");
		sql.append("                                                                                                                                             AND (TABELA001.DatTer = '1900-12-31 00:00:00' ");  
		sql.append("                                                                                                                                             OR TABELA001.DatTer >= cast(floor(cast(@DatRef as float)) as datetime))) ");  
		sql.append("																											  								)	");
		sql.append("  LEFT JOIN R010SIT sit WITH (NOLOCK) ON sit.CodSit = afa.SitAfa   ");
		sql.append("  LEFT JOIN R038HCA hca WITH (NOLOCK) ON hca.NumEmp = fun.NumEmp AND hca.TipCol = fun.TipCol AND hca.NumCad = fun.NumCad AND hca.DatAlt = (SELECT MAX (DATALT) FROM R038HCA TABELA001 WHERE TABELA001.NUMEMP = hca.NUMEMP AND TABELA001.TIPCOL = hca.TIPCOL AND TABELA001.NUMCAD = hca.NUMCAD AND TABELA001.DATALT <= @DatRef) ");  
		sql.append("  LEFT JOIN R024CAR car WITH (NOLOCK) ON car.EstCar = hca.EstCar AND car.CodCar = hca.CodCar   ");
		sql.append("  INNER JOIN R006ESC esc WITH (NOLOCK) ON esc.CodEsc = hes.CodEsc   ");
		sql.append("  INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.NumLoc = hlo.NumLoc AND orn.TabOrg = hlo.TabOrg ");
		sql.append("  INNER JOIN R016HIE org WITH (NOLOCK) ON org.TabOrg = orn.taborg AND org.NumLoc = orn.numloc ");
		sql.append("  INNER JOIN usu_t200reg reg on orn.usu_codreg = reg.USU_CodReg ");
		sql.append("  INNER JOIN R070ACC acs WITH (NOLOCK) ON acs.TipAcc = 100 AND acs.NumCra = hch.NumCra and acs.USU_NumLoc = orn.NumLoc AND acs.DirAcc = 'S' AND acs.DatAcc = @DatRef AND DATEADD(SECOND, acs.SeqAcc, DATEADD(MINUTE, acs.HorAcc, acs.DatAcc)) = (SELECT MAX(DATEADD(SECOND, acs2.SeqAcc, DATEADD(MINUTE, acs2.HorAcc, acs2.DatAcc))) FROM R070ACC acs2 WITH (NOLOCK) WHERE acs2.TipAcc = 100 AND acs2.NumCra = acs.NumCra AND acs2.USU_NumLoc = acs.USU_NumLoc AND acs2.DatAcc = acs.DatAcc AND acs2.DirAcc = acs.DirAcc) "); 
		sql.append("  INNER JOIN R070ACC ace WITH (NOLOCK) ON ace.TipAcc = 100 AND ace.NumCra = hch.NumCra AND ace.USU_NumLoc = orn.NumLoc AND ace.DirAcc = 'E' AND DATEADD(SECOND, ace.SeqAcc, DATEADD(MINUTE, ace.HorAcc, ace.DatAcc)) = (SELECT MAX(DATEADD(SECOND, ace2.SeqAcc, DATEADD(MINUTE, ace2.HorAcc, ace2.DatAcc))) FROM R070ACC ace2 WITH (NOLOCK) WHERE ace2.TipAcc = 100 AND ace2.NumCra = ace.NumCra AND ace2.USU_NumLoc = ace.USU_NumLoc AND ace2.DirAcc = ace.DirAcc AND DATEADD(MINUTE, ace2.HorAcc, ace2.DatAcc) < DATEADD(MINUTE, acs.HorAcc, acs.DatAcc) ) "); 
		sql.append("  LEFT JOIN R064THR thr WITH (NOLOCK) ON fun.numemp = thr.NumEmp AND fun.tipcol = thr.tipcol AND fun.numcad = thr.numcad");
		sql.append("  WHERE (orn.NomLoc like '%Reserva%' OR orn.NomLoc like '%Cobertura%' OR orn.NomLoc like '%aviso%pr%vio%' OR orn.NomLoc like '%afastado%' OR orn.NomLoc like '%FIS COB%') "); 
		sql.append("  and orn.nomloc not like '%almoço%' ");
		sql.append("  AND orn.USU_NumCtr = 1 AND fun.TipCol = 1 ");  
		sql.append("  AND NOT ( ");      
	        sql.append("  EXISTS (SELECT 1 FROM USU_T038COBFUN cob WITH (NOLOCK), USU_T010MoCo mc WITH (NOLOCK) WHERE mc.USU_CodMot = cob.USU_CodMot AND mc.USU_TipCob IN ('D', 'R') AND cob.USU_NumEmp = fun.NumEmp AND cob.USU_TipCol= fun.TipCol AND cob.USU_NumCad = fun.NumCad and cast(cob.USU_DatAlt as date) >= cast(ace.datacc as date) AND cast(cob.USU_DatAlt as date) <= cast(acs.datacc as date)) ");  // cobertura entre um 
	        sql.append("     OR ");
	        sql.append("  EXISTS (SELECT 1 FROM USU_T038COBFUN cob WITH (NOLOCK), USU_T010MoCo mc WITH (NOLOCK) WHERE mc.USU_CodMot = cob.USU_CodMot AND mc.USU_TipCob IN ('D', 'R') AND cob.USU_NumEmp = fun.NumEmp AND cob.USU_TipCol= fun.TipCol AND cob.USU_NumCad = fun.NumCad and DATEADD(MINUTE, acs.HorAcc, acs.DatAcc) >= cob.usu_datalt AND  DATEADD(MINUTE, acs.HorAcc, acs.DatAcc) <= cob.usu_datfim ) ");
	        sql.append(" 	 OR ");	
	        sql.append("  EXISTS (SELECT 1 FROM [CACUPE\\sql02].Fusion_producao.dbo.D_RHViaturaColaborador r WITH (NOLOCK),[CACUPE\\sql02].Fusion_producao.dbo.X_SIGMA90VIATURA sv WITH (NOLOCK) WHERE r.cpfInt = fun.NUMCPF AND sv.NEOID = r.VIATURA_NEOID AND "); 
	        sql.append("     	   EXISTS(SELECT 1 FROM [FSOODB03\\SQL01].SIGMA90.dbo.HISTORICO_ALARME ha WITH (NOLOCK) WHERE ha.CD_VIATURA = sv.CD_VIATURA and cast(ha.dt_recebido as date) >= cast(ace.datacc as date) AND cast(ha.dt_recebido as date) <= cast(acs.datacc as date) and ha.cd_evento <> 'XXX8')) ");
		sql.append("       ) ");
		sql.append(" AND (ACS.DATACC != COALESCE((SELECT TOP 1 THR.DATINI FROM R064THR THR WHERE THR.NUMEMP = FUN.NUMEMP AND THR.TIPCOL = FUN.TIPCOL AND THR.NUMCAD = FUN.NUMCAD AND THR.CODHOR = 9996 ORDER BY THR.DATINI DESC) , '1900-12-31 00:00:00.000'))");
		sql.append(" AND EXISTS (SELECT 1 FROM R006ESC esc  ");
		sql.append(" INNER JOIN R006TMA tma ON tma.CodEsc = esc.CodEsc  ");
		sql.append(" INNER JOIN R006HOR hor ON hor.CodEsc = esc.CodEsc  ");
		sql.append(" INNER JOIN R004MHR mhr ON mhr.CodHor = hor.CodHor  ");
		sql.append(" INNER JOIN R004HOR hori ON hori.CodHor = hor.CodHor  ");
		sql.append(" WHERE DATEDIFF(dd,tma.DatBas,DATEADD(MINUTE, acs.HorAcc, acs.DatAcc)) % (SELECT COUNT(*) FROM R006HOR hr2 WHERE hr2.CodEsc = hor.CodEsc) + 1 = hor.SeqReg ");
		sql.append("  AND ((esc.usu_gruesc in (1,2,7) AND mhr.UsoBat IN (1,2,3)) OR (esc.usu_gruesc in (3,4,5,6,8,9,10,11,12,13))) ");
		sql.append("  AND hor.CodEsc = fun.codesc ");
		sql.append("  AND tma.CodTma = fun.codtma ");
		sql.append("  GROUP BY esc.CodEsc, esc.NomEsc, tma.CodTma, mhr.SeqMar, mhr.HorBat, hori.TipHor , hori.CodHor) ");
		sql.append("  order by orn.NomLoc, fun.NomFun ");
		log.warn("sql=" + sql.toString()); 

		
		Connection connection = null;
		ResultSet rs = null;
		PreparedStatement pst = null;
		try
		{
			connection = OrsegupsUtils.getSqlConnection("VETORH");
			pst = connection.prepareStatement(sql.toString());
			
			//pst.setDate(1, new Date(dataAtual.getTimeInMillis()));
			//pst.setDate(2, new Date(dataAtual.getTimeInMillis()));
			rs = pst.executeQuery();
			
			while (rs.next()){
				
				
					
				Long codReg = rs.getLong("usu_codreg");
				String lotacao = "";
				
				if (codReg == 0 && rs.getString("USU_LotOrn").equals("Coordenadoria Indenização Contratual"))
				{
					lotacao = rs.getString("NomLoc");
				}
				else if (codReg == 1L)
				{
					lotacao = rs.getString("NomLoc");
				}
				else if (codReg == 13L)
				{
					lotacao = rs.getString("NomLoc");
				}
				else
				{
					lotacao = rs.getString("USU_LotOrn");
				}
				
				log.warn("[OCIOSO RT] - Abrir tarefa -> Funcionario: " + rs.getLong("numcad") + " - " + rs.getString("nomfun") + " codReg="+codReg+", lotacao"+lotacao );
				//abrirTarefaSimples(rs.getLong("NumEmp"),rs.getLong("numcad"), rs.getString("nomfun"),codReg,rs.getString("usu_nomreg"),lotacao, rs.getString("titred"), rs.getString("nomloc"), rs.getString("codccu"));
				Long numCpf = Long.parseLong(rs.getString("NumCpf"));
				GregorianCalendar datOci = new GregorianCalendar();
				datOci.setTime(rs.getDate("datacc"));
				String tarefa = abrirTarefaRTOcioso(rs.getLong("NumEmp"), rs.getLong("numcad"), rs.getString("nomfun"), codReg, rs.getString("usu_nomreg"), lotacao, rs.getString("titred"), rs.getString("nomloc"), rs.getString("codccu"),numCpf,
								rs.getLong("TipCol"), datOci, rs.getLong("TabOrg"), rs.getLong("NumLoc"), rs.getInt("horasOciosas"));
				log.warn("[OCIOSO RT] - Abrir tarefa -> Funcionario: " + rs.getLong("numcad") + " - " + rs.getString("nomfun") + " codReg=" + codReg + ", lotacao" + lotacao + ", tarefa: "+tarefa);
//				if (tarefa != null && !tarefa.isEmpty() && !tarefa.contains("Erro"))
//				{
//					InstantiableEntityInfo tarefaFE = AdapterUtils.getInstantiableEntityInfo("HistoricoRTOcioso");
//					NeoObject noFE = tarefaFE.createNewInstance();
//					EntityWrapper feWrapper = new EntityWrapper(noFE);
//
//					feWrapper.findField("empresa").setValue(rs.getLong("NumEmp"));
//					feWrapper.findField("matricula").setValue(rs.getLong("numcad"));
//					feWrapper.findField("funcionario").setValue(rs.getString("nomfun"));
//					feWrapper.findField("codigoRegional").setValue(codReg);
//					feWrapper.findField("regional").setValue(rs.getString("usu_nomreg"));
//					feWrapper.findField("lotacao").setValue(lotacao);
//					feWrapper.findField("cargo").setValue(rs.getString("titred"));
//					feWrapper.findField("nomeLocal").setValue(rs.getString("nomloc"));
//					feWrapper.findField("centroDeCusto").setValue(rs.getString("codccu"));
//					feWrapper.findField("tarefa").setValue(tarefa);
//					String processo = "G005 - Acompanhamento de RT Ocioso";
//					feWrapper.findField("processo").setValue(processo);
//					feWrapper.findField("dataExecucao").setValue(new GregorianCalendar());
//					PersistEngine.persist(noFE);
//				}	
				
			}
			
			log.warn("#### Finalizando Processo de Abertura de Tarefas de RTs Ociosos ####");
		}
		catch (SQLException e)
		{
			log.warn(sql.toString());
			log.error("#### Erro Processo de Abertura de Tarefas de RTs Ociosos ####");
			System.out.println("["+key+"] #### Erro Processo de Abertura de Tarefas de RTs Ociosos ####");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
		}
		finally
		{
			OrsegupsUtils.closeConnection(connection, pst, rs);
			log.warn("####  Processo de Abertura de Tarefas de RTs Ociosos finalizado ####");
		}
		
	}
	
	public static void abrirTarefaSimples(Long numemp, Long numcad, String nomfun, Long codReg, String nomreg, String lotacao, String cargo, String nomloc, String centroCusto ){
		
		
		GregorianCalendar cpt = new GregorianCalendar();

		GregorianCalendar gc = new GregorianCalendar();
		gc.add(GregorianCalendar.DATE, -1);
		
		String msg = ""; //"<p>Colaborador ocioso lotado em RT:"+numemp + " - " +numcad+" - "+nomfun+".</p>";
		msg = "<p>Identificamos que o colaborador "+numemp+"/"+numcad+" - "+nomfun+", "+cargo+", lotado no posto "
				+ nomloc + " - "+centroCusto+", não realizou nenhuma cobertura no dia "+NeoUtils.safeDateFormat(gc,"dd/MM/yyyy")+", restando portanto ocioso.</p>"
				+"<p> Solicitamos informar o motivo.</p>";
		
		NeoObject oTarefa = AdapterUtils.createNewEntityInstance("Tarefa");
		
		final EntityWrapper wTarefa= new EntityWrapper(oTarefa);
		
		NeoUser usuarioSolicitante = PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "dilmoberger" /*"beatriz.malmann"*/) );
		NeoUser usuarioResponsavel = null;//PersistEngine.getObject( NeoUser.class, new QLEqualsFilter("code", "danilo.silva") );

		NeoPaper papel = /*OrsegupsUtils.getPapelJustificarR022(codReg, lotacao); */ OrsegupsUtils.getPapelRTOcioso(codReg, lotacao);

		if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
		{
			for (NeoUser user : papel.getUsers())
			{
				usuarioResponsavel = user;
				log.warn("getPapel->"+user.getCode());
				break;
			}
		}

		GregorianCalendar prazo = new GregorianCalendar();
		prazo.add(GregorianCalendar.DAY_OF_MONTH, 1);
		prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
		prazo.set(GregorianCalendar.MINUTE, 23);
		prazo.set(GregorianCalendar.SECOND, 59);

		wTarefa.findField("Solicitante").setValue(usuarioSolicitante);
		wTarefa.findField("Executor").setValue(usuarioResponsavel);
		wTarefa.findField("Titulo").setValue("Ocioso em Reserva Técnica - ["+ nomreg + "] "+ numcad+ "-" + nomfun);
		wTarefa.findField("DescricaoSolicitacao").setValue(msg);
		wTarefa.findField("Prazo").setValue(OrsegupsUtils.getNextWorkDay(prazo));
		wTarefa.findField("dataSolicitacao").setValue(new GregorianCalendar());
		
		PersistEngine.persist(oTarefa);
		
		iniciaProcessoTarefaSimples(oTarefa, usuarioSolicitante, usuarioResponsavel);
	}

	public static Boolean iniciaProcessoTarefaSimples(NeoObject eformProcesso, NeoUser requester, NeoUser responsavel)
	{
		Boolean result = false;
	
		QLEqualsFilter equal = new QLEqualsFilter("Name", "G001 - Tarefa Simples");
		ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);
		
		/*WFProcess proc = processModel.startProcess(eformProcesso, true, null, null, null, null, requester);
		proc.setRequester(requester);
		log.warn("[OCIOSO RT] - Abriu Tarefa simples n. " + proc.getCode());
	
		proc.setSaved(true);
	
		// Necessita persistir para que o metodo getOpenActivities() consiga buscar as atividades abertas
		PersistEngine.persist(proc);
		PersistEngine.commit(true);
	
		 Finaliza a primeira tarefa 
		Task task = null;
	
		final List<Activity> acts = proc.getOpenActivities();
		log.warn("acts: " + acts);
		if (acts != null)
		{
			log.warn("acts.size(): " + acts.size());
		}
		final Activity activity1 = (Activity) acts.get(0);
		if (activity1 instanceof UserActivity)
		{
			try
			{
				if (((UserActivity) activity1).getTaskList().size() <= 0)
				{
					task = activity1.getModel().getTaskAssigner().assign((UserActivity) activity1, responsavel);
					OrsegupsWorkflowHelper.finishTaskByActivity(activity1);
				}
				else
				{
					task = ((UserActivity) activity1).getTaskList().get(0);
					OrsegupsWorkflowHelper.finishTaskByActivity(activity1);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				System.err.println("Erro ao iniciar a primeira atividade do processo.");
			}
		}*/
		
		String tarefa = null;
		
		try{
			tarefa = OrsegupsWorkflowHelper.iniciaProcesso(processModel, eformProcesso, true, responsavel, true, requester);
			System.out.println("[OCIOSO RESERVA] - Abriu Tarefa simples n. " + tarefa);
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	public String abrirTarefaRTOcioso(Long numemp, Long numcad, String nomfun, Long codReg, String nomreg, String lotacao, String cargo, String nomloc, String centroCusto, 
		Long numCpf, Long tipCol, GregorianCalendar datOci, Long tabOrg, Long numLoc, Integer horasOciosas) {
	    
		NeoUser objSolicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "fernanda.martins"));
		NeoUser usuarioResponsavel = null;

		/*Usuário padrão de execução, com exceção da regional 9 - ACL */
		NeoPaper papel = null;//(NeoPaper) OrsegupsUtils.getPapelRTOcioso(codReg, lotacao);
		if(codReg == 9){
		    papel = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "RTExecutor"));
		} else {
		    papel = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "RTExecutor"));
		}
		
		if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty()){
			for (NeoUser user : papel.getUsers()) {
				usuarioResponsavel = user;
				log.warn("getPapel->" + user.getCode());
				break;
			}
		}

		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);

		GregorianCalendar gc = new GregorianCalendar();
		gc.add(GregorianCalendar.DATE, -1);

		String titulo =  numcad + "-" + nomfun + " - "+"[" + nomreg + "] ";
		String descricao = "";
		descricao = "<p>Identificamos que o colaborador " + numemp + "/" + numcad + " - " + nomfun + ", " + cargo + ", lotado no posto " + nomloc + " - " + centroCusto + ", não realizou nenhuma cobertura no dia " + NeoUtils.safeDateFormat(gc, "dd/MM/yyyy") + ", restando portanto ocioso.</p>" + "<p> Solicitamos informar o motivo.</p>";

		if (NeoUtils.safeIsNull(objSolicitante)){
			return "Erro #1 - Solicitante não encontrado";
		}

		if (NeoUtils.safeIsNull(usuarioResponsavel)){
			return "Erro #7 - Executor não encontrado";
		}

		if (NeoUtils.safeIsNull(titulo)){
			return "Erro #3 - Título não informado";
		}

		if (NeoUtils.safeIsNull(descricao)){
			return "Erro #4 - Descrição não informada";
		}

		if (NeoUtils.safeIsNull(prazo)){
			return "Erro #6 - Prazo não informado";
		}

		//Cria Instancia do Eform Principal
		InstantiableEntityInfo infoTarefa = AdapterUtils.getInstantiableEntityInfo("RTOcioso");
		NeoObject noTarefa = infoTarefa.createNewInstance();
		EntityWrapper tarefaWrapper = new EntityWrapper(noTarefa);

		tarefaWrapper.findField("executor").setValue(usuarioResponsavel);
		//tarefaWrapper.findField("historico").setValue(historico);
		tarefaWrapper.findField("titulo").setValue(titulo);
		tarefaWrapper.findField("nome").setValue(nomfun);
		tarefaWrapper.findField("cpf").setValue(numCpf);
		tarefaWrapper.findField("solicitacao").setValue(descricao);
		tarefaWrapper.findField("prazo").setValue(prazo);
		
		tarefaWrapper.findField("empresa").setValue(numemp);
		tarefaWrapper.findField("tipoColaborador").setValue(tipCol);
		tarefaWrapper.findField("matricula").setValue(numcad);
		tarefaWrapper.findField("dataOciosidade").setValue(datOci);
		tarefaWrapper.findField("tabelaOrg").setValue(tabOrg);
		tarefaWrapper.findField("codigoLocal").setValue(numLoc);
		tarefaWrapper.findField("horasOciosas").setValue(horasOciosas);
		
		//Salva o Eform
		PersistEngine.persist(noTarefa);

		// Cria Instancia do WorkFlow
		QLEqualsFilter equal = new QLEqualsFilter("name", "R005 - Acompanhamento de RT Ocioso");
		ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);

		//ABre a tarefa
		String proc = OrsegupsWorkflowHelper.iniciaProcesso(processModel, noTarefa, true, usuarioResponsavel, false, null);

		return proc;
	}

}
