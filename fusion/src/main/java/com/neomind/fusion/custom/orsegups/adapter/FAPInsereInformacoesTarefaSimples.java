package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class FAPInsereInformacoesTarefaSimples implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		Long aplicacaoPagamento = (Long) processEntity.findValue("aplicacaoPagamento.codigo");
		String tituloTarefa = "";
		String descricaoSolicitacao = "";
		
		if (aplicacaoPagamento == 1L) {
			
			//Título: Ref.: Processo FAP <código> - <aplicação> - <placa> - <fornecedor>
			String codigoTarefaSimples = (String) processEntity.findValue("wfprocess.code");
			String descricaoAplicacao = (String) processEntity.findValue("aplicacaoPagamento.nome");
			String placaViatura = (String) processEntity.findValue("viatura.placa");
			String tipoViatura = (String) processEntity.findValue("viatura.tipoViatura.nome");
			Long codigoFornecedor = (Long) processEntity.findValue("fornecedor.codfor");
			String nomeFornecedor = (String) processEntity.findValue("fornecedor.nomfor");
			
			tituloTarefa = "Ref.: Proceso FAP " + codigoTarefaSimples + " - " + descricaoAplicacao + " - " + placaViatura + " - " + tipoViatura + " - " + codigoFornecedor.toString() + " - "+ nomeFornecedor;
			
			String nomeSolicitante = (String) processEntity.findValue("solicitanteOrcamento.fullName");
			GregorianCalendar dataSolicitacao = (GregorianCalendar) processEntity.findValue("dataSolicitacao");
			String kmViatura = String.valueOf((Long) processEntity.findValue("laudo.kilometragem"));
			String nomeCondutor = (String) processEntity.findValue("laudo.condutor");
			String descricaoTipoManutencao = (String) processEntity.findValue("laudo.tipoManutencao.descricao");
			String descricaoRelato = (String) processEntity.findValue("laudo.relato");
			String descricaoLaudo = (String) processEntity.findValue("laudo.laudo");
			BigDecimal valorTotal = (BigDecimal) processEntity.findValue("valorTotal");
			String responsavelPagamento = (String) processEntity.findValue("responsavelPagamento.nome");
			
			descricaoSolicitacao = descricaoSolicitacao + "<strong>Solicitante:</strong> " + nomeSolicitante + "<br>";
			descricaoSolicitacao = descricaoSolicitacao + "<strong>Data Solicitação:</strong> " + NeoUtils.safeDateFormat(dataSolicitacao, "dd/MM/yyyy") + "<br>";
			descricaoSolicitacao = descricaoSolicitacao + "<strong>Km:</strong> " + kmViatura + "<br>";
			descricaoSolicitacao = descricaoSolicitacao + "<strong>Condutor:</strong> " + nomeCondutor + "<br>";
			descricaoSolicitacao = descricaoSolicitacao + "<strong>Tipo de Manutenção:</strong> " + descricaoTipoManutencao + "<br>";
			descricaoSolicitacao = descricaoSolicitacao + "<strong>Relato:</strong> " + descricaoRelato + "<br>";
			descricaoSolicitacao = descricaoSolicitacao + "<strong>Laudo:</strong> " + descricaoLaudo + "<br>";
			descricaoSolicitacao = descricaoSolicitacao + "<strong>Valor Total:</strong> " + valorTotal + "<br>";
			descricaoSolicitacao = descricaoSolicitacao + "<strong>Responsável pelo pagamento:</strong> " + responsavelPagamento + "<br>";
			
		}
		else if ((aplicacaoPagamento == 2L) || (aplicacaoPagamento == 3L) || (aplicacaoPagamento == 4L) ||
				 (aplicacaoPagamento == 5L) || (aplicacaoPagamento == 6L) || (aplicacaoPagamento == 7L) ||
				 (aplicacaoPagamento == 8L) || (aplicacaoPagamento == 9L))
		{
			//Título: Ref.: Processo FAP <código> - <aplicação> - <conta> - <fantasiaCliente>
			String codigoTarefaSimples = (String) processEntity.findValue("wfprocess.code");
			String descricaoAplicacao = (String) processEntity.findValue("aplicacaoPagamento.nome");
			String contaSigma = (String) processEntity.findValue("codigoConta"); 
			String nomeFantasia = (String) processEntity.findValue("contaSigma.fantasia");
			
			tituloTarefa = "Ref.: Proceso FAP " + codigoTarefaSimples + " - " + descricaoAplicacao + " - " + contaSigma + " - " + nomeFantasia;
			
			String nomeSolicitante = (String) processEntity.findValue("solicitanteOrcamento.fullName");
			GregorianCalendar dataSolicitacao = (GregorianCalendar) processEntity.findValue("dataSolicitacao");
			String descricaoRelato = (String) processEntity.findValue("laudoEletronica.relato");
			String descricaoLaudo = (String) processEntity.findValue("laudoEletronica.laudo");
			BigDecimal valorTotal = (BigDecimal) processEntity.findValue("valorTotal");
			
			descricaoSolicitacao = descricaoSolicitacao + "<strong>Solicitante:</strong> " + nomeSolicitante + "<br>";
			descricaoSolicitacao = descricaoSolicitacao + "<strong><b>Data Solicitação:</strong> " + NeoUtils.safeDateFormat(dataSolicitacao, "dd/MM/yyyy") + "<br>";
			descricaoSolicitacao = descricaoSolicitacao + "<strong><b>Relato:</strong> " + descricaoRelato + "<br>";
			descricaoSolicitacao = descricaoSolicitacao + "<strong><b>Laudo:</strong> " + descricaoLaudo + "<br>";
			descricaoSolicitacao = descricaoSolicitacao + "<strong><b>Valor Total:</strong> " + valorTotal + "<br>";
		}
		
		NeoUser objSolicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "dilmoberger"));
						
		NeoObject noTarefaSimples = AdapterUtils.createNewEntityInstance("Tarefa");
		EntityWrapper centralWrapper = new EntityWrapper(noTarefaSimples);
		centralWrapper.setValue("Solicitante", objSolicitante);
		centralWrapper.setValue("Titulo", tituloTarefa);
		centralWrapper.setValue("DescricaoSolicitacao", NeoUtils.encodeHTMLValue(descricaoSolicitacao));
		
		processEntity.setValue("formTarefaSimples", noTarefaSimples);		
	}
	
	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub
		
	}
}
