package com.neomind.fusion.custom.orsegups.placa;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.neomind.fusion.custom.orsegups.placa.vo.MunicipioVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;

public class OrsegupsPlacaXpl1
{

    	public static String converteDataSript(String d) throws ParseException
	{
        	String data = d.substring(0, 10).replaceAll("-", "/");
        	String[] s = data.split("/");
        	String novaData = s[2]+"/"+s[1]+"/"+s[0];
        	
        	return novaData;
	}
	

	public static List<MunicipioVO> municipios()
	{
		List<MunicipioVO> municipios = new ArrayList<MunicipioVO>();
		Connection connVetorh = PersistEngine.getConnection("SIGMA90");
		ResultSet rsEmpresa = null;
		PreparedStatement stEmpresa = null;
		StringBuilder queryEmpresa = new StringBuilder();
		MunicipioVO mVo = new MunicipioVO();
		mVo.setCdMunicipio(0);
		mVo.setNmMunicipio("Selecione o municipio");
		municipios.add(mVo);

		queryEmpresa.append("SELECT ID_CIDADE,upper(NOME)+'/'+ID_ESTADO AS DE_CIDADE FROM dbCIDADE WHERE ID_CIDADE IN (10040,10471,10645,10667,10674,10754,11259,11272,11511,11897) ORDER BY ID_ESTADO,NOME"); 
		try
		{
			stEmpresa = connVetorh.prepareStatement(queryEmpresa.toString());
			rsEmpresa = stEmpresa.executeQuery();
			while (rsEmpresa.next())
			{
			    	mVo = new MunicipioVO();
			    	mVo.setCdMunicipio(Integer.parseInt(rsEmpresa.getString("ID_CIDADE")));
			    	mVo.setNmMunicipio(rsEmpresa.getString("ID_CIDADE") + " - " + rsEmpresa.getString("DE_CIDADE"));
				municipios.add(mVo);
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connVetorh, stEmpresa, rsEmpresa);
		}

		return municipios;
	}
	
}
