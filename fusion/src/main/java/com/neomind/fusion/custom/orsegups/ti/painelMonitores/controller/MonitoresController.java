package com.neomind.fusion.custom.orsegups.ti.painelMonitores.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.neomind.fusion.custom.orsegups.ti.painelMonitores.beans.MonitoresBean;
import com.neomind.fusion.custom.orsegups.ti.painelMonitores.engine.MonitoresEngine;

@Path(value = "monitores")
public class MonitoresController {

    private MonitoresEngine engine = new MonitoresEngine();

    @GET
    @Path("cpu-usage/{hostname}")
    @Produces("application/json")
    public MonitoresBean cpu(@PathParam("hostname") String hostname) {
	
	MonitoresBean monitor = engine.getCpuUsage(hostname);
	
	return monitor;
    }
    
    @GET
    @Path("free-memory/{hostname}")
    @Produces("application/json")
    public MonitoresBean memory(@PathParam("hostname") String hostname) {
	
	MonitoresBean monitor = engine.getMemoryUsage(hostname);
	
	return monitor;
    }

}
