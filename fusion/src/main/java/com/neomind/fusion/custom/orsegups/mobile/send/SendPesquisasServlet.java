package com.neomind.fusion.custom.orsegups.mobile.send;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.mobile.vo.PesquisaVO;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

/**
 * Classe não utilizada pelo aplicativo mobile.
 * @author neomind
 *
 */
@Deprecated
@WebServlet(name="SendPesquisasServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.mobile.send.SendPesquisasServlet"})
public class SendPesquisasServlet extends HttpServlet {

	
	private static final long serialVersionUID = 1L;
	@Deprecated
	public String pesquisa(HttpServletRequest request) {
		
		String regional = request.getParameter("siglaRegional");
		
		System.out.println("Enviando pesquisas para a regional " + regional);
		
		QLGroupFilter gpRegional = new QLGroupFilter("AND");
		gpRegional.addFilter(new QLEqualsFilter("sigla",regional));
		
		List<NeoObject> lstObjPesqReg = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("IMREGIONAL"),gpRegional);
		
		Long neoIdRegional = 0L;
		if (lstObjPesqReg != null && lstObjPesqReg.size() > 0 ){
			NeoObject oReg = lstObjPesqReg.get(0);
			EntityWrapper wReg = new EntityWrapper(oReg);
			neoIdRegional = NeoUtils.safeLong( NeoUtils.safeOutputString( wReg.findValue("neoId") ) );
			System.out.println("Regional Encontrada:" +wReg.findValue("nome") + "-" + wReg.findValue("codigo"));
		}
		QLGroupFilter gpf = new QLGroupFilter("AND" );
		gpf.addFilter(new QLRawFilter("regional_neoId=" + neoIdRegional));

		InstantiableEntityInfo pesquisa = (InstantiableEntityInfo) EntityRegister.getInstance().getCache().getByType("Pesquisa");
		List<NeoObject> newObjPesq = (List<NeoObject>) PersistEngine.getObjects(pesquisa.getEntityClass(), gpf);
		String json = null;
		int iPes = 0;
		ArrayList<Object> pesquisas = new ArrayList<Object>();

		for (NeoObject pesquisaQuery : newObjPesq) {
			PesquisaVO pvo = new PesquisaVO();
			EntityWrapper wrapperPesq = new EntityWrapper(pesquisaQuery);
			
			NeoObject oRegional = (NeoObject) wrapperPesq.findValue("regional");
			EntityWrapper wRegional = new EntityWrapper(oRegional);
			
			String strRegional = "SEMREG";
			
			if (wRegional.findValue("sigla") != null){
				strRegional = NeoUtils.safeOutputString( wRegional.findValue("sigla") );
			}
			
			pvo.setSiglaRegional(strRegional);
			

			pvo.setDesc_pesq(((String) wrapperPesq.getValue("pesquisatxt")).replace("'", "''"));
			
			/*for (int i = 0; i <= newObjPesq.size(); i++) {

			}*/
			
			List<NeoObject> newObjAssun = ((List<NeoObject>) wrapperPesq.findValue("neoidAssunto"));

			for (NeoObject assuntoQuery : newObjAssun) {

				EntityWrapper wrapperAssun = new EntityWrapper(assuntoQuery);

				for (int i = 0; i <= newObjAssun.size(); i++) {
					pvo.setDesc_assun(((String) wrapperAssun.getValue("descassun")).replace("'", "''"));
					//
				}
				List<NeoObject> newObjPerg = ((List<NeoObject>) wrapperAssun.findValue("neoidPergunta"));
				for (NeoObject perguntaQuery : newObjPerg) {

					EntityWrapper wrapperPerg = new EntityWrapper(perguntaQuery);

					for (int i = 0; i <= newObjPerg.size(); i++) {

						pvo.setDesc_perg(((String) wrapperPerg.getValue("perguntatxt")).replace("'", "''"));
					//	pvo.setNeoid_perg("" + perguntaQuery.getNeoId());

					}

				}
				pesquisas.add(pvo);
				Gson gson = new Gson();
				json = gson.toJson(pesquisas);

			}

		}
		System.out.println("Enviando pesquisas para mobile[com.neomind.fusion.custom.orsegups.mobile.send.SendPesquisasServlet]: " + json);
		return json;

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// response.setContentType("application/json");
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");

		PrintWriter out = response.getWriter();
		try {
			out.print(pesquisa(request));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// pesquisa();
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		// response.setContentType("application/json");

		PrintWriter out = response.getWriter();
		out.print(pesquisa(request));
		// pesquisa();

	}
}
