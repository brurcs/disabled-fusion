package com.neomind.fusion.custom.orsegups.presenca.vo;

import java.util.ArrayList;
import java.util.Collection;

import com.neomind.fusion.security.SecurityEntity;

public class ParameterVO
{
	private Long toleranciaEntradaPre = 0L;

	private Long toleranciaEntradaPos = 0L;

	private Long toleranciaSaidaPre = 0L;

	private Long toleranciaSaidaPos = 0L;

	private Long tempoLogPosto = 30L;

	private Long tempoLogColaborador = 30L;

	private Collection<SecurityEntity> permissaoLogPosto = new ArrayList<SecurityEntity>();

	private Collection<SecurityEntity> permissaoLogColaborador = new ArrayList<SecurityEntity>();

	public Long getToleranciaEntradaPre()
	{
		return toleranciaEntradaPre;
	}

	public void setToleranciaEntradaPre(Long toleranciaEntradaPre)
	{
		if (toleranciaEntradaPre != null)
			this.toleranciaEntradaPre = toleranciaEntradaPre;
	}

	public Long getToleranciaEntradaPos()
	{
		return toleranciaEntradaPos;
	}

	public void setToleranciaEntradaPos(Long toleranciaEntradaPos)
	{
		if (toleranciaEntradaPos != null)
			this.toleranciaEntradaPos = toleranciaEntradaPos;
	}

	public Long getToleranciaSaidaPre()
	{
		return toleranciaSaidaPre;
	}

	public void setToleranciaSaidaPre(Long toleranciaSaidaPre)
	{
		if (toleranciaSaidaPre != null)
			this.toleranciaSaidaPre = toleranciaSaidaPre;
	}

	public Long getToleranciaSaidaPos()
	{
		return toleranciaSaidaPos;
	}

	public void setToleranciaSaidaPos(Long toleranciaSaidaPos)
	{
		if (toleranciaSaidaPos != null)
			this.toleranciaSaidaPos = toleranciaSaidaPos;
	}

	public Long getTempoLogPosto()
	{
		return tempoLogPosto;
	}

	public void setTempoLogPosto(Long tempoLogPosto)
	{
		if (tempoLogPosto != null)
			this.tempoLogPosto = tempoLogPosto;
	}

	public Long getTempoLogColaborador()
	{
		return tempoLogColaborador;
	}

	public void setTempoLogColaborador(Long tempoLogColaborador)
	{
		if (tempoLogColaborador != null)
			this.tempoLogColaborador = tempoLogColaborador;
	}

	public Collection<SecurityEntity> getPermissaoLogPosto()
	{
		return permissaoLogPosto;
	}

	public void setPermissaoLogPosto(Collection<SecurityEntity> permissaoLogPosto)
	{
		if (permissaoLogPosto != null)
			this.permissaoLogPosto = permissaoLogPosto;
	}

	public Collection<SecurityEntity> getPermissaoLogColaborador()
	{
		return permissaoLogColaborador;
	}

	public void setPermissaoLogColaborador(Collection<SecurityEntity> permissaoLogColaborador)
	{
		if (permissaoLogColaborador != null)
			this.permissaoLogColaborador = permissaoLogColaborador;
	}

}
