package com.neomind.fusion.custom.orsegups.e2doc.xml.ressarcimento;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Pastas {
    private String modelo;
    private Campos campos;
    private List<Pasta> pasta;
    
    @XmlElement
    public List<Pasta> getPasta() {
        return pasta;
    }
      
    public void setPasta(List<Pasta> pasta) {
        this.pasta = pasta;
    }
    
    @XmlElement
    public String getModelo() {
	return modelo;
    }
    
    public void setModelo(String modelo) {
	this.modelo = modelo;
    }
    
    @XmlElement
    public Campos getCampos() {
	return campos;
    }

    public void setCampos(Campos campos) {
	this.campos = campos;
    }
    
    
}
