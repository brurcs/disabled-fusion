package com.neomind.fusion.custom.orsegups.converter;

import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.portal.PortalUtil;

public class RRCRelacionadaConverter extends StringConverter
{

	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin)
	{
		/*StringBuilder button = new StringBuilder();
		button.append("<script>");
		button.append("		$('label[for*=_atualizaRRC__]').hide(); ");
		button.append("		function atualizaRRC()");
		button.append("		{ ");
		button.append("		"+this.getJSFunction()+" ");
		button.append("		} ");
		button.append("</script>");
		button.append("<div align='center'>");
		button.append("		<input type='button' class='input_button' id='atualizaRRCRelacionado' onClick='javascript:atualizaRRC()' value='Verificar RRC Relacionado' title='Verificar RRC Relacionado'>");
		button.append("</div>");*/
		
		String botao = "		<input type='button' class='input_button' id='atualizaRRCRelacionado' onClick='javascript:atualizaRRC()' value='Verificar RRC Relacionado' title='Verificar RRC Relacionado'>";
		
		return botao;
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return "";
	}
	
	private String getJSFunction()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("var codCliSpan = document.getElementById('id_txt_clienteSapiens__'); ");
		sb.append("if(codCliSpan != null && codCliSpan.innerHTML != null && codCliSpan.innerHTML != '')");
		sb.append("{");
		sb.append("		var codCli = codCliSpan.innerHTML; ");
		sb.append("		var retorno = callSync('"+PortalUtil.getBaseURL()+"servlet/com.neomind.fusion.custom.orsegups.servlets.OrsegupsServletUtils?action=doVerificaRRCRelacionado&codCli='+codCli); ");
		sb.append("		if(retorno == '')");
		sb.append("		{");
		sb.append("			return;");
		sb.append("		}");
		sb.append("		var ids = retorno.split(';');");
		sb.append("		var count = ellist_processEntity__RRCRelacionados__.getRowCount(); ");
		sb.append("		if((count && count > 0))");
		sb.append("		{");
		sb.append("			var i = 0; ");
		sb.append("			try ");
		sb.append("			{");
		sb.append("				for(i=0;i < count;i++) ");
		sb.append("				{");
		sb.append("					ellist_processEntity__RRCRelacionados__.tBody.deleteRow(i); ");
		sb.append("				}");
		sb.append("			}");
		sb.append("			catch(e){}");
		sb.append("		} ");
		sb.append("		");
		sb.append("		if(ids.length > 0)");
		sb.append("		{");
		sb.append("			var i = 0; ");
		sb.append("			for(i=0;i < ids.length;i++) ");
		sb.append("			{");
		sb.append("				try");
		sb.append("				{");
		sb.append("					var id = ids[i]; ");
		sb.append("					ellist_processEntity__RRCRelacionados__._createRow(i); ");
		sb.append("					ellist_processEntity__RRCRelacionados__.setId(i, id); ");
		sb.append("				}");
		sb.append("				catch(e) {}");
		sb.append("			}");
		sb.append("	}	");
		sb.append("}");
		sb.append("else");
		sb.append("{");
		sb.append("		alert('Nenhum cliente selecionado!');");
		sb.append("}");
	
		return sb.toString();
	}
	
}
