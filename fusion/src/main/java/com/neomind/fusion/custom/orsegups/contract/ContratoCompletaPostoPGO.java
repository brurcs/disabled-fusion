package com.neomind.fusion.custom.orsegups.contract;

import java.util.List;

import com.lowagie.text.pdf.SpotColor;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

/**
 * Esta classe será responsável por preencher os dados do posto de PGO com os dados do posto normal
 * @author danilo.silva
 *
 */
public class ContratoCompletaPostoPGO implements AdapterInterface {

	@Override
	public void start(Task origin, EntityWrapper wrapper, Activity activity) {
		try{
			
			NeoObject  postoNormal = null;
			List<NeoObject> listaPostosFusion = (List<NeoObject>) wrapper.findField("postosContrato").getValue();
			for (NeoObject postoFusion : listaPostosFusion)
			{
				EntityWrapper wPosto = new EntityWrapper(postoFusion);
				
				NeoObject oTipoPosto = (NeoObject) wPosto.findValue("tipoPosto");
				EntityWrapper wTipoPosto = new EntityWrapper(oTipoPosto);
				
				if ( NeoUtils.safeOutputString(wTipoPosto.findValue("codigo")).equals("1") ){
					postoNormal = postoFusion;
				}else if ( NeoUtils.safeOutputString(wTipoPosto.findValue("codigo")).equals("2") ){
					ProtecaoGarantidaUtils.atualizaPostoProtecaoGarantida(postoNormal, postoFusion);
				}
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
			throw new WorkflowException("Erro ao Completar posto Posto de proteção garantida");
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
		// TODO Auto-generated method stub
		
	}

}
