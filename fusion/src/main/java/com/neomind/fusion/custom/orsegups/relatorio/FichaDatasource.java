package com.neomind.fusion.custom.orsegups.relatorio;

/**
 * DataSource pra armazenar os dados da grid do relatório.
 * 
 * @author fábio
 * 
 */

public class FichaDatasource
{

	private String produto;
	private String dataEntrega;
	private String nrCertificado;
	private String vistoColaborador;
	private String depositoEntrega;
	private String dataDevolucao;
	private String vistoEmpresa;
	private String depositoDevolucao;
	private String qtdEntrega;
	private String qtdDevolucao;
	private String codigoSapiens;
	private String tamanho;

	public String getQtdEntrega()
	{
		return qtdEntrega;
	}

	public void setQtdEntrega(String qtdEntrega)
	{
		this.qtdEntrega = qtdEntrega;
	}

	public String getQtdDevolucao()
	{
		return qtdDevolucao;
	}

	public void setQtdDevolucao(String qtdDevolucao)
	{
		this.qtdDevolucao = qtdDevolucao;
	}

	public String getProduto()
	{
		return produto;
	}

	public void setProduto(String produto)
	{
		this.produto = produto;
	}

	public String getDataEntrega()
	{
		return dataEntrega;
	}

	public void setDataEntrega(String dataEntrega)
	{
		this.dataEntrega = dataEntrega;
	}

	public String getNrCertificado()
	{
		return nrCertificado;
	}

	public void setNrCertificado(String nrCertificado)
	{
		this.nrCertificado = nrCertificado;
	}

	public String getVistoColaborador()
	{
		return vistoColaborador;
	}

	public void setVistoColaborador(String vistoColaborador)
	{
		this.vistoColaborador = vistoColaborador;
	}

	public String getDepositoEntrega()
	{
		return depositoEntrega;
	}

	public void setDepositoEntrega(String depositoEntrega)
	{
		this.depositoEntrega = depositoEntrega;
	}

	public String getDataDevolucao()
	{
		return dataDevolucao;
	}

	public void setDataDevolucao(String dataDevolucao)
	{
		this.dataDevolucao = dataDevolucao;
	}

	public String getVistoEmpresa()
	{
		return vistoEmpresa;
	}

	public void setVistoEmpresa(String vistoEmpresa)
	{
		this.vistoEmpresa = vistoEmpresa;
	}

	public String getDepositoDevolucao()
	{
		return depositoDevolucao;
	}

	public void setDepositoDevolucao(String depositoDevolucao)
	{
		this.depositoDevolucao = depositoDevolucao;
	}

	public String getCodigoSapiens()
	{
		return codigoSapiens;
	}

	public void setCodigoSapiens(String codigoSapiens)
	{
		this.codigoSapiens = codigoSapiens;
	}

	public String getTamanho()
	{
		return tamanho;
	}

	public void setTamanho(String tamanho)
	{
		this.tamanho = tamanho;
	}
}
