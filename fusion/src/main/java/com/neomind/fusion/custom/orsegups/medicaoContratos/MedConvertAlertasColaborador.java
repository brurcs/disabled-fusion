package com.neomind.fusion.custom.orsegups.medicaoContratos;

import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.DefaultConverter;
import com.neomind.fusion.eform.converter.OriginEnum;

public class MedConvertAlertasColaborador extends DefaultConverter
{

	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin)
	{
		String alertas = "";
		boolean sitOK = true;
		alertas = (String) field.getForm().getField("alertasColaborador").getValue();

		if (alertas == null)
		{
			alertas = "";
		}
		String icone = "";

		if (alertas.contains("falta"))
		{
			icone = icone + "<img src=\"custom/jsp/orsegups/medicaoDeContratos/imagens/Alerta_Vagas.png\"title=\" Colaborador com menos de 30 dias.\"/>";
			sitOK = false;
		}		
		
		if (alertas.contains("retornoINSS"))
		{
			icone = icone + "<img src=\"custom/jsp/orsegups/medicaoDeContratos/imagens/INSS.png\" title=\" Colaborador retornando INSS.\"/>";
			sitOK = false;
		}

		if (sitOK)
		{
			icone = "<img src=\"custom/jsp/orsegups/medicaoDeContratos/imagens/certo2.png\" title=\" Contrato sem alertas\"/>";
		}

		StringBuilder outBuilder = new StringBuilder();
		outBuilder.append(icone);

		return outBuilder.toString();
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}
}
