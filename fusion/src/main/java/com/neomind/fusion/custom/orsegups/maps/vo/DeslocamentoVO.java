package com.neomind.fusion.custom.orsegups.maps.vo;

public class DeslocamentoVO
{
	private ViaturaVO viaturaVO;
	private String ramalOrigem;
	private String regional;
	private String x8IgnoreTags;
	private String filtrosConta;
	private String filtrosNotConta;
	
	
	
	public String getRamalOrigem()
	{
		return ramalOrigem;
	}
	public void setRamalOrigem(String ramalOrigem)
	{
		this.ramalOrigem = ramalOrigem;
	}
	public String getRegional()
	{
		return regional;
	}
	public void setRegional(String regional)
	{
		this.regional = regional;
	}
	public String getX8IgnoreTags()
	{
		return x8IgnoreTags;
	}
	public void setX8IgnoreTags(String x8IgnoreTags)
	{
		this.x8IgnoreTags = x8IgnoreTags;
	}
	public String getFiltrosConta()
	{
		return filtrosConta;
	}
	public void setFiltrosConta(String filtrosConta)
	{
		this.filtrosConta = filtrosConta;
	}
	public String getFiltrosNotConta()
	{
		return filtrosNotConta;
	}
	public void setFiltrosNotConta(String filtrosNotConta)
	{
		this.filtrosNotConta = filtrosNotConta;
	}
	public ViaturaVO getViaturaVO()
	{
		return viaturaVO;
	}
	public void setViaturaVO(ViaturaVO viaturaVO)
	{
		this.viaturaVO = viaturaVO;
	}
	
	
}
