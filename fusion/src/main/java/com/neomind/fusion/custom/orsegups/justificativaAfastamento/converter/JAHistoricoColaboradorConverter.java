package com.neomind.fusion.custom.orsegups.justificativaAfastamento.converter;

import java.text.SimpleDateFormat;
import java.util.List;

import com.neomind.fusion.custom.orsegups.justificativaAfastamento.util.JAUtils;
import com.neomind.fusion.custom.orsegups.justificativaAfastamento.vo.JAHistoricoAfastamentoBean;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.WFProcess;

public class JAHistoricoColaboradorConverter extends StringConverter {

    @Override
    protected String getHTMLInput(EFormField field, OriginEnum origin) {

	Long idPai = field.getForm().getObjectId();
	EntityWrapper wrapper = JAUtils.getEformPai(idPai);

	StringBuilder html = new StringBuilder();

	WFProcess wfprocess = (WFProcess) wrapper.findField("wfprocess").getValue();

	if (wfprocess != null) {

	    String processCode = wfprocess.toString();

	    String arrayCode[] = processCode.split("-");

	    if (arrayCode.length > 0) {
		
		String code = arrayCode[0].trim();

		int numEmp = Integer.valueOf(arrayCode[1].trim());

		int numCad = Integer.valueOf(arrayCode[2].trim());

		String nomeAtividade = JAUtils.getNomeAtividadeAtual(code);

		List<JAHistoricoAfastamentoBean> listaAfastamentos = JAUtils.getListaAfastamentos(JAUtils.getCpfColaborador(numCad, numEmp), code);
		
		html.append("<table class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\" id=\"historicoAfastamento\">");
		html.append("<tbody>");
		html.append("<th>Tarefa</th>");
		html.append("<th>Inicio processo</th>");
		html.append("<th>Status do processo</th>");
		html.append("<th>Data do afastamento</th>");
		html.append("<th>Data fim do afastamento</th>");
		html.append("<th>Situação</th>");
		
		for (JAHistoricoAfastamentoBean afastamento : listaAfastamentos){
		    if (nomeAtividade.equalsIgnoreCase("Validar atestado médico")){
			if (afastamento.getSituacao() == 14 || afastamento.getSituacao() == 3 || afastamento.getSituacao() == 78){
			    this.htmlAppend(html, afastamento);
			}
		    }else{
			this.htmlAppend(html, afastamento);
		    }
		}	
		
		html.append("</tbody>");		
		html.append("</table></br>");

	    }

	}

	return html.toString();

    }
    
    private void htmlAppend(StringBuilder html, JAHistoricoAfastamentoBean afastamento){
	
	String formatoData = "dd/MM/yyyy";
	
	SimpleDateFormat formatador= new SimpleDateFormat(formatoData);
	
	String linkTarefa = afastamento.getLink() != null ? afastamento.getLink() : "";
		
	String statusProcesso = "";
	
	String inicioProcesso = "";
	
	if (afastamento.getDataProcesso() != null){
	    inicioProcesso = formatador.format(afastamento.getDataProcesso().getTime());
	    statusProcesso = afastamento.getStatusProcesso() == 0 ? "Em execução" : "Finalizado";
	}
	
	html.append("<tr>");
	html.append("<td>"+linkTarefa+"</td>");
	html.append("<td>"+inicioProcesso+"</td>");
	html.append("<td>"+statusProcesso+"</td>");
	html.append("<td>" + formatador.format(afastamento.getDataInicio().getTime()) + "</td>");
	html.append("<td>" + formatador.format(afastamento.getDataTermino().getTime()) + "</td>");
	html.append("<td>" + afastamento.getSituacao() + " - " + afastamento.getDescricao() + "</td>");
	html.append("</tr>");
    }
    
    @Override
    protected String getHTMLView(EFormField field, OriginEnum origin) {
	return getHTMLInput(field, origin);
    }

}
