package com.neomind.fusion.custom.orsegups.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.ti.TarefasGrupoUsuarioVO;
import com.neomind.fusion.custom.orsegups.ti.TarefasUsuarioVO;
import com.neomind.fusion.custom.orsegups.ti.TarefasVO;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

/**
 * Classe para objetos do TarefasUtils, onde serão contidos, valores e métodos para o mesmo.
 * 
 * @author William Bastos
 * @version 1.00
 * @since Release 00 da aplicação
 */

public class TarefasUtils
{
	private static final Log log = LogFactory.getLog(TarefasUtils.class);

	/**
	 * Método para retorno do total de tarefas de um determinado fluxo por grupo e usuário
	 * 
	 * @return List - TarefasVO
	 */

	public static List<TarefasGrupoUsuarioVO> pesquisaTarefasPorGrupo(String wfProcess, String model)
	{

		Connection conn = PersistEngine.getConnection("FUSIONPROD");
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		List<TarefasVO> tarefasVOs = null;
		List<TarefasUsuarioVO> tarefasUsuarioVOs = null;
		List<TarefasGrupoUsuarioVO> tarefasGrupoUsuarioVOs = null;

		log.warn("##### INICIAR PESQUISA POR TAREFAS - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		try
		{

			sql.append(" select  p.neoId as wfprocess, p.code, us.neoid as codigoUsuario,see.code as usuario, " );
			sql.append(" us.fullName,ta.titulo,tas.descricao,t.startDate,ta.prazo, se.code as grupo, g.neoId as codigoGrupo ");
			sql.append(" from wfProcess p with(nolock) ");
			sql.append(" inner join activity a with(nolock)  on p.neoId = a.process_neoId ");
			sql.append(" inner join task t with(nolock)  on t.activity_neoid = a.neoid ");
			sql.append(" inner join neouser us with(nolock)  on us.neoid = t.user_neoId ");
			sql.append(" inner join neogroup g with(nolock)  on g.neoId = us.group_neoId ");
			sql.append(" inner join securityentity se with(nolock)  on se.neoid = g.neoid ");
			sql.append(" inner join securityentity see with(nolock)  on see.neoid = us.neoid ");
			sql.append(" inner join d_tarefa ta with(nolock)  on p.entity_neoid = ta.neoid ");
			sql.append(" inner join d_TarefaRegistroAtividades tas with(nolock)   ");
			sql.append(" on tas.neoid = (select min(tar.registroAtividades_neoid) from d_tarefa_registroAtividades tar with(nolock)  where ta.neoid = tar.d_tarefa_neoid ) ");
			sql.append(" where g.neoId IN ( " + wfProcess + " )");
			sql.append(" and p.processState = 0 ");
			String where = "";
			if (NeoUtils.safeIsNotNull(model) && !model.isEmpty() && model.equals("18888"))
			{
				where = " and  p.model_neoId in (select pmaux.neoId from ProcessModel pmaux where pmaux.name like '%G001 - Tarefa Simples%') ";
			}
			else if (NeoUtils.safeIsNotNull(model) && !model.isEmpty() && model.equals("186043073"))
			{
				where = " and  p.model_neoId in (select pmaux.neoId from ProcessModel pmaux where pmaux.name like '%C034 - Solicitar Chip GPRS para Posto%') ";
			}
			
			
			sql.append(where);
			sql.append(" and p.saved = 1 ");
			sql.append(" and t.finishDate is null ");
			sql.append(" and t.startDate is not null ");
			sql.append(" order by se.code,us.fullName asc ");

			pstm = conn.prepareStatement(sql.toString());
			//pstm.setLong(1, Long.parseLong(wfProcess));
			//pstm.setLong(1, Long.parseLong(model));

			rs = pstm.executeQuery();
			tarefasVOs = new ArrayList<TarefasVO>();
			tarefasUsuarioVOs = new ArrayList<TarefasUsuarioVO>();
			tarefasGrupoUsuarioVOs = new ArrayList<TarefasGrupoUsuarioVO>();
			TarefasVO tarefasVO = null;
			TarefasUsuarioVO tarefasUsuarioVO = null;
			TarefasGrupoUsuarioVO tarefasGrupoUsuarioVO = null;
			int cont = 0;
			int total = 0;
			int totalGrupo = 0;
			String nomeAtual = "";
			String grupoAtual = "";
			String neoIdgrupoAtual = "";
			String nomeUsuario = "";
			Long neoIdUsuario = 0L;
			HtmlToText htmlToText = new HtmlToText();
			while (rs.next())
			{
				tarefasVO = new TarefasVO();
				if (nomeAtual != null && !nomeAtual.equals(rs.getString("fullName")) && cont > 0)
				{

					List<NeoObject> listaTarefas = retornaObjects(nomeUsuario);
					verificaTarefas(listaTarefas, tarefasVOs, nomeUsuario);
					tarefasUsuarioVO = new TarefasUsuarioVO();
					tarefasUsuarioVO.setCodigoUsuario(neoIdUsuario);
					tarefasUsuarioVO.setUsuario(nomeAtual);
					tarefasUsuarioVO.setTotal(String.valueOf(total));
					tarefasUsuarioVO.setTarefas(tarefasVOs);
					tarefasUsuarioVOs.add(tarefasUsuarioVO);
					if (grupoAtual != null && !grupoAtual.equals(rs.getString("grupo")) && cont > 0)
					{
						tarefasGrupoUsuarioVO = new TarefasGrupoUsuarioVO();
						tarefasGrupoUsuarioVO.setGrupo(grupoAtual);
						tarefasGrupoUsuarioVO.setTotal(totalGrupo);
						if (grupoAtual.contains("Sistemas"))
							tarefasGrupoUsuarioVO.setId("sistemas");
						tarefasGrupoUsuarioVO.setTarefas(tarefasUsuarioVOs);
						tarefasGrupoUsuarioVOs.add(tarefasGrupoUsuarioVO);
						tarefasUsuarioVOs = new ArrayList<TarefasUsuarioVO>();
						totalGrupo = 0;
					}
					tarefasVOs = new ArrayList<TarefasVO>();
					total = 0;

				}
				neoIdUsuario = rs.getLong("codigoUsuario");
				nomeUsuario = rs.getString("usuario");
				nomeAtual = rs.getString("fullName");
				tarefasVO.setWfProcess(rs.getLong("wfprocess"));
				tarefasVO.setCodigoTarefa(rs.getLong("code"));
				tarefasVO.setNomeCompleto(nomeAtual);
				tarefasVO.setUsuarioCode(nomeUsuario);
				tarefasVO.setTituloTarefa(htmlToText.parse(rs.getString("titulo")));

				GregorianCalendar dataAux = new GregorianCalendar();
				dataAux.setTime(rs.getTimestamp("startDate"));
				tarefasVO.setDataAberturaTarefa(NeoUtils.safeDateFormat(dataAux, "dd/MM/yyyy HH:mm"));
				GregorianCalendar dataAux2 = new GregorianCalendar();
				if(rs.getTimestamp("prazo") != null)
				dataAux2.setTime(rs.getTimestamp("prazo"));
				tarefasVO.setPrazoTarefa(NeoUtils.safeDateFormat(dataAux2, "dd/MM/yyyy HH:mm"));
				grupoAtual = rs.getString("grupo");
				neoIdgrupoAtual = rs.getString("codigoGrupo");
				String descricao = rs.getString("descricao");
				descricao = htmlToText.parse(descricao);
				descricao = descricao.replaceAll("\\<.*?>", "");
				descricao = descricao.replaceAll("&nbsp;", "");
				descricao = descricao.replaceAll("&amp;", "");
				tarefasVO.setDescricaoTarefa(descricao + "\n Prazo:" + tarefasVO.getPrazoTarefa());

				tarefasVOs.add(tarefasVO);
				cont++;
				total++;
				totalGrupo++;

			}
			if (cont > 0)
			{
				List<NeoObject> listaTarefas = retornaObjects(nomeUsuario);
				verificaTarefas(listaTarefas, tarefasVOs, nomeUsuario);
				tarefasUsuarioVO = new TarefasUsuarioVO();
				tarefasUsuarioVO.setUsuario(nomeAtual);
				tarefasUsuarioVO.setTotal(String.valueOf(total));
				tarefasUsuarioVO.setTarefas(tarefasVOs);
				tarefasUsuarioVOs.add(tarefasUsuarioVO);

				tarefasGrupoUsuarioVO = new TarefasGrupoUsuarioVO();
				tarefasGrupoUsuarioVO.setGrupo(grupoAtual);
				tarefasGrupoUsuarioVO.setTotal(totalGrupo);
				if (grupoAtual.contains("Infra"))
					tarefasGrupoUsuarioVO.setId("infra");
				tarefasGrupoUsuarioVO.setTarefas(tarefasUsuarioVOs);
				tarefasGrupoUsuarioVOs.add(tarefasGrupoUsuarioVO);

			}
			log.warn("##### EXECUTAR PESQUISA POR TAREFAS - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		}
		catch (Exception e)
		{
			log.error("##### ERRO PESQUISA POR TAREFAS - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			e.printStackTrace();
		}
		finally
		{

			try
			{

				OrsegupsUtils.closeConnection(conn, pstm, rs);

				for (TarefasGrupoUsuarioVO grupoUsuarioVO : tarefasGrupoUsuarioVOs)
				{

					for (TarefasUsuarioVO tarefasUsuarioVO : grupoUsuarioVO.getTarefas())
					{
						for (TarefasVO tarefasVO : tarefasUsuarioVO.getTarefas())
						{
							Long countAux = 0L;
							if (tarefasVO.getCodigoTarefa() != null)
							{

								QLGroupFilter groupFilter = new QLGroupFilter("AND");
								groupFilter.addFilter(new QLEqualsFilter("tarefa", String.valueOf(tarefasVO.getCodigoTarefa())));
								groupFilter.addFilter(new QLEqualsFilter("modelo", Long.parseLong("18888")));
								List<NeoObject> tarefasObj = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICONTROLETAREFAS"), groupFilter);
								if (tarefasObj != null && !tarefasObj.isEmpty())
								{
									EntityWrapper entityWrapper = new EntityWrapper(tarefasObj.get(0));
									tarefasVO.setPosicaoTarefa((Long) entityWrapper.findValue("posicao"));
									tarefasVO.setTipoTarefa((String) entityWrapper.findValue("tipo"));
									tarefasVO.setSituacaoTarefa((String) entityWrapper.findValue("situacao"));
			
									countAux++;

								}
							}
						}
						Collections.sort((List<TarefasVO>) tarefasUsuarioVO.getTarefas());

					}
				}

			}
			catch (Exception e)
			{
				log.error("##### ERRO PESQUISA POR TAREFAS - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
				e.printStackTrace();
			}

			log.warn("##### FINALIZAR  PESQUISA POR TAREFAS - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		}
		return tarefasGrupoUsuarioVOs;
	}

	private static List<NeoObject> retornaObjects(String code)
	{
		List<NeoObject> tarefasObjs = null;
		try
		{
			if (code != null && !code.isEmpty())
			{
				NeoUser usuario = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", code));
				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				groupFilter.addFilter(new QLEqualsFilter("usuario", usuario));
				groupFilter.addFilter(new QLEqualsFilter("modelo", Long.parseLong("18888")));
				tarefasObjs = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICONTROLETAREFAS"), groupFilter, -1, -1, " posicao asc ");

				if (tarefasObjs != null && !tarefasObjs.isEmpty())
				{
					Long count = 1L;
					for (NeoObject neoObject2 : tarefasObjs)
					{
						NeoObject neoObject = (NeoObject) neoObject2;

						EntityWrapper tarefaWrapper = new EntityWrapper(neoObject);
						tarefaWrapper.findField("posicao").setValue(count);

						PersistEngine.persist(neoObject);

						count++;
					}

				}
				else
				{
					
					QLGroupFilter groupFilterAux = new QLGroupFilter("AND");
					groupFilterAux.addFilter(new QLEqualsFilter("usuario", usuario));
					tarefasObjs = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICONTROLETAREFAS"), groupFilterAux, -1, -1, " posicao asc ");
					if (tarefasObjs != null && !tarefasObjs.isEmpty())
					{

						for (NeoObject neoObjectAux : tarefasObjs)
						{
							NeoObject neoObject = (NeoObject) neoObjectAux;

							EntityWrapper tarefaWrapper = new EntityWrapper(neoObject);
							if (tarefaWrapper.findValue("modelo") == null)
							{
								tarefaWrapper.findField("modelo").setValue(Long.parseLong("18888"));

								PersistEngine.persist(neoObject);
							}

						}

					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO AO PESQUISAR TAREFAS : " + e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));

		}
		finally
		{
			return tarefasObjs;
		}

	}

	private static void verificaTarefas(List<NeoObject> tarefasFila, List<TarefasVO> tarefas, String neoIdUsuario)
	{
		List<NeoObject> tarefasObjs = null;
		List<String> tarefasStr = new ArrayList<String>();
		try
		{
			if (tarefasFila != null && !tarefasFila.isEmpty())
			{

				boolean flag = false;
				for (NeoObject tarefaVO : tarefasFila)
				{
					EntityWrapper entityWrapper = new EntityWrapper(tarefaVO);
					if (entityWrapper != null)
					{
						if ((String) entityWrapper.findValue("tarefa") != null)
						{

							tarefasStr.add((String) entityWrapper.findValue("tarefa"));
						}
					}

				}
			}
			if (tarefasFila != null && !tarefasFila.isEmpty() && tarefas != null && !tarefas.isEmpty())
			{
				Collection<NeoObject> lista = new ArrayList<NeoObject>();
				for (NeoObject tarefaVO : tarefasFila)
				{	
					boolean flag = false;
					EntityWrapper entityWrapper = new EntityWrapper(tarefaVO);
					if (entityWrapper != null)
					{
						for (TarefasVO tarefaVOs : tarefas)
						{
							if(((String) entityWrapper.findValue("tarefa")).contains(tarefaVOs.getCodigoTarefa().toString()))
							{
								flag = true;
							}
						}
						if(!flag)
							lista.add(tarefaVO);
					}
				}
				PersistEngine.removeObjects(lista);
			}
			if (tarefas != null && !tarefas.isEmpty() && neoIdUsuario != null && !neoIdUsuario.isEmpty())
			{
				NeoUser usuarioResponsavel = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", neoIdUsuario));
				if (usuarioResponsavel != null)
				{
					Long tamanhaLista = 0L;
					for (TarefasVO tarefaVO : tarefas)
					{
						if (tarefasStr == null || tarefasStr.isEmpty() || (usuarioResponsavel != null && !usuarioResponsavel.getCode().isEmpty() && tarefasStr != null && !tarefasStr.isEmpty() && !tarefasStr.contains(tarefaVO.getCodigoTarefa().toString())))
						{

							tarefasStr.add(tarefaVO.getCodigoTarefa().toString());
							tamanhaLista = Long.valueOf(tarefasStr.size());
							NeoObject noPS = AdapterUtils.createNewEntityInstance("TICONTROLETAREFAS");

							EntityWrapper psWrapper = new EntityWrapper(noPS);

							psWrapper.findField("posicao").setValue(tamanhaLista);
							psWrapper.findField("tarefa").setValue(String.valueOf(tarefaVO.getCodigoTarefa()));
							psWrapper.findField("usuario").setValue(usuarioResponsavel);
							psWrapper.findField("modelo").setValue(Long.parseLong("18888"));
							psWrapper.findField("tipo").setValue("S");
							psWrapper.findField("situacao").setValue("E");
							
			

							PersistEngine.persist(noPS);

						}
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO AO PESQUISAR TAREFAS : " + e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));

		}
		finally
		{
			log.warn("##### FINALIZAR PESQUISAR TAREFAS  - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}

	}

	public static void ordenaLista(List<TarefasUsuarioVO> tarefasUsuarioVO)
	{
		try
		{
			for (TarefasUsuarioVO tarefasUsuarioVO2 : tarefasUsuarioVO)
			{
				for (TarefasVO tarefasVO : tarefasUsuarioVO2.getTarefas())
				{

					QLGroupFilter filterAux = new QLGroupFilter("AND");
					NeoUser usuario = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", tarefasVO.getUsuarioCode()));
					if (usuario != null)
					{
						filterAux.addFilter(new QLEqualsFilter("usuario", usuario));
						filterAux.addFilter(new QLEqualsFilter("modelo", Long.parseLong("18888")));
						List<NeoObject> tarefas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICONTROLETAREFAS"), filterAux, -1, -1, "posicao asc");

						if (tarefas != null && !tarefas.isEmpty())
						{

							Long count = 1L;
							for (NeoObject neoObject : tarefas)
							{
								NeoObject neoObject2 = (NeoObject) neoObject;

								EntityWrapper tarefaWrapper = new EntityWrapper(neoObject2);
								tarefaWrapper.findField("posicao").setValue(count);

								PersistEngine.persist(neoObject2);

								count++;
							}
							PersistEngine.getEntityManager().flush();

						}
						else
						{
							QLGroupFilter groupFilterAux = new QLGroupFilter("AND");
							groupFilterAux.addFilter(new QLEqualsFilter("usuario", usuario));
							List<NeoObject> tarefasObjs = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TICONTROLETAREFAS"), groupFilterAux, -1, -1, " posicao asc ");
							if (tarefasObjs != null && !tarefasObjs.isEmpty())
							{

								for (NeoObject neoObjectAux : tarefasObjs)
								{
									NeoObject neoObject = (NeoObject) neoObjectAux;

									EntityWrapper tarefaWrapper = new EntityWrapper(neoObject);
									if (tarefaWrapper.findValue("modelo") == null)
									{
										tarefaWrapper.findField("modelo").setValue(Long.parseLong("18888"));

										PersistEngine.persist(neoObject);
									}

								}

							}
						}

					}
					break;
				}

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

}
