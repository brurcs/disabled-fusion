package com.neomind.fusion.custom.orsegups.processoJuridico.civel.adapter;

import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.RegistraHistoricoGTCs

public class RegistraHistoricoGTCs implements AdapterInterface
{
	@Override
	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{
			origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);

			// GTC a ser lançada
			List<NeoObject> listaGTCs = (List<NeoObject>) processEntity.findGenericValue("listaPagamentos");
			List<NeoObject> listaGTCsLancar = processEntity.findGenericValue("listaGTCLancar");

			for (NeoObject noGtc : listaGTCs)
			{
				EntityWrapper wGtc = new EntityWrapper(noGtc);

				List<NeoObject> listParcelas = wGtc.findGenericValue("parcelas");
				if (listParcelas != null && listParcelas.size() > 0)
				{
					for (NeoObject noParcela : listParcelas)
					{
						EntityWrapper wParcela = new EntityWrapper(noParcela);

						NeoObject noGtcPgto = AdapterUtils.createNewEntityInstance("GTCGestaoTitulosControladoria");
						EntityWrapper wGtcPgto = new EntityWrapper(noGtcPgto);

						NeoObject noEmpresa = processEntity.findGenericValue("empresa");
						NeoObject noFilial = processEntity.findGenericValue("filial");

						// centro de custo informado pelo dpto jurídico
						String lCodccu = processEntity.findGenericValue("centroCusto").toString();
						QLFilter qlCodccu = new QLEqualsFilter("codccu", lCodccu);

						List<NeoObject> listCcu = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), qlCodccu);
						if (listCcu != null && listCcu.size() > 0)
							wGtcPgto.setValue("centroCusto", listCcu.get(0));

						if (noEmpresa != null)
						{
							EntityWrapper wEmpresa = new EntityWrapper(noEmpresa);

							QLFilter qlEmpresa = new QLEqualsFilter("codemp", (Long) wEmpresa.findGenericValue("codemp"));
							QLFilter qlCtaFin = new QLEqualsFilter("ctafin", 122L);

							QLGroupFilter groupFilter = new QLGroupFilter("AND");
							groupFilter.addFilter(qlEmpresa);
							groupFilter.addFilter(qlCtaFin);

							List<NeoObject> listaCtaFinanceira = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE091PLF"), groupFilter);

							if (listaCtaFinanceira != null && listaCtaFinanceira.size() > 0)
								wGtcPgto.setValue("contaFinanceira", listaCtaFinanceira.get(0));

							QLFilter qlEmpresaContabil = new QLEqualsFilter("codemp", (Long) wEmpresa.getValue("codemp"));
							QLFilter qlCtaContabil = new QLEqualsFilter("ctared", 32335L);

							QLGroupFilter groupFilterContabil = new QLGroupFilter("AND");
							groupFilterContabil.addFilter(qlEmpresaContabil);
							groupFilterContabil.addFilter(qlCtaContabil);

							List<NeoObject> listCtaContabil = PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE045PLA"), groupFilterContabil);

							if (listCtaContabil != null && listCtaContabil.size() > 0)
								wGtcPgto.setValue("contaContabil", listCtaContabil.get(0));
						}
						
						wGtcPgto.setValue("usuarioResponsavel", wGtc.getValue("usuarioResponsavel"));
						wGtcPgto.setValue("listaLancamento", wGtc.getValue("listaLancamento"));
						wGtcPgto.setValue("empresa", noEmpresa);
						wGtcPgto.setValue("codigoFilial", noFilial);
						wGtcPgto.setValue("titulo", wGtc.getValue("titulo"));
						wGtcPgto.setValue("tipoTitulo", wGtc.getValue("tipoTitulo"));
						wGtcPgto.setValue("fornecedor", wGtc.getValue("fornecedor"));
						wGtcPgto.setValue("observacao", wGtc.getValue("observacao"));
						wGtcPgto.setValue("valorOriginal", wParcela.getValue("valorParcela"));
						wGtcPgto.setValue("vencimentoProrrogado", wParcela.getValue("vencimento"));
						wGtcPgto.setValue("vencimentoOriginal", wParcela.getValue("vencimento"));
						wGtcPgto.setValue("formaPagamento", wGtc.getValue("formaPagamento"));
						wGtcPgto.setValue("codigoAgencia", wGtc.getValue("codigoAgencia"));
						wGtcPgto.setValue("codigoBanco", wGtc.getValue("codigoBanco"));
						wGtcPgto.setValue("contaBanco", wGtc.getValue("contaBanco"));
						wGtcPgto.setValue("j002Anexo", wGtc.getValue("j002Anexo"));
						wGtcPgto.setValue("j002ClaPgtos", wGtc.getValue("j002ClaPgtos"));
						wGtcPgto.setValue("isDefinitivo", wGtc.getValue("isDefinitivo"));
						
						listaGTCsLancar.add(noGtcPgto);

						/*
						 * NeoObject noRegAti =
						 * AdapterUtils.createNewEntityInstance("j002HistoricoGTCs");
						 * EntityWrapper wRegAti = new EntityWrapper(noRegAti);
						 * wRegAti.setValue("neoIdGtc", wGtc.getValue("neoId"));
						 * wRegAti.setValue("StatusGTC", "Pendente de Lançamento");
						 */
					}
				}
			}
			
			processEntity.setValue("listaPagamentos", null);
		}
		catch (Exception e)
		{
			System.out.println("Erro na classe RegistraHistoricoGTCs do fluxo J003.");
			e.printStackTrace();
			throw new WorkflowException(e.getMessage());
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
