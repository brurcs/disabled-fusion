package com.neomind.fusion.custom.orsegups.teste;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import us.monoid.json.JSONObject;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

@WebServlet(name="LucasTesteServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.teste.LucasTesteServlet"})
public class LucasTesteServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	private static String WEB_SERVICE_SIGMA = "http://192.168.20.218:8080/SigmaWebServices/";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	private void doRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		final PrintWriter out = resp.getWriter();

		String action = "";
		if (req.getParameter("action") != null)
		{
			action = req.getParameter("action");
		}
		
		if (action.equalsIgnoreCase("load"))
		{
			List<NeoObject> objs = PersistEngine.getObjects(AdapterUtils.getEntityClass("USUVNFVBDC"));
		}
		
		if (action.equalsIgnoreCase("rotinaEmailComprovantePonto"))
		{
			this.rotinaEmailComprovantePonto();
		}
		
		if (action.equalsIgnoreCase("telefone"))
		{
			List<NeoObject> telefones = PersistEngine.getObjects(AdapterUtils.getEntityClass("TELECOMCelulares"));
			
			for (NeoObject obj : telefones)
			{
				EntityWrapper wrapper = new EntityWrapper(obj); 
				List<NeoObject> postos = (List<NeoObject>) wrapper.findValue("centroCustoTelefone");
				
				if (NeoUtils.safeIsNotNull(postos))
				{
					for (NeoObject pos : postos)
					{
						EntityWrapper wrapperPosto = new EntityWrapper(pos); 
						String cc = (String) wrapperPosto.findValue("centroCusto");
						List<NeoObject> vorgs = PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORH_USU_Vorg203nv8"), new QLEqualsFilter("usu_codccu", cc));
						if (NeoUtils.safeIsNotNull(vorgs) && vorgs.size() > 0)
						{
							wrapperPosto.setValue("posto", vorgs.get(0));
							PersistEngine.persist(pos);
						}
					}
				}
			}
		}

		if (action.equalsIgnoreCase("ws"))
		{
			//String URL = (WEB_SERVICE_SIGMA + "ReceptorOSWebService?responsibleTechnician=11010&orderId=1430362");
			try
			{
				/*
				 * clientId=?*
				 * &centralId=?*
				 * &partition=?*
				 * &companyId=?*
				 * &responsibleTechnician=?*
				 * &defect=?*
				 * &requester=?*
				 * &description=?
				 * &estimatedTime=?
				 * &scheduledTime=?
				 */
				
				String urlParameters = "responsibleTechnician=107655&defect=166&requester=10011&description=Teste&scheduledTime=2014-12-03T00:00:00Z&orderId=1451418";
				String request = WEB_SERVICE_SIGMA + "ReceptorOSWebService";
				URL url = new URL(request);
				URLConnection conn = url.openConnection();

				conn.setDoOutput(true);

				OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());

				writer.write(urlParameters);
				writer.flush();

				String line;
				BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

				while ((line = reader.readLine()) != null)
				{
					JSONObject json = new JSONObject(line);
					if (json.getString("status").equals("success"))
					{
						System.out.println("OK");
					}
					else
					{
						System.out.println("ERRO = " +json);
					}
				}
				writer.close();
				reader.close();
			}
			catch (Exception e)
			{
				throw new WorkflowException("Erro ao abrir OS no SIGMA, favor informar a TI. " + e);
			}

		}
		out.close();
	}
	
	public void rotinaEmailComprovantePonto()
	{
		Connection conn = PersistEngine.getConnection("VETORHPROD");
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.RotinaEmailComprovantePonto");
		log.warn("##### INICIO AGENDADOR DE TAREFA: Rotina Email ComprovantePonto - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		
		GregorianCalendar lastDate = new GregorianCalendar();
		GregorianCalendar currentDate = new GregorianCalendar();
		currentDate.add(Calendar.MINUTE, -1);
		currentDate.set(Calendar.SECOND, 0);
		currentDate.set(Calendar.MILLISECOND, 0);
		
		
		try
		{
			List<NeoObject> emails = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("QLEmailComprovantePonto"));
			NeoObject email = emails.get(0);
			EntityWrapper wrapper = new EntityWrapper(email);
			lastDate = (GregorianCalendar) wrapper.findValue("lastDate");
			System.out.println(NeoUtils.safeDateFormat(lastDate));
			System.out.println(NeoUtils.safeDateFormat(currentDate));
			
			sql.append(" SELECT TOP 1 acc.numcra, fun.nomfun, DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) as data, acc.horacc, fil.numcgc, fun.numpis ");
			sql.append(" FROM R070ACC acc ");
			sql.append(" INNER JOIN R038HCH hch ON hch.NumCra = acc.NumCra ");
			sql.append(" INNER JOIN R034FUN fun ON fun.NumEmp = hch.NumEmp AND fun.NumCad = hch.NumCad AND fun.TipCol = hch.TipCol ");
			sql.append(" INNER JOIN R030FIL fil on fil.codfil = fun.CodFil and fil.numemp = fun.NumEmp ");
			sql.append(" WHERE acc.CodPlt = 2 AND acc.CodRlg = 1 AND acc.TipAcc >= 100 ");
			sql.append(" AND DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) >= ? AND DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) < ? ");
			sql.append(" GROUP BY acc.numcra, fun.numemp, fun.numcad, fun.nomfun, acc.HorAcc, acc.DatAcc, fil.numcgc, fun.numpis ");
			sql.append(" ORDER BY DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) desc ");
			
			pstm = conn.prepareStatement(sql.toString());
			pstm.setTimestamp(1, new Timestamp(lastDate.getTimeInMillis()));
			pstm.setTimestamp(2, new Timestamp(currentDate.getTimeInMillis()));
			
			rs = pstm.executeQuery();

			while (rs.next())
			{
				String numcra = rs.getString("numcra");
				String nomfun = rs.getString("nomfun");
				
				Timestamp dataAcesso = rs.getTimestamp("data");
				GregorianCalendar gData = new GregorianCalendar();
				gData.setTimeInMillis(dataAcesso.getTime());
				
				Long horacc = rs.getLong("horacc");
				String numcgc = rs.getString("numcgc");
				String numpis = rs.getString("numpis");
			
				if (NeoUtils.safeIsNotNull(numcra) && NeoUtils.safeIsNotNull(nomfun) && NeoUtils.safeIsNotNull(gData) && NeoUtils.safeIsNotNull(horacc) && 
						NeoUtils.safeIsNotNull(numcgc) && NeoUtils.safeIsNotNull(numpis)) 
				{
					String cnpj = String.format("%014d", Long.parseLong(numcgc));
					cnpj = OrsegupsUtils.formatarString(cnpj, "##.###.###/####-##");
					
					String hora = String.format("%04d", horacc);
					
					StringBuilder html = new StringBuilder();
					
					html.append("<!doctype html>");
					html.append("<html>");
					html.append("<head>");
					html.append("<meta charset=\"utf-8\">");
					html.append("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
					html.append("<title>Comprovante de Registro de Ponto do Trabalhador</title>");
					html.append("</head>");
					html.append("<body>");
					html.append("<table width=\"100%\"  cellspacing=\"0\" cellpadding=\"0\">");
					html.append("  <tbody>");
					html.append("    	<tr>");
					html.append("      	<td>");
					html.append("        	<table width=\"460\"  align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin-top:10px;\">");
					html.append("          	<!-- Main Wrapper Table with initial width set to 60opx -->");
					html.append("          		<tbody style=\"background-color:#FFFEB6;padding:10px;\">");
					html.append("            		<tr> ");
					html.append("              		<!-- Introduction area -->");
					html.append("              			<td>");
					html.append("                        	<table width=\"96%\"  align=\"left\" cellpadding=\"0\" cellspacing=\"0\">");
					html.append("                  				<tr> ");
					html.append("                    			<!-- row container for TITLE/EMAIL THEME -->");
					html.append("                    				<td align=\"center\" style=\"font-size: 20px; font-weight: 900; line-height: 1em; color: #333; font-family:monospace;padding:20px;border-bottom:1px dashed #333;\">COMPROVANTE DE REGISTRO DE PONTO DO TRABALHADOR</td>");
					html.append("                  				</tr>");
					html.append("                  				<tr>");
					html.append("                    				<td style=\"font-size: 0; line-height: 0;\" height=\"20\"><table width=\"96%\" align=\"left\"  cellpadding=\"0\" cellspacing=\"0\">");
					html.append("                        				<tr> ");
					html.append("                          				<!-- HTML Spacer row -->");
					html.append("                          					<td style=\"font-size: 0; line-height: 0;\" height=\"20\">&nbsp;</td>");
					html.append("                        				</tr>");
					html.append("                      		</table></td>");
					html.append("                  		</tr>");
					html.append("                  		<tr> ");
					html.append("                    		<!-- Row container for Intro/ Description -->");
					html.append("                    		<td align=\"left\" style=\"font-size: 18px; font-style: normal; font-weight: normal; color: #000; line-height: 1.5em; text-align:justify; padding:10px 20px 20px 20px; font-family:monospace;\">");
					html.append("                    		CNPJ: " + cnpj + "<br />");
					html.append("                    		NOME: " + nomfun + " <br />");
					html.append("                    		PIS: " + numpis + " <br />");
					html.append("                    		DATA: " + NeoUtils.safeDateFormat(gData, "dd/MM/yyyy") + "<br>");
					html.append("                    		HORA: " + NeoUtils.safeDateFormat(gData, "HH:mm") + "<br/>");
					html.append("                    		NSR: " + numcra + NeoUtils.safeDateFormat(gData, "yyyyMMdd") + hora + "</td>");
					html.append("                  		</tr>");
					html.append("                	</table></td>");
					html.append("            </tr>");
					html.append("          </tbody>");
					html.append("        </table></td>");
					html.append("    </tr>");
					html.append("  </tbody>");
					html.append("</table>");
					html.append("</body>");
					html.append("</html>");

					String origem = "fusion@orsegups.com.br";
					String destino = "sindicatopresenca@gmail.com";
					String titulo = "COMPROVANTE DE REGISTRO DE PONTO DO TRABALHADOR - " + nomfun;
					
					//FIXME comentado pois o metodo usado nao estas disponivel
					//OrsegupsUtils.sendEmail2SendGrid(destino, origem, titulo, "", html.toString());
					/*
					String url = "http://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/emailComprovantePonto.jsp";
					Map<String, Object> paramMap = new HashMap<String, Object>();
					paramMap.put("cnpj",cnpj);
					paramMap.put("nome",nomfun);
					paramMap.put("pis",numpis);
					paramMap.put("data",NeoUtils.safeDateFormat(gData, "dd/MM/yyyy"));
					paramMap.put("hora",NeoUtils.safeDateFormat(gData, "HH:mm"));
					paramMap.put("nsr",numcra + NeoUtils.safeDateFormat(gData, "yyyyMMdd") + hora);
					
					FusionRuntime.getInstance().getMailEngine().sendEMail(destino, url, paramMap);	
					*/
				}	
			}
		}
		catch (Exception e)
		{
			log.error("##### AGENDADOR DE TAREFA: Rotina Email ComprovantePonto");
			e.printStackTrace();
		}
		finally
		{

			try
			{
				rs.close();
				pstm.close();
				conn.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}

			log.warn("##### FIM AGENDADOR DE TAREFA: Rotina Email ComprovantePonto - LastDate: " + NeoUtils.safeDateFormat(lastDate) + " - CurrentDate: " + NeoUtils.safeDateFormat(currentDate));
		}
	}
}
