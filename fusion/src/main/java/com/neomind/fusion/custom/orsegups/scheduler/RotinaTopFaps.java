package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.RotinaFapVO;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;

/**
 * 
 * @author herisson.ferreira
 *
 */
public class RotinaTopFaps implements CustomJobAdapter{

	private static final Log log = LogFactory.getLog(RotinaTopFaps.class);
	
	Long logKey = GregorianCalendar.getInstance().getTimeInMillis();
	
	@Override
	public void execute(CustomJobContext ctx) {
		
		// Log
		
		log.error("##### INICIO AGENDADOR DE TAREFA: Rotina TOP Faps - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		System.out.println("##### INICIO AGENDADOR DE TAREFA: Rotina TOP Faps - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
				
		// Conexão
		
		Connection conn = PersistEngine.getConnection("");
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		String razao = null;
		String conta = null;
		String endereco = null;
		
		Long codigo = 0L;
		Long cliente = 0L;
        
	    List <RotinaFapVO> rotinaFapVO = new ArrayList<RotinaFapVO>();
		
		try {
			
			// Privado
			sql.append("SELECT TOP 50 COUNT(wfp.code) TAREFAS, DBCENT.id_central CONTA, DBCENT.razao RAZAO, DBCENT.endereco ENDERECO, FAPAG.codigo CODIGO, DBCENT.cd_cliente CLIENTE ");
			sql.append("FROM D_FAPAutorizacaoDePagamento FAP ");
			sql.append("INNER JOIN D_FAPAplicacaoDoPagamento FAPAG ON FAPAG.neoId = FAP.aplicacaoPagamento_neoId ");
			sql.append("AND FAPAG.codigo = FAP.codigoAplicacaoPagamento ");
			sql.append("INNER JOIN WFProcess WFP ON WFP.neoId = FAP.wfprocess_neoId ");
			sql.append("INNER JOIN X_SIGMACENTRAL SIG ON SIG.neoId = FAP.contaSigma_neoId ");
			sql.append("INNER JOIN [FSOODB03\\SQL01].SIGMA90.dbo.dbCentral DBCENT ON SIG.cd_cliente = DBCENT.cd_cliente ");
			sql.append("WHERE WFP.startDate >= GETDATE()-90 AND FAPAG.codigo = 2 ");
			sql.append("GROUP BY DBCENT.id_central, DBCENT.razao,DBCENT.endereco, FAPAG.codigo, DBCENT.cd_cliente ");
			sql.append("HAVING COUNT(wfp.code) >= 5 ");
			
			sql.append("UNION ALL ");
			
			// Público
			sql.append("SELECT TOP 50 COUNT(wfp.code) TAREFAS, DBCENT.id_central CONTA, DBCENT.razao RAZAO, DBCENT.endereco ENDERECO, FAPAG.codigo CODIGO, DBCENT.cd_cliente CLIENTE ");
			sql.append("FROM D_FAPAutorizacaoDePagamento FAP ");
			sql.append("INNER JOIN D_FAPAplicacaoDoPagamento FAPAG ON FAPAG.neoId = FAP.aplicacaoPagamento_neoId ");
			sql.append("AND FAPAG.codigo = FAP.codigoAplicacaoPagamento ");
			sql.append("INNER JOIN WFProcess WFP ON WFP.neoId = FAP.wfprocess_neoId ");
			sql.append("INNER JOIN X_SIGMACENTRAL SIG ON SIG.neoId = FAP.contaSigma_neoId ");
			sql.append("INNER JOIN [FSOODB03\\SQL01].SIGMA90.dbo.dbCentral DBCENT ON SIG.cd_cliente = DBCENT.cd_cliente ");
			sql.append("WHERE WFP.startDate >= GETDATE()-90 AND FAPAG.codigo = 3 ");
			sql.append("GROUP BY DBCENT.id_central, DBCENT.razao,DBCENT.endereco, FAPAG.codigo, DBCENT.cd_cliente ");
			sql.append("HAVING COUNT(wfp.code) >= 5 ");
			sql.append("ORDER BY TAREFAS DESC");
			
			pstm = conn.prepareStatement(sql.toString());

			rs = pstm.executeQuery();
			
			while (rs.next()){
				
				// Capturar & Armazenar o resultado da consulta
				
				RotinaFapVO rotinaFAPList = new RotinaFapVO();
				
				razao = rs.getString("RAZAO");
				conta = rs.getString("CONTA");
				endereco = rs.getString("ENDERECO");
				codigo = rs.getLong("CODIGO");
				cliente = rs.getLong("CLIENTE");	
				
				rotinaFAPList.setRazaoSocial(razao);
				rotinaFAPList.setConta(conta);
				rotinaFAPList.setEndereco(endereco);
				rotinaFAPList.setCodigo(codigo);
				rotinaFAPList.setCliente(cliente);
				
				rotinaFapVO.add(rotinaFAPList);
			}
			
			Iterator<RotinaFapVO> iterator = rotinaFapVO.iterator();
			
			while(iterator.hasNext()){
			
			RotinaFapVO fap = iterator.next();
			
			final String tipEmc = fap.getCodigo() == 2 ? "PRIVADO" : "PÚBLICO"; // Para diferenciar no título os clientes públicos dos privados.
	        
			// Descrição da Tarefa
			
		    StringBuilder descricaoSB = new StringBuilder();
			
	        descricaoSB.append(" <table border=\"1\">");
			descricaoSB.append(" <tr>"); 
			descricaoSB.append(" <td> Analisar cliente com excesso de FAP nos últimos 90 dias. </td> ");
			descricaoSB.append(" </tr> <tr>");
			descricaoSB.append(" <td> Razão Social: "+ fap.getRazaoSocial() +"</td> ");
			descricaoSB.append(" </tr> <tr>");
			descricaoSB.append(" <td> Endereço: "+ fap.getEndereco() +"</td> ");
			descricaoSB.append(" </tr> <tr>");
			descricaoSB.append(" <td> Conta: "+ fap.getConta() +"</td> ");
			descricaoSB.append(" </tr>");
			descricaoSB.append(" </table>");
			
			NeoFile anexo = gerarPlanilhaFaps(fap.getConta(), fap.getCodigo(), fap.getCliente());
				
			try {				
				abrirTarefaFap(descricaoSB.toString(), tipEmc, fap.getConta(), anexo);	
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error("ERRO NA ROTINA DE TOP FAPS ["+ logKey + "]");
			throw new JobException("Erro na Rotina de TOP Faps [ "+logKey+"] ");
		}finally{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
			log.error("##### FIM AGENDADOR DE TAREFA: Rotina TOP Faps - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			System.out.println("##### FIM AGENDADOR DE TAREFA: Rotina TOP Faps - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
		
	}
	
	public void abrirTarefaFap (String descricao, String tipEmc, String conta, NeoFile anexo){
		
		String papelSolicitante = null;
		String papelExecutor = null;
		String usuarioSolicitante = null;
		String usuarioExecutor = null;

		// Solicitante & Executor
		
		papelSolicitante = "SolicitanteTarefaFAPTopTen";
		papelExecutor = "ExecutorTarefaFAPTopTen";

		usuarioSolicitante = OrsegupsUtils.getUserNeoPaper(papelSolicitante);
		usuarioExecutor = OrsegupsUtils.getUserNeoPaper(papelExecutor);

		// Título
		
		String titulo = null;
		
		titulo = "TOP TEN FAP " + tipEmc + " BA: " + conta;
		
		// Prazo
		
		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 30L);
	    prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
	    prazo.set(GregorianCalendar.MINUTE, 59);
	    prazo.set(GregorianCalendar.SECOND, 59);
	    
	    IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
	    
	    iniciarTarefaSimples.abrirTarefa(usuarioSolicitante, usuarioExecutor, titulo, descricao, "1", "sim", prazo, anexo);
	}
	
	public NeoFile gerarPlanilhaFaps(String conta, Long tipEmc, Long cliente){
		
		// Conexão
		
		Connection conn = PersistEngine.getConnection("");
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		NeoFile anexo = null;
		
		try {
			
			sql.append("SELECT wfp.code TAREFA, wfp.startDate INICIO, wfp.finishDate FIM, FAP.valorTotal VALOR ");
			sql.append("FROM D_FAPAutorizacaoDePagamento FAP ");
			sql.append("INNER JOIN D_FAPAplicacaoDoPagamento FAPAG ON FAPAG.neoId = FAP.aplicacaoPagamento_neoId ");
			sql.append("AND FAPAG.codigo = FAP.codigoAplicacaoPagamento ");
			sql.append("INNER JOIN WFProcess WFP ON WFP.neoId = FAP.wfprocess_neoId ");
			sql.append("INNER JOIN X_SIGMACENTRAL SIG ON SIG.neoId = FAP.contaSigma_neoId ");
			sql.append("INNER JOIN [FSOODB03\\SQL01].SIGMA90.dbo.dbCentral DBCENT ON SIG.cd_cliente = DBCENT.cd_cliente ");
			sql.append("WHERE DBCENT.id_central = ? AND DBCENT.cd_cliente = ? AND FAPAG.codigo = ? AND WFP.startDate >= GETDATE()-90 ");
			sql.append("GROUP BY wfp.code, DBCENT.razao, wfp.startDate, wfp.finishDate, FAP.valorTotal ");
			
			pstm = conn.prepareStatement(sql.toString());

			pstm.setString(1, conta);
			pstm.setString(2, cliente.toString());
			pstm.setString(3, tipEmc.toString());
			
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("Cancelamentos");
			int rownum = 0;
			Cell cell;
			Row row;
			
			Map<Integer, String> mapa = new HashMap<>();

			rs = pstm.executeQuery();
			
			row = sheet.createRow(rownum++);
			
			// Colunas
			
			row.createCell(0).setCellValue("Tarefa");
			row.createCell(1).setCellValue("Data de Início");
			row.createCell(2).setCellValue("Data de Fim");
			row.createCell(3).setCellValue("Valor");

			
			while (rs.next()){
				
				// Preenchimento dos dados
				
				mapa.put(0, rs.getString("TAREFA"));
				mapa.put(1, rs.getString("INICIO"));
				mapa.put(2, rs.getString("FIM"));
				mapa.put(3, rs.getString("VALOR"));
				
				row = sheet.createRow(rownum++);
				
				for(Integer key: mapa.keySet()){
					cell = row.createCell(key);
					cell.setCellValue(mapa.get(key));
				}
				
			}

			// Geração do Arquivo Temporário - Anexo
			
			File file = File.createTempFile("FAPS_CONTA_"+ conta +"_",".xls", null);
			FileOutputStream out = new FileOutputStream(file);
			workbook.write(out);
			out.close();
			
			anexo = OrsegupsUtils.criaNeoFile(file);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new JobException("Erro ao gerar a planilha na Rotina Top Faps [ "+ logKey +" ]");
		}
		
		return anexo;
		
	}
}
