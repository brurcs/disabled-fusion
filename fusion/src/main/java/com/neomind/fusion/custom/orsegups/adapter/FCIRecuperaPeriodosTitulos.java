package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.GregorianCalendar;

import javax.persistence.Query;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.datawarehouse.ExternalEntityInfo;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class FCIRecuperaPeriodosTitulos implements AdapterInterface
{
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		String nomeProcesso = activity.getProcessName();
		
		String nomeFonteDados = "SAPIENS";
		
		String codemp = processEntity.findValue("contratoSapiens.usu_codemp").toString();
		String codfil = processEntity.findValue("contratoSapiens.usu_codfil").toString();
		String numctr = processEntity.findValue("contratoSapiens.usu_numctr").toString();
		String codcli = processEntity.findValue("contratoSapiens.codcli").toString();

		/**
		 * @author orsegups lucas.avila - removido TOP.
		 * @date 08/07/2015
		 */
		String sql = "	SELECT (CASE WHEN DATEDIFF(DAY, E301TCR.VCTPRO, GETDATE()) > 30 THEN 1 ELSE 0 END)" +
					 "  FROM E301TCR " + 
					 "  INNER JOIN E001TNS ON E001TNS.CODEMP = E301TCR.CODEMP AND E001TNS.CODTNS = E301TCR.CODTNS" + 
					 "  LEFT JOIN E140NFV ON E140NFV.CODEMP = E301TCR.CODEMP AND E140NFV.CODFIL = E301TCR.CODFIL AND E140NFV.CODSNF = E301TCR.CODSNF AND E140NFV.NUMNFV = E301TCR.NUMNFV" + 
					 "  LEFT JOIN E140ISV ON E140ISV.CODEMP = E140NFV.CODEMP AND E140ISV.CODFIL = E140NFV.CODFIL AND E140ISV.CODSNF = E140NFV.CODSNF AND E140ISV.NUMNFV = E140NFV.NUMNFV" + 
					 "  LEFT JOIN E140IDE ON E140IDE.CODEMP = E140NFV.CODEMP AND E140IDE.CODFIL = E140NFV.CODFIL AND E140IDE.CODSNF = E140NFV.CODSNF AND E140IDE.NUMNFV = E140NFV.NUMNFV" + 
					 "  WHERE E001TNS.LISMOD = 'CRE'" +                                                               
					 "  AND E301TCR.VLRABE > 0" +                                                                     
					 "  AND E301TCR.VCTPRO <= (GETDATE() - 4)" +    
					 "  AND E301TCR.CODTPT <> 'IFA' 															   " +
					 "  AND ((E301TCR.SITTIT >= 'AA' AND E301TCR.SITTIT <= 'AV') OR (E301TCR.SITTIT = 'CE'))" +       
					 "  AND E301TCR.CodCli = " + codcli +
					 "  AND E140ISV.CodEmp = " + codemp +
					 "  AND E140ISV.CodFil = " + codfil +
					 "  AND E140ISV.NumCtr = " + numctr +
					 "  GROUP BY E301TCR.CODEMP, E301TCR.CODFIL, E301TCR.CODTPT, E301TCR.NUMTIT, E301TCR.DATEMI, E301TCR.VCTORI, E301TCR.VCTPRO, E301TCR.VLRORI, E301TCR.VLRABE, E140NFV.VLRBSE, E140IDE.NUMDFS, E140ISV.NUMCTR" +  
					 "  ORDER BY E301TCR.VCTPRO";
		

		Query query = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sql);
		query.setMaxResults(1);
		@SuppressWarnings("unchecked")
		Collection<Object> resultList = query.getResultList();

		if (resultList != null) {
			for (Object resultSet : resultList) {
				if (resultSet != null)	{
					Object result = (Object) resultSet;
					processEntity.findField("maiorQueTrinta").setValue((Integer)result == 1);
				}
			}
		} else {
			throw new WorkflowException("Não foi possivel recuperar títulos para verificação do vencimento maior que 30 dias.");	
		}

		/**
		 * @author orsegups lucas.avila - removido TOP.
		 * @date 08/07/2015
		 */
		String sql2 = "	SELECT (CASE WHEN DATEDIFF(YEAR, USU_T160CTR.USU_INIVIG, E301TCR.VCTORI) < 1 THEN 1 ELSE 0 END)" +
					  " FROM E301TCR" + 
					  " INNER JOIN E001TNS ON E001TNS.CODEMP = E301TCR.CODEMP AND E001TNS.CODTNS = E301TCR.CODTNS" + 
					  " LEFT JOIN E140NFV ON E140NFV.CODEMP = E301TCR.CODEMP AND E140NFV.CODFIL = E301TCR.CODFIL AND E140NFV.CODSNF = E301TCR.CODSNF AND E140NFV.NUMNFV = E301TCR.NUMNFV" + 
					  " LEFT JOIN E140ISV ON E140ISV.CODEMP = E140NFV.CODEMP AND E140ISV.CODFIL = E140NFV.CODFIL AND E140ISV.CODSNF = E140NFV.CODSNF AND E140ISV.NUMNFV = E140NFV.NUMNFV" + 
					  " LEFT JOIN USU_T160CTR ON USU_T160CTR.USU_CODEMP = E140ISV.CODEMP AND USU_T160CTR.USU_CODFIL = E140ISV.CODFIL AND USU_T160CTR.USU_NUMCTR = E140ISV.NUMCTR" +
					  " LEFT JOIN E140IDE ON E140IDE.CODEMP = E140NFV.CODEMP AND E140IDE.CODFIL = E140NFV.CODFIL AND E140IDE.CODSNF = E140NFV.CODSNF AND E140IDE.NUMNFV = E140NFV.NUMNFV" + 
					  " WHERE E001TNS.LISMOD = 'CRE'" +                                                               
					  " AND E301TCR.VLRABE > 0" +                                                                     
					  " AND E301TCR.VCTPRO <= (GETDATE() - 4)" +                                                        
					  " AND ((E301TCR.SITTIT >= 'AA' AND E301TCR.SITTIT <= 'AV') OR (E301TCR.SITTIT = 'CE'))" +       
					  " AND E301TCR.CodCli = " + codcli +
					  " AND E140ISV.CodEmp = " + codemp +
					  " AND E140ISV.CodFil = " + codfil +
					  " AND E140ISV.NumCtr = " + numctr +
					  " GROUP BY E301TCR.CODEMP, E301TCR.CODFIL, E301TCR.CODTPT, E301TCR.NUMTIT, E301TCR.DATEMI, E301TCR.VCTORI, E301TCR.VCTPRO, E301TCR.VLRORI, E301TCR.VLRABE, E140NFV.VLRBSE, E140IDE.NUMDFS, E140ISV.NUMCTR, USU_T160CTR.USU_INIVIG" +
					  " ORDER BY E301TCR.VCTORI";
	

		Query query2 = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sql2);
		query2.setMaxResults(1);
		@SuppressWarnings("unchecked")
		Collection<Object> resultList2 = query2.getResultList();
	
		// Lista de Títulos em aberto de outros contratos
		processEntity.findField("listaDemaisTitulos").removeValues();
		
		if (resultList2 != null) {
			for (Object resultSet : resultList2) {
				if (resultSet != null)	{		
					Object result = (Object) resultSet;
					processEntity.findField("menorQueDoze").setValue((Integer)result == 1);
				}
			}
		} else {
			throw new WorkflowException("Não foi possivel recuperar títulos para verificação do vencimento menor que 12 meses do início do contrato.");	
		}
		
	}
}