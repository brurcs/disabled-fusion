package com.neomind.fusion.custom.orsegups.processoJuridico.adapter;

import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;


public class AdapterMontarPreenchimentoAutomaticoTS implements AdapterInterface
{
	@Override
	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
	    try{
		NeoObject oTarefaSimples = AdapterUtils.createNewEntityInstance("Tarefa");
		EntityWrapper wrpTarefaSimples = new EntityWrapper(oTarefaSimples);
		
		String titulo = (String) processEntity.findValue("tituloTarefa");
		String solicitacaoInicial = "";
		List<NeoObject> registroAtividade = (List<NeoObject>) processEntity.getValue("j002RegistroHistoricoAtividade");
		if (registroAtividade != null && registroAtividade.size() > 0){
		    EntityWrapper wRegistro = new EntityWrapper(registroAtividade.get(0));
		    solicitacaoInicial = (String) wRegistro.getValue("descricao");
		}
		String descricao = "<strong>Código tarefa</strong> " + activity.getCode() + "<br>";
		       descricao += "<strong>Nº do Processo:</strong> " + processEntity.findValue("numPro") + "<br>";
		       descricao += "<strong>Grau do processo</strong> " + processEntity.findValue("grauProcesso")+ "<br>";
		       descricao += "<strong>Solicitação Inicial:</strong> " + solicitacaoInicial + "<br>";
			
		wrpTarefaSimples.setValue("Titulo", titulo);		
		wrpTarefaSimples.setValue("DescricaoSolicitacao", NeoUtils.encodeHTMLValue(descricao));
		
		processEntity.setValue("tarefaSimples", oTarefaSimples);
	    }catch(Exception e){
		System.out.println("Erro na classe AdapterMontarPreenchimentoAutomaticoTS do fluxo J002.");
		e.printStackTrace();
		throw new WorkflowException(e.getMessage());
	    }
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
