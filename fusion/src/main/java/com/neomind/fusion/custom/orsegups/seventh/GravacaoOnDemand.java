package com.neomind.fusion.custom.orsegups.seventh;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.io.Files;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.common.NeoRunnable;
import com.neomind.fusion.custom.orsegups.seventh.bean.EventoArmeDesarme;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

/**
 * Gravação de imagem baseada nos eventos de arme e desarme das centrais
 * 
 * @author mateus.batista
 * @since 08/11/2016
 * @version 1.0
 */
public class GravacaoOnDemand implements CustomJobAdapter {

    private final Log log = LogFactory.getLog(GravacaoOnDemand.class);
    private Long key = GregorianCalendar.getInstance().getTimeInMillis();
    private final boolean gravarLogs = this.getLogControl();

    @Override
    public void execute(CustomJobContext ctx) {

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	final StringBuffer logsTxt = new StringBuffer();
	
	String servidoresHabilitados = Joiner.on(",").join(SeventhUtils.getServidoresHabilitados());
	
	long inicio = Calendar.getInstance().getTimeInMillis();

	try {

	    conn = PersistEngine.getConnection("SIGMA90");

	    String ultimaExecucao = this.getUltimaExecucao();

	    int maxE = 0;

	    while ((ultimaExecucao == null || ultimaExecucao.isEmpty()) && maxE < 2) {
		ultimaExecucao = this.getUltimaExecucao();
		maxE++;
	    }

	    StringBuilder sql = new StringBuilder();

	    sql.append(" SELECT c.CD_CLIENTE,c.ID_CENTRAL,c.PARTICAO,c.ID_EMPRESA, c.SERVIDORCFTV, c.PORTACFTV, c.USERCFTV, c.SENHACFTV, c.DT_ULTIMO_ARME AS DT_PROCESSADO, ");
	    sql.append(" CASE ");
	    sql.append(" WHEN c.ARMADA = 1 THEN 'A' ");
	    sql.append(" WHEN c.ARMADA = 0 THEN 'D' ");
	    sql.append(" END as OP ");
	    sql.append(" FROM  dbCENTRAL c WITH(NOLOCK) ");
	    sql.append(" WHERE c.DT_ULTIMO_ARME >= '" + ultimaExecucao + "' ");
	    sql.append(" AND c.CTRL_CENTRAL=1 ");
	    sql.append(" AND c.STSERVIDORCFTV=1 ");
	    sql.append(" AND c.TP_CFTV=1 ");
	    sql.append(" AND c.ID_EMPRESA NOT IN (10119, 10120) ");
	    sql.append(" AND c.SERVIDORCFTV IS NOT NULL ");
	    sql.append(" AND c.SERVIDORCFTV IN ("+servidoresHabilitados+") ");
	    sql.append(" AND (c.RAZAO NOT LIKE '%DILMO%BERGER%' OR c.FANTASIA NOT LIKE '%DILMO%BERGER%'  ) ");
	    sql.append(" AND c.FANTASIA NOT LIKE '%monitoramento pessoal inteligente%' ");
	    sql.append(" ORDER BY CD_CLIENTE, DT_PROCESSADO DESC ");

	    stmt = conn.createStatement();

	    this.setUltimaExecucao();

	    rs = stmt.executeQuery(sql.toString());

	    Map<String, List<EventoArmeDesarme>> eventos = new HashMap<String, List<EventoArmeDesarme>>();

	    while (rs.next()) {

		String servidor = rs.getString("SERVIDORCFTV");

		List<EventoArmeDesarme> eventosDoServidor = eventos.get(servidor);

		if (eventosDoServidor == null) {
		    eventosDoServidor = new ArrayList<EventoArmeDesarme>();
		    eventos.put(servidor, eventosDoServidor);
		}

		EventoArmeDesarme evento = new EventoArmeDesarme();

		evento.setCdCliente(rs.getLong("CD_CLIENTE"));
		evento.setIdCentral(rs.getString("ID_CENTRAL"));
		evento.setIdEmpresa(rs.getString("ID_EMPRESA"));
		evento.setParticao(rs.getString("PARTICAO"));
		evento.setDtProcessado(rs.getTimestamp("DT_PROCESSADO"));
		evento.setOperacao(rs.getString("OP"));
		evento.setServidorCFTV(rs.getString("SERVIDORCFTV") != null ? rs.getString("SERVIDORCFTV").trim() : "");
		evento.setPortaCFTV(rs.getString("PORTACFTV") != null ? rs.getString("PORTACFTV") : "");
		evento.setUsuarioCFTV(rs.getString("USERCFTV") != null ? rs.getString("USERCFTV") : "");
		evento.setSenhaCFTV(rs.getString("SENHACFTV") != null ? rs.getString("SENHACFTV") : "");

		eventosDoServidor.add(evento);

	    }
	    	       
	    ExecutorService executor = Executors.newFixedThreadPool(10);
	    
	    for (String key : eventos.keySet()) {

		final List<EventoArmeDesarme> eventosDoServidor = eventos.get(key);

		executor.execute(new Runnable() {
		    public void run() {

			SeventhUtils util = new SeventhUtils();

			for (EventoArmeDesarme evento : eventosDoServidor) {

			    if (evento.getOperacao().equalsIgnoreCase("D")) {
				try {
				    logsTxt.append(util.pausarGravacaoTodasCameras(evento));
				} catch (IOException e) {
				    e.printStackTrace();
				}
			    } else {
				try {
				    logsTxt.append(util.iniciarGravacaoTodasCameras(evento));
				} catch (IOException e) {
				    e.printStackTrace();
				}
			    }
			}
		    }
		});

	    }

	    executor.shutdown();
	    while (!executor.isTerminated()) {
	    }

	    if (this.gravarLogs) {

		String dataDia = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd-MM-yyyy");

		String dataHoraArquivo = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd-MM-yyyy HH-mm-ss");

		File pasta = new File("F:\\GravacaoOnDemandLogs\\" + dataDia);

		if (!pasta.exists()) {
		    pasta.mkdirs();
		}

		File file = new File(pasta, dataHoraArquivo + ".txt");

		try {
		    Files.write(logsTxt, file, Charsets.UTF_8);
		} catch (IOException e) {
		    e.printStackTrace();
		    log.error("GravacaoOnDemand - ERRO execute gravarLog -- Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		}

	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("GravacaoOnDemand - ERRO execute -- Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	long fim = Calendar.getInstance().getTimeInMillis();

	System.out.println("[GravacaoOnDemand] Tempo total: " + ((fim - inicio) / 1000) / 60 + " minuto(s) Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));

    }

    private String getUltimaExecucao() {

	String retorno = "";
	try {
	    Collection<NeoObject> monitoraAgendador = null;

	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("nomeRotina", "GravacaoOnDemand"));

	    monitoraAgendador = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);

	    if (monitoraAgendador != null && !monitoraAgendador.isEmpty()) {
		for (NeoObject neoObject : monitoraAgendador) {
		    EntityWrapper monitoraAgendadorObj = new EntityWrapper(neoObject);

		    GregorianCalendar dataFinalAgendador = (GregorianCalendar) monitoraAgendadorObj.getValue("dataFinalAgendador");

		    retorno = NeoDateUtils.safeDateFormat(dataFinalAgendador, "yyyy-MM-dd HH:mm:ss");
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("GravacaoOnDemand - ERRO getUltimaExecucao - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	}

	return retorno;

    }

    private void setUltimaExecucao() {

	try {
	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("nomeRotina", "GravacaoOnDemand"));

	    List<NeoObject> monitoraAgendador = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);
	    

	    if (monitoraAgendador != null && !monitoraAgendador.isEmpty()) {
		final NeoObject neoObject = (NeoObject) monitoraAgendador.get(0);

		EntityWrapper monitoraAgendadorWrapper = new EntityWrapper(neoObject);

		monitoraAgendadorWrapper.findField("dataFinalAgendador").setValue(new GregorianCalendar());
		
		final NeoRunnable work = new NeoRunnable() {
		    public void run() throws Exception {
			PersistEngine.persist(neoObject);
		    }
		};

		try {
		    PersistEngine.managedRun(work);
		} catch (final Exception e) {
		    e.printStackTrace();
		    log.error("GravacaoOnDemand - ERRO setUltimaExecucao - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		}
		
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("GravacaoOnDemand - ERRO setUltimaExecucao - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	}

    }

    @SuppressWarnings("unused")
    private String getListaClientes() {

	String codigos = "";

	try {
	    List<NeoObject> listaClientes = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("seventhGravacaoOnDemand"));

	    ArrayList<Integer> listaCDCliente = new ArrayList<Integer>();

	    if (listaClientes != null && !listaClientes.isEmpty()) {
		for (NeoObject cliente : listaClientes) {

		    EntityWrapper wrappperExcecao = new EntityWrapper(cliente);
		    Long cdCliente = (Long) wrappperExcecao.findField("cliente.cd_cliente").getValue();
		    listaCDCliente.add(Integer.valueOf(cdCliente.intValue()));
		}
	    }
	    codigos = Joiner.on(",").join(listaCDCliente);
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("GravacaoOnDemand - ERRO getListaClientes - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	}
	return codigos;

    }

    private boolean getLogControl() {

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	sql.append("SELECT ativo FROM GravacaoOnDemandLogs_Control");

	try {
	    conn = PersistEngine.getConnection("TIDB");
	    stmt = conn.createStatement();
	    rs = stmt.executeQuery(sql.toString());

	    while (rs.next()) {

		return rs.getBoolean(1);

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	return false;

    }

    // private String getUltimaOperacao(String cdCliente) {
    //
    // String retorno = "A";
    //
    // Connection conn = null;
    // Statement stmt = null;
    // ResultSet rs = null;
    //
    // try {
    //
    // conn = PersistEngine.getConnection("SIGMA90");
    //
    // String sql = "SELECT * FROM ( "
    // +
    // " SELECT c.CD_CLIENTE,c.ID_CENTRAL,c.ID_EMPRESA,a.DT_PROCESSADO, 'A' AS OP "
    // + " FROM HISTORICO_ARME a "
    // + " INNER JOIN dbCENTRAL c ON a.CD_CLIENTE = c.CD_CLIENTE "
    // +
    // " WHERE a.DT_PROCESSADO = (SELECT MAX(DT_PROCESSADO) FROM HISTORICO_ARME A2 WITH(NOLOCK) WHERE A2.CD_CLIENTE=C.CD_CLIENTE) "
    // + " AND c.CD_CLIENTE IN (" + cdCliente + ") "
    // + " AND c.STSERVIDORCFTV=1 "
    // + " UNION ALL "
    // +
    // " SELECT c.CD_CLIENTE,c.ID_CENTRAL,c.ID_EMPRESA,d.DT_PROCESSADO, 'D' AS OP "
    // + " FROM HISTORICO_DESARME d "
    // + " INNER JOIN dbCENTRAL c ON d.CD_CLIENTE = c.CD_CLIENTE "
    // +
    // " WHERE d.DT_PROCESSADO = (SELECT MAX(DT_PROCESSADO) FROM HISTORICO_DESARME D2 WITH(NOLOCK) WHERE D2.CD_CLIENTE=C.CD_CLIENTE)  "
    // + " AND c.CD_CLIENTE IN (" + cdCliente + ") "
    // + " AND c.STSERVIDORCFTV=1 " + " ) AS LISTA "
    // + " ORDER BY CD_CLIENTE, DT_PROCESSADO DESC";
    //
    // stmt = conn.createStatement();
    //
    // rs = stmt.executeQuery(sql);
    //
    // while (rs.next()) {
    //
    // retorno = rs.getString("OP");
    // break;
    //
    // }
    //
    // } catch (Exception e) {
    // e.printStackTrace();
    // log.error("GravacaoOnDemand - ERRO getUltimaOperacao -- Data: " +
    // NeoDateUtils.safeDateFormat(new GregorianCalendar(),
    // "dd/MM/yyyy HH:mm:ss"));
    // throw new JobException("Erro no processamento, procurar no log por [" +
    // key + "]");
    // } finally {
    // OrsegupsUtils.closeConnection(conn, stmt, rs);
    // }
    //
    // return retorno;
    // }

}
