package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class RotinaAbreTarefaSimplesOrdemServicoAgendada implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(RotinaAbreTarefaSimplesOrdemServicoAgendada.class);

	@Override
	public void execute(CustomJobContext arg0)
	{

		Connection conn = PersistEngine.getConnection("SIGMA90");
		StringBuilder sqlOrdemServico = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;

		log.warn("##### INICIAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Ordens de Serviço Agendamento -- Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		try
		{
			//Ordens de Serviço	
			InstantiableEntityInfo osInfo = AdapterUtils.getInstantiableEntityInfo("SIGMAOSAgendadaTarefaSimples");
			sqlOrdemServico.append(" SELECT top 20 os.ID_ORDEM, c.ID_CENTRAL, c.PARTICAO, os.DATAAGENDADA AS DATA, c.RAZAO, os.DEFEITO, col.NM_COLABORADOR ");
			sqlOrdemServico.append(" FROM dbORDEM os ");
			sqlOrdemServico.append(" INNER JOIN dbCENTRAL c ON c.CD_CLIENTE = os.CD_CLIENTE ");
			sqlOrdemServico.append(" INNER JOIN COLABORADOR col ON col.CD_COLABORADOR = os.ID_INSTALADOR ");
			sqlOrdemServico.append(" WHERE os.DATAAGENDADA >= GETDATE()  AND os.IDOSDEFEITO IN (178, 184)");

			pstm = conn.prepareStatement(sqlOrdemServico.toString());
			rs = pstm.executeQuery();

			int contador = 0;
			while (rs.next())
			{

				String numeroOS = rs.getString("ID_ORDEM");
				Long nuOS = Long.parseLong(numeroOS);
				List listOSTarefa = PersistEngine.getObjects(osInfo.getEntityClass(), new QLEqualsFilter("numeroOS", nuOS), 0, 1, "");
				if (listOSTarefa.isEmpty())
				{
					String solicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteOrdemServicoAgendada"); 
					String titulo = "Acompanhar Instalação - " + rs.getString("RAZAO") + " - OS: " + numeroOS + " ";

					Date date = new Date();
					if (rs.getDate("DATA") != null)
						date = rs.getDate("DATA");

					GregorianCalendar prazo = new GregorianCalendar();

					prazo.setTime(date);
					prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 0L);
					prazo.set(Calendar.HOUR_OF_DAY, 23);
					prazo.set(Calendar.MINUTE, 59);
					prazo.set(Calendar.SECOND, 59);

					String descricao = "";
					descricao = "<strong><u>Acompanhar instalação da venda abaixo:</u></strong><br><br>";
					descricao = descricao + " <strong>Cliente :</strong> " + rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "] " + rs.getString("RAZAO") + " <br>";
					descricao = descricao + " <strong>Data Agendamento :</strong> " + NeoDateUtils.safeDateFormat(prazo, "dd/MM/yyyy") + "<br>";
					descricao = descricao + " <strong>Texto OS :</strong> " + rs.getString("DEFEITO") + "<br>";
					descricao = descricao + " <strong>Instalador :</strong> " + rs.getString("NM_COLABORADOR") + "<br>";

					IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
					String retorno = iniciarTarefaSimples.abrirTarefa(solicitante, "", titulo, descricao, "3", "nao", prazo);

					if (retorno != null && (!retorno.trim().contains("Erro")))
					{
						NeoObject objOS = osInfo.createNewInstance();
						EntityWrapper wrpOS = new EntityWrapper(objOS);
						wrpOS.findField("numeroOS").setValue(nuOS);
						wrpOS.findField("numeroTarefa").setValue(Long.parseLong(retorno));
						PersistEngine.persist(objOS);

					}
					else
					{
						log.error(" Erro ao criar tarefa Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
					}

					contador++;
				}
			}
			log.warn("##### EXECUTAR  AGENDADOR DE TAREFA: Abrir Tarefa Simples Ordens de Serviço Agendamento - Total de tarefas: " + contador);
		}
		catch (Exception e)
		{
			log.error("##### ERRO  AGENDADOR DE TAREFA: Abrir Tarefa Simples  Ordens de Serviço Agendamento- ERRO AO GERAR TAREFA");
			System.out.println("[" + key + "] ##### ERRO  AGENDADOR DE TAREFA: Abrir Tarefa Simples  Ordens de Serviço Agendamento- ERRO AO GERAR TAREFA");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				log.error("##### ERRO  AGENDADOR DE TAREFA: Abrir Tarefa Simples  Ordens de Serviço Agendamento- ERRO DE EXECUÇÃO ");
				e.printStackTrace();
			}

			log.warn("##### FINALIZAR  AGENDADOR DE TAREFA: Abrir Tarefa Simples Ordens de Serviço Agendamento  Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}
}
