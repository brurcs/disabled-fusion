package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.SortedMap;
import java.util.TreeMap;

import com.google.common.base.Joiner;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.ClienteContaCobrancaVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class CadastroExcecoesCobrancaOS implements CustomJobAdapter {

    
    public void execute(CustomJobContext ctx) {
	
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	StringBuilder sql = new StringBuilder();
	
	try{
	    
	    conn =  PersistEngine.getConnection("SIGMA90");
	    
	    sql.append("SELECT DISTINCT cli.codcli, cli.nomcli, (CEN.ID_CENTRAL + '[' + CEN.PARTICAO + ']') AS CONTA ");
	    sql.append("FROM dbCENTRAL AS CEN WITH (NOLOCK) ");
	    sql.append("INNER JOIN [FSOODB04\\SQL02].SAPIENS.dbo.USU_T160SIG sig WITH (NOLOCK) ON sig.usu_codcli = cen.CD_CLIENTE ");
	    sql.append("INNER JOIN [FSOODB04\\SQL02].SAPIENS.dbo.USU_T160CVS cvs WITH (NOLOCK) ON cvs.usu_codemp = sig.usu_codemp ");
	    sql.append("AND cvs.usu_codfil = sig.usu_codfil AND cvs.usu_numctr = sig.usu_numctr AND cvs.usu_numpos = sig.usu_numpos ");
	    sql.append("INNER JOIN [FSOODB04\\SQL02].SAPIENS.dbo.USU_T160CTR CTR WITH(NOLOCK) ON cvs.usu_codemp = ctr.usu_codemp ");
	    sql.append("AND cvs.usu_codfil = ctr.usu_codfil AND cvs.usu_numctr = ctr.usu_numctr ");
	    sql.append("INNER JOIN [FSOODB04\\SQL02].SAPIENS.dbo.E085CLI CLI WITH (NOLOCK) ON CTR.USU_CODCLI = CLI.CODCLI ");
	    sql.append("WHERE cvs.usu_sitcvs = 'A' ");
	    sql.append("AND cli.TipEmc = 1 ");
	    sql.append("AND sig.usu_codemp in (15,18,19,21) ");
	    sql.append("AND CTRL_CENTRAL=1 ");
	    sql.append("AND CVS.USU_CODSER NOT IN ('9002035','9002011', '9002004', '9002005','9002014') ");
	    sql.append("ORDER BY cli.nomcli ");
	    
	    pstm = conn.prepareStatement(sql.toString());
	    
	    rs = pstm.executeQuery();
	    
	    
	    SortedMap<Integer, ClienteContaCobrancaVO> mapaClientes = new TreeMap<Integer, ClienteContaCobrancaVO>();
	    
	    while (rs.next()) {

		int codigoCliente = rs.getInt("codCli");
		String conta = rs.getString("CONTA");
		

		if (mapaClientes.containsKey(codigoCliente)) {
		    
		    ClienteContaCobrancaVO manipula = mapaClientes.get(codigoCliente);
		    manipula.addConta(conta);
		} else {
		    
		    String nomeCliente = rs.getString("nomcli");

		    ClienteContaCobrancaVO clienteVO = new ClienteContaCobrancaVO();

		    clienteVO.setCodigoCliente(codigoCliente);
		    clienteVO.setNomeCliente(nomeCliente);
		    clienteVO.addConta(conta);

		    mapaClientes.put(codigoCliente, clienteVO);
		}

	    }
	    
	    for (Integer key : mapaClientes.keySet()){
		
		ClienteContaCobrancaVO manipula = mapaClientes.get(key);
		
		InstantiableEntityInfo eform = AdapterUtils.getInstantiableEntityInfo("CadastroExcecoesCobrancaOS");
		NeoObject objEform = eform.createNewInstance();
		EntityWrapper wrpEform = new EntityWrapper(objEform);
		
		
		wrpEform.findField("codigoCliente").setValue(Long.valueOf(manipula.getCodigoCliente()));
		wrpEform.findField("cliente").setValue(manipula.getNomeCliente());
		
		String contas = Joiner.on(", ").join(manipula.getContas());
		
		wrpEform.findField("conta").setValue(contas);
		wrpEform.findField("cobrar").setValue(manipula.isCobrar());
		wrpEform.findField("servicoExtra").setValue(manipula.isServicoExtra());
		wrpEform.findField("propEquip").setValue(manipula.getPropEquipamento());
		
		PersistEngine.persist(objEform);
	    }

		


	}catch (java.sql.SQLException e){
	    e.printStackTrace();
	}finally{
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
	
    }

}
