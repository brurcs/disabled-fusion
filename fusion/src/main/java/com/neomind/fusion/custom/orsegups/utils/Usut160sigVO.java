package com.neomind.fusion.custom.orsegups.utils;

public class Usut160sigVO
{
	private Long Usu_CodEmp;
	private Long Usu_CodFil; 
	private Long Usu_NumCtr;
	private Long Usu_NumPos;
	private Long Usu_SigEmp;
	private String Usu_SigCon;
	private Long Usu_SigPar; 
	private String Usu_SitLig;
	private Long Usu_CodCli;
	
	public Long getUsu_CodEmp()
	{
		return Usu_CodEmp;
	}
	public void setUsu_CodEmp(Long usu_CodEmp)
	{
		Usu_CodEmp = usu_CodEmp;
	}
	public Long getUsu_CodFil()
	{
		return Usu_CodFil;
	}
	public void setUsu_CodFil(Long usu_CodFil)
	{
		Usu_CodFil = usu_CodFil;
	}
	public Long getUsu_NumCtr()
	{
		return Usu_NumCtr;
	}
	public void setUsu_NumCtr(Long usu_NumCtr)
	{
		Usu_NumCtr = usu_NumCtr;
	}
	public Long getUsu_NumPos()
	{
		return Usu_NumPos;
	}
	public void setUsu_NumPos(Long usu_NumPos)
	{
		Usu_NumPos = usu_NumPos;
	}
	public Long getUsu_SigEmp()
	{
		return Usu_SigEmp;
	}
	public void setUsu_SigEmp(Long usu_SigEmp)
	{
		Usu_SigEmp = usu_SigEmp;
	}
	public String getUsu_SigCon()
	{
		return Usu_SigCon;
	}
	public void setUsu_SigCon(String usu_SigCon)
	{
		Usu_SigCon = usu_SigCon;
	}
	public Long getUsu_SigPar()
	{
		return Usu_SigPar;
	}
	public void setUsu_SigPar(Long usu_SigPar)
	{
		Usu_SigPar = usu_SigPar;
	}
	public Long getUsu_CodCli()
	{
		return Usu_CodCli;
	}
	public void setUsu_CodCli(Long usu_CodCli)
	{
		Usu_CodCli = usu_CodCli;
	}
	public String getUsu_SitLig()
	{
		return Usu_SitLig;
	}
	public void setUsu_SitLig(String usu_SitLig)
	{
		Usu_SitLig = usu_SitLig;
	}
	@Override
	public String toString()
	{
		return "Usut160sigVO [Usu_CodEmp=" + Usu_CodEmp + ", Usu_CodFil=" + Usu_CodFil + ", Usu_NumCtr=" + Usu_NumCtr + ", Usu_NumPos=" + Usu_NumPos + ", Usu_SigEmp=" + Usu_SigEmp + ", Usu_SigCon=" + Usu_SigCon + ", Usu_SigPar=" + Usu_SigPar + ", Usu_SitLig=" + Usu_SitLig + ", Usu_CodCli=" + Usu_CodCli + "]";
	}
}
