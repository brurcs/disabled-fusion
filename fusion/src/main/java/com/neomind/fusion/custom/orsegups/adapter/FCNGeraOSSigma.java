package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.datawarehouse.ExternalEntityInfo;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class FCNGeraOSSigma implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(FCNGeraOSSigma.class);

	@SuppressWarnings("unchecked")
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);

		PreparedStatement st = null;
		String nomeFonteDados = "SIGMA90";
		String nomeProcesso = activity.getProcessName();
		String textoAtencao = "";

		//String sql = "INSERT INTO tabela (campo1, campo2) VALUES (?, ?)";
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO dbORDEM ");
		sql.append("	      (CD_CLIENTE, ID_INSTALADOR, ABERTURA,  DEFEITO, OPABRIU, IDOSDEFEITO, FG_EMAIL_ENVIADO, CD_OS_SOLICITANTE, FG_TODAS_PARTICOES_EM_MANUTENCAO, DATAAGENDADA, EXECUTADO, TEMPOEXECUCAOPREVISTO )");
		sql.append("	VALUES(?,          ?,             GETDATE(), ?,       ?,       ?,           ?,                ?,                 ?,                                ?,            ?, 		1 )");

		Collection<NeoObject> contasSigma = (Collection<NeoObject>) processEntity.findValue("listaContasSigma");
		Collection<NeoObject> contasSigmaNew = getContasSigma(contasSigma);
		GregorianCalendar dataFinalMonitoramento = (GregorianCalendar) processEntity.findValue("dataFinalMonitoramento");
		Long codigoRegional = (Long) processEntity.findValue("contratoSapiens.usu_codreg");
		String siglaRegional = OrsegupsUtils.getSiglaRegional(codigoRegional);
		Boolean isRetirada = (Boolean) processEntity.findValue("retiraEquipamento");

		if (contasSigmaNew != null && !contasSigmaNew.isEmpty())
		{

			for (NeoObject contaSigma : contasSigmaNew)
			{
				Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);

				EntityWrapper contaSigmaWrapper = new EntityWrapper(contaSigma);
				Long cdCliente = (Long) contaSigmaWrapper.findValue("clienteSigma.cd_cliente");
				String filtro = siglaRegional + "%ANL%";
				QLGroupFilter filterAnalista = new QLGroupFilter("AND");
				filterAnalista.addFilter(new QLEqualsFilter("fg_ativo_colaborador", true));
				filterAnalista.addFilter(new QLOpFilter("nm_colaborador", " LIKE ", filtro));

				// Para definir o Técnico da OS inicialmente verifica-se se Há Analista (ANL) na Regional
				ArrayList<NeoObject> listaAnalista = (ArrayList<NeoObject>) PersistEngine.getObjects(((ExternalEntityInfo) EntityRegister.getInstance().getCache().getByType("SIGMACOLABORADOR")).getEntityClass(), filterAnalista);
				NeoObject noAnalista = null;
				if (listaAnalista != null && listaAnalista.size() > 0)
				{
					noAnalista = listaAnalista.get(0);
				}
				Long cdTecnico = null;
				if (noAnalista != null)
				{
					EntityWrapper analistaWrapper = new EntityWrapper(noAnalista);
					cdTecnico = (Long) analistaWrapper.getValue("cd_colaborador");
				}
				// Se não encontrar o Analista, busca-se o técnico responsável da Conta.
				if (cdTecnico == null)
				{
					cdTecnico = (Long) contaSigmaWrapper.findValue("clienteSigma.cd_tecnico_responsavel");
				}
				// Se não for encontrado o técnico da Conta, coloca-se a OS para o colaborador Padrão.
				if (cdTecnico == null)
				{
					cdTecnico = 9999L;
				}

				String textoDefeito = "OS AUTOMÁTICA: Desprogramar a Central de Alarme do Cliente em virtude de cancelamento de contrato conforme Tarefa " + origin.getCode();
				if (isRetirada)
				{
					textoDefeito = "OS AUTOMÁTICA: Retirar equipamento de contrato cancelado conforme Tarefa " + origin.getCode() + ".\nEquipamentos a Retirar:";

					Collection<NeoObject> listaEquipamentos = (Collection<NeoObject>) processEntity.findValue("listaEquipamento");
					if (listaEquipamentos == null || listaEquipamentos.isEmpty())
					{
						throw new WorkflowException("Lista de equipamentos em branco");
					}
					for (NeoObject equipamento : listaEquipamentos)
					{
						EntityWrapper equipamentoWrapper = new EntityWrapper(equipamento);
						BigDecimal qtde = (BigDecimal) equipamentoWrapper.findValue("quantidadeEquipamento");
						String nomeEquip = (String) equipamentoWrapper.findValue("descricaoEquipamento");

						textoDefeito = textoDefeito + "\n" + qtde + " " + nomeEquip;
					}
				}
				if (nomeProcesso.equalsIgnoreCase(OrsegupsUtils.CANCELAMENTO_CONTRATO_INICIATIVA))
				{
					textoDefeito = textoDefeito + "\n\nATENÇÃO: Esta OS deve ser executada somente A PARTIR da data de agendamento (Cumprimento de aviso prévio).";
				}
				else
				{
					textoDefeito = textoDefeito + "\n\nATENÇÃO: Executar esta OS ATÉ a data de agendamento.";
				}

				if (retornaServicoPosto(processEntity).equals("9002003") || retornaServicoPosto(processEntity).equals("9002039") || retornaServicoPosto(processEntity).equals("9002046"))
				{

					if (nomeProcesso.contains("C023"))
					{

						textoAtencao = "A PARTIR";
					}
					else
					{

						textoAtencao = "ATÈ";
					}

					textoDefeito = "OS AUTOMÁTICA: Retirar equipamento de contrato cancelado conforme Tarefa " + origin.getCode() + ".\nEquipamentos a Retirar:";
					textoDefeito = textoDefeito + "\n\n1.00 RASTREADOR\n\nATENÇÃO: Esta OS deve ser executada somente " + textoAtencao + " da data de agendamento (Cumprimento de aviso prévio).";

				}

				try
				{

					connection.setAutoCommit(false);

					st = connection.prepareStatement(sql.toString());
					// CD_CLIENTE
					st.setLong(1, cdCliente);
					// ID_INSTALADOR - Técnico responsável informado no cadastro da conta
					st.setLong(2, cdTecnico);
					// DEFEITO
					st.setString(3, textoDefeito);
					// OPABRIU
					st.setInt(4, 11010);
					// Se contrato for com locação (total ou parcial), Motivo da OS muda para Retira equipamento
					if (isRetirada)
					{
						// IDOSDEFEITO = 167	OS-RETIRAR EQUIPAMENTO
						st.setInt(5, 167);
					}
					else
					{
						// IDOSDEFEITO = 166	OS-DESABILITAR MONITORAMENT
						st.setInt(5, 166);
					}
					// FG_EMAIL_ENVIADO = 0
					st.setInt(6, 0);
					// CD_OS_SOLICITANTE = 10011 - DEMAIS ORDENS DE SERVIÇO
					st.setInt(7, 10011);
					// FG_TODAS_PARTICOES_EM_MANUTENCAO
					st.setInt(8, 0);
					// DATA AGENDADA
					if (dataFinalMonitoramento != null)
					{
						Timestamp timestamp = new Timestamp(dataFinalMonitoramento.getTimeInMillis());
						st.setTimestamp(9, timestamp);
					}
					else
					{
						st.setTimestamp(9, null);
					}
					// EXECUTADO
					st.setString(10, "");

					st.executeUpdate();
					connection.commit();

				}
				catch (Exception e)
				{
					e.printStackTrace();
					try
					{
						connection.rollback();
					}
					catch (SQLException e1)
					{
						e1.printStackTrace();
						throw new WorkflowException("Falha ao tentar gerar OS.");
					}
					throw new WorkflowException("Falha ao tentar gerar OS. Erro: " + e.getMessage());
				}
				finally
				{
					if (st != null)
					{
						try
						{
							st.close();
							connection.close();
						}
						catch (SQLException e)
						{
							log.error("Erro ao fechar o statement");
							e.printStackTrace();
							throw new WorkflowException("Erro ao tentar gerar OS.");
						}
					}
				}
			}

		}
		else
		{
			throw new WorkflowException("Não foi encontrada nenhuma conta do SIGMA para geração de OS.");
		}

	}

	private String retornaServicoPosto(EntityWrapper processEntity)
	{

		Long codEmp = (Long) processEntity.findValue("contratoSapiens.usu_codemp");
		Long codFil = (Long) processEntity.findValue("contratoSapiens.usu_codfil");
		Long numCtr = (Long) processEntity.findValue("contratoSapiens.usu_numctr");
		String codSer = "";
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		try
		{

			conn = PersistEngine.getConnection("SAPIENS");
			StringBuilder sql = new StringBuilder();
			sql.append("select usu_CodSer from usu_t160cvs ");
			sql.append("where usu_codemp = ? and usu_codfil = ? and usu_numctr = ? ");

			pstm = conn.prepareStatement(sql.toString());
			pstm.setLong(1, codEmp);
			pstm.setLong(2, codFil);
			pstm.setLong(3, numCtr);
			rs = pstm.executeQuery();

			if (rs.next())
			{
				codSer = rs.getString("usu_CodSer");

			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
		return codSer;
	}

    public static Collection<NeoObject> getContasSigma(Collection<NeoObject> contasSigma) {
	Collection<NeoObject> retorno = new ArrayList<NeoObject>();
	if (contasSigma != null) {
	    for (NeoObject conta : contasSigma) {
		if (retorno.size() > 0) {
		    EntityWrapper contaWrapper = new EntityWrapper(conta);
		    boolean isExists = false;
		    for (NeoObject neoObject : retorno) {
			EntityWrapper objWrapper = new EntityWrapper(neoObject);
			if (objWrapper != null && contaWrapper != null) {
			    if (objWrapper.findValue("id_central").equals(contaWrapper.findValue("id_central")) && objWrapper.findValue("cd_empresa").equals(contaWrapper.findValue("cd_empresa"))) {
				Integer partRet = Integer.valueOf((String) objWrapper.findValue("particao"));
				Integer partCont = Integer.valueOf((String) contaWrapper.findValue("particao"));
				if (partCont <= 10) {
				    if (partRet <= 10) {
					isExists = true;
					break;
				    }
				} else {
				    if (partRet > 10) {
					isExists = true;
					break;
				    }
				}
			    }
			}
		    }
		    if (!isExists) {
			retorno.add(conta);
		    }
		} else {
		    retorno.add(conta);
		}
	    }
	}
	return retorno;
    }
	
	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}
}
