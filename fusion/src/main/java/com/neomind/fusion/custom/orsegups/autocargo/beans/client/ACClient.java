package com.neomind.fusion.custom.orsegups.autocargo.beans.client;


public class ACClient {
    
    private int id;
    private String operator_name;
    private String name;
    private String phone_number;
    private String responsible;
    private String address_code;
    private String address_street;
    private String address_number;
    private String address_complement;
    private String address_district;
    private String address_city;
    private String address_state;
    private String created_at;
    private String updated_at;
    
    
    
    public String getPhone_number() {
        return phone_number;
    }
    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }
    public String getResponsible() {
        return responsible;
    }
    public void setResponsible(String responsible) {
        this.responsible = responsible;
    }
    public String getAddress_code() {
        return address_code;
    }
    public void setAddress_code(String address_code) {
        this.address_code = address_code;
    }
    public String getAddress_number() {
        return address_number;
    }
    public void setAddress_number(String address_number) {
        this.address_number = address_number;
    }
    public String getCreated_at() {
        return created_at;
    }
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
    public String getUpdated_at() {
        return updated_at;
    }
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getOperator_name() {
        return operator_name;
    }
    public void setOperator_name(String operator_name) {
        this.operator_name = operator_name;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getAddress_street() {
        return address_street;
    }
    public void setAddress_street(String address_street) {
        this.address_street = address_street;
    }

    public String getAddress_complement() {
        return address_complement;
    }
    public void setAddress_complement(String address_complement) {
        this.address_complement = address_complement;
    }
    public String getAddress_district() {
        return address_district;
    }
    public void setAddress_district(String address_district) {
        this.address_district = address_district;
    }
    public String getAddress_city() {
        return address_city;
    }
    public void setAddress_city(String address_city) {
        this.address_city = address_city;
    }
    public String getAddress_state() {
        return address_state;
    }
    public void setAddress_state(String address_state) {
        this.address_state = address_state;
    }

    
    

}
