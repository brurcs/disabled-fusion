package com.neomind.fusion.custom.orsegups.site.vo;

public class SiteCidadeVO
{
	private Long codcid;
	private String nomcid;
	private String estcid;
	
	public Long getCodcid()
	{
		return codcid;
	}
	public void setCodcid(Long codcid)
	{
		this.codcid = codcid;
	}
	public String getNomcid()
	{
		return nomcid;
	}
	public void setNomcid(String nomcid)
	{
		this.nomcid = nomcid;
	}
	public String getEstcid()
	{
		return estcid;
	}
	public void setEstcid(String estcid)
	{
		this.estcid = estcid;
	}
}
