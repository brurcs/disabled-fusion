package com.neomind.fusion.custom.orsegups.site.vo;

public class HorasVO
{
	Long positivas;
	Long negativas;
	Long total;
	
	public Long getPositivas()
	{
		return positivas;
	}
	public void setPositivas(Long positivas)
	{
		this.positivas = positivas;
	}
	public Long getNegativas()
	{
		return negativas;
	}
	public void setNegativas(Long negativas)
	{
		this.negativas = negativas;
	}
	public Long getTotal()
	{
		return total;
	}
	public void setTotal(Long total)
	{
		this.total = total;
	}
	@Override
	public String toString()
	{
		return "HorasVO [positivas=" + positivas + ", negativas=" + negativas + ", total=" + total + "]";
	}

	
	
}
