package com.neomind.fusion.custom.orsegups.utils;

public class EventosAlertAitLogVO
{
	private double kpi;
	private String placaViatura;
	private String reg;
	private String nmViatura;
	private int qtd;
	private String cdViatura;
	private String textoLog;
	public double getKpi()
	{
		return kpi;
	}
	public void setKpi(double kpi)
	{
		this.kpi = kpi;
	}
	public String getPlacaViatura()
	{
		return placaViatura;
	}
	public void setPlacaViatura(String placaViatura)
	{
		this.placaViatura = placaViatura;
	}
	public String getReg()
	{
		return reg;
	}
	public void setReg(String reg)
	{
		this.reg = reg;
	}
	public String getNmViatura()
	{
		return nmViatura;
	}
	public void setNmViatura(String nmViatura)
	{
		this.nmViatura = nmViatura;
	}
	public int getQtd()
	{
		return qtd;
	}
	public void setQtd(int qtd)
	{
		this.qtd = qtd;
	}
	public String getCdViatura()
	{
		return cdViatura;
	}
	public void setCdViatura(String cdViatura)
	{
		this.cdViatura = cdViatura;
	}
	public String getTextoLog()
	{
		return textoLog;
	}
	public void setTextoLog(String textoLog)
	{
		this.textoLog = textoLog;
	}
}
