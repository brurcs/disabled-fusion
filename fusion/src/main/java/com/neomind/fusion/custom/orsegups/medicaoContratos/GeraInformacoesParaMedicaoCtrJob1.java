package com.neomind.fusion.custom.orsegups.medicaoContratos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.docx4j.docProps.variantTypes.Array;

import com.google.common.base.Equivalence.Wrapper;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.presenca.vo.ColaboradorVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.adapter.casvig.OrsegupsContratoUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.fusion.workflow.simulation.WorkflowService;
import com.neomind.util.NeoUtils;
import com.sun.star.util.Date;

public class GeraInformacoesParaMedicaoCtrJob1 implements CustomJobAdapter
	        
{

	@Override
	public void execute(CustomJobContext ctx)
	{
		if (OrsegupsUtils.isWorkDay(new GregorianCalendar()))
		{
			System.out.println("[MED] - iniciando job Gera Informacoes Para Medicao de contratos");
			processaJob(ctx);
		}
	}

	public static void processaJob(CustomJobContext ctx)
	{

		Connection connVetorh = null;
		ResultSet rsMedicao = null;
		PreparedStatement stMedicao = null;
		try
		{
			StringBuffer queryMedicao = new StringBuffer();
			
			GregorianCalendar datRef = new GregorianCalendar();
			datRef.set(GregorianCalendar.DATE, 1);
			System.out.println("" + NeoUtils.safeDateFormat(datRef));
			datRef.set(GregorianCalendar.HOUR, 23);
			datRef.set(GregorianCalendar.MINUTE, 59);
			datRef.set(GregorianCalendar.SECOND, 59);
			datRef.add(GregorianCalendar.DATE,-1);

			
			System.out.println("" + NeoUtils.safeDateFormat(datRef));

			queryMedicao.append("  SELECT cvs.usu_numctr,cvs.usu_numpos,cvs.usu_codemp,cvs.usu_codfil,cvs.usu_nomloc,car.titred,fun.numcad,fun.nomfun,fun.numemp,fun.tipcol,fun.datadm ,isnull(sit.dessit, 'trabalhando') AS DesSit,"
					+ "orn.usu_lotorn,orn.usu_apecli,reg.usu_codreg,reg.USU_NomReg,emp.nomemp,orn.numloc,orn.usu_numemp");
			queryMedicao.append(" FROM R034FUN fun   ");
			queryMedicao.append(" INNER JOIN R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp AND hlo.TipCol = fun.TipCol AND hlo.NumCad = fun.NumCad AND hlo.DatAlt = ");
			queryMedicao.append(" (SELECT MAX (DATALT) FROM R038HLO TABELA001 WHERE TABELA001.NUMEMP = hlo.NUMEMP AND TABELA001.TIPCOL = hlo.TIPCOL AND TABELA001.NUMCAD = hlo.NUMCAD AND TABELA001.DATALT <= ?)  ");
			queryMedicao.append(" INNER JOIN R038HCA HCA (NOLOCK) ON HCA.CodCar = FUN.codcar AND HCA.NumCad = FUN.NUMCAD AND HCA.TipCol = FUN.TIPCOL AND HCA.NUMEMP = FUN.numemp AND HCA.DatAlt = ");
			queryMedicao.append(" (SELECT MAX(DATALT) FROM R038HCA HCA2 WHERE HCA2.NUMCAD = HCA.NUMCAD AND ");
			queryMedicao.append("										    HCA2.NUMEMP = HCA.NUMEMP AND ");
			queryMedicao.append("											HCA2.TIPCOL = HCA.TIPCOL AND ");
			queryMedicao.append("											HCA2.DatAlt <= ?)");
			queryMedicao.append(" INNER JOIN R024CAR CAR (NOLOCK) ON CAR.CODCAR = HCA.CodCar");
			queryMedicao.append(" INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.NumLoc = hlo.NumLoc and orn.TabOrg = 203  ");
			queryMedicao.append(" INNER JOIN USU_T038CVS cvs WITH (NOLOCK) ON cvs.USU_NumLoc = orn.NumLoc AND cvs.USU_TabOrg = orn.TabOrg AND cvs.USU_SeqAlt = (Select MAX(c2.usu_seqalt) FROM USU_T038CVS c2 Where ");
			queryMedicao.append(" c2.usu_taborg = cvs.usu_taborg And ");
			queryMedicao.append(" c2.usu_numloc = cvs.usu_numloc And ");
			queryMedicao.append(" c2.usu_datalt <= ?)  ");
			queryMedicao.append(" INNER JOIN USU_TFisRes fis ON fis.USU_FisRes = fun.usu_fisres  ");
			queryMedicao.append(" LEFT JOIN R038AFA afa WITH (NOLOCK) ON (afa.NumEmp = fun.NumEmp AND afa.TipCol = fun.TipCol AND afa.NumCad = fun.NumCad AND DATEADD(minute,afa.HORAFA,afa.DATAFA) = ");
			queryMedicao.append(" (SELECT MAX (DATEADD(minute,HORAFA,DATAFA)) FROM R038AFA TABELA001 ");
			queryMedicao.append(" WHERE TABELA001.NUMEMP = afa.NUMEMP AND ");
			queryMedicao.append(" TABELA001.TIPCOL = afa.TIPCOL AND ");
			queryMedicao.append(" TABELA001.NUMCAD = afa.NUMCAD AND ");
			queryMedicao.append(" TABELA001.DATAFA <= ? AND ");
			queryMedicao.append(" (TABELA001.DatTer = '1900-12-31 00:00:00' OR TABELA001.DatTer >= cast(floor(cast(? as float)) as datetime))))  LEFT JOIN R038AFA afaDem WITH (NOLOCK) ON ");
			queryMedicao.append(" afaDem.NumEmp = fun.NumEmp AND afaDem.TipCol = fun.TipCol AND afaDem.NumCad = fun.NumCad AND afaDem.sitAfa = 7 AND DATEADD(minute,afaDem.HORAFA,afaDem.DATAFA) = ");
			queryMedicao.append(" (SELECT MAX (DATEADD(minute,HORAFA,DATAFA)) FROM R038AFA TABELA001 WHERE TABELA001.NUMEMP = afaDem.NUMEMP AND TABELA001.TIPCOL = afaDem.TIPCOL AND TABELA001.NUMCAD = afaDem.NUMCAD)  ");
			queryMedicao.append(" LEFT JOIN R010SIT SIT ON SIT.CODSIT = AFA.SitAfa ");
			queryMedicao.append(" INNER JOIN USU_T200REG reg on reg.USU_CodReg = orn.usu_codreg");
			queryMedicao.append(" INNER JOIN r030emp emp on emp.numemp = orn.usu_numemp");
			queryMedicao.append(" INNER JOIN [FSOODB04\\SQL02].SAPIENS.DBO.E085CLI CLI ON CLI.CODCLI = ORN.usu_codclisap");
			queryMedicao.append(" WHERE fun.DatAdm <= ?  AND (afa.SitAfa IS NULL OR afa.SitAfa <> 7 OR (afa.SitAfa = 7 AND afa.DatAfa > cast(floor(cast(? as float)) as datetime)))  ");
			queryMedicao.append(" AND fun.TipCol = 1  AND orn.usu_numemp IN (2,6,7,8) and orn.usu_codreg in(1,9)");
			queryMedicao.append(" GROUP BY cvs.usu_numctr,cvs.usu_numpos,cvs.usu_codemp,cvs.usu_codfil,cvs.usu_nomloc,car.titred,fun.numcad,fun.nomfun,fun.numemp,fun.tipcol,fun.datadm,DesSit,orn.usu_lotorn,orn.usu_apecli,reg.usu_codreg,reg.USU_NomReg,emp.nomemp,orn.numloc,orn.usu_numemp ");
			queryMedicao.append("  ORDER BY cvs.usu_numctr,cvs.usu_numpos,cvs.usu_codemp,cvs.usu_codfil,cvs.usu_nomloc,car.titred,fun.numcad,fun.nomfun,fun.numemp,fun.tipcol,fun.datadm,DesSit,orn.usu_lotorn,orn.usu_apecli,reg.usu_codreg,reg.USU_NomReg,emp.nomemp,orn.numloc,orn.usu_numemp ");

			connVetorh = PersistEngine.getConnection("VETORH");
			stMedicao = connVetorh.prepareStatement(queryMedicao.toString());
			stMedicao.setTimestamp(1, new Timestamp(datRef.getTimeInMillis()));
			stMedicao.setTimestamp(2, new Timestamp(datRef.getTimeInMillis()));
			stMedicao.setTimestamp(3, new Timestamp(datRef.getTimeInMillis()));
			stMedicao.setTimestamp(4, new Timestamp(datRef.getTimeInMillis()));
			stMedicao.setTimestamp(5, new Timestamp(datRef.getTimeInMillis()));
			stMedicao.setTimestamp(6, new Timestamp(datRef.getTimeInMillis()));
			stMedicao.setTimestamp(7, new Timestamp(datRef.getTimeInMillis()));
			
			rsMedicao = stMedicao.executeQuery();

			List<NeoObject> colaboradores = new ArrayList<NeoObject>();
			long nNumCtrAnt = 0;
			long nNumCtrNov = 0;
			EntityWrapper wMedicao = null;
			NeoObject medicao = null;
			String destacar = "";

			while (rsMedicao.next())
			{
				nNumCtrNov = rsMedicao.getLong("usu_numctr");
				destacar = "";
				List<NeoObject> listConsideracoes = OrsegupsMedicaoUtils.validaAfastamento(rsMedicao.getLong("numemp"), rsMedicao.getLong("numcad"), 1L, datRef, rsMedicao.getLong("NumLoc"));
				NeoObject colaboradorMedicao = AdapterUtils.createNewEntityInstance("MEDColaboradores");
				EntityWrapper wColaborador = new EntityWrapper(colaboradorMedicao);
				int desconto = 30;

				if (listConsideracoes != null && listConsideracoes.size() >= 1)
				{
					
					wColaborador.findField("listaConsideracoes").setValue(listConsideracoes);
					for (int i = 0; i < listConsideracoes.size(); i++)
					{
						NeoObject wFalta = listConsideracoes.get(i);
						EntityWrapper ew = new EntityWrapper(wFalta);
						boolean isFalta = (boolean) ew.findField("isFalta").getValue();
						String descricao = ew.findField("descricao").getValueAsString();
						if (isFalta)
						{
							desconto--;
						}
						if(descricao.contains("** Verificar pois existe cobertura de posto no dia")){
							destacar = "**";
						}
					}
				}

				if (nNumCtrAnt != nNumCtrNov)
				{
					//Validar se deve ser aplicado o persist. Caso nNumCtrAnt for != 0 é um novo contrato, então deve ser persistido antes de alterar.
					if (nNumCtrAnt != 0)
					{
						wMedicao.setValue("listaColaboradores", colaboradores);
						PersistEngine.persist(medicao);
						colaboradores = new ArrayList<NeoObject>();

					}
					medicao = null;
					medicao = AdapterUtils.createNewEntityInstance("MEDMedicaoDeContratos");
					wMedicao = new EntityWrapper(medicao);
					wMedicao.setValue("cliente", destacar+" "+rsMedicao.getString("usu_apecli"));
					wMedicao.setValue("contrato", rsMedicao.getLong("usu_numctr"));
					wMedicao.setValue("codEmpresaContratada",rsMedicao.getLong("numemp"));
					wMedicao.setValue("empresaContratada", destacar+rsMedicao.getString("nomemp"));
					SimpleDateFormat formatter = new SimpleDateFormat("MM/yyyy");
					wMedicao.setValue("responsavel", "Sra. Inês");
					wMedicao.setValue("periodo", formatter.format(datRef.getTime()));
					wMedicao.setValue("codRegional", rsMedicao.getLong("usu_codreg")); 
					wMedicao.setValue("regional", rsMedicao.getString("USU_NomReg"));
					nNumCtrNov = rsMedicao.getLong("usu_numctr");
					nNumCtrAnt = nNumCtrNov;
				}

				Long cargaHorPos = OrsegupsMedicaoUtils.cargaHorariaDoPosto
						(rsMedicao.getLong("usu_codemp"),rsMedicao.getLong("usu_codfil"), rsMedicao.getLong("usu_numctr"), rsMedicao.getLong("usu_numpos"));
				if(cargaHorPos != 0L){
					wColaborador.setValue("cargaHorPos", cargaHorPos+" horas");
				}else{
					wColaborador.setValue("cargaHorPos", "Carga horaria não cadastrada!");
				}				
				
				wColaborador.setValue("funcao", rsMedicao.getString("titred"));
				wColaborador.setValue("local", rsMedicao.getString("usu_lotorn"));
				wColaborador.setValue("numCadColaborador", rsMedicao.getLong("numcad"));
				wColaborador.setValue("nomeColaborador", destacar+rsMedicao.getString("nomfun"));
				wColaborador.setValue("datAdm", NeoUtils.safeDateFormat(rsMedicao.getDate("datadm"), "dd/MM/yyyy") + "");

				String desMed = desconto + " Dia(s)" + " ";
				wColaborador.setValue("descMed", desMed);
				wColaborador.setValue("situacao", rsMedicao.getString("DesSit"));
				colaboradores.add(colaboradorMedicao);
				PersistEngine.persist(colaboradorMedicao);

			}
			// Persist ultima medicao.
			if (nNumCtrNov != 0)
			{
				wMedicao.findField("listaColaboradores").setValue(colaboradores);
				PersistEngine.persist(medicao);
			}

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connVetorh, stMedicao, rsMedicao);
		}
	}

}
