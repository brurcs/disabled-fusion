package com.neomind.fusion.custom.orsegups.justificativaAfastamento.adapter;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class JAImportCID implements CustomJobAdapter {

    @Override
    public void execute(CustomJobContext ctx) {

	try {

	    InstantiableEntityInfo historico = AdapterUtils.getInstantiableEntityInfo("jaCID");

	    FileInputStream file = new FileInputStream(new File("c:\\teste\\cid.xlsx"));

	    // Create Workbook instance holding reference to .xlsx file
	    XSSFWorkbook workbook = new XSSFWorkbook(file);

	    // Get first/desired sheet from the workbook
	    XSSFSheet sheet = workbook.getSheetAt(0);

	    // Iterate through each rows one by one
	    Iterator<Row> rowIterator = sheet.iterator();
	    while (rowIterator.hasNext()) {

		Row row = rowIterator.next();

		Cell cellCod = row.getCell(0);

		String cod = cellCod.getStringCellValue();

		Cell cellDes = row.getCell(1);

		String des = cellDes.getStringCellValue();

		NeoObject objHistorico = historico.createNewInstance();
		EntityWrapper wrapperHistorico = new EntityWrapper(objHistorico);

		wrapperHistorico.findField("codigo").setValue(cod);
		wrapperHistorico.findField("descricao").setValue(des);

		PersistEngine.persist(objHistorico);

		// For each row, iterate through all the columns
		// Iterator<Cell> cellIterator = row.cellIterator();

		// while (cellIterator.hasNext())
		// {
		// Cell cell = cellIterator.next();
		// //Check the cell type and format accordingly
		// switch (cell.getCellType())
		// {
		// case Cell.CELL_TYPE_NUMERIC:
		// System.out.print(cell.getNumericCellValue() + "t");
		// break;
		// case Cell.CELL_TYPE_STRING:
		// System.out.print(cell.getStringCellValue() + "t");
		// break;
		// }
		// }
		System.out.println(cod + " "+ des);
	    }
	    file.close();
	} catch (Exception e) {
	    e.printStackTrace();
	}

    }

}
