package com.neomind.fusion.custom.orsegups.adapter.r001v2;

import java.math.BigInteger;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.engine.runtime.RuntimeEngine;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.security.SecurityEntity;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.TaskInstance;
import com.neomind.fusion.workflow.TaskInstanceActionType;
import com.neomind.fusion.workflow.TaskInstanceHelper;
import com.neomind.fusion.workflow.TaskLog;
import com.neomind.fusion.workflow.TaskLog.TaskLogType;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.AssignmentException;
import com.neomind.fusion.workflow.handler.HandlerFactory;
import com.neomind.fusion.workflow.handler.TaskHandler;
import com.neomind.fusion.workflow.task.central.search.TaskCentralIndex;
import com.neomind.fusion.workflow.task.rule.TaskRuleEngine;
import com.neomind.util.NeoUtils;

public class AlterarResponsavel implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);

		SecurityEntity seResponsavel = (SecurityEntity) (origin != null ? origin.getUser() : "");
		System.out.println(seResponsavel.toString());
		System.out.println(origin.getUser().toString());

		if (seResponsavel.toString().equals("dilmoberger")) {
			
		} else {
			
			
			Set<NeoUser> usuarios = origin.getUser().getGroup().getUpperLevel().getResponsible().getAllUsers();
			NeoUser usuario = new NeoUser();
			
			for (Iterator<NeoUser> it = usuarios.iterator();it.hasNext();) {				
				usuario = it.next();
			}
			
			//seResponsavel = origin.getUser().getGroup().getUpperLevel().getResponsible();
			origin.setUser(usuario);
			
			List<NeoObject> registroAtividades = (List<NeoObject>) processEntity.getValue("r001V2RegistroAtividade");		
			
			InstantiableEntityInfo insRegAti = AdapterUtils.getInstantiableEntityInfo("r001V2RegistroAtividades"); 
			NeoObject objRegAti = insRegAti.createNewInstance();
			EntityWrapper wRegAti = new EntityWrapper(objRegAti);
			
			
			GregorianCalendar dataAcao = new GregorianCalendar();
	
			wRegAti.setValue("usuario", "SISTEMA");
			wRegAti.setValue("dataAcao", dataAcao);
			wRegAti.setValue("descricao", "Tarefa <b>ESCALADA</b> para " + usuario.getFullName());
			wRegAti.setValue("anexo", null);
			
			PersistEngine.persist(objRegAti);
			registroAtividades.add(objRegAti);
	
			usuario = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", usuario.getCode()));
			
			processEntity.setValue("r001V2RegistroAtividade", registroAtividades);
			
			String codes = origin.getProcess().getCode() + ",";
			String model = "'R001V2ProgramacaoDeFerias'";
			String userDestinoCode = usuario.getCode() ;
			String motivo = "Tarefa ESCALADA para " + usuario.getFullName();
			
			transferirTarefa(codes, model, userDestinoCode, motivo);
			
		}		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}
	
	public String transferirTarefa(String codes, String model, String userDestinoCode, String motivo){
		
		String xCodes[] = codes.split(",");
		String strCodes = "'000000'";
		
		for (String x: xCodes){
			strCodes += ",'"+x+"'";
		}
		
		if (model == null ){ return "Você precisa selecionar o modelo de processo da tarefa"; }
		
		StringBuffer  varname1 = new StringBuffer();
		varname1.append("select ti.id, ");
		varname1.append("             (select code ");
		varname1.append("                from SecurityEntity n ");
		varname1.append("               where n.neoId = ti.owner_neoid) as actualUserCode ");
		varname1.append("        from WFProcess p ");
		varname1.append("  inner join Activity a on a.process_neoId = p.neoid ");
		varname1.append("  inner join Task t on t.activity_neoId = a.neoId ");
		varname1.append("  inner join Task_deadLine td on td.Task_neoId = t.neoId ");
		varname1.append("  inner join DeadLine d on d.neoId = td.deadLine_neoId ");
		varname1.append("  inner join TaskInstance ti on ti.task_neoId = t.neoId ");
		varname1.append("       where p.code in ( ");
		varname1.append(strCodes);
		varname1.append(" ) ");
		varname1.append("         and p.model_neoId = (select max(neoid) from ProcessModel pm1 where pm1.name = ");
		varname1.append(model);
		varname1.append(")");
		
		EntityManager entity = PersistEngine.getEntityManager();
		Integer cont = 1;
		if(entity != null)
		{
			Query q = entity.createNativeQuery(varname1.toString());
			List<Object[]> result = (List<Object[]>) q.getResultList();

			for (Object i[] : result)
			{
				Long id = ((BigInteger) i[0]).longValue();
				String actualUserCode = NeoUtils.safeOutputString(i[1]);
				
				try
				{
					//RuntimeEngine.getTaskService().delegate(Long.valueOf(id), actualUserCode, userDestinoCode, motivo);
					String taskId = String.valueOf(id);
				    String userId = actualUserCode;
		            String targetUserId = userDestinoCode;
		            String reason = motivo;
		
		            if (NeoUtils.safeIsNotNull(taskId) && NeoUtils.safeIsNotNull(userId) && NeoUtils.safeIsNotNull(targetUserId) && NeoUtils.safeIsNotNull(reason))
		            {
			
				       TaskInstance taskInstance = PersistEngine.getObject(TaskInstance.class, new QLEqualsFilter("id", Long.valueOf(taskId)));
				       NeoUser user = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", userId));
				       NeoUser targetUser = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", targetUserId));
				
				       delegate(taskInstance, user, targetUser, reason);
				       System.out.println("[ORSETOOL] - debug " + Long.valueOf(id) +" - "+ actualUserCode +" - "+ userDestinoCode +" - "+ motivo);
					   System.out.println(cont + " : Processo transferido" );
			
		            }
					
				}
				catch (Exception e)
				{
					e.printStackTrace();
					
					System.out.println("[ORSETOOL] - " + cont + " : ERRO: Processo não localizado!");
				}
				
				/*
				WFProcess proc = PersistEngine.getObject(WFProcess.class, new QLEqualsFilter("neoId", id));
				
				if (proc != null)
				{
					RuntimeEngine.getProcessService().cancel(proc, motivo);
					System.out.println(cont + " : Processo " + proc.getCode());
				}
				else
				{
					System.out.println(cont + " : ERRO: Processo não localizado!");
				} */
				cont++;
			}
			return "Transferencia Processada com Sucesso!";
		}else{
			return "Nenhuma tarefa encontrada para transferencia";
		}
		
	}
	
	public void delegate(TaskInstance taskInstance, NeoUser user, NeoUser targetUser, String reason)
	{
		if (targetUser == null)
			throw new AssignmentException("");

		Task task = taskInstance.getTask();

		TaskCentralIndex.getInstance().delete(taskInstance, true);

		taskInstance.getTask().getLogs().add(createLog(TaskLogType.TRANSFER, reason, user, taskInstance.getOwner()));

		TaskHandler taskHandler = HandlerFactory.TASK.get(taskInstance.getTask());
		Task t = taskHandler.transfer(user, targetUser);

		taskInstance.setOwner(t.getUser());

		// Remove a caixa
		RuntimeEngine.getTaskService().setBox(taskInstance, null);

		taskInstance.setActionType(TaskInstanceActionType.DELEGATED);

		// Remove os marcadores
		taskInstance.getTags().clear();

		// Executa regras
		this.refresh(taskInstance);

		taskInstance.setTask(t);
	}
	
	private TaskLog createLog(TaskLogType type, String reason, NeoUser user, NeoUser owner)
	{
		TaskLog log = new TaskLog();
		log.setAction(type);
		log.setDateLog(new GregorianCalendar());
		log.setDescription(reason);
		log.setUser(user);
		log.setOwner(owner);

		PersistEngine.persist(log);

		return log;
	}
	
	/**
	 * Método para atualizar os dados da tarefa, e executar novamente seus gatilhos.
	 * 
	 * @param taskInstance
	 */
	public void refresh(TaskInstance taskInstance)
	{
		TaskInstanceHelper.update(taskInstance);
		TaskRuleEngine.get().run(taskInstance);
	}

}
