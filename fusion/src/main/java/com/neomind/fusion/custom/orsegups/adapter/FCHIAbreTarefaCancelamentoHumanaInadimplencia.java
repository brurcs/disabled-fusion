package com.neomind.fusion.custom.orsegups.adapter;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.fusion.workflow.model.ProcessModel;

public class FCHIAbreTarefaCancelamentoHumanaInadimplencia implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try {
			NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "fernanda.maciel"));
			NeoObject contrato = (NeoObject) processEntity.findValue("contratoSapiens");
			if(solicitante == null)
			{
				System.out.println("Erro #4 - Solicitante não encontrado (#");
				return;
			}
			
			final ProcessModel pm = (ProcessModel) PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("name", "C029 - FCN - Cancelamento de Contrato Humana - Inadimplência"));
			if(pm == null)
			{
				System.out.println("Erro #6 - Erro ao recuperar modelo do processo!");
				throw new WorkflowException("Erro ao recuperar modelo do processo!");
			}
			//final WFProcess pmProcess = pm.startProcess(false, solicitante);
			//final NeoObject wkfFCC	= pmProcess.getEntity();
			InstantiableEntityInfo objFormPrincipal = AdapterUtils.getInstantiableEntityInfo("FCNCancelamentoHumanaInadimplencia");
			final NeoObject wkfFCC	= objFormPrincipal.createNewInstance();
			final EntityWrapper ewWkfFCC = new EntityWrapper(wkfFCC);
			
			ewWkfFCC.findField("contratoSapiens").setValue(contrato);
			
			//pmProcess.setSaved(true);
			//pmProcess.setRequester(solicitante);
			
			// Necessita persistir para que o metodo getOpenActivities() consiga buscar as atividades abertas
			//PersistEngine.persist(pmProcess);
			//PersistEngine.commit(true);
	
	
			// Trata a primeira tarefa
			/*Task task = null;
			final List acts = pmProcess.getOpenActivities();
			final Activity activity1 = (Activity) acts.get(0);
			if (activity1 instanceof UserActivity)
			{
				try
				{
					if (((UserActivity) activity1).getTaskList().size() <= 0)
					{
						task = activity1.getModel().getTaskAssigner().assign((UserActivity) activity1, solicitante);
						OrsegupsWorkflowHelper.finishTaskByActivity(activity1);
					} 
					else
					{
						task = ((UserActivity) activity1).getTaskList().get(0);
						OrsegupsWorkflowHelper.finishTaskByActivity(activity1);
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
					System.out.println("Erro ao avançar a primeira tarefa!");
					throw new WorkflowException("Erro ao avançar a primeira tarefa!");
				}
			}*/	
			String tarefa = OrsegupsWorkflowHelper.iniciaProcesso(pm, wkfFCC, false, solicitante , true, solicitante);
			System.out.println("Tarefa "+tarefa +" de Cancelamento Humana Inadimplência, do Contrato "+contrato);
		} catch (Exception e) {
			e.printStackTrace();
			throw new WorkflowException("Erro Abrir Tarefa do Processo Cancelamento Hunana Inadimplência!");
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}
	
}
