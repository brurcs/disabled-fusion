package com.neomind.fusion.custom.orsegups.callcenter;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.jms.NeoJMSPublisher;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.datawarehouse.ExternalEntityInfo;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

import flex.messaging.util.URLDecoder;

@SuppressWarnings("serial")
@WebServlet(name = "OrsegupsIncommingCallServlet", urlPatterns = { "/servlet/com.neomind.fusion.custom.orsegups.callcenter.OrsegupsIncommingCallServlet" }, asyncSupported = true)
public class OrsegupsIncommingCallServlet extends HttpServlet
{
	private static final Log log = LogFactory.getLog(OrsegupsIncommingCallServlet.class);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	@SuppressWarnings({ "rawtypes" })
	private void doRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		resp.setContentType("text/plain");
		resp.setCharacterEncoding("ISO-8859-1");

		System.out.println(URLDecoder.decode(req.getQueryString()));
		
		Gson gson = new Gson();
		String externalNumber = null;
		String extensionNumber = null;
		String searchObjectJson = req.getParameter("searchObject");

		//validacoes para busca manual a partir da tela do callcenter
		SearchObjectVO searchObjectVO = null;
		if (searchObjectJson != null && !searchObjectJson.isEmpty())
		{
			searchObjectVO = gson.fromJson(searchObjectJson, SearchObjectVO.class);
		}

		// busca os dados da chamada feita pelo snep que imprime o json no corpo da requisicao
		BufferedReader inputStream = new BufferedReader(new InputStreamReader(req.getInputStream()));
		String line = null;
		StringBuilder responseData = new StringBuilder();
		while ((line = inputStream.readLine()) != null)
		{
			responseData.append(line);
		}
		String json = responseData.toString();

		if (json != null && !json.isEmpty())
		{
			SnepVO snepVO = gson.fromJson(json, SnepVO.class);
			if(log.isDebugEnabled())
				log.debug("Dados recebidos SNEP: " + responseData.toString());

			externalNumber = snepVO.getCallerid();
			extensionNumber = snepVO.getExten();
		}
		else
		{
			externalNumber = req.getParameter("externalNumber");
			extensionNumber = req.getParameter("extensionNumber");

			if(log.isDebugEnabled())
				log.debug("Dados recebidos chamanda manual GET: externalNumber=" + externalNumber + " - extensionNumber=" + extensionNumber);
		}

		//se neste ponto se o searchObjectVO for nulo é uma busca feita a partir do snep (central telefonica) 
		if (searchObjectVO == null)
		{
			searchObjectVO = new SearchObjectVO();
			searchObjectVO.setTelefoneSearch(externalNumber);
			searchObjectVO.setTipoBusca("snepSearch");
		}

		if (extensionNumber != null && searchObjectVO != null)
		{
			try
			{
				//valida se o ramal que esta recebendo a ligacao e do callcenter
				Class clazz = AdapterUtils.getEntityClass("ListaTelefonesCallCenter");
				Long extensionFilter = PersistEngine.countObjects(clazz, new QLEqualsFilter("ramal", extensionNumber));
				if (extensionFilter == null || extensionFilter.equals(0L))
				{
					//ramal nao faz parte do callcenter, logo nao devera ser buscado os dados do banco nem enviada mensagem para esta ligacao
					if(log.isDebugEnabled())
						log.debug("Desconsiderando requisicao pois ramal nao faz parte do callCenter! Ramal: " + extensionNumber);
					return;
				}

				if (searchObjectVO != null && !searchObjectVO.canSearch())
				{
					if(log.isDebugEnabled())
						log.debug("Descartando busca manual pois o termo pesquisado e nulo ou vazio!");
					return;
				}

				//TODO ADICIONAR CHAVEAMENTO
				Integer ramal = null;
				boolean novoAtendimento = false;
				try {
				    ramal = Integer.parseInt(extensionNumber);				    				    
				} catch (Exception e) {
				    e.printStackTrace();
				}
				
				if (ramal != null){
				    novoAtendimento = this.checkChaveamento(ramal);
				}
				
				if (novoAtendimento){
				    String payload = "{\"exten\": \""+extensionNumber+"\", \"callerid\": \""+externalNumber+"\", \"channel\": \"SIP/"+extensionNumber+"\", \"calleridname\": \""+externalNumber+"\"}";
				    
				    try{
					URL url = new URL("http://apps2.orsegups.com.br:8070/call-center/snep/incommingCall");
					HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
					httpCon.setDoOutput(true);
					httpCon.setRequestMethod("POST");
					httpCon.addRequestProperty("Content-Type", "application/json");
					OutputStream os = httpCon.getOutputStream();
					OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");    
					osw.write(payload);
					osw.flush();
					osw.close();
					os.close();  //don't forget to close the OutputStream
					httpCon.connect();
					
					//read the inputstream and print it
					String result;
					BufferedInputStream bis = new BufferedInputStream(httpCon.getInputStream());
					ByteArrayOutputStream buf = new ByteArrayOutputStream();
					int result2 = bis.read();
					while(result2 != -1) {
					    buf.write((byte) result2);
					    result2 = bis.read();
					}
					result = buf.toString();
					System.out.println("Resultado do chaveamento do ramal "+extensionNumber+" para o numero "+externalNumber+": "+result);
					
				    }catch (Exception e){
					e.printStackTrace();
				    }
				    
				    
				}else{
				    CallCenterResultVO result = this.doSearch(searchObjectVO);
				    result.setExternalNumber(externalNumber);
				    
				    String callCenterJSON = gson.toJson(result);
				    
				    NeoJMSPublisher.getInstance().sendCallCenterMessage(URLEncoder.encode(callCenterJSON, "UTF-8"), extensionNumber);
				}
				
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			if(log.isDebugEnabled())
				log.debug("Erro ao processar ligacao recebida: externalNumber:" + externalNumber + " - extensionNumber: " + extensionNumber);
		}

	}
	
	private boolean checkChaveamento(int ramal){
	    
	    Connection conn = null;
	    PreparedStatement pstm = null;
	    ResultSet rs = null;

	    StringBuilder sql = new StringBuilder();
	    sql.append(" SELECT 1 FROM chaveamento_callcenter WHERE ramal = ? AND ativo = 1");
	    	
	    try {
		conn = PersistEngine.getConnection("TIDB");
		pstm = conn.prepareStatement(sql.toString());
		
		pstm.setInt(1, ramal);
		
		rs = pstm.executeQuery();

		if (rs.next()) {
		  return true;  
		}

	    } catch (SQLException e) {
		e.printStackTrace();
	    } finally {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    }
	    
	    return false;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private CallCenterResultVO doSearch(SearchObjectVO searchObjectVO)
	{
		CallCenterResultVO result = new CallCenterResultVO();

		if (searchObjectVO != null)
		{
			List<String> codigosSigma = new ArrayList<String>();
			List<String> codigosSapiens = new ArrayList<String>();

			//busca pelo telefone

			if (searchObjectVO.getTelefoneSearch() != null && !searchObjectVO.getTelefoneSearch().trim().isEmpty())
			{
				Class clazz = AdapterUtils.getEntityClass("SAPIENSUSUT500FNE");
				Collection<NeoObject> codigosTelefone = PersistEngine.getObjects(clazz, new QLEqualsFilter("usu_numfne", searchObjectVO.getTelefoneSearch()));

				if (codigosTelefone != null && !codigosTelefone.isEmpty()){
					for (NeoObject codigoTelefone : codigosTelefone)
					{
						EntityWrapper codigoTelefoneWrapper = new EntityWrapper(codigoTelefone);

						String codigoSigma = (String) codigoTelefoneWrapper.findValue("usu_cd_clie");
						String codigoSapiens = (String) codigoTelefoneWrapper.findValue("usu_codcli");

						if (codigoSigma != null && !codigoSigma.trim().isEmpty() && !codigoSigma.equals("0"))
						{
							codigosSigma.add(codigoSigma.trim());
						}

						if (codigoSapiens != null && !codigoSapiens.trim().isEmpty() && !codigoSapiens.equals("0"))
						{
							codigosSapiens.add(codigoSapiens.trim());
						}
					}

					searchObjectVO.setCodigosSigma(codigosSigma);
					searchObjectVO.setCodigosSapiens(codigosSapiens);
                    		} else {
                    		    // Verificação de telefone sem o nono digito
                    		    String telefone = searchObjectVO.getTelefoneSearch();
                    
                    		    if (telefone.length() > 10) {
                    			String subFone = telefone.substring(3);
                    
                    			if (subFone.startsWith("7") || subFone.startsWith("8") || subFone.startsWith("9")) {
                    			    telefone = telefone.substring(0, 2) + subFone;
                    
                    			    codigosTelefone = PersistEngine.getObjects(clazz, new QLEqualsFilter("usu_numfne", telefone));
                    
                    			    if (codigosTelefone != null && !codigosTelefone.isEmpty()) {
                    				for (NeoObject codigoTelefone : codigosTelefone) {
                    				    EntityWrapper codigoTelefoneWrapper = new EntityWrapper(codigoTelefone);
                    
                    				    String codigoSigma = (String) codigoTelefoneWrapper.findValue("usu_cd_clie");
                    				    String codigoSapiens = (String) codigoTelefoneWrapper.findValue("usu_codcli");
                    
                    				    if (codigoSigma != null && !codigoSigma.trim().isEmpty() && !codigoSigma.equals("0")) {
                    					codigosSigma.add(codigoSigma.trim());
                    				    }
                    
                    				    if (codigoSapiens != null && !codigoSapiens.trim().isEmpty() && !codigoSapiens.equals("0")) {
                    					codigosSapiens.add(codigoSapiens.trim());
                    				    }
                    				}
                    
                    				searchObjectVO.setCodigosSigma(codigosSigma);
                    				searchObjectVO.setCodigosSapiens(codigosSapiens);
                    
                    			    }
                    
                    			}
                    		    }
                    
                    		}

				
				
			}

			Collection<CallCenterVO> callCenterVOs = new ArrayList<CallCenterVO>();
			Collection<SigmaVO> sigmaVOs = new ArrayList<SigmaVO>();

			callCenterVOs = this.getDadosContrato(searchObjectVO);
			sigmaVOs = this.getDadosContaSigma(searchObjectVO);

			result.setContratos(callCenterVOs);
			result.setContas(sigmaVOs);
			result.setSearch(searchObjectVO);
		}

		return result;
	}

	private CallCenterVO composeCallCenterVO(Object[] object)
	{
		CallCenterVO callCenterVO = null;

		if (object != null)
		{
			//DADOS GERAIS DO CLIENTE
			Integer codigo = (Integer) object[0];
			String razaoSocial = (String) object[1];
			String nomeFantasia = (String) object[2];
			BigInteger cnpjCpf = (BigInteger) object[3]; //BigInteger
			String tipoCliente = (String) object[4];
			String tipoContrato = ((Short) object[5] != null) ? ((Short) object[5]).toString() : "";

			// DADOS ENDEREÇO DO CLIENTE
			String enderecoCliente = (String) object[6];
			String complementoCliente = (String) object[7];
			String bairroClient = (String) object[8];
			Integer cepCliente = (Integer) object[9];
			String cidadeCliente = (String) object[10];
			String ufCliente = (String) object[11];
			//DADOS ENDEREÇO DE COBRANÇA
			String enderecoCobranca = (String) object[12];
			String numeroCobranca = (String) object[13];
			String complementoCobranca = (String) object[14];
			Integer cepCobranca = (Integer) object[15];
			String cidadeCobranca = (String) object[16];
			String ufCobranca = (String) object[17];

			//CONTATOS - TELEFONES/E-MAIL
			String telefone1 = (String) object[18];
			String telefone2 = (String) object[19];
			String telefone3 = (String) object[20];
			String telefone4 = (String) object[21];
			String telefone5 = (String) object[22];
			String foneFax = (String) object[23];
			String foneContrato = (String) object[24];
			String emailCliente = (String) object[25];
			String emailContrato = (String) object[26];
			String pessoaContato = (String) object[27];

			//DADOS DO CONTRATO
			Integer numeroContrato = (Integer) object[28];
			String numeroOficialContrato = (String) object[29];
			String regionalContrato = (String) object[30];
			String situacaoContrato = (String) object[31];
			Integer dataBaseContrato = ((Short) object[32] != null) ? ((Short) object[32]).intValue() : null;
			String formPagamentoContrato = (String) object[33];
			Integer vencimento = ((Short) object[34] != null) ? ((Short) object[34]).intValue() : null;
			String inicioVigenciaContrato = OrsegupsUtils.getFormatedTimeStampDate((Timestamp) object[35]);
			String fimVigenciaContrato = OrsegupsUtils.getFormatedTimeStampDate((Timestamp) object[36]);
			String inicioFaturamentoContrato = OrsegupsUtils.getFormatedTimeStampDate((Timestamp) object[37]);
			String fimFaturamentoContrato = OrsegupsUtils.getFormatedTimeStampDate((Timestamp) object[38]);
			Integer codigoMotivoInativacao = (Integer) object[39];
			String motivoInativacao = (String) object[40];
			String dataInativacao = OrsegupsUtils.getFormatedTimeStampDate((Timestamp) object[41]);
			String codigoServico = (String) object[42];

			callCenterVO = new CallCenterVO();
			callCenterVO.setCodigo(codigo);
			callCenterVO.setRazaoSocial(razaoSocial);
			callCenterVO.setNomeFantasia(nomeFantasia);
			callCenterVO.setCnpjCpf(cnpjCpf);
			if (callCenterVO.getCnpjCpf() != null)
			{
				
				String eq = PortalUtil.getBaseURL();
				String btPesquisaSenha = "<img style=\"cursor: pointer; cursor: hand;\" src='" + eq + "custom/jsp/orsegups/jms/images/searchbox_button.png' onclick=\"recuperaSenhaSite(" + callCenterVO.getCnpjCpf().toString() + ",1)\" /> Pesquisar";
				callCenterVO.setSenhaSite(btPesquisaSenha);

				//callCenterVO.setSenhaSite(recuperaSenhaSite(callCenterVO.getCnpjCpf().toString()));
			}

			if (tipoCliente != null)
			{
				if (tipoCliente.equalsIgnoreCase("J"))
				{
					callCenterVO.setTipoCliente("Pessoa Jurídica");
				}
				else if (tipoCliente.equalsIgnoreCase("F"))
				{
					callCenterVO.setTipoCliente("Pessoa Física");
				}
			}

			if (tipoContrato != null)
			{
				if (tipoContrato.equalsIgnoreCase("1"))
				{
					callCenterVO.setTipoContrato("Contrato Privado");
				}
				else if (tipoContrato.equalsIgnoreCase("2"))
				{
					callCenterVO.setTipoContrato("Contrato Público");
				}
			}
			// DADOS ENDEREÇO DO CLIENTE
			callCenterVO.setEnderecoCliente(enderecoCliente);
			callCenterVO.setComplementoCliente(complementoCliente);
			callCenterVO.setBairroClient(bairroClient);
			callCenterVO.setCepCliente(cepCliente);
			callCenterVO.setCidadeCliente(cidadeCliente);
			callCenterVO.setUfCliente(ufCliente);
			//DADOS ENDEREÇO DE COBRANÇA
			callCenterVO.setEnderecoCobranca(enderecoCobranca);
			callCenterVO.setNumeroCobranca(numeroCobranca);
			callCenterVO.setComplementoCobranca(complementoCobranca);
			callCenterVO.setCepCobranca(cepCobranca);
			callCenterVO.setCidadeCobranca(cidadeCobranca);
			callCenterVO.setUfCobranca(ufCobranca);
			//CONTATOS - TELEFONES/E-MAIL
			callCenterVO.setTelefone1(telefone1);
			callCenterVO.setTelefone2(telefone2);
			callCenterVO.setTelefone3(telefone3);
			callCenterVO.setTelefone4(telefone4);
			callCenterVO.setTelefone5(telefone5);
			callCenterVO.setFoneFax(foneFax);
			callCenterVO.setFoneContrato(foneContrato);
			callCenterVO.setEmailCliente(emailCliente);
			callCenterVO.setEmailContrato(emailContrato);
			callCenterVO.setPessoaContato(pessoaContato);
			//DADOS DO CONTRATO
			callCenterVO.setNumeroContrato(numeroContrato);
			callCenterVO.setNumeroOficialContrato(numeroOficialContrato);
			callCenterVO.setRegionalContrato(regionalContrato);
			callCenterVO.setSituacaoContrato(situacaoContrato);
			callCenterVO.setDataBaseContrato(dataBaseContrato);
			callCenterVO.setFormPagamentoContrato(formPagamentoContrato);
			callCenterVO.setVencimento(vencimento);
			callCenterVO.setInicioVigenciaContrato(inicioVigenciaContrato);
			callCenterVO.setFimVigenciaContrato(fimVigenciaContrato);
			callCenterVO.setInicioFaturamentoContrato(inicioFaturamentoContrato);
			callCenterVO.setFimFaturamentoContrato(fimFaturamentoContrato);
			callCenterVO.setCodigoMotivoInativacao(codigoMotivoInativacao);
			callCenterVO.setMotivoInativacao(motivoInativacao);
			callCenterVO.setDataInativacao(dataInativacao);
			callCenterVO.setCodigoServico(codigoServico);

			//id
			callCenterVO.setId(numeroContrato);

			//label
			if (numeroContrato != null)
			{
				callCenterVO.setTreeLabel(numeroContrato.toString());
			}
			else
			{
				callCenterVO.setTreeLabel("Número do contrato não encontrado!");
			}

			callCenterVO.setIsContrato(true);

			//tratar as datas 
			if (callCenterVO.getInicioVigenciaContrato() != null && callCenterVO.getInicioVigenciaContrato().equalsIgnoreCase("31/12/1900"))
			{
				callCenterVO.setInicioVigenciaContrato("");
			}
			if (callCenterVO.getFimVigenciaContrato() != null && callCenterVO.getFimVigenciaContrato().equalsIgnoreCase("31/12/1900"))
			{
				callCenterVO.setFimVigenciaContrato("");
			}
			if (callCenterVO.getInicioFaturamentoContrato() != null && callCenterVO.getInicioFaturamentoContrato().equalsIgnoreCase("31/12/1900"))
			{
				callCenterVO.setInicioFaturamentoContrato("");
			}
			if (callCenterVO.getFimFaturamentoContrato() != null && callCenterVO.getFimFaturamentoContrato().equalsIgnoreCase("31/12/1900"))
			{
				callCenterVO.setFimFaturamentoContrato("");
			}
			if (callCenterVO.getDataInativacao() != null && callCenterVO.getDataInativacao().equalsIgnoreCase("31/12/1900"))
			{
				callCenterVO.setDataInativacao("");
			}

			//			if(CallCenterSearchProcessServlet.getRSC(String.valueOf((Integer) object[0])))
			//			{
			//				callCenterVO.setPossuiRSC(true);
			//			}else
			//				callCenterVO.setPossuiRSC(false);
			//			
			//			if(CallCenterSearchProcessServlet.getRRC(String.valueOf((Integer) object[0])))
			//			{
			//				callCenterVO.setPossuiRRC(true);
			//			}else
			//				callCenterVO.setPossuiRRC(false);

		}

		return callCenterVO;
	}

	@SuppressWarnings("unchecked")
	private Collection<CallCenterVO> getDadosContrato(SearchObjectVO searchObjectVO)
	{
		if (searchObjectVO.getTipoBusca().equalsIgnoreCase("advancedSearch"))
		{
			//quando for busca avancada realizar a busca somente das cotnas do sigma e nao do contrato sapiens
			return new ArrayList<CallCenterVO>();
		}
		else if (searchObjectVO.getTipoBusca().equalsIgnoreCase("snepSearch") && (searchObjectVO.getCodigosSapiens() == null || searchObjectVO.getCodigosSapiens().isEmpty()))
		{
			//Consulta snep eh somente por numero de telefone, e quando codigosSapeins for vazio signnifica que nao tem nenhum contrato com p telefone pesquisado
			return new ArrayList<CallCenterVO>();
		}

		String term = searchObjectVO.getTermoGenerico();
		Long longTerm = SearchObjectVO.getTermoLong(term);

		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT  ");
		//DADOS GERAIS DO CLIENTE
		sql.append("    CLI.CODCLI AS Codigo_Cliente, ");
		sql.append("	CLI.NOMCLI AS Razao_Social, ");
		sql.append("	CLI.APECLI AS Nome_Fantasia, ");
		sql.append("	CLI.CGCCPF AS Cnpj_Cpf, ");
		sql.append("	CLI.TIPCLI AS Juridico_Fisica, ");
		sql.append("	CLI.TIPEMC AS Publico_Privado, ");
		// DADOS ENDEREÇO DO CLIENTE
		sql.append("	CLI.ENDCLI AS Endereço_Cliente, ");
		sql.append("	CLI.CPLEND AS Complmento,  ");
		sql.append("	CLI.BAICLI AS Bairro, ");
		sql.append("	CLI.CEPCLI AS Cep, ");
		sql.append("	CLI.CIDCLI AS Cidade, ");
		sql.append("	CLI.SIGUFS AS Uf, ");
		//DADOS ENDEREÇO DE COBRANÇA
		sql.append("	CLI.ENDCOB AS Endereço_Cobrança, ");
		sql.append("	CLI.NENCOB AS Número_Cobrança, ");
		sql.append("	CLI.CPLCOB AS Coplemento, ");
		sql.append("	CLI.CEPCOB AS Cep_Cobrança, ");
		sql.append("	CLI.CIDCOB AS Cidade_Cobrança, ");
		sql.append("	CLI.ESTCOB AS UF_Cobrança,  ");
		//CONTATOS - TELEFONES/E-MAIL
		sql.append("	CLI.FONCLI AS Fone_1,  ");
		sql.append("	CLI.FONCL2 AS Fone_2, ");
		sql.append("	CLI.FONCL3 AS Fone_3, ");
		sql.append("	CLI.FONCL4 AS Fone_4, ");
		sql.append("	CLI.FONCL5 AS Fone_5, ");
		sql.append("	CLI.FAXCLI AS Fone_Fax, ");
		sql.append("	CTR.USU_CLIFON AS Fone_Contrato, ");
		sql.append("	CLI.INTNET AS Email_Cliente, ");
		sql.append("	CTR.USU_EMACTR AS Email_contrato, ");
		sql.append("	CTR.USU_CLICON AS Pessoa_de_contato_contrato, ");
		//DADOS DO CONTRATO 
		sql.append("	CTR.USU_NUMCTR AS Numero_Contrato,  ");
		sql.append("	CTR.USU_NUMOFI AS Numero_Oficial,  ");
		sql.append("	CAST( CTR.USU_REGCTR AS VARCHAR(10)) + ' - ' + REG.USU_NOMREG AS Regional_contrato, "); // codigo e descricao da regional
		sql.append("	CTR.USU_SITCTR AS Situação,  ");
		sql.append("	CTR.USU_DIABAS AS Data_base, ");
		sql.append("	CAST( CTR.USU_CODFPG AS VARCHAR(10)) + ' - ' + FPG.DESFPG AS Forma_pagamento, "); // codigo e descricao forma pagamento
		sql.append("	CTR.USU_DIAFIX AS Vencimento,  ");
		sql.append("	CTR.USU_INIVIG AS Inicio_vigencia,  ");
		sql.append("	CTR.USU_FIMVIG AS Fim_vigencia,  ");
		sql.append("	CTR.USU_DATINI AS Inicio_Faturamento,  ");
		sql.append("	CTR.USU_DATFIM AS Fim_Faturamento, ");
		sql.append("	CTR.USU_CODMOT AS Codigo_motivo_inativaçao, ");
		sql.append("	CTR.USU_OBSMOT AS Motivo_Inativaçao, ");
		sql.append("	CTR.USU_DATMOT AS Data_Inativaçao, ");
		sql.append("	CAST( CTR.USU_SERCTR AS VARCHAR(10)) + ' - ' + SCT.USU_DESSER AS TIPO_SERVIÇO_CONTRATO "); //codigo e descricao tipo servico
		sql.append(" FROM   ");
		sql.append("	E085CLI CLI, USU_T160CTR CTR, USU_T200REG REG, E066FPG FPG, USU_T160SCT SCT ");
		sql.append(" WHERE  ");
		sql.append("	CTR.USU_CODCLI = CLI.CODCLI  ");
		sql.append("	AND ");
		sql.append("    CTR.USU_REGCTR = REG.USU_CODREG	");
		sql.append("	AND ");
		sql.append("	CTR.USU_CODEMP = FPG.CODEMP	");
		sql.append("	AND ");
		sql.append("	CTR.USU_CODFPG = FPG.CODFPG	");
		sql.append("	AND ");
		sql.append("	CTR.USU_SERCTR = SCT.USU_SERCTR	");
		sql.append("	AND ");
		sql.append("	CLI.TIPEMC = 1  ");
		sql.append("	AND ");
		//SOMENTE CONTRATOS ATIVOS OU INATIVOS COM DATA DE INATIVAÇÃO MAIOR QUE DATA ATUAL 
		sql.append("	( ");
		sql.append("		(CTR.USU_SITCTR = 'A' )  ");
		sql.append("		OR  ");
		sql.append("		( ");
		sql.append("			CTR.USU_SITCTR = 'I'  ");
		sql.append("			AND ");
		sql.append("			CTR.USU_DATFIM >=GETDATE() ");
		sql.append("		) ");
		sql.append("	) ");
		sql.append("	AND ");
		sql.append("	( ");
		if (searchObjectVO.getTipoBusca().equalsIgnoreCase("quickSearch"))
		{
			//Filtro de NOME FANTASIA
			sql.append("		CLI.APECLI LIKE '%" + term + "%' ");
			sql.append("		OR  ");
			sql.append("		CLI.NOMCLI LIKE '%" + term + "%' ");
			sql.append("		OR  ");
			sql.append("		CLI.INTNET LIKE '%" + term + "%' ");
			sql.append("		OR  ");
			sql.append("		CTR.USU_EMACTR LIKE '%" + term + "%' ");
			if (longTerm != null)
			{
				sql.append("		OR  ");
				sql.append("		CLI.CGCCPF = " + longTerm + " ");
				sql.append("		OR  ");
				sql.append("		CLI.CODCLI = " + longTerm + " ");

			}
		}
		if (searchObjectVO.getCodigosSapiens() != null && !searchObjectVO.getCodigosSapiens().isEmpty())
		{
			//se passou pelo quickSearch tem que adicionar um OR
			if (searchObjectVO.getTipoBusca().equalsIgnoreCase("quickSearch"))
			{
				sql.append("		OR  ");
			}

			if (searchObjectVO.getCodigosSapiens().size() == 1)
			{
				sql.append("		CLI.CODCLI = " + searchObjectVO.getCodigosSapiens().get(0) + " ");
			}
			else
			{
				sql.append("		CLI.CODCLI IN (" + SearchObjectVO.getCodigosSigmaInFormtat(searchObjectVO.getCodigosSapiens()) + ") ");
			}
		}
		sql.append("	) ");
		sql.append("ORDER BY  ");
		sql.append("	CLI.NOMCLI,CTR.USU_NUMCTR ");

		Map<Integer, CallCenterVO> existingClients = new HashMap<Integer, CallCenterVO>();

		Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		Collection<Object> resultList = query.getResultList();

		Collection<CallCenterVO> callCenterVOs = new ArrayList<CallCenterVO>();

		if (resultList != null)
		{
			for (Object result : resultList)
			{
				if (result != null)
				{
					CallCenterVO callCenterVO = this.composeCallCenterVO((Object[]) result);
					if (callCenterVO != null)
					{
						CallCenterVO client = existingClients.get(callCenterVO.getCodigo());

						if (client == null)
						{
							client = this.composeClientVO(callCenterVO);
							existingClients.put(client.getCodigo(), client);

							callCenterVOs.add(client);
						}

						client.addChild(callCenterVO);
					}
				}
			}
		}

		return callCenterVOs;
	}

	private CallCenterVO composeClientVO(CallCenterVO callCenterVO)
	{
		CallCenterVO clientVO = null;

		if (callCenterVO != null)
		{
			//DADOS GERAIS DO CLIENTE
			clientVO = new CallCenterVO();
			clientVO.setCodigo(callCenterVO.getCodigo());
			clientVO.setRazaoSocial(callCenterVO.getRazaoSocial());
			clientVO.setNomeFantasia(callCenterVO.getNomeFantasia());
			clientVO.setCnpjCpf(callCenterVO.getCnpjCpf());
			clientVO.setTipoCliente(callCenterVO.getTipoCliente());
			//clientVO.setTipoContrato(callCenterVO.getTipoContrato());

			//id
			clientVO.setId(callCenterVO.getCodigo());

			//label
			clientVO.setTreeLabel(callCenterVO.getNomeFantasia() + " - " + callCenterVO.getCnpjCpf());

			clientVO.setIsContrato(false);
		}

		return clientVO;
	}

	@SuppressWarnings("unchecked")
	private Collection<SigmaVO> getDadosContaSigma(SearchObjectVO searchObjectVO)
	{
		if (searchObjectVO.getTipoBusca().equalsIgnoreCase("snepSearch") && (searchObjectVO.getCodigosSigma() == null || searchObjectVO.getCodigosSigma().isEmpty()))
		{
			//Consulta snep eh somente por numero de telefone, e quando codigosSigma for vazio signnifica que nao tem nenhuma conta com o telefone pesquisado
			return new ArrayList<SigmaVO>();
		}

		String term = searchObjectVO.getTermoGenerico();
		Long longTerm = SearchObjectVO.getTermoLong(term);

		StringBuilder sql = new StringBuilder();
		sql.append("SELECT ");
		//DADOS GERAIS DA CONTA
		sql.append("CEN.cd_cliente, ");
		sql.append("CEN.ID_empresa, ");
		sql.append("CEN.ID_central, ");
		sql.append("CEN.particao, ");
		sql.append("ROT.NM_rota AS Rota, ");
		sql.append("CEN.razao as Razao_social, ");
		sql.append("CEN.cgccpf as Cnpj_cpf, ");
		sql.append("CEN.endereco as Logradouro, ");
		sql.append("BAI.nome AS Bairro, ");
		sql.append("CID.nome AS Cidade, ");
		sql.append("CEN.id_estado AS Uf, ");
		sql.append("CEN.cep AS Cep, ");
		//PALAVRA CHAVE
		sql.append("CEN.PERGUNTA, ");
		sql.append("CEN.RESPOSTA, ");
		//CONTATOS DA CONTA
		sql.append("CEN.RESPONSAVEL, ");
		sql.append("CEN.EMAILRESP, ");
		sql.append("CEN.FONE1, ");
		sql.append("CEN.FONE2, ");
		sql.append("EMP.NM_COMPLEMENTO ");
		sql.append("FROM dbo.dbcentral AS CEN ");
		sql.append("INNER JOIN dbo.rota AS ROT ON ROT.cd_rota = CEN.id_rota ");
		sql.append("INNER JOIN dbo.dbcidade AS CID ON CID.id_cidade = CEN.id_cidade ");
		sql.append("INNER JOIN dbo.dbbairro AS BAI ON BAI.id_bairro = CEN.id_bairro ");
		sql.append(" INNER JOIN dbo.EMPRESA AS EMP ON EMP.CD_EMPRESA = CEN.ID_EMPRESA ");
		sql.append("WHERE  ");
		sql.append("	CEN.ctrl_central = 1  ");
		//sql.append("	AND ");
		//sql.append("	CEN.tp_pessoa <> 2 ");
		sql.append("	AND ");
		sql.append("	( ");
		if (searchObjectVO.getTipoBusca().equalsIgnoreCase("quickSearch"))
		{
			if (longTerm != null)
			{
				sql.append("		CEN.cd_cliente = " + longTerm + " ");
				sql.append("		OR  ");
			}
			String codeUser = "";
			NeoUser user = PortalUtil.getCurrentUser();
			if (user != null && !user.getCode().equals(""))
			{
				codeUser = user.getCode();
			}
			
			if(log.isDebugEnabled())
				log.debug("LOG PESQUISA CALLCENTER - USUARIO: " + codeUser + " - PESQUISA: "  + term);
			
			sql.append("		CEN.razao LIKE '%" + term + "%' ");
			sql.append("		OR  ");
			sql.append("		CEN.cgccpf = '" + term + "' ");
			sql.append("		OR  ");
			sql.append("		CEN.EMAILRESP like '%" + term + "%' ");
		}
		else if (searchObjectVO.getTipoBusca().equalsIgnoreCase("advancedSearch"))
		{
			Boolean addAnd = false;

			if (SearchObjectVO.getTermoLong(searchObjectVO.getCodigoClienteSearch()) != null)
			{
				if (!addAnd)
				{
					addAnd = true;
				}
				else
				{
					sql.append("		AND  ");
				}
				sql.append("		CEN.cd_cliente = " + SearchObjectVO.getTermoLong(searchObjectVO.getCodigoClienteSearch()) + " ");
			}
			if (searchObjectVO.getRazaoSearch() != null && !searchObjectVO.getRazaoSearch().isEmpty())
			{
				if (!addAnd)
				{
					addAnd = true;
				}
				else
				{
					sql.append("		AND  ");
				}
				sql.append("		CEN.razao LIKE '%" + searchObjectVO.getRazaoSearch() + "%' ");
			}
			if (searchObjectVO.getCpfCnpjSearch() != null && !searchObjectVO.getCpfCnpjSearch().isEmpty())
			{
				if (!addAnd)
				{
					addAnd = true;
				}
				else
				{
					sql.append("		AND  ");
				}
				sql.append("		CEN.cgccpf LIKE '%" + searchObjectVO.getCpfCnpjSearch() + "%' ");
			}
			//resultado da busca por telefone
			if (searchObjectVO.getCodigosSigma() != null && !searchObjectVO.getCodigosSigma().isEmpty())
			{
				if (!addAnd)
				{
					addAnd = true;
				}
				else
				{
					sql.append("		AND  ");
				}

				if (searchObjectVO.getCodigosSigma().size() == 1)
				{
					sql.append("		CEN.cd_cliente = " + searchObjectVO.getCodigosSigma().get(0) + " ");
				}
				else
				{
					sql.append("		CEN.cd_cliente IN (" + SearchObjectVO.getCodigosSigmaInFormtat(searchObjectVO.getCodigosSigma()) + ") ");
				}
			}
			if (searchObjectVO.getEnderecoSearch() != null && !searchObjectVO.getEnderecoSearch().isEmpty())
			{
				if (!addAnd)
				{
					addAnd = true;
				}
				else
				{
					sql.append("		AND  ");
				}
				sql.append("		CEN.endereco LIKE '%" + searchObjectVO.getEnderecoSearch().trim() + "%' ");
			}
			if (searchObjectVO.getBairroSearch() != null && !searchObjectVO.getBairroSearch().isEmpty())
			{
				if (!addAnd)
				{
					addAnd = true;
				}
				else
				{
					sql.append("		AND  ");
				}
				sql.append("		BAI.nome = '" + searchObjectVO.getBairroSearch().trim() + "' ");
			}
			if (searchObjectVO.getCidadeSearch() != null && !searchObjectVO.getCidadeSearch().isEmpty())
			{
				if (!addAnd)
				{
					addAnd = true;
				}
				else
				{
					sql.append("		AND  ");
				}
				sql.append("		CID.nome = '" + searchObjectVO.getCidadeSearch().trim() + "' ");
			}
			if (searchObjectVO.getUfSearch() != null && !searchObjectVO.getUfSearch().isEmpty())
			{
				if (!addAnd)
				{
					addAnd = true;
				}
				else
				{
					sql.append("		AND  ");
				}
				sql.append("		CEN.id_estado = '" + searchObjectVO.getUfSearch().trim() + "' ");
			}
			if (searchObjectVO.getCepSearch() != null && !searchObjectVO.getCepSearch().isEmpty())
			{
				if (!addAnd)
				{
					addAnd = true;
				}
				else
				{
					sql.append("		AND  ");
				}
				sql.append("		CEN.cep like '" + searchObjectVO.getCepSearch().trim() + "' ");
			}
			if (searchObjectVO.getEmailSearch() != null && !searchObjectVO.getEmailSearch().isEmpty())
			{
				if (!addAnd)
				{
					addAnd = true;
				}
				else
				{
					sql.append("		AND  ");
				}
				sql.append("		CEN.emailresp like '" + searchObjectVO.getEmailSearch().trim() + "%' ");
			}

		}
		if (searchObjectVO.getTipoBusca().equalsIgnoreCase("quickSearch") || searchObjectVO.getTipoBusca().equalsIgnoreCase("snepSearch"))
		{
			if (searchObjectVO.getCodigosSigma() != null && !searchObjectVO.getCodigosSigma().isEmpty())
			{
				//se passou pelo quickSearch tem que adicionar um OR
				if (searchObjectVO.getTipoBusca().equalsIgnoreCase("quickSearch"))
				{
					sql.append("		OR  ");
				}

				if (searchObjectVO.getCodigosSigma().size() == 1)
				{
					sql.append("		CEN.cd_cliente = " + searchObjectVO.getCodigosSigma().get(0) + " ");
				}
				else
				{
					sql.append("		CEN.cd_cliente IN (" + SearchObjectVO.getCodigosSigmaInFormtat(searchObjectVO.getCodigosSigma()) + ") ");
				}
			}
		}
		sql.append("	) ");
		sql.append("ORDER BY CEN.RAZAO ");

		Query query = PersistEngine.getEntityManager("SIGMA90").createNativeQuery(sql.toString());
		System.out.println(sql.toString());
		Collection<Object> resultList = query.getResultList();

		Collection<SigmaVO> sigmaVOs = new ArrayList<SigmaVO>();

		Integer id = 0;

		if (resultList != null && !resultList.isEmpty())
		{
			for (Object result : resultList)
			{
				if (result != null)
				{
					Object[] item = (Object[]) result;

					SigmaVO vo = new SigmaVO();
					//dados gerais central
					vo.setCodigoCliente((Integer) item[0]);
					vo.setCodigoEmpresa((Integer) item[1]);
					vo.setCodigoCentral((String) item[2]);
					vo.setParticao((String) item[3]);
					vo.setRota((String) item[4]);
					vo.setRazaoSocial((String) item[5]);
					vo.setCnpjCpf((String) item[6]);
					vo.setLogradouro((String) item[7]);
					vo.setBairro((String) item[8]);
					vo.setCidade((String) item[9]);
					vo.setUf((String) item[10]);
					vo.setCep((String) item[11]);
					//palavra chave
					/**
					 * @author orsegups lucas.avila - alterado Clob Hibernate.
					 * @date 08/07/2015
					 */
					vo.setPergunta(OrsegupsUtils.getStringFromClob((Clob) item[12]));
					vo.setResposta(OrsegupsUtils.getStringFromClob((Clob) item[13]));

					//					if(CallCenterSearchProcessServlet.clienteComOSAbertas((Integer) item[0]))
					//					{
					//						vo.setPossuiOSAbertas(true);
					//					}else
					//						vo.setPossuiOSAbertas(false);
					//	
					//					if(CallCenterSearchProcessServlet.clienteComOSPausada((Integer) item[0]))
					//					{
					//						vo.setPossuiOSPausada(true);
					//					}else
					//						vo.setPossuiOSPausada(false);

					//contatos conta
					vo.setResponsavelCentral((String) item[14]);
					vo.setEmailCentral((String) item[15]);
					vo.setTelefoneCentral1((String) item[16]);
					vo.setTelefoneCentral2((String) item[17]);
					
					String empresa = (String) item[18];
					
					if (empresa == null || empresa.isEmpty()){
					    empresa = "";
					}
					
					vo.setComplementoEmpresa(empresa.toUpperCase());
					String eq = PortalUtil.getBaseURL();
					String btPesquisaEquipamento = "<img style=\"cursor: pointer; cursor: hand;\" src='" + eq + "custom/jsp/orsegups/jms/images/searchbox_button.png' onclick=\"pesquisarResponsavelEquipamento(" + vo.getCodigoCliente().toString() + ")\" /> Pesquisar";
					vo.setEquipamento(btPesquisaEquipamento);
					//vo.setEquipamento((String) setSigmaOSData(vo.getCodigoCliente().toString()));

					//ID
					vo.setId("sigma" + id);
					id++;

					vo = this.getDadosProvidencia(vo);

					if (vo.getCnpjCpf() != null)
					{
						String btPesquisaSenha = "<img style=\"cursor: pointer; cursor: hand;\" src='" + eq + "custom/jsp/orsegups/jms/images/searchbox_button.png' onclick=\"recuperaSenhaSite(" + vo.getCnpjCpf() + ",2)\" /> Pesquisar";
						vo.setSenhaSiteSigma(btPesquisaSenha);
						//vo.setSenhaSiteSigma(recuperaSenhaSite(vo.getCnpjCpf()));
					}

					sigmaVOs.add(vo);
				}
			}
		}

		return sigmaVOs;
	}

	/*
	 * private Collection<Object> mergeResultsFromDBProvidenciaDBCentral(String externalNumber,
	 * Collection<Object> resultsDBCentral)
	 * {
	 * List<Object> results = new ArrayList<Object>();
	 * StringBuilder sql = new StringBuilder();
	 * sql.append("SELECT ");
	 * sql.append("	prov.CD_CLIENTE, ");
	 * sql.append("	CD_PROVIDENCIA, ");
	 * sql.append("	PROV.NOME, ");
	 * sql.append("	PROV.FONE1, ");
	 * sql.append("	PROV.FONE2, ");
	 * sql.append("	PROV.EMAIL, ");
	 * sql.append("	PROV.NU_PRIORIDADE ");
	 * sql.append("FROM ");
	 * sql.append("	dbo.dbPROVIDENCIA AS PROV ");
	 * sql.append("	INNER JOIN dbo.dbCENTRAL as CEN ON CEN.CD_CLIENTE = PROV.CD_CLIENTE ");
	 * sql.append("WHERE  ");
	 * sql.append("( ");
	 * sql.append("		replace(replace(replace(replace(PROV.FONE1, ' ', ''), '-', ''), '(', ''), ')', '') = '"
	 * + externalNumber.trim() + "' ");
	 * sql.append("		OR  ");
	 * sql.append("		replace(replace(replace(replace(PROV.FONE2, ' ', ''), '-', ''), '(', ''), ')', '') = '"
	 * + externalNumber.trim() + "' ");
	 * sql.append(") ");
	 * Query query = PersistEngine.getEntityManager("SIGMA90").createNativeQuery(sql.toString());
	 * Collection<Object> resultList = query.getResultList();
	 * return results;
	 * }
	 */

	@SuppressWarnings("unchecked")
	private SigmaVO getDadosProvidencia(SigmaVO vo)
	{
		if (vo != null && vo.getCodigoCliente() != null)
		{
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("	prov.CD_CLIENTE, ");
			sql.append("	CD_PROVIDENCIA, ");
			sql.append("	PROV.NOME, ");
			sql.append("	PROV.FONE1, ");
			sql.append("	PROV.FONE2, ");
			sql.append("	PROV.EMAIL, ");
			sql.append("	PROV.NU_PRIORIDADE ");
			sql.append("FROM ");
			sql.append("	dbo.dbPROVIDENCIA AS PROV ");
			sql.append("	INNER JOIN dbo.dbCENTRAL as CEN ON CEN.CD_CLIENTE = PROV.CD_CLIENTE ");
			sql.append("WHERE  ");
			sql.append("	CEN.CD_CLIENTE = " + vo.getCodigoCliente() + " ");
			sql.append("ORDER BY PROV.NU_PRIORIDADE ");

			Query query = PersistEngine.getEntityManager("SIGMA90").createNativeQuery(sql.toString());
			Collection<Object> resultList = query.getResultList();

			if (resultList != null && !resultList.isEmpty())
			{
				for (Object result : resultList)
				{
					if (result != null)
					{
						Object[] item = (Object[]) result;

						Integer codigoCliente = (Integer) item[0];
						Integer codigoProvidencia = (Integer) item[1];
						String nome = (String) item[2];
						String telefone1 = (String) item[3];
						String telefone2 = (String) item[4];
						String email = (String) item[5];
						Integer prioridade = ((Short) item[6] != null) ? ((Short) item[6]).intValue() : null;

						ProvidenciaVO providenciaVO = new ProvidenciaVO();
						providenciaVO.setCodigoCliente(codigoCliente);
						providenciaVO.setCodigoProvidencia(codigoProvidencia);
						providenciaVO.setNome(nome);
						providenciaVO.setTelefone1(telefone1);
						providenciaVO.setTelefone2(telefone2);
						providenciaVO.setEmail(email);
						providenciaVO.setPrioridade(prioridade);

						vo.addProvidencia(providenciaVO);
					}
				}
			}
		}

		return vo;
	}

	private String setSigmaOSData(String equip)
	{
		if (equip != null && !equip.equals(""))
		{
			Long cdCliente = Long.valueOf(equip);

			List<NeoObject> contasSigma = PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMACENTRAL"), new QLEqualsFilter("cd_cliente", cdCliente));
			if (contasSigma != null && !contasSigma.isEmpty())
			{
				try
				{

					Connection connSapiens = null;
					PreparedStatement st3 = null;
					ResultSet rsEquip = null;
					connSapiens = PersistEngine.getConnection("SAPIENS");
					StringBuffer sqlEquip = new StringBuffer();
					sqlEquip.append(" SELECT cvs.USU_CodSer ");
					sqlEquip.append(" FROM USU_T160SIG sig ");
					sqlEquip.append(" INNER JOIN USU_T160CVS cvs ON cvs.usu_codemp = sig.usu_codemp AND cvs.usu_numctr = sig.usu_numctr AND cvs.usu_numpos = sig.usu_numpos ");
					sqlEquip.append(" WHERE sig.usu_codcli = " + cdCliente + " ");
					sqlEquip.append(" AND cvs.usu_codser IN ('9002035','9002011', '9002004', '9002005','9002014') ");
					sqlEquip.append(" ORDER BY cvs.usu_sitcvs ");
					st3 = connSapiens.prepareStatement(sqlEquip.toString());
					rsEquip = st3.executeQuery();

					////107655 - SOO - COM CAD - SUELLEN ROSENBROCK
					//108294 - Bruno Brasil
					Integer colEquip = 108294;
					String eq = PortalUtil.getBaseURL();
					String flag_equip = "<img src='" + eq + "imagens/icones/sphere_red_16x16-trans.png'/> Cliente";
					
					
					if (rsEquip.next())
					{
						//107655 - SOO - CM - ANA PAULA
						colEquip = 89428;

						// Equipamento Orsegups
						flag_equip = "<img src='" + eq + "imagens/icones/sphere_yellow_16x16-trans.png'/> Orsegups";
					}
					OrsegupsUtils.closeConnection(connSapiens, st3, rsEquip);
					return flag_equip;
				}
				catch (Exception e)
				{
					e.printStackTrace();
					System.out.println("Erro CallCenterStartProcessServlet: " + e.getMessage());
					return "";
				}
			}
			else
			{
				return "";
			}
		}
		else
		{
			return "";
		}
	}

	private String recuperaSenhaSite(String cgcCpf)
	{
		String senhaSite = "";
		if ((cgcCpf != null) && (!cgcCpf.isEmpty()))
		{
			cgcCpf = cgcCpf.trim();
			cgcCpf = cgcCpf.replaceAll(" ", "");
			cgcCpf = cgcCpf.replaceAll("\\s+", "");
			try
			{
				ExternalEntityInfo infoUsu = (ExternalEntityInfo) EntityRegister.getInstance().getCache().getByString("SAPIENSUSUTUSU");
				NeoObject usuSenha = (NeoObject) PersistEngine.getObject(infoUsu.getEntityClass(), new QLEqualsFilter("usu_cgccpf", Long.valueOf(Long.parseLong(cgcCpf))));
				if (usuSenha != null)
				{
					EntityWrapper usuarioCliente = new EntityWrapper(usuSenha);
					senhaSite = (String) usuarioCliente.getValue("usu_senusu");
					senhaSite = senhaSite + " <a href=\"javascript:void(0)\" title=\"Clique para redefinir senha padrão.\" onclick=\"alterarSenhaParaPadrao(" + cgcCpf + ")\"><img src='" + "http://intranet.orsegups.com.br" + "/fusion/imagens/icones/key_16x16-trans.png'/></a>";
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				return senhaSite;
			}
		}
		return senhaSite;
	}
}
