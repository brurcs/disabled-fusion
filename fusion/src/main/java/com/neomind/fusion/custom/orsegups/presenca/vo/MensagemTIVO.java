package com.neomind.fusion.custom.orsegups.presenca.vo;

import java.util.GregorianCalendar;
import java.util.List;

public class MensagemTIVO
{

	Long neoId;
	Long cpf; 
	Long quantidadeExecucoes; 
	Long quantidadeExecutada;
	GregorianCalendar dataCadastro; 
	String strDataCadastro;
	String usuarioCadastro; 
	List<LeituraMensagemTIVO> leiturasMensagem;
	String tipoMensagem;
	String htmlLeituras;
	GregorianCalendar dataInicio;
	GregorianCalendar dataFim;
	String acao;
	String excluir;
	
	
	public Long getQuantidadeExecutada()
	{
		return quantidadeExecutada;
	}
	public void setQuantidadeExecutada(Long quantidadeExecutada)
	{
		this.quantidadeExecutada = quantidadeExecutada;
	}
	public Long getNeoId()
	{
		return neoId;
	}
	public void setNeoId(Long neoId)
	{
		this.neoId = neoId;
	}
	public Long getCpf()
	{
		return cpf;
	}
	public void setCpf(Long cpf)
	{
		this.cpf = cpf;
	}
	public Long getQuantidadeExecucoes()
	{
		return quantidadeExecucoes;
	}
	public void setQuantidadeExecucoes(Long quantidadeExecucoes)
	{
		this.quantidadeExecucoes = quantidadeExecucoes;
	}
	public GregorianCalendar getDataCadastro()
	{
		return dataCadastro;
	}
	public void setDataCadastro(GregorianCalendar dataCadastro)
	{
		this.dataCadastro = dataCadastro;
	}
	public String getUsuarioCadastro()
	{
		return usuarioCadastro;
	}
	public void setUsuarioCadastro(String usuarioCadastro)
	{
		this.usuarioCadastro = usuarioCadastro;
	}
	public List<LeituraMensagemTIVO> getLeiturasMensagem()
	{
		return leiturasMensagem;
	}
	public void setLeiturasMensagem(List<LeituraMensagemTIVO> leiturasMensagem)
	{
		this.leiturasMensagem = leiturasMensagem;
	}
	public String getTipoMensagem()
	{
		return tipoMensagem;
	}
	public void setTipoMensagem(String tipoMensagem)
	{
		this.tipoMensagem = tipoMensagem;
	}
	
	public String getHtmlLeituras()
	{
		return htmlLeituras;
	}
	public void setHtmlLeituras(String htmlLeituras)
	{
		this.htmlLeituras = htmlLeituras;
	}
	public String getExcluir()
	{
		return excluir;
	}
	public void setExcluir(String excluir)
	{
		this.excluir = excluir;
	}
	public String getStrDataCadastro()
	{
		return strDataCadastro;
	}
	public void setStrDataCadastro(String strDataCadastro)
	{
		this.strDataCadastro = strDataCadastro;
	}
	
	public GregorianCalendar getDataInicio()
	{
		return dataInicio;
	}
	public void setDataInicio(GregorianCalendar dataInicio)
	{
		this.dataInicio = dataInicio;
	}
	public GregorianCalendar getDataFim()
	{
		return dataFim;
	}
	public void setDataFim(GregorianCalendar dataFim)
	{
		this.dataFim = dataFim;
	}
	public String getAcao()
	{
		return acao;
	}
	public void setAcao(String acao)
	{
		this.acao = acao;
	}
	@Override
	public String toString()
	{
		return "MensagemTIVO [neoId=" + neoId + ", cpf=" + cpf + ", quantidadeExecucoes=" + quantidadeExecucoes + ", quantidadeExecutada=" + quantidadeExecutada + ", dataCadastro=" + dataCadastro + ", strDataCadastro=" + strDataCadastro + ", usuarioCadastro=" + usuarioCadastro + ", leiturasMensagem=" + leiturasMensagem + ", tipoMensagem=" + tipoMensagem + ", htmlLeituras=" + htmlLeituras + ", dataInicio=" + dataInicio + ", dataFim=" + dataFim + ", acao=" + acao + ", excluir=" + excluir + "]";
	}
	
	

}
