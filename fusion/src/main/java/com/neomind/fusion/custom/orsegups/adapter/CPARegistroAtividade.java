package com.neomind.fusion.custom.orsegups.adapter;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.ait.controller.ProdutividadeAitController;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class CPARegistroAtividade implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(CPARegistroAtividade.class);

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		String mensagem = "Por favor, contatar o administrador do sistema!";
		
		try
		{
			InstantiableEntityInfo registroAtividade = AdapterUtils.getInstantiableEntityInfo("CPARegistrodeAtividade");
			NeoObject registro = registroAtividade.createNewInstance();
			EntityWrapper wRegistro = new EntityWrapper(registro);

			if (NeoUtils.safeIsNull(origin))
			{
				NeoPaper papel = new NeoPaper();
				papel = OrsegupsUtils.getPaper("sistemaFusionPaper");
				NeoUser usuarioResponsavel = new NeoUser();
				if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
				{
					for (NeoUser user : papel.getUsers())
					{
						usuarioResponsavel = user;
						break;
					}
				}

				wRegistro.findField("responsavel").setValue(usuarioResponsavel);
				wRegistro.findField("dataCPAInicial").setValue(new GregorianCalendar());
				wRegistro.findField("dataCPAAcao").setValue(new GregorianCalendar());
				wRegistro.findField("atividadeCPAAnterior").setValue("Atividade escalada.");
				wRegistro.findField("prazo").setValue(processEntity.findValue("prazo"));
				wRegistro.findField("descricao").setValue("Avançado automaticamente");
			}
			else
			{
				ProdutividadeAitController controller = new ProdutividadeAitController();
				wRegistro.findField("responsavel").setValue(origin.getUser());

				wRegistro.findField("dataCPAInicial").setValue(origin.getStartDate());
				wRegistro.findField("dataCPAAcao").setValue(origin.getFinishDate());

				if (origin.getActivityName().equalsIgnoreCase("Defesa da Produtividade Regional"))
				{
					Boolean defende = (Boolean) processEntity.findValue("defendeProdutividade");
					if (!defende)
					{
						wRegistro.findField("atividadeCPAAnterior").setValue("A regional anulou a produtividade do AIT.");
						processEntity.findField("situacao").setValue(controller.retornaCPASituacao(3L));

					}
					else
					{
						wRegistro.findField("atividadeCPAAnterior").setValue("A regional defendeu a produtividade do AIT e a enviou para confirmação com a superintendência.");
						processEntity.findField("situacao").setValue(controller.retornaCPASituacao(5L));
						
	
					}
					GregorianCalendar prazo = new GregorianCalendar();
					prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 1L);
					prazo.set(Calendar.HOUR_OF_DAY, 23);
					prazo.set(Calendar.MINUTE, 59);
					prazo.set(Calendar.SECOND, 59);
					processEntity.setValue("prazo", prazo);
					wRegistro.findField("prazo").setValue(prazo);
					String txtDescricao = (String) processEntity.findValue("argumento");
					wRegistro.findField("descricao").setValue(txtDescricao);
					
					@SuppressWarnings("unchecked")
					List<NeoObject> arquivos = (List<NeoObject>) processEntity.findValue("anexoGerencia");
																				
					if (arquivos != null ){
					    for (NeoObject arquivo : arquivos){
						
						wRegistro.findField("anexosGerencia").addValue(arquivo);
					    }
					    
					}
					
					
					
					
				}
				else if (origin.getActivityName().equalsIgnoreCase("Defesa da Produtividade Superintendência"))
				{

					Boolean aprovado = (Boolean) processEntity.findValue("aprovaDefesaProdutividade");
					if (!aprovado)
					{
						wRegistro.findField("atividadeCPAAnterior").setValue("A superintendência anulou a produtividade do AIT.");
						processEntity.findField("situacao").setValue(controller.retornaCPASituacao(3L));
					}
					else
					{
						processEntity.findField("situacao").setValue(controller.retornaCPASituacao(4L));
						wRegistro.findField("atividadeCPAAnterior").setValue("A superintendência foi a favor da defesa referente a produtividade do AIT.");
					}
					String txtDescricao = (String) processEntity.findValue("observacao");
					wRegistro.findField("descricao").setValue(txtDescricao);
					GregorianCalendar prazo = new GregorianCalendar();
					prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 1L);
					prazo.set(Calendar.HOUR_OF_DAY, 23);
					prazo.set(Calendar.MINUTE, 59);
					prazo.set(Calendar.SECOND, 59);
					processEntity.setValue("prazo", prazo);
					wRegistro.findField("prazo").setValue(prazo);
					
					
					@SuppressWarnings("unchecked")
					List<NeoObject> arquivosSuperintendencia = (List<NeoObject>) processEntity.findValue("anexoSuperintendencia");
					
					if (arquivosSuperintendencia != null ){
					    for (NeoObject arquivo : arquivosSuperintendencia){
						
						wRegistro.findField("anexosSuperintendencia").addValue(arquivo);
					    }
					    
					}
					
				}
				else if (origin.getActivityName().equalsIgnoreCase("Efetua Lançamento Produtividade Recursos Humanos"))
				{
					Long cdViatura = (Long) processEntity.findValue("cdViatura");

					if (controller.validaVinculoViaturaColaborador(cdViatura))
					{
						Long cpf = controller.retornaCpfColaboradorViatura(cdViatura);
						if (cpf != null)
						{
							String texto = controller.retornaStringMatriculaEmpresaColaborador(cpf);
							if (texto != null && !texto.isEmpty())
							{
								texto = texto.replaceAll(";", " - ");
								processEntity.findField("situacao").setValue(controller.retornaCPASituacao(4L));
								wRegistro.findField("atividadeCPAAnterior").setValue("Encaminhou para finalização lançamento da produtividade efetuado. Informações do colaborador " + texto);
								wRegistro.findField("prazo").setValue(processEntity.findValue("prazo"));
								wRegistro.findField("descricao").setValue("Encaminhado para lançamento.");
							}
						}
						else
						{
							mensagem = "Erro! Não é possível efetuar o lançamento da produtividade para o AIT. Cpf não encontrado.";
							throw new WorkflowException(mensagem);
						}

 						wRegistro.findField("atividadeCPAAnterior").setValue("Encaminhou para finalização lançamento da produtividade efetuado.");

					}
					else
					{
						mensagem = "Erro! Não é possível efetuar o lançamento da produtividade para o AIT, por favor verificar o vínculo entre a viatura e o colaborador";
						throw new WorkflowException(mensagem);
					}
				}
			}
			PersistEngine.persist(registro);
			processEntity.findField("registroAtividadesCPA").addValue(registro);
		}
		catch (Exception e)
		{
			log.error(mensagem);
			e.printStackTrace();
			throw new WorkflowException(mensagem);
		}

	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}

}
