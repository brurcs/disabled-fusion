package com.neomind.fusion.custom.orsegups.rsc;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.security.SecurityEntity;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class RSCEscalarNivelEficacia implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(RSCEscalarNivelCategoriaDiversos.class);

	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);

		String erro = "Por favor, contatar o administrador do sistema!";
		WorkflowException exception = null;
		try
		{
			NeoPaper papelSuperior = new NeoPaper();
			
			if (origin != null)
			{
				OrsegupsUtils paper = new OrsegupsUtils();
				NeoUser responsavelRegistrarRSC = origin.getUser();
			
					if (origin.getFinishByUser() == null)
					{
						if (responsavelRegistrarRSC.getPapers().contains(responsavelRegistrarRSC.getGroup().getResponsible()))
						{
							papelSuperior = responsavelRegistrarRSC.getGroup().getUpperLevel().getResponsible();
							wrapper.setValue("responsavelExecutor", papelSuperior);
						}
						else
						{
							papelSuperior = paper.getPaper(responsavelRegistrarRSC.getGroup().getResponsible().getName());
							wrapper.setValue("responsavelExecutor", papelSuperior);
						}
					}
			}
			else
			{
				NeoPaper superiorExecutor = null;
				NeoPaper gerenteSuperiorExecutor = null;
				
				List<NeoObject> paramEscalaEficaciaPool = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("RSCParametrizacaoEscalonamentoEficaciaPool"));
				if (paramEscalaEficaciaPool != null)
				{
					EntityWrapper wrpEscalaEficaciaPool = new EntityWrapper(paramEscalaEficaciaPool.get(0));
					superiorExecutor = (NeoPaper) wrpEscalaEficaciaPool.findValue("superiorResponsavelExecutor");
					gerenteSuperiorExecutor = (NeoPaper) wrpEscalaEficaciaPool.findValue("gerenteSuperiorResponsavelExecutor");
				}

				papelSuperior = (NeoPaper) wrapper.findValue("responsavelExecutor");
				if(papelSuperior != null && papelSuperior.getCode().equals(superiorExecutor.getCode()))
				{
					wrapper.setValue("responsavelExecutor", gerenteSuperiorExecutor);
				}
				else
				{
					wrapper.setValue("responsavelExecutor", superiorExecutor);
				}
			}
		}
		catch (Exception e)
		{
			log.error(erro);
			e.printStackTrace();
			throw new WorkflowException(erro);
		}
	}

	public void back(EntityWrapper wrapper, Activity activity)
	{
	}
}
