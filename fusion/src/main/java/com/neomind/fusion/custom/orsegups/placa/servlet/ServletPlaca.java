package com.neomind.fusion.custom.orsegups.placa.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.neomind.fusion.custom.orsegups.adapter.OCPAbreEventoSIGMAAPI;

@WebServlet(name = "ServletPlaca", urlPatterns = { "/servlet/com.neomind.fusion.custom.orsegups.placa.servlet.ServletPlaca" })
public class ServletPlaca extends HttpServlet {

    private static final long serialVersionUID = 1L;
    EntityManager entityManager;
    EntityTransaction transaction;

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	doRequest(req, resp);
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	doRequest(req, resp);
    }
    
    private void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      try
      {
        response.setContentType("text/plain");
        response.setCharacterEncoding("ISO-8859-1");

        PrintWriter out = response.getWriter();

        String action = "";

        action = request.getParameter("action");

        if (action.equalsIgnoreCase("gerarEventos"))
        {
          String qtde = request.getParameter("quantidade");
          String cdMunicipio = request.getParameter("cdMunicipio");
          String municipio = request.getParameter("municipio");
          switch (Integer.parseInt(cdMunicipio)) {
          	case 10040:
          	    municipio = "IAI";
              	    break;
          	case 10471:
                    municipio = "PAE";   
                    break;
          	case 10645:
          	    municipio = "PMJ";
          	    break;
          	case 10667:
          	    municipio = "CAS";
          	    break;
                case 10674:
                    municipio = "NHO";  
                    break;
          	case 10754:
          	    municipio = "GNA";
          	    break;
          	case 11259:
          	    municipio = "TRI";
          	    break;
          	case 11272:
          	    municipio = "XLN";
          	    break;
          	case 11511:
          	    municipio = "SRR";
          	    break;
          	case 11897:
          	    municipio = "IBD";
          	    break;
          }
          gerarEvento(qtde, cdMunicipio, municipio, response);
        }

        out.close();
      }
      catch (Exception e)
      {
        e.printStackTrace();
      }
    }
    
    public void gerarEvento(String qtde,String cdMunicipio, String municipio, HttpServletResponse response) throws JSONException, IOException
    { 
	PrintWriter out = response.getWriter();
	String retorno = "Feito";
	try {
	    int i = Integer.parseInt(qtde);
	    i = OCPAbreEventoSIGMAAPI.processaJob(qtde, cdMunicipio, municipio);
	    if (i == 0) {
		retorno = "Esta Regional não possui mais eventos XPL1.";
	    } else {
		retorno = "Foram gerados "+i+" eventos XPL1.";
	    }
	} catch(Exception e) {
	    e.printStackTrace();
	    retorno = "Problemas ao gerar os eventos. Entre em contato com a TI.";
	}
	JSONObject jSonMensagem = new JSONObject();      
	jSonMensagem.put("mensagemSucesso", retorno);
	out.print(jSonMensagem);
	out.flush();
	out.close();
    }
}
