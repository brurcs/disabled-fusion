package com.neomind.fusion.custom.orsegups.medicaoContratos;

import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.DefaultConverter;
import com.neomind.fusion.eform.converter.OriginEnum;

public class MedConvertAlertasPosto extends DefaultConverter
{

	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin)
	{
		String alertas = "";
		boolean sitOK = true;
		long nVagas = 0;
		long nCols = 0;
		alertas = (String) field.getForm().getField("alertasPosto").getValue();
		if(field.getForm().getField("nVagas").getValue() != null){
			nVagas = (long) field.getForm().getField("nVagas").getValue();
		}else{
			
		}
		
		if(field.getForm().getField("nColaboradores").getValue() != null){
			nCols = (long) field.getForm().getField("nColaboradores").getValue();
		}
		
		if (alertas == null)
		{
			alertas = "";
		}
		String icone = "";

		if (alertas.contains("falta"))
		{
			icone = icone + "<img src=\"custom/jsp/orsegups/medicaoDeContratos/imagens/Alerta_Vagas.png\"title=\" Posto possui colaborador(es) com menos de 30 dias.\"/>";
			sitOK = false;
		}

		if (alertas.contains("colaborador demitido"))
		{
			icone = icone + "<img src=\"custom/jsp/orsegups/medicaoDeContratos/imagens/novo_colaborador.png\"title=\" O contrato possui um colaborador demitido no periodo.\"/>";
			sitOK = false;
		}

		if(nVagas != nCols){
			icone = icone + "<img src=\"custom/jsp/orsegups/medicaoDeContratos/imagens/exclamacao2.png\"title=\" O Posto possui divergência entra quantidade de vagas e colaboradores.\"/>";
			sitOK = false;
		}
		
		if (sitOK)
		{
			icone = "<img src=\"custom/jsp/orsegups/medicaoDeContratos/imagens/certo2.png\" title=\" contrato sem alertas\"/>";
		}
		
		
		StringBuilder outBuilder = new StringBuilder();
		outBuilder.append(icone);

		return outBuilder.toString();
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}
}
