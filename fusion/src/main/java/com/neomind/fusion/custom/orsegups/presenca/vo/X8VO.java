package com.neomind.fusion.custom.orsegups.presenca.vo;

public class X8VO {
	
	private String dtRecebido;
	private String dtViaturaDeslocamento;
	private String dtFechamento;
	private String conta;
	private String razao;
	private String observacaoFechamento;
	private String codigo; // representa o código do evento, pois originalmente era para listar somente X8 e agora pode ser qualquer tipo de evento. poderia refatorar, mas não há tempo. 
	
	public String getDtRecebido() {
		return dtRecebido;
	}
	public void setDtRecebido(String dtRecebido) {
		this.dtRecebido = dtRecebido;
	}
	public String getDtViaturaDeslocamento() {
		return dtViaturaDeslocamento;
	}
	public void setDtViaturaDeslocamento(String dtViaturaDeslocamento) {
		this.dtViaturaDeslocamento = dtViaturaDeslocamento;
	}
	public String getDtFechamento() {
		return dtFechamento;
	}
	public void setDtFechamento(String dtFechamento) {
		this.dtFechamento = dtFechamento;
	}
	public String getConta() {
		return conta;
	}
	public void setConta(String conta) {
		this.conta = conta;
	}
	public String getRazao() {
		return razao;
	}
	public void setRazao(String razao) {
		this.razao = razao;
	}
	public String getObservacaoFechamento() {
		return observacaoFechamento;
	}
	public void setObservacaoFechamento(String observacaoFechamento) {
		this.observacaoFechamento = observacaoFechamento;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	
	
	 

}
