package com.neomind.fusion.custom.orsegups.contract.service;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.contract.vo.EnderecoPostoVO;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

@Path("/editarEnderecoPosto")
public class EnderecoPostoService
{
	@POST
	@Path("/saveEndereco")
	@Produces("application/json")
	public Response saveEndereco(EnderecoPostoVO postObj) throws JSONException
	{
		String placas = "";
		
		String ids = postObj.getIds();
		for (String neoId : ids.split(","))
		{
			NeoObject noPosto = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FGCCadastroPostos"), Long.valueOf(neoId));
			EntityWrapper wPosto = new EntityWrapper(noPosto);
			
			String placa = wPosto.findGenericValue("placa");
			if (placas.isEmpty())
				placas = placa;
			else
				placas = placas + ", " + placa;
			
			NeoObject noEndereco = wPosto.findGenericValue("enderecoPosto");
			if (noEndereco == null)
				noEndereco = AdapterUtils.createNewEntityInstance("FGCEndereco");
				
			EntityWrapper wEndereco = new EntityWrapper(noEndereco);			
			wEndereco.setValue("cep", postObj.getCep());
			wEndereco.setValue("endereco", postObj.getEndereco());
			wEndereco.setValue("numero", postObj.getNumero());
			wEndereco.setValue("complemento", postObj.getComplemento());
			wEndereco.setValue("bairro", postObj.getBairro());
			
			NeoObject noEstadoCidade = wEndereco.findGenericValue("estadoCidade");
			if (noEstadoCidade == null)
				noEndereco = AdapterUtils.createNewEntityInstance("FGCEndereco");
			
			if (postObj.getCidade() != null && postObj.getEstado() != null)
			{
				NeoObject noCidade = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSE008CEP"), Long.valueOf(postObj.getCidade()));
				NeoObject noEstado = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("SAPIENSE007UFS"), Long.valueOf(postObj.getEstado()));
				
				EntityWrapper wEstadoCidade = new EntityWrapper(noEstadoCidade);
				wEstadoCidade.setValue("cidade", noCidade);
				wEstadoCidade.setValue("estado", noEstado);
			}
		}
		
		return Response.ok(new JSONObject().put("placas", placas).toString()).build();
	}
}
