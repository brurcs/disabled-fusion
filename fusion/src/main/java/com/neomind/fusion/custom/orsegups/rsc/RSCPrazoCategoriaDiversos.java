package com.neomind.fusion.custom.orsegups.rsc;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.util.NeoUtils;

public class RSCPrazoCategoriaDiversos implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		RSCUtils oRSCUtil = new RSCUtils();
		GregorianCalendar prazoDeadLine = oRSCUtil.retornaPrazoDeadLine("Atender Solicitação", new GregorianCalendar());

		processEntity.findField("prazoAtenderSolicitacao").setValue(prazoDeadLine);	
		
		/*Pego meu último valor da minha lista de Registro de Atividades e altero o valor do campo prazo*/
		List<NeoObject> objLstRegistroAtividades = (List<NeoObject>) processEntity.findValue("RscRelatorioSolicitacaoCliente.registroAtividades");
		if (NeoUtils.safeIsNotNull(objLstRegistroAtividades))
		{
			NeoObject ultima = objLstRegistroAtividades.get(objLstRegistroAtividades.size() - 1);
			EntityWrapper ewUltima = new EntityWrapper(ultima);
			
			ewUltima.findField("prazo").setValue(prazoDeadLine);
		}		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub
	}
}
