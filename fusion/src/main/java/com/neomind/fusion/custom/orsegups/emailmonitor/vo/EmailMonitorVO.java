package com.neomind.fusion.custom.orsegups.emailmonitor.vo;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.model.ProcessModel;

public class EmailMonitorVO extends NeoObject{

	private String name;
	
	private String description;
	
	private ProcessModel process;
	
	private Boolean enabled;
	
	private String customAdapter;
	
	private Boolean senderRequester;
	
	private NeoUser requester;
	
	private Boolean startProcessForEachCC;
	
	private Boolean sendReport;
	
	private Boolean autoAdvance;
	
	private String mailProvider;
	
	private String mailServer;
	
	private Long mailPort;
	
	private String userLogin;
	
	private String userPassword;
	
	private String mailToMonitoring;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ProcessModel getProcess() {
		return process;
	}

	public void setProcess(ProcessModel process) {
		this.process = process;
	}

	public Boolean isEnabled() {
		return enabled != null && enabled.booleanValue();
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String getCustomAdapter() {
		return customAdapter;
	}

	public void setCustomAdapter(String customAdapter) {
		this.customAdapter = customAdapter;
	}

	public Boolean isSenderRequester() {
		return senderRequester != null && senderRequester.booleanValue();
	}

	public void setSenderRequester(Boolean senderRequester) {
		this.senderRequester = senderRequester;
	}

	public NeoUser getRequester() {
		return requester;
	}

	public void setRequester(NeoUser requester) {
		this.requester = requester;
	}

	public Boolean startProcessForEachCC() {
		return startProcessForEachCC != null && startProcessForEachCC.booleanValue();
	}

	public void setStartProcessForEachCC(Boolean startProcessForEachCC) {
		this.startProcessForEachCC = startProcessForEachCC;
	}

	public Boolean getSendReport() {
		return sendReport != null && sendReport.booleanValue();
	}

	public void setSendReport(Boolean sendReport) {
		this.sendReport = sendReport;
	}

	public Boolean getAutoAdvance() {
		return autoAdvance != null && autoAdvance.booleanValue();
	}

	public void setAutoAdvance(Boolean autoAdvance) {
		this.autoAdvance = autoAdvance;
	}
	

	public String getMailProvider() {
		return mailProvider;
	}

	public void setMailProvider(String mailProvider) {
		this.mailProvider = mailProvider;
	}

	public String getMailServer() {
		return mailServer;
	}

	public void setMailServer(String mailServer) {
		this.mailServer = mailServer;
	}

	public String getUserLogin() {
		return userLogin;
	}

	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public Long getMailPort()
	{
		if(mailPort != null)
			return mailPort;
		else
			return 110L;
	}

	public void setMailPort(Long mailPort)
	{
		this.mailPort = mailPort;
	}

	public String getMailToMonitoring()
	{
		return mailToMonitoring;
	}

	public void setMailToMonitoring(String mailToMonitoring)
	{
		this.mailToMonitoring = mailToMonitoring;
	}
}
 