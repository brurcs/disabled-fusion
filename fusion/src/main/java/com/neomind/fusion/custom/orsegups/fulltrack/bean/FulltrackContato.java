package com.neomind.fusion.custom.orsegups.fulltrack.bean;

public class FulltrackContato {
    
    private int ras_ccn_id;
    private String ras_ccn_contato;
    private String ras_ccn_ddi_celular;
    private String ras_ccn_ddi_telefone;
    private String ras_ccn_num_telefone;
    private String ras_ccn_num_celular;
    private String ras_ccn_email;
    private String ras_ccn_email_alerta;
    private String ras_ccn_sms_alerta;
    private String ras_ccn_obs; 
    private String ras_ccn_email_master;
    private String ras_ccn_cli_id;
    
    public int getRas_ccn_id() {
        return ras_ccn_id;
    }
    public void setRas_ccn_id(int ras_ccn_id) {
        this.ras_ccn_id = ras_ccn_id;
    }
    public String getRas_ccn_contato() {
        return ras_ccn_contato;
    }
    public void setRas_ccn_contato(String ras_ccn_contato) {
        this.ras_ccn_contato = ras_ccn_contato;
    }
    public String getRas_ccn_ddi_celular() {
        return ras_ccn_ddi_celular;
    }
    public void setRas_ccn_ddi_celular(String ras_ccn_ddi_celular) {
        this.ras_ccn_ddi_celular = ras_ccn_ddi_celular;
    }
    public String getRas_ccn_ddi_telefone() {
        return ras_ccn_ddi_telefone;
    }
    public void setRas_ccn_ddi_telefone(String ras_ccn_ddi_telefone) {
        this.ras_ccn_ddi_telefone = ras_ccn_ddi_telefone;
    }
    public String getRas_ccn_num_telefone() {
        return ras_ccn_num_telefone;
    }
    public void setRas_ccn_num_telefone(String ras_ccn_num_telefone) {
        this.ras_ccn_num_telefone = ras_ccn_num_telefone;
    }
    public String getRas_ccn_num_celular() {
        return ras_ccn_num_celular;
    }
    public void setRas_ccn_num_celular(String ras_ccn_num_celular) {
        this.ras_ccn_num_celular = ras_ccn_num_celular;
    }
    public String getRas_ccn_email() {
        return ras_ccn_email;
    }
    public void setRas_ccn_email(String ras_ccn_email) {
        this.ras_ccn_email = ras_ccn_email;
    }
    public String getRas_ccn_email_alerta() {
        return ras_ccn_email_alerta;
    }
    public void setRas_ccn_email_alerta(String ras_ccn_email_alerta) {
        this.ras_ccn_email_alerta = ras_ccn_email_alerta;
    }
    public String getRas_ccn_sms_alerta() {
        return ras_ccn_sms_alerta;
    }
    public void setRas_ccn_sms_alerta(String ras_ccn_sms_alerta) {
        this.ras_ccn_sms_alerta = ras_ccn_sms_alerta;
    }
    public String getRas_ccn_obs() {
        return ras_ccn_obs;
    }
    public void setRas_ccn_obs(String ras_ccn_obs) {
        this.ras_ccn_obs = ras_ccn_obs;
    }
    public String getRas_ccn_email_master() {
        return ras_ccn_email_master;
    }
    public void setRas_ccn_email_master(String ras_ccn_email_master) {
        this.ras_ccn_email_master = ras_ccn_email_master;
    }
    public String getRas_ccn_cli_id() {
        return ras_ccn_cli_id;
    }
    public void setRas_ccn_cli_id(String ras_ccn_cli_id) {
        this.ras_ccn_cli_id = ras_ccn_cli_id;
    }
    
    

}
