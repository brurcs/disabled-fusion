package com.neomind.fusion.custom.orsegups.sigma;

public class TaticoContaAnalitico
{
	private String nomeRota;
	private String nomeFantasia;
	private String ctrlCentral;
	private String razaoSocial;
	public String getNomeRota()
	{
		return nomeRota;
	}
	public void setNomeRota(String nomeRota)
	{
		this.nomeRota = nomeRota;
	}
	public String getNomeFantasia()
	{
		return nomeFantasia;
	}
	public void setNomeFantasia(String nomeFantasia)
	{
		this.nomeFantasia = nomeFantasia;
	}
	public String getCtrlCentral()
	{
		return ctrlCentral;
	}
	public void setCtrlCentral(String ctrlCentral)
	{
		this.ctrlCentral = ctrlCentral;
	}
	public String getRazaoSocial()
	{
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial)
	{
		this.razaoSocial = razaoSocial;
	}
	
	
}
