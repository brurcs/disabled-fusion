/**
 * ClientesGravarClientesOutRetornosClientes.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.neomind.fusion.custom.orsegups.contract.clientws.cliente;

public class ClientesGravarClientesOutRetornosClientes  implements java.io.Serializable {
    private java.lang.String cgcCpf;

    private java.lang.Integer codCli;

    private java.lang.String codFor;

    private java.lang.String ideExt;

    private java.lang.String retorno;

    public ClientesGravarClientesOutRetornosClientes() {
    }

    public ClientesGravarClientesOutRetornosClientes(
           java.lang.String cgcCpf,
           java.lang.Integer codCli,
           java.lang.String codFor,
           java.lang.String ideExt,
           java.lang.String retorno) {
           this.cgcCpf = cgcCpf;
           this.codCli = codCli;
           this.codFor = codFor;
           this.ideExt = ideExt;
           this.retorno = retorno;
    }


    /**
     * Gets the cgcCpf value for this ClientesGravarClientesOutRetornosClientes.
     * 
     * @return cgcCpf
     */
    public java.lang.String getCgcCpf() {
        return cgcCpf;
    }


    /**
     * Sets the cgcCpf value for this ClientesGravarClientesOutRetornosClientes.
     * 
     * @param cgcCpf
     */
    public void setCgcCpf(java.lang.String cgcCpf) {
        this.cgcCpf = cgcCpf;
    }


    /**
     * Gets the codCli value for this ClientesGravarClientesOutRetornosClientes.
     * 
     * @return codCli
     */
    public java.lang.Integer getCodCli() {
        return codCli;
    }


    /**
     * Sets the codCli value for this ClientesGravarClientesOutRetornosClientes.
     * 
     * @param codCli
     */
    public void setCodCli(java.lang.Integer codCli) {
        this.codCli = codCli;
    }


    /**
     * Gets the codFor value for this ClientesGravarClientesOutRetornosClientes.
     * 
     * @return codFor
     */
    public java.lang.String getCodFor() {
        return codFor;
    }


    /**
     * Sets the codFor value for this ClientesGravarClientesOutRetornosClientes.
     * 
     * @param codFor
     */
    public void setCodFor(java.lang.String codFor) {
        this.codFor = codFor;
    }


    /**
     * Gets the ideExt value for this ClientesGravarClientesOutRetornosClientes.
     * 
     * @return ideExt
     */
    public java.lang.String getIdeExt() {
        return ideExt;
    }


    /**
     * Sets the ideExt value for this ClientesGravarClientesOutRetornosClientes.
     * 
     * @param ideExt
     */
    public void setIdeExt(java.lang.String ideExt) {
        this.ideExt = ideExt;
    }


    /**
     * Gets the retorno value for this ClientesGravarClientesOutRetornosClientes.
     * 
     * @return retorno
     */
    public java.lang.String getRetorno() {
        return retorno;
    }


    /**
     * Sets the retorno value for this ClientesGravarClientesOutRetornosClientes.
     * 
     * @param retorno
     */
    public void setRetorno(java.lang.String retorno) {
        this.retorno = retorno;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientesGravarClientesOutRetornosClientes)) return false;
        ClientesGravarClientesOutRetornosClientes other = (ClientesGravarClientesOutRetornosClientes) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cgcCpf==null && other.getCgcCpf()==null) || 
             (this.cgcCpf!=null &&
              this.cgcCpf.equals(other.getCgcCpf()))) &&
            ((this.codCli==null && other.getCodCli()==null) || 
             (this.codCli!=null &&
              this.codCli.equals(other.getCodCli()))) &&
            ((this.codFor==null && other.getCodFor()==null) || 
             (this.codFor!=null &&
              this.codFor.equals(other.getCodFor()))) &&
            ((this.ideExt==null && other.getIdeExt()==null) || 
             (this.ideExt!=null &&
              this.ideExt.equals(other.getIdeExt()))) &&
            ((this.retorno==null && other.getRetorno()==null) || 
             (this.retorno!=null &&
              this.retorno.equals(other.getRetorno())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCgcCpf() != null) {
            _hashCode += getCgcCpf().hashCode();
        }
        if (getCodCli() != null) {
            _hashCode += getCodCli().hashCode();
        }
        if (getCodFor() != null) {
            _hashCode += getCodFor().hashCode();
        }
        if (getIdeExt() != null) {
            _hashCode += getIdeExt().hashCode();
        }
        if (getRetorno() != null) {
            _hashCode += getRetorno().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientesGravarClientesOutRetornosClientes.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesGravarClientesOutRetornosClientes"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cgcCpf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cgcCpf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ideExt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ideExt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
