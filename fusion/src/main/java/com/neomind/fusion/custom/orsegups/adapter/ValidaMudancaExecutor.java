package com.neomind.fusion.custom.orsegups.adapter;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class ValidaMudancaExecutor implements AdapterInterface
{
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		List<NeoObject> historicos = new ArrayList<NeoObject>();
		historicos = (List<NeoObject>) processEntity.findValue("registroAtividades");
		
		if (NeoUtils.safeIsNotNull(historicos) && historicos.size() > 0)
		{
			NeoObject hist = historicos.get(historicos.size() - 1);
			NeoUser executorAtual = (NeoUser) processEntity.findValue("Executor");
			
			EntityWrapper historicoWrapper = new EntityWrapper(hist);
			NeoUser executorHistorico = (NeoUser) historicoWrapper.findValue("responsavel");
			
			if (NeoUtils.safeIsNotNull(executorAtual) && NeoUtils.safeIsNotNull(executorAtual) && !executorAtual.getCode().equals(executorHistorico.getCode()))
			{
				GregorianCalendar dataAtual = new GregorianCalendar(new GregorianCalendar().get(1), new GregorianCalendar().get(2), new GregorianCalendar().get(5), 23, 59, 59);
				GregorianCalendar prazo = (GregorianCalendar) processEntity.findField("Prazo").getValue();

				if (prazo.before(dataAtual) )
				{
					throw new WorkflowException("Quando alterado o executor, o prazo informado não deve ser menor que a data atual!");
				}
			}
		}
	}
}