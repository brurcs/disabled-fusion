package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class OCPAbreEventoSIGMA implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		String nomeFonteDados = "SIGMA90";

		Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
		

		// valores utilizados no insert
		Long empresa = (Long) processEntity.findValue("empresaConta");
		String conta = (String) processEntity.findValue("contaSIGMA");;
		String particao = (String) processEntity.findValue("particao");;
		String evento = (String) processEntity.findValue("eventoAGerar");;

		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO RECEPCAO (CD_CENTRAL, NM_EVENTO, NU_AUXILIAR, NU_PARTICAO, NU_PORTA, NU_PROTOCOLO, DT_RECEPCAO, TP_RECEPTORA, CD_EMPRESA, NM_RECEPTORA, TP_RECEPCAO) ");
		sql.append("VALUES (?, ?, '000', ?, 19, 2, GETDATE(), 1, ?, 'Geração Automática', 'T' ) ");
		
		try
		{
			PreparedStatement st = connection.prepareStatement(sql.toString());
			st.setString(1, conta);
			st.setString(2, evento);
			st.setString(3, particao);
			st.setLong(4, empresa);

			st.executeUpdate();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			throw new WorkflowException("Não foi possivel gerar evento no SIGMA.");
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

}
