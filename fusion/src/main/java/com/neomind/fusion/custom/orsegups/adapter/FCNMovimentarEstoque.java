package com.neomind.fusion.custom.orsegups.adapter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.sun.star.bridge.oleautomation.Decimal;

public class FCNMovimentarEstoque implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(FCNMovimentarEstoque.class);

	private String urlstr = "";
	private String NOMUSU = "";
	private String SENUSU = "";
	private String errors = " ";
	private String urls = null;
	private String CodPro = "";
	private String CodDep = "";

	String codigoProduto = "";
	Boolean situacaoOk = false;
	StringBuffer inputLine = null;

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{
			boolean userAutenticated = true;

			// dados eform de conexao SID
			NeoObject noConexao = ((List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("conexaoSID"))).get(0);
			EntityWrapper ewConexao = new EntityWrapper(noConexao);
			this.NOMUSU = "&NOMUSU=labsid";
			this.SENUSU = "&SENUSU=labsid";
			this.urlstr = ewConexao.findField("host").getValue().toString().trim();

			String SIS = "SIS=CO";
			String LOGIN = "&LOGIN=SID";
			String ACAO = "&ACAO=EXESENHA";
			String PROXACAO = "&PROXACAO=SID.Srv.AltEmpFil";
			String CodEmp = "&CodEmp=1";
			String CodFil = "&CodFil=1";

			/* Inicia - Efetua o login do usuário */
			String urlSID = urlstr + SIS + LOGIN + ACAO + NOMUSU + SENUSU;
			log.warn("Url a ser executada para autenticar usuario: " + urlSID);

			inputLine = this.callSapiensSID(urlSID);
			if (inputLine != null && (inputLine.toString().contains("Não foi possível conectar-se ao Middleware")))
			{
				userAutenticated = false;
				errors = "ERRO: Não foi possível conectar-se ao Middleware! Por favor, contate o Departamento de TI!";
				log.error(errors);
				throw new WorkflowException(errors);
			}

			if (inputLine != null && (inputLine.toString().contains("ERRO: Usuário e/ou senha inválido") || inputLine.toString().contains("<html>Time-out error...</html>")))
			{
				userAutenticated = false;
				errors = "ERRO: Usuário e/ou senha inválido. Por favor, contate o Departamento de TI!";
				log.error(errors);
				throw new WorkflowException(errors);
			}
			log.info("Usuário autenticado!");
			/* Final - Efetua o login do usuário */

			/* Inicia - Definir Empresa e Filial */
			urlSID = urlSID + PROXACAO + CodEmp + CodFil;
			log.warn("Url a ser executada para definir empresa e filial: " + urlSID);

			inputLine = this.callSapiensSID(urlSID);
			if (inputLine != null && (inputLine.toString().contains("Erro 1: O servidor está temporariamente fora de operação")))
			{
				userAutenticated = false;
				errors = "ERRO: O servidor está temporariamente fora de operação. Por favor, contate o Departamento de TI!";
				log.error(errors);
				throw new WorkflowException(errors);
			}

			if (inputLine != null && (!inputLine.toString().contains("OK")))
			{
				userAutenticated = false;
				errors = "ERRO: Não foi possível definir empresa e filial:" + inputLine.toString() + ". Por favor, contate o Departamento de TI! ";
				log.error(errors);
				throw new WorkflowException(errors);
			}
			log.info("Empresa e filial definidas!");
			/* Final - Definir Empresa e Filial */

			if (userAutenticated)
			{
				List<NeoObject> listaProdutosRetirar = (List<NeoObject>) processEntity.findValue("equipReceb");
				for (NeoObject objDevolucao : listaProdutosRetirar)
				{
					EntityWrapper wrapperDevolucao = new EntityWrapper(objDevolucao);
					String codigoProduto = (String) wrapperDevolucao.findValue("codigoProduto.codpro");

					if (!codigoProduto.equals("000000"))
					{

						if (codigoProduto != null && codigoProduto != "")
						{
							String situacao = validaSituacaoDoProduto(codigoProduto);
							if (situacao == "I")
							{
								errors = "ERRO: O Produto com código: " + codigoProduto + " está inativo no sistema. Por favor altere o código do produto para um válido!";
								log.error(errors);
								throw new WorkflowException(errors);
							}

							PROXACAO = "&PROXACAO=SID.Est.BuscarEstoqueDisp";
							CodPro = "&CodPro=" + codigoProduto;

							String CodDer = "&CodDer=";
							try
							{
								CodDer = "&CodDer=" + URLEncoder.encode(" ", "ISO-8859-1");
							}
							catch (UnsupportedEncodingException e)
							{
								e.printStackTrace();
								errors = "ERRO: Derivação do produto, Código: " + CodPro + " Derivação: " + CodDer + " retorno: " + e.getMessage();
								log.error(errors);
								throw new WorkflowException(errors);
							}

							CodDep = "&CodDep=LAB02";

							/* Inicia - Busca Estoque Disponível */
							urls = urlstr + SIS + LOGIN + ACAO + NOMUSU + SENUSU + PROXACAO + CodPro + CodDer + CodDep;
							log.warn("Url a ser executada para busca de estoque disponível: " + urls);

							inputLine = callSapiensSID(urls);
							if (inputLine != null && inputLine.toString().contains("ERRO"))
							{
								errors = "ERRO: Ao buscar estoque disponível para o produto:" + codigoProduto + "! " + inputLine.toString() + " \n URL efetuar devolução: " + urls;
								log.error(errors);
								throw new WorkflowException(errors);
							}
							log.info("busca de Estoque efetuada com sucesso!");
							/* Final - Busca Estoque Disponível */
						}
					}
				}

				situacaoOk = true;
				if (situacaoOk)
				{

					for (NeoObject objDevolucao : listaProdutosRetirar)
					{
						EntityWrapper wrapperDevolucao = new EntityWrapper(objDevolucao);
						String codigoProduto = (String) wrapperDevolucao.findValue("codigoProduto.codpro");
						String descricaoEquipamento = (String) wrapperDevolucao.findValue("descricaoEquipamento");
						BigDecimal quantidadeEquipamento = (BigDecimal) wrapperDevolucao.findValue("quantidadeEquipamento");

						if (!codigoProduto.equals("000000"))
						{

							if (codigoProduto != null && codigoProduto != "")
							{
								CodPro = "";

								PROXACAO = "&PROXACAO=SID.Est.Movimentar";
								CodPro = "&CodPro=" + codigoProduto;

								String CodDer = "&CodDer=";
								try
								{
									CodDer = "&CodDer=" + URLEncoder.encode(" ", "ISO-8859-1");
								}
								catch (UnsupportedEncodingException e)
								{
									e.printStackTrace();
									errors = "ERRO: Derivação do produto, Código: " + CodPro + " Derivação: " + CodDer + " retorno: " + e.getMessage();
									log.error(errors);
									throw new WorkflowException(errors);
								}

								String CodTns = "&CodTns=90247";
								CodDep = "&CodDep=LAB02";
								String QtdMov = "&QtdMov=" + quantidadeEquipamento.toString();

								/* Inicia - Movimentação do Produto no Estoque */
								urls = urlstr + SIS + LOGIN + ACAO + NOMUSU + SENUSU + PROXACAO + CodPro + CodDer + CodTns + CodDep + QtdMov;
								log.warn("Url a ser executada para efetuar devolução: " + urls);

								inputLine = callSapiensSID(urls);
								if (inputLine != null && inputLine.toString().contains("ERRO"))
								{
									errors = "ERRO: Ao efetuar devolução do item:" + codigoProduto + "! " + inputLine.toString() + " \n URL efetuar devolução: " + urls;
									log.error(errors);
									throw new WorkflowException(errors);
								}
								log.info("Devolução efetuada com sucesso!");
								/* Final - Movimentação do Produto no Estoque */
							}
						}
					}
				}
			}
		}

		catch (Exception e)
		{
			e.printStackTrace();
			log.error(errors);
			throw new WorkflowException(errors);
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}

	private String validaSituacaoDoProduto(String codPro)
	{
		String situacao = "";

		QLGroupFilter filtroProduto = new QLGroupFilter("AND");
		filtroProduto.addFilter(new QLEqualsFilter("codemp", 1L));
		filtroProduto.addFilter(new QLEqualsFilter("codpro", codPro));
		filtroProduto.addFilter(new QLEqualsFilter("sitpro", "A"));
		List<NeoObject> lstProduto = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSFUEPRO"), filtroProduto);
		if (lstProduto != null && lstProduto.size() > 0)
		{
			situacao = "A";
		}
		else
		{
			situacao = "I";
		}

		return situacao;
	}

	private StringBuffer callSapiensSID(String url)
	{
		StringBuffer inputLine = new StringBuffer();

		url = url.replaceAll(" ", "%20");
		try
		{

			URL murl = new URL(url);
			URLConnection murlc = murl.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(murlc.getInputStream()));

			char letter;

			while ((letter = (char) in.read()) != '\uFFFF')
			{
				inputLine.append(letter);
			}

			in.close();

			log.warn("Retorno Sapiens: " + inputLine.toString());
		}
		catch (Exception e)
		{
			inputLine.append("ERRO: Problema de conexão com o Serviço Sapiens SID" + e.getStackTrace());
			e.printStackTrace();
			errors = "Problema de conexão com o Serviço Sapiens SID! Erro: " + inputLine + e.getStackTrace() + e.getMessage() + e.getCause();
			log.error(errors);
			//throw new Exception(errors);

		}
		return inputLine;
	}

}
