package com.neomind.fusion.custom.orsegups.site.vo;

import java.util.ArrayList;
import java.util.Collection;

public class SiteCompetenciaVO
{
	
	private String competencia;
	private SiteEmpresaVO empresa;
	private String intnet;
	private Boolean chkDeacordoRecnf; // aceita receber fatura por email?
	private Boolean chkDeacordoRecdoc; // aceita consultar documentos só pelo site.
	private Collection<SiteNfvVO> nfv = new ArrayList();
	
	
	  
	
	public String getCompetencia()
	{
		return competencia;
	}
	public void setCompetencia(String competencia)
	{
		this.competencia = competencia;
	}
	
	public Collection<SiteNfvVO> getNfv()
	{
		return nfv;
	}
	public void setNfv(Collection<SiteNfvVO> nfv)
	{
		this.nfv = nfv;
	}
	public SiteEmpresaVO getEmpresa()
	{
		return empresa;
	}
	public void setEmpresa(SiteEmpresaVO empresa)
	{
		this.empresa = empresa;
	}
	public String getIntnet()
	{
		return intnet;
	}
	public void setIntnet(String intnet)
	{
		this.intnet = intnet;
	}
	public Boolean getChkDeacordoRecnf()
	{
		return chkDeacordoRecnf;
	}
	public void setChkDeacordoRecnf(Boolean chkDeacordoRecnf)
	{
		this.chkDeacordoRecnf = chkDeacordoRecnf;
	}
	public Boolean getChkDeacordoRecdoc()
	{
		return chkDeacordoRecdoc;
	}
	public void setChkDeacordoRecdoc(Boolean chkDeacordoRecdoc)
	{
		this.chkDeacordoRecdoc = chkDeacordoRecdoc;
	}
	@Override
	public String toString() {
		return "SiteCompetenciaVO [competencia=" + competencia + ", empresa="
				+ empresa + ", intnet=" + intnet + ", chkDeacordoRecnf="
				+ chkDeacordoRecnf + ", chkDeacordoRecdoc=" + chkDeacordoRecdoc
				+ ", nfv=" + nfv + "]";
	}
	
	
	
	
	  
	  
	  
}
