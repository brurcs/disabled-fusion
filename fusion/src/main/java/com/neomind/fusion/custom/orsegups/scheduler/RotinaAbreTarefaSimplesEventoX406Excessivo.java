package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class RotinaAbreTarefaSimplesEventoX406Excessivo implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(RotinaAbreTarefaSimplesEventoX406Excessivo.class);

	@Override
	public void execute(CustomJobContext arg0)
	{

		Connection conn = null;
		StringBuilder sqlEventoExcessivo = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String reg = "";
		log.warn("##### INICIAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Eventos X406 Excessivos - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		try
		{
			//Eventos	Excessivos	Removido Regional Jaraguá do Sul (JGS) Ref tarefa 738353
			sqlEventoExcessivo.append(" SELECT TOP 50 h.CD_CLIENTE, SUBSTRING(r.NM_ROTA, 1, 3) AS sigla_regional, c.ID_CENTRAL, c.PARTICAO, c.FANTASIA, c.RAZAO, COUNT(*) AS QtdDsl, GETDATE()-15 as PrimeiraData,GETDATE() as UltimaData ");
			sqlEventoExcessivo.append(" FROM HISTORICO_DESARME h WITH (NOLOCK) ");
			sqlEventoExcessivo.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
			sqlEventoExcessivo.append(" INNER JOIN ROTA r WITH (NOLOCK) ON  r.CD_ROTA = c.ID_ROTA ");
			sqlEventoExcessivo.append(" WHERE h.DT_RECEBIDO BETWEEN GETDATE()-15 AND GETDATE() ");
			sqlEventoExcessivo.append(" AND h.CD_EVENTO = 'X406' ");
			sqlEventoExcessivo.append(" AND h.CD_USUARIO_FECHAMENTO <> 9999 ");
			sqlEventoExcessivo.append(" AND h.CD_CLIENTE NOT IN (SELECT DISTINCT CD_CLIENTE FROM PROTOCOLO_CENTRAL WHERE CD_EVENTO IN ('E130', 'E131', 'E132', 'E133', 'E134') AND CD_CODE = 'ALN') ");
			sqlEventoExcessivo.append(" AND SUBSTRING(r.NM_ROTA, 1, 3) IN ('IAI','BQE','BNU','JLE','LGS','CUA','GPR','SOO','CCO','RSL','CTA','TRO','NHO','TRI','CAS','GNA','PMJ','SRR','XLN')   ");
			sqlEventoExcessivo.append(" GROUP BY h.CD_CLIENTE, SUBSTRING(r.NM_ROTA, 1, 3), c.ID_CENTRAL, c.PARTICAO, c.FANTASIA, c.RAZAO ");
			sqlEventoExcessivo.append(" HAVING COUNT(*) >= 3 ");
			sqlEventoExcessivo.append(" ORDER BY 7 DESC ");
			conn = PersistEngine.getConnection("SIGMA90");
			pstm = conn.prepareStatement(sqlEventoExcessivo.toString());

			rs = pstm.executeQuery();
			String executor = "";

			while (rs.next())
			{
			    String solicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteEventoX406Excessivo");
				String tituloAux = "Tratar Eventos X406 em Excesso - " + rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "]%";
				String titulo = "Tratar Eventos X406 em Excesso - " + rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "] "+rs.getString("RAZAO");
				reg = rs.getString("sigla_regional");
				GregorianCalendar prazo = new GregorianCalendar();
				prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 5L);
				prazo.set(Calendar.HOUR_OF_DAY, 23);
				prazo.set(Calendar.MINUTE, 59);
				prazo.set(Calendar.SECOND, 59);

				NeoPaper papel = new NeoPaper();

				papel = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "TarefasEmExcessoX406"));
				
				if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
				{
					for (NeoUser user : papel.getUsers())
					{
						executor = user.getCode();
						break;
					}

					String descricao = OrsegupsUtils.getHistoricoTarefaSimples(tituloAux);
					descricao += " <strong>Conta :</strong> " + rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "] " + " <br>";
					descricao = descricao + " <strong>Razão Social :</strong> " + rs.getString("RAZAO") + " <br>";
					descricao = descricao + " <strong>Nome Fantasia :</strong> " + rs.getString("FANTASIA") + " <br>";
					descricao = descricao + " <strong>Período :</strong> " + NeoDateUtils.safeDateFormat(rs.getDate("PrimeiraData"), "dd/MM/yyyy") + "<strong>  à </strong>" + NeoDateUtils.safeDateFormat(rs.getDate("UltimaData"), "dd/MM/yyyy") + "<br>";
					descricao = descricao + " <strong>Eventos :</strong> " + rs.getString("QtdDsl") + "<br>";

					IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
					String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);

					log.warn("##### EXECUTAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Eventos X406 Excessivos - Responsavel : " + executor + " Regional: " + reg + " Tarefa: " + tarefa);
				}
				else
				{
					log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Eventos X406 Excessivos - Regra: Gerente responsável não encontrado - Regional " + reg);
					continue;
				}

			}

		}
		catch (Exception e)
		{
			log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Eventos X406 Excessivos - ERRO AO GERAR TAREFA - Regional: " + reg);
			System.out.println("[" + key + "] ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Eventos X406 Excessivos - ERRO AO GERAR TAREFA - Regional: " + reg);
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		}
		finally
		{

			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Eventos X406 Excessivos ou Ordens de Serviço em Excesso - ERRO DE EXECUÇÃO - Regional: " + reg);
				e.printStackTrace();
			}

			log.warn("##### FINALIZAR  AGENDADOR DE TAREFA: Abrir Tarefa Simples Eventos X406 Excessivos e Ordens de Serviço em Excesso - Regional: " + reg + " Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}
	
}
