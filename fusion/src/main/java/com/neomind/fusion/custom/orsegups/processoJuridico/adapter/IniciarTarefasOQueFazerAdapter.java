package com.neomind.fusion.custom.orsegups.processoJuridico.adapter;

import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.lang.NumberUtils;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.IniciarTarefasOQueFazerAdapter

public class IniciarTarefasOQueFazerAdapter implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{
			List<NeoObject> listaOqueFazer = (List<NeoObject>) processEntity.findValue("listaOqFazer");
			List<NeoObject> listaOqueFazerHistorico = (List<NeoObject>) processEntity.findValue("j002HistoricoOqueFazer");
			if (listaOqueFazer != null && listaOqueFazer.size() > 0)
			{
				for (NeoObject tarefa : listaOqueFazer)
				{
					String solicitante = PortalUtil.getCurrentUser().getCode();
					String executorTar = "";
					String titulo = "";

					String descricao = "";
					GregorianCalendar prazo = null;
					EntityWrapper wTarefa = new EntityWrapper(tarefa);
					NeoUser executor = (NeoUser) wTarefa.findValue("executorTarNov");
					executorTar = executor.getCode();
					descricao = String.valueOf(wTarefa.findValue("descricao"));
					titulo = String.valueOf(wTarefa.findValue("titulo"));
					prazo = (GregorianCalendar) wTarefa.findValue("prazo");
					NeoFile anexo = (NeoFile) wTarefa.findValue("anexo");
					IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
					try
					{
						String neoIdTarefa = iniciarTarefaSimples.abrirTarefaReturnNeoIdCode(solicitante, executorTar, titulo, descricao, "2", "sim", prazo, anexo);
						String[] neoIdECode = neoIdTarefa.split(";"); 
						
    						wTarefa.setValue("neoIdTarefa", Long.parseLong(neoIdECode[0]));
						
						PersistEngine.persist(tarefa);
						InstantiableEntityInfo insRegAti = AdapterUtils.getInstantiableEntityInfo("j002HistoricoOqueFazer");
						NeoObject objRegAti = insRegAti.createNewInstance();
						EntityWrapper wRegAti = new EntityWrapper(objRegAti);
						wRegAti.setValue("executorTar", executor);

						wRegAti.setValue("titulo", titulo);
						wRegAti.setValue("descricao", descricao);
						wRegAti.setValue("prazo", prazo);
						wRegAti.setValue("anexo", anexo);
						wRegAti.setValue("solicitante", solicitante);
						wRegAti.setValue("neoIdTarefa", Long.parseLong(neoIdECode[0]));
						PersistEngine.persist(objRegAti);
						listaOqueFazerHistorico.add(objRegAti);
	
					}
					catch (WorkflowException e)
					{
						GregorianCalendar dataAtual = new GregorianCalendar();
						if (!OrsegupsUtils.isWorkDay(prazo))
						{
							e.printStackTrace();
							throw new WorkflowException("Prazo informado deve ter um dia útil! ");
						}
						else if ((prazo.before(dataAtual)) || (prazo.equals(dataAtual)))
						{
							e.printStackTrace();
							throw new WorkflowException("Prazo informado deve ser de pelo menos 1 dia!");
						}
						else
						{
							e.printStackTrace();
							throw new WorkflowException("O usuário executor da tarefa de Convocar Testemunha esta inativo!");
						}
					}

				}
			}
			processEntity.setValue("j002HistoricoOqueFazer", listaOqueFazerHistorico);
			processEntity.setValue("listaOqFazer", null);
			processEntity.setValue("temFaz", false);

		}
		catch (Exception e)
		{
			System.out.println("Erro na classe IniciarTarefasOQueFazerAdapter do fluxo J002.");
			e.printStackTrace();
			throw new WorkflowException(e.getMessage());
		}

	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}
}
