package com.neomind.fusion.custom.orsegups.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.common.base.Joiner;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoCalendarUtils;
import com.neomind.util.NeoDateUtils;

public class UraPresencaUtils
{
	private static final Log log = LogFactory.getLog(UraPresencaUtils.class);

	public static String getBuscaMensagens(Long codigoCadastro, Long codigoEmpresa, GregorianCalendar dataAtual, String acao)
	{

		Connection connVetorh = null;
		PreparedStatement stColaborador = null;
		ResultSet rsColaborador = null;
		String retorno = "";

		try
		{

			StringBuffer queryColaborador = new StringBuffer();
			queryColaborador.append(" SELECT fun.NumEmp, fun.TipCol, fun.NumCad, hch.NumCra, fun.DatAdm, fun.NomFun, fun.SitAfa, fun.DatAfa, fun.emicar, sit.usu_blopre, ");
			queryColaborador.append("        fun.NumCpf, hes.CodEsc, hes.CodTma, orn.NumLoc, orn.TabOrg, orn.usu_codccu ");
			queryColaborador.append(" FROM R034FUN fun ");
			queryColaborador.append(" LEFT JOIN R038HCH hch WITH (NOLOCK) ON hch.NUMEMP = fun.NumEmp AND fun.TIPCOL = hch.TIPCOL AND fun.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch.HorIni, hch.DatIni) = (SELECT MAX(DATEADD(MINUTE, hch2.HorIni, hch2.DatIni)) FROM R038HCH hch2 WHERE hch2.NUMEMP = hch.NUMEMP AND hch2.TIPCOL = hch.TIPCOL AND hch2.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch2.HorIni, hch2.DatIni) <= ?)");
			queryColaborador.append(" INNER JOIN R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp AND hlo.TipCol = fun.TipCol AND hlo.NumCad = fun.NumCad AND hlo.DatAlt = (SELECT MAX (hlo2.DATALT) FROM R038HLO hlo2 WHERE hlo2.NUMEMP = hlo.NUMEMP AND hlo2.TIPCOL = hlo.TIPCOL AND hlo2.NUMCAD = hlo.NUMCAD AND hlo2.DATALT <= ?) ");
			queryColaborador.append(" INNER JOIN R038HES hes WITH (NOLOCK) ON hes.NumEmp = fun.NumEmp AND hes.TipCol = fun.TipCol AND hes.NumCad = fun.NumCad AND hes.DatAlt = (SELECT MAX (hes2.DATALT) FROM R038HES hes2 WHERE hes2.NUMEMP = hes.NUMEMP AND hes2.TIPCOL = hes.TIPCOL AND hes2.NUMCAD = hes.NUMCAD AND hes2.DATALT <= ?) ");
			queryColaborador.append(" INNER JOIN R010SIT sit ON sit.CodSit = fun.SitAfa ");
			queryColaborador.append(" INNER JOIN R016ORN orn ON orn.NumLoc = hlo.NumLoc and orn.TabOrg = hlo.TabOrg ");
			queryColaborador.append(" WHERE fun.DatAdm <= ? AND (fun.SitAfa <> 7 OR (fun.SitAfa = 7 AND fun.DatAfa > ?)) ");
			queryColaborador.append(" AND fun.TipCol = 1 AND fun.NumCad = ? AND fun.NumEmp = ? ");

			connVetorh = PersistEngine.getConnection("VETORH");
			Timestamp dataAtualTS = new Timestamp(dataAtual.getTimeInMillis());
			stColaborador = connVetorh.prepareStatement(queryColaborador.toString());
			stColaborador.setTimestamp(1, dataAtualTS);
			stColaborador.setTimestamp(2, dataAtualTS);
			stColaborador.setTimestamp(3, dataAtualTS);
			stColaborador.setTimestamp(4, dataAtualTS);
			stColaborador.setTimestamp(5, dataAtualTS);
			stColaborador.setLong(6, codigoCadastro);
			stColaborador.setLong(7, codigoEmpresa);

			rsColaborador = stColaborador.executeQuery();
			Long cpf = 0L;
			if (rsColaborador.next())
			{
				cpf = rsColaborador.getLong("NumCpf");
			}

			OrsegupsUtils.closeConnection(connVetorh, stColaborador, rsColaborador);

			if (cpf != 0L)
			{
				retorno = retornaMensagem(codigoCadastro, codigoEmpresa, cpf, acao);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connVetorh, stColaborador, rsColaborador);
		}

		return retorno;
	}
	
	public static String retornaMensagem(Long codigoCadastro, Long codigoEmpresa, Long cpf, String acao)
	{
		String DATE_FORMAT = "yyyy-MM-dd";
		
		String retorno = "";
		
		String acaoRegistrada = "";
		
		if (acao.equalsIgnoreCase("login")){
			acaoRegistrada = "'Entrada'";
		}else{
			acaoRegistrada = "'Saída'";
		}
		
		try
		{
			if (cpf != 0L)
			{
					
				String data = NeoCalendarUtils.formatDate(new GregorianCalendar().getTime(), DATE_FORMAT);
				
				QLGroupFilter filterAnd = new QLGroupFilter("AND");
				QLRawFilter filterRaw = new QLRawFilter(" cpf="+cpf
										+" AND ((quantidadeExecutada < quantidadeExecucoes AND dataInicio IS NULL AND dataFim IS NULL)"
										+" OR ('"+data+"' BETWEEN dataInicio AND dataFim AND quantidadeExecucoes=0)) "
										+ "AND (acao.acao="+acaoRegistrada+" OR acao.acao='Ambos' )");
				
				filterAnd.addFilter(filterRaw);
				
				List<NeoObject> mensagemList = null;
				mensagemList = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMensagem"), filterAnd);
				
				ArrayList<String> listaMensagens = new ArrayList<String>();

				if (mensagemList != null && !mensagemList.isEmpty())
				{
					
					for (NeoObject neoObject : mensagemList){
						NeoObject object = (NeoObject) neoObject;

						EntityWrapper entityWrapper = new EntityWrapper(object);

						Long neoId = (Long) entityWrapper.findValue("neoId");
						String tipoMensagem = (String) entityWrapper.findValue("tipoMensagem.descricao");
						String mensagem = (String) entityWrapper.findValue("mensagem");
						
						String mensagemFinal = tipoMensagem + "_" + mensagem +"_"+neoId;
						
						listaMensagens.add(mensagemFinal);
						
					}
					
					retorno = Joiner.on(";").join(listaMensagens);
					
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return retorno;
	}
	/**
	 * Faz a gravação da mensagem no E-Form [TI] Mensagem. As informações CPF, Tipo Mensagem, Mensagem e Ação são obrigatórios.
	 * Os campos número de execuções, dataInicio e dataFim são opcionais, lembrando que ao menos um deles deve ser informado.
	 * Exemplo: Você pode informar um número X de execuções e não informar nenhum periodo (dataInicio e dataFim) ou não informar um número de execuções e informar o período (dataInicio e dataFim).
	 * Se nenhuma informação referente a execução for passada, a mensagem não será gravada.
	 * O tipo de mensagem pode ser consultado em [TI] Tipo Mensagem e a ação em [TI] Ação Mensagem.
	 * Atualmente a mensagem consiste em duas datas em milesegundos dividas por 1000 cada e separados por underline.
	 * Exemplo: 1463419829_1463419829.
	 * @param cpf Obrigatório!
	 * @param numExecucoes Opcional, pode receber nulo ou vazio.
	 * @param tipoMensagem Obrigatório!
	 * @param mensagem Obritatório!
	 * @param dataInicio Opcional, pode receber nulo ou vazio.
	 * @param dataFim Opcional, pode receber nulo ou vazio.
	 * @param idAcao Obrigatório!
	 * @return booleano. true se a mensagem foi gravada, caso contrario false.
	 * @author mateus.batista
	 * @since 16/05/2016
	 */
	public static boolean gravarMensagem(Long cpf, int numExecucoes, int tipoMensagem, String mensagem, GregorianCalendar dataInicio, GregorianCalendar dataFim, int idAcao){
		
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		
		log.info("UraPresencaUtils (gravarMensagem) -Iniciado Gravação de mensagem - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		
		InstantiableEntityInfo eMensagem = AdapterUtils.getInstantiableEntityInfo("TIMensagem");
		NeoObject objMensagem = eMensagem.createNewInstance();
		EntityWrapper wrapperMensagem = new EntityWrapper(objMensagem);
		
		if (cpf == 0 || mensagem == null || mensagem.isEmpty()){
			return false;
		}
		
		if (numExecucoes == 0 && dataInicio == null & dataFim == null){
			return false;
		}
		QLGroupFilter filter = new QLGroupFilter("AND");
		filter.addFilter(new QLEqualsFilter("idTipo", tipoMensagem));
		
		List<NeoObject> listaTipoMensagem = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TITipoMensagem"), filter);
		
		NeoObject valorMensagem = null;
		
		if (listaTipoMensagem == null || listaTipoMensagem.isEmpty()){
			return false;
		}else{
				 valorMensagem = (NeoObject) listaTipoMensagem.get(0);
		}
		
		filter = new QLGroupFilter("AND");
		filter.addFilter(new QLEqualsFilter("idAcao", idAcao));	
		
		List<NeoObject> listaAcao = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIAcaoMensagem"), filter);
		NeoObject valorAcao = null;
		
		if (listaAcao == null || listaAcao.isEmpty()){
			return false;
		}else{
			valorAcao = (NeoObject) listaAcao.get(0);
		}
		
		NeoUser usuario = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "sistemaFusion"));
		
		try{
			wrapperMensagem.findField("cpf").setValue(Long.valueOf(cpf));
			wrapperMensagem.findField("quantidadeExecucoes").setValue(Long.valueOf(numExecucoes));
			wrapperMensagem.findField("dataCadastro").setValue(new GregorianCalendar());
			wrapperMensagem.findField("usuarioCadastro").setValue(usuario);
			wrapperMensagem.findField("tipoMensagem").setValue(valorMensagem);
			wrapperMensagem.findField("mensagem").setValue(mensagem);
			wrapperMensagem.findField("isIntegrarNexti").setValue(true);
			if (dataInicio != null){
				wrapperMensagem.findField("dataInicio").setValue(dataInicio);
			}
			if (dataFim != null){
				wrapperMensagem.findField("dataFim").setValue(dataFim);
			}
			wrapperMensagem.findField("acao").setValue(valorAcao);
			
			PersistEngine.persist(objMensagem);
			
			return true;
		}catch (Exception e){
			e.printStackTrace();
			log.error("UraPresencaUtils (gravarMensagem) - Erro ao persistir mensagem - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		    throw new JobException("UraPresencaUtils (gravarMensagem) - Erro ao persistir mensagem, procurar no log por [" + key + "] "+ NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}
}
