package com.neomind.fusion.custom.orsegups.medicaoContratos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.presenca.vo.EscalaPostoVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class GeraInformacoesParaMedicaoCtrJob implements CustomJobAdapter

{

	@Override
	public void execute(CustomJobContext ctx)
	{
		System.out.println("[MED] - iniciando job Gera Informacoes Para Medicao de contratos");
		processaJob(ctx);
	}

	public static void processaJob(CustomJobContext ctx)
	{

		Connection connVetorh = null;
		ResultSet rsMedicao = null;
		PreparedStatement stMedicao = null;
		try
		{
			StringBuffer queryMedicao = new StringBuffer();

			GregorianCalendar datRef = new GregorianCalendar();

			datRef.set(GregorianCalendar.DATE, 1);
			datRef.add(GregorianCalendar.DATE, -1);
			datRef.set(GregorianCalendar.HOUR_OF_DAY, 23);
			datRef.set(GregorianCalendar.MINUTE, 59);
			datRef.set(GregorianCalendar.SECOND, 59);
			NeoDateUtils.safeDateFormat(datRef, "dd/MM/yyyy");
			String periodo = NeoDateUtils.safeDateFormat(datRef, "MM/yyyy");

			queryMedicao.append(" SELECT cvs.usu_numctr,cvs.usu_numpos,cvs.usu_codemp,cvs.usu_codfil,cvs.usu_nomloc,car.titred,fun.numcad,fun.nomfun,fun.numemp,fun.tipcol,fun.datadm ,isnull(sit.dessit, 'trabalhando') AS DesSit," + "orn.usu_lotorn,orn.usu_apecli,reg.usu_codreg,reg.USU_NomReg,emp.nomemp," + "orn.numloc,orn.usu_numemp,isnull(ctr.usu_numofi,'') as usu_numofi, cvs.usu_codccu,cvs.usu_qtdcon");
			queryMedicao.append(" ,hie.CodLoc , ESC.NomEsc FROM R034FUN fun   ");
			queryMedicao.append(" INNER JOIN R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp AND hlo.TipCol = fun.TipCol AND hlo.NumCad = fun.NumCad AND hlo.DatAlt = ");
			queryMedicao.append(" (SELECT MAX (DATALT) FROM R038HLO TABELA001 WHERE TABELA001.NUMEMP = hlo.NUMEMP AND TABELA001.TIPCOL = hlo.TIPCOL AND TABELA001.NUMCAD = hlo.NUMCAD AND TABELA001.DATALT <= ?)  ");
			queryMedicao.append(" INNER JOIN R038HCA HCA (NOLOCK) ON HCA.CodCar = FUN.codcar AND HCA.NumCad = FUN.NUMCAD AND HCA.TipCol = FUN.TIPCOL AND HCA.NUMEMP = FUN.numemp AND HCA.DatAlt = ");
			queryMedicao.append(" (SELECT MAX(DATALT) FROM R038HCA HCA2 WHERE HCA2.NUMCAD = HCA.NUMCAD AND ");
			queryMedicao.append("										    HCA2.NUMEMP = HCA.NUMEMP AND ");
			queryMedicao.append("											HCA2.TIPCOL = HCA.TIPCOL AND ");
			queryMedicao.append("											HCA2.DatAlt <= ?)");
			queryMedicao.append(" INNER JOIN R016HIE hie WITH (NOLOCK) ON hie.taborg = hlo.taborg AND hlo.numloc = hie.numloc AND hie.datini = (SELECT MAX(h2.datini) FROM dbo.R016HIE h2 WHERE hie.taborg = hie.taborg AND h2.numloc = hie.NumLoc) ");
			queryMedicao.append(" INNER JOIN R024CAR CAR (NOLOCK) ON CAR.CODCAR = HCA.CodCar and car.estcar = 1");
			queryMedicao.append(" INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.NumLoc = hlo.NumLoc and orn.TabOrg = 203  ");
			queryMedicao.append(" INNER JOIN R038HES HES ON HES.NumCad = FUN.numcad AND HES.NumEmp = FUN.numemp AND HES.TipCol = FUN.tipcol AND HES.DatAlt =   ");
			queryMedicao.append(" (SELECT MAX (DATALT) FROM R038HES TABELA001 WHERE TABELA001.NUMEMP = HES.NUMEMP AND TABELA001.TIPCOL = HES.TIPCOL AND TABELA001.NUMCAD = HES.NUMCAD AND TABELA001.DATALT <= ?)  ");
			queryMedicao.append(" INNER JOIN R006ESC ESC ON ESC.CodEsc = HES.CodEsc   ");
			queryMedicao.append(" INNER JOIN USU_T038CVS  cvs WITH (NOLOCK) ON cvs.USU_NumLoc = orn.NumLoc AND cvs.USU_TabOrg = orn.TabOrg AND cvs.USU_SeqAlt = (Select MAX(c2.usu_seqalt) FROM USU_T038CVS c2 Where ");
			queryMedicao.append(" c2.usu_taborg = cvs.usu_taborg And ");
			queryMedicao.append(" c2.usu_numloc = cvs.usu_numloc And ");
			queryMedicao.append(" c2.usu_datalt <= ?)  ");
			queryMedicao.append(" INNER JOIN USU_TFisRes fis ON fis.USU_FisRes = fun.usu_fisres  ");
			queryMedicao.append(" LEFT JOIN R038AFA afa WITH (NOLOCK) ON (afa.NumEmp = fun.NumEmp AND afa.TipCol = fun.TipCol AND afa.NumCad = fun.NumCad AND DATEADD(minute,afa.HORAFA,afa.DATAFA) = ");
			queryMedicao.append(" (SELECT MAX (DATEADD(minute,HORAFA,DATAFA)) FROM R038AFA TABELA001 ");
			queryMedicao.append(" WHERE TABELA001.NUMEMP = afa.NUMEMP AND ");
			queryMedicao.append(" TABELA001.TIPCOL = afa.TIPCOL AND ");
			queryMedicao.append(" TABELA001.NUMCAD = afa.NUMCAD AND ");
			queryMedicao.append(" TABELA001.DATAFA <= ? AND ");
			queryMedicao.append(" (TABELA001.DatTer = '1900-12-31 00:00:00' OR TABELA001.DatTer >= cast(floor(cast(? as float)) as datetime))))  LEFT JOIN R038AFA afaDem WITH (NOLOCK) ON ");
			queryMedicao.append(" afaDem.NumEmp = fun.NumEmp AND afaDem.TipCol = fun.TipCol AND afaDem.NumCad = fun.NumCad AND afaDem.sitAfa = 7 AND DATEADD(minute,afaDem.HORAFA,afaDem.DATAFA) = ");
			queryMedicao.append(" (SELECT MAX (DATEADD(minute,HORAFA,DATAFA)) FROM R038AFA TABELA001 WHERE TABELA001.NUMEMP = afaDem.NUMEMP AND TABELA001.TIPCOL = afaDem.TIPCOL AND TABELA001.NUMCAD = afaDem.NUMCAD)  ");
			queryMedicao.append(" LEFT JOIN R010SIT SIT ON SIT.CODSIT = AFA.SitAfa ");
			queryMedicao.append(" INNER JOIN USU_T200REG reg on reg.USU_CodReg = orn.usu_codreg");
			queryMedicao.append(" INNER JOIN r030emp emp on emp.numemp = orn.usu_numemp");
			queryMedicao.append(" INNER JOIN [FSOODB04\\SQL02].SAPIENS.DBO.E085CLI CLI ON CLI.CODCLI = ORN.usu_codclisap");
			queryMedicao.append(" INNER JOIN [FSOODB04\\SQL02].SAPIENS.DBO.USU_T160CTR CTR ON CTR.usu_numctr = cvs.usu_numctr");
			queryMedicao.append(" WHERE fun.DatAdm <= ?  AND (afa.SitAfa IS NULL OR afa.SitAfa <> 7 OR (afa.SitAfa = 7 AND afa.DatAfa > cast(floor(cast(? as float)) as datetime)))  ");
			queryMedicao.append(" AND fun.TipCol = 1  AND orn.usu_numemp IN (2,6,7,8) and orn.usu_codreg in(1,9) and cvs.usu_sitcvs = 'S' and CLI.TipEmc = 2 ");
			queryMedicao.append(" AND NOT EXISTS( ");
			queryMedicao.append(" SELECT 1 FROM [CACUPE\\SQL02].fusion_producao.DBO.D_MEDMedicaoDeContratos CTR ");
			queryMedicao.append(" INNER JOIN [CACUPE\\SQL02].fusion_producao.DBO.D_MEDMedicaoDeContratos_listaPostos LP ON LP.D_MEDMedicaoDeContratos_neoId = CTR.neoId ");
			queryMedicao.append(" INNER JOIN [CACUPE\\SQL02].fusion_producao.DBO.D_medPosto P ON P.neoId = LP.listaPostos_neoId ");
			queryMedicao.append(" INNER JOIN [CACUPE\\SQL02].fusion_producao.DBO.D_medPosto_listaColaboradores LC ON LC.D_medPosto_neoId = P.neoId ");
			queryMedicao.append(" INNER JOIN [CACUPE\\SQL02].fusion_producao.DBO.D_MEDColaboradores C ON C.neoId = LC.listaColaboradores_neoId ");
			queryMedicao.append(" WHERE periodo = ? AND C.numCadColaborador = FUN.NUMCAD AND CTR.codEmpresaContratada = FUN.NUMEMP AND FUN.TIPCOL = 1 AND orn.usu_codclisap NOT IN (2527,2532)  ");
			queryMedicao.append(" ) ");
			queryMedicao.append(" GROUP BY cvs.usu_numctr,cvs.usu_numpos,cvs.usu_codemp,cvs.usu_codfil,cvs.usu_nomloc,car.titred,fun.numcad,fun.nomfun,fun.numemp,");
			queryMedicao.append(" fun.tipcol,fun.datadm,DesSit,orn.usu_lotorn,orn.usu_apecli,reg.usu_codreg,reg.USU_NomReg,emp.nomemp,orn.numloc,orn.usu_numemp,ctr.usu_numofi,");
			queryMedicao.append(" cvs.usu_codccu,cvs.usu_qtdcon ,hie.CodLoc , ESC.NomEsc");
			queryMedicao.append(" ORDER BY hie.CodLoc,fun.nomfun,cvs.usu_numctr,cvs.usu_numpos,cvs.usu_codemp,cvs.usu_codfil,cvs.usu_nomloc,car.titred,fun.numcad,fun.numemp, ");
			queryMedicao.append(" fun.tipcol,fun.datadm,DesSit,orn.usu_lotorn,orn.usu_apecli,reg.usu_codreg,reg.USU_NomReg,emp.nomemp,orn.numloc,orn.usu_numemp,ctr.usu_numofi, ");
			queryMedicao.append(" cvs.usu_codccu,cvs.usu_qtdcon , ESC.NomEsc");

			connVetorh = PersistEngine.getConnection("VETORH");
			stMedicao = connVetorh.prepareStatement(queryMedicao.toString());
			stMedicao.setTimestamp(1, new Timestamp(datRef.getTimeInMillis()));
			stMedicao.setTimestamp(2, new Timestamp(datRef.getTimeInMillis()));
			stMedicao.setTimestamp(3, new Timestamp(datRef.getTimeInMillis()));
			stMedicao.setTimestamp(4, new Timestamp(datRef.getTimeInMillis()));
			stMedicao.setTimestamp(5, new Timestamp(datRef.getTimeInMillis()));
			stMedicao.setTimestamp(6, new Timestamp(datRef.getTimeInMillis()));
			stMedicao.setTimestamp(7, new Timestamp(datRef.getTimeInMillis()));
			stMedicao.setTimestamp(8, new Timestamp(datRef.getTimeInMillis()));
			stMedicao.setString(9, periodo);

			rsMedicao = stMedicao.executeQuery();

			List<NeoObject> colaboradores = new ArrayList<NeoObject>();
			List<NeoObject> postos = new ArrayList<NeoObject>();
			long nNumCtrAnt = 0;
			long nNumCtrNov = 0;
			EntityWrapper wMedicao = null;
			NeoObject medicao = null;
			String destacar = "";
			String nomOfi = "";
			long nNumPosNov = 0L;
			long nNumPosAnt = 0L;
			EntityWrapper wPostos = null;
			NeoObject posto = null;
			String alertaCtr = "ok";
			int inss = 0;
			Long cargaHorPos;
			while (rsMedicao.next())
			{
				inss = 0;
				nNumCtrNov = rsMedicao.getLong("usu_numctr");
				nNumPosNov = rsMedicao.getLong("usu_numpos");
				int desconto = OrsegupsMedicaoUtilsMensal.retornaInicioDesconto(rsMedicao.getString("datAdm"), datRef);
				inss = OrsegupsMedicaoUtilsMensal.verificaRetornoDoInss(rsMedicao.getLong("numcad"), rsMedicao.getLong("numcad"), 1L, datRef);
				desconto = desconto - inss;
				List<NeoObject> listConsideracoes = OrsegupsMedicaoUtilsMensal.validaAfastamento(rsMedicao.getLong("numemp"), rsMedicao.getLong("numcad"), 1L, datRef, rsMedicao.getLong("NumLoc"));
				NeoObject colaboradorMedicao = AdapterUtils.createNewEntityInstance("MEDColaboradores");
				EntityWrapper wColaborador = new EntityWrapper(colaboradorMedicao);
				if (listConsideracoes != null && listConsideracoes.size() >= 1)
				{
					wColaborador.findField("listaConsideracoes").setValue(listConsideracoes);
					for (int i = 0; i < listConsideracoes.size(); i++)
					{
						NeoObject wFalta = listConsideracoes.get(i);
						EntityWrapper ew = new EntityWrapper(wFalta);
						boolean isFalta = (boolean) ew.findField("isFalta").getValue();
						if (isFalta)
						{
							desconto--;
						}
					}

				}

				if (nNumCtrAnt != nNumCtrNov)
				{

					//Validar se deve ser aplicado o persist. Caso nNumCtrAnt for != 0 é um novo contrato, então deve ser persistido antes de alterar.
					if (nNumCtrAnt != 0)
					{
						wMedicao.setValue("listaPostos", postos);
						wMedicao.setValue("numOfi", nomOfi);
						wMedicao.setValue("alertas", "");
						Long numCtr = (Long) wMedicao.findField("contrato").getValue();
						GregorianCalendar datini = (GregorianCalendar) datRef.clone();
						datini.set(GregorianCalendar.DAY_OF_MONTH, 1);

						wMedicao.setValue("alertas", alertaCtr + OrsegupsMedicaoUtilsMensal.InsereDemitidosNoPeriodo(numCtr, datini, datRef));
						PersistEngine.persist(medicao);
						//BKP
						OrsegupsMedicaoUtilsMensal.registaBKPMedicaoDeContratos(medicao, "MEDMedicaoDeContratos");
						//BKP

						postos = new ArrayList<NeoObject>();
						alertaCtr = "ok";

					}
					medicao = null;
					medicao = AdapterUtils.createNewEntityInstance("MEDMedicaoDeContratos");
					wMedicao = new EntityWrapper(medicao);
					wMedicao.setValue("cliente", destacar + " " + rsMedicao.getString("usu_apecli"));
					wMedicao.setValue("contrato", rsMedicao.getLong("usu_numctr"));
					wMedicao.setValue("codEmpresaContratada", rsMedicao.getLong("numemp"));
					wMedicao.setValue("empresaContratada", destacar + rsMedicao.getString("nomemp"));
					SimpleDateFormat formatter = new SimpleDateFormat("MM/yyyy");
					wMedicao.setValue("responsavel", "Sra. Inês");
					wMedicao.setValue("periodo", formatter.format(datRef.getTime()));
					wMedicao.setValue("codRegional", rsMedicao.getLong("usu_codreg"));
					wMedicao.setValue("regional", rsMedicao.getString("USU_NomReg"));
					nNumCtrNov = rsMedicao.getLong("usu_numctr");
					nNumCtrAnt = nNumCtrNov;
					nomOfi = rsMedicao.getString("usu_numofi");
				}

				if (nNumPosAnt != nNumPosNov)
				{
					//Validar se deve ser aplicado o persist. Caso nNumPosAnt for != 0 é um novo posto, então deve ser persistido antes de alterar.
					if (nNumPosAnt != 0L)
					{
						long nColaboradores = colaboradores.size();
						wPostos.setValue("nColaboradores", nColaboradores);
						wPostos.setValue("listaColaboradores", colaboradores);
						String alertaPosto = "ok";
						for (NeoObject neoObject2 : colaboradores)
						{
							EntityWrapper wColab = new EntityWrapper(neoObject2);
							String falta = (String) wColab.findField("descMed").getValue();

							if (!falta.contains("30"))
							{
								alertaPosto = "falta";
								alertaCtr = "falta";
								break;
							}
						}
						long qtdCol = 0L;
						long qtdVag = 0L;
						qtdCol = (long) wPostos.findField("nColaboradores").getValue();
						qtdVag = (long) wPostos.findField("nVagas").getValue();
						if (qtdCol != qtdVag)
						{
							if (!alertaPosto.contains("ok"))
							{
								alertaPosto += ";vagasDivergentes";
							}
							else
							{
								alertaPosto = ";vagasDivergentes";
							}
						}
						if (inss > 0)
						{
							wPostos.setValue("alertasPosto", alertaPosto += ";retornoINSS");
						}
						wPostos.setValue("alertasPosto", alertaPosto);
						EscalaPostoVO escalas = QLPresencaUtils.getEscalaPosto(rsMedicao.getLong("numloc"), 203L, datRef);
						
						if (escalas != null){
						    wPostos.setValue("codEsc", escalas.toString());
						}else{
						    wPostos.setValue("codEsc", "");
						}
						
						PersistEngine.persist(posto);
						colaboradores = new ArrayList<NeoObject>();
						inss = 0;
					}
					posto = null;
					posto = AdapterUtils.createNewEntityInstance("medPosto");
					wPostos = new EntityWrapper(posto);
					wPostos.setValue("nomLoc", rsMedicao.getString("usu_lotorn") + " - " + rsMedicao.getString("usu_nomLoc"));
					wPostos.setValue("numPos", rsMedicao.getLong("usu_numpos"));
					wPostos.setValue("nVagas", rsMedicao.getLong("usu_qtdcon"));
					wPostos.setValue("numLoc", rsMedicao.getLong("numloc"));
					wPostos.setValue("codLoc", rsMedicao.getString("CodLoc"));
					nNumPosNov = rsMedicao.getLong("usu_numpos");
					wPostos.setValue("codCcu", rsMedicao.getString("usu_codccu"));
					nNumPosAnt = nNumPosNov;
					postos.add(posto);
					colaboradores = new ArrayList<NeoObject>();
				}

				cargaHorPos = OrsegupsMedicaoUtilsMensal.cargaHorariaDoPosto(rsMedicao.getLong("usu_codemp"), rsMedicao.getLong("usu_codfil"), rsMedicao.getLong("usu_numctr"), rsMedicao.getLong("usu_numpos"));
				if (cargaHorPos != 0L)
				{
					wColaborador.setValue("cargaHorPos", cargaHorPos + " horas");
				}
				else
				{
					wColaborador.setValue("cargaHorPos", "Carga horaria não cadastrada!");
				}

				wColaborador.setValue("funcao", rsMedicao.getString("titred"));
				wColaborador.setValue("local", rsMedicao.getString("usu_lotorn"));
				wColaborador.setValue("numCadColaborador", rsMedicao.getLong("numcad"));
				wColaborador.setValue("nomeColaborador", destacar + rsMedicao.getString("nomfun"));
				wColaborador.setValue("datAdm", NeoUtils.safeDateFormat(rsMedicao.getDate("datadm"), "dd/MM/yyyy") + "");
				wColaborador.setValue("codCcu", rsMedicao.getString("usu_codccu"));

				String desMed = desconto + " Dia(s)" + " ";
				wColaborador.setValue("descMed", desMed);
				wColaborador.setValue("situacao", rsMedicao.getString("DesSit"));
				wColaborador.setValue("horMed", 0L);
				wColaborador.setValue("minMed", 0L);
				wColaborador.setValue("horEx", 0L);
				wColaborador.setValue("minEx", 0L);
				String alertaColaborador = "ok";
				if (!desMed.contains("30"))
				{
					alertaColaborador = "falta";
				}

				if (inss > 0)
				{
					if (!alertaColaborador.contains("ok"))
					{
						alertaColaborador += ";retornoINSS";
					}
					else
					{
						alertaColaborador = ";retornoINSS";
					}
				}
				wColaborador.setValue("alertasColaborador", alertaColaborador);
				wColaborador.setValue("codEsc", rsMedicao.getString("NomEsc"));
				colaboradores.add(colaboradorMedicao);
				PersistEngine.persist(colaboradorMedicao);
				//BKP
				OrsegupsMedicaoUtilsMensal.registaBKPMedicaoDeContratos(colaboradorMedicao, "MEDColaboradores");
				//BKP

			}
			// Persist ultima medicao.
			if (nNumCtrNov != 0)
			{
				//Persistir ultimo posto - INICIO
				long nColaboradores = colaboradores.size();
				wPostos.setValue("nColaboradores", nColaboradores);
				wPostos.setValue("listaColaboradores", colaboradores);
				String alertaPosto = "ok";
				for (NeoObject neoObject2 : colaboradores)
				{
					EntityWrapper wColab = new EntityWrapper(neoObject2);
					String falta = (String) wColab.findField("descMed").getValue();

					if (!falta.contains("30"))
					{
						alertaPosto = "falta";
						break;
					}
				}

				long qtdCol = 0;
				long qtdVag = 0;
				qtdCol = (long) wPostos.findField("nColaboradores").getValue();
				qtdVag = (long) wPostos.findField("nVagas").getValue();
				if (qtdCol != qtdVag)
				{
					if (!alertaPosto.contains("ok"))
					{
						alertaPosto += ";vagasDivergentes";
					}
					else
					{
						alertaPosto = ";vagasDivergentes";
					}
				}
				wPostos.setValue("alertasPosto", alertaPosto);
				String alertCtr = alertaPosto;
				PersistEngine.persist(posto);
				//Persistir ultimo posto - FIM

				//Persistir ultima Medição - INICIO
				wMedicao.setValue("alertas", "");
				wMedicao.setValue("numOfi", nomOfi);
				Long numCtr = (Long) wMedicao.findField("contrato").getValue();
				GregorianCalendar datini = (GregorianCalendar) datRef.clone();
				datini.set(GregorianCalendar.DAY_OF_MONTH, 1);
				wMedicao.setValue("alertas", alertCtr + OrsegupsMedicaoUtilsMensal.InsereDemitidosNoPeriodo(numCtr, datini, datRef));
				wMedicao.setValue("listaPostos", postos);
				PersistEngine.persist(medicao);
				//Persistir ultima Medição - FIM
				//BKP
				OrsegupsMedicaoUtilsMensal.registaBKPMedicaoDeContratos(medicao, "MEDMedicaoDeContratos");
				//BKP
				OrsegupsMedicaoUtilsMensal.inserePostosSemColaborador(periodo);
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connVetorh, stMedicao, rsMedicao);
		}
	}
}
