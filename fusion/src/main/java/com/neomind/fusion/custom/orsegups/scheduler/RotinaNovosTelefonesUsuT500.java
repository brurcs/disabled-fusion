package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoUtils;

public class RotinaNovosTelefonesUsuT500 implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(RotinaNovosTelefonesUsuT500.class);
	@Override
	public void execute(CustomJobContext arg0)
	{
		
		log.warn("##### INICIAR ROTINA DE INSERÇÃO DE NOVOS TELEFONES USU_T500FNE - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));


		//CONEXOES
		Connection connSigma = null;
		Connection connSapiens = null;
		
		PreparedStatement pstmSapiens = null;
		PreparedStatement pstmSigma = null;
		PreparedStatement pstmCdm160 = null;
		PreparedStatement pstmSel160 = null;
		
		ResultSet rsSapiens = null;
		ResultSet rsSigma = null;
		ResultSet rsSel160 = null;
		
		int contador = 0;
		//LISTA NOVA
		List<String> listaTelefones = new ArrayList<String>();

		//LISTA ANTIGA
		List<String> listaTelefonesUSU_T160FNE = new ArrayList<String>();

		//LISTA COMANDOS SQL
		List<String> listaCmd = new ArrayList<String>();

		try
		{
			connSapiens = PersistEngine.getConnection("SAPIENS");
			connSigma = PersistEngine.getConnection("SIGMA90");

			StringBuilder sqlSapiens = new StringBuilder();
			sqlSapiens.append(" SELECT cli.foncli, cli.foncl2, cli.foncl3, cli.foncl4, cli.foncl5, ctr.usu_clifon, cli.codcli  ");
			sqlSapiens.append(" FROM e085cli cli  WITH (NOLOCK) ");
			sqlSapiens.append(" INNER JOIN  usu_t160ctr ctr  WITH (NOLOCK) ON (cli.codcli  = ctr.usu_codcli)  ");
			sqlSapiens.append(" INNER JOIN usu_t160cvs cvs  WITH (NOLOCK) ON (cvs.usu_numctr = ctr.usu_numctr and ");
			sqlSapiens.append(" cvs.usu_codfil = ctr.usu_codfil and cvs.usu_codemp = ctr.usu_codemp) ");
			sqlSapiens.append(" WHERE ((cvs.usu_sitcvs = 'A') OR (cvs.usu_sitcvs = 'I' and cvs.usu_datfim >=  GETDATE())) and cli.tipemc = 1");

//			StringBuilder sqlSigma = new StringBuilder();
//			sqlSigma.append(" SELECT DBC.FONE1, DBC.FONE2, DBP.FONE1, DBP.FONE2, DBC.CD_CLIENTE  ");
//			sqlSigma.append(" FROM dbPROVIDENCIA DBP  WITH (NOLOCK)  ");
//			sqlSigma.append(" RIGHT JOIN dbCENTRAL DBC  WITH (NOLOCK) ON (DBC.CD_CLIENTE =  DBP.CD_CLIENTE) ");
//			sqlSigma.append(" WHERE DBC.ctrl_central = 1 AND DBC.TP_PESSOA <> 2 ");

			StringBuilder sqlSapiens160 = new StringBuilder();
			sqlSapiens160.append(" select usu_numfne, usu_cd_clie, usu_codcli from usu_t500fne WITH (NOLOCK) ");

			//BUSCA-SE REGISTROS NA BASE DO SAPIENS
			pstmSapiens = connSapiens.prepareStatement(sqlSapiens.toString());
			rsSapiens = pstmSapiens.executeQuery();
			connSapiens.setAutoCommit(false);
			
			while (rsSapiens.next())
			{
				String str = this.formatarNumero(rsSapiens.getString(1));
				if (!str.equals("not"))
				{
					if (!listaTelefones.contains(str + ".0." + rsSapiens.getString(7)))
					{
						listaTelefones.add(str + ".0." + rsSapiens.getString(7));
					}
				}

				String str2 = this.formatarNumero(rsSapiens.getString(2));
				if (!str2.equals("not"))
				{
					if (!listaTelefones.contains(str2 + ".0." + rsSapiens.getString(7)))
					{
						listaTelefones.add(str2 + ".0." + rsSapiens.getString(7));
					}
				}

				String str3 = this.formatarNumero(rsSapiens.getString(3));
				if (!str3.equals("not"))
				{
					if (!listaTelefones.contains(str3 + ".0." + rsSapiens.getString(7)))
					{
						listaTelefones.add(str3 + ".0." + rsSapiens.getString(7));
					}
				}

				String str4 = this.formatarNumero(rsSapiens.getString(4));
				if (!str4.equals("not"))
				{
					if (!listaTelefones.contains(str4 + ".0." + rsSapiens.getString(7)))
					{
						listaTelefones.add(str4 + ".0." + rsSapiens.getString(7));
					}
				}

				String str5 = this.formatarNumero(rsSapiens.getString(5));
				if (!str5.equals("not"))
				{
					if (!listaTelefones.contains(str5 + ".0." + rsSapiens.getString(7)))
					{
						listaTelefones.add(str5 + ".0." + rsSapiens.getString(7));
					}
				}

				String str6 = this.formatarNumero(rsSapiens.getString(6));
				if (!str6.equals("not"))
				{
					if (!listaTelefones.contains(str6 + ".0." + rsSapiens.getString(7)))
					{
						listaTelefones.add(str6 + ".0." + rsSapiens.getString(7));
					}
				}

			}

			//BUSCA-SE REGISTROS NA BASE DO SIGMA
//			pstmSigma = connSigma.prepareStatement(sqlSigma.toString());
//			rsSigma = pstmSigma.executeQuery();
//			while (rsSigma.next())
//			{
//				String str1 = this.formatarNumero(rsSigma.getString(1));
//				if (!str1.equals("not"))
//				{
//					if (!listaTelefones.contains(str1 + "." + rsSigma.getString(5) + ".0"))
//					{
//						listaTelefones.add(str1 + "." + rsSigma.getString(5) + ".0");
//					}
//				}
//
//				String str2 = this.formatarNumero(rsSigma.getString(2));
//				if (!str2.equals("not"))
//				{
//					if (!listaTelefones.contains(str2 + "." + rsSigma.getString(5) + ".0"))
//					{
//						listaTelefones.add(str2 + "." + rsSigma.getString(5) + ".0");
//					}
//				}
//
//				String str3 = this.formatarNumero(rsSigma.getString(3));
//				if (!str3.equals("not"))
//				{
//					if (!listaTelefones.contains(str3 + "." + rsSigma.getString(5) + ".0"))
//					{
//						listaTelefones.add(str3 + "." + rsSigma.getString(5) + ".0");
//					}
//				}
//
//				String str4 = this.formatarNumero(rsSigma.getString(4));
//				if (!str4.equals("not"))
//				{
//					if (!listaTelefones.contains(str4 + "." + rsSigma.getString(5) + ".0"))
//					{
//						listaTelefones.add(str4 + "." + rsSigma.getString(5) + ".0");
//					}
//				}
//
//			}

			//BUSCA-SE REGISTROS NA BASE DO SAPIENS USU_T160FNE
			pstmSel160 = connSapiens.prepareStatement(sqlSapiens160.toString());
			rsSel160 = pstmSel160.executeQuery();
			while (rsSel160.next())
			{
				listaTelefonesUSU_T160FNE.add(rsSel160.getString(1) + "." + rsSel160.getString(2) + "." + rsSel160.getString(3));
			}
			
//			//REGISTROS A DELETAR
//			for (String deletado : listaTelefonesUSU_T160FNE)
//			{
//				if (!(listaTelefones.contains(deletado)))
//				{
//					String operadores[] = deletado.split("\\."); 
//					listaCmd.add("delete from usu_t500fne where usu_numfne = '"+operadores[0]+"' and usu_cd_clie ='"+operadores[1]+"' and usu_codcli ='"+operadores[2]+"'");
//				}
//			}
			//REGISTROS A INSERIR
			
			for (String inserido : listaTelefones)
			{
				if (!(listaTelefonesUSU_T160FNE.contains(inserido)))
				{
					String operadores[] = inserido.split("\\."); 
					listaCmd.add("insert into usu_t500fne (usu_numfne, usu_cd_clie, usu_codcli) values ('"+operadores[0]+"' ,'"+operadores[1]+"','"+operadores[2]+"')");
				}
			}

			//EXECUTA-SE OS COMANDOS
			
			for (String cmd : listaCmd)
			{
				pstmCdm160 = connSapiens.prepareStatement(cmd);
				pstmCdm160.executeUpdate();
				contador++;
				
				
			}
			
			
			connSapiens.commit();
			connSapiens.setAutoCommit(true);
			/*
			 * String url =
			 * "http://www.orsegups.com.br/fusion/custom/jsp/orsegups/snep/isCliente.jsp?foneOrigem=reload"
			 * ;
			 * try
			 * {
			 * URLEncoder.encode(url, "ISO-8859-1");
			 * URL urlAux = new URL(url);
			 * URLConnection uc = urlAux.openConnection();
			 * BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
			 * String teste = in.readLine();
			 * Log.warn(teste);
			 * }
			 * catch (IOException e)
			 * {
			 * // TODO Auto-generated catch block
			 * e.printStackTrace();
			 * }
			 */
			log.warn("##### EXECUTAR ROTINA DE INSERÇÃO DE NOVOS TELEFONES USU_T500FNE - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			log.error("##### ERRO ROTINA DE INSERÇÃO DE NOVOS TELEFONES USU_T500FNE - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy")+" "+e.getMessage());
		}
		finally
		{
			try
			{
			
				OrsegupsUtils.closeConnection(null, pstmSel160, rsSel160);
				OrsegupsUtils.closeConnection(null, pstmCdm160, null);
				OrsegupsUtils.closeConnection(connSigma, pstmSigma, rsSigma);
				OrsegupsUtils.closeConnection(connSapiens, pstmSapiens, rsSapiens);
				
			}
			catch (Exception e)
			{

				log.error("##### ERRO AO FINALIZAR CONEXÃO ROTINA DE INSERÇÃO DE NOVOS TELEFONES USU_T500FNE - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy")+" "+e.getMessage());
			}
			log.warn("##### FINALIZAR  ROTINA DE INSERÇÃO DE NOVOS TELEFONES USU_T500FNE INSERIDOS: "+contador+" TELEFONES - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));

		}
	}

	public String formatarNumero(String auxFoneStr)
	{
		//VALIDACAO DA STRING
		if (auxFoneStr != null)
		{
			if (!(auxFoneStr.equals(" ") || auxFoneStr.isEmpty()))
			{
				auxFoneStr = auxFoneStr.replaceAll("[^0-9]", "");
				if (auxFoneStr.length() == 10 && !(auxFoneStr.equals("0000000000"))
											  && !(auxFoneStr.equals("1111111111"))
											  && !(auxFoneStr.equals("2222222222"))
											  && !(auxFoneStr.equals("3333333333"))
											  && !(auxFoneStr.equals("4444444444"))
											  && !(auxFoneStr.equals("5555555555"))
											  && !(auxFoneStr.equals("6666666666"))
											  && !(auxFoneStr.equals("7777777777"))
											  && !(auxFoneStr.equals("8888888888"))
											  && !(auxFoneStr.equals("9999999999")))
				{
					return auxFoneStr;
				}
			}
		}
		return "not";
	}
}

