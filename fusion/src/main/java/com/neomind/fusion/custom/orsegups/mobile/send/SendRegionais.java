package com.neomind.fusion.custom.orsegups.mobile.send;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.mobile.vo.RegionalVO;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

/**
 * Servlet implementation class SendRegionais
 */

@WebServlet(name="SendRegionais", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.mobile.send.SendRegionais"})
public class SendRegionais extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SendRegionais() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");

		PrintWriter out = response.getWriter();
		try {
			out.print(regionais());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
	private String regionais(){
		List<NeoObject> lstObjPesqReg = null;
		List<RegionalVO> listaRegionais = null;
		String json = null;
		Gson gson = new Gson();
		
		try{
			listaRegionais = new ArrayList<RegionalVO>();
			lstObjPesqReg = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("IMREGIONAL"));
			for (NeoObject obj : lstObjPesqReg){
				EntityWrapper wObj = new EntityWrapper(obj);
				RegionalVO regional = new RegionalVO();
				regional.setId( NeoUtils.safeOutputString(wObj.findValue("codigo")) );
				regional.setNome( NeoUtils.safeOutputString(wObj.findValue("sigla")) );
				listaRegionais.add(regional);
			}
			json = gson.toJson(listaRegionais);
			System.out.println("Regionais ->" + json);
			return json;
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Regionais ->[]");
			return "";
		}
	}

}
