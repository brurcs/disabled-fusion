package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.HtmlEmail;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.integracaoMobile.IntegracaoPortalMobile;
import com.neomind.fusion.custom.orsegups.objRatMobile.ObjRatMobile;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsEmailUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;

/**
 * Método para envio de e-mail para os clientes
 * 
 * quando a ordem de serviço for agendada
 * 
 * no e-mail será enviado um breve texto contendo o Nº da OS junto com os dados
 * do cliente.
 * 
 * @author Lucas Alison
 * 
 * @param CustomJobContext
 *            arg0
 * 
 * @return void
 */

public class EmailDeliveryOrdemServicoAgendada implements CustomJobAdapter {
    private static final Log log = LogFactory.getLog(EmailDeliveryOrdemServicoAgendada.class);

    @SuppressWarnings("unchecked")
    @Override
    public void execute(CustomJobContext arg0) {

	log.warn("E-Mail OS executado em: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	Long key = GregorianCalendar.getInstance().getTimeInMillis();

	Set<Integer> empresasNotificadas = new HashSet<Integer>();

	Connection conn = null;
	StringBuilder strSigma = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	try {

	    conn = PersistEngine.getConnection("SIGMA90");

	    strSigma = new StringBuilder();

	    InstantiableEntityInfo infoEnvioEmail = (InstantiableEntityInfo) EntityRegister.getEntityInfo("EmailDelivery");
	    InstantiableEntityInfo infoTec = (InstantiableEntityInfo) EntityRegister.getEntityInfo("OSEmailAutomaticoTecnicos");
	    InstantiableEntityInfo infoCadTec = (InstantiableEntityInfo) EntityRegister.getEntityInfo("OSEmaiCadastrolAutomaticoTecnicosCM");

	    strSigma.append(" 	SELECT OS.ID_ORDEM, OS.DATAAGENDADA, C.CGCCPF, C.RAZAO, C.ENDERECO, BAI.NOME AS NOMEBAIRRO, CID.NOME AS NOMECIDADE, OS.DEFEITO, C.EMAILRESP,  ");
	    strSigma.append("	       C.ID_EMPRESA, C.CD_CLIENTE, c.OBSERVACAO, col.CD_COLABORADOR, col.NM_COLABORADOR ");
	    strSigma.append(" 	FROM dbORDEM OS ");
	    strSigma.append(" 	INNER JOIN dbCENTRAL C ON C.CD_CLIENTE = OS.CD_CLIENTE ");
	    strSigma.append(" 	INNER JOIN COLABORADOR col ON col.CD_COLABORADOR = os.ID_INSTALADOR	");
	    strSigma.append(" 	INNER JOIN OSDEFEITO d on d.IDOSDEFEITO = os.IDOSDEFEITO ");
	    strSigma.append(" 	INNER JOIN dbCIDADE cid ON c.ID_CIDADE = cid.ID_CIDADE ");
	    strSigma.append(" 	INNER JOIN dbBAIRRO bai ON c.ID_CIDADE = bai.ID_CIDADE AND c.ID_BAIRRO = bai.ID_BAIRRO ");
	    strSigma.append(" 	WHERE OS.FECHADO <> 1 AND OS.DATAAGENDADA IS NOT NULL AND C.TP_PESSOA != 2 AND OS.DATAAGENDADA >= CONVERT (date, GETDATE()) ");
	    strSigma.append("   AND NOT EXISTS (SELECT 1 FROM [FSOODB04\\SQL02].TIDB.DBO.HISTORICO_RAT_OS_AGENDADA H WHERE H.ID_ORDEM = OS.ID_ORDEM AND H.DT_AGENDAMENTO = OS.DATAAGENDADA ) ");

	    pstm = conn.prepareStatement(strSigma.toString());

	    rs = pstm.executeQuery();

	    while (rs.next()) {
		String cgcCpf = rs.getString("CGCCPF") == null ? "" : rs.getString("CGCCPF").replaceAll("([.\\-/])", "").trim();
		boolean clienteComExcecao = OrsegupsEmailUtils.verificarExcecaoCNPJ(rs.getInt("CD_CLIENTE"));
		String observacao = rs.getString("OBSERVACAO") == null ? "" : rs.getString("OBSERVACAO");
		List<NeoObject> listaEmails = PersistEngine.getObjects(infoTec.getEntityClass(), new QLEqualsFilter("codigo", rs.getString("CD_COLABORADOR")), -1, -1);
		String idOrdem = rs.getString("ID_ORDEM");
		Date dtAgendada = rs.getDate("DATAAGENDADA");
		String dataAtendimento = NeoDateUtils.safeDateFormat(dtAgendada, "dd/MM/yyyy");
		String horaAtendimento = rs.getString("DATAAGENDADA").substring(11, 16);
		GregorianCalendar dataAgendada = new GregorianCalendar();
		dataAgendada.setTime(rs.getTimestamp("DATAAGENDADA"));
		if (!clienteComExcecao) {
		    if (listaEmails != null && !listaEmails.isEmpty()) {
			EntityWrapper wpneo = new EntityWrapper(listaEmails.get(0));
			String email = (rs.getString("EMAILRESP") == null ? "" : rs.getString("EMAILRESP"));
			String razao = (rs.getString("RAZAO") == null ? "" : rs.getString("RAZAO"));
			String cdCliente = rs.getString("CD_CLIENTE");
			String endereco = (rs.getString("ENDERECO") == null ? "" : rs.getString("ENDERECO"));
			String cidade = (rs.getString("NOMECIDADE") == null ? "" : rs.getString("NOMECIDADE"));
			String bairro = (rs.getString("NOMEBAIRRO") == null ? "" : rs.getString("NOMEBAIRRO"));
			String defeito = (rs.getString("DEFEITO") != null ? rs.getString("DEFEITO") : "");

			int empresa = rs.getInt("ID_EMPRESA");

			String matriculaTecnico = (String) wpneo.findValue("matricula");
			String nomeTecnico = wpneo.findValue("nome").toString().toUpperCase();

			String descricao = "Vazio";
			System.out.println(NeoDateUtils.safeDateFormat(dataAgendada, "dd/MM/yyyy HH:mm:ss"));
			if (defeito != null && !defeito.isEmpty()) {
			    descricao = defeito;
			}

			boolean flagAtualizadoNAC = observacao.contains("#AC");
			List<String> emailClie = OrsegupsEmailUtils.validarEmail(email, cdCliente, flagAtualizadoNAC);

			if ((emailClie != null) && (!emailClie.isEmpty())) {

			    StringBuilder noUserMsg = new StringBuilder();

			    final String tipo = OrsegupsEmailUtils.TIPO_OS_AGENDADA;

			    Map<Integer, String> params = OrsegupsEmailUtils.getConfiguracaoEmpresa(empresa);

			    String pasta = null;
			    String remetente = null;
			    String grupo = null;
			    if (params != null) {

				pasta = params.get(OrsegupsEmailUtils.RAT_PASTA);
				remetente = params.get(OrsegupsEmailUtils.RAT_REMETENTE);
				grupo = params.get(OrsegupsEmailUtils.RAT_GRUPO);
				String neoId = params.get(OrsegupsEmailUtils.RAT_NEOID);
				Long tipRat = 13L;
				String ratingToken = DigestUtils.sha256Hex(cgcCpf + Long.toString(tipRat) + Long.toString(new java.util.Date().getTime()));

				for (String emailFor : emailClie) {

				    noUserMsg = new StringBuilder();

				    noUserMsg.append(OrsegupsEmailUtils.getCabecalhoEmail(tipo, pasta, ratingToken));

				    noUserMsg.append(" <!--- main content table begins--->");
				    noUserMsg.append(" <table width=\"600\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\">");
				    noUserMsg.append(" <tbody><tr><td colspan=\"3\"><img width=\"1\" height=\"15\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif\" alt=\"\"></td></tr>");
				    noUserMsg.append(" <tr><td width=\"10\"></td>");
				    noUserMsg.append(" <td align=\"left\" style=\"FONT-WEIGHT: regular; FONT-SIZE: 18px; COLOR: #0298d5; FONT-FAMILY: Helvetica, Arial, sans-serif;\">");
				    noUserMsg.append(" <font style=\"FONT-WEIGHT: regular; FONT-SIZE: 18px; COLOR: #0298d5; FONT-FAMILY: Helvetica, Arial, sans-serif;\">Prezado Cliente,</font>");
				    noUserMsg.append(" <br>");
				    noUserMsg.append(" </td><td width=\"10\"></td>");
				    noUserMsg.append(" </tr>");
				    noUserMsg.append(" <tr><td width=\"10\"></td>");
				    noUserMsg.append(" <td align=\"left\">");
				    noUserMsg.append("  <img width=\"10\" height=\"22\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif\" alt=\"\"></td><td width=\"10\"></td>");
				    noUserMsg.append(" </tr>");
				    noUserMsg.append(" <tr><td width=\"10\"><img width=\"10\" height=\"15\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif\" alt=\"\"></td>");
				    noUserMsg.append(" <td style=\"FONT-WEIGHT: regular; FONT-SIZE: 18px; FONT-FAMILY: Helvetica, Arial, sans-serif;\">");
				    noUserMsg.append(" Informamos que sua ordem de serviço " + idOrdem + " foi agendada para a seguinte data: <b>" + dataAtendimento + " " + (horaAtendimento != null && !horaAtendimento.trim().equals("00:00") ? horaAtendimento : "") + "</b><br>");
				    noUserMsg.append(" <div align=\"center\"><br>");

				    Long neoIdFoto = ((NeoFile) wpneo.getValue("fotoTec")).getNeoId();

				    if (neoIdFoto == null || neoIdFoto.equals(0L)) {
					noUserMsg.append(" <img style=\"max-height:226px\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/default-user.png\" border=\"0\" alt=\"Técnico Responsável\"></div>");
				    } else {
					noUserMsg.append(" <img style=\"max-height:226px\" src=\"http://intranet.orsegups.com.br/fusion/file/download/" + ((NeoFile) wpneo.getValue("fotoTec")).getNeoId() + "\" border=\"0\" alt=\"Técnico Responsável\"></div>");
				    }

				    noUserMsg.append(" <div align=\"center\" style=\"font-size:14px;;line-height:18px; color:#0298d5; font-weight:bold; font-family:Helvetica, Arial, sans-serif\">" + nomeTecnico + "</div> ");
				    noUserMsg.append(" <div align=\"center\" style=\"FONT-WEIGHT: normal; FONT-SIZE: 10px; COLOR: #999999; font-family:Arial;valign:top;\">Matrícula: " + matriculaTecnico + "&nbsp;*</div><br>");
				    noUserMsg.append(" <br>");
				    noUserMsg.append(" <div align=\"left\" style=\"FONT-WEIGHT: normal; FONT-SIZE: 10px; COLOR: #999999; font-family:Arial;valign:top;\">*&nbsp;Para sua segurança, solicite ao nosso técnico o crachá de identificação.</div>");
				    noUserMsg.append(" <br>");
				    noUserMsg.append(" O atendimento refere-se ao seu sistema de segurança instalado no endereço abaixo:<br><br>");
				    noUserMsg.append(" <strong>Nome/Razão Social: </strong> " + razao + "<br>");
				    noUserMsg.append(" <strong>Endereço: </strong>" + endereco + "<br>");
				    noUserMsg.append(" <strong>Bairro: </strong>" + bairro + "<br>");
				    noUserMsg.append(" <strong>Cidade: </strong>" + cidade);
				    noUserMsg.append(" <br>");
				    noUserMsg.append(" <strong>Descrição da OS: </strong>" + descricao);

				    noUserMsg.append(" </td><td width=\"10\"><img width=\"10\" height=\"15\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif\" alt=\"\"></td>");
				    noUserMsg.append(" </tr>");
				    noUserMsg.append(" </tbody></table>");
				    noUserMsg.append(" <!--- main content table ends--->");

				    noUserMsg.append(OrsegupsEmailUtils.getRodapeEmail(tipo, pasta, grupo, email, remetente, ratingToken));

				    // TESTE E-MAIL UTILIZANDO RECURSO FUSION
				    //String subject = "Atendimento Agendado: " + idOrdem;
				    //OrsegupsEmailUtils.sendTestEmail(noUserMsg, subject);

				    GregorianCalendar dataCad = (GregorianCalendar) GregorianCalendar.getInstance();
				    NeoObject emaiEnvio = infoEnvioEmail.createNewInstance();
				    EntityWrapper emailEnvioWp = new EntityWrapper(emaiEnvio);
				    emailEnvioWp.findField("de").setValue("os" + remetente);
				    emailEnvioWp.findField("para").setValue(emailFor + ";emailautomatico@orsegups.com.br");
				    emailEnvioWp.findField("assunto").setValue("Atendimento Agendado: " + idOrdem);
				    emailEnvioWp.findField("Mensagem").setValue(noUserMsg.toString());
				    emailEnvioWp.findField("datCad").setValue(dataCad);
				    PersistEngine.persist(emaiEnvio);

				    try {
					ObjRatMobile objRat = new ObjRatMobile();
					objRat.setTipRat(13L);
					objRat.setRatingToken(ratingToken);
					objRat.setInformativo("Prezado Cliente, Informamos que sua ordem de serviço " + idOrdem + " foi agendada para a seguinte data: " + dataAtendimento + " " + horaAtendimento);
					objRat.setEvento("Atendimento Agendado: " + idOrdem);
					objRat.setResultadoDoAtendimento("");
					objRat.setHashId("Atendimento Agendado: " + idOrdem);
					objRat.setResultado("O atendimento refere-se ao seu sistema de segurança instalado no endereço abaixo:");
					objRat.setAtendidoPor("");
					objRat.setLocal(razao + " - " + endereco + " - " + bairro + " - " + cidade);
					objRat.setDataAtendimento(dataAtendimento);
					objRat.setHoraAtendimento(horaAtendimento);
					objRat.setLnkFotoAit("http://intranet.orsegups.com.br/fusion/file/download/" + ((NeoFile) wpneo.getValue("fotoTec")).getNeoId());
					objRat.setLnkFotoLocal("");
					objRat.setObservacao(defeito.toUpperCase());
					objRat.setEmpRat(grupo);
					objRat.setNeoId(neoId);

					if (!cgcCpf.equals("")) {
					    objRat.setCgcCpf(Long.parseLong(cgcCpf));
					    IntegracaoPortalMobile integracao = new IntegracaoPortalMobile();
					    integracao.inserirInformacoesPush(objRat);
					}
				    } catch (Exception e) {
					log.error("Erro IntegracaoPortalMobile EmailDeliveryOSAgendamento", e);
				    }

				}

			    } else {
				if (!empresasNotificadas.contains(empresa)) {
				    OrsegupsEmailUtils.enviarNotificacaoEmpresaSemConfig(empresa);
				    empresasNotificadas.add(empresa);
				}
			    }

			}
		    } else {
			List<NeoObject> listaEmailsEnviadosCM = PersistEngine.getObjects(infoCadTec.getEntityClass(), new QLEqualsFilter("cdTecnico", rs.getString("CD_COLABORADOR")), -1, -1);

			if (listaEmailsEnviadosCM.isEmpty()) {
			    MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

			    MailSettings mailClone = new MailSettings();
			    mailClone.setMinutesInterval(settings.getMinutesInterval());
			    mailClone.setFromEMail(settings.getFromEMail());
			    mailClone.setFromName(settings.getFromName());
			    mailClone.setRenderServer(settings.getRenderServer());
			    mailClone.setSecurityFromEmail(settings.getSecurityFromEmail());
			    mailClone.setSecurityCopyEmail(settings.getSecurityCopyEmail());
			    mailClone.setSmtpSettings(settings.getSmtpSettings());
			    mailClone.setPort(settings.getPort());
			    mailClone.setSmtpServer(settings.getSmtpServer());
			    mailClone.setEnabled(settings.isEnabled());

			    mailClone.setFromEMail("os@orsegups.com.br");

			    mailClone.setFromName("Orsegups Partipações S.A.");
			    if (mailClone != null && mailClone.isEnabled()) {

				HtmlEmail noUserEmail = new HtmlEmail();
				StringBuilder noUserMsg = new StringBuilder();

				Calendar saudacao = Calendar.getInstance();
				String saudacaoEMail = "";
				if (saudacao.get(Calendar.HOUR_OF_DAY) >= 6 && saudacao.get(Calendar.HOUR_OF_DAY) <= 12) {
				    saudacaoEMail = "Bom dia, ";
				} else if (saudacao.get(Calendar.HOUR_OF_DAY) > 12 && saudacao.get(Calendar.HOUR_OF_DAY) <= 18) {
				    saudacaoEMail = "Boa tarde, ";
				} else {
				    saudacaoEMail = "Boa noite, ";
				}

				noUserEmail.setCharset("ISO-8859-1");
				noUserEmail.addTo("cm@orsegups.com.br");

				noUserEmail.setFrom("os@orsegups.com.br");
				noUserEmail.setSubject("Não Responda - Cadastro de Técnico " + rs.getString("NM_COLABORADOR"));

				noUserMsg.append("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></head><body>");
				noUserMsg.append("<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#ffffff\">");
				noUserMsg.append("          <tbody>");
				noUserMsg.append("		    <td align=\"left\" valign=\"top\" >");
				noUserMsg.append("               <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/logoORS.jpg\" >");
				noUserMsg.append("            </td>");
				noUserMsg.append("			<td>");
				noUserMsg.append("			<strong>" + saudacaoEMail + " favor providenciar o cadastro do técnico para execução de ordens de serviço.</strong><br>");
				noUserMsg.append("			<strong>Código: </strong> " + rs.getString("CD_COLABORADOR") + " <br>");
				noUserMsg.append("			<strong>Nome: </strong>" + rs.getString("NM_COLABORADOR") + " <br>");
				noUserMsg.append("			</td>");
				noUserMsg.append("     </tbody></table>");
				noUserMsg.append("</body></html>");
				noUserEmail.setHtmlMsg(noUserMsg.toString());
				noUserEmail.setContent(noUserMsg.toString(), "text/html");
				mailClone.applyConfig(noUserEmail);

				noUserEmail.send();

				NeoObject emailCadTec = infoCadTec.createNewInstance();
				EntityWrapper emailHisWp = new EntityWrapper(emailCadTec);
				emailHisWp.findField("cdTecnico").setValue(rs.getString("CD_COLABORADOR"));
				PersistEngine.persist(emailCadTec);

			    }
			}
		    }
		}
		gravarRATOSEnviado(idOrdem, dataAgendada);
	    }
	}

	catch (Exception e) {
	    log.error("E-Mail OS erro no processamento:" + e.getMessage());
	    System.out.println("[" + key + "] E-Mail OS erro no processamento: " + e.getMessage());
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

    }

    private void gravarRATOSEnviado(String idOrdem, GregorianCalendar dataAgendada) {

	Connection conn = null;
	PreparedStatement pstm = null;

	StringBuilder sql = new StringBuilder();

	try {
	    conn = PersistEngine.getConnection("TIDB");

	    sql.append(" INSERT INTO HISTORICO_RAT_OS_AGENDADA (ID_ORDEM, DT_AGENDAMENTO, DT_CADASTRO) VALUES (?,?,GETDATE()) ");

	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setLong(1, Long.parseLong(idOrdem));
	    pstm.setTimestamp(2, new Timestamp(dataAgendada.getTimeInMillis()), dataAgendada);

	    pstm.executeUpdate();

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}

    }

}