package com.neomind.fusion.custom.orsegups.medicaoContratos;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class OrsegupsMedicaoUtils
{

	public static List<NeoObject> validaAfastamento(Long numEmp, Long numCad, Long tipCol, GregorianCalendar periodoFim, Long numLoc)

	{
		Connection connVetorh = PersistEngine.getConnection("VETORH");
		ResultSet rsAfastamento = null;
		PreparedStatement stAfastamento = null;
		NeoObject consideracoes = null;
		List<NeoObject> listConsideracoes = new ArrayList<NeoObject>();
		String dataFiltro = retornaDataFormatoSapiensEVetorh(periodoFim);

		StringBuilder queryAfastamento = new StringBuilder();
		try
		{

			queryAfastamento.append("  SELECT afa.obsafa,CONVERT(VARCHAR(15),AFA.DATAFA,103) AS DATAFA,CONVERT(VARCHAR(15),COB.USU_DATALT,103) as USU_DATALT,isnull(COB.USU_NUMCADCOB,0) AS COLEFE,COB.USU_NUMEMPCOB,COB.USU_NUMCAD AS COLSUB,FUN.NOMFUN,");
			queryAfastamento.append("  FUN2.nomfun as funCob,FUN2.numemp as EMPSUB,FUN.numemp,SIT.DesSit,CAR.TitRed,CONVERT(VARCHAR(15),fun2.datadm,103) as datadm,");
			queryAfastamento.append(" CASE WHEN (EXISTS(SELECT 1 FROM USU_T038COBFUN WHERE USU_NUMLOCTRA = ? AND CAST(USU_DATALT AS DATE) = AFA.DATAFA AND (USU_NUMCADCOB =0 OR USU_NUMCADCOB IS NULL)))");
			queryAfastamento.append("  THEN 1 ELSE 0 END AS COBPOS ");
			queryAfastamento.append(" FROM R038AFA AFA");
			queryAfastamento.append(" LEFT JOIN USU_T038COBFUN COB ON COB.USU_NUMCADCOB = AFA.NUMCAD AND COB.USU_NUMEMPCOB = AFA.NUMEMP AND COB.USU_TIPCOLCOB = AFA.TIPCOL AND AFA.DATAFA = CAST(COB.USU_DATALT AS DATE)");
			queryAfastamento.append(" LEFT JOIN R034FUN FUN ON FUN.NUMCAD = COB.USU_NUMCADCOB AND FUN.NUMEMP = COB.USU_NUMEMPCOB AND FUN.TIPCOL = COB.USU_TIPCOLCOB");
			queryAfastamento.append(" LEFT JOIN R034FUN FUN2 ON FUN2.NUMCAD = COB.usu_numcad AND FUN2.NUMEMP = COB.usu_numemp AND FUN2.TIPCOL = COB.usu_tipcol");
			queryAfastamento.append(" LEFT JOIN R010SIT SIT ON SIT.CodSit = FUN2.SitAfa");
			queryAfastamento.append(" LEFT JOIN R038HCA HCA ON HCA.NumEmp = FUN.numemp AND HCA.NumCad = FUN.numcad AND HCA.TipCol = FUN.tipcol AND HCA.DatAlt =");
			queryAfastamento.append(" (SELECT MAX(DATALT) FROM R038HCA HCA2 WHERE HCA2.NumEmp = HCA.NumEmp AND HCA2.TipCol = HCA.TipCol AND HCA2.NumCad = HCA.NumCad)");
			queryAfastamento.append(" LEFT JOIN R024CAR CAR ON CAR.CodCar = HCA.CodCar and car.estcar = 1");
			queryAfastamento.append(" WHERE AFA.NUMCAD = ? AND AFA.NUMEMP = ? AND AFA.TIPCOL = ? AND AFA.DATAFA = ? ");

			stAfastamento = connVetorh.prepareStatement(queryAfastamento.toString());

			stAfastamento.setLong(1, numLoc);
			stAfastamento.setLong(2, numCad);
			stAfastamento.setLong(3, numEmp);
			stAfastamento.setLong(4, tipCol);
			stAfastamento.setString(5, dataFiltro);

			rsAfastamento = stAfastamento.executeQuery();
			while (rsAfastamento.next())
			{
				String escala = "";
				consideracoes = AdapterUtils.createNewEntityInstance("medObs");
				EntityWrapper wConsideracoes = new EntityWrapper(consideracoes);
				if (rsAfastamento.getLong("COLEFE") != 0L)
				{
					List<String> ponto = pontoColaborador(rsAfastamento.getLong("EMPSUB"), rsAfastamento.getLong("COLSUB"), 1L, periodoFim);
					wConsideracoes.setValue("obsCon", "Cobertura:");
					wConsideracoes.setValue("numCad", rsAfastamento.getLong("COLSUB"));
					wConsideracoes.setValue("nomFunCob", rsAfastamento.getString("funCob"));
					wConsideracoes.setValue("titRed", rsAfastamento.getString("TitRed"));
					wConsideracoes.setValue("datAdm", rsAfastamento.getString("datadm"));
					wConsideracoes.setValue("sitAfa", rsAfastamento.getString("DesSit"));
					escala = getEscalaColaborador(rsAfastamento.getLong("EMPSUB"), rsAfastamento.getLong("COLSUB"), 1L);
					wConsideracoes.setValue("DesEscCob", escala);

					if (ponto != null && ponto.size() > 0)
					{
						wConsideracoes.setValue("horIni", ponto.get(0));
						//						if (ponto.get(1) != null && !ponto.get(1).equals("branco"))
						//						{
						//							wConsideracoes.setValue("codjmaEntradaCob", ponto.get(1));
						//
						//						}
					}
					if (ponto != null && ponto.size() > 1)
					{
						wConsideracoes.setValue("horFim", ponto.get(1));
						//						if (ponto.get(3) != null && !ponto.get(3).equals("branco"))
						//						{
						//							wConsideracoes.setValue("codjmaSaidaCob", ponto.get(3));
						//						}
					}

					listConsideracoes.add(consideracoes);
					PersistEngine.persist(consideracoes);

				}
				else
				{
					wConsideracoes.setValue("obsCon", "Justificativa:");
					wConsideracoes.setValue("obsafa", rsAfastamento.getString("obsAfa"));
					listConsideracoes.add(consideracoes);
					PersistEngine.persist(consideracoes);

				}
			}

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connVetorh, stAfastamento, rsAfastamento);
		}
		return listConsideracoes;
	}

	public static Long cargaHorariaDoPosto(Long usu_codemp, Long usu_codfil, Long usu_numctr, Long usu_numpos)
	{
		Connection connSapiens = PersistEngine.getConnection("SAPIENS");
		ResultSet rsPosto = null;
		PreparedStatement stPosto = null;
		StringBuilder queryCargaHorPos = new StringBuilder();
		Long horIni = 0L;
		Long auxIni = 0L;
		Long horFim = 0L;
		Long diaSem = 0L;
		Long carga = 0L;
		Long intervalo = 0L;
		int cont = 0;
		try
		{
			queryCargaHorPos.append("SELECT USU_HORINI,USU_HORFIM,USU_DIASEM FROM USU_T160JOR WHERE USU_NUMCTR = ? AND USU_NUMPOS = ? AND USU_CODEMP = ? AND USU_CODFIL = ? " + "order by USU_DIASEM,USU_HORINI,USU_HORFIM");
			stPosto = connSapiens.prepareStatement(queryCargaHorPos.toString());
			stPosto.setLong(1, usu_numctr);
			stPosto.setLong(2, usu_numpos);
			stPosto.setLong(3, usu_codemp);
			stPosto.setLong(4, usu_codfil);
			rsPosto = stPosto.executeQuery();

			while (rsPosto.next())
			{

				horIni = rsPosto.getLong("USU_HORINI");
				horFim = rsPosto.getLong("USU_HORFIM");

				if (diaSem != rsPosto.getLong("USU_DIASEM"))
				{

					carga = (horFim - horIni) / 60;
					auxIni = horIni;
					diaSem = rsPosto.getLong("USU_DIASEM");
					intervalo = horFim;
					cont++;
				}
				else
				{
					horFim = rsPosto.getLong("USU_HORFIM");
					carga = (horFim - auxIni) / 60;
					intervalo = (horIni - intervalo) / 60;
					cont++;
				}
				if (cont == 2)
				{
					if (carga >= 8L)
					{
						carga = carga - intervalo;
					}
					break;
				}
			}

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connSapiens, stPosto, rsPosto);
		}

		return carga;
	}

	public static String getEscalaColaborador(Long numemp, Long numcad, Long tipcol)
	{
		Connection connSapiens = PersistEngine.getConnection("VETORH");
		String escala = "";
		ResultSet rsPosto = null;
		PreparedStatement stPosto = null;
		StringBuilder queryEscala = new StringBuilder();

		try
		{
			queryEscala.append("select top 1 (ESC.NomEsc+'/'+(convert(varchar,hes.CodTma))) as escalda from R038HES hes ");
			queryEscala.append(" inner join R006ESC ESC ON ESC.CodEsc = HES.CodEsc ");
			queryEscala.append(" where hes.NumCad = ? and hes.NumEmp = ? and hes.tipcol = ? ");
			queryEscala.append(" order by datalt desc ");

			stPosto = connSapiens.prepareStatement(queryEscala.toString());
			stPosto.setLong(1, numcad);
			stPosto.setLong(2, numemp);
			stPosto.setLong(3, tipcol);
			rsPosto = stPosto.executeQuery();

			while (rsPosto.next())
			{
				escala = rsPosto.getString("escalda");
			}

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connSapiens, stPosto, rsPosto);
		}

		return escala;
	}

	public static List<String> pontoColaborador(Long numEmp, Long numCad, Long tipCol, GregorianCalendar datAcc)
	{
		List<String> ponto = new ArrayList<String>();
		Connection connVetorh = PersistEngine.getConnection("VETORH");
		ResultSet rsPonto = null;
		PreparedStatement stPonto = null;
		StringBuilder queryPonto = new StringBuilder();

		/*
		 * queryPonto.append(
		 * " SELECT ACC.HORACC/60 AS HORA,ISNULL(JMA.DESJMA,'')AS DESJMA , CAST(ROUND(((ACC.HORACC/60.0) - (ACC.HORACC/60)) * 60,2 ) AS INT) AS MINUTOS FROM R038HCH HCH"
		 * );
		 * queryPonto.append(" INNER JOIN R070ACC ACC ON ACC.NUMCRA = HCH.NUMCRA ");
		 * queryPonto.append(
		 * " AND DATINI =   (SELECT MAX(DATINI) FROM R038HCH HCH2 WHERE HCH2.NUMCRA = HCH.NUMCRA");
		 * queryPonto.append(" AND DATACC = ACC.DATACC )");
		 * queryPonto.append(" INNER JOIN R070ACC ENTRADA ON ENTRADA.NUMCRA = ACC.NUMCRA");
		 * queryPonto.append("	   AND ENTRADA.DATACC = ACC.DATACC ");
		 * queryPonto.append(" AND ACC.HORACC = ENTRADA.HORACC ");
		 * queryPonto.append(
		 * " AND ENTRADA.HORACC = (SELECT MIN(horacc) FROM R070ACC A where A.numcra = ACC.NUMCRA  AND A.DATACC = ACC.DATACC AND A.TIPACC = 100)"
		 * );
		 * queryPonto.append(" LEFT JOIN R076JMA JMA ON JMA.CODJMA = ACC.USU_CODJMA ");
		 * queryPonto.append(
		 * " WHERE HCH.NUMCAD = ? AND HCH.NUMEMP = ? AND HCH.TIPCOL = ? AND ACC.DATACC = ? AND ACC.TIPACC = 100"
		 * );
		 * queryPonto.append(" UNION ");
		 * queryPonto.append(
		 * " SELECT ACC.HORACC/60 AS HORA,ISNULL(JMA.DESJMA,'')AS DESJMA , CAST(ROUND(((ACC.HORACC/60.0) - (ACC.HORACC/60)) * 60,2 ) AS INT) FROM R038HCH HCH"
		 * );
		 * queryPonto.append(" INNER JOIN R070ACC ACC ON ACC.NUMCRA = HCH.NUMCRA ");
		 * queryPonto.append(
		 * " AND DATINI =   (SELECT MAX(DATINI) FROM R038HCH HCH2 WHERE HCH2.NUMCRA = HCH.NUMCRA");
		 * queryPonto.append(" AND DATACC = ACC.DATACC )	");
		 * queryPonto.append(" INNER JOIN R070ACC SAIDA ON SAIDA.NUMCRA = ACC.NUMCRA");
		 * queryPonto.append(" AND SAIDA.DATACC = ACC.DATACC ");
		 * queryPonto.append(" AND ACC.HORACC = SAIDA.HORACC ");
		 * queryPonto.append(
		 * " AND SAIDA.HORACC = (SELECT MAX(horacc) FROM R070ACC A where A.numcra = ACC.NUMCRA AND a.DATACC = ACC.DATACC AND a.TIPACC = 100)"
		 * );
		 * queryPonto.append(" LEFT JOIN R076JMA JMA ON JMA.CODJMA = ACC.USU_CODJMA ");
		 * queryPonto.append(
		 * " WHERE HCH.NUMCAD = ? AND HCH.NUMEMP = ? AND HCH.TIPCOL = ? AND ACC.DATACC = ? AND ACC.TIPACC = 100"
		 * );
		 */

		queryPonto.append(" SELECT ACC.HORACC as HorasSoma,ACC.HORACC/60 AS HORA,CAST(ROUND(((ACC.HORACC/60.0) - (ACC.HORACC/60)) * 60,2 ) AS INT) AS MINUTOS,acc.DatAcc, DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) as data, acc.dirAcc, acc.codrlg, fun.numcad, fun.numemp, fun.tipcol, jma.DesJMa, acc.usu_obsjma, acc.TipAcc, tac.DesTAc, fun.NumCra, acc.oriacc, ");
		queryPonto.append(" (select cal.perref from dbo.R044CAL cal where cal.NumEmp = 18 and tipcal = 11 and acc.DatAcc >= cal.iniapu and acc.DatAcc <= cal.fimapu) as cpt ");
		queryPonto.append(" FROM R070ACC acc ");
		queryPonto.append(" inner join R070TAC tac on (acc.tipacc = tac.TipAcc)");
		queryPonto.append(" left join r076jma jma on jma.CodJMa = acc.usu_codjma");
		queryPonto.append(" INNER JOIN R038HCH hch ON  hch.NumCra = acc.NumCra");
		queryPonto.append(" INNER JOIN R034FUN fun ON fun.NumEmp = hch.NumEmp AND fun.NumCad = hch.NumCad AND fun.TipCol = hch.TipCol");
		queryPonto.append(" WHERE fun.numcad = ? and fun.numemp = ? and fun.tipcol = ? AND acc.DatAcc = ? AND acc.UsoMar = 2 AND ( (acc.CodPlt = 2 and acc.TipAcc = 100)");
		queryPonto.append(" OR (acc.codplt = 0 and oriacc = 'R')	or (acc.TipAcc = 1 and acc.oriacc = 'R')) ");
		queryPonto.append(" GROUP BY acc.DatAcc, acc.HorAcc, acc.dirAcc, acc.codrlg, fun.numcad, fun.numemp, fun.tipcol, jma.DesJMa, acc.usu_obsjma, acc.TipAcc, tac.DesTAc, fun.NumCra, acc.oriacc");
		queryPonto.append(" ORDER BY DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) ");
		try
		{

			stPonto = connVetorh.prepareStatement(queryPonto.toString());
			stPonto.setLong(1, numCad);
			stPonto.setLong(2, numEmp);
			stPonto.setLong(3, tipCol);
			stPonto.setString(4, retornaDataFormatoSapiensEVetorh(datAcc));
			rsPonto = stPonto.executeQuery();

			String hora = " ";
			String minutos = " ";
			long horIniSum = 0L;
			long horFimSum = 0L;

			while (rsPonto.next())
			{
				if (horIniSum <= horFimSum)
				{
					horIniSum += rsPonto.getLong("HorasSoma");
				}
				else
				{
					horFimSum += rsPonto.getLong("HorasSoma");
				}

				hora = "";
				minutos = "";

				long horaTotalAcc = rsPonto.getLong("HorasSoma");
				long horaDia = horaTotalAcc / 60;
				double minutoDia = ((((horaTotalAcc / 60.0) - (horaTotalAcc / 60)) * 10) / 10.0 * 60);

				if (rsPonto.getLong("HORA") < 10)
				{
					hora = "0" + horaDia;
				}
				else
				{
					hora = String.valueOf(horaDia);
				}

				if (rsPonto.getLong("MINUTOS") < 10)
				{
					minutos = "0" + ((long) minutoDia);
				}
				else
				{
					minutos = String.valueOf((long) minutoDia);
				}

				ponto.add(hora + ":" + minutos);
				//				String desjma = rsPonto.getString("DESJMA");
				//
				//				if ((desjma != null) && (!desjma.equals("")))
				//				{
				//					ponto.add(rsPonto.getString("DESJMA"));
				//				}
			}

			if (ponto.size() > 0)
			{
				if (ponto.size() % 2 == 0)
				{
					long horTotal = horFimSum - horIniSum;
					long horaDia = horTotal / 60;
					double minutoDia = ((((horTotal / 60.0) - (horTotal / 60)) * 10) / 10.0 * 60);

					String minutosString = "";
					if (minutoDia < 10)
					{
						minutosString = "0" + minutoDia;
					}
					else
					{
						minutosString = String.valueOf(minutoDia);
					}

					System.out.println("horaDia : " + horaDia + " - minutos : " + minutosString);
					ponto.add("Total: " + horaDia + ";" + (long) minutoDia);
				}
				else
				{
					ponto.add("Total: 0;0");
				}
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(connVetorh, stPonto, rsPonto);
		}

		return ponto;
	}

	public static String retornaDataFormatoSapiensEVetorh(GregorianCalendar data)
	{
		String novaData = "";

		if (data == null)
		{
			/*
			 * 31/12/1900 o sapiens\Vetorh entende essa data como vazia
			 */
			data = new GregorianCalendar();
			data.set(data.YEAR, 1900);
			data.set(data.MONTH, 11);
			data.set(data.DAY_OF_MONTH, 31);
			data.set(Calendar.HOUR_OF_DAY, 00);
			data.set(Calendar.MINUTE, 00);
			data.set(Calendar.SECOND, 00);
		}

		if (data != null)
		{
			novaData = NeoUtils.safeDateFormat(data, "yyyy-MM-dd");
			novaData = novaData + " 00:00:00";
		}

		return novaData;
	}

	public static String enviaEmail(GregorianCalendar datRef) throws EmailException
	{
		HtmlEmail noUserEmail = new HtmlEmail();
		noUserEmail.setCharset("ISO-8859-1");
		try
		{
			String dataFormat = retornaDataFormatoSapiensEVetorh(datRef);
			dataFormat = dataFormat.substring(8, 10) + "/" + dataFormat.substring(5, 7) + "/" + dataFormat.substring(0, 4);
			File arqRelatorio = getRelatorioMedicao(dataFormat);
			NeoStorage neoStorage = NeoStorage.getDefault();
			NeoFile neoFile = neoStorage.copy(arqRelatorio);
			neoFile.setName("Relatorio_Medicao.pdf");
			List<String> finale = new ArrayList<String>();
			Map<String, Object> paramMap = new HashMap<String, Object>();
			finale.add(((Long) neoFile.getNeoId()).toString());
			paramMap.put("attachList", finale);
			List<NeoObject> objs = PersistEngine.getObjects(AdapterUtils.getEntityClass("emailMedicaoDeContratos"));
			String destinatario = "emailautomatico@orsegups.com.br";
			String separador = ";";
			if (objs.size() > 0)
			{
				for (NeoObject neoObject : objs)
				{
					EntityWrapper medWrapper = new EntityWrapper(neoObject);
					destinatario += separador + medWrapper.findValue("email");
				}
			}

			FusionRuntime.getInstance().getMailEngine().sendEMail(destinatario, "/MedicaoDeContratos/emailRelatorioMedicaoDeContratos.jsp", paramMap);
			noUserEmail.send();
		}
		catch (EmailException e)
		{
			e.printStackTrace();
		}

		return null;
	}

	public static File getRelatorioMedicao(String diaRef)
	{
		File fileProposta = null;
		Connection fusion = PersistEngine.getConnection("");

		try
		{
			String sql = " select contrato.codCli,contrato.codEmp,contrato.codReg,contrato.diaRef,contrato.nomEmp,contrato.nomReg,contrato.nomRes,contrato.numCtr,contrato.nomCli, posto.carPos,posto.codCcu,posto.nomloc,posto.numPos,posto.numVgs,"
					+ " posto.escpos,posto.escpos, colaborador.dataAdm,colaborador.horEnt,colaborador.horSai,colaborador.nomCar,colaborador.nomFun,colaborador.nomSit,colaborador.numCad,  "
					+ " desesc, colaborador.codjmaEntrada, colaborador.codjmaSaida, consideracoes.obsCon,consideracoes.datadm,consideracoes.horfim,consideracoes.horini,consideracoes.numcad as numcadcob,"
					+ " consideracoes.sitafa,consideracoes.titred ,consideracoes.nomFunCob, consideracoes.desEscCob, consideracoes.codjmaEntradaCob, consideracoes.codjmaSaidaCob, consideracoes.obsafa,colaborador.totalHorastrabalhadas,colaborador.totalMinutosTrabalhados"
					+ " from d_medContratos contrato left join D_medContratos_medLisPos contrato_posto on contrato_posto.D_medContratos_neoId = contrato.neoId left join d_medPostos posto on posto.neoId = contrato_posto.medLisPos_neoId left join D_medPostos_lisCol posto_colaborador on posto_colaborador.D_medPostos_neoId = posto.neoId left join d_medColaboradoresDia colaborador on colaborador.neoId = posto_colaborador.lisCol_neoId left join D_medColaboradoresDia_medObs colaborador_consideracoes on colaborador_consideracoes.D_medColaboradoresDia_neoId =  colaborador.neoId left join d_medobs consideracoes on consideracoes.neoId = colaborador_consideracoes.medObs_neoId";

			if (!diaRef.equals("") || (diaRef != null))
			{
				sql = sql + " WHERE diaRef ='" + diaRef + "' ORDER BY codCli, nomCli, numCtr,nomloc,nomFun";
			}
			InputStream is = null;
			String path = "";
			fileProposta = null;
			try
			{
				// ...files/relatorios
				path = NeoStorage.getDefault().getPath() + File.separator + "report" + File.separator + "relMedicaoContratosDiario.jasper";
				is = new BufferedInputStream(new FileInputStream(path));

				HashMap<String, Object> parametros = new HashMap<String, Object>();
				parametros.put("sql", sql);

				if (parametros != null)
				{

					File file = File.createTempFile("Medicao", ".pdf");
					file.deleteOnExit();

					JasperPrint impressao = JasperFillManager.fillReport(is, parametros, fusion);
					if (impressao != null && file != null)
					{
						JasperExportManager.exportReportToPdfFile(impressao, file.getAbsolutePath());
						return file;
					}
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				fusion.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();

			}
		}
		return fileProposta;
	}
}