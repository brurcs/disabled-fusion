package com.neomind.fusion.custom.orsegups.processoJuridico.adapter;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.util.NeoDateUtils;

import edu.emory.mathcs.backport.java.util.Collections;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.ConverterListaAnexo
public class ConverterListaAnexo extends StringConverter
{
	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin){
		NeoObject objPrincipal = field.getForm().getObject();
		EntityWrapper wPrincipal = new EntityWrapper(objPrincipal);
		@SuppressWarnings("unchecked")
		List<NeoObject> listaRegistroArquivos = (List<NeoObject>) wPrincipal.getValue("j002RegistroArquivo");
		StringBuilder retorno = new StringBuilder();
		Collections.sort(listaRegistroArquivos, new ListaAnexoComparator());
		if (listaRegistroArquivos != null && listaRegistroArquivos.size() > 0)
		{

			retorno.append("		<table  class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">");
			retorno.append("			<tr style=\"cursor: auto\">");
			retorno.append("				<th style=\"cursor: auto; white-space: normal\">Quem Anexou</th>");
			retorno.append("				<th style=\"cursor: auto\">Data do Anexo</th>");
			retorno.append("				<th style=\"cursor: auto\">Titulo</th>");
			retorno.append("				<th style=\"cursor: auto; white-space: normal\">Anexo</th>");
			retorno.append("			</tr>");
			retorno.append("			<tbody>");

			for (NeoObject hisArq : listaRegistroArquivos)
			{
				EntityWrapper wHisArq = new EntityWrapper(hisArq);
				retorno.append("<td>" + wHisArq.getValue("respAnexo") + "</td>");
				retorno.append("<td>" + NeoDateUtils.safeDateFormat((GregorianCalendar) wHisArq.getValue("dtAnexo"), "dd/MM/yyyy HH:mm:ss") + "</td>");
				retorno.append("<td>" + wHisArq.getValue("titulo") + "</td>");
				NeoFile file = (NeoFile) wHisArq.getValue("anexo");
				if (file != null)
				{
					retorno.append("	<td><a style=\"cursor:pointer;\" target='_blank' href=\"" + PortalUtil.getBaseURL() + "file/download/" + file.getNeoId() + "\"'; ");
					retorno.append("			><img src=\"imagens/icones_final/document_search_16x16-trans.png\" align=\"absmiddle\" alt=" + file.getName() + "> ");
					retorno.append("			" + file.getName());
					retorno.append("</a></td>");
					}
				else
				{
					retorno.append("	<td></td> ");
				}

				retorno.append("		</tr>");

			}

			retorno.append("		</tbody>");
			retorno.append("		</table>");
		}
		return retorno.toString();
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}
}
