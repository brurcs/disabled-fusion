package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;

public class RotinaAbreTarefaSimplesDocumentosRejeitadosAutoStory implements CustomJobAdapter
{
	 List<String> listcolaboradorAIT = null;
	 List<String> listCaminho = null;
	@Override
	public void execute(CustomJobContext arg0)
	{
		try
		{
			listCaminho = buscaImagens();
			
			if (listCaminho != null && listCaminho.size() > 0 && !listCaminho.isEmpty()) {
				boolean retorno = backupDocumentoPasta(listCaminho);
				if (!retorno) {	
					for (String ait : listCaminho) {
					
						GregorianCalendar dataPr = new GregorianCalendar();
						GregorianCalendar dataPrazo = OrsegupsUtils.getSpecificWorkDay(dataPr, 5L);
						String solicitante = "jairo";
						String titulo = "Reprocessar Documento GED";
						
						String executor = "";

						NeoPaper responsavel = new NeoPaper();
						responsavel = OrsegupsUtils.getPaper("responsavelDocumentoRejeitadosAutoStory");
						if (responsavel != null && responsavel.getAllUsers() != null && !responsavel.getAllUsers().isEmpty())
						{
							for (NeoUser user : responsavel.getUsers())
							{
								executor = user.getCode();
								break;
							}
						}
								
						StringBuilder textoFinal = new StringBuilder();
						textoFinal = new StringBuilder();

						textoFinal.append("Prezado(a), </br></br>");
						textoFinal.append("Segue em anexo o documento rejeitado pelo AutoStory digitalizado pelo QuickCapture, para que o mesmo possa ser processado novamente pela ferramenta AutoCapturer.</br>");
						
						File file = new File(ait);
						NeoFile newFile = new NeoFile();
						newFile.setName(file.getName());
						newFile.setStorage(NeoStorage.getDefault());
						PersistEngine.persist(newFile);
						PersistEngine.evict(newFile);
						OutputStream outi = newFile.getOutputStream();
						InputStream in = null;
						
						try
						{
							in = new BufferedInputStream(new FileInputStream(file));
							NeoStorage.copy(in, outi);
							newFile.setOrigin(NeoFile.Origin.TEMPLATE);
						}
						catch (FileNotFoundException e)
						{
							e.printStackTrace();
						}
						
						IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
						String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, textoFinal.toString(), "1", "sim", dataPrazo, newFile);
						System.out.println("Nº Tarefa: "+tarefa);
					}
					
					/*Boolean retornoDelete = apagarArquivosPdf();
					if (!retornoDelete) {
						enviarEmail(" Erro ao Efetura Exclusão dos Arquivos na pasta Reject");
					}*/
				} else {
					enviarEmail(" Erro ao Efetura Backup em Pasta");
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public List<String> buscaImagens () throws FileNotFoundException {
		 File f = null;
	      File[] paths;
	      
	      try{      
	         // create new file
	         f = new File("\\\\ssooap07\\ProjetoAutoStoreGED\\QC\\REJECT\\");
	         System.out.println("Teste Francisco Arquivo "+f.toString());
	         // create new filename filter
	         FilenameFilter fileNameFilter = new FilenameFilter() {
	   
	            @Override
				public boolean accept(File dir, String name) {
					try {
						if (name.lastIndexOf('.') > 0) {
							// get last index for '.' char
							int lastIndex = name.lastIndexOf('.');

							// get extension
							String str = name.substring(lastIndex);

							// match path name extension
							if (str.equals(".pdf")) {
								return true;
							}
						}
					} catch (Exception e) {
						System.out.println("Erro ao Encontrar o Arquivo PDF");
						e.printStackTrace();
					}

					return false;
				}
	         };
	         // returns pathnames for files and directory
	         paths = f.listFiles(fileNameFilter);
	         
	         listCaminho = new ArrayList<String>();
	         listcolaboradorAIT = new ArrayList<String>();
	         if (paths != null) {
	        	 for(File path:paths) {
	        		 String colaboradorAIT = path.toString();
	        		 listCaminho.add(colaboradorAIT);
	        		 
	        	 }
	        	 if (listCaminho != null && listCaminho.size() > 0 && !listCaminho.isEmpty()) { 
	        		 return listCaminho;
	        	 }
	         } else {
	        	 System.out.println("Não existe o arquivo "+paths);
	         }
	      }catch(Exception e){
	         // if any error occurs
	         e.printStackTrace();
	      }
	      return listCaminho;
	}
	
	public boolean backupDocumentoPasta(List<String> listCaminho) {
		boolean erro = false;
		String dDatHoj = new SimpleDateFormat("dd-MM-yyyy HH-mm").format(new GregorianCalendar().getTime());
		String filename = "\\\\ssoovt09\\f$\\Sistemas\\AutoStory\\QuickCapture\\Reject\\"+dDatHoj+"\\"; ///Servidor de backup documentos rejeitados
		if (!new File(filename).exists()) { // Verifica se o diretório existe.   
			(new File(filename)).mkdir();   // Cria o diretório   
		} 
		for (String ait : listCaminho) {
			try {
				// streams
				filename = "\\\\ssoovt09\\f$\\Sistemas\\AutoStory\\QuickCapture\\Reject\\"+dDatHoj+"\\"; ///Servidor de backup documentos rejeitados
				filename += ait.substring(45, ait.length());
				FileInputStream origem;
				FileOutputStream destino;
				// canais
				FileChannel fcOrigem;
				FileChannel fcDestino;

				origem = new FileInputStream(ait);
				destino = new FileOutputStream(filename);

				fcOrigem = origem.getChannel();
				fcDestino = destino.getChannel();
				// Faz a copia
				fcOrigem.transferTo(0, fcOrigem.size(), fcDestino);

				origem.close();
				destino.close();
				
			} catch (IOException e) {
				erro = true;	
				e.printStackTrace();
				return erro;
			}
		}
		return erro;
	}
	
	public boolean backupDocumentoEform(List<String> listCaminho) {
		boolean erro = false;
		for (String ait : listCaminho) {
			try {
				InstantiableEntityInfo backupEform = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("backupDocumentosRejeitadosAutoStory");
				NeoObject backEfr = backupEform.createNewInstance();
				
				File file = new File(ait);
				NeoFile newFile = new NeoFile();
				newFile.setName(file.getName());
				newFile.setStorage(NeoStorage.getDefault());
				PersistEngine.persist(newFile);
				PersistEngine.evict(newFile);
				OutputStream outi = newFile.getOutputStream();
				InputStream in = null;
				
				try
				{
					in = new BufferedInputStream(new FileInputStream(file));
					NeoStorage.copy(in, outi);
					newFile.setOrigin(NeoFile.Origin.TEMPLATE);
				}
				catch (FileNotFoundException e)
				{
					e.printStackTrace();
				}
				
				EntityWrapper backEfrWp = new EntityWrapper(backEfr);
				backEfrWp.findField("arquivoRejeitado").setValue(newFile);
				PersistEngine.persist(backEfr);
				PersistEngine.commit(true);
				
			} catch (Exception e) {
				erro = true;	
				e.printStackTrace();
				return erro;
			}
		}
		return erro;
	}
	
	public Boolean apagarArquivosPdf()  {
		boolean excluir = false;
		File folder = new File("\\\\ssooap07\\ProjetoAutoStoreGED\\QC\\REJECT\\");
		if (folder.isDirectory()) {  
		    File[] sun = folder.listFiles();  
		    for (File toDelete : sun) {  
		        toDelete.delete();
		        excluir = true;
		    }  
		}
		return excluir;  
	}
	
	public void enviarEmail(String mensagem) {
		MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());
		try {
			String dDatHoj = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new GregorianCalendar().getTime());
			MailSettings mailClone = new MailSettings();
			mailClone.setMinutesInterval(settings.getMinutesInterval());
			mailClone.setFromEMail(settings.getFromEMail());
			mailClone.setFromName(settings.getFromName());
			mailClone.setRenderServer(settings.getRenderServer());
			mailClone.setSecurityFromEmail(settings.getSecurityFromEmail());
			mailClone.setSecurityCopyEmail(settings.getSecurityCopyEmail());
			mailClone.setSmtpSettings(settings.getSmtpSettings());
			mailClone.setPort(settings.getPort());
			mailClone.setSmtpServer(settings.getSmtpServer());
			mailClone.setEnabled(settings.isEnabled());
	
			mailClone.setFromEMail("francisco.junior@orsegups.com.br");
	
			mailClone.setFromName("Orsegups Partipações S.A.");
			if (mailClone != null && mailClone.isEnabled())
			{
				
				HtmlEmail noUserEmail = new HtmlEmail();
				StringBuilder noUserMsg = new StringBuilder();
	
				Calendar saudacao = Calendar.getInstance();
				String saudacaoEMail = "";
				if (saudacao.get(Calendar.HOUR_OF_DAY) >= 6 && saudacao.get(Calendar.HOUR_OF_DAY) <= 12)
				{
					saudacaoEMail = "Bom dia, ";
				}
				else if (saudacao.get(Calendar.HOUR_OF_DAY) > 12 && saudacao.get(Calendar.HOUR_OF_DAY) <= 18)
				{
					saudacaoEMail = "Boa tarde, ";
				}
				else
				{
					saudacaoEMail = "Boa noite, ";
				}
				
				noUserEmail.setCharset("ISO-8859-1");
				noUserEmail.addTo("francisco.junior@orsegups.com.br");
	
				noUserEmail.setFrom("francisco.junior@orsegups.com.br");
				noUserEmail.setSubject("(Não Responda - "+mensagem+", na Data "+dDatHoj);
	
				noUserMsg.append("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></head><body>");
				noUserMsg.append("<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"left\" bgcolor=\"#ffffff\">");
				noUserMsg.append("          <tbody>");
				noUserMsg.append("			<td>");
				noUserMsg.append("			<strong>" + saudacaoEMail+mensagem+"</strong>");
				noUserMsg.append("			</td>");
				noUserMsg.append("     </tbody></table>");
				noUserMsg.append("</body></html>");
				noUserEmail.setHtmlMsg(noUserMsg.toString());
				noUserEmail.setContent(noUserMsg.toString(), "text/html");
				mailClone.applyConfig(noUserEmail);
	
				noUserEmail.send();
			}
		} catch (EmailException e) {
			e.printStackTrace();
		}
	}
}

