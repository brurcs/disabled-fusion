package com.neomind.fusion.custom.orsegups.contract;

import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class ContratoHistoricoConferencia implements AdapterInterface{
	
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		GregorianCalendar dataAtual = new GregorianCalendar();
		NeoUser usuarioAtual = PortalUtil.getCurrentUser();
		
		Boolean contratoAssinado = (Boolean) processEntity.findValue("contratoEntregueAssinado");
		String observacaoExecutivo = (String) processEntity.findValue("observacoesExecutivo");
		String observacaoCerec = (String) processEntity.findValue("observacaoCerec");
		
		NeoObject objHistorico = AdapterUtils.createNewEntityInstance("FGCHistoricoConferenciaContrato");
		
		EntityWrapper ewHistorico = new EntityWrapper(objHistorico);
		
		ewHistorico.findField("dataHistorico").setValue(dataAtual);
		ewHistorico.findField("usuario").setValue(usuarioAtual);
		ewHistorico.findField("contratoEntregueAssinado").setValue(contratoAssinado);
		ewHistorico.findField("observacaoExecutivo").setValue(observacaoExecutivo);
		ewHistorico.findField("observacaoCerec").setValue(observacaoCerec);
				
		PersistEngine.persist(objHistorico);
		
		processEntity.findField("historicoConferencia").addValue(objHistorico);
		
		//processEntity.findField("observacoesExecutivo").setValue("");
		processEntity.findField("observacaoCerec").setValue("");
	}

	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}
}
