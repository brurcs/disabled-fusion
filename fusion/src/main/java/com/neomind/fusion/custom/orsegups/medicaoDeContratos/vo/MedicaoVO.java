package com.neomind.fusion.custom.orsegups.medicaoDeContratos.vo;

import java.math.BigDecimal;

public class MedicaoVO
{
	public String cliente;
	public long contrato;
	public String numOfi;
	public long codEmpresaContratada;
	public String empresaContratada;
	public String responsavel;
	public String periodo;
	public long codRegional;
	public String regional;
	public String alertas;
	public boolean gerCms;
	public BigDecimal vlrCtr;
	public BigDecimal vlrPar;
	public long neoId;
	public String imgAlertas;
	public String linkVisualizar;
	
	public String getLinkVisualizar()
	{
		return linkVisualizar;
	}

	public void setLinkVisualizar(String linkVisualizar)
	{
		this.linkVisualizar = linkVisualizar;
	}

	public String getImgAlertas()
	{
		return imgAlertas;
	}

	public void setImgAlertas(String imgAlertas)
	{
		this.imgAlertas = imgAlertas;
	}

	public long getNeoId()
	{
		return neoId;
	}

	public void setNeoId(long neoId)
	{
		this.neoId = neoId;
	}

	public String getCliente()
	{
		return cliente;
	}

	public void setCliente(String cliente)
	{
		this.cliente = cliente;
	}

	public long getContrato()
	{
		return contrato;
	}

	public void setContrato(long contrato)
	{
		this.contrato = contrato;
	}

	public String getNumOfi()
	{
		return numOfi;
	}

	public void setNumOfi(String numOfi)
	{
		this.numOfi = numOfi;
	}

	public long getCodEmpresaContratada()
	{
		return codEmpresaContratada;
	}

	public void setCodEmpresaContratada(long codEmpresaContratada)
	{
		this.codEmpresaContratada = codEmpresaContratada;
	}

	public String getEmpresaContratada()
	{
		return empresaContratada;
	}

	public void setEmpresaContratada(String empresaContratada)
	{
		this.empresaContratada = empresaContratada;
	}

	public String getResponsavel()
	{
		return responsavel;
	}

	public void setResponsavel(String responsavel)
	{
		this.responsavel = responsavel;
	}

	public String getPeriodo()
	{
		return periodo;
	}

	public void setPeriodo(String periodo)
	{
		this.periodo = periodo;
	}

	public long getCodRegional()
	{
		return codRegional;
	}

	public void setCodRegional(long codRegional)
	{
		this.codRegional = codRegional;
	}

	public String getRegional()
	{
		return regional;
	}

	public void setRegional(String regional)
	{
		this.regional = regional;
	}

	public String getAlertas()
	{
		return alertas;
	}

	public void setAlertas(String alertas)
	{
		this.alertas = alertas;
	}

	public boolean isGerCms()
	{
		return gerCms;
	}

	public void setGerCms(boolean gerCms)
	{
		this.gerCms = gerCms;
	}

	public BigDecimal getVlrCtr()
	{
		return vlrCtr;
	}

	public void setVlrCtr(BigDecimal vlrCtr)
	{
		this.vlrCtr = vlrCtr;
	}

	public BigDecimal getVlrPar()
	{
		return vlrPar;
	}

	public void setVlrPar(BigDecimal vlrPar)
	{
		this.vlrPar = vlrPar;
	}

}
