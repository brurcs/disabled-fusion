package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.GregorianCalendar;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.ait.controller.ProdutividadeAitController;
import com.neomind.fusion.custom.orsegups.ait.vo.AtendimentoValorVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class CPAInsereLancamento implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(CPAInsereLancamento.class);

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		String mensagem = "Por favor, contatar o administrador do sistema!";

		try
		{
			InstantiableEntityInfo registroAtividade = AdapterUtils.getInstantiableEntityInfo("CPARegistrodeAtividade");
			NeoObject registro = registroAtividade.createNewInstance();
			EntityWrapper wRegistro = new EntityWrapper(registro);

			ProdutividadeAitController controller = new ProdutividadeAitController();
			Long cdViatura = (Long) processEntity.findValue("cdViatura");
			String competencia = (String) processEntity.findValue("competencia");

			if (cdViatura != null)
			{	
			    	HashMap<String, Long> dadosColaborador = controller.retornaDadosVinculoViaturaColaborador(cdViatura);
				
			    	Long empresa = dadosColaborador.get("numEmp");
				Long numCad = dadosColaborador.get("numCad");
				
				if (empresa != null)
				{

					Long codCal = controller.retornaCodigoDoCalculo(empresa, competencia);

					if (codCal != null)
					{
						AtendimentoValorVO atendimentoValorVO = controller.getValores(cdViatura, competencia);

						if (atendimentoValorVO != null)
						{
							Double valor = atendimentoValorVO.getTotalValorATD() + atendimentoValorVO.getTotalValorDSL();
							boolean flag = controller.insereLancamentoProdutividade(codCal, valor, empresa, numCad);
							if (!flag)
							{
								mensagem = "Erro! Não foi possível fazer o lançamento de produtividade. Ocorreu uma inconsistência durante a inserção.";
								throw new WorkflowException(mensagem);
							}
							else
							{

								NeoPaper papel = new NeoPaper();
								papel = OrsegupsUtils.getPaper("sistemaFusionPaper");
								NeoUser usuarioResponsavel = new NeoUser();
								if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
								{
									for (NeoUser user : papel.getUsers())
									{
										usuarioResponsavel = user;
										break;
									}
								}
								DecimalFormat formatoReal = new DecimalFormat("#,##0.00");
								processEntity.findField("valor").setValue(BigDecimal.valueOf(valor));
								wRegistro.findField("responsavel").setValue(usuarioResponsavel);
								wRegistro.findField("dataCPAInicial").setValue(new GregorianCalendar());
								wRegistro.findField("dataCPAAcao").setValue(new GregorianCalendar());

								String descricao = "";
								descricao = "<strong>Sucesso ao realizar o lançamento de produtividade : </strong><br>";
								descricao = descricao + "<strong>Quantidade:</strong> " + atendimentoValorVO.getCount() + "<br>";
								descricao = descricao + "<strong>Sub total deslocamento:</strong> " + atendimentoValorVO.getSubTotalDeslocamento() + "<br>";
								descricao = descricao + "<strong>Sub total atendimento:</strong> " + atendimentoValorVO.getSubTotalAtendimento() + "<br>";
								descricao = descricao + "<strong>Valor deslocamento:</strong> " + NeoUtils.safeFormat(formatoReal, atendimentoValorVO.getTotalValorDSL()) + "<br>";
								descricao = descricao + "<strong>Valor atendimento:</strong> " + NeoUtils.safeFormat(formatoReal, atendimentoValorVO.getTotalValorATD()) + "<br>";
								descricao = descricao + "<strong>Valor total:</strong> " + NeoUtils.safeFormat(formatoReal, atendimentoValorVO.getTotalValorDSL() + atendimentoValorVO.getTotalValorATD()) + "<br>";
								descricao = descricao + "<br><br>";
								descricao = descricao.replaceAll("'", "");
								descricao = descricao.replaceAll("\"", "");

								wRegistro.findField("atividadeCPAAnterior").setValue("Sucesso ao realizar o lançamento de produtividade. " + valor);
								wRegistro.findField("descricao").setValue(descricao);
								wRegistro.findField("prazo").setValue(processEntity.findValue("prazo"));

							}
						}
						else
						{
							mensagem = "Erro! Não foi possível fazer o lançamento de produtividade. Valor da produtividade não foi encontrado.";
							throw new WorkflowException(mensagem);
						}
					}
					else
					{
						mensagem = "Erro! Não foi possível fazer o lançamento de produtividade. Código do Cálculo inválido.";
						throw new WorkflowException(mensagem);
					}
				}
				else
				{
					mensagem = "Erro! Não foi possível fazer o lançamento de produtividade. Empresa inválida.";
					throw new WorkflowException(mensagem);
				}
			}
			else
			{
				mensagem = "Erro! Não foi possível fazer o lançamento de produtividade. Viatura inválidA.";
				throw new WorkflowException(mensagem);
			}
			PersistEngine.persist(registro);
			processEntity.findField("registroAtividadesCPA").addValue(registro);
		}
		catch (WorkflowException e)
		{
			log.error(mensagem);
			e.printStackTrace();
			throw new WorkflowException(mensagem);
		}
		catch (Exception ex)
		{
			log.error(mensagem);
			ex.printStackTrace();
			throw new WorkflowException(mensagem);
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}

}
