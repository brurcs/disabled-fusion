package com.neomind.fusion.custom.orsegups.ti;

import java.util.Date;



public class TarefasVO  implements Comparable<TarefasVO>
{
	private Long wfProcess;
	private Long codigoTarefa;
	private String nomeCompleto;
	private String tituloTarefa;
	private String descricaoTarefa;
	private String dataAberturaTarefa;
	private String dataUltimaAlteracao;
	private String prazoTarefa;
	private String tipoTarefa;
	private Long posicaoTarefa;
	private String usuarioCode;
	private String situacaoTarefa;
	private Long modelo;
	private Long codigoSolicitante;
	private String solicitante;
	private String grupoSolicitante;
	private Long codigoGrupoSolicitante;
	private Date startDate;
	
	
	public Long getCodigoSolicitante()
	{
		return codigoSolicitante;
	}
	public void setCodigoSolicitante(Long codigoSolicitante)
	{
		this.codigoSolicitante = codigoSolicitante;
	}
	public String getGrupoSolicitante()
	{
		return grupoSolicitante;
	}
	public void setGrupoSolicitante(String grupoSolicitante)
	{
		this.grupoSolicitante = grupoSolicitante;
	}
	public Long getCodigoGrupoSolicitante()
	{
		return codigoGrupoSolicitante;
	}
	public void setCodigoGrupoSolicitante(Long codigoGrupoSolicitante)
	{
		this.codigoGrupoSolicitante = codigoGrupoSolicitante;
	}
	public String getTipoTarefa()
	{
		return tipoTarefa;
	}
	public void setTipoTarefa(String tipoTarefa)
	{
		this.tipoTarefa = tipoTarefa;
	}
	public Long getPosicaoTarefa()
	{
		return posicaoTarefa;
	}
	public void setPosicaoTarefa(Long posicaoTarefa)
	{
		this.posicaoTarefa = posicaoTarefa;
	}
	public Long getWfProcess()
	{
		return wfProcess;
	}
	public void setWfProcess(Long wfProcess)
	{
		this.wfProcess = wfProcess;
	}
	public Long getCodigoTarefa()
	{
		return codigoTarefa;
	}
	public void setCodigoTarefa(Long codigoTarefa)
	{
		this.codigoTarefa = codigoTarefa;
	}
	public String getNomeCompleto()
	{
		return nomeCompleto;
	}
	public void setNomeCompleto(String nomeCompleto)
	{
		this.nomeCompleto = nomeCompleto;
	}
	public String getTituloTarefa()
	{
		return tituloTarefa;
	}
	public void setTituloTarefa(String tituloTarefa)
	{
		this.tituloTarefa = tituloTarefa;
	}
	
	public String getPrazoTarefa()
	{
		return prazoTarefa;
	}
	public void setPrazoTarefa(String prazoTarefa)
	{
		this.prazoTarefa = prazoTarefa;
	}
	public String getDataAberturaTarefa()
	{
		return dataAberturaTarefa;
	}
	public void setDataAberturaTarefa(String dataAberturaTarefa)
	{
		this.dataAberturaTarefa = dataAberturaTarefa;
	}
	
	public String getDataUltimaAlteracao()
	{
		return dataUltimaAlteracao;
	}
	public void setDataUltimaAlteracao(String dataUltimaAlteracao)
	{
		this.dataUltimaAlteracao = dataUltimaAlteracao;
	}
	
	public String getDescricaoTarefa()
	{
		return descricaoTarefa;
	}
	public void setDescricaoTarefa(String descricaoTarefa)
	{
		this.descricaoTarefa = descricaoTarefa;
	}
	@Override
	public int compareTo(TarefasVO o)
	{
		if(this.getStartDate()!= null && o.getStartDate() != null)
		{
		if(this.getStartDate().after(o.getStartDate()))
			return 1;
		}
		// TODO Auto-generated method stub
		return  -1;
	}
	public String getUsuarioCode()
	{
		return usuarioCode;
	}
	public void setUsuarioCode(String usuarioCode)
	{
		this.usuarioCode = usuarioCode;
	}
	public String getSituacaoTarefa()
	{
		return situacaoTarefa;
	}
	public void setSituacaoTarefa(String situacaoTarefa)
	{
		this.situacaoTarefa = situacaoTarefa;
	}
	public Long getModelo()
	{
		return modelo;
	}
	public void setModelo(Long modelo)
	{
		this.modelo = modelo;
	}
	public String getSolicitante()
	{
		return solicitante;
	}
	public void setSolicitante(String solicitante)
	{
		this.solicitante = solicitante;
	}
	public Date getStartDate()
	{
		return startDate;
	}
	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}
	
	
	
}
