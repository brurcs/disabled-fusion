package com.neomind.fusion.custom.orsegups.adapter;


import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class ChipGPRSInsInfPadroes implements AdapterInterface 
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		
		   Long qtdeChips = NeoUtils.safeLong(NeoUtils.safeOutputString(processEntity.findValue("quantidadeChip")));
		   
		   for(int i = 0; i < qtdeChips; i++)
		   {
			   NeoObject chip = AdapterUtils.createNewEntityInstance("TELECOMChipsGPRS");
			   EntityWrapper wChip = new EntityWrapper(chip);
			   
			   if(NeoUtils.safeIsNotNull(processEntity.findValue("postoSapiens.usu_regcvs")))
			   {
				   Long regional = (Long) processEntity.findValue("postoSapiens.usu_regcvs");
				   NeoObject reg = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("GCEscritorioRegional"), new QLEqualsFilter("codigo",regional));
				   wChip.setValue("escritorioRegional", reg);
			   }
			   
			   if(NeoUtils.safeIsNotNull(processEntity.findValue("postoSapiens.nomcli")))
			   {
				   wChip.setValue("cliente", processEntity.findValue("postoSapiens.nomcli"));
			   }
		    
			   if(NeoUtils.safeIsNotNull(processEntity.findValue("postoSapiens.desccu")))
			   {
				   wChip.setValue("lotacao", processEntity.findValue("postoSapiens.desccu"));
			   }
			   
			   if(NeoUtils.safeIsNotNull(processEntity.findValue("postoSapiens.usu_codccu")))
			   {
				   wChip.setValue("centrodecusto", processEntity.findValue("postoSapiens.usu_codccu"));
			   }
			   
			   //situacaoPosto
			   PersistEngine.persist(chip);
			   processEntity.findField("chipsGPRS").addValue(chip);
		   }
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}
}
