package com.neomind.fusion.custom.orsegups.fap.dto;

import java.util.List;

import com.neomind.fusion.common.NeoObject;


public class TaticoDTO extends AplicacaoTecnicoTaticoDTO {
   
    String rota = null;
    String taticoResponsavel = null;

    public NeoObject getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(NeoObject fornecedor) {
        this.fornecedor = fornecedor;
    }

    public Long getCodTecnico() {
        return codTecnico;
    }

    public void setCodTecnico(Long codTecnico) {
        this.codTecnico = codTecnico;
    }

    public String getRota() {
        return rota;
    }

    public void setRota(String rota) {
        this.rota = rota;
    }

    public String getTaticoResponsavel() {
        return taticoResponsavel;
    }

    public void setTaticoResponsavel(String tecnicoResponsavel) {
        this.taticoResponsavel = tecnicoResponsavel;
    }

    public List<NeoObject> getListaCcu() {
        return listaCcu;
    }
    public void setListaCcu(List<NeoObject> listaCcu) {
        this.listaCcu = listaCcu;
    }
    
    public List<HistoricoFapDTO> getHistorico() {
        return historicoFAP;
    }
    
    public void setHistorico(List<HistoricoFapDTO> historico){
	this.historicoFAP = historico;
    }
    
    public List<NeoObject> getHistoricoContasSigma() {
        return historicoContasSigma;
    }
    
    public void setHistoricoContasSigma(List<NeoObject> historicoContasSigma){
	this.historicoContasSigma = historicoContasSigma;
    }
    
}
