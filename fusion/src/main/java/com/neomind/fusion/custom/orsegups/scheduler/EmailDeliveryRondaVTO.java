package com.neomind.fusion.custom.orsegups.scheduler;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.HtmlEmail;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.integracaoMobile.IntegracaoPortalMobile;
import com.neomind.fusion.custom.orsegups.objRatMobile.ObjRatMobile;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsEmailUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class EmailDeliveryRondaVTO implements CustomJobAdapter {
    private static final Log log = LogFactory.getLog(EmailDeliveryRondaVTO.class);

    @SuppressWarnings({ "unchecked", "static-access", "deprecation" })
    @Override
    public void execute(CustomJobContext arg0) {
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
	int adicionados = 0;
	System.out.println("E-Mail AIT Inicio execução em: " + dateFormat.format(new GregorianCalendar().getTime()));
	Long key = GregorianCalendar.getInstance().getTimeInMillis();

	InstantiableEntityInfo infoHis = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("AITEmailAutomaticoExibicaoCliente");
	InstantiableEntityInfo infoTec = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("AITEmailAutomaticoAIT");
	InstantiableEntityInfo infoCadAIT = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("AITEmaiCadastrolAutomaticoAITCM");
	InstantiableEntityInfo ExcecoesEmail = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("CadastroExcecoesEmails");
	InstantiableEntityInfo infoEnvioEmail = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("EmailDelivery");
	InstantiableEntityInfo infoImagens = (InstantiableEntityInfo) EntityRegister.getEntityInfo("SIGMAImagensEventoRonda");

	Connection conn = PersistEngine.getConnection("SIGMA90");
	StringBuilder strSigma = new StringBuilder();
	PreparedStatement pstm = null;

	String ultimaExecucaoRotina = ultimaExecucaoRotina();
	String ultimaExecucaoRotinaAux = ultimaExecucaoRotinaRange();

	Set<Integer> empresasNotificadas = new HashSet<Integer>();

	try {
	    // TODO XVTO = XXX7, cadastrar grupo vto
	    strSigma.append(" 	SELECT c.OBSERVACAO, vh.CD_HISTORICO, c.CD_GRUPO_CLIENTE ,c.CGCCPF,c.FANTASIA, c.RAZAO, c.ID_CENTRAL, c.PARTICAO, c.ENDERECO, cid.NOME AS NM_CIDADE, bai.NOME AS NM_BAIRRO, vh.DT_VIATURA_NO_LOCAL, c.EMAILRESP, GS.NM_DESCRICAO AS DESCRICAO_GRUPO,");
	    strSigma.append(" 	v.NM_VIATURA, REPLACE(REPLACE(REPLACE(REPLACE(hfe.NM_FRASE_EVENTO, 'NÃO LIGAR', ''), 'ROUBO', ''), '()', ''), '  ', ' ') as NM_FRASE_EVENTO, vh.TX_OBSERVACAO_FECHAMENTO, ISNULL(ma.DS_MOTIVO_ALARME, 'N/A - SEM ALTERAÇÃO') as motivo, vh.CD_EVENTO, c.CD_CLIENTE , v.CD_VIATURA,  c.NU_LATITUDE, c.NU_LONGITUDE,CONVERT(VARCHAR, CONVERT(DECIMAL(10,6), (0.0002504 * SQRT(RAND()) * SIN(2 * PI() * RAND())) + (CONVERT(DECIMAL(10,6), c.NU_LATITUDE)))) AS NU_LAT_VTR, CONVERT(VARCHAR, CONVERT(DECIMAL(10,6), ((0.0002504 * SQRT(RAND()) * COS(2 * PI() * RAND())) / COS(CONVERT(DECIMAL(10,6), c.NU_LATITUDE)) + (CONVERT(DECIMAL(10,6), c.NU_LONGITUDE))))) AS NU_LNG_VTR, c.ID_EMPRESA ");
	    strSigma.append(" 	FROM VIEW_HISTORICO vh with (nolock) ");
	    strSigma.append(" 	INNER JOIN dbCENTRAL c with (nolock) ON c.CD_CLIENTE = vh.CD_CLIENTE ");
	    strSigma.append(" 	INNER JOIN viatura v with (nolock) ON v.CD_VIATURA = vh.CD_VIATURA ");
	    strSigma.append(" 	INNER JOIN HISTORICO_FRASE_EVENTO hfe with (nolock) ON hfe.CD_FRASE_EVENTO = vh.CD_FRASE_EVENTO ");
	    strSigma.append(" 	INNER JOIN GRUPO_CLIENTE GS WITH(NOLOCK) ON C.CD_GRUPO_CLIENTE = GS.CD_GRUPO_CLIENTE ");
	    strSigma.append(" 	LEFT JOIN MOTIVO_ALARME ma with (nolock) ON ma.CD_MOTIVO_ALARME = vh.CD_MOTIVO_ALARME ");
	    strSigma.append(" 	LEFT JOIN dbCIDADE cid with (nolock) ON cid.ID_CIDADE = c.ID_CIDADE ");
	    strSigma.append(" 	LEFT JOIN dbBAIRRO bai with (nolock) ON bai.ID_CIDADE = c.ID_CIDADE AND bai.ID_BAIRRO = c.ID_BAIRRO ");
	    strSigma.append(" 	INNER JOIN [CACUPE\\SQL02].fusion_producao.DBO.X_DBGRUPOCLIENTE g WITH(NOLOCK) ON g.cd_grupo_cliente = c.CD_GRUPO_CLIENTE ");
	    strSigma.append(" 	INNER JOIN [CACUPE\\SQL02].fusion_producao.DBO.D_gruposVTO gvto WITH(NOLOCK) ON gvto.grupoCliente_neoId = g.neoId ");
	    strSigma.append(" 	WHERE vh.FG_STATUS = 4 AND C.TP_PESSOA != 2 ");
	    strSigma.append(" 	AND vh.CD_EVENTO = 'XXX7' ");
	    strSigma.append("   AND vh.DT_VIATURA_NO_LOCAL IS NOT NULL ");
	    strSigma.append(" 	AND (vh.DT_FECHAMENTO > '" + ultimaExecucaoRotina + "' OR vh.DT_FECHAMENTO > '" + ultimaExecucaoRotinaAux + "') ");
	    strSigma.append(" 	AND (vh.CD_MOTIVO_ALARME <> 36 or  vh.CD_MOTIVO_ALARME is null) ");

	    pstm = conn.prepareStatement(strSigma.toString());
	    inserirFimRotina();
	    ResultSet rs = pstm.executeQuery();

	    String calendarDDMM = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM");
	    // String calendarDM = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "d/M");

	    while (rs.next()) {

		boolean clienteComExcecao = OrsegupsEmailUtils.verificarExcecaoCNPJ(rs.getInt("CD_CLIENTE"));
		String observacao = rs.getString("OBSERVACAO") == null ? "" : rs.getString("OBSERVACAO");

		if (!clienteComExcecao) {

		    NeoObject colaborador = PersistEngine.getObject(infoTec.getEntityClass(), new QLEqualsFilter("matricula", rs.getString("CD_VIATURA")));

		    if (colaborador != null) {

			EntityWrapper wpneo = new EntityWrapper(colaborador);
			String cgcCpfPrincipal = rs.getString("CGCCPF") == null ? "" : rs.getString("CGCCPF").replaceAll("([.\\-/])", "").trim();
			String fantasia = rs.getString("FANTASIA");
			String razao = (rs.getString("RAZAO") == null ? "" : rs.getString("RAZAO"));
			String matricula = rs.getString("CD_VIATURA");
			String particao = rs.getString("PARTICAO");
			String endereco = (rs.getString("ENDERECO") == null ? "" : rs.getString("ENDERECO"));
			String cidade = (rs.getString("NM_CIDADE") == null ? "" : rs.getString("NM_CIDADE"));
			String bairro = (rs.getString("NM_BAIRRO") == null ? "" : rs.getString("NM_BAIRRO"));
			Date dtViaturaNoLocal = rs.getDate("DT_VIATURA_NO_LOCAL");
			String nomeViatura = rs.getString("NM_VIATURA");
			String nomeFraseEvento = rs.getString("NM_FRASE_EVENTO");
			String txObsevacaoFechamento = rs.getString("TX_OBSERVACAO_FECHAMENTO");
			String motivo = rs.getString("motivo");
			String evento = rs.getString("CD_EVENTO");
			String email = (rs.getString("EMAILRESP") == null ? "" : rs.getString("EMAILRESP"));
			String cdCliente = rs.getString("CD_CLIENTE");
			String dataAtendimento = NeoDateUtils.safeDateFormat(dtViaturaNoLocal, "dd/MM/yyyy");
			String horaAtendimento = rs.getString("DT_VIATURA_NO_LOCAL").substring(11, 16);
			String imgLocalGrupoVTO = "";
			// String central = rs.getString("ID_CENTRAL");
			String grupoCliente = rs.getString("DESCRICAO_GRUPO");

			int codigoGrupo = rs.getInt("CD_GRUPO_CLIENTE");

			int empresa = rs.getInt("ID_EMPRESA");
			String historico = (rs.getString("CD_HISTORICO") == null ? "" : rs.getString("CD_HISTORICO"));

			this.getImagensEvento(historico);

			// System.out.println("[CM.RAT] [" + key + "]  fantasia:" + fantasia + ", razao:" + razao + ", email" + email);

			String nomeColab = "";
			if (nomeViatura.contains("-")) {
			    int pos = nomeViatura.lastIndexOf("-");
			    nomeColab = nomeViatura.substring(pos + 1, nomeViatura.length());
			} else {
			    nomeColab = nomeViatura;
			}

			if (txObsevacaoFechamento != null && !txObsevacaoFechamento.isEmpty()) {
			    String array[] = txObsevacaoFechamento.split("###");
			    String textoFormatado = "";
			    for (String string : array) {
				textoFormatado += string + "<br/>";
			    }
			    array = txObsevacaoFechamento.split(" ");
			    List<String> respostas = new ArrayList<String>();
			    List<String> cameras = new ArrayList<String>();
			    int i = 0;
			    for (String string : array) {
				if (string.contains("R:")) {
				    String arrayResp[] = string.split("\\n");
				    if (!respostas.contains(arrayResp[0])) {
					respostas.add(arrayResp[0]);
				    }
				}
				if (string.equals("camera") || string.equals("câmera")) {
				    if (array.length > i + 1) {
					String c = string + " " + array[i + 1];
					if (!c.contains("(") && !c.contains(")") && !c.contains("{") && !c.contains("}") && !c.contains("[") && !c.contains("]")) {
					    cameras.add(c);
					}
				    }
				} else {
				    if (string.contains("camera") || string.contains("câmera")) {
					if (string.contains(":")) {
					    String arrayCam[] = string.split(":");
					    cameras.clear();
					    if (arrayCam.length > 1 && array.length > i + 1) {
						cameras.add(arrayCam[1] + " " + array[i + 1]);
					    }
					} else {
					    String arrayCam[] = string.split("\\n");
					    for (String s : arrayCam) {
						if (s.equals("camera") || s.equals("câmera")) {
						    if (array.length > i + 1) {
							cameras.add(s + " " + array[i + 1]);
						    }
						}
					    }
					}
				    }
				}
				i++;
			    }

			    if (!textoFormatado.isEmpty()) {
				textoFormatado = textoFormatado.replaceAll("#", "");
				textoFormatado = textoFormatado.replaceAll("\\r\\n", "");
				textoFormatado = textoFormatado.replaceAll(";", "");
				textoFormatado = textoFormatado.replaceAll(calendarDDMM, "<br/><br/>" + calendarDDMM);
				// textoFormatado = textoFormatado.replaceAll(calendarDM, "<br/><br/>" + calendarDDMM);

				if (respostas.size() > 0) {
				    for (String resposta : respostas) {
					textoFormatado = textoFormatado.replaceAll(resposta, "<br/>" + resposta + "<br/><br/>");
				    }
				}

				if (cameras.size() > 0) {
				    for (String camera : cameras) {
					textoFormatado = textoFormatado.replaceAll(camera, "<br/>" + camera);
				    }
				}

				textoFormatado = textoFormatado.replaceAll("Ligando para cliente!", "Ligando para cliente ");
				textoFormatado = textoFormatado.replaceAll("Ramal", "a partir do ramal ");

				textoFormatado = textoFormatado.replaceAll("FecharEvento", " Finalizar evento.");
				textoFormatado = textoFormatado.replaceAll("EmEspera", " Aguardo deslocamento de um atendente.");
				textoFormatado = textoFormatado.replaceAll("TratarSigma", "");

				textoFormatado = textoFormatado.replaceAll("AtendimentoRealizado", " Finalizar evento.");
				textoFormatado = textoFormatado.replaceAll("SemContato", " Não houve sucesso em nenhuma tentativa de contato.");
				textoFormatado = textoFormatado.replaceAll("AtualizarCadastro", " Atualizar cadastro do cliente.");
				textoFormatado = textoFormatado.replaceAll("NaoEnviaComunicado", " Comunicado de alarme com a central de monitoramento.");

				txObsevacaoFechamento = textoFormatado;
			    }
			}

			boolean flagAtualizadoNAC = observacao.contains("#AC");
			// ESTA É MINHA ADAPTAÇÃO
			List<String> emailClie = OrsegupsEmailUtils.validarEmail(email, cdCliente, flagAtualizadoNAC);

			List<String> emailGrupo = OrsegupsEmailUtils.getEmailsGrupoCliente(codigoGrupo);

			for (String m : emailGrupo) {
			    if (!emailClie.contains(m.toLowerCase().trim())) {
				emailClie.add(m);
			    }
			}

			if ((emailClie != null) && (!emailClie.isEmpty())) {

			    String grupo = null;
			    String neoId = null;
			    Long tipRat = 11L;
			    Long executionTime = new java.util.Date().getTime();
			    String ratingToken = DigestUtils.sha256Hex(cgcCpfPrincipal + Long.toString(tipRat) + Long.toString(executionTime));
			    
			    for (String emailFor : emailClie) {
				StringBuilder noUserMsg = new StringBuilder();

				// boolean achouEmail = false;
				NeoObject emailExce = PersistEngine.getObject(ExcecoesEmail.getEntityClass(), new QLEqualsFilter("email", emailFor));

				if (emailExce == null) {
				    /*
				     * EntityWrapper emailExcWp = new
				     * EntityWrapper(emailExce); String
				     * execEmail = (String)
				     * emailExcWp.findValue("email"); if
				     * (execEmail.equalsIgnoreCase(emailFor)) {
				     * achouEmail = true; } } if (!achouEmail) {
				     */

				    final String tipo = OrsegupsEmailUtils.TIPO_RAT_DESLOCAMENTO;

				    Map<Integer, String> params = OrsegupsEmailUtils.getConfiguracaoEmpresa(empresa);

				    String pasta = null;
				    String remetente = null;

				    if (params != null) {

					pasta = params.get(OrsegupsEmailUtils.RAT_PASTA);
					remetente = params.get(OrsegupsEmailUtils.RAT_REMETENTE);
					grupo = params.get(OrsegupsEmailUtils.RAT_GRUPO);
					neoId = params.get(OrsegupsEmailUtils.RAT_NEOID);

					noUserMsg.append(OrsegupsEmailUtils.getCabecalhoEmail(tipo, pasta, ratingToken));

					// ======================= inicio da mod

					noUserMsg.append("\n <table width=\"600\" border=\"0\">");
					noUserMsg.append("\n <tbody>");
					noUserMsg.append("\n <tr>");

					noUserMsg.append("\n <td><table width=\"600\" border=\"0\">");
					noUserMsg.append("\n <tbody>");
					noUserMsg.append("\n <tr>");
					noUserMsg.append("\n <td colspan=\"5\" style=\"padding-left:1%;padding-right:18%;\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Informamos o atendimento ao seguinte evento de seu sistema de segurança:</p>");
					noUserMsg.append("\n </td> </tr> </br>");
					noUserMsg.append("\n <tr> ");
					noUserMsg.append("\n <td colspan=\"5\" style=\"text-align: center;\"> <p style=\"font-family: 'Verdana';font-weight:normal;font-size:20px;text-align:center;color:#007FFF;font-weight:bold;padding:10px;margin-top:0px;\">RONDA VIZINHANÇA TRANQUILA</p>");
					noUserMsg.append("\n </td> </tr> </br>");

					noUserMsg.append("\n <tr>");
					noUserMsg.append("\n <td colspan=\"5\" style=\"padding-left:1%;padding-right:18%;\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Resultado</p><br>");
					noUserMsg.append("\n </td> </tr> </br>");
					noUserMsg.append("\n <tr>");
					noUserMsg.append("\n <td colspan=\"5\" style=\"text-align: center;\"> <p style=\"font-family: 'Verdana';font-weight:normal;font-size:20px;text-align:center;color:#31B404;font-weight:bold;padding:10px;margin-top:0px;\">VISTORIA REALIZADA - TUDO OK NO LOCAL</p>");
					noUserMsg.append("\n </td> </tr> </br>");
					noUserMsg.append("\n <tr>");
					noUserMsg.append("\n <td colspan=\"5\" style=\"padding-left:1%;padding-right:18%;\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">O atendimento foi realizado por:</p>");
					noUserMsg.append("\n </td> </tr> </br>");
					noUserMsg.append("\n </tbody>");
					noUserMsg.append("\n </table></td>");
					noUserMsg.append("\n </tr>");
					noUserMsg.append("\n <tr>");
					noUserMsg.append("\n <td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/separator-02.jpg\" width=\"600\" height=\"8\" alt=\"\"/></td>");
					noUserMsg.append("\n </tr>");
					noUserMsg.append("\n <tr> ");
					noUserMsg.append("\n <td style=\"padding:5px;\"><h2 style=\"border-bottom:1px solid #CCC;margin:0px 15px 20px 5px;padding-bottom:10px;line-height:14px;font-size:20px;text-align:center;font-family: 'Verdana';font-weight:normal;\">AGENTE DE SEGURANÇA</td>");
					noUserMsg.append("\n </tr>");
					noUserMsg.append("\n <tr>");
					noUserMsg.append("\n <td style=\"padding-left:18%;padding-right:18%;\">");
					noUserMsg.append("\n <table width=\"120%\" border=\"0\" style=\"margin-bottom:-3px;\">");
					noUserMsg.append("\n <tbody>");
					noUserMsg.append("\n <tr>");
					noUserMsg.append("\n <td width=\"34%\">&nbsp;</td>");

					noUserMsg.append("\n <td width=\"25%\"><img style=\"max-height:226px;margin-bottom:-4px;margin-left:auto;\" src=\"https://intranet.orsegups.com.br/fusion/file/download/" + ((NeoFile) wpneo.getValue("fotoTec")).getNeoId() + "\" /></td>");
					noUserMsg.append("\n <td width=\"41%\">&nbsp;</td>");
					noUserMsg.append("\n </tr>");
					noUserMsg.append("\n </tbody>");
					noUserMsg.append("\n </table>");
					noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:20px;text-align:center;color:#007FFF;font-weight:bold;border-top:1px solid #CCC;border-bottom:1px solid #CCC;padding:10px;margin-top:0px;\">" + nomeColab + "</p>");
					noUserMsg.append("\n </td>");
					noUserMsg.append("\n </tr>");
					noUserMsg.append("\n <tr>");
					noUserMsg.append("\n <td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/separator-02.jpg\" width=\"600\" height=\"8\" alt=\"\"/></td>");
					noUserMsg.append("\n </tr>");
					noUserMsg.append("\n <tr>");
					noUserMsg.append("\n <td><table width=\"100%\" border=\"0\">");
					noUserMsg.append("\n <tbody>");
					noUserMsg.append("\n <tr> ");
					noUserMsg.append("\n <td colspan=\"6\" style=\"padding:5px;\"><h2 style=\"border-bottom:1px solid #CCC;margin:0px 15px 20px 5px;padding-bottom:10px;line-height:14px;font-size:20px;text-align:center;font-family: 'Verdana';font-weight:normal;\">DETALHES SOBRE O ATENDIMENTO</td>");
					noUserMsg.append("\n </tr>");
					noUserMsg.append("\n <tr>");
					noUserMsg.append("\n <td colspan=\"4\" align=\"left\" valign=\"top\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:14px;\">");

					noUserMsg.append("<img src=\"");

					if (codigoGrupo != 0) {
					    imgLocalGrupoVTO = OrsegupsEmailUtils.getMapaGrupoClienteSigma(codigoGrupo);
					    noUserMsg.append(imgLocalGrupoVTO);
					} else {
					    noUserMsg.append("https://maps.googleapis.com/maps/api/staticmap?zoom=19&size=300x300&maptype=hybrid&format=png" + "&markers=icon:http://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/maps/images/casa_rat_email.png%7C" + rs.getString("NU_LATITUDE")
						    + "," + rs.getString("NU_LONGITUDE"));
					}

					noUserMsg.append("\" width=\"300\" height=\"300\" alt=\"\"/></p><p style=\"font-family: 'Verdana';font-weight:normal;font-size:8px;width: 300px;\">* A posição da viatura é baseada em coordenadas fornecidas pelo GPS do veículo e pode sofrer variações de posicionamento em virtude de alguns fatores tais como: precisão do equipamento, cobertura, etc.</p></td>");
					noUserMsg.append("\n <td width=\"10%\"><p><br>");
					noUserMsg.append("\n </p></td>");
					noUserMsg.append("\n <td width=\"51%\" align=\"left\" valign=\"top\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Local </br> <strong>" + grupoCliente + "</strong></br>");
					noUserMsg.append("\n </p>");
					noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Data <br> <strong>" + dataAtendimento + "</strong></br>");
					noUserMsg.append("\n Hora  </br> <strong>" + horaAtendimento + "</strong></p>");
					noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Evento </br> ");
					noUserMsg.append("\n <strong>RONDA VIZINHANÇA TRANQUILA</strong></p> ");
					noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Resultado do atendimento</br>");
					noUserMsg.append("\n <b><p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Vistoria realizada, informado tudo ok no local pelo nosso agente de segurança</b></br>");
					noUserMsg.append("\n </tr>");
					if (txObsevacaoFechamento != null && !txObsevacaoFechamento.equals("")) {
					    noUserMsg.append("\n <tr>");
					    noUserMsg.append("\n <td colspan=\"6\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align:justify;\"><strong>Observações</strong></p>");
					    noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align:justify;\"> " + txObsevacaoFechamento + "</p></td>");
					    noUserMsg.append("\n </tr>");
					}

					noUserMsg.append("\n </tr>");
					noUserMsg.append("\n </tbody>");
					noUserMsg.append("\n </table>");

					QLGroupFilter filter = new QLGroupFilter("AND");
					filter.addFilter(new QLEqualsFilter("historico", historico));

					List<NeoObject> listaImagens = (List<NeoObject>) PersistEngine.getObjects(infoImagens.getEntityClass(), filter, -1, -1, "camera asc");

					if (listaImagens != null && !listaImagens.isEmpty()) {
					    int y = 0;

					    noUserMsg.append("\n <br/><table width=\"600\" border=\"0\" style=\"margin-bottom:-3px;\">");
					    noUserMsg.append("\n <tbody>");

					    for (NeoObject neoObject : listaImagens) {
						if (y == 2) {
						    noUserMsg.append("\n </tr>");
						    noUserMsg.append("\n <tr>");
						    y = 0;
						}
						EntityWrapper wImagem = new EntityWrapper(neoObject);
						//
						noUserMsg.append("\n <td valign=\"top\"> ");
						noUserMsg.append(" <img src=\"https://intranet.orsegups.com.br/fusion/file/download/" + ((NeoFile) wImagem.getValue("imagem")).getNeoId() + "\" height=\"240\" width=\"320\" border=\"0\" alt=\"Imagens\"></td>");
						y++;
					    }

					    noUserMsg.append("\n </tr>");
					    noUserMsg.append("\n </tbody>");
					    noUserMsg.append("\n <br/></table>");
					}

					// ============== FIM DO BLOCO PRINCIPAL

					noUserMsg.append(OrsegupsEmailUtils.getRodapeEmail(tipo, pasta, grupo, email, remetente, ratingToken));

					// ADD na lista de e-mail

					NeoObject emailHis = infoHis.createNewInstance();
					EntityWrapper emailHisWp = new EntityWrapper(emailHis);
					adicionados = adicionados + 1;
					emailHisWp.findField("fantasia").setValue(fantasia);
					emailHisWp.findField("razao").setValue(razao);
					emailHisWp.findField("matricula").setValue(matricula);
					emailHisWp.findField("particao").setValue(particao);
					emailHisWp.findField("endereco").setValue(endereco);
					emailHisWp.findField("cidade").setValue(cidade);
					emailHisWp.findField("bairro").setValue(bairro);
					emailHisWp.findField("dataViaturaLocal").setValue(dataAtendimento);
					emailHisWp.findField("horaViaturaLocal").setValue(horaAtendimento);
					emailHisWp.findField("nomeViatura").setValue(nomeViatura);

					if (txObsevacaoFechamento.length() > 5999) {
					    log.error("EmailDeliveryAIT OVER LENGTH - textoObservacaoFechamentos");
					} else {
					    emailHisWp.findField("textoObservacaoFechamentos").setValue(txObsevacaoFechamento);
					}

					emailHisWp.findField("motivoAlarme").setValue(motivo);
					emailHisWp.findField("evento").setValue(evento);
					if (emailFor.length() > 4999) {
					    log.error("EmailDeliveryAIT OVER LENGTH - enviadoPara");
					} else {
					    emailHisWp.findField("enviadoPara").setValue(emailFor);
					}
					emailHisWp.findField("nomeFraseViatura").setValue(nomeFraseEvento);
					PersistEngine.persist(emailHis);

					GregorianCalendar dataCad = (GregorianCalendar) GregorianCalendar.getInstance();
					NeoObject emaiEnvio = infoEnvioEmail.createNewInstance();
					EntityWrapper emailEnvioWp = new EntityWrapper(emaiEnvio);
					emailEnvioWp.findField("de").setValue("cm.rat" + remetente);
					emailEnvioWp.findField("para").setValue(emailFor + ";emailautomatico@orsegups.com.br");
					emailEnvioWp.findField("assunto").setValue("Relatório de Atendimento Tático - " + fantasia + " - " + dataAtendimento + " " + horaAtendimento);
					emailEnvioWp.findField("Mensagem").setValue(noUserMsg.toString());
					emailEnvioWp.findField("datCad").setValue(dataCad);
					PersistEngine.persist(emaiEnvio);

					// TESTE E-MAIL UTILIZANDO RECURSO FUSION
					//String subject = "Relatório de Atendimento Tatico - " + fantasia;
					//OrsegupsEmailUtils.sendTestEmail(noUserMsg, subject);

					OrsegupsUtils.sendEmailWhatsAppRAT(Long.valueOf(cdCliente), "Relatório de Atendimento Tático - " + fantasia + " - " + dataAtendimento + " " + horaAtendimento, endereco + " - " + bairro + " - " + cidade, dataAtendimento + " " + horaAtendimento);

				    } else {
					if (!empresasNotificadas.contains(empresa)) {
					    OrsegupsEmailUtils.enviarNotificacaoEmpresaSemConfig(empresa);
					    empresasNotificadas.add(empresa);
					}
				    }
				}
			    }

			    try {

				List<String> GcgsCpfs = OrsegupsEmailUtils.getCgcCpfGrupoCliente(codigoGrupo);

				for (String cgcCpf : GcgsCpfs) {
				    cgcCpf = cgcCpf.replaceAll("([.\\-/])", "").trim();
				    // Montando informações para serviço Mobile
				    // - Inicio
				    ObjRatMobile objRat = new ObjRatMobile();
				    
				    objRat.setTipRat(tipRat);
				    objRat.setRatingToken(DigestUtils.sha256Hex(cgcCpf + Long.toString(tipRat) + Long.toString(executionTime)));
				    objRat.setInformativo("RONDA VIZINHANÇA TRANQUILA");
				    objRat.setEvento("RONDA VIZINHANÇA TRANQUILA");
				    objRat.setHashId("Relatório de Atendimento Tático - " + cgcCpf + " - " + dataAtendimento + " " + horaAtendimento);
				    objRat.setResultado("Vistoria realizada, informado tudo ok no local");
				    objRat.setResultadoDoAtendimento("Vistoria realizada, tudo ok no local");
				    objRat.setAtendidoPor(nomeColab);
				    objRat.setLocal(grupoCliente);
				    objRat.setDataAtendimento(dataAtendimento);
				    objRat.setHoraAtendimento(horaAtendimento);
				    if (txObsevacaoFechamento != null && !txObsevacaoFechamento.equals("")) {
					objRat.setObservacao(txObsevacaoFechamento);
				    } else {
					objRat.setObservacao("");
				    }
				    objRat.setEmpRat(grupo);
				    objRat.setNeoId(neoId);
				    objRat.setLnkFotoAit("http://intranet.orsegups.com.br/fusion/file/download/" + ((NeoFile) wpneo.getValue("fotoTec")).getNeoId());

				    if (!"".equals(imgLocalGrupoVTO)) {
					objRat.setLnkFotoLocal(imgLocalGrupoVTO);
				    } else {
					objRat.setLnkFotoLocal("https://maps.googleapis.com/maps/api/staticmap?zoom=19&size=300x300&maptype=hybrid&format=png" + "&markers=icon:http://intranet.orsegups.com.br/fusion/custom/jsp/orsegups/maps/images/casa_rat_email.png%7C" + rs.getString("NU_LATITUDE")
						+ "," + rs.getString("NU_LONGITUDE"));
				    }

				    // Montando informações para serviço Mobile - Fim

				    if (!cgcCpf.equals("")) {
					objRat.setCgcCpf(Long.parseLong(cgcCpf));
					IntegracaoPortalMobile integracao = new IntegracaoPortalMobile();
					integracao.inserirInformacoesPush(objRat);
				    } else {
					objRat.setCgcCpf(0L);
				    }

				}

			    } catch (Exception e) {
				log.error("Erro IntegracaoPortalMobile EmailDeliveryAIT", e);
			    }

			} else {
			    System.out.println("[CM.RAT] [" + key + "]  Email do cliente invalido vou vazio ");
			}
		    } else {
			// envio email adastro tecnico
			System.out.println("Inicio cadastro AIT");
			List<NeoObject> listaEmailsEnviadosCM = PersistEngine.getObjects(infoCadAIT.getEntityClass(), new QLEqualsFilter("cdAIT", rs.getString("cd_viatura")), -1, -1);

			if (listaEmailsEnviadosCM.isEmpty()) {

			    System.out.println("[CM.RAT] [" + key + "]  Preparando para enviar email de cadastro de AIT ");

			    MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

			    MailSettings mailClone = new MailSettings();
			    mailClone.setMinutesInterval(settings.getMinutesInterval());
			    mailClone.setFromEMail(settings.getFromEMail());
			    mailClone.setFromName(settings.getFromName());
			    mailClone.setRenderServer(settings.getRenderServer());
			    mailClone.setSecurityFromEmail(settings.getSecurityFromEmail());
			    mailClone.setSecurityCopyEmail(settings.getSecurityCopyEmail());
			    mailClone.setSmtpSettings(settings.getSmtpSettings());
			    mailClone.setPort(settings.getPort());
			    mailClone.setSmtpServer(settings.getSmtpServer());
			    mailClone.setEnabled(settings.isEnabled());

			    mailClone.setFromEMail("cm@orsegups.com.br");

			    mailClone.setFromName("Orsegups Partipações S.A.");
			    if (mailClone != null && mailClone.isEnabled()) {

				HtmlEmail noUserEmail = new HtmlEmail();
				StringBuilder noUserMsg = new StringBuilder();

				Calendar saudacao = Calendar.getInstance();
				String saudacaoEMail = "";
				if (saudacao.get(Calendar.HOUR_OF_DAY) >= 6 && saudacao.get(Calendar.HOUR_OF_DAY) <= 12) {
				    saudacaoEMail = "Bom dia, ";
				} else if (saudacao.get(Calendar.HOUR_OF_DAY) > 12 && saudacao.get(Calendar.HOUR_OF_DAY) <= 18) {
				    saudacaoEMail = "Boa tarde, ";
				} else {
				    saudacaoEMail = "Boa noite, ";
				}

				noUserEmail.setCharset("ISO-8859-1");
				noUserEmail.addTo("cm@orsegups.com.br");
				noUserEmail.addTo("giliardi@orsegups.com.br");
				noUserEmail.addTo("emailautomatico@orsegups.com.br");

				noUserEmail.setFrom("cm@orsegups.com.br");
				noUserEmail.setSubject("(Não Responda - Cadastro de AIT " + rs.getString("NM_VIATURA"));
				System.out.println("[CM.RAT] [" + key + "]  (Não Responda - Cadastro de AIT " + rs.getString("NM_VIATURA"));

				noUserMsg.append("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></head><body>");
				noUserMsg.append("<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#ffffff\">");
				noUserMsg.append("          <tbody>");
				noUserMsg.append("		    <td align=\"left\" valign=\"top\" >");
				noUserMsg.append("               <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/logoORS.jpg\" >");
				noUserMsg.append("            </td>");
				noUserMsg.append("			<td>");
				noUserMsg.append("			<strong>" + saudacaoEMail + " Boa tarde, favor providenciar FOTO para o cadastro do AIT para atendimento de eventos");
				noUserMsg.append("			<strong>Código: </strong> " + rs.getString("cd_viatura") + " <br>");
				noUserMsg.append("			<strong>Nome: </strong>" + rs.getString("NM_VIATURA") + " <br>");
				noUserMsg.append("			</td>");
				noUserMsg.append("     </tbody></table>");
				noUserMsg.append("</body></html>");
				noUserEmail.setHtmlMsg(noUserMsg.toString());
				noUserEmail.setContent(noUserMsg.toString(), "text/html");
				mailClone.applyConfig(noUserEmail);

				noUserEmail.send();

				NeoObject emailCadTec = infoCadAIT.createNewInstance();
				EntityWrapper emailHisWp = new EntityWrapper(emailCadTec);
				emailHisWp.findField("cdAIT").setValue(rs.getString("cd_viatura"));
				PersistEngine.persist(emailCadTec);
				PersistEngine.commit(true);

				System.out.println("[CM.RAT] [" + key + "]  email enviado com sucesso");

			    }
			}
			System.out.println("Fim cadastro AIT");
		    }
		}
	    }
	    System.out.println("E-Mail AIT Fim execução em: " + dateFormat.format(new GregorianCalendar().getTime()));
	} catch (Exception e) {
	    e.printStackTrace();
	    System.out.println("[" + key + "] E-Mail AIT erro no processamento." + e.getMessage());
	    log.error("E-Mail AIT erro no processamento:");
	    throw new JobException("Erro no processamento. Procurar no log por [" + key + "] " + e.getMessage());
	} finally {
	    try {
		if (conn != null) {
		    conn.close();
		}
		if (pstm != null) {
		    pstm.close();
		}
	    } catch (Exception e2) {
		log.error("E-Mail AIT erro fechamento de recursos cadastro AIT:" + e2.getMessage());
	    }

	}

    }

    private void getImagensEvento(String historico) {

	InputStream is = null;

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	BufferedImage image = null;

	File outputfile = null;

	/**
	 * Imagens tiradas pelo AIT
	 */
	try {

	    conn = PersistEngine.getConnection("SIGMA90");

	    String sql = "SELECT BL_IMAGEM FROM VTR_IMAGEM WITH(NOLOCK) WHERE CD_HISTORICO IN (" + historico + ")";

	    stmt = conn.createStatement();

	    rs = stmt.executeQuery(sql);

	    int count = 0;

	    while (rs.next()) {

		is = rs.getBinaryStream("BL_IMAGEM");

		image = ImageIO.read(is);

		outputfile = new File("image.jpg");

		if (image != null) {
		    ImageIO.write(image, "jpg", outputfile);

		    InstantiableEntityInfo colAit = AdapterUtils.getInstantiableEntityInfo("SigmaImagensEventoRonda");

		    NeoObject noInatUsu = colAit.createNewInstance();
		    EntityWrapper tmeWrapper = new EntityWrapper(noInatUsu);

		    NeoFile newFile = new NeoFile();
		    newFile.setName(outputfile.getName());
		    newFile.setStorage(NeoStorage.getDefault());
		    PersistEngine.persist(newFile);
		    PersistEngine.evict(newFile);
		    OutputStream outi = newFile.getOutputStream();
		    InputStream in = null;
		    in = new BufferedInputStream(new FileInputStream(outputfile));
		    NeoStorage.copy(in, outi);
		    newFile.setOrigin(NeoFile.Origin.TEMPLATE);
		    tmeWrapper.findField("historico").setValue(historico);
		    tmeWrapper.findField("camera").setValue("camera" + count);
		    tmeWrapper.findField("imagem").setValue(newFile);
		    tmeWrapper.findField("dataCadastro").setValue(new GregorianCalendar());
		    PersistEngine.persist(noInatUsu);

		    count++;

		}

	    }

	} catch (Exception e) {
	    e.printStackTrace();

	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

    }

    private void inserirFimRotina() {

	try {
	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("nomeRotina", "EmailDeliveryRondaVTO"));

	    List<NeoObject> monitoraAgendador = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);
	    PersistEngine.getEntityManager().flush();

	    if (monitoraAgendador != null && !monitoraAgendador.isEmpty()) {
		NeoObject neoObject = (NeoObject) monitoraAgendador.get(0);

		EntityWrapper monitoraAgendadorWrapper = new EntityWrapper(neoObject);

		monitoraAgendadorWrapper.findField("dataFinalAgendador").setValue(new GregorianCalendar());

		PersistEngine.persist(neoObject);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    private String ultimaExecucaoRotina() {

	String retorno = "";
	try {
	    Collection<NeoObject> monitoraAgendador = null;

	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("nomeRotina", "EmailDeliveryRondaVTO"));

	    monitoraAgendador = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);

	    if (monitoraAgendador != null && !monitoraAgendador.isEmpty()) {
		for (NeoObject neoObject : monitoraAgendador) {
		    EntityWrapper monitoraAgendadorObj = new EntityWrapper(neoObject);

		    GregorianCalendar dataFinalAgendador = (GregorianCalendar) monitoraAgendadorObj.getValue("dataFinalAgendador");

		    retorno = NeoDateUtils.safeDateFormat(dataFinalAgendador, "yyyy-MM-dd HH:mm:ss");
		}
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	}

	return retorno;
    }

    private String ultimaExecucaoRotinaRange() {

	String retorno = "";
	try {
	    Collection<NeoObject> monitoraAgendador = null;

	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("nomeRotina", "EmailDeliveryRondaVTO"));

	    monitoraAgendador = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);

	    if (monitoraAgendador != null && !monitoraAgendador.isEmpty()) {
		for (NeoObject neoObject : monitoraAgendador) {
		    EntityWrapper monitoraAgendadorObj = new EntityWrapper(neoObject);

		    GregorianCalendar dataFinalAgendador = (GregorianCalendar) monitoraAgendadorObj.getValue("dataFinalAgendador");

		    GregorianCalendar dataFinalAgendadorAux = (GregorianCalendar) dataFinalAgendador.clone();

		    dataFinalAgendadorAux.add(Calendar.MINUTE, -15);

		    retorno = NeoDateUtils.safeDateFormat(dataFinalAgendadorAux, "yyyy-MM-dd HH:mm:ss");
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return retorno;
    }

}