package com.neomind.fusion.custom.orsegups.presenca.vo;

public class PostoSupervisaoVO
{
	Long numLoc;
	Long usuCodccu;
	String nomLoc;
	String usuLotOrn;
	Long usuNumCtr;
	RegionalVO regional;
	String linkExcluir;
	
	public Long getNumLoc()
	{
		return numLoc;
	}
	public void setNumLoc(Long numLoc)
	{
		this.numLoc = numLoc;
	}
	public Long getUsuCodccu()
	{
		return usuCodccu;
	}
	public void setUsuCodccu(Long usuCodccu)
	{
		this.usuCodccu = usuCodccu;
	}
	public String getNomLoc()
	{
		return nomLoc;
	}
	public void setNomLoc(String nomLoc)
	{
		this.nomLoc = nomLoc;
	}
	public String getUsuLotOrn()
	{
		return usuLotOrn;
	}
	public void setUsuLotOrn(String usuLotOrn)
	{
		this.usuLotOrn = usuLotOrn;
	}
	public Long getUsuNumCtr()
	{
		return usuNumCtr;
	}
	public void setUsuNumCtr(Long usuNumCtr)
	{
		this.usuNumCtr = usuNumCtr;
	}
	public RegionalVO getRegional()
	{
		return regional;
	}
	public void setRegional(RegionalVO regional)
	{
		this.regional = regional;
	}
	public String getLinkExcluir()
	{
		return linkExcluir;
	}
	public void setLinkExcluir(String linkExcluir)
	{
		this.linkExcluir = linkExcluir;
	}
	@Override
	public String toString()
	{
		return "PostoSupervisaoVO [numLoc=" + numLoc + ", usuCodccu=" + usuCodccu + ", nomLoc=" + nomLoc + ", usuLotOrn=" + usuLotOrn + ", usuNumCtr=" + usuNumCtr + ", regional=" + regional + ", linkExcluir=" + linkExcluir + "]";
	}
	

}
