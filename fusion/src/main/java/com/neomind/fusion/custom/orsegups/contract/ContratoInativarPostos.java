package com.neomind.fusion.custom.orsegups.contract;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.adapter.casvig.OrsegupsContratoUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class ContratoInativarPostos implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(ContratoInativarPostos.class);
	
	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		try
		{
			Long acao = NeoUtils.safeLong(wrapper.findField("movimentoContratoNovo.codTipo").getValueAsString());
			//Long tipoAlteracao = NeoUtils.safeLong(wrapper.findField("movimentoAlteracaoContrato.codTipo").getValueAsString());
			
			/*
			 * só haverá inativação de postos quando for alteração
			 */
			if(acao != 5)
			{
				return;
			}
			
			List<NeoObject> postos = (List<NeoObject>) wrapper.findField("postosContrato").getValue();
			List<NeoObject> postosBackup = (List<NeoObject>) wrapper.findField("postosContratoBackup").getValue();
			//Collection<NeoObject> postos = wrapper.findField("postosContrato").getValues();
			//Collection<NeoObject> postosBackup = wrapper.findField("postosContratoBackup").getValues();
			
			NeoObject postoAtualAlvo = null;
			NeoObject postoAntigoAlvo = null;
			
			GregorianCalendar dataFimpostoNormal  = null;
			
			for(NeoObject posto : postos)
			{
				EntityWrapper wPosto = new EntityWrapper(posto);
				NeoObject oTipoPosto = (NeoObject) wPosto.findValue("tipoPosto");
				EntityWrapper wTipoPosto = new EntityWrapper(oTipoPosto);
				
				try{
					if (NeoUtils.safeOutputString(wTipoPosto.findValue("codigo")).equals("1")) { //só valida se for um posto normal
						ContratoLogUtils.logInfo("Copiando dataFimFaturamento do posto antigo para o posto novo");
						dataFimpostoNormal = (GregorianCalendar) wPosto.findValue("dataFimFaturamento");
					}else{
						wPosto.setValue("dataFimFaturamento", dataFimpostoNormal);
					}
				}catch(Exception e){
					ContratoLogUtils.logInfo("ERRO ao copiar dataFimFaturamento do posto antigo para o posto novo");
					e.printStackTrace();
				}
				
				if (NeoUtils.safeOutputString(wTipoPosto.findValue("codigo")).equals("1")){ // só passa na validação de for posto normal.
					boolean inativar = NeoUtils.safeBoolean(wPosto.findValue("duplicarDesativar"));
					if(inativar)
					{
						for(NeoObject postoBackup : postosBackup)
						{
							EntityWrapper wPostoBackup = new EntityWrapper(postoBackup);
							
							long numPosto = (Long) wPosto.findValue("numPosto");
							long numPostoBackup = (Long) wPostoBackup.findValue("numPosto");
							if(numPosto == numPostoBackup)
							{
								postoAntigoAlvo = postoBackup;
								postoAtualAlvo = posto;
								break;
							}
						}
						
						
						EntityWrapper wPostoAtual = new EntityWrapper(postoAtualAlvo);
						EntityWrapper wPostoAntigo = new EntityWrapper(postoAntigoAlvo);
						//validações antes de inativar um posto
						
						/*
						 * data de inicio de faturamento deve-se buscar do posto antigo
						 * 
						 * data de fim de faturamento deve-se buscar no posto novo, na aba de inativação
						 * campo dataFimFaturamento
						 */
						
						/* 1º
						 * Verifica se a "Data de Inicio" do posto é maior que a "Data fim" e se a 
						 * "Data fim" é diferente de nulo, caso seja verdadeiro gera uma exeção
						 */
						
						if(!validacaoUm(wrapper, wPostoAtual, wPostoAntigo))
						{
							throw new WorkflowException("Data Inicial de Faturamento é maior do que a data Final. Revise!");
						}
						
						
						/* 2º
						 * Verifica se a "Situação do Posto" é diferente do que está no banco e se a "Data fim"
						 * é menor que o meu último dia do mês.
						 */
						
						if(!validacaoDois(wrapper, wPostoAtual, wPostoAntigo))
						{
							throw new WorkflowException("A data fim é diferente do último dia do mês ou a situação do posto no sapiens está diferente de Ativo!");
						}
						
						
						/* 3º
						 * Verifica se na hora da inativação o posto possui algum apontamento ou mão de obra cadastrada. 
						 * Se "Situação do posto" diferente de ativo e "Data fim" menor que o último dia do mês, 
						 * caso seja verdadeiro gera uma exceção.
						 */
						if(!validacaoTres(wrapper, wPostoAtual, wPostoAntigo))
						{
							throw new WorkflowException("Posto já possúi um apontamento ou mão de obra cadastrada nesta competência!");
						}
						
						
						
						String codemp = NeoUtils.safeOutputString(wrapper.findValue("empresa.codemp"));
						String codfil = NeoUtils.safeOutputString(wrapper.findValue("empresa.codfil"));
						String numctr = NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numctr"));
						String numpos = NeoUtils.safeOutputString(wPostoAntigo.findValue("numPosto"));
						String codigoMotivo = NeoUtils.safeOutputString(wPostoAtual.findValue("motivo.codmot"));
						String obsMotivo = NeoUtils.safeOutputString(wPostoAtual.findValue("obsInativacao"));
						
						GregorianCalendar cptAlvo = (GregorianCalendar) wPostoAtual.findValue("periodoFaturamento.de");
						
						if (ContratoUtils.postoJaFaturado(NeoUtils.safeLong(numctr), NeoUtils.safeLong(numpos), NeoUtils.safeLong(codemp), NeoUtils.safeLong(codfil), cptAlvo)){
							throw new WorkflowException("A data inicial do posto novo é menor que a data da ultima fatura do posto atual.");
						}
						
						
						// de/para definido com a fernanda martins
						/*Long codMotivo = 0L;
					if (acao == 5){
						
						switch (tipoAlteracao.intValue()) {
						case 1:
							codMotivo = 136L;
							break;
						case 2:
							codMotivo = 111L;
							break;
						case 3:
							codMotivo = 105L;
							break;
						case 4:
							codMotivo = 106L;
							break;
						case 6:
							codMotivo = 108L;
							break;
						case 9:
							codMotivo = 101L; //(quando houver alteração e endereço o código é 97)
							break;
						case 10:
							codMotivo = 105L;
							break;
						default:
							codMotivo = 0L;
							break;
						}
						
					}
					
					QLGroupFilter key = new QLGroupFilter("AND");
					key.addFilter( new QLEqualsFilter("codmot", codMotivo) );
					NeoObject oMotivo = PersistEngine.getObject(AdapterUtils.getEntityClass("SAPIENSEMOT"), key);
					if (oMotivo != null){
						EntityWrapper wMotivo = new EntityWrapper(oMotivo);
						obsMotivo = NeoUtils.safeOutputString(wMotivo.findValue("desmot"));
					}*/
						
						GregorianCalendar dataFimFaturamento = (GregorianCalendar) wPostoAtual.findValue("dataFimFaturamento");
						
						/*
						 * Inativar os apontamentos já lançados para o posto selecionado para inativação.
						 * É comum postos possuírem lançamnto de apontamentos futuros
						 */
						if(inativaApontamentosPosto(codemp, codfil, numctr, numpos))
						{
							/*
							 * Inativar o posto na Tabela USU_T160CVS
							 */
							if(inativarPostos(codemp, codfil, numctr, numpos, codigoMotivo, obsMotivo, dataFimFaturamento))
							{
								/*
								 * Inativar os postos dos Contratos na tabela E160CVS - tabela nativa de postos do Sapiens
								 */
								if(inativarPostosdoContrato(codemp, codfil, numctr, numpos, codigoMotivo, obsMotivo))
								{
									/*
									 * desativou tudo do posto, agora é gerar a observação
									 */
									Long usu_seqobs = ContratoUtils.retornaSequenciaObservacaoContrato(NeoUtils.safeLong(codemp), NeoUtils.safeLong(codfil), NeoUtils.safeLong(numctr));
									String usu_txtobs = "Posto inativado devido a duplicação de posto pelo FGC - Tarefa " + activity.getProcess().getCode() +
											" - O mesmo foi duplicado da empresa = " + codemp + " filial = " + codfil + " para empresa...";
									String usu_tipobs = "A";
									ContratoLogUtils.logInfo("inativarPostosdoContrato 1 - " + PortalUtil.getCurrentUser().getCode());
									String usu_usumov = buscaUsuarioSapiensCorrespondente(PortalUtil.getCurrentUser().getCode());
									ContratoLogUtils.logInfo("inativarPostosdoContrato 2 - " + usu_usumov);
									GregorianCalendar usu_datmov = new GregorianCalendar();
									Long usu_hormov = (usu_datmov.get(usu_datmov.HOUR_OF_DAY) * 60L) + usu_datmov.get(usu_datmov.MINUTE);
									String usu_codope = "";
									Long usu_numpos = 0L;
									String codervice = "";
									
									//gera observação
									ContratoLogUtils.logInfo("inativarPostosdoContrato 3 - " + usu_usumov);
									ContratoUtils.geraObservacao(NeoUtils.safeLong(codemp), NeoUtils.safeLong(codfil), NeoUtils.safeLong(numctr), usu_seqobs, usu_numpos, codervice, usu_tipobs, usu_txtobs, NeoUtils.safeLong(usu_usumov), usu_datmov, usu_hormov, usu_codope);
									ContratoLogUtils.logInfo("inativarPostosdoContrato 4 - " + usu_usumov);
								}
							}
						}
						
					}
					
				}
				
				
			}
		}catch (Exception e)
        {
           e.printStackTrace();

           if (e instanceof WorkflowException)
           {
                  throw (WorkflowException) e;
           }
           else
           {
                  throw new WorkflowException(
                               "Ocorreu um erro na execução do adapter. Entre em contato com o administrador do sistema. " + e.getMessage());
           }
        }

	}
	
	public static String buscaUsuarioSapiensCorrespondente(String userCode)
	{
		/*
		//Logica para buscar codigo do usuario do sapiens referente ao usuario fusion
		String sapiensUserCode = "";
		NeoObject sapiensUser = null;
		String login = userCode;
		try{
			
			ContratoLogUtils.logInfo("buscaUsuarioSapiensCorrespondente 1 - " + userCode);
			QLEqualsFilter loginFilter = new QLEqualsFilter("nomusu", login);
			//SAPIENS.R999USU
			ArrayList<NeoObject> sapiensUsers = (ArrayList<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUARIO"), loginFilter);
			ContratoLogUtils.logInfo("buscaUsuarioSapiensCorrespondente 2");
			if (sapiensUsers != null && sapiensUsers.size() > 0)
			{
				sapiensUser = sapiensUsers.get(0);
			}
	
			if (sapiensUser != null)
			{
				EntityWrapper sapiensUserEw = new EntityWrapper(sapiensUser);
				sapiensUserCode = sapiensUserEw.findValue("codusu").toString();
			}
	
			if (sapiensUserCode == null || sapiensUserCode.equals(""))
			{
				log.error("Não foi possivel buscar o codigo de usuario sapiens referente ao usuário logado.");
				throw new WorkflowException("Não foi possivel buscar o codigo de usuario sapiens referente ao usuário logado.");
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new WorkflowException("Erro ao consultar correspondencia entre usuário sapiens e fusion. msg:"+e.getMessage());
		}

		return sapiensUserCode;
		*/
		
		String login = userCode;
		String sapiensUserCode = "";
		
		StringBuilder query = new StringBuilder();

		query.append("SELECT codusu FROM R999USU  where nomusu ='"+login+"'" );
		
		ResultSet rs = OrsegupsContratoUtils.getResultSet(query.toString(), "SAPIENS");

		if (rs != null)
		{
			try
			{ 
				Long numeroUltimoObs = rs.getLong("codusu");
				sapiensUserCode = NeoUtils.safeOutputString(numeroUltimoObs);
				if (sapiensUserCode == null || sapiensUserCode.equals(""))
				{
					log.error("Não foi possivel buscar o codigo de usuario sapiens referente ao usuário logado.");
					throw new WorkflowException("Não foi possivel buscar o codigo de usuario sapiens referente ao usuário logado.");
				}
				rs.close();
				rs = null;
			}
			catch (SQLException e)
			{
				System.out.println("[FLUXO CONTRATOS] - Erro ao consultar correspondencia entre usuário sapiens e fusion. msg:"+e.getMessage());
				e.printStackTrace();
				throw new WorkflowException("Erro ao consultar correspondencia entre usuário sapiens e fusion. msg:"+e.getMessage());
			}
		}
		
		return sapiensUserCode;
	}
	
	public static boolean inativarPostosdoContrato(String codemp, String codfil, String numctr, String numpos, String codmot, String motivo)
	{
		boolean result = false;
		
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE "); 
		
		sql.append("E160CVS ");

		sql.append("SET ");
		
		sql.append("SITCVS = 'I', ");
		sql.append("CODMOT = " + codmot + ", ");
		sql.append("OBSMOT = '" + motivo + "' ");
		
		sql.append("WHERE ");
		
		sql.append("CODEMP = " + codemp);
		sql.append(" AND CODFIL = " + codfil);
		sql.append(" AND NUMCTR = " + numctr);
		sql.append(" AND USU_SEQAGR = " + numpos);
		
		Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		try
		{
			query.executeUpdate();
			result = true;
		}catch(Exception e)
		{
			e.getStackTrace();
			return false;
		}
		
		return result;
	}
	
	public static boolean inativarPostos(String codemp, String codfil, String numctr, String numpos, String codmot, String motivo, GregorianCalendar dataFimFaturamento)
	{
		boolean result = false;
		
		//GregorianCalendar dataHoje = new GregorianCalendar();
		GregorianCalendar dataMotivo = new GregorianCalendar();
		
		int hora = dataMotivo.get(dataFimFaturamento.HOUR_OF_DAY);
		int minutos = dataMotivo.get(dataFimFaturamento.MINUTE);

		Long horaMotivo = (hora * 60L) + minutos;
		
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE "); 
		
		sql.append("USU_T160CVS ");

		sql.append("SET ");
		
		sql.append("USU_SITCVS = 'I', ");
		sql.append("USU_DATFIM = '" + ContratoUtils.retornaDataFormatoSapiens(dataFimFaturamento) + "', ");
		sql.append("USU_DATMOT = '" +ContratoUtils.retornaDataFormatoSapiens(dataMotivo) + "', ");
		sql.append("USU_HORMOT = " + horaMotivo + ", ");
		sql.append("USU_CODMOT = " + codmot + ", ");
		sql.append("USU_OBSMOT = '" + motivo + "' ");
		
		sql.append("WHERE ");
		
		sql.append("USU_CODEMP = " + codemp);
		sql.append(" AND USU_CODFIL = " + codfil);
		sql.append(" AND USU_NUMCTR = " + numctr);
		sql.append(" AND USU_NUMPOS = " + numpos);
		sql.append(" AND USU_SITCVS= 'A' ");
		
		Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		try
		{
			query.executeUpdate();
			result = true;
		}catch(Exception e)
		{
			e.getStackTrace();
			return false;
		}
		
		return result;
	}
	
	public static boolean inativaApontamentosPosto(String codemp, String codfil, String numctr, String numpos)
	{
		boolean result = false;
		GregorianCalendar dataHoje = new GregorianCalendar();
		GregorianCalendar mesSeguinte = new GregorianCalendar();
		mesSeguinte.add(GregorianCalendar.MONTH, 1);
		
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE "); 
		
		sql.append("USU_T160CMS ");

		sql.append("SET ");
		
		sql.append("USU_LIBAPO = 'N', ");
		sql.append("USU_DATFIM = '" + ContratoUtils.retornaDataFormatoSapiens(dataHoje) + "' ");
		
		sql.append("WHERE ");
		
		sql.append("USU_CODEMP = " + codemp);
		sql.append(" AND USU_CODFIL = " + codfil);
		sql.append(" AND USU_NUMCTR = " + numctr);
		sql.append(" AND USU_NUMPOS = " + numpos);
		sql.append(" AND USU_LIBAPO = 'S' ");
		//sql.append(" AND USU_ADCSUB = '+' ");
		sql.append(" AND USU_DATCPT >= '" + ContratoUtils.retornaDataFormatoSapiens(mesSeguinte) + "'");
		
		Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		try
		{
			query.executeUpdate();
			result = true;
		}catch(Exception e)
		{
			e.getStackTrace();
			return false;
		}
		
		return result;
	}
	
	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}
	
	public boolean validacaoUm(EntityWrapper wrapper, EntityWrapper wPostoAtual, EntityWrapper wPostoAntigo)
	{
		boolean result = true;
		
		GregorianCalendar dataIni = (GregorianCalendar) wPostoAtual.findValue("periodoFaturamento.de");
		GregorianCalendar dataFim = (GregorianCalendar) wPostoAtual.findValue("dataFimFaturamento");
		
		System.out.println(NeoUtils.safeDateFormat(dataFim));
		System.out.println(NeoUtils.safeDateFormat(dataIni));
		if(dataFim.after(dataIni))
			result = false;
		
		return result;
	}
	
	public String buscaSituacaoPosto(EntityWrapper wrapper, EntityWrapper wPosto)
	{
		String situacao = "";
		
		String codemp = NeoUtils.safeOutputString(wrapper.findValue("empresa.codemp"));
		String codfil = NeoUtils.safeOutputString(wrapper.findValue("empresa.codfil"));
		String numpos = NeoUtils.safeOutputString(wPosto.findValue("numPosto"));
		String numctr = NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numctr"));
		
		StringBuilder sql = new StringBuilder();
		sql.append("select "); 
		//A ou I
		sql.append("sitPosto.usu_sitcvs ");

		sql.append("from USU_T160CVS sitPosto ");
		sql.append("WHERE sitPosto.usu_codemp = " + codemp);
		sql.append(" AND sitPosto.usu_codfil = " + codfil);
		sql.append(" AND sitPosto.usu_numctr = " + numctr);
		sql.append(" AND sitPosto.usu_numpos = " + numpos);
		
		Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		Collection<Object> resultList = query.getResultList();
		if (resultList != null && !resultList.isEmpty())
		{
			for (Object r : resultList)
			{
				if(r != null)
				{
					situacao = NeoUtils.safeOutputString(r);
				}
			}
		}
		return situacao;
	}
	
	/**
	 * Nesta validação iremos verificar se a situação do posto foi alterada, por outros mecanismos, durante a execução do processo
	 * @param wrapper
	 * @param wPosto
	 * @return
	 */
	public boolean validacaoDois(EntityWrapper wrapper, EntityWrapper wPostoAtual, EntityWrapper wPostoAntigo)
	{
		boolean result = true;
		
		Boolean adereProtecaoGarantida = NeoUtils.safeBoolean(wrapper.findValue("adereProtecaoGarantida"));
		if (adereProtecaoGarantida){
		
			GregorianCalendar dataFim = (GregorianCalendar) wPostoAtual.findValue("dataFimFaturamento");
			
			int diaMes = dataFim.get(dataFim.DAY_OF_MONTH);
			int ultimoDiaMes = dataFim.getActualMaximum(dataFim.DAY_OF_MONTH);
			if(diaMes < ultimoDiaMes) result = false;
			
		}
		/*
		 * para o fusion vem apenas os postos ativos, logo precisamos validar apenas a situação Inativo
		 */
		Long numCtr = (Long) wrapper.findValue("numContrato.usu_numctr");

		if(buscaSituacaoPosto(wrapper, wPostoAntigo).equals("") || buscaSituacaoPosto(wrapper, wPostoAntigo).equals("I"))
		{
			result = false;
		}
		
		return result;
	}
	
	public GregorianCalendar getDataFimPosto(EntityWrapper wrapper, EntityWrapper wPosto)
	{
		GregorianCalendar retorno = null;
		
		String codemp = NeoUtils.safeOutputString(wrapper.findValue("empresa.codemp"));
		String codfil = NeoUtils.safeOutputString(wrapper.findValue("empresa.codfil"));
		String numpos = NeoUtils.safeOutputString(wPosto.findValue("numPosto"));
		String numctr = NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numctr"));
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT usu_datfim ");
		sql.append("FROM USU_T160CVS ");
		sql.append("WHERE ");
		sql.append("usu_codemp = codemp " + codemp);
		sql.append("and usu_codfil = " + codfil);
		sql.append("and usu_numctr = " + numctr);
		sql.append("and usu_numpos = " + numpos);
		sql.append("and usu_datfim <> '1900-12-31 00:00:00.0' ");
		
		Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		Collection<Object> resultList = query.getResultList();
		if (resultList != null && !resultList.isEmpty())
		{
			for (Object r : resultList)
			{
				if(r != null)
				{
					GregorianCalendar dataFim = (GregorianCalendar) r;
					dataFim.set(dataFim.DAY_OF_MONTH, 01);
					
					retorno = dataFim;
				}
			}
		}
		
		return retorno;
	}
	
	public static String getMaxCpt(EntityWrapper wrapper, EntityWrapper wPosto)
	{
		GregorianCalendar retorno = null;
		
		String codemp = NeoUtils.safeOutputString(wrapper.findValue("empresa.codemp"));
		String codfil = NeoUtils.safeOutputString(wrapper.findValue("empresa.codfil"));
		String numctr = NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numctr"));
		String numpos = NeoUtils.safeOutputString(wPosto.findValue("numPosto"));
		
		StringBuilder sql2 = new StringBuilder();
		sql2.append("SELECT max(DatCpt) ");
		sql2.append("FROM E160CVS ");
		sql2.append("WHERE ");
		sql2.append("codemp = " + codemp);
		sql2.append("and codfil = " + codfil);
		sql2.append("and numctr = " + numctr);
		sql2.append("and usu_seqagr = " + numpos);
		
		Query query2 = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql2.toString());
		Collection<Object> resultList2 = query2.getResultList();
		GregorianCalendar aDatCpt = null;
		if (resultList2 != null && !resultList2.isEmpty())
		{
			for (Object r2 : resultList2)
			{
				if(r2 != null)
				{
					
					String DATE_PATTERN = "yyyy-MM-dd";
					String date = NeoUtils.safeOutputString(r2).substring(0, 10);
					return date;
					/*
					Date data= NeoCalendarUtils.formatDate(date, DATE_PATTERN);
					GregorianCalendar g = new GregorianCalendar();
					g.setTime(data);
					retorno = g;*/
				}
			}
		}
		
		return "";
	}
	
	public static GregorianCalendar getMaxCptUsu(EntityWrapper wrapper, EntityWrapper wPosto)
	{
		GregorianCalendar retorno = null;
		
		String codemp = NeoUtils.safeOutputString(wrapper.findValue("empresa.codemp"));
		String codfil = NeoUtils.safeOutputString(wrapper.findValue("empresa.codfil"));
		String numpos = NeoUtils.safeOutputString(wPosto.findValue("numPosto"));
		String numctr = NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numctr"));
		
		StringBuilder sql2 = new StringBuilder();
		sql2.append("SELECT max(usu_DatCpt) ");
		sql2.append("FROM USU_T160CMS ");
		sql2.append("WHERE ");
		sql2.append("usu_codemp = " + codemp);
		sql2.append(" and usu_codfil = " + codfil);
		sql2.append(" and usu_numctr = " + numctr);
		sql2.append(" and usu_numpos = " + numpos);
		sql2.append(" and usu_libapo = 'N'");
		
		Query query2 = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql2.toString());
		Collection<Object> resultList2 = query2.getResultList();
		GregorianCalendar aDatCpt = null;
		if (resultList2 != null && !resultList2.isEmpty())
		{
			for (Object r2 : resultList2)
			{
				if(r2 != null)
				{
					Timestamp t = (Timestamp) r2;
					retorno = new GregorianCalendar();
					retorno.setTimeInMillis(t.getTime());
				}
			}
		}
		
		return retorno;
	}
	
	public static String getNumeroNotaFiscal(EntityWrapper wrapper, EntityWrapper wPosto, String aDatCpt)
	{
		String retorno = "";
		
		String codemp = NeoUtils.safeOutputString(wrapper.findValue("empresa.codemp"));
		String codfil = NeoUtils.safeOutputString(wrapper.findValue("empresa.codfil"));
		String numpos = NeoUtils.safeOutputString(wPosto.findValue("numPosto"));
		String numctr = NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numctr"));
		
		StringBuilder sql2 = new StringBuilder();
		sql2.append("SELECT numnfv ");
		sql2.append("FROM E160CVS ");
		sql2.append("WHERE ");
		sql2.append("codemp = " + codemp);
		sql2.append(" and codfil = " + codfil);
		sql2.append(" and numctr = " + numctr);
		sql2.append(" and usu_seqagr = " + numpos);
		sql2.append(" and datcpt = " + aDatCpt);
		
		Query query2 = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql2.toString());
		Collection<Object> resultList2 = query2.getResultList();
		if (resultList2 != null && !resultList2.isEmpty())
		{
			for (Object r2 : resultList2)
			{
				if(r2 != null)
				{
					retorno = NeoUtils.safeOutputString(r2);
				}
			}
		}
		
		return retorno;
	}
	
	public static boolean validaApontamentosPosto(EntityWrapper wrapper, EntityWrapper wPosto, GregorianCalendar dataFim)
	{
		String codemp = NeoUtils.safeOutputString(wrapper.findValue("empresa.codemp"));
		String codfil = NeoUtils.safeOutputString(wrapper.findValue("empresa.codfil"));
		String numpos = NeoUtils.safeOutputString(wPosto.findValue("numPosto"));
		String numctr = NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numctr"));
		
		String data = ContratoUtils.retornaDataFormatoSapiens(dataFim);
		
		/**
		 * @author orsegups lucas.avila - removido TOP.
		 * @date 08/07/2015
		 */
		StringBuilder sql2 = new StringBuilder();
		sql2.append("SELECT USU_NUMPOS ");
		sql2.append("FROM USU_T160CMS ");
		sql2.append("WHERE ");
		sql2.append("usu_codemp = " + codemp);
		sql2.append(" and usu_codfil = " + codfil);
		sql2.append(" and usu_numctr = " + numctr);
		sql2.append(" and usu_numpos = " + numpos);
		sql2.append(" and usu_datcpt > '" + data + "'");
		sql2.append(" and usu_libapo = 'N'");
		
		Query query2 = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql2.toString());
		query2.setMaxResults(1);
		Collection<Object> resultList2 = query2.getResultList();
		if (resultList2 != null && !resultList2.isEmpty())
		{
			for (Object r2 : resultList2)
			{
				if(r2 != null)
				{
					return true;
				}
			}
		}
		
		return false;
	}
	
	public static boolean validacaoTres(EntityWrapper wrapper, EntityWrapper wPostoAtual, EntityWrapper wPostoAntigo)
	{
		boolean result = true;
		
		GregorianCalendar dataFim = (GregorianCalendar) wPostoAtual.findValue("dataFimFaturamento");
		GregorianCalendar dtFim = (GregorianCalendar) dataFim.clone();
		dtFim.set(dtFim.DAY_OF_MONTH, 01);
		dtFim.set(Calendar.HOUR_OF_DAY, 0);
		dtFim.set(Calendar.MINUTE, 0);
		dtFim.set(Calendar.SECOND, 0);

		String aDatCpt = getMaxCpt(wrapper, wPostoAntigo);
		
		if(!"".equals(aDatCpt))
		{
			String numnfv = getNumeroNotaFiscal(wrapper, wPostoAntigo, aDatCpt);
			if(numnfv != null && !numnfv.equals(""))
			{
				/*
				 * lançar mensagem de erro:
				 * 
				 * "Não é permitido inativar o posto porque já está pré-faturado ou já possuí NF" + numnfv + " emitida."
				 */
				throw new WorkflowException("Não é permitido inativar o posto porque já está pré-faturado ou já possuí NF" + numnfv + " emitida.");
			}
		}
		
		if(validaApontamentosPosto(wrapper, wPostoAntigo, dtFim))
		{
			/*
			 * lançar mensagem de erro:
			 * 
			 * "Posto já possui um apontamento ou mão de obra cadastrada nesta competência."
			 */
			throw new WorkflowException("Posto possui um apontamento ou mão de obra cadastrado com status de 'não aprovado'. Por gentileza verificar.");
		}
		
		if(getMaxCptUsu(wrapper, wPostoAntigo) != null)
		{
			GregorianCalendar maxDatCpd = getMaxCptUsu(wrapper, wPostoAntigo);
			
			maxDatCpd.set(Calendar.HOUR_OF_DAY, 00);
			maxDatCpd.set(Calendar.MINUTE, 00);
			maxDatCpd.set(Calendar.SECOND, 00);
			if(dtFim.before(maxDatCpd))
			{
				/*
				 * lançar mensagem de erro:
				 * 
				 * "Posto possui um apontamento cadastrado e não pode ser inativado!"
				 */
				throw new WorkflowException("Posto possui um apontamento cadastrado e não pode ser inativado!");
			}
		}
		
		
		return result;
	}
	
	public boolean validacaoQuatro(EntityWrapper wrapper, EntityWrapper wPosto)
	{
		boolean result = false;
		
		return result;
	}
	
	public boolean enviaEmailNotificacao(EntityWrapper wrapper, EntityWrapper wPosto)
	{
		boolean result = false;
		
		return result;
	}
	
	
	
	
}
