package com.neomind.fusion.custom.orsegups.fap.dto;

import java.util.List;

import com.neomind.fusion.common.NeoObject;

public class TecnicoDTO extends AplicacaoTecnicoTaticoDTO {
    String nomeTecnico = null;
    int qtdContaCFTV = 0;
    int qtdContaAlarme = 0;
    
    public Long getCodTecnico() {
        return codTecnico;
    }
    public void setCodTecnico(Long codTecnico) {
        this.codTecnico = codTecnico;
    }
    public int getQtdContaCFTV() {
        return qtdContaCFTV;
    }
    public void setQtdContaCFTV(int qtdContaCFTV) {
        this.qtdContaCFTV = qtdContaCFTV;
    }
    public int getQtdContaAlarme() {
        return qtdContaAlarme;
    }
    public void setQtdContaAlarme(int qtdContaAlarme) {
        this.qtdContaAlarme = qtdContaAlarme;
    }
    public String getNomeTecnico() {
        return nomeTecnico;
    }
    public void setNomeTecnico(String nomeTecnico) {
        this.nomeTecnico = nomeTecnico;
    }
    public NeoObject getFornecedor() {
        return fornecedor;
    }
    public void setFornecedor(NeoObject codFornecedor) {
        this.fornecedor = codFornecedor;
    }
    public List<NeoObject> getListaCcu() {
        return listaCcu;
    }
    public void setListaCcu(List<NeoObject> listaCcu) {
        this.listaCcu = listaCcu;
    }
    
    public List<HistoricoFapDTO> getHistorico() {
        return historicoFAP;
    }
    
    public void setHistorico(List<HistoricoFapDTO> historico){
	this.historicoFAP = historico;
    }
    
    public List<NeoObject> getHistoricoContasSigma() {
        return historicoContasSigma;
    }
    
    public void setHistoricoContasSigma(List<NeoObject> historicoContasSigma){
	this.historicoContasSigma = historicoContasSigma;
    }
    
    
       
}
