package com.neomind.fusion.custom.orsegups.rsc;

import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class RSCRegistroAtividadeVerificacao implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(RSCRegistroAtividadeVerificacao.class);

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);

		String mensagem = "Por favor, contatar o administrador do sistema!";
		WorkflowException exception = null;
		EntityWrapper verEfiWrapper = null;
		
		try
		{
			/* Início das funções úteis --> Funções necessárias para decorrer dos testes */
			//System.out.println(NeoDateUtils.safeDateFormat((GregorianCalendar) processEntity.findValue("prazoRegistrarRSC"), "dd/MM/yyyy HH:mm:ss"));
			/* Fim das funções úteis */

			RSCUtils rscUtils = new RSCUtils();
			GregorianCalendar prazoDeadLine = new GregorianCalendar();

			InstantiableEntityInfo registroAtividade = AdapterUtils.getInstantiableEntityInfo("RSCRegistroAtividade");
			NeoObject registro = registroAtividade.createNewInstance();
			EntityWrapper wRegistro = new EntityWrapper(registro);

			if (NeoUtils.safeIsNull(origin))
			{
				NeoPaper papel = new NeoPaper();
				papel = OrsegupsUtils.getPaper("sistemaFusionPaper");
				NeoUser usuarioResponsavel = new NeoUser();
				if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
				{
					for (NeoUser user : papel.getUsers())
					{
						usuarioResponsavel = user;
						break;
					}
				}

				wRegistro.findField("descricao").setValue("Tarefa avançada automaticamente do POOL, pois não foi atendida no prazo!");
				wRegistro.findField("responsavel").setValue(usuarioResponsavel);
				wRegistro.findField("dataInicialRSC").setValue(new GregorianCalendar());
				wRegistro.findField("dataFinalRSC").setValue(new GregorianCalendar());
				
				if(((Long) processEntity.findValue("etapaVerificacaoEficacia")) == 1L)
				{
					wRegistro.findField("atividade").setValue("1ª Verificação Eficácia - Sol. Cliente / Verificação Eficácia - Sol. Cliente - Escalada");
				}
				else if(((Long) processEntity.findValue("etapaVerificacaoEficacia")) == 2L)
				{
					wRegistro.findField("atividade").setValue("2ª Verificação Eficácia - Sol. Cliente / Verificação Eficácia - Sol. Cliente - Escalada");
				}
				else if(((Long) processEntity.findValue("etapaVerificacaoEficacia")) == 3L)
				{
					wRegistro.findField("atividade").setValue("3ª Verificação Eficácia - Sol. Cliente / Verificação Eficácia - Sol. Cliente - Escalada");
				}
				else if(((Long) processEntity.findValue("etapaVerificacaoEficacia")) == 4L)
				{
					wRegistro.findField("atividade").setValue("4ª Verificação Eficácia - Sol. Cliente / Verificação Eficácia - Sol. Cliente - Escalada");
				}
				else if(((Long) processEntity.findValue("etapaVerificacaoEficacia")) == 5L)
				{
					wRegistro.findField("atividade").setValue("5ª Verificação Eficácia - Sol. Cliente / Verificação Eficácia - Sol. Cliente - Escalada");
				}
				
				prazoDeadLine = rscUtils.retornaPrazoDeadLine("Verificação Eficácia - Sol. Cliente - Escalada", new GregorianCalendar());
				processEntity.findField("prazoVerificacaoEficaciaEscalada").setValue(prazoDeadLine);
				wRegistro.findField("prazo").setValue(prazoDeadLine);
			}
			else
			{
				wRegistro.findField("responsavel").setValue(origin.getUser());
				wRegistro.findField("dataInicialRSC").setValue(origin.getStartDate());
				wRegistro.findField("dataFinalRSC").setValue(origin.getFinishDate());

				/* Histórico de Atividade do Fluxo C027.099 - RSC - Verificação de Eficácia */
				if (activity.getProcess().getModel().getName().equals("C027.099 - RSC - Verificação de Eficácia"))
				{
					if (origin.getActivityName().equalsIgnoreCase("1ª Verificação Eficácia - Sol. Cliente"))
					{
						if (origin.getFinishByUser() == null)
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Verificação Eficácia - Sol. Cliente - Escalada", (GregorianCalendar) processEntity.findValue("prazoVerificacaoEficacia"));
							processEntity.findField("prazoVerificacaoEficaciaEscalada").setValue(prazoDeadLine);
							
							String des = "1ª. Verificação de Eficácia não realizada pelo colaborador " + origin.getUser().getFullName();
							
							wRegistro.findField("descricao").setValue(des);
							wRegistro.findField("atividade").setValue("1ª Verificação Eficácia - Sol. Cliente");
							wRegistro.findField("prazo").setValue(prazoDeadLine);
						}
						else
						{
							String txtDescricao = rscUtils.retornaDescricaoVerificacaoEficacia((List<NeoObject>) processEntity.findValue("historicoOcorrencias"));
							
							wRegistro.findField("atividade").setValue("1ª Verificação Eficácia - Sol. Cliente");
							wRegistro.findField("descricao").setValue(txtDescricao);
						}
					}
					else if (origin.getActivityName().equalsIgnoreCase("2ª Verificação Eficácia - Sol. Cliente"))
					{
						if (origin.getFinishByUser() == null)
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Verificação Eficácia - Sol. Cliente - Escalada", (GregorianCalendar) processEntity.findValue("prazoVerificacaoEficacia"));
							processEntity.findField("prazoVerificacaoEficaciaEscalada").setValue(prazoDeadLine);
							
							String des = "2ª. Verificação de Eficácia não realizada pelo colaborador " + origin.getUser().getFullName();
							wRegistro.findField("descricao").setValue(des);
							wRegistro.findField("atividade").setValue("2ª Verificação Eficácia - Sol. Cliente");
							wRegistro.findField("prazo").setValue(prazoDeadLine);
							
						}
						else
						{
							String txtDescricao = rscUtils.retornaDescricaoVerificacaoEficacia((List<NeoObject>) processEntity.findValue("historicoOcorrencias"));
							
							wRegistro.findField("atividade").setValue("2ª Verificação Eficácia - Sol. Cliente");
							wRegistro.findField("descricao").setValue(txtDescricao);
						}
					}
					else if (origin.getActivityName().equalsIgnoreCase("3ª Verificação Eficácia - Sol. Cliente"))
					{
						if (origin.getFinishByUser() == null)
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Verificação Eficácia - Sol. Cliente - Escalada", (GregorianCalendar) processEntity.findValue("prazoVerificacaoEficacia"));
							processEntity.findField("prazoVerificacaoEficaciaEscalada").setValue(prazoDeadLine);
							
							String des = "3ª. Verificação de Eficácia não realizada pelo colaborador " + origin.getUser().getFullName();
							wRegistro.findField("descricao").setValue(des);
							wRegistro.findField("atividade").setValue("3ª Verificação Eficácia - Sol. Cliente");
							wRegistro.findField("prazo").setValue(prazoDeadLine);
						}
						else
						{
							String txtDescricao = rscUtils.retornaDescricaoVerificacaoEficacia((List<NeoObject>) processEntity.findValue("historicoOcorrencias"));
							
							wRegistro.findField("atividade").setValue("3ª Verificação Eficácia - Sol. Cliente");
							wRegistro.findField("descricao").setValue(txtDescricao);
						}
					}
					else if (origin.getActivityName().equalsIgnoreCase("4ª Verificação Eficácia - Sol. Cliente"))
					{
						if (origin.getFinishByUser() == null)
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Verificação Eficácia - Sol. Cliente - Escalada", (GregorianCalendar) processEntity.findValue("prazoVerificacaoEficacia"));
							processEntity.findField("prazoVerificacaoEficaciaEscalada").setValue(prazoDeadLine);							
							
							String des = "4ª. Verificação de Eficácia não realizada pelo colaborador " + origin.getUser().getFullName();
							wRegistro.findField("descricao").setValue(des);
							wRegistro.findField("atividade").setValue("4ª Verificação Eficácia - Sol. Cliente");
							wRegistro.findField("prazo").setValue(prazoDeadLine);
						}
						else
						{
							String txtDescricao = rscUtils.retornaDescricaoVerificacaoEficacia((List<NeoObject>) processEntity.findValue("historicoOcorrencias"));
							
							wRegistro.findField("atividade").setValue("4ª Verificação Eficácia - Sol. Cliente");
							wRegistro.findField("descricao").setValue(txtDescricao);
						}
					}
					else if (origin.getActivityName().equalsIgnoreCase("5ª Verificação Eficácia - Sol. Cliente"))
					{
						if (origin.getFinishByUser() == null)
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Verificação Eficácia - Sol. Cliente - Escalada", (GregorianCalendar) processEntity.findValue("prazoVerificacaoEficacia"));
							processEntity.findField("prazoVerificacaoEficaciaEscalada").setValue(prazoDeadLine);		
							
							String des = "5ª. Verificação de Eficácia não realizada pelo colaborador " + origin.getUser().getFullName();
							wRegistro.findField("descricao").setValue(des);
							wRegistro.findField("atividade").setValue("5ª Verificação Eficácia - Sol. Cliente");
							wRegistro.findField("prazo").setValue(prazoDeadLine);
						}
						else
						{
							String txtDescricao = rscUtils.retornaDescricaoVerificacaoEficacia((List<NeoObject>) processEntity.findValue("historicoOcorrencias"));
							
							wRegistro.findField("atividade").setValue("5ª Verificação Eficácia - Sol. Cliente");
							wRegistro.findField("descricao").setValue(txtDescricao);
						}
					}
					else if (origin.getActivityName().equalsIgnoreCase("Verificação Eficácia - Sol. Cliente - Escalada"))
					{
						if (origin.getFinishByUser() == null)
						{
							String des = "Verificação de Eficácia não realizada pelo colaborador " + origin.getUser().getFullName();
							wRegistro.findField("descricao").setValue(des);
							wRegistro.findField("atividade").setValue("Verificação Eficácia - Sol. Cliente - Escalada");
							
							String papel = (String) processEntity.findValue("responsavelExecutor.name");
							if (papel.contains("Diretor") || papel.contains("Presidente"))
							{
								prazoDeadLine = rscUtils.retornaPrazoDeadLine("Verificação Eficácia - Sol. Cliente - Escalada Diretoria", (GregorianCalendar) processEntity.findValue("prazoVerificacaoEficaciaEscalada"));
								processEntity.findField("prazoVerificacaoEficaciaEscaladaDiretoria").setValue(prazoDeadLine);
								processEntity.findField("souPessoaResponsavel").setValue(false);
								processEntity.findField("continuarRSC").setValue(true);


								wRegistro.findField("prazo").setValue(prazoDeadLine);
								
								rscUtils.registrarTarefaSimples(processEntity, activity);
							}
							else
							{
								prazoDeadLine = rscUtils.retornaPrazoDeadLine("Verificação Eficácia - Sol. Cliente - Escalada", (GregorianCalendar) processEntity.findValue("prazoVerificacaoEficaciaEscalada"));
								processEntity.findField("prazoVerificacaoEficaciaEscalada").setValue(prazoDeadLine);

								wRegistro.findField("prazo").setValue(prazoDeadLine);
							}
						}
						else
						{
							String txtDescricao = rscUtils.retornaDescricaoVerificacaoEficacia((List<NeoObject>) processEntity.findValue("historicoOcorrencias"));
							
							wRegistro.findField("atividade").setValue("Verificação Eficácia - Sol. Cliente - Escalada");
							wRegistro.findField("descricao").setValue(txtDescricao);
						}
					}
					else if (origin.getActivityName().equalsIgnoreCase("Verificação Eficácia - Sol. Cliente - Escalada Diretoria"))
					{
						if (origin.getFinishByUser() == null)
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Verificação Eficácia - Sol. Cliente - Escalada Presidência", (GregorianCalendar) processEntity.findValue("prazoVerificacaoEficaciaEscaladaDiretoria"));
							processEntity.findField("prazoVerificacaoEficaciaEscaladaPresidencia").setValue(prazoDeadLine);		
							
							String des = "Verificação de Eficácia não realizada pelo colaborador " + origin.getUser().getFullName();
							wRegistro.findField("descricao").setValue(des);
							wRegistro.findField("atividade").setValue("Verificação Eficácia - Sol. Cliente - Escalada Diretoria");
							wRegistro.findField("prazo").setValue(prazoDeadLine);
							
							rscUtils.registrarTarefaSimples(processEntity, activity);
						}
						else
						{
							String txtDescricao = rscUtils.retornaDescricaoVerificacaoEficacia((List<NeoObject>) processEntity.findValue("historicoOcorrencias"));
							
							wRegistro.findField("atividade").setValue("Verificação Eficácia - Sol. Cliente - Escalada Diretoria");
							wRegistro.findField("descricao").setValue(txtDescricao);
						}
					}
					else if (origin.getActivityName().equalsIgnoreCase("Verificação Eficácia - Sol. Cliente - Escalada Presidência"))
					{
						String txtDescricao = rscUtils.retornaDescricaoVerificacaoEficacia((List<NeoObject>) processEntity.findValue("historicoOcorrencias"));
					
						wRegistro.findField("atividade").setValue("Verificação Eficácia - Sol. Cliente - Escalada Presidência");
						wRegistro.findField("descricao").setValue(txtDescricao);
					}

					if (origin.getFinishByUser() != null)
					{
						processEntity.setValue("ultimoStatusEficacia", rscUtils.retornaStatusEficacia((List<NeoObject>) processEntity.findValue("historicoOcorrencias")));
					}
				}
			}

			/* Alteração Boleto NF */
			if (NeoUtils.safeIsNotNull(processEntity.findValue("RscCategoriaAlteracaoBoletoNF")))
			{
				NeoObject rscVerificaoEficacia = (NeoObject) processEntity.findValue("RscCategoriaAlteracaoBoletoNF");
				verEfiWrapper = new EntityWrapper(rscVerificaoEficacia);
			}

			/* Atualização Cadastral */
			if (NeoUtils.safeIsNotNull(processEntity.findValue("RscCategoriaAtualizacaoCadastral")))
			{
				NeoObject rscVerificaoEficacia = (NeoObject) processEntity.findValue("RscCategoriaAtualizacaoCadastral");
				verEfiWrapper = new EntityWrapper(rscVerificaoEficacia);
			}

			/* Cancelamento */
			if (NeoUtils.safeIsNotNull(processEntity.findValue("RscCategoriaCancelamento")))
			{
				NeoObject rscVerificaoEficacia = (NeoObject) processEntity.findValue("RscCategoriaCancelamento");
				verEfiWrapper = new EntityWrapper(rscVerificaoEficacia);
			}

			/* Diversos */
			if (NeoUtils.safeIsNotNull(processEntity.findValue("RscCategoriaDiversos")))
			{
				NeoObject rscVerificaoEficacia = (NeoObject) processEntity.findValue("RscCategoriaDiversos");
				verEfiWrapper = new EntityWrapper(rscVerificaoEficacia);
			}

			/* Orçamento */
			if (NeoUtils.safeIsNotNull(processEntity.findValue("RscCategoriaOrcamento")))
			{
				NeoObject rscVerificaoEficacia = (NeoObject) processEntity.findValue("RscCategoriaOrcamento");
				verEfiWrapper = new EntityWrapper(rscVerificaoEficacia);
			}
			if (NeoUtils.safeIsNotNull(processEntity.findValue("RscCategoriaOrcamentoVisita")))
			{
				NeoObject rscVerificaoEficacia = (NeoObject) processEntity.findValue("RscCategoriaOrcamentoVisita");
				verEfiWrapper = new EntityWrapper(rscVerificaoEficacia);
			}
			
			NeoObject rscRegistroSolicitacaoCliente = (NeoObject) verEfiWrapper.findValue("RscRelatorioSolicitacaoCliente");
			EntityWrapper rscWrapper = new EntityWrapper(rscRegistroSolicitacaoCliente);

			PersistEngine.persist(registro);
			rscWrapper.findField("registroAtividades").addValue(registro);
		}
		catch (Exception e)
		{
			log.error(mensagem);
			e.printStackTrace();
			throw new WorkflowException(mensagem);
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}
}
