package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.ProcessState;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class AbreTarefaSimplesAdmissao implements CustomJobAdapter
{
	@Override
	public void execute(CustomJobContext arg0)
	{
		Connection conn = PersistEngine.getConnection("VETORH");
		StringBuilder sql = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.AbreTarefaSimplesAdmissao");
		log.warn("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Simples Adminissão - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		Long key = GregorianCalendar.getInstance().getTimeInMillis(); 

		try
		{
			sql.append(" SELECT fun.NumEmp, fun.TipCol, fun.NumCad, fun.NumCpf, fun.DatAdm, fun.NomFun,orn.USU_CodReg, car.TitRed, orn.USU_LotOrn, orn.NomLoc, reg.USU_NomReg ");
			sql.append(" FROM R034FUN fun WITH (NOLOCK) ");
			sql.append(" INNER JOIN R024CAR car WITH (NOLOCK) ON car.EstCar = fun.EstCar AND car.CodCar = fun.CodCar ");
			sql.append(" INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.TabOrg = fun.TabOrg AND orn.NumLoc = fun.NumLoc ");
			sql.append(" INNER JOIN USU_T200REG reg WITH (NOLOCK) ON reg.USU_CodReg = orn.usu_codreg ");
			sql.append(" WHERE ((fun.numemp in (27,28,29) AND fun.datadm >= '2017-06-01') or (fun.numemp not in (27,28,29) and fun.DatAdm > '2014-11-17')) ");
			sql.append(" AND NOT EXISTS (SELECT * FROM [CACUPE\\SQL02].fusion_producao.DBO.d_TarefaAdmissao tad WITH (NOLOCK) ");
			sql.append(" 			     WHERE tad.numemp = fun.numemp AND tad.tipcol = fun.tipcol AND tad.numcad = fun.numcad ");
			sql.append("			     AND CAST(floor(CAST(tad.datadm AS FLOAT)) AS datetime) = CAST(floor(CAST(fun.datadm AS FLOAT)) AS datetime)) ");
			sql.append(" AND orn.USU_CodReg IN (1,2,3,4,5,6,7,9,10,11,12,13,14,15) AND car.SisCar = 3 AND fun.conRho = 2 AND fun.tipCol = 1 ");
			sql.append(" and 2 >= ( select count(*) from r038hlo hlo inner join r034fun f on hlo.numcad = f.numcad and hlo.numemp = f.numemp and hlo.tipcol = f.tipcol where f.numcpf = fun.numcpf ) "); // essa garante que ao transferir um funcionário, não seja aberta tarefa de adimissão. Tarefa 619054 

			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery(); 
			List<Long> listCpf = new ArrayList<Long>();

			while (rs.next())
			{	
			    	Long cpf = rs.getLong("NumCpf");
			    	Long numCad = rs.getLong("NumCad");
			    	Long numEmp = rs.getLong("NumEmp");
			    	
			    	QLGroupFilter qlGroup = new QLGroupFilter("AND");
			    	qlGroup.addFilter(new QLEqualsFilter("numcad", numCad));
			    	qlGroup.addFilter(new QLEqualsFilter("numemp", numEmp));
			    	NeoObject noTarefa = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("TarefaAdmissao"), qlGroup);
			    	
			    	if(NeoUtils.safeIsNotNull(noTarefa)){
        			    	EntityWrapper wTarefa = new EntityWrapper(noTarefa);
        			    	String tarefa = wTarefa.findGenericValue("tarefa");
        			    	
        			    	WFProcess wf = PersistEngine.getNeoObject(WFProcess.class, new QLEqualsFilter("code", tarefa));	 
        			    	
        			    	if(wf.getProcessState() == ProcessState.running){
        			    	    continue;
        			    	}
			    	}
			    	
			    	if(listCpf.contains(cpf)){
			    	    continue;
			    	}			    	
			    	listCpf.add(cpf);
			    	
				String solicitante = OrsegupsUtils.getUserNeoPaper("G001SolicitanteAdmissao");
				String executor = OrsegupsUtils.getUserNeoPaper("G001ExecutorAdmissao");
				String titulo = "Justificar Admissão - " + rs.getInt("NumEmp") + "/" + rs.getInt("NumCad") + " - " + rs.getString("NomFun");
				String descricao = "Justificar a admissão do colaborador " + rs.getInt("NumEmp") + "/" + rs.getInt("NumCad") + " - " + rs.getString("NomFun") + ", " + rs.getString("TitRed") +", Regional: "+rs.getString("USU_NomReg")+
						", informando o motivo de não ter utilizado colaborador da Cobertura de Férias.";
				GregorianCalendar prazo = new GregorianCalendar();
				prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
				prazo.set(Calendar.HOUR_OF_DAY, 23);
				prazo.set(Calendar.MINUTE, 59);
				prazo.set(Calendar.SECOND, 59);
				
				Long codReg = rs.getLong("USU_CodReg");
				Long codEmp = rs.getLong("NumEmp");
				String lotacao = rs.getString("USU_LotOrn");

				/*NeoPaper papel = new NeoPaper();
				
				if (codEmp != 18) {
					papel = OrsegupsUtils.getPapelGerenteRegional(codReg);
				} else {
					papel = papel = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code","Coordenador de Vigilância Eletrônica"));
				}*/
				
				//papel = OrsegupsUtils.getPapelResponsavelASOSede(codReg, lotacao); 
			

				IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
				String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);
				
				InstantiableEntityInfo tarefaAD = AdapterUtils.getInstantiableEntityInfo("TarefaAdmissao");
				NeoObject noAD = tarefaAD.createNewInstance();
				EntityWrapper ajWrapper = new EntityWrapper(noAD);

				GregorianCalendar datAdm = new GregorianCalendar();
				datAdm.setTime(rs.getDate("DatAdm"));

				ajWrapper.findField("numemp").setValue(rs.getLong("NumEmp"));
				ajWrapper.findField("tipcol").setValue(rs.getLong("TipCol"));
				ajWrapper.findField("numcad").setValue(rs.getLong("NumCad"));
				ajWrapper.findField("datadm").setValue(datAdm);
				ajWrapper.findField("tarefa").setValue(tarefa);
				PersistEngine.persist(noAD);

				log.warn("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Admissão - Responsavel " + executor + " Colaborador: " + rs.getInt("NumEmp") + "/" + rs.getInt("NumCad") + " - Data: " + NeoUtils.safeDateFormat(rs.getDate("DatAdm"), "dd/MM/yyyy"));
				
				Thread.sleep(1000); 
				
			}
		}
		catch (Exception e)
		{
			log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Admissão");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
		}
		finally
		{

			try
			{
				rs.close();
				pstm.close();
				conn.close();
			}
			catch (SQLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			log.warn("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Simples Admissão - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}
}
