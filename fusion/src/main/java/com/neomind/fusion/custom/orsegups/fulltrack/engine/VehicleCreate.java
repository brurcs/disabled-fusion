package com.neomind.fusion.custom.orsegups.fulltrack.engine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.neomind.fusion.custom.orsegups.autocargo.imports.messages.SIGMAClientInsertError;
import com.neomind.fusion.custom.orsegups.autocargo.imports.messages.SIGMAClientInsertSucess;
import com.neomind.fusion.custom.orsegups.fulltrack.bean.FulltrackCliente;
import com.neomind.fusion.custom.orsegups.fulltrack.bean.FulltrackContato;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;

public class VehicleCreate {
    
    public void createVehicles() {

	StringBuilder sql = new StringBuilder();
	
	sql.append(" SELECT CLI.*,V.ID AS ID_VEICULO, V.PLACA, V.VEICULO, V.ANO, V.COR, C.ID_CENTRAL FROM FULLTRACK_CLIENTE CLI "); 
	sql.append(" INNER JOIN FULLTRACK_VEICULO V ON V.ID_CLIENTE = CLI.ID ");
	sql.append(" INNER JOIN [FSOODB03\\SQL01].SIGMA90.dbo.dbCENTRAL C WITH(NOLOCK) ON C.CD_CLIENTE = CLI.CD_CLIENTE ");
	sql.append(" WHERE V.CD_CLIENTE IS NULL ");
	

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	List<FulltrackCliente> clientes = new ArrayList<FulltrackCliente>();

	try {
	    conn = PersistEngine.getConnection("IMPORTACOES");

	    stmt = conn.createStatement();

	    rs = stmt.executeQuery(sql.toString());

	    while (rs.next()) {
		FulltrackCliente novoCliente = new FulltrackCliente();

		novoCliente.setRas_cli_id(rs.getInt("ID_VEICULO"));
		novoCliente.setRas_cli_razao(rs.getString(3).toUpperCase());
		novoCliente.setRas_cli_endereco(rs.getString(4).toUpperCase());
		novoCliente.setRas_cli_bairro(rs.getString(5).toUpperCase());
		novoCliente.setRas_cli_cep(rs.getString(6).replace("-", "").replace(" ", ""));
		novoCliente.setRas_cli_uf(rs.getString(7).toUpperCase());
		novoCliente.setRas_cli_cidade(rs.getString(8).toUpperCase());
		novoCliente.setRas_cli_cnpj(rs.getString(9));
		
		novoCliente.setRas_cli_desc(rs.getString("PLACA"));		
		novoCliente.setRas_cli_liberado(rs.getString("ID_CENTRAL"));
		
		String veiculo = rs.getString("VEICULO") != null ? rs.getString("VEICULO") : "";
		String cor = rs.getString("COR") != null ? rs.getString("COR") : "NÃO INFORMADA";
		String anoFabricacao = rs.getString("ANO"); 
		
		
		String referencia = veiculo+" COR: "+cor+" ANO/MODELO: "+anoFabricacao+" PLACA: "+novoCliente.getRas_cli_desc();
		
		novoCliente.setRas_cli_tipo(referencia);
		
		
		clientes.add(novoCliente);
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	this.clientInsert(clientes);

    }

    private void clientInsert(List<FulltrackCliente> clientes) {

	for (FulltrackCliente c : clientes) {

	    StringBuilder retorno = new StringBuilder();

	    try {

		StringBuilder params = new StringBuilder();

		params.append("HTTP://192.168.20.218:8080/SigmaWebServices/ClientInsert?");
		params.append("companyId=10144");
		params.append("&account=");
		params.append(c.getRas_cli_liberado());
		
		String particao = this.getParticao(c.getRas_cli_liberado());
		
		params.append("&partition=");
		params.append(particao);
		
		String nome = c.getRas_cli_razao().trim().toUpperCase();
		
		if (nome.length()> 50){
		    nome = nome.substring(0, 50);
		}
		
		params.append("&corporationName=");
		params.append(URLEncoder.encode(nome, "UTF-8"));
		params.append("&tradeName=");
		params.append(URLEncoder.encode(nome, "UTF-8"));
		
		List<FulltrackContato> contatos = ContatoFromService.run(c.getRas_cli_id());
		
		if (contatos != null && !contatos.isEmpty()){
		    
		    FulltrackContato con = contatos.get(0);
		    
		    params.append("&responsible=");
		    
		    String contato = con.getRas_ccn_contato();
		    
		    if (contato.length()> 50){
			contato = contato.substring(0, 50);
		    }
		    
		    params.append(URLEncoder.encode(contato, "UTF-8"));
		    
		    params.append("&phone1=");
		    
		    String telefone = con.getRas_ccn_num_telefone();
		    
		    if (telefone.length()> 14){
			telefone = telefone.substring(0, 14);
		    }
		    
		    params.append(URLEncoder.encode(telefone, "UTF-8"));
		    
		    params.append("&phone2=");
		    
		    String celular = con.getRas_ccn_num_celular();
		    
		    if (celular.length()> 14){
			celular = celular.substring(0, 14);
		    }
		       
		    params.append(URLEncoder.encode(celular, "UTF-8"));
		    
		}
		
		if (c.getRas_cli_cep() != null && !c.getRas_cli_cep().isEmpty()) {
		    params.append("&zipCode=");
		    params.append(c.getRas_cli_cep());

		}

		if (c.getRas_cli_endereco() != null && !c.getRas_cli_endereco().isEmpty()) {
		    params.append("&address=");

		    String endereco = c.getRas_cli_endereco().toUpperCase();

		    params.append(URLEncoder.encode(endereco, "UTF-8"));
		}

		params.append("&stateId=");
		params.append(c.getRas_cli_uf().toUpperCase());

		int codigoCidade = this.getCodigoCidade(c.getRas_cli_uf().toUpperCase().trim(), c.getRas_cli_cidade().toUpperCase().trim());

		if (codigoCidade != 0) {
		    params.append("&cityId=");
		    params.append(codigoCidade);

		    int codigoBairro = this.getCodigoBairro(codigoCidade, c.getRas_cli_bairro().toUpperCase().trim());

		    if (codigoBairro != 0) {			
			params.append("&districtId=");
			params.append(codigoBairro);
		    }else{
			params.append("&districtId=10346");
		    }

		} else {
		    params.append("&cityId=10075");
		    params.append("&districtId=10346");
		}

		params.append("&complement=");
		params.append(c.getRas_cli_id());
		
		params.append("&tracker=");
		params.append(URLEncoder.encode(c.getRas_cli_desc().trim(),"UTF-8"));
		
		params.append("&references=");
		params.append(URLEncoder.encode(c.getRas_cli_tipo().trim(),"UTF-8"));


		params.append("&route1Id=86007");
		params.append("&installerId=89427");
		params.append("&sellerId=89427");
		params.append("&branchActivityId=10001");
		params.append("&controlPartition=1");
		params.append("&active=1");
		params.append("&unifyPartition=0");

		System.out.println(params.toString());

		URL url = new URL(params.toString());
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");

		System.out.println(conn.getResponseCode());
		if (conn.getResponseCode() != 200) {
		    this.logErrorClient("Failed : HTTP error code : " + conn.getResponseCode(), c.getRas_cli_id());
		    continue;
		} else {
		    BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

		    String output;

		    while ((output = br.readLine()) != null) {
			retorno.append(output);
		    }

		    conn.disconnect();

		    String resposta = retorno.toString();

		    Gson gson = new Gson();

		    SIGMAClientInsertError erro = null;
		    SIGMAClientInsertSucess sucesso = null;
		    if (resposta.contains("errors")) {
			erro = gson.fromJson(resposta, SIGMAClientInsertError.class);
			
			StringBuilder msg = new StringBuilder();
			
			HashMap<String, String> erros = erro.getErrors();
			
			for (String key : erros.keySet()){
			    msg.append("Campo: "+key+" Problema: "+erros.get(key)+ " ");
			}
			
			int update = this.logErrorClient(msg.toString(), c.getRas_cli_id());
			
			int count = 0;
			
			while (update < 1 && count < 2){
			    update = this.logErrorClient(msg.toString(), c.getRas_cli_id());
			    count ++;
			}
			
			System.out.println("Problema! "+msg.toString());
			
		    } else {
			sucesso = gson.fromJson(resposta, SIGMAClientInsertSucess.class);
			
			System.out.println("Sucesso!");
			
			int update = this.setCdCliente(c.getRas_cli_id(), sucesso.getEntityId());
			
			int count = 0;
			
			while (update < 1 && count < 2){
			    update = this.setCdCliente(c.getRas_cli_id(), sucesso.getEntityId());
			    count ++;
			}
						
		    }
		}

	    } catch (MalformedURLException e) {

		e.printStackTrace();

	    } catch (IOException e) {

		e.printStackTrace();

	    }

	}

    }

    private String getParticao(String idCentral) {
	
	String sql = "SELECT MAX(CAST(PARTICAO AS INT)) FROM dbCENTRAL WHERE ID_CENTRAL = ? AND ID_EMPRESA=10144";
	
	String retorno = "";
		
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	
	int particao = 0;

	try {
	    conn = PersistEngine.getConnection("SIGMA90");
	    pstm = conn.prepareStatement(sql);
	    pstm.setString(1, idCentral);

	    rs = pstm.executeQuery();

	    if (rs.next()) {
		particao = rs.getInt(1);
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
	
	particao ++;
	
	if (particao < 10){
	    retorno = "00"+particao;
	}else if (particao < 100){
	    retorno = "0"+particao;
	}else{
	    retorno = String.valueOf(particao);
	}
	
	
	return retorno;
    }
    
    private int getCodigoCidade(String estado, String cidade) {

	String sql = "SELECT MAX(C.ID_CIDADE) FROM dbCIDADE C WITH(NOLOCK) WHERE C.NOME=? AND C.ID_ESTADO=? ";

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	try {
	    conn = PersistEngine.getConnection("SIGMA90");
	    pstm = conn.prepareStatement(sql);
	    pstm.setString(1, cidade);
	    pstm.setString(2, estado);

	    rs = pstm.executeQuery();

	    if (rs.next()) {
		return rs.getInt(1);
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

	return 0;
    }

    private int getCodigoBairro(int codigoCidade, String bairro) {

	String sql = "SELECT B.ID_BAIRRO FROM dbBAIRRO B WITH(NOLOCK) WHERE B.ID_CIDADE = ? AND B.NOME = ? ";

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	try {
	    conn = PersistEngine.getConnection("SIGMA90");
	    pstm = conn.prepareStatement(sql);
	    pstm.setInt(1, codigoCidade);
	    pstm.setString(2, bairro);

	    rs = pstm.executeQuery();

	    if (rs.next()) {
		return rs.getInt(1);
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

	return 0;
    }
    
    private int logErrorClient(String msg, int idCliente){
	
	String sql = "UPDATE FULLTRACK_VEICULO SET ERROS = ? WHERE ID = ?";
	
	Connection conn = null;
	PreparedStatement pstm = null;

	try {
	    conn = PersistEngine.getConnection("IMPORTACOES");
	    pstm = conn.prepareStatement(sql);
	    pstm.setString(1, msg);
	    pstm.setInt(2, idCliente);

	    return pstm.executeUpdate();

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}
	
	return 0;

    }
    
    private int setCdCliente (int idCliente, int cdCliente){
	
	String sql = "UPDATE FULLTRACK_VEICULO SET CD_CLIENTE = ? WHERE ID = ?";
	
	Connection conn = null;
	PreparedStatement pstm = null;

	try {
	    conn = PersistEngine.getConnection("IMPORTACOES");
	    pstm = conn.prepareStatement(sql);
	    pstm.setInt(1, cdCliente);
	    pstm.setInt(2, idCliente);

	    return pstm.executeUpdate();

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}
	
	return 0;
	
    }

}
