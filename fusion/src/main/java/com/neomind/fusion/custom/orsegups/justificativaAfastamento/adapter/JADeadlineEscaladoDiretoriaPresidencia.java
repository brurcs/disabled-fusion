package com.neomind.fusion.custom.orsegups.justificativaAfastamento.adapter;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class JADeadlineEscaladoDiretoriaPresidencia implements AdapterInterface {

    @Override
    public void start(Task origin, EntityWrapper processEntity, Activity activity) {
	GregorianCalendar prazo = new GregorianCalendar();
	prazo.add(Calendar.DAY_OF_MONTH, +10);
	
	prazo.set(Calendar.HOUR_OF_DAY, 23);
	prazo.set(Calendar.MINUTE, 59);
	prazo.set(Calendar.SECOND, 59);
	
	while (!OrsegupsUtils.isWorkDay(prazo)) {
	    prazo.add(Calendar.DAY_OF_MONTH, +1);
	}
	processEntity.findField("isEscalada").setValue(true);
	processEntity.findField("prazo").setValue(prazo);
	
    }

    @Override
    public void back(EntityWrapper processEntity, Activity activity) {
	// TODO Auto-generated method stub
	
    }

}
