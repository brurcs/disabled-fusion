package com.neomind.fusion.custom.orsegups.ti.sigma.beans;

import java.util.Date;

public class Recepcao {
    private String cdRecepcao;
    private Date dtRecepcao;
    private String dtRecepcaoFormatada;
    private String nmReceptora;
    private String cdCentral;
    private String nuParticao;
    private String cdEvento;
    

    public String getCdEvento() {
        return cdEvento;
    }
    public void setCdEvento(String cdEvento) {
        this.cdEvento = cdEvento;
    }
    public String getCdRecepcao() {
        return cdRecepcao;
    }
    public void setCdRecepcao(String cdRecepcao) {
        this.cdRecepcao = cdRecepcao;
    }
    public Date getDtRecepcao() {
        return dtRecepcao;
    }
    public void setDtRecepcao(Date dtRecepcao) {
        this.dtRecepcao = dtRecepcao;
    }
    public String getDtRecepcaoFormatada() {
        return dtRecepcaoFormatada;
    }
    public void setDtRecepcaoFormatada(String dtRecepcaoFormatada) {
        this.dtRecepcaoFormatada = dtRecepcaoFormatada;
    }
    public String getNmReceptora() {
        return nmReceptora;
    }
    public void setNmReceptora(String nmReceptora) {
        this.nmReceptora = nmReceptora;
    }
    public String getCdCentral() {
        return cdCentral;
    }
    public void setCdCentral(String cdCentral) {
        this.cdCentral = cdCentral;
    }
    public String getNuParticao() {
        return nuParticao;
    }
    public void setNuParticao(String nuParticao) {
        this.nuParticao = nuParticao;
    }
    
    
}
