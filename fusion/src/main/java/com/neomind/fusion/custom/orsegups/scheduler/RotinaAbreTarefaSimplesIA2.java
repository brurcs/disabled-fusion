package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.EventosAitAlertaVO;
import com.neomind.fusion.custom.orsegups.utils.EventosAlertAitLogVO;
import com.neomind.fusion.custom.orsegups.utils.IDAAITVO;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsConnectionUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class RotinaAbreTarefaSimplesIA2 implements CustomJobAdapter {
    private static final Log log = LogFactory.getLog(RotinaAbreTarefaSimplesIA2.class);
    private Map<Integer, String> serverInfo = OrsegupsConnectionUtils.getServerInfo("SIGMA");

    public void execute(CustomJobContext arg0) {
	List<String> regionais = new ArrayList<String>();
	regionais.add("IAI");
	regionais.add("BQE");
	regionais.add("BNU");
	regionais.add("JLE");
	regionais.add("LGS");
	regionais.add("CUA");
	regionais.add("SOO");
	regionais.add("CCO");
	regionais.add("RSL");
	regionais.add("JGS");
	regionais.add("CTA");
	regionais.add("CSC");
	regionais.add("TRO");
	Long key = GregorianCalendar.getInstance().getTimeInMillis();
	DecimalFormat decimalFormat = new DecimalFormat("0.00");
	String periodoInicial = "";
	String periodoFinal = "";
	if (NeoUtils.safeIsNotNull(regionais) && !regionais.isEmpty()) {

	    Calendar periodo = Calendar.getInstance();
	    periodoInicial = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "yyyy-MM-dd");
	    periodoFinal = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "yyyy-MM-dd");

	    if (periodo.get(Calendar.HOUR_OF_DAY) >= 0 && periodo.get(Calendar.HOUR_OF_DAY) < 6) {
		GregorianCalendar date = new GregorianCalendar();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date.getTime());

		int diasParaDecrementar = -1;
		cal.add(Calendar.DATE, diasParaDecrementar);
		date.setTime(cal.getTime());
		periodoInicial = NeoDateUtils.safeDateFormat(date, "yyyy-MM-dd");

		periodoInicial += " 17:00";
		periodoFinal += " 05:59";
	    } else if (periodo.get(Calendar.HOUR_OF_DAY) >= 6 && periodo.get(Calendar.HOUR_OF_DAY) < 18) {
		periodoInicial += " 06:00";
		periodoFinal += " 17:59";
	    } else if (periodo.get(Calendar.HOUR_OF_DAY) >= 18 ) {

		GregorianCalendar date = new GregorianCalendar();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date.getTime());

		int diasParaIncrementar = 1;
		cal.add(Calendar.DATE, diasParaIncrementar);
		date.setTime(cal.getTime());
		periodoFinal = NeoDateUtils.safeDateFormat(date, "yyyy-MM-dd");

		periodoInicial += " 17:00";
		periodoFinal += " 05:59";
	    }
	}

	for (String regional : regionais) {
	    Connection connSigma = null;
	    PreparedStatement stSeqSIG = null;
	    ResultSet rsSeqSIG = null;
	    Connection connFusion = null;
	    PreparedStatement stSeqCL = null;
	    PreparedStatement stSeqSL = null;
	    ResultSet rsSeqCL = null;
	    ResultSet rsSeqSL = null;
	    IDAAITVO graficoVO = null;

	    List<IDAAITVO> listaGraficoVO = new ArrayList<IDAAITVO>();
	    List<EventosAitAlertaVO> listaEventosAitAlertaVO = new ArrayList<EventosAitAlertaVO>();
	    List<EventosAlertAitLogVO> listaEventosAlertAitLogCLVO = new ArrayList<EventosAlertAitLogVO>();
	    List<EventosAlertAitLogVO> listaEventosAlertAitLogSLVO = new ArrayList<EventosAlertAitLogVO>();
	    List<String> contasList = new ArrayList<String>();
	    log.warn("##### INICIAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Produtividade IA2 - Regional: " + regional + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

	    try {
		StringBuilder select_EV = null;
		StringBuilder select_CL = null;
		StringBuilder select_SL = null;

		select_EV = new StringBuilder();

		select_EV.append(" SELECT VW.CD_VIATURA,V.NM_VIATURA,C.ID_CENTRAL,C.PARTICAO,VW.CD_HISTORICO FROM VIEW_HISTORICO VW WITH(NOLOCK)  ");
		select_EV.append(" INNER JOIN dbCENTRAL C WITH (NOLOCK) ON C.CD_CLIENTE = VW.CD_CLIENTE  ");
		select_EV.append(" INNER JOIN VIATURA V WITH (NOLOCK) ON VW.CD_VIATURA = V.CD_VIATURA  ");
		select_EV.append("  WHERE V.FG_ATIVO = 1  ");
		select_EV.append("  AND SUBSTRING(V.NM_VIATURA, 1, 3)  IN ('" + regional + "')   	 ");
		select_EV.append(" AND VW.DT_FECHAMENTO BETWEEN  '" + periodoInicial + "' AND '" + periodoFinal + "'  ");
		select_EV.append("  GROUP BY VW.CD_VIATURA,V.NM_VIATURA ,C.ID_CENTRAL,C.PARTICAO,VW.CD_HISTORICO  ");
		select_EV.append("  ORDER BY V.NM_VIATURA DESC    ");

		// select_EV.append(" SELECT VW.CD_VIATURA,V.NM_VIATURA,C.ID_CENTRAL,C.PARTICAO,VW.CD_HISTORICO  FROM [FSOODB04\\SQL02].Staging.dbo.VIEW_HISTORICO VW WITH(NOLOCK)  ");
		// select_EV.append(" INNER JOIN [FSOODB04\\SQL02].Staging.sigma90.dbCENTRAL C WITH (NOLOCK) ON C.CD_CLIENTE = VW.CD_CLIENTE ");
		// select_EV.append(" INNER JOIN [FSOODB04\\SQL02].Staging.sigma90.VIATURA V WITH (NOLOCK) ON VW.CD_VIATURA = V.CD_VIATURA ");
		// select_EV.append(" WHERE V.FG_ATIVO = 1 ");
		// select_EV.append(" AND SUBSTRING(V.NM_VIATURA, 1, 3)  IN ("+
		// regional +")  ");
		// select_EV.append(" AND VW.DT_FECHAMENTO BETWEEN  '" +
		// periodoInicial + "' AND '" + periodoFinal + "'  ");
		// select_EV.append(" GROUP BY VW.CD_VIATURA,V.NM_VIATURA ,C.ID_CENTRAL,C.PARTICAO,VW.CD_HISTORICO   ");
		// select_EV.append(" ORDER BY V.NM_VIATURA DESC  ");

		select_CL = new StringBuilder();

		select_CL.append("  SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, dataLog, dataLogFim))) /60  , 2) AS DECIMAL(9,2)) AS KPI, placaViatura ,  ");
		select_CL.append("  SUBSTRING(V.NM_VIATURA,1 , 3 ) AS REG , V.NM_VIATURA, COUNT(*) AS QTD, V.CD_VIATURA ,SL1.textoLog     ");
		select_CL.append("  FROM d_SIGMALogViatura SL1  WITH (NOLOCK)   ");
		select_CL.append("  INNER JOIN X_SIGMA90VIATURA VF WITH (NOLOCK) ON SL1.VIATURA_NEOID = VF.NEOID  ");
		select_CL.append("  INNER JOIN ["+this.serverInfo.get(OrsegupsConnectionUtils.NOME_SERVIDOR)+"\\"+this.serverInfo.get(OrsegupsConnectionUtils.INSTANCIA)+"].SIGMA90.dbo.VIATURA AS V WITH (NOLOCK)   ON V.CD_VIATURA = VF.CD_VIATURA  ");
		select_CL.append("  WHERE dataLogFim is not null and textoLog LIKE '%Sistema Fusion. Viatura em alerta%' AND V.FG_ATIVO = 1   and textoLog not like  '%excesso de tempo no local.%' ");
		select_CL.append("  AND SUBSTRING(V.NM_VIATURA, 1, 3) IN ('" + regional + "')  ");
		select_CL.append("  AND  EXISTS( SELECT SL2.placaViatura   ");
		select_CL.append("  FROM d_SIGMALogViatura   SL2  ");
		select_CL.append("  INNER JOIN X_SIGMA90VIATURA VF2 WITH (NOLOCK) ON SL2.VIATURA_NEOID = VF2.NEOID  ");
		select_CL.append("  INNER JOIN ["+this.serverInfo.get(OrsegupsConnectionUtils.NOME_SERVIDOR)+"\\"+this.serverInfo.get(OrsegupsConnectionUtils.INSTANCIA)+"].SIGMA90.dbo.VIATURA AS V2 WITH (NOLOCK)   ON V2.CD_VIATURA = VF.CD_VIATURA ");
		select_CL.append("  AND V.FG_ATIVO = 1  ");
		select_CL.append("  WHERE SL2.dataLog between SL1.dataLog   and SL1.dataLogFim ");
		select_CL.append("  AND textoLog LIKE '%Ligando para AIT devido ao alerta de deslocamento Chamada efetuada a partir%' ");
		select_CL.append("  AND SL2.placaViatura = SL1.placaViatura )  ");
		select_CL.append("  AND dataLog BETWEEN  '" + periodoInicial + "' AND '" + periodoFinal + "'  ");
		select_CL.append("  GROUP BY placaViatura,  SUBSTRING(V.NM_VIATURA,1 , 3 ) , V.NM_VIATURA , V.CD_VIATURA   ,textoLog   ");
		select_CL.append("  ORDER BY  NM_VIATURA  DESC ");

		select_SL = new StringBuilder();

		select_SL.append("  SELECT CAST(ROUND(AVG(CONVERT(FLOAT, DATEDIFF(SECOND, dataLog, dataLogFim))) /60  , 2) AS DECIMAL(9,2)) AS KPI, placaViatura ,  ");
		select_SL.append("  SUBSTRING(V.NM_VIATURA,1 , 3 ) AS REG , V.NM_VIATURA, COUNT(DISTINCT textoLog) AS QTD, V.CD_VIATURA   ,SL1.textoLog    ");
		select_SL.append("  FROM d_SIGMALogViatura SL1  WITH (NOLOCK)   ");
		select_SL.append("  INNER JOIN X_SIGMA90VIATURA VF WITH (NOLOCK) ON SL1.VIATURA_NEOID = VF.NEOID  ");
		select_SL.append("  INNER JOIN ["+this.serverInfo.get(OrsegupsConnectionUtils.NOME_SERVIDOR)+"\\"+this.serverInfo.get(OrsegupsConnectionUtils.INSTANCIA)+"].SIGMA90.dbo.VIATURA AS V WITH (NOLOCK)   ON V.CD_VIATURA = VF.CD_VIATURA  ");
		select_SL.append("  WHERE dataLogFim is not null and textoLog LIKE '%Sistema Fusion. Viatura em alerta%' AND V.FG_ATIVO = 1  and textoLog not like  '%excesso de tempo no local.%' ");
		select_SL.append("  AND SUBSTRING(V.NM_VIATURA, 1, 3) IN ('" + regional + "')  ");
		select_SL.append("  AND NOT  EXISTS( SELECT SL2.placaViatura   ");
		select_SL.append("  FROM d_SIGMALogViatura   SL2  ");
		select_SL.append("  INNER JOIN X_SIGMA90VIATURA VF2 WITH (NOLOCK) ON SL2.VIATURA_NEOID = VF2.NEOID  ");
		select_SL.append("  INNER JOIN ["+this.serverInfo.get(OrsegupsConnectionUtils.NOME_SERVIDOR)+"\\"+this.serverInfo.get(OrsegupsConnectionUtils.INSTANCIA)+"].SIGMA90.dbo.VIATURA AS V2 WITH (NOLOCK)   ON V2.CD_VIATURA = VF.CD_VIATURA ");
		select_SL.append("  AND V.FG_ATIVO = 1  ");
		select_SL.append("  WHERE SL2.dataLog between SL1.dataLog   and SL1.dataLogFim ");
		select_SL.append("  AND textoLog LIKE '%Ligando para AIT devido ao alerta de deslocamento Chamada efetuada a partir%' ");
		select_SL.append("  AND SL2.placaViatura = SL1.placaViatura )  ");
		select_SL.append("  AND dataLog BETWEEN  '" + periodoInicial + "' AND '" + periodoFinal + "'   ");
		select_SL.append("  GROUP BY placaViatura,  SUBSTRING(V.NM_VIATURA,1 , 3 ) , V.NM_VIATURA , V.CD_VIATURA   ,textoLog  ");
		select_SL.append("  ORDER BY NM_VIATURA DESC  ");

		if (select_EV != null) {
		    // connSigma = PersistEngine.getConnection("STAGING");
		    connSigma = PersistEngine.getConnection("SIGMA90");

		    stSeqSIG = connSigma.prepareStatement(select_EV.toString());
		    rsSeqSIG = stSeqSIG.executeQuery();
		    EventosAitAlertaVO eventosAitAlertaVO = null;
		    while (rsSeqSIG.next()) {
			eventosAitAlertaVO = new EventosAitAlertaVO();
			eventosAitAlertaVO.setTotal(1);
			eventosAitAlertaVO.setIdCentral(rsSeqSIG.getString("ID_CENTRAL"));
			eventosAitAlertaVO.setCdViatura(rsSeqSIG.getString("CD_VIATURA"));
			eventosAitAlertaVO.setCdHistorico(rsSeqSIG.getString("CD_HISTORICO"));
			eventosAitAlertaVO.setNmViatura(rsSeqSIG.getString("NM_VIATURA"));

			eventosAitAlertaVO.setParticao(rsSeqSIG.getString("PARTICAO"));
			listaEventosAitAlertaVO.add(eventosAitAlertaVO);
		    }

		    OrsegupsUtils.closeConnection(connSigma, stSeqSIG, rsSeqSIG);
		    connFusion = PersistEngine.getConnection("FUSIONPROD");

		    stSeqCL = connFusion.prepareStatement(select_CL.toString());
		    rsSeqCL = stSeqCL.executeQuery();

		    EventosAlertAitLogVO eventosAlertAitLogVO = null;

		    while (rsSeqCL.next()) {

			String textoLog = rsSeqCL.getString("textoLog");
			for (EventosAitAlertaVO eventosAitAlertaVO2 : listaEventosAitAlertaVO) {
			    String conta = eventosAitAlertaVO2.getIdCentral() + "[" + eventosAitAlertaVO2.getParticao() + "]";

			    if (textoLog != null && textoLog.contains(conta) && !contasList.contains(conta)) {
				contasList.add(conta);
				eventosAlertAitLogVO = new EventosAlertAitLogVO();
				eventosAlertAitLogVO.setCdViatura(rsSeqCL.getString("CD_VIATURA"));
				eventosAlertAitLogVO.setKpi(rsSeqCL.getDouble("KPI"));
				eventosAlertAitLogVO.setNmViatura(rsSeqCL.getString("NM_VIATURA"));
				eventosAlertAitLogVO.setPlacaViatura(rsSeqCL.getString("placaViatura"));
				eventosAlertAitLogVO.setReg(rsSeqCL.getString("REG"));
				eventosAlertAitLogVO.setQtd(rsSeqCL.getInt("QTD"));
				eventosAlertAitLogVO.setTextoLog(rsSeqCL.getString("textoLog"));
				listaEventosAlertAitLogCLVO.add(eventosAlertAitLogVO);
			    }
			}

		    }
		    OrsegupsUtils.closeConnection(connFusion, stSeqCL, rsSeqCL);
		    
		    connFusion = PersistEngine.getConnection("FUSIONPROD");
		    stSeqSL = connFusion.prepareStatement(select_SL.toString());
		    rsSeqSL = stSeqSL.executeQuery();
		    int countSl = 0;
		    while (rsSeqSL.next()) {

			String textoLog = rsSeqSL.getString("textoLog");

			if (contasList != null) {
			    for (EventosAitAlertaVO eventosAitAlertaVO2 : listaEventosAitAlertaVO) {
				countSl++;
				String conta = eventosAitAlertaVO2.getIdCentral() + "[" + eventosAitAlertaVO2.getParticao() + "]";
				if (textoLog != null && textoLog.contains(conta) && !contasList.contains(conta)) {
				    contasList.add(conta);
				    countSl = 0;
				    break;
				}
			    }
			}

			if (countSl == 0) {
			    eventosAlertAitLogVO = new EventosAlertAitLogVO();
			    eventosAlertAitLogVO.setCdViatura(rsSeqSL.getString("CD_VIATURA"));
			    eventosAlertAitLogVO.setKpi(rsSeqSL.getDouble("KPI"));
			    eventosAlertAitLogVO.setNmViatura(rsSeqSL.getString("NM_VIATURA"));
			    eventosAlertAitLogVO.setPlacaViatura(rsSeqSL.getString("placaViatura"));
			    eventosAlertAitLogVO.setReg(rsSeqSL.getString("REG"));
			    eventosAlertAitLogVO.setQtd(rsSeqSL.getInt("QTD"));
			    eventosAlertAitLogVO.setTextoLog(rsSeqSL.getString("textoLog"));
			    listaEventosAlertAitLogSLVO.add(eventosAlertAitLogVO);

			}

		    }
		    OrsegupsUtils.closeConnection(connFusion, stSeqSL, rsSeqSL);
		}

		// UNIFICAR CONSULTAS
		String nmVtr = "";
		int countQnt = 0;

		for (EventosAitAlertaVO eventosAitAlertaVO : listaEventosAitAlertaVO) {

		    if (countQnt == 0) {
			nmVtr = eventosAitAlertaVO.getNmViatura();
		    }
		    if (nmVtr.equals(eventosAitAlertaVO.getNmViatura())) {
			countQnt++;
		    } else {

			graficoVO = new IDAAITVO();
			if (nmVtr != null)
			    graficoVO.setNomeViatura(nmVtr);
			graficoVO.setTotalEvento(countQnt);
			if (graficoVO != null)
			    listaGraficoVO.add(graficoVO);
			countQnt = 0;
			nmVtr = eventosAitAlertaVO.getNmViatura();
			countQnt++;

		    }

		}

		graficoVO = new IDAAITVO();
		if (nmVtr != null)
		    graficoVO.setNomeViatura(nmVtr);
		graficoVO.setTotalEvento(countQnt);
		listaGraficoVO.add(graficoVO);

		int alerta = 0;
		double alertaTempo = 0.0;

		for (IDAAITVO graficoVO2 : listaGraficoVO) {
		    for (EventosAlertAitLogVO eventosAlertAitLogVO : listaEventosAlertAitLogCLVO) {

			if (graficoVO2.getNomeViatura().equals(eventosAlertAitLogVO.getNmViatura())) {

			    alerta++;
			    alertaTempo += eventosAlertAitLogVO.getKpi();
			    if (graficoVO2.getTotalEvento() < alerta)
				break;
			}
		    }
		    graficoVO2.setAlertaComLigacao(alerta);
		    graficoVO2.setAlertaComLigacaoTempo(alertaTempo);
		    alerta = 0;
		    alertaTempo = 0.0;

		    for (EventosAlertAitLogVO eventosAlertAitLogVO : listaEventosAlertAitLogSLVO) {

			if (graficoVO2.getNomeViatura().equals(eventosAlertAitLogVO.getNmViatura())) {

			    alerta++;
			    alertaTempo += eventosAlertAitLogVO.getKpi();
			    if (graficoVO2.getTotalEvento() < alerta)
				break;
			}
		    }
		    graficoVO2.setAlertaSemLigacao(alerta);
		    graficoVO2.setAlertaSemLigacaoTempo(alertaTempo);
		    alerta = 0;
		    alertaTempo = 0.0;
		}

		for (IDAAITVO vo : listaGraficoVO) {
		    double ida = (((vo.getAlertaComLigacao() * vo.getAlertaComLigacaoTempo()) + (vo.getAlertaSemLigacao() * vo.getAlertaSemLigacaoTempo())) + 1) / vo.getTotalEvento();
		    vo.setTotalEventoTempo(ida);
		}

		Collections.sort(listaGraficoVO);
		if (listaGraficoVO != null && !listaGraficoVO.isEmpty()) {
		    for (IDAAITVO vo : listaGraficoVO) {
			
			if (vo.getTotalEvento() > 0 && (vo.getAlertaComLigacao() > 0 || vo.getAlertaSemLigacao() > 0)){
			    
				String solicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteIA2");
				String titulo = "Acompanhar produtividade do AIT - " + listaGraficoVO.get(0).getNomeViatura() + " - Regional: " + regional + " ";

				Date date = new Date();

				GregorianCalendar prazo = new GregorianCalendar();

				prazo.setTime(date);
				prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
				prazo.set(Calendar.HOUR_OF_DAY, 23);
				prazo.set(Calendar.MINUTE, 59);
				prazo.set(Calendar.SECOND, 59);

				String descricao = "";
				descricao = "<strong><u>Acompanhar produtividade do AIT que esta abaixo da média:</u></strong><br><br>";
				descricao = descricao + " <strong>AIT :</strong> " + vo.getNomeViatura() + "<br>";
				descricao = descricao + " <strong>Alertas C/L :</strong> " + vo.getAlertaComLigacao() + "  <strong> Tempo :</strong> " + vo.getAlertaComLigacaoTempo() + " min <br>";
				descricao = descricao + " <strong>Alertas S/L :</strong> " + vo.getAlertaSemLigacao() + "  <strong> Tempo :</strong> " + vo.getAlertaSemLigacaoTempo() + " min <br>";
				descricao = descricao + " <strong>Eventos Atendidos :</strong> " + vo.getTotalEvento() + "<br>";
				descricao = descricao + " <strong>IDA :</strong> " + decimalFormat.format(vo.getTotalEventoTempo()) + "<br>";

				NeoPaper papel = new NeoPaper();
				Long codReg = OrsegupsUtils.getCodigoRegional(regional);
				String executor = "";
				if (NeoUtils.safeIsNotNull(codReg) && codReg != 13L && codReg != 9L && codReg != 1L)
				    papel = OrsegupsUtils.getPapelCoordenadorRegional(codReg, null);
				else if (NeoUtils.safeIsNotNull(codReg) && codReg == 1L)
				    papel = OrsegupsUtils.getPaper("Coordenador de Vigilância Eletrônica");
				else if (NeoUtils.safeIsNotNull(codReg) && codReg == 13L)
				    papel = OrsegupsUtils.getPaper("Coordenador Área Tática CTA");

				if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty()) {
				    for (NeoUser user : papel.getUsers()) {
					executor = user.getCode();
					break;
				    }
				} else {
				    log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Produtividade IA2 - Regra: Coordenador responsável não encontrado - Papel " + papel.getName());
				    continue;
				}

				if (executor != null && !executor.isEmpty()) {
				    IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
				    String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);
				    System.out.println(" AGENDADOR DE TAREFA: Abrir Tarefa Simples Produtividade IA2 - TAREFA " + tarefa);

				}

				break;
			}


		    }
		}

	    } catch (Exception e) {
		log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Produtividade IA2 - ERRO AO GERAR TAREFA - Regional:  " + regional);
		System.out.println("[" + key + "] ##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Produtividade IA2 - ERRO AO GERAR TAREFA - Regional:  " + regional);
		e.printStackTrace();
		throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	    } finally {

		try {
		    OrsegupsUtils.closeConnection(connFusion, stSeqCL, rsSeqCL);
		    OrsegupsUtils.closeConnection(connFusion, stSeqSL, rsSeqSL);
		    OrsegupsUtils.closeConnection(connSigma, stSeqSIG, rsSeqSIG);
		} catch (Exception e) {
		    log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Produtividade IA2 ou Ordens de Serviço em Excesso - ERRO DE EXECUÇÃO - Regional: " + regional);
		    e.printStackTrace();
		}

		log.warn("##### FINALIZAR  AGENDADOR DE TAREFA: Abrir Tarefa Simples Produtividade IA2 e Ordens de Serviço em Excesso - Regional: " + regional + " Data: "
			+ NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	    }
	}
    }
}
