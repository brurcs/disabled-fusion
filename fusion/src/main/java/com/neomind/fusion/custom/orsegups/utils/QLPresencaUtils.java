package com.neomind.fusion.custom.orsegups.utils;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.contract.ContratoUtils;
import com.neomind.fusion.custom.orsegups.e2doc.controller.E2docHandler;
import com.neomind.fusion.custom.orsegups.presenca.vo.AcessoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.AfastamentoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.AnotacaoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ApuracaoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ArmaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ChecklistVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ChipGPRSVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ClienteVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ColaboradorVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ContaSigmaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ContaSigmaVinculoHumanaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ContraChequeVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.DocumentoTreeVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EnderecoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EntradaAutorizadaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EquipamentoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EscalaPostoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EscalaRevezamentoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EscalaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EventoSigmaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EventosColaboradorVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.FichaEpiVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.FinancasTituloVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.HistoricoCargoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.HistoricoEscalaPostoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.HistoricoEscalaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.HistoricoLocalVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.HorarioVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.HorariosEscalaHorarioVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.HorariosEscalaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.InspecaoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.JornadaPostoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.JustificativaMarcacaoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.LeituraMensagemTIVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.MensagemTIVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ObsHoristaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ParameterVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.PostoSupervisaoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.PostoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.PresencaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.RTOciosoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ReclamacaoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.RegionalVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.SituacaoAcessoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.SituacoesApuracaoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.SolicitacaoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.StatusVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.TarefaFaltaEfetivoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.UsuT160JorVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.UsuarioFusionVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ValeTransporteVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.VisitaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.X8VO;
import com.neomind.fusion.custom.orsegups.site.vo.CompetenciaVO;
import com.neomind.fusion.datawarehouse.ExternalEntityInfo;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.entity.dynamic.DocumentEntityInfo;
import com.neomind.fusion.i18n.I18nUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.security.SecurityEntity;
import com.neomind.fusion.workflow.ProcessState;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.adapter.casvig.OrsegupsContratoUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class QLPresencaUtils
{
    private static final Log log = LogFactory.getLog(QLPresencaUtils.class);
    private static Map<Integer, String> serverInfo = OrsegupsConnectionUtils.getServerInfo("SIGMA");

    public static Collection<ClienteVO> getClientes(Collection<ColaboradorVO> colaboradores, Collection<EntradaAutorizadaVO> entradasAutorizadas, List<EscalaPostoVO> escalas, Long codigoRegional, Long codigoEmpresa, Long codigoOrganograma, GregorianCalendar dataAtual, Boolean isContratos, Long codigoTipCli, String codigoLocal, Boolean isAsseio)
    {

	Connection connVetorh = PersistEngine.getConnection("VETORH");
	Collection<ClienteVO> listaClientes = new ArrayList<ClienteVO>();
	Collection<PostoVO> listaPostos = new ArrayList<PostoVO>();

	GregorianCalendar dataHoje = new GregorianCalendar();

	String stringDataAtual = NeoUtils.safeDateFormat(dataAtual, "dd/MM/yyyy");
	String stringDataHoje = NeoUtils.safeDateFormat(dataHoje, "dd/MM/yyyy");
	Boolean diaAtual = true;

	//Data diferente da data atual
	if (!stringDataAtual.endsWith(stringDataHoje))
	{
	    diaAtual = false;
	}

	try
	{
	    /*** Recupera Postos ***/
	    StringBuffer queryPostos = new StringBuffer();
	    queryPostos.append(" SELECT SUBSTRING(hie.CodLoc, 1, 12) AS NumLocCli, orn.numloc, hie.codloc, SUBSTRING(hie.CodLoc, 1, 24) AS CodPai, cvs.usu_nomloc, cvs.usu_desser, pos.usu_codser, cvs.usu_numctr, cvs.usu_numpos, orn.usu_lotorn, ");
	    queryPostos.append(" 		cvs.usu_codreg, orn.usu_cliorn, cvs.usu_sitcvs, cvs.usu_qtdcon, cvs.usu_numloc, cvs.usu_codccu, cvs.usu_codemp, cvs.usu_codfil, cvs.usu_endloc, ");
	    queryPostos.append(" 		cid.NomCid, cid.EstCid, cvs.usu_endcep, orn.usu_cplctr,orn.usu_nomcid, cvs.usu_preuni * cvs.usu_qtdcvs AS valor, cvs.usu_datini, cvs.usu_datfim, cvs.usu_fimvig, ");
	    queryPostos.append(" 		cvs.usu_nomloc AS cvs_nomloc, cvs.usu_codccu AS cvs_codccu, cvs.usu_numctr AS cvs_numctr, cvs.usu_numpos AS cvs_numpos, pos.usu_peresc, mot.codmot,  mot.desmot ");
	    queryPostos.append(" FROM R016ORN orn WITH (NOLOCK) ");
	    queryPostos.append(" INNER JOIN USU_T038CVS cvs WITH (NOLOCK) ON cvs.usu_numloc = orn.numloc AND cvs.usu_taborg = orn.taborg AND cvs.usu_seqalt = (Select MAX(c2.usu_seqalt) From USU_T038CVS c2 Where c2.usu_taborg = cvs.usu_taborg And c2.usu_numloc = cvs.usu_numloc And c2.usu_datalt <= ?) ");
	    queryPostos.append(" INNER JOIN R016HIE hie WITH (NOLOCK) ON hie.taborg = cvs.usu_taborg AND cvs.usu_numloc = hie.numloc AND hie.datini = (SELECT MAX(h2.datini) FROM dbo.R016HIE h2 WHERE hie.taborg = cvs.usu_taborg AND h2.numloc = cvs.usu_numloc) ");
	    queryPostos.append(" INNER JOIN [FSOODB04\\SQL02].SAPIENS.dbo.USU_T160CVS pos ON pos.usu_codemp = cvs.usu_codemp AND pos.usu_codfil = cvs.usu_codfil AND pos.usu_numctr = cvs.usu_numctr AND pos.usu_numpos = cvs.usu_numpos AND pos.usu_codccu = cvs.usu_codccu ");
	    queryPostos.append(" LEFT JOIN R074CID cid WITH (NOLOCK) ON cid.CodCid = orn.usu_codcid ");
	    queryPostos.append(" LEFT JOIN [FSOODB04\\SQL02].SAPIENS.dbo.E021MOT mot ON mot.codmot = pos.usu_codmot ");
	    queryPostos.append(" WHERE (cvs.usu_taborg = ?) ");
	    queryPostos.append(" AND (LEN(hie.CodLoc) = 27) ");

	    if (diaAtual)
	    {
		queryPostos.append(" AND (cvs.USU_SITCVS = 'S' OR DATEADD(MINUTE, 1439, cvs.usu_datfim) >= ? OR cvs.usu_datfim = '1900-12-31' OR EXISTS (SELECT * FROM R034FUN fun WHERE fun.TIPCOL = 1 AND cvs.usu_numloc = fun.numloc AND cvs.usu_taborg = fun.taborg AND (fun.sitafa <> 7 OR (fun.sitafa = 7 AND fun.datafa > getDate())))) ");
	    }
	    else
	    {
		queryPostos.append(" AND (cvs.usu_sitcvs = 'S') ");
		queryPostos.append(" AND (DATEADD(MINUTE, 1439, cvs.usu_datfim) >= ? OR cvs.usu_datfim = '1900-12-31') ");
	    }

	    //Por Regional
	    if (NeoUtils.safeIsNotNull(codigoRegional))
	    {
		queryPostos.append(" AND cvs.usu_codreg = ? ");

		if (isContratos)
		{
		    queryPostos.append(" AND (hie.codloc LIKE '1.5%' OR hie.codloc LIKE '1.6%') ");

		    //Asseio
		    if (codigoTipCli > 0L)
		    {
			queryPostos.append(" AND (cvs.usu_codemp IN (2, 6, 7, 8,29) OR (cvs.usu_codemp IN (15) AND cvs.usu_desser like '%Serviço Extra - Apoio Administrativo%')) ");
		    }
		    else
		    {
			//SOO
			if (codigoRegional == 1)
			{
			    queryPostos.append(" AND cvs.usu_codemp IN (1, 2, 6, 7, 8, 15, 17, 18, 19, 21, 22, 27, 28,29) ");
			}
			else
			{
			    queryPostos.append(" AND cvs.usu_codemp IN (1, 7, 15, 17, 18, 19, 21, 22, 27, 28,29) ");
			}
			
			if (!isAsseio){
    			    queryPostos.append(" AND (((cvs.usu_codemp = 7 or cvs.usu_codemp = 29) AND (cvs.usu_desser not like '%servente%' AND cvs.usu_desser not like '%zelador%' AND cvs.usu_desser not like '%Aux. Serv. Gerais%' ");
    			    queryPostos.append(" AND cvs.usu_desser not like '%AUXILIAR%' AND cvs.usu_desser not like '%Jardineiro%' AND cvs.usu_desser not like '%SERVIÇO DE VIGILANCIA HUMANA MTA%' ");
    			    queryPostos.append(" AND cvs.usu_desser not like '%ENCARREGADO MTA%' AND cvs.usu_desser not like '%PTO DE AUX DE SERV GERAIS 9H DE SEG A QUI E 8H SEX MTA%' AND cvs.usu_desser not like '%RECEPCIONISTA MTA%' ");
    			    queryPostos.append(" AND cvs.usu_desser not like '%RECEPCIONISTA 8%' AND cvs.usu_desser not like '%PTO DE AUX DE SERV GERAIS 4H DE SEG A SEX E 2H SAB MTA%')) OR (cvs.usu_codemp <> 7 and cvs.usu_codemp <> 29)) ");
			}
			
			
			

		    }
		}
		else
		{
		    queryPostos.append(" AND (hie.codloc LIKE '1.1%' ) ");
		    queryPostos.append(" AND cvs.usu_codemp IN (1, 2, 6, 7, 8, 15, 17, 18, 19, 21, 22, 27, 28, 29) ");
		    if (codigoLocal != null && !codigoLocal.equals(""))
		    {
			queryPostos.append(" AND (hie.codloc LIKE '" + codigoLocal + "' ) ");
		    }
		}
		//Asseio
		if (codigoTipCli > 0L && codigoRegional == 9)
		{
		    queryPostos.append(" AND orn.usu_tipcli = ? ");
		}
	    }
	    //Por Empresa
	    else
	    {
		//queryPostos.append(" AND cvs.usu_codemp = 1 ");

		if (isContratos)
		{
		    queryPostos.append(" AND (hie.codloc LIKE '1.5%' OR hie.codloc LIKE '1.6%') ");
		}
		else
		{
		    queryPostos.append(" AND (hie.codloc LIKE '1.1%' ) ");
		    queryPostos.append(" AND cvs.usu_codemp IN (1, 2, 6, 7, 8, 15, 17, 18, 19, 21, 22, 27, 28, 29) ");
		    if (!isAsseio){
			queryPostos.append(" AND (((cvs.usu_codemp = 7 or cvs.usu_codemp = 29) AND (cvs.usu_desser not like '%servente%' AND cvs.usu_desser not like '%zelador%' AND cvs.usu_desser not like '%Aux. Serv. Gerais%' ");
			queryPostos.append(" AND cvs.usu_desser not like '%AUXILIAR%' AND cvs.usu_desser not like '%Jardineiro%' AND cvs.usu_desser not like '%SERVIÇO DE VIGILANCIA HUMANA MTA%' ");
			queryPostos.append(" AND cvs.usu_desser not like '%ENCARREGADO MTA%' AND cvs.usu_desser not like '%PTO DE AUX DE SERV GERAIS 9H DE SEG A QUI E 8H SEX MTA%' AND cvs.usu_desser not like '%RECEPCIONISTA MTA%' ");
			queryPostos.append(" AND cvs.usu_desser not like '%RECEPCIONISTA 8%')) OR (cvs.usu_codemp <> 7 and cvs.usu_codemp <> 29)) ");
		    }
		}
	    }

	    //Caso ADM ordenar pelo campo usu_ordexi, que será administrado pelo Sr. Valtair.
	    if (!isContratos)
	    {
		queryPostos.append(" ORDER BY orn.usu_cliorn, orn.usu_ordexi, hie.codloc ");
	    }
	    else
	    {
		queryPostos.append(" ORDER BY orn.usu_datfim desc, orn.usu_cliorn, cvs.usu_sitcvs DESC, hie.codloc ");
	    }

	    PreparedStatement stPostos = connVetorh.prepareStatement(queryPostos.toString());
	    stPostos.setTimestamp(1, new Timestamp(dataAtual.getTimeInMillis()));
	    stPostos.setLong(2, codigoOrganograma);
	    stPostos.setTimestamp(3, new Timestamp(dataAtual.getTimeInMillis()));

	    //Por Regional
	    if (NeoUtils.safeIsNotNull(codigoRegional))
	    {
		stPostos.setLong(4, codigoRegional);
		//Asseio
		if (codigoTipCli > 0L && codigoRegional == 9)
		{
		    stPostos.setLong(5, codigoTipCli);
		}
	    }
	    else
	    {
		//stPostos.setLong(4, codigoEmpresa);
	    }

	    ResultSet rsPostos = stPostos.executeQuery();

	    String codigoLocalClienteAnterior = "";
	    String nomeClienteAnterior = "";
	    Boolean primeiroRegistro = true;
	    ClienteVO cliente = new ClienteVO();
	    String codigoCliente = "";
	    String nomeCliente = "";

	    while (rsPostos.next())
	    {

		codigoCliente = rsPostos.getString("NumLocCli");
		nomeCliente = rsPostos.getString("usu_cliorn");
		PostoVO posto = new PostoVO();

		posto.setNomePosto(rsPostos.getString("usu_nomloc"));
		posto.setCentroCusto(rsPostos.getString("usu_codccu"));
		posto.setCodigoPai(rsPostos.getString("CodPai"));
		posto.setServico(rsPostos.getString("usu_desser"));
		posto.setSituacao((rsPostos.getString("usu_sitcvs").equals("S") ? "Ativo" : "Inativo"));
		posto.setVagas(rsPostos.getLong("usu_qtdcon"));
		posto.setNumeroContrato(rsPostos.getLong("usu_numctr"));
		posto.setNumeroPosto(rsPostos.getLong("usu_numpos"));
		posto.setEndereco(rsPostos.getString("usu_endloc"));
		posto.setCep(rsPostos.getString("usu_endcep"));
		posto.setValor(rsPostos.getBigDecimal("valor"));
		posto.setDataInicio(rsPostos.getDate("usu_datini"));
		posto.setDataFim(rsPostos.getDate("usu_datfim"));
		posto.setDataFimVigencia(rsPostos.getDate("usu_fimvig"));
		posto.setTelefone("");
		posto.setNumeroLocal(rsPostos.getLong("numloc"));
		posto.setCodigoLocal(rsPostos.getString("codloc"));
		posto.setCodigoOrganograma(codigoOrganograma);
		posto.setCidade(rsPostos.getString("nomcid"));
		posto.setEstado(rsPostos.getString("estcid"));
		posto.setLotacao(rsPostos.getString("usu_lotorn"));
		posto.setBairro(rsPostos.getString("usu_nomcid"));
		posto.setNumero(rsPostos.getString("usu_cplctr"));
		posto.setAjusteEscala(rsPostos.getLong("usu_peresc"));
		posto.setMotivoEncerramento(rsPostos.getString("desmot"));

		posto.setNumemp(rsPostos.getLong("usu_codemp"));
		posto.setNumfil(rsPostos.getLong("usu_codfil"));
		posto.setCodSer(rsPostos.getString("usu_codser"));

		//System.out.println(posto.getNumeroLocal());

		if (posto.getSituacao().equalsIgnoreCase("Ativo"))
		{
		    posto.setEscalaPosto(getJornadaEscalaPosto(posto.getNumeroLocal(), posto.getCodigoOrganograma(), escalas));

		    GregorianCalendar newDataAtual = (GregorianCalendar) dataAtual.clone();
		    newDataAtual.set(Calendar.HOUR_OF_DAY, 0);
		    newDataAtual.set(Calendar.MINUTE, 0);
		    newDataAtual.set(Calendar.SECOND, 0);
		    newDataAtual.set(Calendar.MILLISECOND, 0);

		    GregorianCalendar newDataFim = new GregorianCalendar();
		    newDataFim.setTime(posto.getDataFim());
		    newDataFim.set(Calendar.HOUR_OF_DAY, 0);
		    newDataFim.set(Calendar.MINUTE, 0);
		    newDataFim.set(Calendar.SECOND, 0);
		    newDataFim.set(Calendar.MILLISECOND, 0);

		    if (newDataFim.after(newDataAtual))
		    {
			posto.setSituacao("Encerrando");
		    }
		}

		if (diaAtual && posto.getDataInicio().after(dataAtual.getTime()))
		{
		    posto.setSituacao("Iniciando");
		}

		posto.setTelefone(getTelefonePosto(posto.getCentroCusto()));
		posto.setColaboradores(getColaboradores(colaboradores, posto, dataAtual));
		posto.setEntradasAutorizadas(getEntradasAutorizadas(entradasAutorizadas, posto, dataAtual));
		posto.setStatus(getStatusPosto(posto, dataAtual));

		if (primeiroRegistro)
		{
		    codigoLocalClienteAnterior = codigoCliente;
		    nomeClienteAnterior = nomeCliente;
		    primeiroRegistro = false;
		}

		if (!codigoLocalClienteAnterior.equals(codigoCliente))
		{
		    // Mudou o cliente, adiciona a lista de postos (antiga) ao cliente 
		    // e o cliente a lista de clientes, renovando o cliente e adicionando o
		    // novo posto a nova lista de postos
		    cliente.setCodigoCliente(codigoLocalClienteAnterior);
		    cliente.setNomeCliente(nomeClienteAnterior);
		    cliente.setPostos(listaPostos);
		    listaClientes.add(cliente);

		    listaPostos = new ArrayList<PostoVO>();
		    cliente = new ClienteVO();
		    codigoLocalClienteAnterior = codigoCliente;
		    nomeClienteAnterior = nomeCliente;
		}
		if (NeoUtils.safeIsNotNull(codigoEmpresa))
		{
		    if (posto.getColaboradores().size() > 0)
		    {
			listaPostos.add(posto);
		    }
		}
		else
		{
		    listaPostos.add(posto);
		}
	    }
	    cliente = new ClienteVO();
	    cliente.setCodigoCliente(codigoCliente);
	    cliente.setNomeCliente(nomeCliente);
	    cliente.setPostos(listaPostos);
	    listaClientes.add(cliente);

	    rsPostos.close();
	    stPostos.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}

	return listaClientes;
    }

    public static List<EscalaPostoVO> getEscalasPosto(Long codigoRegional, Long codigoEmpresa, Long codigoOrganograma, GregorianCalendar dataAtual, Boolean isContratos, Long codigoTipCli, String codigoLocal)
    {
	List<EscalaPostoVO> escalas = new ArrayList<EscalaPostoVO>();
	List<JornadaPostoVO> jornadas = listaEscalaPosto(codigoRegional, codigoEmpresa, codigoOrganograma, dataAtual, isContratos, codigoTipCli, codigoLocal);

	if (jornadas != null && !jornadas.isEmpty())
	{
	    ListIterator<JornadaPostoVO> i = jornadas.listIterator();  
	    while (i.hasNext()) 
	    {    
		JornadaPostoVO jor = i.next();
		JornadaPostoVO pro = null;

		if (i.hasNext())
		{
		    pro = i.next();
		    i.previous();
		}

		EscalaPostoVO escala = new EscalaPostoVO();
		escala.setTaborg(jor.getTaborg());
		escala.setNumloc(jor.getNumloc());
		escala.setDiaSemana(jor.getDiasem());
		escala.setPermanenciaPosto(jor.getPeresc());
		escala = buscaEscalaPosto(escala, dataAtual, jor.getHorini(), jor.getHorfim(), jor.getDiasem());

		while (pro != null && jor.getNumloc().toString().equals(pro.getNumloc().toString()))
		{
		    escala = buscaEscalaPosto(escala, dataAtual, pro.getHorini(), pro.getHorfim(), pro.getDiasem());
		    i.next();
		    if (i.hasNext())
		    {
			pro = i.next();
			i.previous();
		    }
		    else
		    {
			pro = null;
		    }
		} 
		escalas.add(escala);
	    } 
	}
	return escalas;
    }

    public static EscalaPostoVO buscaEscalaPosto(EscalaPostoVO escala, GregorianCalendar dataAtual, Long horini, Long horfim, Long diasem)
    {
	HorarioVO horario = new HorarioVO();
	horario.setDataInicial(convertHoraSenior(dataAtual, horini));
	horario.setDataFinal(convertHoraSenior(dataAtual, horfim));

	//Caso hora 00:00 adiciona um dia
	if (horario.getDataFinal().get(Calendar.HOUR_OF_DAY) == 0 && horario.getDataFinal().get(Calendar.MINUTE) == 0)
	{
	    horario.getDataFinal().add(Calendar.DAY_OF_MONTH, +1);
	}
	switch (diasem.intValue())
	{
	case 1:
	    if (escala.getHorariosSegunda() == null)
		escala.setHorariosSegunda(new ArrayList<HorarioVO>());
	    escala.getHorariosSegunda().add(horario);
	    break;
	case 2:
	    if (escala.getHorariosTerca() == null)
		escala.setHorariosTerca(new ArrayList<HorarioVO>());
	    escala.getHorariosTerca().add(horario);
	    break;
	case 3:
	    if (escala.getHorariosQuarta() == null)
		escala.setHorariosQuarta(new ArrayList<HorarioVO>());
	    escala.getHorariosQuarta().add(horario);
	    break;
	case 4:
	    if (escala.getHorariosQuinta() == null)
		escala.setHorariosQuinta(new ArrayList<HorarioVO>());
	    escala.getHorariosQuinta().add(horario);
	    break;
	case 5:
	    if (escala.getHorariosSexta() == null)
		escala.setHorariosSexta(new ArrayList<HorarioVO>());
	    escala.getHorariosSexta().add(horario);
	    break;
	case 6:
	    if (escala.getHorariosSabado() == null)
		escala.setHorariosSabado(new ArrayList<HorarioVO>());
	    escala.getHorariosSabado().add(horario);
	    break;
	case 7:
	    if (escala.getHorariosDomingo() == null)
		escala.setHorariosDomingo(new ArrayList<HorarioVO>());
	    escala.getHorariosDomingo().add(horario);
	    break;
	case 8:
	    if (escala.getHorariosFeriado() == null)
		escala.setHorariosFeriado(new ArrayList<HorarioVO>());
	    escala.getHorariosFeriado().add(horario);
	    break;
	default:
	    break;
	}
	return escala;
    }

    public static List<JornadaPostoVO> listaEscalaPosto(Long codigoRegional, Long codigoEmpresa, Long codigoOrganograma, GregorianCalendar dataAtual, Boolean isContratos, Long codigoTipCli, String codigoLocal)
    {
	List<JornadaPostoVO> jornadas = new ArrayList<JornadaPostoVO>();
	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    Boolean diaAtual = true;
	    GregorianCalendar dataHoje = new GregorianCalendar();
	    String stringDataAtual = NeoUtils.safeDateFormat(dataAtual, "dd/MM/yyyy");
	    String stringDataHoje = NeoUtils.safeDateFormat(dataHoje, "dd/MM/yyyy");

	    //Data diferente da data atual
	    if (!stringDataAtual.endsWith(stringDataHoje))
		diaAtual = false;

	    Integer diaSemana = dataAtual.get(Calendar.DAY_OF_WEEK) - 1;

	    if (diaSemana == 0)
		diaSemana = 7;

	    if (!OrsegupsUtils.isHoliday(dataAtual))
		diaSemana = 8;

	    StringBuffer query = new StringBuffer();

	    query.append(" SELECT orn.numloc, orn.taborg, jor.usu_horini, jor.usu_horfim, jor.usu_diasem, orn.usu_peresc ");
	    query.append(" FROM R016ORN orn  ");
	    query.append(" INNER JOIN USU_T038CVS cvs WITH (NOLOCK) ON cvs.usu_numloc = orn.numloc AND cvs.usu_taborg = orn.taborg AND cvs.usu_seqalt = (Select MAX(c2.usu_seqalt) From USU_T038CVS c2 Where c2.usu_taborg = cvs.usu_taborg And c2.usu_numloc = cvs.usu_numloc And c2.usu_datalt <= ?) ");
	    query.append(" INNER JOIN R016HIE hie WITH (NOLOCK) ON hie.taborg = cvs.usu_taborg AND cvs.usu_numloc = hie.numloc AND hie.datini = (SELECT MAX(h2.datini) FROM dbo.R016HIE h2 WHERE hie.taborg = cvs.usu_taborg AND h2.numloc = cvs.usu_numloc) ");
	    query.append(" LEFT JOIN [FSOODB04\\SQL02].SAPIENS.dbo.USU_T160CVS pos ON pos.usu_codemp = cvs.usu_codemp AND pos.usu_codfil = cvs.usu_codfil AND pos.usu_numctr = cvs.usu_numctr AND pos.usu_numpos = cvs.usu_numpos ");
	    query.append(" LEFT JOIN [FSOODB04\\SQL02].SAPIENS.dbo.USU_T160JOR jor ON jor.usu_codemp = cvs.usu_codemp AND jor.usu_codfil = cvs.usu_codfil AND jor.usu_numctr = cvs.usu_numctr AND jor.usu_numpos = cvs.usu_numpos ");
	    query.append(" WHERE (cvs.usu_taborg = ?) ");
	    query.append(" AND (LEN(hie.CodLoc) = 27) ");
	    query.append(" AND jor.usu_diasem = ? ");

	    if (diaAtual)
	    {
		query.append(" AND (cvs.USU_SITCVS = 'S' OR DATEADD(MINUTE, 1439, cvs.usu_datfim) >= ? OR cvs.usu_datfim = '1900-12-31' OR EXISTS (SELECT * FROM R034FUN fun WHERE fun.TIPCOL = 1 AND cvs.usu_numloc = fun.numloc AND cvs.usu_taborg = fun.taborg AND (fun.sitafa <> 7 OR (fun.sitafa = 7 AND fun.datafa > getDate())))) ");
	    }
	    else
	    {
		query.append(" AND (cvs.usu_sitcvs = 'S') ");
		query.append(" AND (DATEADD(MINUTE, 1439, cvs.usu_datfim) <= ? OR cvs.usu_datfim = '1900-12-31') ");
	    }

	    //Por Regional
	    if (NeoUtils.safeIsNotNull(codigoRegional))
	    {
		query.append(" AND cvs.usu_codreg = ? ");

		if (isContratos)
		{
		    query.append(" AND (hie.codloc LIKE '1.5%' OR hie.codloc LIKE '1.6%') ");

		    //Asseio
		    if (codigoTipCli > 0L)
		    {
			query.append(" AND cvs.usu_codemp IN (2, 6, 7, 8) ");
		    }
		    else
		    {
			//SOO
			if (codigoRegional == 1)
			{
			    query.append(" AND cvs.usu_codemp IN (1, 2, 6, 7, 8, 15, 17, 18, 19, 21, 22, 27, 28, 29) ");
			}
			else
			{
			    query.append(" AND cvs.usu_codemp IN (1, 7, 15, 17, 18, 19, 21, 22, 27, 28, 29) ");
			}
		    }
		}
		else
		{
		    query.append(" AND (hie.codloc LIKE '1.1%' ) ");
		    query.append(" AND cvs.usu_codemp IN (1, 2, 6, 7, 8, 15, 17, 18, 19, 21, 22, 27, 28, 29) ");

		    if (codigoLocal != null && !codigoLocal.equals(""))
		    {
			query.append(" AND (hie.codloc LIKE '" + codigoLocal + "' ) ");
		    }
		}
		//Asseio
		if (codigoTipCli > 0L && codigoRegional == 9)
		{
		    query.append(" AND orn.usu_tipcli = ? ");
		}
	    }
	    //Por Empresa
	    else
	    {
		if (isContratos)
		{
		    query.append(" AND (hie.codloc LIKE '1.5%' OR hie.codloc LIKE '1.6%') ");
		}
		else
		{
		    query.append(" AND (hie.codloc LIKE '1.1%' ) ");
		    query.append(" AND cvs.usu_codemp IN (1, 2, 6, 7, 8, 15, 17, 18, 19, 21, 22, 27, 28, 29) ");
		}
	    }

	    query.append(" ORDER BY orn.taborg, orn.numloc, jor.usu_diasem, jor.usu_horini, jor.usu_horfim ");

	    PreparedStatement st = connVetorh.prepareStatement(query.toString());
	    st.setTimestamp(1, new Timestamp(dataAtual.getTimeInMillis()));
	    st.setLong(2, codigoOrganograma);
	    st.setInt(3, diaSemana);
	    st.setTimestamp(4, new Timestamp(dataAtual.getTimeInMillis()));

	    //Por Regional
	    if (NeoUtils.safeIsNotNull(codigoRegional))
	    {
		st.setLong(5, codigoRegional);
		//Asseio
		if (codigoTipCli > 0L && codigoRegional == 9)
		{
		    st.setLong(6, codigoTipCli);
		}
	    }

	    ResultSet rs = st.executeQuery();

	    while (rs.next())
	    {
		JornadaPostoVO jornadaPosto = new JornadaPostoVO();
		jornadaPosto.setNumloc(rs.getLong("numloc"));
		jornadaPosto.setTaborg(rs.getLong("taborg"));
		jornadaPosto.setHorini(rs.getLong("usu_horini"));
		jornadaPosto.setHorfim(rs.getLong("usu_horfim"));
		jornadaPosto.setDiasem(rs.getLong("usu_diasem"));
		jornadaPosto.setPeresc(rs.getLong("usu_peresc"));
		jornadas.add(jornadaPosto);
	    }
	    rs.close();
	    st.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}
	return jornadas;
    }

    public static List<ColaboradorVO> getListaColaboradores(Long codigoRegional, Long codigoEmpresa, Long codigoOrganograma, List<EscalaVO> escalas, GregorianCalendar dataAtual, Long codigoTipCli, boolean isContratos, boolean isAsseio)
    {

	Connection connVetorh = PersistEngine.getConnection("VETORH");
	List<ColaboradorVO> listaColaboradores = new ArrayList<ColaboradorVO>();
	ColaboradorVO colaborador = new ColaboradorVO();

	PresencaVO presenca = new PresencaVO();
	GregorianCalendar entrada;
	GregorianCalendar saida;

	GregorianCalendar dataHoje = new GregorianCalendar();

	String stringDataAtual = NeoUtils.safeDateFormat(dataAtual, "dd/MM/yyyy");
	String stringDataHoje = NeoUtils.safeDateFormat(dataHoje, "dd/MM/yyyy");
	Boolean diaAtual = true;

	String numCraAnt = "";

	GregorianCalendar dataInicioCompetencia = new GregorianCalendar();
	GregorianCalendar dataFimCompetencia = new GregorianCalendar();

	try
	{
	    dataInicioCompetencia = OrsegupsUtils.getDiaInicioCompetencia(OrsegupsUtils.getCompetenciaApuracao(dataAtual));
	    dataFimCompetencia = OrsegupsUtils.getDiaFimCompetencia(OrsegupsUtils.getCompetenciaApuracao(dataAtual));
	    dataFimCompetencia.add(Calendar.DAY_OF_MONTH, -4);
	}
	catch (ParseException e1)
	{
	    e1.printStackTrace();
	}

	//Data diferente da data atual
	if (!stringDataAtual.endsWith(stringDataHoje))
	{
	    diaAtual = false;
	}

	try
	{
	    Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();

	    /*** Recupera Colaboradores ***/
	    StringBuffer queryColaborador = new StringBuffer();
	    queryColaborador.append(" SELECT fun.NumEmp, fun.TipCol, fun.NumCad, hch.NumCra, fun.NomFun, fun.NumCpf, fun.DatNas, fun.TipSex, fun.DatAdm, ");
	    queryColaborador.append("        esc.NomEsc, car.TitRed, sis.DesSis, hes.CodEsc, hes.CodTma, cvs.USU_NumLoc, cvs.USU_TabOrg, fun.EmiCar,  ");
	    queryColaborador.append("        ISNULL(afa.SitAfa, 1) AS SitAfa, ISNULL(sit.DesSit, 'Trabalhando') AS DesSit, ISNULL(sit.usu_blopre, 'N') AS BloPre, ");
	    queryColaborador.append("        afa.DatAfa AS dataAfa, afa.DatTer, fis.USU_NomFis, afaDem.DatAfa as dataDem, cauDem.DesDem, fun.USU_MatRes, ");
	    queryColaborador.append(" 		 DATEADD(MINUTE, accS.HorAcc, accS.DatAcc) AS DatAccS, accS.DirAcc AS DirAccS, accS.USU_TipDir AS TipDirS, accS.codrlg AS codrlgS, ");
	    queryColaborador.append("        DATEADD(MINUTE, accE.HorAcc, accE.DatAcc) AS DatAccE, accE.DirAcc AS DirAccE, accE.USU_TipDir AS TipDirE, accE.codrlg AS codrlgE, ");
	    queryColaborador.append("        COUNT(sto.CodSit) AS qdeExcecao ");
	    queryColaborador.append(" FROM R034FUN fun  ");
	    queryColaborador.append(" LEFT JOIN R038HCH hch WITH (NOLOCK) ON hch.NUMEMP = fun.NumEmp AND fun.TIPCOL = hch.TIPCOL AND fun.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch.HorIni, hch.DatIni) = (SELECT MAX(DATEADD(MINUTE, hch2.HorIni, hch2.DatIni)) FROM R038HCH hch2 WHERE hch2.NUMEMP = hch.NUMEMP AND hch2.TIPCOL = hch.TIPCOL AND hch2.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch2.HorIni, hch2.DatIni) <= ?)");
	    queryColaborador.append(" INNER JOIN R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp AND hlo.TipCol = fun.TipCol AND hlo.NumCad = fun.NumCad AND hlo.DatAlt = (SELECT MAX (DATALT) FROM R038HLO TABELA001 WHERE TABELA001.NUMEMP = hlo.NUMEMP AND TABELA001.TIPCOL = hlo.TIPCOL AND TABELA001.NUMCAD = hlo.NUMCAD AND TABELA001.DATALT <= ?) ");
	    queryColaborador.append(" INNER JOIN R038HES hes WITH (NOLOCK) ON hes.NumEmp = fun.NumEmp AND hes.TipCol = fun.TipCol AND hes.NumCad = fun.NumCad AND hes.DatAlt = (SELECT MAX (DATALT) FROM R038HES TABELA001 WHERE TABELA001.NUMEMP = hes.NUMEMP AND TABELA001.TIPCOL = hes.TIPCOL AND TABELA001.NUMCAD = hes.NUMCAD AND TABELA001.DATALT <= ?) ");
	    queryColaborador.append(" INNER JOIN R006ESC esc ON esc.CodEsc = hes.CodEsc ");
	    queryColaborador.append(" INNER JOIN R038HCA hca WITH (NOLOCK) ON hca.NumEmp = fun.NumEmp AND hca.TipCol = fun.TipCol AND hca.NumCad = fun.NumCad AND hca.DatAlt = (SELECT MAX (DATALT) FROM R038HCA TABELA001 WHERE TABELA001.NUMEMP = hca.NUMEMP AND TABELA001.TIPCOL = hca.TIPCOL AND TABELA001.NUMCAD = hca.NUMCAD AND TABELA001.DATALT <= ?) ");
	    queryColaborador.append(" INNER JOIN R024CAR car ON car.EstCar = hca.EstCar AND car.CodCar = hca.CodCar ");
	    queryColaborador.append(" LEFT JOIN R024SIS sis ON sis.SisCar = car.SisCar ");
	    queryColaborador.append(" INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.NumLoc = hlo.NumLoc and orn.TabOrg = ? ");
	    queryColaborador.append(" INNER JOIN USU_T038CVS cvs WITH (NOLOCK) ON cvs.USU_NumLoc = orn.NumLoc AND cvs.USU_TabOrg = orn.TabOrg AND cvs.USU_SeqAlt = (Select MAX(c2.usu_seqalt) FROM USU_T038CVS c2 Where c2.usu_taborg = cvs.usu_taborg And c2.usu_numloc = cvs.usu_numloc And c2.usu_datalt <= ?) ");
	    queryColaborador.append(" INNER JOIN USU_TFisRes fis ON fis.USU_FisRes = fun.usu_fisres ");
	    queryColaborador.append(" LEFT JOIN R038AFA afa WITH (NOLOCK) ON (afa.NumEmp = fun.NumEmp AND afa.TipCol = fun.TipCol AND afa.NumCad = fun.NumCad AND DATEADD(minute,afa.HORAFA,afa.DATAFA) = (SELECT MAX (DATEADD(minute,HORAFA,DATAFA)) FROM R038AFA TABELA001 WHERE TABELA001.NUMEMP = afa.NUMEMP AND TABELA001.TIPCOL = afa.TIPCOL AND TABELA001.NUMCAD = afa.NUMCAD AND TABELA001.DATAFA <= ? AND (TABELA001.DatTer = '1900-12-31 00:00:00' OR TABELA001.DatTer >= cast(floor(cast(? as float)) as datetime)))) ");
	    queryColaborador.append(" LEFT JOIN R038AFA afaDem WITH (NOLOCK) ON afaDem.NumEmp = fun.NumEmp AND afaDem.TipCol = fun.TipCol AND afaDem.NumCad = fun.NumCad AND afaDem.sitAfa = 7 AND DATEADD(minute,afaDem.HORAFA,afaDem.DATAFA) = (SELECT MAX (DATEADD(minute,HORAFA,DATAFA)) FROM R038AFA TABELA001 WHERE TABELA001.NUMEMP = afaDem.NUMEMP AND TABELA001.TIPCOL = afaDem.TIPCOL AND TABELA001.NUMCAD = afaDem.NUMCAD) ");
	    queryColaborador.append(" LEFT JOIN R042CAU cauDem ON cauDem.CauDem = afaDem.CauDem ");
	    queryColaborador.append(" LEFT JOIN R010SIT sit ON sit.CodSit = afa.SitAfa ");

	    if (isContratos)
	    {
		queryColaborador.append(" LEFT JOIN R070ACC accS ON (accS.NumCra = hch.NumCra AND accS.DatAcc > DATEADD(mm, -1, ?) AND accS.TipAcc = 100 AND accS.CodPlt = 2 AND accS.usu_TabOrg = hlo.TabOrg AND accS.usu_NumLoc = hlo.NumLoc AND accS.usu_tipdir in ('P') ");
		queryColaborador.append(" 			AND DATEADD(MINUTE, accS.HorAcc, accS.DatAcc) = (SELECT MAX(DATEADD(MINUTE, accS2.HorAcc, accS2.DatAcc)) FROM R070ACC accS2 WHERE accS2.NumCra = accS.NumCra AND accS2.DatAcc > DATEADD(mm, -1, ?) AND accS2.usu_TabOrg = accS.usu_TabOrg AND accS2.usu_NumLoc = accS.usu_NumLoc AND accS2.TipAcc = 100 AND accS2.CodPlt = 2 AND accS2.usu_tipdir = accS.usu_tipdir AND DATEADD(MINUTE, accS2.HorAcc, accS2.DatAcc) <= ?))  ");
		queryColaborador.append(" LEFT JOIN R070ACC accE ON (accE.NumCra = hch.NumCra AND accE.DatAcc > DATEADD(mm, -1, ?) AND accE.TipAcc = 100 AND accE.CodPlt = 2 AND accE.usu_TabOrg = hlo.TabOrg AND accE.usu_NumLoc = hlo.NumLoc AND accE.usu_tipdir in ('P') AND accE.DirAcc = 'E' ");
		queryColaborador.append(" 			AND DATEADD(MINUTE, accE.HorAcc, accE.DatAcc) = (SELECT MAX(DATEADD(MINUTE, accE2.HorAcc, accE2.DatAcc)) FROM R070ACC accE2 WHERE accE2.NumCra = accE.NumCra AND accE2.DatAcc > DATEADD(mm, -1, ?) AND accE2.usu_TabOrg = accE.usu_TabOrg AND accE2.usu_NumLoc = accE.usu_NumLoc AND accE2.TipAcc = 100 AND accE2.CodPlt = 2 AND accE2.usu_tipdir = accE.usu_tipdir AND accE2.DirAcc = 'E' AND DATEADD(MINUTE, accE2.HorAcc, accE2.DatAcc) <= ?))  ");
	    }
	    else
	    {
		queryColaborador.append(" LEFT JOIN R070ACC accS ON (accS.NumCra = hch.NumCra AND accS.DatAcc > DATEADD(mm, -1, ?) AND accS.TipAcc = 100 AND accS.CodPlt = 2 ");
		queryColaborador.append(" 			AND DATEADD(MINUTE, accS.HorAcc, accS.DatAcc) = (SELECT MAX(DATEADD(MINUTE, accS2.HorAcc, accS2.DatAcc)) FROM R070ACC accS2 WHERE accS2.NumCra = accS.NumCra AND accS2.DatAcc > DATEADD(mm, -1, ?) AND accS2.TipAcc = 100 AND accS2.CodPlt = 2 AND DATEADD(MINUTE, accS2.HorAcc, accS2.DatAcc) <= ?)) ");
		queryColaborador.append(" LEFT JOIN R070ACC accE ON (accE.NumCra = hch.NumCra AND accE.DatAcc > DATEADD(mm, -1, ?) AND accE.TipAcc = 100 AND accE.CodPlt = 2 AND accE.DirAcc = 'E' ");
		queryColaborador.append(" 			AND DATEADD(MINUTE, accE.HorAcc, accE.DatAcc) = (SELECT MAX(DATEADD(MINUTE, accE2.HorAcc, accE2.DatAcc)) FROM R070ACC accE2 WHERE accE2.NumCra = accE.NumCra AND accE2.DatAcc > DATEADD(mm, -1, ?) AND accE2.TipAcc = 100 AND accE2.CodPlt = 2 AND accE2.DirAcc = 'E' AND DATEADD(MINUTE, accE2.HorAcc, accE2.DatAcc) <= ?)) ");
	    }

	    //Apuracao
	    queryColaborador.append(" LEFT JOIN R066APU apu ON apu.NumEmp = fun.numemp AND apu.TipCol = fun.TipCol AND apu.NumCad = fun.NumCad AND apu.DatApu >= ? AND apu.DatApu <= ? AND fun.ConRho = 2 ");
	    queryColaborador.append(" LEFT JOIN R004HOR hor ON hor.CodHor = apu.HorDat AND hor.CodHor > 9000 ");
	    queryColaborador.append(" LEFT JOIN R066SIT sap ON sap.NumEmp = apu.NumEmp AND sap.TipCol = apu.TipCol AND sap.NumCad = apu.NumCad AND sap.DatApu = apu.DatApu AND sap.USU_ExcVal <> 'S' ");
	    queryColaborador.append(" LEFT JOIN R010SIT sto ON sto.CodSit = sap.CodSit AND sto.SitExc = 'S' ");

	    queryColaborador.append(" WHERE fun.DatAdm <= ? ");
	    queryColaborador.append(" AND (afa.SitAfa IS NULL OR afa.SitAfa <> 7 OR (afa.SitAfa = 7 AND afa.DatAfa > cast(floor(cast(? as float)) as datetime))) ");
	    queryColaborador.append(" AND fun.TipCol = 1 ");
	    queryColaborador.append(" AND fun.NumEmp not in (5, 11, 14) ");

	    //Por Regional
	    if (NeoUtils.safeIsNotNull(codigoRegional))
	    {
		queryColaborador.append(" AND cvs.usu_codreg = ? ");
		if (!isAsseio && isContratos){
		    queryColaborador.append(" AND ((fun.numemp = 7 AND (car.TitRed like '%vigia%' OR car.TitRed like '%ATENDENTE DE PISO%' OR car.TitRed = 'ATENDENTE' OR car.TitRed like '%ATENDENTE DE MALL%' OR car.TitRed like '%OPERADOR DE MOLL%' OR car.TitRed like '%PORTEIRO%' OR car.TitRed like '%BOMBEIRO%' OR car.TitRed like 'CONTROLADOR DE ACESSO')) OR fun.numemp <> 7) ");
		}
	    }
	    //Por Empresa
	    else
	    {
		queryColaborador.append(" AND fun.numemp = ? ");
		if (!isAsseio && isContratos){
		    if (codigoEmpresa != null && codigoEmpresa.equals(7L)){
			queryColaborador.append(" AND ((fun.numemp = 7 AND (car.TitRed like '%vigia%' OR car.TitRed like '%ATENDENTE DE PISO%' OR car.TitRed = 'ATENDENTE' OR car.TitRed like '%ATENDENTE DE MALL%' OR car.TitRed like '%OPERADOR DE MOLL%' OR car.TitRed like '%PORTEIRO%' OR car.TitRed like '%BOMBEIRO%' OR car.TitRed like 'CONTROLADOR DE ACESSO')) OR fun.numemp <> 7) ");
		    }
		}
	    }

	    queryColaborador.append(" GROUP BY orn.usu_codreg,fun.NumEmp, fun.TipCol, fun.NumCad, hch.NumCra, fun.NomFun, fun.NumCpf, fun.DatNas, fun.TipSex, fun.DatAdm, ");
	    queryColaborador.append(" 		   esc.NomEsc, car.TitRed, sis.DesSis, hes.CodEsc, hes.CodTma, cvs.USU_NumLoc, cvs.USU_TabOrg, fun.EmiCar, fun.CodTma, afa.SitAfa, ");
	    queryColaborador.append(" 		   sit.DesSit, sit.usu_blopre, accS.HorAcc, accS.DatAcc, accE.HorAcc, accE.DatAcc, accS.DirAcc, accE.DirAcc, accS.codrlg, accE.codrlg, ");
	    queryColaborador.append(" 		   accS.USU_TipDir, accE.USU_TipDir, afa.DatAfa, afa.DatTer, fis.USU_NomFis, afaDem.DatAfa, cauDem.DesDem, fun.USU_MatRes ");

	    //Asseio
	    if (codigoTipCli != 0L)
	    {
		queryColaborador.append(" ORDER BY fun.NumEmp, fun.NomFun ");
	    }
	    else
	    {
		queryColaborador.append(" ORDER BY fun.CodTma, esc.NomEsc, fun.NumEmp, fun.NomFun ");
	    }

	    Timestamp dataAtualTS = new Timestamp(dataAtual.getTimeInMillis());
	    Timestamp dataInicioCompTS = new Timestamp(dataInicioCompetencia.getTimeInMillis());
	    Timestamp dataFimCompTS = new Timestamp(dataFimCompetencia.getTimeInMillis());

	    PreparedStatement stColaborador = connVetorh.prepareStatement(queryColaborador.toString());
	    stColaborador.setTimestamp(1, dataAtualTS);
	    stColaborador.setTimestamp(2, dataAtualTS);
	    stColaborador.setTimestamp(3, dataAtualTS);
	    stColaborador.setTimestamp(4, dataAtualTS);
	    stColaborador.setLong(5, codigoOrganograma);
	    stColaborador.setTimestamp(6, dataAtualTS);
	    stColaborador.setTimestamp(7, dataAtualTS);
	    stColaborador.setTimestamp(8, dataAtualTS);
	    stColaborador.setTimestamp(9, dataAtualTS);
	    stColaborador.setTimestamp(10, dataAtualTS);
	    stColaborador.setTimestamp(11, dataAtualTS);
	    stColaborador.setTimestamp(12, dataAtualTS);
	    stColaborador.setTimestamp(13, dataAtualTS);
	    stColaborador.setTimestamp(14, dataAtualTS);
	    stColaborador.setTimestamp(15, dataInicioCompTS);
	    stColaborador.setTimestamp(16, dataFimCompTS);
	    stColaborador.setTimestamp(17, dataAtualTS);
	    stColaborador.setTimestamp(18, dataAtualTS);

	    //Por Regional
	    if (NeoUtils.safeIsNotNull(codigoRegional))
	    {
		stColaborador.setLong(19, codigoRegional);
	    }
	    //Por Empresa
	    else
	    {
		stColaborador.setLong(19, codigoEmpresa);
	    }

	    ResultSet rsColaborador = stColaborador.executeQuery();

	    log.warn("SQL - getListaColaboradores - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");

	    while (rsColaborador.next())
	    {
		colaborador = new ColaboradorVO();
		colaborador.setNumeroEmpresa(rsColaborador.getLong("NumEmp"));
		colaborador.setTipoColaborador(rsColaborador.getLong("TipCol"));
		colaborador.setNumeroCadastro(rsColaborador.getLong("NumCad"));
		colaborador.setNomeColaborador(rsColaborador.getString("NomFun"));
		colaborador.setCargo(rsColaborador.getString("TitRed"));
		colaborador.setSistemaCargo(rsColaborador.getString("DesSis"));
		colaborador.setSituacao(rsColaborador.getString("DesSit"));
		colaborador.setAcessoBloqueado("S".equals(rsColaborador.getString("BloPre")));
		colaborador.setControlaAcesso("S".equals(rsColaborador.getString("EmiCar")));
		colaborador.setCpf(rsColaborador.getLong("NumCpf"));
		colaborador.setSexo(rsColaborador.getString("TipSex"));
		colaborador.setDataNascimento(rsColaborador.getDate("DatNas"));
		colaborador.setDataAdmissao(rsColaborador.getDate("datAdm"));
		colaborador.setNumeroLocal(rsColaborador.getLong("USU_NumLoc"));
		colaborador.setCodigoOrganograma(rsColaborador.getLong("USU_TabOrg"));
		colaborador.setCodigoEscala(rsColaborador.getLong("CodEsc"));
		colaborador.setTurmaEscala(rsColaborador.getLong("CodTma"));
		colaborador.setDataAfastamento(rsColaborador.getDate("dataAfa"));
		colaborador.setDataDemissao(rsColaborador.getDate("dataDem"));
		colaborador.setFiscal(rsColaborador.getString("USU_NomFis"));
		colaborador.setMatrizResponsabilidade(rsColaborador.getString("USU_MatRes"));
		colaborador.setCausaDemissao(rsColaborador.getString("DesDem"));
		colaborador.setQdeExcecao(rsColaborador.getLong("qdeExcecao"));

		setEscalaColaborador(escalas, colaborador);

		colaborador.getEscala().setDescricao(rsColaborador.getString("NomEsc") + " / " + colaborador.getTurmaEscala());

		if (rsColaborador.getString("numcra") != null)
		{
		    colaborador.setNumeroCracha(rsColaborador.getString("numcra"));
		}
		else
		{
		    colaborador.setNumeroCracha("");
		}

		String obsSituacao = "";
		if (rsColaborador.getLong("SitAfa") != 1L)
		{
		    GregorianCalendar dataInicioAfastamento = new GregorianCalendar();
		    GregorianCalendar dataFimAfastamento = new GregorianCalendar();

		    if (rsColaborador.getTimestamp("DataAfa") != null)
		    {
			dataInicioAfastamento.setTime(rsColaborador.getTimestamp("DataAfa"));
		    }
		    if (rsColaborador.getTimestamp("DatTer") != null)
		    {
			dataFimAfastamento.setTime(rsColaborador.getTimestamp("DatTer"));
		    }
		    if ((dataInicioAfastamento != null))
		    {
			if (dataFimAfastamento.get(Calendar.YEAR) == 1900)
			{
			    obsSituacao = "A partir de " + NeoUtils.safeDateFormat(dataInicioAfastamento, "dd/MM/yyyy");
			}
			else
			{
			    obsSituacao = "De: " + NeoUtils.safeDateFormat(dataInicioAfastamento, "dd/MM/yyyy");
			    obsSituacao = obsSituacao + " a " + NeoUtils.safeDateFormat(dataFimAfastamento, "dd/MM/yyyy");
			}
		    }
		}

		if (colaborador.getDataDemissao() != null)
		{
		    if (diaAtual)
		    {
			obsSituacao = "Em aviso prévio até " + NeoUtils.safeDateFormat(colaborador.getDataDemissao(), "dd/MM/yyyy");
		    }
		    else
		    {
			if (colaborador.getDataDemissao().after(dataHoje.getTime()))
			{
			    obsSituacao = "Em aviso prévio até " + NeoUtils.safeDateFormat(colaborador.getDataDemissao(), "dd/MM/yyyy");
			}
			else
			{
			    obsSituacao = "Demitido em " + NeoUtils.safeDateFormat(colaborador.getDataDemissao(), "dd/MM/yyyy") + " por " + colaborador.getCausaDemissao();
			}
		    }
		}

		colaborador.setObsSituacao(obsSituacao);

		//PresencaVO presenca = getPresenca(colaborador, dataAtual);

		presenca = new PresencaVO();
		entrada = new GregorianCalendar();
		saida = new GregorianCalendar();

		String direcaoAcesso1 = rsColaborador.getString("DirAccS");
		Timestamp dataAcesso1 = rsColaborador.getTimestamp("DatAccS");
		String tipoDirecaoAcesso1 = rsColaborador.getString("TipDirS");
		Long coletor1 = rsColaborador.getLong("codrlgS");

		String direcaoAcesso2 = rsColaborador.getString("DirAccE");
		Timestamp dataAcesso2 = rsColaborador.getTimestamp("DatAccE");
		String tipoDirecaoAcesso2 = rsColaborador.getString("TipDirE");
		Long coletor2 = rsColaborador.getLong("codrlgE");

		if (direcaoAcesso1 != null && !direcaoAcesso1.equals(""))
		{

		    entrada = new GregorianCalendar();

		    if (direcaoAcesso1.equals("E"))
		    {
			entrada.setTimeInMillis(dataAcesso1.getTime());
			presenca.setEntrada(entrada);
			presenca.setTipoDirecao(tipoDirecaoAcesso1);
			presenca.setColetorEntrada(coletor1);
		    }
		    else if (direcaoAcesso2 != null && !direcaoAcesso2.equals(""))
		    {
			saida.setTimeInMillis(dataAcesso1.getTime());
			presenca.setSaida(saida);
			presenca.setTipoDirecao(tipoDirecaoAcesso1);
			presenca.setColetorSaida(coletor1);
		    }
		}

		if (direcaoAcesso2 != null && !direcaoAcesso2.equals(""))
		{

		    saida = new GregorianCalendar();

		    if (direcaoAcesso2.equals("E"))
		    {
			entrada.setTimeInMillis(dataAcesso2.getTime());
			presenca.setEntrada(entrada);
			presenca.setTipoDirecao(tipoDirecaoAcesso2);
			presenca.setColetorEntrada(coletor2);
		    }
		    else if (direcaoAcesso1 != null && !direcaoAcesso1.equals(""))
		    {
			saida.setTimeInMillis(dataAcesso2.getTime());
			presenca.setSaida(saida);
			presenca.setTipoDirecao(tipoDirecaoAcesso2);
			presenca.setColetorSaida(coletor2);
		    }
		}

		presenca.setStatus(getStatusPresenca(colaborador.getEscala(), presenca, dataAtual));
		colaborador.setPresenca(presenca);

		//Verifica duplicidade no caso de possuir dois acessos na mesma data
		if (numCraAnt.equals("") || !numCraAnt.equals(colaborador.getNumeroCracha()))
		{
		    // Adicionando a lista
		    listaColaboradores.add(colaborador);
		}

		numCraAnt = colaborador.getNumeroCracha();

	    }
	    getlistaAvosFerias(listaColaboradores, dataAtual);
	    rsColaborador.close();
	    stColaborador.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}

	return listaColaboradores;
    }

    public static void getlistaAvosFerias(List<ColaboradorVO> listaColaboradores, GregorianCalendar dataAtual)
    {

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();

	    /*** Recupera Colaboradores com mais de 21 Avos de férias ***/
	    StringBuffer queryColaborador = new StringBuffer();
	    queryColaborador.append(" SELECT per.NumEmp, fun.tipcol, per.NumCad, fun.nomfun, SUM(per.AvoFer) AS AvoFer ");
	    queryColaborador.append(" FROM R040PER per ");
	    queryColaborador.append(" INNER JOIN R034FUN fun on fun.numemp = per.numemp AND fun.tipcol = per.tipcol AND fun.numcad = per.numcad ");
	    queryColaborador.append(" LEFT JOIN R038AFA afa WITH (NOLOCK) ON (afa.NumEmp = fun.NumEmp AND afa.TipCol = fun.TipCol AND afa.NumCad = fun.NumCad AND afa.DatAfa = (SELECT MAX (afa2.DatAfa) FROM R038AFA afa2 WHERE afa2.NUMEMP = afa.NUMEMP AND afa2.TIPCOL = afa.TIPCOL AND afa2.NUMCAD = afa.NUMCAD AND afa2.DATAFA <= ? AND (afa2.DatTer = '1900-12-31 00:00:00' OR afa2.DatTer >= cast(floor(cast(? as float)) as datetime)))) ");
	    queryColaborador.append(" LEFT JOIN R010SIT sit ON sit.CodSit = afa.SitAfa ");
	    queryColaborador.append(" WHERE per.SitPer = 0 AND fun.tipcol = 1 AND per.iniper >= '2000-01-01' AND per.iniper <= ? AND fun.NumEmp NOT IN (3, 5, 11, 12, 13, 14, 999) ");
	    queryColaborador.append(" AND (afa.SitAfa IS NULL OR afa.SitAfa NOT IN (3,4,7,8,74) OR (afa.SitAfa = 7 AND afa.DatAfa > cast(floor(cast(? as float)) as datetime))) ");
	    queryColaborador.append(" GROUP BY per.NumEmp, fun.tipcol, fun.nomfun, per.NumCad, afa.SitAfa, sit.DesSit HAVING SUM(per.AvoFer) >= 22");

	    Timestamp dataAtualTS = new Timestamp(dataAtual.getTimeInMillis());

	    PreparedStatement stColaborador = connVetorh.prepareStatement(queryColaborador.toString());
	    stColaborador.setTimestamp(1, dataAtualTS);
	    stColaborador.setTimestamp(2, dataAtualTS);
	    stColaborador.setTimestamp(3, dataAtualTS);
	    stColaborador.setTimestamp(4, dataAtualTS);

	    ResultSet rsColaborador = stColaborador.executeQuery();

	    log.warn("SQL - listaAvosFerias - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");

	    while (rsColaborador.next())
	    {
		for (ColaboradorVO col : listaColaboradores)
		{
		    if (col.getNumeroEmpresa() == rsColaborador.getLong("NumEmp") && col.getTipoColaborador() == rsColaborador.getLong("TipCol") && col.getNumeroCadastro() == rsColaborador.getLong("NumCad"))
		    {
			col.setAvosFerias(rsColaborador.getLong("AvoFer"));
		    }
		}
	    }
	    rsColaborador.close();
	    stColaborador.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}
    }

    public static ColaboradorVO getBuscaColaborador(Long codigoCadastro, Long codigoEmpresa, GregorianCalendar dataAtual)
    {

	Connection connVetorh = PersistEngine.getConnection("VETORH");
	ColaboradorVO colaborador = null;
	EscalaVO escala = new EscalaVO();

	try
	{
	    /*** Recupera Colaboradores ***/
	    StringBuffer queryColaborador = new StringBuffer();
	    queryColaborador.append(" SELECT fun.NumEmp, fun.TipCol, fun.NumCad, hch.NumCra, fun.DatAdm, fun.NomFun, fun.SitAfa, fun.DatAfa, fun.emicar, sit.usu_blopre, ");
	    queryColaborador.append("        fun.NumCpf, hes.CodEsc, hes.CodTma, orn.NumLoc, orn.TabOrg, orn.usu_codccu ");
	    queryColaborador.append(" FROM R034FUN fun ");
	    queryColaborador.append(" LEFT JOIN R038HCH hch WITH (NOLOCK) ON hch.NUMEMP = fun.NumEmp AND fun.TIPCOL = hch.TIPCOL AND fun.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch.HorIni, hch.DatIni) = (SELECT MAX(DATEADD(MINUTE, hch2.HorIni, hch2.DatIni)) FROM R038HCH hch2 WHERE hch2.NUMEMP = hch.NUMEMP AND hch2.TIPCOL = hch.TIPCOL AND hch2.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch2.HorIni, hch2.DatIni) <= ?)");
	    queryColaborador.append(" INNER JOIN R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp AND hlo.TipCol = fun.TipCol AND hlo.NumCad = fun.NumCad AND hlo.DatAlt = (SELECT MAX (hlo2.DATALT) FROM R038HLO hlo2 WHERE hlo2.NUMEMP = hlo.NUMEMP AND hlo2.TIPCOL = hlo.TIPCOL AND hlo2.NUMCAD = hlo.NUMCAD AND hlo2.DATALT <= ?) ");
	    queryColaborador.append(" INNER JOIN R038HES hes WITH (NOLOCK) ON hes.NumEmp = fun.NumEmp AND hes.TipCol = fun.TipCol AND hes.NumCad = fun.NumCad AND hes.DatAlt = (SELECT MAX (hes2.DATALT) FROM R038HES hes2 WHERE hes2.NUMEMP = hes.NUMEMP AND hes2.TIPCOL = hes.TIPCOL AND hes2.NUMCAD = hes.NUMCAD AND hes2.DATALT <= ?) ");
	    queryColaborador.append(" INNER JOIN R010SIT sit ON sit.CodSit = fun.SitAfa ");
	    queryColaborador.append(" INNER JOIN R016ORN orn ON orn.NumLoc = hlo.NumLoc and orn.TabOrg = hlo.TabOrg ");
	    queryColaborador.append(" WHERE fun.DatAdm <= ? AND (fun.SitAfa <> 7 OR (fun.SitAfa = 7 AND fun.DatAfa > ?)) ");
	    queryColaborador.append(" AND fun.TipCol = 1 AND fun.NumCad = ? AND fun.NumEmp = ? ");

	    Timestamp dataAtualTS = new Timestamp(dataAtual.getTimeInMillis());
	    PreparedStatement stColaborador = connVetorh.prepareStatement(queryColaborador.toString());
	    stColaborador.setTimestamp(1, dataAtualTS);
	    stColaborador.setTimestamp(2, dataAtualTS);
	    stColaborador.setTimestamp(3, dataAtualTS);
	    stColaborador.setTimestamp(4, dataAtualTS);
	    stColaborador.setTimestamp(5, dataAtualTS);
	    stColaborador.setLong(6, codigoCadastro);
	    stColaborador.setLong(7, codigoEmpresa);

	    ResultSet rsColaborador = stColaborador.executeQuery();

	    if (rsColaborador.next())
	    {
		colaborador = new ColaboradorVO();
		colaborador.setNumeroEmpresa(rsColaborador.getLong("NumEmp"));
		colaborador.setTipoColaborador(rsColaborador.getLong("TipCol"));
		colaborador.setNumeroCadastro(rsColaborador.getLong("NumCad"));
		colaborador.setNomeColaborador(rsColaborador.getString("NomFun"));
		colaborador.setCpf(rsColaborador.getLong("NumCpf"));
		colaborador.setNumeroLocal(rsColaborador.getLong("NumLoc"));
		colaborador.setCodigoOrganograma(rsColaborador.getLong("TabOrg"));
		colaborador.setCentroCusto(rsColaborador.getString("usu_codccu"));
		colaborador.setCodigoEscala(rsColaborador.getLong("CodEsc"));
		colaborador.setTurmaEscala(rsColaborador.getLong("CodTma"));
		colaborador.setAcessoBloqueado("S".equals(rsColaborador.getString("usu_blopre")));
		colaborador.setControlaAcesso("S".equals(rsColaborador.getString("EmiCar")));
		setEscalaColaboradorUnico(colaborador, dataAtual);

		if (rsColaborador.getString("numcra") != null)
		{
		    colaborador.setNumeroCracha(rsColaborador.getString("numcra"));
		}
		else
		{
		    colaborador.setNumeroCracha("");
		}
	    }
	    rsColaborador.close();
	    stColaborador.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}

	return colaborador;
    }

    public static Collection<ColaboradorVO> getColaboradores(Collection<ColaboradorVO> colaboradores, PostoVO posto, GregorianCalendar dataAtual)
    {

	Collection<ColaboradorVO> listaColaboradores = new ArrayList<ColaboradorVO>();

	if (colaboradores != null)
	{
	    for (ColaboradorVO colaborador : colaboradores)
	    {
		if (posto.getNumeroLocal().equals(colaborador.getNumeroLocal()))
		{
		    listaColaboradores.add(colaborador);
		    if (colaborador.getCodigoEscala().equals(999L))
		    {
			EscalaVO escalaHorista = new EscalaVO();

			if (posto.getEscalaPosto() != null)
			{
			    escalaHorista.setHorarios(posto.getEscalaPosto().getHorariosHoje());
			    escalaHorista.setConsiderarEscala(Boolean.TRUE);
			}
			colaborador.setEscala(escalaHorista);
			colaborador.getEscala().setDescricao("HORISTA *A.E.P.*");
		    }
		}
	    }
	}

	if (listaColaboradores != null)
	{
	    for (ColaboradorVO colaborador : listaColaboradores)
	    {
		colaboradores.remove(colaborador);
	    }
	}

	return listaColaboradores;
    }

    public static List<EntradaAutorizadaVO> getListaEntradasAutorizadas(Long codigoRegional, Long codigoOrganograma, Collection<ColaboradorVO> colaboradores, GregorianCalendar dataAtual, Long codigoTipCli, List<EscalaVO> escalas)
    {
	Connection connVetorh = PersistEngine.getConnection("VETORH");
	List<EntradaAutorizadaVO> entradasAutorizadas = new ArrayList<EntradaAutorizadaVO>();
	EntradaAutorizadaVO entradaVO = new EntradaAutorizadaVO();
	ColaboradorVO coberturaVO;

	PresencaVO presenca = new PresencaVO();

	GregorianCalendar dataHoje = new GregorianCalendar();

	String stringDataAtual = NeoUtils.safeDateFormat(dataAtual, "dd/MM/yyyy");
	String stringDataHoje = NeoUtils.safeDateFormat(dataHoje, "dd/MM/yyyy");
	Boolean diaAtual = true;

	String numcraAnt = "";
	Long datiniAnt = 0L;
	Long datfimAnt = 0L;
	String horarioAnt = "";

	//Data diferente da data atual
	if (!stringDataAtual.endsWith(stringDataHoje))
	{
	    diaAtual = false;
	}

	GregorianCalendar dataInicioCompetencia = new GregorianCalendar();
	GregorianCalendar dataFimCompetencia = new GregorianCalendar();

	try
	{
	    dataInicioCompetencia = OrsegupsUtils.getDiaInicioCompetencia(OrsegupsUtils.getCompetenciaApuracao(dataAtual));
	    dataFimCompetencia = OrsegupsUtils.getDiaFimCompetencia(OrsegupsUtils.getCompetenciaApuracao(dataAtual));
	    dataFimCompetencia.add(Calendar.DAY_OF_MONTH, -4);
	}
	catch (ParseException e1)
	{
	    e1.printStackTrace();
	}

	try
	{
	    /*** Recupera Operacoes em Posto ***/
	    StringBuffer queryOperacao = new StringBuffer();
	    queryOperacao.append(" SELECT cob.usu_numemp, cob.usu_tipcol, cob.usu_numcad, hch.numcra, cob.usu_datalt, cob.usu_datfim, cob.USU_NumCadCob, orn.NumLoc, orn.TabOrg, ");
	    queryOperacao.append(" 		  fun.nomfun, fun.NumCpf, fun.DatNas, fun.TipSex, fun.DatAdm, fun.USU_MatRes, fun.emicar, car.titred, sis.DesSis, USU_NumEmpCob, esc.NomEsc, fun2.NomFun AS NomFun2, fun2.CodTma, ");
	    queryOperacao.append(" 		  cob.USU_HorIni, fun2.CodEsc, mo.USU_DesPre, ISNULL(sit.DesSit, 'Trabalhando') AS DesSit, afa.DatAfa AS dataAfa, afa.DatTer, ISNULL(sit.usu_blopre, 'N') AS BloPre, ");
	    queryOperacao.append(" 		  DATEADD(MINUTE, accS.HorAcc, accS.DatAcc) AS DatAccS, accS.DirAcc AS DirAccS, accS.usu_tipdir AS TipDirS, accS.codrlg AS codrlgS, ");
	    queryOperacao.append(" 		  DATEADD(MINUTE, accE.HorAcc, accE.DatAcc) AS DatAccE, accE.DirAcc AS DirAccE, accE.usu_tipdir AS TipDirE, accE.codrlg AS codrlgE, COUNT(sto.CodSit) AS qdeExcecao ");
	    queryOperacao.append(" FROM USU_T038COBFUN cob WITH (NOLOCK) ");
	    queryOperacao.append(" INNER JOIN R034FUN fun WITH (NOLOCK) ON fun.NumEmp = cob.USU_NumEmp AND fun.TipCol = cob.USU_TipCol AND fun.NumCad = cob.USU_NumCad ");
	    queryOperacao.append(" LEFT JOIN R038HCH hch ON hch.NUMEMP = fun.NumEmp AND fun.TIPCOL = hch.TIPCOL AND fun.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch.HorIni, hch.DatIni) = (SELECT MAX(DATEADD(MINUTE, hch2.HorIni, hch2.DatIni)) FROM R038HCH hch2 WHERE hch2.NUMEMP = hch.NUMEMP AND hch2.TIPCOL = hch.TIPCOL AND hch2.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch2.HorIni, hch2.DatIni) <= ?)");
	    queryOperacao.append(" INNER JOIN R024CAR car WITH (NOLOCK) ON car.EstCar = fun.EstCar AND car.CodCar = fun.CodCar ");
	    queryOperacao.append(" LEFT JOIN R024SIS sis ON sis.SisCar = car.SisCar ");
	    queryOperacao.append(" LEFT JOIN R034FUN fun2 WITH (NOLOCK) ON fun2.NumEmp = cob.USU_NumEmpCob AND fun2.TipCol = cob.USU_TipColCob AND fun2.NumCad = cob.USU_NumCadCob ");
	    queryOperacao.append(" LEFT JOIN R038HES hes ON hes.NumEmp = fun2.NumEmp AND hes.TipCol = fun2.TipCol AND hes.NumCad = fun2.NumCad AND hes.DatAlt = (SELECT MAX (DATALT) FROM R038HES TABELA001 WHERE TABELA001.NUMEMP = hes.NUMEMP AND TABELA001.TIPCOL = hes.TIPCOL AND TABELA001.NUMCAD = hes.NUMCAD AND TABELA001.DATALT <= ?) ");
	    queryOperacao.append(" LEFT JOIN R006ESC esc WITH (NOLOCK) ON esc.CodEsc = fun2.CodEsc and esc.claesc = 8");
	    queryOperacao.append(" LEFT JOIN USU_T010MoCo mo WITH (NOLOCK) ON mo.USU_CodMot = cob.USU_codMot ");
	    queryOperacao.append(" INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.numloc = cob.usu_numloctra AND orn.taborg = cob.usu_taborgtra ");
	    queryOperacao.append(" INNER JOIN R016HIE hie WITH (NOLOCK) ON hie.taborg = orn.taborg AND orn.numloc = hie.numloc AND hie.datini = (SELECT MAX(h2.datini) FROM dbo.R016HIE h2 WHERE hie.taborg = orn.taborg AND h2.numloc = orn.numloc) ");
	    queryOperacao.append(" LEFT JOIN R038AFA afa WITH (NOLOCK) ON (afa.NumEmp = fun.NumEmp AND afa.TipCol = fun.TipCol AND afa.NumCad = fun.NumCad AND afa.DatAfa = (SELECT MAX (DATAFA) FROM R038AFA TABELA001 WHERE TABELA001.NUMEMP = afa.NUMEMP AND TABELA001.TIPCOL = afa.TIPCOL AND TABELA001.NUMCAD = afa.NUMCAD AND TABELA001.DATAFA <= ? AND (TABELA001.DatTer = '1900-12-31 00:00:00' OR TABELA001.DatTer >= cast(floor(cast(? as float)) as datetime)))) ");
	    queryOperacao.append(" LEFT JOIN R010SIT sit ON sit.CodSit = afa.SitAfa ");
	    queryOperacao.append(" LEFT JOIN R070ACC accS ON (accS.NumCra = hch.NumCra AND accS.TipAcc = 100 AND accS.DatAcc > DATEADD(mm, -1, ?) AND accS.CodPlt = 2 AND accS.usu_tipdir <> 'P' AND accS.usu_TabOrg = cob.usu_taborgtra AND accS.usu_NumLoc = cob.usu_numloctra ");
	    queryOperacao.append(" 			 AND DATEADD(MINUTE, accS.HorAcc, accS.DatAcc) = (SELECT MAX(DATEADD(MINUTE, accS2.HorAcc, accS2.DatAcc)) FROM R070ACC accS2 WHERE accS2.NumCra = accS.NumCra AND accS2.DatAcc > DATEADD(mm, -1, ?) AND accS2.usu_TabOrg = accS.usu_TabOrg AND accS2.usu_NumLoc = accS.usu_NumLoc AND accS2.TipAcc = 100 AND accS2.CodPlt = 2 AND accS2.usu_tipdir <> 'P' AND DATEADD(MINUTE, accS2.HorAcc, accS2.DatAcc) <= ?)) ");
	    queryOperacao.append(" LEFT JOIN R070ACC accE ON (accE.NumCra = hch.NumCra AND accE.DatAcc > DATEADD(mm, -1, ?) AND accE.usu_TabOrg = cob.usu_taborgtra AND accE.usu_NumLoc = cob.usu_numloctra AND accE.TipAcc = 100 AND accE.CodPlt = 2 AND accE.DirAcc = 'E' AND accE.usu_tipdir <> 'P' ");
	    queryOperacao.append(" 			 AND DATEADD(MINUTE, accE.HorAcc, accE.DatAcc) = (SELECT MAX(DATEADD(MINUTE, accE2.HorAcc, accE2.DatAcc)) FROM R070ACC accE2 WHERE accE2.NumCra = accE.NumCra AND accE2.DatAcc > DATEADD(mm, -1, ?) AND accE2.usu_TabOrg = accE.usu_TabOrg AND accE2.usu_NumLoc = accE.usu_NumLoc AND accE2.TipAcc = 100 AND accE2.CodPlt = 2 AND accE2.DirAcc = 'E' AND accE2.usu_tipdir <> 'P' AND DATEADD(MINUTE, accE2.HorAcc, accE2.DatAcc) <= ?)) ");
	    queryOperacao.append(" LEFT JOIN R066APU apu ON apu.NumEmp = fun.numemp AND apu.TipCol = fun.TipCol AND apu.NumCad = fun.NumCad AND apu.DatApu >= ? AND apu.DatApu <= ? AND fun.ConRho = 2 ");
	    queryOperacao.append(" LEFT JOIN R004HOR hor ON hor.CodHor = apu.HorDat AND hor.CodHor > 9000 ");
	    queryOperacao.append(" LEFT JOIN R066SIT sap ON sap.NumEmp = apu.NumEmp AND sap.TipCol = apu.TipCol AND sap.NumCad = apu.NumCad AND sap.DatApu = apu.DatApu ");
	    queryOperacao.append(" LEFT JOIN R010SIT sto ON sto.CodSit = sap.CodSit AND sto.SitExc = 'S' ");
	    queryOperacao.append(" WHERE cob.usu_taborgtra = ? ");
	    queryOperacao.append(" AND cast(floor(CAST(cob.USU_DatFim AS FLOAT)) AS datetime) >= CAST(floor(CAST(? AS FLOAT)) AS datetime) ");
	    queryOperacao.append(" AND mo.USU_TipCob in ('C', 'D', 'R') ");
	    queryOperacao.append(" AND orn.taborg = 203 ");
	    queryOperacao.append(" AND LEN(hie.CodLoc) = 27 ");
	    queryOperacao.append(" AND fun.NumEmp not in (5) ");

	    if (!diaAtual)
	    {
		queryOperacao.append(" AND CAST(floor(CAST(cob.usu_datalt AS FLOAT)) AS datetime) <= CAST(floor(CAST(? AS FLOAT)) AS datetime) ");

		if (NeoUtils.safeIsNotNull(codigoRegional))
		{
		    queryOperacao.append(" AND orn.usu_codreg = ? ");

		    if (codigoTipCli != 0L && codigoRegional == 9)
		    {
			queryOperacao.append(" AND orn.usu_tipcli = ? ");
		    }
		}
	    }
	    else
	    {
		if (NeoUtils.safeIsNotNull(codigoRegional))
		{
		    queryOperacao.append(" AND orn.usu_codreg = ? ");

		    if (codigoTipCli != 0L && codigoRegional == 9)
		    {
			queryOperacao.append(" AND orn.usu_tipcli = ? ");
		    }
		}
	    }

	    queryOperacao.append(" GROUP BY cob.usu_numemp, cob.usu_tipcol, cob.usu_numcad, hch.numcra, cob.usu_datalt, cob.usu_datfim, cob.USU_NumCadCob, orn.NumLoc, orn.TabOrg, ");
	    queryOperacao.append(" fun.nomfun, fun.NumCpf, fun.DatNas, fun.TipSex, fun.DatAdm, fun.USU_MatRes, fun.emicar, car.titred, sis.DesSis, USU_NumEmpCob, esc.NomEsc, fun2.NomFun, fun2.CodTma, ");
	    queryOperacao.append(" cob.USU_HorIni, fun2.CodEsc, mo.USU_DesPre, sit.DesSit, afa.DatAfa, afa.DatTer, cob.usu_numloctra, sit.usu_blopre, ");
	    queryOperacao.append(" accS.HorAcc, accS.DatAcc, accS.DirAcc, accS.usu_tipdir, accE.HorAcc, accE.DatAcc, accE.DirAcc, accE.usu_tipdir, accS.codrlg, accE.codrlg ");

	    queryOperacao.append(" ORDER BY cob.usu_numloctra, cob.USU_Datalt ");

	    PreparedStatement stOperacao = connVetorh.prepareStatement(queryOperacao.toString());
	    stOperacao.setTimestamp(1, new Timestamp(dataAtual.getTimeInMillis()));
	    stOperacao.setTimestamp(2, new Timestamp(dataAtual.getTimeInMillis()));
	    stOperacao.setTimestamp(3, new Timestamp(dataAtual.getTimeInMillis()));
	    stOperacao.setTimestamp(4, new Timestamp(dataAtual.getTimeInMillis()));
	    stOperacao.setTimestamp(5, new Timestamp(dataAtual.getTimeInMillis()));
	    stOperacao.setTimestamp(6, new Timestamp(dataAtual.getTimeInMillis()));
	    stOperacao.setTimestamp(7, new Timestamp(dataAtual.getTimeInMillis()));
	    stOperacao.setTimestamp(8, new Timestamp(dataAtual.getTimeInMillis()));
	    stOperacao.setTimestamp(9, new Timestamp(dataAtual.getTimeInMillis()));
	    stOperacao.setTimestamp(10, new Timestamp(dataAtual.getTimeInMillis()));
	    stOperacao.setTimestamp(11, new Timestamp(dataInicioCompetencia.getTimeInMillis()));
	    stOperacao.setTimestamp(12, new Timestamp(dataFimCompetencia.getTimeInMillis()));
	    stOperacao.setLong(13, codigoOrganograma);
	    stOperacao.setTimestamp(14, new Timestamp(dataAtual.getTimeInMillis()));

	    if (!diaAtual)
	    {
		stOperacao.setTimestamp(15, new Timestamp(dataAtual.getTimeInMillis()));

		if (NeoUtils.safeIsNotNull(codigoRegional))
		{
		    stOperacao.setLong(16, codigoRegional);

		    if (codigoTipCli != 0L && codigoRegional == 9)
		    {
			stOperacao.setLong(17, codigoTipCli);
		    }
		}
	    }
	    else
	    {
		if (NeoUtils.safeIsNotNull(codigoRegional))
		{
		    stOperacao.setLong(15, codigoRegional);

		    if (codigoTipCli != 0L && codigoRegional == 9)
		    {
			stOperacao.setLong(16, codigoTipCli);
		    }
		}
	    }

	    ResultSet rsOperacao = stOperacao.executeQuery();
	    while (rsOperacao.next())
	    {
		coberturaVO = new ColaboradorVO();
		entradaVO = new EntradaAutorizadaVO();
		presenca = new PresencaVO();
		EscalaVO escala = new EscalaVO();

		coberturaVO.setNumeroEmpresa(rsOperacao.getLong("usu_numemp"));
		coberturaVO.setTipoColaborador(rsOperacao.getLong("usu_tipcol"));
		coberturaVO.setNumeroCadastro(rsOperacao.getLong("usu_numcad"));
		coberturaVO.setNomeColaborador(rsOperacao.getString("nomfun"));
		coberturaVO.setCargo(rsOperacao.getString("titred"));
		coberturaVO.setSistemaCargo(rsOperacao.getString("DesSis"));
		coberturaVO.setSituacao(rsOperacao.getString("DesSit"));
		coberturaVO.setAcessoBloqueado("S".equals(rsOperacao.getString("BloPre")));
		coberturaVO.setControlaAcesso("S".equals(rsOperacao.getString("EmiCar")));
		coberturaVO.setCpf(rsOperacao.getLong("NumCpf"));
		coberturaVO.setNumeroLocal(rsOperacao.getLong("numloc"));
		coberturaVO.setCodigoOrganograma(rsOperacao.getLong("taborg"));
		coberturaVO.setDataAfastamento(rsOperacao.getDate("dataAfa"));
		coberturaVO.setSexo(rsOperacao.getString("TipSex"));
		coberturaVO.setDataNascimento(rsOperacao.getDate("DatNas"));
		coberturaVO.setDataAdmissao(rsOperacao.getDate("datAdm"));
		coberturaVO.setMatrizResponsabilidade(rsOperacao.getString("USU_MatRes"));
		coberturaVO.setQdeExcecao(rsOperacao.getLong("qdeExcecao"));

		if (rsOperacao.getString("numcra") != null)
		{
		    coberturaVO.setNumeroCracha(rsOperacao.getString("numcra"));
		}
		else
		{
		    coberturaVO.setNumeroCracha("");
		}

		entradaVO.setDescricaoCobertura(rsOperacao.getString("usu_despre"));
		entradaVO.setHoraInicial(rsOperacao.getLong("usu_horini"));

		GregorianCalendar dataInicio = (GregorianCalendar) dataAtual.clone();
		GregorianCalendar dataFim = (GregorianCalendar) dataAtual.clone();

		dataInicio.setTime(rsOperacao.getTimestamp("usu_datalt"));
		entradaVO.setDataInicial(OrsegupsUtils.atribuiHora(dataInicio));

		dataFim.setTime(rsOperacao.getTimestamp("usu_datfim"));
		entradaVO.setDataFinal(OrsegupsUtils.atribuiHora(dataFim));

		HorarioVO horario = new HorarioVO();
		horario.setDataInicial(dataInicio);
		horario.setDataFinal(dataFim);

		// Se USU_NumCadCob é igual a 0, é cobertura de posto, senão é cobertura de colaborador
		if (rsOperacao.getLong("USU_NumCadCob") == 0L)
		{

		    escala.setHorarios(getHorariosOperacoesPostos(horario, dataAtual));
		    escala.setDescricao(NeoUtils.safeDateFormat(dataInicio, "HH:mm") + "-" + NeoUtils.safeDateFormat(dataFim, "HH:mm"));
		    String descHorarios = escala.toString();
		    escala.setDescricaoHorarios(descHorarios);

		    coberturaVO.setEscala(escala);

		    //presenca = getPresenca(coberturaVO, dataAtual);
		}
		else
		{
		    if (colaboradores != null)
		    {
			for (ColaboradorVO substituidoVO : colaboradores)
			{
			    if (substituidoVO.getNumeroCadastro().equals(rsOperacao.getLong("USU_NumCadCob")) && substituidoVO.getNumeroEmpresa().equals(rsOperacao.getLong("USU_NumEmpCob")))
			    {
				entradaVO.setColaboradorSubstituido(substituidoVO);
				coberturaVO.setEscala(substituidoVO.getEscala().clone());
				coberturaVO.getEscala().setConsiderarEscala(Boolean.TRUE);

				GregorianCalendar dtInicioAux = (GregorianCalendar) horario.getDataInicial().clone();
				GregorianCalendar dtAtualAux = (GregorianCalendar) dataAtual.clone();

				dtInicioAux.set(Calendar.HOUR_OF_DAY, 0);
				dtInicioAux.set(Calendar.MINUTE, 0);
				dtInicioAux.set(Calendar.SECOND, 0);
				dtInicioAux.set(Calendar.MILLISECOND, 0);

				dtAtualAux.set(Calendar.HOUR_OF_DAY, 0);
				dtAtualAux.set(Calendar.MINUTE, 0);
				dtAtualAux.set(Calendar.SECOND, 0);
				dtAtualAux.set(Calendar.MILLISECOND, 0);

				if (dtInicioAux.after(dtAtualAux))
				{
				    coberturaVO.getEscala().setHorarios(new ArrayList<HorarioVO>());
				}

				String descHorarios = coberturaVO.getEscala().toString();
				coberturaVO.getEscala().setDescricaoHorarios(descHorarios);

				break;
			    }
			}
		    }
		    // Se não for localizado o substituido, seta valores padroes
		    if (entradaVO.getColaboradorSubstituido() == null)
		    {
			ColaboradorVO substituidoVO = new ColaboradorVO();
			substituidoVO.setNomeColaborador("NÃO LOCALIZADO");
			coberturaVO.setEscala(new EscalaVO());
			coberturaVO.getEscala().setDescricao("NÃO LOCALIZADA");
			coberturaVO.getEscala().setHorarios(new ArrayList<HorarioVO>());
			entradaVO.setColaboradorSubstituido(substituidoVO);
		    }
		}

		if (coberturaVO.getEscala() != null)
		{
		    //presenca = getPresenca(coberturaVO, dataAtual);

		    presenca = new PresencaVO();
		    GregorianCalendar gcDataAcesso1 = new GregorianCalendar();
		    GregorianCalendar gcDataAcesso2 = new GregorianCalendar();

		    String direcaoAcesso1 = rsOperacao.getString("DirAccS");
		    String tipoDirecaoAcesso1 = rsOperacao.getString("TipDirS");
		    Timestamp dataAcesso1 = rsOperacao.getTimestamp("DatAccS");
		    Long coletor1 = rsOperacao.getLong("codrlgS");

		    String direcaoAcesso2 = rsOperacao.getString("DirAccE");
		    String tipoDirecaoAcesso2 = rsOperacao.getString("TipDirS");
		    Timestamp dataAcesso2 = rsOperacao.getTimestamp("DatAccE");
		    Long coletor2 = rsOperacao.getLong("codrlgS");

		    if (coberturaVO.getEscala().getHorarios() != null && !coberturaVO.getEscala().getHorarios().isEmpty())
		    {
			if (direcaoAcesso1 != null)
			{

			    gcDataAcesso1.setTimeInMillis(dataAcesso1.getTime());

			    if (direcaoAcesso1.equals("E"))
			    {
				presenca.setEntrada(gcDataAcesso1);
				presenca.setColetorEntrada(coletor1);
			    }
			    else if (direcaoAcesso2 != null && !direcaoAcesso2.equals(""))
			    {
				presenca.setSaida(gcDataAcesso1);
				presenca.setColetorSaida(coletor1);
			    }
			}

			if (direcaoAcesso2 != null && !direcaoAcesso2.equals(""))
			{

			    gcDataAcesso2.setTimeInMillis(dataAcesso2.getTime());

			    if (direcaoAcesso2.equals("E"))
			    {
				presenca.setEntrada(gcDataAcesso2);
				presenca.setColetorEntrada(coletor2);
			    }
			    else if (direcaoAcesso1 != null && !direcaoAcesso1.equals(""))
			    {
				presenca.setSaida(gcDataAcesso2);
				presenca.setColetorSaida(coletor2);
			    }
			}
		    }

		    presenca.setStatus(getStatusPresenca(coberturaVO.getEscala(), presenca, dataAtual));
		    coberturaVO.setPresenca(presenca);
		}

		Long datiniNew = entradaVO.getDataInicial().getTimeInMillis();
		Long datfimNew = entradaVO.getDataFinal().getTimeInMillis();

		String horarioNew = "";

		if (coberturaVO != null && coberturaVO.getEscala() != null)
		{
		    horarioNew = coberturaVO.getEscala().toString();
		}

		//Regra para remover inconsistencia quando colaborador esta afastado e não esteja cobrindo ele mesmo
		if (entradaVO.getColaboradorSubstituido() != null && !coberturaVO.getNumeroCracha().equals(entradaVO.getColaboradorSubstituido().getNumeroCracha()) && coberturaVO.isAcessoBloqueado())
		{
		    coberturaVO.getEscala().setConsiderarEscala(Boolean.FALSE);
		}

		//Regra para remover inconsistencia quando colaborador nao emite cartao ponto
		if (!coberturaVO.isControlaAcesso())
		{
		    coberturaVO.getEscala().setConsiderarEscala(Boolean.FALSE);
		}

		//Verifica duplicidade no caso de possuir dois acessos na mesma data
		if (!numcraAnt.equals(coberturaVO.getNumeroCracha()) || !datiniAnt.toString().equals(datiniNew.toString()) || !datfimAnt.toString().equals(datfimNew.toString()) || !horarioAnt.toString().equals(horarioNew))
		{
		    entradaVO.setColaborador(coberturaVO);
		    entradasAutorizadas.add(entradaVO);
		    //System.out.println(coberturaVO.getEscala());
		}

		numcraAnt = coberturaVO.getNumeroCracha();
		datiniAnt = entradaVO.getDataInicial().getTimeInMillis();
		datfimAnt = entradaVO.getDataFinal().getTimeInMillis();

		if (entradaVO.getColaborador() != null && entradaVO.getColaborador().getEscala() != null)
		{
		    horarioAnt = entradaVO.getColaborador().getEscala().toString();
		}
	    }
	    rsOperacao.close();
	    stOperacao.close();

	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}

	return entradasAutorizadas;
    }

    public static List<EntradaAutorizadaVO> getEntradasAutorizadasColaborador(ColaboradorVO colaborador, GregorianCalendar dataAtual, String centroCusto)
    {
	Connection connVetorh = PersistEngine.getConnection("VETORH");
	List<EntradaAutorizadaVO> entradasAutorizadas = new ArrayList<EntradaAutorizadaVO>();
	ColaboradorVO cobertura = new ColaboradorVO();

	ParameterVO parameter = new ParameterVO();
	parameter = getQLParameters();

	GregorianCalendar dataToleranciaIni = new GregorianCalendar();
	dataToleranciaIni = (GregorianCalendar) dataAtual.clone();

	GregorianCalendar dataToleranciaFim = new GregorianCalendar();
	dataToleranciaFim = (GregorianCalendar) dataAtual.clone();

	//Regra Comentada Bloqueio
	dataToleranciaIni.set(Calendar.MINUTE, dataAtual.get(Calendar.MINUTE) + parameter.getToleranciaEntradaPre().intValue());
	dataToleranciaFim.set(Calendar.MINUTE, dataAtual.get(Calendar.MINUTE) - parameter.getToleranciaSaidaPos().intValue());

	//		System.out.println(NeoUtils.safeDateFormat(dataToleranciaIni, "yyyy-MM-dd HH:mm:ss"));
	//		System.out.println(NeoUtils.safeDateFormat(dataToleranciaFim, "yyyy-MM-dd HH:mm:ss"));

	//dataToleranciaIni.set(Calendar.MINUTE, dataAtual.get(Calendar.MINUTE) + 120);
	//dataToleranciaFim.set(Calendar.MINUTE, dataAtual.get(Calendar.MINUTE) - 120);

	try
	{
	    /*** Recupera Operacoes em Posto ***/
	    StringBuffer queryOperacao = new StringBuffer();
	    queryOperacao.append(" SELECT cob.usu_numemp, cob.usu_tipcol, cob.usu_numcad, cob.usu_datalt, cob.usu_datfim, hch.numcra, cob.USU_NumEmpCob, cob.USU_NumCadCob, mo.usu_despre, orn.NumLoc, orn.TabOrg ");
	    queryOperacao.append(" FROM USU_T038COBFUN cob ");
	    queryOperacao.append(" INNER JOIN R034FUN fun WITH (NOLOCK) ON fun.NumEmp = cob.USU_NumEmp AND fun.TipCol = cob.USU_TipCol AND fun.NumCad = cob.USU_NumCad ");
	    queryOperacao.append(" LEFT JOIN R038HCH hch ON hch.NUMEMP = fun.NumEmp AND fun.TIPCOL = hch.TIPCOL AND fun.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch.HorIni, hch.DatIni) = (SELECT MAX(DATEADD(MINUTE, hch2.HorIni, hch2.DatIni)) FROM R038HCH hch2 WHERE hch2.NUMEMP = hch.NUMEMP AND hch2.TIPCOL = hch.TIPCOL AND hch2.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch2.HorIni, hch2.DatIni) <= ?)");
	    queryOperacao.append(" INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.numloc = cob.usu_numloctra AND orn.taborg = cob.usu_taborgtra ");
	    queryOperacao.append(" INNER JOIN USU_T010MoCo mo WITH (NOLOCK) ON mo.USU_CodMot = cob.USU_codMot ");
	    queryOperacao.append(" WHERE cob.USU_DatAlt <= ? AND cob.USU_DatFim >= ? ");
	    queryOperacao.append(" AND orn.USU_CodCcu in (" + centroCusto + ") ");
	    queryOperacao.append(" AND fun.numemp = ? AND fun.numcad = ? AND fun.tipcol = ? ");



	    PreparedStatement stOperacao = connVetorh.prepareStatement(queryOperacao.toString());

	    stOperacao.setTimestamp(1, new Timestamp(dataToleranciaIni.getTimeInMillis()));
	    stOperacao.setTimestamp(2, new Timestamp(dataToleranciaIni.getTimeInMillis()));
	    stOperacao.setTimestamp(3, new Timestamp(dataToleranciaFim.getTimeInMillis()));
	    stOperacao.setLong(4, colaborador.getNumeroEmpresa());
	    stOperacao.setLong(5, colaborador.getNumeroCadastro());
	    stOperacao.setLong(6, colaborador.getTipoColaborador());

	    ResultSet rsOperacao = stOperacao.executeQuery();

	    while (rsOperacao.next())
	    {
		EntradaAutorizadaVO entrada = new EntradaAutorizadaVO();
		EscalaVO escala = new EscalaVO();

		GregorianCalendar dataInicio = (GregorianCalendar) dataAtual.clone();
		GregorianCalendar dataFim = (GregorianCalendar) dataAtual.clone();

		cobertura.setNumeroEmpresa(rsOperacao.getLong("usu_numemp"));
		cobertura.setTipoColaborador(rsOperacao.getLong("usu_tipcol"));
		cobertura.setNumeroCadastro(rsOperacao.getLong("usu_numcad"));
		cobertura.setNumeroCracha(rsOperacao.getString("numcra"));
		cobertura.setNumeroLocal(rsOperacao.getLong("numloc"));
		cobertura.setCodigoOrganograma(rsOperacao.getLong("taborg"));

		dataInicio.setTime(rsOperacao.getTimestamp("usu_datalt"));
		dataFim.setTime(rsOperacao.getTimestamp("usu_datfim"));

		Long empresaSubstituido = rsOperacao.getLong("USU_NumEmpCob");
		Long cadastroSubstituido = rsOperacao.getLong("USU_NumCadCob");

		entrada.setDescricaoCobertura(rsOperacao.getString("usu_despre"));

		HorarioVO horario = new HorarioVO();
		horario.setDataInicial(dataInicio);
		horario.setDataFinal(dataFim);

		// Se USU_NumCadCob é igual a 0, é cobertura de posto, senão é cobertura de colaborador
		if (rsOperacao.getLong("USU_NumCadCob") == 0L)
		{
		    escala.setHorarios(getHorariosOperacoesPostos(horario, dataAtual));
		    cobertura.setEscala(escala);
		}
		else
		{
		    ColaboradorVO substituido = new ColaboradorVO();
		    substituido = getBuscaColaborador(cadastroSubstituido, empresaSubstituido, dataAtual);
		    if (substituido != null)
		    {
			escala = substituido.getEscala().clone();
			escala.setConsiderarEscala(Boolean.TRUE);

			if (substituido != null && substituido.getNumeroCadastro().equals(rsOperacao.getLong("USU_NumCadCob")) && substituido.getNumeroEmpresa().equals(rsOperacao.getLong("USU_NumEmpCob")) && horario.getDataInicial().after(dataAtual))
			{
			    escala.setHorarios(new ArrayList<HorarioVO>());
			}
			cobertura.setEscala(escala);
		    }
		}
		entrada.setColaborador(cobertura);
		entradasAutorizadas.add(entrada);
	    }
	    rsOperacao.close();
	    stOperacao.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}

	return entradasAutorizadas;
    }

    public static Collection<EntradaAutorizadaVO> getEntradasAutorizadas(Collection<EntradaAutorizadaVO> entradasAutorizadas, PostoVO posto, GregorianCalendar dataAtual)
    {

	Collection<EntradaAutorizadaVO> listaEntradasAutorizadas = new ArrayList<EntradaAutorizadaVO>();

	if (entradasAutorizadas != null)
	{
	    for (EntradaAutorizadaVO entradaAutorizada : entradasAutorizadas)
	    {
		if (posto.getNumeroLocal().equals(entradaAutorizada.getColaborador().getNumeroLocal()))
		{
		    listaEntradasAutorizadas.add(entradaAutorizada);
		}
	    }
	}

	if (listaEntradasAutorizadas != null)
	{
	    for (EntradaAutorizadaVO entradaAutorizada : listaEntradasAutorizadas)
	    {
		entradasAutorizadas.remove(entradaAutorizada);
	    }
	}

	return listaEntradasAutorizadas;
    }

    public static void atualizaStatusColaboradores(Collection<ColaboradorVO> colaboradores, Collection<EntradaAutorizadaVO> entradasAutorizadas)
    {
	if (entradasAutorizadas != null)
	{
	    for (EntradaAutorizadaVO entradaAutorizada : entradasAutorizadas)
	    {
		if (entradaAutorizada != null)
		{
		    if (entradaAutorizada.getColaborador() != null)
		    {
			if (entradaAutorizada.getColaborador().getPresenca() != null)
			{
			    if (entradaAutorizada.getColaborador().getPresenca().getStatus() != null)
			    {
				if (!entradaAutorizada.getColaborador().getPresenca().getStatus().isStatusNa() && colaboradores != null)
				{
				    for (ColaboradorVO colaborador : colaboradores)
				    {
					if (entradaAutorizada.getColaborador().getNumeroCadastro().equals(colaborador.getNumeroCadastro()) && entradaAutorizada.getColaborador().getNumeroEmpresa().equals(colaborador.getNumeroEmpresa()) && entradaAutorizada.getColaborador().getSituacao().equals("Trabalhando"))
					{
					    StatusVO status = new StatusVO();
					    status.setCodigo("EVA");
					    status.setDescricao("Colaborador em cobertura");
					    status.setStatusCobertura();

					    if (entradaAutorizada.getColaboradorSubstituido() != null)
					    {
						status.setDescricao(getTextoCobertura(entradaAutorizada.getColaboradorSubstituido().getCodigoOrganograma(), entradaAutorizada.getColaboradorSubstituido().getNumeroLocal(), entradaAutorizada.getDescricaoCobertura()));
					    }

					    colaborador.getPresenca().setStatus(status);
					    break;
					}
				    }
				}
			    }
			    else
			    {
				log.warn("##### atualizaStatusColaboradores ENTRADA AUTORIZADA PRESENCA - " + entradaAutorizada.getColaborador().getPresenca());
			    }
			}
			else
			{
			    log.warn("##### atualizaStatusColaboradores ENTRADA AUTORIZADA COLABORADOR - " + entradaAutorizada.getColaborador());
			}
		    }
		    else
		    {
			log.warn("##### atualizaStatusColaboradores ENTRADA AUTORIZADA" + entradaAutorizada);
		    }
		}
		else
		{
		    log.warn("##### atualizaStatusColaboradores ENTRADA AUTORIZADA");
		}

	    }
	}

    }

    public static void atualizaCoberuraHE(Collection<ColaboradorVO> colaboradores, Collection<EntradaAutorizadaVO> entradasAutorizadas)
    {
	try
	{
	    if (NeoUtils.safeIsNotNull(colaboradores) && NeoUtils.safeIsNotNull(NeoUtils.safeIsNotNull(colaboradores)))
	    {
		for (EntradaAutorizadaVO ent : entradasAutorizadas)
		{
		    if (NeoUtils.safeIsNotNull(ent.getDescricaoCobertura()) && ent.getDescricaoCobertura().equals("HORA EXTRA - Cobertura Posto") && NeoUtils.safeIsNotNull(ent.getColaborador().getSituacao()) && ent.getColaborador().getSituacao().equals("Trabalhando") && NeoUtils.safeIsNotNull(ent.getColaborador().getEscala().getHorarios()) && ent.getColaborador().getEscala().getHorarios().size() > 0)
		    {

			for (ColaboradorVO col : colaboradores)
			{
			    if (NeoUtils.safeIsNotNull(ent.getColaborador().getNumeroLocal()) && NeoUtils.safeIsNotNull(col.getNumeroLocal()) && ent.getColaborador().getNumeroLocal().intValue() == col.getNumeroLocal().intValue())
			    {

				if (col.getNumeroCracha().equals(ent.getColaborador().getNumeroCracha()) && NeoUtils.safeIsNotNull(col.getEscala().getHorarios()) && col.getEscala().getHorarios().size() > 0)
				{
				    ParameterVO parameterVO = getQLParameters();
				    Long tolerancia = parameterVO.getToleranciaEntradaPre();

				    //Quando HE antes do horario
				    GregorianCalendar dataFimCobertura = (GregorianCalendar) BeanUtils.cloneBean(ent.getColaborador().getEscala().getHorarios().get(0).getDataFinal());
				    GregorianCalendar dataInicioPadraoPos = (GregorianCalendar) BeanUtils.cloneBean(col.getEscala().getHorarios().get(0).getDataInicial());
				    GregorianCalendar dataInicioPadraoPre = (GregorianCalendar) BeanUtils.cloneBean(dataInicioPadraoPos);

				    dataInicioPadraoPre.add(Calendar.MINUTE, -tolerancia.intValue());
				    dataFimCobertura.set(Calendar.SECOND, 0);
				    dataFimCobertura.set(Calendar.MILLISECOND, 0);
				    dataInicioPadraoPre.set(Calendar.SECOND, 0);
				    dataInicioPadraoPos.set(Calendar.SECOND, 0);
				    dataInicioPadraoPre.set(Calendar.MILLISECOND, 0);
				    dataInicioPadraoPos.set(Calendar.MILLISECOND, 0);

				    //Quando HE depois do horario
				    GregorianCalendar dataInicioCobertura = (GregorianCalendar) BeanUtils.cloneBean(ent.getColaborador().getEscala().getHorarios().get(0).getDataInicial());
				    GregorianCalendar dataFimPadraoPre = (GregorianCalendar) BeanUtils.cloneBean(col.getEscala().getHorarios().get(col.getEscala().getHorarios().size() - 1).getDataFinal());
				    GregorianCalendar dataFimPadraoPos = (GregorianCalendar) BeanUtils.cloneBean(dataFimPadraoPre);

				    dataFimPadraoPos.add(Calendar.MINUTE, +tolerancia.intValue());
				    dataInicioCobertura.set(Calendar.SECOND, 0);
				    dataInicioCobertura.set(Calendar.MILLISECOND, 0);
				    dataFimPadraoPre.set(Calendar.SECOND, 0);
				    dataFimPadraoPos.set(Calendar.SECOND, 0);
				    dataFimPadraoPre.set(Calendar.MILLISECOND, 0);
				    dataFimPadraoPos.set(Calendar.MILLISECOND, 0);

				    if ((dataFimCobertura.compareTo(dataInicioPadraoPre) >= 0 && dataFimCobertura.compareTo(dataInicioPadraoPos) <= 0))
				    {
					EscalaVO escala = new EscalaVO();
					escala = (EscalaVO) BeanUtils.cloneBean(col.getEscala());

					HorarioVO horario = new HorarioVO();
					horario = (HorarioVO) BeanUtils.cloneBean(escala.getHorarios().get(0));

					String descricaoAntiga = "<p><font color='black''>" + col.getEscala().toString() + "</strong></font><font color='blue''>";
					horario.setDataInicial((GregorianCalendar) ent.getColaborador().getEscala().getHorarios().get(0).getDataInicial().clone());
					ent.getColaborador().getEscala().setHorarios(new ArrayList<HorarioVO>());
					escala.getHorarios().remove(0);
					escala.getHorarios().add(horario);
					String descricaoNova = descricaoAntiga + " / " + escala.toString() + "</font></p>";
					escala.setDescricaoHorarios(descricaoNova);
					col.setEscala(escala);
				    }

				    if ((dataInicioCobertura.compareTo(dataFimPadraoPre) >= 0 && dataInicioCobertura.compareTo(dataFimPadraoPos) <= 0))
				    {
					EscalaVO escala = new EscalaVO();
					escala = (EscalaVO) BeanUtils.cloneBean(col.getEscala());

					HorarioVO horario = new HorarioVO();
					horario = (HorarioVO) BeanUtils.cloneBean(escala.getHorarios().get(escala.getHorarios().size() - 1));

					String descricaoAntiga = "<p><font color='black''>" + col.getEscala().toString() + "</strong></font><font color='blue''>";
					horario.setDataFinal((GregorianCalendar) ent.getColaborador().getEscala().getHorarios().get(0).getDataFinal().clone());
					ent.getColaborador().getEscala().setHorarios(new ArrayList<HorarioVO>());

					escala.getHorarios().remove(escala.getHorarios().size() - 1);
					escala.getHorarios().add(horario);
					String descricaoNova = descricaoAntiga + " / " + col.getEscala().toString() + "</font></p>";
					escala.setDescricaoHorarios(descricaoNova);
					col.setEscala(escala);
				    }
				}
			    }
			}
		    }
		}
	    }
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}

    }

    public static void ajustaEscalaColaborador(Collection<ClienteVO> clientes)
    {
	try
	{
	    for (ClienteVO cli : clientes)
	    {
		for (PostoVO pos : cli.getPostos())
		{
		    if (pos.getAjusteEscala() != null && pos.getAjusteEscala() != 0L && pos.getAjusteEscala() != 80L)
		    {
			for (ColaboradorVO col : pos.getColaboradores())
			{
			    if (col.getNumeroLocal().toString().equals(pos.getNumeroLocal().toString()) && col.getCodigoOrganograma().toString().equals(pos.getCodigoOrganograma().toString()) && col.getEscala() != null && col.getEscala().getHorarios() != null && !col.getEscala().getHorarios().isEmpty())
			    {
				ajustarEscala(pos.getAjusteEscala(), col, pos.getEscalaPosto());
			    }
			}
		    }
		}
	    }
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
    }

    public static void atualizaTrocaDeEscala(Collection<ColaboradorVO> colaboradores, Collection<EntradaAutorizadaVO> entradasAutorizadas)
    {
	for (EntradaAutorizadaVO entrada : entradasAutorizadas)
	{
	    if (entrada.getDescricaoCobertura().equals("QL - Inversão de Escala") && entrada.getColaborador().getEscala().getHorarios().size() > 0)
	    {
		for (ColaboradorVO col : colaboradores)
		{
		    if (col.getNumeroEmpresa().toString().equals(entrada.getColaboradorSubstituido().getNumeroEmpresa().toString()) && col.getTipoColaborador().toString().equals(entrada.getColaboradorSubstituido().getTipoColaborador().toString()) && col.getNumeroCadastro().toString().equals(entrada.getColaboradorSubstituido().getNumeroCadastro().toString()))
		    {
			col.getEscala().setHorarios(new ArrayList<HorarioVO>());
			col.getEscala().setDescricaoHorarios(col.getEscala().toString());
		    }
		}
	    }
	}
    }

    public static Boolean possuiCoberuraHEColaborador(ColaboradorVO colaborador, EntradaAutorizadaVO entradasAutorizada, String direcaoAcesso)
    {
	Boolean possui = false;

	try {

	    if (NeoUtils.safeIsNotNull(colaborador) && NeoUtils.safeIsNotNull(NeoUtils.safeIsNotNull(colaborador)))
	    {
		if (NeoUtils.safeIsNotNull(entradasAutorizada.getDescricaoCobertura()))
		{

		    if (NeoUtils.safeIsNotNull(entradasAutorizada.getColaborador().getNumeroLocal()) && NeoUtils.safeIsNotNull(colaborador.getNumeroLocal()) && entradasAutorizada.getColaborador().getNumeroLocal().intValue() == colaborador.getNumeroLocal().intValue())
		    {
			if (colaborador.getNumeroCracha().equals(entradasAutorizada.getColaborador().getNumeroCracha()) && NeoUtils.safeIsNotNull(colaborador.getEscala().getHorarios()) && colaborador.getEscala().getHorarios().size() > 0 && NeoUtils.safeIsNotNull(colaborador.getEscala().getHorarios().get(0).getDataInicial()))
			{
			    ParameterVO parameterVO = getQLParameters();
			    Long tolerancia = parameterVO.getToleranciaEntradaPre();

			    //Quando HE antes do horario
			    GregorianCalendar dataFimCobertura = (GregorianCalendar) entradasAutorizada.getColaborador().getEscala().getHorarios().get(0).getDataFinal().clone();
			    GregorianCalendar dataInicioPadraoPos = (GregorianCalendar) colaborador.getEscala().getHorarios().get(0).getDataInicial().clone();
			    GregorianCalendar dataInicioPadraoPre = (GregorianCalendar) dataInicioPadraoPos.clone();
			    dataInicioPadraoPre.add(Calendar.MINUTE, -tolerancia.intValue());
			    dataFimCobertura.set(Calendar.SECOND, 0);
			    dataFimCobertura.set(Calendar.MILLISECOND, 0);
			    dataInicioPadraoPre.set(Calendar.SECOND, 0);
			    dataInicioPadraoPos.set(Calendar.SECOND, 0);
			    dataInicioPadraoPre.set(Calendar.MILLISECOND, 0);
			    dataInicioPadraoPos.set(Calendar.MILLISECOND, 0);

			    //Quando HE depois do horario
			    GregorianCalendar dataInicioCobertura = (GregorianCalendar) entradasAutorizada.getColaborador().getEscala().getHorarios().get(0).getDataInicial().clone();
			    GregorianCalendar dataFimPadraoPre = (GregorianCalendar) colaborador.getEscala().getHorarios().get(colaborador.getEscala().getHorarios().size() - 1).getDataFinal().clone();
			    GregorianCalendar dataFimPadraoPos = (GregorianCalendar) dataFimPadraoPre.clone();
			    dataFimPadraoPos.add(Calendar.MINUTE, +tolerancia.intValue());
			    dataInicioCobertura.set(Calendar.SECOND, 0);
			    dataInicioCobertura.set(Calendar.MILLISECOND, 0);
			    dataFimPadraoPre.set(Calendar.SECOND, 0);
			    dataFimPadraoPos.set(Calendar.SECOND, 0);
			    dataFimPadraoPre.set(Calendar.MILLISECOND, 0);
			    dataFimPadraoPos.set(Calendar.MILLISECOND, 0);

			    //System.out.println("dataFimCobertura - " + NeoUtils.safeDateFormat(dataFimCobertura,"dd/MM/yyyy HH:mm:ss.SSS"));
			    //System.out.println("dataInicioPadraoPos - " + NeoUtils.safeDateFormat(dataInicioPadraoPos,"dd/MM/yyyy HH:mm:ss.SSS"));
			    //System.out.println("dataInicioPadraoPre - " + NeoUtils.safeDateFormat(dataInicioPadraoPre,"dd/MM/yyyy HH:mm:ss.SSS"));

			    //System.out.println("dataInicioCobertura - " + NeoUtils.safeDateFormat(dataInicioCobertura,"dd/MM/yyyy HH:mm:ss"));
			    //System.out.println("dataFimPadraoPre - " + NeoUtils.safeDateFormat(dataFimPadraoPre,"dd/MM/yyyy HH:mm:ss"));
			    //System.out.println("dataFimPadraoPos - " + NeoUtils.safeDateFormat(dataFimPadraoPos,"dd/MM/yyyy HH:mm:ss.SSS"));

			    if ((direcaoAcesso.equals("S") && dataInicioCobertura.compareTo(dataFimPadraoPre) >= 0 && dataInicioCobertura.compareTo(dataFimPadraoPos) <= 0))
			    {
				possui = true;
			    }

			    if ((direcaoAcesso.equals("E") && dataFimCobertura.compareTo(dataInicioPadraoPre) >= 0 && dataFimCobertura.compareTo(dataInicioPadraoPos) <= 0))
			    {
				possui = true;
			    }
			}
		    }
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return possui;
    }


    public static List<EscalaVO> getEscalasColaboradores(GregorianCalendar dataAtual, Long codEscala, Long codTurma, boolean validaAfa){

	List<EscalaVO> escalas = new ArrayList<EscalaVO>();

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    StringBuffer queryHorariosEscalas = new StringBuffer();
	    queryHorariosEscalas.append(" SELECT esc.CodEsc, esc.NomEsc, tma.CodTma, mhr.SeqMar, mhr.HorBat, 0 AS Dia, hori.TipHor  ");
	    queryHorariosEscalas.append(" FROM R006ESC esc ");
	    queryHorariosEscalas.append(" INNER JOIN R006TMA tma ON tma.CodEsc = esc.CodEsc ");
	    queryHorariosEscalas.append(" INNER JOIN R006HOR hor ON hor.CodEsc = esc.CodEsc ");
	    queryHorariosEscalas.append(" INNER JOIN R004MHR mhr ON mhr.CodHor = hor.CodHor ");
	    queryHorariosEscalas.append(" INNER JOIN R004HOR hori ON hori.CodHor = hor.CodHor ");
	    queryHorariosEscalas.append(" WHERE DATEDIFF(dd,tma.DatBas,?-1) % (SELECT COUNT(*) FROM R006HOR hr2 WHERE hr2.CodEsc = hor.CodEsc) + 1 = hor.SeqReg ");
	    queryHorariosEscalas.append(" AND ((esc.usu_gruesc in (1,2,7) AND mhr.UsoBat IN (1,2,3)) OR (esc.usu_gruesc in (3,4,5,6,8,9,10,11,12,13))) ");
	    if (codEscala != 0L)
	    {
		queryHorariosEscalas.append(" AND hor.CodEsc = ?");
	    }
	    if (codTurma != 0L)
	    {
		queryHorariosEscalas.append(" AND tma.CodTma = ?");
	    }
	    queryHorariosEscalas.append(" GROUP BY esc.CodEsc, esc.NomEsc, tma.CodTma, mhr.SEQMAR, mhr.HorBat, hori.TipHor ");
	    queryHorariosEscalas.append(" UNION ");
	    queryHorariosEscalas.append(" SELECT esc.CodEsc, esc.NomEsc, tma.CodTma, mhr.SeqMar, mhr.HorBat, 1 AS Dia, hori.TipHor  ");
	    queryHorariosEscalas.append(" FROM R006ESC esc ");
	    queryHorariosEscalas.append(" INNER JOIN R006TMA tma ON tma.CodEsc = esc.CodEsc ");
	    queryHorariosEscalas.append(" INNER JOIN R006HOR hor ON hor.CodEsc = esc.CodEsc ");
	    queryHorariosEscalas.append(" INNER JOIN R004MHR mhr ON mhr.CodHor = hor.CodHor ");
	    queryHorariosEscalas.append(" INNER JOIN R004HOR hori ON hori.CodHor = hor.CodHor ");
	    queryHorariosEscalas.append(" WHERE DATEDIFF(dd,tma.DatBas,?) % (SELECT COUNT(*) FROM R006HOR hr2 WHERE hr2.CodEsc = hor.CodEsc) + 1 = hor.SeqReg ");
	    queryHorariosEscalas.append(" AND ((esc.usu_gruesc in (1,2,7) AND mhr.UsoBat IN (1,2,3)) OR (esc.usu_gruesc in (3,4,5,6,8,9,10,11,12,13))) ");
	    if (codEscala != 0L)
	    {
		queryHorariosEscalas.append(" AND hor.CodEsc = ?");
	    }
	    if (codTurma != 0L)
	    {
		queryHorariosEscalas.append(" AND tma.CodTma = ?");
	    }
	    queryHorariosEscalas.append(" GROUP BY esc.CodEsc, esc.NomEsc, tma.CodTma, mhr.SeqMar, mhr.HorBat, hori.TipHor ");
	    queryHorariosEscalas.append(" ORDER BY esc.CodEsc, tma.CodTma, Dia, mhr.SeqMar  ");

	    PreparedStatement stHorariosEscalas = connVetorh.prepareStatement(queryHorariosEscalas.toString());
	    stHorariosEscalas.setTimestamp(1, new Timestamp(dataAtual.getTimeInMillis()));

	    if (codEscala != 0L)
	    {
		stHorariosEscalas.setLong(2, codEscala);
		stHorariosEscalas.setLong(3, codTurma);
		stHorariosEscalas.setTimestamp(4, new Timestamp(dataAtual.getTimeInMillis()));
		stHorariosEscalas.setLong(5, codEscala);
		stHorariosEscalas.setLong(6, codTurma);
	    }
	    else
	    {
		stHorariosEscalas.setTimestamp(2, new Timestamp(dataAtual.getTimeInMillis()));
	    }

	    ResultSet rs = stHorariosEscalas.executeQuery();
	    ResultSetMetaData rsmd = rs.getMetaData();

	    int numColumns = rsmd.getColumnCount();
	    //List listHorarios = new ArrayList();
	    List<HorariosEscalaVO> listHorariosEscala = new ArrayList<HorariosEscalaVO>();

	    Long codigoEscalaAnterior = 0L;
	    Long codigoTurmaAnterior = 0L;
	    int stackPointer = 0;
	    while (rs.next())
	    {

		Long codigoEscala = rs.getLong("CodEsc");
		Long codigoTurma = rs.getLong("CodTma");
		String nomeEscala = rs.getString("NomEsc");


		if (codigoEscalaAnterior.equals(codigoEscala) && codigoTurmaAnterior.equals(codigoTurma) ){ // só adiciona um novo horario para uma escala e turma distinta

		    HorariosEscalaHorarioVO heHoVO = new HorariosEscalaHorarioVO();
		    heHoVO.setSeqMar(rs.getLong("SeqMar"));
		    heHoVO.setHorBat(rs.getLong("HorBat"));
		    heHoVO.setDia(rs.getLong("Dia"));
		    heHoVO.setTipoHorario(rs.getLong("TipHor"));

		    //listHorariosEscala.get(stackPointer-1).getHorarios().add(heHoVO);

		    ArrayList<HorariosEscalaHorarioVO> listaH = (ArrayList<HorariosEscalaHorarioVO>) (listHorariosEscala.get(stackPointer-1)).getHorarios();
		    listaH.add(heHoVO);

		    listHorariosEscala.get(stackPointer-1).setHorarios(listaH);


		}else{ // cria novo objeto para escala e turma
		    codigoEscalaAnterior = codigoEscala;
		    codigoTurmaAnterior = codigoTurma;

		    HorariosEscalaVO heVO = new HorariosEscalaVO();
		    heVO.setCodEsc(codigoEscala);
		    heVO.setCodTma(codigoTurma);
		    heVO.setNomEsc(nomeEscala);

		    ArrayList<HorariosEscalaHorarioVO> horarios = new ArrayList<HorariosEscalaHorarioVO>();

		    HorariosEscalaHorarioVO heHoVO = new HorariosEscalaHorarioVO();
		    heHoVO.setSeqMar(rs.getLong("SeqMar"));
		    heHoVO.setHorBat(rs.getLong("HorBat"));
		    heHoVO.setDia(rs.getLong("Dia"));
		    heHoVO.setTipoHorario(rs.getLong("TipHor"));
		    horarios.add(heHoVO);

		    heVO.setHorarios(horarios);

		    listHorariosEscala.add(heVO);
		    stackPointer++;
		}

		//System.out.println("[    ]" + listHorariosEscala.get(stackPointer-1) );
	    }

	    OrsegupsUtils.closeConnection(null, stHorariosEscalas, rs);
	    /*rs.close();
			stHorariosEscalas.close();*/

	    Long codigoEscalaLoop = 0L;
	    Long codigoTurmaLoop = 0L;

	    for (HorariosEscalaVO heVO: listHorariosEscala){

		Long codigoEscala = heVO.getCodEsc();
		Long codigoTurma = heVO.getCodTma();

		if (!codigoEscalaLoop.equals(codigoEscala) || !codigoTurmaLoop.equals(codigoTurma))
		{
		    codigoEscalaLoop = codigoEscala;
		    codigoTurmaLoop = codigoTurma;

		    EscalaVO escala = new EscalaVO();

		    escala.setCodigoEscala(codigoEscala);
		    escala.setCodigoTurma(codigoTurma);
		    escala.setDescricao( heVO.getNomEsc() + " / " + heVO.getCodTma() );
		    escala.setHorarios(new ArrayList<HorarioVO>());

		    Integer horarioAnterior = 0;
		    Integer diaRegistroAnterior = 0;

		    boolean addDiaAnterior = false;
		    boolean addDiaAtual = false;

		    List<List> listDatas = new ArrayList<List>();

		    Boolean diaTurma = false;
		    Long tipoHorario = null;
		    for(HorariosEscalaHorarioVO heHoVO: heVO.getHorarios() ){
			if (heHoVO.getTipoHorario() != null && heHoVO.getTipoHorario().equals(5L)){
			    tipoHorario = 5L;
			}
			Integer horario = heHoVO.getHorBat().intValue();
			Integer seq = heHoVO.getSeqMar().intValue();

			if (horario == 1439 && seq == 1)
			{
			    horario = 0;
			}
			Integer diaRegistro = heHoVO.getDia().intValue(); // 0 - para dia anterior | 1 - para dia atual

			int hora = horario.intValue() / 60;
			int minuto = horario.intValue() % 60;

			GregorianCalendar dataRegistro = (GregorianCalendar) dataAtual.clone();
			dataRegistro.set(Calendar.HOUR_OF_DAY, hora);
			dataRegistro.set(Calendar.MINUTE, minuto);
			dataRegistro.set(Calendar.SECOND, 0);

			/*
			 * Se o registro é 0 significa que a data é anterior a data atual
			 * Se o registro é 1 significa que a data é a atual
			 */

			if (!diaRegistroAnterior.equals(diaRegistro))
			{
			    horarioAnterior = 0;
			}

			if (diaRegistro.intValue() == 0)
			{
			    dataRegistro.add(Calendar.DAY_OF_MONTH, -1);

			    if (horarioAnterior > horario || addDiaAnterior)
			    {
				//iserir hora 2359
				if (!addDiaAnterior && hora != 23 && minuto != 59)
				{
				    GregorianCalendar dataAnt = (GregorianCalendar) dataRegistro.clone();
				    dataAnt.set(Calendar.HOUR_OF_DAY, 23);
				    dataAnt.set(Calendar.MINUTE, 59);
				    List itens = new ArrayList();
				    itens.add(dataAnt);
				    itens.add(diaRegistro);
				    listDatas.add(itens);

				    GregorianCalendar dataInicio = (GregorianCalendar) dataRegistro.clone();
				    dataInicio.add(Calendar.DAY_OF_MONTH, 1);
				    dataInicio.set(Calendar.HOUR_OF_DAY, 0);
				    dataInicio.set(Calendar.MINUTE, 0);

				    //quando montar a arvore de escalas
				    if (codEscala == 0L)
				    {
					dataInicio.set(Calendar.SECOND, 00);
				    }
				    else
					//quando busca a escala individual para verificar a tolerencia
				    {
					dataInicio.set(Calendar.SECOND, 59);
				    }

				    itens = new ArrayList();
				    itens.add(dataInicio);
				    itens.add(diaRegistro);
				    listDatas.add(itens);
				}
				addDiaAnterior = true;
				dataRegistro.add(Calendar.DAY_OF_MONTH, 1);
			    }
			}
			else
			{
			    if (horarioAnterior > horario || addDiaAtual)
			    {

				if (!addDiaAtual)
				{
				    GregorianCalendar dataAnt = (GregorianCalendar) dataRegistro.clone();
				    dataAnt.set(Calendar.HOUR_OF_DAY, 23);
				    dataAnt.set(Calendar.MINUTE, 59);

				    //quando montar a arvore de escalas
				    if (codEscala == 0L)
				    {
					dataAnt.set(Calendar.SECOND, 00);
				    }
				    else
					//quando busca a escala individual para verificar a tolerencia
				    {
					dataAnt.set(Calendar.SECOND, 59);
				    }

				    List itens = new ArrayList();
				    itens.add(dataAnt);
				    itens.add(diaRegistro);
				    listDatas.add(itens);

				    GregorianCalendar dataInicio = (GregorianCalendar) dataRegistro.clone();
				    dataInicio.add(Calendar.DAY_OF_MONTH, 1);
				    dataInicio.set(Calendar.HOUR_OF_DAY, 0);
				    dataInicio.set(Calendar.MINUTE, 0);
				    dataInicio.set(Calendar.SECOND, 0);
				    itens = new ArrayList();
				    itens.add(dataInicio);
				    itens.add(diaRegistro);
				    listDatas.add(itens);
				}
				addDiaAtual = true;
				dataRegistro.add(Calendar.DAY_OF_MONTH, 1);
			    }
			}

			List itens = new ArrayList();
			itens.add(dataRegistro);
			itens.add(diaRegistro);
			listDatas.add(itens);
			horarioAnterior = horario;
			diaRegistroAnterior = diaRegistro;


		    } // end for horariosEscalaHorario

		    //Compara se a data atual é igual aos registros do HorarioVO
		    GregorianCalendar parametroDataAtual = (GregorianCalendar) dataAtual.clone();
		    int diaAtual = parametroDataAtual.get(Calendar.DAY_OF_MONTH);
		    int cont = 0;
		    GregorianCalendar horIniEsc = new GregorianCalendar();
		    for (int l = 0; l < listDatas.size(); l++)
		    {
			HorarioVO horario = new HorarioVO();
			horario.setTipoHorario(tipoHorario);
			Boolean isDataInicial = l % 2 == 0;

			if (isDataInicial)
			{
			    //							if(horario.getDataInicial() == null && cont == 0 && validaAfa){
			    //								horIniEsc = (GregorianCalendar) ((GregorianCalendar) listDatas.get(0).get(0)).clone();
			    //								cont = 1;
			    //							}
			    GregorianCalendar dataIni = (GregorianCalendar) listDatas.get(l).get(0);
			    GregorianCalendar dataFim = (GregorianCalendar) listDatas.get(l + 1).get(0);

			    int diaDataIni = dataIni.get(Calendar.DAY_OF_MONTH);

			    //Verifica se e o dia atual
			    if (diaDataIni == diaAtual)
			    {
				Calendar calendarIni = Calendar.getInstance();
				Calendar calendarFim = Calendar.getInstance();

				calendarIni.setTime(dataIni.getTime());
				calendarFim.setTime(dataFim.getTime());

				if (calendarIni.get(Calendar.SECOND) == 00)
				{
				    horario.setDataInicial(dataIni);
				}
				//									else if(validaAfa){
				//									horario.setDataInicial(horIniEsc);
				//									
				//								}

				if (calendarFim.get(Calendar.SECOND) == 00)
				{
				    horario.setDataFinal(dataFim);
				}
				
				//horario.setDataFinal(dataFim);
				escala.getHorarios().add(horario);
			    }
			}
		    }
		    String descHorarios = escala.toString();
		    escala.setDescricaoHorarios(descHorarios);
		    escalas.add(escala);
		    //System.out.println(descHorarios );

		}

	    }

	    /*for (int i = 0; i < listHorarios.size(); i++)
			{

				Long codigoEscala = Long.parseLong((String) ((List) listHorarios.get(i)).get(0));
				Long codigoTurma = Long.parseLong((String) ((List) listHorarios.get(i)).get(2));


				if (!codigoEscalaLoop.equals(codigoEscala) || !codigoTurmaLoop.equals(codigoTurma))
				{
					codigoEscalaLoop = codigoEscala;
					codigoTurmaLoop = codigoTurma;

					EscalaVO escala = new EscalaVO();

					escala.setCodigoEscala(codigoEscala);
					escala.setCodigoTurma(codigoTurma);
					escala.setDescricao((String) ((List) listHorarios.get(i)).get(1) + " / " + (String) ((List) listHorarios.get(i)).get(2));
					escala.setHorarios(new ArrayList<HorarioVO>());

					Integer horarioAnterior = 0;
					Integer diaRegistroAnterior = 0;

					boolean addDiaAnterior = false;
					boolean addDiaAtual = false;

					List<List> listDatas = new ArrayList<List>();

					Boolean diaTurma = false;

					for (int j = i; j < listHorarios.size(); j++)
					{

						if (Long.valueOf((String) ((List) listHorarios.get(j)).get(0)).equals(codigoEscala) && Long.valueOf((String) ((List) listHorarios.get(j)).get(2)).equals(codigoTurma))
						{

							Integer horario = Integer.valueOf((String) ((List) listHorarios.get(j)).get(4));
							Integer seq = Integer.valueOf((String) ((List) listHorarios.get(j)).get(3));

							if (horario == 1439 && seq == 1)
							{
								horario = 0;
							}
							Integer diaRegistro = Integer.valueOf((String) ((List) listHorarios.get(j)).get(5)); // 0 - para dia anterior | 1 - para dia atual

							int hora = horario.intValue() / 60;
							int minuto = horario.intValue() % 60;

							GregorianCalendar dataRegistro = (GregorianCalendar) dataAtual.clone();
							dataRegistro.set(Calendar.HOUR_OF_DAY, hora);
							dataRegistro.set(Calendar.MINUTE, minuto);
							dataRegistro.set(Calendar.SECOND, 0);


	     * Se o registro é 0 significa que a data é anterior a data atual
	     * Se o registro é 1 significa que a data é a atual


							if (!diaRegistroAnterior.equals(diaRegistro))
							{
								horarioAnterior = 0;
							}

							if (diaRegistro.intValue() == 0)
							{
								dataRegistro.add(Calendar.DAY_OF_MONTH, -1);

								if (horarioAnterior > horario || addDiaAnterior)
								{
									//iserir hora 2359
									if (!addDiaAnterior && hora != 23 && minuto != 59)
									{
										GregorianCalendar dataAnt = (GregorianCalendar) dataRegistro.clone();
										dataAnt.set(Calendar.HOUR_OF_DAY, 23);
										dataAnt.set(Calendar.MINUTE, 59);
										List itens = new ArrayList();
										itens.add(dataAnt);
										itens.add(diaRegistro);
										listDatas.add(itens);

										GregorianCalendar dataInicio = (GregorianCalendar) dataRegistro.clone();
										dataInicio.add(Calendar.DAY_OF_MONTH, 1);
										dataInicio.set(Calendar.HOUR_OF_DAY, 0);
										dataInicio.set(Calendar.MINUTE, 0);

										//quando montar a arvore de escalas
										if (codEscala == 0L)
										{
											dataInicio.set(Calendar.SECOND, 00);
										}
										else
										//quando busca a escala individual para verificar a tolerencia
										{
											dataInicio.set(Calendar.SECOND, 59);
										}

										itens = new ArrayList();
										itens.add(dataInicio);
										itens.add(diaRegistro);
										listDatas.add(itens);
									}
									addDiaAnterior = true;
									dataRegistro.add(Calendar.DAY_OF_MONTH, 1);
								}
							}
							else
							{
								if (horarioAnterior > horario || addDiaAtual)
								{

									if (!addDiaAtual)
									{
										GregorianCalendar dataAnt = (GregorianCalendar) dataRegistro.clone();
										dataAnt.set(Calendar.HOUR_OF_DAY, 23);
										dataAnt.set(Calendar.MINUTE, 59);

										//quando montar a arvore de escalas
										if (codEscala == 0L)
										{
											dataAnt.set(Calendar.SECOND, 00);
										}
										else
										//quando busca a escala individual para verificar a tolerencia
										{
											dataAnt.set(Calendar.SECOND, 59);
										}

										List itens = new ArrayList();
										itens.add(dataAnt);
										itens.add(diaRegistro);
										listDatas.add(itens);

										GregorianCalendar dataInicio = (GregorianCalendar) dataRegistro.clone();
										dataInicio.add(Calendar.DAY_OF_MONTH, 1);
										dataInicio.set(Calendar.HOUR_OF_DAY, 0);
										dataInicio.set(Calendar.MINUTE, 0);
										dataInicio.set(Calendar.SECOND, 0);
										itens = new ArrayList();
										itens.add(dataInicio);
										itens.add(diaRegistro);
										listDatas.add(itens);
									}
									addDiaAtual = true;
									dataRegistro.add(Calendar.DAY_OF_MONTH, 1);
								}
							}
							List itens = new ArrayList();
							itens.add(dataRegistro);
							itens.add(diaRegistro);
							listDatas.add(itens);
							horarioAnterior = horario;
							diaRegistroAnterior = diaRegistro;

						}

					}

					//Compara se a data atual é igual aos registros do HorarioVO
					GregorianCalendar parametroDataAtual = (GregorianCalendar) dataAtual.clone();
					int diaAtual = parametroDataAtual.get(Calendar.DAY_OF_MONTH);

					for (int l = 0; l < listDatas.size(); l++)
					{
						HorarioVO horario = new HorarioVO();
						Boolean isDataInicial = l % 2 == 0;

						if (isDataInicial)
						{
							GregorianCalendar dataIni = (GregorianCalendar) listDatas.get(l).get(0);
							GregorianCalendar dataFim = (GregorianCalendar) listDatas.get(l + 1).get(0);

							int diaDataIni = dataIni.get(Calendar.DAY_OF_MONTH);

							//Verifica se e o dia atual
							if (diaDataIni == diaAtual)
							{
								Calendar calendarIni = Calendar.getInstance();
								Calendar calendarFim = Calendar.getInstance();

								calendarIni.setTime(dataIni.getTime());
								calendarFim.setTime(dataFim.getTime());

								if (calendarIni.get(Calendar.SECOND) == 00)
								{
									horario.setDataInicial(dataIni);
								}

								if (calendarFim.get(Calendar.SECOND) == 00)
								{
									horario.setDataFinal(dataFim);
								}

								//horario.setDataFinal(dataFim);
								escala.getHorarios().add(horario);
							}
						}
					}
					String descHorarios = escala.toString();
					escala.setDescricaoHorarios(descHorarios);
					escalas.add(escala);
					//System.out.println(descHorarios);
				}
			}*/
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	    //System.out.println("");
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}

	return escalas;

    }

    public static List<EscalaVO> getEscalasColaboradoresOld(GregorianCalendar dataAtual, Long codEscala, Long codTurma)
    {
	List<EscalaVO> escalas = new ArrayList<EscalaVO>();

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    StringBuffer queryHorariosEscalas = new StringBuffer();
	    queryHorariosEscalas.append(" SELECT esc.CodEsc, esc.NomEsc, tma.CodTma, mhr.SeqMar, mhr.HorBat, 0 AS Dia  ");
	    queryHorariosEscalas.append(" FROM R006ESC esc ");
	    queryHorariosEscalas.append(" INNER JOIN R006TMA tma ON tma.CodEsc = esc.CodEsc ");
	    queryHorariosEscalas.append(" INNER JOIN R006HOR hor ON hor.CodEsc = esc.CodEsc ");
	    queryHorariosEscalas.append(" INNER JOIN R004MHR mhr ON mhr.CodHor = hor.CodHor ");
	    queryHorariosEscalas.append(" WHERE DATEDIFF(dd,tma.DatBas,?-1) % (SELECT COUNT(*) FROM R006HOR hr2 WHERE hr2.CodEsc = hor.CodEsc) + 1 = hor.SeqReg ");
	    queryHorariosEscalas.append(" AND ((esc.usu_gruesc in (1,2,7) AND mhr.UsoBat IN (1,2,3)) OR (esc.usu_gruesc in (3,4,5,6,8,9,10,11,12,13))) ");
	    if (codEscala != 0L)
	    {
		queryHorariosEscalas.append(" AND hor.CodEsc = ?");
	    }
	    if (codTurma != 0L)
	    {
		queryHorariosEscalas.append(" AND tma.CodTma = ?");
	    }
	    queryHorariosEscalas.append(" GROUP BY esc.CodEsc, esc.NomEsc, tma.CodTma, mhr.SEQMAR, mhr.HorBat ");
	    queryHorariosEscalas.append(" UNION ");
	    queryHorariosEscalas.append(" SELECT esc.CodEsc, esc.NomEsc, tma.CodTma, mhr.SeqMar, mhr.HorBat, 1 AS Dia  ");
	    queryHorariosEscalas.append(" FROM R006ESC esc ");
	    queryHorariosEscalas.append(" INNER JOIN R006TMA tma ON tma.CodEsc = esc.CodEsc ");
	    queryHorariosEscalas.append(" INNER JOIN R006HOR hor ON hor.CodEsc = esc.CodEsc ");
	    queryHorariosEscalas.append(" INNER JOIN R004MHR mhr ON mhr.CodHor = hor.CodHor ");
	    queryHorariosEscalas.append(" WHERE DATEDIFF(dd,tma.DatBas,?) % (SELECT COUNT(*) FROM R006HOR hr2 WHERE hr2.CodEsc = hor.CodEsc) + 1 = hor.SeqReg ");
	    queryHorariosEscalas.append(" AND ((esc.usu_gruesc in (1,2,7) AND mhr.UsoBat IN (1,2,3)) OR (esc.usu_gruesc in (3,4,5,6,8,9,10,11,12,13))) ");
	    if (codEscala != 0L)
	    {
		queryHorariosEscalas.append(" AND hor.CodEsc = ?");
	    }
	    if (codTurma != 0L)
	    {
		queryHorariosEscalas.append(" AND tma.CodTma = ?");
	    }
	    queryHorariosEscalas.append(" GROUP BY esc.CodEsc, esc.NomEsc, tma.CodTma, mhr.SeqMar, mhr.HorBat ");
	    queryHorariosEscalas.append(" ORDER BY esc.CodEsc, tma.CodTma, Dia, mhr.SeqMar  ");

	    PreparedStatement stHorariosEscalas = connVetorh.prepareStatement(queryHorariosEscalas.toString());
	    stHorariosEscalas.setTimestamp(1, new Timestamp(dataAtual.getTimeInMillis()));

	    if (codEscala != 0L)
	    {
		stHorariosEscalas.setLong(2, codEscala);
		stHorariosEscalas.setLong(3, codTurma);
		stHorariosEscalas.setTimestamp(4, new Timestamp(dataAtual.getTimeInMillis()));
		stHorariosEscalas.setLong(5, codEscala);
		stHorariosEscalas.setLong(6, codTurma);
	    }
	    else
	    {
		stHorariosEscalas.setTimestamp(2, new Timestamp(dataAtual.getTimeInMillis()));
	    }

	    ResultSet rs = stHorariosEscalas.executeQuery();
	    ResultSetMetaData rsmd = rs.getMetaData();

	    int numColumns = rsmd.getColumnCount();
	    List listHorarios = new ArrayList();
	    while (rs.next())
	    {
		List columns = new ArrayList();
		for (int i = 1; i <= numColumns; i++)
		{
		    columns.add(rs.getString(i));
		}
		listHorarios.add(columns);
	    }

	    rs.close();
	    stHorariosEscalas.close();

	    Long codigoEscalaLoop = 0L;
	    Long codigoTurmaLoop = 0L;

	    for (int i = 0; i < listHorarios.size(); i++)
	    {

		Long codigoEscala = Long.parseLong((String) ((List) listHorarios.get(i)).get(0));
		Long codigoTurma = Long.parseLong((String) ((List) listHorarios.get(i)).get(2));


		if (!codigoEscalaLoop.equals(codigoEscala) || !codigoTurmaLoop.equals(codigoTurma))
		{
		    codigoEscalaLoop = codigoEscala;
		    codigoTurmaLoop = codigoTurma;

		    EscalaVO escala = new EscalaVO();

		    escala.setCodigoEscala(codigoEscala);
		    escala.setCodigoTurma(codigoTurma);
		    escala.setDescricao((String) ((List) listHorarios.get(i)).get(1) + " / " + (String) ((List) listHorarios.get(i)).get(2));
		    escala.setHorarios(new ArrayList<HorarioVO>());

		    Integer horarioAnterior = 0;
		    Integer diaRegistroAnterior = 0;

		    boolean addDiaAnterior = false;
		    boolean addDiaAtual = false;

		    List<List> listDatas = new ArrayList<List>();

		    Boolean diaTurma = false;

		    for (int j = i; j < listHorarios.size(); j++)
		    {

			if (Long.valueOf((String) ((List) listHorarios.get(j)).get(0)).equals(codigoEscala) && Long.valueOf((String) ((List) listHorarios.get(j)).get(2)).equals(codigoTurma))
			{

			    Integer horario = Integer.valueOf((String) ((List) listHorarios.get(j)).get(4));
			    Integer seq = Integer.valueOf((String) ((List) listHorarios.get(j)).get(3));

			    if (horario == 1439 && seq == 1)
			    {
				horario = 0;
			    }
			    Integer diaRegistro = Integer.valueOf((String) ((List) listHorarios.get(j)).get(5)); // 0 - para dia anterior | 1 - para dia atual

			    int hora = horario.intValue() / 60;
			    int minuto = horario.intValue() % 60;

			    GregorianCalendar dataRegistro = (GregorianCalendar) dataAtual.clone();
			    dataRegistro.set(Calendar.HOUR_OF_DAY, hora);
			    dataRegistro.set(Calendar.MINUTE, minuto);
			    dataRegistro.set(Calendar.SECOND, 0);

			    /*
			     * Se o registro é 0 significa que a data é anterior a data atual
			     * Se o registro é 1 significa que a data é a atual
			     */

			    if (!diaRegistroAnterior.equals(diaRegistro))
			    {
				horarioAnterior = 0;
			    }

			    if (diaRegistro.intValue() == 0)
			    {
				dataRegistro.add(Calendar.DAY_OF_MONTH, -1);

				if (horarioAnterior > horario || addDiaAnterior)
				{
				    //iserir hora 2359
				    if (!addDiaAnterior && hora != 23 && minuto != 59)
				    {
					GregorianCalendar dataAnt = (GregorianCalendar) dataRegistro.clone();
					dataAnt.set(Calendar.HOUR_OF_DAY, 23);
					dataAnt.set(Calendar.MINUTE, 59);
					List itens = new ArrayList();
					itens.add(dataAnt);
					itens.add(diaRegistro);
					listDatas.add(itens);

					GregorianCalendar dataInicio = (GregorianCalendar) dataRegistro.clone();
					dataInicio.add(Calendar.DAY_OF_MONTH, 1);
					dataInicio.set(Calendar.HOUR_OF_DAY, 0);
					dataInicio.set(Calendar.MINUTE, 0);

					//quando montar a arvore de escalas
					if (codEscala == 0L)
					{
					    dataInicio.set(Calendar.SECOND, 00);
					}
					else
					    //quando busca a escala individual para verificar a tolerencia
					{
					    dataInicio.set(Calendar.SECOND, 59);
					}

					itens = new ArrayList();
					itens.add(dataInicio);
					itens.add(diaRegistro);
					listDatas.add(itens);
				    }
				    addDiaAnterior = true;
				    dataRegistro.add(Calendar.DAY_OF_MONTH, 1);
				}
			    }
			    else
			    {
				if (horarioAnterior > horario || addDiaAtual)
				{

				    if (!addDiaAtual)
				    {
					GregorianCalendar dataAnt = (GregorianCalendar) dataRegistro.clone();
					dataAnt.set(Calendar.HOUR_OF_DAY, 23);
					dataAnt.set(Calendar.MINUTE, 59);

					//quando montar a arvore de escalas
					if (codEscala == 0L)
					{
					    dataAnt.set(Calendar.SECOND, 00);
					}
					else
					    //quando busca a escala individual para verificar a tolerencia
					{
					    dataAnt.set(Calendar.SECOND, 59);
					}

					List itens = new ArrayList();
					itens.add(dataAnt);
					itens.add(diaRegistro);
					listDatas.add(itens);

					GregorianCalendar dataInicio = (GregorianCalendar) dataRegistro.clone();
					dataInicio.add(Calendar.DAY_OF_MONTH, 1);
					dataInicio.set(Calendar.HOUR_OF_DAY, 0);
					dataInicio.set(Calendar.MINUTE, 0);
					dataInicio.set(Calendar.SECOND, 0);
					itens = new ArrayList();
					itens.add(dataInicio);
					itens.add(diaRegistro);
					listDatas.add(itens);
				    }
				    addDiaAtual = true;
				    dataRegistro.add(Calendar.DAY_OF_MONTH, 1);
				}
			    }
			    List itens = new ArrayList();
			    itens.add(dataRegistro);
			    itens.add(diaRegistro);
			    listDatas.add(itens);
			    horarioAnterior = horario;
			    diaRegistroAnterior = diaRegistro;

			}

		    }

		    //Compara se a data atual é igual aos registros do HorarioVO
		    GregorianCalendar parametroDataAtual = (GregorianCalendar) dataAtual.clone();
		    int diaAtual = parametroDataAtual.get(Calendar.DAY_OF_MONTH);

		    for (int l = 0; l < listDatas.size(); l++)
		    {
			HorarioVO horario = new HorarioVO();
			Boolean isDataInicial = l % 2 == 0;

			if (isDataInicial)
			{
			    GregorianCalendar dataIni = (GregorianCalendar) listDatas.get(l).get(0);
			    GregorianCalendar dataFim = (GregorianCalendar) listDatas.get(l + 1).get(0);

			    int diaDataIni = dataIni.get(Calendar.DAY_OF_MONTH);

			    //Verifica se e o dia atual
			    if (diaDataIni == diaAtual)
			    {
				Calendar calendarIni = Calendar.getInstance();
				Calendar calendarFim = Calendar.getInstance();

				calendarIni.setTime(dataIni.getTime());
				calendarFim.setTime(dataFim.getTime());

				if (calendarIni.get(Calendar.SECOND) == 00)
				{
				    horario.setDataInicial(dataIni);
				}

				if (calendarFim.get(Calendar.SECOND) == 00)
				{
				    horario.setDataFinal(dataFim);
				}

				//horario.setDataFinal(dataFim);
				escala.getHorarios().add(horario);
			    }
			}
		    }
		    String descHorarios = escala.toString();
		    escala.setDescricaoHorarios(descHorarios);
		    escalas.add(escala);
		    //System.out.println(descHorarios);
		}
	    }
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	    //System.out.println("");
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}

	return escalas;

    }

    public static void setEscalaColaborador(List<EscalaVO> escalas, ColaboradorVO colaborador)
    {
	EscalaVO escalaColaborador = new EscalaVO();
	escalaColaborador.setCodigoEscala(0L);
	escalaColaborador.setCodigoTurma(0L);
	escalaColaborador.setDescricao("&nbsp;");
	escalaColaborador.setHorarios(new ArrayList<HorarioVO>());
	colaborador.setEscala(escalaColaborador);
	for (EscalaVO escala : escalas)
	{
	    if (escala.getCodigoEscala().equals(colaborador.getCodigoEscala()) && escala.getCodigoTurma().equals(colaborador.getTurmaEscala()))
	    {
		colaborador.setEscala(escala.clone());
		colaborador.getEscala().setDescricaoHorarios(escala.getDescricaoHorarios());
		break;

	    }

	}

	colaborador.getEscala().setConsiderarEscala(Boolean.TRUE);
	if (colaborador.isAcessoBloqueado())
	{
	    colaborador.getEscala().setConsiderarEscala(Boolean.FALSE);
	}
	if (!colaborador.isControlaAcesso())
	{
	    colaborador.getEscala().setConsiderarEscala(Boolean.FALSE);
	}

    }

    public static void setEscalaColaboradorUnico(ColaboradorVO colaborador, GregorianCalendar dataAtual)
    {
	List<EscalaVO> escalas = new ArrayList<EscalaVO>();
	escalas = getEscalasColaboradores(dataAtual, colaborador.getCodigoEscala(), colaborador.getTurmaEscala(),false);

	if (escalas != null && !escalas.isEmpty())
	{
	    colaborador.setEscala(escalas.get(0));
	    colaborador.getEscala().setConsiderarEscala(Boolean.TRUE);

	    if (colaborador.isAcessoBloqueado())
	    {
		colaborador.getEscala().setConsiderarEscala(Boolean.FALSE);
	    }
	    if (!colaborador.isControlaAcesso())
	    {
		colaborador.getEscala().setConsiderarEscala(Boolean.FALSE);
	    }
	}
	else
	{
	    colaborador.setEscala(new EscalaVO());
	}
    }

    public static List<HorarioVO> getHorariosEscalaPosto(Long codigoEscala, GregorianCalendar dataAtual)
    {

	List<HorarioVO> horariosPosto = new ArrayList<HorarioVO>();
	if (codigoEscala.equals(0L))
	{
	    horariosPosto = null;
	}
	else
	{
	    QLGroupFilter horarioFilter = new QLGroupFilter("AND");
	    horarioFilter.addFilter(new QLEqualsFilter("usu_codesc", codigoEscala));
	    horarioFilter.addFilter(new QLEqualsFilter("usu_diasem", getDiaPresenca(dataAtual)));

	    List<NeoObject> horarios = PersistEngine.getObjects(((ExternalEntityInfo) EntityRegister.getInstance().getCache().getByType("SAPIENSUSUTHOR")).getEntityClass(), horarioFilter, -1, -1, " usu_horini ");
	    if (NeoUtils.safeIsNotNull(horarios) && horarios.size() > 0)
	    {
		for (NeoObject horario : horarios)
		{
		    EntityWrapper wrpHorario = new EntityWrapper(horario);
		    Long horIni = (Long) wrpHorario.findField("usu_horini").getValue();
		    Long horFim = (Long) wrpHorario.findField("usu_horfim").getValue();

		    HorarioVO horarioPosto = new HorarioVO();

		    horarioPosto.setDataInicial(convertHoraSenior(dataAtual, horIni));
		    horarioPosto.setDataFinal(convertHoraSenior(dataAtual, horFim));
		    horariosPosto.add(horarioPosto);

		}
	    }
	}

	return horariosPosto;
    }

    public static EscalaPostoVO getJornadaEscalaPosto(Long codEmp, Long codFil, Long numCtr, Long numPos, String codSer, GregorianCalendar dataAtual, Long perEsc, Boolean diaSemana)
    {
	EscalaPostoVO escalaPosto = new EscalaPostoVO();

	List<HorarioVO> horariosSegunda = new ArrayList<HorarioVO>();
	List<HorarioVO> horariosTerca = new ArrayList<HorarioVO>();
	List<HorarioVO> horariosQuarta = new ArrayList<HorarioVO>();
	List<HorarioVO> horariosQuinta = new ArrayList<HorarioVO>();
	List<HorarioVO> horariosSexta = new ArrayList<HorarioVO>();
	List<HorarioVO> horariosSabado = new ArrayList<HorarioVO>();
	List<HorarioVO> horariosDomingo = new ArrayList<HorarioVO>();
	List<HorarioVO> horariosFeriado = new ArrayList<HorarioVO>();

	QLGroupFilter horarioFilter = new QLGroupFilter("AND");
	horarioFilter.addFilter(new QLEqualsFilter("usu_codemp", codEmp));
	horarioFilter.addFilter(new QLEqualsFilter("usu_numctr", numCtr));
	horarioFilter.addFilter(new QLEqualsFilter("usu_numpos", numPos));

	if (codFil != null)
	{
	    //horarioFilter.addFilter(new QLEqualsFilter("usu_codfil", codFil));
	}

	//horarioFilter.addFilter(new QLEqualsFilter("usu_codser", codSer));
	Long diaPresenca = null;
	if (diaSemana)
	{
	    //horarioFilter.addFilter(new QLEqualsFilter("usu_diasem", getDiaPresenca(dataAtual)));
	    diaPresenca = getDiaPresenca(dataAtual);
	}

	List<UsuT160JorVO> horarios = listaJornadas(codEmp, numCtr, numPos,codFil,diaPresenca);

	//List<NeoObject> horarios = PersistEngine.getObjects(((ExternalEntityInfo) EntityRegister.getInstance().getCache().getByType("SAPIENSUSUTJOR")).getEntityClass(), horarioFilter, -1, -1, " usu_diasem,usu_horini ");
	if (NeoUtils.safeIsNotNull(horarios) && horarios.size() > 0)
	{
	    for (UsuT160JorVO horario : horarios)
	    {
		/*EntityWrapper wrpHorario = new EntityWrapper(horario);
				Long horIni = (Long) wrpHorario.findField("usu_horini").getValue();
				Long horFim = (Long) wrpHorario.findField("usu_horfim").getValue();
				Long diaSem = (Long) wrpHorario.findField("usu_diasem").getValue();*/

		Long horIni = horario.getUsu_horini();
		Long horFim = horario.getUsu_horfim(); 
		Long diaSem = horario.getUsu_diasem();

		Integer diaSemInt = Integer.valueOf(diaSem.toString());

		HorarioVO horarioPosto = new HorarioVO();

		horarioPosto.setDataInicial(convertHoraSenior(dataAtual, horIni));
		horarioPosto.setDataFinal(convertHoraSenior(dataAtual, horFim));

		//Caso hora 00:00 adiciona um dia
		if (horarioPosto.getDataFinal().get(Calendar.HOUR_OF_DAY) == 0 && horarioPosto.getDataFinal().get(Calendar.MINUTE) == 0)
		{
		    horarioPosto.getDataFinal().add(Calendar.DAY_OF_MONTH, +1);
		}

		switch (diaSemInt)
		{
		case 1:
		    horariosSegunda.add(horarioPosto);
		    break;
		case 2:
		    horariosTerca.add(horarioPosto);
		    break;
		case 3:
		    horariosQuarta.add(horarioPosto);
		    break;
		case 4:
		    horariosQuinta.add(horarioPosto);
		    break;
		case 5:
		    horariosSexta.add(horarioPosto);
		    break;
		case 6:
		    horariosSabado.add(horarioPosto);
		    break;
		case 7:
		    horariosDomingo.add(horarioPosto);
		    break;
		case 8:
		    horariosFeriado.add(horarioPosto);
		    break;
		default:
		    break;
		}
	    }
	    escalaPosto.setHorariosSegunda(horariosSegunda);
	    escalaPosto.setHorariosTerca(horariosTerca);
	    escalaPosto.setHorariosQuarta(horariosQuarta);
	    escalaPosto.setHorariosQuinta(horariosQuinta);
	    escalaPosto.setHorariosSexta(horariosSexta);
	    escalaPosto.setHorariosSabado(horariosSabado);
	    escalaPosto.setHorariosDomingo(horariosDomingo);
	    escalaPosto.setHorariosFeriado(horariosFeriado);
	    escalaPosto.setDiaSemana(getDiaPresenca(dataAtual));
	    escalaPosto.setPermanenciaPosto(perEsc);
	}
	else
	{
	    escalaPosto = null;
	}

	return escalaPosto;
    }

    public static EscalaPostoVO getJornadaEscalaPosto(Long numloc, Long taborg, List<EscalaPostoVO> escalas)
    {
	EscalaPostoVO escalaPosto = null;

	if (escalas != null && !escalas.isEmpty())
	{
	    for (EscalaPostoVO esc : escalas)
	    {
		if (esc.getNumloc().toString().equals(numloc.toString()) && esc.getTaborg().toString().equals(taborg.toString()))
		{
		    escalaPosto = esc;
		    break;
		}
	    }
	}
	return escalaPosto;
    }

    //Atribuição da escala correta da operação de cobertura de posto
    public static List<HorarioVO> getHorariosOperacoesPostos(HorarioVO horario, GregorianCalendar dataHoraAtual)
    {
	GregorianCalendar horarioInicio = (GregorianCalendar) horario.getDataInicial().clone();
	GregorianCalendar horarioFim = (GregorianCalendar) horario.getDataFinal().clone();
	GregorianCalendar dataAtual = OrsegupsUtils.atribuiHora(dataHoraAtual);

	//Calcula os minutos dos horários informados
	int minutosInicio = horarioInicio.get(Calendar.HOUR_OF_DAY) * 60 + horarioInicio.get(Calendar.MINUTE);
	int minutosFim = horarioFim.get(Calendar.HOUR_OF_DAY) * 60 + horarioFim.get(Calendar.MINUTE);

	horarioInicio = OrsegupsUtils.atribuiData(horarioInicio, dataAtual);
	horarioFim = OrsegupsUtils.atribuiData(horarioFim, dataAtual);

	List<HorarioVO> horarios = new ArrayList<HorarioVO>();

	if (OrsegupsUtils.atribuiHora(horario.getDataInicial()).compareTo(dataAtual) <= 0 && dataAtual.compareTo(OrsegupsUtils.atribuiHora(horario.getDataFinal())) <= 0)
	{
	    if (minutosInicio < minutosFim)
		//Caso o horário inicial for anterior ao final, a escala do dia conterá apenas um HorarioVO
	    {
		HorarioVO novoHorario = new HorarioVO();
		novoHorario.setDataInicial(horarioInicio);
		novoHorario.setDataFinal(horarioFim);
		horarios.add(novoHorario);
	    }
	    else
	    {

		HorarioVO primeiroHorario = new HorarioVO();
		HorarioVO segundoHorario = new HorarioVO();

		//Criando HorarioVO início às 00:00:00.000 e fim no horário da data inicial
		primeiroHorario.setDataInicial(OrsegupsUtils.atribuiHora(dataAtual));
		primeiroHorario.setDataFinal(horarioFim);

		//Criando HorarioVO início às 23:59:00.000 e fim no horário da data final
		GregorianCalendar horarioFimDia = OrsegupsUtils.atribuiHora(dataAtual, 23, 59, 00, 000);
		segundoHorario.setDataInicial(horarioInicio);
		segundoHorario.setDataFinal(horarioFimDia);

		if (OrsegupsUtils.atribuiHora(horario.getDataInicial()).compareTo(OrsegupsUtils.atribuiHora(dataAtual)) < 0 && OrsegupsUtils.atribuiHora(dataAtual).compareTo(OrsegupsUtils.atribuiHora(horario.getDataFinal())) < 0)
		{
		    horarios.add(primeiroHorario);
		    horarios.add(segundoHorario);
		}
		else if (OrsegupsUtils.atribuiHora(horario.getDataInicial()).compareTo(OrsegupsUtils.atribuiHora(dataAtual)) == 0)
		{
		    horarios.add(segundoHorario);
		}
		else
		{
		    horarios.add(primeiroHorario);
		}
	    }
	}
	else
	{
	    //horarios.add(horario);
	}

	return horarios;
    }

    public static StatusVO getStatusPosto(PostoVO posto, GregorianCalendar dataAtual)
    {
	StatusVO status = new StatusVO();

	if (posto.getSituacao().equals("Inativo"))
	{
	    status.setCodigo("SI");
	    status.setDescricao("Posto inativo");
	    status.setStatusOffline();
	}
	else
	{

	    ParameterVO parameterVO = getQLParameters();

	    Long toleranciaEntradaPre = parameterVO.getToleranciaEntradaPre();
	    Long toleranciaEntradaPos = parameterVO.getToleranciaEntradaPos();
	    Long toleranciaSaidaPre = parameterVO.getToleranciaSaidaPre();
	    Long toleranciaSaidaPos = parameterVO.getToleranciaSaidaPos();

	    GregorianCalendar dataInicialPre = null;
	    GregorianCalendar dataInicialPos = null;
	    GregorianCalendar dataFinalPre = null;
	    GregorianCalendar dataFinalPos = null;

	    boolean isTolerancia = false;
	    boolean isEscala = false;

	    int consistentes = 0;
	    int inconsistentes = 0;
	    List<ColaboradorVO> colaboradoresCobertos = new ArrayList<ColaboradorVO>();

	    if (posto.getEscalaPosto() != null && posto.getEscalaPosto().getDiaSemana() != null && posto.getEscalaPosto().getDiaSemana() != 0L && posto.getEscalaPosto().getHorariosHoje() != null)
	    {
		for (HorarioVO horario : posto.getEscalaPosto().getHorariosHoje())
		{
		    dataInicialPre = (GregorianCalendar) horario.getDataInicial().clone();
		    dataInicialPos = (GregorianCalendar) horario.getDataInicial().clone();
		    dataFinalPre = (GregorianCalendar) horario.getDataFinal().clone();
		    dataFinalPos = (GregorianCalendar) horario.getDataFinal().clone();

		    dataInicialPre.add(Calendar.MINUTE, toleranciaEntradaPre.intValue() * -1);
		    dataInicialPos.add(Calendar.MINUTE, toleranciaEntradaPos.intValue());
		    dataFinalPre.add(Calendar.MINUTE, toleranciaSaidaPre.intValue() * -1);
		    dataFinalPos.add(Calendar.MINUTE, toleranciaSaidaPos.intValue());

		    if ((dataInicialPre.compareTo(dataAtual) < 0 && dataAtual.compareTo(dataInicialPos) < 0) || (dataFinalPre.compareTo(dataAtual) < 0 && dataAtual.compareTo(dataFinalPos) < 0))
		    {
			isTolerancia = true;
		    }

		    if (dataInicialPos.compareTo(dataAtual) < 0 && dataAtual.compareTo(dataFinalPre) < 0)
		    {
			isEscala = true;
		    }
		}
	    }

	    if (posto.getEntradasAutorizadas() != null)
	    {
		for (EntradaAutorizadaVO entradaAutorizada : posto.getEntradasAutorizadas())
		{
		    if (entradaAutorizada != null)
		    {
			if (entradaAutorizada.getColaboradorSubstituido() != null)
			{
			    colaboradoresCobertos.add(entradaAutorizada.getColaboradorSubstituido());
			}
			if (entradaAutorizada.getColaborador() != null)
			{
			    if (entradaAutorizada.getColaborador().getPresenca() != null)
			    {
				if (entradaAutorizada.getColaborador().getPresenca().getStatus() != null)
				{
				    if (entradaAutorizada.getColaborador().getPresenca().getStatus().getCodigo().equals("EVP"))
				    {
					consistentes++;
				    }
				    if (entradaAutorizada.getColaborador().getPresenca().getStatus().getCodigo().equals("SNP") || entradaAutorizada.getColaborador().getPresenca().getStatus().getCodigo().equals("EIP") || entradaAutorizada.getColaborador().getPresenca().getStatus().getCodigo().equals("ENA") || entradaAutorizada.getColaborador().getPresenca().getStatus().getCodigo().equals("EIA"))
				    {
					inconsistentes++;
				    }
				}
				else
				{
				    log.warn("##### getStatusPosto ENTRADA AUTORIZADA PRESENCA - " + entradaAutorizada.getColaborador().getPresenca());
				}
			    }
			    else
			    {
				log.warn("##### getStatusPosto ENTRADA AUTORIZADA COLABORADOR - " + entradaAutorizada.getColaborador());
			    }
			}
			else
			{
			    log.warn("##### getStatusPosto ENTRADA AUTORIZADA" + entradaAutorizada);
			}
		    }
		    else
		    {
			log.warn("##### getStatusPosto ENTRADA AUTORIZADA");
		    }
		}
	    }

	    if (posto.getColaboradores() != null)
	    {
		for (ColaboradorVO colaborador : posto.getColaboradores())
		{
		    if (colaborador != null)
		    {
			if (colaborador.getPresenca() != null)
			{
			    if (colaborador.getPresenca().getStatus() != null)
			    {
				if (colaborador.getPresenca().getStatus().getCodigo().equals("EVP"))
				{
				    consistentes++;
				}
				if (colaborador.getPresenca().getStatus().getCodigo().equals("SNP") || colaborador.getPresenca().getStatus().getCodigo().equals("EIP") || colaborador.getPresenca().getStatus().getCodigo().equals("ENA") || colaborador.getPresenca().getStatus().getCodigo().equals("EIA"))
				{
				    inconsistentes++;
				}
			    }
			    else
			    {
				log.warn("##### COLABORADOR POSTO PRESENCA - " + colaborador.getPresenca());
			    }
			}
			else
			{
			    log.warn("##### COLABORADOR POSTO COLABORADOR - " + colaborador);
			}
		    }
		    else
		    {
			log.warn("##### COLABORADOR POSTO");
		    }
		}
	    }

	    if (isTolerancia)
	    {
		if (inconsistentes == 0)
		{
		    if (consistentes == 0)
		    {
			status.setCodigo("TVA");
			status.setDescricao("Posto consistente");
			status.setStatusNa();
		    }
		    else if (consistentes == 1)
		    {
			status.setCodigo("TVP");
			status.setDescricao("Posto consistente");
			status.setStatusOnline();
		    }
		    else
		    {
			status.setCodigo("TI");
			status.setDescricao("Posto inconsistente");
			status.setStatusOffline();
		    }
		}
		else
		{
		    status.setCodigo("TI");
		    status.setDescricao("Posto inconsistente");
		    status.setStatusOffline();
		}
	    }
	    else if (isEscala)
	    {
		if (consistentes >= 1 && inconsistentes == 0)
		{
		    status.setCodigo("EV");
		    status.setDescricao("Posto consistente");
		    status.setStatusOnline();
		}
		else
		{
		    status.setCodigo("EI");
		    status.setDescricao("Posto inconsistente");
		    status.setStatusOffline();
		}
	    }
	    else
	    {
		if (consistentes == 0 && inconsistentes == 0)
		{
		    status.setCodigo("SV");
		    status.setDescricao("Posto consistente");
		    status.setStatusNa();
		}
		else
		{
		    status.setCodigo("SI");
		    status.setDescricao("Posto inconsistente");
		    status.setStatusOffline();
		}
	    }
	}
	return status;
    }

    public static StatusVO getStatusPresenca(EscalaVO escala, PresencaVO presenca, GregorianCalendar dataAtual)
    {
	ParameterVO parameterVO = getQLParameters();

	Long toleranciaEntradaPre = parameterVO.getToleranciaEntradaPre();
	Long toleranciaEntradaPos = parameterVO.getToleranciaEntradaPos();
	Long toleranciaSaidaPre = parameterVO.getToleranciaSaidaPre();
	Long toleranciaSaidaPos = parameterVO.getToleranciaSaidaPos();

	GregorianCalendar dataInicial = null;
	GregorianCalendar dataFinal = null;

	Boolean horarioValidoColaborador = false;
	StatusVO status = new StatusVO();

	if (escala == null || escala.getHorarios() == null || escala.getHorarios().size() == 0)
	{
	    if (presenca.isPresente())
	    {
		status.setCodigo("SNP");
		status.setDescricao("Colaborador sem escala e presente");
		status.setStatusOffline();
	    }
	    else
	    {
		status.setCodigo("SNA");
		status.setDescricao("Colaborador sem escala e não presente");
		status.setStatusNa();
	    }
	}
	else
	{

	    if (presenca.isPresente())
	    {

		if (escala != null && escala.getHorarios() != null)
		{
		    for (HorarioVO horario : escala.getHorarios())
		    {

			dataInicial = (GregorianCalendar) horario.getDataInicial().clone();
			dataFinal = (GregorianCalendar) horario.getDataFinal().clone();
			dataInicial.add(Calendar.MINUTE, toleranciaEntradaPre.intValue() * -1);
			dataFinal.add(Calendar.MINUTE, toleranciaSaidaPos.intValue());

			if (dataInicial.compareTo(dataAtual) < 0 && dataAtual.compareTo(dataFinal) < 0)
			{
			    horarioValidoColaborador = true;
			}

		    }
		}
		if (horarioValidoColaborador)
		{
		    status.setCodigo("EVP");
		    status.setDescricao("Colaborador presente");
		    status.setStatusOnline();
		}
		else
		{
		    status.setCodigo("EIP");
		    status.setDescricao("Presença do colaborador em desacordo sua escala");
		    status.setStatusOffline();
		}

	    }
	    else
	    {

		if (escala != null && escala.getHorarios() != null)
		{
		    horarioValidoColaborador = true;

		    for (HorarioVO horario : escala.getHorarios())
		    {

			dataInicial = (GregorianCalendar) horario.getDataInicial().clone();
			dataFinal = (GregorianCalendar) horario.getDataFinal().clone();
			dataInicial.add(Calendar.MINUTE, toleranciaEntradaPos.intValue());
			dataFinal.add(Calendar.MINUTE, toleranciaSaidaPre.intValue() * -1);

			if (dataInicial.compareTo(dataAtual) < 0 && dataAtual.compareTo(dataFinal) < 0)
			{
			    horarioValidoColaborador = false;
			}
		    }

		    if (horarioValidoColaborador)
		    {
			status.setCodigo("EVA");
			status.setDescricao("Colaborador não presente");
			status.setStatusNa();
		    }
		    else
		    {
			status.setCodigo("EIA");
			status.setDescricao("Colaborador ausente");
			status.setStatusOffline();
		    }
		}
		else
		{
		    status.setCodigo("SNA");
		    status.setDescricao("Colaborador sem escala e não presente");
		    status.setStatusNa();
		}
	    }

	}

	return status;
    }

    public static Long getDiaPresenca(GregorianCalendar data)
    {

	Map<Integer, Long> diaSemana = new HashMap<Integer, Long>();
	diaSemana.put(GregorianCalendar.MONDAY, 1L);
	diaSemana.put(GregorianCalendar.TUESDAY, 2L);
	diaSemana.put(GregorianCalendar.WEDNESDAY, 3L);
	diaSemana.put(GregorianCalendar.THURSDAY, 4L);
	diaSemana.put(GregorianCalendar.FRIDAY, 5L);
	diaSemana.put(GregorianCalendar.SATURDAY, 6L);
	diaSemana.put(GregorianCalendar.SUNDAY, 7L);

	Long dia = diaSemana.get(data.get(GregorianCalendar.DAY_OF_WEEK));

	// Checar Feriados 
	if (!OrsegupsUtils.isHoliday(data))
	{
	    dia = 8L;
	}

	return dia;

    }

    public static String getTelefonePosto(String centroCusto)
    {

	String telefonePosto = null;

	try
	{

	    QLEqualsFilter centroCustoTelefoneFilter = new QLEqualsFilter("centroCustoTelefone.centroCusto", centroCusto);
	    QLEqualsFilter usoPresencaFilter = new QLEqualsFilter("usoPresenca", Boolean.TRUE);

	    QLGroupFilter telefoneGroupFilter = new QLGroupFilter("AND");
	    telefoneGroupFilter.addFilter(centroCustoTelefoneFilter);
	    telefoneGroupFilter.addFilter(usoPresencaFilter);

	    /*** Recupera Telefone(s) do Posto ***/
	    List<NeoObject> listaTelefone = PersistEngine.getObjects(AdapterUtils.getEntityClass("TELECOMCelulares"), telefoneGroupFilter);
	    if (NeoUtils.safeIsNotNull(listaTelefone) && listaTelefone.size() > 0)
	    {
		NeoObject noTelefone = (NeoObject) listaTelefone.get(0);
		EntityWrapper wrpTelefone = new EntityWrapper(noTelefone);
		telefonePosto = (String) (wrpTelefone.findValue("ddd") != null ? wrpTelefone.findValue("ddd") : "") + " " + (String) wrpTelefone.findValue("numero");
	    }

	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}

	return telefonePosto;
    }


    public static String getScriptContato(String centroCusto)
    {
	String telefoneContato = null;

	try
	{
	    QLEqualsFilter centroCustoFilter = new QLEqualsFilter("centroCustoTelefone.centroCusto", centroCusto);
	    QLEqualsFilter usoPresencaFilter = new QLEqualsFilter("usoPresenca", Boolean.TRUE);

	    QLGroupFilter contatoGroupFilter = new QLGroupFilter("AND");
	    contatoGroupFilter.addFilter(centroCustoFilter);
	    contatoGroupFilter.addFilter(usoPresencaFilter);
	    contatoGroupFilter.addFilter(new QLRawFilter("dddContato IS NOT NULL"));
	    contatoGroupFilter.addFilter(new QLRawFilter("numeroContato IS NOT NULL"));

	    /*** Recupera Contato(s) do Posto ***/
	    List<NeoObject> listaContato = PersistEngine.getObjects(AdapterUtils.getEntityClass("TELECOMCelulares"), contatoGroupFilter);

	    if(NeoUtils.safeIsNotNull(listaContato) && listaContato.size() > 0)
	    {
		NeoObject noContato = (NeoObject) listaContato.get(0);
		EntityWrapper wrpContato = new EntityWrapper(noContato);
		telefoneContato = (String) (wrpContato.findValue("dddContato") != null ? wrpContato.findValue("dddContato") : "") + " " + (String) wrpContato.findValue("numeroContato");

		if (telefoneContato.equals(" ")){
		    telefoneContato = ("<img src='imagens/icones_final/sphere_yellow_att_16x16-trans.png' alt='Sem Contato'/> Sem Contato");
		}else{

		    if (PortalUtil.getCurrentUser() != null)
		    {
			String ramal = null;
			EntityWrapper usuarioOrigemWrapper = new EntityWrapper(PortalUtil.getCurrentUser());
			ramal = (String) usuarioOrigemWrapper.findValue("ramal");
			return "<a href='javascript:dial(\"" + ramal + "\", \"" + telefoneContato.replaceAll("\\D+", "") + "\");'><img src='imagens/custom/phone.png' align='absMiddle' alt='Discar para o contato do posto'> " + telefoneContato + "</a>";
		    }
		}
	    }else
	    {
		telefoneContato = ("<img src='imagens/icones_final/sphere_yellow_att_16x16-trans.png' alt='Sem Contato'/> Sem Contato");
	    }
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	return telefoneContato;
    }


    public static String getListaTelefonePosto(String centroCusto)
    {

	StringBuilder telefonePosto = new StringBuilder();

	try
	{
	    QLEqualsFilter centroCustoTelefoneFilter = new QLEqualsFilter("centroCustoTelefone.centroCusto", centroCusto);
	    QLEqualsFilter usoPresencaFilter = new QLEqualsFilter("usoPresenca", Boolean.TRUE);

	    QLGroupFilter telefoneGroupFilter = new QLGroupFilter("AND");
	    telefoneGroupFilter.addFilter(centroCustoTelefoneFilter);
	    telefoneGroupFilter.addFilter(usoPresencaFilter);

	    /*** Recupera Telefone(s) do Posto ***/
	    List<NeoObject> listaTelefone = PersistEngine.getObjects(AdapterUtils.getEntityClass("TELECOMCelulares"), telefoneGroupFilter);
	    if (NeoUtils.safeIsNotNull(listaTelefone) && listaTelefone.size() > 0)
	    {
		String fone = "";
		for (NeoObject tel : listaTelefone)
		{
		    EntityWrapper wrpTelefone = new EntityWrapper(tel);
		    fone = (wrpTelefone.findValue("ddd") != null ? wrpTelefone.findValue("ddd") : "") + " " + (String) wrpTelefone.findValue("numero");

		    if (PortalUtil.getCurrentUser() != null)
		    {
			String ramal = null;
			EntityWrapper usuarioOrigemWrapper = new EntityWrapper(PortalUtil.getCurrentUser());
			ramal = (String) usuarioOrigemWrapper.findValue("ramal");

			telefonePosto.append("<a href='javascript:dial(\"" + ramal + "\", \"" + fone.replaceAll("\\D+", "") + "\");'><img src='imagens/custom/phone.png' align='absMiddle' alt='Discar para o telefone do posto'> " + fone + "</a>");
		    }
		}
	    }
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}

	return telefonePosto.toString();
    }

    public static String getScriptTelefone(String telefone)
    {

	if (telefone != null)
	{
	    if (PortalUtil.getCurrentUser() != null)
	    {
		String ramal = null;
		EntityWrapper usuarioOrigemWrapper = new EntityWrapper(PortalUtil.getCurrentUser());
		ramal = (String) usuarioOrigemWrapper.findValue("ramal");

		return "<a href='javascript:dial(\"" + ramal + "\", \"" + telefone.replaceAll("\\D+", "") + "\");'><img src='imagens/custom/phone.png' align='absMiddle' alt='Discar para o telefone do posto'> " + telefone + "</a>";
	    }
	    else
	    {
		return "<img src='imagens/icones_final/sphere_yellow_att_16x16-trans.png' alt='Não cadastrado'/> Não cadastrado";
	    }
	}
	else
	{
	    return "<img src='imagens/icones_final/sphere_yellow_att_16x16-trans.png' alt='Não cadastrado'/> Não cadastrado";
	}
    }

    public static String getLogEscalaPostoLink(EscalaPostoVO escalaPosto)
    {
	String contentString = "";

	if (escalaPosto != null && escalaPosto.getHorariosHoje() != null)
	{
	    String textoLog = getLogEscalaPosto(escalaPosto);
	    contentString = " <a  tabIndex=\"-1\"  onmouseover=\"return overlib('" + textoLog + "', CAPTION, '<b>Escala semanal:</b>', STICKY, CLOSETEXT, 'Fechar', TEXTSIZE, '10px', WIDTH, 350, CELLPAD, 10, 5, FOLLOWMOUSE, VAUTO);\" onmouseout=\"return nd();\">" + " <img src=\"imagens/icones_final/clock_16x16.png\"/></a> ";
	}

	return contentString;
    }

    public static List<NeoObject> getLogPosto(String codLoc)
    {
	ParameterVO parameterVO = getQLParameters();
	List<NeoObject> objPosto = PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORH_USU_Vorg203nv8"), new QLEqualsFilter("codloc", codLoc));
	List<NeoObject> logs = new ArrayList<NeoObject>();

	if (objPosto != null && objPosto.size() > 0)
	{
	    QLEqualsFilter postoFilter = new QLEqualsFilter("posto", objPosto.get(0));

	    GregorianCalendar datalimite = new GregorianCalendar();
	    datalimite.add(Calendar.DAY_OF_MONTH, -parameterVO.getTempoLogPosto().intValue());

	    QLOpFilter datefilter = new QLOpFilter("dataLog", ">=", (GregorianCalendar) datalimite);

	    QLGroupFilter groupFilterPosto = new QLGroupFilter("AND");
	    groupFilterPosto.addFilter(postoFilter);
	    groupFilterPosto.addFilter(datefilter);

	    Class clazz = AdapterUtils.getEntityClass("QLLogPresencaPosto");
	    logs = PersistEngine.getObjects(clazz, groupFilterPosto, -1, -1, "dataLog desc");
	}
	return logs;
    }

    public static List<NeoObject> getLogColaborador(String numcpf)
    {
	ParameterVO parameterVO = getQLParameters();

	QLEqualsFilter colaboradorFilter = new QLEqualsFilter("cpf", Long.parseLong(numcpf));

	GregorianCalendar datalimite = new GregorianCalendar();
	datalimite.add(Calendar.DAY_OF_MONTH, -parameterVO.getTempoLogColaborador().intValue());

	QLOpFilter datefilter = new QLOpFilter("dataLog", ">=", (GregorianCalendar) datalimite);

	QLGroupFilter groupFilterColaborador = new QLGroupFilter("AND");
	groupFilterColaborador.addFilter(colaboradorFilter);
	groupFilterColaborador.addFilter(datefilter);

	Class clazz = AdapterUtils.getEntityClass("QLLogPresencaColaborador");
	List<NeoObject> logs = PersistEngine.getObjects(clazz, groupFilterColaborador, -1, -1, "dataLog desc");

	return logs;
    }

    private static String getLogEscalaPosto(EscalaPostoVO escalaPosto)
    {
	Long diaSem = escalaPosto.getDiaSemana();
	String resultLog = "";
	String permanenciaPosto = "";

	if (escalaPosto.getPermanenciaPosto() != null)
	{
	    switch (escalaPosto.getPermanenciaPosto().intValue())
	    {
	    case 0:
		break;
	    case 10:
		permanenciaPosto = "Inicio";
		break;
	    case 20:
		permanenciaPosto = "Fim";
		break;
	    case 30:
		permanenciaPosto = "Intervalo";
		break;
	    case 40:
		permanenciaPosto = "Periodo Integral";
		break;
	    case 50:
		permanenciaPosto = "Inicio e Intervalo";
		break;
	    case 60:
		permanenciaPosto = "Inicio e Fim";
		break;
	    case 70:
		permanenciaPosto = "Intervalo e Fim";
		break;
	    }
	}

	if (!permanenciaPosto.equals(""))
	{
	    resultLog += "<b>Ajuste de Escala: " + permanenciaPosto + "</b></br>";
	}

	escalaPosto.setDiaSemana(1L);
	resultLog += "<li>Segunda-feira - " + escalaPosto + "</li>";
	escalaPosto.setDiaSemana(2L);
	resultLog += "<li>Terça-feira - " + escalaPosto + "</li>";
	escalaPosto.setDiaSemana(3L);
	resultLog += "<li>Quarta-feira - " + escalaPosto + "</li>";
	escalaPosto.setDiaSemana(4L);
	resultLog += "<li>Quinta-feira - " + escalaPosto + "</li>";
	escalaPosto.setDiaSemana(5L);
	resultLog += "<li>Sexta-feira - " + escalaPosto + "</li>";
	escalaPosto.setDiaSemana(6L);
	resultLog += "<li>Sabado - " + escalaPosto + "</li>";
	escalaPosto.setDiaSemana(7L);
	resultLog += "<li>Domingo - " + escalaPosto + "</li>";
	escalaPosto.setDiaSemana(8L);
	resultLog += "<li>Feriado - " + escalaPosto + "</li>";
	escalaPosto.setDiaSemana(diaSem);

	return resultLog.replaceAll("\"", "'");
    }

    public static String getLinkBtnDelete(EntradaAutorizadaVO entradaAutorizada)
    {
	String contentString = "";

	if (entradaAutorizada != null)
	{

	    SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
	    String dataInicial = sf.format(entradaAutorizada.getDataInicial().getTime());

	    String href = "";

	    if (entradaAutorizada.getColaboradorSubstituido() != null)
	    {
		href = "\"javascript:deleteRegistroCobertura('" + entradaAutorizada.getColaborador().getNumeroEmpresa() + "', '" + entradaAutorizada.getColaborador().getTipoColaborador() + "', '" + entradaAutorizada.getColaborador().getNumeroCadastro() + "', '" + dataInicial + "', '" + entradaAutorizada.getHoraInicial() + "', '" + entradaAutorizada.getColaboradorSubstituido().getNumeroEmpresa() + "', '" + entradaAutorizada.getColaboradorSubstituido().getTipoColaborador() + "', '"
			+ entradaAutorizada.getColaboradorSubstituido().getNumeroCadastro() + "');";
	    }
	    //Envia as informações do outro colaborador para excluir a troca de escala
	    else
	    {
		href = "\"javascript:deleteRegistroCobertura('" + entradaAutorizada.getColaborador().getNumeroEmpresa() + "', '" + entradaAutorizada.getColaborador().getTipoColaborador() + "', '" + entradaAutorizada.getColaborador().getNumeroCadastro() + "', '" + dataInicial + "', '" + entradaAutorizada.getHoraInicial() + "', '', '', '');";
	    }
	    contentString = "<a href=" + href + "\"> <img src=\"imagens/icones_final/delete_16x16-trans.png\" alt=\"Excluir Cobertura\"></a>";
	}

	return contentString;

    }

    @SuppressWarnings("unchecked")
    public static ParameterVO getQLParameters()
    {
	List<NeoObject> listaParametros = PersistEngine.getObjects(AdapterUtils.getEntityClass("QLParametros"));
	ParameterVO parameterVO = null;

	if (listaParametros != null)
	{
	    parameterVO = new ParameterVO();
	    EntityWrapper wrapperParametros = new EntityWrapper(listaParametros.get(0));

	    parameterVO.setToleranciaEntradaPos((Long) wrapperParametros.findValue("toleranciaEntradaPos"));
	    parameterVO.setToleranciaEntradaPre((Long) wrapperParametros.findValue("toleranciaEntradaPre"));
	    parameterVO.setToleranciaSaidaPos((Long) wrapperParametros.findValue("toleranciaSaidaPos"));
	    parameterVO.setToleranciaSaidaPre((Long) wrapperParametros.findValue("toleranciaSaidaPre"));
	    parameterVO.setTempoLogColaborador((Long) wrapperParametros.findValue("tempoLogColaborador"));
	    parameterVO.setTempoLogPosto((Long) wrapperParametros.findValue("tempoLogPosto"));
	    parameterVO.setPermissaoLogColaborador((Collection<SecurityEntity>) wrapperParametros.findValue("permissaoLogColaborador"));
	    parameterVO.setPermissaoLogPosto((Collection<SecurityEntity>) wrapperParametros.findValue("permissaoLogPosto"));
	}

	return parameterVO;
    }

    public static Boolean canCurrentUserCreateLog(String type)
    {
	Collection<SecurityEntity> permissionList = null;
	;

	if (type.equalsIgnoreCase("posto"))
	{
	    permissionList = getQLParameters().getPermissaoLogPosto();
	}
	else if (type.equalsIgnoreCase("colaborador"))
	{
	    permissionList = getQLParameters().getPermissaoLogColaborador();
	}

	NeoUser currentUser = PortalUtil.getCurrentUser();

	if (permissionList != null && !permissionList.isEmpty() && currentUser != null)
	{
	    for (SecurityEntity securityEntity : permissionList)
	    {
		if (currentUser.isInSecurityEntity(securityEntity))
		{
		    return true;
		}
	    }
	}

	return false;
    }

    public static GregorianCalendar convertHoraSenior(GregorianCalendar dataBase, Long horasSenior)
    {

	GregorianCalendar dataBaseRetorno = (GregorianCalendar) dataBase.clone();

	Integer hora = 0;
	Integer minuto = 0;

	if (horasSenior > 0)
	{
	    hora = horasSenior.intValue() / 60;
	    minuto = horasSenior.intValue() % 60;
	}

	dataBaseRetorno.set(GregorianCalendar.HOUR_OF_DAY, hora);
	dataBaseRetorno.set(GregorianCalendar.MINUTE, minuto);
	dataBaseRetorno.set(GregorianCalendar.SECOND, 0);

	return dataBaseRetorno;

    }

    public static List<EntradaAutorizadaVO> getListaCoberturas(String colaborador, String colaboradorSub, String numLoc, String tabOrg, String dataInicialDe, String dataInicialAte, String dataFinalDe, String dataFinalAte)
    {

	Connection connVetorh = PersistEngine.getConnection("VETORH");
	List<EntradaAutorizadaVO> entradasAutorizadas = new ArrayList<EntradaAutorizadaVO>();
	EntradaAutorizadaVO entradaVO = new EntradaAutorizadaVO();
	ColaboradorVO colaboradorVO = new ColaboradorVO();
	try
	{
	    GregorianCalendar dataIniDe = new GregorianCalendar();
	    GregorianCalendar dataIniAte = new GregorianCalendar();
	    GregorianCalendar dataFimDe = new GregorianCalendar();
	    GregorianCalendar dataFimAte = new GregorianCalendar();

	    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	    Date dInicioDe = df.parse(dataInicialDe);

	    dataIniDe.setTime(dInicioDe);

	    if (dataInicialAte != null && !dataInicialAte.isEmpty())
	    {
		Date dInicioAte = df.parse(dataInicialAte);
		dataIniAte.setTime(dInicioAte);
	    }

	    Date dFimDe = df.parse(dataFinalDe);
	    dataFimDe.setTime(dFimDe);

	    if (dataFinalAte != null && !dataFinalAte.isEmpty())
	    {
		Date dFimAte = df.parse(dataFinalAte);
		dataFimAte.setTime(dFimAte);
	    }

	    /*** Recupera Operacoes de Cobertura por Colaborador ***/
	    StringBuffer queryConsultaCoberturas = new StringBuffer();
	    queryConsultaCoberturas.append(" SELECT cob.usu_numemp, cob.usu_tipcol, cob.usu_numcad, cob.usu_datalt, cob.usu_datfim, cob.USU_NumCadCob, fun.nomfun, ");
	    queryConsultaCoberturas.append(" car.titred, USU_NumEmpCob, esc.NomEsc, fun2.NomFun AS NomFun2, fun2.CodTma, fun2.CodEsc, cob.USU_HorIni, mo.USU_DesPre, cob.USU_TabOrg, cob.USU_NumLoc ");
	    queryConsultaCoberturas.append(" FROM USU_T038COBFUN cob WITH (NOLOCK) ");
	    queryConsultaCoberturas.append(" INNER JOIN R034FUN fun WITH (NOLOCK) ON fun.NumEmp = cob.USU_NumEmp AND fun.TipCol = cob.USU_TipCol AND fun.NumCad = cob.USU_NumCad  ");
	    queryConsultaCoberturas.append(" INNER JOIN R024CAR car WITH (NOLOCK) ON car.EstCar = fun.EstCar AND car.CodCar = fun.CodCar ");
	    queryConsultaCoberturas.append(" LEFT JOIN R034FUN fun2 WITH (NOLOCK) ON fun2.NumEmp = cob.USU_NumEmpCob AND fun2.TipCol = cob.USU_TipColCob AND fun2.NumCad = cob.USU_NumCadCob ");
	    queryConsultaCoberturas.append(" LEFT JOIN R006ESC esc WITH (NOLOCK) ON esc.CodEsc = fun2.CodEsc ");
	    queryConsultaCoberturas.append(" INNER JOIN USU_T010MoCo mo WITH (NOLOCK) ON mo.USU_CodMot = cob.USU_codMot WHERE ");

	    if (dataInicialDe != null && !dataInicialDe.isEmpty())
	    {
		queryConsultaCoberturas.append(" cob.USU_DatAlt >= ? ");
	    }
	    if (dataInicialAte != null && !dataInicialAte.isEmpty())
	    {
		queryConsultaCoberturas.append(" AND cob.USU_DatAlt <= ? ");
	    }
	    if (colaborador != null && !colaborador.isEmpty())
	    {
		queryConsultaCoberturas.append("  AND cob.usu_numcad = ? ");
	    }
	    if (colaboradorSub != null && !colaboradorSub.isEmpty())
	    {
		queryConsultaCoberturas.append("  AND cob.usu_numcadcob = ? ");
	    }
	    if (numLoc != null && tabOrg != null && !numLoc.isEmpty() && !tabOrg.isEmpty())
	    {
		queryConsultaCoberturas.append("  AND cob.usu_taborg = ? AND cob.usu_numloc = ?");
	    }

	    PreparedStatement stConsultaCoberturas = connVetorh.prepareStatement(queryConsultaCoberturas.toString());

	    int index = 0;
	    stConsultaCoberturas.setTimestamp(1, new Timestamp(dataIniDe.getTimeInMillis()));
	    stConsultaCoberturas.setTimestamp(2, new Timestamp(dataIniAte.getTimeInMillis()));
	    index += 2;

	    if (colaborador != null && !colaborador.isEmpty())
	    {
		index++;
		stConsultaCoberturas.setLong(index, new Long(colaborador));
	    }
	    if (colaboradorSub != null && !colaboradorSub.isEmpty())
	    {
		index++;
		stConsultaCoberturas.setLong(index, new Long(colaborador));
	    }
	    if (numLoc != null && tabOrg != null && !numLoc.isEmpty() && !tabOrg.isEmpty())
	    {
		index++;
		stConsultaCoberturas.setLong(index, new Long(tabOrg));
		stConsultaCoberturas.setLong(index + 1, new Long(numLoc));
	    }

	    ResultSet rsCoberturas = stConsultaCoberturas.executeQuery();
	    while (rsCoberturas.next())
	    {
		entradaVO = new EntradaAutorizadaVO();
		EscalaVO escala = new EscalaVO();
		colaboradorVO.setNumeroEmpresa(rsCoberturas.getLong("usu_numemp"));
		colaboradorVO.setTipoColaborador(rsCoberturas.getLong("usu_tipcol"));
		colaboradorVO.setNumeroCadastro(rsCoberturas.getLong("usu_numcad"));
		colaboradorVO.setNomeColaborador(rsCoberturas.getString("nomfun"));
		colaboradorVO.setCargo(rsCoberturas.getString("titred"));
		entradaVO.setDescricaoCobertura(rsCoberturas.getString("usu_despre"));
		GregorianCalendar dataInicio = new GregorianCalendar();
		dataInicio.setTime(rsCoberturas.getTimestamp("usu_datalt"));
		entradaVO.setDataInicial(dataInicio);
		GregorianCalendar dataFim = new GregorianCalendar();
		dataFim.setTime(rsCoberturas.getTimestamp("usu_datfim"));
		entradaVO.setDataFinal(dataFim);
		entradaVO.setHoraInicial(rsCoberturas.getLong("usu_horini"));
		entradaVO.setColaborador(colaboradorVO);

		escala.setDescricao(rsCoberturas.getString("nomesc") + " / " + rsCoberturas.getString("codtma"));
		// Adicionando na lista geral de entradas autorizadas
		entradasAutorizadas.add(entradaVO);

	    }
	    rsCoberturas.close();
	    stConsultaCoberturas.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}
	return entradasAutorizadas;

    }

    public boolean isValidDate(String inDate)
    {

	boolean isValid = true;
	if (inDate == null)
	{
	    isValid = false;
	}
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	if (inDate.trim().length() != dateFormat.toPattern().length())
	{
	    isValid = false;
	}
	dateFormat.setLenient(false);
	try
	{
	    dateFormat.parse(inDate.trim());
	}
	catch (ParseException pe)
	{
	    isValid = false;
	}
	return isValid;

    }

    public boolean isValidHour(String inDate)
    {

	boolean isValid = true;
	if (inDate == null)
	{
	    isValid = false;
	}
	SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
	if (inDate.trim().length() != dateFormat.toPattern().length())
	{
	    isValid = false;
	}
	dateFormat.setLenient(false);
	try
	{
	    dateFormat.parse(inDate.trim());
	}
	catch (ParseException pe)
	{
	    isValid = false;
	}
	return isValid;

    }

    /**
     * Validar se possui escala no posto e se os horarios estao de acordo com a tolerancia
     * 
     * @param numloc
     * @param taborg
     * @param dataAtual
     * @return
     */
    public static boolean validaHorarioPosto(EscalaPostoVO escalaPosto, GregorianCalendar dataAtual, String direcaoAcesso)
    {
	boolean horarioValido = false;

	if (escalaPosto != null && escalaPosto.getHorariosHoje() != null)
	{
	    for (HorarioVO horario : escalaPosto.getHorariosHoje())
	    {
		ParameterVO parameterVO = getQLParameters();

		Long toleranciaEntrada = parameterVO.getToleranciaEntradaPre();
		Long toleranciaSaida = parameterVO.getToleranciaEntradaPre();

		GregorianCalendar dataEntrada = (GregorianCalendar) horario.getDataInicial().clone();
		GregorianCalendar dataSaida = (GregorianCalendar) horario.getDataFinal().clone();

		dataEntrada.add(Calendar.MINUTE, toleranciaEntrada.intValue() * -1);
		dataEntrada.set(Calendar.SECOND, 0);

		/*
		 * caso saida entre 00:00 e 00:15 adiciona um dia
		 * if (direcaoAcesso.equals("S") && dataAtual.get(Calendar.HOUR_OF_DAY) == 0 &&
		 * dataAtual.get(Calendar.MINUTE) <= 15)
		 * {
		 * dataAtual.add(Calendar.DATE, +1);
		 * }
		 */

		//Caso o minuto 59 acrecenta mais um minuto da tolerancia
		if (dataSaida.get(Calendar.MINUTE) == 59)
		{
		    dataSaida.add(Calendar.MINUTE, toleranciaSaida.intValue() + 1);
		}
		else
		{
		    dataSaida.add(Calendar.MINUTE, toleranciaSaida.intValue());
		}
		dataSaida.set(Calendar.SECOND, 59);

		if (dataAtual.compareTo(dataEntrada) >= 0 && dataAtual.compareTo(dataSaida) <= 0)
		{
		    horarioValido = true;
		    break;
		}
	    }
	}
	//System.out.println("Valida Horario Posto " + horarioValido) ;
	return horarioValido;
    }

    public static EscalaPostoVO getJornadaPosto(String centroCusto, Long numeroPosto, String situacaoPosto, GregorianCalendar dataAtual)
    {

	Connection connVetorh = PersistEngine.getConnection("SAPIENS");
	//		EntityManager entityManager = PersistEngine.getEntityManager("SAPIENS");
	EscalaPostoVO escalaPosto = new EscalaPostoVO();

	try
	{

	    /*** Recupera Escala Posto ***/
	    StringBuffer queryEscala = new StringBuffer();
	    queryEscala.append(" SELECT top 1 usu_codemp, usu_codfil, usu_numctr, usu_numpos, usu_codser,usu_peresc  ");
	    queryEscala.append(" FROM USU_T160CVS cvs WITH (NOLOCK) ");
	    queryEscala.append(" INNER JOIN E080SER ser WITH (NOLOCK) ON ser.CodEmp = cvs.USU_CodEmp AND ser.CodSer = cvs.USU_CodSer ");
	    queryEscala.append(" WHERE (cvs.USU_SitCvs = 'A' OR (cvs.USU_SitCvs = 'I' AND ? BETWEEN cvs.USU_DatIni AND DATEADD(MINUTE, 1439, cvs.USU_DatFim))) ");
	    queryEscala.append(" AND cvs.USU_CodCcu = ? ");
	    queryEscala.append(" AND cvs.USU_NumPos = ? ");
	    queryEscala.append(" AND ser.CodFam <> 'SER102' ");

	    PreparedStatement stEscala = connVetorh.prepareStatement(queryEscala.toString());
	    stEscala.setTimestamp(1, new Timestamp(dataAtual.getTimeInMillis()));
	    stEscala.setString(2, centroCusto);
	    stEscala.setLong(3, numeroPosto);
	    ResultSet rsEscala = stEscala.executeQuery();

	    Long codEmp = 0L;
	    Long codFil = 0L;
	    Long numCtr = 0L;
	    Long numPos = 0L;
	    String codSer = "";
	    Long peresc = 0L;

	    if (rsEscala.next() && situacaoPosto.equals("Ativo"))
	    {
		codEmp = rsEscala.getLong("usu_codemp");
		codFil = rsEscala.getLong("usu_codfil");
		numCtr = rsEscala.getLong("usu_numctr");
		numPos = rsEscala.getLong("usu_numpos");
		codSer = rsEscala.getString("usu_codser");
		peresc = rsEscala.getLong("usu_peresc");

	    }
	    rsEscala.close();
	    stEscala.close();

	    if (codEmp != null && codFil != null && numCtr != null && numPos != null && codSer != null && codEmp != 0 && codFil != 0 && numCtr != 0 && numPos != 0 && !codSer.equals(""))
	    {
		escalaPosto = getJornadaEscalaPosto(codEmp, codFil, numCtr, numPos, codSer, dataAtual, peresc, false);
	    }
	    else
	    {
		escalaPosto = null;
	    }
	}
	catch (NoResultException noResultException)
	{

	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}

	return escalaPosto;
    }

    public static EscalaPostoVO getEscalaPosto(Long numloc, Long taborg, GregorianCalendar dataAtual)
    {

	Connection connVetorh = PersistEngine.getConnection("VETORH");
	EscalaPostoVO escalaPosto = new EscalaPostoVO();

	try
	{

	    /*** Recupera Escala Posto ***/
	    StringBuffer queryEscala = new StringBuffer();
	    queryEscala.append(" SELECT top 1 orn.usu_numemp, orn.usu_numctr, orn.usu_numpos, orn.usu_peresc ");
	    queryEscala.append(" FROM r016orn orn ");
	    queryEscala.append(" WHERE orn.numloc = ? ");
	    queryEscala.append(" AND orn.taborg = ? ");

	    PreparedStatement stEscala = connVetorh.prepareStatement(queryEscala.toString());
	    stEscala.setLong(1, numloc);
	    stEscala.setLong(2, taborg);
	    ResultSet rsEscala = stEscala.executeQuery();

	    Long codEmp = 0L;
	    Long numCtr = 0L;
	    Long numPos = 0L;
	    Long peresc = 0L;

	    //Nao possuem na R016ORN
	    Long codFil = null;
	    String codSer = null;

	    if (rsEscala.next())
	    {
		codEmp = rsEscala.getLong("usu_numemp");
		numCtr = rsEscala.getLong("usu_numctr");
		numPos = rsEscala.getLong("usu_numpos");
		peresc = rsEscala.getLong("usu_peresc");
	    }

	    if (peresc == null || peresc == 0L)
	    {
		peresc = 80L;
	    }
	    rsEscala.close();
	    stEscala.close();

	    if (codEmp != null && numCtr != null && numPos != null && codEmp != 0 && numCtr != 0 && numPos != 0)
	    {
		escalaPosto = getJornadaEscalaPosto(codEmp, codFil, numCtr, numPos, codSer, dataAtual, peresc, false);
	    }
	    else
	    {
		escalaPosto = null;
	    }
	}
	catch (NoResultException noResultException)
	{

	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}

	return escalaPosto;
    }

    /**
     * Responsavel por validar os horarios de acesso no presenca
     * 
     * @param escala
     * @param direcaoAcesso
     * @param dataAtual
     * @param tipoDirecaoAcesso
     * @param validaDuplicidade
     * @return 1 - OK
     *         3 - Acesso duplicado
     *         4 - Acesso nao permitido
     */
    public static Integer validaToleranciaEscala(ColaboradorVO colaborador, EscalaPostoVO escalaPosto, Long ajusteEscala, String direcaoAcesso, GregorianCalendar dataAtual, String tipoDirecaoAcesso, boolean validaDuplicidade)
    {
	Integer retorno = 4;
	
	dataAtual.set(Calendar.SECOND, 0);
	try
	{
	    ajustarEscala(ajusteEscala, colaborador, escalaPosto);
	    Long tipoHorario = buscarTipoHorarioColaborador(colaborador);
	    //System.out.println(NeoUtils.safeDateFormat(dataAtual));
	    ListIterator<HorarioVO> listaHorIterator = colaborador.getEscala().getHorarios().listIterator();
	    for (HorarioVO hor : colaborador.getEscala().getHorarios())
	    {
		Boolean validarToleranciaPadrao = false;
		if (tipoHorario != null && tipoHorario.equals(5L)){
		    Collection<ToleranciaHorarioVO> horariosCol = buscarToleranciaEscalaColaborador(colaborador);
		    if (horariosCol != null){
			for (ToleranciaHorarioVO toleranciaHorarioVO : horariosCol) {
			    GregorianCalendar gc = new GregorianCalendar();
			    gc.set(Calendar.HOUR_OF_DAY, 0);
			    gc.set(Calendar.MINUTE, 0);
			    gc.set(Calendar.SECOND, 0);
			    gc.set(Calendar.MILLISECOND, 0);
			    gc.add(Calendar.MINUTE, toleranciaHorarioVO.getFaiMov().intValue());
			    if (toleranciaHorarioVO.getSeqMar().equals(2l)){
				if (dataAtual.compareTo(gc) < 0){
				    validarToleranciaPadrao = true;
				    break;
				}
			    }else if (toleranciaHorarioVO.getSeqMar().equals(3l)){
				if (dataAtual.compareTo(gc) > 0){
				    validarToleranciaPadrao = false;
				    break;
				}
			    }
			}
		    }
		}else{
		    validarToleranciaPadrao = true;
		}    
		 
		if (validarToleranciaPadrao){
		    listaHorIterator.next();
		    if (hor != null)
		    {
			ParameterVO parameterVO = getQLParameters();

			if (direcaoAcesso.endsWith("E"))
			{
			    if (hor.getDataInicial() != null)
			    {
				Long toleranciaEntradaPre = parameterVO.getToleranciaEntradaPre();
				Long toleranciaEntradaPos = parameterVO.getToleranciaEntradaPre();

				//Caso Chegada ao local, seta o limite da entrada em 2 horas
				if (tipoDirecaoAcesso.equals("L"))
				{
				    toleranciaEntradaPos = 120L;
				}

				GregorianCalendar dataEntradaPre = (GregorianCalendar) hor.getDataInicial().clone();
				GregorianCalendar dataEntradaPos = (GregorianCalendar) hor.getDataInicial().clone();

				dataEntradaPre.add(Calendar.MINUTE, toleranciaEntradaPre.intValue() * -1);
				dataEntradaPos.add(Calendar.MINUTE, toleranciaEntradaPos.intValue());

				dataEntradaPre.set(Calendar.SECOND, 0);
				dataEntradaPos.set(Calendar.SECOND, 0);

				//System.out.println(NeoUtils.safeDateFormat(dataEntradaPre));
				//System.out.println(NeoUtils.safeDateFormat(dataEntradaPos));

				if (dataAtual.get(Calendar.HOUR_OF_DAY) == 23 && dataAtual.get(Calendar.MINUTE) >= 45)
				{
				    dataEntradaPre.add(Calendar.DATE, +1);
				    dataEntradaPos.add(Calendar.DATE, +1);
				}

				if (dataAtual.compareTo(dataEntradaPre) >= 0 && dataAtual.compareTo(dataEntradaPos) <= 0)
				{
				    if (validaDuplicidade && isDuplicidadeAcesso(colaborador.getNumeroCracha(), dataEntradaPre, dataEntradaPos, direcaoAcesso, tipoDirecaoAcesso))
				    {
					retorno = 3;
					break;
				    }
				    else
				    {
					retorno = 1;
					break;
				    }
				}
			    }
			}
			else
			{
			    if (hor.getDataFinal() != null)
			    {
				Long toleranciaSaidaPre = parameterVO.getToleranciaEntradaPre();
				Long toleranciaSaidaPos = parameterVO.getToleranciaEntradaPre();

				GregorianCalendar dataSaidaPre = (GregorianCalendar) hor.getDataFinal().clone();
				GregorianCalendar dataSaidaPos = (GregorianCalendar) hor.getDataFinal().clone();
				GregorianCalendar dataFimEsc = (GregorianCalendar) hor.getDataFinal().clone();

				//caso saida 00:00 adiciona um dia
				if (dataSaidaPre.get(Calendar.HOUR_OF_DAY) == 00 && dataSaidaPre.get(Calendar.MINUTE) == 00 && dataAtual.get(Calendar.HOUR_OF_DAY) == 23 && dataAtual.get(Calendar.MINUTE) >= 45)
				{
				    dataSaidaPre.add(Calendar.DATE, +1);
				    dataSaidaPos.add(Calendar.DATE, +1);
				}

				dataSaidaPre.add(Calendar.MINUTE, toleranciaSaidaPre.intValue() * -1);
				dataSaidaPos.add(Calendar.MINUTE, toleranciaSaidaPos.intValue());

				dataSaidaPre.set(Calendar.SECOND, 0);
				dataSaidaPos.set(Calendar.SECOND, 0);

				//System.out.println(NeoUtils.safeDateFormat(dataSaidaPre));
				//System.out.println(NeoUtils.safeDateFormat(dataSaidaPos));

				if(dataFimEsc.get(Calendar.HOUR_OF_DAY) == 23 && dataFimEsc.get(Calendar.MINUTE) == 59 && !listaHorIterator.hasNext()){
				    GregorianCalendar dataAtuClone = (GregorianCalendar) dataAtual.clone();
				    if(dataAtuClone.get(Calendar.HOUR_OF_DAY) == 00){
					if(dataAtual.get(Calendar.MINUTE) < 15)
					{
					    dataSaidaPre.add(Calendar.DATE, -1);
					    dataSaidaPos.add(Calendar.DATE, -1);
					}
				    }
				    if (dataAtuClone.compareTo(dataSaidaPre) >= 0 && dataAtuClone.compareTo(dataSaidaPos) <= 0){
					retorno = 1;
					break;									
				    }
				}

				if (dataAtual.compareTo(dataSaidaPre) >= 0 && dataAtual.compareTo(dataSaidaPos) <= 0)
				{
				    //OK
				    if (validaDuplicidade && isDuplicidadeAcesso(colaborador.getNumeroCracha(), dataSaidaPre, dataSaidaPos, direcaoAcesso, tipoDirecaoAcesso))
				    {
					retorno = 3;
					break;
				    }
				    else
				    {
					retorno = 1;
					break;
				    }
				}
			    }
			}
		    }
		}else{
		    retorno = 1;
		    break;
		}
	    }
	}
	catch (Exception e)
	{
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return retorno;
    }
    
    public static Collection<ToleranciaHorarioVO> buscarToleranciaEscalaColaborador(ColaboradorVO colaborador){
	Collection<ToleranciaHorarioVO> retorno = new ArrayList<ToleranciaHorarioVO>();
	
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();
	Long codHor = buscarHorarioHojeColaborador(colaborador);
	if (codHor != null){
	    try {
		conn = PersistEngine.getConnection("VETORH");
		
		sql.append(" select hor.TipHor, esc.CodEsc, mhr.CodHor, mhr.SeqMar, mhr.UsoBat, mhr.HorBat, mhr.TolAnt, mhr.TolApo, mhr.FaiMov from R034FUN fun ");
		sql.append(" inner join R006HOR esc on esc.CodEsc = fun.codesc ");
		sql.append(" inner join R004HOR hor on hor.CodHor = esc.CodHor ");
		sql.append(" inner join R004MHR mhr on mhr.CodHor = hor.CodHor ");
		sql.append(" where fun.numcpf = ? and hor.TipHor = 5 and hor.CodHor = ? ");
		sql.append(" group by hor.TipHor, esc.CodEsc, mhr.CodHor, mhr.SeqMar, mhr.UsoBat, mhr.HorBat, mhr.TolAnt, mhr.TolApo, mhr.FaiMov ");
		
		pstm = conn.prepareStatement(sql.toString());
		
		pstm.setLong(1, colaborador.getCpf());
		pstm.setLong(2, codHor);
		
		rs = pstm.executeQuery();
		
		while (rs.next()) {
		    ToleranciaHorarioVO thVO = new ToleranciaHorarioVO();
		    thVO.setFaiMov(rs.getLong("FaiMov"));
		    thVO.setHorBat(rs.getLong("HorBat"));
		    thVO.setTolAnt(rs.getLong("TolAnt"));
		    thVO.setTolApo(rs.getLong("TolApo"));
		    thVO.setSeqMar(rs.getLong("SeqMar"));
		    retorno.add(thVO);
		}
		
	    } catch (SQLException e) {
		e.printStackTrace();
	    } finally {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    }
	}
	
	return retorno;
    }
    
    public static Long buscarHorarioHojeColaborador(ColaboradorVO colaborador){
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();
	Long codHor = null;
	try {
	    conn = PersistEngine.getConnection("VETORH");

	    sql.append(" SELECT esc.CodEsc, hr2.DesHor, tma.CodTma, hr1.CodHor "); 
	    sql.append(" FROM R006ESC esc   ");
	    sql.append(" INNER JOIN R006TMA tma ON tma.CodEsc = esc.CodEsc "); 
	    sql.append(" INNER JOIN R006HOR hr1 ON hr1.CodEsc = esc.CodEsc  ");
	    sql.append(" INNER JOIN R004HOR hr2 ON hr2.CodHor = hr1.CodHor  ");
	    sql.append(" WHERE DATEDIFF(dd,tma.DatBas,?) % (SELECT COUNT(*) FROM R006HOR hr3 WHERE hr3.CodEsc = esc.CodEsc) + 1 = hr1.SeqReg "); 
	    sql.append(" AND hr1.CodEsc = ?  ");
	    sql.append(" and tma.CodTma = (select top 1 hes.CodTma from R038HES hes where hes.NumCad = ? and hes.NumEmp = ? order by DatAlt desc) ");
	    sql.append(" GROUP BY esc.CodEsc, hr2.DesHor, tma.CodTma, hr1.CodHor");
	    
	    pstm = conn.prepareStatement(sql.toString());
	    GregorianCalendar gData = new GregorianCalendar();
	    GregorianCalendar dataI = new GregorianCalendar(gData.get(GregorianCalendar.YEAR), gData.get(GregorianCalendar.MONTH), gData.get(GregorianCalendar.DAY_OF_MONTH));
	    pstm.setTimestamp(1, new Timestamp(dataI.getTimeInMillis()));
	    pstm.setLong(2, colaborador.getEscala().getCodigoEscala());
	    pstm.setLong(3, colaborador.getNumeroCadastro());
	    pstm.setLong(4, colaborador.getNumeroEmpresa());
	    
	    rs = pstm.executeQuery();	    

	    while (rs.next()) {
		codHor = rs.getLong("CodHor");
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
	return codHor;
    }

    public static boolean isDuplicidadeAcesso(String numcra, GregorianCalendar dataPre, GregorianCalendar dataPos, String direcaoAcesso, String tipoDirecaoAcesso)
    {
	Boolean isDuplicidade = false;

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    StringBuffer sqlAcesso = new StringBuffer();
	    sqlAcesso.append(" SELECT acc.numcra FROM R070ACC acc ");
	    sqlAcesso.append(" WHERE acc.NumCra = ? ");
	    sqlAcesso.append(" AND DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) >= ? ");
	    sqlAcesso.append(" AND DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) <= ? ");
	    sqlAcesso.append(" AND acc.diracc = ? AND acc.USU_TipDir = ? AND acc.TipAcc = 100 AND acc.codplt = 2 ");

	    PreparedStatement stAcesso = connVetorh.prepareStatement(sqlAcesso.toString());
	    stAcesso.setString(1, numcra);
	    stAcesso.setString(2, NeoUtils.safeDateFormat(dataPre, "yyyy-MM-dd HH:mm:ss"));
	    stAcesso.setString(3, NeoUtils.safeDateFormat(dataPos, "yyyy-MM-dd HH:mm:ss"));
	    stAcesso.setString(4, direcaoAcesso);
	    stAcesso.setString(5, tipoDirecaoAcesso);

	    ResultSet rsAcesso = stAcesso.executeQuery();

	    if (rsAcesso.next())
	    {
		isDuplicidade = true;
	    }
	    rsAcesso.close();
	    stAcesso.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}

	return isDuplicidade;
    }
    
    

    /**
     * Verifica se existe alguma entrada padrao para o colaborador nas ultimas 12 horas
     * 
     * @param numcra
     * @param dataAtual
     * @return
     */
    public static String verificaUltimaEntradaPadrao(String numcra, GregorianCalendar dataAtual)
    {
	String tipoDirecaoAcesso = "A";

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    StringBuffer sqlEntrada = new StringBuffer();
	    sqlEntrada.append(" SELECT TOP 1 acc.DirAcc, acc.USU_TipDir FROM R070ACC acc ");
	    sqlEntrada.append(" WHERE acc.NumCra = ?  ");
	    sqlEntrada.append(" AND DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) > DATEADD(hh,-12, ?) ");
	    sqlEntrada.append(" AND acc.TipAcc = 100 AND acc.USU_TipDir in ('P','L') ");
	    sqlEntrada.append(" ORDER BY DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) desc ");

	    PreparedStatement stEntrada = connVetorh.prepareStatement(sqlEntrada.toString());
	    stEntrada.setString(1, numcra);
	    stEntrada.setString(2, NeoUtils.safeDateFormat(dataAtual, "yyyy-MM-dd HH:mm:ss"));
	    ResultSet rsEntrada = stEntrada.executeQuery();

	    //Entrada Autorizada
	    if (rsEntrada.next())
	    {
		String diracc = rsEntrada.getString("DirAcc");

		if (diracc.equals("E"))
		{
		    tipoDirecaoAcesso = "L";
		}
	    }

	    rsEntrada.close();
	    stEntrada.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}
	return tipoDirecaoAcesso;
    }

    /**
     * Verifica se existe alguma entrada Autorizada e informar o tipo direcao
     * 
     * @param numcra
     * @param numloc
     * @param taborg
     * @param dataAtual
     * @return (P / A / L)
     */
    public static String verificaUltimaEntradaAutorizada(String numcra, Long numloc, Long taborg, GregorianCalendar dataAtual)
    {
	String tipoDirecaoAcesso = "A";

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    StringBuffer sqlEntrada = new StringBuffer();
	    sqlEntrada.append(" SELECT TOP 1 USU_TipDir FROM R070ACC acc ");
	    sqlEntrada.append(" WHERE acc.NumCra = ? AND USU_NumLoc = ? AND USU_TabOrg = ? AND acc.TipAcc = 100 AND acc.CodPlt = 2 AND acc.DirAcc = 'E' AND acc.USU_TipDir in ('A','L') ");
	    sqlEntrada.append(" AND DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) = (SELECT MAX(DATEADD(MINUTE, acc2.HorAcc, acc2.DatAcc)) ");
	    sqlEntrada.append(" 	FROM R070ACC acc2 WHERE acc2.NumCra = acc.NumCra AND acc2.usu_TabOrg = acc.usu_TabOrg AND acc2.usu_NumLoc = acc.usu_NumLoc ");
	    sqlEntrada.append(" 	AND acc2.TipAcc = 100 AND acc2.CodPlt = 2 AND acc2.DirAcc = 'E' AND acc2.usu_tipdir = acc.usu_tipdir AND DATEADD(MINUTE, acc2.HorAcc, acc2.DatAcc) <= ?) ");
	    sqlEntrada.append(" ORDER BY DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) desc ");

	    PreparedStatement stEntrada = connVetorh.prepareStatement(sqlEntrada.toString());
	    stEntrada.setString(1, numcra);
	    stEntrada.setLong(2, numloc);
	    stEntrada.setLong(3, taborg);
	    stEntrada.setTimestamp(4, new Timestamp(dataAtual.getTimeInMillis()));
	    ResultSet rsEntrada = stEntrada.executeQuery();

	    //Entrada Autorizada
	    if (rsEntrada.next())
	    {
		tipoDirecaoAcesso = rsEntrada.getString("USU_TipDir");
	    }

	    rsEntrada.close();
	    stEntrada.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}
	return tipoDirecaoAcesso;
    }

    public static void insereRegistroAcesso(ColaboradorVO colaborador, String direcaoAcesso, String tipoDirecaoAcesso, GregorianCalendar dataAtual, Long tipoAcesso, Long permanenciaPosto, String fone) throws Exception
    {
	/*
	 * 1. Insere registro de Acesso no Ronda
	 */
	//if (true) return;
	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    Long usoMarcacao = 2L;

	    if (tipoDirecaoAcesso.equals("L"))
	    {
		usoMarcacao = 1L;
	    }

	    StringBuffer sqlRegistro = new StringBuffer();

	    sqlRegistro.append(" INSERT INTO R070ACC (NumCra, DatAcc, HorAcc, SeqAcc, TipAcc, CodPlt, DirAcc, OriAcc, UsoMar, CodRlg, FlaAcc, CodBnf, StaRlg, ExcPon, CodDsp, USU_TabOrg, USU_NumLoc, USU_TipDir, USU_PerEsc, USU_FonAcc) ");
	    sqlRegistro.append(" SELECT ?, DATEADD(dd, 0, DATEDIFF(dd, 0, ?)), (DATEPART(HOUR, ?) * 60) + DATEPART(MINUTE, ?), ISNULL(MAX(SeqAcc)+1, 1), ?, 2, ?, 'E', ?, 1, 0, 0, 1, 'N', 2, ?, ?, ?, ?, ? ");
	    sqlRegistro.append(" FROM R070ACC WHERE NumCra = ? AND  DatAcc = DATEADD(dd, 0, DATEDIFF(dd, 0, ?)) AND HorAcc = (DATEPART(HOUR, ?) * 60) + DATEPART(MINUTE, ?) ");

	    PreparedStatement stRegistro = connVetorh.prepareStatement(sqlRegistro.toString());

	    stRegistro.setString(1, colaborador.getNumeroCracha());
	    stRegistro.setTimestamp(2, new Timestamp(dataAtual.getTimeInMillis()), dataAtual);
	    stRegistro.setTimestamp(3, new Timestamp(dataAtual.getTimeInMillis()), dataAtual);
	    stRegistro.setTimestamp(4, new Timestamp(dataAtual.getTimeInMillis()), dataAtual);
	    stRegistro.setLong(5, tipoAcesso);
	    stRegistro.setString(6, direcaoAcesso);
	    stRegistro.setLong(7, usoMarcacao);
	    stRegistro.setLong(8, colaborador.getCodigoOrganograma());
	    stRegistro.setLong(9, colaborador.getNumeroLocal());
	    stRegistro.setString(10, tipoDirecaoAcesso);
	    stRegistro.setLong(11, permanenciaPosto);
	    stRegistro.setString(12, fone);
	    stRegistro.setString(13, colaborador.getNumeroCracha());
	    stRegistro.setTimestamp(14, new Timestamp(dataAtual.getTimeInMillis()), dataAtual);
	    stRegistro.setTimestamp(15, new Timestamp(dataAtual.getTimeInMillis()), dataAtual);
	    stRegistro.setTimestamp(16, new Timestamp(dataAtual.getTimeInMillis()), dataAtual);

	    stRegistro.executeUpdate();
	    stRegistro.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	    throw new Exception(e);
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}
    }

    public static List<AcessoVO> getErrosAcesso(String numcra, GregorianCalendar dataAtual)
    {
	List<AcessoVO> acessos = new ArrayList<AcessoVO>();
	AcessoVO acesso = new AcessoVO();

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    StringBuffer sqlAcesso = new StringBuffer();
	    sqlAcesso.append(" SELECT DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) as data,DirAcc,USU_TipDir,DesTac, USU_FonACc FROM R070ACC acc ");
	    sqlAcesso.append(" INNER JOIN R070TAC tac ON tac.TipAcc = acc.TipAcc ");
	    sqlAcesso.append(" WHERE acc.NumCra = ? AND acc.DatAcc >= DATEADD(MM, -3, ?) AND acc.tipacc > 100 AND acc.CodPlt = 2 ");
	    sqlAcesso.append(" ORDER BY DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) desc ");

	    PreparedStatement stAcesso = connVetorh.prepareStatement(sqlAcesso.toString());
	    stAcesso.setString(1, numcra);
	    stAcesso.setTimestamp(2, new Timestamp(dataAtual.getTimeInMillis()));
	    ResultSet rsAcesso = stAcesso.executeQuery();

	    //Acessos
	    while (rsAcesso.next())
	    {
		acesso = new AcessoVO();

		Timestamp dataAcesso = rsAcesso.getTimestamp("data");
		GregorianCalendar gData = new GregorianCalendar();
		gData.setTimeInMillis(dataAcesso.getTime());

		acesso.setData(gData);
		acesso.setTipoAcesso(rsAcesso.getString("DesTac"));

		if (rsAcesso.getString("DirAcc").equals("E"))
		{
		    acesso.setDirecao("Entrada");
		}
		else
		{
		    acesso.setDirecao("Saída");
		}

		if (rsAcesso.getString("USU_TipDir").equals("P"))
		{
		    acesso.setTipoDirecao("Padrão");
		}
		else if (rsAcesso.getString("USU_TipDir").equals("A"))
		{
		    acesso.setTipoDirecao("Autorizada");
		}
		else
		{
		    acesso.setTipoDirecao("Local");
		}

		if (rsAcesso.getString("USU_FonACc") != null)
		{
		    acesso.setFone(rsAcesso.getString("USU_FonACc"));
		}
		else
		{
		    acesso.setFone("");
		}
		acessos.add(acesso);
	    }

	    rsAcesso.close();
	    stAcesso.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}

	return acessos;
    }

    public static ColaboradorVO buscaFichaColaborador(Long codigoEmpresa, Long tipColaborador, Long codigoCadastro, GregorianCalendar dataAtual)
    {

	Connection connVetorh = PersistEngine.getConnection("VETORH");
	ColaboradorVO colaborador = null;
	EscalaVO escala = new EscalaVO();

	GregorianCalendar dataNulaSenior = new GregorianCalendar();
	dataNulaSenior.set(GregorianCalendar.YEAR, 1900);
	dataNulaSenior.set(GregorianCalendar.MONTH, 12);
	dataNulaSenior.set(GregorianCalendar.DATE, 31);
	dataNulaSenior.set(GregorianCalendar.HOUR, 00);
	dataNulaSenior.set(GregorianCalendar.MINUTE, 00);
	dataNulaSenior.set(GregorianCalendar.SECOND, 00);
	dataNulaSenior.set(GregorianCalendar.MILLISECOND, 00);



	try
	{
	    /*** Recupera Colaboradores ***/
	    StringBuffer queryColaborador = new StringBuffer();
	    queryColaborador.append(" SELECT fun.NumEmp, fun.TipCol, fun.NumCad, hch.NumCra, fun.NomFun, fun.NumCpf, fun.DatNas, fun.TipSex, fun.DatAdm, ");
	    queryColaborador.append("        esc.NomEsc, car.TitRed, sis.DesSis, hes.CodEsc, hes.CodTma, orn.NumLoc, fun.EmiCar, fun.USU_DepEmp, ");
	    queryColaborador.append("        ISNULL(afa.SitAfa, 1) AS SitAfa, ISNULL(sit.DesSit, 'Trabalhando') AS DesSit, ISNULL(sit.usu_blopre, 'N') AS BloPre, ");
	    queryColaborador.append("        afa.DatAfa AS dataAfa, afa.DatTer, fis.USU_NomFis, afaDem.DatAfa as dataDem, cauDem.DesDem, fun.USU_MatRes, ");
	    queryColaborador.append("        pai.NomPai,cid.EstCid, cid.NomCid, bai.NomBai, cpl.EndCep, cpl.EndNum, cpl.EndRua, cpl.EndCpl, cpl.NumTel, ");
	    queryColaborador.append("        orn.usu_endloc as endPosto, cidPosto.EstCid as estcidPosto, cidPosto.nomcid as nomcidPosto, baiPosto.nomBai as nomBaiPosto, paiPosto.nompai as nompaiPosto, orn.usu_endcep as endcepPosto, orn.usu_cplctr,  ");
	    queryColaborador.append("		 cpl.ultExm, cpl.proExm, cpl.usu_dat_rec, cadcur.usu_datfor, cpl.nmtel2");
	    queryColaborador.append(" FROM R034FUN fun  ");
	    queryColaborador.append(" LEFT JOIN R038HCH hch WITH (NOLOCK) ON hch.NUMEMP = fun.NumEmp AND fun.TIPCOL = hch.TIPCOL AND fun.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch.HorIni, hch.DatIni) = (SELECT MAX(DATEADD(MINUTE, hch2.HorIni, hch2.DatIni)) FROM R038HCH hch2 WHERE hch2.NUMEMP = hch.NUMEMP AND hch2.TIPCOL = hch.TIPCOL AND hch2.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch2.HorIni, hch2.DatIni) <= ?)");
	    queryColaborador.append(" LEFT JOIN R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp AND hlo.TipCol = fun.TipCol AND hlo.NumCad = fun.NumCad AND hlo.DatAlt = (SELECT MAX (DATALT) FROM R038HLO TABELA001 WHERE TABELA001.NUMEMP = hlo.NUMEMP AND TABELA001.TIPCOL = hlo.TIPCOL AND TABELA001.NUMCAD = hlo.NUMCAD AND TABELA001.DATALT <= ?) ");
	    queryColaborador.append(" LEFT JOIN R038HES hes WITH (NOLOCK) ON hes.NumEmp = fun.NumEmp AND hes.TipCol = fun.TipCol AND hes.NumCad = fun.NumCad AND hes.DatAlt = (SELECT MAX (DATALT) FROM R038HES TABELA001 WHERE TABELA001.NUMEMP = hes.NUMEMP AND TABELA001.TIPCOL = hes.TIPCOL AND TABELA001.NUMCAD = hes.NUMCAD AND TABELA001.DATALT <= ?) ");
	    queryColaborador.append(" LEFT JOIN R006ESC esc ON esc.CodEsc = hes.CodEsc ");
	    queryColaborador.append(" LEFT JOIN R038HCA hca WITH (NOLOCK) ON hca.NumEmp = fun.NumEmp AND hca.TipCol = fun.TipCol AND hca.NumCad = fun.NumCad AND hca.DatAlt = (SELECT MAX (DATALT) FROM R038HCA TABELA001 WHERE TABELA001.NUMEMP = hca.NUMEMP AND TABELA001.TIPCOL = hca.TIPCOL AND TABELA001.NUMCAD = hca.NUMCAD AND TABELA001.DATALT <= ?) ");
	    queryColaborador.append(" LEFT JOIN R024CAR car ON car.EstCar = hca.EstCar AND car.CodCar = hca.CodCar ");
	    queryColaborador.append(" LEFT JOIN R024SIS sis ON sis.SisCar = car.SisCar ");
	    queryColaborador.append(" LEFT JOIN R016ORN orn WITH (NOLOCK) ON orn.NumLoc = hlo.NumLoc and orn.TabOrg = 203 ");
	    queryColaborador.append(" LEFT JOIN USU_TFisRes fis ON fis.USU_FisRes = fun.usu_fisres ");
	    queryColaborador.append(" LEFT JOIN R038AFA afa WITH (NOLOCK) ON (afa.NumEmp = fun.NumEmp AND afa.TipCol = fun.TipCol AND afa.NumCad = fun.NumCad AND afa.DatAfa = (SELECT MAX (DATAFA) FROM R038AFA TABELA001 WHERE TABELA001.NUMEMP = afa.NUMEMP AND TABELA001.TIPCOL = afa.TIPCOL AND TABELA001.NUMCAD = afa.NUMCAD AND TABELA001.DATAFA <= ? AND (TABELA001.DatTer = '1900-12-31 00:00:00' OR TABELA001.DatTer >= cast(floor(cast(? as float)) as datetime)))) ");
	    queryColaborador.append(" LEFT JOIN R038AFA afaDem WITH (NOLOCK) ON afaDem.NumEmp = fun.NumEmp AND afaDem.TipCol = fun.TipCol AND afaDem.NumCad = fun.NumCad AND afaDem.sitAfa = 7 AND afaDem.DatAfa = (SELECT MAX (DATAFA) FROM R038AFA TABELA001 WHERE TABELA001.NUMEMP = afaDem.NUMEMP AND TABELA001.TIPCOL = afaDem.TIPCOL AND TABELA001.NUMCAD = afaDem.NUMCAD) ");
	    queryColaborador.append(" LEFT JOIN R042CAU cauDem ON cauDem.CauDem = afaDem.CauDem ");
	    queryColaborador.append(" LEFT JOIN R010SIT sit ON sit.CodSit = afa.SitAfa ");
	    queryColaborador.append(" LEFT JOIN R034CPL cpl ON cpl.NumEmp = fun.NumEmp AND cpl.TipCol = fun.TipCol AND cpl.NumCad = fun.NumCad ");
	    queryColaborador.append(" LEFT JOIN R074CID cid ON cid.CodCid = cpl.CodCid ");
	    queryColaborador.append(" LEFT JOIN R074BAI bai ON bai.CodCid = cpl.CodCid AND bai.CodBai = cpl.CodBai ");
	    queryColaborador.append(" LEFT JOIN R074PAI pai ON pai.CodPai = cid.CodPai ");
	    queryColaborador.append(" LEFT JOIN R034FOT fot ON fot.NumCad = fun.NumCad AND fot.NumEmp = fun.NumEmp AND fot.TipCol = fun.TipCol ");

	    queryColaborador.append(" LEFT JOIN R074CID cidPosto ON cidPosto.CodCid = orn.usu_CodCid "); 
	    queryColaborador.append(" LEFT JOIN R074BAI baiPosto ON baiPosto.CodCid = orn.usu_CodCid AND baiPosto.CodBai = orn.usu_CodBai "); 
	    queryColaborador.append(" LEFT JOIN R074PAI paiPosto ON paiPosto.CodPai = cidPosto.CodPai ");

	    queryColaborador.append(" LEFT JOIN USU_TCadCur cadcur on fun.tipcol = cadcur.usu_tipcol and fun.numemp = cadcur.usu_numemp and fun.numcad = cadcur.usu_numcad ");

	    queryColaborador.append(" WHERE fun.DatAdm <= ?  ");
	    queryColaborador.append(" AND fun.TipCol = 1 AND fun.NumCad = ? AND fun.NumEmp = ? ");

	    Timestamp dataAtualTS = new Timestamp(dataAtual.getTimeInMillis());
	    PreparedStatement stColaborador = connVetorh.prepareStatement(queryColaborador.toString());
	    stColaborador.setTimestamp(1, dataAtualTS);
	    stColaborador.setTimestamp(2, dataAtualTS);
	    stColaborador.setTimestamp(3, dataAtualTS);
	    stColaborador.setTimestamp(4, dataAtualTS);
	    stColaborador.setTimestamp(5, dataAtualTS);
	    stColaborador.setTimestamp(6, dataAtualTS);
	    stColaborador.setTimestamp(7, dataAtualTS);
	    stColaborador.setLong(8, codigoCadastro);
	    stColaborador.setLong(9, codigoEmpresa);

	    ResultSet rsColaborador = stColaborador.executeQuery();

	    if (rsColaborador.next())
	    {
		colaborador = new ColaboradorVO();
		colaborador.setNumeroEmpresa(rsColaborador.getLong("NumEmp"));
		colaborador.setTipoColaborador(rsColaborador.getLong("TipCol"));
		colaborador.setNumeroCadastro(rsColaborador.getLong("NumCad"));
		colaborador.setNomeColaborador(rsColaborador.getString("NomFun"));
		colaborador.setCargo(rsColaborador.getString("TitRed"));
		colaborador.setSistemaCargo(rsColaborador.getString("DesSis"));
		colaborador.setSituacao(rsColaborador.getString("DesSit"));
		colaborador.setAcessoBloqueado("S".equals(rsColaborador.getString("BloPre")));
		colaborador.setControlaAcesso("S".equals(rsColaborador.getString("EmiCar")));
		colaborador.setCpf(rsColaborador.getLong("NumCpf"));
		colaborador.setSexo(rsColaborador.getString("TipSex"));
		colaborador.setDataNascimento(rsColaborador.getDate("DatNas"));
		colaborador.setDataAdmissao(rsColaborador.getDate("datAdm"));
		colaborador.setNumeroLocal(rsColaborador.getLong("NumLoc"));
		colaborador.setCodigoEscala(rsColaborador.getLong("CodEsc"));
		colaborador.setTurmaEscala(rsColaborador.getLong("CodTma"));
		colaborador.setDataAfastamento(rsColaborador.getDate("dataAfa"));
		colaborador.setDataDemissao(rsColaborador.getDate("dataDem"));
		colaborador.setFiscal(rsColaborador.getString("USU_NomFis"));
		colaborador.setMatrizResponsabilidade(rsColaborador.getString("USU_MatRes"));
		colaborador.setCausaDemissao(rsColaborador.getString("DesDem"));
		//colaborador.setFoto(rsColaborador.getBytes("FotEmp"));
		colaborador.setTelefone(rsColaborador.getString("NumTel"));
		colaborador.setTelefone2(rsColaborador.getString("nmtel2"));

		Long coddep = rsColaborador.getLong("USU_DepEmp");

		if (rsColaborador.getDate("ultExm") != null){
		    GregorianCalendar gc = new GregorianCalendar();
		    gc.setTimeInMillis(rsColaborador.getDate("ultExm").getTime());
		    colaborador.setUltExm(NeoUtils.safeDateFormat(gc, "dd/MM/yyyy"));
		}
		if (rsColaborador.getDate("proExm") != null){
		    GregorianCalendar gc = new GregorianCalendar();
		    gc.setTimeInMillis(rsColaborador.getDate("proExm").getTime());
		    colaborador.setProExm(NeoUtils.safeDateFormat(gc, "dd/MM/yyyy"));
		}
		if (rsColaborador.getDate("usu_dat_rec") != null){
		    GregorianCalendar gc = new GregorianCalendar();
		    gc.setTimeInMillis(rsColaborador.getDate("usu_dat_rec").getTime());
		    colaborador.setUsuDatRec(NeoUtils.safeDateFormat(gc, "dd/MM/yyyy"));
		}
		if (rsColaborador.getDate("usu_datfor") != null){
		    GregorianCalendar gc = new GregorianCalendar();
		    gc.setTimeInMillis(rsColaborador.getDate("usu_datfor").getTime());
		    colaborador.setUsuDatfor(NeoUtils.safeDateFormat(gc, "dd/MM/yyyy"));
		}










		if (coddep != null)
		{
		    List<NeoObject> objs = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("FichaBasicaDepartamento"), new QLEqualsFilter("codigo", coddep));
		    if (objs != null && objs.size() > 0)
		    {
			EntityWrapper wObj = new EntityWrapper(objs.get(0));
			String dep = (String) wObj.findValue("descricao");
			if (dep != null)
			{
			    colaborador.setDepartamento(dep);
			}
		    }
		}

		EnderecoVO endereco = new EnderecoVO();
		if (rsColaborador.getString("NomBai") != null)
		{
		    endereco.setBairro(rsColaborador.getString("NomBai"));
		    endereco.setCep(rsColaborador.getString("EndCep"));
		    endereco.setCidade(rsColaborador.getString("NomCid"));
		    endereco.setComplemento(rsColaborador.getString("EndCpl"));
		    endereco.setLogradouro(rsColaborador.getString("EndRua"));
		    endereco.setNumero(rsColaborador.getString("EndNum"));
		    endereco.setPais(rsColaborador.getString("NomPai"));
		    endereco.setUf(rsColaborador.getString("EstCid"));
		    colaborador.setEndereco(endereco);
		}

		EnderecoVO enderecoPosto = new EnderecoVO();
		if (rsColaborador.getString("endPosto") != null)
		{
		    enderecoPosto.setBairro(NeoUtils.safeOutputString(rsColaborador.getString("nomBaiPosto")));
		    enderecoPosto.setCep(rsColaborador.getString("endcepPosto"));
		    enderecoPosto.setCidade(rsColaborador.getString("nomcidPosto"));
		    enderecoPosto.setComplemento(NeoUtils.safeOutputString(rsColaborador.getString("usu_cplctr")));
		    enderecoPosto.setLogradouro(rsColaborador.getString("endPosto"));
		    enderecoPosto.setNumero("");
		    enderecoPosto.setPais(rsColaborador.getString("nompaiPosto"));
		    enderecoPosto.setUf(rsColaborador.getString("estcidPosto"));
		    colaborador.setEnderecoPosto(enderecoPosto);
		}

		setEscalaColaboradorUnico(colaborador, dataAtual);

		colaborador.getEscala().setDescricao(rsColaborador.getString("NomEsc") + " / " + colaborador.getTurmaEscala());

		if (rsColaborador.getString("numcra") != null)
		{
		    colaborador.setNumeroCracha(rsColaborador.getString("numcra"));
		}
		else
		{
		    colaborador.setNumeroCracha("");
		}

		String obsSituacao = "";
		if (rsColaborador.getLong("SitAfa") != 1L)
		{
		    GregorianCalendar dataInicioAfastamento = new GregorianCalendar();
		    GregorianCalendar dataFimAfastamento = new GregorianCalendar();

		    if (rsColaborador.getTimestamp("DataAfa") != null)
		    {
			dataInicioAfastamento.setTime(rsColaborador.getTimestamp("DataAfa"));
		    }
		    if (rsColaborador.getTimestamp("DatTer") != null)
		    {
			dataFimAfastamento.setTime(rsColaborador.getTimestamp("DatTer"));
		    }
		    if ((dataInicioAfastamento != null))
		    {
			if (dataFimAfastamento.get(Calendar.YEAR) == 1900)
			{
			    obsSituacao = "A partir de " + NeoUtils.safeDateFormat(dataInicioAfastamento, "dd/MM/yyyy");
			}
			else
			{
			    obsSituacao = "De: " + NeoUtils.safeDateFormat(dataInicioAfastamento, "dd/MM/yyyy");
			    obsSituacao = obsSituacao + " a " + NeoUtils.safeDateFormat(dataFimAfastamento, "dd/MM/yyyy");
			}
		    }
		}

		if (colaborador.getDataDemissao() != null)
		{
		    obsSituacao = "Em aviso prévio até " + NeoUtils.safeDateFormat(colaborador.getDataDemissao(), "dd/MM/yyyy");
		}

		colaborador.setObsSituacao(obsSituacao);

		colaborador.setAvosFerias(retornaAvosFerias(colaborador.getNumeroEmpresa(), colaborador.getTipoColaborador(), colaborador.getNumeroCadastro()));

	    }
	    rsColaborador.close();
	    stColaborador.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}

	return colaborador;
    }


    public static List<ObsHoristaVO> listaObsHoristas(Long numemp, Long tipcol, Long numcad, String numcpf){
	QLFilter numcadFilter = new QLEqualsFilter("numCadHor", numcad);
	QLFilter numempFilter = new QLEqualsFilter("numEmpHor", numemp);
	QLFilter tipcolFilter = new QLEqualsFilter("tipcol", tipcol);
	QLGroupFilter groupFilter = new QLGroupFilter("AND");
	groupFilter.addFilter(numcadFilter);
	groupFilter.addFilter(numempFilter);
	groupFilter.addFilter(tipcolFilter);
	List<NeoObject> objs = PersistEngine.getObjects(AdapterUtils.getEntityClass("obsHoristas"), groupFilter);
	List<ObsHoristaVO> obssHorista = new ArrayList<ObsHoristaVO>();

	for(NeoObject obj : objs){
	    EntityWrapper whapper = new EntityWrapper(obj);
	    ObsHoristaVO obsHorista = new ObsHoristaVO();
	    if(whapper.findField("numCadHor").getValue() != null){
		obsHorista.setNumCadHor((Long)whapper.findField("numCadHor").getValue());
	    }else{
		obsHorista.setNumCadHor(0L);
	    }

	    if(whapper.findField("numEmpHor").getValue() != null){
		obsHorista.setNumEmpHor((Long)whapper.findField("numEmpHor").getValue());
	    }else{
		obsHorista.setNumEmpHor(0L);
	    }

	    if(whapper.findField("tipcol").getValue() != null){
		obsHorista.setTipCol((Long)whapper.findField("tipcol").getValue());
	    }else{
		obsHorista.setTipCol(0L);
	    }

	    if(whapper.findField("dataRegistro").getValue() != null){
		obsHorista.setDataRegistro(NeoDateUtils.safeDateFormat((GregorianCalendar) whapper.findField("dataRegistro").getValue(),"dd/MM/yyyy"));
	    }

	    if(whapper.findField("horRegistro").getValue() != null){
		obsHorista.setHoraRegistro((Long)whapper.findField("horRegistro").getValue());
	    }else{
		obsHorista.setHoraRegistro(0L);
	    }
	    obsHorista.setNeoId(String.valueOf(whapper.findField("neoId").getValue()));
	    obsHorista.setLinkExcluir("<a title='Remover' style='cursor:pointer' onclick='javascript: if (confirm(\"Tem Certeza?\") == true) { removerObsHorista("+obsHorista.getNumCadHor()+","+obsHorista.getNumEmpHor()+","+obsHorista.getTipCol()+","+obsHorista.getNeoId()+","+numcpf+"); }'><img src='imagens/icones_final/delete_16x16-trans.png'></a>");
	    obsHorista.setObservacao(String.valueOf(whapper.findField("obsHorista").getValue()));
	    obssHorista.add(obsHorista);
	}
	return obssHorista;
    }

    public static List<AfastamentoVO> listaAfastamentos(Long numemp, Long tipcol, Long numcad)
    {

	Long key = GregorianCalendar.getInstance().getTimeInMillis();
	//System.out.println("[PRESENCA AFASTAMENTOS] ["+key+"] numcad:" + numcad+ ", numemp:"+numemp + ", tipcol:"+tipcol);

	List<AfastamentoVO> afastamentos = new ArrayList<AfastamentoVO>();
	AfastamentoVO afastamento = new AfastamentoVO();

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    StringBuffer sqlAfastamento = new StringBuffer();
	    sqlAfastamento.append(" SELECT fun.NumEmp, fun.TipCol, fun.NumCad, fun.NumCpf, afa.DatAfa, afa.HorAfa, afa.DatTer, afa.ObsAfa, sit.DesSit FROM R038AFA afa ");
	    sqlAfastamento.append(" INNER JOIN R034FUN fun ON fun.NumEmp = afa.NumEmp AND afa.NumCad = fun.NumCad AND fun.TipCol = afa.TipCol ");
	    sqlAfastamento.append(" INNER JOIN R010SIT sit ON sit.CodSit = afa.SitAfa ");
	    sqlAfastamento.append(" WHERE fun.NumEmp = ? AND fun.TipCol = ? AND fun.NumCad = ? ");
	    sqlAfastamento.append(" ORDER BY afa.datafa desc ");


	    //System.out.println("[PRESENCA AFASTAMENTOS] ["+key+"] sql=>" + sqlAfastamento.toString());

	    PreparedStatement stAfastamento = connVetorh.prepareStatement(sqlAfastamento.toString());
	    stAfastamento.setLong(1, numemp);
	    stAfastamento.setLong(2, tipcol);
	    stAfastamento.setLong(3, numcad);
	    ResultSet rsAfastamento = stAfastamento.executeQuery();

	    //Afastamentos
	    int cont = 0;
	    while (rsAfastamento.next())
	    {
		//System.out.println("[PRESENCA AFASTAMENTOS] ["+key+"] numcad:" + numcad+ ", numemp:"+numemp + ", tipcol:"+tipcol +" afa."+cont++);
		afastamento = new AfastamentoVO();

		Timestamp dataAfaIni = rsAfastamento.getTimestamp("DatAfa");
		GregorianCalendar gDataAfaIni = new GregorianCalendar();
		gDataAfaIni.setTimeInMillis(dataAfaIni.getTime());

		Timestamp dataAfaFim = rsAfastamento.getTimestamp("DatTer");
		GregorianCalendar gDataAfaFim = new GregorianCalendar();
		gDataAfaFim.setTimeInMillis(dataAfaFim.getTime());

		ColaboradorVO colaborador = new ColaboradorVO();
		colaborador.setNumeroEmpresa(rsAfastamento.getLong("NumEmp"));
		colaborador.setTipoColaborador(rsAfastamento.getLong("Tipcol"));
		colaborador.setNumeroCadastro(rsAfastamento.getLong("NumCad"));
		colaborador.setCpf(rsAfastamento.getLong("NumCpf"));

		afastamento.setColaborador(colaborador);
		afastamento.setDataInicio(gDataAfaIni);
		afastamento.setDataFinal(gDataAfaFim);
		afastamento.setObservacao(rsAfastamento.getString("ObsAfa"));
		afastamento.setSituacao(rsAfastamento.getString("DesSit"));

		NeoUser user = PortalUtil.getCurrentUser();
		NeoPaper papel500 = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "[QLP] Exclusao Afastamento Situacao 500"));
		NeoPaper papel15 = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "[QLP] Exclusao Afastamento Situacao 15"));

		Boolean isPapel500 = Boolean.FALSE;
		Boolean isPapel15 = Boolean.FALSE;

		if (user != null && user.getPapers() != null && user.getPapers().contains(papel500))
		{
		    isPapel500 = Boolean.TRUE;
		}

		if (user != null && user.getPapers() != null && user.getPapers().contains(papel15))
		{
		    isPapel15 = Boolean.TRUE;
		}

		String txtExcluir = "<a title='Excluir Afastamento' style='cursor:pointer' onclick='javascript:deleteAfastamento(" + afastamento.getColaborador().getNumeroEmpresa() + "," + afastamento.getColaborador().getTipoColaborador() + "," + afastamento.getColaborador().getNumeroCadastro() + "," + afastamento.getColaborador().getCpf() + ",\"" + NeoUtils.safeDateFormat(afastamento.getDataInicio(), "dd/MM/yyyy") + "\")'><img src='imagens/custom/delete.png'></a>";

		if (user != null && (user.isAdm() || isPapel500) && (afastamento.getSituacao() != null && afastamento.getSituacao().equals("Situação Indefinida (QL)")))
		{
		    afastamento.setExcluir(txtExcluir);
		}

		if (user != null && (user.isAdm() || isPapel15) && (afastamento.getSituacao() != null && afastamento.getSituacao().equals("Faltas")))
		{
		    afastamento.setExcluir(txtExcluir);
		}

		//System.out.println("[PRESENCA AFASTAMENTOS] ["+key+"] numcad:" + numcad+ ", numemp:"+numemp + ", tipcol:"+tipcol +" buscar tarefa de afastamento.");
		Long numcpf = rsAfastamento.getLong("NumCpf");
		Long horafa = rsAfastamento.getLong("HorAfa");
		QLEqualsFilter filterCpf = new QLEqualsFilter("numcpf", numcpf);
		QLEqualsFilter filterData = new QLEqualsFilter("datafa", gDataAfaIni);
		QLEqualsFilter filterHora = new QLEqualsFilter("horafa", horafa);

		QLGroupFilter filterGroup = new QLGroupFilter("AND");
		filterGroup.addFilter(filterCpf);
		filterGroup.addFilter(filterData);
		filterGroup.addFilter(filterHora);


		List<NeoObject> tarefas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TarefaAfastamentoJustificativa"), filterGroup);

		if (tarefas != null && !tarefas.isEmpty()){
		    //System.out.println("[PRESENCA AFASTAMENTOS] ["+key+"] tem tarefas de afastamento...");
		    
		    StringBuilder links = new StringBuilder();
		    
		    for (NeoObject obj : tarefas){
			    EntityWrapper tarefaWrapper = new EntityWrapper(obj);
			    String tarefa = (String) tarefaWrapper.findValue("tarefa");
			    
			    String[] ts = tarefa.split(",");
			    
			    for (String t : ts){
				
				    if (NeoUtils.safeIsNull(tarefa))
				    {
					continue;
				    }

				    if (tarefa.equals("Afastamento Informado"))
				    {
					links.append("<a title='Afastamento informando com antecedência' ><img src='imagens/custom/papel_16x16-info.png'></a>");
					continue;
				    }
				    //teste
				    /*QLEqualsFilter filterTarefa = new QLEqualsFilter("Name", "G001 - Tarefa Simples");
							ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, filterTarefa);

							QLEqualsFilter filterModel = new QLEqualsFilter("model", processModel);
							QLEqualsFilter filterCode = new QLEqualsFilter("code", tarefa);

							QLGroupFilter filterGroupWf = new QLGroupFilter("AND");
							filterGroupWf.addFilter(filterCode);
							filterGroupWf.addFilter(filterModel);

							WFProcess process = (WFProcess) PersistEngine.getObject(WFProcess.class, filterGroupWf);

							if (NeoUtils.safeIsNull(process))
							{
								break;
							}*/

				    GregorianCalendar dataTarefa = (GregorianCalendar) tarefaWrapper.findValue("datafa");

				    System.out.println("Data tarefa: "+NeoDateUtils.safeDateFormat(dataTarefa, "dd/MM/yyyy"));

				    GregorianCalendar dataReferencia = new GregorianCalendar(2016, 10, 30);

				    System.out.println("Data referencia: "+NeoDateUtils.safeDateFormat(dataReferencia, "dd/MM/yyyy"));

				    Long processo = 0L;

				    if (dataTarefa.after(dataReferencia)){
					processo = retornaNeoIdProcess("[JA] Justificativa de Afastamento",t);
				    }else{
					processo = retornaNeoIdProcess("G001 - Tarefa Simples",t);
				    }

				    if (processo != null && !processo.equals(0L)){
					links.append("<a title='Visualizar tarefa' style='cursor:pointer' onclick='javascript:showTarefa(" + processo + ")'><img src='imagens/custom/preview.png'></a>");
				    }
				
			    }
			    
		    }
		    
		    afastamento.setLink(links.toString());
		    
		}			
		afastamentos.add(afastamento);
		System.out.println("[PRESENCA AFASTAMENTOS] ["+key+"] numcad:" + numcad+ ", numemp:"+numemp + ", tipcol:"+tipcol +" buscar tarefa de afastamentos: "+ afastamentos.size());
	    }

	    rsAfastamento.close();
	    stAfastamento.close();
	}
	catch (Exception e)
	{
	    System.out.println("[PRESENCA AFASTAMENTOS ERRO] ["+key+"] numcad:" + numcad+ ", numemp:"+numemp + ", tipcol:"+tipcol +" buscar tarefa de afastamento.");
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}
	//System.out.println("[PRESENCA AFASTAMENTOS] ["+key+"] total afastamentos." + afastamentos.size());
	return afastamentos;
    }

    public static List<AnotacaoVO> listaAnotacoes(Long numemp, Long tipcol, Long numcad)
    {
	List<AnotacaoVO> anotacoes = new ArrayList<AnotacaoVO>();
	AnotacaoVO anotacao = new AnotacaoVO();

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    StringBuffer sqlAnotacao = new StringBuffer();
	    sqlAnotacao.append(" SELECT ano.NumEmp, ano.TipCol, ano.NumCad, ano.DatNot, tip.DesNot, ano.NotFic FROM R038NOT ano ");
	    sqlAnotacao.append(" LEFT JOIN R022NOT tip ON tip.CodNot = ano.TipNot ");
	    sqlAnotacao.append(" WHERE ano.NumEmp = ? AND ano.TipCol = ? AND ano.NumCad = ? ");
	    sqlAnotacao.append(" ORDER BY ano.DatNot desc ");

	    PreparedStatement stAnotacao = connVetorh.prepareStatement(sqlAnotacao.toString());
	    stAnotacao.setLong(1, numemp);
	    stAnotacao.setLong(2, tipcol);
	    stAnotacao.setLong(3, numcad);
	    ResultSet rsAnotacao = stAnotacao.executeQuery();

	    //Anotacaos
	    while (rsAnotacao.next())
	    {
		anotacao = new AnotacaoVO();

		Timestamp data = rsAnotacao.getTimestamp("DatNot");
		GregorianCalendar gData = new GregorianCalendar();
		gData.setTimeInMillis(data.getTime());

		ColaboradorVO colaborador = new ColaboradorVO();
		colaborador.setNumeroEmpresa(rsAnotacao.getLong("NumEmp"));
		colaborador.setTipoColaborador(rsAnotacao.getLong("Tipcol"));
		colaborador.setNumeroCadastro(rsAnotacao.getLong("NumCad"));

		anotacao.setColaborador(colaborador);
		anotacao.setData(gData);
		anotacao.setDescricao(rsAnotacao.getString("NotFic"));
		anotacao.setTipoAnotacao(rsAnotacao.getString("DesNot"));

		anotacoes.add(anotacao);
	    }

	    rsAnotacao.close();
	    stAnotacao.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}
	return anotacoes;
    }

    public static List<EntradaAutorizadaVO> listaCoberturas(Long numemp, Long tipcol, Long numcad)
    {
	List<EntradaAutorizadaVO> entradaAutorizadas = new ArrayList<EntradaAutorizadaVO>();
	EntradaAutorizadaVO entradaAutorizada = new EntradaAutorizadaVO();

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    StringBuffer sqlEntradaAutorizada = new StringBuffer();
	    sqlEntradaAutorizada.append(" SELECT TOP(1000) fun.NumEmp AS NumEmpFun, fun.TipCol AS TipColFun, fun.NumCad AS NumCadFun, cob.USU_DatAlt, cob.USU_HorIni, ");
	    sqlEntradaAutorizada.append(" 	   cob.USU_DatFim,sub.NumEmp AS NumEmpSub, sub.TipCol AS TipColSub, sub.NumCad As NumCadSub, sub.NomFun AS NomFunSub, mot.USU_DesMot, cob.USU_ObsCob, ");
	    sqlEntradaAutorizada.append(" 	   orn.USU_NomCli, orn.USU_LotOrn, orn.NomLoc, orn.USU_CodCcu, hes.CodEsc, hes.CodTma, esc.NomEsc, fun.numcpf, cob.usu_datIniRet, cob.usu_datFimRet ");
	    sqlEntradaAutorizada.append(" FROM USU_T038COBFUN cob ");
	    sqlEntradaAutorizada.append(" INNER JOIN R034FUN fun ON fun.NumEmp = cob.USU_NumEmp AND fun.TipCol = cob.USU_TipCol AND fun.NumCad = cob.USU_NumCad ");
	    sqlEntradaAutorizada.append(" LEFT JOIN R034FUN sub ON sub.NumEmp = cob.USU_NumEmpCob AND sub.TipCol = cob.USU_TipColCob AND sub.NumCad = cob.USU_NumCadCob ");
	    sqlEntradaAutorizada.append(" LEFT JOIN R038HES hes ON hes.NumEmp = sub.NumEmp AND hes.TipCol = sub.TipCol AND hes.NumCad = sub.NumCad AND hes.DatAlt = ( ");
	    sqlEntradaAutorizada.append(" 	   SELECT MAX (DatAlt) FROM R038HES hes2 WHERE hes2.NumEmp = hes.NumEmp AND hes2.TipCol = hes.TipCol AND hes2.NumCad = hes.NumCad AND hes2.DatAlt <= cob.USU_DatAlt) ");
	    sqlEntradaAutorizada.append(" LEFT JOIN R006ESC esc ON esc.CodEsc = hes.CodEsc ");
	    sqlEntradaAutorizada.append(" INNER JOIN R016ORN orn ON orn.NumLoc = cob.USU_NumLocTra AND orn.TabOrg = cob.USU_TabOrgTra ");
	    sqlEntradaAutorizada.append(" INNER JOIN USU_T010MoCo mot ON mot.USU_CodMot = cob.USU_codMot ");
	    sqlEntradaAutorizada.append(" WHERE fun.NumEmp = ? AND fun.TipCol = ? AND fun.NumCad = ? AND mot.USU_CodMot not in (4, 5, 8) ");
	    sqlEntradaAutorizada.append(" ORDER BY cob.USU_DatAlt DESC ");

	    PreparedStatement stEntradaAutorizada = connVetorh.prepareStatement(sqlEntradaAutorizada.toString());
	    stEntradaAutorizada.setLong(1, numemp);
	    stEntradaAutorizada.setLong(2, tipcol);
	    stEntradaAutorizada.setLong(3, numcad);
	    ResultSet rsEntradaAutorizada = stEntradaAutorizada.executeQuery();

	    //EntradaAutorizadas
	    while (rsEntradaAutorizada.next())
	    {
		entradaAutorizada = new EntradaAutorizadaVO();

		Timestamp dataCobIni = rsEntradaAutorizada.getTimestamp("USU_DatAlt");
		GregorianCalendar gDataCobIni = new GregorianCalendar();
		gDataCobIni.setTimeInMillis(dataCobIni.getTime());

		SimpleDateFormat sfinc = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		String dataCoberturaInicial = sfinc.format(dataCobIni.getTime());

		Timestamp dataCobFim = null;

		if(rsEntradaAutorizada.getTimestamp("USU_DatFim") != null){
		    dataCobFim = rsEntradaAutorizada.getTimestamp("USU_DatFim");
		}

		GregorianCalendar gDataCobFim = new GregorianCalendar();

		SimpleDateFormat sfFim = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		String dataCoberturaFinal = null;

		if(dataCobFim != null){
		    dataCoberturaFinal = sfFim.format(dataCobFim.getTime());
		}

		if (dataCobFim != null)
		{
		    gDataCobFim.setTimeInMillis(dataCobFim.getTime());
		}


		Timestamp dataCobIniRet = rsEntradaAutorizada.getTimestamp("usu_datIniRet");
		GregorianCalendar gDataCobIniRet = null;
		if (dataCobIniRet != null && !dataCobIniRet.equals("")){
		    gDataCobIniRet = new GregorianCalendar();				    
		    gDataCobIniRet.setTimeInMillis(dataCobIniRet.getTime());
		}


		Timestamp dataCobFimRet = rsEntradaAutorizada.getTimestamp("usu_datFimRet");
		GregorianCalendar gDataCobFimRet = null;
		if (dataCobFimRet != null && !dataCobFimRet.equals("")){
		    gDataCobFimRet = new GregorianCalendar();
		    gDataCobFimRet.setTimeInMillis(dataCobFimRet.getTime());
		}

		PostoVO posto = new PostoVO();
		posto.setCentroCusto(rsEntradaAutorizada.getString("USU_CodCcu"));
		posto.setLotacao(rsEntradaAutorizada.getString("USU_LotOrn"));
		posto.setNomePosto(rsEntradaAutorizada.getString("NomLoc"));

		entradaAutorizada.setDataInicial(gDataCobIni);
		entradaAutorizada.setDataFinal(gDataCobFim);
		entradaAutorizada.setHoraInicial(rsEntradaAutorizada.getLong("USU_HorIni"));
		entradaAutorizada.setDescricaoCobertura(rsEntradaAutorizada.getString("USU_DesMot"));
		entradaAutorizada.setPosto(posto);
		entradaAutorizada.setObservacao(rsEntradaAutorizada.getString("USU_ObsCob"));
		entradaAutorizada.setDataIniRet(gDataCobIniRet);
		entradaAutorizada.setDataFimRet(gDataCobFimRet);

		ColaboradorVO colaborador = new ColaboradorVO();

		colaborador.setNumeroEmpresa(rsEntradaAutorizada.getLong("NumEmpFun"));
		colaborador.setTipoColaborador(rsEntradaAutorizada.getLong("TipColFun"));
		colaborador.setNumeroCadastro(rsEntradaAutorizada.getLong("NumCadFun"));
		colaborador.setCpf(rsEntradaAutorizada.getLong("numcpf"));

		entradaAutorizada.setColaborador(colaborador);

		ColaboradorVO substituido = new ColaboradorVO();
		EscalaVO escala = new EscalaVO();

		// Se USU_NumCadCob é igual a 0, é cobertura de posto, senão é cobertura de colaborador
		if (rsEntradaAutorizada.getLong("NumCadSub") == 0L)
		{

		    escala.setDescricao(NeoUtils.safeDateFormat(gDataCobIni, "HH:mm") + "-" + NeoUtils.safeDateFormat(gDataCobFim, "HH:mm"));
		    substituido.setEscala(escala);
		}
		else
		{
		    substituido.setNumeroEmpresa(rsEntradaAutorizada.getLong("NumEmpSub"));
		    substituido.setTipoColaborador(rsEntradaAutorizada.getLong("TipColSub"));
		    substituido.setNumeroCadastro(rsEntradaAutorizada.getLong("NumCadSub"));
		    substituido.setNomeColaborador(rsEntradaAutorizada.getString("NomFunSub"));

		    escala.setCodigoEscala(rsEntradaAutorizada.getLong("CodEsc"));
		    escala.setCodigoTurma(rsEntradaAutorizada.getLong("CodTma"));
		    escala.setDescricao(rsEntradaAutorizada.getString("NomEsc") + " / " + escala.getCodigoTurma());
		    substituido.setEscala(escala);
		}
		entradaAutorizada.setColaboradorSubstituido(substituido);

		String txtExcluir = "";

		NeoUser user = PortalUtil.getCurrentUser();
		NeoPaper papelOperador = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "[QLP] Operador do Quadro de Lotação Presença"));

		Boolean isPapelOperador = Boolean.FALSE;

		if (user != null && user.getPapers() != null && user.getPapers().contains(papelOperador))
		{
		    isPapelOperador = Boolean.TRUE;
		}

		if (user != null && (user.isAdm() || isPapelOperador))
		{
		    txtExcluir = getLinkBtnDelete(entradaAutorizada);
		}

		SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
		String dataInicial = sf.format(entradaAutorizada.getDataInicial().getTime());

		if (entradaAutorizada.getColaboradorSubstituido() != null)
		{
		    if (user != null && (user.isAdm() || isPapelOperador))
		    {
			txtExcluir = "<a title='Excluir Cobertura' style='cursor:pointer' onclick='javascript:deleteCobertura(" + entradaAutorizada.getColaborador().getNumeroEmpresa() + "," + entradaAutorizada.getColaborador().getTipoColaborador() + 
				"," + entradaAutorizada.getColaborador().getNumeroCadastro() + ",\"" + dataInicial + "\"," + entradaAutorizada.getHoraInicial() + "," + entradaAutorizada.getColaboradorSubstituido().getNumeroEmpresa() + 
				"," + entradaAutorizada.getColaboradorSubstituido().getTipoColaborador() + "," + entradaAutorizada.getColaboradorSubstituido().getNumeroCadastro() + ", "+colaborador.getCpf()+", \""+entradaAutorizada.getDescricaoCobertura()+"\" ,\"" + dataCoberturaFinal + "\" ,\"" + dataCoberturaInicial + "\")'><img src='imagens/custom/delete.png'></a>";
		    }
		}
		else
		{
		    if (user != null && (user.isAdm() || isPapelOperador))
		    {
			txtExcluir = "<a title='Excluir Cobertura' style='cursor:pointer' onclick='javascript:deleteCobertura(" + entradaAutorizada.getColaborador().getNumeroEmpresa() + "," + entradaAutorizada.getColaborador().getTipoColaborador() + 
				"," + entradaAutorizada.getColaborador().getNumeroCadastro() + ",\"" + dataInicial + "\"," + entradaAutorizada.getHoraInicial() + 
				", '', '', '', " + colaborador.getCpf() + ", \""+entradaAutorizada.getDescricaoCobertura()+"\", \"" + dataCoberturaFinal + "\", \"" + dataCoberturaInicial + "\")'><img src='imagens/custom/delete.png'></a>";
		    }
		}

		entradaAutorizada.setExcluir(txtExcluir);

		entradaAutorizadas.add(entradaAutorizada);
	    }

	    rsEntradaAutorizada.close();
	    stEntradaAutorizada.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}
	return entradaAutorizadas;
    }

    public static List<HistoricoLocalVO> listaHistoricoLocal(Long numemp, Long tipcol, Long numcad)
    {
	List<HistoricoLocalVO> locais = new ArrayList<HistoricoLocalVO>();
	HistoricoLocalVO local = new HistoricoLocalVO();

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    StringBuffer sqlLocal = new StringBuffer();
	    sqlLocal.append(" select fun.NumEmp, fun.TipCol, fun.NumCad, orn.NumLoc, orn.TabOrg, orn.usu_lotorn, orn.nomloc, orn.USU_CodCcu, hlo.DatAlt, cob.usu_obscob from R038HLO hlo ");
	    sqlLocal.append(" INNER JOIN R034FUN fun ON fun.NumEmp = hlo.NumEmp AND fun.NumCad = hlo.NumCad AND fun.TipCol = hlo.TipCol ");
	    sqlLocal.append(" INNER JOIN R016ORN orn ON orn.NumLoc = hlo.NumLoc AND orn.TabOrg = hlo.TabOrg ");
	    sqlLocal.append(" left join  usu_t038cobfun cob on cob.usu_numcad = fun.numcad and cob.usu_numemp = fun.numemp and convert(date,cob.usu_datalt,101) = hlo.DatAlt "); 
	    sqlLocal.append(" WHERE fun.NumEmp = ? AND fun.TipCol = ? AND fun.NumCad = ? ");
	    sqlLocal.append(" ORDER BY hlo.datalt desc ");

	    PreparedStatement stLocal = connVetorh.prepareStatement(sqlLocal.toString());
	    stLocal.setLong(1, numemp);
	    stLocal.setLong(2, tipcol);
	    stLocal.setLong(3, numcad);
	    ResultSet rsLocal = stLocal.executeQuery();

	    //Locals
	    while (rsLocal.next())
	    {
		local = new HistoricoLocalVO();

		Timestamp data = rsLocal.getTimestamp("DatAlt");
		GregorianCalendar gData = new GregorianCalendar();
		gData.setTimeInMillis(data.getTime());

		ColaboradorVO colaborador = new ColaboradorVO();
		colaborador.setNumeroEmpresa(rsLocal.getLong("NumEmp"));
		colaborador.setTipoColaborador(rsLocal.getLong("Tipcol"));
		colaborador.setNumeroCadastro(rsLocal.getLong("NumCad"));

		PostoVO posto = new PostoVO();
		posto.setNumeroLocal(rsLocal.getLong("NumLoc"));
		posto.setCodigoOrganograma(rsLocal.getLong("TabOrg"));
		posto.setLotacao(rsLocal.getString("usu_lotorn"));
		posto.setNomePosto(rsLocal.getString("NomLoc"));
		posto.setCentroCusto(rsLocal.getString("USU_CodCcu"));

		if(rsLocal.getString("usu_obscob") != null){
		    posto.setEntradasAutorizadas(new ArrayList<EntradaAutorizadaVO>());
		    EntradaAutorizadaVO entAut = new EntradaAutorizadaVO();
		    entAut.setObservacao(rsLocal.getString("usu_obscob"));
		    posto.getEntradasAutorizadas().add(entAut);
		}

		local.setColaborador(colaborador);
		local.setPosto(posto);
		local.setData(gData);

		locais.add(local);
	    }

	    rsLocal.close();
	    stLocal.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}
	return locais;
    }

    public static List<HistoricoCargoVO> listaHistoricoCargo(Long numemp, Long tipcol, Long numcad)
    {
	List<HistoricoCargoVO> cargos = new ArrayList<HistoricoCargoVO>();
	HistoricoCargoVO cargo = new HistoricoCargoVO();

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    StringBuffer sqlCargo = new StringBuffer();
	    sqlCargo.append(" SELECT fun.NumEmp, fun.TipCol, fun.NumCad, car.CodCar,car.TitCar, hca.DatAlt from R038HCA hca ");
	    sqlCargo.append(" INNER JOIN R034FUN fun ON fun.NumEmp = hca.NumEmp AND fun.NumCad = hca.NumCad AND fun.TipCol = hca.TipCol ");
	    sqlCargo.append(" INNER JOIN R024CAR car ON car.CodCar = hca.CodCar AND car.EstCar = hca.EstCar ");
	    sqlCargo.append(" WHERE fun.NumEmp = ? AND fun.TipCol = ? AND fun.NumCad = ? ");
	    sqlCargo.append(" ORDER BY hca.datalt desc ");

	    PreparedStatement stCargo = connVetorh.prepareStatement(sqlCargo.toString());
	    stCargo.setLong(1, numemp);
	    stCargo.setLong(2, tipcol);
	    stCargo.setLong(3, numcad);
	    ResultSet rsCargo = stCargo.executeQuery();

	    //Cargos
	    while (rsCargo.next())
	    {
		cargo = new HistoricoCargoVO();

		Timestamp data = rsCargo.getTimestamp("DatAlt");
		GregorianCalendar gData = new GregorianCalendar();
		gData.setTimeInMillis(data.getTime());

		ColaboradorVO colaborador = new ColaboradorVO();
		colaborador.setNumeroEmpresa(rsCargo.getLong("NumEmp"));
		colaborador.setTipoColaborador(rsCargo.getLong("Tipcol"));
		colaborador.setNumeroCadastro(rsCargo.getLong("NumCad"));

		cargo.setColaborador(colaborador);
		cargo.setCargo(rsCargo.getString("TitCar"));
		cargo.setData(gData);

		cargos.add(cargo);
	    }

	    rsCargo.close();
	    stCargo.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}
	return cargos;
    }

    public static List<HistoricoEscalaVO> listaHistoricoEscala(Long numemp, Long tipcol, Long numcad)
    {
	List<HistoricoEscalaVO> escalas = new ArrayList<HistoricoEscalaVO>();
	HistoricoEscalaVO escala = new HistoricoEscalaVO();

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    StringBuffer sqlEscala = new StringBuffer();
	    sqlEscala.append(" SELECT fun.NumEmp, fun.TipCol, fun.NumCad, fun.numcpf, esc.CodEsc, esc.NomEsc, hes.CodTma, hes.DatAlt from R038HES hes ");
	    sqlEscala.append(" INNER JOIN R034FUN fun ON fun.NumEmp = hes.NumEmp AND fun.NumCad = hes.NumCad AND fun.TipCol = hes.TipCol ");
	    sqlEscala.append(" INNER JOIN R006ESC esc ON esc.CodEsc = hes.CodEsc ");
	    sqlEscala.append(" WHERE fun.NumEmp = ? AND fun.TipCol = ? AND fun.NumCad = ? ");
	    sqlEscala.append(" ORDER BY hes.datalt desc ");

	    PreparedStatement stEscala = connVetorh.prepareStatement(sqlEscala.toString());
	    stEscala.setLong(1, numemp);
	    stEscala.setLong(2, tipcol);
	    stEscala.setLong(3, numcad);
	    ResultSet rsEscala = stEscala.executeQuery();

	    //Escalas
	    while (rsEscala.next())
	    {
		escala = new HistoricoEscalaVO();

		Timestamp data = rsEscala.getTimestamp("DatAlt");
		GregorianCalendar gData = new GregorianCalendar();
		gData.setTimeInMillis(data.getTime());

		ColaboradorVO colaborador = new ColaboradorVO();
		colaborador.setNumeroEmpresa(rsEscala.getLong("NumEmp"));
		colaborador.setTipoColaborador(rsEscala.getLong("Tipcol"));
		colaborador.setNumeroCadastro(rsEscala.getLong("NumCad"));
		colaborador.setCpf(rsEscala.getLong("NumCpf"));

		EscalaVO esc = new EscalaVO();
		esc.setCodigoEscala(rsEscala.getLong("CodEsc"));
		esc.setDescricao(rsEscala.getString("NomEsc") + " / " + rsEscala.getLong("CodTma"));

		escala.setColaborador(colaborador);
		escala.setEscala(esc);
		escala.setData(gData);

		NeoUser user = PortalUtil.getCurrentUser();
		NeoPaper papelOperador = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "[QLP] Operador do Quadro de Lotação Presença"));
		String txtExcluir = "";

		Boolean isPapelOperador = Boolean.FALSE;

		if (user != null && user.getPapers() != null && user.getPapers().contains(papelOperador))
		{
		    isPapelOperador = Boolean.TRUE;
		}

		if (user != null && (user.isAdm() || isPapelOperador))
		{
		    // Removido opção para exclusão de escala
		    txtExcluir = "<a title='Excluir histórico de escala' style='cursor:pointer' onclick='javascript:deleteHistoricoEscala(" + colaborador.getNumeroEmpresa() + "," + colaborador.getTipoColaborador() + "," + colaborador.getNumeroCadastro() + "," + colaborador.getCpf() + ",\"" + NeoUtils.safeDateFormat(gData, "dd/MM/yyyy") + "\")'><img src='imagens/custom/delete.png'></a>";
		}

		escala.setExcluir(txtExcluir);

		escalas.add(escala);
	    }

	    rsEscala.close();
	    stEscala.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}
	return escalas;
    }

    public static List<String> listaCompetencias(Long numcpf)
    {
	List<String> competencias = new ArrayList<String>();
	String competencia = "";

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    StringBuffer sqlAcesso = new StringBuffer();
	    /*sqlAcesso.append(" SELECT max(cal.perref) as perref FROM R070ACC acc ");
			sqlAcesso.append(" INNER JOIN R038HCH hch ON hch.NumCra = acc.NumCra ");
			sqlAcesso.append(" INNER JOIN R034FUN fun ON fun.NumEmp = hch.NumEmp AND fun.NumCad = hch.NumCad AND fun.TipCol = hch.TipCol ");
			sqlAcesso.append(" left JOIN dbo.R044CAL cal on cal.NumEmp = fun.numemp and tipcal = 11 and cal.perref = acc.DatAcc ");
			sqlAcesso.append(" WHERE fun.NumCpf = ? AND acc.tipacc IN (100,101,102,103,104) AND acc.CodPlt = 2 AND acc.ExcPon = 'N' ");
			sqlAcesso.append(" GROUP BY cal.perref ");
			sqlAcesso.append(" ORDER BY cal.perref desc ");*/

	    sqlAcesso.append(" select max(cal.perref) as perref from dbo.R044CAL cal where tipcal = 11 and inicmp <= getdate() ");

	    PreparedStatement stAcesso = connVetorh.prepareStatement(sqlAcesso.toString());
	    //stAcesso.setLong(1, numcpf);
	    ResultSet rsAcesso = stAcesso.executeQuery();

	    //Acessos
	    if (rsAcesso.next())
	    {
		Timestamp dataAcesso = rsAcesso.getTimestamp("perref");
		GregorianCalendar gData = new GregorianCalendar();
		gData.setTimeInMillis(dataAcesso.getTime());

		competencia = NeoUtils.safeDateFormat(gData ,"MM/yyyy"); 

		if (competencias == null || competencias.isEmpty() || !competencias.contains(competencia))
		{
		    competencias.add(competencia);
		}

		for (int i = 1; i <= 5; i++){
		    gData.add(GregorianCalendar.MONTH, -1);
		    competencia = NeoUtils.safeDateFormat(gData ,"MM/yyyy");
		    competencias.add(competencia);
		}
	    }

	    rsAcesso.close();
	    stAcesso.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}

	return competencias;
    }

    public static List<String> listaCompetenciasApuracao(Long numemp, Long tipcol, Long numcad)
    {
	List<String> competencias = new ArrayList<String>();
	String competencia = "";

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    StringBuffer sqlAcesso = new StringBuffer();
	    /*sqlAcesso.append(" SELECT apu.DatApu FROM R066APU apu ");
			sqlAcesso.append(" INNER JOIN R034FUN fun ON fun.NumEmp = apu.NumEmp AND fun.NumCad = apu.NumCad AND fun.TipCol = apu.TipCol ");
			sqlAcesso.append(" WHERE fun.NumEmp = ? AND fun.TipCol = ? AND fun.NumCad = ? AND fun.ConRho = 2 ");
			sqlAcesso.append(" GROUP BY apu.DatApu ");
			sqlAcesso.append(" ORDER BY apu.DatApu desc ");*/

	    sqlAcesso.append(" select max(cal.perref) as perref from dbo.R044CAL cal where tipcal = 11 and inicmp <= getdate() ");


	    PreparedStatement stAcesso = connVetorh.prepareStatement(sqlAcesso.toString());
	    /*stAcesso.setLong(1, numemp);
			stAcesso.setLong(2, tipcol);
			stAcesso.setLong(3, numcad);*/
	    ResultSet rsAcesso = stAcesso.executeQuery();

	    //Acessos
	    /*while (rsAcesso.next())
			{
				Timestamp dataAcesso = rsAcesso.getTimestamp("DatApu");
				GregorianCalendar gData = new GregorianCalendar();
				gData.setTimeInMillis(dataAcesso.getTime());

				competencia = OrsegupsUtils.getCompetencia(gData);

				if (competencias == null || competencias.isEmpty() || !competencias.contains(competencia))
				{
					competencias.add(competencia);
				}
			}*/

	    if (rsAcesso.next())
	    {
		Timestamp dataAcesso = rsAcesso.getTimestamp("perref");
		GregorianCalendar gData = new GregorianCalendar();
		gData.setTimeInMillis(dataAcesso.getTime());

		competencia = NeoUtils.safeDateFormat(gData ,"MM/yyyy"); 

		if (competencias == null || competencias.isEmpty() || !competencias.contains(competencia))
		{
		    competencias.add(competencia);
		}

		for (int i = 1; i <= 5; i++){
		    gData.add(GregorianCalendar.MONTH, -1);
		    competencia = NeoUtils.safeDateFormat(gData ,"MM/yyyy");
		    competencias.add(competencia);
		}
	    }

	    rsAcesso.close();
	    stAcesso.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}

	return competencias;
    }

    public static List<ApuracaoVO> listaApuracoes(Long numemp, Long tipcol, Long numcad, GregorianCalendar dataInicio, GregorianCalendar dataFim)
    {
	List<ApuracaoVO> apuracoes = new ArrayList<ApuracaoVO>();
	ApuracaoVO apuracao = new ApuracaoVO();
	String dataAnt = "";
	Long codsit = 0L;

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    StringBuffer sqlApuracao = new StringBuffer();
	    sqlApuracao.append(" SELECT apu.NumEmp, apu.TipCol, apu.NumCad, apu.DatApu, COUNT(sto.CodSit) AS excecao, esc.CodEsc, esc.NomEsc, apu.CodTma, hor.CodHor, hor.DesHor ");
	    sqlApuracao.append(" FROM R066APU apu ");
	    sqlApuracao.append(" INNER JOIN R034FUN fun ON fun.NumEmp = apu.NumEmp AND fun.TipCol = apu.TipCol AND fun.NumCad = apu.NumCad ");
	    sqlApuracao.append(" LEFT JOIN R004HOR hor ON hor.CodHor = apu.HorDat ");
	    sqlApuracao.append(" LEFT JOIN R066SIT sit ON sit.NumEmp = apu.NumEmp AND sit.TipCol = apu.TipCol AND sit.NumCad = apu.NumCad AND sit.DatApu = apu.DatApu AND (sit.usu_excval is null OR sit.usu_excval = 'N')");
	    sqlApuracao.append(" LEFT JOIN R010SIT sto ON sto.CodSit = sit.CodSit AND sto.SitExc = 'S' ");
	    sqlApuracao.append(" LEFT JOIN R006ESC esc ON esc.CodEsc = apu.CodEsc ");
	    sqlApuracao.append(" WHERE apu.NumEmp = ? AND apu.TipCol = ? AND apu.NumCad = ? AND apu.DatApu >= ? AND apu.DatApu <= ? AND fun.ConRho = 2 ");
	    sqlApuracao.append(" GROUP BY apu.NumEmp, apu.TipCol, apu.NumCad, apu.DatApu, esc.CodEsc, esc.NomEsc, apu.CodTma, hor.CodHor, hor.DesHor ");
	    sqlApuracao.append(" ORDER BY apu.DatApu ");

	    PreparedStatement stApuracao = connVetorh.prepareStatement(sqlApuracao.toString());
	    stApuracao.setLong(1, numemp);
	    stApuracao.setLong(2, tipcol);
	    stApuracao.setLong(3, numcad);
	    stApuracao.setTimestamp(4, new Timestamp(dataInicio.getTimeInMillis()));
	    stApuracao.setTimestamp(5, new Timestamp(dataFim.getTimeInMillis()));

	    //System.out.println(NeoUtils.safeDateFormat(dataInicio) + " - " + NeoUtils.safeDateFormat(dataFim));

	    ResultSet rsApuracao = stApuracao.executeQuery();

	    //Apuracaos
	    while (rsApuracao.next())
	    {

		apuracao = new ApuracaoVO();

		Timestamp dataApuracao = rsApuracao.getTimestamp("DatApu");
		GregorianCalendar gData = new GregorianCalendar();
		gData.setTimeInMillis(dataApuracao.getTime());

		ColaboradorVO colaborador = new ColaboradorVO();

		colaborador.setNumeroEmpresa(rsApuracao.getLong("NumEmp"));
		colaborador.setTipoColaborador(rsApuracao.getLong("Tipcol"));
		colaborador.setNumeroCadastro(rsApuracao.getLong("NumCad"));

		apuracao.setColaborador(colaborador);
		apuracao.setData(gData);
		apuracao.setDataApuracao(NeoUtils.safeDateFormat(gData, "dd/MM/yyyy"));
		apuracao.setDiaApuracao(OrsegupsUtils.getDayOfWeek(gData));
		apuracao.setQdeExcecao(rsApuracao.getLong("excecao"));
		apuracao.setEscala(rsApuracao.getString("CodEsc") + " - " + rsApuracao.getString("NomEsc") + "/" + rsApuracao.getString("CodTma"));
		apuracao.setHorario(rsApuracao.getString("CodHor") + " - " + rsApuracao.getString("DesHor"));

		List<AcessoVO> acessos = listaAcessosApuracao(numemp, tipcol, numcad, gData, apuracao);
		List<SituacaoAcessoVO> situacoes = listaSituacoes(numemp, tipcol, numcad, gData, apuracao);

		StringBuilder ocorrencias = new StringBuilder();

		for (AcessoVO acesso : acessos)
		{
		    ocorrencias.append(acesso.getHoraAcesso());
		    ocorrencias.append(" ");
		}

		if (situacoes != null && !situacoes.isEmpty())
		{
		    // ultimo item
		    ocorrencias.append(situacoes.get(situacoes.size() - 1).getDescricaoSituacao());
		}

		if (ocorrencias == null || ocorrencias.toString().equals(""))
		{
		    ocorrencias.append(rsApuracao.getString("DesHor"));
		}

		apuracao.setAcessos(acessos);
		apuracao.setSituacoes(situacoes);
		apuracao.setOcorrenciaApuracao(ocorrencias.toString());

		apuracoes.add(apuracao);
	    }

	    rsApuracao.close();
	    stApuracao.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}

	return apuracoes;
    }

    public static List<AcessoVO> listaAcessos(Long numcpf, GregorianCalendar perref, GregorianCalendar dataInicio, GregorianCalendar dataFim, String exibirExcecao)
    {
	List<AcessoVO> acessos = new ArrayList<AcessoVO>();
	AcessoVO acesso = new AcessoVO();

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    String tipacc = "AND ( acc.tipacc in ";
	    if (NeoUtils.safeIsNotNull(exibirExcecao) && exibirExcecao.equals("Sim"))
	    {
		//tipacc = tipacc + "(100,101,102,103,104)  or (acc.codplt = 0 and oriacc = 'R') )";
		tipacc = " and ( (acc.CodPlt = 2 and acc.TipAcc >= 100)  or  acc.oriacc = 'R'	  )  ";
	    }
	    else
	    {
		tipacc = tipacc + "(100)) and acc.CodPlt = 2 ";
	    }

	    StringBuffer sqlAcesso = new StringBuffer();
	    /*sqlAcesso.append(" SELECT acc.numcra, acc.numcad, fun.numcpf, DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) as data,acc.horacc,acc.seqacc,acc.DirAcc,acc.USU_TipDir, ");
			sqlAcesso.append(" 		  acc.tipacc, tac.DesTac, rlg.DesRlg, acc.usu_numloc, acc.usu_taborg, acc.usu_peresc, acc.usu_fonacc, jma.DesJma, acc.usu_obsjma ");
			sqlAcesso.append(" FROM R070ACC acc ");
			sqlAcesso.append(" INNER JOIN R038HCH hch ON hch.NumCra = acc.NumCra ");
			sqlAcesso.append(" INNER JOIN R034FUN fun ON fun.NumEmp = hch.NumEmp AND fun.NumCad = hch.NumCad AND fun.TipCol = hch.TipCol ");
			sqlAcesso.append(" INNER JOIN R070TAC tac ON tac.TipAcc = acc.TipAcc ");
			sqlAcesso.append(" INNER JOIN R058RLG rlg ON rlg.CodRlg = acc.CodRlg AND rlg.CodPlt = acc.CodPlt ");
			sqlAcesso.append(" LEFT JOIN R076JMA jma ON jma.CodJma = acc.USU_CodJma");
			sqlAcesso.append(" WHERE fun.NumCpf = ? " + tipacc + " AND acc.CodPlt = 2 AND acc.ExcPon = 'N' AND acc.DatAcc >= ? AND acc.DatAcc <= ? ");
			sqlAcesso.append(" GROUP BY acc.numcra, acc.numcad, fun.numcpf, acc.DatAcc, acc.HorAcc, acc.seqacc, acc.DirAcc, acc.USU_TipDir, acc.tipacc, ");
			sqlAcesso.append(" 			tac.DesTac, rlg.DesRlg, acc.usu_numloc, acc.usu_taborg, acc.usu_peresc, acc.usu_fonacc, jma.DesJma, acc.usu_obsjma ");
			sqlAcesso.append(" ORDER BY DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) desc ");*/

	    sqlAcesso.append(" SELECT acc.numcra, acc.numcad, fun.numcpf, DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) as data,acc.horacc,acc.seqacc,acc.DirAcc,acc.USU_TipDir, ");
	    sqlAcesso.append(" 		  acc.tipacc, tac.DesTac, rlg.DesRlg, acc.usu_numloc, acc.usu_taborg, acc.usu_peresc, acc.usu_fonacc, jma.DesJma, acc.usu_obsjma ");
	    sqlAcesso.append(" FROM R070ACC acc ");
	    sqlAcesso.append(" INNER JOIN R038HCH hch ON hch.NumCra = acc.NumCra ");
	    sqlAcesso.append(" INNER JOIN R034FUN fun ON fun.NumEmp = hch.NumEmp AND fun.NumCad = hch.NumCad AND fun.TipCol = hch.TipCol ");
	    sqlAcesso.append(" INNER JOIN R070TAC tac ON tac.TipAcc = acc.TipAcc ");
	    //sqlAcesso.append(" left JOIN R058RLG rlg ON rlg.CodRlg = acc.CodRlg AND rlg.CodPlt = acc.CodPlt ");

	    sqlAcesso.append(" inner join dbo.R044CAL cal on cal.NumEmp = fun.numemp and tipcal = 11 and cal.perref = ? ");

	    sqlAcesso.append(" inner JOIN R058RLG rlg ON rlg.CodRlg = (case when acc.CodRlg = 0 then 2 else acc.CodRlg end) AND rlg.CodPlt = (case when acc.CodPlt = 0 then 2 else acc.CodPlt end) ");
	    sqlAcesso.append(" LEFT JOIN R076JMA jma ON jma.CodJma = acc.USU_CodJma");
	    //sqlAcesso.append(" WHERE fun.NumCpf = ? " + tipacc + " AND acc.ExcPon = 'N' AND acc.DatAcc >= ? AND acc.DatAcc <= ? ");
	    sqlAcesso.append(" WHERE fun.NumCpf = ? " + tipacc + "  AND acc.ExcPon = 'N' AND acc.DatAcc >= cal.iniapu AND acc.DatAcc <= cal.fimapu AND acc.UsoMar = 2 ");
	    sqlAcesso.append(" and (hch.DatFim = '1900-12-31 00:00:00.000' OR hch.DatFim >= ?) ");
	    sqlAcesso.append(" GROUP BY acc.numcra, acc.numcad, fun.numcpf, acc.DatAcc, acc.HorAcc, acc.seqacc, acc.DirAcc, acc.USU_TipDir, acc.tipacc, ");
	    sqlAcesso.append(" 			tac.DesTac, rlg.DesRlg, acc.usu_numloc, acc.usu_taborg, acc.usu_peresc, acc.usu_fonacc, jma.DesJma, acc.usu_obsjma ");
	    sqlAcesso.append(" ORDER BY DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) desc ");

	    /*System.out.println(" SELECT acc.numcra, acc.numcad, fun.numcpf, DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) as data,acc.horacc,acc.seqacc,acc.DirAcc,acc.USU_TipDir, ");
			System.out.println(" 		  acc.tipacc, tac.DesTac, rlg.DesRlg, acc.usu_numloc, acc.usu_taborg, acc.usu_peresc, acc.usu_fonacc, jma.DesJma, acc.usu_obsjma ");
			System.out.println(" FROM R070ACC acc ");
			System.out.println(" INNER JOIN R038HCH hch ON hch.NumCra = acc.NumCra ");
			System.out.println(" INNER JOIN R034FUN fun ON fun.NumEmp = hch.NumEmp AND fun.NumCad = hch.NumCad AND fun.TipCol = hch.TipCol ");
			System.out.println(" INNER JOIN R070TAC tac ON tac.TipAcc = acc.TipAcc ");
			//System.out.println(" left JOIN R058RLG rlg ON rlg.CodRlg = acc.CodRlg AND rlg.CodPlt = acc.CodPlt ");
			System.out.println(" inner JOIN R058RLG rlg ON rlg.CodRlg = (case when acc.CodRlg = 0 then 2 else acc.CodRlg end) AND rlg.CodPlt = (case when acc.CodPlt = 0 then 1 else acc.CodPlt end) ");
			System.out.println(" LEFT JOIN R076JMA jma ON jma.CodJma = acc.USU_CodJma");
			System.out.println(" WHERE fun.NumCpf =  '"+ numcpf +"' " + tipacc + " AND acc.ExcPon = 'N' AND acc.DatAcc >= '"+ContratoUtils.retornaDataFormatoSapiens(dataInicio)+"' AND acc.DatAcc <= '"+ContratoUtils.retornaDataFormatoSapiens(dataFim)+"' ");
			System.out.println(" GROUP BY acc.numcra, acc.numcad, fun.numcpf, acc.DatAcc, acc.HorAcc, acc.seqacc, acc.DirAcc, acc.USU_TipDir, acc.tipacc, ");
			System.out.println(" 			tac.DesTac, rlg.DesRlg, acc.usu_numloc, acc.usu_taborg, acc.usu_peresc, acc.usu_fonacc, jma.DesJma, acc.usu_obsjma ");
			System.out.println(" ORDER BY DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) desc ");*/

	    //System.out.println(sqlAcesso.toString());
	    PreparedStatement stAcesso = connVetorh.prepareStatement(sqlAcesso.toString());

	    //GregorianCalendar gcPerref = new GregorianCalendar();
	    /*dataInicio.set(GregorianCalendar.DATE, 01);
			dataInicio.set(GregorianCalendar.HOUR, 0);
			dataInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
			dataInicio.set(GregorianCalendar.MINUTE, 0);
			dataInicio.set(GregorianCalendar.SECOND, 0);
			dataInicio.set(GregorianCalendar.MILLISECOND, 0);*/


	    stAcesso.setTimestamp(1, new Timestamp( perref.getTimeInMillis() ));
	    stAcesso.setLong(2, numcpf);
	    stAcesso.setTimestamp(3, new Timestamp( perref.getTimeInMillis() ));
	    //stAcesso.setTimestamp(2, new Timestamp(dataInicio.getTimeInMillis()));
	    //stAcesso.setTimestamp(3, new Timestamp(dataFim.getTimeInMillis()));

	    //			System.out.println(NeoUtils.safeDateFormat(dataInicio));
	    //			System.out.println(NeoUtils.safeDateFormat(dataFim));
	    ResultSet rsAcesso = stAcesso.executeQuery();

	    //Acessos
	    while (rsAcesso.next())
	    {
		acesso = new AcessoVO();

		Timestamp dataAcesso = rsAcesso.getTimestamp("data");
		GregorianCalendar gData = new GregorianCalendar();
		gData.setTimeInMillis(dataAcesso.getTime());

		ColaboradorVO colaborador = new ColaboradorVO();
		colaborador.setNumeroCracha(rsAcesso.getString("numcra"));
		colaborador.setCpf(rsAcesso.getLong("numcpf"));

		acesso.setColaborador(colaborador);
		acesso.setData(gData);
		acesso.setDataAcesso(NeoUtils.safeDateFormat(gData));
		acesso.setSequencia(rsAcesso.getLong("seqacc"));
		acesso.setTipoAcesso(rsAcesso.getString("DesTac"));
		acesso.setRelogio(rsAcesso.getString("DesRlg").substring(13));
		acesso.setTipoDirecao(rsAcesso.getString("usu_tipdir"));
		acesso.setCodigoOrganograma(rsAcesso.getLong("usu_taborg"));
		acesso.setNumeroLocal(rsAcesso.getLong("usu_numloc"));
		acesso.setPermanenciaPosto(rsAcesso.getLong("usu_peresc"));
		acesso.setFone(rsAcesso.getString("usu_fonacc"));
		acesso.setFone(rsAcesso.getString("usu_fonacc"));
		acesso.setJustificativaAcesso(rsAcesso.getString("DesJma"));
		acesso.setObsAcesso(rsAcesso.getString("USU_ObsJma"));

		Long numcad = rsAcesso.getLong("numcad");
		String txtExcluir = "";

		NeoUser user = PortalUtil.getCurrentUser();
		NeoPaper papelOperador = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "[QLP] Operador do Quadro de Lotação Presença"));

		Boolean isPapelOperador = Boolean.FALSE;

		if (user != null && user.getPapers() != null && user.getPapers().contains(papelOperador))
		{
		    isPapelOperador = Boolean.TRUE;
		}

		if (user != null && (user.isAdm() || isPapelOperador) && (numcad == null || numcad == 0L))
		{
		    txtExcluir = "<a title='Excluir Acesso' style='cursor:pointer' onclick='javascript:deleteAcesso(" + colaborador.getNumeroCracha() + "," + colaborador.getCpf() + ",\"" + acesso.getDataAcesso() + "\"," + acesso.getSequencia() + ")'><img src='imagens/custom/delete.png'></a>";
		}

		acesso.setExcluir(txtExcluir);

		if (rsAcesso.getString("DirAcc").equals("E"))
		{
		    acesso.setDirecao("Entrada");
		}
		else
		{
		    acesso.setDirecao("Saída");
		}

		if (rsAcesso.getString("USU_TipDir") == null){
		    acesso.setTipoDirecao("Regularizada");
		}
		else if (rsAcesso.getString("USU_TipDir").equals("P"))
		{
		    acesso.setTipoDirecao("Padrão");
		}
		else if (rsAcesso.getString("USU_TipDir").equals("A"))
		{
		    acesso.setTipoDirecao("Autorizada");
		}
		else
		{
		    acesso.setTipoDirecao("Local");
		}

		acessos.add(acesso);
	    }

	    rsAcesso.close();
	    stAcesso.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}

	return acessos;
    }

    public static List<AcessoVO> listaAcessosApuracao(Long numemp, Long tipcol, Long numcad, GregorianCalendar dataApuracao, ApuracaoVO apuracao)
    {
	List<AcessoVO> acessos = new ArrayList<AcessoVO>();
	AcessoVO acesso = new AcessoVO();

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    StringBuffer sqlAcesso = new StringBuffer();
	    sqlAcesso.append(" SELECT acc.numemp, acc.tipcol, acc.numcad, DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) as data, acc.horacc, acc.seqacc, acc.DirAcc, acc.USU_TipDir, jus.CodJMa, jus.ObsJMa,");
	    sqlAcesso.append(" 		  acc.tipacc, tac.DesTac, rlg.DesRlg, acc.usu_peresc, acc.OriAcc FROM R070ACC acc ");
	    sqlAcesso.append(" INNER JOIN R034FUN fun ON fun.NumEmp = acc.NumEmp AND fun.TipCol = acc.TipCol AND fun.NumCad = acc.NumCad ");
	    sqlAcesso.append(" INNER JOIN R070TAC tac ON tac.TipAcc = acc.TipAcc ");
	    sqlAcesso.append(" LEFT JOIN R058RLG rlg ON rlg.CodRlg = acc.CodRlg AND rlg.CodPlt = acc.CodPlt ");
	    sqlAcesso.append(" LEFT JOIN R070JUS jus ON jus.DatAcc = acc.datacc AND jus.HorAcc = acc.horacc AND jus.NumCra = acc.numcra AND jus.SeqAcc = acc.seqacc ");
	    sqlAcesso.append(" WHERE acc.NumEmp = ? AND acc.TipCol = ? AND acc.NumCad = ? AND acc.UsoMar = 2 AND acc.DatApu = ? AND fun.ConRho = 2 ");
	    sqlAcesso.append(" GROUP BY acc.numemp, acc.tipcol, acc.numcad, acc.DatAcc, acc.HorAcc, acc.seqacc, acc.DirAcc, acc.USU_TipDir, tac.DesTac, rlg.DesRlg, acc.tipacc, tac.DesTac, acc.usu_peresc, acc.OriAcc, jus.CodJMa, jus.ObsJMa ");
	    sqlAcesso.append(" ORDER BY DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) ");

	    PreparedStatement stAcesso = connVetorh.prepareStatement(sqlAcesso.toString());
	    stAcesso.setLong(1, numemp);
	    stAcesso.setLong(2, tipcol);
	    stAcesso.setLong(3, numcad);
	    stAcesso.setTimestamp(4, new Timestamp(dataApuracao.getTimeInMillis()));
	    ResultSet rsAcesso = stAcesso.executeQuery();

	    //Acessos
	    while (rsAcesso.next())
	    {
		acesso = new AcessoVO();

		Timestamp dataAcesso = rsAcesso.getTimestamp("data");
		GregorianCalendar gData = new GregorianCalendar();
		gData.setTimeInMillis(dataAcesso.getTime());

		ColaboradorVO colaborador = new ColaboradorVO();
		colaborador.setNumeroEmpresa(rsAcesso.getLong("numemp"));
		colaborador.setTipoColaborador(rsAcesso.getLong("tipcol"));
		colaborador.setNumeroCadastro(rsAcesso.getLong("numcad"));

		acesso.setData(gData);
		acesso.setDataAcesso(NeoUtils.safeDateFormat(gData, "dd/MM/yyyy"));
		acesso.setHoraAcesso(NeoUtils.safeDateFormat(gData, "HH:mm"));
		acesso.setSequencia(rsAcesso.getLong("seqacc"));
		acesso.setTipoAcesso(rsAcesso.getString("DesTac"));
		if (rsAcesso.getString("DesRlg") != null && rsAcesso.getString("DesRlg").length() >= 13){
		    acesso.setRelogio(rsAcesso.getString("DesRlg").substring(13));
		}else{
		    acesso.setRelogio("");
		}
		acesso.setTipoDirecao(rsAcesso.getString("usu_tipdir"));
		acesso.setTipoAcesso(rsAcesso.getString("tipacc"));
		acesso.setPermanenciaPosto(rsAcesso.getLong("usu_peresc"));
		acesso.setOrigem(rsAcesso.getString("OriAcc"));
		acesso.setCodigoJustificativa(rsAcesso.getLong("CodJMa"));
		acesso.setObsJustificativa(rsAcesso.getString("ObsJMa"));

		if (rsAcesso.getString("DirAcc").equals("E"))
		{
		    acesso.setDirecao("Entrada");
		}
		else
		{
		    acesso.setDirecao("Saída");
		}

		if (rsAcesso.getString("USU_TipDir") == null || rsAcesso.getString("USU_TipDir").equals("P"))
		{
		    acesso.setTipoDirecao("Padrão");
		}
		else if (rsAcesso.getString("USU_TipDir").equals("A"))
		{
		    acesso.setTipoDirecao("Autorizada");
		}
		else
		{
		    acesso.setTipoDirecao("Local");
		}

		if (apuracao != null)
		{
		    ApuracaoVO apu = new ApuracaoVO();
		    apu = (ApuracaoVO) BeanUtils.cloneBean(apuracao);
		    acesso.setApuracao(apu);
		}
		acessos.add(acesso);
	    }

	    rsAcesso.close();
	    stAcesso.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}

	return acessos;
    }

    public static List<SituacaoAcessoVO> listaSituacoes(Long numemp, Long tipcol, Long numcad, GregorianCalendar dataApuracao, ApuracaoVO apuracao)
    {
	List<SituacaoAcessoVO> situacoes = new ArrayList<SituacaoAcessoVO>();
	SituacaoAcessoVO situacao = new SituacaoAcessoVO();

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    StringBuffer sqlSituacao = new StringBuffer();
	    sqlSituacao.append(" SELECT sit.QtdHor, sit.CodSit, sto.DesSit, sto.SitExc, sit.USU_ExcVal ");
	    sqlSituacao.append(" FROM R066SIT sit ");
	    sqlSituacao.append(" INNER JOIN R034FUN fun ON fun.NumEmp = sit.NumEmp AND fun.TipCol = sit.TipCol AND fun.NumCad = sit.NumCad ");
	    sqlSituacao.append(" INNER JOIN R010SIT sto ON sto.CodSit = sit.CodSit ");
	    sqlSituacao.append(" WHERE sit.NumEmp = ? AND sit.TipCol = ? AND sit.NumCad = ? AND sit.DatApu = ? AND fun.ConRho = 2 ");
	    sqlSituacao.append(" ORDER BY sit.CodSit ");

	    PreparedStatement stSituacao = connVetorh.prepareStatement(sqlSituacao.toString());
	    stSituacao.setLong(1, numemp);
	    stSituacao.setLong(2, tipcol);
	    stSituacao.setLong(3, numcad);
	    stSituacao.setTimestamp(4, new Timestamp(dataApuracao.getTimeInMillis()));
	    ResultSet rsSituacao = stSituacao.executeQuery();

	    //Situacaos
	    while (rsSituacao.next())
	    {

		situacao = new SituacaoAcessoVO();

		situacao.setCodigoSituacao(rsSituacao.getLong("CodSit"));
		situacao.setDescricaoSituacao(rsSituacao.getString("DesSit"));
		situacao.setQdeHorasSituacao(rsSituacao.getLong("QtdHor"));
		situacao.setSituacaoExcecao(rsSituacao.getString("SitExc"));
		situacao.setHorasSituacao(OrsegupsUtils.getHorario(rsSituacao.getLong("QtdHor")));
		String excecaoValida = rsSituacao.getString("USU_ExcVal");

		if (apuracao != null)
		{
		    ApuracaoVO apu = new ApuracaoVO();
		    apu = (ApuracaoVO) BeanUtils.cloneBean(apuracao);
		    situacao.setApuracao(apu);
		}

		String link = "";
		if (situacao.getSituacaoExcecao() != null && situacao.getSituacaoExcecao().equals("S") && situacao.getCodigoSituacao() != 999L)
		{
		    if (excecaoValida != null && excecaoValida.equals("S"))
		    {
			link = "<a title='Retornar Exceção' style='cursor:pointer' onclick='javascript:retornaExcecao(" + numemp + "," + tipcol + "," + numcad + "," + situacao.getCodigoSituacao() + ",\"" + NeoUtils.safeDateFormat(dataApuracao, "dd/MM/yyyy") + "\")'>" + "<img src='custom/jsp/orsegups/jms/themes/icons/undo.png' />" + "</a>";
		    }
		    else
		    {
			link = "<a title='Validar Exceção' style='cursor:pointer' onclick='javascript:validaExcecao(" + numemp + "," + tipcol + "," + numcad + "," + situacao.getCodigoSituacao() + ",\"" + NeoUtils.safeDateFormat(dataApuracao, "dd/MM/yyyy") + "\")'>" + "<img src='custom/jsp/orsegups/jms/themes/icons/ok.png' />" + "</a>";
		    }
		    situacao.setLinkIntrajornada(link);
		}
		else if (situacao.getCodigoSituacao() == 15L)
		{
		    link = "<a title='Retornar Exceção' style='cursor:pointer' onclick='javascript:retornaExcecao(" + numemp + "," + tipcol + "," + numcad + "," + situacao.getCodigoSituacao() + ",\"" + NeoUtils.safeDateFormat(dataApuracao, "dd/MM/yyyy") + "\")'>" + "<img src='custom/jsp/orsegups/jms/themes/icons/undo.png' />" + "</a>";
		}
		switch (situacao.getCodigoSituacao().intValue())
		{
		//Extra Intrajornada
		case 311:
		case 312:

		    if (possuiHoraExtra(numemp, tipcol, numcad, dataApuracao))
		    {
			link = "<a title='Inserir Programação de Hora extra' style='cursor:pointer' onclick='javascript:removeHE(" + numemp + "," + tipcol + "," + numcad + ",\"" + NeoUtils.safeDateFormat(dataApuracao, "dd/MM/yyyy") + "\")'>" + "<img src='imagens/custom/del_clock.png' />" + "</a>";
			situacao.setLinkIntrajornada(link);
		    }
		    break;

		    //Extra Intrajornada Indevida
		case 313:
		case 314:
		    link = "<a title='Inserir Programação de Hora extra' style='cursor:pointer' onclick='javascript:insereHE(" + numemp + "," + tipcol + "," + numcad + ",\"" + NeoUtils.safeDateFormat(dataApuracao, "dd/MM/yyyy") + "\")'>" + "<img src='imagens/custom/add_clock_16x16.png' />" + "</a>";
		    situacao.setLinkIntrajornada(link);
		    break;

		default:
		    break;
		}
		situacoes.add(situacao);
	    }

	    rsSituacao.close();
	    stSituacao.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}

	return situacoes;
    }

    public static List<SituacoesApuracaoVO> listaSituacoesApuracao(Long numemp, Long tipcol, Long numcad, GregorianCalendar dataApuracao)
    {
	List<SituacoesApuracaoVO> situacoes = new ArrayList<SituacoesApuracaoVO>();
	SituacoesApuracaoVO situacao = new SituacoesApuracaoVO();

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    StringBuffer sqlSituacao = new StringBuffer();
	    sqlSituacao.append(" SELECT apu.NumEmp, apu.TipCol, apu.NumCad, apu.DatApu, sit.QtdHor, sit.CodSit, sto.DesSit ");
	    sqlSituacao.append(" FROM R066APU apu ");
	    sqlSituacao.append(" INNER JOIN R034FUN fun ON fun.NumEmp = apu.NumEmp AND fun.TipCol = apu.TipCol AND fun.NumCad = apu.NumCad ");
	    sqlSituacao.append(" LEFT JOIN R066SIT sit ON sit.NumEmp = apu.NumEmp AND sit.TipCol = apu.TipCol AND sit.NumCad = apu.NumCad AND sit.DatApu = apu.DatApu ");
	    sqlSituacao.append(" LEFT JOIN R010SIT sto ON sto.CodSit = sit.CodSit ");
	    sqlSituacao.append(" WHERE apu.NumEmp = ? AND apu.TipCol = ? AND apu.NumCad = ? AND apu.DatApu = ? AND fun.ConRho = 2");
	    sqlSituacao.append(" ORDER BY sit.CodSit ");

	    PreparedStatement stSituacao = connVetorh.prepareStatement(sqlSituacao.toString());
	    stSituacao.setLong(1, numemp);
	    stSituacao.setLong(2, tipcol);
	    stSituacao.setLong(3, numcad);
	    stSituacao.setTimestamp(4, new Timestamp(dataApuracao.getTimeInMillis()));
	    ResultSet rsSituacao = stSituacao.executeQuery();

	    //Situacaos
	    while (rsSituacao.next())
	    {

		situacao = new SituacoesApuracaoVO();

		Timestamp data = rsSituacao.getTimestamp("DatApu");
		GregorianCalendar gData = new GregorianCalendar();
		gData.setTimeInMillis(data.getTime());

		ColaboradorVO colaborador = new ColaboradorVO();

		colaborador.setNumeroEmpresa(rsSituacao.getLong("NumEmp"));
		colaborador.setTipoColaborador(rsSituacao.getLong("Tipcol"));
		colaborador.setNumeroCadastro(rsSituacao.getLong("NumCad"));

		ApuracaoVO apuracao = new ApuracaoVO();
		apuracao.setColaborador(colaborador);
		apuracao.setData(gData);

		situacao.setApuracao(apuracao);
		situacao.setCodigoSituacao(rsSituacao.getLong("CodSit"));
		situacao.setDescricaoSituacao(rsSituacao.getString("DesSit"));
		situacao.setQdeHoras(rsSituacao.getLong("QtdHor"));

		situacoes.add(situacao);
	    }

	    rsSituacao.close();
	    stSituacao.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}

	return situacoes;
    }

    public static List<String> listaCompetenciasFinacas(Long numcpf)
    {
	List<String> competencias = new ArrayList<String>();
	String competencia = "";

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    StringBuffer sqlChq = new StringBuffer();
	    sqlChq.append(" Select chq.usu_perref From USU_T240CHQ chq ");
	    sqlChq.append(" Inner Join R034FUN fun On fun.numemp = chq.usu_numemp And fun.tipcol = chq.usu_tipcol And fun.numcad = chq.usu_numcad ");
	    sqlChq.append(" Where fun.numcpf = ? ");
	    sqlChq.append(" Order By chq.usu_perref Desc ");

	    PreparedStatement stChq = connVetorh.prepareStatement(sqlChq.toString());
	    stChq.setLong(1, numcpf);
	    ResultSet rsChq = stChq.executeQuery();

	    //Situacaos
	    while (rsChq.next())
	    {
		Timestamp perRef = rsChq.getTimestamp("usu_perref");
		GregorianCalendar gData = new GregorianCalendar();
		gData.setTimeInMillis(perRef.getTime());

		competencia = NeoUtils.safeDateFormat(gData, "MM/yyyy");

		if (competencias == null || competencias.isEmpty() || !competencias.contains(competencia))
		{
		    competencias.add(competencia);
		}
	    }

	    rsChq.close();
	    stChq.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}
	return competencias;
    }

    public static ContraChequeVO buscaContraCheque(Long numcpf, GregorianCalendar competencia)
    {
	Connection connVetorh = PersistEngine.getConnection("VETORH");
	ContraChequeVO contraCheque = new ContraChequeVO();


	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();
	Long numEmp = null;
	Long numCad = null;
	try {
	    conn = PersistEngine.getConnection("VETORH");

	    sql.append(" select hfi.numemp, hfi.tipcol, hfi.numcad, hfi.DatAlt From r034fun fun ");
	    sql.append("            inner join R038HFI hfi on hfi.NumEmp = fun.numemp ");
	    sql.append("   and hfi.TipCol = fun.tipcol ");
	    sql.append("                      and hfi.NumCad = fun.numcad ");
	    sql.append(" where fun.numcpf = ? ");
	    sql.append("  and hfi.NumEmp <> hfi.EmpAtu ");
	    sql.append("  and hfi.NumCad <> hfi.CadAtu ");
	    //		    sql.append(" UNION ");
	    //		    sql.append(" select hfi.numemp, hfi.tipcol, hfi.numcad, hfi.DatAlt+1 From r034fun fun ");
	    //		    sql.append(" inner join R038HFI hfi on hfi.NumEmp = fun.numemp ");
	    //		    sql.append("                      and hfi.TipCol = fun.tipcol ");
	    //		    sql.append("                      and hfi.NumCad = fun.numcad ");
	    //		    sql.append(" where fun.numcpf = ? and sitafa = 1  ");
	    sql.append(" order by hfi.DatAlt ");

	    pstm = conn.prepareStatement(sql.toString());
	    pstm.setLong(1, numcpf);
	    rs = pstm.executeQuery();

	    while (rs.next()) {
		if (numEmp == null && numCad == null){
		    Timestamp datAlt = rs.getTimestamp("DatAlt");
		    GregorianCalendar gDatAlt = new GregorianCalendar();
		    gDatAlt.setTimeInMillis(datAlt.getTime());
		    if (competencia.before(gDatAlt)){
			numEmp = rs.getLong("numemp");
			numCad = rs.getLong("numcad");
		    }
		}
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

	//		if (numCad == null && numEmp == null){
	//		    conn = null;
	//		    pstm = null;
	//		    rs = null;
	//
	//		    sql = new StringBuilder();
	//
	//		    
	//		    try {
	//			conn = PersistEngine.getConnection("VETORH");
	//			sql.append("select numcad, numemp from r034fun where numcpf = ? ");
	//
	//			pstm = conn.prepareStatement(sql.toString());
	//
	//			pstm.setLong(1, numcpf);
	//			rs = pstm.executeQuery();
	//
	//			while (rs.next()) {
	//			    numEmp = rs.getLong("numemp");
	//			    numCad = rs.getLong("numcad");
	//			}
	//
	//		    } catch (SQLException e) {
	//			e.printStackTrace();
	//		    } finally {
	//			OrsegupsUtils.closeConnection(conn, pstm, rs);
	//		    }
	//		}


	try
	{

	    StringBuffer sqlChq = new StringBuffer();
	    /*sqlChq.append(" Select chq.usu_perref, chq.usu_titcar, chq.usu_datadm, chq.usu_txtban, chq.usu_txtage, chq.usu_txtcon, chq.usu_salbas, chq.usu_basfgt, ");
			sqlChq.append(" 	   chq.usu_salins, chq.usu_basirf, chq.usu_fgtmes, chq.usu_totvct, chq.usu_totdsc, chq.usu_vlrliq, chq.usu_msgchq, ");
			sqlChq.append(" 	   fun.numemp, fun.tipcol, fun.numcad, emp.apeemp ");
			sqlChq.append(" From USU_T240CHQ chq ");
			sqlChq.append(" Inner Join R034FUN fun On fun.numemp = chq.usu_numemp And fun.tipcol = chq.usu_tipcol And fun.numcad = chq.usu_numcad ");
			sqlChq.append(" Inner Join R030EMP emp On emp.numemp = fun.numemp ");
			sqlChq.append(" Where fun.numcpf = ? And chq.usu_perref = ? ");*/

	    sqlChq.append(" Select evc.TipEve, ver.codeve, ver.refeve, ver.valeve, ver.OriEve, evc.DesEve, cal.perref, fun.datadm, fun.numemp, fun.tipcol, fun.numcad, emp.apeemp, car.TitCar, fun.codban, fun.codage, fun.conban ");
	    sqlChq.append(" ,( case when evc.tipeve in (1,2,5) then 'P' else 'D' end ) tipoLancamento, emp.apeemp, hsa.ValSal ");
	    sqlChq.append(" From R046VER ver "); 
	    sqlChq.append(" Inner Join R034FUN fun On fun.numemp = ver.numemp And fun.tipcol = ver.tipcol And fun.numcad = ver.numcad "); 
	    sqlChq.append(" inner join R038HCA hca WITH (NOLOCK) on hca.NumEmp = fun.NumEmp AND hca.TipCol = fun.TipCol AND hca.NumCad = fun.NumCad "); 
	    sqlChq.append("                                                                         AND hca.DatAlt = (SELECT MAX (DATALT) "); 
	    sqlChq.append("                                                                                                 FROM R038HCA TABELA001 "); 
	    sqlChq.append("                                                                                                 WHERE TABELA001.NUMEMP = hca.NUMEMP AND TABELA001.TIPCOL = hca.TIPCOL AND TABELA001.NUMCAD = hca.NUMCAD AND TABELA001.DATALT <= DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())) "); 
	    sqlChq.append("                                                                                          ) ");
	    sqlChq.append(" INNER JOIN R024CAR car WITH (NOLOCK) ON car.EstCar = hca.EstCar AND car.CodCar = hca.CodCar ");   
	    sqlChq.append(" Inner Join R030EMP emp On emp.numemp = fun.numemp "); 
	    sqlChq.append(" inner join r008evc evc on ver.codeve = evc.codeve and emp.tabeve = evc.codtab and (evc.TipEve <> 4 or ver.codeve = 300 ) and evc.tipeve <> 5");
	    sqlChq.append(" inner join dbo.R044CAL cal on cal.NumEmp = fun.numemp and tipcal = 11 and cal.perref = ? and ver.CodCal = cal.CodCal ");
	    sqlChq.append(" inner join r038hsa hsa on (hsa.numcad= fun.numcad and hsa.numemp = fun.numemp and hsa.TipCol = fun.tipcol) ");
	    sqlChq.append(" Where 1=1 ");
	    if (numCad != null && numEmp != null){
		sqlChq.append(" and fun.numemp = ? and fun.numcad = ? ");
	    }else{
		sqlChq.append(" and fun.numcpf = ? ");
	    }
	    sqlChq.append("and ver.codeve not in (472) and evc.TipEve not in(5) "); // 472 -> Taxa 0,8% Sindicato. Pelo que ví no presença original não aparece esse valor nos calculos
	    sqlChq.append(" and hsa.DatAlt = (  ");
	    sqlChq.append("            			Select max(hsa1.datalt) from r038hsa hsa1 where hsa1.NumEmp = hsa.numemp and hsa1.TipCol = hsa.TipCol and hsa1.NumCad = hsa.NumCad "); 
	    sqlChq.append("          		 )  ");
	    sqlChq.append(" and hsa.SeqAlt = (  "); 
	    sqlChq.append(" 					Select max(hsa1.SeqAlt) from r038hsa hsa1 where hsa1.NumEmp = hsa.numemp and hsa1.TipCol = hsa.TipCol and hsa1.NumCad = hsa.NumCad and hsa1.DatAlt = hsa.DatAlt "); 
	    sqlChq.append("   				 )  ");
	    sqlChq.append(" order by ver.codeve ");

	    PreparedStatement stChq = connVetorh.prepareStatement(sqlChq.toString());
	    stChq.setTimestamp(1, new Timestamp(competencia.getTimeInMillis()));
	    if (numCad != null && numEmp != null){
		stChq.setLong(2, numEmp);
		stChq.setLong(3, numCad);
	    }else{
		stChq.setLong(2, numcpf);
	    }
	    ResultSet rsChq = stChq.executeQuery();

	    //Situacaos



	    if (rsChq.next()){

		Timestamp perRef = rsChq.getTimestamp("perref");
		GregorianCalendar gPerRef = new GregorianCalendar();
		gPerRef.setTimeInMillis(perRef.getTime());

		Timestamp datAdm = rsChq.getTimestamp("datadm");
		GregorianCalendar gDatAdm = new GregorianCalendar();
		gDatAdm.setTimeInMillis(datAdm.getTime());

		contraCheque.setPerRef(NeoUtils.safeDateFormat(perRef, "MM/yyyy"));
		contraCheque.setTitCar(rsChq.getString("TitCar"));
		contraCheque.setDatAdm(NeoUtils.safeDateFormat(datAdm, "dd/MM/yyyy"));
		contraCheque.setTxtBan(rsChq.getString("codban"));
		contraCheque.setTxtAge(rsChq.getString("codage"));
		contraCheque.setTxtCon(rsChq.getString("conban"));

		ColaboradorVO colaborador = new ColaboradorVO();
		colaborador.setNumeroEmpresa(rsChq.getLong("numemp"));
		colaborador.setTipoColaborador(rsChq.getLong("tipcol"));
		colaborador.setNumeroCadastro(rsChq.getLong("numcad"));
		colaborador.setNomeEmpresa(rsChq.getString("apeemp"));
		contraCheque.setColaborador(colaborador);

		List<EventosColaboradorVO> listaEventos = new ArrayList<EventosColaboradorVO>();
		BigDecimal proventosTotal = new BigDecimal(0);
		BigDecimal descontosTotal = new BigDecimal(0);
		BigDecimal trocoMes = new BigDecimal(0);
		do{
		    //System.out.println(rsChq.getBigDecimal("codeve") + " - " + rsChq.getBigDecimal("valeve"));
		    switch (rsChq.getInt("codeve")) {
		    case 1: // salario base
			proventosTotal = proventosTotal.add(rsChq.getBigDecimal("valeve"));
			break;
		    case 300: // FGTS Mês
			contraCheque.setFgtMes(NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), rsChq.getBigDecimal("valeve")));
			//descontosTotal = descontosTotal.add(rsChq.getBigDecimal("valeve"));
			break;
		    case 163: // troco, não soma.
			trocoMes = trocoMes.add(rsChq.getBigDecimal("valeve"));
			break;
		    case 327: // troco, não soma.
			trocoMes = trocoMes.add(rsChq.getBigDecimal("valeve"));
			break;

		    default:
			if (rsChq.getString("tipoLancamento").equals("P")){
			    proventosTotal = proventosTotal.add(rsChq.getBigDecimal("valeve"));
			}else{
			    //Eventos do tipo 6 é somente transitório, não realiza desconto.
			    if(rsChq.getLong("tipeve") != 6){
				descontosTotal = descontosTotal.add(rsChq.getBigDecimal("valeve"));
			    }
			}
			break;
		    }
		    EventosColaboradorVO evento = new EventosColaboradorVO();
		    evento.setCodEve(rsChq.getString("codeve"));
		    evento.setDesEve(rsChq.getString("DesEve"));
		    evento.setRefEve(NeoUtils.safeOutputString(rsChq.getString("refeve")).equals("0.00")? "" : rsChq.getString("refeve"));
		    if (rsChq.getString("tipoLancamento").equals("P")){
			evento.setValPro(NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), rsChq.getBigDecimal("valeve")));
		    }else{
			evento.setValDes(NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), rsChq.getBigDecimal("valeve")));
		    }
		    if (rsChq.getInt("codeve") != 300){
			listaEventos.add(evento);
		    }
		}while (rsChq.next());
		contraCheque.setEventosColaborador(listaEventos);
		BigDecimal  salarioBase = getSalarioPorCompetencia(colaborador.getNumeroEmpresa(), colaborador.getNumeroCadastro(), colaborador.getTipoColaborador(), competencia);
		contraCheque.setSalBas(NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), salarioBase));
		OrsegupsUtils.closeConnection(connVetorh, stChq, rsChq);



		contraCheque.setBasFgt(NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), proventosTotal ));
		contraCheque.setSalIns(NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), proventosTotal ));
		contraCheque.setBasIrf(NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), proventosTotal ));

		contraCheque.setTotVct(NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), proventosTotal.add(trocoMes) ));
		contraCheque.setTotDsc(NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), descontosTotal ));
		contraCheque.setVlrLiq(NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), proventosTotal.add(trocoMes).subtract(descontosTotal) ));
		//contraCheque.setMsgChq(rsChq.getString(""));
	    }


	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}

	return contraCheque;
    }
    
    public static BigDecimal getSalarioPorCompetencia(Long numemp, Long numcad, Long tipCol, GregorianCalendar competencia){
	
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	BigDecimal retorno = BigDecimal.ZERO;
	
	try {
	    conn = PersistEngine.getConnection("VETORH");
	    
	    sql.append(" SELECT * FROM R038HSA WHERE NumCad = ? AND NumEmp = ? AND TipCol = ? order by DatAlt desc ");
	    
	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setLong(1, numcad);
	    pstm.setLong(2, numemp);
	    pstm.setLong(3, tipCol);

	    rs = pstm.executeQuery();

	    while (rs.next()) {
		Timestamp datAlt = rs.getTimestamp("DatAlt");
		GregorianCalendar gDatAlt = new GregorianCalendar();
		gDatAlt.setTimeInMillis(datAlt.getTime());
		
		gDatAlt.set(Calendar.DAY_OF_MONTH, 1);
		competencia.set(Calendar.DAY_OF_MONTH, 1);
		
		if (competencia.getTimeInMillis() >= gDatAlt.getTimeInMillis()){
		    retorno = rs.getBigDecimal("ValSal");
		    break;
		}
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
	
	return retorno;
    }

    public static boolean possuiHoraExtra(Long numemp, Long tipcol, Long numcad, GregorianCalendar dataApuracao)
    {
	Connection connVetorh = PersistEngine.getConnection("VETORH");
	boolean possui = false;

	try
	{
	    StringBuffer sqlHE = new StringBuffer();
	    sqlHE.append(" SELECT ext.NumCad FROM R064EXT ext WHERE ext.NumEmp = ? AND ext.TipCol = ? AND ext.NumCad = ?  ");
	    sqlHE.append(" AND ext.DatIni >= ? AND ext.DatFim <= ? AND ext.HorIni = 0 AND ext.HorFim = 0 ");

	    PreparedStatement stHE = connVetorh.prepareStatement(sqlHE.toString());
	    stHE.setLong(1, numemp);
	    stHE.setLong(2, tipcol);
	    stHE.setLong(3, numcad);
	    stHE.setTimestamp(4, new Timestamp(dataApuracao.getTimeInMillis()));
	    stHE.setTimestamp(5, new Timestamp(dataApuracao.getTimeInMillis()));
	    ResultSet rsHE = stHE.executeQuery();

	    if (rsHE.next())
	    {
		possui = true;
	    }

	    rsHE.close();
	    stHE.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}

	return possui;
    }

    public static List<JustificativaMarcacaoVO> listaJustificativaMarcacao(Long codigo)
    {
	List<JustificativaMarcacaoVO> justificativas = new ArrayList<JustificativaMarcacaoVO>();
	JustificativaMarcacaoVO justificativa = new JustificativaMarcacaoVO();

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    StringBuffer sql = new StringBuffer();
	    sql.append(" SELECT jma.CodJma, jma.DesJma ");
	    sql.append(" FROM R076JMA jma ");
	    if (codigo != null && codigo > 0)
	    {
		sql.append(" WHERE jma.CodJma = ? ");
	    }
	    sql.append(" ORDER BY jma.CodJma ");

	    PreparedStatement st = connVetorh.prepareStatement(sql.toString());

	    if (codigo != null && codigo > 0)
	    {
		st.setLong(1, codigo);
	    }

	    ResultSet rs = st.executeQuery();

	    //Situacaos
	    while (rs.next())
	    {

		justificativa = new JustificativaMarcacaoVO();

		justificativa.setCodJma(rs.getLong("CodJma"));
		justificativa.setDesJma(rs.getString("DesJma"));

		justificativas.add(justificativa);
	    }

	    rs.close();
	    st.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}
	return justificativas;
    }

    public static Long contaExcecaoApuracao(Long numemp, Long tipcol, Long numcad, GregorianCalendar dataInicio, GregorianCalendar dataFim)
    {
	Long count = 0L;

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    StringBuffer sqlApuracao = new StringBuffer();
	    sqlApuracao.append(" SELECT apu.NumEmp, apu.TipCol, apu.NumCad, COUNT(sto.CodSit) AS Excecao ");
	    sqlApuracao.append(" FROM R066APU apu ");
	    sqlApuracao.append(" INNER JOIN R034FUN fun ON fun.NumEmp = apu.NumEmp AND fun.TipCol = apu.TipCol AND fun.NumCad = apu.NumCad ");
	    sqlApuracao.append(" LEFT JOIN R004HOR hor ON hor.CodHor = apu.HorDat AND hor.CodHor > 9000  ");
	    sqlApuracao.append(" LEFT JOIN R066SIT sit ON sit.NumEmp = apu.NumEmp AND sit.TipCol = apu.TipCol AND sit.NumCad = apu.NumCad AND sit.DatApu = apu.DatApu ");
	    sqlApuracao.append(" LEFT JOIN R010SIT sto ON sto.CodSit = sit.CodSit AND sto.SitExc = 'S'  ");
	    sqlApuracao.append(" WHERE apu.NumEmp = ? AND apu.TipCol = ? AND apu.NumCad = ? AND apu.DatApu >= ? AND apu.DatApu <= ? AND fun.ConRho = 2 ");
	    sqlApuracao.append(" GROUP BY apu.NumEmp, apu.TipCol, apu.NumCad ");

	    PreparedStatement stApuracao = connVetorh.prepareStatement(sqlApuracao.toString());
	    stApuracao.setLong(1, numemp);
	    stApuracao.setLong(2, tipcol);
	    stApuracao.setLong(3, numcad);
	    stApuracao.setTimestamp(4, new Timestamp(dataInicio.getTimeInMillis()));
	    stApuracao.setTimestamp(5, new Timestamp(dataFim.getTimeInMillis()));
	    ResultSet rsApuracao = stApuracao.executeQuery();

	    //Apuracaos
	    if (rsApuracao.next())
	    {
		count = rsApuracao.getLong("Excecao");
	    }

	    rsApuracao.close();
	    stApuracao.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}

	return count;
    }

    public static PostoVO buscaFichaPosto(Long numeroLocal, Long codigoOrganograma, GregorianCalendar dataAtual)
    {

	Connection connVetorh = PersistEngine.getConnection("VETORH");
	PostoVO posto = new PostoVO();

	try
	{
	    /*** Recupera Postos ***/
	    StringBuffer queryPostos = new StringBuffer();
	    queryPostos.append(" SELECT orn.usu_codclisap, orn.numloc, orn.usu_tipins, orn.usu_cgccpf, hie.codloc, cvs.usu_nomloc, cvs.usu_desser, pos.usu_codser, cvs.usu_numctr, cvs.usu_numpos, orn.usu_lotorn, ");
	    queryPostos.append(" 		cvs.usu_codreg, orn.usu_cliorn, cvs.usu_sitcvs, cvs.usu_qtdcon, cvs.usu_numloc, cvs.usu_codccu, cvs.usu_codemp, cvs.usu_codfil, cvs.usu_endloc, ");
	    queryPostos.append(" 		cid.NomCid, cid.EstCid, cvs.usu_endcep, orn.usu_cplctr,orn.usu_nomcid, cvs.usu_preuni * cvs.usu_qtdcvs AS valor, cvs.usu_datini, cvs.usu_datfim, cvs.usu_fimvig, ");
	    queryPostos.append(" 		cvs.usu_nomloc AS cvs_nomloc, cvs.usu_codccu AS cvs_codccu, cvs.usu_numctr AS cvs_numctr, cvs.usu_numpos AS cvs_numpos, pos.usu_peresc, mot.codmot, mot.desmot, cli.foncli ");
	    queryPostos.append(" 		,orn.usu_possup, orn.usu_issup ");
	    queryPostos.append(" 		,pos.usu_obsmot");
	    queryPostos.append(" FROM R016ORN orn WITH (NOLOCK) ");
	    queryPostos.append(" INNER JOIN USU_T038CVS cvs WITH (NOLOCK) ON cvs.usu_numloc = orn.numloc AND cvs.usu_taborg = orn.taborg AND cvs.usu_seqalt = (Select MAX(c2.usu_seqalt) From USU_T038CVS c2 Where c2.usu_taborg = cvs.usu_taborg And c2.usu_numloc = cvs.usu_numloc And c2.usu_datalt <= ?) ");
	    queryPostos.append(" INNER JOIN R016HIE hie WITH (NOLOCK) ON hie.taborg = cvs.usu_taborg AND cvs.usu_numloc = hie.numloc AND hie.datini = (SELECT MAX(h2.datini) FROM dbo.R016HIE h2 WHERE hie.taborg = cvs.usu_taborg AND h2.numloc = cvs.usu_numloc) ");
	    queryPostos.append(" LEFT JOIN [FSOODB04\\SQL02].SAPIENS.dbo.USU_T160CVS pos ON pos.usu_codemp = cvs.usu_codemp AND pos.usu_codfil = cvs.usu_codfil AND pos.usu_numctr = cvs.usu_numctr AND pos.usu_numpos = cvs.usu_numpos ");
	    queryPostos.append(" LEFT JOIN [FSOODB04\\SQL02].SAPIENS.dbo.USU_T160CTR ctr ON ctr.usu_codemp = cvs.usu_codemp AND ctr.usu_codfil = cvs.usu_codfil AND ctr.usu_numctr = cvs.usu_numctr ");
	    queryPostos.append(" LEFT JOIN [FSOODB04\\SQL02].SAPIENS.dbo.E085CLI cli ON cli.codcli = ctr.usu_codcli ");
	    queryPostos.append(" LEFT JOIN [FSOODB04\\SQL02].SAPIENS.dbo.E021MOT mot ON mot.codmot = pos.usu_codmot ");
	    queryPostos.append(" LEFT JOIN R074CID cid WITH (NOLOCK) ON cid.CodCid = orn.usu_codcid ");
	    queryPostos.append(" WHERE cvs.usu_numloc = ? AND cvs.usu_taborg = ? ");

	    PreparedStatement stPostos = connVetorh.prepareStatement(queryPostos.toString());
	    stPostos.setTimestamp(1, new Timestamp(dataAtual.getTimeInMillis()));
	    stPostos.setLong(2, numeroLocal);
	    stPostos.setLong(3, codigoOrganograma);

	    ResultSet rsPostos = stPostos.executeQuery();

	    while (rsPostos.next())
	    {
		ClienteVO cliente = new ClienteVO();
		cliente.setCodigoCliente(rsPostos.getString("usu_codclisap"));
		cliente.setNomeCliente(rsPostos.getString("usu_cliorn"));
		cliente.setTipoPessoa(rsPostos.getString("usu_tipins"));
		cliente.setTelefone(rsPostos.getString("foncli"));

		String cpfCnpj = "";
		if (cliente.getTipoPessoa().equals("F"))
		{
		    cpfCnpj = String.format("%011d", rsPostos.getLong("usu_cgccpf"));
		    cpfCnpj = OrsegupsUtils.formatarString(cpfCnpj, "###.###.###-##");
		}
		else
		{
		    cpfCnpj = String.format("%014d", rsPostos.getLong("usu_cgccpf"));
		    cpfCnpj = OrsegupsUtils.formatarString(cpfCnpj, "##.###.###/####-##");
		}
		cliente.setCpfCnpj(cpfCnpj);
		posto.setCliente(cliente);

		posto.setNomePosto(rsPostos.getString("usu_nomloc"));
		posto.setCentroCusto(rsPostos.getString("usu_codccu"));
		posto.setServico(rsPostos.getString("usu_desser"));
		posto.setSituacao((rsPostos.getString("usu_sitcvs").equals("S") ? "Ativo" : "Inativo"));
		posto.setVagas(rsPostos.getLong("usu_qtdcon"));
		posto.setNumeroContrato(rsPostos.getLong("usu_numctr"));
		posto.setNumeroPosto(rsPostos.getLong("usu_numpos"));
		posto.setEndereco(rsPostos.getString("usu_endloc"));
		posto.setCep(rsPostos.getString("usu_endcep"));
		posto.setValor(rsPostos.getBigDecimal("valor"));
		posto.setDataInicio(rsPostos.getDate("usu_datini"));
		posto.setDataFim(rsPostos.getDate("usu_datfim"));
		posto.setDataFimVigencia(rsPostos.getDate("usu_fimvig"));
		posto.setTelefone("");
		posto.setNumeroLocal(rsPostos.getLong("numloc"));
		posto.setCodigoLocal(rsPostos.getString("codloc"));
		posto.setCodigoPai(posto.getCodigoLocal().substring(0, 24));
		posto.setCodigoOrganograma(codigoOrganograma);
		posto.setCidade(rsPostos.getString("nomcid"));
		posto.setEstado(rsPostos.getString("estcid"));
		posto.setLotacao(rsPostos.getString("usu_lotorn"));
		posto.setBairro(rsPostos.getString("usu_nomcid"));
		posto.setNumero(rsPostos.getString("usu_cplctr"));

		posto.setNumemp(rsPostos.getLong("usu_codemp"));
		posto.setNumfil(rsPostos.getLong("usu_codfil"));
		posto.setCodSer(rsPostos.getString("usu_codser"));

		//posto.setUsuPossup(rsPostos.getLong("usu_possup"));
		posto.setUsuIssup(rsPostos.getLong("usu_issup"));
		posto.setUsuCodreg(rsPostos.getLong("usu_codreg"));


		if (posto.getSituacao().equalsIgnoreCase("Ativo"))
		{
		    posto.setEscalaPosto(getJornadaEscalaPosto(rsPostos.getLong("usu_codemp"), rsPostos.getLong("usu_codfil"), rsPostos.getLong("usu_numctr"), rsPostos.getLong("usu_numpos"), null, dataAtual, rsPostos.getLong("usu_peresc"), true));

		    GregorianCalendar newDataAtual = (GregorianCalendar) dataAtual.clone();
		    newDataAtual.set(Calendar.HOUR_OF_DAY, 0);
		    newDataAtual.set(Calendar.MINUTE, 0);
		    newDataAtual.set(Calendar.SECOND, 0);
		    newDataAtual.set(Calendar.MILLISECOND, 0);

		    GregorianCalendar newDataFim = new GregorianCalendar();
		    newDataFim.setTime(posto.getDataFim());
		    newDataFim.set(Calendar.HOUR_OF_DAY, 0);
		    newDataFim.set(Calendar.MINUTE, 0);
		    newDataFim.set(Calendar.SECOND, 0);
		    newDataFim.set(Calendar.MILLISECOND, 0);

		    if (newDataFim.after(newDataAtual))
		    {
			posto.setSituacao("Encerrando");
		    }
		}

		GregorianCalendar gDataFim = new GregorianCalendar();
		gDataFim.setTime(posto.getDataFim());

		if (gDataFim.get(Calendar.YEAR) != 1900)
		{
		    posto.setMotivoEncerramento(rsPostos.getString("codmot") + " - " + rsPostos.getString("desmot"));
		    posto.setObsEncerramento(rsPostos.getString("usu_obsmot"));
		}

		posto.setTelefone(getTelefonePosto(posto.getCentroCusto()));
		posto.setStatus(getStatusPosto(posto, dataAtual));
	    }
	    rsPostos.close();
	    stPostos.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}

	return posto;
    }

    public static List<EquipamentoVO> listaEquipamentos(Long numctr, Long numpos)
    {
	List<EquipamentoVO> equipamentos = new ArrayList<EquipamentoVO>();
	Connection connSapiens = PersistEngine.getConnection("SAPIENS");

	try
	{
	    StringBuffer sqlEqp = new StringBuffer();
	    sqlEqp.append(" SELECT eqp.usu_seqeqp, eqp.usu_codpro, pro.DesPro, eqp.usu_tippro, eqp.usu_qtdeqp, ins.usu_nomins, eqp.usu_datins,eqp.usu_reqeqp, eqp.usu_rmceqp FROM USU_T160EQP eqp ");
	    sqlEqp.append(" INNER JOIN E075PRO pro ON pro.CodPro = eqp.usu_codpro AND pro.CodEmp = eqp.usu_codemp ");
	    sqlEqp.append(" INNER JOIN USU_T190INS ins ON ins.usu_codins = eqp.usu_codins ");
	    sqlEqp.append(" WHERE eqp.usu_numctr = ? and eqp.usu_numpos = ?  ");
	    sqlEqp.append(" ORDER BY eqp.usu_seqeqp ");

	    PreparedStatement stEqp = connSapiens.prepareStatement(sqlEqp.toString());
	    stEqp.setLong(1, numctr);
	    stEqp.setLong(2, numpos);
	    ResultSet rsEqp = stEqp.executeQuery();

	    //Cargos
	    while (rsEqp.next())
	    {
		EquipamentoVO eqp = new EquipamentoVO();

		Timestamp data = rsEqp.getTimestamp("usu_datins");
		GregorianCalendar gData = new GregorianCalendar();

		if (NeoUtils.safeIsNotNull(data))
		{
		    gData.setTimeInMillis(data.getTime());

		    if (gData != null && gData.get(Calendar.YEAR) != 1900)
		    {
			eqp.setDataInstalacao(NeoUtils.safeDateFormat(gData, "dd/MM/yyyy"));
		    }
		}

		PostoVO posto = new PostoVO();
		posto.setNumeroContrato(numctr);
		posto.setNumeroContrato(numpos);

		eqp.setPosto(posto);
		eqp.setSequencia(rsEqp.getLong("usu_seqEqp"));
		eqp.setCodigoProduto(rsEqp.getString("usu_codpro"));
		eqp.setDescricaoProduto(rsEqp.getString("despro"));
		eqp.setTipoProduto(rsEqp.getString("usu_tippro"));
		eqp.setQuantidade(rsEqp.getLong("usu_qtdeqp"));
		eqp.setNomeInstalador(rsEqp.getString("usu_nomins"));
		eqp.setRequisicao(rsEqp.getLong("usu_reqeqp"));
		eqp.setRmc(rsEqp.getLong("usu_rmceqp"));

		equipamentos.add(eqp);
	    }

	    rsEqp.close();
	    stEqp.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connSapiens.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}
	return equipamentos;
    }

    public static List<ChecklistVO> listaChecklist(Long numctr, Long numpos)
    {
	List<ChecklistVO> checklists = new ArrayList<ChecklistVO>();
	Connection connSapiens = PersistEngine.getConnection("SAPIENS");

	try
	{
	    StringBuffer sqlChecklist = new StringBuffer();
	    sqlChecklist.append(" SELECT chk.usu_seqchk, chk.usu_coditm, chk.usu_itmchk, chk.usu_qtdchk, chk.usu_datchk, chk.usu_tarfus, chk.usu_rmcchk FROM USU_T160CHK chk  ");
	    sqlChecklist.append(" WHERE chk.usu_numctr = ? and chk.usu_numpos = ? ");
	    sqlChecklist.append(" ORDER BY chk.usu_seqchk ");

	    PreparedStatement stChecklist = connSapiens.prepareStatement(sqlChecklist.toString());
	    stChecklist.setLong(1, numctr);
	    stChecklist.setLong(2, numpos);
	    ResultSet rsChecklist = stChecklist.executeQuery();

	    //Cargos
	    while (rsChecklist.next())
	    {
		ChecklistVO checklist = new ChecklistVO();

		Timestamp data = rsChecklist.getTimestamp("usu_datchk");
		GregorianCalendar gData = new GregorianCalendar();

		if (NeoUtils.safeIsNotNull(data))
		{
		    gData.setTimeInMillis(data.getTime());

		    if (gData != null && gData.get(Calendar.YEAR) != 1900)
		    {
			checklist.setPrazo(NeoUtils.safeDateFormat(gData, "dd/MM/yyyy"));
		    }
		}

		PostoVO posto = new PostoVO();
		posto.setNumeroContrato(numctr);
		posto.setNumeroContrato(numpos);

		Long codItem = rsChecklist.getLong("usu_coditm");
		Long qdeItem = rsChecklist.getLong("usu_qtdchk");
		Long tarefa = rsChecklist.getLong("usu_tarfus");
		String tarefaS = "";

		checklist.setPosto(posto);
		checklist.setSequencia(rsChecklist.getLong("usu_seqchk"));

		if (tarefa != null && tarefa > 0)
		{
		    tarefaS = String.format("%06d", tarefa);
		}

		if (codItem != null && codItem > 0)
		{
		    checklist.setCodigoItem(codItem);
		}

		if (qdeItem != null && qdeItem > 0)
		{
		    checklist.setQuantidade(rsChecklist.getLong("usu_qtdchk"));
		}
		checklist.setItem(rsChecklist.getString("usu_itmchk"));
		checklist.setTarefa(tarefaS);
		checklist.setRmc(rsChecklist.getLong("usu_rmcchk"));

		checklists.add(checklist);
	    }

	    rsChecklist.close();
	    stChecklist.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connSapiens.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}
	return checklists;
    }

    public static List<ContaSigmaVO> listaContasSigma(Long numctr, Long numpos)
    {
	List<ContaSigmaVO> contas = new ArrayList<ContaSigmaVO>();
	Connection connSapiens = PersistEngine.getConnection("SAPIENS");

	try
	{
	    StringBuffer sqlConta = new StringBuffer();
	    sqlConta.append(" Select cen.cd_cliente, cen.id_central, cen.particao, cen.id_empresa, cen.fantasia ");
	    sqlConta.append(" From USU_T160SIG sig ");
	    sqlConta.append(" Inner Join ["+serverInfo.get(OrsegupsConnectionUtils.NOME_SERVIDOR)+"\\"+serverInfo.get(OrsegupsConnectionUtils.INSTANCIA)+"].SIGMA90.dbo.dbCentral cen ON cen.cd_cliente = sig.usu_codcli ");
	    sqlConta.append(" Where sig.usu_numctr = ? And sig.usu_numpos = ? And cen.ctrl_central = 1 ");
	    sqlConta.append(" Group By cen.cd_cliente, cen.id_central, cen.particao, cen.id_empresa, cen.fantasia ");

	    PreparedStatement stConta = connSapiens.prepareStatement(sqlConta.toString());
	    stConta.setLong(1, numctr);
	    stConta.setLong(2, numpos);
	    ResultSet rsConta = stConta.executeQuery();

	    //Cargos
	    while (rsConta.next())
	    {
		ContaSigmaVO conta = new ContaSigmaVO();
		conta.setCdCliente(rsConta.getLong("cd_cliente"));
		conta.setCentral(rsConta.getString("id_central"));
		conta.setParticao(rsConta.getString("particao"));
		conta.setEmpresa(rsConta.getString("id_empresa"));
		conta.setCentralParticao(conta.getCentral() + "[" + conta.getParticao() + "]");
		conta.setFantasia(rsConta.getString("fantasia"));
		conta.setLinkEventos("<a title='Dados do Cliente' style='cursor:pointer' onclick='javascript:showEventosSigma(" + conta.getCdCliente() + ",\"" + conta.getCentralParticao() + "\",\"" + conta.getFantasia() + "\")'><img src='imagens/icones_final/properties_16x16-trans.png'></a>");
		contas.add(conta);
	    }

	    rsConta.close();
	    stConta.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connSapiens.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}
	return contas;
    }

    public static List<ChipGPRSVO> listaChipGPRS(Long numctr, Long numpos)
    {
	List<ChipGPRSVO> chips = new ArrayList<ChipGPRSVO>();
	Connection connSapiens = PersistEngine.getConnection("SAPIENS");

	try
	{
	    StringBuffer sqlChip = new StringBuffer();
	    sqlChip.append(" Select chp.usu_numtel, chp.usu_iccid, chp.usu_codope ");
	    sqlChip.append(" From USU_T160CHP chp ");
	    sqlChip.append(" Where chp.usu_numctr = ? And chp.usu_numpos = ? ");

	    PreparedStatement stChip = connSapiens.prepareStatement(sqlChip.toString());
	    stChip.setLong(1, numctr);
	    stChip.setLong(2, numpos);
	    ResultSet rsChip = stChip.executeQuery();

	    //Cargos
	    while (rsChip.next())
	    {
		ChipGPRSVO chip = new ChipGPRSVO();
		chip.setNumerotelefone(rsChip.getString("usu_numtel"));
		chip.setIccId(rsChip.getString("usu_iccid"));
		chip.setCodigoOperadora(rsChip.getString("usu_codope"));
		chips.add(chip);
	    }

	    rsChip.close();
	    stChip.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connSapiens.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}
	return chips;
    }

    public static List<EntradaAutorizadaVO> listaCoberturaPosto(Long numloc, Long taborg)
    {
	List<EntradaAutorizadaVO> entradaAutorizadas = new ArrayList<EntradaAutorizadaVO>();
	EntradaAutorizadaVO entradaAutorizada = new EntradaAutorizadaVO();

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    StringBuffer sqlEntradaAutorizada = new StringBuffer();
	    sqlEntradaAutorizada.append(" SELECT TOP(1000) cob.USU_DatAlt, cob.USU_DatFim, mot.USU_despre, cob.USU_ObsCob, ");
	    sqlEntradaAutorizada.append("		 fun.NumEmp AS NumEmpCob, fun.NumCad AS NumCadCob, fun.NomFun AS NomFunCob, ");
	    sqlEntradaAutorizada.append("		 sub.NumEmp AS NumEmpSub, sub.NumCad AS NumCadSub, sub.NomFun AS NomFunSub, ");
	    sqlEntradaAutorizada.append(" 	     orn.USU_NomCli, orn.USU_LotOrn, orn.NomLoc, orn.USU_CodCcu ");
	    sqlEntradaAutorizada.append(" FROM USU_T038COBFUN cob ");
	    sqlEntradaAutorizada.append(" INNER JOIN R034FUN fun ON fun.NumEmp = cob.USU_NumEmp AND fun.TipCol = cob.USU_TipCol AND fun.NumCad = cob.USU_NumCad ");
	    sqlEntradaAutorizada.append(" LEFT JOIN R034FUN sub ON sub.NumEmp = cob.USU_NumEmpCob AND sub.TipCol = cob.USU_TipColCob AND sub.NumCad = cob.USU_NumCadCob ");
	    sqlEntradaAutorizada.append(" LEFT JOIN R038HES hes ON hes.NumEmp = fun.NumEmp AND hes.TipCol = fun.TipCol AND hes.NumCad = fun.NumCad AND hes.DatAlt = ( ");
	    sqlEntradaAutorizada.append(" 	   SELECT MAX (DatAlt) FROM R038HES hes2 WHERE hes2.NumEmp = hes.NumEmp AND hes2.TipCol = hes.TipCol AND hes2.NumCad = hes.NumCad AND hes2.DatAlt <= cob.USU_DatAlt) ");
	    sqlEntradaAutorizada.append(" LEFT JOIN R006ESC esc ON esc.CodEsc = hes.CodEsc ");
	    sqlEntradaAutorizada.append(" INNER JOIN R016ORN orn ON orn.NumLoc = cob.USU_NumLocTra AND orn.TabOrg = cob.USU_TabOrgTra ");
	    sqlEntradaAutorizada.append(" INNER JOIN USU_T010MoCo mot ON mot.USU_CodMot = cob.USU_codMot ");
	    sqlEntradaAutorizada.append(" WHERE cob.USU_NumLocTra = ? AND cob.USU_TabOrgTra = ? AND mot.USU_CodMot not in (4, 5, 8) ");
	    sqlEntradaAutorizada.append(" ORDER BY cob.USU_DatAlt DESC ");

	    PreparedStatement stEntradaAutorizada = connVetorh.prepareStatement(sqlEntradaAutorizada.toString());
	    stEntradaAutorizada.setLong(1, numloc);
	    stEntradaAutorizada.setLong(2, taborg);
	    ResultSet rsEntradaAutorizada = stEntradaAutorizada.executeQuery();

	    //EntradaAutorizadas
	    while (rsEntradaAutorizada.next())
	    {
		entradaAutorizada = new EntradaAutorizadaVO();

		Timestamp dataCobIni = rsEntradaAutorizada.getTimestamp("USU_DatAlt");
		GregorianCalendar gDataCobIni = new GregorianCalendar();
		gDataCobIni.setTimeInMillis(dataCobIni.getTime());

		Timestamp dataCobFim = rsEntradaAutorizada.getTimestamp("USU_DatFim");
		GregorianCalendar gDataCobFim = new GregorianCalendar();

		if (dataCobFim != null)
		{
		    gDataCobFim.setTimeInMillis(dataCobFim.getTime());
		}

		PostoVO posto = new PostoVO();
		posto.setCentroCusto(rsEntradaAutorizada.getString("USU_CodCcu"));
		posto.setLotacao(rsEntradaAutorizada.getString("USU_LotOrn"));
		posto.setNomePosto(rsEntradaAutorizada.getString("NomLoc"));

		ColaboradorVO colaborador = new ColaboradorVO();
		colaborador.setNumeroEmpresa(rsEntradaAutorizada.getLong("NumEmpCob"));
		colaborador.setNumeroCadastro(rsEntradaAutorizada.getLong("NumCadCob"));
		colaborador.setNomeColaborador(rsEntradaAutorizada.getString("NomFunCob"));

		ColaboradorVO substituido = new ColaboradorVO();
		substituido.setNumeroEmpresa(rsEntradaAutorizada.getLong("NumEmpSub"));
		substituido.setNumeroCadastro(rsEntradaAutorizada.getLong("NumCadSub"));
		substituido.setNomeColaborador(rsEntradaAutorizada.getString("NomFunSub"));

		entradaAutorizada.setColaborador(colaborador);
		entradaAutorizada.setColaboradorSubstituido(substituido);
		entradaAutorizada.setDataInicial(gDataCobIni);
		entradaAutorizada.setDataFinal(gDataCobFim);
		entradaAutorizada.setDescricaoCobertura(rsEntradaAutorizada.getString("USU_despre"));
		entradaAutorizada.setPosto(posto);
		entradaAutorizada.setObservacao(rsEntradaAutorizada.getString("USU_ObsCob"));

		String txtExcluir = "";

		NeoUser user = PortalUtil.getCurrentUser();
		NeoPaper papelOperador = (NeoPaper) PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "[QLP] Operador do Quadro de Lotação Presença"));

		Boolean isPapelOperador = Boolean.FALSE;

		if (user != null && user.getPapers() != null && user.getPapers().contains(papelOperador))
		{
		    isPapelOperador = Boolean.TRUE;
		}

		if (user != null && (user.isAdm() || isPapelOperador))
		{
		    txtExcluir = getLinkBtnDelete(entradaAutorizada);
		}

		entradaAutorizada.setExcluir(txtExcluir);

		entradaAutorizadas.add(entradaAutorizada);
	    }

	    rsEntradaAutorizada.close();
	    stEntradaAutorizada.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}
	return entradaAutorizadas;
    }

    public static List<EventoSigmaVO> listaEventoX8(Long numctr, Long numpos)
    {
	List<EventoSigmaVO> eventos = new ArrayList<EventoSigmaVO>();
	Connection connSapiens = PersistEngine.getConnection("SAPIENS");

	try
	{
	    StringBuffer sqlEvento = new StringBuffer();
	    sqlEvento.append(" Select Top(100) cen.cd_cliente, cen.id_central, cen.particao, cen.id_empresa, cen.fantasia, his.cd_historico_alarme, his.cd_evento, ");
	    sqlEvento.append(" 		  his.dt_recebido, his.dt_viatura_deslocamento, his.dt_viatura_no_local, his.dt_fechamento, his.tx_observacao_fechamento, ");
	    sqlEvento.append(" 		  his.nu_tempo_viatura_deslocamento, via.nm_viatura ");
	    sqlEvento.append(" From USU_T160SIG sig ");
	    sqlEvento.append(" Inner Join ["+serverInfo.get(OrsegupsConnectionUtils.NOME_SERVIDOR)+"\\"+serverInfo.get(OrsegupsConnectionUtils.INSTANCIA)+"].SIGMA90.dbo.dbCentral cen ON cen.cd_cliente = sig.usu_codcli ");
	    sqlEvento.append(" Inner Join ["+serverInfo.get(OrsegupsConnectionUtils.NOME_SERVIDOR)+"\\"+serverInfo.get(OrsegupsConnectionUtils.INSTANCIA)+"].SIGMA90.dbo.Historico_Alarme his on his.cd_cliente = cen.cd_cliente ");
	    sqlEvento.append(" Left Join ["+serverInfo.get(OrsegupsConnectionUtils.NOME_SERVIDOR)+"\\"+serverInfo.get(OrsegupsConnectionUtils.INSTANCIA)+"].SIGMA90.dbo.Viatura via on via.cd_viatura = his.cd_viatura ");
	    sqlEvento.append(" Where sig.usu_numctr = ? And sig.usu_numpos = ? And cen.ctrl_central = 1 And his.cd_evento = 'XXX8'");
	    sqlEvento.append(" Order By his.dt_viatura_deslocamento Desc, cen.id_central, cen.particao ");

	    PreparedStatement stEvento = connSapiens.prepareStatement(sqlEvento.toString());
	    stEvento.setLong(1, numctr);
	    stEvento.setLong(2, numpos);
	    ResultSet rsEvento = stEvento.executeQuery();

	    //Cargos
	    while (rsEvento.next())
	    {
		Timestamp dataRecebido = rsEvento.getTimestamp("dt_recebido");
		Timestamp dataDeslocamento = rsEvento.getTimestamp("dt_viatura_deslocamento");
		Timestamp dataLocal = rsEvento.getTimestamp("dt_viatura_no_local");
		Timestamp dataFechamento = rsEvento.getTimestamp("dt_fechamento");

		EventoSigmaVO evento = new EventoSigmaVO();

		evento.setCodigo(rsEvento.getLong("cd_historico_alarme"));
		evento.setCdCliente(rsEvento.getLong("cd_cliente"));
		evento.setCentralParticao(rsEvento.getString("id_central") + "[" + rsEvento.getString("particao") + "]");
		evento.setObservacaoFechamento(rsEvento.getString("tx_observacao_fechamento"));
		evento.setTempoDeslocamento(OrsegupsUtils.getDuration(rsEvento.getLong("nu_tempo_viatura_deslocamento")));
		evento.setNomeViatura(rsEvento.getString("nm_viatura"));

		if (NeoUtils.safeIsNotNull(dataRecebido))
		{
		    GregorianCalendar gDataRecebido = new GregorianCalendar();
		    gDataRecebido.setTimeInMillis(dataRecebido.getTime());
		    evento.setDataRecebido(NeoUtils.safeDateFormat(gDataRecebido, "dd/MM/yyyy HH:mm:ss"));
		}

		if (NeoUtils.safeIsNotNull(dataDeslocamento))
		{
		    GregorianCalendar gDataDeslocamento = new GregorianCalendar();
		    gDataDeslocamento.setTimeInMillis(dataDeslocamento.getTime());
		    evento.setDataDeslocamento(NeoUtils.safeDateFormat(gDataDeslocamento, "dd/MM/yyyy HH:mm:ss"));
		}

		if (NeoUtils.safeIsNotNull(dataLocal))
		{
		    GregorianCalendar gDataLocal = new GregorianCalendar();
		    gDataLocal.setTimeInMillis(dataLocal.getTime());
		    evento.setDataLocal(NeoUtils.safeDateFormat(gDataLocal, "dd/MM/yyyy HH:mm:ss"));
		}

		if (NeoUtils.safeIsNotNull(dataFechamento))
		{
		    GregorianCalendar gDataFechamento = new GregorianCalendar();
		    gDataFechamento.setTimeInMillis(dataFechamento.getTime());
		    evento.setDataFechamento(NeoUtils.safeDateFormat(gDataFechamento, "dd/MM/yyyy HH:mm:ss"));
		}
		eventos.add(evento);
	    }
	    rsEvento.close();
	    stEvento.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connSapiens.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}
	return eventos;
    }

    public static List<EventoSigmaVO> listaEventosSigma(Long cdCliente, GregorianCalendar dataAtual, Long dias, String tipoEvento)
    {
	List<EventoSigmaVO> eventos = new ArrayList<EventoSigmaVO>();
	Connection connSigma = PersistEngine.getConnection("SIGMA90");

	try
	{
	    StringBuffer sql = new StringBuffer();

	    sql.append(" Select  his.cd_evento, his.dt_recebido, his.dt_viatura_deslocamento, his.dt_viatura_no_local, his.dt_fechamento, his.tx_observacao_fechamento, ");
	    sql.append(" 		his.nu_tempo_viatura_deslocamento, via.nm_viatura ");
	    sql.append(" From dbCentral cen ");
	    sql.append(" Inner Join " + tipoEvento + " his on his.cd_cliente = cen.cd_cliente ");
	    sql.append(" Left Join Viatura via on via.cd_viatura = his.cd_viatura ");
	    sql.append(" Where cen.cd_cliente = ? And cen.ctrl_central = 1 And his.dt_recebido Between ? - ? And ? ");
	    sql.append(" Order By his.dt_viatura_deslocamento Desc ");

	    PreparedStatement st = connSigma.prepareStatement(sql.toString());
	    st.setLong(1, cdCliente);
	    st.setTimestamp(2, new Timestamp(dataAtual.getTimeInMillis()));
	    st.setLong(3, dias);
	    st.setTimestamp(4, new Timestamp(dataAtual.getTimeInMillis()));
	    ResultSet rsEvento = st.executeQuery();

	    //Cargos
	    while (rsEvento.next())
	    {
		Timestamp dataRecebido = rsEvento.getTimestamp("dt_recebido");
		Timestamp dataDeslocamento = rsEvento.getTimestamp("dt_viatura_deslocamento");
		Timestamp dataLocal = rsEvento.getTimestamp("dt_viatura_no_local");
		Timestamp dataFechamento = rsEvento.getTimestamp("dt_fechamento");

		EventoSigmaVO evento = new EventoSigmaVO();

		evento.setTipoEvento(rsEvento.getString("cd_evento"));
		evento.setObservacaoFechamento(rsEvento.getString("tx_observacao_fechamento"));
		evento.setTempoDeslocamento(OrsegupsUtils.getDuration(rsEvento.getLong("nu_tempo_viatura_deslocamento")));
		evento.setNomeViatura(rsEvento.getString("nm_viatura"));

		if (NeoUtils.safeIsNotNull(dataRecebido))
		{
		    GregorianCalendar gDataRecebido = new GregorianCalendar();
		    gDataRecebido.setTimeInMillis(dataRecebido.getTime());
		    evento.setDataRecebido(NeoUtils.safeDateFormat(gDataRecebido, "dd/MM/yyyy HH:mm:ss"));
		}

		if (NeoUtils.safeIsNotNull(dataDeslocamento))
		{
		    GregorianCalendar gDataDeslocamento = new GregorianCalendar();
		    gDataDeslocamento.setTimeInMillis(dataDeslocamento.getTime());
		    evento.setDataDeslocamento(NeoUtils.safeDateFormat(gDataDeslocamento, "dd/MM/yyyy HH:mm:ss"));
		}

		if (NeoUtils.safeIsNotNull(dataLocal))
		{
		    GregorianCalendar gDataLocal = new GregorianCalendar();
		    gDataLocal.setTimeInMillis(dataLocal.getTime());
		    evento.setDataLocal(NeoUtils.safeDateFormat(gDataLocal, "dd/MM/yyyy HH:mm:ss"));
		}

		if (NeoUtils.safeIsNotNull(dataFechamento))
		{
		    GregorianCalendar gDataFechamento = new GregorianCalendar();
		    gDataFechamento.setTimeInMillis(dataFechamento.getTime());
		    evento.setDataFechamento(NeoUtils.safeDateFormat(gDataFechamento, "dd/MM/yyyy HH:mm:ss"));
		}
		eventos.add(evento);
	    }

	    rsEvento.close();
	    st.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connSigma.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}
	return eventos;
    }

    public static List<SolicitacaoVO> listaSolicitacoes(Long codcli)
    {
	List<SolicitacaoVO> solicitacoes = new ArrayList<SolicitacaoVO>();

	try
	{
	    QLEqualsFilter filterCodigo = new QLEqualsFilter("codcli", codcli);
	    NeoObject cliente = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("SAPIENS_Clientes"), filterCodigo);

	    if (NeoUtils.safeIsNotNull(cliente))
	    {
		QLEqualsFilter filterCliente = new QLEqualsFilter("clienteSapiens", cliente);
		QLRawFilter processStateFilter = new QLRawFilter("wfprocess.processState not in (" + ProcessState.canceled.ordinal() + ")");

		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		groupFilter.addFilter(filterCliente);
		groupFilter.addFilter(processStateFilter);

		List<NeoObject> rscsNovoModelo = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("RSCRelatorioSolicitacaoClienteNovo"), groupFilter, -1, -1, "neoId desc");
		List<NeoObject> rscs = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("RSCRelatorioSolicitacaoCliente"), groupFilter, -1, -1, "neoId desc");

		//Novo modelo de tarefas de RSC
		if (NeoUtils.safeIsNotNull(rscsNovoModelo))
		{
		    for (NeoObject rsc : rscsNovoModelo)
		    {
			EntityWrapper rscWrapper = new EntityWrapper(rsc);
			WFProcess process = (WFProcess) rscWrapper.findValue("wfprocess");
			String origem = NeoUtils.safeOutputString(rscWrapper.findValue("origem.sequencia"));

			if (NeoUtils.safeIsNull(process))
			{
			    continue;
			}

			List<Task> tasks = (List<Task>) process.getAllPendingTaskList();

			//Verifica se possui Task pendente
			if (NeoUtils.safeIsNotNull(tasks) && !tasks.isEmpty())
			{
			    //Verifica se possui apenas uma task e se a mesma esta em execucao, neste caso nao deve mostrar na listagem
			    //Teste para mostrar RSC no site
			    if (process.getAllPendingTaskList().size() == 0 && !origem.equals("05"))
			    {
				continue;
			    }

			}
			else
			{
			    //Verifica se nao possui Task pendente e se o processo esta com status em execucao 
			    if (process.getProcessState().ordinal() == ProcessState.running.ordinal() && !process.getAllPendingTaskList().isEmpty())
			    {
				continue;
			    }
			}

			Long neoid = (Long) rscWrapper.findValue("wfprocess.neoId");
			String codigoTarefa = (String) rscWrapper.findValue("wfprocess.code");
			String solicitante = (String) rscWrapper.findValue("wfprocess.requester.fullName");
			//TODO RSC - Criar adapter para exibir categoria verificando se os subeforms estão diferentes de null
			//String categoria = (String) rscWrapper.findValue("categoriasRsc.descricao");
			GregorianCalendar dataInicio = (GregorianCalendar) rscWrapper.findValue("wfprocess.startDate");
			GregorianCalendar dataConclusao = (GregorianCalendar) rscWrapper.findValue("wfprocess.finishDate");
			ProcessState processState = (ProcessState) rscWrapper.findValue("wfprocess.processState");

			SolicitacaoVO solicitacao = new SolicitacaoVO();
			solicitacao.setNeoid(neoid);
			solicitacao.setCodigoTarefa(codigoTarefa);
			//solicitacao.setCategoria(categoria);
			solicitacao.setSolicitante(solicitante);
			solicitacao.setSituacao(I18nUtils.getString(processState.name()));
			solicitacao.setLink("<a title='Visualizar tarefa' style='cursor:pointer' onclick='javascript:showSolicitacao(" + solicitacao.getNeoid().toString() + ")'><img src='imagens/icones_final/properties_16x16-trans.png'></a>");

			if (NeoUtils.safeIsNotNull(dataInicio))
			{
			    solicitacao.setDataInicio(NeoUtils.safeDateFormat(dataInicio));
			}

			if (NeoUtils.safeIsNotNull(dataConclusao))
			{
			    solicitacao.setDataConclusao(NeoUtils.safeDateFormat(dataConclusao));
			}

			//						List<RegistroAtividadeRSCVO> registroAtividade = new ArrayList<RegistroAtividadeRSCVO>();
			//						List<NeoObject> objsRegAtividade = (List<NeoObject>) rscWrapper.getValue("registroAtividades");
			//						
			//						for(NeoObject objRegAtividade : objsRegAtividade){
			//							EntityWrapper wRegAtividade = new EntityWrapper(objRegAtividade);
			//							RegistroAtividadeRSCVO registroAtividadeRSCVO = new RegistroAtividadeRSCVO();
			//							String nomeAtividade = String.valueOf(wRegAtividade.getValue("atividade"));
			//							String descricaoAtv =  String.valueOf(wRegAtividade.getValue("descricao"));
			//							GregorianCalendar dataInicialRSC = (GregorianCalendar) wRegAtividade.getValue("dataInicialRSC");
			//							GregorianCalendar dataFinalRSC = (GregorianCalendar) wRegAtividade.getValue("dataFinalRSC");
			//							GregorianCalendar prazo = (GregorianCalendar) wRegAtividade.getValue("prazo");
			//							NeoUser responsavel = (NeoUser) wRegAtividade.getValue("responsavel");
			//							Long neoId = (Long) wRegAtividade.getValue("neoId");
			//							
			//							registroAtividadeRSCVO.setDataFinalRSC(dataFinalRSC);
			//							registroAtividadeRSCVO.setDataInicialRSC(dataInicialRSC);
			//							registroAtividadeRSCVO.setDescricao(descricaoAtv);
			//							registroAtividadeRSCVO.setNeoId(neoId);
			//							registroAtividadeRSCVO.setNomeAtividade(nomeAtividade);
			//							registroAtividadeRSCVO.setPrazo(prazo);
			//							registroAtividadeRSCVO.setResponsavel(responsavel);
			//							
			//							registroAtividade.add(registroAtividadeRSCVO);
			//						}
			//
			//						if(objsRegAtividade.size() > 0){
			//							solicitacao.setRegistroAtividades(registroAtividade);
			//						}

			solicitacoes.add(solicitacao);
		    }
		}

		//TODO RSC - Esse trecho pode ser removido após 01/05/2017 pois só o novo modelo estara em vigor.
		if (NeoUtils.safeIsNotNull(rscs))
		{
		    for (NeoObject rsc : rscs)
		    {
			EntityWrapper rscWrapper = new EntityWrapper(rsc);
			WFProcess process = (WFProcess) rscWrapper.findValue("wfprocess");
			String origem = NeoUtils.safeOutputString(rscWrapper.findValue("origem.sequencia"));

			if (NeoUtils.safeIsNull(process))
			{
			    continue;
			}

			List<Task> tasks = (List<Task>) process.getAllPendingTaskList();

			//Verifica se possui Task pendente
			if (NeoUtils.safeIsNotNull(tasks) && !tasks.isEmpty())
			{
			    //Verifica se possui apenas uma task e se a mesma esta em execucao, neste caso nao deve mostrar na listagem
			    //Teste para mostrar RSC no site
			    if (process.getAllPendingTaskList().size() == 0 && !origem.equals("05"))
			    {
				continue;
			    }

			}
			else
			{
			    //Verifica se nao possui Task pendente e se o processo esta com status em execucao 
			    if (process.getProcessState().ordinal() == ProcessState.running.ordinal() && !process.getAllPendingTaskList().isEmpty())
			    {
				continue;
			    }
			}

			Long neoid = (Long) rscWrapper.findValue("wfprocess.neoId");
			String codigoTarefa = (String) rscWrapper.findValue("wfprocess.code");
			String solicitante = (String) rscWrapper.findValue("wfprocess.requester.fullName");
			String categoria = (String) rscWrapper.findValue("categoriasRsc.descricao");
			GregorianCalendar dataInicio = (GregorianCalendar) rscWrapper.findValue("wfprocess.startDate");
			GregorianCalendar dataConclusao = (GregorianCalendar) rscWrapper.findValue("wfprocess.finishDate");
			ProcessState processState = (ProcessState) rscWrapper.findValue("wfprocess.processState");

			SolicitacaoVO solicitacao = new SolicitacaoVO();
			solicitacao.setNeoid(neoid);
			solicitacao.setCodigoTarefa(codigoTarefa);
			solicitacao.setCategoria(categoria);
			solicitacao.setSolicitante(solicitante);
			solicitacao.setSituacao(I18nUtils.getString(processState.name()));
			solicitacao.setLink("<a title='Visualizar tarefa' style='cursor:pointer' onclick='javascript:showSolicitacao(" + solicitacao.getNeoid().toString() + ")'><img src='imagens/icones_final/properties_16x16-trans.png'></a>");

			if (NeoUtils.safeIsNotNull(dataInicio))
			{
			    solicitacao.setDataInicio(NeoUtils.safeDateFormat(dataInicio));
			}

			if (NeoUtils.safeIsNotNull(dataConclusao))
			{
			    solicitacao.setDataConclusao(NeoUtils.safeDateFormat(dataConclusao));
			}

			solicitacoes.add(solicitacao);
		    }
		}
	    }
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}

	return solicitacoes;
    }

    public static List<ReclamacaoVO> listaReclamacao(Long codcli)
    {
	List<ReclamacaoVO> solicitacoes = new ArrayList<ReclamacaoVO>();

	try
	{
	    QLEqualsFilter filterCodigo = new QLEqualsFilter("codcli", codcli);
	    NeoObject cliente = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("SAPIENS_Clientes"), filterCodigo);

	    if (NeoUtils.safeIsNotNull(cliente))
	    {
		QLEqualsFilter filterCliente = new QLEqualsFilter("clienteSapiens", cliente);
		QLRawFilter processStateFilter = new QLRawFilter("wfprocess.processState not in (" + ProcessState.canceled.ordinal() + ")");

		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		groupFilter.addFilter(filterCliente);
		groupFilter.addFilter(processStateFilter);

		List<NeoObject> rrcs = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("RRCRelatorioReclamacaoCliente"), groupFilter, -1, -1, "neoId desc");

		if (NeoUtils.safeIsNotNull(rrcs))
		{
		    for (NeoObject rrc : rrcs)
		    {
			EntityWrapper rscWrapper = new EntityWrapper(rrc);
			WFProcess process = (WFProcess) rscWrapper.findValue("wfprocess");
			String origem = NeoUtils.safeOutputString(rscWrapper.findValue("origem.sequencia"));

			if (NeoUtils.safeIsNull(process))
			{
			    continue;
			}

			List<Task> tasks = (List<Task>) process.getAllPendingTaskList();

			//Verifica se possui Task pendente
			if (NeoUtils.safeIsNotNull(tasks) && !tasks.isEmpty())
			{
			    //Verifica se possui apenas uma task e se a mesma esta em execucao, neste caso nao deve mostrar na listagem
			    //Teste para mostrar RSC no site
			    if (process.getAllPendingTaskList().size() == 0 && !origem.equals("05"))
			    {
				continue;
			    }

			}
			else
			{
			    //Verifica se nao possui Task pendente e se o processo esta com status em execucao 
			    if (process.getProcessState().ordinal() == ProcessState.running.ordinal() && !process.getAllPendingTaskList().isEmpty())
			    {
				continue;
			    }
			}

			Long neoid = (Long) rscWrapper.findValue("wfprocess.neoId");
			String codigoTarefa = (String) rscWrapper.findValue("wfprocess.code");
			String solicitante = (String) rscWrapper.findValue("wfprocess.requester.fullName");
			GregorianCalendar dataInicio = (GregorianCalendar) rscWrapper.findValue("wfprocess.startDate");
			GregorianCalendar dataConclusao = (GregorianCalendar) rscWrapper.findValue("wfprocess.finishDate");
			ProcessState processState = (ProcessState) rscWrapper.findValue("wfprocess.processState");

			ReclamacaoVO reclamacao = new ReclamacaoVO();
			reclamacao.setNeoid(neoid);
			reclamacao.setCodigoTarefa(codigoTarefa);
			reclamacao.setSolicitante(solicitante);
			reclamacao.setSituacao(I18nUtils.getString(processState.name()));
			reclamacao.setLink("<a title='Visualizar tarefa' style='cursor:pointer' onclick='javascript:showSolicitacao(" + reclamacao.getNeoid().toString() + ")'><img src='imagens/icones_final/properties_16x16-trans.png'></a>");

			if (NeoUtils.safeIsNotNull(dataInicio))
			{
			    reclamacao.setDataInicio(NeoUtils.safeDateFormat(dataInicio));
			}

			if (NeoUtils.safeIsNotNull(dataConclusao))
			{
			    reclamacao.setDataConclusao(NeoUtils.safeDateFormat(dataConclusao));
			}
			solicitacoes.add(reclamacao);
		    }
		}
	    }
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}

	return solicitacoes;
    }

    public static List<VisitaVO> listaVisita(String codpai)
    {
	List<VisitaVO> visitas = new ArrayList<VisitaVO>();

	try
	{
	    String nomeEform = AdapterUtils.getEntityClass("AVRPrincipal").getName();
	    StringBuilder hql = new StringBuilder();
	    hql.append(" Select avr.neoId, avr.dataAgenda, avr.dataRelato, avr.contatoCliente, avr.area.descricao, avr.telefone, avr.email, avr.relatoGeral,  ");
	    hql.append(" 		avr.relatoX8, avr.relatoCoberturas, avr.relatoSolicitacoes, avr.relatoReclamacoes, relatoEquipamentos, relatoChecklist, ");
	    hql.append(" 		avr.relatoContasSigma, avr.relatoEventos, avr.relatoVisitas ");
	    hql.append(" From " + nomeEform + " As avr ");
	    hql.append(" Where avr.clienteLotacaoNiveis.nivel7.codloc = :codpai");
	    Query query = (Query) PersistEngine.getEntityManager().createQuery(hql.toString());
	    query.setParameter("codpai", codpai);

	    @SuppressWarnings("unchecked")
	    Collection<Object[]> resultList = query.getResultList();

	    for (Object[] obj : resultList)
	    {
		VisitaVO visita = new VisitaVO();

		visita.setNeoid((Long) obj[0]);
		visita.setDataAgenda(NeoUtils.safeDateFormat((GregorianCalendar) obj[1], "dd/MM/yyyy"));
		visita.setDataRelato(NeoUtils.safeDateFormat((GregorianCalendar) obj[2], "dd/MM/yyyy"));
		visita.setContatoCliente((String) obj[3]);
		visita.setArea((String) obj[4]);
		visita.setTelefone((String) obj[5]);
		visita.setEmail((String) obj[6]);
		visita.setRelatoGeral((String) obj[7]);
		visita.setRelatoX8((String) obj[8]);
		visita.setRelatoCoberturas((String) obj[9]);
		visita.setRelatoSolicitacoes((String) obj[10]);
		visita.setRelatoReclamacoes((String) obj[11]);
		visita.setRelatoEquipamentos((String) obj[12]);
		visita.setRelatoChecklist((String) obj[13]);
		visita.setRelatoContasSigma((String) obj[14]);
		visita.setRelatoEventos((String) obj[15]);
		visita.setRelatoVisitas((String) obj[16]);

		visita.setLink("<a title='Dados da Visita' style='cursor:pointer' onclick='javascript:showVisita(\"" + visita.getNeoid().toString() + "\")'><img src='imagens/icones_final/properties_16x16-trans.png'></a>");

		visitas.add(visita);
	    }
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}

	return visitas;
    }

    public static VisitaVO buscaPorVisitaPorNeoid(Long neoId)
    {
	VisitaVO visita = new VisitaVO();

	try
	{
	    String nomeEform = AdapterUtils.getEntityClass("AVRPrincipal").getName();
	    StringBuilder hql = new StringBuilder();
	    hql.append(" Select avr.neoId, avr.dataAgenda, avr.dataRelato, avr.contatoCliente, avr.area.descricao, avr.telefone, avr.email, avr.relatoGeral,  ");
	    hql.append(" 		avr.relatoX8, avr.relatoCoberturas, avr.relatoSolicitacoes, avr.relatoReclamacoes, relatoEquipamentos, relatoChecklist, ");
	    hql.append(" 		avr.relatoContasSigma, avr.relatoEventos, avr.relatoVisitas ");
	    hql.append(" From " + nomeEform + " As avr ");
	    hql.append(" Where avr.neoId = :neoId");
	    Query query = (Query) PersistEngine.getEntityManager().createQuery(hql.toString());
	    query.setParameter("neoId", neoId);

	    Object[] obj = (Object[]) query.getSingleResult();

	    visita.setNeoid((Long) obj[0]);
	    visita.setDataAgenda(NeoUtils.safeDateFormat((GregorianCalendar) obj[1], "dd/MM/yyyy"));
	    visita.setDataRelato(NeoUtils.safeDateFormat((GregorianCalendar) obj[2], "dd/MM/yyyy"));
	    visita.setContatoCliente((String) obj[3]);
	    visita.setArea((String) obj[4]);
	    visita.setTelefone((String) obj[5]);
	    visita.setEmail((String) obj[6]);
	    visita.setRelatoGeral((String) obj[7]);
	    visita.setRelatoX8((String) obj[8]);
	    visita.setRelatoCoberturas((String) obj[9]);
	    visita.setRelatoSolicitacoes((String) obj[10]);
	    visita.setRelatoReclamacoes((String) obj[11]);
	    visita.setRelatoEquipamentos((String) obj[12]);
	    visita.setRelatoChecklist((String) obj[13]);
	    visita.setRelatoContasSigma((String) obj[14]);
	    visita.setRelatoEventos((String) obj[15]);
	    visita.setRelatoVisitas((String) obj[16]);
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}

	return visita;
    }

    public static String insereHoraExtra(Long numemp, Long tipcol, Long numcad, String data)
    {
	Connection connVetorh = PersistEngine.getConnection("VETORH");
	String retorno = "";

	try
	{
	    GregorianCalendar gData = OrsegupsUtils.stringToGregorianCalendar(data, "dd/MM/yyyy");
	    StringBuffer sqlHE = new StringBuffer();

	    sqlHE.append(" INSERT INTO R064EXT (NumEmp, TipCol, NumCad, DatIni, HorIni, DatFim, HorFim, StaAcc) ");
	    sqlHE.append(" VALUES (?, ?, ?, ?, 0, ?, 0, 0)");

	    PreparedStatement stHE = connVetorh.prepareStatement(sqlHE.toString());

	    stHE.setLong(1, numemp);
	    stHE.setLong(2, tipcol);
	    stHE.setLong(3, numcad);
	    stHE.setTimestamp(4, new Timestamp(gData.getTimeInMillis()));
	    stHE.setTimestamp(5, new Timestamp(gData.getTimeInMillis()));

	    stHE.executeUpdate();
	    stHE.close();

	    retorno = "Autorização de Hora extra inserida com sucesso.";
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	    retorno = "Erro ao inserir Autorização de Hora extra.";
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
		retorno = "Erro ao inserir Autorização de Hora extra.";

	    }
	}
	return retorno;
    }

    public static String removeHoraExtra(Long numemp, Long tipcol, Long numcad, String data)
    {
	Connection connVetorh = PersistEngine.getConnection("VETORH");
	String retorno = "";

	try
	{
	    GregorianCalendar gData = OrsegupsUtils.stringToGregorianCalendar(data, "dd/MM/yyyy");
	    StringBuffer sqlHE = new StringBuffer();

	    sqlHE.append(" DELETE FROM R064EXT WHERE NumEmp = ? AND TipCol = ? AND NumCad = ? AND DatIni = ? AND HorIni = 0 AND DatFim = ? AND HorFim = 0 ");

	    PreparedStatement stHE = connVetorh.prepareStatement(sqlHE.toString());

	    stHE.setLong(1, numemp);
	    stHE.setLong(2, tipcol);
	    stHE.setLong(3, numcad);
	    stHE.setTimestamp(4, new Timestamp(gData.getTimeInMillis()));
	    stHE.setTimestamp(5, new Timestamp(gData.getTimeInMillis()));

	    stHE.executeUpdate();
	    stHE.close();

	    retorno = "Autorização de Hora extra inserida com sucesso.";
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	    retorno = "Erro ao inserir Autorização de Hora extra.";
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
		retorno = "Erro ao inserir Autorização de Hora extra.";

	    }
	}
	return retorno;
    }

    public static String validaExcecao(String excval, Long numemp, Long tipcol, Long numcad, Long codsit, String datapu)
    {
	EntityManager entityManager = PersistEngine.getEntityManager("VETORH");
	EntityTransaction transaction = entityManager.getTransaction();

	String retorno = "";

	try
	{
	    String sqlHE = " UPDATE R066SIT SET USU_ExcVal =:excval WHERE NumEmp = :numemp AND TipCol = :tipcol AND NumCad = :numcad AND CodSit =:codsit AND CONVERT(varchar, DatApu, 103) = :datapu ";
	    Query updateSit = entityManager.createNativeQuery(sqlHE);
	    updateSit.setParameter("excval", excval);
	    updateSit.setParameter("numemp", numemp);
	    updateSit.setParameter("tipcol", tipcol);
	    updateSit.setParameter("numcad", numcad);
	    updateSit.setParameter("codsit", codsit);
	    updateSit.setParameter("datapu", datapu);

	    updateSit.executeUpdate();

	    //Caso Falta transforma afastamento em Falta (Ronda).
	    if (codsit == 15L)
	    {
		String sqlAfa = " UPDATE R038AFA SET SitAfa = :codsit, usu_datmod=DATEADD(dd, 0, DATEDIFF(dd, 0, :datMod1)), usu_hormod=(DATEPART(HOUR, :datMod2) * 60) + DATEPART(MINUTE, :datMod3) WHERE NumEmp = :numemp AND TipCol = :tipcol AND NumCad = :numcad AND CONVERT(varchar, DatAfa, 103) = :datafa ";
		Query updateAfa = entityManager.createNativeQuery(sqlAfa);
		updateAfa.setParameter("codsit", 16);
		GregorianCalendar datMod = new GregorianCalendar();
		updateAfa.setParameter("datMod1", new Timestamp(datMod.getTimeInMillis()));
		updateAfa.setParameter("datMod2", new Timestamp(datMod.getTimeInMillis()));
		updateAfa.setParameter("datMod3", new Timestamp(datMod.getTimeInMillis()));
		updateAfa.setParameter("numemp", numemp);
		updateAfa.setParameter("tipcol", tipcol);
		updateAfa.setParameter("numcad", numcad);
		updateAfa.setParameter("datafa", datapu);

		updateAfa.executeUpdate();

		retorno = "Exceção retornada com sucesso!";
	    }

	    //Caso Falta (Ronda) transforma afastamento em Falta.
	    if (codsit == 16L || codsit == 68L)
	    {
		String sqlAfa = " UPDATE R038AFA SET SitAfa = :codsit, usu_datmod=DATEADD(dd, 0, DATEDIFF(dd, 0, :datMod1)), usu_hormod=(DATEPART(HOUR, :datMod2) * 60) + DATEPART(MINUTE, :datMod3) WHERE NumEmp = :numemp AND TipCol = :tipcol AND NumCad = :numcad AND CONVERT(varchar, DatAfa, 103) = :datafa ";
		Query updateAfa = entityManager.createNativeQuery(sqlAfa);
		if (codsit == 16L)
		{
		    updateAfa.setParameter("codsit", 15);
		}
		else
		{
		    updateAfa.setParameter("codsit", 66);
		}
		GregorianCalendar datMod = new GregorianCalendar();
		updateAfa.setParameter("datMod1", new Timestamp(datMod.getTimeInMillis()));
		updateAfa.setParameter("datMod2", new Timestamp(datMod.getTimeInMillis()));
		updateAfa.setParameter("datMod3", new Timestamp(datMod.getTimeInMillis()));
		updateAfa.setParameter("numemp", numemp);
		updateAfa.setParameter("tipcol", tipcol);
		updateAfa.setParameter("numcad", numcad);
		updateAfa.setParameter("datafa", datapu);

		updateAfa.executeUpdate();

		retorno = "Exceção validada com sucesso!";
	    }
	    transaction.commit();
	    //entityManager.flush();
	}
	catch (Exception ex)
	{
	    if (transaction.isActive())
	    {
		transaction.rollback();
	    }
	    ex.printStackTrace();
	    retorno = "Erro ao inserir Autorização de Hora extra.";
	}
	return retorno;
    }

    public static List<DocumentoTreeVO> listaDocumentoTree(Long cpf)
    {
	List<DocumentoTreeVO> documentos = new ArrayList<DocumentoTreeVO>();
	List<NeoObject> colaboradores = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("GEDRHDADOSCOLABORADOR"), new QLEqualsFilter("numeroCPF", cpf), -1, -1, " _vo.neoObjectType.neoType, creationDate desc ");

	E2docHandler e2docHandler = new E2docHandler();
	documentos.addAll(e2docHandler.documentosFuncionarioPresenca(cpf.toString()));

	if (NeoUtils.safeIsNotNull(colaboradores))
	{
	    log.debug("<br>tamanho da colaboradores: " + colaboradores.size());
	    DocumentoTreeVO documentoAnt = new DocumentoTreeVO();
	    for (NeoObject col : colaboradores)
	    {
		log.debug("<br>neoid do colaborador:" + col.getNeoId());
		EntityWrapper colaboradorWrapper = new EntityWrapper(col);
		col = PersistEngine.reload(col);
		DocumentEntityInfo docInfo = (DocumentEntityInfo) colaboradorWrapper.findValue("docInfo");
		List<NeoObject> arquivos = (List<NeoObject>) colaboradorWrapper.findValue("arquivos");

		log.debug("<br>tamanho do arquivos:" + arquivos.size());
		Long neoid = (Long) colaboradorWrapper.findValue("neoId");

		DocumentoTreeVO documento = new DocumentoTreeVO();
		documento.setId(neoid);
		documento.setNome(docInfo.getTitle());
		documento.setState("closed");

		List<DocumentoTreeVO> childrens = new ArrayList<DocumentoTreeVO>();

		if (NeoUtils.safeIsNotNull(arquivos))
		{
		    for (NeoObject arq : arquivos)
		    {

			EntityWrapper arquivoWrapper = new EntityWrapper(arq);
			NeoFile file = (NeoFile) arquivoWrapper.findValue("arquivo");
			Long fileNeoId = (Long)arquivoWrapper.findValue("neoId");
			log.debug("<br>NeoId do arquivo: " + fileNeoId);
			System.out.println("Caminho: "+file.getAbsolutePath());

			Long mes = (Long) arquivoWrapper.findValue("mes");
			Long ano = (Long) arquivoWrapper.findValue("ano");
			Long id = (Long) arquivoWrapper.findValue("neoId");

			GregorianCalendar gData = new GregorianCalendar();

			String competencia = "";

			if (mes != null && mes != 0 && ano != null && ano != 0)
			{
			    competencia = String.format("%02d", mes) + "/" + ano.toString();
			    gData = new GregorianCalendar(ano.intValue(), mes.intValue() - 1, 1);
			}
			else
			{
			    gData = new GregorianCalendar(1900, 0, 1);
			}
			DocumentoTreeVO children = new DocumentoTreeVO();
			children.setId(id);
			children.setNome(file.getName() + "." + file.getSufix().toLowerCase());
			children.setData(competencia);
			children.setgData(gData);
			children.setLink(OrsegupsUtils.getLink(file));
			childrens.add(children);
		    }
		}
		documento.setChildren(childrens);
		if (documentoAnt != null && documentoAnt.getNome() != null && documentoAnt.getNome().equals(documento.getNome()))
		{
		    documentoAnt.getChildren().addAll(documento.getChildren());
		}
		else
		{
		    documentos.add(documento);
		}
		documentoAnt = documento;
	    }
	}

	for (DocumentoTreeVO doc : documentos)
	{
	    Collections.sort(doc.getChildren(), new DocumentoComparator());
	}

	documentos.addAll(retornaDocumentosFap(cpf));

	return documentos;
    }

    public static List<DocumentoTreeVO> retornaDocumentosFap(Long cpf){
	List<DocumentoTreeVO> retorno = new ArrayList<DocumentoTreeVO>();


	try{
	    QLGroupFilter qgf = new QLGroupFilter("AND");
	    qgf.addFilter(new QLEqualsFilter("numcpf", cpf));
	    //QLRawFilter("sitafa != ")
	    //List<NeoObject> colaboradores = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORHRFUN"), new QLEqualsFilter("numcpf", cpf), -1, -1, "neoType, creationDate desc ");
	}catch(Exception e){
	    e.printStackTrace();
	    return retorno;
	}

	/*
		DocumentoTreeVO dfap = new DocumentoTreeVO();
		dfap.setId(1L);
		dfap.setNome("FAP XXXX");

		ArrayList<DocumentoTreeVO> childrens = new ArrayList<DocumentoTreeVO>();
		DocumentoTreeVO children = new DocumentoTreeVO();
		children.setNome("Boleto");
		children.setId(123L);

		childrens.add(children);
		dfap.setChildren(childrens);

		documentos.add(dfap);
	 */


	return retorno;

    }

    /*
	  //método original, sem as faps
	  public static List<DocumentoTreeVO> listaDocumentoTree(Long cpf)
		{
			List<DocumentoTreeVO> documentos = new ArrayList<DocumentoTreeVO>();
			List<NeoObject> colaboradores = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("GEDRHDADOSCOLABORADOR"), new QLEqualsFilter("numeroCPF", cpf), -1, -1, "neoType, creationDate desc ");

			if (NeoUtils.safeIsNotNull(colaboradores))
			{
				log.debug("<br>tamanho da colaboradores: " + colaboradores.size());
				DocumentoTreeVO documentoAnt = new DocumentoTreeVO();
				for (NeoObject col : colaboradores)
				{
					log.debug("<br>neoid do colaborador:" + col.getNeoId());
					EntityWrapper colaboradorWrapper = new EntityWrapper(col);
					col = PersistEngine.reload(col);
					DocumentEntityInfo docInfo = (DocumentEntityInfo) colaboradorWrapper.findValue("docInfo");
					List<NeoObject> arquivos = (List<NeoObject>) colaboradorWrapper.findValue("arquivos");

					log.debug("<br>tamanho do arquivos:" + arquivos.size());
					Long neoid = (Long) colaboradorWrapper.findValue("neoId");

					DocumentoTreeVO documento = new DocumentoTreeVO();
					documento.setId(neoid);
					documento.setNome(docInfo.getTitle());
					documento.setState("closed");

					List<DocumentoTreeVO> childrens = new ArrayList<DocumentoTreeVO>();

					if (NeoUtils.safeIsNotNull(arquivos))
					{
						for (NeoObject arq : arquivos)
						{

							EntityWrapper arquivoWrapper = new EntityWrapper(arq);
							NeoFile file = (NeoFile) arquivoWrapper.findValue("arquivo");
							Long fileNeoId = (Long)arquivoWrapper.findValue("neoId");
							log.debug("<br>NeoId do arquivo: " + fileNeoId);

							Long mes = (Long) arquivoWrapper.findValue("mes");
							Long ano = (Long) arquivoWrapper.findValue("ano");
							Long id = (Long) arquivoWrapper.findValue("neoId");

							GregorianCalendar gData = new GregorianCalendar();

							String competencia = "";

							if (mes != null && mes != 0 && ano != null && ano != 0)
							{
								competencia = String.format("%02d", mes) + "/" + ano.toString();
								gData = new GregorianCalendar(ano.intValue(), mes.intValue() - 1, 1);
							}
							else
							{
								gData = new GregorianCalendar(1900, 0, 1);
							}
							DocumentoTreeVO children = new DocumentoTreeVO();
							children.setId(id);
							children.setNome(file.getName() + "." + file.getSufix().toLowerCase());
							children.setData(competencia);
							children.setgData(gData);
							children.setLink(OrsegupsUtils.getLink(file));
							childrens.add(children);
						}
					}
					documento.setChildren(childrens);
					if (documentoAnt != null && documentoAnt.getNome() != null && documentoAnt.getNome().equals(documento.getNome()))
					{
						documentoAnt.getChildren().addAll(documento.getChildren());
					}
					else
					{
						documentos.add(documento);
					}
					documentoAnt = documento;
				}
			}

			for (DocumentoTreeVO doc : documentos)
			{
				Collections.sort(doc.getChildren(), new DocumentoComparator());
			}
			return documentos;
		}

     */

    public static List<EscalaRevezamentoVO> listaEscalaRevezamento(Long codesc, String data)
    {
	List<EscalaRevezamentoVO> revezamentos = new ArrayList<EscalaRevezamentoVO>();
	Connection connSapiens = PersistEngine.getConnection("VETORH");

	try
	{
	    GregorianCalendar gData = OrsegupsUtils.stringToGregorianCalendar(data, "dd/MM/yyyy");
	    GregorianCalendar dataI = new GregorianCalendar(gData.get(GregorianCalendar.YEAR), gData.get(GregorianCalendar.MONTH), gData.get(GregorianCalendar.DAY_OF_MONTH));
	    GregorianCalendar dataF = new GregorianCalendar(gData.get(GregorianCalendar.YEAR), gData.get(GregorianCalendar.MONTH), gData.get(GregorianCalendar.DAY_OF_MONTH));

	    dataI.add(GregorianCalendar.DAY_OF_MONTH, -15);
	    dataF.add(GregorianCalendar.DAY_OF_MONTH, +15);

	    for (GregorianCalendar dt = dataI; dt.compareTo(dataF) <= 0;)
	    {

		EscalaRevezamentoVO revezamento = new EscalaRevezamentoVO();
		revezamento.setData(NeoUtils.safeDateFormat(dt, "dd/MM/yyyy"));
		revezamento.setDiaSemana(OrsegupsUtils.getDayOfWeek(dt));

		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT esc.CodEsc, hr2.DesHor, tma.CodTma, hr1.CodHor ");
		sql.append(" FROM R006ESC esc  ");
		sql.append(" INNER JOIN R006TMA tma ON tma.CodEsc = esc.CodEsc ");
		sql.append(" INNER JOIN R006HOR hr1 ON hr1.CodEsc = esc.CodEsc ");
		sql.append(" INNER JOIN R004HOR hr2 ON hr2.CodHor = hr1.CodHor ");
		sql.append(" WHERE DATEDIFF(dd,tma.DatBas,?) % (SELECT COUNT(*) FROM R006HOR hr3 WHERE hr3.CodEsc = esc.CodEsc) + 1 = hr1.SeqReg ");
		sql.append(" AND hr1.CodEsc = ? ");
		sql.append(" GROUP BY esc.CodEsc, hr2.DesHor, tma.CodTma, hr1.CodHor ");

		PreparedStatement st = connSapiens.prepareStatement(sql.toString());
		st.setTimestamp(1, new Timestamp(dt.getTimeInMillis()));
		st.setLong(2, codesc);
		ResultSet rs = st.executeQuery();

		//Cargos
		while (rs.next())
		{
		    Integer turma = rs.getInt("CodTma");

		    switch (turma)
		    {
		    case 1:
			revezamento.setDescricao(rs.getString("DesHor"));
			revezamento.setTurma1(rs.getString("CodHor"));
			break;

		    case 2:
			revezamento.setTurma2(rs.getString("CodHor"));
			break;

		    case 3:
			revezamento.setTurma3(rs.getString("CodHor"));
			break;

		    case 4:
			revezamento.setTurma4(rs.getString("CodHor"));
			break;

		    case 5:
			revezamento.setTurma5(rs.getString("CodHor"));
			break;

		    case 6:
			revezamento.setTurma6(rs.getString("CodHor"));
			break;

		    case 7:
			revezamento.setTurma7(rs.getString("CodHor"));
			break;

		    case 8:
			revezamento.setTurma8(rs.getString("CodHor"));
			break;
		    }
		}
		revezamentos.add(revezamento);
		dt.set(Calendar.DAY_OF_MONTH, dt.get(Calendar.DAY_OF_MONTH) + 1);
		rs.close();
		st.close();
	    }
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connSapiens.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}

	return revezamentos;
    }

    public static String moverMarcacao(GregorianCalendar gDataApu, GregorianCalendar gDataAcc, String horacc, Long seqacc, Long numemp, Long tipcol, Long numcad, String op)
    {
	Connection connVetorh = PersistEngine.getConnection("VETORH");
	String retorno = "";

	try
	{
	    String numcra = buscaCracha(numemp, tipcol, numcad, gDataApu);

	    StringBuffer sql = new StringBuffer();

	    sql.append(" UPDATE R070ACC SET DatApu = ? WHERE NumCra = ? AND DatAcc = DATEADD(dd, 0, DATEDIFF(dd, 0, ?)) ");
	    sql.append(" AND HorAcc = (DATEPART(HOUR, ?) * 60) + DATEPART(MINUTE, ?) AND SeqAcc = ? ");

	    PreparedStatement st = connVetorh.prepareStatement(sql.toString());

	    st.setTimestamp(1, new Timestamp(gDataApu.getTimeInMillis()));
	    st.setString(2, numcra);
	    st.setTimestamp(3, new Timestamp(gDataAcc.getTimeInMillis()));
	    st.setTimestamp(4, new Timestamp(gDataAcc.getTimeInMillis()));
	    st.setTimestamp(5, new Timestamp(gDataAcc.getTimeInMillis()));
	    st.setLong(6, seqacc);

	    st.executeUpdate();
	    st.close();

	    sql = new StringBuffer();

	    sql.append(" UPDATE R066APU SET UsuVer = 389 WHERE NumEmp = ? AND TipCol = ? AND NumCad = ? AND DatApu = ? ");

	    PreparedStatement st2 = connVetorh.prepareStatement(sql.toString());

	    st2.setLong(1, numemp);
	    st2.setLong(2, tipcol);
	    st2.setLong(3, numcad);
	    st2.setTimestamp(4, new Timestamp(gDataAcc.getTimeInMillis()));

	    st2.executeUpdate();
	    st2.close();

	    retorno = "Marcação movida com sucesso";
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	    retorno = "Erro ao mover marcação.";
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
		retorno = "Erro ao inserir Autorização de Hora extra.";

	    }
	}
	return retorno;
    }

    private static String buscaCracha(Long numemp, Long tipcol, Long numcad, GregorianCalendar dataAtual)
    {
	Connection connVetorh = PersistEngine.getConnection("VETORH");
	String cracha = "";

	try
	{
	    StringBuffer query = new StringBuffer();
	    query.append(" SELECT hch.NumCra FROM R038HCH hch ");
	    query.append(" WHERE hch.NumEmp = ? AND hch.TipCol = ? AND hch.NumCad = ? AND hch.DatIni = (");
	    query.append(" SELECT MAX(hch2.DatIni) FROM R038HCH hch2 WHERE hch2.NumEmp = hch.NumEmp AND hch2.TipCol = hch.TipCol AND hch2.NumCad = hch.NumCad AND hch2.DatIni <= ?)");

	    Timestamp dataAtualTS = new Timestamp(dataAtual.getTimeInMillis());
	    PreparedStatement st = connVetorh.prepareStatement(query.toString());
	    st.setLong(1, numemp);
	    st.setLong(2, tipcol);
	    st.setLong(3, numcad);
	    st.setTimestamp(4, dataAtualTS);

	    ResultSet rs = st.executeQuery();

	    if (rs.next())
	    {
		cracha = rs.getString("NumCra");
	    }
	    rs.close();
	    st.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}

	return cracha;
    }

    public static String getTextoCobertura(Long taborg, Long numloc, String txtCobertura)
    {
	Connection connVetorh = PersistEngine.getConnection("VETORH");
	String txt = "";

	try
	{
	    StringBuffer query = new StringBuffer();
	    query.append(" SELECT orn.USU_CodCCU, orn.USU_LotOrn, orn.NomLoc FROM R016ORN orn ");
	    query.append(" WHERE orn.TabOrg = ? AND orn.NumLoc = ? ");

	    PreparedStatement st = connVetorh.prepareStatement(query.toString());
	    st.setLong(1, taborg);
	    st.setLong(2, numloc);

	    ResultSet rs = st.executeQuery();

	    if (rs.next())
	    {
		txt = "Colaborador realizando a operação de " + txtCobertura + " no posto " + rs.getString("USU_CodCCU") + " - " + rs.getString("USU_LotOrn") + " - " + rs.getString("NomLoc");
	    }
	    rs.close();
	    st.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}
	return txt;
    }

    public static void ajustarEscala(Long peresc, ColaboradorVO col, EscalaPostoVO escPosto) throws Exception
    {
	EscalaVO escala = new EscalaVO();
	escala = (EscalaVO) BeanUtils.cloneBean(col.getEscala());
	GregorianCalendar horarioInicio = new GregorianCalendar();
	GregorianCalendar horarioFinal = new GregorianCalendar();
	String descricaoNova = "";
	Boolean escalaInvertida = false;

	//Nos casos de escala de um dia para o outro EX: 00:00-07:00 / 19:00-23:59 inverte inicio e fim
	if (escPosto != null && escPosto.getHorariosHoje() != null && escPosto.getHorariosHoje().size() >= 2 && escPosto.getHorariosHoje().get(0).getDataInicial().get(Calendar.HOUR_OF_DAY) == 0 && escPosto.getHorariosHoje().get(0).getDataInicial().get(Calendar.MINUTE) == 0 && escPosto.getHorariosHoje().get(1).getDataFinal().get(Calendar.HOUR_OF_DAY) == 23 && escPosto.getHorariosHoje().get(1).getDataFinal().get(Calendar.MINUTE) == 59)
	{
	    escalaInvertida = true;
	}

	switch (peresc.intValue())
	{
	//Inicio / Inicio e Intervalo
	case 10:
	case 50:
	    if (escalaInvertida)
	    {
		if (escala.getHorarios().get(escala.getHorarios().size() - 1).getDataInicial() != null) {
		    horarioInicio = (GregorianCalendar) BeanUtils.cloneBean(escala.getHorarios().get(escala.getHorarios().size() - 1).getDataInicial());
		}
	    }
	    else
	    {	if (escala.getHorarios().get(0).getDataInicial() != null) {
		horarioInicio = (GregorianCalendar) BeanUtils.cloneBean(escala.getHorarios().get(0).getDataInicial());
	    }
	    }

	    if (horarioInicio.get(Calendar.HOUR_OF_DAY) != 0 && horarioInicio.get(Calendar.MINUTE) != 0)
	    {
		escala.getHorarios().get(0).setDataInicial((GregorianCalendar) escPosto.getHorariosHoje().get(0).getDataInicial().clone());
		descricaoNova = "<p><font color='blue'>" + escala.toString() + "</font></p>";
		escala.setDescricaoHorarios(descricaoNova);
		col.setEscala((EscalaVO) BeanUtils.cloneBean(escala));
	    }
	    break;
	    //Fim / Intervalo e Fim
	case 20:
	case 70:
	    //Nos casos de escala de um dia para o outro EX: 00:00-07:00 / 19:00-23:59 inverte inicio e fim
	    if (escalaInvertida)
	    {
		if (escala.getHorarios().get(0).getDataFinal() != null) {
		    if (escala.getHorarios().size() == 2) {
			horarioFinal = (GregorianCalendar) BeanUtils.cloneBean(escala.getHorarios().get(0).getDataFinal());
		    } else {//3
			horarioFinal = (GregorianCalendar) BeanUtils.cloneBean(escala.getHorarios().get(1).getDataFinal());
		    }
		}
	    }
	    else
	    {
		if (escala.getHorarios().get(escala.getHorarios().size() - 1).getDataFinal() != null) {
		    horarioFinal = (GregorianCalendar) BeanUtils.cloneBean(escala.getHorarios().get(escala.getHorarios().size() - 1).getDataFinal());
		}
	    }

	    if (horarioFinal.get(Calendar.HOUR_OF_DAY) != 23 && horarioFinal.get(Calendar.MINUTE) != 59)
	    {
		if (escalaInvertida) {
		    escala.getHorarios().get(escala.getHorarios().size() - 1).setDataFinal((GregorianCalendar) escPosto.getHorariosHoje().get(0).getDataFinal().clone());
		} else {
		    escala.getHorarios().get(escala.getHorarios().size() - 1).setDataFinal((GregorianCalendar) escPosto.getHorariosHoje().get(escPosto.getHorariosHoje().size() - 1).getDataFinal().clone());
		}
		descricaoNova = "<p><font color='blue'>" + escala.toString() + "</font></p>";
		escala.setDescricaoHorarios(descricaoNova);
		col.setEscala((EscalaVO) BeanUtils.cloneBean(escala));
	    }
	    break;
	    //Intervalo
	case 30:

	    break;

	    //Periodo Integral / Inicio e Fim
	case 40:
	case 60:
	    if (escalaInvertida)
	    {
		horarioInicio = new GregorianCalendar();
		horarioFinal = new GregorianCalendar();
		if (escala.getHorarios().get(escala.getHorarios().size() - 1).getDataInicial() != null) { 
		    horarioInicio = (GregorianCalendar) BeanUtils.cloneBean(escala.getHorarios().get(escala.getHorarios().size() - 1).getDataInicial());
		}
		if (escala.getHorarios().get(0).getDataFinal() != null) {
		    horarioFinal = (GregorianCalendar) BeanUtils.cloneBean(escala.getHorarios().get(0).getDataFinal());
		}
	    }
	    else
	    {
		horarioInicio = new GregorianCalendar();
		horarioFinal = new GregorianCalendar();
		if (escala.getHorarios().get(0).getDataInicial()!= null) {
		    horarioInicio = (GregorianCalendar) BeanUtils.cloneBean(escala.getHorarios().get(0).getDataInicial());
		}
		if (escala.getHorarios().get(escala.getHorarios().size() - 1).getDataFinal() != null) {
		    horarioFinal = (GregorianCalendar) BeanUtils.cloneBean(escala.getHorarios().get(escala.getHorarios().size() - 1).getDataFinal());
		}
	    }
	    if (horarioInicio.get(Calendar.HOUR_OF_DAY) != 0 || horarioInicio.get(Calendar.MINUTE) != 0)
	    {
		escala.getHorarios().get(0).setDataInicial((GregorianCalendar) escPosto.getHorariosHoje().get(0).getDataInicial().clone());
	    }
	    if (horarioFinal.get(Calendar.HOUR_OF_DAY) != 23 || horarioFinal.get(Calendar.MINUTE) != 59)
	    {
		escala.getHorarios().get(escala.getHorarios().size() - 1).setDataFinal((GregorianCalendar) escPosto.getHorariosHoje().get(escPosto.getHorariosHoje().size() - 1).getDataFinal().clone());
	    }

	    descricaoNova = "<p><font color='blue'>" + escala.toString() + "</font></p>";
	    escala.setDescricaoHorarios(descricaoNova); //aqui vai trocar de 16:00-20:00 / 21:00-null para 16:00-20:00 / 21:00-null
	    col.setEscala((EscalaVO) BeanUtils.cloneBean(escala));
	    break;
	}
    }
    
    public static Long buscarTipoHorarioColaborador(ColaboradorVO col) throws Exception{
	
	Long retorno = tipoHorarioTrocaHorario(col);
	if (retorno == null){
	    retorno = tipoHorarioTrocaEscala(col);
	    if (retorno == null){
		retorno = tipoHorarioEscalaColaborador(col);
	    }
	}
	
	return retorno;
    }
    
    private static Long tipoHorarioTrocaHorario(ColaboradorVO col){
	
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	try {
	    conn = PersistEngine.getConnection("VETORH");
	    
	    sql.append(" select top 1 hor.TipHor from R064THR thr "); 
	    sql.append(" inner join R004HOR hor on hor.CodHor = thr.CodHor ");
	    sql.append(" where thr.NumCad = ? and thr.NumEmp = ? and thr.DatIni = CONVERT(DATETIME, FLOOR(CONVERT(FLOAT(24), GETDATE()))) ");
	    sql.append(" and hor.TipHor = 5 ");
	    
	    pstm = conn.prepareStatement(sql.toString());
	    
	    pstm.setLong(1, col.getNumeroCadastro());
	    pstm.setLong(2, col.getNumeroEmpresa());

	    rs = pstm.executeQuery();

	    while (rs.next()) {
		return rs.getLong("TipHor");
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
	
	return null;
    }
    
    private static Long tipoHorarioTrocaEscala(ColaboradorVO col){
	
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	try {
	    conn = PersistEngine.getConnection("VETORH");
	    
	    sql.append(" select top 1 hor.TipHor from R064TES TES ");
	    sql.append(" inner join R006HOR esc on esc.CodEsc = TES.CodEsc ");
	    sql.append(" inner join R004HOR hor on hor.CodHor = esc.CodHor ");
	    sql.append(" where TES.NumCad = ? and tes.NumEmp = ? and CONVERT(DATETIME, FLOOR(CONVERT(FLOAT(24), GETDATE()))) between TES.DatIni and TES.DatFim ");
	    sql.append(" and hor.TipHor = 5 ");
	    
	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setLong(1, col.getNumeroCadastro());
	    pstm.setLong(2, col.getNumeroEmpresa());	    

	    rs = pstm.executeQuery();

	    while (rs.next()) {
		return rs.getLong("TipHor");
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
	
	return null;
    }
    
    private static Long tipoHorarioEscalaColaborador(ColaboradorVO col){
	
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	try {
	    conn = PersistEngine.getConnection("VETORH");
	    
	    sql.append(" select top 1 hor.TipHor from R034FUN fun ");
	    sql.append(" inner join R006HOR esc on esc.CodEsc = fun.codesc ");
	    sql.append(" inner join R004HOR hor on hor.CodHor = esc.CodHor ");
	    sql.append(" where fun.numcpf = ? and hor.TipHor = 5 ");
	    
	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setLong(1, col.getCpf());

	    rs = pstm.executeQuery();

	    while (rs.next()) {
		return rs.getLong("TipHor");
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
	
	return null;
    }

    public static void saveLog(String codccu, String texto)
    {
	QLGroupFilter groupFilter = new QLGroupFilter("AND");
	groupFilter.addFilter(new QLEqualsFilter("usu_codccu", codccu));
	groupFilter.addFilter(new QLEqualsFilter("usu_sitati", "S"));

	List<NeoObject> postos = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORH_USU_Vorg203nv8"), groupFilter);

	if (postos != null && !postos.isEmpty())
	{
	    NeoObject postoObject = postos.get(0);
	    NeoObject logPosto = AdapterUtils.createNewEntityInstance("QLLogPresencaPosto");

	    if (logPosto != null)
	    {
		EntityWrapper postoWrapper = new EntityWrapper(logPosto);
		postoWrapper.setValue("posto", postoObject);
		postoWrapper.setValue("dataLog", new GregorianCalendar());
		postoWrapper.setValue("textoLog", texto);
		postoWrapper.setValue("usuario", PortalUtil.getCurrentUser());

		PersistEngine.persist(logPosto);
	    }
	}
    }

    //	private static List<ColaboradorVO> listaTrocaEscala(GregorianCalendar dataAtual)
    //	{
    //		Connection connVetorh = PersistEngine.getConnection("VETORH");
    //		List<ColaboradorVO> colaboradores = new ArrayList<ColaboradorVO>();
    //
    //		try
    //		{
    //			StringBuffer query = new StringBuffer();
    //			query.append(" SELECT tes.NumEmp, tes.TipCol, tes.NumCad, tes.CodEsc, tes.CodTma FROM R064TES tes ");
    //			query.append(" WHERE tes.DatIni = cast(floor(cast(? as float)) as datetime)");
    //
    //			Timestamp dataAtualTS = new Timestamp(dataAtual.getTimeInMillis());
    //			PreparedStatement st = connVetorh.prepareStatement(query.toString());
    //			st.setTimestamp(1, dataAtualTS);
    //
    //			ResultSet rs = st.executeQuery();
    //
    //			while (rs.next())
    //			{
    //				ColaboradorVO col = new ColaboradorVO();
    //				col.setNumeroEmpresa(rs.getLong("NumEmp"));
    //				col.setTipoColaborador(rs.getLong("TipCol"));
    //				col.setNumeroCadastro(rs.getLong("NumCad"));
    //				col.setCodigoEscala(rs.getLong("CodEsc"));
    //				col.setTurmaEscala(rs.getLong("CodTma"));
    //				colaboradores.add(col);
    //			}
    //			rs.close();
    //			st.close();
    //		}
    //		catch (Exception e)
    //		{
    //			e.printStackTrace();
    //		}
    //		finally
    //		{
    //			try
    //			{
    //				connVetorh.close();
    //			}
    //			catch (SQLException e)
    //			{
    //				e.printStackTrace();
    //
    //			}
    //		}
    //
    //		return colaboradores;
    //	}

    public static Boolean buscaHorarioFlexivel(Long numemp, Long numcad)
    {

	Connection connVetorh = null;
	PreparedStatement st = null;
	ResultSet rs = null;
	Boolean horarioFlexivel = Boolean.FALSE;
	String empresaSql = "";
	if(numemp < 10){
	    empresaSql = "0"+String.valueOf(numemp);
	}else{
	    empresaSql = String.valueOf(numemp);
	}

	int horMinIni = 0;
	int horMinFim = 0;
	String horFim = "";
	String horIni = "";

	GregorianCalendar datAtu = new GregorianCalendar();
	String dataFiltro = NeoDateUtils.safeDateFormat(datAtu, "yyyy-MM-dd");
	EscalaVO escala = QLPresencaUtils.getEscalaColaborador(numcad, numemp, 1L);
	if(escala != null){
	    if(escala.getDescricao().contains("MRF") || escala.getDescricao().contains("RFL"))
	    {
		String descricao = escala.getDescricao();
		int posFin = descricao.indexOf("-");
		horIni = descricao.substring(0, posFin);

		posFin = descricao.indexOf("/");
		horFim = descricao.substring(posFin+1, descricao.length());

		int posIni = horFim.indexOf("-");
		posFin = horFim.indexOf(" ");
		horFim = horFim.substring(posIni+1, posFin);

		int cont1 = horIni.indexOf(":");
		int cont2 = horFim.indexOf(":");

		horMinIni = (Integer.parseInt(((horIni.substring(0,cont1))))*60 + Integer.parseInt(horIni.substring(cont1+1,horIni.length())));
		horMinFim = (Integer.parseInt(((horFim.substring(0,cont2))))*60 + Integer.parseInt(horFim.substring(cont2+1,horFim.length())));
	    }
	}

	try
	{
	    connVetorh = PersistEngine.getConnection("VETORH");
	    StringBuffer query = new StringBuffer();
	    query.append(" DECLARE @MARCACOES INT; ");
	    if(horMinIni > horMinFim){
		GregorianCalendar datAnt = new GregorianCalendar();
		datAnt.add(GregorianCalendar.DAY_OF_YEAR, -1);
		String dataFiltroIni = NeoDateUtils.safeDateFormat(datAnt, "yyyy-MM-dd");
		GregorianCalendar datAtual = new GregorianCalendar();

		//pegar data atual para verificar o horario que esta batendo o ponto
		GregorianCalendar dataPonto = new GregorianCalendar();
		int horMarcacao = dataPonto.get(GregorianCalendar.HOUR_OF_DAY);
		int minMarcacao = dataPonto.get(GregorianCalendar.MINUTE);
		int totalMarcacao = (horMarcacao * 60) + minMarcacao;

		if(totalMarcacao > (horMinIni-16) && totalMarcacao < (horMinIni+16)){
		    query.append(" SET @MARCACOES  = 0 "); 
		}else if (totalMarcacao > (horMinFim-16) && totalMarcacao < (horMinFim+16)){
		    query.append(" SET @MARCACOES = 0 ");
		}else{
		    query.append(" SET @MARCACOES = 1 ");
		}

	    }else{
		String numcadString = String.valueOf(numcad);
		if(numcadString.length() < 8){
		    for(int i = 0; i<8;i++){
			numcadString = 0+numcadString;
			if(numcadString.length() == 8){
			    break;
			}
		    }
		}

		query.append(" SET @MARCACOES  = (SELECT COUNT(*) FROM r070acc WHERE "
			+ "numcra LIKE '10"+empresaSql+"%"+numcadString+"' AND datacc = '"+dataFiltro+"' AND tipacc = 100);");
	    }

	    query.append(" select ho4.TipHor from r034fun fun ");
	    query.append(" inner join r006esc esc on esc.codesc = fun.codesc  ");
	    query.append(" inner join r006hor hor on hor.codesc = esc.codesc ");
	    query.append(" inner join r004hor ho4 on ho4.CodHor = hor.CodHor ");
	    query.append(" inner join R004MHR mhr on mhr.CodHor = ho4.CodHor ");
	    //query.append(" where numemp = ? and numcad = ? and tiphor = 3 ");
	    query.append(" where numemp = ? and numcad = ? and ((tiphor = 3) or (tiphor = 6 and usobat = 4 AND ((@MARCACOES = 1) or (@MARCACOES >= 1 and @MARCACOES <= 2)))) ");

	    st = connVetorh.prepareStatement(query.toString());
	    st.setLong(1, numemp);
	    st.setLong(2, numcad);

	    rs = st.executeQuery();

	    if (rs.next())
	    {
		horarioFlexivel = Boolean.TRUE;
	    }

	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    OrsegupsUtils.closeConnection(connVetorh, st, rs);

	}

	return horarioFlexivel;

    }

    public static List<ValeTransporteVO> listaValeTransporte(Long numemp, Long tipcol, Long numcad)
    {
	List<ValeTransporteVO> vales = new ArrayList<ValeTransporteVO>();

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	ResultSet rs = null;

	try
	{
	    /*String query = 	" select fun.nomfun, lvt.nomlin, vt.usu_qtdEnt, vt.USU_MesAno "+
					" from r034fun fun  "+
					" join usu_t028cmvt vt on (fun.numcad = vt.USU_NumCad and fun.numemp = vt.USU_NumEmp and fun.tipcol = vt.USU_TipCol ) "+
					" join r028LVT lvt on (vt.usu_codlin = lvt.codlin) "+
					" where (left(CONVERT(varchar(12), vt.USU_MesAno, 12),4)) >= (left(CONVERT(varchar(12), getdate(), 12),4)) "+
					" and usu_numemp = ? and usu_tipcol = ? and usu_numcad = ?   ";
	     */
	    String query = 	"select fun.numcad, fun.nomfun, lvt.nomlin,  sum(usu_qtdlin) as usu_qtdEnt, vt.USU_MesAno"+
		    " from r034fun fun "+ 
		    " join usu_t028mcvt vt on (fun.numcad = vt.USU_NumCad and fun.numemp = vt.USU_NumEmp and fun.tipcol = vt.USU_TipCol )"+ 
		    " join r028LVT lvt on (vt.usu_codlin = lvt.codlin)"+ 
		    " where 1=1 and (left(CONVERT(varchar(12), vt.usu_datlan, 12),4)) >= (left(CONVERT(varchar(12), getdate(), 12),4))"+ 
		    " and usu_numemp = ? and usu_tipcol = ? and usu_numcad = ? and usu_pervtr = 'N'"+
		    " and vt.USU_MesAno = (select max(vt1.USU_MesAno) from usu_t028cmvt vt1 where vt1.usu_numcad = fun.numcad and vt1.usu_numemp = fun.numemp and vt1.usu_tipcol = fun.tipcol) " +
		    " group by fun.numcad,fun.nomfun, lvt.nomlin,vt.USU_MesAno";

	    PreparedStatement stValeTransporte = connVetorh.prepareStatement(query);
	    stValeTransporte.setLong(1, numemp);
	    stValeTransporte.setLong(2, tipcol);
	    stValeTransporte.setLong(3, numcad);
	    rs = stValeTransporte.executeQuery();

	    //Afastamentos
	    while (rs.next())
	    {
		ValeTransporteVO valeTransporte = new ValeTransporteVO();

		valeTransporte.setLinha(rs.getString("nomlin"));
		valeTransporte.setQuantidade(rs.getString("usu_qtdEnt"));

		GregorianCalendar gc = new GregorianCalendar();
		gc.setTimeInMillis(rs.getDate("USU_MesAno").getTime());
		valeTransporte.setData(NeoUtils.safeDateFormat(gc,"MM/yyyy"));

		vales.add(valeTransporte);
	    }

	    rs.close();
	    stValeTransporte.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}
	return vales;
    }

    public static Boolean contratoControlaASO(Long numctr,Long numposto)
    {
	Connection connVetorh = null;
	PreparedStatement st = null;
	ResultSet rs = null;
	Boolean retorno = Boolean.FALSE;

	try
	{
	    connVetorh = PersistEngine.getConnection("SAPIENS");
	    StringBuffer query = new StringBuffer();

	    query.append(" select usu_atuaso from usu_t160ctr ctr  ");
	    query.append(" inner join usu_t160cvs cvs on ctr.usu_numctr = cvs.usu_numctr and ctr.usu_codemp = cvs.usu_codemp and ctr.usu_codfil = cvs.usu_codfil ");
	    query.append(" where ctr.usu_numctr = ? and cvs.usu_numpos = ? ");


	    st = connVetorh.prepareStatement(query.toString());
	    st.setLong(1, numctr);
	    st.setLong(2, numposto);

	    rs = st.executeQuery();

	    if (rs.next())
	    {
		if (NeoUtils.safeOutputString(rs.getString(1)).equals("S")){
		    retorno = Boolean.TRUE;
		}
	    }

	    OrsegupsUtils.closeConnection(connVetorh, st, rs);

	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    OrsegupsUtils.closeConnection(connVetorh, st, rs);

	}

	return retorno;

    }

    /**
     * 
     * @param numctr
     * @param numposto
     * @param controlaASO
     * @return 0 - Erro, 1- OK
     */
    public static int atualizaASO(Long numctr, Long numemp, Long numfil, Long numposto, String codSer, String usuario, String controlaASO) {
	EntityManager entityManager = PersistEngine.getEntityManager("SAPIENS");

	String retorno = "";

	try
	{
	    String sqlHE = " update usu_t160ctr  set usu_t160ctr.usu_atuaso = ? where  usu_t160ctr.usu_numctr = ? and usu_t160ctr.usu_sitctr='A' and exists (select 1 from usu_t160cvs cvs where cvs.usu_numctr = usu_t160ctr.usu_numctr and cvs.usu_numpos = ? ) ";
	    Query updateSit = entityManager.createNativeQuery(sqlHE);
	    updateSit.setParameter(1, (controlaASO.equals("1")? "S":"N"));
	    updateSit.setParameter(2, numctr);
	    updateSit.setParameter(3, numposto);

	    int regsUp = updateSit.executeUpdate();

	    if (regsUp > 0){

		Long seqObs = retornaSequenciaObservacaoContrato(numemp, numfil, numctr);


		String obs = (controlaASO.equals("1")? "Este contrato foi marcado para controlar ASO. Lançamento feito por: "+usuario:"Este contrato foi marcado para não controlar ASO. Lançamento feito por: "+usuario);

		/*
		 * Usuário de lançamento no sapiens vai ser o código 1166L - fusion (usuário criado somente para isso) 
		 */
		geraObservacao(numemp, numfil, numctr, seqObs, numposto, codSer, "A", obs, 1166L, new GregorianCalendar(), NeoUtils.safeLong(ContratoUtils.retornaHoraFormatoSapiens(new GregorianCalendar())), "");
	    }

	    return 1;

	}
	catch (Exception ex)
	{

	    ex.printStackTrace();
	    retorno = "Erro ao atualizar controle ASO.";
	    return 0;
	}

    }

    /**
     * Busca o número da ultima observação inserida USU_T160obs e incrementa +1
     * select * FROM USU_T160obs where usu_codemp = 18 and usu_codfil = 1 and usu_numctr = 331478 oRDER BY usu_seqobs DESC
     * 
     * @return numero nova obs
     */
    public static  Long retornaSequenciaObservacaoContrato(Long usu_codemp, Long usu_codfil, Long numContrato)
    {
	Long numeroObsNovo = new Long (1);

	StringBuffer query = new StringBuffer();

	query.append("SELECT usu_seqobs FROM USU_T160obs where usu_codemp = "+usu_codemp+" and usu_codfil = "+usu_codfil+" and usu_numctr = "+numContrato+" ORDER BY usu_seqobs DESC" );

	ResultSet rs = OrsegupsContratoUtils.getResultSet(query.toString(), "SAPIENS");

	if (rs != null)
	{
	    try
	    { 
		Long numeroUltimoObs = rs.getLong("usu_seqobs");
		numeroObsNovo = numeroUltimoObs + 1;
		rs.close();
		rs =null;
	    }
	    catch (SQLException e)
	    {
		System.out.println("[QL] - ERRO: Falha ao verificar USU_T160obs para retornaSequenciaObservacaoContrato.");
		e.printStackTrace();
	    }
	}

	return numeroObsNovo;

    }

    public static void geraObservacao(Long usu_codemp, Long usu_codfil, Long usu_numctr, Long usu_seqobs, Long usu_numpos, String usu_codser,
	    String usu_tipobs, String usu_txtobs, Long usu_usumov, GregorianCalendar usu_datmov, Long usu_hormov, String usu_codope) throws Exception
    {
	/*INSERT INTO USU_T160OBS (USU_CODEMP,USU_CODFIL,USU_NUMCTR,USU_SEQOBS,USU_NUMPOS,USU_CODSER, USU_TIPOBS,USU_TXTOBS,USU_USUMOV,USU_DATMOV,USU_HORMOV,USU_CODOPE) \
				VALUES (vCodEmp, vCodFil,vNumCtr,vSeqObs,vNumPos,vCodSer,atipobs,vTxtObs,CodUsu;vDatAtu,HorSis,'O')	
	 */
	Connection connection = OrsegupsUtils.getSqlConnection("SAPIENS");
	StringBuffer query = new StringBuffer();

	query.append("INSERT INTO USU_T160OBS" +
		" (USU_CODEMP,USU_CODFIL,USU_NUMCTR,USU_SEQOBS,USU_NUMPOS,USU_CODSER, " +
		"USU_TIPOBS,USU_TXTOBS,USU_USUMOV,USU_DATMOV,USU_HORMOV,USU_CODOPE, USU_NUMRMC) ");

	query.append("VALUES ("+usu_codemp +", "+ usu_codfil +", "+ usu_numctr +", "+ usu_seqobs +", "+ usu_numpos +", '"+ usu_codser 
		+"', '"+ usu_tipobs +"', '"+ usu_txtobs +"', "+ usu_usumov +", '"+ ContratoUtils.retornaDataFormatoSapiens(usu_datmov) +"', "+ usu_hormov +", '"+ usu_codope+"', 0)") ;

	PreparedStatement st = null;
	try
	{	
	    st = connection.prepareStatement(query.toString());
	    st.executeUpdate();
	    System.out.println("[QL] - Contrato: "+usu_numctr+" - Posto: " +usu_numpos+ " - obs: "+usu_seqobs);
	}

	catch (Exception e) {
	    if (e instanceof WorkflowException)
	    {
		throw (WorkflowException) e;
	    }
	    else
	    {
		System.out.println("[QL] - Erro ao gerar obs posto no Fluxo de contratos."+ e.getMessage());
		throw new Exception("Erro ao gerar Observações posto no Fluxo de contratos. "+e);
	    }
	}
	finally
	{
	    if (st != null)
	    {
		try
		{
		    st.close();
		    connection.close();
		}
		catch (SQLException e)
		{
		    System.out.println("[FLUXO CONTRATOS] - Erro ao fechar o statement");
		    e.printStackTrace();
		}
	    }

	}
    }

    public static String encodeUsuarioString(String usuario){
	try {
	    return URLEncoder.encode(usuario,"UTF-8");
	} catch (UnsupportedEncodingException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	    return usuario;
	}
    }


    public static List<FinancasTituloVO> listaFinancasPosto(long numctr, long numpos) {
	List<FinancasTituloVO> listaFinancas = new ArrayList<FinancasTituloVO>();


	Connection connSapiens = PersistEngine.getConnection("SAPIENS");
	PreparedStatement pstFinancas = null;
	ResultSet rsFinancas = null;

	try
	{
	    StringBuffer sql = new StringBuffer();


	    sql.append(" select tcr.numtit, tcr.sitTit, nfv.VlrBse ,  (tcr.VlrOri - tcr.VlrAbe) vlrPag,  tcr.VlrOri,  tcr.VlrAbe,  tcr.DatEmi,  tcr.VctPro, tcr.DatPpt datPag ");
	    sql.append(" from e160cvs cvs "); 
	    sql.append(" join e140nfv nfv on( cvs.CodEmp = nfv.CodEmp and cvs.CodFil = nfv.CodFil and cvs.CodSnf = nfv.CodSnf and cvs.NumNfv = nfv.NumNfv) ");
	    sql.append(" left join e301tcr tcr on (nfv.CodEmp = tcr.CodEmp and nfv.CodFil = tcr.CodFil and nfv.CodSnf = tcr.CodSnf and nfv.NumNfv = tcr.NumNfv) ");
	    sql.append(" where cvs.numctr = ? and cvs.USU_SEQAGR = ? ");
	    sql.append(" order by tcr.datemi desc ");

	    pstFinancas = connSapiens.prepareStatement(sql.toString());
	    pstFinancas.setLong(1, numctr);
	    pstFinancas.setLong(2, numpos);
	    rsFinancas = pstFinancas.executeQuery();
	    DecimalFormat formatoReal = new DecimalFormat("#,##0.00");
	    while(rsFinancas.next()){
		FinancasTituloVO financaVo = new FinancasTituloVO();
		financaVo.setNumtit(rsFinancas.getString("numtit"));
		financaVo.setSitTit(rsFinancas.getString("sitTit"));

		financaVo.setVlrBco(NeoUtils.safeFormat(formatoReal, rsFinancas.getBigDecimal("vlrBse")));
		financaVo.setVlrAbe(NeoUtils.safeFormat(formatoReal, rsFinancas.getBigDecimal("vlrAbe")));
		financaVo.setVlrOri(NeoUtils.safeFormat(formatoReal, rsFinancas.getBigDecimal("vlrOri")));
		financaVo.setVlrPag(NeoUtils.safeFormat(formatoReal, rsFinancas.getBigDecimal("vlrPag")));

		GregorianCalendar gcVctPro = new GregorianCalendar();
		gcVctPro.setTimeInMillis(rsFinancas.getDate("vctPro").getTime());
		GregorianCalendar gcDatEmi = new GregorianCalendar();
		gcDatEmi.setTimeInMillis(rsFinancas.getDate("datEmi").getTime());
		GregorianCalendar gcDatPag = new GregorianCalendar();
		gcDatPag.setTimeInMillis(rsFinancas.getDate("datPag").getTime());

		financaVo.setVctPro(NeoUtils.safeDateFormat(gcVctPro,"dd/MM/yyyy"));
		financaVo.setDatEmi(NeoUtils.safeDateFormat(gcDatEmi,"dd/MM/yyyy"));
		financaVo.setDatPag(NeoUtils.safeDateFormat(gcDatPag,"dd/MM/yyyy"));

		listaFinancas.add(financaVo);
	    }
	    rsFinancas.close();

	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    OrsegupsUtils.closeConnection(connSapiens, pstFinancas, rsFinancas);
	}
	return listaFinancas;
    }

    public static List<X8VO> listaX8Colabordor(long numcpf, GregorianCalendar datIni, GregorianCalendar datFim, String evt) {
	List<X8VO> listaEventosX8 = new ArrayList<X8VO>();

	Connection connSigma = PersistEngine.getConnection("SIGMA90");
	PreparedStatement pstX8 = null;
	ResultSet rsX8 = null;

	try
	{
	    StringBuffer sql = new StringBuffer();


	    sql.append(" select h.DT_RECEBIDO, h.DT_VIATURA_DESLOCAMENTO, h.dt_fechamento, h.cd_evento, central.id_central+'['+central.PARTICAO+']' conta, central.RAZAO, h.TX_OBSERVACAO_FECHAMENTO  from dbo.VIEW_HISTORICO h with(NOLOCK) ");
	    sql.append(" join dbcentral central with(NOLOCK) on ( h.CD_CLIENTE = central.cd_cliente) ");
	    sql.append(" where h.cd_viatura is not null  "); //--cd_viatura está ligada com outra conta no campo cpf
	    if ("0".equals(evt)){
		sql.append("and  h.cd_evento = 'XXX8' ");
	    }
	    sql.append(" and h.cd_viatura = ( select v.cd_viatura from [cacupe\\sql02].Fusion_Producao.dbo.D_RHViaturaColaborador vc join [cacupe\\sql02].Fusion_Producao.dbo.X_SIGMA90VIATURA v on (v.neoId = vc.viatura_neoId ) where vc.cpfint =  ? ) ");

	    if (datIni != null && datFim != null){
		sql.append(" and h.DT_RECEBIDO >= ? and h.DT_RECEBIDO <= ? ");
	    }
	    sql.append(" order by h.dt_viatura_deslocamento desc, 3  ");



	    pstX8 = connSigma.prepareStatement(sql.toString());
	    pstX8.setLong(1, numcpf); //15L

	    if (datIni != null && datFim != null){

		datIni.set(GregorianCalendar.HOUR, 0);
		datIni.set(GregorianCalendar.MINUTE, 0);
		datIni.set(GregorianCalendar.SECOND, 0);

		datFim.set(GregorianCalendar.HOUR, 0);
		datFim.set(GregorianCalendar.MINUTE, 0);
		datFim.set(GregorianCalendar.SECOND, 0);

		java.sql.Date dtIni = new java.sql.Date(datIni.getTimeInMillis());
		java.sql.Date dtFim = new java.sql.Date(datFim.getTimeInMillis());
		pstX8.setDate(2, dtIni);
		pstX8.setDate(3, dtFim);
	    }

	    rsX8 = pstX8.executeQuery();
	    DecimalFormat formatoReal = new DecimalFormat("#,##0.00");

	    while(rsX8.next()){
		X8VO x8vo = new X8VO();

		x8vo.setConta(rsX8.getString("conta"));
		x8vo.setRazao(rsX8.getString("RAZAO"));
		x8vo.setObservacaoFechamento(rsX8.getString("TX_OBSERVACAO_FECHAMENTO"));

		GregorianCalendar gcDtRecebido = new GregorianCalendar();
		gcDtRecebido.setTimeInMillis(rsX8.getTimestamp("DT_RECEBIDO").getTime());
		GregorianCalendar gcDtDeslocamento = new GregorianCalendar();
		gcDtDeslocamento.setTimeInMillis(rsX8.getTimestamp("DT_VIATURA_DESLOCAMENTO").getTime());
		GregorianCalendar gcDtFechamento = new GregorianCalendar();
		gcDtFechamento.setTimeInMillis(rsX8.getTimestamp("dt_fechamento").getTime());


		x8vo.setDtRecebido(NeoUtils.safeDateFormat(gcDtRecebido,"dd/MM/yyyy HH:mm:ss"));
		x8vo.setDtViaturaDeslocamento(NeoUtils.safeDateFormat(gcDtDeslocamento,"dd/MM/yyyy HH:mm:ss"));
		x8vo.setDtFechamento(NeoUtils.safeDateFormat(gcDtFechamento,"dd/MM/yyyy HH:mm:ss"));
		x8vo.setCodigo(rsX8.getString("cd_evento"));

		listaEventosX8.add(x8vo);
	    }
	    rsX8.close();

	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    OrsegupsUtils.closeConnection(connSigma, pstX8, rsX8);
	}
	return listaEventosX8;
    }


    public static boolean existeEventos(long numcpf, GregorianCalendar datIni, GregorianCalendar datFim) {
	boolean retorno = false;
	Connection connSigma = PersistEngine.getConnection("SIGMA90");
	PreparedStatement pstX8 = null;
	ResultSet rsX8 = null;

	try
	{
	    StringBuffer sql = new StringBuffer();


	    sql.append(" select * from dbo.VIEW_HISTORICO h with(NOLOCK) ");
	    sql.append(" join dbcentral central with(NOLOCK) on ( h.CD_CLIENTE = central.cd_cliente) ");
	    sql.append(" where h.cd_evento = 'XXX8' and h.cd_viatura is not null  "); //--cd_viatura está ligada com outra conta no campo cpf
	    sql.append(" and h.cd_viatura = (select v.cd_viatura from [cacupe\\sql02].Fusion_Producao.dbo.D_RHViaturaColaborador vc join [cacupe\\sql02].Fusion_Producao.dbo.X_SIGMA90VIATURA v on (v.neoId = vc.viatura_neoId ) where vc.cpfint =  ?) ");

	    if (datIni != null && datFim != null){
		sql.append(" and h.DT_RECEBIDO between ? and ? ");
	    }
	    //sql.append(" order by h.dt_recebido desc, 3  ");


	    pstX8 = connSigma.prepareStatement(sql.toString());
	    pstX8.setLong(1, numcpf); //15L

	    if (datIni != null && datFim != null){
		java.sql.Date dtIni = new java.sql.Date(datIni.getTimeInMillis());
		java.sql.Date dtFim = new java.sql.Date(datFim.getTimeInMillis());
		pstX8.setDate(2, dtIni);
		pstX8.setDate(3, dtFim);
	    }

	    rsX8 = pstX8.executeQuery();
	    DecimalFormat formatoReal = new DecimalFormat("#,##0.00");

	    if(rsX8.next()){
		retorno = true;
	    }
	    return retorno;
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	    return false;
	}
	finally
	{
	    OrsegupsUtils.closeConnection(connSigma, pstX8, rsX8);
	}
    }

    public static boolean existeVinculoViatura(long numcpf) {
	boolean retorno = false;
	Connection connSigma = PersistEngine.getConnection("SIGMA90");
	PreparedStatement pstVinculo = null;
	ResultSet rsVinculo = null;

	try
	{
	    StringBuffer sql = new StringBuffer();

	    sql.append(" select v.cd_viatura from [cacupe\\sql02].Fusion_Producao.dbo.D_RHViaturaColaborador vc join [cacupe\\sql02].Fusion_Producao.dbo.X_SIGMA90VIATURA v on (v.neoId = vc.viatura_neoId ) where vc.cpfint =  ? ");

	    pstVinculo = connSigma.prepareStatement(sql.toString());
	    pstVinculo.setLong(1, numcpf); 

	    rsVinculo = pstVinculo.executeQuery();

	    if(rsVinculo.next()){
		retorno = true;
	    }
	    return retorno;
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	    return false;
	}
	finally
	{
	    OrsegupsUtils.closeConnection(connSigma, pstVinculo, rsVinculo);
	}
    }

    public static String validacaoEventosVinculos(long numcpf, GregorianCalendar datIni, GregorianCalendar datFim) {
	String retorno = "";
	boolean temVinculo = false;
	boolean temX8 = false;


	temX8 = existeEventos(numcpf, datIni, datFim);
	temVinculo = existeVinculoViatura(numcpf);
	//verificar sem tem vinculo no fusion
	//verificar se tem x8 no sigma


	if (!temVinculo){
	    retorno += "Não há vinculo do colaborador com a viatura.";
	}else{
	    if (!temX8){
		retorno += "Não há eventos XXX8.";
	    }else{
		retorno = "OK";
	    }

	}

	return retorno;

    }

    public static String retornaObservacoesComerciaisContrato(long numctr, long numemp) {
	String retorno = "";
	Connection conn = PersistEngine.getConnection("SAPIENS");
	PreparedStatement pstObs = null;
	ResultSet rs = null;

	try
	{
	    StringBuffer sql = new StringBuffer();

	    sql.append(" select usu_obscom from usu_t160ctr where usu_numctr = "+numctr+" and usu_codemp = "+numemp);
	    pstObs = conn.prepareStatement(sql.toString());

	    rs = pstObs.executeQuery();

	    if(rs.next()){
		retorno = rs.getString("usu_obscom");
	    }
	    return retorno;
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	    return null;
	}
	finally
	{
	    OrsegupsUtils.closeConnection(conn, pstObs, rs);
	}
    }

    public static List<FichaEpiVO> listaFichaEpi(String numcpf){

	List<FichaEpiVO> retorno = new ArrayList<FichaEpiVO>();
	ArrayList<FichaEpiVO> retornoProcessado = new ArrayList<FichaEpiVO>();
	//1- empresa, 2-tipcol, 3-numcad
	StringBuilder sql = new StringBuilder();

	/*sql.append(" select epi.neoId, wf.code, wf.startDate from D_FUEProcessoFichaUniformeEPI epi ");
		sql.append(" join dbo.WFProcess wf on (epi.neoId = wf.entity_neoId) ");
		sql.append(" where epi.colaborador_neoId = (select xusu.neoId from X_VETORHUSUFUNFUSION xusu where substring(xusu.pk,3,2) = ? and substring(xusu.pk,5,1) = ? and  cast(substring(xusu.pk,6,9) as integer) = ?) ");
		//sql.append(" and wf.saved = 1 and wf.processState <> 2 ");
		sql.append(" order by wf.code ");*/

	/*sql.append(" select epi.neoId, wf.code, wf.startDate, epi.colaborador_neoId from D_FUEProcessoFichaUniformeEPI epi "); 
		 sql.append(" inner join dbo.WFProcess wf on (epi.neoId = wf.entity_neoId)  ");
		 sql.append(" where epi.colaborador_neoId in ( ");
		 sql.append("      select xusu.neoId from X_VETORHUSUFUNFUSION xusu "); 
		 sql.append("      inner join [FSOODB04\\SQL02].vetorh.dbo.r034fun fun  on cast(replace(substring(xusu.pk,3,2),'-','') as integer) = fun.numemp and substring(xusu.pk,5,1) = cast(fun.tipcol as varchar) and  substring(xusu.pk,6,9)  = fun.numcad ");
		 sql.append("      where fun.numcpf = ? "); 
		 sql.append(" ) "); 
		 //sql.append(" and wf.saved = 1 and wf.processState <> 2 "); 
		 sql.append(" order by wf.startDate desc ");*/

	sql.append(" select epi.neoId from D_FCEFichaUniformeEpi epi "); 
	sql.append(" inner join [FSOODB04\\SQL02].vetorh.dbo.r034fun fun on ( fun.numcpf = ? and epi.matricula = fun.numcad and epi.empresa = fun.numemp) order by epi.neoId ");

	Connection conn = PersistEngine.getConnection("");
	PreparedStatement pstEPI = null;
	ResultSet rs = null;

	try
	{

	    pstEPI = conn.prepareStatement(sql.toString());
	    pstEPI.setString(1, numcpf);

	    rs = pstEPI.executeQuery();

	    while(rs.next()){
		FichaEpiVO epiVo = new FichaEpiVO();
		epiVo.setNeoId(rs.getString("neoId"));
		//epiVo.setCode(rs.getString("code"));
		//epiVo.setStartDate(NeoUtils.safeDateFormat(rs.getTimestamp("startDate"), "dd/MM/yyyy HH:mm"));

		retorno.add(epiVo);
	    }

	    if ( retorno.size() > 0 ){

		for (FichaEpiVO epiObj : retorno){

		    // lista de entregas abertas
		    List<NeoObject> lFicha = PersistEngine.getObjects( AdapterUtils.getEntityClass("FCEFichaUniformeEpi") , new QLEqualsFilter("neoId", NeoUtils.safeLong( epiObj.getNeoId() ) ) );
		    NeoObject ficha = null;
		    for(NeoObject obj: lFicha){
			ficha = obj;
		    }
		    EntityWrapper wFicha = new EntityWrapper(ficha);

		    Collection<NeoObject> listaEntregas = (Collection<NeoObject>) wFicha.findField("listaEntregas").getValues();
		    for (NeoObject oEntrega: listaEntregas){
			EntityWrapper wEntrega = new EntityWrapper(oEntrega);


			NeoObject oProtudo = (NeoObject) wEntrega.findValue("produto");
			EntityWrapper wProduto = new EntityWrapper(oProtudo);

			FichaEpiVO epi = new FichaEpiVO();

			epi.setCode(epiObj.getCode());
			epi.setNeoId(epiObj.getNeoId());
			epi.setStartDate(epiObj.getDataEntrega());

			epi.setCodigoProduto(NeoUtils.safeOutputString(wProduto.findValue("codpro")));
			epi.setDescricao(NeoUtils.safeOutputString(wProduto.findValue("despro")));
			epi.setDerivacao(NeoUtils.safeOutputString(wEntrega.findValue("derivacao.desder")));

			epi.setDataEntrega( NeoUtils.safeDateFormat( (GregorianCalendar) wEntrega.findValue("dataEntrega"), "dd/MM/yyyy") );
			epi.setQuantidadeEntrega( NeoUtils.safeOutputString( wEntrega.findValue("quantidadeEntregue")) );

			Collection<NeoObject> listaDevolucoes = (Collection<NeoObject>) wEntrega.findField("listaDevolucoes").getValues();
			for (NeoObject oDevolucao: listaDevolucoes){
			    EntityWrapper wDevolucao = new EntityWrapper(oDevolucao);

			    epi.setDataDevolucao(NeoUtils.safeDateFormat( (GregorianCalendar) wDevolucao.findValue("DataDevolucao"), "dd/MM/yyyy") );
			    epi.setQuantidadeDevolucao(NeoUtils.safeOutputString( wDevolucao.findValue("quantidadeDevolucao")) );

			}
			retornoProcessado.add(epi);
		    }
		}
	    }
	    return retornoProcessado;



	}
	catch (Exception e)
	{
	    e.printStackTrace();
	    return null;
	}
	finally
	{
	    OrsegupsUtils.closeConnection(conn, pstEPI, rs);
	}

	/*
	 *  select substring(pk,3,2), substring(pk,5,1) , cast(substring(pk,6,8) as integer),  * from X_VETORHUSUFUNFUSION where pk like '%7688%'
	 *	select top 100 * from USU_V034FUSION where substring(pk,3,2) = '18' and substring(pk,5,1) = '1' and  cast(substring(pk,6,8) as integer) = 768
	 */
    }

    public static ArrayList<Long[]> getEmpresasColaborador(String numcpf) {
	ArrayList<Long[]> retorno = new ArrayList<Long[]>();
	Connection conn = PersistEngine.getConnection("VETORH");
	PreparedStatement pstCads = null;
	ResultSet rs = null;

	try
	{
	    StringBuffer sql = new StringBuffer();

	    sql.append(" select numemp, tipcol, numcad from r034fun where numcpf = ? order by numemp ");
	    pstCads = conn.prepareStatement(sql.toString());
	    pstCads.setLong(1, NeoUtils.safeLong(numcpf) );

	    rs = pstCads.executeQuery();

	    if(rs.next()){
		Long ret[] = new Long[3];
		ret[0] = rs.getLong("numemp");
		ret[1] = rs.getLong("tipcol");
		ret[2] = rs.getLong("numcad");
		retorno.add(ret);
	    }
	    return retorno;
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	    return retorno;
	}
	finally
	{
	    OrsegupsUtils.closeConnection(conn, pstCads, rs);
	}
    }

    public static ColaboradorVO buscaFichaColaboradorArquivo(long numempL, long numcadL, long numCpf)
    {

	Connection connVetorh = PersistEngine.getConnection("VETORH");
	ColaboradorVO colaborador = null;
	EscalaVO escala = new EscalaVO();

	try
	{
	    /*** Recupera Colaboradores ***/
	    StringBuffer queryColaborador = new StringBuffer();

	    queryColaborador.append(" SELECT top 10  fun.NumEmp, fun.TipCol, fun.NumCad, fun.NomFun, fun.NumCpf, fun.DatNas, fun.TipSex, fun.DatAdm,  "); 
	    queryColaborador.append(" fun.EmiCar, fun.USU_DepEmp, fun.USU_MatRes, pai.NomPai,cid.EstCid, cid.NomCid, bai.NomBai, cpl.EndCep, cpl.EndNum, cpl.EndRua, cpl.EndCpl, cpl.NumTel  ");
	    queryColaborador.append(" FROM R034FUN fun  ");  
	    queryColaborador.append(" LEFT JOIN R034CPL cpl ON cpl.NumEmp = fun.NumEmp AND cpl.TipCol = fun.TipCol AND cpl.NumCad = fun.NumCad  "); 
	    queryColaborador.append(" LEFT JOIN R074CID cid ON cid.CodCid = cpl.CodCid  "); 
	    queryColaborador.append(" LEFT JOIN R074BAI bai ON bai.CodCid = cpl.CodCid AND bai.CodBai = cpl.CodBai  "); 
	    queryColaborador.append(" LEFT JOIN R074PAI pai ON pai.CodPai = cid.CodPai  "); 
	    queryColaborador.append(" LEFT JOIN R034FOT fot ON fot.NumCad = fun.NumCad AND fot.NumEmp = fun.NumEmp AND fot.TipCol = fun.TipCol  "); 
	    queryColaborador.append(" where fun.numcpf = '"+numCpf+"' ");
	    /*queryColaborador.append(" and fun.NumCad = "+numcadL+" ");
			queryColaborador.append(" and fun.NumEmp = "+numempL+" ");*/

	    PreparedStatement stColaborador = connVetorh.prepareStatement(queryColaborador.toString());

	    ResultSet rsColaborador = stColaborador.executeQuery();

	    if (rsColaborador.next())
	    {
		colaborador = new ColaboradorVO();
		colaborador.setNumeroEmpresa(rsColaborador.getLong("NumEmp"));
		colaborador.setTipoColaborador(rsColaborador.getLong("TipCol"));
		colaborador.setNumeroCadastro(rsColaborador.getLong("NumCad"));
		colaborador.setNomeColaborador(rsColaborador.getString("NomFun"));
		colaborador.setControlaAcesso("S".equals(rsColaborador.getString("EmiCar")));
		colaborador.setCpf(rsColaborador.getLong("NumCpf"));
		colaborador.setSexo(rsColaborador.getString("TipSex"));
		colaborador.setDataNascimento(rsColaborador.getDate("DatNas"));
		colaborador.setDataAdmissao(rsColaborador.getDate("datAdm"));
		colaborador.setMatrizResponsabilidade(rsColaborador.getString("USU_MatRes"));
		colaborador.setTelefone(rsColaborador.getString("NumTel"));

		EnderecoVO endereco = new EnderecoVO();
		if (rsColaborador.getString("NomBai") != null)
		{
		    endereco.setBairro(rsColaborador.getString("NomBai"));
		    endereco.setCep(rsColaborador.getString("EndCep"));
		    endereco.setCidade(rsColaborador.getString("NomCid"));
		    endereco.setComplemento(rsColaborador.getString("EndCpl"));
		    endereco.setLogradouro(rsColaborador.getString("EndRua"));
		    endereco.setNumero(rsColaborador.getString("EndNum"));
		    endereco.setPais(rsColaborador.getString("NomPai"));
		    endereco.setUf(rsColaborador.getString("EstCid"));
		    colaborador.setEndereco(endereco);
		}

	    }
	    rsColaborador.close();
	    stColaborador.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}

	return colaborador;
    }

    public static List<ColaboradorVO> getListaColaboradoresArquivo(Long cpf)
    {

	Connection connVetorh = PersistEngine.getConnection("VETORH");
	List<ColaboradorVO> listaColaboradores = new ArrayList<ColaboradorVO>();
	ColaboradorVO colaborador = new ColaboradorVO();

	PresencaVO presenca = new PresencaVO();

	try
	{
	    Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();

	    /*** Recupera Colaboradores ***/
	    StringBuffer queryColaborador = new StringBuffer();
	    queryColaborador.append(" SELECT fun.NumEmp, fun.TipCol, fun.NumCad, fun.NomFun, fun.NumCpf, fun.DatNas, fun.TipSex, fun.DatAdm, ");
	    queryColaborador.append("        fun.USU_MatRes ");
	    queryColaborador.append(" FROM R034FUN fun  ");
	    queryColaborador.append(" WHERE fun.NumCpf = "+cpf);

	    //Asseio

	    PreparedStatement stColaborador = connVetorh.prepareStatement(queryColaborador.toString());

	    ResultSet rsColaborador = stColaborador.executeQuery();

	    log.warn("SQL - getListaColaboradores - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");

	    while (rsColaborador.next())
	    {
		colaborador = new ColaboradorVO();
		colaborador.setNumeroEmpresa(rsColaborador.getLong("NumEmp"));
		colaborador.setTipoColaborador(rsColaborador.getLong("TipCol"));
		colaborador.setNumeroCadastro(rsColaborador.getLong("NumCad"));
		colaborador.setNomeColaborador(rsColaborador.getString("NomFun"));
		colaborador.setCpf(rsColaborador.getLong("NumCpf"));
		colaborador.setSexo(rsColaborador.getString("TipSex"));
		colaborador.setDataNascimento(rsColaborador.getDate("DatNas"));
		colaborador.setDataAdmissao(rsColaborador.getDate("datAdm"));
		colaborador.setMatrizResponsabilidade(rsColaborador.getString("USU_MatRes"));

		// Adicionando a lista
		listaColaboradores.add(colaborador);
		break;
	    }
	    rsColaborador.close();
	    stColaborador.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}

	return listaColaboradores;
    }

    public static List<DocumentoTreeVO> listaDocumentoTreeArquivo(String cpf)
    {
	List<DocumentoTreeVO> documentos = new ArrayList<DocumentoTreeVO>();
	List<NeoObject> colaboradores = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("GEDRHDADOSCOLABORADOR"), new QLEqualsFilter("numeroCPF", cpf), -1, -1, "neoType, creationDate desc ");

	if (NeoUtils.safeIsNotNull(colaboradores))
	{
	    DocumentoTreeVO documentoAnt = new DocumentoTreeVO();
	    for (NeoObject col : colaboradores)
	    {
		EntityWrapper colaboradorWrapper = new EntityWrapper(col);
		DocumentEntityInfo docInfo = (DocumentEntityInfo) colaboradorWrapper.findValue("docInfo");
		List<NeoObject> arquivos = (List<NeoObject>) colaboradorWrapper.findValue("arquivos");
		Long neoid = (Long) colaboradorWrapper.findValue("neoId");

		DocumentoTreeVO documento = new DocumentoTreeVO();
		documento.setId(neoid);
		documento.setNome(docInfo.getTitle());
		documento.setState("closed");

		List<DocumentoTreeVO> childrens = new ArrayList<DocumentoTreeVO>();

		if (NeoUtils.safeIsNotNull(arquivos))
		{
		    for (NeoObject arq : arquivos)
		    {

			EntityWrapper arquivoWrapper = new EntityWrapper(arq);
			NeoFile file = (NeoFile) arquivoWrapper.findValue("arquivo");
			Long mes = (Long) arquivoWrapper.findValue("mes");
			Long ano = (Long) arquivoWrapper.findValue("ano");
			Long id = (Long) arquivoWrapper.findValue("neoId");

			GregorianCalendar gData = new GregorianCalendar();

			String competencia = "";

			if (mes != null && mes != 0 && ano != null && ano != 0)
			{
			    competencia = String.format("%02d", mes) + "/" + ano.toString();
			    gData = new GregorianCalendar(ano.intValue(), mes.intValue() - 1, 1);
			}
			else
			{
			    gData = new GregorianCalendar(1900, 0, 1);
			}
			DocumentoTreeVO children = new DocumentoTreeVO();
			children.setId(id);
			children.setNome(file.getName() + "." + file.getSufix().toLowerCase());
			children.setData(competencia);
			children.setgData(gData);
			children.setLink(OrsegupsUtils.getLink(file));
			childrens.add(children);
		    }
		}
		documento.setChildren(childrens);
		if (documentoAnt != null && documentoAnt.getNome() != null && documentoAnt.getNome().equals(documento.getNome()))
		{
		    documentoAnt.getChildren().addAll(documento.getChildren());
		}
		else
		{
		    documentos.add(documento);
		}
		documentoAnt = documento;
	    }
	}

	for (DocumentoTreeVO doc : documentos)
	{
	    Collections.sort(doc.getChildren(), new DocumentoComparator());
	}
	return documentos;
    }

    /**
     * Lista todas inspeções não canceladas. Obs.: O que liga o posto às tarefas de inspetoria é apenas o nome da lotação constante na r016orn do vetorh.
     * Se algum dia houver tempo, modificar a tarefa a inspetoria para registrar o código do posto. #umabosta
     * @param numctr
     * @param numpos
     * @return
     */
    public static List<InspecaoVO> listaInspecoes(long numctr,	long numpos) {
	ArrayList<InspecaoVO> retorno = new ArrayList<InspecaoVO>();
	Connection conn = PersistEngine.getConnection("");
	PreparedStatement pst = null;
	ResultSet rs = null;

	try
	{
	    StringBuffer sql = new StringBuffer();

	    sql.append(" select wf.neoId, wf.code, wf.startDate, requester.fullName, (case when wf.processState = 0 then 'Em execução' else case when wf.processState = 1 then 'Finalizado' end end) situacao, pres.descpesquisa  from wfprocess wf "); 
	    sql.append(" join D_IMInspetoria im on (wf.entity_neoId = im.neoId) ");
	    sql.append(" join NeoUser requester on (wf.requester_neoId = requester.neoID) ");
	    sql.append(" join D_PesquisaResultado pres on (im.relatorioInspecao_neoId = pres.neoId) ");
	    sql.append(" where wf.processState != 2 and wf.saved > 0 and pres.codCcu is not null and  pres.codCcu <> '' ");
	    sql.append(" and pres.codCcu in ( select usu_codccu from [FSOODB04\\SQL02].VETORH.dbo.r016orn orn where orn.usu_numctr = ?  and orn.usu_numpos = ? ) order by wf.startDate ");

	    pst = conn.prepareStatement(sql.toString());
	    pst.setLong(1, numctr );
	    pst.setLong(2, numpos );

	    rs = pst.executeQuery();

	    while(rs.next()){
		InspecaoVO insVo = new InspecaoVO();

		insVo.setNeoId(rs.getString("neoId"));
		insVo.setCode(rs.getString("code"));

		GregorianCalendar gc = new GregorianCalendar();
		gc.setTimeInMillis(rs.getTimestamp("startDate").getTime());
		insVo.setStartDate(NeoUtils.safeDateFormat(gc,"dd/MM/yyyy"));

		insVo.setInspetor( rs.getString("fullName") );
		insVo.setSituacao( rs.getString("situacao") );
		insVo.setNomePesquisa(rs.getString("descpesquisa"));

		insVo.setLinkTarefa("<a title='Visualizar tarefa' style='cursor:pointer' onclick='javascript:showSolicitacao(" + insVo.getNeoId() + ")'><img src='imagens/icones_final/properties_16x16-trans.png'></a> " + insVo.getCode());

		retorno.add(insVo);
	    }
	    return retorno;
	}
	catch (Exception e)
	{
	    log.error("Erro ao listar inspeções");
	    e.printStackTrace();
	    return retorno;
	}
	finally
	{
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }


    /**
     * Lista contas sigma para vinculo na ficha do posto do presença.
     * @param filtro -> Indica se a consulta será feita por:  1- Conta, 2- Fantasia
     * @param param -> o parametro de consulta propriamente dito.
     * @return
     */
    public static List<ContaSigmaVO> buscaContaSigma(String filtro, String param){
	List<ContaSigmaVO> retorno = new ArrayList<ContaSigmaVO>();
	PreparedStatement pst = null;
	Connection conn = PersistEngine.getConnection("SIGMA90");
	ResultSet rs = null;
	StringBuilder query = new StringBuilder();
	try{
	    query.append("Select cen.cd_cliente, cen.id_central, cen.particao, emp.NM_FANTASIA, cen.fantasia ");
	    query.append("from dbCentral cen ");  
	    query.append("join Empresa emp on (cen.id_empresa = emp.cd_empresa) ");
	    query.append("where 1=1 ");


	    String queryString = param;

	    if (filtro.equals("1")){
		query.append(" and cen.id_central = ? ");
	    }else if (filtro.equals("2")){
		query.append(" and cen.fantasia like ? ");
		queryString += "%";
	    }else{
		query.append(" and cen.id_central = ? ");
	    }

	    pst = conn.prepareStatement(query.toString());
	    pst.setString(1, queryString );

	    rs = pst.executeQuery();

	    while(rs.next()){
		ContaSigmaVO contaSigma = new ContaSigmaVO();
		contaSigma.setCdCliente(rs.getLong("cd_cliente"));
		contaSigma.setCentral(rs.getString("id_central"));
		contaSigma.setParticao(rs.getString("particao"));
		contaSigma.setEmpresa(rs.getString("NM_FANTASIA"));
		contaSigma.setCentralParticao(contaSigma.getCentral() + "[" + contaSigma.getParticao() + "]");
		contaSigma.setFantasia(rs.getString("fantasia"));
		contaSigma.setLinkEventos("<a title='Vincular' style='cursor:pointer' onclick='javascript:setVinculoContaPosto("+contaSigma.getCdCliente()+",\""+contaSigma.getCentral()+"\")'><img src='imagens/icones_final/doc_link.png'></a>");
		retorno.add(contaSigma);
	    }

	    return retorno;
	}
	catch (Exception e)
	{
	    log.error("Erro buscaContaSigma");
	    e.printStackTrace();
	    return retorno;
	}
	finally
	{
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public static ContaSigmaVO buscaContaSigmaPorCdCliente(String cdCliente){
	ContaSigmaVO retorno = new ContaSigmaVO();
	PreparedStatement pst = null;
	Connection conn = PersistEngine.getConnection("SIGMA90");
	ResultSet rs = null;
	StringBuilder query = new StringBuilder();
	try{
	    query.append("Select cen.cd_cliente, cen.id_central, cen.particao, emp.NM_FANTASIA, cen.fantasia ");
	    query.append("from dbCentral cen ");  
	    query.append("join Empresa emp on (cen.id_empresa = emp.cd_empresa) ");
	    query.append("where cen.cd_cliente = ? ");

	    pst = conn.prepareStatement(query.toString());
	    pst.setString(1, cdCliente );

	    rs = pst.executeQuery();

	    if(rs.next()){
		retorno = new ContaSigmaVO();
		retorno.setCdCliente(rs.getLong("cd_cliente"));
		retorno.setCentral(rs.getString("id_central"));
		retorno.setParticao(rs.getString("particao"));
		retorno.setEmpresa(rs.getString("NM_FANTASIA"));
		retorno.setCentralParticao(retorno.getCentral() + "[" + retorno.getParticao() + "]");
		retorno.setFantasia(rs.getString("fantasia"));
		retorno.setLinkEventos("<a title='Vincular' style='cursor:pointer' onclick='javascript:setVinculoContaPosto("+retorno.getCdCliente()+",\""+retorno.getCentral()+"\")'><img src='imagens/icones_final/doc_link.png'></a>");
	    }

	    return retorno;
	}
	catch (Exception e)
	{
	    log.error("Erro buscaContaSigmaPorCdCliente");
	    e.printStackTrace();
	    return retorno;
	}
	finally
	{
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public static boolean buscaVinculoContaSigmaPostoHumana(String numctr, String numposto, ContaSigmaVO conta){
	boolean retorno = false;
	PreparedStatement pst = null;
	Connection conn = PersistEngine.getConnection("VETORH");
	ResultSet rs = null;
	StringBuilder query = new StringBuilder();
	try{
	    query.append("SELECT    usu_numctr,    usu_numpos,    usu_cdcliente,    usu_idcentral FROM    dbo.usu_t160sigpos where usu_numctr = ? and usu_numpos = ? and usu_cdcliente = ?");

	    pst = conn.prepareStatement(query.toString());
	    pst.setLong(1, NeoUtils.safeLong(numctr) );
	    pst.setLong(2, NeoUtils.safeLong(numposto) );
	    pst.setLong(3, conta.getCdCliente() );

	    rs = pst.executeQuery();

	    if(rs.next()){
		retorno = true;
	    }

	    return retorno;
	}
	catch (Exception e)
	{
	    log.error("Erro buscaVinculoContaSigmaPostoHumana");
	    e.printStackTrace();
	    return retorno;
	}
	finally
	{
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public static boolean vinculaContaSigmaPostoHumana(String numctr, String numposto, ContaSigmaVO conta){
	boolean retorno = false;

	/*
	 * Verificar se o vinculo já existe, se não existe, vincular 
	 */
	if (!buscaVinculoContaSigmaPostoHumana(numctr, numposto, conta)){
	    //inserir
	    retorno = insereContaSigmaPostoHumana(numctr, numposto, conta);
	}


	return retorno;

    }

    public static boolean insereContaSigmaPostoHumana(String numctr, String numposto, ContaSigmaVO conta){
	boolean retorno = false;
	PreparedStatement pst = null;
	Connection conn = PersistEngine.getConnection("VETORH");
	ResultSet rs = null;
	StringBuilder query = new StringBuilder();
	try{

	    query.append(" INSERT INTO dbo.usu_t160sigpos ( usu_numctr, usu_numpos, usu_cdcliente, usu_idcentral ) VALUES    (?,?,?,?) ");

	    pst = conn.prepareStatement(query.toString());
	    pst.setLong(1, NeoUtils.safeLong(numctr) );
	    pst.setLong(2, NeoUtils.safeLong(numposto) );
	    pst.setLong(3, conta.getCdCliente() );
	    pst.setString(4, conta.getCentral() );

	    int cont = pst.executeUpdate();

	    if( cont > 0 ){
		retorno = true;
	    }

	    return retorno;
	}
	catch (Exception e)
	{
	    log.error("Erro InsereContaSigmaPostoHumana");
	    e.printStackTrace();
	    return retorno;
	}
	finally
	{
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public static boolean removeContaSigmaPostoHumana(String numctr, String numposto, ContaSigmaVO conta){
	boolean retorno = false;
	PreparedStatement pst = null;
	Connection conn = PersistEngine.getConnection("VETORH");
	ResultSet rs = null;
	StringBuilder query = new StringBuilder();
	try{

	    query.append(" DELETE FROM  dbo.usu_t160sigpos WHERE    usu_numctr = ? AND usu_numpos = ? AND usu_cdcliente = ? ");

	    pst = conn.prepareStatement(query.toString());
	    pst.setLong(1, NeoUtils.safeLong(numctr) );
	    pst.setLong(2, NeoUtils.safeLong(numposto) );
	    pst.setLong(3, conta.getCdCliente() );

	    int cont = pst.executeUpdate();

	    if( cont > 0 ){
		retorno = true;
	    }

	    return retorno;
	}
	catch (Exception e)
	{
	    log.error("Erro removeContaSigmaPostoHumana");
	    e.printStackTrace();
	    return retorno;
	}
	finally
	{
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public static List<ContaSigmaVinculoHumanaVO> listaContasVinculadas(String numctr, String numpos){
	List<ContaSigmaVinculoHumanaVO> retorno = new ArrayList<ContaSigmaVinculoHumanaVO>();
	PreparedStatement pst = null;
	Connection conn = PersistEngine.getConnection("VETORH");
	ResultSet rs = null;
	StringBuilder query = new StringBuilder();
	try{
	    query.append("SELECT    usu_numctr,    usu_numpos,    usu_cdcliente,    usu_idcentral FROM    dbo.usu_t160sigpos where usu_numctr = ? and usu_numpos = ? ");

	    pst = conn.prepareStatement(query.toString());
	    pst.setString(1, numctr );
	    pst.setString(2, numpos );

	    rs = pst.executeQuery();

	    while(rs.next()){
		ContaSigmaVinculoHumanaVO contaSigma = new ContaSigmaVinculoHumanaVO();
		contaSigma.setNumctr(rs.getString("usu_numctr"));
		contaSigma.setNumposto(rs.getString("usu_numpos"));
		contaSigma.setCdCliente(rs.getString("usu_cdcliente"));
		contaSigma.setIdCentral(rs.getString("usu_idcentral"));
		contaSigma.setLinkExcluir("<a title='Desvincular' style='cursor:pointer' onclick='javascript: if (confirm(\"Tem Certeza?\") == true) { removerVinculoContaPosto("+contaSigma.getCdCliente()+",\""+contaSigma.getIdCentral()+"\"); }'><img src='imagens/icones_final/delete_16x16-trans.png'></a>");
		retorno.add(contaSigma);
	    }

	    return retorno;
	}
	catch (Exception e)
	{
	    log.error("Erro buscaContaSigma");
	    e.printStackTrace();
	    return retorno;
	}
	finally
	{
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public static List<ArmaVO> listaArmasPosto(String numctr, String numpos, GregorianCalendar dataFiltro){
	List<ArmaVO> retorno = new ArrayList<ArmaVO>();
	PreparedStatement pst = null;
	Connection conn = PersistEngine.getConnection("");
	ResultSet rs = null;
	StringBuilder query = new StringBuilder();
	System.out.println(NeoUtils.safeDateFormat(dataFiltro));
	try{

	    query.append(" select arma.neoId, esp.descricaoEspecie, marca.descricaoMarca, arma.modelo, arma.numeroArma, arma.numeroSinarm, sit.descricaoSituacao, arma.dataValidade, hlot.dataAlteracao as dataLotacao");
	    query.append(" from D_ARMArmamento arma  ");
	    query.append(" inner join D_ARMMarca marca on (arma.marca_neoId = marca.neoId) ");
	    query.append(" inner join D_ARMSituacao sit on (arma.situacao_neoId = sit.neoId) ");
	    query.append(" inner join D_ARMEspecie esp on (arma.especie_neoId = esp.neoId) "); 
	    query.append(" inner join D_ARMArmamento_posto arpos on (arma.neoId =arpos.D_ARMArmamento_neoId ) "); 
	    query.append(" inner join D_ARMHistoricoLotacao hlot on ( arpos.posto_neoId = hlot.neoId and hlot.dataAlteracao = ");
	    query.append("                                                 (select max(hmlot.dataAlteracao) from  D_ARMHistoricoLotacao hmlot "); 
	    query.append("                                                 inner join D_ARMArmamento_posto harpos on ( hmlot.neoId = harpos.posto_neoId) "); 
	    query.append("                                                 where harpos.D_ARMArmamento_neoId = arpos.D_ARMArmamento_neoId and hmlot.dataAlteracao <= ? ) ");
	    query.append("                                          ) ");
	    query.append(" inner join X_VETORHR016ORNNV8 xnv8 on (hlot.posto_neoId = xnv8.neoId) ");
	    query.append(" inner join [FSOODB04\\SQL02].VETORH.dbo.r016hie hie on (xnv8.codloc = hie.codloc) ");
	    query.append(" inner join [FSOODB04\\SQL02].VETORH.dbo.r016orn orn on (hie.numloc = orn.numloc) ");
	    query.append(" where sit.mostrarPresenca = 1 and usu_numctr = ? and usu_numpos = ? ");

	    pst = conn.prepareStatement(query.toString());

	    pst.setTimestamp(1, new Timestamp(dataFiltro.getTimeInMillis()));
	    pst.setString(2, numctr );
	    pst.setString(3, numpos );

	    rs = pst.executeQuery();

	    while(rs.next()){
		ArmaVO arma = new ArmaVO();

		arma.setNeoId(rs.getString("neoId"));
		arma.setDescricaoEspecie(rs.getString("descricaoEspecie"));
		arma.setDescricaoMarca(rs.getString("descricaoMarca"));
		arma.setModelo(rs.getString("modelo"));
		arma.setNumeroArma(rs.getString("numeroArma"));
		arma.setNumeroSinarm(rs.getString("numeroSinarm"));
		arma.setSituacao(rs.getString("descricaoSituacao"));

		GregorianCalendar gcDatVal = new GregorianCalendar();
		gcDatVal.setTimeInMillis(rs.getTimestamp("dataValidade").getTime());

		arma.setDataValidade(NeoUtils.safeDateFormat(gcDatVal,"dd/MM/yyyy"));

		GregorianCalendar gcDatLot = new GregorianCalendar();
		gcDatLot.setTimeInMillis(rs.getTimestamp("dataLotacao").getTime());

		arma.setDataLotacao(NeoUtils.safeDateFormat(gcDatLot,"dd/MM/yyyy"));
		arma.setLinkDetalhes("<a href=\"javascript:viewItemFusion('"+arma.getNeoId()+"');\" ><img class=\"tableIcon\" src=\"imagens/icones_final/properties_16x16-trans.png\" title=\"Visualizar\" align=\"absmiddle\"></a>");
		retorno.add(arma);
	    }

	    return retorno;
	}
	catch (Exception e)
	{
	    log.error("Erro armas do posto");
	    e.printStackTrace();
	    return retorno;
	}
	finally
	{
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public static List<RTOciosoVO> listaOciosidade(String numemp, String tipcol, String numcad){
	List<RTOciosoVO> retorno = new ArrayList<RTOciosoVO>();
	PreparedStatement pst = null;
	Connection conn = PersistEngine.getConnection("");
	ResultSet rs = null;
	StringBuilder query = new StringBuilder();
	try{


	    query.append(" select wf.neoId, wf.code, wf.startDate from wfprocess wf ");
	    query.append(" inner join D_RTOCIOSO rto on (wf.entity_neoId = rto.neoId) ");
	    query.append(" where (ocioso = 1 or aprovacaoSolicitante = 1)  "); //and  and aprovarJustificativa = 1
	    query.append(" and rto.cpf = (select fun.numcpf from [FSOODB04\\SQL02].vetorh.dbo.r034fun fun where numemp = ? and tipcol = ? and numcad = ?) ");
	    query.append(" and wf.processState <> 2 ");

	    pst = conn.prepareStatement(query.toString());
	    pst.setString(1, numemp );
	    pst.setString(2, tipcol );
	    pst.setString(3, numcad );

	    rs = pst.executeQuery();

	    while(rs.next()){
		RTOciosoVO rt = new RTOciosoVO();

		rt.setNeoId(rs.getString("neoId"));
		rt.setTarefa(rs.getString("code"));

		GregorianCalendar gcDatTarefa = new GregorianCalendar();
		gcDatTarefa.setTimeInMillis(rs.getTimestamp("startDate").getTime());

		rt.setData(NeoUtils.safeDateFormat(gcDatTarefa,"dd/MM/yyyy"));

		rt.setLink("<a title='Visualizar tarefa' style='cursor:pointer' onclick='javascript:showTarefa(" + rt.getNeoId() + ")'><img src='imagens/icones_final/properties_16x16-trans.png'");

		retorno.add(rt);
	    }

	    return retorno;
	}
	catch (Exception e)
	{
	    log.error("Erro Ociosidade");
	    e.printStackTrace();
	    return retorno;
	}
	finally
	{
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }



    public static List<UsuT160JorVO> listaJornadas(Long codemp, Long numctr, Long numpos,  Long codfil,  Long diaSem ){
	List<UsuT160JorVO> retorno = new ArrayList<UsuT160JorVO>();

	PreparedStatement pst = null;
	Connection conn = PersistEngine.getConnection("SAPIENS");
	ResultSet rs = null;
	StringBuilder query = new StringBuilder();
	try{


	    query.append(" select usu_horini, usu_horfim, usu_diasem from usu_t160jor where ");
	    query.append(" usu_codemp = ? ");
	    query.append(" and usu_numctr = ? ");
	    query.append(" and usu_numpos = ? ");

	    if (codfil != null){
		query.append(" and usu_codfil =  " + codfil);
	    }

	    if (diaSem != null){
		query.append(" and usu_diasem =  " + diaSem);
	    }

	    query.append(" order by usu_diasem,usu_horini ");


	    pst = conn.prepareStatement(query.toString());
	    pst.setLong(1, codemp );
	    pst.setLong(2, numctr );
	    pst.setLong(3, numpos );

	    rs = pst.executeQuery();

	    while(rs.next()){
		UsuT160JorVO jor = new UsuT160JorVO();

		jor.setUsu_horini(rs.getLong("usu_horini"));
		jor.setUsu_horfim(rs.getLong("usu_horfim"));
		jor.setUsu_diasem(rs.getLong("usu_diasem"));


		retorno.add(jor);
	    }

	    return retorno;
	}
	catch (Exception e)
	{
	    log.error("Erro jornada");
	    e.printStackTrace();
	    return retorno;
	}
	finally
	{
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }


    public static List<TarefaFaltaEfetivoVO> listaFaltaEfetivo(String codccu){
	List<TarefaFaltaEfetivoVO> retorno = new ArrayList<TarefaFaltaEfetivoVO>();
	PreparedStatement pst = null;
	Connection conn = PersistEngine.getConnection("");
	ResultSet rs = null;
	StringBuilder query = new StringBuilder();
	try{


	    query.append(" select x1.neoId , x1.code, x1.startDate, x1.titulo from ( ");
	    query.append(" 		select wf.neoId, wf.code, wf.startDate, t.titulo,dataSolicitacao, (select fullname from dbo.NeoUser where neoId = t.solicitante_neoId) slocitante, (select fullname from dbo.NeoUser where neoId = t.executor_neoId) executor "); 
	    query.append(" 		from wfprocess wf with (nolock) ");
	    query.append(" 		join D_tarefa t with (nolock) on (wf.entity_neoId = t.neoId) "); 
	    query.append(" 		where DATEADD(dd, 0, DATEDIFF(dd, 0, dataSolicitacao))  >= datediff(MONTH, 6,getdate()) ");
	    query.append(" 		and "); 
	    query.append(" 			( ");
	    query.append("				titulo like '#Informar falta de efetivo no posto%"+codccu+"'");
	    /*query.append(" 		         titulo like '#Informar falta de efetivo no posto%' + (select cast( usu_codccu as varchar ) from [FSOODB04\\SQL02].VETORH.dbo.usu_t038cvs cvs  where cvs.usu_numctr = ? and cvs.usu_numpos = ? "); 
			query.append(" 		                                                                and cvs.usu_seqalt = (select max(cvs1.usu_seqalt) from [FSOODB04\\SQL02].VETORH.dbo.usu_t038cvs cvs1 where  cvs1.usu_numpos = cvs.usu_numpos and cvs1.usu_numctr = cvs.usu_numctr)) ");*/
	    query.append(" 			) ");
	    query.append(" 		) as  x1 ");

	    pst = conn.prepareStatement(query.toString());

	    rs = pst.executeQuery();

	    while(rs.next()){
		TarefaFaltaEfetivoVO feVO = new TarefaFaltaEfetivoVO();

		feVO.setNeoIdProcesso(rs.getLong("neoId"));
		feVO.setCode(rs.getString("code"));

		GregorianCalendar gcDatTarefa = new GregorianCalendar();
		gcDatTarefa.setTimeInMillis(rs.getTimestamp("startDate").getTime());

		feVO.setStartDate(NeoUtils.safeDateFormat(gcDatTarefa,"dd/MM/yyyy"));

		feVO.setLink("<a title='Visualizar tarefa' style='cursor:pointer' onclick='javascript:showSolicitacao(" + feVO.getNeoIdProcesso() + ")'><img src='imagens/icones_final/properties_16x16-trans.png'");

		retorno.add(feVO);
	    }

	    return retorno;
	}
	catch (Exception e)
	{
	    log.error("Erro Falta de Efetivo");
	    e.printStackTrace();
	    return retorno;
	}
	finally
	{
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }


    public static List<HistoricoEscalaPostoVO> listaHistoricoEscalaPosto(String numloc, String taborg){
	List<HistoricoEscalaPostoVO> retorno = new ArrayList<HistoricoEscalaPostoVO>();
	PreparedStatement pst = null;
	Connection conn = OrsegupsUtils.getConnection("SAPIENS");
	ResultSet rs = null;
	StringBuilder query = new StringBuilder();
	try{


	    query.append(" select hjo.usu_numctr, hjo.usu_numpos, hjo.usu_diasem, hjo.usu_horini, hjo.usu_horfim, hjo.usu_datalt from usu_t160hjo hjo with (nolock) "); 
	    query.append(" inner join vetorh.dbo.r016orn orn with (nolock) on (hjo.usu_numctr = orn.usu_numctr and hjo.usu_numpos = orn.usu_numpos) ");
	    query.append(" where orn.numloc = ? and orn.taborg = ? ");
	    query.append(" and not exists (select 1 from usu_t160jor jor with (nolock) where jor.usu_numctr = hjo.usu_numctr and jor.usu_numpos = hjo.usu_numpos and jor.usu_datalt = hjo.usu_datalt ) ");
	    query.append(" order by hjo.usu_datalt desc ");

	    pst = conn.prepareStatement(query.toString());
	    pst.setString(1, numloc );
	    pst.setString(2, taborg );

	    rs = pst.executeQuery();

	    while(rs.next()){
		HistoricoEscalaPostoVO hesp = new HistoricoEscalaPostoVO();

		hesp.setUsuNumctr(rs.getString("usu_numctr"));
		hesp.setUsuNumpos(rs.getString("usu_numpos"));
		hesp.setUsuDiasem(rs.getString("usu_diasem"));
		hesp.setUsuHorini(rs.getLong("usu_horini"));
		hesp.setUsuHorfim(rs.getLong("usu_horfim"));

		GregorianCalendar gcDatTarefa = new GregorianCalendar();
		gcDatTarefa.setTimeInMillis(rs.getTimestamp("usu_datalt").getTime());

		hesp.setUsuDatalt(NeoUtils.safeDateFormat(gcDatTarefa,"dd/MM/yyyy"));


		retorno.add(hesp);
	    }

	    return retorno;
	}
	catch (Exception e)
	{
	    log.error("Erro Historico de Escala do Posto");
	    e.printStackTrace();
	    return retorno;
	}
	finally
	{
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    /**
     * Retorna os dados de uma competencia utilizando a empresa 1 como parametro
     * @param competencia
     * @return
     */
    public static CompetenciaVO getCompetenciaR044Cal(String competencia){
	CompetenciaVO retorno = null;
	PreparedStatement pst = null;
	Connection conn = OrsegupsUtils.getConnection("VETORH");
	ResultSet rs = null;
	StringBuilder query = new StringBuilder();
	try{
	    GregorianCalendar gcComp = new GregorianCalendar();

	    int mes = Integer.parseInt(competencia.substring(0,2));
	    int ano = Integer.parseInt(competencia.substring(3,7));

	    gcComp.set(GregorianCalendar.DATE, 1);
	    gcComp.set(GregorianCalendar.MONTH, mes-1);
	    gcComp.set(GregorianCalendar.YEAR, ano);

	    gcComp.set(GregorianCalendar.HOUR, 0);
	    gcComp.set(GregorianCalendar.MINUTE, 0);
	    gcComp.set(GregorianCalendar.SECOND, 0);


	    query.append(" select NumEmp, CodCal, TipCal, SitCal, PerRef, IniApu, FimApu from dbo.R044CAL cal where tipcal = 11 and perref = ? and numemp = 1 "); 

	    pst = conn.prepareStatement(query.toString());
	    pst.setDate(1, new java.sql.Date(gcComp.getTimeInMillis()) );

	    rs = pst.executeQuery();

	    while(rs.next()){
		CompetenciaVO cpt = new CompetenciaVO();

		cpt.setNumEmp(rs.getLong("NumEmp"));
		cpt.setCodCal(rs.getLong("CodCal"));
		cpt.setTipCal(rs.getLong("TipCal"));
		cpt.setSitCal(rs.getString("SitCal"));

		GregorianCalendar gcPerRef = new GregorianCalendar();
		gcPerRef.setTimeInMillis(rs.getTimestamp("PerRef").getTime());
		cpt.setPerRef( gcPerRef );

		GregorianCalendar gcIniApu = new GregorianCalendar();
		gcIniApu.setTimeInMillis(rs.getTimestamp("IniApu").getTime());
		cpt.setIniApu( gcIniApu );

		GregorianCalendar gcFimApu = new GregorianCalendar();
		gcFimApu.setTimeInMillis(rs.getTimestamp("FimApu").getTime());
		cpt.setFimApu( gcFimApu );
		System.out.println(cpt);
		retorno = cpt;
	    }

	    return retorno;
	}
	catch (Exception e)
	{
	    log.error(" Erro getCompetenciaR044Cal ");
	    e.printStackTrace();
	    return retorno;
	}
	finally
	{
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    /**
     * Retorna os dados de uma competencia utilizando a empresa 1 como parametro
     * @param competencia
     * @return
     */
    public static CompetenciaVO getCompetenciaSiteR044Cal(String competencia){
	CompetenciaVO retorno = null;
	PreparedStatement pst = null;
	Connection conn = OrsegupsUtils.getConnection("VETORH");
	ResultSet rs = null;
	StringBuilder query = new StringBuilder();
	try{
	    GregorianCalendar gcComp = new GregorianCalendar();

	    int mes = Integer.parseInt(competencia.substring(0,2));
	    int ano = Integer.parseInt(competencia.substring(3,7));

	    gcComp.set(GregorianCalendar.DATE, 1);
	    gcComp.set(GregorianCalendar.MONTH, mes-2);
	    gcComp.set(GregorianCalendar.YEAR, ano);

	    gcComp.set(GregorianCalendar.HOUR, 0);
	    gcComp.set(GregorianCalendar.MINUTE, 0);
	    gcComp.set(GregorianCalendar.SECOND, 0);


	    query.append(" select NumEmp, CodCal, TipCal, SitCal, PerRef, IniApu, FimApu from dbo.R044CAL cal where tipcal = 11 and perref = ? and numemp = 1 "); 

	    pst = conn.prepareStatement(query.toString());
	    pst.setDate(1, new java.sql.Date(gcComp.getTimeInMillis()) );

	    rs = pst.executeQuery();

	    while(rs.next()){
		CompetenciaVO cpt = new CompetenciaVO();

		cpt.setNumEmp(rs.getLong("NumEmp"));
		cpt.setCodCal(rs.getLong("CodCal"));
		cpt.setTipCal(rs.getLong("TipCal"));
		cpt.setSitCal(rs.getString("SitCal"));

		GregorianCalendar gcPerRef = new GregorianCalendar();
		gcPerRef.setTimeInMillis(rs.getTimestamp("PerRef").getTime());
		cpt.setPerRef( gcPerRef );

		GregorianCalendar gcIniApu = new GregorianCalendar();
		gcIniApu.setTimeInMillis(rs.getTimestamp("IniApu").getTime());
		cpt.setIniApu( gcIniApu );

		GregorianCalendar gcFimApu = new GregorianCalendar();
		gcFimApu.setTimeInMillis(rs.getTimestamp("FimApu").getTime());
		cpt.setFimApu( gcFimApu );
		System.out.println(cpt);
		retorno = cpt;
	    }

	    return retorno;
	}
	catch (Exception e)
	{
	    log.error(" Erro getCompetenciaR044Cal ");
	    e.printStackTrace();
	    return retorno;
	}
	finally
	{
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }


    public static String retornaEscalaPorData(String numloc, String taborg, GregorianCalendar gcData){
	String retorno = "";
	PreparedStatement pst = null;
	Connection conn = OrsegupsUtils.getConnection("SAPIENS");
	ResultSet rs = null;

	StringBuilder query = new StringBuilder();

	query.append(" DECLARE @DatRef  Datetime "); 
	query.append(" SELECT @DatRef = '2015-09-15 12:00:01' ");
	query.append(" select hjo.usu_numctr, hjo.usu_numpos, hjo.usu_diasem, hjo.usu_horini, hjo.usu_horfim, hjo.usu_datalt from usu_t160hjo hjo with (nolock) "); 
	query.append(" inner join vetorh.dbo.r016orn orn with (nolock) on (hjo.usu_numctr = orn.usu_numctr and hjo.usu_numpos = orn.usu_numpos) "); 
	query.append(" where orn.numloc = ? and orn.taborg = ? "); 
	query.append(" and hjo.usu_diasem = ?  ");
	query.append(" and hjo.usu_datalt = ( ");  
	query.append("                         select isnull(max(hjo1.usu_datalt),     ( ");
	query.append("                                                                 select min(hjo2.usu_datalt) "); 
	query.append("                                                                 from usu_t160hjo hjo2 "); 
	query.append("                                                                 where hjo2.usu_numctr = hjo.usu_numctr "); 
	query.append("                                                                 and hjo2.usu_numpos = hjo.usu_numpos "); 
	query.append("                                                                 and hjo2.usu_diasem = hjo.usu_diasem "); 
	query.append("                                                                 and hjo2.usu_datalt >= dateadd(d, datediff(d,0, @DatRef), 0)) ");  
	query.append("                                       ) as dataAltCorrente "); 
	query.append("                         from usu_t160hjo hjo1 "); 
	query.append("                         where hjo1.usu_numctr = hjo.usu_numctr and hjo1.usu_numpos = hjo.usu_numpos and hjo1.usu_diasem = hjo.usu_diasem "); 
	query.append("                         and hjo1.usu_datalt <= dateadd(d, datediff(d,0, @DatRef), 0) ");
	query.append("                      ) ");
	query.append(" order by hjo.usu_datalt, hjo.usu_horini asc ");

	try{
	    pst = conn.prepareStatement(query.toString());
	    pst.setLong(1, Long.parseLong(numloc));
	    pst.setLong(2, Long.parseLong(taborg));

	    int diaSem = gcData.get(GregorianCalendar.DAY_OF_WEEK);

	    pst.setInt(3, diaSem);

	    rs = pst.executeQuery();

	    boolean firstTime = true;
	    while (rs.next()){
		HistoricoEscalaPostoVO hev = new HistoricoEscalaPostoVO();
		hev.setUsuNumctr(rs.getString("usu_numctr"));
		hev.setUsuNumpos(rs.getString("usu_numpos"));
		hev.setUsuDiasem(rs.getString("usu_diasem"));
		hev.setUsuHorini(rs.getLong("usu_horini"));
		hev.setUsuHorfim(rs.getLong("usu_horfim"));

		if (firstTime){
		    retorno = hev.toString();
		}else{
		    retorno += " / " + hev;
		}

		firstTime = false;
	    }
	    return retorno;
	}catch(Exception e){
	    e.printStackTrace();
	    return null;
	}finally{
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    /**
     * Busca o neoId do wfProcess de uma tarefa pelo nome do modelo e código da tarefa. Isso se faz necessário na versão 3.2.x do fusion pois agora
     * para cada versão de workflow é duplicado o process model. 
     * @param processModelName
     * @param code
     * @return
     */
    public static Long retornaNeoIdProcess(String processModelName, String code){
	Long retorno = 0L;
	PreparedStatement pst = null;
	Connection conn = OrsegupsUtils.getConnection("");
	ResultSet rs = null;

	StringBuilder query = new StringBuilder();

	query.append("select neoId, code from wfprocess   "); 
	query.append("where model_neoid in (select neoId from processmodel pm  where pm.name = ? ) and code = ?  ");

	try{
	    pst = conn.prepareStatement(query.toString());
	    pst.setString(1, processModelName);
	    pst.setString(2, code);

	    rs = pst.executeQuery();

	    if (rs.next()){
		retorno = rs.getLong("neoId");
	    }

	    return retorno;
	}catch(Exception e){
	    e.printStackTrace();
	    return null;
	}finally{
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    /**
     * Verifica que o fusionCode tem um numcad no vetorh. Esse método é utilizado no presença para saber se o colaborador está acessando a sua propria ficha do colaborador.
     * @param fusionCode
     * @param numcad
     * @return
     */
    public static Boolean isUsuario(String fusionCode, Long numcad){
	Boolean retorno = false;
	PreparedStatement pst = null;
	Connection conn = OrsegupsUtils.getConnection("VETORH");
	ResultSet rs = null;
	String filtro = fusionCode.replace(".", "%");
	System.out.println("Filtro:" + filtro);

	StringBuilder query = new StringBuilder();

	query.append("select numcad from r034fun where nomfun like '"+filtro+"%'and numcad = " + numcad); 


	try{
	    pst = conn.prepareStatement(query.toString());


	    rs = pst.executeQuery();

	    if (rs.next()){
		retorno = true;;
	    }

	    return retorno;
	}catch(Exception e){
	    e.printStackTrace();
	    return null;
	}finally{
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }



    public static List<String> retornaArvoresCCCliente(Long codcliSapiens){
	List<String> retorno = new ArrayList<String>();

	PreparedStatement pst = null;
	Connection conn = OrsegupsUtils.getConnection("VETORH");
	ResultSet rs = null;



	StringBuilder query = new StringBuilder();

	query.append(" select distinct substring(replace(codloc,'.',''),0,10) from r016hie hie ");
	query.append(" inner join usu_t038cvs cvs on hie.NumLoc = cvs.usu_numloc ");  
	query.append(" inner join r016orn orn on cvs.usu_numloc = orn.numloc "); 
	query.append(" where cvs.usu_sitcvs = 'S' and usu_codclisap = " + codcliSapiens);
	System.out.println("[BI PRESENCA] - codcli " + codcliSapiens);

	try{
	    pst = conn.prepareStatement(query.toString());


	    rs = pst.executeQuery();

	    while (rs.next()){
		retorno.add( NeoUtils.safeOutputString(rs.getString(1)) );
		System.out.println("[BI PRESENCA] - codcli " + NeoUtils.safeOutputString(rs.getString(1)));
	    }

	    return retorno;
	}catch(Exception e){
	    e.printStackTrace();
	    return null;
	}finally{
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public static Long retornaAvosFerias(Long numemp, Long tipcol, Long numcad){
	Long retorno = 0L;
	PreparedStatement pst = null;
	Connection conn = OrsegupsUtils.getConnection("VETORH");
	ResultSet rs = null;

	StringBuilder query = new StringBuilder();

	query.append("select top 1 DATEDIFF(month,IniPer,GETDATE()) as avosFerias from r040per where numcad = ? and numemp = ? and tipcol = ? and sitper = 0"); 


	try{
	    pst = conn.prepareStatement(query.toString());

	    pst.setLong(1, numcad);
	    pst.setLong(2, numemp);
	    pst.setLong(3,tipcol);

	    rs = pst.executeQuery();

	    if (rs.next()){
		retorno = rs.getLong("avosFerias");
	    }

	    return retorno;
	}catch(Exception e){
	    e.printStackTrace();
	    return 0L;
	}finally{
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }


    /**
     * Verifica de o funcionário está de férias na data atual. Obs. Utilizado no presecaViaSnep para marcar a entrada do colaborador em férias como entrada local.
     * @param numemp
     * @param tipcol
     * @param numcad
     * @return
     */
    public static Boolean isFuncionarioFerias(Long numemp, Long tipcol, Long numcad){
	PreparedStatement pst = null;
	Connection conn = OrsegupsUtils.getConnection("VETORH");
	ResultSet rs = null;

	StringBuilder query = new StringBuilder();


	query.append(" DECLARE @DatRef  Datetime ");
	query.append(" SELECT @DatRef = DATEADD(dd, 0, DATEDIFF(dd, 0, getdate())) ");
	query.append(" "); 
	query.append(" ");  
	query.append(" SELECT fun.NumEmp, fun.TipCol, fun.NumCad, fun.NomFun, fun.NumCpf, hch.numcra, "); 
	query.append("         /*esc.NomEsc, car.TitRed, sis.DesSis, hes.CodEsc, hes.CodTma, cvs.USU_NumLoc, cvs.USU_TabOrg, fun.EmiCar,  */ ");
	query.append("         ISNULL(afa.SitAfa, 1) AS SitAfa, ISNULL(sit.DesSit, 'Trabalhando') AS DesSit, ISNULL(sit.usu_blopre, 'N') AS BloPre ");
	query.append("         /*afa.DatAfa AS dataAfa, afa.DatTer, fis.USU_NomFis, afaDem.DatAfa as dataDem, cauDem.DesDem, fun.USU_MatRes*/ ");
	query.append(" FROM R034FUN fun ");  
	query.append(" LEFT JOIN R038HCH hch WITH (NOLOCK) ON hch.NUMEMP = fun.NumEmp AND fun.TIPCOL = hch.TIPCOL AND fun.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch.HorIni, hch.DatIni) = (SELECT MAX(DATEADD(MINUTE, hch2.HorIni, hch2.DatIni)) FROM R038HCH hch2 WHERE hch2.NUMEMP = hch.NUMEMP AND hch2.TIPCOL = hch.TIPCOL AND hch2.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch2.HorIni, hch2.DatIni) <= @DatRef) ");
	query.append(" INNER JOIN R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp AND hlo.TipCol = fun.TipCol AND hlo.NumCad = fun.NumCad AND hlo.DatAlt = (SELECT MAX (DATALT) FROM R038HLO TABELA001 WHERE TABELA001.NUMEMP = hlo.NUMEMP AND TABELA001.TIPCOL = hlo.TIPCOL AND TABELA001.NUMCAD = hlo.NUMCAD AND TABELA001.DATALT <= @DatRef) "); 
	query.append(" INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.NumLoc = hlo.NumLoc and orn.TabOrg = 203 "); 
	query.append(" INNER JOIN USU_T038CVS cvs WITH (NOLOCK) ON cvs.USU_NumLoc = orn.NumLoc AND cvs.USU_TabOrg = orn.TabOrg AND cvs.USU_SeqAlt = (Select MAX(c2.usu_seqalt) FROM USU_T038CVS c2 Where c2.usu_taborg = cvs.usu_taborg And c2.usu_numloc = cvs.usu_numloc And c2.usu_datalt <= @DatRef) "); 
	query.append(" INNER JOIN USU_TFisRes fis ON fis.USU_FisRes = fun.usu_fisres "); 
	query.append(" LEFT JOIN R038AFA afa WITH (NOLOCK) ON (afa.NumEmp = fun.NumEmp AND afa.TipCol = fun.TipCol AND afa.NumCad = fun.NumCad AND DATEADD(minute,afa.HORAFA,afa.DATAFA) = (SELECT MAX (DATEADD(minute,HORAFA,DATAFA)) FROM R038AFA TABELA001 WHERE TABELA001.NUMEMP = afa.NUMEMP AND TABELA001.TIPCOL = afa.TIPCOL AND TABELA001.NUMCAD = afa.NUMCAD AND TABELA001.DATAFA <= @DatRef AND (TABELA001.DatTer = '1900-12-31 00:00:00' OR TABELA001.DatTer >= cast(floor(cast(@DatRef as float)) as datetime)))) "); 
	query.append(" LEFT JOIN R038AFA afaDem WITH (NOLOCK) ON afaDem.NumEmp = fun.NumEmp AND afaDem.TipCol = fun.TipCol AND afaDem.NumCad = fun.NumCad AND afaDem.sitAfa = 7 AND DATEADD(minute,afaDem.HORAFA,afaDem.DATAFA) = (SELECT MAX (DATEADD(minute,HORAFA,DATAFA)) FROM R038AFA TABELA001 WHERE TABELA001.NUMEMP = afaDem.NUMEMP AND TABELA001.TIPCOL = afaDem.TIPCOL AND TABELA001.NUMCAD = afaDem.NUMCAD) "); 
	query.append(" LEFT JOIN R042CAU cauDem ON cauDem.CauDem = afaDem.CauDem "); 
	query.append(" LEFT JOIN R010SIT sit ON sit.CodSit = afa.SitAfa "); 

	query.append("  WHERE fun.DatAdm <=  @DatRef ");
	query.append(" AND (afa.SitAfa IS NULL OR afa.SitAfa <> 7 OR (afa.SitAfa = 7 AND afa.DatAfa > cast(floor(cast(@DatRef as float)) as datetime))) "); 
	query.append(" AND fun.TipCol = 1 "); 
	query.append(" AND fun.NumEmp not in (5, 11, 14) "); 

	query.append(" and afa.sitafa = 2 "); //Situação de Férias
	query.append(" and fun.numcad = ? ");
	query.append(" and fun.numemp = ? ");
	query.append(" and fun.tipcol = ? ");


	try{
	    pst = conn.prepareStatement(query.toString());

	    pst.setLong(1, numcad);
	    pst.setLong(2, numemp);
	    pst.setLong(3,tipcol);

	    rs = pst.executeQuery();

	    if (rs.next()){
		return true;
	    }
	    return false;
	}catch(Exception e){
	    e.printStackTrace();
	    return false;
	}finally{
	    OrsegupsUtils.closeConnection(conn, pst, rs);
	}
    }

    public static PostoVO buscaDadosPosto(String numPos, String codCcu)
    {

	Connection connVetorh = PersistEngine.getConnection("SAPIENS");
	PostoVO posto = new PostoVO();

	try
	{
	    /*** Recupera Postos ***/
	    StringBuffer queryPostos = new StringBuffer();
	    queryPostos.append(" SELECT USU_ENDCTR,USU_BAICTR,USU_CIDCTR,USU_UFSCTR FROM USU_T160CVS WHERE USU_NUMPOS = ? AND USU_CODCCU = ?") ;

	    PreparedStatement stPostos = connVetorh.prepareStatement(queryPostos.toString());
	    stPostos.setString(1, numPos);
	    stPostos.setString(2, codCcu);

	    ResultSet rsPostos = stPostos.executeQuery();

	    while (rsPostos.next())
	    {
		posto.setEndereco(rsPostos.getString("USU_ENDCTR"));
		posto.setBairro(rsPostos.getString("USU_BAICTR"));
		posto.setCidade(rsPostos.getString("USU_CIDCTR"));
		posto.setEstado(rsPostos.getString("USU_UFSCTR"));
	    }
	    rsPostos.close();
	    stPostos.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}
	return posto;
    }

    public static String listaEmpresaMatriculaColaborador(Collection<ColaboradorVO> colaboradores)
    {
	String Colaboradores = " ";

	if (!colaboradores.isEmpty()) 
	{
	    for(ColaboradorVO colaborador : colaboradores){
		if(!colaborador.getPresenca().getStatus().getCodigo().equals("EVP") && !colaborador.getPresenca().getStatus().getCodigo().equals("EVA") && !colaborador.getPresenca().getStatus().getCodigo().equals("SNA")){
		    Colaboradores = Colaboradores+colaborador.getNumeroEmpresa()+"/"+colaborador.getNumeroCadastro()+","; 	
		}
	    }
	}
	return Colaboradores;
    }
    public static ColaboradorVO infoColaboradoresInconsistentes(String codEmp, String numCad)
    {
	ColaboradorVO colaboradorVo = new ColaboradorVO();
	StringBuffer queryColaborador = new StringBuffer();
	colaboradorVo.setEscala(new EscalaVO());
	Connection connVetorh = PersistEngine.getConnection("VETORH");
	try
	{
	    queryColaborador.append("SELECT * FROM R034FUN FUN ");
	    queryColaborador.append(" INNER JOIN R034CPL CPL ON CPL.NUMEMP = FUN.NUMEMP AND CPL.TIPCOL = FUN.TIPCOL AND CPL.NUMCAD = FUN.NUMCAD");
	    queryColaborador.append(" INNER JOIN R038HES HES ON HES.NUMEMP = FUN.NUMEMP AND"); 
	    queryColaborador.append("						  HES.TIPCOL = FUN.TIPCOL AND"); 
	    queryColaborador.append("						  HES.NUMCAD = FUN.NUMCAD AND"); 
	    queryColaborador.append("						  DATALT = (SELECT MAX(DATALT) FROM R038HES HES1 WHERE HES.NUMEMP = HES1.NUMEMP AND");
	    queryColaborador.append("						  HES.TIPCOL = HES1.TIPCOL AND"); 
	    queryColaborador.append("						  HES.NUMCAD = HES1.NUMCAD)"); 
	    queryColaborador.append(" INNER JOIN R006ESC ESC ON ESC.CODESC = HES.CODESC");						   
	    queryColaborador.append(" WHERE FUN.NUMCAD = ? AND FUN.NUMEMP = ? ");

	    PreparedStatement stVetorh = connVetorh.prepareStatement(queryColaborador.toString());
	    stVetorh.setString(1, numCad);
	    stVetorh.setString(2, codEmp);
	    ResultSet rsVetorh = stVetorh.executeQuery();
	    while (rsVetorh.next())
	    {
		colaboradorVo.getEscala().setDescricao(rsVetorh.getString("NomEsc"));
		colaboradorVo.setNomeColaborador(rsVetorh.getString("NomFun"));
		colaboradorVo.setTelefone(rsVetorh.getString("NumTel"));
		colaboradorVo.setNumeroEmpresa(rsVetorh.getLong("NumEmp"));
		colaboradorVo.setNumeroCadastro(rsVetorh.getLong("NumCad"));
	    }
	    rsVetorh.close();
	    stVetorh.close();
	}
	catch (SQLException e)
	{
	    e.printStackTrace();
	}



	return colaboradorVo;
    }


    /**
     * Verifica que o colaborador está presente no momento. Se a data for diferente de nulo, vai considerar a presença pela data passada por prametro.
     * @param numEmp - Código da empresa
     * @param numCad - Matriculo do colaborador
     * @param gcData - Se null, considera a data atual
     * @return
     */
    private static boolean verificaPresencaColaborador(Long numEmp, Long numCad, GregorianCalendar gcData){
	System.out.println("verificaPresencaColaborador("+numEmp+", " + numCad +")");
	Boolean retorno = false;
	Connection conVetorh = PersistEngine.getConnection("VETORH");
	PreparedStatement pstColaboradores = null;
	ResultSet rs = null;

	StringBuilder sqlColaboradoresPresentes = new StringBuilder();

	sqlColaboradoresPresentes.append(" DECLARE @DatRef  Datetime   ");
	sqlColaboradoresPresentes.append(" SELECT @DatRef = ? ");
	sqlColaboradoresPresentes.append(" SELECT fun.numemp, fun.numcad, fun.nomfun, orn.numloc, orn.usu_telloc , DATEADD(SECOND, ace.SeqAcc, DATEADD(MINUTE, ace.HorAcc, ace.DatAcc))  as entrada , DATEADD(SECOND, acs.SeqAcc, DATEADD(MINUTE, acs.HorAcc, acs.DatAcc)) as saida ");
	sqlColaboradoresPresentes.append(" FROM R034FUN fun "); 
	sqlColaboradoresPresentes.append(" INNER JOIN R038HCH hch ON  fun.numemp = hch.numemp and fun.numcad = hch.numcad and fun.tipcol = hch.tipcol ");    
	sqlColaboradoresPresentes.append(" INNER JOIN R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp  AND hlo.TipCol = fun.TipCol ");    
	sqlColaboradoresPresentes.append("                                                                  AND hlo.NumCad = fun.NumCad ");    
	sqlColaboradoresPresentes.append("                                                                  AND hlo.DatAlt = (  ");
	sqlColaboradoresPresentes.append("                                                                                     SELECT MAX (DATALT) ");    
	sqlColaboradoresPresentes.append("                                                                                     FROM R038HLO TABELA001 ");     
	sqlColaboradoresPresentes.append("                                                                                     WHERE TABELA001.NUMEMP = hlo.NUMEMP ");    
	sqlColaboradoresPresentes.append("                                                                                     AND TABELA001.TIPCOL = hlo.TIPCOL ");    
	sqlColaboradoresPresentes.append("                                                                                     AND TABELA001.NUMCAD = hlo.NUMCAD ");    
	sqlColaboradoresPresentes.append("                                                                                     AND TABELA001.DATALT <= @DatRef ");    
	sqlColaboradoresPresentes.append("                                                                                   ) ");   
	sqlColaboradoresPresentes.append(" INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.NumLoc = hlo.NumLoc AND orn.TabOrg = hlo.TabOrg ");  
	sqlColaboradoresPresentes.append(" LEFT JOIN R038HCA hca WITH (NOLOCK) ON hca.NumEmp = fun.NumEmp AND hca.TipCol = fun.TipCol AND hca.NumCad = fun.NumCad AND hca.DatAlt = (SELECT MAX (DATALT) FROM R038HCA TABELA001 WHERE TABELA001.NUMEMP = hca.NUMEMP AND TABELA001.TIPCOL = hca.TIPCOL AND TABELA001.NUMCAD = hca.NUMCAD AND TABELA001.DATALT <= @DatRef) ");    
	sqlColaboradoresPresentes.append(" LEFT JOIN R024CAR car WITH (NOLOCK) ON car.EstCar = hca.EstCar AND car.CodCar = hca.CodCar ");    
	sqlColaboradoresPresentes.append(" left JOIN R070ACC ace WITH (NOLOCK) ON ace.TipAcc = 100 AND ace.NumCra = hch.NumCra AND ace.USU_NumLoc = orn.NumLoc AND ace.DirAcc = 'E' ");  
	sqlColaboradoresPresentes.append("                                 AND DATEADD(SECOND, ace.SeqAcc, DATEADD(MINUTE, ace.HorAcc, ace.DatAcc)) = (SELECT MAX(DATEADD(SECOND, ace2.SeqAcc, DATEADD(MINUTE, ace2.HorAcc, ace2.DatAcc))) FROM R070ACC ace2 WITH (NOLOCK) WHERE ace2.TipAcc = 100 AND ace2.NumCra = ace.NumCra AND ace2.USU_NumLoc = ace.USU_NumLoc AND ace2.DirAcc = ace.DirAcc  ) ");   
	sqlColaboradoresPresentes.append(" left JOIN R070ACC acs WITH (NOLOCK) ON acs.TipAcc = 100 AND acs.NumCra = hch.NumCra and acs.USU_NumLoc = orn.NumLoc AND acs.DirAcc = 'S' ");  
	sqlColaboradoresPresentes.append("                                 AND DATEADD(SECOND, acs.SeqAcc, DATEADD(MINUTE, acs.HorAcc, acs.DatAcc)) = (SELECT MAX(DATEADD(SECOND, acs2.SeqAcc, DATEADD(MINUTE, acs2.HorAcc, acs2.DatAcc))) FROM R070ACC acs2 WITH (NOLOCK) WHERE acs2.TipAcc = 100 AND acs2.NumCra = acs.NumCra AND acs2.USU_NumLoc = acs.USU_NumLoc AND acs2.DatAcc = acs.DatAcc AND acs2.DirAcc = acs.DirAcc AND DATEADD(MINUTE, acs2.HorAcc, acs2.DatAcc) > DATEADD(MINUTE, ace.HorAcc, ace.DatAcc)) ");   
	sqlColaboradoresPresentes.append(" WHERE 1=1 ");  
	sqlColaboradoresPresentes.append(" and @DatRef >= DATEADD(MINUTE, ace.HorAcc, ace.DatAcc) ");  
	sqlColaboradoresPresentes.append(" and @DatRef <= (case when acs.HorAcc is not null then DATEADD(MINUTE, acs.HorAcc, acs.DatAcc) else getdate() end ) ");  
	sqlColaboradoresPresentes.append(" and fun.sitafa <> 7 "); 
	sqlColaboradoresPresentes.append(" and fun.numemp = ? and fun.numcad = ? ");

	try{

	    pstColaboradores = conVetorh.prepareStatement(sqlColaboradoresPresentes.toString());

	    if (gcData == null){
		pstColaboradores.setTimestamp(1, new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
	    }else{
		pstColaboradores.setTimestamp(1, new Timestamp(gcData.getTimeInMillis()));
	    }
	    pstColaboradores.setLong(2, numEmp);
	    pstColaboradores.setLong(3, numCad);

	    rs = pstColaboradores.executeQuery();

	    if (rs.next()){
		retorno = true;
	    }

	    return retorno;
	}catch(Exception e){
	    System.out.println("Erro ao consultar Presenca do colaborador");
	    e.printStackTrace();
	    return retorno;
	}finally{
	    OrsegupsUtils.closeConnection(conVetorh, pstColaboradores, rs);
	}
    }

    /**
     * Retorna a lista de postos de supervisao
     * @return
     */
    public static List<PostoSupervisaoVO> listaPostosSupervisao(Long usuCodReg){
	List<PostoSupervisaoVO> retorno = new ArrayList<PostoSupervisaoVO>();
	Connection conVetorh = PersistEngine.getConnection("VETORH");
	PreparedStatement pst = null;
	ResultSet rs = null;

	StringBuilder sqlPostosSupervisao = new StringBuilder();

	/*sqlPostosSupervisao.append(" select orn.usu_codccu, orn.usu_lotorn, orn.numloc, orn.nomloc, orn.usu_codreg, orn.usu_numctr, reg.USU_NomReg  ");
		sqlPostosSupervisao.append(" from r016orn orn                                                                                   ");
		sqlPostosSupervisao.append(" inner join usu_t200reg reg on orn.usu_codreg = reg.USU_CodReg                                      ");
		sqlPostosSupervisao.append(" where (nomloc like '%SUP0%' or nomloc like '%SUP1%' or nomloc like '%SUP2%' or nomloc like '%SUP3%' ");
		sqlPostosSupervisao.append(" or nomloc like '%SUPERVI%')                                                                         ");
		sqlPostosSupervisao.append(" and orn.usu_codreg != 0 and orn.usu_sitati = 'S'   order by reg.USU_NomReg                                                  ");*/

	sqlPostosSupervisao.append(" select orn.usu_codccu , orn.numloc, orn.usu_lotorn, orn.nomloc, orn.usu_codreg, orn.usu_numctr, reg.USU_NomReg ");
	sqlPostosSupervisao.append(" from r016orn orn "); 
	sqlPostosSupervisao.append(" inner join usu_t200reg reg on orn.usu_codreg = reg.USU_CodReg ");
	sqlPostosSupervisao.append(" where  orn.usu_sitati = 'S' ");
	sqlPostosSupervisao.append(" and orn.usu_issup = 1 and reg.USU_CodReg = ? ");

	try{
	    pst = conVetorh.prepareStatement(sqlPostosSupervisao.toString());
	    pst.setLong(1, usuCodReg);
	    rs = pst.executeQuery();

	    while (rs.next()){
		PostoSupervisaoVO posto = new PostoSupervisaoVO();

		RegionalVO reg = new RegionalVO();
		reg.setUsuCodReg(rs.getLong("usu_codreg"));
		reg.setUsuNomReg(rs.getString("USU_NomReg"));

		posto.setNumLoc(rs.getLong("numloc"));
		posto.setNomLoc(rs.getString("nomloc"));
		posto.setUsuLotOrn(rs.getString("usu_lotorn"));
		posto.setUsuNumCtr(rs.getLong("usu_numctr"));
		posto.setUsuCodccu(rs.getLong("usu_codccu"));
		posto.setRegional(reg);

		retorno.add(posto);
	    }

	    return retorno;
	}catch(Exception e){
	    System.out.println("Erro ao consultar Presenca do colaborador");
	    e.printStackTrace();
	    return retorno;
	}finally{
	    OrsegupsUtils.closeConnection(conVetorh, pst, rs);
	}
    }

    /**
     * Retorna os postos de supervisão de um posto operacional
     * @param codccu -- codigo do centro de custo do posto supervisionado
     */
    public static List<PostoSupervisaoVO> listaPostosSupervisaoVinculados(Long codccu)
    {
	List<PostoSupervisaoVO> retorno = new ArrayList<PostoSupervisaoVO>();
	Connection conVetorh = PersistEngine.getConnection("VETORH");
	PreparedStatement pst = null;
	ResultSet rs = null;

	StringBuilder sqlPostosSupervisao = new StringBuilder();

	sqlPostosSupervisao.append(" select orn.usu_codccu, orn.usu_lotorn, orn.numloc, orn.nomloc, orn.usu_codreg, orn.usu_numctr, reg.USU_NomReg, sup.usu_codccusup, sup.usu_datcad  ");
	sqlPostosSupervisao.append(" from r016orn orn                                                                                      ");
	sqlPostosSupervisao.append(" inner join usu_t016sup sup on orn.usu_codccu = sup.usu_codccusup                                      ");
	sqlPostosSupervisao.append(" inner join usu_t200reg reg on orn.usu_codreg = reg.USU_CodReg 										   ");
	sqlPostosSupervisao.append(" where orn.usu_sitati = 'S' and sup.usu_codccu = ?  order by sup.usu_datcad    ");


	try{
	    pst = conVetorh.prepareStatement(sqlPostosSupervisao.toString());
	    pst.setLong(1, codccu);
	    rs = pst.executeQuery();

	    while (rs.next()){
		PostoSupervisaoVO posto = new PostoSupervisaoVO();

		RegionalVO reg = new RegionalVO();
		reg.setUsuCodReg(rs.getLong("usu_codreg"));
		reg.setUsuNomReg(rs.getString("USU_NomReg"));

		posto.setNumLoc(rs.getLong("numloc"));
		posto.setNomLoc(rs.getString("nomloc"));
		posto.setUsuLotOrn(rs.getString("usu_lotorn"));
		posto.setUsuNumCtr(rs.getLong("usu_numctr"));
		posto.setUsuCodccu(rs.getLong("usu_codccu"));
		posto.setRegional(reg);
		posto.setLinkExcluir("<a title='Desvincular' style='cursor:pointer' onclick='javascript: if (confirm(\"Tem Certeza?\") == true) { removerVinculoSupervisaoPosto("+rs.getLong("usu_codccusup")+"); }'><img src='imagens/icones_final/delete_16x16-trans.png'></a>");

		retorno.add(posto);
	    }

	    return retorno;
	}catch(Exception e){
	    System.out.println("Erro ao consultar vinculos de supervisao no posto");
	    e.printStackTrace();
	    return retorno;
	}finally{
	    OrsegupsUtils.closeConnection(conVetorh, pst, rs);
	}
    }

    /**
     * Salva vinculo do posto de supervisao com o posto supervisionado.
     * @param codccu - Centro de custo do posto supervisionado
     * @param numloc - Numero do local do posto supervisionado
     * @param usuario - usuario que fez a alteração.
     * @param ccusup - Centro de custo do posto de supervisao
     * @return
     */
    public static Boolean salvaVinculoSupervisaoPosto(String codccu, String numloc, String usuario, String ccusup)
    {
	Boolean retorno = false;
	Connection conVetorh = PersistEngine.getConnection("VETORH");
	PreparedStatement pst = null;
	try{
	    String sql = " insert into usu_t016sup (usu_codccu, usu_codccusup, usu_datcad) values (?,?,?) ";
	    pst = conVetorh.prepareStatement(sql);

	    pst.setLong(1, NeoUtils.safeLong(codccu));
	    pst.setLong(2, NeoUtils.safeLong(ccusup));
	    pst.setTimestamp(3, new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()));
	    int r = pst.executeUpdate();
	    System.out.println("avinc->"+r);
	    if ( r > 0){
		retorno = true;
	    }

	    return retorno;
	}catch(Exception e){
	    System.out.println("Erro ao salvarVinculoSupervisaoPosto");
	    e.printStackTrace();
	    return retorno;
	}finally{
	    OrsegupsUtils.closeConnection(conVetorh, pst, null);
	}
    }

    public static Boolean salvaObsHorista(String numcad, String numemp, String tipcol, String obsHorista)
    {
	InstantiableEntityInfo eformObsHorista = AdapterUtils.getInstantiableEntityInfo("obsHoristas");
	NeoObject neoObsHorista = eformObsHorista.createNewInstance();
	EntityWrapper wObsHorista = new EntityWrapper(neoObsHorista);
	GregorianCalendar datAtu = new GregorianCalendar();
	try{
	    wObsHorista.findField("numCadHor").setValue(Long.parseLong(numcad));
	    wObsHorista.findField("numEmpHor").setValue(Long.parseLong(numemp));
	    wObsHorista.findField("tipcol").setValue(Long.parseLong(tipcol));
	    wObsHorista.findField("obsHorista").setValue(obsHorista);
	    wObsHorista.findField("dataRegistro").setValue(datAtu);
	    //hora + minuto atual
	    int hor = Integer.parseInt(NeoDateUtils.safeDateFormat(datAtu, "HH"));
	    int min = Integer.parseInt(NeoDateUtils.safeDateFormat(datAtu, "mm"));
	    long horMin = (hor*60)+min;
	    wObsHorista.findField("horRegistro").setValue(horMin);
	    //Salva o Eform
	    PersistEngine.persist(neoObsHorista);
	}catch(Exception e){
	    System.out.println("Erro ao persister a informarção da observação no e-form D_obsHoristas");
	    e.printStackTrace();
	    return false;
	}
	return true;

    }

    public static Boolean existeVinculoSupervisaoPosto(String codccu, String ccusup)
    {
	if (ccusup == null || "0".equals(ccusup)){ return true; }

	Boolean retorno = false;
	Connection conVetorh = PersistEngine.getConnection("VETORH");
	PreparedStatement pst = null;
	ResultSet rs = null;
	try{
	    String sql = " select 1 from usu_t016sup where usu_codccu = ? and usu_codccusup = ? ";
	    pst = conVetorh.prepareStatement(sql);

	    pst.setLong(1, NeoUtils.safeLong(codccu));
	    pst.setLong(2, NeoUtils.safeLong(ccusup));

	    rs = pst.executeQuery();

	    if (rs.next()){
		retorno = true;
	    }

	    return retorno;
	}catch(Exception e){
	    System.out.println("Erro ao executar existeVinculoSupervisaoPosto");
	    e.printStackTrace();
	    return retorno;
	}finally{
	    OrsegupsUtils.closeConnection(conVetorh, pst, rs);
	}
    }

    public static Boolean removerVinculoSupervisaoPosto(String codccu, String numloc, String usuario, String ccusup)
    {
	Boolean retorno = false;
	Connection conVetorh = PersistEngine.getConnection("VETORH");
	PreparedStatement pst = null;
	try{
	    String sql = " delete usu_t016sup where usu_codccu = ? and usu_codccusup = ? ";
	    pst = conVetorh.prepareStatement(sql);

	    pst.setLong(1, NeoUtils.safeLong(codccu));
	    pst.setLong(2, NeoUtils.safeLong(ccusup));
	    int r = pst.executeUpdate();
	    System.out.println("dvinc->"+r);
	    if ( r > 0){
		retorno = true;
	    }

	    return retorno;
	}catch(Exception e){
	    System.out.println("Erro ao removerVinculoSupervisaoPosto");
	    e.printStackTrace();
	    return retorno;
	}finally{
	    OrsegupsUtils.closeConnection(conVetorh, pst, null);
	}
    }

    public static Boolean removerObsHorista(String neoId)
    {
	Boolean retorno = false;
	Connection conVetorh = PersistEngine.getConnection("");
	PreparedStatement pst = null;
	try{
	    String sql = " delete d_obsHoristas where neoId = ?";
	    pst = conVetorh.prepareStatement(sql);

	    pst.setString(1, neoId);

	    int r = pst.executeUpdate();
	    System.out.println("dvinc->"+r);
	    if ( r > 0){
		retorno = true;
	    }

	    return retorno;
	}catch(Exception e){
	    System.out.println("Erro ao remover Observação do Horista");
	    e.printStackTrace();
	    return retorno;
	}finally{
	    OrsegupsUtils.closeConnection(conVetorh, pst, null);
	}
    }

    /**
     * Verifica na tabela R016ORN se os dois centros de custo são da mesma regional. 
     * @param codccu1
     * @param codccu2
     * @return true - caso os centros de custo sejam da mesma regional
     */
    public static Boolean comparaRegionalDoCentroDeCusto(Long codccu1, Long codccu2)
    {
	Boolean retorno = false;
	Connection conVetorh = PersistEngine.getConnection("VETORH");
	PreparedStatement pst = null;
	ResultSet rs = null;
	try{

	    String sql =  "	select orn1.usu_codreg from r016orn orn1 where orn1.usu_codccu = ? "+
		    " and exists (select orn2.usu_codreg from r016orn orn2 where orn2.usu_codccu = ? and orn1.usu_codreg=orn2.usu_codreg) ";

	    pst = conVetorh.prepareStatement(sql);

	    pst.setLong(1, codccu1);
	    pst.setLong(2, codccu2);

	    rs = pst.executeQuery();
	    if (rs.next()){
		retorno = true;
	    }

	    return retorno;
	}catch(Exception e){
	    System.out.println("Erro ao executar comparaRegionalDoCentroDeCusto");
	    e.printStackTrace();
	    return retorno;
	}finally{
	    OrsegupsUtils.closeConnection(conVetorh, pst, null);
	}
    }

    /**
     * Atualiza a informação que indica se o posto é supervisão ou não
     * @param codccu - centro de custo do posto
     * @param numloc - numero do local
     * @param usuario - usuário que fez a alteração
     * @param isPostoSup - 0- posto normal, 1- posto de supervisao
     * @return
     */
    public static Boolean atualizaPostoSup(String codccu, String numloc, String usuario, String isPostoSup)
    {
	Boolean retorno = false;
	Connection conVetorh = PersistEngine.getConnection("VETORH");
	PreparedStatement pst = null;
	try{
	    String sql = "update R016orn set usu_issup = ? where numloc = ? and usu_codccu = ? ";
	    pst = conVetorh.prepareStatement(sql);

	    pst.setLong(1, NeoUtils.safeLong(isPostoSup));
	    pst.setLong(2, NeoUtils.safeLong(numloc));
	    pst.setLong(3, NeoUtils.safeLong(codccu));

	    if (pst.executeUpdate() > 0){
		retorno = true;
	    }

	    return retorno;
	}catch(Exception e){
	    System.out.println("Erro ao salvarVinculoSupervisaoPosto");
	    e.printStackTrace();
	    return retorno;
	}finally{
	    OrsegupsUtils.closeConnection(conVetorh, pst, null);
	}
    }

    /**
     * Verifica na Tabela R016ORN se o posto é de supervisão retornando true caso seja.
     * @param numLoc
     * @param codCcu
     * @return
     */
    public static Boolean isPostoSupervisao(Long numLoc, Long codCcu){
	Boolean retorno = false;
	Connection conVetorh = PersistEngine.getConnection("VETORH");
	PreparedStatement pstColaboradores = null;
	ResultSet rs = null;

	StringBuilder sqlPostosSupervisao = new StringBuilder();

	sqlPostosSupervisao.append(" select usu_issup from R016ORN where numloc = ? and usu_codccu = ? ");

	try{

	    pstColaboradores = conVetorh.prepareStatement(sqlPostosSupervisao.toString());

	    pstColaboradores.setLong(1, numLoc);
	    pstColaboradores.setLong(2, codCcu);

	    rs = pstColaboradores.executeQuery();

	    if(rs.next()){
		Long isSup = rs.getLong("usu_issup");
		if (isSup != null && isSup == 1){
		    retorno = true;
		}
	    }

	    return retorno;
	}catch(Exception e){
	    System.out.println("Erro ao consultar isPostoSupervisao");
	    e.printStackTrace();
	    return retorno;
	}finally{
	    OrsegupsUtils.closeConnection(conVetorh, pstColaboradores, rs);
	}
    }

    public static EscalaVO getEscalaColaborador(Long numCad, Long numEmp, Long tipCol){

	EscalaVO escala = new EscalaVO();
	StringBuffer queryColaborador = new StringBuffer();
	Connection connVetorh = PersistEngine.getConnection("VETORH");
	try
	{
	    queryColaborador.append("SELECT ESC.CodEsc, ESC.NOMESC,HES.CODTMA FROM R034FUN FUN ");
	    queryColaborador.append(" INNER JOIN R034CPL CPL ON CPL.NUMEMP = FUN.NUMEMP AND CPL.TIPCOL = FUN.TIPCOL AND CPL.NUMCAD = FUN.NUMCAD");
	    queryColaborador.append(" INNER JOIN R038HES HES ON HES.NUMEMP = FUN.NUMEMP AND"); 
	    queryColaborador.append("						  HES.TIPCOL = FUN.TIPCOL AND"); 
	    queryColaborador.append("						  HES.NUMCAD = FUN.NUMCAD AND"); 
	    queryColaborador.append("						  DATALT = (SELECT MAX(DATALT) FROM R038HES HES1 WHERE HES.NUMEMP = HES1.NUMEMP AND");
	    queryColaborador.append("						  HES.TIPCOL = HES1.TIPCOL AND"); 
	    queryColaborador.append("						  HES.NUMCAD = HES1.NUMCAD)"); 
	    queryColaborador.append(" INNER JOIN R006ESC ESC ON ESC.CODESC = HES.CODESC");						   
	    queryColaborador.append(" WHERE FUN.NUMCAD = ? AND FUN.NUMEMP = ? AND FUN.TIPCOL = ?");

	    PreparedStatement stVetorh = connVetorh.prepareStatement(queryColaborador.toString());
	    stVetorh.setLong(1, numCad);
	    stVetorh.setLong(2, numEmp);
	    stVetorh.setLong(3, tipCol);
	    ResultSet rsVetorh = stVetorh.executeQuery();
	    while (rsVetorh.next())
	    {
		escala.setCodigoEscala(rsVetorh.getLong("CodEsc"));
		escala.setDescricao(rsVetorh.getString("NOMESC"));
		escala.setCodigoTurma(rsVetorh.getLong("CODTMA"));
	    }
	    rsVetorh.close();
	    stVetorh.close();
	}
	catch (SQLException e)
	{
	    e.printStackTrace();
	}finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}

	return escala;
    }
    public static List<EntradaAutorizadaVO> listaCoberturasTarefasProgramacaoDeFerias(Long numemp, Long tipcol, Long numcad)
    {
	List<EntradaAutorizadaVO> entradaAutorizadas = new ArrayList<EntradaAutorizadaVO>();
	EntradaAutorizadaVO entradaAutorizada = new EntradaAutorizadaVO();

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    StringBuffer sqlEntradaAutorizada = new StringBuffer();
	    sqlEntradaAutorizada.append(" SELECT TOP(5) fun.NumEmp AS NumEmpFun, fun.TipCol AS TipColFun, fun.NumCad AS NumCadFun, cob.USU_DatAlt, cob.USU_HorIni, ");
	    sqlEntradaAutorizada.append(" 	   cob.USU_DatFim,sub.NumEmp AS NumEmpSub, sub.TipCol AS TipColSub, sub.NumCad As NumCadSub, sub.NomFun AS NomFunSub, mot.USU_DesMot, cob.USU_ObsCob, ");
	    sqlEntradaAutorizada.append(" 	   orn.USU_NomCli, orn.USU_LotOrn, orn.NomLoc, orn.USU_CodCcu, hes.CodEsc, hes.CodTma, esc.NomEsc, fun.numcpf");
	    sqlEntradaAutorizada.append(" FROM USU_T038COBFUN cob ");
	    sqlEntradaAutorizada.append(" INNER JOIN R034FUN fun ON fun.NumEmp = cob.USU_NumEmp AND fun.TipCol = cob.USU_TipCol AND fun.NumCad = cob.USU_NumCad ");
	    sqlEntradaAutorizada.append(" LEFT JOIN R034FUN sub ON sub.NumEmp = cob.USU_NumEmpCob AND sub.TipCol = cob.USU_TipColCob AND sub.NumCad = cob.USU_NumCadCob ");
	    sqlEntradaAutorizada.append(" LEFT JOIN R038HES hes ON hes.NumEmp = sub.NumEmp AND hes.TipCol = sub.TipCol AND hes.NumCad = sub.NumCad AND hes.DatAlt = ( ");
	    sqlEntradaAutorizada.append(" 	   SELECT MAX (DatAlt) FROM R038HES hes2 WHERE hes2.NumEmp = hes.NumEmp AND hes2.TipCol = hes.TipCol AND hes2.NumCad = hes.NumCad AND hes2.DatAlt <= cob.USU_DatAlt) ");
	    sqlEntradaAutorizada.append(" LEFT JOIN R006ESC esc ON esc.CodEsc = hes.CodEsc ");
	    sqlEntradaAutorizada.append(" INNER JOIN R016ORN orn ON orn.NumLoc = cob.USU_NumLocTra AND orn.TabOrg = cob.USU_TabOrgTra ");
	    sqlEntradaAutorizada.append(" INNER JOIN USU_T010MoCo mot ON mot.USU_CodMot = cob.USU_codMot ");
	    sqlEntradaAutorizada.append(" WHERE fun.NumEmp = ? AND fun.TipCol = ? AND fun.NumCad = ? ");
	    sqlEntradaAutorizada.append(" ORDER BY cob.USU_DatAlt DESC ");

	    PreparedStatement stEntradaAutorizada = connVetorh.prepareStatement(sqlEntradaAutorizada.toString());
	    stEntradaAutorizada.setLong(1, numemp);
	    stEntradaAutorizada.setLong(2, tipcol);
	    stEntradaAutorizada.setLong(3, numcad);
	    ResultSet rsEntradaAutorizada = stEntradaAutorizada.executeQuery();

	    //EntradaAutorizadas
	    while (rsEntradaAutorizada.next())
	    {
		entradaAutorizada = new EntradaAutorizadaVO();

		Timestamp dataCobIni = rsEntradaAutorizada.getTimestamp("USU_DatAlt");
		GregorianCalendar gDataCobIni = new GregorianCalendar();
		gDataCobIni.setTimeInMillis(dataCobIni.getTime());

		SimpleDateFormat sfinc = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		String dataCoberturaInicial = sfinc.format(dataCobIni.getTime());

		Timestamp dataCobFim = null;

		if(rsEntradaAutorizada.getTimestamp("USU_DatFim") != null){
		    dataCobFim = rsEntradaAutorizada.getTimestamp("USU_DatFim");
		}

		GregorianCalendar gDataCobFim = new GregorianCalendar();

		SimpleDateFormat sfFim = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		String dataCoberturaFinal = null;

		if(dataCobFim != null){
		    dataCoberturaFinal = sfFim.format(dataCobFim.getTime());
		}

		if (dataCobFim != null)
		{
		    gDataCobFim.setTimeInMillis(dataCobFim.getTime());
		}

		PostoVO posto = new PostoVO();
		posto.setCentroCusto(rsEntradaAutorizada.getString("USU_CodCcu"));
		posto.setLotacao(rsEntradaAutorizada.getString("USU_LotOrn"));
		posto.setNomePosto(rsEntradaAutorizada.getString("NomLoc"));

		entradaAutorizada.setDataInicial(gDataCobIni);
		entradaAutorizada.setDataFinal(gDataCobFim);
		entradaAutorizada.setHoraInicial(rsEntradaAutorizada.getLong("USU_HorIni"));
		entradaAutorizada.setDescricaoCobertura(rsEntradaAutorizada.getString("USU_DesMot"));
		entradaAutorizada.setPosto(posto);
		entradaAutorizada.setObservacao(rsEntradaAutorizada.getString("USU_ObsCob"));

		ColaboradorVO colaborador = new ColaboradorVO();

		colaborador.setNumeroEmpresa(rsEntradaAutorizada.getLong("NumEmpFun"));
		colaborador.setTipoColaborador(rsEntradaAutorizada.getLong("TipColFun"));
		colaborador.setNumeroCadastro(rsEntradaAutorizada.getLong("NumCadFun"));
		colaborador.setCpf(rsEntradaAutorizada.getLong("numcpf"));

		entradaAutorizada.setColaborador(colaborador);

		ColaboradorVO substituido = new ColaboradorVO();
		EscalaVO escala = new EscalaVO();

		// Se USU_NumCadCob é igual a 0, é cobertura de posto, senão é cobertura de colaborador
		if (rsEntradaAutorizada.getLong("NumCadSub") == 0L)
		{

		    escala.setDescricao(NeoUtils.safeDateFormat(gDataCobIni, "HH:mm") + "-" + NeoUtils.safeDateFormat(gDataCobFim, "HH:mm"));
		    substituido.setEscala(escala);
		}
		else
		{
		    substituido.setNumeroEmpresa(rsEntradaAutorizada.getLong("NumEmpSub"));
		    substituido.setTipoColaborador(rsEntradaAutorizada.getLong("TipColSub"));
		    substituido.setNumeroCadastro(rsEntradaAutorizada.getLong("NumCadSub"));
		    substituido.setNomeColaborador(rsEntradaAutorizada.getString("NomFunSub"));

		    escala.setCodigoEscala(rsEntradaAutorizada.getLong("CodEsc"));
		    escala.setCodigoTurma(rsEntradaAutorizada.getLong("CodTma"));
		    escala.setDescricao(rsEntradaAutorizada.getString("NomEsc") + " / " + escala.getCodigoTurma());
		    substituido.setEscala(escala);
		}
		entradaAutorizada.setColaboradorSubstituido(substituido);
		entradaAutorizadas.add(entradaAutorizada);

	    }

	}catch (SQLException e)
	{
	    e.printStackTrace();
	}finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}
	return entradaAutorizadas;
    }
    public static HistoricoLocalVO listaHistoricoLocal(Long numemp, Long tipcol, Long numcad, String datCor)
    {
	HistoricoLocalVO local = new HistoricoLocalVO();
	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    StringBuffer sqlLocal = new StringBuffer();
	    sqlLocal.append(" select TOP 1 fun.NumEmp, fun.TipCol, fun.NumCad, orn.NumLoc, orn.TabOrg, orn.usu_lotorn, orn.nomloc, orn.USU_CodCcu, hlo.DatAlt from R038HLO hlo ");
	    sqlLocal.append(" INNER JOIN R034FUN fun ON fun.NumEmp = hlo.NumEmp AND fun.NumCad = hlo.NumCad AND fun.TipCol = hlo.TipCol ");
	    sqlLocal.append(" INNER JOIN R016ORN orn ON orn.NumLoc = hlo.NumLoc AND orn.TabOrg = hlo.TabOrg ");
	    sqlLocal.append(" WHERE fun.NumEmp = ? AND fun.TipCol = ? AND fun.NumCad = ? and hlo.datalt < ? ");
	    sqlLocal.append(" ORDER BY hlo.datalt desc ");

	    PreparedStatement stLocal = connVetorh.prepareStatement(sqlLocal.toString());
	    stLocal.setLong(1, numemp);
	    stLocal.setLong(2, tipcol);
	    stLocal.setLong(3, numcad);
	    stLocal.setString(4, datCor);
	    ResultSet rsLocal = stLocal.executeQuery();

	    //Locals
	    while (rsLocal.next())
	    {
		local = new HistoricoLocalVO();

		Timestamp data = rsLocal.getTimestamp("DatAlt");
		GregorianCalendar gData = new GregorianCalendar();
		gData.setTimeInMillis(data.getTime());

		PostoVO posto = new PostoVO();
		posto.setNumeroLocal(rsLocal.getLong("NumLoc"));
		posto.setCodigoOrganograma(rsLocal.getLong("TabOrg"));
		posto.setLotacao(rsLocal.getString("usu_lotorn"));
		posto.setNomePosto(rsLocal.getString("NomLoc"));
		local.setPosto(posto);
	    }

	    rsLocal.close();
	    stLocal.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();
	    }
	}
	return local;
    }

    /**
     * Com base no cpf busca no cadastro de usuários do fusion o colaborador correspondente.
     * @param numCpf - numero do cpf do colaborador
     * @return Login do colaborador ou null em caso de erro ou não encontrar o registro.
     */
    public static UsuarioFusionVO getUsuarioFusionByCPF(Long numCpf){
	UsuarioFusionVO retorno = null;
	Connection con = null;
	PreparedStatement pst = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	sql.append(" select su.code, nu.email, col.cpf ,er.codigo                                       ");
	sql.append(" from neoUser nu                                                                    ");
	sql.append(" inner join securityEntity su on nu.neoId = su.neoId                                ");
	sql.append(" inner join D_colaboradores col on col.neoId = nu.neoId                             ");
	sql.append(" inner join D_GCEscritorioRegional er on col.escritorioRegional_neoId = er.neoId    ");
	sql.append(" where  su.active = 1 and nu.fullname not like 'zz%' and col.cpf = ?      			");

	try{
	    con = PersistEngine.getConnection("");
	    pst = con.prepareStatement(sql.toString());

	    pst.setLong(1, numCpf);

	    rs = pst.executeQuery();

	    if(rs.next()){
		retorno = new UsuarioFusionVO();
		retorno.setCode(rs.getString("code"));
		retorno.setEmail(rs.getString("email"));
		retorno.setCodReg(rs.getLong("codigo"));
		retorno.setCpf(rs.getLong("cpf"));
	    }

	    return retorno;
	}catch(Exception e){
	    System.out.println("Erro ao consultar usuário fusion");
	    e.printStackTrace();
	    return retorno;
	}finally{
	    OrsegupsUtils.closeConnection(con, pst, rs);
	}
    }

    public static List<MensagemTIVO> listaMensagensURA(Long numcpf)
    {
	List<MensagemTIVO> retorno = new ArrayList<MensagemTIVO>();
	Connection con = null;
	PreparedStatement pst = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	sql.append(" select m.neoId, m.cpf, m.dataCadastro, m.quantidadeExecucoes, m.dataInicio, m.dataFim, tm.descricao, am.acao, m.quantidadeExecutada from D_TIMensagem m  ");
	sql.append(" inner join D_TITipoMensagem tm on m.tipoMensagem_neoId = tm.neoId                               ");
	sql.append(" left join D_TIAcaoMensagem am on m.acao_neoId = am.neoID 										 ");
	sql.append(" left join NeoUser u on m.usuarioCadastro_neoId = u.neoId                                        ");
	sql.append(" where m.cpf = ? ");

	try{
	    con = PersistEngine.getConnection("");
	    pst = con.prepareStatement(sql.toString());

	    pst.setLong(1, numcpf);

	    rs = pst.executeQuery();

	    while(rs.next()){
		MensagemTIVO msg = new MensagemTIVO();
		msg.setNeoId(rs.getLong("neoId"));
		msg.setCpf(rs.getLong("cpf"));

		Timestamp ts = rs.getTimestamp("dataCadastro");
		GregorianCalendar gcCadastro = new GregorianCalendar();
		gcCadastro.setTimeInMillis(ts.getTime());
		msg.setDataCadastro(gcCadastro);
		msg.setStrDataCadastro(NeoDateUtils.safeDateFormat(gcCadastro));

		Timestamp tsInicio = rs.getTimestamp("dataCadastro");
		GregorianCalendar gcDataInicio = new GregorianCalendar();
		gcDataInicio.setTimeInMillis(tsInicio.getTime());
		msg.setDataInicio(gcDataInicio);


		Timestamp tsFim = rs.getTimestamp("dataCadastro");
		GregorianCalendar gcDataFim = new GregorianCalendar();
		gcDataFim.setTimeInMillis(tsFim.getTime());
		msg.setDataFim(gcDataFim);


		msg.setQuantidadeExecucoes(rs.getLong("quantidadeExecucoes"));
		msg.setTipoMensagem(rs.getString("descricao"));
		msg.setQuantidadeExecutada(rs.getLong("quantidadeExecutada"));

		msg.setLeiturasMensagem(listaLeituraMensagensURA(msg.getNeoId()));

		if (msg.getLeiturasMensagem() != null){
		    msg.setQuantidadeExecutada( (long) msg.getLeiturasMensagem().size() );
		}

		if (msg.getLeiturasMensagem() != null){
		    StringBuilder html = new StringBuilder();
		    html.append("<br><table style=\"border: 1px solid silver;\">");
		    /*html.append("	<tr style=\"background-color: #CCC;\">");
					html.append("		<td style=\"padding: 3px;\" colspan=\"4\">&nbsp;Execuções da mensagem&nbsp;</td>");
					html.append("	</tr>");*/
		    html.append("	<tr style=\"background-color: #CCC;\">");
		    html.append("		<td style=\"padding: 3px;\">&nbsp;Id mensagem&nbsp;</td>");
		    html.append("		<td style=\"padding: 3px;\">&nbsp;Telefone&nbsp;</td>");
		    html.append("		<td style=\"padding: 3px;\">&nbsp;Data Hora&nbsp;</td>");
		    html.append("		<td style=\"padding: 3px;\">&nbsp;Conclusão Mensagem&nbsp;</td>");
		    html.append("	</tr>");

		    for (LeituraMensagemTIVO leitura: msg.getLeiturasMensagem()){
			html.append("<tr>");
			html.append("	<td style=\"padding: 3px;\">"+leitura.getUserField()+"</td>");
			html.append("	<td style=\"padding: 3px;\">"+leitura.getTelefone()+"</td>");
			html.append("	<td style=\"padding: 3px;\">"+NeoDateUtils.safeDateFormat(leitura.getDataHora(),"dd/MM/yyyy HH:mm")+"</td>");
			html.append("	<td style=\"padding: 3px;\">"+leitura.getConclusaoMensagem()+"</td>");
			html.append("</tr>");
		    }
		    html.append("</table><br>");
		    msg.setHtmlLeituras(html.toString());
		}

		msg.setAcao(rs.getString("acao"));

		retorno.add(msg);
	    }

	    return retorno;
	}catch(Exception e){
	    System.out.println("Erro ao consultar usuário fusion");
	    e.printStackTrace();
	    return retorno;
	}finally{
	    OrsegupsUtils.closeConnection(con, pst, rs);
	}
    }

    public static List<LeituraMensagemTIVO> listaLeituraMensagensURA(Long neoIdMensagem)
    {
	List<LeituraMensagemTIVO> retorno = new ArrayList<LeituraMensagemTIVO>();
	Connection con = null;
	PreparedStatement pst = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	sql.append(" select  userField, telefone, dataHora, conclusaoMensagem  from D_TILeituraMensagem lm ");
	sql.append(" inner join D_TIMensagem_leituraMensagem mlm on mlm.leituraMensagem_neoId = lm.neoId and mlm.D_TIMensagem_neoId = ? ");

	try{
	    con = PersistEngine.getConnection("");
	    pst = con.prepareStatement(sql.toString());

	    pst.setLong(1, neoIdMensagem);

	    rs = pst.executeQuery();

	    while(rs.next()){
		LeituraMensagemTIVO leitura = new LeituraMensagemTIVO();
		leitura.setConclusaoMensagem(rs.getString("conclusaoMensagem"));
		leitura.setUserField(rs.getString("userField"));
		leitura.setTelefone(rs.getString("telefone"));

		Timestamp ts = rs.getTimestamp("dataHora");
		GregorianCalendar gcDataHora = new GregorianCalendar();
		gcDataHora.setTimeInMillis(ts.getTime());
		leitura.setDataHora(gcDataHora);

		retorno.add(leitura);
	    }

	    return retorno;
	}catch(Exception e){
	    System.out.println("Erro ao consultar usuário fusion");
	    e.printStackTrace();
	    return retorno;
	}finally{
	    OrsegupsUtils.closeConnection(con, pst, rs);
	}
    }

    public static List<ColaboradorVO> getListaColaboradoresHoristas(Long codigoRegional, Long codigoEmpresa, Long codigoOrganograma, List<EscalaVO> escalas, GregorianCalendar dataAtual, Long codigoTipCli, boolean isContratos)
    {

	Connection connVetorh = PersistEngine.getConnection("VETORH");
	List<ColaboradorVO> listaColaboradores = new ArrayList<ColaboradorVO>();
	ColaboradorVO colaborador = new ColaboradorVO();

	PresencaVO presenca = new PresencaVO();
	GregorianCalendar entrada;
	GregorianCalendar saida;

	GregorianCalendar dataHoje = new GregorianCalendar();

	String stringDataAtual = NeoUtils.safeDateFormat(dataAtual, "dd/MM/yyyy");
	String stringDataHoje = NeoUtils.safeDateFormat(dataHoje, "dd/MM/yyyy");
	Boolean diaAtual = true;

	String numCraAnt = "";

	GregorianCalendar dataInicioCompetencia = new GregorianCalendar();
	GregorianCalendar dataFimCompetencia = new GregorianCalendar();

	try
	{
	    dataInicioCompetencia = OrsegupsUtils.getDiaInicioCompetencia(OrsegupsUtils.getCompetenciaApuracao(dataAtual));
	    dataFimCompetencia = OrsegupsUtils.getDiaFimCompetencia(OrsegupsUtils.getCompetenciaApuracao(dataAtual));
	    dataFimCompetencia.add(Calendar.DAY_OF_MONTH, -4);
	}
	catch (ParseException e1)
	{
	    e1.printStackTrace();
	}

	//Data diferente da data atual
	if (!stringDataAtual.endsWith(stringDataHoje))
	{
	    diaAtual = false;
	}

	try
	{
	    Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();

	    /*** Recupera Colaboradores ***/
	    StringBuffer queryColaborador = new StringBuffer();
	    queryColaborador.append(" SELECT fun.NumEmp, fun.TipCol, fun.NumCad, hch.NumCra, fun.NomFun, fun.NumCpf, fun.DatNas, fun.TipSex, fun.DatAdm, ");
	    queryColaborador.append("        esc.NomEsc, car.TitRed, sis.DesSis, hes.CodEsc, hes.CodTma, cvs.USU_NumLoc, cvs.USU_TabOrg, fun.EmiCar,  ");
	    queryColaborador.append("        ISNULL(afa.SitAfa, 1) AS SitAfa, ISNULL(sit.DesSit, 'Trabalhando') AS DesSit, ISNULL(sit.usu_blopre, 'N') AS BloPre, ");
	    queryColaborador.append("        afa.DatAfa AS dataAfa, afa.DatTer, fis.USU_NomFis, afaDem.DatAfa as dataDem, cauDem.DesDem, fun.USU_MatRes, ");
	    queryColaborador.append(" 		 DATEADD(MINUTE, accS.HorAcc, accS.DatAcc) AS DatAccS, accS.DirAcc AS DirAccS, accS.USU_TipDir AS TipDirS, accS.codrlg AS codrlgS, ");
	    queryColaborador.append("        DATEADD(MINUTE, accE.HorAcc, accE.DatAcc) AS DatAccE, accE.DirAcc AS DirAccE, accE.USU_TipDir AS TipDirE, accE.codrlg AS codrlgE, ");
	    queryColaborador.append("        COUNT(sto.CodSit) AS qdeExcecao ");
	    queryColaborador.append(" FROM R034FUN fun  ");
	    queryColaborador.append(" LEFT JOIN R038HCH hch WITH (NOLOCK) ON hch.NUMEMP = fun.NumEmp AND fun.TIPCOL = hch.TIPCOL AND fun.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch.HorIni, hch.DatIni) = (SELECT MAX(DATEADD(MINUTE, hch2.HorIni, hch2.DatIni)) FROM R038HCH hch2 WHERE hch2.NUMEMP = hch.NUMEMP AND hch2.TIPCOL = hch.TIPCOL AND hch2.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch2.HorIni, hch2.DatIni) <= ?)");
	    queryColaborador.append(" INNER JOIN R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp AND hlo.TipCol = fun.TipCol AND hlo.NumCad = fun.NumCad AND hlo.DatAlt = (SELECT MAX (DATALT) FROM R038HLO TABELA001 WHERE TABELA001.NUMEMP = hlo.NUMEMP AND TABELA001.TIPCOL = hlo.TIPCOL AND TABELA001.NUMCAD = hlo.NUMCAD AND TABELA001.DATALT <= ?) ");
	    queryColaborador.append(" INNER JOIN R038HES hes WITH (NOLOCK) ON hes.NumEmp = fun.NumEmp AND hes.TipCol = fun.TipCol AND hes.NumCad = fun.NumCad AND hes.DatAlt = (SELECT MAX (DATALT) FROM R038HES TABELA001 WHERE TABELA001.NUMEMP = hes.NUMEMP AND TABELA001.TIPCOL = hes.TIPCOL AND TABELA001.NUMCAD = hes.NUMCAD AND TABELA001.DATALT <= ?) ");
	    queryColaborador.append(" INNER JOIN R006ESC esc ON esc.CodEsc = hes.CodEsc and claesc = 8 ");
	    queryColaborador.append(" INNER JOIN R038HCA hca WITH (NOLOCK) ON hca.NumEmp = fun.NumEmp AND hca.TipCol = fun.TipCol AND hca.NumCad = fun.NumCad AND hca.DatAlt = (SELECT MAX (DATALT) FROM R038HCA TABELA001 WHERE TABELA001.NUMEMP = hca.NUMEMP AND TABELA001.TIPCOL = hca.TIPCOL AND TABELA001.NUMCAD = hca.NUMCAD AND TABELA001.DATALT <= ?) ");
	    queryColaborador.append(" INNER JOIN R024CAR car ON car.EstCar = hca.EstCar AND car.CodCar = hca.CodCar ");
	    queryColaborador.append(" LEFT JOIN R024SIS sis ON sis.SisCar = car.SisCar ");
	    queryColaborador.append(" INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.NumLoc = hlo.NumLoc and orn.TabOrg = ? ");
	    queryColaborador.append(" INNER JOIN USU_T038CVS cvs WITH (NOLOCK) ON cvs.USU_NumLoc = orn.NumLoc AND cvs.USU_TabOrg = orn.TabOrg AND cvs.USU_SeqAlt = (Select MAX(c2.usu_seqalt) FROM USU_T038CVS c2 Where c2.usu_taborg = cvs.usu_taborg And c2.usu_numloc = cvs.usu_numloc And c2.usu_datalt <= ?) ");
	    queryColaborador.append(" INNER JOIN USU_TFisRes fis ON fis.USU_FisRes = fun.usu_fisres ");
	    queryColaborador.append(" LEFT JOIN R038AFA afa WITH (NOLOCK) ON (afa.NumEmp = fun.NumEmp AND afa.TipCol = fun.TipCol AND afa.NumCad = fun.NumCad AND DATEADD(minute,afa.HORAFA,afa.DATAFA) = (SELECT MAX (DATEADD(minute,HORAFA,DATAFA)) FROM R038AFA TABELA001 WHERE TABELA001.NUMEMP = afa.NUMEMP AND TABELA001.TIPCOL = afa.TIPCOL AND TABELA001.NUMCAD = afa.NUMCAD AND TABELA001.DATAFA <= ? AND (TABELA001.DatTer = '1900-12-31 00:00:00' OR TABELA001.DatTer >= cast(floor(cast(? as float)) as datetime)))) ");
	    queryColaborador.append(" LEFT JOIN R038AFA afaDem WITH (NOLOCK) ON afaDem.NumEmp = fun.NumEmp AND afaDem.TipCol = fun.TipCol AND afaDem.NumCad = fun.NumCad AND afaDem.sitAfa = 7 AND DATEADD(minute,afaDem.HORAFA,afaDem.DATAFA) = (SELECT MAX (DATEADD(minute,HORAFA,DATAFA)) FROM R038AFA TABELA001 WHERE TABELA001.NUMEMP = afaDem.NUMEMP AND TABELA001.TIPCOL = afaDem.TIPCOL AND TABELA001.NUMCAD = afaDem.NUMCAD) ");
	    queryColaborador.append(" LEFT JOIN R042CAU cauDem ON cauDem.CauDem = afaDem.CauDem ");
	    queryColaborador.append(" LEFT JOIN R010SIT sit ON sit.CodSit = afa.SitAfa ");

	    if (isContratos)
	    {
		queryColaborador.append(" LEFT JOIN R070ACC accS ON (accS.NumCra = hch.NumCra AND accS.DatAcc > DATEADD(mm, -1, ?) AND accS.TipAcc = 100 AND accS.CodPlt = 2 AND accS.usu_TabOrg = hlo.TabOrg AND accS.usu_NumLoc = hlo.NumLoc AND accS.usu_tipdir in ('P') ");
		queryColaborador.append(" 			AND DATEADD(MINUTE, accS.HorAcc, accS.DatAcc) = (SELECT MAX(DATEADD(MINUTE, accS2.HorAcc, accS2.DatAcc)) FROM R070ACC accS2 WHERE accS2.NumCra = accS.NumCra AND accS2.DatAcc > DATEADD(mm, -1, ?) AND accS2.usu_TabOrg = accS.usu_TabOrg AND accS2.usu_NumLoc = accS.usu_NumLoc AND accS2.TipAcc = 100 AND accS2.CodPlt = 2 AND accS2.usu_tipdir = accS.usu_tipdir AND DATEADD(MINUTE, accS2.HorAcc, accS2.DatAcc) <= ?))  ");
		queryColaborador.append(" LEFT JOIN R070ACC accE ON (accE.NumCra = hch.NumCra AND accE.DatAcc > DATEADD(mm, -1, ?) AND accE.TipAcc = 100 AND accE.CodPlt = 2 AND accE.usu_TabOrg = hlo.TabOrg AND accE.usu_NumLoc = hlo.NumLoc AND accE.usu_tipdir in ('P') AND accE.DirAcc = 'E' ");
		queryColaborador.append(" 			AND DATEADD(MINUTE, accE.HorAcc, accE.DatAcc) = (SELECT MAX(DATEADD(MINUTE, accE2.HorAcc, accE2.DatAcc)) FROM R070ACC accE2 WHERE accE2.NumCra = accE.NumCra AND accE2.DatAcc > DATEADD(mm, -1, ?) AND accE2.usu_TabOrg = accE.usu_TabOrg AND accE2.usu_NumLoc = accE.usu_NumLoc AND accE2.TipAcc = 100 AND accE2.CodPlt = 2 AND accE2.usu_tipdir = accE.usu_tipdir AND accE2.DirAcc = 'E' AND DATEADD(MINUTE, accE2.HorAcc, accE2.DatAcc) <= ?))  ");
	    }
	    else
	    {
		queryColaborador.append(" LEFT JOIN R070ACC accS ON (accS.NumCra = hch.NumCra AND accS.DatAcc > DATEADD(mm, -1, ?) AND accS.TipAcc = 100 AND accS.CodPlt = 2 ");
		queryColaborador.append(" 			AND DATEADD(MINUTE, accS.HorAcc, accS.DatAcc) = (SELECT MAX(DATEADD(MINUTE, accS2.HorAcc, accS2.DatAcc)) FROM R070ACC accS2 WHERE accS2.NumCra = accS.NumCra AND accS2.DatAcc > DATEADD(mm, -1, ?) AND accS2.TipAcc = 100 AND accS2.CodPlt = 2 AND DATEADD(MINUTE, accS2.HorAcc, accS2.DatAcc) <= ?)) ");
		queryColaborador.append(" LEFT JOIN R070ACC accE ON (accE.NumCra = hch.NumCra AND accE.DatAcc > DATEADD(mm, -1, ?) AND accE.TipAcc = 100 AND accE.CodPlt = 2 AND accE.DirAcc = 'E' ");
		queryColaborador.append(" 			AND DATEADD(MINUTE, accE.HorAcc, accE.DatAcc) = (SELECT MAX(DATEADD(MINUTE, accE2.HorAcc, accE2.DatAcc)) FROM R070ACC accE2 WHERE accE2.NumCra = accE.NumCra AND accE2.DatAcc > DATEADD(mm, -1, ?) AND accE2.TipAcc = 100 AND accE2.CodPlt = 2 AND accE2.DirAcc = 'E' AND DATEADD(MINUTE, accE2.HorAcc, accE2.DatAcc) <= ?)) ");
	    }

	    //Apuracao
	    queryColaborador.append(" LEFT JOIN R066APU apu ON apu.NumEmp = fun.numemp AND apu.TipCol = fun.TipCol AND apu.NumCad = fun.NumCad AND apu.DatApu >= ? AND apu.DatApu <= ? AND fun.ConRho = 2 ");
	    queryColaborador.append(" LEFT JOIN R004HOR hor ON hor.CodHor = apu.HorDat AND hor.CodHor > 9000 ");
	    queryColaborador.append(" LEFT JOIN R066SIT sap ON sap.NumEmp = apu.NumEmp AND sap.TipCol = apu.TipCol AND sap.NumCad = apu.NumCad AND sap.DatApu = apu.DatApu AND sap.USU_ExcVal <> 'S' ");
	    queryColaborador.append(" LEFT JOIN R010SIT sto ON sto.CodSit = sap.CodSit AND sto.SitExc = 'S' ");

	    queryColaborador.append(" WHERE fun.DatAdm <= ? ");
	    queryColaborador.append(" AND (afa.SitAfa IS NULL OR afa.SitAfa <> 7 OR (afa.SitAfa = 7 AND afa.DatAfa > cast(floor(cast(? as float)) as datetime))) ");
	    queryColaborador.append(" AND fun.TipCol = 1 ");
	    queryColaborador.append(" AND fun.NumEmp not in (5, 11, 14) ");

	    //Por Regional
	    if (NeoUtils.safeIsNotNull(codigoRegional))
	    {
		queryColaborador.append(" AND cvs.usu_codreg = ? ");
	    }
	    //Por Empresa
	    else
	    {
		queryColaborador.append(" AND fun.numemp = ? ");
	    }

	    queryColaborador.append(" GROUP BY orn.usu_codreg,fun.NumEmp, fun.TipCol, fun.NumCad, hch.NumCra, fun.NomFun, fun.NumCpf, fun.DatNas, fun.TipSex, fun.DatAdm, ");
	    queryColaborador.append(" 		   esc.NomEsc, car.TitRed, sis.DesSis, hes.CodEsc, hes.CodTma, cvs.USU_NumLoc, cvs.USU_TabOrg, fun.EmiCar, fun.CodTma, afa.SitAfa, ");
	    queryColaborador.append(" 		   sit.DesSit, sit.usu_blopre, accS.HorAcc, accS.DatAcc, accE.HorAcc, accE.DatAcc, accS.DirAcc, accE.DirAcc, accS.codrlg, accE.codrlg, ");
	    queryColaborador.append(" 		   accS.USU_TipDir, accE.USU_TipDir, afa.DatAfa, afa.DatTer, fis.USU_NomFis, afaDem.DatAfa, cauDem.DesDem, fun.USU_MatRes ");

	    //Asseio
	    if (codigoTipCli != 0L)
	    {
		queryColaborador.append(" ORDER BY fun.NumEmp, fun.NomFun ");
	    }
	    else
	    {
		queryColaborador.append(" ORDER BY fun.CodTma, esc.NomEsc, fun.NumEmp, fun.NomFun ");
	    }

	    Timestamp dataAtualTS = new Timestamp(dataAtual.getTimeInMillis());
	    Timestamp dataInicioCompTS = new Timestamp(dataInicioCompetencia.getTimeInMillis());
	    Timestamp dataFimCompTS = new Timestamp(dataFimCompetencia.getTimeInMillis());

	    PreparedStatement stColaborador = connVetorh.prepareStatement(queryColaborador.toString());
	    stColaborador.setTimestamp(1, dataAtualTS);
	    stColaborador.setTimestamp(2, dataAtualTS);
	    stColaborador.setTimestamp(3, dataAtualTS);
	    stColaborador.setTimestamp(4, dataAtualTS);
	    stColaborador.setLong(5, codigoOrganograma);
	    stColaborador.setTimestamp(6, dataAtualTS);
	    stColaborador.setTimestamp(7, dataAtualTS);
	    stColaborador.setTimestamp(8, dataAtualTS);
	    stColaborador.setTimestamp(9, dataAtualTS);
	    stColaborador.setTimestamp(10, dataAtualTS);
	    stColaborador.setTimestamp(11, dataAtualTS);
	    stColaborador.setTimestamp(12, dataAtualTS);
	    stColaborador.setTimestamp(13, dataAtualTS);
	    stColaborador.setTimestamp(14, dataAtualTS);
	    stColaborador.setTimestamp(15, dataInicioCompTS);
	    stColaborador.setTimestamp(16, dataFimCompTS);
	    stColaborador.setTimestamp(17, dataAtualTS);
	    stColaborador.setTimestamp(18, dataAtualTS);

	    //Por Regional
	    if (NeoUtils.safeIsNotNull(codigoRegional))
	    {
		stColaborador.setLong(19, codigoRegional);
	    }
	    //Por Empresa
	    else
	    {
		stColaborador.setLong(19, codigoEmpresa);
	    }

	    ResultSet rsColaborador = stColaborador.executeQuery();

	    log.warn("SQL - getListaColaboradores - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");

	    while (rsColaborador.next())
	    {
		colaborador = new ColaboradorVO();
		colaborador.setNumeroEmpresa(rsColaborador.getLong("NumEmp"));
		colaborador.setTipoColaborador(rsColaborador.getLong("TipCol"));
		colaborador.setNumeroCadastro(rsColaborador.getLong("NumCad"));
		colaborador.setNomeColaborador(rsColaborador.getString("NomFun"));
		colaborador.setCargo(rsColaborador.getString("TitRed"));
		colaborador.setSistemaCargo(rsColaborador.getString("DesSis"));
		colaborador.setSituacao(rsColaborador.getString("DesSit"));
		colaborador.setAcessoBloqueado("S".equals(rsColaborador.getString("BloPre")));
		colaborador.setControlaAcesso("S".equals(rsColaborador.getString("EmiCar")));
		colaborador.setCpf(rsColaborador.getLong("NumCpf"));
		colaborador.setSexo(rsColaborador.getString("TipSex"));
		colaborador.setDataNascimento(rsColaborador.getDate("DatNas"));
		colaborador.setDataAdmissao(rsColaborador.getDate("datAdm"));
		colaborador.setNumeroLocal(rsColaborador.getLong("USU_NumLoc"));
		colaborador.setCodigoOrganograma(rsColaborador.getLong("USU_TabOrg"));
		colaborador.setCodigoEscala(rsColaborador.getLong("CodEsc"));
		colaborador.setTurmaEscala(rsColaborador.getLong("CodTma"));
		colaborador.setDataAfastamento(rsColaborador.getDate("dataAfa"));
		colaborador.setDataDemissao(rsColaborador.getDate("dataDem"));
		colaborador.setFiscal(rsColaborador.getString("USU_NomFis"));
		colaborador.setMatrizResponsabilidade(rsColaborador.getString("USU_MatRes"));
		colaborador.setCausaDemissao(rsColaborador.getString("DesDem"));
		colaborador.setQdeExcecao(rsColaborador.getLong("qdeExcecao"));

		setEscalaColaborador(escalas, colaborador);

		colaborador.getEscala().setDescricao(rsColaborador.getString("NomEsc") + " / " + colaborador.getTurmaEscala());

		if (rsColaborador.getString("numcra") != null)
		{
		    colaborador.setNumeroCracha(rsColaborador.getString("numcra"));
		}
		else
		{
		    colaborador.setNumeroCracha("");
		}

		String obsSituacao = "";
		if (rsColaborador.getLong("SitAfa") != 1L)
		{
		    GregorianCalendar dataInicioAfastamento = new GregorianCalendar();
		    GregorianCalendar dataFimAfastamento = new GregorianCalendar();

		    if (rsColaborador.getTimestamp("DataAfa") != null)
		    {
			dataInicioAfastamento.setTime(rsColaborador.getTimestamp("DataAfa"));
		    }
		    if (rsColaborador.getTimestamp("DatTer") != null)
		    {
			dataFimAfastamento.setTime(rsColaborador.getTimestamp("DatTer"));
		    }
		    if ((dataInicioAfastamento != null))
		    {
			if (dataFimAfastamento.get(Calendar.YEAR) == 1900)
			{
			    obsSituacao = "A partir de " + NeoUtils.safeDateFormat(dataInicioAfastamento, "dd/MM/yyyy");
			}
			else
			{
			    obsSituacao = "De: " + NeoUtils.safeDateFormat(dataInicioAfastamento, "dd/MM/yyyy");
			    obsSituacao = obsSituacao + " a " + NeoUtils.safeDateFormat(dataFimAfastamento, "dd/MM/yyyy");
			}
		    }
		}

		if (colaborador.getDataDemissao() != null)
		{
		    if (diaAtual)
		    {
			obsSituacao = "Em aviso prévio até " + NeoUtils.safeDateFormat(colaborador.getDataDemissao(), "dd/MM/yyyy");
		    }
		    else
		    {
			if (colaborador.getDataDemissao().after(dataHoje.getTime()))
			{
			    obsSituacao = "Em aviso prévio até " + NeoUtils.safeDateFormat(colaborador.getDataDemissao(), "dd/MM/yyyy");
			}
			else
			{
			    obsSituacao = "Demitido em " + NeoUtils.safeDateFormat(colaborador.getDataDemissao(), "dd/MM/yyyy") + " por " + colaborador.getCausaDemissao();
			}
		    }
		}

		colaborador.setObsSituacao(obsSituacao);

		//PresencaVO presenca = getPresenca(colaborador, dataAtual);

		presenca = new PresencaVO();
		entrada = new GregorianCalendar();
		saida = new GregorianCalendar();

		String direcaoAcesso1 = rsColaborador.getString("DirAccS");
		Timestamp dataAcesso1 = rsColaborador.getTimestamp("DatAccS");
		String tipoDirecaoAcesso1 = rsColaborador.getString("TipDirS");
		Long coletor1 = rsColaborador.getLong("codrlgS");

		String direcaoAcesso2 = rsColaborador.getString("DirAccE");
		Timestamp dataAcesso2 = rsColaborador.getTimestamp("DatAccE");
		String tipoDirecaoAcesso2 = rsColaborador.getString("TipDirE");
		Long coletor2 = rsColaborador.getLong("codrlgE");

		if (direcaoAcesso1 != null && !direcaoAcesso1.equals(""))
		{

		    entrada = new GregorianCalendar();

		    if (direcaoAcesso1.equals("E"))
		    {
			entrada.setTimeInMillis(dataAcesso1.getTime());
			presenca.setEntrada(entrada);
			presenca.setTipoDirecao(tipoDirecaoAcesso1);
			presenca.setColetorEntrada(coletor1);
		    }
		    else if (direcaoAcesso2 != null && !direcaoAcesso2.equals(""))
		    {
			saida.setTimeInMillis(dataAcesso1.getTime());
			presenca.setSaida(saida);
			presenca.setTipoDirecao(tipoDirecaoAcesso1);
			presenca.setColetorSaida(coletor1);
		    }
		}

		if (direcaoAcesso2 != null && !direcaoAcesso2.equals(""))
		{

		    saida = new GregorianCalendar();

		    if (direcaoAcesso2.equals("E"))
		    {
			entrada.setTimeInMillis(dataAcesso2.getTime());
			presenca.setEntrada(entrada);
			presenca.setTipoDirecao(tipoDirecaoAcesso2);
			presenca.setColetorEntrada(coletor2);
		    }
		    else if (direcaoAcesso1 != null && !direcaoAcesso1.equals(""))
		    {
			saida.setTimeInMillis(dataAcesso2.getTime());
			presenca.setSaida(saida);
			presenca.setTipoDirecao(tipoDirecaoAcesso2);
			presenca.setColetorSaida(coletor2);
		    }
		}

		presenca.setStatus(getStatusPresenca(colaborador.getEscala(), presenca, dataAtual));
		colaborador.setPresenca(presenca);

		//Verifica duplicidade no caso de possuir dois acessos na mesma data
		if (numCraAnt.equals("") || !numCraAnt.equals(colaborador.getNumeroCracha()))
		{
		    // Adicionando a lista
		    listaColaboradores.add(colaborador);
		}

		numCraAnt = colaborador.getNumeroCracha();

	    }
	    getlistaAvosFerias(listaColaboradores, dataAtual);
	    rsColaborador.close();
	    stColaborador.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}

	return listaColaboradores;
    }
    public static Collection<ClienteVO> getClientesComHoristas(Collection<ColaboradorVO> colaboradores, Collection<EntradaAutorizadaVO> entradasAutorizadas, List<EscalaPostoVO> escalas, Long codigoRegional, Long codigoEmpresa, Long codigoOrganograma, GregorianCalendar dataAtual, Boolean isContratos, Long codigoTipCli, String codigoLocal)
    {


	Connection connVetorh = PersistEngine.getConnection("VETORH");
	Collection<ClienteVO> listaClientes = new ArrayList<ClienteVO>();
	Collection<PostoVO> listaPostos = new ArrayList<PostoVO>();

	GregorianCalendar dataHoje = new GregorianCalendar();

	String stringDataAtual = NeoUtils.safeDateFormat(dataAtual, "dd/MM/yyyy");
	String stringDataHoje = NeoUtils.safeDateFormat(dataHoje, "dd/MM/yyyy");
	Boolean diaAtual = true;

	//Data diferente da data atual
	if (!stringDataAtual.endsWith(stringDataHoje))
	{
	    diaAtual = false;
	}

	try
	{
	    /*** Recupera Postos ***/
	    StringBuffer queryPostos = new StringBuffer();
	    queryPostos.append(" SELECT SUBSTRING(hie.CodLoc, 1, 12) AS NumLocCli, orn.numloc, hie.codloc, SUBSTRING(hie.CodLoc, 1, 24) AS CodPai, cvs.usu_nomloc, cvs.usu_desser, pos.usu_codser, cvs.usu_numctr, cvs.usu_numpos, orn.usu_lotorn, ");
	    queryPostos.append(" 		cvs.usu_codreg, orn.usu_cliorn, cvs.usu_sitcvs, cvs.usu_qtdcon, cvs.usu_numloc, cvs.usu_codccu, cvs.usu_codemp, cvs.usu_codfil, cvs.usu_endloc, ");
	    queryPostos.append(" 		cid.NomCid, cid.EstCid, cvs.usu_endcep, orn.usu_cplctr,orn.usu_nomcid, cvs.usu_preuni * cvs.usu_qtdcvs AS valor, cvs.usu_datini, cvs.usu_datfim, cvs.usu_fimvig, ");
	    queryPostos.append(" 		cvs.usu_nomloc AS cvs_nomloc, cvs.usu_codccu AS cvs_codccu, cvs.usu_numctr AS cvs_numctr, cvs.usu_numpos AS cvs_numpos, pos.usu_peresc, mot.codmot,  mot.desmot ");
	    queryPostos.append(" FROM R016ORN orn WITH (NOLOCK) ");
	    queryPostos.append(" INNER JOIN USU_T038CVS cvs WITH (NOLOCK) ON cvs.usu_numloc = orn.numloc AND cvs.usu_taborg = orn.taborg AND cvs.usu_seqalt = (Select MAX(c2.usu_seqalt) From USU_T038CVS c2 Where c2.usu_taborg = cvs.usu_taborg And c2.usu_numloc = cvs.usu_numloc And c2.usu_datalt <= ?) ");
	    queryPostos.append(" INNER JOIN R016HIE hie WITH (NOLOCK) ON hie.taborg = cvs.usu_taborg AND cvs.usu_numloc = hie.numloc AND hie.datini = (SELECT MAX(h2.datini) FROM dbo.R016HIE h2 WHERE hie.taborg = cvs.usu_taborg AND h2.numloc = cvs.usu_numloc) ");
	    queryPostos.append(" LEFT JOIN [FSOODB04\\SQL02].SAPIENS.dbo.USU_T160CVS pos ON pos.usu_codemp = cvs.usu_codemp AND pos.usu_codfil = cvs.usu_codfil AND pos.usu_numctr = cvs.usu_numctr AND pos.usu_numpos = cvs.usu_numpos ");
	    queryPostos.append(" LEFT JOIN R074CID cid WITH (NOLOCK) ON cid.CodCid = orn.usu_codcid ");
	    queryPostos.append(" LEFT JOIN [FSOODB04\\SQL02].SAPIENS.dbo.E021MOT mot ON mot.codmot = pos.usu_codmot ");
	    queryPostos.append(" WHERE (cvs.usu_taborg = ?) ");
	    queryPostos.append(" AND (LEN(hie.CodLoc) = 27) AND EXISTS ");
	    queryPostos.append(" ( ");
	    queryPostos.append(" SELECT 1 FROM r034fun FUN  ");
	    queryPostos.append(" INNER JOIN R038HES hes WITH (NOLOCK) ON hes.NumEmp = fun.NumEmp AND hes.TipCol = fun.TipCol AND hes.NumCad = fun.NumCad AND hes.DatAlt = (SELECT MAX (DATALT) FROM R038HES TABELA001 WHERE TABELA001.NUMEMP = hes.NUMEMP AND TABELA001.TIPCOL = hes.TIPCOL AND TABELA001.NUMCAD = hes.NUMCAD AND TABELA001.DATALT <= ?) "); 
	    queryPostos.append(" INNER JOIN R006ESC esc ON esc.CodEsc = hes.CodEsc and claesc = 8 ");
	    queryPostos.append(" INNER JOIN R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp AND hlo.TipCol = fun.TipCol AND hlo.NumCad = fun.NumCad AND hlo.DatAlt = (SELECT MAX (DATALT) FROM R038HLO TABELA001 WHERE TABELA001.NUMEMP = hlo.NUMEMP AND TABELA001.TIPCOL = hlo.TIPCOL AND TABELA001.NUMCAD = hlo.NUMCAD AND TABELA001.DATALT <= ?) ");
	    queryPostos.append(" WHERE hlo.NumLoc = orn.numloc AND hlo.taborg = orn.taborg AND fun.sitafa <> 7");
	    queryPostos.append(" ) ");

	    if (diaAtual)
	    {
		queryPostos.append(" AND (cvs.USU_SITCVS = 'S' OR DATEADD(MINUTE, 1439, cvs.usu_datfim) >= ? OR cvs.usu_datfim = '1900-12-31' OR EXISTS (SELECT * FROM R034FUN fun WHERE fun.TIPCOL = 1 AND cvs.usu_numloc = fun.numloc AND cvs.usu_taborg = fun.taborg AND (fun.sitafa <> 7 OR (fun.sitafa = 7 AND fun.datafa > getDate())))) ");
	    }
	    else
	    {
		queryPostos.append(" AND (cvs.usu_sitcvs = 'S') ");
		queryPostos.append(" AND (DATEADD(MINUTE, 1439, cvs.usu_datfim) >= ? OR cvs.usu_datfim = '1900-12-31') ");
	    }

	    //Por Regional
	    if (NeoUtils.safeIsNotNull(codigoRegional))
	    {
		queryPostos.append(" AND cvs.usu_codreg = ? ");

		if (isContratos)
		{
		    queryPostos.append(" AND (hie.codloc LIKE '1.5%' OR hie.codloc LIKE '1.6%') ");

		    //Asseio
		    if (codigoTipCli > 0L)
		    {
			queryPostos.append(" AND cvs.usu_codemp IN (2, 6, 7, 8) ");
		    }
		    else
		    {
			//SOO
			if (codigoRegional == 1)
			{
			    queryPostos.append(" AND cvs.usu_codemp IN (1, 2, 6, 7, 8, 15, 17, 18, 19, 21, 22) ");
			}
			else
			{
			    queryPostos.append(" AND cvs.usu_codemp IN (1, 15, 17, 18, 19, 21, 22, 27, 28, 29) ");
			}
		    }
		}
		else
		{
		    queryPostos.append(" AND (hie.codloc LIKE '1.1%' ) ");
		    queryPostos.append(" AND cvs.usu_codemp IN (1, 2, 6, 7, 8, 15, 17, 18, 19, 21, 22) ");

		    if (codigoLocal != null && !codigoLocal.equals(""))
		    {
			queryPostos.append(" AND (hie.codloc LIKE '" + codigoLocal + "' ) ");
		    }
		}
		//Asseio
		if (codigoTipCli > 0L && codigoRegional == 9)
		{
		    queryPostos.append(" AND orn.usu_tipcli = ? ");
		}
	    }
	    //Por Empresa
	    else
	    {
		//queryPostos.append(" AND cvs.usu_codemp = 1 ");

		if (isContratos)
		{
		    queryPostos.append(" AND (hie.codloc LIKE '1.5%' OR hie.codloc LIKE '1.6%') ");
		}
		else
		{
		    queryPostos.append(" AND (hie.codloc LIKE '1.1%' ) ");
		    queryPostos.append(" AND cvs.usu_codemp IN (1, 2, 6, 7, 8, 15, 17, 18, 19, 21, 22) ");
		}
	    }

	    //Caso ADM ordenar pelo campo usu_ordexi, que será administrado pelo Sr. Valtair.
	    if (!isContratos)
	    {
		queryPostos.append(" ORDER BY orn.usu_cliorn, orn.usu_ordexi, hie.codloc ");
	    }
	    else
	    {
		queryPostos.append(" ORDER BY orn.usu_datfim desc, orn.usu_cliorn, cvs.usu_sitcvs DESC, hie.codloc ");
	    }

	    PreparedStatement stPostos = connVetorh.prepareStatement(queryPostos.toString());
	    stPostos.setTimestamp(1, new Timestamp(dataAtual.getTimeInMillis()));
	    stPostos.setLong(2, codigoOrganograma);
	    stPostos.setTimestamp(3, new Timestamp(dataAtual.getTimeInMillis()));
	    stPostos.setTimestamp(4, new Timestamp(dataAtual.getTimeInMillis()));
	    stPostos.setTimestamp(5, new Timestamp(dataAtual.getTimeInMillis()));

	    //Por Regional
	    if (NeoUtils.safeIsNotNull(codigoRegional))
	    {
		stPostos.setLong(6, codigoRegional);
		//Asseio
		if (codigoTipCli > 0L && codigoRegional == 9)
		{
		    stPostos.setLong(7, codigoTipCli);
		}
	    }
	    else
	    {
		//stPostos.setLong(4, codigoEmpresa);
	    }

	    ResultSet rsPostos = stPostos.executeQuery();

	    String codigoLocalClienteAnterior = "";
	    String nomeClienteAnterior = "";
	    Boolean primeiroRegistro = true;
	    ClienteVO cliente = new ClienteVO();
	    String codigoCliente = "";
	    String nomeCliente = "";

	    while (rsPostos.next())
	    {

		codigoCliente = rsPostos.getString("NumLocCli");
		nomeCliente = rsPostos.getString("usu_cliorn");
		PostoVO posto = new PostoVO();

		posto.setNomePosto(rsPostos.getString("usu_nomloc"));
		posto.setCentroCusto(rsPostos.getString("usu_codccu"));
		posto.setCodigoPai(rsPostos.getString("CodPai"));
		posto.setServico(rsPostos.getString("usu_desser"));
		posto.setSituacao((rsPostos.getString("usu_sitcvs").equals("S") ? "Ativo" : "Inativo"));
		posto.setVagas(rsPostos.getLong("usu_qtdcon"));
		posto.setNumeroContrato(rsPostos.getLong("usu_numctr"));
		posto.setNumeroPosto(rsPostos.getLong("usu_numpos"));
		posto.setEndereco(rsPostos.getString("usu_endloc"));
		posto.setCep(rsPostos.getString("usu_endcep"));
		posto.setValor(rsPostos.getBigDecimal("valor"));
		posto.setDataInicio(rsPostos.getDate("usu_datini"));
		posto.setDataFim(rsPostos.getDate("usu_datfim"));
		posto.setDataFimVigencia(rsPostos.getDate("usu_fimvig"));
		posto.setTelefone("");
		posto.setNumeroLocal(rsPostos.getLong("numloc"));
		posto.setCodigoLocal(rsPostos.getString("codloc"));
		posto.setCodigoOrganograma(codigoOrganograma);
		posto.setCidade(rsPostos.getString("nomcid"));
		posto.setEstado(rsPostos.getString("estcid"));
		posto.setLotacao(rsPostos.getString("usu_lotorn"));
		posto.setBairro(rsPostos.getString("usu_nomcid"));
		posto.setNumero(rsPostos.getString("usu_cplctr"));
		posto.setAjusteEscala(rsPostos.getLong("usu_peresc"));
		posto.setMotivoEncerramento(rsPostos.getString("desmot"));

		posto.setNumemp(rsPostos.getLong("usu_codemp"));
		posto.setNumfil(rsPostos.getLong("usu_codfil"));
		posto.setCodSer(rsPostos.getString("usu_codser"));

		//System.out.println(posto.getNumeroLocal());

		if (posto.getSituacao().equalsIgnoreCase("Ativo"))
		{
		    posto.setEscalaPosto(getJornadaEscalaPosto(posto.getNumeroLocal(), posto.getCodigoOrganograma(), escalas));

		    GregorianCalendar newDataAtual = (GregorianCalendar) dataAtual.clone();
		    newDataAtual.set(Calendar.HOUR_OF_DAY, 0);
		    newDataAtual.set(Calendar.MINUTE, 0);
		    newDataAtual.set(Calendar.SECOND, 0);
		    newDataAtual.set(Calendar.MILLISECOND, 0);

		    GregorianCalendar newDataFim = new GregorianCalendar();
		    newDataFim.setTime(posto.getDataFim());
		    newDataFim.set(Calendar.HOUR_OF_DAY, 0);
		    newDataFim.set(Calendar.MINUTE, 0);
		    newDataFim.set(Calendar.SECOND, 0);
		    newDataFim.set(Calendar.MILLISECOND, 0);

		    if (newDataFim.after(newDataAtual))
		    {
			posto.setSituacao("Encerrando");
		    }
		}

		if (diaAtual && posto.getDataInicio().after(dataAtual.getTime()))
		{
		    posto.setSituacao("Iniciando");
		}

		posto.setTelefone(getTelefonePosto(posto.getCentroCusto()));
		posto.setColaboradores(getColaboradores(colaboradores, posto, dataAtual));
		posto.setEntradasAutorizadas(getEntradasAutorizadas(entradasAutorizadas, posto, dataAtual));
		posto.setStatus(getStatusPosto(posto, dataAtual));

		if (primeiroRegistro)
		{
		    codigoLocalClienteAnterior = codigoCliente;
		    nomeClienteAnterior = nomeCliente;
		    primeiroRegistro = false;
		}

		if (!codigoLocalClienteAnterior.equals(codigoCliente))
		{
		    // Mudou o cliente, adiciona a lista de postos (antiga) ao cliente 
		    // e o cliente a lista de clientes, renovando o cliente e adicionando o
		    // novo posto a nova lista de postos
		    cliente.setCodigoCliente(codigoLocalClienteAnterior);
		    cliente.setNomeCliente(nomeClienteAnterior);
		    cliente.setPostos(listaPostos);
		    listaClientes.add(cliente);

		    listaPostos = new ArrayList<PostoVO>();
		    cliente = new ClienteVO();
		    codigoLocalClienteAnterior = codigoCliente;
		    nomeClienteAnterior = nomeCliente;
		}
		if (NeoUtils.safeIsNotNull(codigoEmpresa))
		{
		    if (posto.getColaboradores().size() > 0)
		    {
			listaPostos.add(posto);
		    }
		}
		else
		{
		    listaPostos.add(posto);
		}
	    }
	    cliente = new ClienteVO();
	    cliente.setCodigoCliente(codigoCliente);
	    cliente.setNomeCliente(nomeCliente);
	    cliente.setPostos(listaPostos);
	    listaClientes.add(cliente);

	    rsPostos.close();
	    stPostos.close();
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}

	return listaClientes;

    }

    public static List<EntradaAutorizadaVO> getListaEntradasAutorizadasHoristas(Long codigoRegional, Long codigoOrganograma, Collection<ColaboradorVO> colaboradores, GregorianCalendar dataAtual, Long codigoTipCli, List<EscalaVO> escalas)
    {

	Connection connVetorh = PersistEngine.getConnection("VETORH");
	List<EntradaAutorizadaVO> entradasAutorizadas = new ArrayList<EntradaAutorizadaVO>();
	EntradaAutorizadaVO entradaVO = new EntradaAutorizadaVO();
	ColaboradorVO coberturaVO;

	PresencaVO presenca = new PresencaVO();

	GregorianCalendar dataHoje = new GregorianCalendar();

	String stringDataAtual = NeoUtils.safeDateFormat(dataAtual, "dd/MM/yyyy");
	String stringDataHoje = NeoUtils.safeDateFormat(dataHoje, "dd/MM/yyyy");
	Boolean diaAtual = true;

	String numcraAnt = "";
	Long datiniAnt = 0L;
	Long datfimAnt = 0L;
	String horarioAnt = "";

	//Data diferente da data atual
	if (!stringDataAtual.endsWith(stringDataHoje))
	{
	    diaAtual = false;
	}

	GregorianCalendar dataInicioCompetencia = new GregorianCalendar();
	GregorianCalendar dataFimCompetencia = new GregorianCalendar();

	try
	{
	    dataInicioCompetencia = OrsegupsUtils.getDiaInicioCompetencia(OrsegupsUtils.getCompetenciaApuracao(dataAtual));
	    dataFimCompetencia = OrsegupsUtils.getDiaFimCompetencia(OrsegupsUtils.getCompetenciaApuracao(dataAtual));
	    dataFimCompetencia.add(Calendar.DAY_OF_MONTH, -4);
	}
	catch (ParseException e1)
	{
	    e1.printStackTrace();
	}

	try
	{
	    /*** Recupera Operacoes em Posto ***/
	    StringBuffer queryOperacao = new StringBuffer();
	    queryOperacao.append(" SELECT cob.usu_numemp, cob.usu_tipcol, cob.usu_numcad, hch.numcra, cob.usu_datalt, cob.usu_datfim, cob.USU_NumCadCob, orn.NumLoc, orn.TabOrg, ");
	    queryOperacao.append(" 		  fun.nomfun, fun.NumCpf, fun.DatNas, fun.TipSex, fun.DatAdm, fun.USU_MatRes, fun.emicar, car.titred, sis.DesSis, USU_NumEmpCob, esc.NomEsc, fun2.NomFun AS NomFun2, fun2.CodTma, ");
	    queryOperacao.append(" 		  cob.USU_HorIni, fun2.CodEsc, mo.USU_DesPre, ISNULL(sit.DesSit, 'Trabalhando') AS DesSit, afa.DatAfa AS dataAfa, afa.DatTer, ISNULL(sit.usu_blopre, 'N') AS BloPre, ");
	    queryOperacao.append(" 		  DATEADD(MINUTE, accS.HorAcc, accS.DatAcc) AS DatAccS, accS.DirAcc AS DirAccS, accS.usu_tipdir AS TipDirS, accS.codrlg AS codrlgS, ");
	    queryOperacao.append(" 		  DATEADD(MINUTE, accE.HorAcc, accE.DatAcc) AS DatAccE, accE.DirAcc AS DirAccE, accE.usu_tipdir AS TipDirE, accE.codrlg AS codrlgE, COUNT(sto.CodSit) AS qdeExcecao ");
	    queryOperacao.append(" FROM USU_T038COBFUN cob WITH (NOLOCK) ");
	    queryOperacao.append(" INNER JOIN R034FUN fun WITH (NOLOCK) ON fun.NumEmp = cob.USU_NumEmp AND fun.TipCol = cob.USU_TipCol AND fun.NumCad = cob.USU_NumCad ");
	    queryOperacao.append(" INNER JOIN R038HES hes2 ON hes2.NumEmp = fun.NumEmp AND hes2.TipCol = fun.TipCol AND hes2.NumCad = fun.NumCad AND hes2.DatAlt = (SELECT MAX (DATALT) FROM R038HES TABELA001 WHERE TABELA001.NUMEMP = hes2.NUMEMP AND TABELA001.TIPCOL = hes2.TIPCOL AND TABELA001.NUMCAD = hes2.NUMCAD AND TABELA001.DATALT <= ?) ");
	    queryOperacao.append(" INNER JOIN R006ESC esc2 WITH (NOLOCK) ON esc2.CodEsc = fun.CodEsc and esc2.claesc = 8");
	    queryOperacao.append(" LEFT JOIN R038HCH hch ON hch.NUMEMP = fun.NumEmp AND fun.TIPCOL = hch.TIPCOL AND fun.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch.HorIni, hch.DatIni) = (SELECT MAX(DATEADD(MINUTE, hch2.HorIni, hch2.DatIni)) FROM R038HCH hch2 WHERE hch2.NUMEMP = hch.NUMEMP AND hch2.TIPCOL = hch.TIPCOL AND hch2.NUMCAD = hch.NUMCAD AND DATEADD(MINUTE, hch2.HorIni, hch2.DatIni) <= ?)");
	    queryOperacao.append(" INNER JOIN R024CAR car WITH (NOLOCK) ON car.EstCar = fun.EstCar AND car.CodCar = fun.CodCar ");
	    queryOperacao.append(" LEFT JOIN R024SIS sis ON sis.SisCar = car.SisCar ");
	    queryOperacao.append(" LEFT JOIN R034FUN fun2 WITH (NOLOCK) ON fun2.NumEmp = cob.USU_NumEmpCob AND fun2.TipCol = cob.USU_TipColCob AND fun2.NumCad = cob.USU_NumCadCob ");
	    queryOperacao.append(" LEFT JOIN R038HES hes ON hes.NumEmp = fun2.NumEmp AND hes.TipCol = fun2.TipCol AND hes.NumCad = fun2.NumCad AND hes.DatAlt = (SELECT MAX (DATALT) FROM R038HES TABELA001 WHERE TABELA001.NUMEMP = hes.NUMEMP AND TABELA001.TIPCOL = hes.TIPCOL AND TABELA001.NUMCAD = hes.NUMCAD AND TABELA001.DATALT <= ?) ");
	    queryOperacao.append(" LEFT JOIN R006ESC esc WITH (NOLOCK) ON esc.CodEsc = fun2.CodEsc and esc.claesc = 8");
	    queryOperacao.append(" LEFT JOIN USU_T010MoCo mo WITH (NOLOCK) ON mo.USU_CodMot = cob.USU_codMot ");
	    queryOperacao.append(" INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.numloc = cob.usu_numloctra AND orn.taborg = cob.usu_taborgtra ");
	    queryOperacao.append(" INNER JOIN R016HIE hie WITH (NOLOCK) ON hie.taborg = orn.taborg AND orn.numloc = hie.numloc AND hie.datini = (SELECT MAX(h2.datini) FROM dbo.R016HIE h2 WHERE hie.taborg = orn.taborg AND h2.numloc = orn.numloc) ");
	    queryOperacao.append(" LEFT JOIN R038AFA afa WITH (NOLOCK) ON (afa.NumEmp = fun.NumEmp AND afa.TipCol = fun.TipCol AND afa.NumCad = fun.NumCad AND afa.DatAfa = (SELECT MAX (DATAFA) FROM R038AFA TABELA001 WHERE TABELA001.NUMEMP = afa.NUMEMP AND TABELA001.TIPCOL = afa.TIPCOL AND TABELA001.NUMCAD = afa.NUMCAD AND TABELA001.DATAFA <= ? AND (TABELA001.DatTer = '1900-12-31 00:00:00' OR TABELA001.DatTer >= cast(floor(cast(? as float)) as datetime)))) ");
	    queryOperacao.append(" LEFT JOIN R010SIT sit ON sit.CodSit = afa.SitAfa ");
	    queryOperacao.append(" LEFT JOIN R070ACC accS ON (accS.NumCra = hch.NumCra AND accS.TipAcc = 100 AND accS.DatAcc > DATEADD(mm, -1, ?) AND accS.CodPlt = 2 AND accS.usu_tipdir <> 'P' AND accS.usu_TabOrg = cob.usu_taborgtra AND accS.usu_NumLoc = cob.usu_numloctra ");
	    queryOperacao.append(" 			 AND DATEADD(MINUTE, accS.HorAcc, accS.DatAcc) = (SELECT MAX(DATEADD(MINUTE, accS2.HorAcc, accS2.DatAcc)) FROM R070ACC accS2 WHERE accS2.NumCra = accS.NumCra AND accS2.DatAcc > DATEADD(mm, -1, ?) AND accS2.usu_TabOrg = accS.usu_TabOrg AND accS2.usu_NumLoc = accS.usu_NumLoc AND accS2.TipAcc = 100 AND accS2.CodPlt = 2 AND accS2.usu_tipdir <> 'P' AND DATEADD(MINUTE, accS2.HorAcc, accS2.DatAcc) <= ?)) ");
	    queryOperacao.append(" LEFT JOIN R070ACC accE ON (accE.NumCra = hch.NumCra AND accE.DatAcc > DATEADD(mm, -1, ?) AND accE.usu_TabOrg = cob.usu_taborgtra AND accE.usu_NumLoc = cob.usu_numloctra AND accE.TipAcc = 100 AND accE.CodPlt = 2 AND accE.DirAcc = 'E' AND accE.usu_tipdir <> 'P' ");
	    queryOperacao.append(" 			 AND DATEADD(MINUTE, accE.HorAcc, accE.DatAcc) = (SELECT MAX(DATEADD(MINUTE, accE2.HorAcc, accE2.DatAcc)) FROM R070ACC accE2 WHERE accE2.NumCra = accE.NumCra AND accE2.DatAcc > DATEADD(mm, -1, ?) AND accE2.usu_TabOrg = accE.usu_TabOrg AND accE2.usu_NumLoc = accE.usu_NumLoc AND accE2.TipAcc = 100 AND accE2.CodPlt = 2 AND accE2.DirAcc = 'E' AND accE2.usu_tipdir <> 'P' AND DATEADD(MINUTE, accE2.HorAcc, accE2.DatAcc) <= ?)) ");
	    queryOperacao.append(" LEFT JOIN R066APU apu ON apu.NumEmp = fun.numemp AND apu.TipCol = fun.TipCol AND apu.NumCad = fun.NumCad AND apu.DatApu >= ? AND apu.DatApu <= ? AND fun.ConRho = 2 ");
	    queryOperacao.append(" LEFT JOIN R004HOR hor ON hor.CodHor = apu.HorDat AND hor.CodHor > 9000 ");
	    queryOperacao.append(" LEFT JOIN R066SIT sap ON sap.NumEmp = apu.NumEmp AND sap.TipCol = apu.TipCol AND sap.NumCad = apu.NumCad AND sap.DatApu = apu.DatApu ");
	    queryOperacao.append(" LEFT JOIN R010SIT sto ON sto.CodSit = sap.CodSit AND sto.SitExc = 'S' ");
	    queryOperacao.append(" WHERE cob.usu_taborgtra = ? ");
	    queryOperacao.append(" AND cast(floor(CAST(cob.USU_DatFim AS FLOAT)) AS datetime) >= CAST(floor(CAST(? AS FLOAT)) AS datetime) ");
	    queryOperacao.append(" AND mo.USU_TipCob in ('C', 'D', 'R') ");
	    queryOperacao.append(" AND orn.taborg = 203 ");
	    queryOperacao.append(" AND LEN(hie.CodLoc) = 27 ");
	    queryOperacao.append(" AND fun.NumEmp not in (5) ");

	    if (!diaAtual)
	    {
		queryOperacao.append(" AND CAST(floor(CAST(cob.usu_datalt AS FLOAT)) AS datetime) <= CAST(floor(CAST(? AS FLOAT)) AS datetime) ");

		if (NeoUtils.safeIsNotNull(codigoRegional))
		{
		    queryOperacao.append(" AND orn.usu_codreg = ? ");

		    if (codigoTipCli != 0L && codigoRegional == 9)
		    {
			queryOperacao.append(" AND orn.usu_tipcli = ? ");
		    }
		}
	    }
	    else
	    {
		if (NeoUtils.safeIsNotNull(codigoRegional))
		{
		    queryOperacao.append(" AND orn.usu_codreg = ? ");

		    if (codigoTipCli != 0L && codigoRegional == 9)
		    {
			queryOperacao.append(" AND orn.usu_tipcli = ? ");
		    }
		}
	    }

	    queryOperacao.append(" GROUP BY cob.usu_numemp, cob.usu_tipcol, cob.usu_numcad, hch.numcra, cob.usu_datalt, cob.usu_datfim, cob.USU_NumCadCob, orn.NumLoc, orn.TabOrg, ");
	    queryOperacao.append(" fun.nomfun, fun.NumCpf, fun.DatNas, fun.TipSex, fun.DatAdm, fun.USU_MatRes, fun.emicar, car.titred, sis.DesSis, USU_NumEmpCob, esc.NomEsc, fun2.NomFun, fun2.CodTma, ");
	    queryOperacao.append(" cob.USU_HorIni, fun2.CodEsc, mo.USU_DesPre, sit.DesSit, afa.DatAfa, afa.DatTer, cob.usu_numloctra, sit.usu_blopre, ");
	    queryOperacao.append(" accS.HorAcc, accS.DatAcc, accS.DirAcc, accS.usu_tipdir, accE.HorAcc, accE.DatAcc, accE.DirAcc, accE.usu_tipdir, accS.codrlg, accE.codrlg ");

	    queryOperacao.append(" ORDER BY cob.usu_numloctra, cob.USU_Datalt ");

	    PreparedStatement stOperacao = connVetorh.prepareStatement(queryOperacao.toString());
	    stOperacao.setTimestamp(1, new Timestamp(dataAtual.getTimeInMillis()));
	    stOperacao.setTimestamp(2, new Timestamp(dataAtual.getTimeInMillis()));
	    stOperacao.setTimestamp(3, new Timestamp(dataAtual.getTimeInMillis()));
	    stOperacao.setTimestamp(4, new Timestamp(dataAtual.getTimeInMillis()));
	    stOperacao.setTimestamp(5, new Timestamp(dataAtual.getTimeInMillis()));
	    stOperacao.setTimestamp(6, new Timestamp(dataAtual.getTimeInMillis()));
	    stOperacao.setTimestamp(7, new Timestamp(dataAtual.getTimeInMillis()));
	    stOperacao.setTimestamp(8, new Timestamp(dataAtual.getTimeInMillis()));
	    stOperacao.setTimestamp(9, new Timestamp(dataAtual.getTimeInMillis()));
	    stOperacao.setTimestamp(10, new Timestamp(dataAtual.getTimeInMillis()));
	    stOperacao.setTimestamp(11, new Timestamp(dataAtual.getTimeInMillis()));
	    stOperacao.setTimestamp(12, new Timestamp(dataInicioCompetencia.getTimeInMillis()));
	    stOperacao.setTimestamp(13, new Timestamp(dataFimCompetencia.getTimeInMillis()));
	    stOperacao.setLong(14, codigoOrganograma);
	    stOperacao.setTimestamp(15, new Timestamp(dataAtual.getTimeInMillis()));

	    if (!diaAtual)
	    {
		stOperacao.setTimestamp(16, new Timestamp(dataAtual.getTimeInMillis()));

		if (NeoUtils.safeIsNotNull(codigoRegional))
		{
		    stOperacao.setLong(17, codigoRegional);

		    if (codigoTipCli != 0L && codigoRegional == 9)
		    {
			stOperacao.setLong(18, codigoTipCli);
		    }
		}
	    }
	    else
	    {
		if (NeoUtils.safeIsNotNull(codigoRegional))
		{
		    stOperacao.setLong(16, codigoRegional);

		    if (codigoTipCli != 0L && codigoRegional == 9)
		    {
			stOperacao.setLong(17, codigoTipCli);
		    }
		}
	    }

	    ResultSet rsOperacao = stOperacao.executeQuery();
	    while (rsOperacao.next())
	    {
		coberturaVO = new ColaboradorVO();
		entradaVO = new EntradaAutorizadaVO();
		presenca = new PresencaVO();
		EscalaVO escala = new EscalaVO();

		coberturaVO.setNumeroEmpresa(rsOperacao.getLong("usu_numemp"));
		coberturaVO.setTipoColaborador(rsOperacao.getLong("usu_tipcol"));
		coberturaVO.setNumeroCadastro(rsOperacao.getLong("usu_numcad"));
		coberturaVO.setNomeColaborador(rsOperacao.getString("nomfun"));
		coberturaVO.setCargo(rsOperacao.getString("titred"));
		coberturaVO.setSistemaCargo(rsOperacao.getString("DesSis"));
		coberturaVO.setSituacao(rsOperacao.getString("DesSit"));
		coberturaVO.setAcessoBloqueado("S".equals(rsOperacao.getString("BloPre")));
		coberturaVO.setControlaAcesso("S".equals(rsOperacao.getString("EmiCar")));
		coberturaVO.setCpf(rsOperacao.getLong("NumCpf"));
		coberturaVO.setNumeroLocal(rsOperacao.getLong("numloc"));
		coberturaVO.setCodigoOrganograma(rsOperacao.getLong("taborg"));
		coberturaVO.setDataAfastamento(rsOperacao.getDate("dataAfa"));
		coberturaVO.setSexo(rsOperacao.getString("TipSex"));
		coberturaVO.setDataNascimento(rsOperacao.getDate("DatNas"));
		coberturaVO.setDataAdmissao(rsOperacao.getDate("datAdm"));
		coberturaVO.setMatrizResponsabilidade(rsOperacao.getString("USU_MatRes"));
		coberturaVO.setQdeExcecao(rsOperacao.getLong("qdeExcecao"));

		if (rsOperacao.getString("numcra") != null)
		{
		    coberturaVO.setNumeroCracha(rsOperacao.getString("numcra"));
		}
		else
		{
		    coberturaVO.setNumeroCracha("");
		}

		entradaVO.setDescricaoCobertura(rsOperacao.getString("usu_despre"));
		entradaVO.setHoraInicial(rsOperacao.getLong("usu_horini"));

		GregorianCalendar dataInicio = (GregorianCalendar) dataAtual.clone();
		GregorianCalendar dataFim = (GregorianCalendar) dataAtual.clone();

		dataInicio.setTime(rsOperacao.getTimestamp("usu_datalt"));
		entradaVO.setDataInicial(OrsegupsUtils.atribuiHora(dataInicio));

		dataFim.setTime(rsOperacao.getTimestamp("usu_datfim"));
		entradaVO.setDataFinal(OrsegupsUtils.atribuiHora(dataFim));

		HorarioVO horario = new HorarioVO();
		horario.setDataInicial(dataInicio);
		horario.setDataFinal(dataFim);

		// Se USU_NumCadCob é igual a 0, é cobertura de posto, senão é cobertura de colaborador
		if (rsOperacao.getLong("USU_NumCadCob") == 0L)
		{

		    escala.setHorarios(getHorariosOperacoesPostos(horario, dataAtual));
		    escala.setDescricao(NeoUtils.safeDateFormat(dataInicio, "HH:mm") + "-" + NeoUtils.safeDateFormat(dataFim, "HH:mm"));
		    String descHorarios = escala.toString();
		    escala.setDescricaoHorarios(descHorarios);

		    coberturaVO.setEscala(escala);

		    //presenca = getPresenca(coberturaVO, dataAtual);
		}
		else
		{
		    if (colaboradores != null)
		    {
			for (ColaboradorVO substituidoVO : colaboradores)
			{
			    if (substituidoVO.getNumeroCadastro().equals(rsOperacao.getLong("USU_NumCadCob")) && substituidoVO.getNumeroEmpresa().equals(rsOperacao.getLong("USU_NumEmpCob")))
			    {
				entradaVO.setColaboradorSubstituido(substituidoVO);
				coberturaVO.setEscala(substituidoVO.getEscala().clone());
				coberturaVO.getEscala().setConsiderarEscala(Boolean.TRUE);

				GregorianCalendar dtInicioAux = (GregorianCalendar) horario.getDataInicial().clone();
				GregorianCalendar dtAtualAux = (GregorianCalendar) dataAtual.clone();

				dtInicioAux.set(Calendar.HOUR_OF_DAY, 0);
				dtInicioAux.set(Calendar.MINUTE, 0);
				dtInicioAux.set(Calendar.SECOND, 0);
				dtInicioAux.set(Calendar.MILLISECOND, 0);

				dtAtualAux.set(Calendar.HOUR_OF_DAY, 0);
				dtAtualAux.set(Calendar.MINUTE, 0);
				dtAtualAux.set(Calendar.SECOND, 0);
				dtAtualAux.set(Calendar.MILLISECOND, 0);

				if (dtInicioAux.after(dtAtualAux))
				{
				    coberturaVO.getEscala().setHorarios(new ArrayList<HorarioVO>());
				}

				String descHorarios = coberturaVO.getEscala().toString();
				coberturaVO.getEscala().setDescricaoHorarios(descHorarios);

				break;
			    }
			}
		    }
		    // Se não for localizado o substituido, seta valores padroes
		    if (entradaVO.getColaboradorSubstituido() == null)
		    {
			ColaboradorVO substituidoVO = new ColaboradorVO();
			substituidoVO.setNomeColaborador("NÃO LOCALIZADO");
			coberturaVO.setEscala(new EscalaVO());
			coberturaVO.getEscala().setDescricao("NÃO LOCALIZADA");
			coberturaVO.getEscala().setHorarios(new ArrayList<HorarioVO>());
			entradaVO.setColaboradorSubstituido(substituidoVO);
		    }
		}

		if (coberturaVO.getEscala() != null)
		{
		    //presenca = getPresenca(coberturaVO, dataAtual);

		    presenca = new PresencaVO();
		    GregorianCalendar gcDataAcesso1 = new GregorianCalendar();
		    GregorianCalendar gcDataAcesso2 = new GregorianCalendar();

		    String direcaoAcesso1 = rsOperacao.getString("DirAccS");
		    String tipoDirecaoAcesso1 = rsOperacao.getString("TipDirS");
		    Timestamp dataAcesso1 = rsOperacao.getTimestamp("DatAccS");
		    Long coletor1 = rsOperacao.getLong("codrlgS");

		    String direcaoAcesso2 = rsOperacao.getString("DirAccE");
		    String tipoDirecaoAcesso2 = rsOperacao.getString("TipDirS");
		    Timestamp dataAcesso2 = rsOperacao.getTimestamp("DatAccE");
		    Long coletor2 = rsOperacao.getLong("codrlgS");

		    if (coberturaVO.getEscala().getHorarios() != null && !coberturaVO.getEscala().getHorarios().isEmpty())
		    {
			if (direcaoAcesso1 != null)
			{

			    gcDataAcesso1.setTimeInMillis(dataAcesso1.getTime());

			    if (direcaoAcesso1.equals("E"))
			    {
				presenca.setEntrada(gcDataAcesso1);
				presenca.setColetorEntrada(coletor1);
			    }
			    else if (direcaoAcesso2 != null && !direcaoAcesso2.equals(""))
			    {
				presenca.setSaida(gcDataAcesso1);
				presenca.setColetorSaida(coletor1);
			    }
			}

			if (direcaoAcesso2 != null && !direcaoAcesso2.equals(""))
			{

			    gcDataAcesso2.setTimeInMillis(dataAcesso2.getTime());

			    if (direcaoAcesso2.equals("E"))
			    {
				presenca.setEntrada(gcDataAcesso2);
				presenca.setColetorEntrada(coletor2);
			    }
			    else if (direcaoAcesso1 != null && !direcaoAcesso1.equals(""))
			    {
				presenca.setSaida(gcDataAcesso2);
				presenca.setColetorSaida(coletor2);
			    }
			}
		    }

		    presenca.setStatus(getStatusPresenca(coberturaVO.getEscala(), presenca, dataAtual));
		    coberturaVO.setPresenca(presenca);
		}

		Long datiniNew = entradaVO.getDataInicial().getTimeInMillis();
		Long datfimNew = entradaVO.getDataFinal().getTimeInMillis();

		String horarioNew = "";

		if (coberturaVO != null && coberturaVO.getEscala() != null)
		{
		    horarioNew = coberturaVO.getEscala().toString();
		}

		//Regra para remover inconsistencia quando colaborador esta afastado e não esteja cobrindo ele mesmo
		if (entradaVO.getColaboradorSubstituido() != null && !coberturaVO.getNumeroCracha().equals(entradaVO.getColaboradorSubstituido().getNumeroCracha()) && coberturaVO.isAcessoBloqueado())
		{
		    coberturaVO.getEscala().setConsiderarEscala(Boolean.FALSE);
		}

		//Regra para remover inconsistencia quando colaborador nao emite cartao ponto
		if (!coberturaVO.isControlaAcesso())
		{
		    coberturaVO.getEscala().setConsiderarEscala(Boolean.FALSE);
		}

		//Verifica duplicidade no caso de possuir dois acessos na mesma data
		if (!numcraAnt.equals(coberturaVO.getNumeroCracha()) || !datiniAnt.toString().equals(datiniNew.toString()) || !datfimAnt.toString().equals(datfimNew.toString()) || !horarioAnt.toString().equals(horarioNew))
		{
		    entradaVO.setColaborador(coberturaVO);
		    entradasAutorizadas.add(entradaVO);
		    //System.out.println(coberturaVO.getEscala());
		}

		numcraAnt = coberturaVO.getNumeroCracha();
		datiniAnt = entradaVO.getDataInicial().getTimeInMillis();
		datfimAnt = entradaVO.getDataFinal().getTimeInMillis();

		if (entradaVO.getColaborador() != null && entradaVO.getColaborador().getEscala() != null)
		{
		    horarioAnt = entradaVO.getColaborador().getEscala().toString();
		}
	    }
	    rsOperacao.close();
	    stOperacao.close();

	}
	catch (Exception e)
	{
	    e.printStackTrace();
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}

	return entradasAutorizadas;
    }

    public static List<EscalaVO> getEscalasColaboradoresHoristas(GregorianCalendar dataAtual, Long codEscala, Long codTurma, boolean validaAfa){

	List<EscalaVO> escalas = new ArrayList<EscalaVO>();

	Connection connVetorh = PersistEngine.getConnection("VETORH");

	try
	{
	    StringBuffer queryHorariosEscalas = new StringBuffer();
	    queryHorariosEscalas.append(" SELECT esc.CodEsc, esc.NomEsc, tma.CodTma, mhr.SeqMar, mhr.HorBat, 0 AS Dia  ");
	    queryHorariosEscalas.append(" FROM R006ESC esc ");
	    queryHorariosEscalas.append(" INNER JOIN R006TMA tma ON tma.CodEsc = esc.CodEsc ");
	    queryHorariosEscalas.append(" INNER JOIN R006HOR hor ON hor.CodEsc = esc.CodEsc ");
	    queryHorariosEscalas.append(" INNER JOIN R004MHR mhr ON mhr.CodHor = hor.CodHor ");
	    queryHorariosEscalas.append(" WHERE esc.claesc = 8 and DATEDIFF(dd,tma.DatBas,?-1) % (SELECT COUNT(*) FROM R006HOR hr2 WHERE hr2.CodEsc = hor.CodEsc) + 1 = hor.SeqReg ");
	    queryHorariosEscalas.append(" AND ((esc.usu_gruesc in (1,2,7) AND mhr.UsoBat IN (1,2,3)) OR (esc.usu_gruesc in (3,4,5,6,8,9,10,11,12,13))) ");
	    if (codEscala != 0L)
	    {
		queryHorariosEscalas.append(" AND hor.CodEsc = ?");
	    }
	    if (codTurma != 0L)
	    {
		queryHorariosEscalas.append(" AND tma.CodTma = ?");
	    }
	    queryHorariosEscalas.append(" GROUP BY esc.CodEsc, esc.NomEsc, tma.CodTma, mhr.SEQMAR, mhr.HorBat ");
	    queryHorariosEscalas.append(" UNION ");
	    queryHorariosEscalas.append(" SELECT esc.CodEsc, esc.NomEsc, tma.CodTma, mhr.SeqMar, mhr.HorBat, 1 AS Dia  ");
	    queryHorariosEscalas.append(" FROM R006ESC esc ");
	    queryHorariosEscalas.append(" INNER JOIN R006TMA tma ON tma.CodEsc = esc.CodEsc ");
	    queryHorariosEscalas.append(" INNER JOIN R006HOR hor ON hor.CodEsc = esc.CodEsc ");
	    queryHorariosEscalas.append(" INNER JOIN R004MHR mhr ON mhr.CodHor = hor.CodHor ");
	    queryHorariosEscalas.append(" WHERE esc.claesc = 8 and DATEDIFF(dd,tma.DatBas,?) % (SELECT COUNT(*) FROM R006HOR hr2 WHERE hr2.CodEsc = hor.CodEsc) + 1 = hor.SeqReg ");
	    queryHorariosEscalas.append(" AND ((esc.usu_gruesc in (1,2,7) AND mhr.UsoBat IN (1,2,3)) OR (esc.usu_gruesc in (3,4,5,6,8,9,10,11,12,13))) ");
	    if (codEscala != 0L)
	    {
		queryHorariosEscalas.append(" AND hor.CodEsc = ?");
	    }
	    if (codTurma != 0L)
	    {
		queryHorariosEscalas.append(" AND tma.CodTma = ?");
	    }
	    queryHorariosEscalas.append(" GROUP BY esc.CodEsc, esc.NomEsc, tma.CodTma, mhr.SeqMar, mhr.HorBat ");
	    queryHorariosEscalas.append(" ORDER BY esc.CodEsc, tma.CodTma, Dia, mhr.SeqMar  ");

	    PreparedStatement stHorariosEscalas = connVetorh.prepareStatement(queryHorariosEscalas.toString());
	    stHorariosEscalas.setTimestamp(1, new Timestamp(dataAtual.getTimeInMillis()));

	    if (codEscala != 0L)
	    {
		stHorariosEscalas.setLong(2, codEscala);
		stHorariosEscalas.setLong(3, codTurma);
		stHorariosEscalas.setTimestamp(4, new Timestamp(dataAtual.getTimeInMillis()));
		stHorariosEscalas.setLong(5, codEscala);
		stHorariosEscalas.setLong(6, codTurma);
	    }
	    else
	    {
		stHorariosEscalas.setTimestamp(2, new Timestamp(dataAtual.getTimeInMillis()));
	    }

	    ResultSet rs = stHorariosEscalas.executeQuery();
	    ResultSetMetaData rsmd = rs.getMetaData();

	    int numColumns = rsmd.getColumnCount();
	    //List listHorarios = new ArrayList();
	    List<HorariosEscalaVO> listHorariosEscala = new ArrayList<HorariosEscalaVO>();

	    Long codigoEscalaAnterior = 0L;
	    Long codigoTurmaAnterior = 0L;
	    int stackPointer = 0;
	    while (rs.next())
	    {

		Long codigoEscala = rs.getLong("CodEsc");
		Long codigoTurma = rs.getLong("CodTma");
		String nomeEscala = rs.getString("NomEsc");


		if (codigoEscalaAnterior.equals(codigoEscala) && codigoTurmaAnterior.equals(codigoTurma) ){ // só adiciona um novo horario para uma escala e turma distinta

		    HorariosEscalaHorarioVO heHoVO = new HorariosEscalaHorarioVO();
		    heHoVO.setSeqMar(rs.getLong("SeqMar"));
		    heHoVO.setHorBat(rs.getLong("HorBat"));
		    heHoVO.setDia(rs.getLong("Dia"));

		    //listHorariosEscala.get(stackPointer-1).getHorarios().add(heHoVO);

		    ArrayList<HorariosEscalaHorarioVO> listaH = (ArrayList<HorariosEscalaHorarioVO>) (listHorariosEscala.get(stackPointer-1)).getHorarios();
		    listaH.add(heHoVO);

		    listHorariosEscala.get(stackPointer-1).setHorarios(listaH);


		}else{ // cria novo objeto para escala e turma
		    codigoEscalaAnterior = codigoEscala;
		    codigoTurmaAnterior = codigoTurma;

		    HorariosEscalaVO heVO = new HorariosEscalaVO();
		    heVO.setCodEsc(codigoEscala);
		    heVO.setCodTma(codigoTurma);
		    heVO.setNomEsc(nomeEscala);

		    ArrayList<HorariosEscalaHorarioVO> horarios = new ArrayList<HorariosEscalaHorarioVO>();

		    HorariosEscalaHorarioVO heHoVO = new HorariosEscalaHorarioVO();
		    heHoVO.setSeqMar(rs.getLong("SeqMar"));
		    heHoVO.setHorBat(rs.getLong("HorBat"));
		    heHoVO.setDia(rs.getLong("Dia"));

		    horarios.add(heHoVO);

		    heVO.setHorarios(horarios);

		    listHorariosEscala.add(heVO);
		    stackPointer++;
		}
	    }

	    OrsegupsUtils.closeConnection(null, stHorariosEscalas, rs);
	    /*rs.close();
			stHorariosEscalas.close();*/

	    Long codigoEscalaLoop = 0L;
	    Long codigoTurmaLoop = 0L;

	    for (HorariosEscalaVO heVO: listHorariosEscala){

		Long codigoEscala = heVO.getCodEsc();
		Long codigoTurma = heVO.getCodTma();

		if (!codigoEscalaLoop.equals(codigoEscala) || !codigoTurmaLoop.equals(codigoTurma))
		{
		    codigoEscalaLoop = codigoEscala;
		    codigoTurmaLoop = codigoTurma;

		    EscalaVO escala = new EscalaVO();

		    escala.setCodigoEscala(codigoEscala);
		    escala.setCodigoTurma(codigoTurma);
		    escala.setDescricao( heVO.getNomEsc() + " / " + heVO.getCodTma() );
		    escala.setHorarios(new ArrayList<HorarioVO>());

		    Integer horarioAnterior = 0;
		    Integer diaRegistroAnterior = 0;

		    boolean addDiaAnterior = false;
		    boolean addDiaAtual = false;

		    List<List> listDatas = new ArrayList<List>();

		    Boolean diaTurma = false;

		    for(HorariosEscalaHorarioVO heHoVO: heVO.getHorarios() ){

			Integer horario = heHoVO.getHorBat().intValue();
			Integer seq = heHoVO.getSeqMar().intValue();

			if (horario == 1439 && seq == 1)
			{
			    horario = 0;
			}
			Integer diaRegistro = heHoVO.getDia().intValue(); // 0 - para dia anterior | 1 - para dia atual

			int hora = horario.intValue() / 60;
			int minuto = horario.intValue() % 60;

			GregorianCalendar dataRegistro = (GregorianCalendar) dataAtual.clone();
			dataRegistro.set(Calendar.HOUR_OF_DAY, hora);
			dataRegistro.set(Calendar.MINUTE, minuto);
			dataRegistro.set(Calendar.SECOND, 0);

			/*
			 * Se o registro é 0 significa que a data é anterior a data atual
			 * Se o registro é 1 significa que a data é a atual
			 */

			if (!diaRegistroAnterior.equals(diaRegistro))
			{
			    horarioAnterior = 0;
			}

			if (diaRegistro.intValue() == 0)
			{
			    dataRegistro.add(Calendar.DAY_OF_MONTH, -1);

			    if (horarioAnterior > horario || addDiaAnterior)
			    {
				//iserir hora 2359
				if (!addDiaAnterior && hora != 23 && minuto != 59)
				{
				    GregorianCalendar dataAnt = (GregorianCalendar) dataRegistro.clone();
				    dataAnt.set(Calendar.HOUR_OF_DAY, 23);
				    dataAnt.set(Calendar.MINUTE, 59);
				    List itens = new ArrayList();
				    itens.add(dataAnt);
				    itens.add(diaRegistro);
				    listDatas.add(itens);

				    GregorianCalendar dataInicio = (GregorianCalendar) dataRegistro.clone();
				    dataInicio.add(Calendar.DAY_OF_MONTH, 1);
				    dataInicio.set(Calendar.HOUR_OF_DAY, 0);
				    dataInicio.set(Calendar.MINUTE, 0);

				    //quando montar a arvore de escalas
				    if (codEscala == 0L)
				    {
					dataInicio.set(Calendar.SECOND, 00);
				    }
				    else
					//quando busca a escala individual para verificar a tolerencia
				    {
					dataInicio.set(Calendar.SECOND, 59);
				    }

				    itens = new ArrayList();
				    itens.add(dataInicio);
				    itens.add(diaRegistro);
				    listDatas.add(itens);
				}
				addDiaAnterior = true;
				dataRegistro.add(Calendar.DAY_OF_MONTH, 1);
			    }
			}
			else
			{
			    if (horarioAnterior > horario || addDiaAtual)
			    {

				if (!addDiaAtual)
				{
				    GregorianCalendar dataAnt = (GregorianCalendar) dataRegistro.clone();
				    dataAnt.set(Calendar.HOUR_OF_DAY, 23);
				    dataAnt.set(Calendar.MINUTE, 59);

				    //quando montar a arvore de escalas
				    if (codEscala == 0L)
				    {
					dataAnt.set(Calendar.SECOND, 00);
				    }
				    else
					//quando busca a escala individual para verificar a tolerencia
				    {
					dataAnt.set(Calendar.SECOND, 59);
				    }

				    List itens = new ArrayList();
				    itens.add(dataAnt);
				    itens.add(diaRegistro);
				    listDatas.add(itens);

				    GregorianCalendar dataInicio = (GregorianCalendar) dataRegistro.clone();
				    dataInicio.add(Calendar.DAY_OF_MONTH, 1);
				    dataInicio.set(Calendar.HOUR_OF_DAY, 0);
				    dataInicio.set(Calendar.MINUTE, 0);
				    dataInicio.set(Calendar.SECOND, 0);
				    itens = new ArrayList();
				    itens.add(dataInicio);
				    itens.add(diaRegistro);
				    listDatas.add(itens);
				}
				addDiaAtual = true;
				dataRegistro.add(Calendar.DAY_OF_MONTH, 1);
			    }
			}

			List itens = new ArrayList();
			itens.add(dataRegistro);
			itens.add(diaRegistro);
			listDatas.add(itens);
			horarioAnterior = horario;
			diaRegistroAnterior = diaRegistro;


		    } // end for horariosEscalaHorario

		    //Compara se a data atual é igual aos registros do HorarioVO
		    GregorianCalendar parametroDataAtual = (GregorianCalendar) dataAtual.clone();
		    int diaAtual = parametroDataAtual.get(Calendar.DAY_OF_MONTH);
		    int cont = 0;
		    GregorianCalendar horIniEsc = new GregorianCalendar();
		    for (int l = 0; l < listDatas.size(); l++)
		    {
			HorarioVO horario = new HorarioVO();

			Boolean isDataInicial = l % 2 == 0;

			if (isDataInicial)
			{
			    GregorianCalendar dataIni = (GregorianCalendar) listDatas.get(l).get(0);
			    GregorianCalendar dataFim = (GregorianCalendar) listDatas.get(l + 1).get(0);

			    int diaDataIni = dataIni.get(Calendar.DAY_OF_MONTH);

			    //Verifica se e o dia atual
			    if (diaDataIni == diaAtual)
			    {
				Calendar calendarIni = Calendar.getInstance();
				Calendar calendarFim = Calendar.getInstance();

				calendarIni.setTime(dataIni.getTime());
				calendarFim.setTime(dataFim.getTime());

				if (calendarIni.get(Calendar.SECOND) == 00)
				{
				    horario.setDataInicial(dataIni);
				}

				if (calendarFim.get(Calendar.SECOND) == 00)
				{
				    horario.setDataFinal(dataFim);
				}

				//horario.setDataFinal(dataFim);
				escala.getHorarios().add(horario);
			    }
			}
		    }
		    String descHorarios = escala.toString();
		    escala.setDescricaoHorarios(descHorarios);
		    escalas.add(escala);
		}
	    }
	}
	catch (Exception e)
	{
	    e.printStackTrace();
	    //System.out.println("");
	}
	finally
	{
	    try
	    {
		connVetorh.close();
	    }
	    catch (SQLException e)
	    {
		e.printStackTrace();

	    }
	}

	return escalas;

    }

}
