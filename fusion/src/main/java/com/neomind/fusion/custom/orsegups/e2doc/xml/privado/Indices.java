package com.neomind.fusion.custom.orsegups.e2doc.xml.privado;

public class Indices {
    
    private String i0;
    private String i1;
    private String i2;
    private String i3;
    private String i4;
    private String i5;
    
    public String getI0() {
        return i0;
    }
    public void setI0(String i0) {
        this.i0 = i0;
    }
    public String getI1() {
        return i1;
    }
    public void setI1(String i1) {
        this.i1 = i1;
    }
    public String getI2() {
        return i2;
    }
    public void setI2(String i2) {
        this.i2 = i2;
    }
    public String getI3() {
        return i3;
    }
    public void setI3(String i3) {
        this.i3 = i3;
    }
    public String getI4() {
        return i4;
    }
    public void setI4(String i4) {
        this.i4 = i4;
    }
    public String getI5() {
	return i5;
    }
    public void setI5(String i5) {
	this.i5 = i5;
    }
}
