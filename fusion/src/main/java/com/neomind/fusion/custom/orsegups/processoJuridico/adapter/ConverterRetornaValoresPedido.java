package com.neomind.fusion.custom.orsegups.processoJuridico.adapter;

import java.math.BigDecimal;
import java.util.GregorianCalendar;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.ConverterRetornaValoresPedido
public class ConverterRetornaValoresPedido extends StringConverter
{
	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin)
	{
		NeoObject objPrincipal = field.getForm().getObject();
		EntityWrapper wPedido = new EntityWrapper(objPrincipal);
		StringBuilder sql = new StringBuilder();

		sql.append("select p.neoId from D_j002Principal p ");
		sql.append("inner join D_j002Principal_lisPed lp on p.neoId = lp.D_j002Principal_neoId ");
		sql.append("where lp.lisPed_neoId = :neoId");

		EntityManager entityManager = PersistEngine.getEntityManager("");
		Query queryBuscaNeoId = entityManager.createNativeQuery(sql.toString());
		queryBuscaNeoId.setParameter("neoId", (Long) wPedido.getValue("neoId"));
		queryBuscaNeoId.setMaxResults(1);
		Long neoIdPrincipal = 0L;

		Object obj = queryBuscaNeoId.getSingleResult();
		if (NeoUtils.safeIsNotNull(obj))
		{
			neoIdPrincipal = Long.parseLong(obj.toString());
		}

		if (neoIdPrincipal > 0)
		{
			NeoObject principal = PersistEngine.getNeoObject(neoIdPrincipal);
			EntityWrapper wrapper = new EntityWrapper(principal);
			NeoObject colaborador = (NeoObject) wrapper.getValue("colaborador");
			EntityWrapper wColaborador = new EntityWrapper(colaborador);
			GregorianCalendar datDem = null;
			String semData = "31/12/1900";

			if (wColaborador.getValue("datafa") != null && !semData.equals(NeoDateUtils.safeDateFormat((GregorianCalendar) wColaborador.getValue("datafa"), "dd/MM/yyyy")))
			{
				datDem = (GregorianCalendar) wColaborador.getValue("datafa");
			}
			else
			{
				datDem = new GregorianCalendar();
			}

			GregorianCalendar datAdm = (GregorianCalendar) wColaborador.getValue("datadm");
			BigDecimal diferencaMeses = null;
			if (wPedido.getValue("qtdMesTrab") == null || (long) wPedido.getValue("qtdMesTrab") == 0)
			{
				diferencaMeses = new BigDecimal((datAdm.getTime().getTime() - datDem.getTime().getTime()) / (1000 * 60 * 60 * 24) / 30 * -1);
				wPedido.setValue("qtdMesTrab", Long.parseLong(diferencaMeses.toString()));
			}
			else
			{
				diferencaMeses = new BigDecimal(Long.parseLong(String.valueOf(wPedido.getValue("qtdMesTrab"))));
			}
			//Fator multiplicador
			NeoObject claPed = (NeoObject) wPedido.getValue("j002ClaPed");
			if (claPed != null)
			{
				EntityWrapper wClaPed = new EntityWrapper(claPed);
				if (wClaPed.getValue("fatorMultiplicador") != null)
				{
					BigDecimal fatorM = new BigDecimal(String.valueOf(wClaPed.getValue("fatorMultiplicador")));
					BigDecimal vlrUni = new BigDecimal(String.valueOf(wPedido.getValue("vlrUniMen")));

					BigDecimal vlrPed = new BigDecimal(((vlrUni.doubleValue() * diferencaMeses.doubleValue()) / 100) * fatorM.doubleValue());
					wPedido.findField("vlrPed").setValue(null);
					wPedido.findField("vlrPed").setValue(new BigDecimal(12));
					return "<input name=\"var_lisPed__vlrPed__\" errorspan=\"err_var_lisPed__vlrPed__\" id=\"var_lisPed__vlrPed__\" class=\"input_text innerField\" mandatory=\"true\" value=\"" + vlrPed.toString() + "\"></input>";
				}
			}

		}

		//		long numcad = 10127;
		//		long numemp = 1L;
		//		String codccu = "6459200";
		//		String dep = "0";
		//		StringBuilder outBuilder = new StringBuilder();
		//		outBuilder.append(" <script>");
		//		outBuilder.append(" 	function showFichaColaborador(numemp, tipcol, numcad, dep, codccu) {");
		//		outBuilder.append(" 	var caminho = 'http://localhost:80/fusion/custom/jsp/orsegups/processoJuridico/fichaColaboradorJuridico.jsp?numemp='+numemp+'&tipcol='+tipcol+'&numcad='+numcad+'&dep='+dep+'&codccu='+codccu;");
		//		outBuilder.append("     var newModalId = NEO.neoUtils.dialog().addModal(true,null,null,null,null,''); ");
		//		outBuilder.append("		var win = window.name;  ");
		//		outBuilder.append("	    NEO.neoUtils.dialog().createModal(caminho+'&modalId='+newModalId+'&idDiv='+win);");
		//		outBuilder.append(" 	}");
		//		outBuilder.append(" </script>");
		//
		//		outBuilder.append(" <script type=\"text/javascript\"");
		//		outBuilder.append(" src=" + PortalUtil.getBaseURL() + "custom/jsp/orsegups/maps/js/jquery-1.7.2.min.js></script>  ");
		//		outBuilder.append(" <script type=\"text/javascript\" ");
		//		outBuilder.append(" src=" + PortalUtil.getBaseURL() + "ustom/jsp/orsegups/jms/jquery.easyui.min.js></script> ");
		//		outBuilder.append(" <script type=\"text/javascript\" ");
		//		outBuilder.append(" src=" + PortalUtil.getBaseURL() + "custom/jsp/orsegups/jms/jquery.easyui-lang-pt_BR.js></script> ");
		//		outBuilder.append(" <link rel=\"stylesheet\" type=\"text/css\"  ");
		//		outBuilder.append(" href=" + PortalUtil.getBaseURL() + "custom/jsp/orsegups/jms/themes/default/easyui.css> ");
		//		outBuilder.append(" <link rel=\"stylesheet\" type=\"text/css\" ");
		//		outBuilder.append("<script type=\"text/javascript\" ");
		//		outBuilder.append("src=" + PortalUtil.getBaseURL() + "custom/jsp/orsegups/overlib/overlib.js></script>");
		//		outBuilder.append("<script type=\"text/javascript\"");
		//		outBuilder.append("src=" + PortalUtil.getBaseURL() + "custom/jsp/orsegups/maps/js/json2.js\"></script>");
		//		outBuilder.append(" <link rel=\"stylesheet\" type=\"text/css\" href="+PortalUtil.getBaseURL()+"custom/jsp/orsegups/jms/themes/icon.css> "); 
		//		outBuilder.append(" <script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script> ");
		//		outBuilder.append("	<a href=\"javascript:showFichaColaborador(" + numemp + "," + 1 + "," + numcad + "," + dep + "," + codccu + ");\"><img class=\"tableIcon\" src=\"imagens/icones_final/properties_16x16-trans.png\" title=\"Visualizar\" align=\"absmiddle\"></a>&nbsp;" + "");
		//		return outBuilder.toString();
		return "";
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}
}
