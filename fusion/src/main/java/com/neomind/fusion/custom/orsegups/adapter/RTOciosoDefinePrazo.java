package com.neomind.fusion.custom.orsegups.adapter;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class RTOciosoDefinePrazo implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity) 
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		GregorianCalendar novoPrazo = new GregorianCalendar(); 
		int hora = novoPrazo.get(Calendar.HOUR_OF_DAY);
		int minuto = novoPrazo.get(Calendar.MINUTE);
		int segundos = novoPrazo.get(Calendar.SECOND);
		
		NeoUser responsavel = (NeoUser) origin.getUser();	
		
		if((responsavel.getGroup().getCode().contains("Diretor")) || (responsavel.getGroup().getCode().contains("Presidente")))
		{
			novoPrazo  = OrsegupsUtils.getSpecificWorkDay(novoPrazo, 2L);
		} 
		else 
		{
			novoPrazo  = OrsegupsUtils.getSpecificWorkDay(novoPrazo, 1L);		
		}
		
		novoPrazo.set(GregorianCalendar.HOUR_OF_DAY, hora);
		novoPrazo.set(GregorianCalendar.MINUTE, minuto);
		novoPrazo.set(GregorianCalendar.SECOND, segundos);
	
		processEntity.setValue("prazo", novoPrazo);		
	}
	
	@Override
	public void back(EntityWrapper arg0, Activity arg1) 
	{
		// TODO Auto-generated method stub
		
	}	

}