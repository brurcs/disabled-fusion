package com.neomind.fusion.custom.orsegups.justificativaAfastamento.adapter;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;

public class JAAbrirTarefaSimples implements AdapterInterface {

    @Override
    public void start(Task origin, EntityWrapper processEntity, Activity activity) {

	origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);

	@SuppressWarnings("unchecked")
	List<NeoObject> tarefas = (List<NeoObject>) processEntity.findValue("tarefaSimples");

	for (NeoObject tarefa : tarefas) {
	    EntityWrapper wTarefa = new EntityWrapper(tarefa);

	    NeoUser executorObj = (NeoUser) wTarefa.findField("executor").getValue();

	    String executor = executorObj.getCode();

	    NeoUser solicitanteObj = (NeoUser) wTarefa.findField("solicitante").getValue();

	    String solicitante = solicitanteObj.getCode();

	    String tituloPai = activity.getTitle();

	    String arrayPai[] = tituloPai.split("-");

	    String titulo = "J003-" + arrayPai[0] + "-" + arrayPai[2] + "-" + arrayPai[3] + "-" + arrayPai[4] + "- " + (String) wTarefa.findField("titulo").getValue();

	    String descricao = (String) wTarefa.findField("descricao").getValue();

	    GregorianCalendar prazo = new GregorianCalendar();

	    prazo = (GregorianCalendar) wTarefa.findField("prazo").getValue();

	    NeoDateUtils.safeDateFormat(prazo, "dd/MM/yyyy HH:mm:ss");

	    prazo.set(Calendar.HOUR_OF_DAY, 23);
	    prazo.set(Calendar.MINUTE, 59);
	    prazo.set(Calendar.SECOND, 59);

	    NeoFile anexo = (NeoFile) processEntity.findValue("anexo");

	    IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();

	    String codigoTarefa = "";
	    if (anexo != null) {
		codigoTarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "hadouken", prazo, anexo);
	    } else {
		codigoTarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "hadouken", prazo);
	    }

	    if (codigoTarefa.isEmpty() || codigoTarefa.contains("erro")) {

		throw new WorkflowException("Erro ao abrir tarefa simples! Verifique o prazo informado.");

	    }

	}

    }

    @Override
    public void back(EntityWrapper processEntity, Activity activity) {

    }

}
