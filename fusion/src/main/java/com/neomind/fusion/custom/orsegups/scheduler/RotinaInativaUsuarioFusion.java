package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.GregorianCalendar;

import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.SecurityEntity;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class RotinaInativaUsuarioFusion implements CustomJobAdapter
{

	private static final Log log = LogFactory.getLog(AbreTarefaSimplesRelatorioSimplesRsc.class);

	@SuppressWarnings("unchecked")
	@Override
	public void execute(CustomJobContext arg0)
	{

		log.warn("##### INICIO AGENDADOR DE TAREFA: Inativa Usuário Fusion - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		try
		{
			StringBuilder sql = new StringBuilder();

			sql.append(" select us.fullName, en.code,  us.lastLogin, en.active ");
			sql.append(" from dbo.NeoUser us ");
			sql.append(" inner join dbo.SecurityEntity en on en.neoId = us.neoId ");
			sql.append(" where  en.active = 1 ");
			sql.append(" and us.lastLogin <= getDate() - 90 ");
			sql.append(" and us.fullName not like '%berger%' ");
			sql.append(" and en.code not in ('giliardi', 'gilsoncesar', 'sistemaFusion','sistemaSigma', 'sistemaSapiens', 'usuario.reciclagem', 'usuario_reciclagem','tamara.oliveira') ");
			sql.append(" order by us.lastLogin desc ");

			Query query = PersistEngine.getEntityManager().createNativeQuery(sql.toString());
			Collection<Object> resultList = query.getResultList();

			for (Object tarefa : resultList)
			{
				Object[] result = (Object[]) tarefa;
				String nome = (String) result[0];
				String code = (String) result[1];
				String dataUltimoAcesso = NeoDateUtils.safeDateFormat((Timestamp) result[2], "dd/MM/yyyy HH:mm:ss");
				
				SecurityEntity entity = (SecurityEntity) PersistEngine.getNeoObject(SecurityEntity.class, new QLEqualsFilter("code", code));
				entity.setActive(Boolean.FALSE);

				InstantiableEntityInfo inatUsu = AdapterUtils.getInstantiableEntityInfo("InativaUsuarioFusion");
				NeoObject noInatUsu = inatUsu.createNewInstance();
				EntityWrapper tmeWrapper = new EntityWrapper(noInatUsu);
				tmeWrapper.findField("codogoUsuario").setValue(code);
				tmeWrapper.findField("nomeUsuario").setValue(nome);
				tmeWrapper.findField("dataUltimoAcessoFusion").setValue(dataUltimoAcesso);
				PersistEngine.persist(noInatUsu);

				PersistEngine.persist(entity);
			}
		}
		catch (Exception e)
		{

			log.error("##### AGENDADOR DE TAREFA: Erro ao Inativar Usuário no Fusion");
			System.out.println("[" + key + "] ##### AGENDADOR DE TAREFA: Erro ao Inativar Usuário no Fusion");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		}
		log.warn("##### FIM Inativar Usuário no Fusiono - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	}

}
