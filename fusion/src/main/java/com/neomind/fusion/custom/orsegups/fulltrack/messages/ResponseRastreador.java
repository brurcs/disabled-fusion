package com.neomind.fusion.custom.orsegups.fulltrack.messages;

import java.util.List;

import com.neomind.fusion.custom.orsegups.fulltrack.bean.FulltrackRastreador;

public class ResponseRastreador extends Response{

    private List<FulltrackRastreador> data;

    public List<FulltrackRastreador> getData() {
        return data;
    }

    public void setData(List<FulltrackRastreador> data) {
        this.data = data;
    }
    
    
    
}
