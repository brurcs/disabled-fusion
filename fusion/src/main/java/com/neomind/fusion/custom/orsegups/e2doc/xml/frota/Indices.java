package com.neomind.fusion.custom.orsegups.e2doc.xml.frota;

public class Indices {
    
    private String i0;
    private String i1;
    private String i2;
    
    public String getI0() {
        return i0;
    }
    public void setI0(String i0) {
        this.i0 = i0;
    }
    public String getI1() {
        return i1;
    }
    public void setI1(String i1) {
        this.i1 = i1;
    }
    public String getI2() {
        return i2;
    }
    public void setI2(String i2) {
        this.i2 = i2;
    }
    
    
}
