package com.neomind.fusion.custom.orsegups.mainlingcontratos.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.neomind.fusion.custom.orsegups.mainlingcontratos.utils.ContratosUtils;
import com.neomind.fusion.custom.orsegups.mainlingcontratos.utils.GeraRelatorioMainlingEmXLS;
import com.neomind.fusion.custom.orsegups.mainlingcontratos.vo.ContratosVO;
import com.neomind.fusion.custom.orsegups.mainlingcontratos.vo.RegionalVO;
import com.neomind.fusion.custom.orsegups.mainlingcontratos.vo.ServicosVO;
import com.neomind.util.NeoDateUtils;

@WebServlet(name = "mainlingContratosServlet", urlPatterns = { "/servlet/com.neomind.fusion.custom.orsegups.mainlingcontratos.servlet.MainlingContratosServlet" })
public class MainlingContratosServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	private static final Log log = LogFactory.getLog(MainlingContratosServlet.class);
	EntityManager entityManager;
	EntityTransaction transaction;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);

	}

	private void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		try
		{

			PrintWriter out = null;

			String action = "";

			if (request.getParameter("action") != null)
			{
				action = request.getParameter("action");
				response.setCharacterEncoding("ISO-8859-1");
				if (!action.equals("exportadorXls"))
				{
					response.setContentType("text/plain");

					out = response.getWriter();
				}

				System.out.println("Action qlservlet:" + action);
			}

			if (action.equalsIgnoreCase("listaContratos"))
			{
				ContratosVO contratosVO = new ContratosVO();
				RegionalVO regionalVO = new RegionalVO();
				ServicosVO servicoVO = new ServicosVO();
				contratosVO.setRazao(request.getParameter("razaoSocial"));
				contratosVO.setFaturamentoContrato(request.getParameter("faturamentoContrato"));
				contratosVO.setCidadeContrato(request.getParameter("cidadeContrato"));
				contratosVO.setCidadeCliente(request.getParameter("cidadeCliente"));
				contratosVO.setSituacao(request.getParameter("situacao"));
				contratosVO.setTipoEmc(request.getParameter("tipoEmc"));
				contratosVO.setTipoCliente(request.getParameter("tipoCliente"));
				String datainicio = request.getParameter("dataInicio");
				String dataFim = request.getParameter("dataFim");
				String dataCadastroInicio = request.getParameter("dataCadastroInicio");
				String dataCadastroFim = request.getParameter("dataCadastroFim");
				regionalVO.setCodigoRegional(request.getParameter("regional"));
				servicoVO.setAbreviatura(request.getParameter("servico"));
				contratosVO.setRegional(regionalVO);
				contratosVO.setServicosVO(servicoVO);

				listaContrato(contratosVO, datainicio, dataFim, dataCadastroInicio, dataCadastroFim, out);

			}

			if (action.equalsIgnoreCase("listaRegionais"))
			{

				listaRegionais(out);

			}

			if (action.equalsIgnoreCase("listaServicos"))
			{

				listaServicos(out);

			}

			if (action.equalsIgnoreCase("exportadorXls"))
			{
				ContratosVO contratosVO = new ContratosVO();
				RegionalVO regionalVO = new RegionalVO();
				ServicosVO servicoVO = new ServicosVO();
				contratosVO.setRazao(request.getParameter("razaoSocial"));
				contratosVO.setFaturamentoContrato(request.getParameter("faturamentoContrato"));
				contratosVO.setCidadeContrato(request.getParameter("cidadeContrato"));
				contratosVO.setCidadeCliente(request.getParameter("cidadeCliente"));
				contratosVO.setSituacao(request.getParameter("situacao"));
				contratosVO.setTipoEmc(request.getParameter("tipoEmc"));
				contratosVO.setTipoCliente(request.getParameter("tipoCliente"));
				String datainicio = request.getParameter("dataInicio");
				String dataFim = request.getParameter("dataFim");
				String dataCadastroInicio = request.getParameter("dataCadastroInicio");
				String dataCadastroFim = request.getParameter("dataCadastroFim");
				regionalVO.setCodigoRegional(request.getParameter("regional"));
				servicoVO.setAbreviatura(request.getParameter("servico"));
				contratosVO.setRegional(regionalVO);
				contratosVO.setServicosVO(servicoVO);
				exportadorXls(contratosVO, datainicio, dataFim, dataCadastroInicio, dataCadastroFim, response);

			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void listaContrato(ContratosVO contratosVO, String dataInicio, String dataFim, String dataCadastroInicio, String dataCadastroFim, PrintWriter out) throws JSONException
	{

		List<ContratosVO> listaContrato = null;
		ContratosUtils utils = new ContratosUtils();

		listaContrato = utils.getLista(contratosVO, dataInicio, dataFim, dataCadastroInicio, dataCadastroFim);

		JSONArray jsonContratos = new JSONArray();

		if (!listaContrato.isEmpty())
		{

			for (ContratosVO ctr : listaContrato)
			{
				JSONObject jsonCobertura = new JSONObject();

				jsonCobertura.put("razao", ctr.getRazao());
				jsonCobertura.put("faturamentoContrato", ctr.getFaturamentoContrato());
				jsonCobertura.put("situacao", ctr.getSituacao());
				jsonCobertura.put("cidadeContrato", ctr.getCidadeContrato());
				jsonCobertura.put("regional", ctr.getRegional().getNomeRegional());
				jsonCobertura.put("servico", ctr.getServicosVO().getDescricaoServico());
				jsonCobertura.put("email", ctr.getEmail());
				jsonCobertura.put("cidadeCliente", ctr.getCidadeCliente());
				jsonCobertura.put("dataInicio", NeoDateUtils.safeDateFormat(ctr.getDataInicio(), "dd/MM/yyyy"));
				jsonCobertura.put("dataCadastro", NeoDateUtils.safeDateFormat(ctr.getDataCadastroInicio(), "dd/MM/yyyy"));
				jsonCobertura.put("tipoEmc", ctr.getTipoEmc());
				jsonCobertura.put("tipoCliente", ctr.getTipoCliente());

				jsonContratos.put(jsonCobertura);
			}
		}
		out.print(jsonContratos);
		out.flush();
		out.close();
	}

	public void listaRegionais(PrintWriter out)
	{

		ContratosUtils utils = new ContratosUtils();

		List<RegionalVO> listaRegional = utils.getLista();

		Gson gson = new Gson();
		String retorno = gson.toJson(listaRegional);

		out.print(retorno);
		out.flush();
		out.close();

	}

	public void listaServicos(PrintWriter out)
	{

		ContratosUtils utils = new ContratosUtils();

		List<ServicosVO> listaServico = utils.getListaServicos();

		Gson gson = new Gson();
		String retorno = gson.toJson(listaServico);

		out.print(retorno);
		out.flush();
		out.close();

	}

	public void exportadorXls(ContratosVO contratosVO, String dataInicio, String dataFim, String dataCadastroInicio, String dataCadastroFim, HttpServletResponse resp) throws IOException
	{

		ContratosUtils utils = new ContratosUtils();
		List<ContratosVO> listaContrato = utils.getLista(contratosVO, dataInicio, dataFim, dataCadastroInicio, dataCadastroFim);

		GeraRelatorioMainlingEmXLS.execute(resp, listaContrato);

	}

}
