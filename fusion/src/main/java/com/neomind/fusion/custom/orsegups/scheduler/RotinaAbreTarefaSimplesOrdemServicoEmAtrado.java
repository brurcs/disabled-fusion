package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class RotinaAbreTarefaSimplesOrdemServicoEmAtrado implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(RotinaAbreTarefaSimplesOrdemServicoEmAtrado.class);

	@Override
	public void execute(CustomJobContext arg0)
	{
		List<String> regionais = new ArrayList<String>();
		regionais = OrsegupsUtils.listaRegionaisSigla();
		
		for (int i = 0; i < regionais.size(); i++)
		{
			String reg = regionais.get(i);
			Long codReg = OrsegupsUtils.getCodigoRegional(reg);
			Connection conn = null;
			PreparedStatement pstm = null;
			ResultSet rs = null;
			StringBuilder txtCol = null;
			StringBuffer sql1 = null;
			List<NeoObject> cols = null;
			Long key = GregorianCalendar.getInstance().getTimeInMillis();
			
			log.warn("##### INICIAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Ordens de Serviço Em Atraso -- Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			
			try
			{
				conn = PersistEngine.getConnection("SIGMA90");
				txtCol = new StringBuilder();
				sql1 = new StringBuffer();
				cols = new ArrayList<NeoObject>();

				cols = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("OSSigmaColaboradores"));

				if (cols != null && !cols.isEmpty())
				{
					int cont = 0;
					for (NeoObject col : cols)
					{
						EntityWrapper colWrapper = new EntityWrapper(col);
						String nomCol = (String) colWrapper.findValue("colaborador.nm_colaborador");

						txtCol.append("'");
						txtCol.append(nomCol);
						txtCol.append("'");
						if (cont < cols.size() - 1)
						{
							txtCol.append(",");
						}
						cont++;
					}
				}

				//Ordens de Serviço	
				sql1.append(" SELECT 'OS aberta a mais de 05 dias úteis sem execução e agendamento' AS TIPOOS, ");
				sql1.append(" col.NM_COLABORADOR , def.DESCRICAODEFEITO, os.ID_ORDEM, os.ABERTURA, os.DEFEITO, os.ID_INSTALADOR, ");
				sql1.append(" c.ID_EMPRESA, c.ID_CENTRAL, c.PARTICAO , c.CD_CLIENTE, c.FANTASIA, c.RAZAO, c.ENDERECO, bai.NOME AS NOMEBAIRRO, ");
				sql1.append(" cid.NOME AS NOMECIDADE, os.FECHAMENTO, os.FECHADO, osh.DATAINICIOEXECUCAO, osh.DATAFIMEXECUCAO, os.EXECUTADO, sol.NM_DESCRICAO, CD_COLABORADOR, ");
				sql1.append(" mp.NM_DESCRICAO AS NOMEMOTIVOPAUSA, p.TX_OBSERVACAO, p.CD_MOTIVO_PAUSA, os.DATAAGENDADA, p.DT_PAUSA ");
				sql1.append(" FROM dbORDEM os WITH (NOLOCK) ");
				sql1.append(" INNER JOIN COLABORADOR col WITH (NOLOCK) ON col.CD_COLABORADOR = os.ID_INSTALADOR ");
				sql1.append(" INNER JOIN OS_SOLICITANTE sol WITH (NOLOCK) ON sol.CD_OS_SOLICITANTE = os.CD_OS_SOLICITANTE ");
				sql1.append(" INNER JOIN OSDEFEITO def WITH (NOLOCK) ON def.IDOSDEFEITO = os.IDOSDEFEITO ");
				sql1.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = os.CD_CLIENTE ");
				sql1.append(" LEFT JOIN dbBAIRRO bai WITH (NOLOCK) ON bai.ID_BAIRRO = c.ID_BAIRRO AND bai.ID_CIDADE = c.ID_CIDADE ");
				sql1.append(" LEFT JOIN dbCIDADE cid WITH (NOLOCK) ON cid.ID_CIDADE = c.ID_CIDADE ");
				sql1.append(" LEFT JOIN OSHISTORICO osh WITH (NOLOCK) ON osh.ID_ORDEM = os.ID_ORDEM AND osh.IDOSHISTORICO = (SELECT MIN(IDOSHISTORICO) FROM OSHISTORICO WHERE ID_ORDEM = os.ID_ORDEM) ");
				sql1.append(" LEFT JOIN ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p ON p.CD_ORDEM_SERVICO = os.ID_ORDEM AND p.CD_ORDEM_SERVICO_MOTIVO_PAUSA = (SELECT MAX(p2.CD_ORDEM_SERVICO_MOTIVO_PAUSA) FROM ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p2 WHERE p2.CD_ORDEM_SERVICO = p.CD_ORDEM_SERVICO ) ");
				sql1.append(" LEFT OUTER JOIN MOTIVO_PAUSA mp ON mp.CD_MOTIVO_PAUSA = p.CD_MOTIVO_PAUSA ");
				sql1.append(" WHERE (col.NM_COLABORADOR LIKE '" + reg + "%' or col.CD_COLABORADOR = 9999)  AND col.FG_ATIVO_COLABORADOR = 1 ");
				sql1.append(" AND os.ABERTURA > '2016-02-18' ");
				sql1.append(" --AND col.NM_COLABORADOR NOT IN ( " + txtCol + ") ");
				sql1.append(" AND col.NM_COLABORADOR LIKE '%TERC%' ");
				sql1.append(" AND (os.FECHADO = 0 AND GETDATE() >  (CAST((STR( YEAR( os.ABERTURA  ) ) + '/' + STR( MONTH( os.ABERTURA  ) ) + '/' + STR( DAY( os.ABERTURA  + 5 ) ))AS DATETIME)) AND (osh.DATAINICIOEXECUCAO IS NULL AND os.DATAAGENDADA IS NULL)) ");
				sql1.append("  ");
				sql1.append(" UNION ALL ");
				sql1.append("  ");
				sql1.append(" SELECT 'OS em execução a mais de 02 dias úteis' AS TIPOOS, ");
				sql1.append(" col.NM_COLABORADOR , def.DESCRICAODEFEITO, os.ID_ORDEM, os.ABERTURA, os.DEFEITO, os.ID_INSTALADOR, ");
				sql1.append(" c.ID_EMPRESA, c.ID_CENTRAL, c.PARTICAO , c.CD_CLIENTE, c.FANTASIA, c.RAZAO, c.ENDERECO, bai.NOME AS NOMEBAIRRO, ");
				sql1.append(" cid.NOME AS NOMECIDADE, os.FECHAMENTO, os.FECHADO, osh.DATAINICIOEXECUCAO, osh.DATAFIMEXECUCAO, os.EXECUTADO, sol.NM_DESCRICAO, CD_COLABORADOR, ");
				sql1.append(" mp.NM_DESCRICAO AS NOMEMOTIVOPAUSA, p.TX_OBSERVACAO, p.CD_MOTIVO_PAUSA, os.DATAAGENDADA, p.DT_PAUSA ");
				sql1.append(" FROM dbORDEM os WITH (NOLOCK) ");
				sql1.append(" INNER JOIN COLABORADOR col WITH (NOLOCK) ON col.CD_COLABORADOR = os.ID_INSTALADOR ");
				sql1.append(" INNER JOIN OS_SOLICITANTE sol WITH (NOLOCK) ON sol.CD_OS_SOLICITANTE = os.CD_OS_SOLICITANTE ");
				sql1.append(" INNER JOIN OSDEFEITO def WITH (NOLOCK) ON def.IDOSDEFEITO = os.IDOSDEFEITO ");
				sql1.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = os.CD_CLIENTE ");
				sql1.append(" LEFT JOIN dbBAIRRO bai WITH (NOLOCK) ON bai.ID_BAIRRO = c.ID_BAIRRO AND bai.ID_CIDADE = c.ID_CIDADE ");
				sql1.append(" LEFT JOIN dbCIDADE cid WITH (NOLOCK) ON cid.ID_CIDADE = c.ID_CIDADE ");
				sql1.append(" LEFT JOIN OSHISTORICO osh WITH (NOLOCK) ON osh.ID_ORDEM = os.ID_ORDEM AND osh.IDOSHISTORICO = (SELECT MIN(IDOSHISTORICO) FROM OSHISTORICO WHERE ID_ORDEM = os.ID_ORDEM) ");
				sql1.append(" LEFT JOIN ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p ON p.CD_ORDEM_SERVICO = os.ID_ORDEM AND p.CD_ORDEM_SERVICO_MOTIVO_PAUSA = (SELECT MAX(p2.CD_ORDEM_SERVICO_MOTIVO_PAUSA) FROM ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p2 WHERE p2.CD_ORDEM_SERVICO = p.CD_ORDEM_SERVICO ) ");
				sql1.append(" LEFT OUTER JOIN MOTIVO_PAUSA mp ON mp.CD_MOTIVO_PAUSA = p.CD_MOTIVO_PAUSA ");
				sql1.append(" WHERE (col.NM_COLABORADOR LIKE '" + reg + "%' or col.CD_COLABORADOR = 9999)  AND col.FG_ATIVO_COLABORADOR = 1 ");
				sql1.append(" AND os.ABERTURA > '2016-02-18' ");
				sql1.append(" --AND col.NM_COLABORADOR NOT IN ( " + txtCol + ") ");
				sql1.append(" AND col.NM_COLABORADOR LIKE '%TERC%' ");
				sql1.append(" AND os.FECHADO = 2 AND GETDATE() >= DATEADD(HOUR, +48, osh.DATAINICIOEXECUCAO) ");
				sql1.append("  ");
				sql1.append(" UNION ALL ");
				sql1.append("  ");
				sql1.append(" SELECT 'OS agendada a mais de 02 dias úteis sem execução' AS TIPOOS, ");
				sql1.append(" col.NM_COLABORADOR , def.DESCRICAODEFEITO, os.ID_ORDEM, os.ABERTURA, os.DEFEITO, os.ID_INSTALADOR, ");
				sql1.append(" c.ID_EMPRESA, c.ID_CENTRAL, c.PARTICAO , c.CD_CLIENTE, c.FANTASIA, c.RAZAO, c.ENDERECO, bai.NOME AS NOMEBAIRRO, ");
				sql1.append(" cid.NOME AS NOMECIDADE, os.FECHAMENTO, os.FECHADO, osh.DATAINICIOEXECUCAO, osh.DATAFIMEXECUCAO, os.EXECUTADO, sol.NM_DESCRICAO, CD_COLABORADOR, ");
				sql1.append(" mp.NM_DESCRICAO AS NOMEMOTIVOPAUSA, p.TX_OBSERVACAO, p.CD_MOTIVO_PAUSA, os.DATAAGENDADA, p.DT_PAUSA ");
				sql1.append(" FROM dbORDEM os WITH (NOLOCK) ");
				sql1.append(" INNER JOIN COLABORADOR col WITH (NOLOCK) ON col.CD_COLABORADOR = os.ID_INSTALADOR ");
				sql1.append(" INNER JOIN OS_SOLICITANTE sol WITH (NOLOCK) ON sol.CD_OS_SOLICITANTE = os.CD_OS_SOLICITANTE ");
				sql1.append(" INNER JOIN OSDEFEITO def WITH (NOLOCK) ON def.IDOSDEFEITO = os.IDOSDEFEITO ");
				sql1.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = os.CD_CLIENTE ");
				sql1.append(" LEFT JOIN dbBAIRRO bai WITH (NOLOCK) ON bai.ID_BAIRRO = c.ID_BAIRRO AND bai.ID_CIDADE = c.ID_CIDADE ");
				sql1.append(" LEFT JOIN dbCIDADE cid WITH (NOLOCK) ON cid.ID_CIDADE = c.ID_CIDADE ");
				sql1.append(" LEFT JOIN OSHISTORICO osh WITH (NOLOCK) ON osh.ID_ORDEM = os.ID_ORDEM AND osh.IDOSHISTORICO = (SELECT MIN(IDOSHISTORICO) FROM OSHISTORICO WHERE ID_ORDEM = os.ID_ORDEM) ");
				sql1.append(" LEFT JOIN ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p ON p.CD_ORDEM_SERVICO = os.ID_ORDEM AND p.CD_ORDEM_SERVICO_MOTIVO_PAUSA = (SELECT MAX(p2.CD_ORDEM_SERVICO_MOTIVO_PAUSA) FROM ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p2 WHERE p2.CD_ORDEM_SERVICO = p.CD_ORDEM_SERVICO ) ");
				sql1.append(" LEFT OUTER JOIN MOTIVO_PAUSA mp ON mp.CD_MOTIVO_PAUSA = p.CD_MOTIVO_PAUSA ");
				sql1.append(" WHERE (col.NM_COLABORADOR LIKE '" + reg + "%' or col.CD_COLABORADOR = 9999)  AND col.FG_ATIVO_COLABORADOR = 1 ");
				sql1.append(" AND os.ABERTURA > '2016-02-18' ");
				sql1.append(" --AND col.NM_COLABORADOR NOT IN ( " + txtCol + ") ");
				sql1.append(" AND col.NM_COLABORADOR LIKE '%TERC%' ");
				sql1.append(" AND os.FECHADO = 0 AND os.DATAAGENDADA IS NOT NULL AND CAST(CONVERT(CHAR(16),os.DATAAGENDADA,113) AS datetime) + 2 < GETDATE() ");
				sql1.append(" AND osh.DATAINICIOEXECUCAO IS NULL ");
				sql1.append("  ");
				sql1.append(" ORDER BY 1, col.NM_COLABORADOR, os.FECHAMENTO DESC, sol.NM_DESCRICAO, os.DATAAGENDADA, os.ABERTURA ");
				pstm = conn.prepareStatement(sql1.toString());
				rs = pstm.executeQuery();

				InstantiableEntityInfo osInfo = AdapterUtils.getInstantiableEntityInfo("SIGMAOSAtrasadaTarefaSimples");

				int contador = 0;
				while (rs.next())
				{
					String numeroOS = rs.getString("ID_ORDEM");
					Long nuOS = Long.parseLong(numeroOS);

					List<NeoObject> listOSTarefa = (List<NeoObject>) PersistEngine.getObjects(osInfo.getEntityClass(), new QLEqualsFilter("numeroOS", nuOS), 0, 1, "");

					if (listOSTarefa != null && listOSTarefa.isEmpty())
					{
						String solicitante = "edson.heinz";
						String titulo = rs.getString("TIPOOS") + " - OS: " + numeroOS + " ";
						
						GregorianCalendar dataInicio = new GregorianCalendar();
						GregorianCalendar dataFim = new GregorianCalendar();
						switch (titulo)
						{
							case "OS aberta a mais de 05 dias úteis sem execução e agendamento":
								dataInicio.setTime(rs.getTimestamp("ABERTURA"));
								dataFim = OrsegupsUtils.getSpecificWorkDay(dataInicio, 5L);
								break;
							case "OS em execução a mais de 02 dias úteis":
								dataInicio.setTime(rs.getTimestamp("DATAINICIOEXECUCAO"));
								dataFim = OrsegupsUtils.getSpecificWorkDay(dataInicio, 2L);
								break;
							case "OS agendada a mais de 02 dias úteis sem execução":
								dataInicio.setTime(rs.getTimestamp("DATAAGENDADA"));
								dataFim = OrsegupsUtils.getSpecificWorkDay(dataInicio, 2L);
								break;
						}
						
						if (dataFim.after(new GregorianCalendar()))
						{
							continue;
						}

						GregorianCalendar prazo = new GregorianCalendar();

						prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 1L);
						prazo.set(Calendar.HOUR_OF_DAY, 23);
						prazo.set(Calendar.MINUTE, 59);
						prazo.set(Calendar.SECOND, 59);

						String descricao = "";
						descricao = "<strong><u>Acompanhar, " + rs.getString("TIPOOS") + ":</u></strong><br><br>";
						descricao = descricao + " <strong>Cliente :</strong> " + rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "] " + rs.getString("RAZAO") + " <br>";
						descricao = descricao + " <strong>Texto OS :</strong> " + rs.getString("DEFEITO") + "<br>";
						descricao = descricao + " <strong>Instalador :</strong> " + rs.getString("NM_COLABORADOR") + "<br>";

						IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();

						String executor = "fabio.pires";

						
						NeoPaper papel = OrsegupsUtils.getPapelCoordenadorRegional(codReg);

						//Caso não localizar o papel abre a tarefa para o Fabio
						if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
						{
							for (NeoUser user : papel.getUsers())
							{
								executor = user.getCode();
								break;
							}
						}
						else
						{
							log.warn("##### INICIAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Ordens de Serviço Em Atraso - Papel sem usuário definido - Regional: " + reg + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
						}
						String retorno = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);

						if (retorno != null && (!retorno.trim().contains("Erro")))
						{
							NeoObject objOS = osInfo.createNewInstance();
							EntityWrapper wrpOS = new EntityWrapper(objOS);
							wrpOS.findField("numeroOS").setValue(nuOS);
							wrpOS.findField("numeroTarefa").setValue(Long.parseLong(retorno));
							PersistEngine.persist(objOS);
						}
						else
						{
							log.error(" Erro ao criar tarefa Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
						}
						contador++;
					}
				}
				log.warn("##### EXECUTAR  AGENDADOR DE TAREFA: Abrir Tarefa Simples Ordens de Serviço Em Atraso - Total de tarefas: " + contador);
			}
			catch (Exception e)
			{
				log.error("##### ERRO  AGENDADOR DE TAREFA: Abrir Tarefa Simples Ordens de Serviço Em Atraso - ERRO AO GERAR TAREFA");
				System.out.println("[" + key + "] ##### ERRO  AGENDADOR DE TAREFA: Abrir Tarefa Simples Ordens de Serviço Em Atraso - ERRO AO GERAR TAREFA");
				e.printStackTrace();
				throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
			}
			finally
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
				log.warn("##### FINALIZAR  AGENDADOR DE TAREFA: Abrir Tarefa Simples Ordens de Serviço Em Atraso - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
			}
		}
	}
}