package com.neomind.fusion.custom.casvig.adapters;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.entity.dynamic.EntityCloner;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class CasvigGestaoUniformesHistorico implements AdapterInterface {
    
    private static final Log log = LogFactory.getLog(CasvigGestaoUniformesHistorico.class);
    
    public void back(EntityWrapper entitywrapper, Activity activity1)
	{
	}
    
    @Override
    public void start(Task origin, EntityWrapper processEntity, Activity activity) 
    {
	// TODO Auto-generated method stub
	log.warn("GERANDO HISTÓRIO DE ENTREGAS -------  CasvigGestaoUniformesHistorico!");
	
	 defineHistorico(processEntity);
    }
    
    public void defineHistorico(EntityWrapper processEntity)
	{
		//Se já existe histórico é porque itens já foram persistidos no Sapiens, neste caso copia integralmente os itens de histórico para 
		//a lista de Entregas

		List<NeoObject> listaEntregasEmAberto = (List<NeoObject>) processEntity.findField("fichaUniformeEPI.listaEntregas").getValue();
		List<NeoObject> listaEntregasFinalizadas = (List<NeoObject>) processEntity.findField("fichaUniformeEPI.listaEntregasFinalizadas").getValue();
		List<NeoObject> listaHistorico = (List<NeoObject>) processEntity.findField("fichaUniformeEPI.historicoEntregas").getValue();

		//Se histórico não é vazio, deve ser clonado para a lista de entregas
		if (listaHistorico != null && !listaHistorico.isEmpty())
		{
			/* Limpa a lista de entregas em aberto */
			if (listaEntregasEmAberto != null)
			{
				int tamanho = listaEntregasEmAberto.size();
				for (int i = 1; i < tamanho + 1; i++)
				{
					NeoObject entrega = listaEntregasEmAberto.get(tamanho - i);
					processEntity.findField("fichaUniformeEPI.listaEntregas").removeValue(entrega);
				}
			}

			/* Limpa a lista de entregas finalizadas */
			if (listaEntregasFinalizadas != null)
			{
				int tamanho = listaEntregasFinalizadas.size();
				for (int i = 1; i < tamanho + 1; i++)
				{
					NeoObject entrega = listaEntregasFinalizadas.get(tamanho - i);
					processEntity.findField("fichaUniformeEPI.listaEntregasFinalizadas").removeValue(entrega);
				}
			}

			/* Copia o histórico para a lista de entregas */
			for (NeoObject entregaHistorico : listaHistorico)
			{
				EntityWrapper wrapperEntrega = new EntityWrapper(entregaHistorico);
				InstantiableEntityInfo infoListaEntregas = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("FUEListaEntregas");
				NeoObject cloneNoEntrega = infoListaEntregas.createNewInstance();

				EntityWrapper wrapperCloneEntrega = new EntityWrapper(cloneNoEntrega);

				/* Define os valores padrões que não são clonados */
				wrapperCloneEntrega.findField("depositoEntrega").setValue(wrapperEntrega.findField("depositoEntrega").getValue());
				wrapperCloneEntrega.findField("produto").setValue(wrapperEntrega.findField("produto").getValue());
				wrapperCloneEntrega.findField("derivacao").setValue(wrapperEntrega.findField("derivacao").getValue());
				wrapperCloneEntrega.findField("observacao").setValue(wrapperEntrega.findField("observacao").getValue().toString());
				wrapperCloneEntrega.findField("quantidadeEntregue").setValue((Long) wrapperEntrega.findField("quantidadeEntregue").getValue());
				wrapperCloneEntrega.findField("codigoEntrega").setValue(wrapperEntrega.findField("codigoEntrega").getValue());
				wrapperCloneEntrega.findField("dataEntrega").setValue(wrapperEntrega.findField("dataEntrega").getValue());
				wrapperCloneEntrega.findField("usuario").setValue(wrapperEntrega.findField("usuario").getValue());
				wrapperCloneEntrega.findField("solicitarCompra").setValue(wrapperEntrega.findValue("solicitarCompra"));
				wrapperCloneEntrega.findField("codigoSapiens").setValue(wrapperEntrega.findValue("codigoSapiens"));

				List<NeoObject> listaDevolucoes = (List<NeoObject>) wrapperEntrega.findField("listaDevolucoes").getValue();

				Long entregue = (Long) wrapperEntrega.findField("quantidadeEntregue").getValue();
				Long devolvido = 0L;

				for (NeoObject devolucao : listaDevolucoes)
				{
					NeoObject cloneDevolucao = EntityCloner.cloneNeoObject(devolucao);

					EntityWrapper devolucaoWrapper = new EntityWrapper(devolucao);

					devolucaoWrapper.findField("exibeRelatorio").setValue(false);
					
					if (devolucaoWrapper.findValue("quantidadeDevolucao") != null)
					{
						devolvido += (Long) devolucaoWrapper.findValue("quantidadeDevolucao");
					}

					wrapperCloneEntrega.findField("listaDevolucoes").addValue(cloneDevolucao);
				}
				wrapperCloneEntrega.findField("entregaIntegrada").setValue(true);
				wrapperCloneEntrega.findField("exibeRelatorio").setValue(false);
				//wrapperCloneEntrega.setValue("solicitarCompra", false);

				if (devolvido < entregue)
				{
					wrapperCloneEntrega.setValue("descontoTotalItemFolha", false);
					wrapperCloneEntrega.setValue("DevolucaoTotal", false);
					
					processEntity.findField("fichaUniformeEPI.listaEntregas").addValue(cloneNoEntrega);
				}
				else
				{
					processEntity.findField("fichaUniformeEPI.listaEntregasFinalizadas").addValue(cloneNoEntrega);
				}
			}
		}
	}

    

}
