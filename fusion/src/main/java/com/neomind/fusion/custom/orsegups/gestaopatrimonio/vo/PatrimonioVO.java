package com.neomind.fusion.custom.orsegups.gestaopatrimonio.vo;

public class PatrimonioVO
{
	private String codigo;
	private String produto;
	private String dataPatrimonio;
	private String empresa;
	private String setor;
	private String local;
	private String descricao;
	private String marca;
	private String cor;
	private String numeroSerie;
	private String reponsavel;
	private String fornecedor;
	private String numeroNota;
	private String ativo;

	public String getCodigo()
	{
		return codigo;
	}

	public void setCodigo(String codigo)
	{
		this.codigo = codigo;
	}

	public String getProduto()
	{
		return produto;
	}

	public void setProduto(String produto)
	{
		this.produto = produto;
	}

	public String getDataPatrimonio()
	{
		return dataPatrimonio;
	}

	public void setDataPatrimonio(String dataPatrimonio)
	{
		this.dataPatrimonio = dataPatrimonio;
	}

	public String getEmpresa()
	{
		return empresa;
	}

	public void setEmpresa(String empresa)
	{
		this.empresa = empresa;
	}

	public String getSetor()
	{
		return setor;
	}

	public void setSetor(String setor)
	{
		this.setor = setor;
	}

	public String getLocal()
	{
		return local;
	}

	public void setLocal(String local)
	{
		this.local = local;
	}

	public String getDescricao()
	{
		return descricao;
	}

	public void setDescricao(String descricao)
	{
		this.descricao = descricao;
	}

	public String getMarca()
	{
		return marca;
	}

	public void setMarca(String marca)
	{
		this.marca = marca;
	}

	public String getCor()
	{
		return cor;
	}

	public void setCor(String cor)
	{
		this.cor = cor;
	}

	public String getNumeroSerie()
	{
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie)
	{
		this.numeroSerie = numeroSerie;
	}

	public String getReponsavel()
	{
		return reponsavel;
	}

	public void setReponsavel(String reponsavel)
	{
		this.reponsavel = reponsavel;
	}

	public String getFornecedor()
	{
		return fornecedor;
	}

	public void setFornecedor(String fornecedor)
	{
		this.fornecedor = fornecedor;
	}

	public String getNumeroNota()
	{
		return numeroNota;
	}

	public void setNumeroNota(String numeroNota)
	{
		this.numeroNota = numeroNota;
	}

	public String getAtivo()
	{
		return ativo;
	}

	public void setAtivo(String ativo)
	{
		this.ativo = ativo;
	}
	
}
