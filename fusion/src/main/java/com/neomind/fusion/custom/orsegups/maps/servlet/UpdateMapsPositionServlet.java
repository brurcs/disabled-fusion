package com.neomind.fusion.custom.orsegups.maps.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.maps.vo.TrackEventVO;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

@WebServlet(name = "UpdateMapsPositionServlet", urlPatterns = { "/servlet/com.neomind.fusion.custom.orsegups.maps.servlet.UpdateMapsPositionServlet" })
public class UpdateMapsPositionServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Log log = LogFactory.getLog(UpdateMapsPositionServlet.class);
    private final String UPDATE_POSITION = "updatePosition";
    private final String UPDATE_MODULE_STATUS = "updateModuleStatus";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	this.doRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	this.doRequest(req, resp);
    }

    private void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	final PrintWriter out = response.getWriter();
	Gson gson = new Gson();

	String method = request.getParameter("method");
	method = (method == null) ? "" : method;

	// log.warn("Metodo: " + method);

//	if (method.equalsIgnoreCase(this.UPDATE_POSITION)) {
	    // log.warn("UpdateMapsPositionServlet INICIO");
	    TrackEventVO event = gson.fromJson(request.getParameter("event"), TrackEventVO.class);
	    // Boolean history = gson.fromJson(request.getParameter("history"),
	    // Boolean.class);

	    // log.warn("JSON: "+request.getParameter("event"));
	    // log.warn(event.toString());

	    NeoObject viatura = this.getViatura(event.getId());

	    Boolean success = true;

	    if (viatura != null) {
		EntityWrapper viaturaWrapper = new EntityWrapper(viatura);
		String modelo = (String) viaturaWrapper.findValue("rastreador.modeloEquipamento.nomeModelo");

		if ((modelo.contains("QUATENUS") || modelo.contains("AUTOCARGO")) && !event.isIgnition()) {
		    event.setEvent(53L);
		}

		if (event.getEvent() == 53L) {
		    event.setStatus(1L);
		} else if (event.isIgnition()) {
		    if (event.getSpeed() <= 0L) {
			event.setStatus(3L);
		    } else {
			event.setStatus(4L);
		    }
		} else {
		    event.setStatus(2L);
		}

		viaturaWrapper.setValue("ultimaAtualizacao", event.getTime());
		viaturaWrapper.setValue("codigoEvento", event.getEvent());
		viaturaWrapper.setValue("latitude", new BigDecimal(event.getLat()));
		viaturaWrapper.setValue("longitude", new BigDecimal(event.getLng()));
		viaturaWrapper.setValue("altitude", event.getAlt());
		viaturaWrapper.setValue("velocidade", event.getSpeed());
		viaturaWrapper.setValue("direcao", event.getHeading());
		viaturaWrapper.setValue("satelites", event.getSat());
		viaturaWrapper.setValue("entrada", event.getIn());
		viaturaWrapper.setValue("saida", event.getOut());
		viaturaWrapper.setValue("ignicao", event.isIgnition());

		NeoObject statusViatura = this.getStatusViatura(event.getStatus());

		if (statusViatura != null) {
		    viaturaWrapper.setValue("statusViatura", statusViatura);
		}

		// Se em deslocamento, atualizar o campo do ultima data de
		// deslocamento
		if (event.getStatus() == 4L) {
		    viaturaWrapper.setValue("ultimoStatusDeslocamento", event.getTime());
		    viaturaWrapper.setValue("atrasoDeslocamento", Boolean.FALSE);

		}
		log.error(request.getParameter("event"));
		log.error("Viatura:" + event.getId()+"; status: "+event.getStatus()+"; ultimoStatusDeslocamento: "+NeoDateUtils.safeDateFormat(event.getTime().getTime(), "dd/MM/yyyy HH:mm:ss")+" Speed: "+event.getSpeed());
	    } else {
		log.error("Viatura nao encontrada: " + event.toString());
		success = false;
	    }

	    String returnString = success.toString() + ";" + "false";

	    // String successJson = gson.toJson(success);
	    String successJson = gson.toJson(returnString);

	    out.print(successJson);
//	} else if (method.equalsIgnoreCase(this.UPDATE_MODULE_STATUS)) {
//	    
//	}
	// log.warn("UpdateMapsPositionServlet FIM");
	out.close();
    }

    private NeoObject getViatura(String codigoRastreador) {
	NeoObject viatura = null;

	try {
	    Class<? extends NeoObject> viaturaClazz = AdapterUtils.getEntityClass("OTSViatura");

	    QLEqualsFilter equalsFilter = new QLEqualsFilter("rastreador.id", codigoRastreador);

	    viatura = PersistEngine.getObject(viaturaClazz, equalsFilter);
	} catch (Exception e) {
	    e.printStackTrace();
	}

	return viatura;
    }

    private NeoObject getStatusViatura(Long status) {
	NeoObject statusViatura = null;

	try {
	    Class<? extends NeoObject> statusViaturaClazz = AdapterUtils.getEntityClass("OTSStatusViatura");

	    QLEqualsFilter equalsFilter = new QLEqualsFilter("codigoStatus", status);

	    statusViatura = PersistEngine.getObject(statusViaturaClazz, equalsFilter);
	} catch (Exception e) {
	    e.printStackTrace();
	}

	return statusViatura;
    }
}
