package com.neomind.fusion.custom.orsegups.sigma;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class CreateSigmaOSAdapter implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO dbORDEM ");
		sql.append("	      (CD_CLIENTE, ID_INSTALADOR, ABERTURA,  DEFEITO, OPABRIU, IDOSDEFEITO, FG_EMAIL_ENVIADO, CD_OS_SOLICITANTE, FG_TODAS_PARTICOES_EM_MANUTENCAO,  EXECUTADO,	TEMPOEXECUCAOPREVISTO )");
		sql.append("	VALUES(?,          ?,             GETDATE(), ?,       ?,       ?,           ?,                ?,                 ?,									?,			1 )");

		Long cdCliente;
		Long idInstalador;
		String abertura = "";
		Long opaAbriu = 11010L; //usuario que esta abrindo a OS - Fusion
		Long idOsDefeito;
		Long fgEmailEnviado = 0L;
		Long cdOsSolicitante;
		Long fgTodasParticoesEmManutencao = 0L;
		String executado = "";

		cdCliente = (Long) processEntity.findValue("Conta.cd_cliente");
		idInstalador = (Long) processEntity.findValue("TecnicoResponsavel.cd_colaborador");
		abertura += (String) processEntity.findValue("Descricao");
		idOsDefeito = (Long) processEntity.findValue("Defeito.idosdefeito");
		cdOsSolicitante = (Long) processEntity.findValue("Solicitante.cd_os_solicitante");

		Connection connection = OrsegupsUtils.getSqlConnection("SIGMA90");
		PreparedStatement st = null;
		
		try
		{
			connection.setAutoCommit(false);

			st = connection.prepareStatement(sql.toString(),Statement.RETURN_GENERATED_KEYS);
			// CD_CLIENTE
			st.setLong(1, cdCliente);
			// ID_INSTALADOR - Técnico responsável informado no cadastro da conta
			st.setLong(2, idInstalador);
			// DEFEITO
			st.setString(3, abertura);
			// OPABRIU
			st.setLong(4, opaAbriu);
			// IDOSDEFEITO
			st.setLong(5, idOsDefeito);
			// FG_EMAIL_ENVIADO = 0
			st.setLong(6, fgEmailEnviado);
			// CD_OS_SOLICITANTE
			st.setLong(7, cdOsSolicitante);
			// FG_TODAS_PARTICOES_EM_MANUTENCAO
			st.setLong(8, fgTodasParticoesEmManutencao);
			// EXECUTADO
			st.setString(9, executado);

			st.executeUpdate();
			connection.commit();
			
			ResultSet generatedKeys = st.getGeneratedKeys();
			
			if (generatedKeys.next()) {
				processEntity.setValue("numeroOSSigma", generatedKeys.getLong(1));
	        } else {
	            throw new SQLException("Não foi possível criar um número de OS");
	        }
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Erro ao abrir OS Sigma! "+e.getMessage());
		}
		finally
		{
			try
			{
				st.close();
				connection.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
