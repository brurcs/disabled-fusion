package com.neomind.fusion.custom.casvig;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.casvig.mobile.CasvigMobileServlet;
import com.neomind.util.CustomFlags;

/**
 * 
 * @author Daniel Henrique Joppi
 * 
 */
@SuppressWarnings("serial")
@WebServlet(name="CasvigServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.casvig.CasvigServlet"})
public class CasvigServlet extends HttpServlet
{
	@SuppressWarnings("unused")
	private static final Log log = LogFactory.getLog(CasvigServlet.class);

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		if (!CustomFlags.CASVIG)
			return;

		//response.setCharacterEncoding("ISO-8859-1");
		response.setContentType("text/plain");

		String function = request.getParameter("id");

		if (function.equals("CasvigMobile"))
		{
			CasvigMobileServlet.getInstance().doGet(request, response);
		}
	}
}