package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.datawarehouse.ExternalEntityInfo;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.UserActivity;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.fusion.workflow.simulation.WorkflowService;

public class RSCIniciaProcessoRSC implements AdapterInterface
{

	private final Log log = LogFactory.getLog(RSCIniciaProcessoRSC.class);

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		Long codCli = (Long) processEntity.findValue("contratoSapiens.codcli");
		
		ExternalEntityInfo infoClienteSapiens = (ExternalEntityInfo)EntityRegister.getInstance().getCache().getByString("SAPIENS_Clientes");
		QLEqualsFilter filterCodCli = new QLEqualsFilter("codcli", codCli);
		NeoObject noClienteSapiens = (NeoObject) PersistEngine.getObject(infoClienteSapiens.getEntityClass(), filterCodCli);
		
		InstantiableEntityInfo infoOrigem = AdapterUtils.getInstantiableEntityInfo("RRCOrigem");
		QLEqualsFilter filterOrigem = new QLEqualsFilter("sequencia", "05");
		
		InstantiableEntityInfo infoCategoria = AdapterUtils.getInstantiableEntityInfo("RSCCategorias");
		QLEqualsFilter filterCategoria = new QLEqualsFilter("codigo", "05");
		
		InstantiableEntityInfo infoDestino = AdapterUtils.getInstantiableEntityInfo("RRCDestino");
		QLEqualsFilter filterDestino = new QLEqualsFilter("sequencia", "14");
		
		EntityWrapper clienteSapiensWrapper = new EntityWrapper(noClienteSapiens);
		
		InstantiableEntityInfo infoConfiguracao = AdapterUtils.getInstantiableEntityInfo("RSCConfiguracaoIntegracao");
		QLEqualsFilter filterConfiguracao = new QLEqualsFilter("identificador", "FCI");
		NeoObject noConfiguracao = (NeoObject) PersistEngine.getObject(infoConfiguracao.getEntityClass(), filterConfiguracao);
		
		EntityWrapper configuracaoWrapper = new EntityWrapper(noConfiguracao);
		
		InstantiableEntityInfo infoRSC = AdapterUtils.getInstantiableEntityInfo("RSCRelatorioSolicitacaoClienteNovo"); 
		NeoObject noRSC = infoRSC.createNewInstance();
		EntityWrapper rscWrapper = new EntityWrapper(noRSC);
		
		//TODO RSC - Portar para abertura via RSCHelper.abrirRSC
		rscWrapper.findField("clienteSapiens").setValue(noClienteSapiens);
		//rscWrapper.findField("cidade").setValue((String) clienteSapiensWrapper.findValue("cidcli"));
		rscWrapper.findField("contato").setValue(processEntity.findValue("contato"));
		rscWrapper.findField("email").setValue(processEntity.findValue("email"));
		rscWrapper.findField("telefones").setValue(processEntity.findValue("telefones"));
		rscWrapper.findField("descricao").setValue("Aberto via processo de cobrança " + origin.getProcessName() + " tarefa " + origin.getCode() + "\nDescrição: " + (String) processEntity.findValue("obs05"));
		rscWrapper.findField("origem").setValue((NeoObject) PersistEngine.getObject(infoOrigem.getEntityClass(), filterOrigem));
		//rscWrapper.findField("categoriasRsc").setValue((NeoObject) PersistEngine.getObject(infoCategoria.getEntityClass(), filterCategoria));
		//rscWrapper.findField("destino").setValue((NeoObject) PersistEngine.getObject(infoDestino.getEntityClass(), filterDestino));
		rscWrapper.findField("prazoRegistrarRSC").setValue(OrsegupsUtils.getSpecificWorkDay(new GregorianCalendar(), (Long) configuracaoWrapper.findValue("prazo")));
		rscWrapper.findField("responsavelAtendimento").setValue(configuracaoWrapper.findValue("responsavel"));
		
		PersistEngine.persist(noRSC);

		QLEqualsFilter equal = new QLEqualsFilter("Name", "C027 - RSC - Relatório de Solicitação de Cliente Novo");
		ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);

		/**
		 * @author neomind willian.mews - Alterado para utilizar a nova arquitetura de processos do Fusion
		 * @date 11/03/2015
		 */
		
		NeoUser solicitante = origin.getUser();
		final WFProcess processo = WorkflowService.startProcess(processModel, false, solicitante);
		
		try
		{
			new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividade(solicitante);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			log.error("Erro ao iniciar a primeira atividade do processo.");
		}
		
		
		/**
		 * FIM ALTERAÇÕES - NEOMIND
		 */
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}
}