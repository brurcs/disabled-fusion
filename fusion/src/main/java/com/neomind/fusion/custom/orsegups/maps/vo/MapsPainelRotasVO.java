package com.neomind.fusion.custom.orsegups.maps.vo;

import java.util.List;

public class MapsPainelRotasVO {
    
    private String nomeRegional;
    
    private boolean controlarRotaRegional;
    
    public boolean isControlarRotaRegional() {
        return controlarRotaRegional;
    }

    public void setControlarRotaRegional(boolean controlarRotaRegional) {
        this.controlarRotaRegional = controlarRotaRegional;
    }

    private List<ViaturaVO> viaturas;

    public String getNomeRegional() {
        return nomeRegional;
    }

    public void setNomeRegional(String nomeRegional) {
        this.nomeRegional = nomeRegional;
    }

    public List<ViaturaVO> getViaturas() {
        return viaturas;
    }

    public void setViaturas(List<ViaturaVO> viaturas) {
        this.viaturas = viaturas;
    }
    
}
