package com.neomind.fusion.custom.orsegups.adapter.integraRubi;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

public class ParametrizacaoAdmissao
{

	static String eformParams = "RH01ParametrizacaoAdmissao";

	/**
	 * Consulta um parametro e retorna null caso o mesmo não seja econtrato ou ocorra algum erro de
	 * acesso ao eform
	 * 
	 * @param parameter
	 * @return
	 */
	public static String findParameter(String parameter)
	{
		String retorno = "";
		try
		{
			QLGroupFilter gp = new QLGroupFilter("AND");
			gp.addFilter(new QLEqualsFilter("parametro", parameter));
			NeoObject oControleParametreizacao = PersistEngine.getObject(AdapterUtils.getEntityClass(eformParams), gp);
			EntityWrapper wControleParametreizacao = new EntityWrapper(oControleParametreizacao);
			return (String) wControleParametreizacao.findValue("valor");
		}
		catch (Exception e)
		{
			return null;
		}
	}

}
