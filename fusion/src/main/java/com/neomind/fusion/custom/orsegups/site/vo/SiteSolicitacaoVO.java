package com.neomind.fusion.custom.orsegups.site.vo;

import java.util.ArrayList;
import java.util.Collection;
import com.neomind.fusion.custom.orsegups.presenca.vo.SolicitacaoVO;

public class SiteSolicitacaoVO
{
	private String codigoCliente;
	private Collection<SolicitacaoVO> solicitacoes = new ArrayList();
	
	public Collection<SolicitacaoVO> getSolicitacoes()
	{
		return solicitacoes;
	}
	public void setSolicitacoes(Collection<SolicitacaoVO> solicitacoes)
	{
		this.solicitacoes = solicitacoes;
	}
	public String getCodigoCliente()
	{
		return codigoCliente;
	}
	public void setCodigoCliente(String codigoCliente)
	{
		this.codigoCliente = codigoCliente;
	}
}
