/**
 * ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.neomind.fusion.custom.orsegups.contract.clientws.cliente;

public class ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica  implements java.io.Serializable {
    private java.lang.String cidNat;

    private java.lang.String codSex;

    private java.lang.String datNas;

    private java.lang.String datReg;

    private java.lang.Integer estCiv;

    private java.lang.String nomMae;

    private java.lang.String nomPai;

    private java.lang.String numRge;

    private java.lang.String orgRge;

    private java.lang.String refCm1;

    public ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica() {
    }

    public ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica(
           java.lang.String cidNat,
           java.lang.String codSex,
           java.lang.String datNas,
           java.lang.String datReg,
           java.lang.Integer estCiv,
           java.lang.String nomMae,
           java.lang.String nomPai,
           java.lang.String numRge,
           java.lang.String orgRge,
           java.lang.String refCm1) {
           this.cidNat = cidNat;
           this.codSex = codSex;
           this.datNas = datNas;
           this.datReg = datReg;
           this.estCiv = estCiv;
           this.nomMae = nomMae;
           this.nomPai = nomPai;
           this.numRge = numRge;
           this.orgRge = orgRge;
           this.refCm1 = refCm1;
    }


    /**
     * Gets the cidNat value for this ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica.
     * 
     * @return cidNat
     */
    public java.lang.String getCidNat() {
        return cidNat;
    }


    /**
     * Sets the cidNat value for this ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica.
     * 
     * @param cidNat
     */
    public void setCidNat(java.lang.String cidNat) {
        this.cidNat = cidNat;
    }


    /**
     * Gets the codSex value for this ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica.
     * 
     * @return codSex
     */
    public java.lang.String getCodSex() {
        return codSex;
    }


    /**
     * Sets the codSex value for this ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica.
     * 
     * @param codSex
     */
    public void setCodSex(java.lang.String codSex) {
        this.codSex = codSex;
    }


    /**
     * Gets the datNas value for this ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica.
     * 
     * @return datNas
     */
    public java.lang.String getDatNas() {
        return datNas;
    }


    /**
     * Sets the datNas value for this ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica.
     * 
     * @param datNas
     */
    public void setDatNas(java.lang.String datNas) {
        this.datNas = datNas;
    }


    /**
     * Gets the datReg value for this ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica.
     * 
     * @return datReg
     */
    public java.lang.String getDatReg() {
        return datReg;
    }


    /**
     * Sets the datReg value for this ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica.
     * 
     * @param datReg
     */
    public void setDatReg(java.lang.String datReg) {
        this.datReg = datReg;
    }


    /**
     * Gets the estCiv value for this ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica.
     * 
     * @return estCiv
     */
    public java.lang.Integer getEstCiv() {
        return estCiv;
    }


    /**
     * Sets the estCiv value for this ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica.
     * 
     * @param estCiv
     */
    public void setEstCiv(java.lang.Integer estCiv) {
        this.estCiv = estCiv;
    }


    /**
     * Gets the nomMae value for this ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica.
     * 
     * @return nomMae
     */
    public java.lang.String getNomMae() {
        return nomMae;
    }


    /**
     * Sets the nomMae value for this ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica.
     * 
     * @param nomMae
     */
    public void setNomMae(java.lang.String nomMae) {
        this.nomMae = nomMae;
    }


    /**
     * Gets the nomPai value for this ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica.
     * 
     * @return nomPai
     */
    public java.lang.String getNomPai() {
        return nomPai;
    }


    /**
     * Sets the nomPai value for this ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica.
     * 
     * @param nomPai
     */
    public void setNomPai(java.lang.String nomPai) {
        this.nomPai = nomPai;
    }


    /**
     * Gets the numRge value for this ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica.
     * 
     * @return numRge
     */
    public java.lang.String getNumRge() {
        return numRge;
    }


    /**
     * Sets the numRge value for this ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica.
     * 
     * @param numRge
     */
    public void setNumRge(java.lang.String numRge) {
        this.numRge = numRge;
    }


    /**
     * Gets the orgRge value for this ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica.
     * 
     * @return orgRge
     */
    public java.lang.String getOrgRge() {
        return orgRge;
    }


    /**
     * Sets the orgRge value for this ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica.
     * 
     * @param orgRge
     */
    public void setOrgRge(java.lang.String orgRge) {
        this.orgRge = orgRge;
    }


    /**
     * Gets the refCm1 value for this ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica.
     * 
     * @return refCm1
     */
    public java.lang.String getRefCm1() {
        return refCm1;
    }


    /**
     * Sets the refCm1 value for this ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica.
     * 
     * @param refCm1
     */
    public void setRefCm1(java.lang.String refCm1) {
        this.refCm1 = refCm1;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica)) return false;
        ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica other = (ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cidNat==null && other.getCidNat()==null) || 
             (this.cidNat!=null &&
              this.cidNat.equals(other.getCidNat()))) &&
            ((this.codSex==null && other.getCodSex()==null) || 
             (this.codSex!=null &&
              this.codSex.equals(other.getCodSex()))) &&
            ((this.datNas==null && other.getDatNas()==null) || 
             (this.datNas!=null &&
              this.datNas.equals(other.getDatNas()))) &&
            ((this.datReg==null && other.getDatReg()==null) || 
             (this.datReg!=null &&
              this.datReg.equals(other.getDatReg()))) &&
            ((this.estCiv==null && other.getEstCiv()==null) || 
             (this.estCiv!=null &&
              this.estCiv.equals(other.getEstCiv()))) &&
            ((this.nomMae==null && other.getNomMae()==null) || 
             (this.nomMae!=null &&
              this.nomMae.equals(other.getNomMae()))) &&
            ((this.nomPai==null && other.getNomPai()==null) || 
             (this.nomPai!=null &&
              this.nomPai.equals(other.getNomPai()))) &&
            ((this.numRge==null && other.getNumRge()==null) || 
             (this.numRge!=null &&
              this.numRge.equals(other.getNumRge()))) &&
            ((this.orgRge==null && other.getOrgRge()==null) || 
             (this.orgRge!=null &&
              this.orgRge.equals(other.getOrgRge()))) &&
            ((this.refCm1==null && other.getRefCm1()==null) || 
             (this.refCm1!=null &&
              this.refCm1.equals(other.getRefCm1())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCidNat() != null) {
            _hashCode += getCidNat().hashCode();
        }
        if (getCodSex() != null) {
            _hashCode += getCodSex().hashCode();
        }
        if (getDatNas() != null) {
            _hashCode += getDatNas().hashCode();
        }
        if (getDatReg() != null) {
            _hashCode += getDatReg().hashCode();
        }
        if (getEstCiv() != null) {
            _hashCode += getEstCiv().hashCode();
        }
        if (getNomMae() != null) {
            _hashCode += getNomMae().hashCode();
        }
        if (getNomPai() != null) {
            _hashCode += getNomPai().hashCode();
        }
        if (getNumRge() != null) {
            _hashCode += getNumRge().hashCode();
        }
        if (getOrgRge() != null) {
            _hashCode += getOrgRge().hashCode();
        }
        if (getRefCm1() != null) {
            _hashCode += getRefCm1().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientesGravarClientesInDadosGeraisClienteClientePessoaFisica.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesGravarClientesInDadosGeraisClienteClientePessoaFisica"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cidNat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cidNat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codSex");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codSex"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datNas");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datNas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datReg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datReg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estCiv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estCiv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomMae");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomMae"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomPai");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomPai"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numRge");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numRge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orgRge");
        elemField.setXmlName(new javax.xml.namespace.QName("", "orgRge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("refCm1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "refCm1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
