package com.neomind.fusion.custom.orsegups.RE;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.persistence.Query;

import com.neomind.fusion.persist.PersistEngine;
import com.neomind.util.NeoUtils;

public class TomadorDaoImpl {

	public static boolean existeTomador(String tomador, int empresa){
		boolean retorno = false;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT oem.NumCgc ");
			sql.append("FROM R032OEM oem ");
			sql.append("INNER JOIN R030FIL fil ON fil.CodOem = oem.CodOem ");
			sql.append("WHERE oem.NumCgc = '"+tomador+"' AND fil.NumEmp = "+empresa); 

			Query query = PersistEngine.getEntityManager("VETORH").createNativeQuery(sql.toString());
			Collection<Object> resultList = query.getResultList();
			if (resultList.size() >0) {
				retorno = true;
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return retorno;
	}
	
	/**
	 * Retorna Empresa e Filial conforme cnpj da prestadora de serviço
	 * @param cnpjEmpresa - 
	 * @return Long[] => [0]-> Codigo da empresa, [1]-> Codigo da filial
	 */
	public static Long[] getEmpFil(String cnpjEmpresa){
		Long retorno[] = {0L,0L};
		
		try{
			StringBuilder sql = new StringBuilder();
			sql.append(" select codemp, (case when codemp = 15 then 1 else codfil end) codfilial from dbo.E070FIL where numcgc = ? order by codfil ");

			Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
			query.setParameter(1, cnpjEmpresa.replace(".", "").replace("/", "").replace("-", "").trim());
			Collection<Object> resultList = query.getResultList();
			if (resultList != null && resultList.size() >0) {
				for (Object res : resultList){
					Object obj[] = (Object[]) res;
					retorno[0] = NeoUtils.safeLong(NeoUtils.safeOutputString(obj[0]) );
					retorno[1] = NeoUtils.safeLong(NeoUtils.safeOutputString(obj[1]) );
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
		return retorno;
	}
	
	public static void main(String args[]){
		ArrayList<String> files = new ArrayList<String>();
		
		
		for (String path : files){
			System.out.print("deletando arquivo ");
			File f = new File(path);
			f.delete();
			System.out.println(" [OK]");
		}
	}
	
	
	
	
}
