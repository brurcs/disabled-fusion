package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;

public class AbreTarefaSimplesTransferirContratos implements CustomJobAdapter
{

	@Override
	public void execute(CustomJobContext ctx)
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();

		Connection conn = PersistEngine.getConnection("SAPIENS");
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();

		String solicitante = "maurelio.pinto";
		String executor = "robson.oliveira";

		String descricao = "";
		String tarefa = "";

		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 60L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);

		try
		{
			sql.append(" SELECT CTR.USU_CODEMP, CLI.CODCLI, CLI.NOMCLI, CLI.CGCCPF                                ");
			sql.append(" FROM USU_T160CTR CTR																	  ");
			sql.append(" INNER JOIN E085CLI CLI ON CTR.USU_CODCLI = CLI.CODCLI									  ");		
			sql.append(" WHERE ((CTR.USU_SITCTR = 'A') OR (CTR.USU_SITCTR = 'I' AND CTR.USU_DATFIM >= GETDATE())) "); 
			sql.append(" AND CTR.USU_CODEMP IN (29)															      ");
			sql.append(" GROUP BY CTR.USU_CODEMP, CLI.CODCLI, CLI.NOMCLI, CLI.CGCCPF							  ");

			pstm = conn.prepareStatement(sql.toString());
			rs = pstm.executeQuery();

			while (rs.next())
			{
				String titulo = "Transferir Contratos - " + rs.getString("CgcCpf") + " - " + rs.getString("NomCli");

				if(rs.getLong("USU_CODEMP") == 27L)
				{
					descricao = "Providenciar a transferência dos Contratos do Cliente " + rs.getString("CodCli") + " - " + rs.getString("NomCli") + rs.getString("CgcCpf");
					descricao = descricao + " da empresa VIASEG para ORSEGUPS PRINCESA DA SERRA (15).";
				}
				else
				{
					descricao = "Providenciar a transferência dos Contratos do Cliente " + rs.getString("CodCli") + " - " + rs.getString("NomCli") + rs.getString("CgcCpf");
					descricao = descricao + " da empresa ITASEG para ORSEGUPS SERVIÇOS (7).";
				}				
				
				IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
				tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);
				
				Thread.sleep(4000);
			}
		}
		catch (Exception e)
		{
			System.out.println("[" + key + "] ERRO AGENDADOR DE TAREFA: Abre Tarefa Simples Atualiza Gestores Regionais");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		}
		finally
		{
			try
			{
				rs.close();
				pstm.close();
				conn.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}

	}

}
