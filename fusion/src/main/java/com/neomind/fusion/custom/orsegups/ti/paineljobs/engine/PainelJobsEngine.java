package com.neomind.fusion.custom.orsegups.ti.paineljobs.engine;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.common.base.Joiner;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.e2doc.engine.E2docEngine;
import com.neomind.fusion.custom.orsegups.ti.paineljobs.bean.PainelJobBean;
import com.neomind.fusion.custom.orsegups.ti.paineljobs.bean.PainelJobErrosBean;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;


public class PainelJobsEngine {

    private static final Log log = LogFactory.getLog(E2docEngine.class);
    
    public PainelJobsEngine() {
    }

    public List<PainelJobBean> buscarJobsFusion(){

	Connection conn = null;
	PreparedStatement stmt = null;
	ResultSet rs = null;
	List<PainelJobBean> jobs = new ArrayList<PainelJobBean>();

	try{
	    conn = PersistEngine.getConnection("");
	    Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();

	    StringBuffer queryJobs = new StringBuffer();

	    queryJobs.append(" SELECT j.neoId as idJob, ");
	    queryJobs.append(" j.name as tituloJob, ");
	    queryJobs.append(" J.description as dsJob, ");
	    queryJobs.append(" Case ");
	    queryJobs.append(" When s.schedulerType = 1 Then 'Por Hora' "); 
	    queryJobs.append(" When s.schedulerType = 2 Then 'Todos os Dias da Semana' "); 
	    queryJobs.append(" When s.schedulerType = 3 Then 'Diário'  ");
	    queryJobs.append(" When s.schedulerType = 4 Then 'Semanal'  ");
	    queryJobs.append(" When s.schedulerType = 5 Then 'Mensal'  ");
	    queryJobs.append(" When s.schedulerType = 6 Then 'Anual'  ");
	    queryJobs.append(" When s.schedulerType = 7 Then 'Por Minuto' "); 
	    queryJobs.append(" End As periodicidade, ");
	    queryJobs.append(" s.ntime as horarioExecucao, ");
	    queryJobs.append(" T.prevFireTime as ultimaAtualizacao, ");
	    queryJobs.append(" T.nextFireTime as proximaAtualizacao, ");
	    queryJobs.append(" CJ.adapter as adapter, ");
	    queryJobs.append(" Case When J.enabled = 0 Then 'Desabilitado' ");
	    queryJobs.append(" When J.enabled = 1 Then 'Ativo' ");
	    queryJobs.append(" End As status ");
	    queryJobs.append(" FROM Job J WITH(NOLOCK) ");
	    queryJobs.append(" INNER JOIN NeoTrigger T WITH(NOLOCK)ON T.job_neoId = J.neoId ");
	    queryJobs.append(" INNER JOIN Scheduler S WITH(NOLOCK) ON T.scheduler_neoId = S.neoId ");
	    queryJobs.append(" INNER JOIN CustomJob CJ WITH(NOLOCK) ON CJ.neoId = J.neoId ");
	    
	    stmt = conn.prepareStatement(queryJobs.toString());
	    
	    rs = stmt.executeQuery();


	    log.warn("SQL - buscarJobs - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");

	    while (rs.next()){
		PainelJobBean job = new PainelJobBean();
		job.setNomeJob(rs.getString("tituloJob"));
		job.setDescricaoJob(rs.getString("dsJob"));
		job.setPeriodicidade(rs.getString("periodicidade"));
		job.setHorarioExecucao(rs.getString("horarioExecucao"));
		Timestamp dataUltima = rs.getTimestamp("ultimaAtualizacao");
		String dat = null;
		if (dataUltima != null){
		    GregorianCalendar teste = new GregorianCalendar();	
		    teste.setTimeInMillis(dataUltima.getTime());
		    dat = NeoDateUtils.safeDateFormat(teste, "dd/MM/yyyy HH:mm:ss");	    
		}
		job.setDataHorarioUltimoExecucao(dat);
		Timestamp dataProxima = rs.getTimestamp("proximaAtualizacao");
		dat = null;
		if (dataProxima != null){
		    GregorianCalendar teste = new GregorianCalendar();	
		    teste.setTimeInMillis(dataProxima.getTime());
		    dat = NeoDateUtils.safeDateFormat(teste, "dd/MM/yyyy HH:mm:ss");
		}

		job.setDataHorarioProximaExecucao(dat);
		String adapter = rs.getString("adapter");
		if (adapter != null){
		    int index = adapter.lastIndexOf('.');
		    adapter = (adapter.substring(index +1));
		    
		}
		job.setAdapter(adapter);
		job.setStatus(rs.getString("status"));
		job.setIdJob(rs.getLong("idJob"));
		
		List<String> tarefas = buscarTarefasJob(job.getIdJob(), 0l);
		StringBuffer filtroTarefa = new StringBuffer();
		if (tarefas != null){
		    for (String string : tarefas) {
			filtroTarefa.append(string);
			filtroTarefa.append(" ");
		    }
		}
		job.setFiltroTarefas(filtroTarefa.toString());
		job.setSistema("Fusion");
		jobs.add(job);
	    }
	}catch (Exception e){
	    e.printStackTrace();
	}finally{
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	return jobs;
    }
    
    public List<PainelJobBean> buscarJobsVetorh(){

	Connection conn = null;
	PreparedStatement stmt = null;
	ResultSet rs = null;
	List<PainelJobBean> jobs = new ArrayList<PainelJobBean>();

	try{
	    conn = PersistEngine.getConnection("VETORH");
	    Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();

	    StringBuffer queryJobs = new StringBuffer();

	    queryJobs.append(" SELECT R300PRO.codpro 'idJob', ");
	    queryJobs.append(" R300PRO.despro 'tituloJob',  ");
	    queryJobs.append(" R999AGE.tipper 'codPer',  ");
	    queryJobs.append(" CASE  WHEN R999AGE.tipper = 0 THEN 'Uma única vez' ");  
	    queryJobs.append(" WHEN R999AGE.tipper = 1 THEN 'Minuto' ");
	    queryJobs.append(" WHEN R999AGE.tipper = 2 THEN 'Hora' ");
	    queryJobs.append(" WHEN R999AGE.tipper = 3 THEN 'Diário' ");
	    queryJobs.append(" WHEN R999AGE.tipper = 4 THEN 'Semanal' ");
	    queryJobs.append(" WHEN R999AGE.tipper = 5 THEN 'Mensal'END AS 'periodicidade', ");
	    queryJobs.append(" R999AGE.interv 'intervalo', ");
	    queryJobs.append(" CASE len(CAST((R999AGE.horult /60) as varchar(2))) when 1 then '0' + cast((R999AGE.horult /60) as varchar(2)) ");
	    queryJobs.append(" ELSE CAST((R999AGE.horult /60) as varchar(2)) END  + ':' + ");
	    queryJobs.append(" CASE len(CAST(CAST(ROUND(((R999AGE.horult/60.0) - (R999AGE.horult/60)) * 60,2 ) AS INT) as varchar(2))) ");
	    queryJobs.append(" when 1 then '0' + CAST(CAST(ROUND(((R999AGE.horult/60.0) - (R999AGE.horult/60)) * 60,2 ) AS INT) as varchar(2)) ");
	    queryJobs.append(" else cast(CAST(ROUND(((R999AGE.horult/60.0) - (R999AGE.horult/60)) * 60,2 ) AS INT) as varchar(2)) END AS 'horarioExecucao', "); 
	    queryJobs.append(" R999AGE.datult 'ultimaAtualizacao',  ");
	    queryJobs.append(" R300PRO.DatExe 'proximaAtualizacao', ");
	    queryJobs.append(" R300PRO.codreg 'regra', ");
	    queryJobs.append(" CASE WHEN R300PRO.stapro = 0 THEN 'Desabilitado' ");
	    queryJobs.append(" WHEN R300PRO.stapro = 1 THEN 'Ativo' END AS 'status', ");       
	    queryJobs.append(" R300PRO.tippat 'tarefa', ");
	    queryJobs.append(" (SELECT valkey FROM r996lsf WHERE r996lsf.lstnam = 'LTipPat' AND r996lsf.keynam = R300PRO.tippat ) AS 'nomeTarefa' ");
	    queryJobs.append(" FROM R999AGE, R300PRO  ");
	    queryJobs.append(" WHERE R999AGE.codpro = R300PRO.codage ");
	    
	    stmt = conn.prepareStatement(queryJobs.toString());
	    
	    rs = stmt.executeQuery();


	    log.warn("SQL - buscarJobs - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");

	    while (rs.next()){
		PainelJobBean job = new PainelJobBean();
		job.setNomeJob(rs.getString("tituloJob"));
		job.setDescricaoJob(rs.getString("tituloJob"));
		job.setPeriodicidade(rs.getString("periodicidade"));
		job.setHorarioExecucao(rs.getString("horarioExecucao"));
		Timestamp dataUltima = rs.getTimestamp("ultimaAtualizacao");
		String dat = null;
		GregorianCalendar teste = new GregorianCalendar();	
		if (dataUltima != null){
		    teste.setTimeInMillis(dataUltima.getTime());
		    dat = NeoDateUtils.safeDateFormat(teste, "dd/MM/yyyy HH:mm:ss");	    
		}
		job.setDataHorarioUltimoExecucao(dat);
		
		Integer codPer = rs.getInt("codPer");	
		Integer intervalo = rs.getInt("intervalo");
		job.setDataHorarioProximaExecucao(calcularProxExecucao(codPer, intervalo, teste));
		job.setAdapter("");
		job.setStatus(rs.getString("status"));
		job.setIdJob(rs.getLong("idJob"));
		
		List<String> tarefas = buscarTarefasJob(job.getIdJob(), 1l);
		StringBuffer filtroTarefa = new StringBuffer();
		if (tarefas != null){
		    for (String string : tarefas) {
			filtroTarefa.append(string);
			filtroTarefa.append(" ");
		    }
		}
		job.setFiltroTarefas(filtroTarefa.toString());
		job.setSistema("Vetorh");
		jobs.add(job);
	    }
	}catch (Exception e){
	    e.printStackTrace();
	}finally{
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	return jobs;
    }
    
    public List<PainelJobBean> buscarJobsSapiens(){

	Connection conn = null;
	PreparedStatement stmt = null;
	ResultSet rs = null;
	List<PainelJobBean> jobs = new ArrayList<PainelJobBean>();

	try{
	    conn = PersistEngine.getConnection("SAPIENS");
	    Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();

	    StringBuffer queryJobs = new StringBuffer();

	    queryJobs.append(" SELECT E000AGE.CodPra 'idJob', ");
	    queryJobs.append(" E000AGE.DesPra 'tituloJob', ");
	    queryJobs.append(" R999AGE.tipper 'codPer',  ");
	    queryJobs.append(" CASE  WHEN R999AGE.tipper = 0 THEN 'Uma única vez' ");  
	    queryJobs.append(" WHEN R999AGE.tipper = 1 THEN 'Minuto' ");
	    queryJobs.append(" WHEN R999AGE.tipper = 2 THEN 'Hora' ");
	    queryJobs.append(" WHEN R999AGE.tipper = 3 THEN 'Diário' ");
	    queryJobs.append(" WHEN R999AGE.tipper = 4 THEN 'Semanal' ");
	    queryJobs.append(" WHEN R999AGE.tipper = 5 THEN 'Mensal'END AS 'periodicidade', ");
	    queryJobs.append(" R999AGE.interv 'intervalo', ");
	    queryJobs.append(" CASE len(CAST((R999AGE.horult /60) as varchar(2))) when 1 then '0' + cast((R999AGE.horult /60) as varchar(2)) ");
	    queryJobs.append(" ELSE CAST((R999AGE.horult /60) as varchar(2)) END  + ':' + ");
	    queryJobs.append(" CASE len(CAST(CAST(ROUND(((R999AGE.horult/60.0) - (R999AGE.horult/60)) * 60,2 ) AS INT) as varchar(2))) ");
	    queryJobs.append(" when 1 then '0' + CAST(CAST(ROUND(((R999AGE.horult/60.0) - (R999AGE.horult/60)) * 60,2 ) AS INT) as varchar(2)) ");
	    queryJobs.append(" else cast(CAST(ROUND(((R999AGE.horult/60.0) - (R999AGE.horult/60)) * 60,2 ) AS INT) as varchar(2)) END AS 'horarioExecucao', "); 
	    queryJobs.append(" R999AGE.datult 'ultimaAtualizacao',  ");
	    queryJobs.append(" '1900-12-31 00:00:00.000' as 'proximaAtualizacao', ");
	    queryJobs.append(" E000AGE.codreg 'regra', ");
	    queryJobs.append(" CASE WHEN R999AGE.ENABLE = 'F' THEN 'Desabilitado' "); 
	    queryJobs.append("  WHEN R999AGE.ENABLE = 'T' THEN 'Ativo' END AS 'status', ");
	    queryJobs.append(" E000AGE.TipPra 'tarefa', ");
	    queryJobs.append(" (SELECT valkey FROM r996lsf WHERE r996lsf.lstnam = 'LTipPra' AND r996lsf.keynam = E000AGE.TipPra ) AS 'nomeTarefa' ");
	    queryJobs.append(" FROM R999AGE, E000AGE  ");
	    queryJobs.append(" WHERE R999AGE.codpro = E000AGE.CodPra ");
	    queryJobs.append(" AND R999AGE.ENABLE = 'T' ");
	    queryJobs.append(" order by codpro ");
	    
	    stmt = conn.prepareStatement(queryJobs.toString());
	    
	    rs = stmt.executeQuery();


	    log.warn("SQL - buscarJobs - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");

	    while (rs.next()){
		PainelJobBean job = new PainelJobBean();
		job.setNomeJob(rs.getString("tituloJob"));
		job.setDescricaoJob(rs.getString("tituloJob"));
		job.setPeriodicidade(rs.getString("periodicidade"));
		job.setHorarioExecucao(rs.getString("horarioExecucao"));
		Timestamp dataUltima = rs.getTimestamp("ultimaAtualizacao");
		String dat = null;
		GregorianCalendar teste = new GregorianCalendar();	
		if (dataUltima != null){
		    teste.setTimeInMillis(dataUltima.getTime());
		    dat = NeoDateUtils.safeDateFormat(teste, "dd/MM/yyyy HH:mm:ss");	    
		}
		job.setDataHorarioUltimoExecucao(dat);
		
		Integer codPer = rs.getInt("codPer");	
		Integer intervalo = rs.getInt("intervalo");
		job.setDataHorarioProximaExecucao(calcularProxExecucao(codPer, intervalo, teste));
		job.setAdapter("");
		job.setStatus(rs.getString("status"));
		job.setIdJob(rs.getLong("idJob"));
		
		List<String> tarefas = buscarTarefasJob(job.getIdJob(), 1l);
		StringBuffer filtroTarefa = new StringBuffer();
		if (tarefas != null){
		    for (String string : tarefas) {
			filtroTarefa.append(string);
			filtroTarefa.append(" ");
		    }
		}
		job.setFiltroTarefas(filtroTarefa.toString());
		job.setSistema("Sapiens");
		jobs.add(job);
	    }
	}catch (Exception e){
	    e.printStackTrace();
	}finally{
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	return jobs;
    }
 
    public String calcularProxExecucao(Integer codPer, Integer intervalo, GregorianCalendar dataUltExec){
	
	if (codPer == 0){
	    dataUltExec.add(Calendar.DAY_OF_MONTH, intervalo);
	}else if (codPer == 1){
	    dataUltExec.add(Calendar.MINUTE, intervalo);
	}else if (codPer == 2){
	    dataUltExec.add(Calendar.HOUR_OF_DAY, intervalo);
	}else if (codPer == 3){
	    dataUltExec.add(Calendar.DAY_OF_MONTH, intervalo);
	}else if (codPer == 4){
	    dataUltExec.add(Calendar.WEEK_OF_MONTH, intervalo);
	}else if (codPer == 5){
	    dataUltExec.add(Calendar.MONTH, intervalo);
	}
	
	return NeoDateUtils.safeDateFormat(dataUltExec, "dd/MM/yyyy HH:mm:ss");
    }
    
    public List<String> buscarTarefasJob(Long idJob, Long sistema){

	Connection conn = null;
	PreparedStatement stmt = null;
	ResultSet rs = null;
	List<String> tarefas = new ArrayList<String>();

	try{
	    conn = PersistEngine.getConnection("");
	    Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();

	    StringBuffer queryJobs = new StringBuffer();

	    queryJobs.append(" select job.job as job, tarefa.tituloTarefa as tituloTarefa  ");
	    queryJobs.append(" from D_ControleTarefasJob job with(NOLOCK) ");
	    queryJobs.append(" inner join D_ControleTarefasJob_tarefas tarefaJob with(NOLOCK) on tarefaJob.D_ControleTarefasJob_neoId = job.neoId ");
	    queryJobs.append(" inner join D_TituloTarefaJob tarefa with(NOLOCK) on tarefa.neoId = tarefaJob.tarefas_neoId where 1=1");
	    if (idJob != null){
		queryJobs.append(" and job.job = ");
		queryJobs.append(idJob); 
	    }
	    if (sistema != null){
		queryJobs.append(" and job.sistema = ");
		queryJobs.append(sistema);
	    }
	    
	    stmt = conn.prepareStatement(queryJobs.toString());
	    
	    rs = stmt.executeQuery();


	    log.warn("SQL - buscarJobs - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");

	    while (rs.next()){
		String tarefa = rs.getString("tituloTarefa");
		tarefas.add(tarefa);
	    }
	}catch (Exception e){
	    e.printStackTrace();
	}finally{
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	return tarefas;
    }
    
    public List<PainelJobErrosBean> buscarJobsComErro(List<Long> idsJobs){

	Connection conn = null;
	PreparedStatement stmt = null;
	ResultSet rs = null;
	List<PainelJobErrosBean> jobsErros = new ArrayList<PainelJobErrosBean>();

	try{
	    conn = PersistEngine.getConnection("");
	    Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();

	    StringBuffer queryJobs = new StringBuffer();
	    String listaDeJobs = "";
	    if (idsJobs != null){
		listaDeJobs = Joiner.on(",").join(idsJobs); 
	    }
	    
	    queryJobs.append(" Select ");
	    queryJobs.append(" Case  ");
	    queryJobs.append(" When log.state = 2 Then 'JOB_EXECUTIONVETOED' "); 
	    queryJobs.append(" When log.state = 6 Then 'JOB_WASEXECUTED_EXCEPTION' "); 
	    queryJobs.append(" When log.state = 7 Then 'JOB_EXECUTION_EXCEPTION'  ");
	    queryJobs.append(" When log.state = 10 Then 'TRIGGER_MISFIRED'  ");
	    queryJobs.append(" End As error, job.neoId as idJob, job.name as nomeJob, log.details as detalhes, log.firedTime as dataErro ");
	    queryJobs.append(" From Job job With (NOLOCK)  ");
	    queryJobs.append(" Inner join JobEventLog log With (NOLOCK) on log.job_neoId = job.neoId "); 
	    queryJobs.append(" Inner join qrtz_triggers q on q.job_name = job.name+'_'+cast(job.neoid as varchar) ");
//	    queryJobs.append(" Inner join NeoTrigger tri With (NOLOCK) on tri.neoId = log.trigger_neoId  ");
//	    queryJobs.append(" Inner join Scheduler sch With (NOLOCK) on sch.neoid = tri.scheduler_neoId  ");
	    queryJobs.append(" Where  ");
	    queryJobs.append(" ( (log.firedTime >= DATEADD(MINUTE,-5,GETDATE()) And log.firedTime < GETDATE() and log.state in (2,6,7,10)) "); 
	    queryJobs.append(" or ( (job.nextFireTime is null or job.nextFireTime = '1900-01-01 00:00:00.000'))  or (job.nextFireTime < DATEADD(MINUTE,-5,GETDATE()))) and job.name not like 'Abre tarefa simples Job% com problemas' and job.enabled = 1  ");//and Tri.neoId = 632748707 ");//and job.enabled = 1 ");
	    if (idsJobs != null && !idsJobs.isEmpty()){
		queryJobs.append(" and job.neoId not in ("+listaDeJobs+") ");
	    }
	    queryJobs.append(" Order By log.firedTime Desc,  ");
	    queryJobs.append(" job.neoid,  ");
	    queryJobs.append(" log.state ");
	    
	    stmt = conn.prepareStatement(queryJobs.toString());
	    
	    rs = stmt.executeQuery();

	    log.warn("SQL - buscarJobsComErro - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");

	    while (rs.next()){
		PainelJobErrosBean erro = new PainelJobErrosBean();
		erro.setIdJob(rs.getLong("idJob"));
		
		erro.setNomeJob(rs.getString("nomeJob"));
		String detalhes = "NULL";
		if (rs.getString("detalhes") != null && !rs.getString("detalhes").isEmpty()){
		    detalhes = rs.getString("detalhes");
		}
		erro.setDetalhes(detalhes);
		String error = "NULL";
		if (rs.getString("error") != null && !rs.getString("error").isEmpty()){
		    error = rs.getString("error");
		}
		
		erro.setError(error);
		
		Timestamp dataUltima = rs.getTimestamp("dataErro");
		String dat = null;
		if (dataUltima != null){
		    GregorianCalendar teste = new GregorianCalendar();	
		    teste.setTimeInMillis(dataUltima.getTime());
		    dat = NeoDateUtils.safeDateFormat(teste, "dd/MM/yyyy HH:mm:ss");	    
		}
		erro.setDataErro(dat);
		erro.setSistema("Fusion");
		jobsErros.add(erro);
	    }
	    
	}catch (Exception e){
	    e.printStackTrace();
	}finally{
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	return jobsErros;
    }
    
    public List<PainelJobErrosBean> buscarJobsVetorhComErro(List<Long> idsJobs){

	Connection conn = null;
	PreparedStatement stmt = null;
	ResultSet rs = null;
	List<PainelJobErrosBean> jobsErros = new ArrayList<PainelJobErrosBean>();

	try{
	    conn = PersistEngine.getConnection("VETORH");
	    Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();

	    StringBuffer queryJobs = new StringBuffer();
	    String listaDeJobs = "";
	    if (idsJobs != null){
		listaDeJobs = Joiner.on(",").join(idsJobs); 
	    }
	    
	    queryJobs.append("  SELECT convert(datetime, CONVERT(float,R300LOG.dthlog-2)) 'dataErro', ");
	    queryJobs.append("  R300LOG.codpro 'idJob', R300PRO.despro 'nomeJob', ");
	    queryJobs.append("  (SELECT valkey FROM r996lsf WHERE r996lsf.lstnam = 'LTipLor' AND r996lsf.keynam = R300LOG.tiplor ) AS 'error', ");
	    queryJobs.append("  R300LOG.msglog '	' ");
	    queryJobs.append("  FROM R300LOG, R300PRO  ");
	    queryJobs.append("  WHERE tiplor = 'E' ");
	    queryJobs.append("  AND R300LOG.TipPat =  R300PRO.TipPat ");
	    queryJobs.append("  AND R300LOG.CodPro =  R300PRO.CodPro ");
	    queryJobs.append("  AND convert(datetime, CONVERT(float,R300LOG.dthlog-2)) >= DATEADD(MINUTE,-5,GETDATE()) ");
	    if (idsJobs != null && !idsJobs.isEmpty()){
		queryJobs.append(" and R300LOG.codpro not in ("+listaDeJobs+") ");
	    }
	    queryJobs.append("  ORDER BY 'dataErro' desc ");
	    
	    stmt = conn.prepareStatement(queryJobs.toString());
	    
	    rs = stmt.executeQuery();

	    log.warn("SQL - buscarJobsVetorhComErro - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");

	    while (rs.next()){
		PainelJobErrosBean erro = new PainelJobErrosBean();
		erro.setIdJob(rs.getLong("idJob"));
		
		erro.setNomeJob(rs.getString("nomeJob"));
		String detalhes = "NULL";
		if (rs.getString("detalhes") != null && !rs.getString("detalhes").isEmpty()){
		    detalhes = rs.getString("detalhes");
		}
		erro.setDetalhes(detalhes);
		String error = "NULL";
		if (rs.getString("error") != null && !rs.getString("error").isEmpty()){
		    error = rs.getString("error");
		}
		
		erro.setError(error);
		
		Timestamp dataUltima = rs.getTimestamp("dataErro");
		String dat = null;
		if (dataUltima != null){
		    GregorianCalendar teste = new GregorianCalendar();	
		    teste.setTimeInMillis(dataUltima.getTime());
		    dat = NeoDateUtils.safeDateFormat(teste, "dd/MM/yyyy HH:mm:ss");	    
		}
		erro.setDataErro(dat);
		erro.setSistema("Vetorh");
		jobsErros.add(erro);
	    }
	    
	}catch (Exception e){
	    e.printStackTrace();
	}finally{
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	return jobsErros;
    }
    
    public List<PainelJobErrosBean> buscarJobsSapiensComErro(List<Long> idsJobs){

	Connection conn = null;
	PreparedStatement stmt = null;
	ResultSet rs = null;
	List<PainelJobErrosBean> jobsErros = new ArrayList<PainelJobErrosBean>();

	try{
	    conn = PersistEngine.getConnection("SAPIENS");
	    Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();

	    StringBuffer queryJobs = new StringBuffer();
	    String listaDeJobs = "";
	    if (idsJobs != null){
		listaDeJobs = Joiner.on(",").join(idsJobs); 
	    }
	    
	    queryJobs.append("  SELECT E000LPA.DatFim 'dataErro', ");
	    queryJobs.append(" E000AGE.CodPra 'idJob', ");
	    queryJobs.append(" E000AGE.DesPra 'nomeJob',  ");
	    queryJobs.append(" (SELECT valkey FROM r996lsf WHERE r996lsf.lstnam = 'LTipLor' AND r996lsf.keynam = E000LPA.tiplor ) AS 'error', ");
	    queryJobs.append(" E000LPA.msglg1 'detalhes' ");
	    queryJobs.append(" FROM E000LPA ");
	    queryJobs.append(" INNER JOIN E000AGE on E000LPA.CodPra = E000AGE.CodPra AND E000LPA.DatFim = (SELECT MAX(DATFIM) FROM E000LPA LPA1 WHERE LPA1.CodPra = E000LPA.CodPra) ");
	    queryJobs.append(" INNER JOIN R999AGE on R999AGE.CODPRO = E000AGE.CodPra ");
	    queryJobs.append(" WHERE tiplor = 'E' ");
	    if (idsJobs != null && !idsJobs.isEmpty()){
		queryJobs.append(" and E000AGE.CodPra not in ("+listaDeJobs+") ");
	    }
	    queryJobs.append(" AND R999AGE.ENABLE = 'T' ");
	    queryJobs.append(" ORDER BY 'dataErro' desc  ");
	    
	    stmt = conn.prepareStatement(queryJobs.toString());
	    
	    rs = stmt.executeQuery();

	    log.warn("SQL - buscarJobsVetorhComErro - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");

	    while (rs.next()){
		PainelJobErrosBean erro = new PainelJobErrosBean();
		erro.setIdJob(rs.getLong("idJob"));
		
		erro.setNomeJob(rs.getString("nomeJob"));
		String detalhes = "NULL";
		if (rs.getString("detalhes") != null && !rs.getString("detalhes").isEmpty()){
		    detalhes = rs.getString("detalhes");
		}
		erro.setDetalhes(detalhes);
		String error = "NULL";
		if (rs.getString("error") != null && !rs.getString("error").isEmpty()){
		    error = rs.getString("error");
		}
		
		erro.setError(error);
		
		Timestamp dataUltima = rs.getTimestamp("dataErro");
		String dat = null;
		if (dataUltima != null){
		    GregorianCalendar teste = new GregorianCalendar();	
		    teste.setTimeInMillis(dataUltima.getTime());
		    dat = NeoDateUtils.safeDateFormat(teste, "dd/MM/yyyy HH:mm:ss");	    
		}
		erro.setDataErro(dat);
		erro.setSistema("Vetorh");
		jobsErros.add(erro);
	    }
	    
	}catch (Exception e){
	    e.printStackTrace();
	}finally{
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	return jobsErros;
    }
    
    public void atualizarErros(){
	List<NeoObject> listaErros = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("errosJobs"));
	List<Long> idsJobs = null;
	List<Long> idsJobsNULL = null;
	if (listaErros != null && !listaErros.isEmpty()){
	    idsJobs = new ArrayList<Long>();
	    idsJobsNULL = new ArrayList<Long>();
	    for (NeoObject neo : listaErros) {
		EntityWrapper erro = new EntityWrapper(neo);
		Long idJob = (Long) erro.findField("idJob").getValue();
		String nomeErro = (String) erro.findField("nomeErro").getValue();
		String detalhesErro = (String) erro.findField("detalhesErro").getValue();
		Long sistema = (Long) erro.findField("sistema").getValue();
		if (sistema != null && sistema.equals(0l)){
		    if (idJob != null){
			idsJobs.add(idJob);
			if (nomeErro.equals("NULL") && detalhesErro.equals("NULL")){
			    idsJobsNULL.add(idJob);
			}
		    }
		}
	    }
	}
	if (idsJobs == null){
	    idsJobs = new ArrayList<Long>();
	}
	if (idsJobsNULL == null){
	    idsJobsNULL = new ArrayList<Long>();
	}
	List<PainelJobErrosBean> errosJobs = buscarJobsComErro(idsJobs);
	if (errosJobs != null && !errosJobs.isEmpty()){
	    List<Long> idsErrosJobs = new ArrayList<Long>();
	    for (PainelJobErrosBean painelJobErrosBean : errosJobs) {		
		if (!idsErrosJobs.contains(painelJobErrosBean.getIdJob())){		    
		    idsErrosJobs.add(painelJobErrosBean.getIdJob());
		}
	    }
	    
	    for (Long idJob : idsJobsNULL){
		if (!idsErrosJobs.contains(idJob)){
		    gravarVerificacaoErro(idJob, 0l);
		}
	    }
	    
	    for (PainelJobErrosBean painelJobErrosBean : errosJobs) {
		if (!idsJobs.contains(painelJobErrosBean.getIdJob())){
		    gravarErroJob(painelJobErrosBean.getIdJob(), painelJobErrosBean.getNomeJob(), painelJobErrosBean.getError(), painelJobErrosBean.getDetalhes(), painelJobErrosBean.getDataErro(), 0L);
		    idsJobs.add(painelJobErrosBean.getIdJob());
		}
	    }
	}else{
	    for (Long idJob : idsJobsNULL){
		gravarVerificacaoErro(idJob, 0l);
	    }
	}
	atualizarErrosVetorh();
    }
    
    public void atualizarErrosVetorh(){
	List<NeoObject> listaErros = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("errosJobs"));
	List<Long> idsJobs = null;
	if (listaErros != null && !listaErros.isEmpty()){
	    idsJobs = new ArrayList<Long>();
	    for (NeoObject neo : listaErros) {
		EntityWrapper erro = new EntityWrapper(neo);
		Long idJob = (Long) erro.findField("idJob").getValue();
		Long sistema = (Long) erro.findField("sistema").getValue();
		if (sistema != null && sistema.equals(1l)){
		    if (idJob != null){
			idsJobs.add(idJob);
		    }
		}
	    }
	}
	if (idsJobs == null){
	    idsJobs = new ArrayList<Long>();
	}
	List<PainelJobErrosBean> errosJobs = buscarJobsVetorhComErro(idsJobs);
	if (errosJobs != null && !errosJobs.isEmpty()){
	    List<Long> idsErrosJobs = new ArrayList<Long>();
	    for (PainelJobErrosBean painelJobErrosBean : errosJobs) {		
		if (!idsErrosJobs.contains(painelJobErrosBean.getIdJob())){		    
		    idsErrosJobs.add(painelJobErrosBean.getIdJob());
		}
	    }
	    
	    for (PainelJobErrosBean painelJobErrosBean : errosJobs) {
		if (!idsJobs.contains(painelJobErrosBean.getIdJob())){
		    gravarErroJob(painelJobErrosBean.getIdJob(), painelJobErrosBean.getNomeJob(), painelJobErrosBean.getError(), painelJobErrosBean.getDetalhes(), painelJobErrosBean.getDataErro(), 1L);
		    idsJobs.add(painelJobErrosBean.getIdJob());
		}
	    }
	}
	atualizarErrosSapiens();
    }
    
    public void atualizarErrosSapiens(){
	List<NeoObject> listaErros = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("errosJobs"));
	List<Long> idsJobs = null;
	if (listaErros != null && !listaErros.isEmpty()){
	    idsJobs = new ArrayList<Long>();
	    for (NeoObject neo : listaErros) {
		EntityWrapper erro = new EntityWrapper(neo);
		Long idJob = (Long) erro.findField("idJob").getValue();
		Long sistema = (Long) erro.findField("sistema").getValue();
		if (sistema != null && sistema.equals(2l)){
		    if (idJob != null){
			idsJobs.add(idJob);
		    }
		}
	    }
	}
	if (idsJobs == null){
	    idsJobs = new ArrayList<Long>();
	}
	List<PainelJobErrosBean> errosJobs = buscarJobsSapiensComErro(idsJobs);
	if (errosJobs != null && !errosJobs.isEmpty()){
	    List<Long> idsErrosJobs = new ArrayList<Long>();
	    for (PainelJobErrosBean painelJobErrosBean : errosJobs) {		
		if (!idsErrosJobs.contains(painelJobErrosBean.getIdJob())){		    
		    idsErrosJobs.add(painelJobErrosBean.getIdJob());
		}
	    }
	    
	    for (PainelJobErrosBean painelJobErrosBean : errosJobs) {
		if (!idsJobs.contains(painelJobErrosBean.getIdJob())){
		    gravarErroJob(painelJobErrosBean.getIdJob(), painelJobErrosBean.getNomeJob(), painelJobErrosBean.getError(), painelJobErrosBean.getDetalhes(), painelJobErrosBean.getDataErro(), 2L);
		    idsJobs.add(painelJobErrosBean.getIdJob());
		}
	    }
	}
    }
    
    public List<PainelJobErrosBean> buscarErrosAtuais(){
	List<NeoObject> listaErros = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("errosJobs"));
	List<PainelJobErrosBean> erros = null;
	if (listaErros != null && !listaErros.isEmpty()){
	    erros = new ArrayList<PainelJobErrosBean>();
	    for (NeoObject neo : listaErros) {
		EntityWrapper erro = new EntityWrapper(neo);
		PainelJobErrosBean pjeb = new PainelJobErrosBean();
		pjeb.setIdJob((Long) erro.findField("idJob").getValue());
		pjeb.setNomeJob((String) erro.findField("nomeJob").getValue());
		pjeb.setError((String) erro.findField("nomeErro").getValue());
		pjeb.setDetalhes((String) erro.findField("detalhesErro").getValue());
		pjeb.setDataErro((String) erro.findField("dataErro").getValue());
		Long sistema = (Long) erro.findField("sistema").getValue();
		if (sistema != null){
		    if (sistema.equals(0l)){
			pjeb.setSistema("Fusion");
		    }else if (sistema.equals(1l)){
			pjeb.setSistema("Vetorh");
		    }else if (sistema.equals(2l)){
			pjeb.setSistema("Sapiens");
		    }
		}
		erros.add(pjeb);
	    }
	}
	return erros;
    }
    
    public List<String> buscarTarefasAgendador(){

	Connection conn = null;
	PreparedStatement stmt = null;
	ResultSet rs = null;
	List<String> tarefas = new ArrayList<String>();

	try{
	    conn = PersistEngine.getConnection("");
	    Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();

	    StringBuffer queryJobs = new StringBuffer();

	    queryJobs.append(" SELECT T.titulo, ");
	    queryJobs.append(" I.descricao,  ");
	    queryJobs.append(" T.diaDoMes,  ");
	    queryJobs.append(" T.ultimaExecucao, "); 
	    queryJobs.append(" T.abreTarefa  ");
	    queryJobs.append(" FROM D_TIAgendadorTarefas T WITH(NOLOCK) ");
	    queryJobs.append(" INNER JOIN D_TIAgendadorTarefasIntervalo I ON I.neoId = T.intervalo_neoId ");
	    
	    stmt = conn.prepareStatement(queryJobs.toString());
	    
	    rs = stmt.executeQuery();


	    log.warn("SQL - buscarTarefasAgendador - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");

	    while (rs.next()){
		String tarefa = rs.getString("titulo");
		tarefas.add(tarefa);
	    }
	}catch (Exception e){
	    e.printStackTrace();
	}finally{
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	return tarefas;
    }
    /**
     * 
     * @autor Lucas.alison
     * @param idJob
     */
    public void gravarVerificacaoErro(Long idJob, Long sistema){
	QLGroupFilter groupFilter = new QLGroupFilter("AND");
	groupFilter.addFilter(new QLEqualsFilter("idJob", idJob));
	groupFilter.addFilter(new QLEqualsFilter("sistema", sistema));
	NeoObject o = PersistEngine.getObject(AdapterUtils.getEntityClass("errosJobs"), groupFilter);
	if (o != null)
	    PersistEngine.remove(o);
    }
    
    public void gravarErroJob(Long idJob, String nomeJob, String erro, String detalhes, String data, Long sistema){
	InstantiableEntityInfo errosVerificados = AdapterUtils.getInstantiableEntityInfo("errosJobs");
	NeoObject objErrosVerificador = errosVerificados.createNewInstance();
	EntityWrapper wrapperErrosVerificador = new EntityWrapper(objErrosVerificador);
	
	wrapperErrosVerificador.findField("idJob").setValue(idJob);
	wrapperErrosVerificador.findField("nomeJob").setValue(nomeJob);
	wrapperErrosVerificador.findField("nomeErro").setValue(erro);
	wrapperErrosVerificador.findField("detalhesErro").setValue(detalhes);
	wrapperErrosVerificador.findField("dataErro").setValue(data);
	wrapperErrosVerificador.findField("sistema").setValue(sistema);
	
	PersistEngine.persist(objErrosVerificador);
    }
    
    public List<HashMap<String, Object>> tratarDadosGraficoDiario(List<HashMap<String, Object>> dados){
	List<HashMap<String, Object>> retorno = new ArrayList<>();
	for (HashMap<String, Object> map : dados) {
	    if (map.get("schedulerType").equals(1)){
		int repeat = (int)map.get("repeat");
		Calendar c = new GregorianCalendar();
		c.setTime(new Date());
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		int currentDay = c.get(Calendar.DAY_OF_MONTH);
		while(currentDay == c.get(Calendar.DAY_OF_MONTH)){
		    HashMap<String, Object> dado = new HashMap<>();
		    dado.put("idJob", map.get("idJob"));
		    dado.put("nomeJob", map.get("nomeJob"));
		    dado.put("schedulerType", map.get("schedulerType"));
		    dado.put("repeat", map.get("repeat"));
		    dado.put("ntime", c.getTimeInMillis());
		    dado.put("sequencia", map.get("sequencia"));
		    retorno.add(dado);
		    c.add(Calendar.HOUR_OF_DAY, repeat);
		}
	    }else if (map.get("schedulerType").equals(2)){
		retorno.add(map);
	    }else if (map.get("schedulerType").equals(3)){
		retorno.add(map);
	    }else if (map.get("schedulerType").equals(7)){
		int repeat = (int) map.get("repeat");
		Calendar c = new GregorianCalendar();
		c.setTime(new Date());
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		int currentDay = c.get(Calendar.DAY_OF_MONTH);
		while(currentDay == c.get(Calendar.DAY_OF_MONTH)){
		    HashMap<String, Object> dado = new HashMap<>();
		    dado.put("idJob", map.get("idJob"));
		    dado.put("nomeJob", map.get("nomeJob"));
		    dado.put("schedulerType", map.get("schedulerType"));
		    dado.put("repeat", map.get("repeat"));
		    dado.put("ntime", c.getTimeInMillis());
		    dado.put("sequencia", map.get("sequencia"));
		    retorno.add(dado);
		    c.add(Calendar.MINUTE, repeat);
		}
	    }
	}
	return retorno;
    }
    
    public List<HashMap<String, Object>> tratarCoresJobsRetorno(List<HashMap<String, Object>> dados, String range){
	List<HashMap<String, Object>> retorno = new ArrayList<>();
	HashMap<String,Object> dado = null;
	Random random = new Random();
	int r = 0, g = 0, b = 0;
	for (HashMap<String, Object> map : dados) {
	    if (containsJob(retorno, (String) map.get("nomeJob"))){
		for (HashMap<String, Object> hashMap : retorno) {
		    if (hashMap.get("name").equals(map.get("nomeJob"))){
			dado = hashMap;
		    }
		}
		ArrayList<Object> data = new ArrayList<>();
		data.add(map.get(range));
		data.add(map.get("sequencia"));
		((ArrayList<Object>)dado.get("data")).add(data);
	    }else{
		dado = new HashMap<>();
		dado.put("name", map.get("nomeJob"));
		r = random.nextInt(255)+1;
		g = random.nextInt(255)+1;
		b = random.nextInt(255)+1;
		dado.put("color", "rgba("+r+","+g+","+b+",.5)");
		ArrayList<Object> data = new ArrayList<>();
		data.add(map.get(range));
		data.add(map.get("sequencia"));
		ArrayList<Object> datas = new ArrayList<>();
		datas.add(data);
		dado.put("data", datas);
		retorno.add(dado);
	    }
	}
	return retorno;
    }
    
    public Boolean containsJob(List<HashMap<String, Object>> list, String job){
	for (HashMap<String, Object> map : list) {
	    if (map.get("name").equals(job)){
		return true;
	    }
	}
	return false;
    }
    
    public List<HashMap<String, Object>> buscarDadosDiarioGraficoJobs(String tipos){
	Connection conn = null;
	PreparedStatement stmt = null;
	ResultSet rs = null;
	List<HashMap<String, Object>> retorno = new ArrayList<>();
	try{
	    conn = PersistEngine.getConnection("");
	    Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();

	    StringBuffer queryJobs = new StringBuffer();
	    
	    queryJobs.append(" SELECT job.neoId as idJob,  ");
	    queryJobs.append(" job.name as nomeJob, ");
	    queryJobs.append(" S.schedulerType, ");
	    queryJobs.append(" S.repeat, ");
	    queryJobs.append(" convert(Char(5),cast(S.ntime as time),114) as ntime ");
	    queryJobs.append(" FROM NeoTrigger tri WITH (NOLOCK) ");
	    queryJobs.append(" INNER JOIN Scheduler S WITH(NOLOCK) ON tri.scheduler_neoId = S.neoId ");
	    queryJobs.append(" Inner join Job job With (NOLOCK) on job.neoId = tri.job_neoId ");
	    queryJobs.append(" WHERE TRI.enabled = 1 ");
	    queryJobs.append(" AND Job.enabled = 1 ");
	    if (tipos != null && !tipos.isEmpty() && !tipos.equals("0")){
		queryJobs.append(" AND S.schedulerType = ? ");
	    }else{
		queryJobs.append(" AND S.schedulerType in (1,2,3,7) ");
	    }
	    queryJobs.append(" order by ntime ");
	    
	    stmt = conn.prepareStatement(queryJobs.toString());
	    if (tipos != null && !tipos.isEmpty() && !tipos.equals("0")){
		stmt.setString(1, tipos);
	    }
	    rs = stmt.executeQuery();


	    log.warn("SQL - buscarDadosGraficoJobs - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
	    float i = 1f;
	    while (rs.next()){
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("idJob", rs.getLong("idJob"));
		map.put("nomeJob", rs.getString("nomeJob"));
		Calendar c = new GregorianCalendar();
		c.setTime(new Date());
		String string[] = rs.getString("ntime").split(":");		
		c.set(Calendar.HOUR_OF_DAY, Integer.valueOf(string[0]));
		c.set(Calendar.MINUTE, Integer.valueOf(string[1]));
		map.put("ntime", c.getTimeInMillis());
		map.put("schedulerType", rs.getInt("schedulerType"));
		map.put("repeat", rs.getInt("repeat"));
		map.put("sequencia", i);
		i++;
		retorno.add(map);
	    }
	}catch (Exception e){
	    e.printStackTrace();
	}finally{
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	return tratarCoresJobsRetorno(tratarDadosGraficoDiario(retorno), "ntime");
    }
    
    public List<HashMap<String, Object>> buscarDadosHorarioGraficoJobs(){
	Connection conn = null;
	PreparedStatement stmt = null;
	ResultSet rs = null;
	List<HashMap<String, Object>> retorno = new ArrayList<>();
	try{
	    conn = PersistEngine.getConnection("");
	    Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();

	    StringBuffer queryJobs = new StringBuffer();
	    
	    queryJobs.append(" SELECT job.neoId as idJob,  ");
	    queryJobs.append(" job.name as nomeJob, ");
	    queryJobs.append(" S.schedulerType, ");
	    queryJobs.append(" S.repeat, ");
	    queryJobs.append(" convert(Char(5),cast(S.ntime as time),114) as ntime ");
	    queryJobs.append(" FROM NeoTrigger tri WITH (NOLOCK) ");
	    queryJobs.append(" INNER JOIN Scheduler S WITH(NOLOCK) ON tri.scheduler_neoId = S.neoId ");
	    queryJobs.append(" Inner join Job job With (NOLOCK) on job.neoId = tri.job_neoId ");
//	    queryJobs.append(" WHERE TRI.enabled = 1 ");
//	    queryJobs.append(" AND Job.enabled = 1 ");
	    queryJobs.append(" order by ntime ");
	    
	    stmt = conn.prepareStatement(queryJobs.toString());
	    rs = stmt.executeQuery();


	    log.warn("SQL - buscarDadosGraficoJobs - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
	    float i = 1f;
	    while (rs.next()){
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("idJob", rs.getLong("idJob"));
		map.put("nomeJob", rs.getString("nomeJob"));
		Calendar c = new GregorianCalendar();
		c.setTime(new Date());
		String string[] = rs.getString("ntime").split(":");		
		c.set(Calendar.HOUR_OF_DAY, Integer.valueOf(string[0]));
		c.set(Calendar.MINUTE, Integer.valueOf(string[1]));
		map.put("ntime", c.getTimeInMillis());
		map.put("schedulerType", rs.getInt("schedulerType"));
		map.put("repeat", rs.getInt("repeat"));
		map.put("sequencia", i);
		i++;
		retorno.add(map);
	    }
	}catch (Exception e){
	    e.printStackTrace();
	}finally{
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}
	return tratarCoresJobsRetorno(retorno, "ntime");
    }
    
    public List<HashMap<String, Object>> buscarDadosMensalGraficoJobs(){
	Connection conn = null;
	PreparedStatement stmt = null;
	ResultSet rs = null;
	List<HashMap<String, Object>> retorno = new ArrayList<>();
	try{
	    conn = PersistEngine.getConnection("");
	    Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();

	    StringBuffer queryJobs = new StringBuffer();
	    
	    queryJobs.append(" SELECT job.neoId as idJob, ");
	    queryJobs.append(" job.name as nomeJob, ");
	    queryJobs.append(" S.schedulerType, ");
	    queryJobs.append(" S.repeat, ");
	    queryJobs.append(" S.dayOfMonth, ");
	    queryJobs.append(" convert(Char(5),cast(S.ntime as time),114) as ntime ");
	    queryJobs.append(" FROM NeoTrigger tri WITH (NOLOCK) ");
	    queryJobs.append(" INNER JOIN Scheduler S WITH(NOLOCK) ON tri.scheduler_neoId = S.neoId ");
	    queryJobs.append(" Inner join Job job With (NOLOCK) on job.neoId = tri.job_neoId ");
	    queryJobs.append(" WHERE TRI.enabled = 1 ");
	    queryJobs.append(" AND Job.enabled = 1 ");
	    queryJobs.append(" AND S.schedulerType = 5 ");
	    queryJobs.append(" order by ntime ");
	    
	    stmt = conn.prepareStatement(queryJobs.toString());
	    
	    rs = stmt.executeQuery();

	    log.warn("SQL - buscarDadosGraficoJobs - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");
	    float i = 1f;
	    while (rs.next()){
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("idJob", rs.getLong("idJob"));
		map.put("nomeJob", rs.getString("nomeJob"));
		Calendar c = new GregorianCalendar();
		c.setTime(new Date());
		String string[] = rs.getString("ntime").split(":");		
		c.set(Calendar.HOUR_OF_DAY, Integer.valueOf(string[0]));
		c.set(Calendar.MINUTE, Integer.valueOf(string[1]));
		map.put("ntime", c.getTimeInMillis());
		map.put("schedulerType", rs.getInt("schedulerType"));
		map.put("repeat", rs.getInt("repeat"));
		map.put("dayOfMonth", rs.getInt("dayOfMonth"));
		map.put("sequencia", i);
		i++;
		retorno.add(map);
	    }

	}catch (Exception e){
	    e.printStackTrace();
	}finally{
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	return tratarCoresJobsRetorno(retorno,"dayOfMonth");
    }
    
}
