package com.neomind.fusion.custom.orsegups.rsc;

import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.rsc.vo.RSCVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class RSCRegistroAtividadeNovaSolicitacao implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(RSCRegistroAtividadeNovaSolicitacao.class);

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);

		String mensagem = "Por favor, contatar o administrador do sistema!";
		WorkflowException exception = null;
		try
		{
			/* Início das funções úteis --> Funções necessárias para decorrer dos testes */

			//System.out.println(NeoDateUtils.safeDateFormat((GregorianCalendar) processEntity.findValue("prazoRegistrarRSC"), "dd/MM/yyyy HH:mm:ss"));

			/* Fim das funções úteis */

			RSCUtils rscUtils = new RSCUtils();
			GregorianCalendar prazoDeadLine = new GregorianCalendar();

			InstantiableEntityInfo registroAtividade = AdapterUtils.getInstantiableEntityInfo("RSCRegistroAtividade");
			NeoObject registro = registroAtividade.createNewInstance();
			EntityWrapper wRegistro = new EntityWrapper(registro);

			if (NeoUtils.safeIsNull(origin))
			{
				NeoPaper papel = new NeoPaper();
				papel = OrsegupsUtils.getPaper("sistemaFusionPaper");
				NeoUser usuarioResponsavel = new NeoUser();
				if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
				{
					for (NeoUser user : papel.getUsers())
					{
						usuarioResponsavel = user;
						break;
					}
				}

				wRegistro.findField("atividade").setValue("Registrar RSC");
				wRegistro.findField("descricao").setValue("");
				wRegistro.findField("responsavel").setValue(usuarioResponsavel);
				wRegistro.findField("dataInicialRSC").setValue(new GregorianCalendar());
				wRegistro.findField("dataFinalRSC").setValue(new GregorianCalendar());
			}
			else
			{
				wRegistro.findField("responsavel").setValue(origin.getUser());
				wRegistro.findField("dataInicialRSC").setValue(origin.getStartDate());
				wRegistro.findField("dataFinalRSC").setValue(origin.getFinishDate());
			}

			if (origin != null)
			{
				RSCVO rscDadosGerais = new RSCVO();
				rscDadosGerais.setTarefa((String) processEntity.findValue("wfprocess.code"));
				rscDadosGerais.setTipoSolicitacao((NeoObject) processEntity.findValue("tipoSolicitacao"));
				rscDadosGerais.setOrigemSolicitacao((String) processEntity.findValue("origem.descricao")); //Site Orsegups, SIGMA OS Pausada
				rscDadosGerais.setNome((String) processEntity.findValue("contato"));
				rscDadosGerais.setMensagem((String) processEntity.findValue("descricao"));
				rscDadosGerais.setEmail((String) processEntity.findValue("email"));
				rscDadosGerais.setTelefone((String) processEntity.findValue("telefones"));
				rscDadosGerais.setResponsavelAtendimento((NeoUser) processEntity.findValue("responsavelAtendimento"));
				
				String nomeClienteSapiens = (String) processEntity.findValue("clienteSapiens.nomcli"); 
				String nomeClienteFusion = (String) processEntity.findValue("clienteOrcamento");
				
				/* Histórico de Atividade do Fluxo C027 - RSC - Relatório de Solicitação de Cliente Novo */
				if (activity.getProcess().getModel().getName().equals("C027 - RSC - Relatório de Solicitação de Cliente Novo"))
				{
					if (origin.getActivityName().equalsIgnoreCase("Registrar RSC"))
					{
						String txtDescricao = (String) processEntity.findValue("obsSolicitacao");
						if (origin.getFinishByUser() == null)
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Registrar RSC - Escalada", (GregorianCalendar) processEntity.findValue("prazoRegistrarRSC"));							
							processEntity.findField("prazoRegistrarRSCEscalada").setValue(prazoDeadLine);
							
							String des = "RSC não registrada pelo colaborador " + origin.getUser().getFullName();
							wRegistro.findField("atividade").setValue("Registrar RSC");
							wRegistro.findField("descricao").setValue(des);
							wRegistro.findField("prazo").setValue(prazoDeadLine);
						}
						else
						{
							Boolean encaminharAoGestorRSC = (Boolean) processEntity.findValue("encaminharAoGestorDeRSC");
							if (encaminharAoGestorRSC)
							{
								prazoDeadLine = rscUtils.retornaPrazoDeadLine("Gestor de RSC", prazoDeadLine);
								processEntity.findField("prazoGestorRSC").setValue(prazoDeadLine);

								wRegistro.findField("atividade").setValue("Registrar RSC");
								wRegistro.findField("descricao").setValue(txtDescricao);
								wRegistro.findField("prazo").setValue(prazoDeadLine);
								
								if(!((String) processEntity.findValue("origem.descricao")).equalsIgnoreCase("Site Orsegups") && (NeoUtils.safeIsNotNull((String) processEntity.findValue("email"))))
								{
									if(!((String) processEntity.findValue("tipoSolicitacao.descricao")).equalsIgnoreCase("Cancelamento"))
									{
										rscUtils.enviaEmailRegistroRSC(rscDadosGerais);
									}
								}
							}
							else
							{
								wRegistro.findField("atividade").setValue("Registrar RSC");
								wRegistro.findField("descricao").setValue(txtDescricao);
								
								if(!((String) processEntity.findValue("origem.descricao")).equalsIgnoreCase("Site Orsegups") && (NeoUtils.safeIsNotNull((String) processEntity.findValue("email"))))
								{
									if(!((String) processEntity.findValue("tipoSolicitacao.descricao")).equalsIgnoreCase("Cancelamento"))
									{
										rscUtils.enviaEmailRegistroRSC(rscDadosGerais);
									}
									if(((String) processEntity.findValue("tipoSolicitacao.descricao")).equalsIgnoreCase("Orçamento"))
									{
										rscUtils.enviarEmailRSCOrcamentoExecutivo(rscDadosGerais, nomeClienteSapiens, nomeClienteFusion);
									}
								}
							}
						}
					}
					else if (origin.getActivityName().equalsIgnoreCase("Registrar RSC - Escalada"))
					{
						String txtDescricao = (String) processEntity.findValue("obsSolicitacao");
						if (origin.getFinishByUser() == null)
						{
							String des = "RSC não registrada pelo colaborador " + origin.getUser().getFullName();
							wRegistro.findField("atividade").setValue("Registrar RSC - Escalada");
							wRegistro.findField("descricao").setValue(des);

							String papel = (String) processEntity.findValue("responsavelExecutor.name");
							if (papel.contains("Diretor") || papel.contains("Presidente"))
							{
								prazoDeadLine = rscUtils.retornaPrazoDeadLine("Registrar RSC - Escalada Diretoria", (GregorianCalendar) processEntity.findValue("prazoRegistrarRSCEscalada"));
								processEntity.findField("prazoRegistrarRSCEscaladaDiretoria").setValue(prazoDeadLine);
								processEntity.findField("souPessoaResponsavel").setValue(false);
								processEntity.findField("continuarRSC").setValue(true);

								wRegistro.findField("prazo").setValue(prazoDeadLine);
								
								rscUtils.registrarTarefaSimplesRSCPrincipal(processEntity, activity);
							}
							else
							{
								prazoDeadLine = rscUtils.retornaPrazoDeadLine("Registrar RSC - Escalada", (GregorianCalendar) processEntity.findValue("prazoRegistrarRSCEscalada"));
								processEntity.findField("prazoRegistrarRSCEscalada").setValue(prazoDeadLine);

								wRegistro.findField("prazo").setValue(prazoDeadLine);
							}
						}
						else
						{
							Boolean encaminharAoGestorRSC = (Boolean) processEntity.findValue("encaminharAoGestorDeRSC");
							if (encaminharAoGestorRSC)
							{
								prazoDeadLine = rscUtils.retornaPrazoDeadLine("Gestor de RSC", prazoDeadLine);
								processEntity.findField("prazoGestorRSC").setValue(prazoDeadLine);

								wRegistro.findField("atividade").setValue("Registrar RSC");
								wRegistro.findField("descricao").setValue(txtDescricao);
								wRegistro.findField("prazo").setValue(prazoDeadLine);
								
								if(!((String) processEntity.findValue("origem.descricao")).equalsIgnoreCase("Site Orsegups") && (NeoUtils.safeIsNotNull((String) processEntity.findValue("email"))))
								{
									if(!((String) processEntity.findValue("tipoSolicitacao.descricao")).equalsIgnoreCase("Cancelamento"))
									{
										rscUtils.enviaEmailRegistroRSC(rscDadosGerais);
									}
								}
							}
							else
							{								
								wRegistro.findField("atividade").setValue("Registrar RSC - Escalada");
								wRegistro.findField("descricao").setValue(txtDescricao);
								
								if(!((String) processEntity.findValue("origem.descricao")).equalsIgnoreCase("Site Orsegups") && (NeoUtils.safeIsNotNull((String) processEntity.findValue("email"))))
								{
									if(!((String) processEntity.findValue("tipoSolicitacao.descricao")).equalsIgnoreCase("Cancelamento"))
									{
										rscUtils.enviaEmailRegistroRSC(rscDadosGerais);
									}																		
									if(((String) processEntity.findValue("tipoSolicitacao.descricao")).equalsIgnoreCase("Orçamento"))
									{
										rscUtils.enviarEmailRSCOrcamentoExecutivo(rscDadosGerais, nomeClienteSapiens, nomeClienteFusion);
									}
								}
							}
						}

						processEntity.setValue("obsSolicitacao", null);
					}
					else if (origin.getActivityName().equalsIgnoreCase("Registrar RSC - Escalada Diretoria"))
					{
						String txtDescricao = (String) processEntity.findValue("obsSolicitacao");
						if (origin.getFinishByUser() == null)
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Registrar RSC - Escalada Presidência", (GregorianCalendar) processEntity.findValue("prazoRegistrarRSCEscaladaDiretoria"));
							processEntity.findField("prazoRegistrarRSCEscaladaPresidencia").setValue(prazoDeadLine);							
							
							String des = "RSC não registrada pelo colaborador " + origin.getUser().getFullName();
							wRegistro.findField("atividade").setValue("Registrar RSC - Escalada Diretoria");
							wRegistro.findField("descricao").setValue(des);
							wRegistro.findField("prazo").setValue(prazoDeadLine);
							
							rscUtils.registrarTarefaSimplesRSCPrincipal(processEntity, activity);
						}
						else
						{							
							wRegistro.findField("atividade").setValue("Registrar RSC - Escalada Diretoria");
							wRegistro.findField("descricao").setValue(txtDescricao);
							
							if(!((String) processEntity.findValue("origem.descricao")).equalsIgnoreCase("Site Orsegups") && (NeoUtils.safeIsNotNull((String) processEntity.findValue("email"))))
							{
								if(!((String) processEntity.findValue("tipoSolicitacao.descricao")).equalsIgnoreCase("Cancelamento"))
								{
									rscUtils.enviaEmailRegistroRSC(rscDadosGerais);	
								}																	
								if(((String) processEntity.findValue("tipoSolicitacao.descricao")).equalsIgnoreCase("Orçamento"))
								{
									rscUtils.enviarEmailRSCOrcamentoExecutivo(rscDadosGerais, nomeClienteSapiens, nomeClienteFusion);
								}
							}
						}

						processEntity.setValue("obsSolicitacao", null);
					}
					else if (origin.getActivityName().equalsIgnoreCase("Registrar RSC - Escalada Presidência"))
					{
						String txtDescricao = (String) processEntity.findValue("obsSolicitacao");

						wRegistro.findField("atividade").setValue("Registrar RSC - Escalada Presidência");
						wRegistro.findField("descricao").setValue(txtDescricao);
						
						if(!((String) processEntity.findValue("origem.descricao")).equalsIgnoreCase("Site Orsegups") && (NeoUtils.safeIsNotNull((String) processEntity.findValue("email"))))
						{
							if(!((String) processEntity.findValue("tipoSolicitacao.descricao")).equalsIgnoreCase("Cancelamento"))
							{
								rscUtils.enviaEmailRegistroRSC(rscDadosGerais);
							}								
							if(((String) processEntity.findValue("tipoSolicitacao.descricao")).equalsIgnoreCase("Orçamento"))
							{
								rscUtils.enviarEmailRSCOrcamentoExecutivo(rscDadosGerais, nomeClienteSapiens, nomeClienteFusion);
							}
						}
					}
					else if (origin.getActivityName().equalsIgnoreCase("Gestor de RSC"))
					{
						String txtDescricao = (String) processEntity.findValue("obsSolicitacao");
						if (origin.getFinishByUser() == null)
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Supervisão do Gestor de RSC - Escalada", (GregorianCalendar) processEntity.findValue("prazoGestorRSC"));
							processEntity.findField("prazoSupervisaoGestorRSCEscalada").setValue(prazoDeadLine);
							
							String des = "RSC não encaminhada para a atividade de atendimento da solitacação pelo colaborador " + origin.getUser().getFullName();
							wRegistro.findField("atividade").setValue("Gestor de RSC");
							wRegistro.findField("descricao").setValue(des);
							wRegistro.findField("prazo").setValue(prazoDeadLine);
						}
						else
						{														
							wRegistro.findField("atividade").setValue("Gestor de RSC");
							wRegistro.findField("descricao").setValue(txtDescricao);
							
							if(!((String) processEntity.findValue("origem.descricao")).equalsIgnoreCase("Site Orsegups") && (NeoUtils.safeIsNotNull((String) processEntity.findValue("email"))))
							{
								if(((String) processEntity.findValue("tipoSolicitacao.descricao")).equalsIgnoreCase("Orçamento"))
								{
									rscUtils.enviarEmailRSCOrcamentoExecutivo(rscDadosGerais, nomeClienteSapiens, nomeClienteFusion);
								}
							}
						}

						processEntity.setValue("obsSolicitacao", null);
					}
					else if (origin.getActivityName().equalsIgnoreCase("Supervisão do Gestor de RSC - Escalada"))
					{
						String txtDescricao = (String) processEntity.findValue("obsSolicitacao");
						if (origin.getFinishByUser() == null)
						{
							String des = "RSC não encaminhada para a atividade de atendimento da solitacação pelo colaborador " + origin.getUser().getFullName();
							wRegistro.findField("atividade").setValue("Supervisão do Gestor de RSC - Escalada");
							wRegistro.findField("descricao").setValue(des);

							String papel = (String) processEntity.findValue("responsavelExecutor.name");
							if (!papel.contains("Supervisor"))
							{
								prazoDeadLine = rscUtils.retornaPrazoDeadLine("Supervisão do Gestor de RSC - Escalada", (GregorianCalendar) processEntity.findValue("prazoSupervisaoGestorRSCEscalada"));
								processEntity.findField("prazoSupervisaoGestorRSCEscalada").setValue(prazoDeadLine);
								wRegistro.findField("prazo").setValue(prazoDeadLine);
							}
							
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Gerência do Gestor de RSC - Escalada", (GregorianCalendar) processEntity.findValue("prazoSupervisaoGestorRSCEscalada"));
							processEntity.findField("prazoGerenciaGestorRSCEscalada").setValue(prazoDeadLine);
							wRegistro.findField("prazo").setValue(prazoDeadLine);
						}
						else
						{							
							wRegistro.findField("atividade").setValue("Supervisão do Gestor de RSC - Escalada");
							wRegistro.findField("descricao").setValue(txtDescricao);
							
							if(!((String) processEntity.findValue("origem.descricao")).equalsIgnoreCase("Site Orsegups") && (NeoUtils.safeIsNotNull((String) processEntity.findValue("email"))))
							{
								if(((String) processEntity.findValue("tipoSolicitacao.descricao")).equalsIgnoreCase("Orçamento"))
								{
									rscUtils.enviarEmailRSCOrcamentoExecutivo(rscDadosGerais, nomeClienteSapiens, nomeClienteFusion);
								}
							}
						}

						processEntity.setValue("obsSolicitacao", null);
					}
					else if (origin.getActivityName().equalsIgnoreCase("Gerência do Gestor de RSC - Escalada"))
					{						
						String txtDescricao = (String) processEntity.findValue("obsSolicitacao");

						wRegistro.findField("atividade").setValue("Gerência do Gestor de RSC - Escalada");
						wRegistro.findField("descricao").setValue(txtDescricao);
						
						if(!((String) processEntity.findValue("origem.descricao")).equalsIgnoreCase("Site Orsegups") && (NeoUtils.safeIsNotNull((String) processEntity.findValue("email"))))
						{
							if(((String) processEntity.findValue("tipoSolicitacao.descricao")).equalsIgnoreCase("Orçamento"))
							{
								rscUtils.enviarEmailRSCOrcamentoExecutivo(rscDadosGerais, nomeClienteSapiens, nomeClienteFusion);
							}
						}

						processEntity.setValue("obsSolicitacao", null);
					}					
				}
			}
			PersistEngine.persist(registro);
			processEntity.findField("registroAtividades").addValue(registro);
		}
		catch (Exception e)
		{
			log.error(mensagem);
			e.printStackTrace();
			throw new WorkflowException(mensagem);
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}
}
