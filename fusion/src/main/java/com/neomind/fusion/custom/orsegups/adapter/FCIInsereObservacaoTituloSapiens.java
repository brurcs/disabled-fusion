package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class FCIInsereObservacaoTituloSapiens implements AdapterInterface
{

	private static final Log log = LogFactory.getLog(FCIInsereObservacaoTituloSapiens.class);
	private static final FCIInsereObservacaoTituloSapiens lock = new FCIInsereObservacaoTituloSapiens();

	public void back(EntityWrapper entitywrapper, Activity activity1)
	{
	}

	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		
		List<NeoObject> listaObs = (List<NeoObject>) processEntity.findField("historicoChamadas").getValue();
		List<NeoObject> listaTitulos = (List<NeoObject>) processEntity.findField("listaTitulos").getValue();
		
		if (listaObs != null && !listaObs.isEmpty() && listaTitulos != null && !listaTitulos.isEmpty()) {
			
			for (NeoObject obs : listaObs) {
				EntityWrapper wrapper = new EntityWrapper(obs);
				if (wrapper != null) {
					try	{
						GregorianCalendar dataHora = (GregorianCalendar) wrapper.findValue("data");
						GregorianCalendar data = new GregorianCalendar( dataHora.get(GregorianCalendar.YEAR), dataHora.get(GregorianCalendar.MONTH), dataHora.get(GregorianCalendar.DAY_OF_MONTH));
						String texto = (String) wrapper.findValue("chamada");
						Long codigoUsuarioSapiens = buscaUsuarioSapiensCorrespondente(((NeoUser)wrapper.findValue("responsavel")).getCode());
						Long ocorrencia = (Long)wrapper.findValue("tipoOcorrencia.usu_codocr");
						Boolean integradoSapiens = (Boolean) wrapper.findValue("integradoSapiens");
					
						if (!integradoSapiens) {
							System.out.println("Texto Que irá para o Sapiens: Texto: "+texto+", Código Usuário: "+codigoUsuarioSapiens+", Ocorrencia: "+ocorrencia+", Data: "+data+", Data e Hora: "+dataHora);
							insereObservacaoTituloSapiens(listaTitulos, data, dataHora, texto, codigoUsuarioSapiens, ocorrencia);
							wrapper.setValue("integradoSapiens", true);
						}

					} catch (WorkflowException we)	{
						System.out.println("Contrato que apresentou problema no método start. Classe FCIInsereObservacaoTituloSapiens:");
						throw we;
					} catch (Exception e) {
						e.printStackTrace();
						throw new WorkflowException(e.getMessage());
					}						
				}
			}
		}
			
	}

	@SuppressWarnings("unchecked")
	public void insereObservacaoTituloSapiens(List<NeoObject> listaTitulos, GregorianCalendar data, GregorianCalendar dataHora, String texto, Long usuario, Long ocorrencia) throws Exception
	{
		synchronized (lock)
		{
			for (NeoObject titulo : listaTitulos) {
				EntityWrapper wrapper = new EntityWrapper(titulo);
				Long empresa = (Long) wrapper.findValue("empresa");
				Long filial = (Long) wrapper.findValue("filial");
				String numeroTitulo = (String) wrapper.findValue("numeroTitulo");
				String tipoTitulo = (String) wrapper.findValue("tipoTitulo");
				Integer sequencial = 1;
				
				String nomeFonteDados = "SAPIENS";
				
				String sqlSelect = "SELECT (CASE WHEN max(seqmov) > 0 THEN MAX(seqmov)+1 ELSE 1 END) FROM E301MOR WHERE codemp = :codemp AND codfil = :codfil AND numtit = :numtit AND codtpt = :codtpt ";
				
				try{
					Query query = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sqlSelect);
					query.setParameter("codemp", empresa);
					query.setParameter("codfil", filial);
					query.setParameter("numtit", numeroTitulo);
					query.setParameter("codtpt", tipoTitulo);
					List<Object> resultList = query.getResultList();
					
					if(resultList != null && !resultList.isEmpty())	{
						sequencial = (Integer)resultList.get(0);
					}
				
				}
				catch (Exception ex)
				{
					System.out.println("Contrato que apresentou problema no Exception do Select. Classe FCIInsereObservacaoTituloSapiens no método insereObservacaoTituloSapiens: "+numeroTitulo);
					throw new WorkflowException("Sistema Sapiens indisponível para inserir observação no título, tente novamente mais tarde!");
				}
				
				Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
				
				String sql = "INSERT INTO E301MOR (CodEmp, CodFil, NumTit, CodTpt, SeqMov, TipObs, ObsTit, UsuMov, DatMov, HorMov, USU_CodOcr) ";
				sql += "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, (DATEPART(HOUR, ?) * 60) + DATEPART(MINUTE, ?), ?) ";
				PreparedStatement st = null;
				try
				{	
					st = connection.prepareStatement(sql);
					st.setLong(1, empresa);
					st.setLong(2, filial);
					st.setString(3, numeroTitulo);
					st.setString(4, tipoTitulo);
					st.setInt(5, sequencial);
					st.setString(6, "M");
					st.setString(7, texto +" (Fusion)");
					st.setLong(8, usuario);
					st.setTimestamp(9, new Timestamp(data.getTimeInMillis()));
					st.setTimestamp(10, new Timestamp(dataHora.getTimeInMillis()));
					st.setTimestamp(11, new Timestamp(dataHora.getTimeInMillis()));
					st.setLong(12, ocorrencia);
					
					System.out.println("Contrato que apresentou problema antes de executar o st.executeUpdate(). Classe FCIInsereObservacaoTituloSapiens no método insereObservacaoTituloSapiens: "+numeroTitulo);
					st.executeUpdate();
					System.out.println("Texto Que Foi para o Sapiens: Texto: "+texto+", Ocorrencia: "+ocorrencia+", Data: "+data+", Data e Hora: "+dataHora);
				}
				catch (SQLException e)
				{
					System.out.println("Contrato que apresentou problema no SQLException. Classe FCIInsereObservacaoTituloSapiens no método insereObservacaoTituloSapiens: "+numeroTitulo);
					throw new WorkflowException("Sistema Sapiens indisponível para inserir observação no título, tente novamente mais tarde!");
				}
				catch (Exception ex)
				{
					System.out.println("Contrato que apresentou problema no Exception. Classe FCIInsereObservacaoTituloSapiens no método insereObservacaoTituloSapiens: :  "+numeroTitulo);
					throw new WorkflowException("Sistema Sapiens indisponível para inserir observação no título, tente novamente mais tarde!");
				}
				finally
				{
					if (st != null)
					{
						try
						{
							st.close();
							connection.close();
						}
						catch (SQLException e)
						{
							log.error("Erro ao fechar o statement");
							e.printStackTrace();
							throw e;
						}
					}
				}
	
			}
		}

	}

	private Long buscaUsuarioSapiensCorrespondente(String userCode) throws Exception
	{
		//Logica para buscar codigo do usuario do sapiens referente ao usuario fusion
		Long sapiensUserCode = 0L;
		NeoObject sapiensUser = null;
		InstantiableEntityInfo ieiEX = AdapterUtils.getInstantiableEntityInfo("SAPIENSUSUARIO");
		String login = userCode;

		QLEqualsFilter loginFilter = new QLEqualsFilter("nomusu", login);
		@SuppressWarnings("unchecked")
		ArrayList<NeoObject> sapiensUsers = (ArrayList<NeoObject>) PersistEngine.getObjects(ieiEX.getEntityClass(), loginFilter);

		if (sapiensUsers != null && sapiensUsers.size() > 0)
		{
			sapiensUser = sapiensUsers.get(0);
		}

		if (sapiensUser != null)
		{
			EntityWrapper sapiensUserEw = new EntityWrapper(sapiensUser);
			sapiensUserCode = (Long)sapiensUserEw.findValue("codusu");
		}

		if (sapiensUserCode == null || sapiensUserCode == 0L)
		{
			log.error("Não foi possivel buscar o codigo de usuario sapiens referente ao usuário logado no Fusion - " + userCode);
			throw new WorkflowException("Não foi possivel buscar o codigo de usuario sapiens referente ao usuário logado no Fusion.");
		}

		return sapiensUserCode;
	}

}
