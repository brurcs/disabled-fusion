package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoUtils;

public class CancelaTarefasCobranca implements CustomJobAdapter
{
	@SuppressWarnings("unused")
	private static final Log log = LogFactory.getLog(CancelaTarefasCobranca.class);

	@Override
	public void execute(CustomJobContext arg0)
	{
		Connection conn = PersistEngine.getConnection("FUSIONDEV");
		StringBuilder sqlCancelaHumana = new StringBuilder();
		StringBuilder sqlCancelaEletronica = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.CancelaTarefasCobranca");
		log.warn("##### INICIO AGENDADOR DE TAREFA: Cancela Tarefas Cobrança - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		try
		{
			sqlCancelaHumana.append(" SELECT CODE FROM D_FCIHUMANAINADIMPLENCIA H ");
			sqlCancelaHumana.append(" JOIN WFPROCESS WF ON H.WFPROCESS_NEOID = WF.NEOID ");
			sqlCancelaHumana.append(" LEFT JOIN D_FCIHUMANAINADIMPLENCIA_LISTATITULOS LT ON H.NEOID = LT.D_FCIHUMANAINADIMPLENCIA_NEOID ");
			sqlCancelaHumana.append(" WHERE LT.D_FCIHUMANAINADIMPLENCIA_NEOID IS NULL ");
			sqlCancelaHumana.append(" AND WF.PROCESSSTATE = 0 ");
			pstm = conn.prepareStatement(sqlCancelaHumana.toString());
			rs = pstm.executeQuery();

			while (rs.next())
			{
				String code = rs.getString("CODE");

				String urlTarefa = "";
				urlTarefa = OrsegupsUtils.URL_HOMOLOGACAO + "/fusion/custom/jsp/orsegups/cancelProcess.jsp?codigoCancelamento=cancelaHumanaInadimplencia&codigoProcesso="+code;

				URL url = new URL(urlTarefa);
				URLConnection uc = url.openConnection();
				BufferedReader leitorArquivo = new BufferedReader(new InputStreamReader(uc.getInputStream()));

				Thread.sleep(10000);
			}

			sqlCancelaEletronica.append(" SELECT CODE FROM D_FCICOBRANCAINADIMPLENCIA H ");
			sqlCancelaEletronica.append(" JOIN WFPROCESS WF ON H.WFPROCESS_NEOID = WF.NEOID ");
			sqlCancelaEletronica.append(" LEFT JOIN D_FCICOBRANCAINADIMPLENCIA_LISTATITULOS LT ON H.NEOID = LT.D_FCICOBRANCAINADIMPLENCIA_NEOID ");
			sqlCancelaEletronica.append(" WHERE LT.D_FCICOBRANCAINADIMPLENCIA_NEOID IS NULL ");
			sqlCancelaEletronica.append(" AND WF.PROCESSSTATE = 0  ");
			
			pstm = conn.prepareStatement(sqlCancelaEletronica.toString());
			rs = null;
			rs = pstm.executeQuery();

			while (rs.next())
			{
				String code = rs.getString("CODE");

				String urlTarefa = "";
				urlTarefa = OrsegupsUtils.URL_HOMOLOGACAO + "/fusion/custom/jsp/orsegups/cancelProcess.jsp?codigoCancelamento=cancelaCobrancaInadimplencia&codigoProcesso="+code;

				URL url = new URL(urlTarefa);
				URLConnection uc = url.openConnection();
				BufferedReader leitorArquivo = new BufferedReader(new InputStreamReader(uc.getInputStream()));

				Thread.sleep(10000);
			}
		}
		catch (Exception e)
		{

			log.error("##### AGENDADOR DE TAREFA: Cancela Tarefas Cobrança");
			e.printStackTrace();
		}
		finally
		{

			try
			{
				rs.close();
				pstm.close();
				conn.close();
			}
			catch (SQLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			log.warn("##### FIM AGENDADOR DE TAREFA: Cancela Tarefas Cobrança - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}
}
