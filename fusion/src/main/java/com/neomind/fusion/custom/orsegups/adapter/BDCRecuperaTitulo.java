package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class BDCRecuperaTitulo implements AdapterInterface
{
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{
			Long codigoEmpresa = (Long) processEntity.findValue("nfSapiens.codemp");
			Long codigoFilial = (Long) processEntity.findValue("nfSapiens.codfil");
			Long numeroNfv = (Long) processEntity.findValue("nfSapiens.numnfv");
			String codigoSerie = (String) processEntity.findValue("nfSapiens.codsnf");
			
			QLEqualsFilter filtroEmpresa = new QLEqualsFilter("codemp",codigoEmpresa);
			QLEqualsFilter filtroFilial = new QLEqualsFilter("codfil", codigoFilial);
			QLEqualsFilter filtroNfv = new QLEqualsFilter("numnfv", numeroNfv);
			QLEqualsFilter filtroSerie = new QLEqualsFilter("codsnf", codigoSerie);
			
			QLGroupFilter filtroTitulo = new QLGroupFilter("AND");
			filtroTitulo.addFilter(filtroEmpresa);
			filtroTitulo.addFilter(filtroFilial);
			filtroTitulo.addFilter(filtroNfv);
			filtroTitulo.addFilter(filtroSerie);
			
			List<NeoObject> titulos = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSETCR"),filtroTitulo);
			
			if (titulos != null && !titulos.isEmpty())
			{
				processEntity.setValue("tituloSapiens", titulos.get(0));
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException(e.getMessage());
		}
	}
}