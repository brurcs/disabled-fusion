package com.neomind.fusion.custom.orsegups.adapter;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;


/**
 * Regras de Negócio:
 * 1. Quando o executorAtual for o Presidente, não há mais níveis de escalonamento. O Executor será sempre o mesmo.
 * 2. Quando o executorAtual for o responsável pelo próprio grupo, sobe um nível hierarquico de grupos e pega o responsável.
 * 3. Quando o executorAtual não for o responsável pelo próprio grupo, pega o responsável do grupo
 * 4. Quando o papel de responsável estiver vazio, o executor será sempre o executor anterior.
 * 
 * @author herisson.ferreira
 * 
 * */
public class RMCEscalarAuditarNaoEnvio implements AdapterInterface {

    @Override
    public void start(Task origin, EntityWrapper processEntity, Activity activity) {

	origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);

	String msgErro = null;

	try {	

	    NeoUser executorAtual = origin.getUser();
	    NeoPaper responsavelGrupoExecutorAtual = executorAtual.getGroup().getResponsible(); 

	    NeoUser novoExecutor = null;
	    NeoPaper papelNovoExecutor = null;	

	    // Quando o executorAtual for o(a) Presidente, não há mais níveis de escalonamento. 
	    if (executorAtual.getPapers().contains("Presidente")){ 
		novoExecutor = executorAtual;
	    } else if (executorAtual.getPapers().contains(responsavelGrupoExecutorAtual)){ // Caso o usuário seja o responsável do seu grupo, pega o responsável do nível superior		    
		papelNovoExecutor = executorAtual.getGroup().getUpperLevel().getResponsible();
		novoExecutor = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelNovoExecutor);	
	    } else {
		papelNovoExecutor = executorAtual.getGroup().getResponsible();
		novoExecutor = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelNovoExecutor);
	    }

	    if (novoExecutor == null){
		novoExecutor = executorAtual;
	    }

	    processEntity.setValue("responsavelAuditarNaoEnvio", novoExecutor); 

	}
	catch(Exception e)
	{
	    e.printStackTrace();
	    throw new WorkflowException("Erro na classe: "+ this.getClass() +". Verifique junto ao setor de T.I");
	}

    }

    @Override
    public void back(EntityWrapper processEntity, Activity activity) {
	// TODO Auto-generated method stub

    }

}
