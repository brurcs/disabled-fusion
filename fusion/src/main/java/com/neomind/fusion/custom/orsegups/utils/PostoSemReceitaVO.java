package com.neomind.fusion.custom.orsegups.utils;

import java.math.BigDecimal;
import java.util.GregorianCalendar;

public class PostoSemReceitaVO
{

	private String competencia;
	private String tipoMercado;
	private int codigoEmpresa;
	private String nomeEmpresa;
	private String nomeRegional;
	private int codigoCliente;
	private String nomeCliente;
	private String nomeLotacao;
	private String nomePosto;
	private BigDecimal valorReceita;
	private BigDecimal valorCusto;
	private long regionalPosto;
	private long codigoNegocio;

	public long getRegionalPosto()
	{
		return regionalPosto;
	}

	public void setRegionalPosto(long regionalPosto)
	{
		this.regionalPosto = regionalPosto;
	}

	public String getCompetencia()
	{
		return competencia;
	}

	public void setCompetencia(String competencia)
	{
		this.competencia = competencia;
	}

	public String getTipoMercado()
	{
		return tipoMercado;
	}

	public void setTipoMercado(String tipoMercado)
	{
		this.tipoMercado = tipoMercado;
	}

	public String getNomeEmpresa()
	{
		return nomeEmpresa;
	}

	public void setNomeEmpresa(String nomeEmpresa)
	{
		this.nomeEmpresa = nomeEmpresa;
	}

	public String getNomeRegional()
	{
		return nomeRegional;
	}

	public void setNomeRegional(String nomeRegional)
	{
		this.nomeRegional = nomeRegional;
	}

	public String getNomeCliente()
	{
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente)
	{
		this.nomeCliente = nomeCliente;
	}

	public String getNomeLotacao()
	{
		return nomeLotacao;
	}

	public void setNomeLotacao(String nomeLotacao)
	{
		this.nomeLotacao = nomeLotacao;
	}

	public String getNomePosto()
	{
		return nomePosto;
	}

	public void setNomePosto(String nomePosto)
	{
		this.nomePosto = nomePosto;
	}

	public BigDecimal getValorReceita()
	{
		return valorReceita;
	}

	public void setValorReceita(BigDecimal valorReceita)
	{
		this.valorReceita = valorReceita;
	}

	public BigDecimal getValorCusto()
	{
		return valorCusto;
	}

	public void setValorCusto(BigDecimal valorCusto)
	{
		this.valorCusto = valorCusto;
	}

	public int getCodigoCliente()
	{
		return codigoCliente;
	}

	public void setCodigoCliente(int codigoCliente)
	{
		this.codigoCliente = codigoCliente;
	}
	public int getCodigoEmpresa()
	{
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(int codigoEmpresa)
	{
		this.codigoEmpresa = codigoEmpresa;
	}

	public long getCodigoNegocio()
	{
		return codigoNegocio;
	}

	public void setCodigoNegocio(long codigoNegocio)
	{
		this.codigoNegocio = codigoNegocio;
	}
}
