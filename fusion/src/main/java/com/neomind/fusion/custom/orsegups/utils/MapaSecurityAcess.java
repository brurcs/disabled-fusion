package com.neomind.fusion.custom.orsegups.utils;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;


public class MapaSecurityAcess {

	public static boolean verificaAcessoUrl(String remoteIP, String liberaAcesso){

		boolean acessoLiberado = false;
		NeoUser user = PortalUtil.getCurrentUser();


		// 1 - Verifica parametro que libera acesso ao mapa sem validação.

		if(liberaAcesso != null && liberaAcesso.equalsIgnoreCase("true")){
			return true;
		}

		// 2 - Valida se existe usuário logado no Fusion, se existir verificar se está na lista de acesso liberado

		List<NeoObject> cadastrosAcesso = PersistEngine.getObjects(AdapterUtils.getEntityClass("cadastroAcessoMapa"));
		if(NeoUtils.safeIsNotNull(cadastrosAcesso) && cadastrosAcesso.size() > 0)
		{
			NeoObject cadAcesso = cadastrosAcesso.get(0);
			EntityWrapper cadastroAcesso = new EntityWrapper(cadAcesso);

			List<NeoUser> usuariosFusion = (List<NeoUser>) cadastroAcesso.findField("usuarioFusion").getValues();
			for(NeoUser usuarioFusion : usuariosFusion){
				if(usuarioFusion.equals(user)){
					return true;
				}
			}
		}

		// 3 - Valida se o usuário está dentro da rede interna.
		// 127.0.0.1 e 0:0:0:0:0:0:0:1 (ipv6) - localhost

		if ((user != null) || ( (remoteIP.matches("0:0:0:0:0:0:0:1")) ||
				(remoteIP.matches("127.0.0.1")) ||
				(remoteIP.matches("192\\.168\\.[1-5]\\d\\.\\d{1,3}")) ||
				(remoteIP.matches("201\\.47\\.43\\.1\\d{2}")))) 
		{
			return true;
		}else{
			return false;
		}

		// 4 - Valida acesso ao mapa pelo applet

	}

	public static boolean verificaDadosCadastradosPC(String parametros){

		String[] param = parametros.split(";");

		String nomeSO      = param[0];
		String nomeUsuario = param[1];
		String nomePC      = param[2];

		List<NeoObject> cadastroMapa = PersistEngine.getObjects(AdapterUtils.getEntityClass("cadastroAcessoMapa"));
		if(cadastroMapa != null && cadastroMapa.size() > 0){
			NeoObject cadastroMapaPC = cadastroMapa.get(0);
			EntityWrapper ewCadastroMapaPC = new EntityWrapper(cadastroMapaPC);
			List<NeoObject> listaPcCadastrado = (List<NeoObject>) ewCadastroMapaPC.findField("cadastroPC").getValues();
			for(NeoObject pcCadastrado : listaPcCadastrado){
				EntityWrapper ewInformacaoPc = new EntityWrapper(pcCadastrado);

				String sistemaOperacional = (String) ewInformacaoPc.findField("sistemaOperacional").getValue();
				String nomeComputador = (String) ewInformacaoPc.findField("nomeComputador").getValue();
				String usuarioLogado = (String) ewInformacaoPc.findField("usuarioLogadoWin").getValue();
				Boolean acessoLiberado = (Boolean) ewInformacaoPc.findField("acessoLiberado").getValue();

				if(sistemaOperacional.equals(nomeSO) && nomeComputador.equals(nomePC) && usuarioLogado.equals(nomeUsuario) && acessoLiberado ){
					//setar data do ultimo acesso
					//Caso tenha o PC cadastrado libera o acesso	
					ewInformacaoPc.findField("dataUltimoAcesso").setValue(new GregorianCalendar());
					PersistEngine.persist(pcCadastrado);
					return true;
				}
			}
		}

		//Verifica se o cadastro não está bloqueado
		QLGroupFilter groupFilter2 = new QLGroupFilter("AND");
		groupFilter2.addFilter(new QLEqualsFilter("sistemaOperacional", nomeSO));
		groupFilter2.addFilter(new QLEqualsFilter("nomeComputador", nomePC));
		groupFilter2.addFilter(new QLEqualsFilter("usuarioLogadoWin", nomeUsuario));
		groupFilter2.addFilter(new QLEqualsFilter("acessoLiberado", false));

		List<NeoObject> listInfoPCnaoLiberado = PersistEngine.getObjects(AdapterUtils.getEntityClass("cadastroInformacoesPC"), groupFilter2);
		if(listInfoPCnaoLiberado != null && listInfoPCnaoLiberado.size() > 0){
			return false;

		}else{
			//Se não tem cadastro, será cadastrado mas com acesso bloqueado

			QLGroupFilter groupFilter = new QLGroupFilter("AND");
			groupFilter.addFilter(new QLEqualsFilter("sistemaOperacional", nomeSO));
			groupFilter.addFilter(new QLEqualsFilter("nomeComputador", nomePC));
			groupFilter.addFilter(new QLEqualsFilter("usuarioLogadoWin", nomeUsuario));

			NeoObject usuarioCadastrado = PersistEngine.getObject(AdapterUtils.getEntityClass("cadastroInformacoesPC"), groupFilter );
			if(usuarioCadastrado == null){


				InstantiableEntityInfo cadastroPc = AdapterUtils.getInstantiableEntityInfo("cadastroInformacoesPC");

				NeoObject novoCadastroPC = cadastroPc.createNewInstance();
				EntityWrapper ewNovoCadastroPC = new EntityWrapper(novoCadastroPC);

				ewNovoCadastroPC.setValue("sistemaOperacional", nomeSO);
				ewNovoCadastroPC.setValue("nomeComputador", nomePC);
				ewNovoCadastroPC.setValue("usuarioLogadoWin", nomeUsuario);
				ewNovoCadastroPC.setValue("dataCadastro", nomeUsuario);
				ewNovoCadastroPC.setValue("acessoLiberado", false);
				ewNovoCadastroPC.setValue("dataCadastro", new GregorianCalendar());
				PersistEngine.persist(novoCadastroPC);

				List<NeoObject> listCadastroAcessoMapa = PersistEngine.getObjects(AdapterUtils.getEntityClass("cadastroAcessoMapa"));
				if(NeoUtils.safeIsNotNull(listCadastroAcessoMapa) && listCadastroAcessoMapa.size() > 0){
					NeoObject cadastroAcessoMapa = listCadastroAcessoMapa.get(0);
					EntityWrapper ewCadastroAcessoMapa = new EntityWrapper(cadastroAcessoMapa);
					ewCadastroAcessoMapa.findField("cadastroPC").addValue(novoCadastroPC);
					PersistEngine.persist(cadastroAcessoMapa);
				}
				return false;
			}else{
				return false;
			}
		}
	}
}
