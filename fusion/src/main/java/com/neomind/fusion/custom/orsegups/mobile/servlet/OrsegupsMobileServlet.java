package com.neomind.fusion.custom.orsegups.mobile.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.mobile.engine.SaveInspection;
import com.neomind.fusion.custom.orsegups.mobile.engine.SendReinspection;
import com.neomind.fusion.custom.orsegups.mobile.vo.Inspecao;

/**
 * Classe que controla a comunica��o entre o Fusion e o Mobile da Casvig usado
 * nas inspetorias de qualidade
 * 
 * @author Daniel Henrique Joppi
 * 
 */

@WebServlet(name="OrsegupsMobileServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.mobile.servlet.OrsegupsMobileServlet"})
public class OrsegupsMobileServlet extends HttpServlet
{
	public static final Log log = LogFactory.getLog(OrsegupsMobileServlet.class);

	public static OrsegupsMobileServlet getInstance()
	{
		return new OrsegupsMobileServlet();
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doGet(request, response);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		String function = request.getParameter("action");
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
	
		final PrintWriter out = response.getWriter();
		

		// checar se login � v�lido
		if (function.equals("login"))
		{
			//ActionLogin.getInstance().doGet(request, response);
		}
		// enviar/atualizar xml de clientes para o mobile
		else if (function.equals("updateClient"))
		{
			//	ActionUpdateClient.getInstance().doGet(request, response);
		}
		// enviar/atualizar xml de pesquisa para o mobile
		else if (function.equals("updateResearch"))
		{
			//	ActionUpdateResearch.getInstance().doGet(request, response);
		}
		// enviar reinspecoes para mobile
		else if (function.equals("sendReinspection"))
		{
			SendReinspection send = new SendReinspection();
			List<Inspecao> lista = send.buildReinspection(request, response);
			//response.setCharacterEncoding("UTF-8");
			Gson g = new GsonBuilder().disableHtmlEscaping().create();
			System.out.println("["+key+"][INSP] - Enviando reinspeções para o mobile [com.neomind.fusion.custom.orsegups.mobile.servlet-OrsegupsMobileServlet]: " + g.toJson(lista));
			out.append(g.toJson(lista));

		}
		// receber as inspe��es do mobile
		else if (function.equals("inspecao"))
		{
			Gson gson = new Gson();
			System.out.println("["+key+"][INSP] - recebendo reinspeção do mobile[com.neomind.fusion.custom.orsegups.mobile.servlet-OrsegupsMobileServlet]: " + request.getParameter("inspecao"));
			Inspecao insp = gson.fromJson(request.getParameter("inspecao"), Inspecao.class);
			System.out.println("token:" + request.getParameter("token"));
			
			String strToken = request.getParameter("token");
			System.out.println("["+key+"][INSP] - token:" + strToken);
			
			
			try
			{
				if ("".equals(strToken) || strToken == null){
					throw new Exception("Token invalido ["+strToken+"], faça um logout e login.");
				}else{
					insp.setToken(strToken);
				}

				SaveInspection s = new SaveInspection();
				List<NeoObject> saveInspection = s.saveInspection(insp);

				if (saveInspection != null && !saveInspection.isEmpty())
				{
					System.out.println("["+key+"][INSP] - [re]inspeção salvo com sucesso");
					out.print("OK");
				}
				else
				{
					System.out.println("["+key+"][INSP] - [re]inspeção Não foi salvo");
					out.print("NOK - Erro: checklist não pode salvo");
				}
				System.out.println("["+key+"][INSP] - [re]terminou try sem erros");
			}
			catch (Exception e)
			{
				System.out.println("["+key+"][INSP] - Erro ao receber [re]inspeção:" + e.getMessage());
				e.printStackTrace();
				if (strToken == null){
					out.print("NOK - Erro: Login expirado, faça um logout e um login  e tente novamente."  );
				}else{
					out.print("NOK - Erro:"+e.getMessage());
				}
				

			}
		}
	}
}