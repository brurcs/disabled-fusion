package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Time;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.drools.lang.DRLExpressions.neg_operator_key_return;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.eform.EForm;
import com.neomind.fusion.entity.EntityAdapter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class RMCHistoricoLigacao implements EntityAdapter
{

	@Override
	public void run(Map<String, Object> params)
	{
		try
		{
			EForm eform = (EForm) params.get(EFORM);

			EntityWrapper wrapper = new EntityWrapper(eform.getObject());

			Long numeroOs = (Long) wrapper.findValue("NumeroOs");
			if (numeroOs != null && numeroOs != 0){
			String mensagem = "";
			
			QLEqualsFilter filtroOrdem = new QLEqualsFilter("id_ordem", numeroOs);
			List<NeoObject> listOrdem = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("dbORDEM"), filtroOrdem);
			if (listOrdem != null && listOrdem.size() > 0)
			{
				listOrdem.get(0);
			}
			else
			{
				mensagem = "Não encontrado o numero da OS!";
				throw new WorkflowException(mensagem);
			}

			List<NeoObject> listHistoricoChamado = (List<NeoObject>) wrapper.findValue("historicoChamadas");
			if (listHistoricoChamado != null)
			{

				NeoObject ultimoRegistro = listHistoricoChamado.get(listHistoricoChamado.size() - 1);
				EntityWrapper wrpUltimoRegistro = new EntityWrapper(ultimoRegistro);
				String observacaoChamado = (String) wrpUltimoRegistro.findValue("observacoesChamada");
				GregorianCalendar dataLigacao = (GregorianCalendar) wrpUltimoRegistro.findValue("dataLigacao");
				Time horaLigacao = (Time) wrpUltimoRegistro.findValue("horaLigacao");
				InstantiableEntityInfo registroAtividade = AdapterUtils.getInstantiableEntityInfo("SIGMALogOS");
				NeoObject logOs = registroAtividade.createNewInstance();
				EntityWrapper wRegistro = new EntityWrapper(logOs);

				wRegistro.findField("dataLog").setValue(dataLigacao);
				wRegistro.findField("horaLog").setValue(horaLigacao);
				wRegistro.findField("textoLog").setValue(observacaoChamado);
				wRegistro.findField("os").setValue(listOrdem.get(0));
				wRegistro.findField("usuario").setValue(PortalUtil.getCurrentUser());
				wRegistro.findField("numeroOS").setValue(numeroOs);
				PersistEngine.persist(logOs);

			}
			else
			{
				mensagem = "Não encontrado Registro para o histórico de chamados!";
				throw new WorkflowException(mensagem);
			}

		}
		}
		catch (Exception e)
		{
			e.printStackTrace();

		}

	}

}
