package com.neomind.fusion.custom.orsegups.fap.adapter;

import java.util.HashMap;
import java.util.Map;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.workflow.event.TaskFinishEvent;
import com.neomind.fusion.workflow.event.TaskFinishEventListener;
import com.neomind.fusion.workflow.exception.TaskException;
import com.neomind.util.NeoUtils;


/**
 * @author Diogo Simão
 * 
 * Este adapter de eventos é responsável por distribuir a atividade de Slipar Pagamento individual
 * em diferentes pool's, dependendo do tipo de aplicação da FAP em questão.
 * 
 * */
public class FAPDistribuiAtividadeSliparPagamentoPorAplicacao implements TaskFinishEventListener {

	NeoPaper papelSliparIndividual = null;
	NeoPaper papelSliparAgrupado = null;

	@Override
	public void onFinish(TaskFinishEvent event) throws TaskException {

		EntityWrapper ewProcessEntity = new EntityWrapper(event.getProcess().getEntity());

		Long codigoAplicacao = ewProcessEntity.findGenericValue("aplicacaoPagamento.codigo");
		
		papelSliparIndividual = getPapelResponsavelIndividual(codigoAplicacao);
		papelSliparAgrupado = getPapelResponsavelAgrupado(codigoAplicacao);
		
		ewProcessEntity.findField("responsavelSliparPagamento").setValue(papelSliparIndividual);
		ewProcessEntity.findField("responsavelSliparPagamentoAgrupado").setValue(papelSliparAgrupado);
	}

	/**
	 * @param codigoAplicacao - O código da aplicação da FAP.
	 * @return NeoPaper - O papel que será atribuído como responsável da atividade.
	 * 
	 * */
	public NeoPaper getPapelResponsavelIndividual(Long codigoAplicacao) {		
		NeoPaper papel = null;
		
		Map<Long, String> responsaveis = new HashMap<>();
		
		responsaveis.put(1L, "FAPSliparPagamentoIndividualFrota");
		responsaveis.put(10L,"FAPSliparPagamentoIndividualTecnico");
		responsaveis.put(11L,"FAPSliparPagamentoIndividualTatico");

		for(Long key : responsaveis.keySet()){
			if(codigoAplicacao == key){
				papel = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", responsaveis.get(key)));
			}
		}
		// Caso não exista divisão para a aplicação em questão, será atribuído o papel padrão.
		if(NeoUtils.safeIsNull(papel)){
			papel = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "[FAP] Controladoria"));
		}
		return papel;
	}
	
	/**
	 * @param codigoAplicacao - O código da aplicação da FAP.
	 * @return NeoPaper - O papel que será atribuído como responsável da atividade.
	 * 
	 * */
	public NeoPaper getPapelResponsavelAgrupado(Long codigoAplicacao) {		
		NeoPaper papel = null;
		
		Map<Long, String> responsaveis = new HashMap<>();
		
		responsaveis.put(1L, "FAPSliparPagamentoAgrupadoFrota");
		responsaveis.put(10L,"FAPSliparPagamentoAgrupadoTecnico");
		responsaveis.put(11L,"FAPSliparPagamentoAgrupadoTatico");

		for(Long key : responsaveis.keySet()){
			if(codigoAplicacao == key){
				papel = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", responsaveis.get(key)));
			}
		}
		// Caso não exista divisão para a aplicação em questão, será atribuído o papel padrão.
		if(NeoUtils.safeIsNull(papel)){
			papel = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "[FAP] Controladoria Slipar Agrupado"));
		}
		return papel;
	}




}
