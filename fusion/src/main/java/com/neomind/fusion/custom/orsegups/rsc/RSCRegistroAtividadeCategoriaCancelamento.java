package com.neomind.fusion.custom.orsegups.rsc;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.HtmlEmail;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.rsc.vo.ServicosVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class RSCRegistroAtividadeCategoriaCancelamento implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(RSCRegistroAtividadeCategoriaCancelamento.class);

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		List<ServicosVO> listaServicos = new ArrayList();

		String mensagem = "Por favor, contatar o administrador  do sistema!";
		WorkflowException exception = null;
		try
		{
			RSCUtils rscUtils = new RSCUtils();
			GregorianCalendar prazoDeadLine = new GregorianCalendar();

			InstantiableEntityInfo registroAtividade = AdapterUtils.getInstantiableEntityInfo("RSCRegistroAtividade");
			NeoObject registro = registroAtividade.createNewInstance();
			EntityWrapper wRegistro = new EntityWrapper(registro);

			wRegistro.findField("responsavel").setValue(origin.getUser());
			wRegistro.findField("dataInicialRSC").setValue(origin.getStartDate());
			wRegistro.findField("dataFinalRSC").setValue(origin.getFinishDate());

			/* Histórico de Atividade do Fluxo C027.001 - RSC - Categoria Diversos */
			if (activity.getProcess().getModel().getName().equals("C027.003 - RSC - Categoria Cancelamento"))
			{
				String des = "";
				if (origin.getActivityName().equalsIgnoreCase("Tentar Reversão"))
				{
					String txtDescricao = "";
					if (origin.getFinishByUser() == null)
					{
						prazoDeadLine = rscUtils.retornaPrazoDeadLine("Tentar Reversão - Escalada", (GregorianCalendar) processEntity.findValue("prazoTentarReversao"));
						processEntity.findField("prazoTentarReversaoEscalada").setValue(prazoDeadLine);

						des = "RSC não atendida pelo colaborador " + origin.getUser().getFullName();
						wRegistro.findField("descricao").setValue(des);
						wRegistro.findField("atividade").setValue("Tentar Reversão");
						wRegistro.findField("prazo").setValue(prazoDeadLine);

						processEntity.setValue("souPessoaResponsavel", true);
					}
					else
					{
						Boolean souPessoaResponsavel = (Boolean) processEntity.findValue("souPessoaResponsavel");
						if (souPessoaResponsavel)
						{
							Boolean cancelamentoRevertido = (Boolean) processEntity.findValue("cancelamentoFoiRevertido");
							if (cancelamentoRevertido)
							{
								String codFam;
								Long regionalContrato = (Long) processEntity.findValue("contratoSapiens.usu_codreg");
								Long empresaContrato = (Long) processEntity.findValue("contratoSapiens.usu_codemp");
								Long filialContrato = (Long) processEntity.findValue("contratoSapiens.usu_codfil");
								Long Contrato = (Long) processEntity.findValue("contratoSapiens.usu_numctr");

								listaServicos = rscUtils.contratoServico(empresaContrato, filialContrato, Contrato);
								codFam = rscUtils.recuperaFamilia(listaServicos);
								NeoObject objPapelGerenteRegional = rscUtils.getPapelGerenteRegional(regionalContrato, codFam);
								processEntity.setValue("responsavelExecutorAprovacaoBonificacao", objPapelGerenteRegional);

								des = (String) processEntity.findValue("observacao");

								Boolean revertidoComBonificacao = (Boolean) processEntity.findValue("revertidoComBonificacao");
								if (cancelamentoRevertido && revertidoComBonificacao)
								{
									prazoDeadLine = rscUtils.retornaPrazoDeadLine("Aprovar Bonificação", new GregorianCalendar());
									processEntity.findField("prazoAprovarBonificacao").setValue(prazoDeadLine);

									wRegistro.findField("descricao").setValue(des);
									wRegistro.findField("atividade").setValue("Tentar Reversão");
									wRegistro.findField("prazo").setValue(prazoDeadLine);

									processEntity.setValue("observacao", null);
								}
								if (cancelamentoRevertido && !revertidoComBonificacao)
								{
									Long codigoCliente = (Long) processEntity.findValue("contratoSapiens.codcli");
									String nomeCliente = (String) processEntity.findValue("contratoSapiens.nomcli");
									Long numeroContrato = (Long) processEntity.findValue("contratoSapiens.usu_numctr");
									String subject = "RSC de Cancelamento " + activity.getCode() + " Contrato revertido";
									String textoObjeto = "RSC de cancelamento" + activity.getCode() + " Cliente: " + codigoCliente + "-" + nomeCliente + " Contrato: " + numeroContrato + " foi revertido";
									enviarEmail(textoObjeto, subject);
									wRegistro.findField("descricao").setValue(des);
									wRegistro.findField("atividade").setValue("Tentar Reversão");
								}
								if (!cancelamentoRevertido)
								{
									prazoDeadLine = rscUtils.retornaPrazoDeadLine("Anexar Carta de Cancelamento", new GregorianCalendar());
									processEntity.findField("prazoAnexarCartaCancelamento").setValue(prazoDeadLine);

									wRegistro.findField("descricao").setValue(des);
									wRegistro.findField("atividade").setValue("Tentar Reversão");
									wRegistro.findField("prazo").setValue(prazoDeadLine);
								}
							}
							else
							{
								/* Adicionar o prazo para o executivo Visitar o Cliente */
								des = (String) processEntity.findValue("observacao");
								Boolean revertidoComBonificacao = (Boolean) processEntity.findValue("revertidoComBonificacao");
								if (cancelamentoRevertido && revertidoComBonificacao)
								{
									prazoDeadLine = rscUtils.retornaPrazoDeadLine("Aprovar Bonificação", new GregorianCalendar());
									processEntity.findField("prazoAprovarBonificacao").setValue(prazoDeadLine);

									wRegistro.findField("descricao").setValue(des);
									wRegistro.findField("atividade").setValue("Tentar Reversão");
									wRegistro.findField("prazo").setValue(prazoDeadLine);
								}
								if (cancelamentoRevertido && !revertidoComBonificacao)
								{
									prazoDeadLine = rscUtils.retornaPrazoTarefaEficacia(processEntity);

									wRegistro.findField("descricao").setValue(des);
									wRegistro.findField("atividade").setValue("Tentar Reversão");
									wRegistro.findField("prazo").setValue(prazoDeadLine);
								}
								if (!cancelamentoRevertido)
								{
									prazoDeadLine = rscUtils.retornaPrazoDeadLine("Visitar Cliente", new GregorianCalendar());
									processEntity.findField("prazoVisitarCliente").setValue(prazoDeadLine);

									wRegistro.findField("descricao").setValue(des);
									wRegistro.findField("atividade").setValue("Tentar Reversão");
									wRegistro.findField("prazo").setValue(prazoDeadLine);
								}
							}
						}
						else
						{
							des = "Ajustado a tarefa para o colaborador " + processEntity.findValue("usuarioResponsavel.fullName") + " com a seguinte justificativa: " + processEntity.findValue("justifique");
							wRegistro.findField("atividade").setValue("Tentar Reversão");
							wRegistro.findField("descricao").setValue(des);
							wRegistro.findField("prazo").setValue((GregorianCalendar) processEntity.findValue("prazoTentarReversao"));

							/* Limpa os campos */
							processEntity.setValue("usuarioResponsavel", null);
							processEntity.setValue("justifique", null);
						}
					}
				}
				else if (origin.getActivityName().equalsIgnoreCase("Tentar Reversão - Escalada"))
				{
					String txtDescricao = "";
					if (origin.getFinishByUser() == null)
					{
						des = "RSC não atendida pelo colaborador " + origin.getUser().getFullName();
						wRegistro.findField("descricao").setValue(des);
						wRegistro.findField("atividade").setValue("Tentar Reversão - Escalada");
						wRegistro.findField("prazo").setValue(prazoDeadLine);

						String papel = (String) processEntity.findValue("superiorResponsavelExecutorTentarReversao.name");
						if (papel.contains("Diretor") || papel.contains("Presidente"))
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Tentar Reversão - Escalada Diretoria", (GregorianCalendar) processEntity.findValue("prazoTentarReversaoEscalada"));
							processEntity.findField("prazoTentarReversaoEscaladaDiretoria").setValue(prazoDeadLine);
							processEntity.findField("souPessoaResponsavel").setValue(false);
							processEntity.findField("continuarRSC").setValue(true);

							wRegistro.findField("prazo").setValue(prazoDeadLine);
							
							rscUtils.registrarTarefaSimples(processEntity, activity);
						}
						else
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Tentar Reversão - Escalada", (GregorianCalendar) processEntity.findValue("prazoTentarReversaoEscalada"));
							processEntity.findField("prazoTentarReversaoEscalada").setValue(prazoDeadLine);

							wRegistro.findField("prazo").setValue(prazoDeadLine);
						}

						processEntity.setValue("souPessoaResponsavel", true);
					}
					else
					{
						Boolean cancelamentoRevertido = (Boolean) processEntity.findValue("cancelamentoFoiRevertido");
						if (cancelamentoRevertido)
						{
							String codFam;
							Long regionalContrato = (Long) processEntity.findValue("contratoSapiens.usu_codreg");
							Long empresaContrato = (Long) processEntity.findValue("contratoSapiens.usu_codemp");
							Long filialContrato = (Long) processEntity.findValue("contratoSapiens.usu_codfil");
							Long Contrato = (Long) processEntity.findValue("contratoSapiens.usu_numctr");

							listaServicos = rscUtils.contratoServico(empresaContrato, filialContrato, Contrato);
							codFam = rscUtils.recuperaFamilia(listaServicos);
							NeoObject objPapelGerenteRegional = rscUtils.getPapelGerenteRegional(regionalContrato, codFam);
							processEntity.setValue("responsavelExecutorAprovacaoBonificacao", objPapelGerenteRegional);

							des = (String) processEntity.findValue("observacao");

							Boolean revertidoComBonificacao = (Boolean) processEntity.findValue("revertidoComBonificacao");
							if (cancelamentoRevertido && revertidoComBonificacao)
							{
								prazoDeadLine = rscUtils.retornaPrazoDeadLine("Aprovar Bonificação", new GregorianCalendar());
								processEntity.findField("prazoAprovarBonificacao").setValue(prazoDeadLine);

								wRegistro.findField("descricao").setValue(des);
								wRegistro.findField("atividade").setValue("Tentar Reversão - Escalada");
								wRegistro.findField("prazo").setValue(prazoDeadLine);

								processEntity.setValue("observacao", null);
							}
							if (cancelamentoRevertido && !revertidoComBonificacao)
							{
								wRegistro.findField("descricao").setValue(des);
								wRegistro.findField("atividade").setValue("Tentar Reversão - Escalada");
							}
							if (!cancelamentoRevertido)
							{
								prazoDeadLine = rscUtils.retornaPrazoDeadLine("Anexar Carta de Cancelamento", new GregorianCalendar());
								processEntity.findField("prazoAnexarCartaCancelamento").setValue(prazoDeadLine);

								wRegistro.findField("descricao").setValue(des);
								wRegistro.findField("atividade").setValue("Tentar Reversão - Escalada");
								wRegistro.findField("prazo").setValue(prazoDeadLine);
							}
						}
						else
						{
							/* Adicionar o prazo para o executivo Visitar o Cliente */
							des = (String) processEntity.findValue("observacao");
							Boolean revertidoComBonificacao = (Boolean) processEntity.findValue("revertidoComBonificacao");
							if (cancelamentoRevertido && revertidoComBonificacao)
							{
								prazoDeadLine = rscUtils.retornaPrazoDeadLine("Aprovar Bonificação", new GregorianCalendar());
								processEntity.findField("prazoAprovarBonificacao").setValue(prazoDeadLine);

								wRegistro.findField("descricao").setValue(des);
								wRegistro.findField("atividade").setValue("Tentar Reversão - Escalada");
								wRegistro.findField("prazo").setValue(prazoDeadLine);
							}
							if (cancelamentoRevertido && !revertidoComBonificacao)
							{
								prazoDeadLine = rscUtils.retornaPrazoTarefaEficacia(processEntity);

								wRegistro.findField("descricao").setValue(des);
								wRegistro.findField("atividade").setValue("Tentar Reversão - Escalada");
								wRegistro.findField("prazo").setValue(prazoDeadLine);
							}
							if (!cancelamentoRevertido)
							{
								prazoDeadLine = rscUtils.retornaPrazoDeadLine("Visitar Cliente", new GregorianCalendar());
								processEntity.findField("prazoVisitarCliente").setValue(prazoDeadLine);

								wRegistro.findField("descricao").setValue(des);
								wRegistro.findField("atividade").setValue("Tentar Reversão - Escalada");
								wRegistro.findField("prazo").setValue(prazoDeadLine);
							}
						}
					}
				}
				else if (origin.getActivityName().equalsIgnoreCase("Tentar Reversão - Escalada Diretoria"))
				{
					String txtDescricao = "";
					if (origin.getFinishByUser() == null)
					{
						prazoDeadLine = rscUtils.retornaPrazoDeadLine("Tentar Reversão - Escalada Presidência", (GregorianCalendar) processEntity.findValue("prazoTentarReversaoEscaladaDiretoria"));
						processEntity.findField("prazoTentarReversaoEscaladaPresidencia").setValue(prazoDeadLine);

						des = "RSC não atendida pelo colaborador " + origin.getUser().getFullName();
						wRegistro.findField("atividade").setValue("Tentar Reversão - Escalada Diretoria");
						wRegistro.findField("descricao").setValue(des);

						wRegistro.findField("prazo").setValue(prazoDeadLine);
						
						rscUtils.registrarTarefaSimples(processEntity, activity);
					}
					else
					{
						Boolean cancelamentoRevertido = (Boolean) processEntity.findValue("cancelamentoFoiRevertido");
						if (cancelamentoRevertido)
						{
							String codFam;
							Long regionalContrato = (Long) processEntity.findValue("contratoSapiens.usu_codreg");
							Long empresaContrato = (Long) processEntity.findValue("contratoSapiens.usu_codemp");
							Long filialContrato = (Long) processEntity.findValue("contratoSapiens.usu_codfil");
							Long Contrato = (Long) processEntity.findValue("contratoSapiens.usu_numctr");

							listaServicos = rscUtils.contratoServico(empresaContrato, filialContrato, Contrato);
							codFam = rscUtils.recuperaFamilia(listaServicos);
							NeoObject objPapelGerenteRegional = rscUtils.getPapelGerenteRegional(regionalContrato, codFam);
							processEntity.setValue("responsavelExecutorAprovacaoBonificacao", objPapelGerenteRegional);

							des = (String) processEntity.findValue("observacao");

							Boolean revertidoComBonificacao = (Boolean) processEntity.findValue("revertidoComBonificacao");
							if (cancelamentoRevertido && revertidoComBonificacao)
							{
								prazoDeadLine = rscUtils.retornaPrazoDeadLine("Aprovar Bonificação", new GregorianCalendar());
								processEntity.findField("prazoAprovarBonificacao").setValue(prazoDeadLine);

								wRegistro.findField("descricao").setValue(des);
								wRegistro.findField("atividade").setValue("Tentar Reversão - Escalada Diretoria");
								wRegistro.findField("prazo").setValue(prazoDeadLine);

								processEntity.setValue("observacao", null);
							}
							if (cancelamentoRevertido && !revertidoComBonificacao)
							{
								wRegistro.findField("descricao").setValue(des);
								wRegistro.findField("atividade").setValue("Tentar Reversão - Escalada Diretoria");
							}
							if (!cancelamentoRevertido)
							{
								prazoDeadLine = rscUtils.retornaPrazoDeadLine("Anexar Carta de Cancelamento", new GregorianCalendar());
								processEntity.findField("prazoAnexarCartaCancelamento").setValue(prazoDeadLine);

								wRegistro.findField("descricao").setValue(des);
								wRegistro.findField("atividade").setValue("Tentar Reversão - Escalada Diretoria");
								wRegistro.findField("prazo").setValue(prazoDeadLine);
							}
						}
						else
						{
							/* Adicionar o prazo para o executivo Visitar o Cliente */
							des = (String) processEntity.findValue("observacao");
							Boolean revertidoComBonificacao = (Boolean) processEntity.findValue("revertidoComBonificacao");
							if (cancelamentoRevertido && revertidoComBonificacao)
							{
								prazoDeadLine = rscUtils.retornaPrazoDeadLine("Aprovar Bonificação", new GregorianCalendar());
								processEntity.findField("prazoAprovarBonificacao").setValue(prazoDeadLine);

								wRegistro.findField("descricao").setValue(des);
								wRegistro.findField("atividade").setValue("Tentar Reversão - Escalada Diretoria");
								wRegistro.findField("prazo").setValue(prazoDeadLine);
							}
							if (cancelamentoRevertido && !revertidoComBonificacao)
							{
								prazoDeadLine = rscUtils.retornaPrazoTarefaEficacia(processEntity);

								wRegistro.findField("descricao").setValue(des);
								wRegistro.findField("atividade").setValue("Tentar Reversão - Escalada Diretoria");
								wRegistro.findField("prazo").setValue(prazoDeadLine);
							}
							if (!cancelamentoRevertido)
							{
								prazoDeadLine = rscUtils.retornaPrazoDeadLine("Visitar Cliente", new GregorianCalendar());
								processEntity.findField("prazoVisitarCliente").setValue(prazoDeadLine);

								wRegistro.findField("descricao").setValue(des);
								wRegistro.findField("atividade").setValue("Tentar Reversão - Escalada Diretoria");
								wRegistro.findField("prazo").setValue(prazoDeadLine);
							}
						}
					}
				}
				else if (origin.getActivityName().equalsIgnoreCase("Tentar Reversão - Escalada Presidência"))
				{
					Boolean cancelamentoRevertido = (Boolean) processEntity.findValue("cancelamentoFoiRevertido");
					if (cancelamentoRevertido)
					{
						String codFam;
						Long regionalContrato = (Long) processEntity.findValue("contratoSapiens.usu_codreg");
						Long empresaContrato = (Long) processEntity.findValue("contratoSapiens.usu_codemp");
						Long filialContrato = (Long) processEntity.findValue("contratoSapiens.usu_codfil");
						Long Contrato = (Long) processEntity.findValue("contratoSapiens.usu_numctr");

						listaServicos = rscUtils.contratoServico(empresaContrato, filialContrato, Contrato);
						codFam = rscUtils.recuperaFamilia(listaServicos);
						NeoObject objPapelGerenteRegional = rscUtils.getPapelGerenteRegional(regionalContrato, codFam);
						processEntity.setValue("responsavelExecutorAprovacaoBonificacao", objPapelGerenteRegional);

						des = (String) processEntity.findValue("observacao");

						Boolean revertidoComBonificacao = (Boolean) processEntity.findValue("revertidoComBonificacao");
						if (cancelamentoRevertido && revertidoComBonificacao)
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Aprovar Bonificação", new GregorianCalendar());
							processEntity.findField("prazoAprovarBonificacao").setValue(prazoDeadLine);

							wRegistro.findField("descricao").setValue(des);
							wRegistro.findField("atividade").setValue("Tentar Reversão - Escalada Presidência");
							wRegistro.findField("prazo").setValue(prazoDeadLine);

							processEntity.setValue("observacao", null);
						}
						if (cancelamentoRevertido && !revertidoComBonificacao)
						{
							wRegistro.findField("descricao").setValue(des);
							wRegistro.findField("atividade").setValue("Tentar Reversão - Escalada Presidência");
						}
						if (!cancelamentoRevertido)
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Anexar Carta de Cancelamento", new GregorianCalendar());
							processEntity.findField("prazoAnexarCartaCancelamento").setValue(prazoDeadLine);

							wRegistro.findField("descricao").setValue(des);
							wRegistro.findField("atividade").setValue("Tentar Reversão - Escalada Presidência");
							wRegistro.findField("prazo").setValue(prazoDeadLine);
						}
					}
					else
					{
						/* Adicionar o prazo para o executivo Visitar o Cliente */
						des = (String) processEntity.findValue("observacao");
						Boolean revertidoComBonificacao = (Boolean) processEntity.findValue("revertidoComBonificacao");
						if (cancelamentoRevertido && revertidoComBonificacao)
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Aprovar Bonificação", new GregorianCalendar());
							processEntity.findField("prazoAprovarBonificacao").setValue(prazoDeadLine);

							wRegistro.findField("descricao").setValue(des);
							wRegistro.findField("atividade").setValue("Tentar Reversão - Escalada Presidência");
							wRegistro.findField("prazo").setValue(prazoDeadLine);
						}
						if (cancelamentoRevertido && !revertidoComBonificacao)
						{
							prazoDeadLine = rscUtils.retornaPrazoTarefaEficacia(processEntity);

							wRegistro.findField("descricao").setValue(des);
							wRegistro.findField("atividade").setValue("Tentar Reversão - Escalada Presidência");
							wRegistro.findField("prazo").setValue(prazoDeadLine);
						}
						if (!cancelamentoRevertido)
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Visitar Cliente", new GregorianCalendar());
							processEntity.findField("prazoVisitarCliente").setValue(prazoDeadLine);

							wRegistro.findField("descricao").setValue(des);
							wRegistro.findField("atividade").setValue("Tentar Reversão - Escalada Presidência");
							wRegistro.findField("prazo").setValue(prazoDeadLine);
						}
					}
				}
				else if (origin.getActivityName().equalsIgnoreCase("Visitar Cliente"))
				{
					String txtDescricao = "";
					if (origin.getFinishByUser() == null)
					{
						prazoDeadLine = rscUtils.retornaPrazoDeadLine("Visitar Cliente", (GregorianCalendar) processEntity.findValue("prazoVisitarCliente"));
						processEntity.findField("prazoVisitarClienteEscalada").setValue(prazoDeadLine);

						des = "RSC não atendida pelo colaborador " + origin.getUser().getFullName();
						wRegistro.findField("descricao").setValue(des);
						wRegistro.findField("atividade").setValue("Visitar Cliente");
						wRegistro.findField("prazo").setValue(prazoDeadLine);

						processEntity.setValue("souPessoaResponsavel", true);
						processEntity.setValue("visitouCliente", true);

						String codFam;
						Long regionalContrato = (Long) processEntity.findValue("contratoSapiens.usu_codreg");
						Long empresaContrato = (Long) processEntity.findValue("contratoSapiens.usu_codemp");
						Long filialContrato = (Long) processEntity.findValue("contratoSapiens.usu_codfil");
						Long Contrato = (Long) processEntity.findValue("contratoSapiens.usu_numctr");

						listaServicos = rscUtils.contratoServico(empresaContrato, filialContrato, Contrato);
						codFam = rscUtils.recuperaFamilia(listaServicos);
						NeoObject objPapelGerenteRegional = rscUtils.getPapelGerenteRegional(regionalContrato, codFam);
						processEntity.setValue("responsavelExecutorAprovacaoBonificacao", objPapelGerenteRegional);
					}
					else
					{

						String codFam;
						Long regionalContrato = (Long) processEntity.findValue("contratoSapiens.usu_codreg");
						Long empresaContrato = (Long) processEntity.findValue("contratoSapiens.usu_codemp");
						Long filialContrato = (Long) processEntity.findValue("contratoSapiens.usu_codfil");
						Long Contrato = (Long) processEntity.findValue("contratoSapiens.usu_numctr");

						listaServicos = rscUtils.contratoServico(empresaContrato, filialContrato, Contrato);
						codFam = rscUtils.recuperaFamilia(listaServicos);
						NeoObject objPapelGerenteRegional = rscUtils.getPapelGerenteRegional(regionalContrato, codFam);
						processEntity.setValue("responsavelExecutorAprovacaoBonificacao", objPapelGerenteRegional);

						Boolean cancelamentoRevertido = (Boolean) processEntity.findValue("cancelamentoFoiRevertido");
						if (!cancelamentoRevertido && ((Long) processEntity.findValue("etapaVerificacaoEficacia")) == 1L)
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Anexar Carta de Cancelamento", new GregorianCalendar());
							processEntity.findField("prazoAnexarCartaCancelamento").setValue(prazoDeadLine);
						}

						des = (String) processEntity.findValue("observacao");

						wRegistro.findField("descricao").setValue(des);
						wRegistro.findField("atividade").setValue("Visitar Cliente");
						wRegistro.findField("prazo").setValue(prazoDeadLine);

						processEntity.setValue("souPessoaResponsavel", true);
						processEntity.setValue("visitouCliente", true);
					}
				}
				else if (origin.getActivityName().equalsIgnoreCase("Visitar Cliente - Escalada"))
				{
					String txtDescricao = "";
					if (origin.getFinishByUser() == null)
					{
						des = "RSC não atendida pelo colaborador " + origin.getUser().getFullName();
						wRegistro.findField("descricao").setValue(des);
						wRegistro.findField("atividade").setValue("Visitar Cliente - Escalada");
						wRegistro.findField("prazo").setValue(prazoDeadLine);

						String papel = (String) processEntity.findValue("superiorResponsavelExecutor.name");
						if (papel.contains("Diretor") || papel.contains("Presidente"))
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Visitar Cliente - Escalada Diretoria", (GregorianCalendar) processEntity.findValue("prazoVisitarClienteEscalada"));
							processEntity.findField("prazoVisitarClienteEscaladaDiretoria").setValue(prazoDeadLine);

							wRegistro.findField("prazo").setValue(prazoDeadLine);
							
							rscUtils.registrarTarefaSimples(processEntity, activity);
						}
						else
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Visitar Cliente - Escalada", (GregorianCalendar) processEntity.findValue("prazoVisitarClienteEscalada"));
							processEntity.findField("prazoAtenderSolicitacaoEscalada").setValue(prazoDeadLine);

							wRegistro.findField("prazo").setValue(prazoDeadLine);
						}

						processEntity.setValue("souPessoaResponsavel", true);
					}
					else
					{
						Boolean cancelamentoRevertido = (Boolean) processEntity.findValue("cancelamentoFoiRevertido");
						if (!cancelamentoRevertido && ((Long) processEntity.findValue("etapaVerificacaoEficacia")) == 1L)
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Anexar Carta de Cancelamento", new GregorianCalendar());
							processEntity.findField("prazoAnexarCartaCancelamento").setValue(prazoDeadLine);
						}

						des = (String) processEntity.findValue("observacao");

						wRegistro.findField("descricao").setValue(des);
						wRegistro.findField("atividade").setValue("Visitar Cliente - Escalada");
						wRegistro.findField("prazo").setValue(prazoDeadLine);

						processEntity.setValue("souPessoaResponsavel", true);
						processEntity.setValue("visitouCliente", true);
					}
				}
				else if (origin.getActivityName().equalsIgnoreCase("Visitar Cliente - Escalada Diretoria"))
				{
					String txtDescricao = "";
					if (origin.getFinishByUser() == null)
					{
						prazoDeadLine = rscUtils.retornaPrazoDeadLine("Visitar Cliente - Escalada Presidência", (GregorianCalendar) processEntity.findValue("prazoVisitarClienteEscaladaDiretoria"));
						processEntity.findField("prazoVisitarClienteEscaladaPresidencia").setValue(prazoDeadLine);

						des = "RSC não atendida pelo colaborador " + origin.getUser().getFullName();
						wRegistro.findField("atividade").setValue("Visitar Cliente - Escalada Diretoria");
						wRegistro.findField("descricao").setValue(des);

						wRegistro.findField("prazo").setValue(prazoDeadLine);
						
						rscUtils.registrarTarefaSimples(processEntity, activity);
					}
					else
					{
						Boolean cancelamentoRevertido = (Boolean) processEntity.findValue("cancelamentoFoiRevertido");
						if (!cancelamentoRevertido && ((Long) processEntity.findValue("etapaVerificacaoEficacia")) == 1L)
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Anexar Carta de Cancelamento", new GregorianCalendar());
							processEntity.findField("prazoAnexarCartaCancelamento").setValue(prazoDeadLine);
						}

						des = (String) processEntity.findValue("observacao");

						wRegistro.findField("descricao").setValue(des);
						wRegistro.findField("atividade").setValue("Visitar Cliente - Escalada Diretoria");
						wRegistro.findField("prazo").setValue(prazoDeadLine);

						processEntity.setValue("souPessoaResponsavel", true);
						processEntity.setValue("visitouCliente", true);
					}
				}
				else if (origin.getActivityName().equalsIgnoreCase("Visitar Cliente - Escalada Presidência"))
				{
					Boolean cancelamentoRevertido = (Boolean) processEntity.findValue("cancelamentoFoiRevertido");
					if (!cancelamentoRevertido && ((Long) processEntity.findValue("etapaVerificacaoEficacia")) == 1L)
					{
						prazoDeadLine = rscUtils.retornaPrazoDeadLine("Anexar Carta de Cancelamento", new GregorianCalendar());
						processEntity.findField("prazoAnexarCartaCancelamento").setValue(prazoDeadLine);
					}

					des = (String) processEntity.findValue("observacao");

					wRegistro.findField("descricao").setValue(des);
					wRegistro.findField("atividade").setValue("Visitar Cliente - Escalada Presidência");
					wRegistro.findField("prazo").setValue(prazoDeadLine);

					processEntity.setValue("souPessoaResponsavel", true);
					processEntity.setValue("visitouCliente", true);
				}
				else if (origin.getActivityName().equalsIgnoreCase("Aprovar Bonificação"))
				{
					String txtDescricao = "";
					if (origin.getFinishByUser() == null)
					{
						prazoDeadLine = rscUtils.retornaPrazoDeadLine("Aprovar Bonificação - Escalada", (GregorianCalendar) processEntity.findValue("prazoAprovarBonificacao"));
						processEntity.findField("prazoAprovarBonificacaoEscalada").setValue(prazoDeadLine);

						des = "RSC não atendida pelo colaborador " + origin.getUser().getFullName();
						wRegistro.findField("atividade").setValue("Aprovar Bonificação");
						wRegistro.findField("descricao").setValue(des);

						wRegistro.findField("prazo").setValue(prazoDeadLine);
					}
					else
					{
						Boolean bonificacaoAprovada = (Boolean) processEntity.findValue("bonificacaoAprovada");
						if (bonificacaoAprovada)
						{
							NeoObject objResponsavelValidacaoApontamento = OrsegupsUtils.getPaper("RSCLancarApontamentos");
							processEntity.setValue("responsavelValidacaoApontamentos", objResponsavelValidacaoApontamento);
						}

						des = (String) processEntity.findValue("observacao");

						wRegistro.findField("atividade").setValue("Aprovar Bonificação");
						wRegistro.findField("descricao").setValue(des);
					}
				}
				else if (origin.getActivityName().equalsIgnoreCase("Aprovar Bonificação - Escalada"))
				{
					String txtDescricao = "";
					if (origin.getFinishByUser() == null)
					{
						des = "RSC não atendida pelo colaborador " + origin.getUser().getFullName();
						wRegistro.findField("descricao").setValue(des);
						wRegistro.findField("atividade").setValue("Aprovar Bonificação - Escalada");
						wRegistro.findField("prazo").setValue(prazoDeadLine);

						String papel = (String) processEntity.findValue("superiorResponsavelExecutorAprovacaoBonificacao.name");
						if (papel.contains("Diretor") || papel.contains("Presidente"))
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Aprovar Bonificação - Escalada Diretoria", (GregorianCalendar) processEntity.findValue("prazoAprovarBonificacaoEscalada"));
							processEntity.findField("prazoAprovarBonificacaoEscaladaDiretoria").setValue(prazoDeadLine);

							wRegistro.findField("prazo").setValue(prazoDeadLine);
							
							rscUtils.registrarTarefaSimples(processEntity, activity);
						}
						else
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Aprovar Bonificação - Escalada", (GregorianCalendar) processEntity.findValue("prazoAprovarBonificacaoEscalada"));
							processEntity.findField("prazoAprovarBonificacaoEscalada").setValue(prazoDeadLine);

							wRegistro.findField("prazo").setValue(prazoDeadLine);
						}

						processEntity.setValue("souPessoaResponsavel", true);
					}
					else
					{
						Boolean bonificacaoAprovada = (Boolean) processEntity.findValue("bonificacaoAprovada");
						if (bonificacaoAprovada)
						{
							NeoObject objResponsavelValidacaoApontamento = OrsegupsUtils.getPaper("Supervisor CEREC");
							processEntity.setValue("responsavelValidacaoApontamentos", objResponsavelValidacaoApontamento);
						}
					}
				}
				else if (origin.getActivityName().equalsIgnoreCase("Aprovar Bonificação - Escalada Diretoria"))
				{
					String txtDescricao = "";
					if (origin.getFinishByUser() == null)
					{
						prazoDeadLine = rscUtils.retornaPrazoDeadLine("Aprovar Bonificação - Escalada Presidência", (GregorianCalendar) processEntity.findValue("prazoAprovarBonificacaoEscaladaDiretoria"));
						processEntity.findField("prazoAprovarBonificacaoEscaladaPresidencia").setValue(prazoDeadLine);

						des = "RSC não atendida pelo colaborador " + origin.getUser().getFullName();
						wRegistro.findField("atividade").setValue("Aprovar Bonificação - Escalada Diretoria");
						wRegistro.findField("descricao").setValue(des);

						wRegistro.findField("prazo").setValue(prazoDeadLine);
						
						rscUtils.registrarTarefaSimples(processEntity, activity);
					}
					else
					{
						Boolean bonificacaoAprovada = (Boolean) processEntity.findValue("bonificacaoAprovada");
						if (bonificacaoAprovada)
						{
							NeoObject objResponsavelValidacaoApontamento = OrsegupsUtils.getPaper("Supervisor CEREC");
							processEntity.setValue("responsavelValidacaoApontamentos", objResponsavelValidacaoApontamento);
						}
					}
				}
				else if (origin.getActivityName().equalsIgnoreCase("Aprovar Bonificação - Escalada Presidência"))
				{
					Boolean bonificacaoAprovada = (Boolean) processEntity.findValue("bonificacaoAprovada");
					if (bonificacaoAprovada)
					{
						NeoObject objResponsavelValidacaoApontamento = OrsegupsUtils.getPaper("Supervisor CEREC");
						processEntity.setValue("responsavelValidacaoApontamentos", objResponsavelValidacaoApontamento);
					}
				}
				else if (origin.getActivityName().equalsIgnoreCase("Efetivar Cancelamento do Contrato"))
				{
					String txtDescricao = "";
					if (origin.getFinishByUser() == null)
					{
						prazoDeadLine = rscUtils.retornaPrazoDeadLine("Efetivar Cancelamento do Contrato", (GregorianCalendar) processEntity.findValue("prazoEfetivarCancelamento"));
						processEntity.findField("prazoEfetivarCancelamentoEscalada").setValue(prazoDeadLine);

						des = "RSC não atendida pelo colaborador " + origin.getUser().getFullName();
						wRegistro.findField("atividade").setValue("Efetivar Cancelamento do Contrato");
						wRegistro.findField("descricao").setValue(des);

						wRegistro.findField("prazo").setValue(prazoDeadLine);
					}
					else
					{
						des = (String) processEntity.findValue("observacao");
						wRegistro.findField("atividade").setValue("Efetivar Cancelamento do Contrato");
						wRegistro.findField("descricao").setValue(des);

						wRegistro.findField("prazo").setValue(new GregorianCalendar());
					}
				}
				else if (origin.getActivityName().equalsIgnoreCase("Efetivar Cancelamento do Contrato - Escalada"))
				{
					String txtDescricao = "";
					if (origin.getFinishByUser() == null)
					{
						des = "RSC não atendida pelo colaborador " + origin.getUser().getFullName();
						wRegistro.findField("descricao").setValue(des);
						wRegistro.findField("atividade").setValue("Efetivar Cancelamento do Contrato - Escalada");
						wRegistro.findField("prazo").setValue(prazoDeadLine);

						String papel = (String) processEntity.findValue("superiorResponsavelExecutorCancelamento.name");
						if (papel.contains("Diretor") || papel.contains("Presidente"))
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Efetivar Cancelamento do Contrato - Escalada Diretoria", (GregorianCalendar) processEntity.findValue("prazoEfetivarCancelamentoEscalada"));
							processEntity.findField("prazoEfetivarCancelamentoEscaladaDiretoria").setValue(prazoDeadLine);

							wRegistro.findField("prazo").setValue(prazoDeadLine);
							
							rscUtils.registrarTarefaSimples(processEntity, activity);
						}
						else
						{
							prazoDeadLine = rscUtils.retornaPrazoDeadLine("Efetivar Cancelamento do Contrato - Escalada", (GregorianCalendar) processEntity.findValue("prazoEfetivarCancelamentoEscalada"));
							processEntity.findField("prazoEfetivarCancelamentoEscalada").setValue(prazoDeadLine);

							wRegistro.findField("prazo").setValue(prazoDeadLine);
						}

						processEntity.setValue("souPessoaResponsavel", true);
					}
					else
					{
						des = (String) processEntity.findValue("observacao");
						wRegistro.findField("atividade").setValue("Efetivar Cancelamento do Contrato - Escalada");
						wRegistro.findField("descricao").setValue(des);

						wRegistro.findField("prazo").setValue(new GregorianCalendar());
					}
				}
				else if (origin.getActivityName().equalsIgnoreCase("Efetivar Cancelamento do Contrato - Escalada Diretoria"))
				{
					String txtDescricao = "";
					if (origin.getFinishByUser() == null)
					{
						prazoDeadLine = rscUtils.retornaPrazoDeadLine("Efetivar Cancelamento do Contrato - Escalada Presidência", (GregorianCalendar) processEntity.findValue("prazoEfetivarCancelamentoEscaladaDiretoria"));
						processEntity.findField("prazoEfetivarCancelamentoEscaladaPresidencia").setValue(prazoDeadLine);

						des = "RSC não atendida pelo colaborador " + origin.getUser().getFullName();
						wRegistro.findField("atividade").setValue("Efetivar Cancelamento do Contrato - Escalada Diretoria");
						wRegistro.findField("descricao").setValue(des);

						wRegistro.findField("prazo").setValue(prazoDeadLine);
						
						rscUtils.registrarTarefaSimples(processEntity, activity);
					}
					else
					{
						des = (String) processEntity.findValue("observacao");
						wRegistro.findField("atividade").setValue("Efetivar Cancelamento do Contrato - Escalada Diretoria");
						wRegistro.findField("descricao").setValue(des);

						wRegistro.findField("prazo").setValue(new GregorianCalendar());
					}
				}
				else if (origin.getActivityName().equalsIgnoreCase("Efetivar Cancelamento do Contrato - Escalada Presidência"))
				{
					des = (String) processEntity.findValue("observacao");
					wRegistro.findField("atividade").setValue("Efetivar Cancelamento do Contrato - Escalada Diretoria");
					wRegistro.findField("descricao").setValue(des);

					wRegistro.findField("prazo").setValue(new GregorianCalendar());
				}
				else if (origin.getActivityName().equalsIgnoreCase("Validar Inserção dos Apontamentos"))
				{
					wRegistro.findField("descricao").setValue(des);
					wRegistro.findField("atividade").setValue("Validar Inserção dos Apontamentos");
				}
				else if (origin.getActivityName().equalsIgnoreCase("Anexar Carta de Cancelamento"))
				{
					if (origin.getFinishByUser() == null)
					{
						des = "Aberto Tarefa Simples para Anexar a Carta de Cancelamento";
						wRegistro.findField("atividade").setValue("Anexar Carta de Cancelamento");
						wRegistro.findField("descricao").setValue(des);
					}
					else
					{
						prazoDeadLine = rscUtils.retornaPrazoDeadLine("Efetivar Cancelamento do Contrato", new GregorianCalendar());
						processEntity.findField("prazoEfetivarCancelamento").setValue(prazoDeadLine);

						des = (String) processEntity.findValue("observacao");

						wRegistro.findField("descricao").setValue(des);
						wRegistro.findField("atividade").setValue("Anexar Carta de Cancelamento");

						NeoObject objEfetivarCancelamento = OrsegupsUtils.getPaper("RSCEfetivarCancelmentoContrato");
						processEntity.findField("responsavelExecutorCancelamento").setValue(objEfetivarCancelamento);
					}
				}

				NeoObject rscRegistroSolicitacaoCliente = (NeoObject) processEntity.findValue("RscRelatorioSolicitacaoCliente");
				EntityWrapper rscWrapper = new EntityWrapper(rscRegistroSolicitacaoCliente);

				PersistEngine.persist(registro);
				rscWrapper.findField("registroAtividades").addValue(registro);
			}
		}
		catch (Exception e)
		{
			log.error(mensagem);
			e.printStackTrace();
			throw new WorkflowException(mensagem);
		}
	}

	public void enviarEmail(String textoObjeto, String subject)
	{
		try
		{
			MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

			MailSettings mailClone = new MailSettings();
			mailClone.setMinutesInterval(settings.getMinutesInterval());
			mailClone.setFromEMail(settings.getFromEMail());
			mailClone.setFromName(settings.getFromName());
			mailClone.setRenderServer(settings.getRenderServer());
			mailClone.setSecurityFromEmail(settings.getSecurityFromEmail());
			mailClone.setSecurityCopyEmail(settings.getSecurityCopyEmail());
			mailClone.setSmtpSettings(settings.getSmtpSettings());
			mailClone.setPort(settings.getPort());
			mailClone.setSmtpServer(settings.getSmtpServer());
			mailClone.setEnabled(settings.isEnabled());
			mailClone.setFromEMail("fusion@orsegups.com.br");//da onde esta vindo
			mailClone.setFromName("Orsegups Partipações S.A.");

			HtmlEmail noUserEmail = new HtmlEmail();
			StringBuilder noUserMsg = new StringBuilder();

			noUserEmail.setCharset("ISO-8859-1");

			//fernanda.maciel@orsegups.com.br;rafael.santos@orsegups.com.br;ivanya.silva@orsegups.com.br;valtair.souza@orsegups.com.br;camila.silva@orsegups.com.br
			noUserEmail.addTo("daiana.porciuncula@orsegups.com.br");
			//noUserEmail.addTo("thiago.coelho@orsegups.com.br");

			//noUserEmail.addCc("emailautomatico@orsegups.com.br");//para quem vai com copia

			noUserEmail.setSubject(subject);
			noUserMsg.append("<!doctype html>");
			noUserMsg.append("<html>");
			noUserMsg.append("<head>");
			noUserMsg.append("<meta charset=\"utf-8\">");
			noUserMsg.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">Este é um e-mail automático do Sistema Fusion e não deve ser respondido.<br><br>");
			noUserMsg.append("</head>");
			noUserMsg.append("<body>");
			noUserMsg.append("<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#ffffff\">");
			noUserMsg.append("          <tbody>");
			noUserMsg.append("		    <td align=\"left\" valign=\"top\" >");
			noUserMsg.append("               <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/logoORS.jpg\" >");
			noUserMsg.append("            </td>");
			noUserMsg.append("			<td>");
			noUserMsg.append(textoObjeto);
			noUserMsg.append("			</td>");
			noUserMsg.append("     </tbody></table>");
			noUserMsg.append("</body></html>");
			noUserEmail.setHtmlMsg(noUserMsg.toString());
			noUserEmail.setContent(noUserMsg.toString(), "text/html");
			mailClone.applyConfig(noUserEmail);

			noUserEmail.send();
			//OrsegupsUtils.sendEmail2SendGrid(addTo, from, subject, "", noUserMsg.toString());
		}
		catch (Exception e)
		{
			e.printStackTrace();

		}

	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}
}
