package com.neomind.fusion.custom.orsegups.fap.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.dto.FornecedorDTO;
import com.neomind.fusion.custom.orsegups.fap.factory.FapFactory;
import com.neomind.fusion.custom.orsegups.fap.utils.FAPParametrizacao;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class JobTecnicoTaticoDAO {

    /**
     * Retorna a lista de técnicos cadastrados pelo financeiro para abrir as
     * FAP's do tipo aplicação técnico.
     * 
     * @return List<FornecedorDTO>
     * @throws Exception
     */
    public static List<FornecedorDTO> getFornecedoresTecnicos() throws Exception {
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	StringBuilder sql = new StringBuilder();

	List<FornecedorDTO> listaFornecedores = new ArrayList<>();

	try {

	    conn = PersistEngine.getConnection("");

	    sql.append(" SELECT SIG.NM_COLABORADOR, SIG.CD_COLABORADOR, FORNEC.NOMFOR, FORNEC.CODFOR FROM D_FAPAplicacaoTecnico FAP_TEC WITH (NOLOCK)");
	    sql.append(" INNER JOIN X_SIGMACOLABORADOR SIGCOL WITH (NOLOCK) ON FAP_TEC.tecnico_neoId = SIGCOL.neoId ");
	    sql.append(" INNER JOIN [FSOODB03\\SQL01].SIGMA90.dbo.colaborador SIG WITH (NOLOCK) on SIGCOL.cd_colaborador = sig.cd_colaborador ");
	    sql.append(" INNER JOIN X_ETABFOR AS TABFOR WITH (NOLOCK) ON TABFOR.neoId = FAP_TEC.FORNECEDOR_NEOID ");
	    sql.append(" INNER JOIN [FSOODB04\\SQL02].Sapiens.dbo.E095FOR AS FORNEC WITH (NOLOCK) ON FORNEC.CODFOR = TABFOR.codfor ");
	    //sql.append(" INNER JOIN [CACHOEIRA\\SQL01].SAPIENSTST.DBO.E095FOR AS FORNEC WITH (NOLOCK) ON FORNEC.CODFOR = TABFOR.codfor ");
	    sql.append(" WHERE");
	    sql.append(" FAP_TEC.ATIVO = 1");
	    sql.append(" AND NOT EXISTS                                                                                               ");
	    sql.append(" (SELECT * FROM D_FAPTaticoControleAberturaTarefaViaJob                                                       ");
	    sql.append(" 	  WHERE codigoTecnico = SIG.CD_COLABORADOR                                                                     ");
	    sql.append(" 	    AND DATAABERTURA BETWEEN ? AND ?)                                                                 ");
	    sql.append(" ORDER BY USU_DATCOR, FAP_TEC.tecnico_neoId ");

	    // sql.append(" INNER JOIN [FSOODB04\\SQL02].SAPIENS.dbo.E095FOR AS FORNEC ON FORNEC.CODFOR = TABFOR.codfor ");
	    
	    boolean debug = (FAPParametrizacao.findParameter("debug", true).equals("1") ? true : false);

		int diaBaseControlaAberturaTarefa = Integer.parseInt(FAPParametrizacao.findParameter("diaBaseControleAberturaTarefaViaJob", true));

		GregorianCalendar gcInicial = new GregorianCalendar();
		gcInicial.set(GregorianCalendar.DAY_OF_MONTH, diaBaseControlaAberturaTarefa);
		gcInicial.set(Calendar.HOUR_OF_DAY, 0);
		gcInicial.set(Calendar.MINUTE, 0);
		gcInicial.set(Calendar.SECOND, 0);
		gcInicial.set(Calendar.MILLISECOND, 0);
		String gcStringInicial = null;	

		GregorianCalendar gcFinal = new GregorianCalendar();
		gcFinal.set(GregorianCalendar.DAY_OF_MONTH, diaBaseControlaAberturaTarefa);
		gcFinal.set(Calendar.HOUR_OF_DAY, 23);
		gcFinal.set(Calendar.MINUTE, 59);
		gcFinal.set(Calendar.SECOND, 59);
		gcFinal.set(Calendar.MILLISECOND, 59);
		String gcStringFinal = null;

		if(debug){     //Local	    
		    gcStringInicial = NeoDateUtils.safeDateFormat(gcInicial, "dd-MM-yyyy HH:mm:ss");
		    gcStringFinal = NeoDateUtils.safeDateFormat(gcFinal, "dd-MM-yyyy HH:mm:ss");
		} else {       // Produção
		    gcStringInicial = NeoDateUtils.safeDateFormat(gcInicial, "yyyy-MM-dd HH:mm:ss");
		    gcStringFinal = NeoDateUtils.safeDateFormat(gcFinal, "yyyy-MM-dd HH:mm:ss");
		}

	    pstm = conn.prepareStatement(sql.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
	    pstm.setString(1, gcStringInicial);
	    pstm.setString(2, gcStringFinal);
	    rs = pstm.executeQuery();
	    
	    int quantidadeDeTarefasPorVez = Integer.parseInt(FAPParametrizacao.findParameter("quantidadeDeTarefasPorVez", true));
		int contadorAuxiliar = 0;

	    if (rs.next()) {
			rs.beforeFirst();
			while (rs.next() && (contadorAuxiliar <= quantidadeDeTarefasPorVez)) {
			    FornecedorDTO fornecedor = new FornecedorDTO();
	
			    fornecedor.setNomeFornecedor(rs.getString("NOMFOR"));
			    fornecedor.setCodigoFornecedor(rs.getString("CODFOR"));
			    fornecedor.setCodigoTecnico(rs.getLong("CD_COLABORADOR"));
			    fornecedor.setNomeTecnico(rs.getString("NM_COLABORADOR"));
	
			    listaFornecedores.add(fornecedor);
			}
			return listaFornecedores;
		} else {
			return null;
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    throw new Exception(e.getMessage());
	} finally {
	    try {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}
    }

    
    /**
     * Consulta as rotas que possuem a string "%PARC%" no nome.
     * @return listaDeRotasParceiras - Objeto List contendo String's. São as rotas que atenderam as restrições da consulta em questão.
     * @throws Exception Para exceções em geral.  
     * */
    public static List<String> getRotasParceiras() throws Exception {
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	StringBuilder sql = new StringBuilder();

	List<String> listaDeRotasParceiras = new ArrayList<>();
	List<NeoObject> listaRotasSemCadastro = new ArrayList<>();

	sql.append(" SELECT ROTA.CD_ROTA, ROTA.NM_ROTA FROM ROTA");
	sql.append(" INNER JOIN dbCENTRAL C ON ROTA.CD_ROTA = C.ID_ROTA");
	sql.append(" WHERE NM_ROTA LIKE '%PARC%'");
	sql.append(" AND C.FG_ATIVO = 1 AND C.CTRL_CENTRAL = 1");
	sql.append(" GROUP BY ROTA.CD_ROTA, ROTA.NM_ROTA");

	try {
	    conn = PersistEngine.getConnection("SIGMA90");
	    pstm = conn.prepareStatement(sql.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

	    rs = pstm.executeQuery();
	    if (rs.next()) {
		rs.beforeFirst();

		while (rs.next()) {
		    Long codigoRota = rs.getLong("CD_ROTA");
		    NeoObject sigmaRota = FapFactory.neoObjectFactory("SIGMAROTA", "cd_rota", codigoRota);
		    QLEqualsFilter filter = new QLEqualsFilter("rotaSigma", sigmaRota);
		    NeoObject aplicacaoTatico = PersistEngine.getObject(AdapterUtils.getEntityClass("FAPAplicacaoTatico"), filter);

		    if (NeoUtils.safeIsNotNull(aplicacaoTatico)) {
			listaDeRotasParceiras.add(codigoRota.toString());
		    } else {
			listaRotasSemCadastro.add(sigmaRota);
		    }
		}	
		return listaDeRotasParceiras;

	    } else {
		return null;
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    throw new Exception(e.getMessage());
	} finally {
	    try {
		// Fecha as conexões
		OrsegupsUtils.closeConnection(conn, pstm, rs);
		// Trata as rotas que não tem cadastro
		//FapUtils.rotasParceirasSemCadastro(listaRotasSemCadastro);
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}

    }

    
    /**
     * Este método consulta todos os fornecedores táticos cadastrados no e-form FAPAplicacaoTatico e que, no dia específicado na parametrização,
     * ainda não abriram FAP. A quantidade de objetos na lista que é retornada também é controlada pelo eform de parametrização.
     * @param lista - Objeto List contendo String. É o resultado da consulta realizada no método getRotasParceiras();
     * @return listaFornecedores -  Objeto List contendo FornecedorDTO. Lista de objetos com os dados dos fornecedores.
     * @throws Exception Para exceções generalizadas.  
     * */
    public static List<FornecedorDTO> getFornecedoresTaticos(List<String> lista) throws Exception {
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	StringBuilder sql = new StringBuilder();

	List<FornecedorDTO> listaFornecedores = new ArrayList<>();
	List<String> listaDeRotasParceiras = lista;

	String rotas = StringUtils.join(listaDeRotasParceiras, ",");

	boolean debug = (FAPParametrizacao.findParameter("debug").equals("1") ? true : false);

	int diaBaseControlaAberturaTarefa = Integer.parseInt(FAPParametrizacao.findParameter("diaBaseControleAberturaTarefaViaJob"));

	GregorianCalendar gcInicial = new GregorianCalendar();
	gcInicial.set(GregorianCalendar.DAY_OF_MONTH, diaBaseControlaAberturaTarefa);
	gcInicial.set(Calendar.HOUR_OF_DAY, 0);
	gcInicial.set(Calendar.MINUTE, 0);
	gcInicial.set(Calendar.SECOND, 0);
	gcInicial.set(Calendar.MILLISECOND, 0);
	String gcStringInicial = null;	

	GregorianCalendar gcFinal = new GregorianCalendar();
	gcFinal.set(GregorianCalendar.DAY_OF_MONTH, diaBaseControlaAberturaTarefa);
	gcFinal.set(Calendar.HOUR_OF_DAY, 23);
	gcFinal.set(Calendar.MINUTE, 59);
	gcFinal.set(Calendar.SECOND, 59);
	gcFinal.set(Calendar.MILLISECOND, 59);
	String gcStringFinal = null;

	if(debug){     //Local	    
	    gcStringInicial = NeoDateUtils.safeDateFormat(gcInicial, "dd-MM-yyyy HH:mm:ss");
	    gcStringFinal = NeoDateUtils.safeDateFormat(gcFinal, "dd-MM-yyyy HH:mm:ss");
	} else {       // Produção
	    gcStringInicial = NeoDateUtils.safeDateFormat(gcInicial, "yyyy-MM-dd HH:mm:ss");
	    gcStringFinal = NeoDateUtils.safeDateFormat(gcFinal, "yyyy-MM-dd HH:mm:ss");
	}

	int quantidadeDeTarefasPorVez = Integer.parseInt(FAPParametrizacao.findParameter("quantidadeDeTarefasPorVez"));
	int contadorAuxiliar = 0;
	
	try {
	    conn = PersistEngine.getConnection("");

	    sql.append(" SELECT FORNEC.CODFOR, FORNEC.NOMFOR, FORNEC.USU_DATCOR,ROTA.CD_ROTA, ROTA.NM_ROTA, FAP_TAT.AGRUPARFAP        ");                                                       
	    sql.append(" FROM D_FAPAplicacaoTatico FAP_TAT                                                                            ");
	    sql.append(" INNER JOIN X_SIGMAROTA SIGROTA WITH (NOLOCK) ON FAP_TAT.rotaSigma_neoId = SIGROTA.neoId                      ");
	    sql.append(" INNER JOIN [FSOODB03\\SQL01].SIGMA90.dbo.ROTA ROTA WITH (NOLOCK) on SIGROTA.cd_rota = rota.cd_rota           ");
	    sql.append(" INNER JOIN X_ETABFOR AS TABFOR WITH (NOLOCK) ON TABFOR.neoId = FAP_TAT.FORNECEDOR_NEOID                      ");
	    sql.append(" INNER JOIN [FSOODB04\\SQL02].Sapiens.dbo.E095FOR AS FORNEC WITH (NOLOCK) ON FORNEC.CODFOR = TABFOR.codfor    ");
	    sql.append(" INNER JOIN D_FAPTaticoModalidadePgto MODAL ON FAP_TAT.modalidade_neoId = MODAL.neoId                         ");
	    sql.append(" WHERE MODAL.CODIGO NOT IN (4) AND FAP_TAT.ATIVO = 1 AND ROTA.CD_ROTA in ("+ rotas +")                        ");
	    sql.append(" AND NOT EXISTS                                                                                               ");
	    sql.append(" (SELECT * FROM D_FAPTaticoControleAberturaTarefaViaJob                                                       ");
	    sql.append(" 	  WHERE codigoRota = ROTA.CD_ROTA                                                                     ");
	    sql.append(" 	    AND DATAABERTURA BETWEEN ? AND ?)                                                                 ");
	    sql.append(" ORDER BY FORNEC.CODFOR, FORNEC.NOMFOR, FORNEC.USU_DATCOR, ROTA.CD_ROTA, ROTA.NM_ROTA                         ");

	    pstm = conn.prepareStatement(sql.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
	    pstm.setString(1, gcStringInicial);
	    pstm.setString(2, gcStringFinal);
	    rs = pstm.executeQuery();

	    if (rs.next()) {
		rs.beforeFirst();
		while (rs.next() && (contadorAuxiliar <= quantidadeDeTarefasPorVez)) {
		    FornecedorDTO fornecedor = new FornecedorDTO();

		    fornecedor.setNomeFornecedor(rs.getString("NOMFOR"));
		    fornecedor.setCodigoFornecedor(rs.getString("CODFOR"));
		    fornecedor.setDataCorte(rs.getInt("USU_DATCOR"));
		    fornecedor.setCodigoRota(rs.getLong("CD_ROTA"));
		    fornecedor.setNomeRota(rs.getString("NM_ROTA"));
		    fornecedor.setAgruparFap(rs.getBoolean("AGRUPARFAP"));
		    listaFornecedores.add(fornecedor);
		    contadorAuxiliar++;
		}
	    }
	    return listaFornecedores;
	} catch (Exception e) {
	    e.printStackTrace();
	    throw new Exception(e.getMessage());
	} finally {
	    try {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}
    }
}
