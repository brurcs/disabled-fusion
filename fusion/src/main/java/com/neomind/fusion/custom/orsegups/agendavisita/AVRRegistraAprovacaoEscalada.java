package com.neomind.fusion.custom.orsegups.agendavisita;

import java.util.Collection;
import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class AVRRegistraAprovacaoEscalada implements AdapterInterface {

	@Override
	public void start(Task origin, EntityWrapper wrapper,
			Activity activity) {
		try
		{
			String usuarioLogado = PortalUtil.getCurrentUser().getCode();
			
			NeoObject oRegistroAtividade = AdapterUtils.createNewEntityInstance("AVRRegistroAtividades");
			EntityWrapper wRegistroAtividade = new EntityWrapper(oRegistroAtividade);
			
			wRegistroAtividade.setValue("dataRegistro", new GregorianCalendar());
			wRegistroAtividade.setValue("usuario", usuarioLogado );
			boolean diretorAprovou = NeoUtils.safeBoolean(wrapper.findValue("aprovadorDiretorr"));
			boolean presidenteAprovou = NeoUtils.safeBoolean(wrapper.findValue("aprovadoPresidencia"));
			
			String obs = NeoUtils.safeOutputString(wrapper.findValue("motivoReprovacao")) + NeoUtils.safeOutputString(wrapper.findValue("motivoReprovaca2")); 
			
			if (diretorAprovou && presidenteAprovou) {
				wRegistroAtividade.setValue("acao", "Aprovou Visita Escalada - Diretor pelo Presidente");
			}else{
				wRegistroAtividade.setValue("acao", "Reprovou Visita Escalada - Diretor pelo Presidente");
				wRegistroAtividade.setValue("obs2", obs);
			}
			
			Collection<NeoObject> listaRegistros = wrapper.findField("registroHistorico").getValues();
			listaRegistros.add(oRegistroAtividade);
			
			wrapper.setValue("motivoReprovacao", "");
			if (wrapper.findValue("motivoReprovaca2") != null) wrapper.setValue("motivoReprovaca2", "");
		}catch(Exception e){
			e.printStackTrace();
			throw new WorkflowException("Erro ao relatar visita. " + e.getMessage());
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
		
	}

}
