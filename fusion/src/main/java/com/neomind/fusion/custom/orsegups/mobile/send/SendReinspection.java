package com.neomind.fusion.custom.orsegups.mobile.send;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.casvig.mobile.CasvigMobileServlet;
import com.neomind.fusion.custom.orsegups.mobile.engine.CasvigMobilePool;
import com.neomind.fusion.custom.orsegups.mobile.vo.ReinspecaoVO;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.util.NeoUtils;

@WebServlet(name="SendReinspection", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.mobile.send.SendReinspection"})
public class SendReinspection extends HttpServlet{


	private static final long serialVersionUID = 1L;

public String reinspecao(List<CasvigMobilePool> listPool){
	String json = null;
	int iPes = 0;
	ArrayList<Object> reinspecao = new ArrayList<Object>();

	
	for (CasvigMobilePool pool : listPool)
	{
		NeoObject entity = pool.getActivity().getProcess().getEntity();
		try
		{
			EntityWrapper wrapper = new EntityWrapper(entity);

			NeoObject objInspetoria = (NeoObject) wrapper.getValue("relatorioInspecao");
			EntityWrapper wrapperResult = new EntityWrapper(objInspetoria);
			
			
			ReinspecaoVO rvo = new ReinspecaoVO();
			
			rvo.setTipo_inspecao(NeoUtils.safeIsNull(pool.getTypeInspection()) ? "" : pool.getTypeInspection());
			rvo.setCod_task(String.valueOf(pool.getTask().getNeoId()));
			rvo.setMobile_neoId("00000");
			rvo.setCod_postoTrabalho((String) wrapperResult.getValue("codposto"));
			rvo.setNome_empresa((String) wrapperResult.getValue("nomeempresa"));
			rvo.setNome_cliente((String) wrapperResult.getValue("nomecliente"));
			rvo.setNome_lotacao((String) wrapperResult.getValue("nomelotacao"));
			rvo.setNome_postoTrabalho((String) wrapperResult.getValue("nomeposto"));
			rvo.setGestor_cliente((String) wrapperResult.getValue("gestorcliente"));
			rvo.setColaboradorentrevistado((String) wrapperResult.getValue("colaboradorentrevistado"));
			rvo.setFuncao_colaborador((String) wrapperResult.getValue("funccolaborador"));
			rvo.setNome_inspetor((String) wrapperResult.getValue("nomeinspetor"));

			
			if (pool.getTypeInspection().equals("Reinspeção") || pool.getTypeInspection().equals("Reinspecao"))
			{
				rvo.setDesc_perg("Resposta à Inconsistência");
				rvo.setResp_perg((NeoUtils.safeIsNull(wrapper.getValue("respostaInconsistencia"))) ? "" : wrapper.getValue(
						"respostaInconsistencia").toString());
			}
			else
			{
				rvo.setDesc_perg("Resposta à Não-conformidade");
				rvo.setResp_perg(NeoUtils.safeIsNull(wrapper.getValue("respostaNaoConformidade")) ? "" : wrapper.getValue(
						"respostaNaoConformidade").toString());
			}

			rvo.setLogical("null");
			rvo.setCode_perg("0");

	
			int iRes = 1;
			List<NeoObject> listObjResp = ((List<NeoObject>) wrapperResult.findValue("neoidResposta"));
			for (NeoObject respQuery : listObjResp)
			{
				
				EntityWrapper wrapperResp = new EntityWrapper(respQuery);


				if (!wrapperResp.getValue("descperg").equals("Resposta à Inconsistência") || !wrapperResp.getValue("descperg").equals("Resposta a Inconsistencia"))
				{

					rvo.setDesc_perg((String.valueOf(wrapperResp.getValue("descperg"))));
					rvo.setResp_perg((String.valueOf( wrapperResp.getValue("respperg"))));
					rvo.setCode_perg((String.valueOf( wrapperResp.getValue("codeperg"))));
					rvo.setLogical((String.valueOf( wrapperResp.getValue("booleanValor"))));
					iRes++;
				}
			}
			
			iPes++;
		}
		catch (Exception npe)
		{
			System.out.println("IM_Inspecao - neoId = " + entity.getNeoId() + " - neoIdTask = " + pool.getTask().getNeoId() + " error "+ npe);
		}
	}
	
	 
	 Gson gson = new Gson();
	json = gson.toJson(reinspecao);
	
	System.out.println(json);
	
	return json;
	
	
}
	
	
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String msg = null;
		DataOutputStream out = null;

		boolean error = false;
		
			out = new DataOutputStream(response.getOutputStream());

			CasvigMobileServlet.log.info("Mobile request Reinspecao.xml");
			boolean have = false;

			List<CasvigMobilePool> mobilePool = (List<CasvigMobilePool>) PersistEngine.getObjects(CasvigMobilePool.class, QLEqualsFilter.buildFromObject("sendToMobile", true));
			if (mobilePool == null || mobilePool.size() <= 0)
			{
				//MobileUtils.sendString(ERROR, response);
				CasvigMobileServlet.log.info("No have reinspection - " + ((mobilePool == null) ? "null" : String.valueOf(mobilePool.size())));
				return;
			}else{
				response.setContentType("application/json");

				
				try {
					out.writeBytes(reinspecao(mobilePool));

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			
				reinspecao(mobilePool);
			}
			
			

			/*List<CasvigMobilePool> listPool = new ArrayList<CasvigMobilePool>();

			String strListNeoId = MobileUtils.getOldString(request.getParameter("LNId"));
			if (!NeoUtils.safeIsNull(strListNeoId))
				CasvigMobileServlet.log.info(strListNeoId);
			String[] arrayLNid = strListNeoId.split(";");

			NeoUser user = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", request.getParameter("code")));

			for (CasvigMobilePool pool : mobilePool)
			{
				Task task = pool.getTask();

				// se deve enviar a reinspe��o para o mobile
				if (pool.getSendToMobile() 
						// verifica se o mobile j� n�o recebeu esta reinspe��o
						&& !MobileUtils.haveNeoId(arrayLNid, task.getNeoId())
						// verifica se tem alguma reinspe��o para o inspetor solicitante
						&& pool.getUsers().contains(user)) 
				{
					listPool.add(pool);

					// n�o envia mais para o mobile
					/*
					 * fernando.rebelo
					 * n�o setar fara false, para que cada vez que sincronizar o 
					 * mobile trazer todas que o usu�rio n�o possui no mobile
					 
					//deixar sempre pra enviar para mobile, assim usuarios podem baixar as reinspecs mesmo se j� tiverem baixado.
					//pool.setSendToMobile(false); 

					if (!have)
						have = true;
				}
			}

			if (listPool == null || listPool.size() == 0)
			{
				MobileUtils.sendString(ERROR, response);
				CasvigMobileServlet.log.info("No have new reinspection");
				return;
			}

			if (have)
			{
				reinspecao(listPool);

				if (reinspecao(listPool) == null)
				{
					MobileUtils.sendString(ERROR, response);
					CasvigMobileServlet.log.error("XML exception - reinspection");
					return;
				}
				msg = new String("Send to mobile: Reinspecao.xml");

			

				CasvigMobileServlet.log.info("Reinspeção "+msg);
			}
			else
			{
				CasvigMobileServlet.log.error("No have Reinspecao.xml");
				MobileUtils.sendString(ERROR, response);
			}
			return;
		}
		catch (Exception e)
		{
			error = true;
		}
		finally
		{
			if (out != null)
			{
				try
				{
					out.close();
				}
				catch (IOException ioe)
				{
					ioe.printStackTrace();
					error = true;
				}
			}
			if (error)
			{
				if (msg != null)
					CasvigMobileServlet.log.error(msg);
			}
		}
	}

	private static String ERROR = "ERROR";*/
}			
	}
	
