package com.neomind.fusion.custom.orsegups.site;

public enum GraficosEnum
{
	ATENDIMENTOTATICO(1, "Ultimos Atendimentos Taticos","colors:['blue'],title: 'Ultimos Atendimentos Taticos',titleTextStyle:{bold:true,fontSize:14},legend: {position: 'top',textStyle:{bold:true,fontSize:12}},tooltip: {textStyle:{bold:true,fontSize:12}},pointSize: 5,height: 220,hAxis: {title: 'Periodo da ocorrencia',format: 'dd/MM/yyyy', textStyle:{bold:true,fontSize:11},titleTextStyle: {bold:true,italic:false,fontSize:11}},vAxis: {baseline: '0', title: 'Tempo em minutos',format:'# min', textStyle:{bold:true,fontSize:10}, titleTextStyle: {bold:true,italic:false,fontSize:12}},chartArea:{left:60,top:50, bottom:50,width:'100%',height:'120'}"),
	DESVIOHABITO(2, "Ultimos Atendimentos de Desvio de Habito", "colors:['orange'],title: 'Ultimos Atendimentos de Desvio de Habito',titleTextStyle:{bold:true,fontSize:14},legend: {position: 'top',textStyle:{bold:true,fontSize:12}},tooltip: {textStyle:{bold:true,fontSize:12}},pointSize: 5,height: 220,hAxis: {title: 'Periodo da ocorrencia',format: 'dd/MM/yyyy', textStyle:{bold:true,fontSize:11},titleTextStyle: {bold:true,italic:false,fontSize:11}},vAxis: {baseline: '0', title: 'Tempo em minutos',format:'# min', textStyle:{bold:true,fontSize:10}, titleTextStyle: {bold:true,italic:false,fontSize:12}},chartArea:{left:60,top:50, bottom:50,width:'100%',height:'120'}"),
	ORDEMSERVICO(3, "Ultimas Ordens de Servico", "colors:['green'],title: 'Ultimas Ordens de Servico',titleTextStyle:{bold:true,fontSize:14},legend: {position: 'top',textStyle:{bold:true,fontSize:12}},tooltip: {textStyle:{bold:true,fontSize:12}},pointSize: 5,height: 220,hAxis: {title: 'Periodo da ocorrencia',format: 'dd/MM/yyyy', textStyle:{bold:true,fontSize:11},titleTextStyle: {bold:true,italic:false,fontSize:11}},vAxis: {baseline: '0', title: 'Tempo em dias',format:'# dias', textStyle:{bold:true,fontSize:10}, titleTextStyle: {bold:true,italic:false,fontSize:12}},chartArea:{left:60,top:50, bottom:50,width:'100%',height:'120'}");
	
	private int codigo;
	private String descricao;
	private String options;
	
	GraficosEnum( int codigo, String descricao,String options) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.options = options;
	}
	
	public int getCodigo() {
		return codigo;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public String getOptions() {
		return options;
	}
}