package com.neomind.fusion.custom.orsegups.mobile.vo2;

import java.util.ArrayList;

public class inspetoriaPerguntasVO {
	
	private ArrayList<PesquisaVO> pesquisas;
	private Boolean error;
	
	public ArrayList<PesquisaVO> getPesquisas() {
		return pesquisas;
	}
	public void setPesquisas(ArrayList<PesquisaVO> pesquisas) {
		this.pesquisas = pesquisas;
	}
	public Boolean getError() {
		return error;
	}
	public void setError(Boolean error) {
		this.error = error;
	}
	
}
