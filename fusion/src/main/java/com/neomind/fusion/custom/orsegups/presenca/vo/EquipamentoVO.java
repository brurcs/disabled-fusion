package com.neomind.fusion.custom.orsegups.presenca.vo;

import java.util.GregorianCalendar;

public class EquipamentoVO
{
	private PostoVO posto;
	private Long sequencia;
	private String codigoProduto;
	private String descricaoProduto;
	private String tipoProduto;
	private String derivacao;
	private Long quantidade;
	private Long  codigoInstalador;
	private String nomeInstalador;
	private String dataInstalacao;
	private String tipoInstalacao;
	private Long requisicao;
	private Long rmc;
	private String situacaoRequisicao;
	
	public PostoVO getPosto()
	{
		return posto;
	}
	public void setPosto(PostoVO posto)
	{
		this.posto = posto;
	}
	public String getCodigoProduto()
	{
		return codigoProduto;
	}
	public void setCodigoProduto(String codigoProduto)
	{
		this.codigoProduto = codigoProduto;
	}
	public String getDescricaoProduto()
	{
		return descricaoProduto;
	}
	public void setDescricaoProduto(String descricaoProduto)
	{
		this.descricaoProduto = descricaoProduto;
	}
	public String getDerivacao()
	{
		return derivacao;
	}
	public void setDerivacao(String derivacao)
	{
		this.derivacao = derivacao;
	}
	public Long getQuantidade()
	{
		return quantidade;
	}
	public void setQuantidade(Long quantidade)
	{
		this.quantidade = quantidade;
	}
	public Long getCodigoInstalador()
	{
		return codigoInstalador;
	}
	public void setCodigoInstalador(Long codigoInstalador)
	{
		this.codigoInstalador = codigoInstalador;
	}
	public String getNomeInstalador()
	{
		return nomeInstalador;
	}
	public void setNomeInstalador(String nomeInstalador)
	{
		this.nomeInstalador = nomeInstalador;
	}
	public String getDataInstalacao()
	{
		return dataInstalacao;
	}
	public void setDataInstalacao(String dataInstalacao)
	{
		this.dataInstalacao = dataInstalacao;
	}
	public String getTipoInstalacao()
	{
		return tipoInstalacao;
	}
	public void setTipoInstalacao(String tipoInstalacao)
	{
		this.tipoInstalacao = tipoInstalacao;
	}
	public Long getRequisicao()
	{
		return requisicao;
	}
	public void setRequisicao(Long requisicao)
	{
		this.requisicao = requisicao;
	}
	public Long getRmc()
	{
		return rmc;
	}
	public void setRmc(Long rmc)
	{
		this.rmc = rmc;
	}
	public String getSituacaoRequisicao()
	{
		return situacaoRequisicao;
	}
	public void setSituacaoRequisicao(String situacaoRequisicao)
	{
		this.situacaoRequisicao = situacaoRequisicao;
	}
	public Long getSequencia()
	{
		return sequencia;
	}
	public void setSequencia(Long sequencia)
	{
		this.sequencia = sequencia;
	}
	public String getTipoProduto()
	{
		return tipoProduto;
	}
	public void setTipoProduto(String tipoProduto)
	{
		this.tipoProduto = tipoProduto;
	}
	
}
