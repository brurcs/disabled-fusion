package com.neomind.fusion.custom.orsegups.callcenter;

import java.util.Collection;
import java.util.List;

public class SearchObjectVO
{
	//Valores: quickSearch, advancedSearch, snepSearch
	private String tipoBusca;

	private String termoGenerico;

	private String codigoClienteSearch;

	private String razaoSearch;

	private String cpfCnpjSearch;

	private String telefoneSearch;

	private String enderecoSearch;

	private String bairroSearch;

	private String cidadeSearch;

	private String ufSearch;

	private String cepSearch;
	
	private String emailSearch;

	private List<String> codigosSigma;

	private List<String> codigosSapiens;

	public String getTipoBusca()
	{
		return tipoBusca;
	}

	public void setTipoBusca(String tipoBusca)
	{
		this.tipoBusca = tipoBusca;
	}

	public String getTermoGenerico()
	{
		return termoGenerico;
	}

	public void setTermoGenerico(String termoGenerico)
	{
		this.termoGenerico = termoGenerico;
	}

	public String getCodigoClienteSearch()
	{
		return codigoClienteSearch;
	}

	public void setCodigoClienteSearch(String codigoClienteSearch)
	{
		this.codigoClienteSearch = codigoClienteSearch;
	}

	public String getRazaoSearch()
	{
		return razaoSearch;
	}

	public void setRazaoSearch(String razaoSearch)
	{
		this.razaoSearch = razaoSearch;
	}

	public String getCpfCnpjSearch()
	{
		return cpfCnpjSearch;
	}

	public void setCpfCnpjSearch(String cpfCnpjSearch)
	{
		this.cpfCnpjSearch = cpfCnpjSearch;
	}

	public String getTelefoneSearch()
	{
		if (telefoneSearch != null && !telefoneSearch.trim().isEmpty())
		{
			return this.telefoneSearch;
		}
		else if (getTermoLong(this.termoGenerico) != null && !this.termoGenerico.trim().isEmpty())
		{
			return this.termoGenerico;
		}
		else
		{
			return null;
		}
	}

	public void setTelefoneSearch(String telefoneSearch)
	{
		this.telefoneSearch = telefoneSearch;
	}

	public String getEnderecoSearch()
	{
		return enderecoSearch;
	}

	public void setEnderecoSearch(String enderecoSearch)
	{
		this.enderecoSearch = enderecoSearch;
	}

	public String getBairroSearch()
	{
		return bairroSearch;
	}

	public void setBairroSearch(String bairroSearch)
	{
		this.bairroSearch = bairroSearch;
	}

	public String getCidadeSearch()
	{
		return cidadeSearch;
	}

	public void setCidadeSearch(String cidadeSearch)
	{
		this.cidadeSearch = cidadeSearch;
	}

	public String getUfSearch()
	{
		return ufSearch;
	}

	public void setUfSearch(String ufSearch)
	{
		this.ufSearch = ufSearch;
	}

	public String getCepSearch()
	{
		return cepSearch;
	}

	public void setCepSearch(String cepSearch)
	{
		this.cepSearch = cepSearch;
	}

	public List<String> getCodigosSigma()
	{
		return codigosSigma;
	}

	public void setCodigosSigma(List<String> codigosSigma)
	{
		this.codigosSigma = codigosSigma;
	}

	public List<String> getCodigosSapiens()
	{
		return codigosSapiens;
	}

	public void setCodigosSapiens(List<String> codigosSapiens)
	{
		this.codigosSapiens = codigosSapiens;
	}

	public Boolean canSearch()
	{
		Boolean canSearch = false;

		if (this.tipoBusca != null)
		{
			if (this.tipoBusca.equalsIgnoreCase("quickSearch") && this.termoGenerico != null && !this.termoGenerico.isEmpty())
			{
				canSearch = true;
			}
			else if (this.tipoBusca.equalsIgnoreCase("advancedSearch"))
			{
				if (this.codigoClienteSearch != null && !this.codigoClienteSearch.isEmpty())
				{
					canSearch = true;
				}
				else if (this.razaoSearch != null && !this.razaoSearch.isEmpty())
				{
					canSearch = true;
				}
				else if (this.cpfCnpjSearch != null && !this.cpfCnpjSearch.isEmpty())
				{
					canSearch = true;
				}
				else if (this.telefoneSearch != null && !this.telefoneSearch.isEmpty())
				{
					canSearch = true;
				}
				else if (this.enderecoSearch != null && !this.enderecoSearch.isEmpty())
				{
					canSearch = true;
				}
				else if (this.bairroSearch != null && !this.bairroSearch.isEmpty())
				{
					canSearch = true;
				}
				else if (this.cidadeSearch != null && !this.cidadeSearch.isEmpty())
				{
					canSearch = true;
				}
				else if (this.ufSearch != null && !this.ufSearch.isEmpty())
				{
					canSearch = true;
				}
				else if (this.cepSearch != null && !this.cepSearch.isEmpty())
				{
					canSearch = true;
				}
				else if (this.emailSearch != null && !this.emailSearch.isEmpty())
				{
					canSearch = true;
				}
			}
			else if (this.tipoBusca.equalsIgnoreCase("snepSearch") && this.telefoneSearch != null && !this.telefoneSearch.isEmpty())
			{
				canSearch = true;
			}
		}

		return canSearch;
	}

	public static Long getTermoLong(String termo)
	{
		Long termoLong = null;

		try
		{
			termoLong = Long.valueOf(termo);
		}
		catch (NumberFormatException e)
		{

		}

		return termoLong;
	}

	public static String getCodigosSigmaInFormtat(Collection<String> codigos)
	{
		String result = "";

		if (codigos != null && !codigos.isEmpty())
		{
			Boolean first = true;

			for (String codigo : codigos)
			{
				if (first)
				{
					result = codigo;
					first = false;
				}
				else
				{
					result += "," + codigo;
				}
			}
		}

		return result;
	}

	public String getEmailSearch()
	{
		return emailSearch;
	}

	public void setEmailSearch(String emailSearch)
	{
		this.emailSearch = emailSearch;
	}

}
