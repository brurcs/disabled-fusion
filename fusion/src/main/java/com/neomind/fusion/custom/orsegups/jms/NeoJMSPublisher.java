package com.neomind.fusion.custom.orsegups.jms;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;

public final class NeoJMSPublisher
{
  private static NeoJMSPublisher neoJMSPublisher;
  private static InitialContext initialContext;
  private static Topic topic;
  private static Topic mapAlertAitTopic;
  private static Topic alertEventTopic;
  private static Topic alertEventVideoTopic;
  private static TopicConnection topicConnection;
  private static final Log log = LogFactory.getLog(NeoJMSPublisher.class);
//  private static Logger logger = Logger.getLogger(NeoJMSPublisher.class);
  
  private NeoJMSPublisher() {}
  
  public static NeoJMSPublisher getInstance()
  {
    if (neoJMSPublisher == null)
    {
      //SimpleLayout layout = new SimpleLayout();
      //FileAppender appender = null;
      //GregorianCalendar dataAtual = new GregorianCalendar();
      //SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh-mm");
      //try
      //{
       // appender = new FileAppender(layout, "C:/temp/jmsLog" + sdf.format(dataAtual.getTime()).toString() + ".log", false);
      //}
      //catch (IOException e)
      //{
      //  e.printStackTrace();
      //}
//      logger.addAppender(appender);
//      logger.setLevel(Level.DEBUG);
      
      neoJMSPublisher = new NeoJMSPublisher();
      TopicConnectionFactory topicConnectionFactory = null;
      
      try
      {
        initialContext = new InitialContext();
        
        topicConnectionFactory = (TopicConnectionFactory)initialContext.lookup("java:comp/env/jms/TopicConnectionFactory");
       
        topic = (Topic)initialContext.lookup("java:comp/env/jms/messageTopic");
        
        mapAlertAitTopic = (Topic)initialContext.lookup("java:comp/env/jms/mapAlertAitTopic");
        
        alertEventTopic = (Topic)initialContext.lookup("java:comp/env/jms/alertEventTopic");
        
        alertEventVideoTopic = (Topic)initialContext.lookup("java:comp/env/jms/alertEventVideoTopic");
      }
      catch (NamingException e)
      {
    	log.error(e.getMessage());  
        e.printStackTrace();
        //if (log.isDebugEnabled()) {
        //  log.debug(e.getMessage());
        //}
      }
      catch (Exception e) {
    	  log.error(e.getMessage());
        e.printStackTrace();
        //if (log.isDebugEnabled()) {
        //  log.debug(e.getMessage());
        //}
      }
      
      try
      {
        topicConnection = topicConnectionFactory.createTopicConnection();

      }
      catch (JMSException e)
      {
        e.printStackTrace();
      }
      catch (Exception e)
      {
    	  log.error(e.getMessage());
        e.printStackTrace();
        //if (log.isDebugEnabled()) {
        //  log.debug(e.getMessage());
        //}
      }
    }
    if (topicConnection == null)
    {
      try
      {
        if (initialContext != null)
        {
          TopicConnectionFactory topicConnectionFactory = (TopicConnectionFactory)initialContext.lookup("java:comp/env/jms/TopicConnectionFactory");
          topicConnection = topicConnectionFactory.createTopicConnection();
        }
        else
        {
          neoJMSPublisher = null;
          getInstance();
        }
      }
      catch (JMSException e)
      {
    	log.error("Erro ao criar o topico do JMS.");
        e.printStackTrace();
        //if (log.isDebugEnabled()) {
        //  log.debug("Erro ao criar o topico do JMS.");
        //}
      } catch (NamingException e) {
    	  log.error("Erro ao criar o topico do JMS com o nome 'java:comp/env/jms/TopicConnectionFactory'.");
    	e.printStackTrace();
    	
        //if (log.isDebugEnabled()) {
        // log.debug("Erro ao criar o topico do JMS com o nome 'java:comp/env/jms/TopicConnectionFactory'.");
        //}
      } catch (Exception e) {
    	  log.error("Erro ao criar o topico do JMS.");
        e.printStackTrace();
        //if (log.isDebugEnabled()) {
        //  log.debug("Erro ao criar o topico do JMS.");
        //}
      }
    }
    
    return neoJMSPublisher;
  }
  

  public void sendCallCenterMessage(String message, String selector)
  {
    try
    {
      GregorianCalendar dataAtual = new GregorianCalendar();
      SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//      logger.info("########################################################");
//      logger.info("##### Hora do Evento: " + sdf.format(dataAtual.getTime()).toString());
//      logger.info("Tipo: sendCallCenterMessage");
//      logger.info("Mensagem: " + message);
//      logger.info("Seletor: " + selector);
      
      topicConnection = null;
      getTopicConnection();
      
      TopicSession topicSession = topicConnection.createTopicSession(false, 1);
      TopicPublisher topicPublisher = topicSession.createPublisher(topic);
      ObjectMessage messageContent = topicSession.createObjectMessage();
      messageContent.setStringProperty("identifier", selector);
      messageContent.setObject(message);
      topicPublisher.publish(messageContent);
      
      topicPublisher.close();
//      logger.info("Fim processamento");
//      logger.info("########################################################");
    }
    catch (NullPointerException nullPointer)
    {
      log.error("Erro ao buscar instancia da sessao.");
      nullPointer.printStackTrace();
      //if (log.isDebugEnabled()) {
      //  log.debug("Erro ao buscar instancia da sessao.");
      //}

if (topicConnection == null) {}
    }
    catch (JMSException e)
    {
    	log.error("JMSException ao publicar mensagem no sendCallCenterMessage. " + e.getMessage() + e.getStackTrace());
      e.printStackTrace();
      //if (log.isDebugEnabled()) {
      //  log.debug("JMSException ao publicar mensagem no sendCallCenterMessage. " + e.getMessage() + e.getStackTrace());
    //}

if (topicConnection == null) {}
    }
    catch (Exception e)
    {
      log.error(e.getMessage());
      e.printStackTrace();
      //if (log.isDebugEnabled()) {
      //  log.debug(e.getMessage());
      //}
      
if (topicConnection == null) {} } finally { if (topicConnection != null) {
        try
        {
          topicConnection.close();
        }
        catch (JMSException e) {
          if (log.isDebugEnabled()) {
            log.debug("Closing error: " + e.toString());
          }
        }
      }
    }
  }
  
  public void sendAlertAitMessage(String message, String selector)
  {
    try
    {
      GregorianCalendar dataAtual = new GregorianCalendar();
      SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//      logger.info("########################################################");
//      logger.info("##### Hora do Evento: " + sdf.format(dataAtual.getTime()).toString());
//      logger.info("Tipo: sendAlertAitMessage");
//      logger.info("Mensagem: " + message);
//      logger.info("Seletor: " + selector);
      
      topicConnection = null;
      getTopicConnection();
      
      TopicSession topicSession = topicConnection.createTopicSession(false, 1);
      TopicPublisher topicPublisher = topicSession.createPublisher(mapAlertAitTopic);
      ObjectMessage messageContent = topicSession.createObjectMessage();
      messageContent.setStringProperty("identifier", selector);
      messageContent.setObject(message);
      topicPublisher.publish(messageContent);
      
      topicPublisher.close();
//      logger.info("Fim processamento");
//      logger.info("########################################################");
    }
    catch (NullPointerException nullPointer)
    {
   	  log.error("Erro ao buscar instancia da sessao.");
      nullPointer.printStackTrace();
      //if (log.isDebugEnabled()) {
      //  log.debug("Erro ao buscar instancia da sessao.");
      //}

if (topicConnection == null) {}
    }
    catch (JMSException e)
    {
    	log.error("JMSException ao publicar mensagem no sendAlertAitMessage. " + e.getMessage() + e.getStackTrace());
      e.printStackTrace();
      //if (log.isDebugEnabled()) {
      //  log.debug("JMSException ao publicar mensagem no sendAlertAitMessage. " + e.getMessage() + e.getStackTrace());
     //}
      
if (topicConnection == null) {}
    }
    catch (Exception e)
    {
      log.error(e.getMessage());
      e.printStackTrace();
      //if (log.isDebugEnabled()) {
      //  log.debug(e.getMessage());
      //}
      
      if (topicConnection == null) {} } finally { if (topicConnection != null) {
        try
        {
          topicConnection.close();
        }
        catch (JMSException e) {
        	log.error("Closing error: " + e.toString());
          //if (log.isDebugEnabled()) {
          //  log.debug("Closing error: " + e.toString());
          //}
        }
      }
    }
  }
  
  public void sendAlertEventMessage(String message, String selector)
  {
    try
    {
      GregorianCalendar dataAtual = new GregorianCalendar();
      SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//      logger.info("########################################################");
//      logger.info("##### Hora do Evento: " + sdf.format(dataAtual.getTime()).toString());
//      logger.info("Tipo: sendAlertEventMessage");
//      logger.info("Mensagem: " + message);
//      logger.info("Seletor: " + selector);
      
      topicConnection = null;
      getTopicConnection();
      
      TopicSession topicSession = topicConnection.createTopicSession(false, 1);
      TopicPublisher topicPublisher = topicSession.createPublisher(alertEventTopic);
      ObjectMessage messageContent = topicSession.createObjectMessage();
      messageContent.setStringProperty("identifier", selector);
      messageContent.setObject(message);
      topicPublisher.publish(messageContent);
      
      topicPublisher.close();
//      logger.info("Fim processamento");
//      logger.info("########################################################");
    }
    catch (NullPointerException nullPointer)
    {
      log.error("Erro ao buscar instancia da sessao.");
      nullPointer.printStackTrace();
      //if (log.isDebugEnabled()) {
      //  log.debug("Erro ao buscar instancia da sessao.");
      //}
      
if (topicConnection == null) {}
    }
    catch (JMSException e)
    {
      log.error("JMSException ao publicar mensagem no sendAlertEventMessage. " + e.getMessage() + e.getStackTrace());
      e.printStackTrace();
      //if (log.isDebugEnabled()) {
      //  log.debug("JMSException ao publicar mensagem no sendAlertEventMessage. " + e.getMessage() + e.getStackTrace());
      //}
      
if (topicConnection == null) {}
    }
    catch (Exception e)
    {
      log.error(e.getMessage());
      e.printStackTrace();
      //if (log.isDebugEnabled()) {
      //  log.debug(e.getMessage());
      //}
      
      if (topicConnection == null) {} } finally { if (topicConnection != null) {
        try
        {
          topicConnection.close();
        }
        catch (JMSException e) {
        	log.error("Closing error: " + e.toString());	
          //if (log.isDebugEnabled()) {
          //  log.debug("Closing error: " + e.toString());
          //}
        }
      }
    }
  }
  
  public void sendAlertEventVideoMessage(String message, String selector)
  {
    try
    {
      GregorianCalendar dataAtual = new GregorianCalendar();
      SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//      logger.info("########################################################");
//      logger.info("##### Hora do Evento: " + sdf.format(dataAtual.getTime()).toString());
//      logger.info("Tipo: sendAlertEventVideoMessage");
//      logger.info("Mensagem: " + message);
//      logger.info("Seletor: " + selector);
      
      topicConnection = null;
      getTopicConnection();
      
      TopicSession topicSession = topicConnection.createTopicSession(false, 1);
      TopicPublisher topicPublisher = topicSession.createPublisher(alertEventVideoTopic);
      ObjectMessage messageContent = topicSession.createObjectMessage();
      messageContent.setStringProperty("identifier", selector);
      messageContent.setObject(message);
      topicPublisher.publish(messageContent);
      
      topicPublisher.close();
//      logger.info("Fim processamento");
//      logger.info("########################################################");
    }
    catch (NullPointerException nullPointer)
    {
      nullPointer.printStackTrace();
      log.error("Erro ao buscar instancia da sessao.");
if (topicConnection == null) {}
    }
    catch (JMSException e)
    {
    	log.error("JMSException ao publicar mensagem no sendAlertEventVideoMessage. " + e.getMessage() + e.getStackTrace());
      e.printStackTrace();
      //if (log.isDebugEnabled()) {
      //  log.debug("JMSException ao publicar mensagem no sendAlertEventVideoMessage. " + e.getMessage() + e.getStackTrace());
    //}

if (topicConnection == null) {}
    }
    catch (Exception e)
    {
    	log.error(e.getMessage());
      e.printStackTrace();
      //if (log.isDebugEnabled()) {
      //  log.debug(e.getMessage());
    //}
      
      if (topicConnection == null) {} } finally { if (topicConnection != null) {
        try
        {
          topicConnection.close();
        }
        catch (JMSException e) {
          log.error("Closing error: " + e.toString());
        	//if (log.isDebugEnabled()) {
          //  log.debug("Closing error: " + e.toString());
         // }
        }
      }
    }
  }
  
  public TopicConnection getTopicConnection() {
    if (topicConnection == null)
    {
      try
      {
        if (initialContext != null)
        {
          TopicConnectionFactory topicConnectionFactory = (TopicConnectionFactory)initialContext.lookup("java:comp/env/jms/TopicConnectionFactory");
          topicConnection = topicConnectionFactory.createTopicConnection();
        }
      }
      catch (NamingException e)
      {
     	  log.error("Erro ao criar o topico do JMS com o nome 'java:comp/env/jms/TopicConnectionFactory'.");
        e.printStackTrace();
        //if (log.isDebugEnabled()) {
        //  log.debug("Erro ao criar o topico do JMS com o nome 'java:comp/env/jms/TopicConnectionFactory'.");
        //}
      } catch (Exception e) {
    	log.error("Erro ao criar o topico do JMS.");
        e.printStackTrace();
        //if (log.isDebugEnabled()) {
        //  log.debug("Erro ao criar o topico do JMS.");
        //}
      }
    }
    if (topicConnection == null)
    {

      neoJMSPublisher = null;
      getInstance();
    }
    
    return topicConnection;
  }
}