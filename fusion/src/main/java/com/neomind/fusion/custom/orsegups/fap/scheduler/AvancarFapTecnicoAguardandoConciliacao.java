package com.neomind.fusion.custom.orsegups.fap.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.common.NeoRunnable;
import com.neomind.fusion.custom.orsegups.fap.utils.FAPParametrizacao;
import com.neomind.fusion.custom.orsegups.utils.FAPAgrupadorVO;
import com.neomind.fusion.custom.orsegups.utils.FornecedorVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.UserActivity;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

/**
 * @author diogo.silva
 *         Esta classe tem por objetivo avançar todas as FAP's Técnicos que estão na atividade
 *         "AGUARDANDO CONCILIAÇÃO - FAP TÉCNICO".
 *         O JOB que chama este adapter é executado no dia 05 de cada mês.
 * */
public class AvancarFapTecnicoAguardandoConciliacao implements CustomJobAdapter
{

	private static final Log log = LogFactory.getLog(AvancarFapTecnicoAguardandoConciliacao.class);

	@Override
	public void execute(CustomJobContext ctx)
	{
		Connection conn = null;
		StringBuilder sqlFapPorFornecedor = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String dataCorteString = null;

		Long key = GregorianCalendar.getInstance().getTimeInMillis();

		try
		{
			log.warn("##### INICIAR PROCESSO DE FINALIZAÇÃO DAS TAREFAS EM FASE DE CONCILIAÇÃO DE PAGAMENTO (FAP TÉCNICO) - Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));

			conn = PersistEngine.getConnection("");

			List<FornecedorVO> lstFornecedor = new ArrayList<FornecedorVO>();
			sqlFapPorFornecedor.append(" SELECT FORNEC.CODFOR FROM D_FAPAplicacaoTecnico AS FAPTECNICO WITH (NOLOCK) ");
			sqlFapPorFornecedor.append(" INNER JOIN X_ETABFOR AS XFOR WITH (NOLOCK) ON FAPTECNICO.fornecedor_neoId = XFOR.neoId        ");
			sqlFapPorFornecedor.append(" INNER JOIN [CACHOEIRA\\SQL01].SAPIENS.DBO.E095FOR AS FORNEC ON XFOR.codfor = FORNEC.CODFOR    ");
			//sqlFapPorFornecedor.append(" WHERE FORNEC.CODFOR = 72281"); // Retirar em produção

			//Abre Conexão com o Sapiens para consultar os fornecedores com dataCorte (getDate() - 1))
			pstm = conn.prepareStatement(sqlFapPorFornecedor.toString());
			rs = pstm.executeQuery();

			while (rs.next())
			{
				FornecedorVO fornecedorVO = new FornecedorVO();
				fornecedorVO.setCodigoFornecedor(rs.getLong("CODFOR"));
				lstFornecedor.add(fornecedorVO);
			}

			OrsegupsUtils.closeConnection(conn, pstm, rs);
			//Fecha Conexão com o Sapiens para consultar os fornecedores com dataCorte (getDate() - 1))

			if (lstFornecedor != null && !lstFornecedor.isEmpty())
			{
				for (FornecedorVO forFornecedorVO : lstFornecedor)
				{
					GregorianCalendar dtCorte = new GregorianCalendar();
					/* AS FAPS TÉCNICOS VÃO AGRUPAR FIXO SEMPRE NO DIA 05 DE CADA MÊS */
					dtCorte.set(Calendar.DAY_OF_MONTH, Integer.valueOf(FAPParametrizacao.findParameter("diaCorte", true)));
					//dtCorte.add(Calendar.MONTH, 1);
					dtCorte.set(Calendar.HOUR_OF_DAY, 23);
					dtCorte.set(Calendar.MINUTE, 59);
					dtCorte.set(Calendar.SECOND, 59);

					boolean debug = (FAPParametrizacao.findParameter("debug").equals("1") ? true : false);
					if (debug)
					{ //Local	    
						dataCorteString = NeoDateUtils.safeDateFormat(dtCorte, "dd-MM-yyyy HH:mm:ss");
					}
					else
					{ // Produção
						dataCorteString = NeoDateUtils.safeDateFormat(dtCorte, "yyyy-MM-dd HH:mm:ss");
					}

					if (NeoUtils.safeIsNotNull(dataCorteString))
					{
						AvançarFapAgrupadoraeFilhas(forFornecedorVO.getCodigoFornecedor(), dataCorteString);
					}

				}
			}
		}
		catch (Exception e)
		{
			log.error("##### ERRO AGENDADOR DE TAREFA: FAP TÉCNICO - Avanca Tarefas Aguardando Conciliacao");
			System.out.println("[" + key + "] ##### ERRO AGENDADOR DE TAREFA: FAP TÉCNICO - Avanca Tarefas Aguardando Conciliacao");
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				log.error("##### ERRO AO INICIAR O PROCESSO DE FINALIZAÇÃO DAS TAREFAS EM FASE DE CONCILIAÇÃO DE PAGAMENTO (FAP TÉCNICO) - " + "Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
				e.printStackTrace();
			}

			log.warn("##### FIM DO PROCESSO DE FINALIZAÇÃO DAS TAREFAS EM FASE DE CONCILIAÇÃO DE PAGAMENTO (FAP TÉCNICO) - " + "Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		}
	}

	public static void AvançarFapAgrupadoraeFilhas(Long codFor, String dataCorte)
	{
		Connection connTarefasFap = null;
		StringBuilder sqlFapsPorFornecedor = new StringBuilder();
		PreparedStatement pstmFapsPorFornecedor = null;
		ResultSet rsFapsPorFornecedor = null;

		try
		{
			log.warn("##### INICIANDO O AVANÇO DAS FAPS AGRUPADAS - Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));

			connTarefasFap = PersistEngine.getConnection("");

			List<FAPAgrupadorVO> fapsagrupadoras = new ArrayList<FAPAgrupadorVO>();
			sqlFapsPorFornecedor.append(" SELECT WF.CODE, FAPAPL.CODIGO, FAP.VALORTOTAL FROM D_FAPAUTORIZACAODEPAGAMENTO FAP WITH (NOLOCK)         ");
			sqlFapsPorFornecedor.append(" INNER JOIN D_FAPAplicacaoDoPagamento FAPAPL WITH (NOLOCK) ON FAP.aplicacaoPagamento_neoId = FAPAPL.neoId ");
			sqlFapsPorFornecedor.append(" INNER JOIN X_ETABFOR FORN WITH (NOLOCK) ON FORN.NEOID = FAP.FORNECEDOR_NEOID 		                   ");
			sqlFapsPorFornecedor.append(" INNER JOIN WFPROCESS WF WITH (NOLOCK) ON FAP.WFPROCESS_NEOID = WF.NEOID 			           ");
			sqlFapsPorFornecedor.append(" INNER JOIN ACTIVITY A WITH (NOLOCK) ON WF.NEOID = A.PROCESS_NEOID 				           ");
			sqlFapsPorFornecedor.append(" LEFT JOIN ACTIVITYMODEL AM WITH (NOLOCK) ON AM.NEOID = A.MODEL_NEOID 				           ");
			sqlFapsPorFornecedor.append(" WHERE WF.PROCESSSTATE = 0 AND									           ");
			sqlFapsPorFornecedor.append(" 	    WF.SAVED = 1 AND										   ");
			sqlFapsPorFornecedor.append("       FAP.APROVADO = 1 AND									           ");
			sqlFapsPorFornecedor.append("       FAP.DATASOLICITACAO >= '2015-07-01 00:00:00.000' AND              			           ");
			sqlFapsPorFornecedor.append("       FORN.CODFOR = " + codFor + " AND                                   			           ");
			sqlFapsPorFornecedor.append("       A.STARTDATE >= '2015-07-01 00:00:00.000' AND					  	           ");
			sqlFapsPorFornecedor.append("       A.STARTDATE <= '" + dataCorte + "' AND							           ");
			sqlFapsPorFornecedor.append("       A.FINISHDATE IS NULL AND									   ");
			sqlFapsPorFornecedor.append("       AM.NAME = 'AGUARDANDO CONCILIAÇÃO - FAP TÉCNICO' AND						   ");
			sqlFapsPorFornecedor.append("       FAPAPL.CODIGO = 10								                   ");
			sqlFapsPorFornecedor.append(" ORDER BY WF.CODE ASC										           ");

			//Abre Conexão com o Fusion para consultar as tarefas em fase de "Aguardando Conciliação" para agrupa-las
			pstmFapsPorFornecedor = connTarefasFap.prepareStatement(sqlFapsPorFornecedor.toString());
			rsFapsPorFornecedor = pstmFapsPorFornecedor.executeQuery();

			while (rsFapsPorFornecedor.next())
			{
				FAPAgrupadorVO faps = new FAPAgrupadorVO();
				faps.setCodigoProcesso(rsFapsPorFornecedor.getString("code"));
				faps.setValorTotal(rsFapsPorFornecedor.getBigDecimal("valortotal"));
				fapsagrupadoras.add(faps);
			}

			OrsegupsUtils.closeConnection(connTarefasFap, pstmFapsPorFornecedor, rsFapsPorFornecedor);
			//Fecha Conexão com o Fusion para consultar as tarefas em fase de "Aguardando Conciliação" para agrupa-las

			if (fapsagrupadoras != null && !fapsagrupadoras.isEmpty())
			{
				FAPAgrupadorVO agrupadorVO = fapsagrupadoras.get(fapsagrupadoras.size() - 1);
				final String codigoTarefaAgrupadora = agrupadorVO.getCodigoProcesso();

				for (final FAPAgrupadorVO fapAgrupadorVO : fapsagrupadoras)
				{
					final NeoRunnable work = new NeoRunnable()
					{
						public void run() throws Exception
						{
							//Código da sub-transação, exemplo da classe AvançarFAPAguardandoConciliacao
							if (fapAgrupadorVO.getCodigoProcesso().equals(codigoTarefaAgrupadora))
							{
								AvançarFapAgrupadora(fapAgrupadorVO.getCodigoProcesso(), true, codigoTarefaAgrupadora);
							}
							else
							{
								AvançarFapAgrupadora(fapAgrupadorVO.getCodigoProcesso(), false, codigoTarefaAgrupadora);
							}
						}
					};
					try
					{
						PersistEngine.managedRun(work);
					}
					catch (final Exception e)
					{
						log.error(e.getMessage(), e);
					}
				}
			}
		}
		catch (Exception e)
		{
			log.error("##### ERRO AO AVANÇAR AS TAREFAS DE FAPS AGRUPADAS - " + "Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
			e.printStackTrace();
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(connTarefasFap, pstmFapsPorFornecedor, rsFapsPorFornecedor);
			}
			catch (Exception e)
			{
				log.error("##### ERRO AO AVANÇAR AS TAREFAS DE FAPS AGRUPADAS - " + "Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
				e.printStackTrace();
			}
			log.warn("##### FINALIZANDO O AVANÇO DAS FAPS AGRUPADAS - " + "Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		}
	}

	public static void AvançarFapAgrupadora(String codigoTarefa, Boolean fapAgrupadora, String codigoTarefaAgrupadora)
	{
		NeoUser objSolicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "sistemaFusion"));
		if (NeoUtils.safeIsNull(objSolicitante))
		{
			log.error("Erro #1 - Solicitante não encontrado - Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		}

		WFProcess processo = null;
		NeoObject wkfTarefaSimples = null;
		EntityWrapper ewWkfTarefaSimples = null;

		processo = (WFProcess) PersistEngine.getNeoObject(WFProcess.class, new QLRawFilter("code = '" + codigoTarefa + "' and saved = 1 and processstate = 0 and model.name = 'F001 - FAP - AutorizacaoDePagamento'"));
		wkfTarefaSimples = processo.getEntity();
		ewWkfTarefaSimples = new EntityWrapper(wkfTarefaSimples);

		if (fapAgrupadora == true)
		{
			ewWkfTarefaSimples.setValue("fapAgrupadora", true);
			ewWkfTarefaSimples.setValue("codigoTarefa", codigoTarefaAgrupadora);
		}
		else
		{
			ewWkfTarefaSimples.setValue("fapAgrupadora", false);
			ewWkfTarefaSimples.setValue("codigoTarefa", codigoTarefaAgrupadora);
		}

		// Trata a primeira tarefa
		@SuppressWarnings("unused")
		Task task = null;
		final List<Activity> acts = processo.getOpenActivities();

		List<Activity> activity1 = (List<Activity>) acts;
		for (Activity activity2 : activity1)
		{
			System.out.println("Avançando a Tarefa: " + ewWkfTarefaSimples.findValue("wfprocess.code") + " para a próxima atividade!");
			if (activity2.getActivityName().equals("Aguardando Conciliação - Fap Técnico"))
			{
				if (activity2 instanceof UserActivity)
				{
					try
					{
						if (((UserActivity) activity2).getTaskList().size() <= 0)
						{
							task = activity2.getModel().getTaskAssigner().assign((UserActivity) activity2, objSolicitante);
							OrsegupsWorkflowHelper.finishTaskByActivity(activity2);
						}
						else
						{
							task = ((UserActivity) activity2).getTaskList().get(0);
							OrsegupsWorkflowHelper.finishTaskByActivity(activity2);
						}
					}
					catch (Exception e)
					{
						log.error("Erro #2 - Erro ao avançar a primeira tarefa - " + "Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
						e.printStackTrace();
					}
				}
			}
		}
	}
}
