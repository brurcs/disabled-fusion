package com.neomind.fusion.custom.orsegups.ti.sigma.beans;

import java.util.ArrayList;
import java.util.List;

public class BufferCount {
    
    private int count;
    
    private int countAtraso;
    
    public int getCountAtraso() {
        return countAtraso;
    }
    public void setCountAtraso(int countAtraso) {
        this.countAtraso = countAtraso;
    }
    private List<Recepcao> listaRecepcao = new ArrayList<Recepcao>();
    
    public int getCount() {
        return count;
    }
    public void setCount(int count) {
        this.count = count;
    }
    public List<Recepcao> getListaRecepcao() {
        return listaRecepcao;
    }
    public void setListaRecepcao(List<Recepcao> listaRecepcao) {
        this.listaRecepcao = listaRecepcao;
    }
    
    
    
}
