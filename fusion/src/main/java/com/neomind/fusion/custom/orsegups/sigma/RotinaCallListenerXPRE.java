package com.neomind.fusion.custom.orsegups.sigma;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class RotinaCallListenerXPRE
{
	private static final String URL_FUSION = "http://intranet.orsegups.com.br/";
	private static final Log log = LogFactory.getLog(RotinaCallListenerXPRE.class);
	private static RotinaCallListenerXPRE instancia;
	
	
	public static synchronized RotinaCallListenerXPRE getInstancia()
	{
		if(instancia == null)
		{
			instancia = new RotinaCallListenerXPRE();
		}
		return instancia;
	}
	
	private RotinaCallListenerXPRE()
	{
		executaRotinaXPRE();
	}
	
	public static void executaRotinaXPRE()
	{

		try
		{

			Thread rotinaEventosMonitoramentoCorporativo = new Thread()
			{
				public void run()
				{
					while (true)
					{
						System.out.println("callxpre");
						executaURL("fusion/custom/jsp/orsegups/RotinaDisparaEventoXPRE.jsp?act=testex&t="+GregorianCalendar.getInstance().getTimeInMillis());
						try
						{
							Thread.sleep(10000);
						}
						catch (InterruptedException e)
						{
							System.err.println("Erro ao executar XPRE");
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
				}
			};
			rotinaEventosMonitoramentoCorporativo.start();

			log.debug(" Executando rotina de XPRE ");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error(" Executando rotina de XPRE ");
		}
		finally
		{

		}

	}
	
	
	public static void executaURL(String URL)
	{
		int code = 0;
		InputStream buffer = null;
		InputStream in = null;
		Reader r = null;
		HttpURLConnection uc = null;
		try
		{
			log.debug(" Executando URL: " + URL_FUSION + URL);
			StringBuffer urlTarefa = new StringBuffer();
			urlTarefa.append(URL_FUSION + URL);
			URL url = new URL(urlTarefa.toString());
	        uc = (HttpURLConnection) url.openConnection();
	        uc.setConnectTimeout(700);
	        uc.setReadTimeout(5000);
	        uc.setDoOutput(true);
	        uc.setRequestMethod("GET");
	        uc.setRequestProperty("Accept", "application/json");
			in = uc.getInputStream();
		    buffer = new BufferedInputStream(in);
		    // chain the InputStream to a Reader
		    r = new InputStreamReader(buffer);
		    int c;
		    while ((c = r.read()) > 0) {
		    	log.debug((char) c);
				
		    }
		    
			log.debug(" Executando URL: " + URL_FUSION + URL+" Code : "+code);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("Erro ao executar regra! "+e+" - "+code);

		}
		finally
		{
			try
			{
				if(r != null)
					r.close();
				if(buffer != null)
					buffer.close();
				if(in != null)
					in.close();
				if (uc != null)
					uc.disconnect();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		}
	}
	
	public static void main(String args[]){
		executaRotinaXPRE();
	}


}
