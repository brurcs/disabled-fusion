package com.neomind.fusion.custom.orsegups;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class CotacaoOnline
{
	private static final org.apache.commons.logging.Log log = LogFactory.getLog(CotacaoOnline.class);

	public static void gravaDadosSapiensSID(NeoObject entity)
	{
		EntityWrapper wrapper = new EntityWrapper(entity);
	
		InstantiableEntityInfo infoURL =  (InstantiableEntityInfo) EntityRegister.getInstance().getCache().getByString("urlsSapiensCotacao");
		List<NeoObject> obj = (List<NeoObject>) PersistEngine.getObjects(infoURL.getEntityClass());

		if (infoURL != null)
		{
			EntityWrapper wrpURL = new EntityWrapper(obj.get(0));
			String urlLogin = (String) wrpURL.getValue("login");
			String urlAlterar = (String) wrpURL.getValue("alterar");
			String urlGravacao = (String) wrpURL.getValue("atualiza");
			String urlAjuste = (String) wrpURL.getValue("ajusta");
			
			Boolean liberaCotacao = (Boolean) wrapper.getValue("liberarCotacao");
			
			if (liberaCotacao)
			{
				
				Long numsol = (Long) wrapper.getValue("numsol");
				Long seqsol = (Long) wrapper.getValue("seqsol");
				Long codfor = (Long) wrapper.getValue("codfor");
				Long numcot = (Long) wrapper.getValue("numcot");
				Long prazoEntrega = (Long) wrapper.getValue("przentrega");
							
				BigDecimal qtdCotada = (BigDecimal) wrapper.getValue("qtdCotada");
				BigDecimal dsccot = (BigDecimal) wrapper.getValue("dsccot");
				BigDecimal icmcot = (BigDecimal) wrapper.getValue("icmcot");
				BigDecimal precot = (BigDecimal) wrapper.getValue("precot");
				BigDecimal ipiCot = (BigDecimal) wrapper.getValue("IpiCot");
			
				GregorianCalendar dtaPrv = (GregorianCalendar) wrapper.getValue("datPrv");
				String date = NeoUtils.safeDateFormat(dtaPrv, "dd/MM/yyyy");
				String obs = (String) wrapper.getValue("ObsCot");
				NeoObject ciffob = (NeoObject) wrapper.getValue("ciffob");
				EntityWrapper wrpFrete = new EntityWrapper(ciffob);
				String code = (String) wrpFrete.getValue("codigo");
			
				NeoObject codcpg = (NeoObject) wrapper.getValue("codcpg");
				EntityWrapper wrpCP = new EntityWrapper(codcpg);
				String cp = (String) wrpCP.getValue("codcpg");
			
				//Encode de caracteres para executar uma URL
				try
				{
					obs = java.net.URLEncoder.encode(obs, "ISO-8859-1");
				}
				catch (UnsupportedEncodingException e1)
				{
					e1.printStackTrace();
				} 
				
				urlGravacao = urlGravacao.concat("&NumSol=" + numsol);
				urlGravacao = urlGravacao.concat("&SeqSol=" + seqsol);
				urlGravacao = urlGravacao.concat("&CodFor=" + codfor);
				urlGravacao = urlGravacao.concat("&QtdCot=" + qtdCotada);
				urlGravacao = urlGravacao.concat("&PreCot=" + precot);
				urlGravacao = urlGravacao.concat("&PrzEnt=" + prazoEntrega);
				urlGravacao = urlGravacao.concat("&TipRet=0");
				urlGravacao = urlGravacao.concat("&DscCot=" + dsccot);
				urlGravacao = urlGravacao.concat("&IpiCot=" + ipiCot);
				urlGravacao = urlGravacao.concat("&IcmCot=" + icmcot);
				urlGravacao = urlGravacao.concat("&CodCpg=" + cp);
				urlGravacao = urlGravacao.concat("&DatPrv=" + date);
				urlGravacao = urlGravacao.concat("&ObsCot=" + obs);
				urlGravacao = urlGravacao.concat("&CifFob=" + code);
				urlGravacao = urlGravacao.concat("&USER=sapienssid");
				urlGravacao = urlGravacao.concat("&CONNECTION=");
			
				urlAjuste = urlAjuste.concat("&NumReg=170");
				urlAjuste = urlAjuste.concat("&NumCot=" + numcot);
				urlAjuste = urlAjuste.concat("&CodFor=" + codfor);
				urlAjuste = urlAjuste.concat("&USER=sapienssid");
				urlAjuste = urlAjuste.concat("&CONNECTION=");
			
				try
				{
					log.warn("Iniciando nova cotação: Número Cotação #"+ numcot +" Código fornecedor #"+codfor);
					URL url = new URL(urlLogin);	
					BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
					String menssagemSapiensSID;
					while ((menssagemSapiensSID = in.readLine()) != null)
					{
						if (!menssagemSapiensSID.equalsIgnoreCase(""))
						{
							String codeEntrada = menssagemSapiensSID;
							
							URL urlAlteracao = new URL(urlAlterar + codeEntrada);
								
							BufferedReader in2 = new BufferedReader(new InputStreamReader(urlAlteracao.openStream()));
							String menssagemSapiensSID2;
							
							while ((menssagemSapiensSID2 = in2.readLine()) != null)
							{
								if (menssagemSapiensSID2.equalsIgnoreCase("OK"))
								{		
									
									URL urlAtualizacao = new URL(urlGravacao + codeEntrada);
									
									BufferedReader in3 = new BufferedReader(new InputStreamReader(urlAtualizacao.openStream()));
									String menssagemSapiensSID3;
									
									while ((menssagemSapiensSID3 = in3.readLine()) != null)
									{
										if (menssagemSapiensSID3.equalsIgnoreCase("OK"))
										{											
											URL urlAjustes = new URL(urlAjuste + codeEntrada);										
											BufferedReader in4 = new BufferedReader(new InputStreamReader(urlAjustes.openStream()));
											String menssagemSapiensSID4;
											log.warn("URL ajuste sapiens");
											while ((menssagemSapiensSID4 = in4.readLine()) != null)
											{
												if (menssagemSapiensSID4.equalsIgnoreCase("OK"))
												{
													log.warn("Cotação realizada CONNECTION #"+ codeEntrada + "Numero Cotação #"+ numcot +"Fornecedor código #"+codfor);
                                                    wrapper.setValue("statusCotacao", "Efetuada com Sucesso!");
                                                    PersistEngine.persist(entity);
                                                    PersistEngine.commit(true);
												}
												else
												{
													log.warn(menssagemSapiensSID4);
												}
											}
										}
										else
										{			
											log.warn("Cotação de numero : "+ numcot+ " do fornecedor código : " + codfor +" não pode ser efetuada, AVISO SAPIENS: "+ menssagemSapiensSID2);
											wrapper.setValue("statusCotacao", menssagemSapiensSID2);
											wrapper.setValue("liberarCotacao", false);
		                                    PersistEngine.persist(entity);
		                                    PersistEngine.commit(true);
										}
									}
								}
								else
								{
									log.warn("Mensagem Sapiens ALTERAÇÃO"+  menssagemSapiensSID2);
								}
								
							}
						}
						else
						{
							log.warn("Mensagem Sapiens LOGIN "+  menssagemSapiensSID);
						}
					}
					in.close();
				}
				catch (MalformedURLException e)
				{
					e.printStackTrace();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
	}
}