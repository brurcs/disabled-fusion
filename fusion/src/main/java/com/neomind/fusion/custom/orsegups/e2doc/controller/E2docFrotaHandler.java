package com.neomind.fusion.custom.orsegups.e2doc.controller;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.neomind.fusion.custom.orsegups.e2doc.engine.E2docFrotaEngine;
import com.neomind.fusion.custom.orsegups.e2doc.xml.frota.Pastas;

@Path(value = "documentosFrota")
public class E2docFrotaHandler {
    
	@POST
	@Path("pesquisaFrota")
	@Produces("application/json")
	public Pastas pesquisaFrota(
		@MatrixParam("tiposDocumentos") String tiposDocumentos, @MatrixParam("placa") String placa,
		@MatrixParam("ano") String ano) {
	    
	    //nome da chave, valor 
	    HashMap<String, String> atributosPesquisa = new HashMap<String, String>();
	    
	    if (tiposDocumentos != null && !tiposDocumentos.trim().isEmpty()){
		atributosPesquisa.put("TIPO DE DOCUMENTO", tiposDocumentos.trim());
	    }
	    
	    if(placa != null && !placa.trim().isEmpty()){
		atributosPesquisa.put("PLACA", "%"+placa.trim()+"%");
	    }
	    
	    if(ano != null && !ano.trim().isEmpty()){
		atributosPesquisa.put("ANO", ano.trim());
	    }

	    E2docFrotaEngine e2docEngine = new E2docFrotaEngine();
	    
	    Pastas pastas = e2docEngine.pesquisaFrota(atributosPesquisa);
	    	    
	    return pastas;

	}
	
	@POST
	@Path("getTiposDocumentos")
	@Produces("application/json")
	public List<String> getTiposDocumentos() {
	    
	    E2docFrotaEngine e2docEngine = new E2docFrotaEngine();
	    
	    return e2docEngine.getTiposDocumentos();

	}
	
	@GET
	@Path("downloadImagem2/{id}")
	@Produces("application/pdf")
	public Response getFile2(@PathParam("id") String id) throws MalformedURLException, IOException
	{
	    E2docFrotaEngine e2docEngine = new E2docFrotaEngine();

	    HashMap<String,Object> map = e2docEngine.retornaLinkImagemDocumento2(id);
	    String ext = (String) map.get("ext");
	    File file = (File) map.get("file");
	    ResponseBuilder response = Response.ok((Object)file);
	    response.header("Content-Disposition", "attachment;filename=temp-"+new GregorianCalendar().getTimeInMillis()+"."+ext);
	    return response.build();

	}
}
