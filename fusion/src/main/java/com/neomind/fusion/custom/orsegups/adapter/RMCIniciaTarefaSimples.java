package com.neomind.fusion.custom.orsegups.adapter;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class RMCIniciaTarefaSimples implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		NeoUser usuario = PortalUtil.getCurrentUser();
		NeoObject tarefa = (NeoObject) processEntity.findValue("tarefaSimples");
		
		OrsegupsUtils.iniciaProcessoTarefaSimples(tarefa, usuario);
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}
}