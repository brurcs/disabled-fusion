package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.HtmlEmail;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsEmailUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class EmailDeliverySemControleOutros implements CustomJobAdapter {
    private static final Log log = LogFactory.getLog(EmailDeliverySemControleOutros.class);

    private Set<Integer> empresasNotificadas = new HashSet<Integer>();

    @Override
    public void execute(CustomJobContext arg0) {

	log.warn("E-Mail Desvio de hábito outros - Inicio execução em: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	Long key = GregorianCalendar.getInstance().getTimeInMillis();

	Connection conn = PersistEngine.getConnection("SIGMA90");
	StringBuilder strSigma = new StringBuilder();
	PreparedStatement pstm = null;
	ResultSet rs = null;

	String ultimaExecucaoRotina = ultimaExecucaoRotina();
	String ultimaExecucaoRotinaAux = ultimaExecucaoRotinaRange();

	try {

	    strSigma.append(" 	 SELECT c.OBSERVACAO, c.FANTASIA, c.RAZAO, c.ID_CENTRAL, c.PARTICAO, c.ENDERECO, cid.NOME AS NM_CIDADE, bai.NOME AS NM_BAIRRO, h.DT_FECHAMENTO, c.EMAILRESP,");
	    strSigma.append(" 	 hfe.NM_FRASE_EVENTO as NM_FRASE_EVENTO,col.NM_COLABORADOR,h.TX_OBSERVACAO_GERENTE, ");
	    strSigma.append(" 	 h.TX_OBSERVACAO_FECHAMENTO, ISNULL(ma.DS_MOTIVO_ALARME, 'N/A - SEM ALTERAÇÃO') as motivo, h.CD_HISTORICO_SEM_CONTROLE AS CD_HISTORICO, ");
	    strSigma.append(" 	 h.CD_EVENTO, c.CD_CLIENTE,  c.NU_LATITUDE, c.NU_LONGITUDE, h.DT_RECEBIDO, c.ID_EMPRESA ");
	    strSigma.append(" 	 FROM HISTORICO_SEM_CONTROLE h  WITH (NOLOCK)   ");
	    strSigma.append(" 	 INNER JOIN dbCENTRAL c  WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
	    strSigma.append(" 	 INNER JOIN ROTA r  WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA   ");
	    strSigma.append(" 	 INNER JOIN dbCODE cod WITH (NOLOCK) ON cod.ID_CODE = h.CD_CODE  ");
	    strSigma.append(" 	 INNER JOIN HISTORICO_FRASE_EVENTO hfe ON hfe.CD_FRASE_EVENTO = h.CD_FRASE_EVENTO  ");
	    strSigma.append(" 	 LEFT JOIN MOTIVO_ALARME ma ON ma.CD_MOTIVO_ALARME = h.CD_MOTIVO_ALARME  ");
	    strSigma.append(" 	 LEFT JOIN dbCIDADE cid ON cid.ID_CIDADE = c.ID_CIDADE  ");
	    strSigma.append(" 	 LEFT JOIN dbBAIRRO bai ON bai.ID_CIDADE = c.ID_CIDADE AND bai.ID_BAIRRO = c.ID_BAIRRO  ");
	    strSigma.append(" 	 INNER JOIN USUARIO us ON us.CD_USUARIO = h.CD_USUARIO_FECHAMENTO    ");
	    strSigma.append(" 	 INNER JOIN COLABORADOR col ON us.CD_COLABORADOR = col.CD_COLABORADOR  ");
	    strSigma.append(" 	 WHERE  CD_EVENTO IN ('XXX2','XXX5') AND C.TP_PESSOA != 2 ");
	    strSigma.append(" 	 AND NOT EXISTS (SELECT * FROM [CACUPE\\SQL02].Fusion_Producao.dbo.D_EmailAutomaticoDesvioDeHabito em where em.historico = h.CD_HISTORICO_SEM_CONTROLE)  ");
	    strSigma.append(" 	 AND (h.TX_OBSERVACAO_FECHAMENTO LIKE '%#AtualizarCadastro%' AND NOT EXISTS (SELECT * FROM [CACUPE\\SQL02].Fusion_Producao.dbo.D_EmailAutomaticoDesvioDeHabito em where em.historico = h.CD_HISTORICO_SEM_CONTROLE)  ");
	    strSigma.append(" 	 AND  EXISTS (select * from [CACUPE\\SQL02].Fusion_Producao.dbo.D_tarefa t with(nolock) ");
	    strSigma.append(" 	 inner join [CACUPE\\SQL02].Fusion_Producao.dbo.WFProcess p with(nolock) on p.neoId = t.wfprocess_neoId ");
	    strSigma.append(" 	 where P.processState = 0 AND P.startDate >= DATEADD(DAY,-10,GETDATE()) AND  t.titulo COLLATE Latin1_General_CI_AS like 'Atualização Cadastral - '+h.CD_EVENTO+' -  '+c.ID_CENTRAL+'\\['+c.PARTICAO+']  - '+c.RAZAO+' ('+c.FANTASIA+')' escape '\\')) ");
	    strSigma.append("    AND (h.TX_OBSERVACAO_FECHAMENTO LIKE '%#AtualizarCadastro%' OR h.TX_OBSERVACAO_FECHAMENTO LIKE '%#NaoEnviaComunicado%')   ");
	    strSigma.append(" 	 AND DT_VIATURA_DESLOCAMENTO IS NULL AND DT_ESPERA IS NULL ");
	    strSigma.append(" 	 AND CD_USUARIO_FECHAMENTO <> 9999     ");
	    strSigma.append(" 	 AND ( DT_RECEBIDO > '" + ultimaExecucaoRotina + "' OR  DT_RECEBIDO > '" + ultimaExecucaoRotinaAux + "' ) ");
	    // strSigma.append(" 	 AND ( DT_RECEBIDO > '2017-08-06 00:00:00.000' ) ");

	    pstm = conn.prepareStatement(strSigma.toString());

	    inserirFimRotina();

	    rs = pstm.executeQuery();

	    List<EventoVO> eventoVOs = new ArrayList<EventoVO>();
	    EventoVO eventoVO = null;
	    while (rs.next()) {

		boolean clienteComExcecao = OrsegupsEmailUtils.verificarExcecaoCNPJ(rs.getInt("CD_CLIENTE"));

		if (!clienteComExcecao) {
		    eventoVO = new EventoVO();
		    String codigoCentral = (rs.getString("ID_CENTRAL") == null ? "" : rs.getString("ID_CENTRAL"));
		    eventoVO.setCodigoCentral(codigoCentral);
		    String fantasia = (rs.getString("FANTASIA") == null ? "" : rs.getString("FANTASIA"));
		    eventoVO.setFantasia(fantasia);
		    String razao = (rs.getString("RAZAO") == null ? "" : rs.getString("RAZAO"));
		    eventoVO.setRazao(razao);
		    String particao = (rs.getString("PARTICAO") == null ? "" : rs.getString("PARTICAO"));
		    eventoVO.setParticao(particao);
		    String endereco = (rs.getString("ENDERECO") == null ? "" : rs.getString("ENDERECO"));
		    eventoVO.setEndereco(endereco);
		    String cidade = (rs.getString("NM_CIDADE") == null ? "" : rs.getString("NM_CIDADE"));
		    eventoVO.setNomeCidade(cidade);
		    String bairro = (rs.getString("NM_BAIRRO") == null ? "" : rs.getString("NM_BAIRRO"));
		    eventoVO.setBairro(bairro);

		    Timestamp dtFechamento = rs.getTimestamp("DT_FECHAMENTO");
		    String dtFechamentoStr = NeoDateUtils.safeDateFormat(dtFechamento, "dd/MM/yyyy");
		    eventoVO.setDataFechamento(dtFechamentoStr);

		    String txObsevacaoFechamento = (rs.getString("TX_OBSERVACAO_FECHAMENTO") == null ? "" : rs.getString("TX_OBSERVACAO_FECHAMENTO"));
		    String txObsevacaoFechamentoGerente = (rs.getString("TX_OBSERVACAO_GERENTE") == null ? "" : rs.getString("TX_OBSERVACAO_GERENTE"));

		    String nomeFraseEvento = (rs.getString("NM_FRASE_EVENTO") == null ? "" : rs.getString("NM_FRASE_EVENTO"));
		    eventoVO.setNomeEvento(nomeFraseEvento);

		    String motivo = (rs.getString("motivo") == null ? "" : rs.getString("motivo"));
		    eventoVO.setMotivo(motivo);
		    String evento = (rs.getString("CD_EVENTO") == null ? "" : rs.getString("CD_EVENTO"));
		    eventoVO.setCodigoEvento(evento);

		    String email = (rs.getString("EMAILRESP") == null ? "" : rs.getString("EMAILRESP"));
		    eventoVO.setEmailResponsavel(email);

		    String cdCliente = rs.getString("CD_CLIENTE");
		    eventoVO.setCodigoCliente(cdCliente);

		    // String horaAtendimento = rs.getString("DT_FECHAMENTO").substring(11, 16);
		    String historico = (rs.getString("CD_HISTORICO") == null ? "" : rs.getString("CD_HISTORICO"));
		    eventoVO.setCodigoHistorico(historico);
		    String nmColaborador = (rs.getString("NM_COLABORADOR") == null ? "" : rs.getString("NM_COLABORADOR"));
		    eventoVO.setUsuarioFechamento(nmColaborador);

		    String observacao = rs.getString("OBSERVACAO") == null ? "" : rs.getString("OBSERVACAO");
		    eventoVO.setObsTemp(observacao);

		    Timestamp dtRecebido = rs.getTimestamp("DT_RECEBIDO");
		    String dtRecebidoStr = NeoDateUtils.safeDateFormat(dtRecebido, "dd/MM/yyyy HH:mm:ss");
		    eventoVO.setDataRecebimento(dtRecebidoStr);

		    log.warn("E-Mail Desvio de hábito outros -  Desvio de hábito outros - fantasia:" + fantasia + ", razao:" + razao + ", email" + email);

		    eventoVO.setTextoObservacao(txObsevacaoFechamento);
		    eventoVO.setTextoObservacaoGerente(txObsevacaoFechamentoGerente);

		    eventoVO.setEmpresa(rs.getString("ID_EMPRESA"));

		    eventoVOs.add(eventoVO);
		}
	    }
	    log.warn("Desvio de hábito outros -  Fim execução em: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	    OrsegupsUtils.closeConnection(conn, pstm, rs);

	    if (eventoVOs != null && !eventoVOs.isEmpty()) {
		for (EventoVO eventoVOAux : eventoVOs) {
		    if (eventoVOAux != null) {
			String texto = (eventoVOAux.getTextoObservacao() == null ? "" : eventoVOAux.getTextoObservacao());
			boolean flagAtualizadoNAC = eventoVO.getObsTemp().contains("#AC");
			if (texto.contains("AtualizarCadastro")) {
			    abreTarefaAtualizacaoCadastral(eventoVOAux);
			    System.out.println("EVENTO MAIL AtualizarCadastro " + eventoVOAux.getCodigoHistorico() + " - " + eventoVOAux.getUsuarioFechamento());
			} else if (texto.contains("AtendimentoRealizado")) {
			    System.out.println("EVENTO MAIL AtendimentoRealizado " + eventoVOAux.getCodigoHistorico() + " - " + eventoVOAux.getUsuarioFechamento());
			} else if (texto.contains("SemContato")) {
			    enviaEmailCliente(montaEmailEventoSemContato(eventoVOAux), eventoVOAux, 0, flagAtualizadoNAC);
			    System.out.println("EVENTO MAIL SemContato " + eventoVOAux.getCodigoHistorico() + " - " + eventoVOAux.getUsuarioFechamento());
			} else if (texto.contains("NaoEnviaComunicado")) {
			    enviaEmailCliente(montaEmailNaoEnviarCliente(eventoVOAux), eventoVOAux, 1, flagAtualizadoNAC);
			    System.out.println("EVENTO MAIL NaoEnviaComunicado " + eventoVOAux.getCodigoHistorico() + " - " + eventoVOAux.getUsuarioFechamento());
			}
		    }
		}
	    }
	} catch (Exception e) {

	    log.error("E-Mail Desvio de hábito sem controle outros - erro no processamento: " + e.getMessage());
	    System.out.println("[" + key + "] E-Mail Desvio de hábito sem controle outros - erro no processamento: " + e.getMessage());
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

    }

    @SuppressWarnings({ "unchecked", "static-access", "deprecation" })
    private void enviaEmailCliente(StringBuilder msn, EventoVO eventoVO, int i, boolean flagAtualizadoNAC) {

	int empresa = Integer.parseInt(eventoVO.getEmpresa());

	try {
	    if (eventoVO != null && msn != null && !msn.toString().isEmpty()) {

		InstantiableEntityInfo infoHis = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("EmailAutomaticoDesvioDeHabito");
		InstantiableEntityInfo excecoesEmail = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("CadastroExcecoesEmailsDesvioDeHabito");
		InstantiableEntityInfo infoEnvioEmail = (InstantiableEntityInfo) EntityRegister.getInstance().getEntityInfo("EmailDelivery");
		List<String> emailCliente = null;
		if (i == 0) {
		    emailCliente = OrsegupsEmailUtils.validarEmail(eventoVO.getEmailResponsavel(), eventoVO.getCodigoCliente(), flagAtualizadoNAC);
		} else if (i == 1) {
		    emailCliente = new ArrayList<String>();
		    emailCliente.add("cm@orsegups.com.br");
		}

		if ((emailCliente != null) && (!emailCliente.isEmpty())) {

		    final String tipo = OrsegupsEmailUtils.TIPO_RAT_DESVIO_HABITO;

		    Map<Integer, String> params = OrsegupsEmailUtils.getConfiguracaoEmpresa(empresa);

		    String pasta = null;
		    String remetente = null;
		    String grupo = null;

		    if (params != null) {

			pasta = params.get(OrsegupsEmailUtils.RAT_PASTA);
			remetente = params.get(OrsegupsEmailUtils.RAT_REMETENTE);
			grupo = params.get(OrsegupsEmailUtils.RAT_GRUPO);

			for (String emailFor : emailCliente) {
			    StringBuilder noUserMsg = new StringBuilder();
			    QLGroupFilter groupFilter = new QLGroupFilter("AND");
			    groupFilter.addFilter(new QLEqualsFilter("email", emailFor));

			    if (eventoVO != null && eventoVO.getCodigoEvento() != null && eventoVO.getCodigoEvento().contains("XXX2")) {
				groupFilter.addFilter(new QLEqualsFilter("tratarXXX2", Boolean.TRUE));
			    } else if (eventoVO != null && eventoVO.getCodigoEvento() != null && eventoVO.getCodigoEvento().contains("XXX5")) {
				groupFilter.addFilter(new QLEqualsFilter("tratarXXX5", Boolean.TRUE));
			    }

			    List<NeoObject> neoObjects = PersistEngine.getObjects(excecoesEmail.getEntityClass(), groupFilter);

			    if (neoObjects == null || (neoObjects != null && neoObjects.isEmpty())) {

				NeoObject emailHis = infoHis.createNewInstance();
				EntityWrapper emailHisWp = new EntityWrapper(emailHis);
				emailHisWp.findField("fantasia").setValue(eventoVO.getFantasia());
				emailHisWp.findField("razao").setValue(eventoVO.getRazao());
				emailHisWp.findField("particao").setValue(eventoVO.getParticao());
				emailHisWp.findField("endereco").setValue(eventoVO.getEndereco());
				emailHisWp.findField("cidade").setValue(eventoVO.getNomeCidade());
				emailHisWp.findField("bairro").setValue(eventoVO.getBairro());
				emailHisWp.findField("dataFechamento").setValue(NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
				emailHisWp.findField("horaFechamento").setValue(NeoDateUtils.safeDateFormat(new GregorianCalendar(), "HH:mm:ss"));
				emailHisWp.findField("motivoAlarme").setValue(eventoVO.getNomeEvento());
				emailHisWp.findField("evento").setValue(eventoVO.getCodigoEvento());
				emailHisWp.findField("enviadoPara").setValue(emailCliente.toString());
				emailHisWp.findField("nomeFraseEvento").setValue("SEM CONTATO");
				emailHisWp.findField("historico").setValue(eventoVO.getCodigoHistorico());

				PersistEngine.persist(emailHis);
				String assunto = "Relatório de Atendimento " + eventoVO.getCodigoEvento() + " - " + eventoVO.getFantasia() + " - " + eventoVO.getDataRecebimento();

				noUserMsg.append(OrsegupsEmailUtils.getCabecalhoEmail(tipo, pasta, null));

				noUserMsg.append("\n <table width=\"600\" border=\"0\">");
				noUserMsg.append("\n <tbody>");
				noUserMsg.append("\n <tr>");

				noUserMsg.append(msn);

				noUserMsg.append("\n </tr>");
				noUserMsg.append("\n </tbody>");
				noUserMsg.append("\n </table>");
				noUserMsg.append(OrsegupsEmailUtils.getRodapeEmail(tipo, pasta, grupo, emailFor, remetente, null));

				// TESTE E-MAIL UTILIZANDO RECURSO FUSION
				//OrsegupsEmailUtils.sendTestEmail(noUserMsg, assunto);

				GregorianCalendar dataCad = (GregorianCalendar) GregorianCalendar.getInstance();
				NeoObject emaiEnvio = infoEnvioEmail.createNewInstance();
				EntityWrapper emailEnvioWp = new EntityWrapper(emaiEnvio);
				emailEnvioWp.findField("de").setValue("cm.rat" + remetente);
				emailEnvioWp.findField("para").setValue(emailFor + ";emailautomatico@orsegups.com.br;copia@orsegups.com.br");
				emailEnvioWp.findField("assunto").setValue(assunto);
				emailEnvioWp.findField("Mensagem").setValue(noUserMsg.toString());
				emailEnvioWp.findField("datCad").setValue(dataCad);
				PersistEngine.persist(emaiEnvio);

			    }
			}
			
			
			String assunto = "Relatório de Atendimento " + eventoVO.getCodigoEvento() + " - " + eventoVO.getFantasia() + " - " + eventoVO.getDataRecebimento();
			String endereco = eventoVO.getEndereco() + " - " + eventoVO.getBairro() + " - " + eventoVO.getNomeCidade();
			OrsegupsUtils.sendEmailWhatsAppRAT(Long.valueOf(eventoVO.getCodigoCliente()), assunto, endereco, eventoVO.getDataRecebimento());
		    } else {
			if (!empresasNotificadas.contains(empresa)) {
			    OrsegupsEmailUtils.enviarNotificacaoEmpresaSemConfig(empresa);
			    empresasNotificadas.add(empresa);
			}
		    }

		}
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("##### ERRO ao enviar comunicado de férias para cliente - Data: " + e.getMessage().toString() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

	}

    }

    private String buscaUsuarioEventoArmado(String cdCliente, String data) {
	Connection conn = null;
	PreparedStatement st = null;
	ResultSet rs = null;
	StringBuilder builder = new StringBuilder();
	log.warn("INICIAR BUSCA EVENTO ARMADO ANTERIOR. HISTORICO -" + cdCliente);
	String retorno = "";
	try {

	    if (cdCliente != null && !cdCliente.isEmpty() && data != null && !data.isEmpty()) {

		conn = PersistEngine.getConnection("SIGMA90");

		builder.append("     SELECT TOP 1 AC.NOME,H.DT_FECHAMENTO FROM HISTORICO_SEM_CONTROLE H WITH(NOLOCK)   ");
		builder.append("     LEFT JOIN dbACESSO AC WITH(NOLOCK) on CONVERT(VARCHAR(19),AC.ID_ACESSO)= CONVERT(VARCHAR(19),h.NU_AUXILIAR)   ");
		builder.append("     AND AC.CD_CLIENTE = H.CD_CLIENTE    ");
		builder.append("     WHERE DATEADD(ms, -DATEPART(ms, h.DT_RECEBIDO), h.DT_RECEBIDO) <= '" + data + "'  ");
		builder.append("     AND H.CD_CLIENTE = ?  ORDER BY DT_RECEBIDO DESC   ");

		st = conn.prepareStatement(builder.toString());
		st.setString(1, cdCliente);
		rs = st.executeQuery();

		if (rs.next()) {
		    if (rs.getString("NOME") != null && rs.getTimestamp("DT_FECHAMENTO") != null) {
			Timestamp dataFechamento = rs.getTimestamp("DT_FECHAMENTO");
			retorno = rs.getString("NOME") + " em " + NeoDateUtils.safeDateFormat(dataFechamento, "dd/MM/yyyy") + " e " + NeoDateUtils.safeDateFormat(dataFechamento, "HH:mm") + "hs";
		    }
		}

	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    log.error(" ERRO BUSCA EVENTO ARMADO ANTERIOR. HISTORICO -" + cdCliente);
	} finally {
	    OrsegupsUtils.closeConnection(conn, st, rs);
	}
	return retorno;

    }

    private void inserirFimRotina() {

	try {
	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("nomeRotina", "EmailAtendimentoSemControleOutros"));

	    List<NeoObject> monitoraAgendador = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);
	    PersistEngine.getEntityManager().flush();

	    if (monitoraAgendador != null && !monitoraAgendador.isEmpty()) {
		NeoObject neoObject = (NeoObject) monitoraAgendador.get(0);

		EntityWrapper monitoraAgendadorWrapper = new EntityWrapper(neoObject);

		monitoraAgendadorWrapper.findField("dataFinalAgendador").setValue(new GregorianCalendar());

		PersistEngine.persist(neoObject);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}

    }

    private String ultimaExecucaoRotina() {

	String retorno = "";
	try {
	    Collection<NeoObject> monitoraAgendador = null;

	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("nomeRotina", "EmailAtendimentoSemControleOutros"));

	    monitoraAgendador = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);

	    if (monitoraAgendador != null && !monitoraAgendador.isEmpty()) {
		for (NeoObject neoObject : monitoraAgendador) {
		    EntityWrapper monitoraAgendadorObj = new EntityWrapper(neoObject);

		    GregorianCalendar dataFinalAgendador = (GregorianCalendar) monitoraAgendadorObj.getValue("dataFinalAgendador");

		    retorno = NeoDateUtils.safeDateFormat(dataFinalAgendador, "yyyy-MM-dd HH:mm:ss");
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return retorno;

    }

    public static List<String> dias(GregorianCalendar calendar) {
	List<String> cptList = null;
	if (NeoUtils.safeIsNotNull(calendar)) {
	    long milisecInicial = calendar.getTime().getTime();
	    long milisecFinal = new GregorianCalendar().getTime().getTime();
	    long dif = milisecFinal - milisecInicial;

	    long dias = (((dif / 1000) / 60) / 60) / 24;
	    cptList = new ArrayList<String>();
	    Calendar dia = Calendar.getInstance();
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM");
	    for (int i = 0; i <= dias; i++) {
		String diaStr = dateFormat.format(dia.getTime());
		cptList.add(diaStr);
		dia.add(Calendar.DAY_OF_MONTH, -1);
	    }

	}
	return cptList;
    }

    private StringBuilder montaEmailEventoSemContato(EventoVO eventoVO) {

	StringBuilder noUserMsg = new StringBuilder();
	try {

	    if (eventoVO != null) {

		noUserMsg.append("<tr></tr>");
		noUserMsg.append("\n <td><table width=\"600\"align=\"center\" cellpadding=\"0\" cellspacing=\"0\">");
		noUserMsg.append("\n <tbody>");
		noUserMsg.append("\n <tr>");
		noUserMsg.append("\n <td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/header.jpg\" width=\"600\" height=\"145\" alt=\"\"/></td>");
		noUserMsg.append("\n </tr>");
		noUserMsg.append("\n <tr> ");
		noUserMsg.append("\n <td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/separator-01.jpg\" width=\"600\" height=\"22\" alt=\"\"/></td>");
		noUserMsg.append("\n </tr>");
		noUserMsg.append("\n <tr> ");
		noUserMsg.append("\n <td><table width=\"100%\" border=\"0\">");
		noUserMsg.append("\n <tbody>");
		noUserMsg.append("\n <tr> ");
		noUserMsg.append("\n <td colspan=\"5\" style=\"text-align: center;\"> ");
		noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:28px;text-align:center;color:red;font-weight:bold;padding:10px;margin-top:0px;\">ATENÇÃO: Aviso Importante</p>");
		noUserMsg.append("\n </td> </tr>");
		noUserMsg.append("\n </tr>");
		noUserMsg.append("\n <tr>");
		noUserMsg.append("\n <td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/separator-02.jpg\" width=\"600\" height=\"8\" alt=\"\"/></td>");
		noUserMsg.append("\n </tr>");
		noUserMsg.append("\n <tr>");
		noUserMsg.append("\n <td><table width=\"100%\" border=\"0\">");
		noUserMsg.append("\n <tbody>");

		noUserMsg.append("\n <tr>");
		noUserMsg.append("\n <td colspan=\"6\">");
		noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align:justify;\"> ");

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyy HH:mm:ss");

		java.util.Date dateAux = dateFormat.parse(eventoVO.getDataRecebimento());
		String data = NeoDateUtils.safeDateFormat(dateAux, "yyyy-MM-dd HH:mm:ss");

		String nomeUsuarioEventoArmado = buscaUsuarioEventoArmado(eventoVO.getCodigoCliente(), data);

		if (eventoVO != null && eventoVO.getCodigoEvento() != null && eventoVO.getCodigoEvento().contains("X406") && nomeUsuarioEventoArmado != null && !nomeUsuarioEventoArmado.isEmpty()) {
		    noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align:justify;\"> ");
		    noUserMsg.append("\n Informamos que em " + eventoVO.getDataRecebimento() + " nossa Central de Monitoramento 24h identificou um disparo em seu sistema de segurança e na sequência foi <strong>desarmado</strong> pelo Sr(a) " + nomeUsuarioEventoArmado + ". ");
		    noUserMsg.append("\n </p>");
		    noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align:justify;\"> ");
		    noUserMsg.append("\n Conforme pode-se constatar no relatório abaixo, ");
		    noUserMsg.append("\n durante este atendimento, nossos operadores tentaram realizar contato telefônico com as pessoas indicadas no cadastro para verificar necessidade de auxílio com o sistema, porém <strong>não houve sucesso em nenhuma tentativa.</strong> ");
		    noUserMsg.append("\n </p>");
		    noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align:justify;\"> ");
		    noUserMsg.append("\n Como o sistema foi desarmado pelo Sr(a). " + nomeUsuarioEventoArmado + " com sua senha <strong>PADRÃO</strong>, o evento foi encerrado por nossos operadores. ");
		    noUserMsg.append("\n </p>");
		} else {
		    if (eventoVO != null && eventoVO.getCodigoEvento() != null && eventoVO.getCodigoEvento().contains("X406")) {
			noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align:justify;\"> ");
			noUserMsg.append("\n Informamos que em " + eventoVO.getDataRecebimento() + " nossa Central de Monitoramento 24h identificou um disparo em seu sistema de segurança e na sequência foi <strong>desarmado</strong>. ");
			noUserMsg.append("\n </p>");
		    } else if (eventoVO != null && eventoVO.getCodigoEvento() != null && eventoVO.getCodigoEvento().contains("XXX2")) {
			noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align:justify;\"> ");
			noUserMsg.append("\n Informamos que em " + eventoVO.getDataRecebimento() + " nossa Central de Monitoramento 24h identificou que o seu sistema de segurança estava como <strong>não armado</strong>. ");
			noUserMsg.append("\n </p>");
		    } else if (eventoVO != null && eventoVO.getCodigoEvento() != null && eventoVO.getCodigoEvento().contains("XXX5")) {
			noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align:justify;\"> ");
			noUserMsg.append("\n Informamos que em " + eventoVO.getDataRecebimento() + " nossa Central de Monitoramento 24h identificou que o seu sistema de segurança estava como <strong>desarmado fora do horário</strong>. ");
			noUserMsg.append("\n </p>");
		    }

		    noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align:justify;\"> ");
		    noUserMsg.append("\n Conforme pode-se constatar no relatório abaixo, ");
		    noUserMsg.append("\n durante este atendimento, nossos operadores tentaram realizar contato telefônico com as pessoas indicadas no cadastro para verificar necessidade de auxílio com o sistema, <strong>não houve sucesso em nenhuma tentativa.</strong> ");
		    noUserMsg.append("\n </p>");

		    if (eventoVO != null && eventoVO.getCodigoEvento() != null && eventoVO.getCodigoEvento().contains("X406")) {
			noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align:justify;\"> ");
			noUserMsg.append("\n Como o sistema foi desarmado com sua senha <strong>PADRÃO</strong>, o evento foi encerrado por nossos operadores. ");
			noUserMsg.append("\n </p>");
		    } else if (eventoVO != null && eventoVO.getCodigoEvento() != null && eventoVO.getCodigoEvento().contains("XXX2")) {
			noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align:justify;\"> ");
			noUserMsg.append("\n Como o sistema não foi armado, o evento foi encerrado por nossos operadores. ");
			noUserMsg.append("\n </p>");
		    } else if (eventoVO != null && eventoVO.getCodigoEvento() != null && eventoVO.getCodigoEvento().contains("XXX5")) {
			noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align:justify;\"> ");
			noUserMsg.append("\n Como o sistema foi desarmado fora do horário, o evento foi encerrado por nossos operadores. ");
			noUserMsg.append("\n </p>");
		    }
		}

		noUserMsg.append("\n </p></td>");
		noUserMsg.append("\n </tr><br/>");

		String calendarDDMM = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM");
		// String calendarDM = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "d/M");
		String txObsevacaoFechamento = eventoVO.getTextoObservacao();

		if (txObsevacaoFechamento != null && !txObsevacaoFechamento.isEmpty()) {
		    String array[] = txObsevacaoFechamento.split("###");
		    String textoFormatado = "";
		    for (String string : array) {
			textoFormatado += string + "<br/>";
		    }

		    if (!textoFormatado.isEmpty()) {
			textoFormatado = textoFormatado.replaceAll("#", "");
			textoFormatado = textoFormatado.replaceAll("\\r\\n", "");
			textoFormatado = textoFormatado.replaceAll(";", "");
			GregorianCalendar calendar = new GregorianCalendar();

			DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			java.util.Date date = format.parse(eventoVO.getDataRecebimento());

			calendar.setTime(date);
			List<String> dias = dias(calendar);

			if (dias != null && !dias.isEmpty()) {
			    for (String diaStr : dias) {
				textoFormatado = textoFormatado.replaceAll(diaStr, "<br/><br/>" + diaStr);
			    }

			} else {
			    textoFormatado = textoFormatado.replaceAll(calendarDDMM, "<br/><br/>" + calendarDDMM);
			    // textoFormatado = textoFormatado.replaceAll(calendarDM, "<br/><br/>" + calendarDDMM);
			}

			textoFormatado = textoFormatado.replaceAll("Ligando para cliente!", "Ligando para cliente ");
			textoFormatado = textoFormatado.replaceAll("Ramal", "a partir do ramal ");

			textoFormatado = textoFormatado.replaceAll("FecharEvento", " Finalizar evento.");
			textoFormatado = textoFormatado.replaceAll("EmEspera", " Aguardo deslocamento de um atendente.");
			textoFormatado = textoFormatado.replaceAll("TratarSigma", "");

			textoFormatado = textoFormatado.replaceAll("AtendimentoRealizado", " Finalizar evento.");
			textoFormatado = textoFormatado.replaceAll("SemContato", " Não houve sucesso em nenhuma tentativa de contato.");
			textoFormatado = textoFormatado.replaceAll("AtualizarCadastro", " Atualizar cadastro do cliente.");
			textoFormatado = textoFormatado.replaceAll("NaoEnviaComunicado", " Comunicado de alarme com a central de monitoramento.");

			txObsevacaoFechamento = textoFormatado;

			noUserMsg.append("\n <tr>");
			noUserMsg.append("\n <td colspan=\"6\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\"><strong>Relatório do atendimento: </strong></br>");
			noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align:justify;\"> " + NeoUtils.encodeHTMLValue(txObsevacaoFechamento).replaceAll("&lt;br/&gt;", "").replaceAll("&lt;li&gt;", "").replaceAll("&lt;br&gt;", "") + "</p></td>");
			noUserMsg.append("\n </tr><br/>");
		    } else {

			noUserMsg.append("\n <tr>");
			noUserMsg.append("\n <td colspan=\"6\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\"><strong>Relatório do atendimento: </strong></br>");
			noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align:justify;\"> Não encontrado.</p></td>");
			noUserMsg.append("\n </tr><br/>");

		    }
		}

		noUserMsg.append("\n <tr>");
		noUserMsg.append("\n <td width=\"100%\" align=\"left\" valign=\"top\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Local Monitorado <br/> <strong>" + eventoVO.getRazao() + "</strong><br/>");
		noUserMsg.append("\n <strong>" + eventoVO.getEndereco() + "</strong><br/>");
		noUserMsg.append("\n <strong>" + eventoVO.getBairro() + "</strong><br/>");
		noUserMsg.append("\n <strong>" + eventoVO.getNomeCidade() + "</strong></p>");
		noUserMsg.append("\n </tr>");
		noUserMsg.append("\n <tr>");
		noUserMsg.append("\n <td colspan=\"6\"><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/separator-02.jpg\" width=\"600\" height=\"8\" alt=\"\"/></td>");
		noUserMsg.append("\n </tr>");

	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return noUserMsg;
    }

    private StringBuilder montaEmailNaoEnviarCliente(EventoVO eventoVO) {

	StringBuilder noUserMsg = new StringBuilder();
	try {

	    if (eventoVO != null) {
		noUserMsg.append("<tr></tr>");
		noUserMsg.append("\n <td><table width=\"100%\" border=\"0\">");
		noUserMsg.append("\n <tbody>");
		noUserMsg.append("\n <tr> ");
		noUserMsg.append("\n <td colspan=\"5\" style=\"text-align: center;\"> ");
		noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:28px;text-align:center;color:red;font-weight:bold;padding:10px;margin-top:0px;\">ALERTA: Evento fechado sem comunicado ao cliente</p>");
		noUserMsg.append("\n </td> </tr> ");

		noUserMsg.append("\n </tr>");
		noUserMsg.append("\n <tr>");
		noUserMsg.append("\n <td><img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/separator-02.jpg\" width=\"600\" height=\"8\" alt=\"\"/></td>");
		noUserMsg.append("\n </tr>");
		noUserMsg.append("\n <tr>");
		noUserMsg.append("\n <td><table width=\"100%\" border=\"0\">");
		noUserMsg.append("\n <tbody>");

		noUserMsg.append("\n <tr>");
		noUserMsg.append("\n <td colspan=\"6\">");
		noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align:justify;\"> ");

		// DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyy HH:mm:ss");
		// System.out.println(eventoVO.getDataRecebimento());
		// java.util.Date dateAux = dateFormat.parse(eventoVO.getDataRecebimento());
		// String data = NeoDateUtils.safeDateFormat(dateAux, "yyyy-MM-dd HH:mm:ss");

		// String nomeUsuarioEventoArmado = buscaUsuarioEventoArmado(eventoVO.getCodigoCliente(), data);

		if (eventoVO.getUsuarioFechamento() != null && !eventoVO.getUsuarioFechamento().isEmpty()) {
		    noUserMsg.append("\n Informamos que o evento  <strong>" + eventoVO.getCodigoEvento() + " </strong>  recebido em  <strong>" + eventoVO.getDataRecebimento() + " </strong> foi encerrado pelo operador  <strong>" + eventoVO.getUsuarioFechamento()
			    + " </strong> sem comunicado ao cliente.");
		} else {
		    noUserMsg.append("\n Informamos que o evento " + eventoVO.getCodigoEvento() + "  recebido em " + eventoVO.getDataRecebimento() + " foi encerrado pelo operador " + eventoVO.getUsuarioFechamento() + " sem comunicado ao cliente.");

		}

		noUserMsg.append("\n </p></td>");
		noUserMsg.append("\n </tr><br/>");
		String calendarDDMM = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM");
		String calendarDM = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "d/M");
		String txObsevacaoFechamento = eventoVO.getTextoObservacao();

		if (txObsevacaoFechamento != null && !txObsevacaoFechamento.isEmpty()) {
		    String array[] = txObsevacaoFechamento.split("###");
		    String textoFormatado = "";
		    for (String string : array) {
			textoFormatado += string + "<br/>";
		    }

		    if (!textoFormatado.isEmpty()) {
			textoFormatado = textoFormatado.replaceAll("#", "");
			textoFormatado = textoFormatado.replaceAll("\\r\\n", "");
			textoFormatado = textoFormatado.replaceAll(";", "");
			GregorianCalendar calendar = new GregorianCalendar();

			DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			java.util.Date date = format.parse(eventoVO.getDataRecebimento());

			calendar.setTime(date);
			List<String> dias = dias(calendar);

			if (dias != null && !dias.isEmpty()) {
			    for (String diaStr : dias) {
				textoFormatado = textoFormatado.replaceAll(diaStr, "<br/><br/>" + diaStr);
			    }

			} else {
			    textoFormatado = textoFormatado.replaceAll(calendarDDMM, "<br/><br/>" + calendarDDMM);
			    textoFormatado = textoFormatado.replaceAll(calendarDM, "<br/><br/>" + calendarDDMM);
			}

			textoFormatado = textoFormatado.replaceAll("Ligando para cliente!", "Ligando para cliente ");
			textoFormatado = textoFormatado.replaceAll("Ramal", "a partir do ramal ");

			textoFormatado = textoFormatado.replaceAll("FecharEvento", " Finalizar evento.");
			textoFormatado = textoFormatado.replaceAll("EmEspera", " Aguardo deslocamento de um atendente.");
			textoFormatado = textoFormatado.replaceAll("TratarSigma", "");

			textoFormatado = textoFormatado.replaceAll("AtendimentoRealizado", " Finalizar evento.");
			textoFormatado = textoFormatado.replaceAll("SemContato", " Não houve sucesso em nenhuma tentativa de contato.");
			textoFormatado = textoFormatado.replaceAll("AtualizarCadastro", " Atualizar cadastro do cliente.");
			textoFormatado = textoFormatado.replaceAll("NaoEnviaComunicado", " Comunicado de alarme com a central de monitoramento.");

			txObsevacaoFechamento = textoFormatado;

			noUserMsg.append("\n <tr>");
			noUserMsg.append("\n <td colspan=\"6\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\"><strong>Relatório do atendimento: </strong></br>");
			noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align:justify;\"> " + NeoUtils.encodeHTMLValue(txObsevacaoFechamento).replaceAll("&lt;br/&gt;", "").replaceAll("&lt;li&gt;", "").replaceAll("&lt;br&gt;", "") + "</p></td>");
			noUserMsg.append("\n </tr><br/>");
		    } else {

			noUserMsg.append("\n <tr>");
			noUserMsg.append("\n <td colspan=\"6\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\"><strong>Relatório do atendimento: </strong></br>");
			noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align:justify;\"> Não encontrado.</p></td>");
			noUserMsg.append("\n </tr><br/>");

		    }
		    String textoObsGerente = eventoVO.getTextoObservacaoGerente();
		    if (textoObsGerente != null && !textoObsGerente.equals("")) {
			textoFormatado = "";
			if (textoObsGerente.contains("###")) {
			    String arrayAux[] = textoObsGerente.split("###");

			    for (String string : arrayAux) {
				textoFormatado += string + "<br/>";
			    }
			} else {
			    textoFormatado = textoObsGerente;
			}
			textoFormatado = textoFormatado.replaceAll("#", "");
			textoFormatado = textoFormatado.replaceAll("\\r\\n", "");
			textoFormatado = textoFormatado.replaceAll(";", "");
			textoFormatado = textoFormatado.replaceAll(calendarDDMM, "<br/><br/>" + calendarDDMM);
			textoFormatado = textoFormatado.replaceAll(calendarDM, "<br/><br/>" + calendarDDMM);

			textoFormatado = textoFormatado.replaceAll("FecharEvento", " Finalizar evento.");
			textoFormatado = textoFormatado.replaceAll("EmEspera", " Aguardo deslocamento de um atendente.");
			textoFormatado = textoFormatado.replaceAll("TratarSigma", "");

			textoFormatado = textoFormatado.replaceAll("AtendimentoRealizado", " Finalizar evento.");
			textoFormatado = textoFormatado.replaceAll("SemContato", " Não houve sucesso em nenhuma tentativa de contato.");
			textoFormatado = textoFormatado.replaceAll("AtualizarCadastro", " Atualizar cadastro do cliente.");
			textoFormatado = textoFormatado.replaceAll("NaoEnviaComunicado", " Comunicado de alarme com a central de monitoramento.");
			txObsevacaoFechamento = textoFormatado;

			noUserMsg.append("\n <tr>");
			noUserMsg.append("\n <td colspan=\"6\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\"><strong>Informações da Gerencia: </strong></br>");
			noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align:justify;\"> " + NeoUtils.encodeHTMLValue(txObsevacaoFechamento).replaceAll("&lt;br/&gt;", "").replaceAll("&lt;li&gt;", "").replaceAll("&lt;br&gt;", "") + "</p></td>");
			noUserMsg.append("\n </tr><br/>");

		    } else {
			noUserMsg.append("\n <tr>");
			noUserMsg.append("\n <td colspan=\"6\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\"><strong>Informações da Gerencia:  </strong></br>");
			noUserMsg.append("\n <p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;text-align:justify;\"> Não encontrado.</p></td>");
			noUserMsg.append("\n </tr><br/>");
		    }

		    noUserMsg.append("\n <tr>");
		    noUserMsg.append("\n <td width=\"100%\" align=\"left\" valign=\"top\"><p style=\"font-family: 'Verdana';font-weight:normal;font-size:15px;\">Local Monitorado  <br/> <strong>" + eventoVO.getRazao() + "</strong><br/>");
		    noUserMsg.append("\n <strong>" + eventoVO.getEndereco() + "</strong><br/>");
		    noUserMsg.append("\n <strong>" + eventoVO.getBairro() + "</strong><br/>");
		    noUserMsg.append("\n <strong>" + eventoVO.getNomeCidade() + "</strong></p>");
		    noUserMsg.append("\n </tr>");

		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return noUserMsg;
    }

    private void abreTarefaAtualizacaoCadastral(EventoVO eventoVO) {

	System.out.println("INICIAR ABRIR TAREFA DE ATUALIZAÇÃO CADASTRAL. HISTORICO -");

	try {

	    if (eventoVO != null) {
		String usuario = eventoVO.getUsuarioFechamento();
		String arrayStr = eventoVO.getTextoObservacao();

		String array[] = arrayStr.split("###");
		String textoFormatado = "";
		String calendarDDMM = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM");
		String calendarDM = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "d/M");
		for (String string : array) {
		    textoFormatado += string + "<br/>";
		}

		textoFormatado = textoFormatado.replaceAll("#", "");
		textoFormatado = textoFormatado.replaceAll("\\r\\n", "");
		textoFormatado = textoFormatado.replaceAll(";", "");
		textoFormatado = textoFormatado.replaceAll(calendarDDMM, "<br/><br/>" + calendarDDMM);
		textoFormatado = textoFormatado.replaceAll(calendarDM, "<br/><br/>" + calendarDDMM);

		textoFormatado = textoFormatado.replaceAll("FecharEvento", " Finalizar evento.");
		textoFormatado = textoFormatado.replaceAll("EmEspera", " Aguardo deslocamento de um atendente.");
		textoFormatado = textoFormatado.replaceAll("TratarSigma", "");

		textoFormatado = textoFormatado.replaceAll("AtendimentoRealizado", " Finalizar evento.");
		textoFormatado = textoFormatado.replaceAll("SemContato", " Não houve sucesso em nenhuma tentativa de contato.");
		textoFormatado = textoFormatado.replaceAll("AtualizarCadastro", " Atualizar cadastro do cliente.");
		textoFormatado = textoFormatado.replaceAll("NaoEnviaComunicado", " Comunicado de alarme com a central de monitoramento.");

		String txObsevacaoFechamento = textoFormatado;

		String solicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteEmailDeliverySemControleOutros");
		String executor = OrsegupsUtils.getUserNeoPaper("ExecutorEmailDeliverySemControleOutros");
		String titulo = "Atualização Cadastral - " + eventoVO.getCodigoEvento() + " -  " + eventoVO.getCodigoCentral() + "[" + eventoVO.getParticao() + "]  - " + eventoVO.getRazao() + " (" + eventoVO.getFantasia() + ")";

		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);
		String descricao = "";
		descricao = "Identificada necessidade de atualização cadastral para a conta abaixo, durante o atendimento do evento " + eventoVO.getCodigoEvento() + ".<br>";
		descricao = descricao + " <strong>Conta :</strong> " + eventoVO.getCodigoCentral() + "[" + eventoVO.getParticao() + "]  - " + eventoVO.getRazao() + " (" + eventoVO.getFantasia() + ") <br>";
		descricao = descricao + " <strong>Log :</strong> " + txObsevacaoFechamento + " <br>";
		descricao = descricao + " <strong>Operador :</strong> " + usuario + " <br>";

		String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);
		System.out.println("ABRIR TAREFA DE ATUALIZAÇÃO CADASTRAL. CONTA -" + eventoVO.getCodigoCentral() + "[" + eventoVO.getParticao() + "] Fantasia - " + eventoVO.getFantasia() + " TAREFA - " + tarefa);
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    System.out.println(" ERRO AO ABRIR TAREFA DE ATUALIZAÇÃO CADASTRAL. " + e.getMessage());
	} finally {
	    System.out.println("FINALIZAR ABRIR TAREFA DE ATUALIZAÇÃO CADASTRAL. HISTORICO -");
	}

    }

    private String ultimaExecucaoRotinaRange() {

	String retorno = "";
	try {
	    Collection<NeoObject> monitoraAgendador = null;

	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("nomeRotina", "EmailAtendimentoSemControleOutros"));

	    monitoraAgendador = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);

	    if (monitoraAgendador != null && !monitoraAgendador.isEmpty()) {
		for (NeoObject neoObject : monitoraAgendador) {
		    EntityWrapper monitoraAgendadorObj = new EntityWrapper(neoObject);

		    GregorianCalendar dataFinalAgendador = (GregorianCalendar) monitoraAgendadorObj.getValue("dataFinalAgendador");
		    GregorianCalendar dataFinalAgendadorAux = (GregorianCalendar) dataFinalAgendador.clone();

		    dataFinalAgendadorAux.add(Calendar.MINUTE, -15);

		    retorno = NeoDateUtils.safeDateFormat(dataFinalAgendadorAux, "yyyy-MM-dd HH:mm:ss");
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}

	return retorno;
    }

}