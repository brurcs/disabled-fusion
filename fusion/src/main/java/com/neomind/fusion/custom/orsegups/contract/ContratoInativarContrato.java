package com.neomind.fusion.custom.orsegups.contract;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.mail.Mail2Render;
import com.neomind.fusion.mail.MailEngine;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoCalendarUtils;
import com.neomind.util.NeoUtils;

public class ContratoInativarContrato implements AdapterInterface
{
	
	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		long numctr = NeoUtils.safeLong(NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numctr")));
		
		long codemp = (Long) wrapper.findValue("empresa.codemp");
		long codfil = (Long) wrapper.findValue("empresa.codfil");
		String tipcli = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.tipemc"));
		String codcli = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.codcli"));
		
		long codigoMotivo = (Long) wrapper.findValue("motivoInativacao.codmot");
		
		if(codigoMotivo == 95 && tipcli.equals("1"))
		{
			throw new WorkflowException("Código 95 - Realinhamento desabilitado. Utilize outro código de cancelamento.");
		}
		
		if(codigoMotivo == 98)
		{
			throw new WorkflowException("Código 98 - Insatisfação do Cliente desabilitado. Utilize os códigos de cancelamento: 126, 127, 128, 129 ou 130. Especificando, na observação, a área da insatisfação.");
		}
		
		String sitCtr = consultaSituacaoContrato(codemp, codfil, numctr);
		if(sitCtr.equals("") || !sitCtr.equals("A"))
		{
			throw new WorkflowException("Situação do contrato ja se encontra inativo no sapiens ou o mesmo não foi encontrado. Favor entrar em contato com o adaministrador do sistema.");
		}
		
		NeoUser usuarioInativador = PortalUtil.getCurrentUser();
		
		String textoEmail = "";
		textoEmail += "Empresa......: " + codemp + " - " + wrapper.findValue("empresa.nomfil") + "<br>";
		
		String numctr_ofi = NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numofi"));
		textoEmail += "Número Interno...: " + numctr + "<br>";
		textoEmail += "Número Oficial...: " + numctr_ofi + "<br>";
		
		String nomeCliente = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.nomcli"));
		String endereco = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.endcli"));
		String bairro = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.baicli"));
		String cidade = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.cidcli"));
		textoEmail += "Cliente...: " + codcli + " - " + nomeCliente + "<br>";
		textoEmail += "           " + endereco + "<br>";
		textoEmail += "           " + bairro + "<br>";
		textoEmail += "           " + cidade + "<br>";
		
		//calculo
		float vMtaCom = 0;
		float vMtbCom = 0;
		float vValCom = 0;
		float vVtrCom = 0;
		float vVreCom = 0;
		float vTotCom = 0;
		float vMtaFin = 0;
		float vMtbFin = 0;
		float vValFin = 0;
		float vVtrFin = 0;
		float vVreFin = 0;
		float vTotFin = 0;
		float vPosCom = 0;
		float vPosFin = 0;
		float vTotFun = 0;
		float vVlrMta = 0;
		float usu_tipctr = 0;
		if(wrapper.findValue("numContrato.usu_tipctr") != null)
		{
			usu_tipctr = NeoUtils.safeLong(NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_tipctr")));
		}
		//consultaValoresContrato(codemp, codfil, numctr, vMtaCom, vMtbCom, vValCom, vVtrCom, vVreCom, vTotCom, vMtaFin, vMtbFin, vValFin, vVtrFin, vVreFin, vVlrMta, vTotFin, vPosCom, vPosFin, vTotFun, usu_tipctr);
		{
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT "); 
			sql.append("USU_QTDCVS, ");
			sql.append("USU_PREUNI, ");
			sql.append("USU_VLRMTA, ");
			sql.append("USU_VLRMTB, ");
			sql.append("USU_VLRVA4, ");
			sql.append("USU_VLRVA6, ");
			sql.append("USU_VLRVA6, ");
			sql.append("USU_VLRVAV, ");
			sql.append("USU_VLRVTR, ");
			sql.append("USU_VLRVRE, ");
			sql.append("USU_LIBFAT, ");
			sql.append("USU_NUMPOS, ");
			sql.append("USU_QTDFUN ");

			sql.append("FROM USU_T160CVS ");
			sql.append("WHERE usu_codemp = "+ codemp);
			sql.append("  AND usu_codfil = "+ codfil);
			sql.append("  AND usu_numctr = "+ numctr);
			sql.append("  AND usu_sitcvs = 'A'");

			Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
			Collection<Object> resultList = query.getResultList();
			if (resultList != null && !resultList.isEmpty())
			{
				for (Object result : resultList)
				{
					if (result != null)
					{
						Object[] objs = (Object[]) result;
						
						float qtdcvs = ((BigDecimal) objs[0]).floatValue();
						float preuni = ((BigDecimal) objs[1]).floatValue();
						float vlrmta = ((BigDecimal) objs[2]).floatValue();
						float vlrmtb = ((BigDecimal) objs[3]).floatValue();
						float vlrva4 = ((BigDecimal) objs[4]).floatValue();
						float vlrva6 = ((BigDecimal) objs[5]).floatValue();
						float vlrva8 = ((BigDecimal) objs[6]).floatValue();
						float vlrvav = ((BigDecimal) objs[7]).floatValue();
						float vlrvtr = ((BigDecimal) objs[8]).floatValue();
						float vlrvre = ((BigDecimal) objs[9]).floatValue();
						String libfat = (NeoUtils.safeOutputString(objs[10]));
						long numpos = NeoUtils.safeLong(NeoUtils.safeOutputString(objs[11]));
						float qtdfun = ((BigDecimal) objs[12]).floatValue();
						
						vValCom += (vlrva4 + vlrva6 + vlrva8 + vlrvav) * qtdcvs;
						vVtrCom += (vlrvtr * qtdcvs);
						vVreCom += (vlrvre * qtdcvs);
						vMtaCom += (vlrmta * qtdcvs);
						vMtbCom += (vlrmtb * qtdcvs);
						vTotCom += (qtdcvs * preuni);
						vPosCom += qtdcvs;
						
						if(libfat.equals("S"))
							vPosFin += qtdcvs;
						
						/*
						 * aguardar retorno do thiago sobre a variável usu_DesMon para fazer as validações abaixo
						 * por enquanto ficará com zero para não entrar nas regras
						 */
						float usu_DesMon = 0;
						if(usu_DesMon == 2 && libfat.equals("S"))
						{
							vValFin += ((vlrva4 + vlrva6 + vlrva8 + vlrvav) * qtdcvs);
						}
						
						if(usu_DesMon == 3 && libfat.equals("S"))
						{
							vValFin += ((vlrva4 + vlrva6 + vlrva8 + vlrvav) * qtdcvs);
							vVtrFin += (vlrvtr * qtdcvs);
						}
						
						if(usu_DesMon == 4 && libfat.equals("S"))
						{
							vValFin += ((vlrva4 + vlrva6 + vlrva8 + vlrvav) * qtdcvs);
							vVreFin += (vlrvre * qtdcvs);
						}
						
						if(usu_DesMon == 5)
						{
							vValFin += ((vlrva4 + vlrva6 + vlrva8 + vlrvav) * qtdcvs);
							vVtrFin += (vlrvtr * qtdcvs);
							vVreFin += (vlrvre * qtdcvs);
							vMtbFin += (vlrmtb + qtdcvs);
						}
						
						vVlrMta += (preuni - vValFin - vVreFin - vVtrFin - vMtbFin) * qtdcvs;
						vTotFin += (qtdcvs * preuni);
						
						if(usu_tipctr == 1)
						{
							vTotFun += (qtdcvs * qtdfun);
						}else
						{
							vTotFun += qtdfun;
						}
					}
				}
			}
		}
		
		textoEmail += "<br>";
		String inivig = NeoCalendarUtils.dateToString((GregorianCalendar) wrapper.findValue("numContrato.usu_inivig"));
		String fimvig = NeoCalendarUtils.dateToString((GregorianCalendar) wrapper.findValue("dataVigenciaInativacao"));
		String datini = NeoCalendarUtils.dateToString((GregorianCalendar) wrapper.findValue("numContrato.usu_datini"));
		String datfim = NeoCalendarUtils.dateToString((GregorianCalendar) wrapper.findValue("dataFimFaturamento"));
		long diabas =  NeoUtils.safeLong(NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_diabas")));
		
		textoEmail += "Data início do contrato............: " + inivig + " à " + fimvig + ".<br>" ;
		textoEmail += "Data início do faturamento.........: " + datini + " à " + datfim + ".<br>" ;
		
		textoEmail += "Total de funcionários do contrato..: " + vTotFun + "<br>";
		textoEmail += "Valor Total do contrato............: " + vTotFin + "<br>";
		textoEmail += "Dia base de faturamento............: " + diabas + "<br>";
		textoEmail += "Usuário de Inativação..............: " + usuarioInativador.getCode() + "<br>";
		textoEmail += "----------------------------------------------------------<br><br>";
		textoEmail += "Serviços <br>===========<br>";
		
		String servicos = buscarServicos(codemp, codfil, numctr);
		textoEmail += servicos;
		
		String desmot = NeoUtils.safeOutputString(wrapper.findValue("motivoInativacao.desmot"));
		String obsmot = NeoUtils.safeOutputString(wrapper.findValue("observacaoInativacao"));
		
		textoEmail += "<br>-----------------------------------------------<br><br>";
		textoEmail += "Motivo: " + desmot + "<br><br>";
		textoEmail += "<br>-----------------------------------------------<br><br>";
		textoEmail += "Observações<br> " + obsmot + "<br><br>";
		textoEmail += "<br>-----------------------------------------------<br><br>";
		
		/*String emails = "";
		emails = "rogerio@orsegups.com.br;cristiane@orsegups.com.br;maurelio.pinto@orsegups.com.br";
		long regctr = NeoUtils.safeLong(NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_regctr")));
		if(tipcli.equals("1"))
		{
			emails = "rogerio@orsegups.com.br;fernanda.martins@orsegups.com.br;maurelio.pinto@orsegups.com.br";
			
			
			if(regctr == 9 || regctr == 1)
			{
				emails += ";andersoncorreia@orsegups.com.br;geraldo.santos@orsegups.com.br";
				
				if(validaRegionalContrato(numctr, codemp, codfil))
					emails += ";fernando.lunelli@orsegups.com.br";
			}
		}
		
		if(tipcli.equals("2"))
		{
			emails = "cristiane@orsegups.com.br;alessandra@orsegups.com.br;maurelio.pinto@orsegups.com.br";
		}
		
		String emailRegional = getEmailGerenteRegional(regctr);
		if(!emailRegional.equals(""))
			emails += ";" + emailRegional;
		
		String tituloEmail = "Encerramento de Contrato No. " + numctr_ofi + " (" + numctr + ")";
		//copia??
		emails += ";rosileny@orsegups.com.br;giliardi@orsegups.com.br;charlot.andrade@orsegups.com.br;" + usuarioInativador.getEmail();
		
		if(codigoMotivo == 95 || codigoMotivo == 97 || codigoMotivo == 98 || codigoMotivo == 99 || codigoMotivo == 100 || 
		   codigoMotivo == 101 || codigoMotivo == 107 || codigoMotivo == 112 || codigoMotivo == 113 || codigoMotivo == 114 || 
		   codigoMotivo == 116 || codigoMotivo == 119 || codigoMotivo == 126 || codigoMotivo == 127 || codigoMotivo == 128 || 
		   codigoMotivo == 129 || codigoMotivo == 130 || codigoMotivo == 115)
		{
			//copia??
			emails += ";dilmoberger@orsegups.com.br;cristine@orsegups.com.br;gilsoncesar@orsegups.com.brvaltair.souza@orsegups.com.br";
		}
		
		if(codigoMotivo == 126 || codigoMotivo == 127 || codigoMotivo == 128)
		{
			//copia??
			emails += ";cm@orsegups.com.br";
		}
		
		//copia oculta??
		emails += ";graziela.martins@orsegups.com.br;thiago.coelho@orsegups.com.br;william.carvalho@orsegups.com.br";	
		
		//teste
		emails = "fernando.rebelo@neomind.com.br;thiago.coelho@orsegups.com.br";
		String listaEmails[] = emails.split(";");
		String template = "mail/emailInativacaoContrato.jsp";
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("textoEMail", textoEmail);
		params.put("titulo", tituloEmail);
		Collection<Mail2Render> listaMail2Send = new ArrayList<Mail2Render>();
		for(String email : listaEmails)
		{
			Mail2Render mail2Render = new Mail2Render(email, template, params);
			listaMail2Send.add(mail2Render);
		}
		
		for(Mail2Render m : listaMail2Send)
		{
			FusionRuntime.getInstance().getMailEngine().sendEMail(m);
		}*/
		
		String codeUser = usuarioInativador.getCode(); 
		
		
		long sequenciaObs = ContratoUtils.retornaSequenciaObservacaoContrato(codemp, codfil, numctr);
		NeoUser user = PortalUtil.getCurrentUser();
		long codUserSapiens = 0L;
		try
		{
			codUserSapiens = ContratoUtils.retornaCodeUsuarioSapiens(user.getCode());
		}catch(Exception e)
		{
			codUserSapiens = 1;
		}
		GregorianCalendar usu_datmov = new GregorianCalendar();
		long usu_hormov = (usu_datmov.get(usu_datmov.HOUR_OF_DAY) * 60) + usu_datmov.get(usu_datmov.MINUTE);
		ContratoUtils.geraObservacao(codemp, codfil, numctr, sequenciaObs, null, "", "A", textoEmail, codUserSapiens, usu_datmov, usu_hormov, "X");
		
		inativarContrato(codemp, codfil, numctr, codigoMotivo, obsmot, codeUser, wrapper);
	}
	
	public void inativarContrato(long codemp, long codfil, long numctr, long codigoMotivo, String obsMot, String codUserSapiens, EntityWrapper wrapper)
	{
		String query = " UPDATE E160CTR SET sitctr = 'I', codmot = " + codigoMotivo + ", obsmot = '" + obsMot + "' " +
		" WHERE codemp = " + codemp + " and codfil = " + codfil + " and numctr = " + numctr;

		StringBuilder sql = new StringBuilder();
		sql.append(query);
		Query queryExecute = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		try
		{
			queryExecute.executeUpdate();
		}catch(Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Erro " + e.getMessage());
		}
		
		GregorianCalendar dtVigencia = (GregorianCalendar) wrapper.findValue("dataVigenciaInativacao");
		String dataVigencia = ContratoUtils.retornaDataFormatoSapiens(dtVigencia);
		GregorianCalendar dtFimFaturamento = (GregorianCalendar) wrapper.findValue("dataFimFaturamento");
		String dataFimFaturamento = ContratoUtils.retornaDataFormatoSapiens(dtFimFaturamento);
		GregorianCalendar dtInativacao = new GregorianCalendar();
		String dataInativacao = ContratoUtils.retornaDataFormatoSapiens(dtInativacao);
		long horaInativacao = (dtInativacao.get(dtInativacao.HOUR) * 60) + dtInativacao.get(dtInativacao.MINUTE);
		
		NeoUser user = PortalUtil.getCurrentUser();
		long codigoUserSapiens = 0L;
		try
		{
			codigoUserSapiens = ContratoUtils.retornaCodeUsuarioSapiens(user.getCode());
		}catch(Exception e)
		{
			codigoUserSapiens = 1;
		}
		
		query = " UPDATE USU_T160CTR SET usu_sitctr = 'I', usu_fimvig = '" + dataVigencia + "', usu_datFim = '" + dataFimFaturamento + "', " +
		"usu_datmot = '" + dataInativacao + "', usu_hormot = " + horaInativacao + ", usu_codmot = " + codigoMotivo + ", usu_obsmot = '" + obsMot + "', " +
		"usu_usualt = " + codigoUserSapiens + ", usu_datalt = '2013-08-16 00:00:00.000', usu_horalt = 577 " +
		" WHERE usu_codemp = " + codemp + " and usu_codfil = " + codfil + " and usu_numctr = " + numctr + " and usu_sitctr = 'A'";

		sql = new StringBuilder();
		sql.append(query);
		queryExecute = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		try
		{
			queryExecute.executeUpdate();
		}catch(Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Erro -"+e.getMessage());
		}
	}
	
	public String getEmailGerenteRegional(long regctr)
	{
		String email = "";
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT "); 
		sql.append("R900CPL.Email ");
		sql.append("FROM USU_T200REG, R900CPL ");
		sql.append("WHERE USU_T200REG.usu_codreg = "+ regctr);
		sql.append("  AND USU_T200REG.usu_gerreg = R900CPL.Perid");
		
		Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		Collection<Object> resultList = query.getResultList();
		if (resultList != null && !resultList.isEmpty())
		{
			for (Object result : resultList)
			{
				if (result != null)
				{
					email = NeoUtils.safeOutputString(result);
				}
			}
		}
		
		return email;
	}
	
	public boolean validaRegionalContrato(long numctr, long codemp, long codfil)
	{
		boolean resposta = false;
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT "); 
		sql.append("usu_regcvs, ");
		sql.append("usu_cidctr ");
		sql.append("FROM USU_T160CVS ");
		sql.append("WHERE usu_codemp = "+ codemp);
		sql.append("  AND usu_codfil = "+ codfil);
		sql.append("  AND usu_numctr = "+ numctr);
		
		Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		Collection<Object> resultList = query.getResultList();
		if (resultList != null && !resultList.isEmpty())
		{
			for (Object result : resultList)
			{
				if (result != null)
				{
					Object[] objs = (Object[]) result;
					long regcvs = NeoUtils.safeLong(NeoUtils.safeOutputString(objs[0]));
					String cidctr = NeoUtils.safeOutputString(objs[1]);
					
					if(regcvs == 5 && cidctr.equals("ITAPOA"))
						resposta = true;
				}
			}
		}
		
		return resposta;
	}
	
	public String buscarServicos(long codemp, long codfil, long numctr)
	{
		String servicos = "";
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT "); 
		sql.append("USU_CPLCVS ");

		sql.append("FROM USU_T160CVS ");
		sql.append("WHERE usu_codemp = "+ codemp);
		sql.append("  AND usu_codfil = "+ codfil);
		sql.append("  AND usu_numctr = "+ numctr);
		sql.append("  AND usu_sitcvs = 'A' ");
		sql.append("GROUP BY USU_CPLCVS ");
		sql.append("ORDER BY USU_CPLCVS ");
		
		Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		Collection<Object> resultList = query.getResultList();
		if (resultList != null && !resultList.isEmpty())
		{
			for (Object result : resultList)
			{
				if (result != null)
				{
					servicos += NeoUtils.safeOutputString(result) + "<br>";
				}
			}
		}
		
		return servicos;
	}
	
	public void consultaValoresContrato(float codemp, float codfil, float numctr, float vMtaCom, float vMtbCom, float vValCom, float vVtrCom, float vVreCom, 
		float vTotCom, float vMtaFin, float vMtbFin, float vValFin, float vVtrFin, float vVreFin, float vVlrMta, float vTotFin, float vPosCom, float vPosFin, 
		float vTotFun, float usu_tipctr)
	{
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT "); 
		sql.append("USU_QTDCVS, ");
		sql.append("USU_PREUNI, ");
		sql.append("USU_VLRMTA, ");
		sql.append("USU_VLRMTB, ");
		sql.append("USU_VLRVA4, ");
		sql.append("USU_VLRVA6, ");
		sql.append("USU_VLRVA6, ");
		sql.append("USU_VLRVAV, ");
		sql.append("USU_VLRVTR, ");
		sql.append("USU_VLRVRE, ");
		sql.append("USU_LIBFAT, ");
		sql.append("USU_NUMPOS, ");
		sql.append("USU_QTDFUN ");

		sql.append("FROM USU_T160CVS ");
		sql.append("WHERE usu_codemp = "+ codemp);
		sql.append("  AND usu_codfil = "+ codfil);
		sql.append("  AND usu_numctr = "+ numctr);
		sql.append("  AND usu_sitcvs = 'A'");

		Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		Collection<Object> resultList = query.getResultList();
		if (resultList != null && !resultList.isEmpty())
		{
			for (Object result : resultList)
			{
				if (result != null)
				{
					Object[] objs = (Object[]) result;
					
					float qtdcvs = ((BigDecimal) objs[0]).floatValue();
					float preuni = ((BigDecimal) objs[1]).floatValue();
					float vlrmta = ((BigDecimal) objs[2]).floatValue();
					float vlrmtb = ((BigDecimal) objs[3]).floatValue();
					float vlrva4 = ((BigDecimal) objs[4]).floatValue();
					float vlrva6 = ((BigDecimal) objs[5]).floatValue();
					float vlrva8 = ((BigDecimal) objs[6]).floatValue();
					float vlrvav = ((BigDecimal) objs[7]).floatValue();
					float vlrvtr = ((BigDecimal) objs[8]).floatValue();
					float vlrvre = ((BigDecimal) objs[9]).floatValue();
					String libfat = (NeoUtils.safeOutputString(objs[10]));
					long numpos = NeoUtils.safeLong(NeoUtils.safeOutputString(objs[11]));
					float qtdfun = ((BigDecimal) objs[12]).floatValue();
					
					vValCom += (vlrva4 + vlrva6 + vlrva8 + vlrvav) * qtdcvs;
					vVtrCom += (vlrvtr * qtdcvs);
					vVreCom += (vlrvre * qtdcvs);
					vMtaCom += (vlrmta * qtdcvs);
					vMtbCom += (vlrmtb * qtdcvs);
					vTotCom += (qtdcvs * preuni);
					vPosCom += qtdcvs;
					
					if(libfat.equals("S"))
						vPosFin += qtdcvs;
					
					/*
					 * aguardar retorno do thiago sobre a variável usu_DesMon para fazer as validações abaixo
					 * por enquanto ficará com zero para não entrar nas regras
					 */
					float usu_DesMon = 0;
					if(usu_DesMon == 2 && libfat.equals("S"))
					{
						vValFin += ((vlrva4 + vlrva6 + vlrva8 + vlrvav) * qtdcvs);
					}
					
					if(usu_DesMon == 3 && libfat.equals("S"))
					{
						vValFin += ((vlrva4 + vlrva6 + vlrva8 + vlrvav) * qtdcvs);
						vVtrFin += (vlrvtr * qtdcvs);
					}
					
					if(usu_DesMon == 4 && libfat.equals("S"))
					{
						vValFin += ((vlrva4 + vlrva6 + vlrva8 + vlrvav) * qtdcvs);
						vVreFin += (vlrvre * qtdcvs);
					}
					
					if(usu_DesMon == 5)
					{
						vValFin += ((vlrva4 + vlrva6 + vlrva8 + vlrvav) * qtdcvs);
						vVtrFin += (vlrvtr * qtdcvs);
						vVreFin += (vlrvre * qtdcvs);
						vMtbFin += (vlrmtb + qtdcvs);
					}
					
					vVlrMta += (preuni - vValFin - vVreFin - vVtrFin - vMtbFin) * qtdcvs;
					vTotFin += (qtdcvs * preuni);
					
					if(usu_tipctr == 1)
					{
						vTotFun += (qtdcvs * qtdfun);
					}else
					{
						vTotFun += qtdfun;
					}
				}
			}
		}
	}
	
	public String consultaSituacaoContrato(long codemp, long codfil, long numctr)
	{
		String sitctr = "";
		
		//select * from USU_T160CTR where usu_codemp = and usu_codfil = and usu_numctr
		StringBuilder sql = new StringBuilder();
		sql.append("select "); 
		sql.append("ctr.usu_sitctr ");

		sql.append("from USU_T160CTR ctr ");
		sql.append("WHERE ctr.usu_codemp = "+ codemp);
		sql.append("  AND ctr.usu_codfil = "+ codfil);
		sql.append("  AND ctr.usu_numctr = "+ numctr);
		
		Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		Collection<Object> resultList = query.getResultList();
		if (resultList != null && !resultList.isEmpty())
		{
			for (Object result : resultList)
			{
				if (result != null)
				{
					sitctr = NeoUtils.safeOutputString(result);
				}
			}
		}
		
		return sitctr;
	}
	
	public void back(EntityWrapper wrapper, Activity activity)
	{
		
	}
	
	
}
