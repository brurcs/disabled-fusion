package com.neomind.fusion.custom.orsegups.rsc;

import java.util.GregorianCalendar;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.util.NeoDateUtils;


public class RSCPrazoNovaSolicitacao implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		RSCUtils oRSCUtil = new RSCUtils();
		GregorianCalendar prazoDeadLine = oRSCUtil.retornaPrazoDeadLine("Registrar RSC", new GregorianCalendar());
			
		processEntity.findField("prazoRegistrarRSC").setValue(prazoDeadLine);		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub
	}
}
