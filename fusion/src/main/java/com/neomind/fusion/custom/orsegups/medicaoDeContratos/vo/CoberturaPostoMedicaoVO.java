package com.neomind.fusion.custom.orsegups.medicaoDeContratos.vo;

import java.util.GregorianCalendar;

public class CoberturaPostoMedicaoVO
{
	public long numPos;
	public String dataCobertura;
	public GregorianCalendar datCob;
	public long numCad;
	public String nomFun;
	public long numEmp;

	public GregorianCalendar getDatCob()
	{
		return datCob;
	}

	public void setDatCob(GregorianCalendar datCob)
	{
		this.datCob = datCob;
	}

	public long getNumPos()
	{
		return numPos;
	}

	public void setNumPos(long numPos)
	{
		this.numPos = numPos;
	}

	public String getDataCobertura()
	{
		return dataCobertura;
	}

	public void setDataCobertura(String dataCobertura)
	{
		this.dataCobertura = dataCobertura;
	}

	public long getNumCad()
	{
		return numCad;
	}

	public void setNumCad(long numCad)
	{
		this.numCad = numCad;
	}

	public String getNomFun()
	{
		return nomFun;
	}

	public void setNomFun(String nomFun)
	{
		this.nomFun = nomFun;
	}

	public long getNumEmp()
	{
		return numEmp;
	}

	public void setNumEmp(long numEmp)
	{
		this.numEmp = numEmp;
	}

}
