package com.neomind.fusion.custom.orsegups.contract;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class ContratoInviabilizarTarefa implements AdapterInterface {
	
	@Override
	public void start(Task origin, EntityWrapper wrapper,Activity activity) {
		ArrayList<String> postosContratoRemover = new ArrayList<String>();
		try{
			
			Long codEmpresa = (Long) wrapper.findValue("empresa.codemp");
			Long codFil = (Long) wrapper.findValue("empresa.codfil");
			Long movimentoContrato = (Long) wrapper.findValue("movimentoContratoNovo.codTipo");
			Long movimentoAlteracaoContrato = (Long) wrapper.findValue("movimentoAlteracaoContrato.codTipo");
			
			NeoObject oDadosGerais = (NeoObject) wrapper.findValue("dadosGeraisContrato");
			EntityWrapper wDadosGerais = new EntityWrapper(oDadosGerais);
			String numContrato = NeoUtils.safeOutputString(wDadosGerais.findValue("numeroContratoSapiens"));
			
			if (movimentoContrato != 5){ //se contrato novo
				if (numContrato != null){
					removeContratoNovo(Long.parseLong(numContrato), codEmpresa, codFil);
				}
			}else{ // se for alteração de cnpj e se tiver gerado contrato novo
				if (numContrato != null && movimentoContrato == 1){
					removeContratoNovo(Long.parseLong(numContrato), codEmpresa, codFil);
				}
			}
			
		}catch(Exception e){
			throw new WorkflowException("programar a Inviabilização da Tarefa, Erro: " + e );
		}
		
	}

	@Override
	public void back(EntityWrapper wrapper, Activity activity) {
		// TODO Auto-generated method stub
		
	}
	
	
	public static void removeContratoNovo(Long usu_numctr, Long usu_codemp, Long usu_codfil){
		StringBuilder query = new StringBuilder();
		
		//remove observacoes
		query.append(" delete usu_t160OBS where usu_numctr ="+usu_numctr + " ");
		query.append(" and not exists (select 1 from e160ctr where numctr = usu_numctr) ");
		query.append(" and not exists (select 1 from usu_t160cvs where usu_numctr = usu_t160obs.usu_numctr); ");
		
		//remove contrato
		query.append(" delete usu_t160ctr where usu_numctr = "+usu_numctr+" and usu_codemp = "+usu_codemp+" and usu_codfil = "+usu_codfil+" " );
		query.append(" and not exists (select 1 from e160ctr where numctr = usu_numctr) ");
		query.append(" and not exists (select 1 from usu_t160cvs where usu_numctr = usu_t160ctr.usu_numctr); ");
		
		Connection connection = OrsegupsUtils.getSqlConnection("SAPIENS");
			
		PreparedStatement st = null;
		try
		{	
			if (query != null && !query.toString().equals("")){
				st = connection.prepareStatement(query.toString());
				System.out.println("[FLUXO CONTRATOS]- removeContratoNovo" + st.executeUpdate());
			}
		}

		catch (Exception e) {
			e.printStackTrace();
			throw new WorkflowException("Erro ao fazer rollback na inserção dos postos."+e.getMessage());
		}
		finally
		{
			if (st != null)
			{
				try
				{
					st.close();
					connection.close();
				}
				catch (SQLException e)
				{
					e.printStackTrace();
				}
			}
		}

	}

}
