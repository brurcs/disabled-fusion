package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.HtmlEmail;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.FCNCancelarContratoEletrônica;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsConnectionUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.engine.runtime.RuntimeEngine;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class CancelarProcessoC025 implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(CancelarProcessoC025.class);
	private Map<Integer, String> serverInfo = OrsegupsConnectionUtils.getServerInfo("SIGMA");

	@Override
	public void execute(CustomJobContext arg0)
	{

		Connection conn = null;
		StringBuilder sqlTarefas = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		Long key = GregorianCalendar.getInstance().getTimeInMillis();

		try
		{

			log.warn("##### INICIAR CANCELAMENTO TAREFA C025 - Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));

			conn = PersistEngine.getConnection("");
			GregorianCalendar dataAtual = (GregorianCalendar) GregorianCalendar.getInstance();
			sqlTarefas.append(" select wf.code from dbo.D_FCNcancelamentoEletInadimplenciaNew fcn ");
			sqlTarefas.append(" inner join dbo.WFProcess wf on fcn.wfprocess_neoId = wf.neoId ");
			sqlTarefas.append(" where wf.processState = 0 ");
			sqlTarefas.append(" and wf.saved = 1 ");
			sqlTarefas.append(" and wf.startDate >= '2015-04-06'");
			sqlTarefas.append(" order by wf.code ");
			pstm = conn.prepareStatement(sqlTarefas.toString());
			rs = pstm.executeQuery();

			FCNCancelarContratoEletrônica cancelarContratoEletrônica = new FCNCancelarContratoEletrônica();
			List<FCNCancelarContratoEletrônica> listTarefas = new ArrayList<FCNCancelarContratoEletrônica>();
			List<FCNCancelarContratoEletrônica> listTarefasCanceladas = new ArrayList<FCNCancelarContratoEletrônica>();

			int count = 0;
			while (rs.next())
			{

				cancelarContratoEletrônica = new FCNCancelarContratoEletrônica();

				cancelarContratoEletrônica.setCode(String.format("%06d", rs.getInt("code")));

				listTarefas.add(cancelarContratoEletrônica);

				System.out.println("Contador>>> " + count + ", Tarefa>>> " + listTarefas.get(count).getCode());

				count++;
			}
			System.out.println("Fim de capturar as Tarefas");

			OrsegupsUtils.closeConnection(conn, pstm, rs);

			String textoObjeto = "";
			if (listTarefas != null && !listTarefas.isEmpty())
			{
				for (FCNCancelarContratoEletrônica cancelamento : listTarefas)
				{
					sqlTarefas = new StringBuilder();
					conn = null;
					conn = PersistEngine.getConnection("SAPIENS");
					pstm = null;
					rs = null;
					sqlTarefas.append(" Select wf.neoId,wf.code, t.VctPro, t.VctOri, cvs.NumCtr, t.CodCli");
					sqlTarefas.append(" from [CACUPE\\SQL02].Fusion_PRODUCAO.DBO.D_FCNcancelamentoEletInadimplenciaNew fcn ");
					sqlTarefas.append(" inner join [CACUPE\\SQL02].Fusion_PRODUCAO.dbo.WFProcess wf on fcn.wfprocess_neoId = wf.neoId ");
					sqlTarefas.append(" inner join [CACUPE\\SQL02].Fusion_PRODUCAO.dbo.D_FCNcancelamentoEletInadimplenciaNew_listaTitulos ti on fcn.neoid = ti.D_FCNcancelamentoEletInadimplenciaNew_neoId ");
					sqlTarefas.append(" inner join [CACUPE\\SQL02].Fusion_PRODUCAO.dbo.D_FCITituloSapiens ts on ts.neoId = ti.listaTitulos_neoId ");
					sqlTarefas.append(" inner join [CACUPE\\SQL02].Fusion_PRODUCAO.dbo.X_VFUSCTRSAP c on c.neoId = fcn.contratoSapiens_neoId ");
					sqlTarefas.append(" inner join e301tcr t on t.codemp = ts.empresa and t.codfil = ts.filial and t.numtit = ts.numeroTitulo and t.codtpt = ts.tipoTitulo ");
					sqlTarefas.append(" inner join e160cvs cvs on cvs.codemp = t.codemp and cvs.filnfv = t.codfil and cvs.numnfv = t.numnfv and cvs.codsnf = t.codsnf and cvs.numctr = substring(c.pk,9,6) ");
					sqlTarefas.append(" where wf.processState = 0 ");
					sqlTarefas.append(" and wf.startDate >= '2015-04-06' ");
					sqlTarefas.append(" and wf.saved = 1 ");
					sqlTarefas.append(" and  t.sittit = 'AB' ");
					sqlTarefas.append(" and t.VctPro >=  getDate() -1 ");
					sqlTarefas.append(" and wf.code = " + cancelamento.getCode());

					pstm = conn.prepareStatement(sqlTarefas.toString());
					rs = pstm.executeQuery();

					boolean entrou = Boolean.FALSE;
					while (rs.next())
					{
						Long neoId = rs.getLong("neoId");

						WFProcess proc = PersistEngine.getObject(WFProcess.class, new QLEqualsFilter("neoId", neoId));

						if (proc != null)
						{
							RuntimeEngine.getProcessService().cancel(proc, "Rotina Cancelar Processo C025");

							//adiciona informações para a listaTarefas canceladas 
							cancelarContratoEletrônica = new FCNCancelarContratoEletrônica();
							cancelarContratoEletrônica.setCode(String.format("%06d", rs.getInt("code")));

							listTarefasCanceladas.add(cancelarContratoEletrônica);
						}

						//salvar as tarefas canceladas
						InstantiableEntityInfo cda = AdapterUtils.getInstantiableEntityInfo("CancelarTarefaDeCancelamentoEletronicaInadiplencia");
						NeoObject noFCI = cda.createNewInstance();
						EntityWrapper tmeWrapper = new EntityWrapper(noFCI);
						tmeWrapper.findField("codigoTarefa").setValue(cancelamento.getCode());
						tmeWrapper.findField("dataCancelamento").setValue(dataAtual);
						tmeWrapper.findField("cliente").setValue(rs.getString("CodCli"));
						tmeWrapper.findField("contrato").setValue(rs.getString("NumCtr"));
						PersistEngine.persist(noFCI);

						entrou = Boolean.TRUE;
						System.out.println("Tarefa cancelada do Processo C025: " + cancelamento.getCode());
						textoObjeto += "<p><strong>Tarefa cancelada: </strong> " + cancelamento.getCode() + "- Cliente: " + rs.getString("CodCli") + "</p>";
						break;
					}
					if (!entrou)
					{
						System.out.println("Não Finalizou a Tarefa " + cancelamento.getCode());
					}
					OrsegupsUtils.closeConnection(conn, pstm, rs);

				}

			}

			OrsegupsUtils.closeConnection(conn, pstm, rs);

			listTarefasCanceladas.add(cancelarContratoEletrônica);
			
			if (!textoObjeto.equals(""))
			{
				String subject = "[Cancelamento das Tarefas - Cancelamento Eletrônica Inadimplência] Resumo do dia " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy");
				enviarEmail(textoObjeto, subject);
			}

			//Método para verificar se a tarefa passou pela atividade Cancelar Contrato Sapiens 
			if (listTarefasCanceladas != null && !listTarefasCanceladas.isEmpty())
			{
				verificarAtividadeCancelarContratoSapiens(listTarefasCanceladas);
			}

			log.warn("##### FIM CANCELAMENTO TAREFA C025 - Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		}
		catch (Exception e)
		{
			log.error("#### ERRO AO INICIAR CANCELAMENTO TAREFA C025 Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
			System.out.println("[" + key + "] #### ERRO AO INICIAR CANCELAMENTO TAREFA C025 Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		}
		finally
		{
			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				log.error("##### ERRO AO INICIAR CANCELAMENTO TAREFA C025 Data " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));

				return;
			}
		}

	}

	public int somaDataVencimentoOrigem(GregorianCalendar dataInicio, GregorianCalendar dataFim)
	{
		Date data1 = dataFim.getTime();
		Date data2 = dataInicio.getTime();
		double diffDias = Math.floor((data1.getTime() - data2.getTime()) / 1000.0 / 86400.00);
		int inteiro = (int) diffDias;
		return inteiro;
	}

	public void enviarEmail(String textoObjeto, String subject)
	{
		try
		{
			MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

			MailSettings mailClone = new MailSettings();
			mailClone.setMinutesInterval(settings.getMinutesInterval());
			mailClone.setFromEMail(settings.getFromEMail());
			mailClone.setFromName(settings.getFromName());
			mailClone.setRenderServer(settings.getRenderServer());
			mailClone.setSecurityFromEmail(settings.getSecurityFromEmail());
			mailClone.setSecurityCopyEmail(settings.getSecurityCopyEmail());
			mailClone.setSmtpSettings(settings.getSmtpSettings());
			mailClone.setPort(settings.getPort());
			mailClone.setSmtpServer(settings.getSmtpServer());
			mailClone.setEnabled(settings.isEnabled());
			mailClone.setFromEMail("fusion@orsegups.com.br");//da onde esta vindo
			mailClone.setFromName("Orsegups Partipações S.A.");

			HtmlEmail noUserEmail = new HtmlEmail();
			StringBuilder noUserMsg = new StringBuilder();

			noUserEmail.setCharset("ISO-8859-1");

			//fernanda.maciel@orsegups.com.br;rafael.santos@orsegups.com.br;ivanya.silva@orsegups.com.br;valtair.souza@orsegups.com.br;camila.silva@orsegups.com.br
			noUserEmail.addTo("fernanda.maciel@orsegups.com.br");
			noUserEmail.addTo("ivanya.silva@orsegups.com.br");
			noUserEmail.addTo("camila.silva@orsegups.com.br");
			noUserEmail.addTo("emailautomatico@orsegups.com.br");
			//noUserEmail.addTo("thiago.coelho@orsegups.com.br");
			

			//noUserEmail.addCc("emailautomatico@orsegups.com.br");//para quem vai com copia

			noUserEmail.setSubject(subject);
			noUserMsg.append("<!doctype html>");
			noUserMsg.append("<html>");
			noUserMsg.append("<head>");
			noUserMsg.append("<meta charset=\"utf-8\">");
			noUserMsg.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">Este é um e-mail automático do Sistema Fusion e não deve ser respondido.<br><br>");
			noUserMsg.append("</head>");
			noUserMsg.append("<body>");
			noUserMsg.append("<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#ffffff\">");
			noUserMsg.append("          <tbody>");
			noUserMsg.append("		    <td align=\"left\" valign=\"top\" >");
			noUserMsg.append("               <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/logoORS.jpg\" >");
			noUserMsg.append("            </td>");
			noUserMsg.append("			<td>");
			noUserMsg.append(textoObjeto);
			noUserMsg.append("			</td>");
			noUserMsg.append("     </tbody></table>");
			noUserMsg.append("</body></html>");
			noUserEmail.setHtmlMsg(noUserMsg.toString());
			noUserEmail.setContent(noUserMsg.toString(), "text/html");
			mailClone.applyConfig(noUserEmail);

			noUserEmail.send();
			//OrsegupsUtils.sendEmail2SendGrid(addTo, from, subject, "", noUserMsg.toString());
		}
		catch (Exception e)
		{
			e.printStackTrace();

		}

	}

	private void verificarAtividadeCancelarContratoSapiens(List<FCNCancelarContratoEletrônica> listTarefas)
	{

		Connection conn = null;
		StringBuilder sqlTarefas = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		String solicitante = OrsegupsUtils.getUserNeoPaper("solicitantePgtoCancelamentoCTR");
		String executor = OrsegupsUtils.getUserNeoPaper("executorPgtoCancelamentoCTR");
		String origem = "1";
		String avanca = "vai";
		String textoObjeto = "";
		String code = "";
		String titulo = "";
		String descricao = "";

		try
		{

			for (FCNCancelarContratoEletrônica cancelamento : listTarefas)
			{
				sqlTarefas = new StringBuilder();
				conn = null;
				conn = PersistEngine.getConnection("SAPIENS");
				pstm = null;
				rs = null;
				
				sqlTarefas.append(" Select wf.code, cvs.NumCtr, t.codemp, t.codfil, cli.CodCli, cli.NomCli");
				sqlTarefas.append(" from [CACUPE\\SQL02].Fusion_PRODUCAO.DBO.D_FCNcancelamentoEletInadimplenciaNew fcn ");
				sqlTarefas.append(" inner join [CACUPE\\SQL02].Fusion_PRODUCAO.dbo.WFProcess wf WITH (NOLOCK) on fcn.wfprocess_neoId = wf.neoId ");
				sqlTarefas.append(" inner join [CACUPE\\SQL02].Fusion_PRODUCAO.dbo.activity a WITH (NOLOCK) on (wf.neoId= a.process_neoid) ");
				sqlTarefas.append(" inner join [CACUPE\\SQL02].Fusion_PRODUCAO.dbo.activitymodel am WITH (NOLOCK) on am.neoid = a.model_neoid  ");
				sqlTarefas.append(" inner join [CACUPE\\SQL02].Fusion_PRODUCAO.dbo.D_FCNcancelamentoEletInadimplenciaNew_listaTitulos ti WITH (NOLOCK) on fcn.neoid = ti.D_FCNcancelamentoEletInadimplenciaNew_neoId ");
				sqlTarefas.append(" inner join [CACUPE\\SQL02].Fusion_PRODUCAO.dbo.D_FCITituloSapiens ts WITH (NOLOCK) on ts.neoId = ti.listaTitulos_neoId ");
				sqlTarefas.append(" inner join [CACUPE\\SQL02].Fusion_PRODUCAO.dbo.X_VFUSCTRSAP c WITH (NOLOCK) on c.neoId = fcn.contratoSapiens_neoId ");
				sqlTarefas.append(" inner join e301tcr t WITH (NOLOCK) on t.codemp = ts.empresa and t.codfil = ts.filial and t.numtit = ts.numeroTitulo and t.codtpt = ts.tipoTitulo ");
				sqlTarefas.append(" inner join e160cvs cvs WITH (NOLOCK) on cvs.codemp = t.codemp and cvs.filnfv = t.codfil and cvs.numnfv = t.numnfv and cvs.codsnf = t.codsnf and cvs.numctr = substring(c.pk,9,6) ");
				sqlTarefas.append(" inner join E085CLI cli WITH (NOLOCK) ON t.CodCli = cli.CodCli ");
				sqlTarefas.append(" and am.name = 'Cancelar Contrato Sapiens - Inadimplência' ");
				sqlTarefas.append(" and (a.finishdate is not null or a.finishdate <> '') ");
				sqlTarefas.append(" and wf.code = " + cancelamento.getCode());
				sqlTarefas.append(" group by wf.code, cvs.NumCtr, t.codemp, t.codfil, cli.CodCli, cli.NomCli ");

				pstm = conn.prepareStatement(sqlTarefas.toString());
				rs = pstm.executeQuery();

				GregorianCalendar prazo = OrsegupsUtils.getSpecificWorkDay(new GregorianCalendar(), 1L);
				prazo.set(Calendar.HOUR_OF_DAY, 23);
				prazo.set(Calendar.MINUTE, 59);
				prazo.set(Calendar.SECOND, 59);

				while (rs.next())
				{
					
					titulo = "Pagamento após cancelamento do contrato " + rs.getInt("NumCtr");
					descricao = "O(s) título(s) foram pagos/prorrogados após o cancelamento do contrato " + rs.getInt("NumCtr") + "<br/><br/>";
					descricao = descricao + "<b>Empresa/Filial:</b> " + rs.getInt("codemp") + "/" + rs.getInt("codfil") + "<br/>";
					descricao = descricao + "<b>Cliente:</b> " + rs.getInt("CodCli") + " - " + rs.getString("NomCli") + "<br/><br/>";
					descricao = descricao + "Seguem as respectivas Contas Sigma desse contrato:<br/>";
					
					StringBuilder sqlContaSigma = new StringBuilder();
					Connection conn1 = null;
					conn1 = PersistEngine.getConnection("SAPIENS");
					PreparedStatement pstm1 = null;
					ResultSet rs1 = null;
					
					sqlContaSigma.append(" select central.id_central, central.particao from usu_t160sig sig "); 
					sqlContaSigma.append(" left join ["+this.serverInfo.get(OrsegupsConnectionUtils.NOME_SERVIDOR)+"\\"+this.serverInfo.get(OrsegupsConnectionUtils.INSTANCIA)+"].SIGMA90.dbo.dbCentral central on sig.usu_codcli = central.cd_cliente ");
					sqlContaSigma.append(" where sig.usu_codemp = " + rs.getInt("codemp") + " and sig.usu_codfil = " + rs.getInt("codfil") + " and sig.usu_numctr = " + rs.getInt("NumCtr"));
					sqlContaSigma.append(" group by central.id_central, central.particao ");
					
					pstm1 = conn1.prepareStatement(sqlContaSigma.toString());
					rs1 = pstm1.executeQuery();

					while (rs1.next())
					{
						descricao = descricao + "<b>Conta:</b> " + rs1.getString("id_central") + " [" + rs1.getString("particao") + "]" + "<br/>";
					}
					OrsegupsUtils.closeConnection(conn1, pstm1, rs1);

					IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
					code = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, origem, avanca, prazo);
					textoObjeto += "<p><strong>Abertura da Tarefa: Pagamento após cancelamento do contrato, devido a tarefa de Cancelamento Eletrônica Inadimplência: </strong> " + cancelamento.getCode() + "- Tarefa Simples: " + code + "</p>";
					break;
				}
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			OrsegupsUtils.closeConnection(conn, pstm, rs);

			if (!textoObjeto.equals(""))
			{
				String subject = "[Pagamento após cancelamento do contrato] Resumo do dia " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy");
				enviarEmail(textoObjeto, subject);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}
}
