package com.neomind.fusion.custom.orsegups.ait.controller;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.ait.engine.ProdutividadeAitEngine;
import com.neomind.fusion.custom.orsegups.ait.vo.AtendimentoValorVO;
import com.neomind.fusion.custom.orsegups.ait.vo.AtendimentoVtrVO;
import com.neomind.fusion.custom.orsegups.ait.vo.AtendimentosAitVO;
import com.neomind.fusion.custom.orsegups.maps.vo.ViaturaVO;
import com.neomind.util.NeoDateUtils;

@Path(value = "produtividadeAit")
public class ProdutividadeAitController
{
	private static final Log log = LogFactory.getLog(ProdutividadeAitController.class);

	@GET
	@Path("competencia")
	@Produces("application/json")
	public List<String> listaCompetencias()
	{
		log.warn("Executando consulta competencias " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		ProdutividadeAitEngine produtividadeAitEngine = new ProdutividadeAitEngine();
		List<String> listaCompetencias = produtividadeAitEngine.getCompetencias(new GregorianCalendar());

		return listaCompetencias;

	}

	@GET
	@Path("ait")
	@Produces("application/json")
	public List<AtendimentoVtrVO> listaAit()
	{
		log.warn("Executando consulta ait " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		ProdutividadeAitEngine produtividadeAitEngine = new ProdutividadeAitEngine();
		List<AtendimentoVtrVO> listaViaturas = produtividadeAitEngine.getListaViatura();

		return listaViaturas;

	}

	@GET
	@Path("atendimentos/{cdViatura}/{mes}/{ano}")
	@Produces("application/json")
	public List<AtendimentosAitVO> listaAtendimentos(@PathParam("cdViatura") String cdViatura, @PathParam("mes") String mes, @PathParam("ano") String ano)
	{
		log.warn("Executando consulta atendimentos " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		ProdutividadeAitEngine produtividadeAitEngine = new ProdutividadeAitEngine();
		List<AtendimentosAitVO> listaAtendimentos = produtividadeAitEngine.getAtendimentosAit(cdViatura, mes + "/" + ano);

		return listaAtendimentos;

	}

	@GET
	@Path("foto/{cdViatura}")
	@Produces(MediaType.APPLICATION_JSON)
	public String buscaFotoAit(@PathParam("cdViatura") String cdViatura)
	{
		log.warn("Executando consulta foto " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		ProdutividadeAitEngine produtividadeAitEngine = new ProdutividadeAitEngine();
		Long idFoto = produtividadeAitEngine.getFotoAit(cdViatura);

		return String.valueOf(idFoto);

	}

	@GET
	@Path("dadosAit/{cdViatura}")
	@Produces(MediaType.APPLICATION_JSON)
	public ViaturaVO buscaDadosAit(@PathParam("cdViatura") String cdViatura)
	{
		log.warn("Executando consulta foto " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		ProdutividadeAitEngine produtividadeAitEngine = new ProdutividadeAitEngine();
		ViaturaVO viaturaVO = produtividadeAitEngine.getDadosAit(cdViatura);

		return viaturaVO;

	}

	@GET
	@Path("dadosProdutividade/{cdViatura}")
	@Produces(MediaType.APPLICATION_JSON)
	public ViaturaVO buscaProdutividadeAit(@PathParam("cdViatura") String cdViatura)
	{
		log.warn("Executando consulta foto " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		ProdutividadeAitEngine produtividadeAitEngine = new ProdutividadeAitEngine();
		ViaturaVO viaturaVO = produtividadeAitEngine.getDadosAit(cdViatura);

		return viaturaVO;

	}

	@GET
	@Path("relatoAtendimento/{cdViatura}/{mes}/{ano}/{relato}")
	@Produces(MediaType.APPLICATION_JSON)
	public String cadastraRelato(@PathParam("cdViatura") String cdViatura, @PathParam("mes") String mes, @PathParam("ano") String ano, @PathParam("relato") String relato)
	{

		log.warn("Executando consulta atendimentos " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		ProdutividadeAitEngine produtividadeAitEngine = new ProdutividadeAitEngine();
		String retorno = produtividadeAitEngine.cadastraRelatoProdutividade(cdViatura, mes + "/" + ano, relato);
		
		return "{\"id\":\"" + retorno + "\", \"return\":\"OK\"}";

	}

	@GET
	@Path("obsevacaoProdutividade/{cdViatura}/{mes}/{ano}")
	@Produces(MediaType.APPLICATION_JSON)
	public AtendimentosAitVO dadosProdutividade(@PathParam("cdViatura") String cdViatura, @PathParam("mes") String mes, @PathParam("ano") String ano)
	{
		log.warn("Executando consulta foto " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		ProdutividadeAitEngine produtividadeAitEngine = new ProdutividadeAitEngine();
		AtendimentosAitVO aitVO = produtividadeAitEngine.buscaProdutividade(cdViatura, mes + "/" + ano);

		return aitVO;

	}

	public Boolean validaVinculoViaturaColaborador(Long cdViatura)
	{
		log.warn("Executando consulta foto " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		ProdutividadeAitEngine produtividadeAitEngine = new ProdutividadeAitEngine();
		Boolean flag = produtividadeAitEngine.validaVinculoViaturaColaborador(cdViatura);

		return flag;

	}
	
	public HashMap<String, Long> retornaDadosVinculoViaturaColaborador(Long cdViatura)
	{
		log.warn("Executando consulta foto " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		ProdutividadeAitEngine produtividadeAitEngine = new ProdutividadeAitEngine();
		HashMap<String, Long> dados = produtividadeAitEngine.retornaDadosViaturaColaborador(cdViatura);

		return dados;

	}
	
	public Long retornaCodigoDoCalculo(Long empresa, String competencia)
	{
		log.warn("Executando consulta foto " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		ProdutividadeAitEngine produtividadeAitEngine = new ProdutividadeAitEngine();
		Long codCal = produtividadeAitEngine.retornaCodigoCalculo(empresa, competencia);

		return codCal;

	}
	
	public boolean insereLancamentoProdutividade(Long codCal, Double valor, Long empresa, Long numCad)
	{
		log.warn("Executando consulta foto " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		ProdutividadeAitEngine produtividadeAitEngine = new ProdutividadeAitEngine();
		boolean flag = produtividadeAitEngine.insereLancamento(codCal, valor, empresa, numCad);

		return flag;

	}
	
	public String retornaStringMatriculaEmpresaColaborador(Long cpf)
	{
		log.warn("Executando consulta foto " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		ProdutividadeAitEngine produtividadeAitEngine = new ProdutividadeAitEngine();
		String texto = produtividadeAitEngine.getColaborador(cpf);

		return texto;

	}
	
	public Long retornaCpfColaboradorViatura(Long cdViatura)
	{
		log.warn("Executando consulta foto " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		ProdutividadeAitEngine produtividadeAitEngine = new ProdutividadeAitEngine();
		Long cpf = produtividadeAitEngine.retornaCpf(cdViatura);

		return cpf;

	}
	
	public NeoObject retornaCPASituacao(Long codigo)
	{
		log.warn("Executando consulta foto " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		ProdutividadeAitEngine produtividadeAitEngine = new ProdutividadeAitEngine();
		NeoObject neoObject = produtividadeAitEngine.retornaCPASituacao(codigo);

		return neoObject;

	}
	
	public AtendimentoValorVO getValores(Long cdViatura, String competencia)
	{
		log.warn("Executando consulta valores " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		ProdutividadeAitEngine produtividadeAitEngine = new ProdutividadeAitEngine();
		AtendimentoValorVO atendimentoValorVO = produtividadeAitEngine.getValorLancado(competencia, cdViatura);

		return atendimentoValorVO;

	}
	
	@GET
	@Path("rankingProdutividade/{regional}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AtendimentoVtrVO> getRankingProdutividade(@PathParam("regional") String regional)
	{
		log.warn("Executando consulta ranking " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		ProdutividadeAitEngine produtividadeAitEngine = new ProdutividadeAitEngine();
		List<AtendimentoVtrVO> ranking = produtividadeAitEngine.getRankingProdutividade(regional);

		return ranking;

	}
	
	
	@GET
	@Path("getPermissaoExibicao")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, Boolean> getPermissaoExibicao()
	{
		log.warn("Executando consulta permissão exibição " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		ProdutividadeAitEngine produtividadeAitEngine = new ProdutividadeAitEngine();
		Map<String, Boolean>  permissao = produtividadeAitEngine.getPermissaoExibicao();
				
		return permissao;

	}
		
}
