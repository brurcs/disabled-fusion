package com.neomind.fusion.custom.orsegups.jms;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TesteJmsServlet
 */
@WebServlet(name = "TesteJMSServlet", urlPatterns = { "/testejms/*" })
public class TesteJmsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TesteJmsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String transaction = request.getParameter("type");
		
		if(transaction.equals("callCenter"))
			NeoJMSPublisher.getInstance().sendCallCenterMessage("teste jms neomind", "testeNeo");
		else if(transaction.equals("mapAlertAitTopic"))
			NeoJMSPublisher.getInstance().sendAlertAitMessage("teste jms Neomind", "testeNeo");
		else if(transaction.equals("alertEventTopic"))
			NeoJMSPublisher.getInstance().sendAlertEventMessage("teste jms Neomind", "testeNeo");
		else if(transaction.equals("alertEventVideoTopic"))
			NeoJMSPublisher.getInstance().sendAlertEventVideoMessage("teste jms Neomind", "testeNeo");

		
		System.out.println("transaction testada: " + transaction);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
