package com.neomind.fusion.custom.orsegups.ti;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;

@WebServlet(name = "ProdutividadeNacServlet", urlPatterns = { "/servlet/com.neomind.fusion.custom.orsegups.ti.ProdutividadeNacServlet" })
public class ProdutividadeNacServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	public ProdutividadeNacServlet()
	{
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		String action = request.getParameter("action");

		if (action.equalsIgnoreCase("listaUsuarios"))
		{
			String usuario = request.getParameter("usuario");
			String fluxo = request.getParameter("fluxo");
			String dataInicio = request.getParameter("dataInicio");
			String dataFim = request.getParameter("dataFim");
			ArrayList<TarefasVO> listaUsuarios = getListaUsuarios(usuario, fluxo, dataInicio, dataFim);
			String json = new Gson().toJson(listaUsuarios);

			response.setContentType("application/json");
			PrintWriter out = response.getWriter();
			out.print(json);
			System.out.println(json);
			out.flush();

		}
		
		if (action.equalsIgnoreCase("getListaUsuariosFusion"))
		{
			ArrayList<String> listaUsuarios = this.getListaUsuariosFusion();
			String json = new Gson().toJson(listaUsuarios);

			response.setContentType("application/json");
			PrintWriter out = response.getWriter();
			out.print(json);
			out.flush();

		}

	}

	private ArrayList<TarefasVO> getListaUsuarios(String usuario, String fluxo, String dataInicio, String dataFim)
	{

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
	

		ArrayList<TarefasVO> usuarios = new ArrayList<TarefasVO>();

		java.sql.Date dateInicio = null;
		java.sql.Date dateFim = null;
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try
		{
			Date datIni = formatter.parse(dataInicio);
			dateInicio = new java.sql.Date(datIni.getTime());
			Date datFim = formatter.parse(dataFim);
			dateFim = new java.sql.Date(datFim.getTime());
		}
		catch (Exception e1)
		{
			e1.printStackTrace();
		}
		
		try
		{

			conn = PersistEngine.getConnection("");
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT u.fullName ,pm.name as pm ,am.name as am,wf.code ");
			sql.append("FROM activity a WITH (NOLOCK) ");
			sql.append("INNER JOIN activitymodel am WITH (NOLOCK) ON am.neoid = a.model_neoid ");
			sql.append("INNER JOIN wfProcess wf WITH (NOLOCK) ON (wf.neoId= a.process_neoid) ");
			sql.append("INNER JOIN processmodel pm WITH (NOLOCK) on pm.neoid = wf.model_neoid ");
			sql.append("INNER JOIN task t WITH (NOLOCK) ON t.activity_neoid = a.neoid ");
			sql.append("INNER JOIN neouser u WITH (NOLOCK) ON u.neoId = t.user_neoId ");
			sql.append("WHERE t.finishDate BETWEEN ? AND ? and ");
			sql.append("u.fullName in (?) ");
			sql.append("AND am.name = 'Realizar tarefa' ");
			sql.append("AND wf.processState <> 2 ");
			sql.append("AND wf.finishDate BETWEEN ? AND ? ");
			sql.append("AND t.situation in (0,8) ");
			sql.append("AND pm.name like ? ");
			sql.append("AND t.finishByUser_neoId is not null ");
			sql.append("GROUP BY u.fullName,pm.name,am.name,wf.code ");

			pstm = conn.prepareStatement(sql.toString());
			pstm.setDate(1, dateInicio);
			pstm.setDate(2, dateFim);
			pstm.setString(3, usuario);
			pstm.setDate(4, dateInicio);
			pstm.setDate(5, dateFim);
			pstm.setString(6, fluxo + "%");
			

			rs = pstm.executeQuery();

			while (rs.next())
			{
				TarefasVO  vo = new TarefasVO();
				vo.setNomeCompleto(rs.getString("fullName"));
				vo.setProcesso(rs.getString("pm"));
				vo.setAtividade(rs.getString("am"));
				vo.setCodigoTarefa(rs.getLong("code"));
				usuarios.add(vo);
			}

		}
		catch (Exception e)
		{

		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

		return usuarios;
	}
	
	private ArrayList<String> getListaUsuariosFusion()
	{

		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		ArrayList<String> usuarios = new ArrayList<String>();

		try
		{

			conn = PersistEngine.getConnection("");

			String sql = "SELECT S.name FROM NeoUser U WITH(NOLOCK) " + " INNER JOIN SecurityEntity S WITH(NOLOCK)ON S.neoId = U.neoId " + " WHERE  S.active=1";

			pstm = conn.prepareStatement(sql);

			rs = pstm.executeQuery();

			while (rs.next())
			{
				usuarios.add(rs.getString("name"));
			}

		}
		catch (Exception e)
		{

		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}

		return usuarios;
	}

}
