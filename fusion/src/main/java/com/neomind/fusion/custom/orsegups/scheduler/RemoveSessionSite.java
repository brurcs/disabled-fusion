package com.neomind.fusion.custom.orsegups.scheduler;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javassist.expr.NewArray;

import javax.persistence.Persistence;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class RemoveSessionSite implements CustomJobAdapter
{

	private static final Log log = LogFactory.getLog(RemoveSessionSite.class);

	@Override
	public void execute(CustomJobContext arg0)
	{
		final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.RemoveSessionSite");
		log.warn("##### INICIAR REMOVER SESSÃO: Remover sessões com data fim menor que : " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		Long key = GregorianCalendar.getInstance().getTimeInMillis(); 
		
		try
		{
			String ooo = "Gerência CEREC";
			String xxx = OrsegupsUtils.getUserNeoGroup(ooo);
			System.out.println(xxx);
			String uuu = "EXECUTOROSRSL";
			String yyy = OrsegupsUtils.getUserNeoPaper(uuu);
			System.out.println(yyy);
			QLOpFilter datefilterfim = new QLOpFilter("time", "<", new GregorianCalendar());

			QLGroupFilter filtroMnc = new QLGroupFilter("AND");
			filtroMnc.addFilter(datefilterfim);
			List<NeoObject> sessions = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("UsuarioWebServiceSession"), filtroMnc);

			GregorianCalendar calendar = new GregorianCalendar();
			calendar.add(Calendar.DATE, -3);
			System.out.println(NeoUtils.safeDateFormat(calendar, "dd/MM/yyyy"));

			if (sessions != null && !sessions.isEmpty())
			{
				PersistEngine.removeObjects(sessions);
			}
			log.warn("##### EXECUTAR REMOVER SESSÃO: Remover sessões com data fim menor que : " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		}
		catch (Exception e)
		{
			log.warn("##### ERRO REMOVER SESSÃO: Remover sessões com data fim menor que : " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
		}
		finally
		{
			log.warn("##### FINALIZAR REMOVER SESSÃO: Remover sessões com data fim menor que : " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		}
	}
}
