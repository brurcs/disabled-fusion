package com.neomind.fusion.custom.orsegups.utils;


import java.math.BigDecimal;
import java.util.ArrayList;

import com.neomind.util.NeoUtils;

public class NumberWordsOrsegups
{
	
	
	public static void main(String args[]){
		NumberWordsOrsegups nw = NumberWordsOrsegups.getInstance();
		
		NumberWordsVarsOrsegups nwvar = new NumberWordsVarsOrsegups();
		nwvar.setMoney(true);
		nwvar.setFloatInDouble(0d);
		nwvar.setFloatInInt(0);
		nwvar.setFloatInString("0");
		
		//System.out.println(nw.convertImpl("1100200.25"));
		System.out.println(nw.convert(1100200.01,nwvar));
	}
	
	private static NumberWordsOrsegups instance;

	private String Qualificadores[][] = { { "centavo", "centavos" }, { "", "" }, { "mil", "mil" },
			{ "milhão", "milhões" }, { "bilhão", "bilhões" }, { "trilhão", "trilhões" },
			{ "quatrilhão", "quatrilhões" }, { "quintilhão", "quintilhões" },
			{ "sextilhão", "sextilhões" }, { "septilhão", "septilhões" } };

	private String Numeros[][] = {
			{ "zero", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove", "dez",
					"onze", "doze", "treze", "quatorze", "quinze", "desesseis", "dezessete", "dezoito",
					"dezenove" },
			{ "vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa" },
			{ "cem", "cento", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos",
					"setecentos", "oitocentos", "novecentos" } };

	private String Unidades[][] = { { "décimo", "décimos" }, { "inteiro", "inteiros" },
			{ "centésimo", "centésimos" }, { "milésimo", "milésimos" } };

	public static NumberWordsOrsegups getInstance()
	{
		if (instance == null)
			instance = new NumberWordsOrsegups();
		return instance;
	}

	private NumberWordsOrsegups()
	{

	}
	
	
	public static String convertMoeda(BigDecimal valor){
		NumberWordsOrsegups nw = NumberWordsOrsegups.getInstance();
		
		NumberWordsVarsOrsegups nwvar = new NumberWordsVarsOrsegups();
		nwvar.setMoney(true);
		nwvar.setFloatInDouble(0d);
		nwvar.setFloatInInt(0);
		nwvar.setFloatInString("0");
		
		return nw.convert(valor.doubleValue(),nwvar);
	}

	public static String convert(final String dec)
	{
		return NumberWordsOrsegups.getInstance().convertImpl(dec);
	}

	public void setNumber(double dec, ArrayList nro, NumberWordsVarsOrsegups vars)
	{
		setNumber(new BigDecimal(dec), nro, vars);
	}

	public void setNumber(BigDecimal dec, ArrayList nro, NumberWordsVarsOrsegups vars)
	{
		// fabio: Verifica se o número é negativo
		if (dec.floatValue() < 0)
		{
			dec = dec.multiply(new BigDecimal(-1));
			vars.setIsNeg(true);
		}

		// Converte para inteiro arredondando os centavos
		BigDecimal num = dec.setScale(2, BigDecimal.ROUND_HALF_UP).multiply(BigDecimal.valueOf(100));

		// Adiciona valores
		nro.clear();
		if (num.doubleValue() == 0)
		{
			// Centavos
			nro.add(new Integer(0));
			// Valor
			nro.add(new Integer(0));
		}
		else
		{
			// Adiciona centavos
			num = addRemainder(num, nro, 100);

			// Adiciona grupos de 1000
			while (num.doubleValue() != 0)
			{
				num = addRemainder(num, nro, 1000);
			}
		}
	}

	/**
	 * Trata o parametro(Número) recebido, removendo espaços, máscara("R$") e substituindo "." por
	 * vazio.
	 * 
	 * @param numberAsString
	 * @return numberAsString
	 */
	public String paramTreatment(String numberAsString, NumberWordsVarsOrsegups vars)
	{
		try
		{
			if (numberAsString.contains("R$"))
			{
				numberAsString = numberAsString.replace("R$", "");
				vars.setMoney(true);
			}

			//Fixo para valor monetário, rever para outras situações.
			if (numberAsString.indexOf(" ") != -1)
				//numberAsString = numberAsString.split(" ")[1];
				numberAsString = numberAsString.replace(" ", "");

			//Formatação de valores decimais
			if (numberAsString.indexOf(",") != -1 || numberAsString.indexOf(".") != -1)
				numberAsString = numberAsString.replace(".", "").replace(",", ".");

		}
		catch (Exception e)
		{
			// Número informado não confere com o pattern em getValidationMask() ou a String está Vazia.
		}
		return numberAsString;
	}

	/**
	 * Recebe um número como String e alimenta variáveis necessárias para o trabalho da classe. <br>
	 * O Método é usado pelos contrutores e as váriáveis para a lógica.
	 * 
	 * @param numberAsString
	 */
	public void numberToTypes(String numberAsString, NumberWordsVarsOrsegups vars)
	{
		String floatInString;
		int floatInInt;
		int intNumPart;
		double floatInDouble;
		String intNumPartInString;

		if (numberAsString.indexOf(".") != -1 && numberAsString.indexOf(",") == -1)
		{
			floatInString = "0"
					+ numberAsString.substring(numberAsString.indexOf("."), numberAsString.length());
			floatInInt = Integer.parseInt(floatInString.substring(floatInString.indexOf(".") + 1,
					floatInString.length()));
			intNumPart = Integer.parseInt(numberAsString.substring(0, numberAsString.indexOf(".")));
			floatInDouble = Double.parseDouble(floatInString);
		}
		else
		{
			floatInString = numberAsString;
			floatInInt = Integer.parseInt(numberAsString);
			intNumPart = floatInInt;
			floatInDouble = 0.0;
		}
		intNumPartInString = String.valueOf(intNumPart);

		vars.setFloatInString(floatInString);
		vars.setFloatInInt(floatInInt);
		vars.setIntNumPart(intNumPart);
		vars.setFloatInDouble(floatInDouble);
		vars.setIntNumPartInString(intNumPartInString);
	}

	/**
	 * Método link para contrutor <br>
	 * Cria uma instância para a conversão usando diretamente uma parâmetro(Número) em double.
	 * 
	 * @param dec Double
	 * @return String
	 */
	public String convert(double dec, NumberWordsVarsOrsegups vars)
	{
		ArrayList nro = new ArrayList();

		setNumber(dec, nro, vars);

		return toString(vars, nro);
	}

	/**
	 * Método link com o construtor <br>
	 * Inicia o processo de conversão usando diretamente uma parâmetro(Número) em String.
	 * 
	 * @param dec String
	 * @return String
	 */
	public String convertImpl(String dec)
	{
		NumberWordsVarsOrsegups vars = new NumberWordsVarsOrsegups();

		if (NeoUtils.safeIsNull(dec))
			return "";

		String newNumberExt = null;
		try
		{
			dec = paramTreatment(dec, vars);
			numberToTypes(dec, vars);

			newNumberExt = checkIsMoney(Double.parseDouble(dec), vars.isMoney(), vars);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}

		if (newNumberExt == null)
			newNumberExt = "";

		return newNumberExt;
	}

	/**
	 * Verifica se é dinheiro ou não e remove o que for necessário. <br>
	 * Retorna o número em extenso já tratado pelo replaceUnnecessary(numero).
	 * 
	 * @param number
	 * @param isMoney
	 * @return String
	 */
	public String checkIsMoney(double number, boolean isMoney, NumberWordsVarsOrsegups vars)
	{
		vars.setMoney(isMoney);
		String numExt = convert(number, vars).toString();

		return replaceUnnecessary(numExt, vars);

	}

	/**
	 * Método usado para retirar da String o que não for necessário <br>
	 * Usando para a definição as condições de plural ou singular
	 * e se é dinheiro ou não.
	 * 
	 * @param number
	 * @return numberReplaced
	 */
	private String replaceUnnecessary(String numExt, NumberWordsVarsOrsegups vars)
	{
		String numberReplaced = numExt;
		int intNumPart = vars.getIntNumPart();
		try
		{
			if (!vars.isMoney())
			{
				if (intNumPart == 1)
				{
					numberReplaced = numberReplaced.replace("real", "");
					numberReplaced = numberReplaced.replace("centavos", "");
					numberReplaced = numberReplaced.replace("centavo", "");
				}
				else
				{
					// TODO Sintax Sugar needed here
					if (intNumPart >= 1000 && intNumPart <= 1999)
						numberReplaced = numberReplaced.replace("Um ", "");

					if (replaceEforWhiteSpace(vars))
						numberReplaced = replaceEforWhiteSpace(numberReplaced, vars);

					numberReplaced = numberReplaced.replace("de ", "");
					numberReplaced = numberReplaced.replace("centavos", "");
					numberReplaced = numberReplaced.replace("centavo", "");
					numberReplaced = numberReplaced.replaceFirst("reais", "");

					if (intNumPart > 1000000)
						numberReplaced = insertCommas(numberReplaced);
				}
			}
			else
			{
				// TODO Sintax Sugar needed here
				if (intNumPart >= 1000 && intNumPart <= 1999)
					numberReplaced = numberReplaced.replace("Um ", "");

				if (replaceEforWhiteSpace(vars))
					numberReplaced = replaceEforWhiteSpace(numberReplaced, vars);

				numberReplaced = numberReplaced.replace("inteiros", "");
				numberReplaced = numberReplaced.replace("inteiro", "");
				numberReplaced = numberReplaced.replace("décimos", "");
				numberReplaced = numberReplaced.replace("décimo", "");
				numberReplaced = numberReplaced.replace("centésimos", "");
				numberReplaced = numberReplaced.replace("centésimo", "");

				if (intNumPart > 1000000)
					numberReplaced = insertCommas(numberReplaced);
			}

			return "(" + (upFirstChar(numberReplaced)).trim() + ")";
		}
		catch (StringIndexOutOfBoundsException e)
		{
			//
			return "";
		}
	}

	/**
	 * Verifica se a primeira* letra ' e' deve ser substituida por ' '.
	 * 
	 * @Exemplo Antes: 1801(Mil e oitocentos e um), Depois: 1801(Mil oitocentos e um)
	 * @return boolean
	 */
	private boolean replaceEforWhiteSpace(NumberWordsVarsOrsegups vars)
	{
		// Estas validações correspondem às regras gramaticais do Portugues Brasil,
		// porém não abrangem a separação por virgulas quando se trata de milhares ou bilhão.
		// o método insertCommas() ajuda a complementar as regras gramaticais.

		// TODO Sintax Sugar needed here
		// Expressão regular aqui seria uma boa.
		String intNumPartInString = vars.getIntNumPartInString();
		int numLength = intNumPartInString.length();
		int intNumPart = vars.getIntNumPart();

		if (((numLength >= 4 && numLength <= 7)
				&& !intNumPartInString.endsWith("00")
				&& ((numLength - intNumPartInString.lastIndexOf("0")) != 3)
				&& ((numLength == 4 && !String.valueOf(intNumPartInString.charAt(1)).contains("0"))
						|| (numLength == 5 && !String.valueOf(intNumPartInString.charAt(2))
								.contains("0"))
						|| (numLength == 6 && !String.valueOf(intNumPartInString.charAt(3))
								.contains("0")) || (numLength == 7 && !String.valueOf(
						intNumPartInString.charAt(4)).contains("0"))) && !intNumPartInString
				.endsWith("000"))

				|| (intNumPartInString.length() == 6
						&& ((intNumPartInString.length() - intNumPartInString.lastIndexOf("0")) != 3)
						&& (!String.valueOf(intNumPartInString.charAt(3)).contains("0"))
						&& intNumPart < 100999 && intNumPart > 100020) //)

				|| (intNumPartInString.length() == 10))
		{
			return true;
		}
		else
			return false;
	}

	/**
	 * Faz as válidações para remover o ' e' corretamente <br>
	 * de acordo com a gramática do português do Brasil.
	 * 
	 * @param (String)numberToFix
	 * @return (String)numberReplaced
	 */
	private String replaceEforWhiteSpace(String numberToFix, NumberWordsVarsOrsegups vars)
	{
		int intNumPart = vars.getIntNumPart();
		String intNumPartInString = vars.getIntNumPartInString();
		String numberReplaced = numberToFix;

		// Validações garantem que o ' e' correto seja substituido por ' ';
		numberReplaced = numberReplaced.replaceFirst(" e", "###");
		if (intNumPart > 21000)
		{
			if (numberReplaced.indexOf(" e") != -1)
			{
				if (intNumPart > 100999)
				{
					numberReplaced = numberReplaced.replaceFirst(" e", "####");

					if (intNumPart > 1000000)
					{
						if (numberReplaced.indexOf(" e") != -1)
						{
							numberReplaced = numberReplaced.replaceFirst(" e", "#####");
							if (numberReplaced.lastIndexOf("mil") < numberReplaced.indexOf(" e"))
							{
								numberReplaced = numberReplaced.replaceFirst("#####", " ");
							}
							else if (numberReplaced.indexOf("reais inteiros") > numberReplaced
									.indexOf(" e"))
							{
								numberReplaced = numberReplaced.replaceFirst(" e", " ");
							}
						}
					}
					else if (intNumPartInString.length() == 6)
					{
						numberReplaced = numberReplaced.replaceFirst(" e", " ");
					}
					else
					{
						numberReplaced = numberReplaced.replaceFirst("####", " ");
					}
				}
				else if (intNumPartInString.length() == 6
						&& (intNumPart < 100101 || intNumPartInString.endsWith("0") || intNumPart > 100100))
				{
					numberReplaced = numberReplaced.replaceFirst("###", " ");
				}
				else
				{
					numberReplaced = numberReplaced.replaceFirst(" e", " ");
				}
			}

			numberReplaced = numberReplaced.replaceFirst("#####", " e");
			numberReplaced = numberReplaced.replaceFirst("####", " e");
			numberReplaced = numberReplaced.replaceFirst("###", " e");
		}

		return numberReplaced = numberReplaced.replaceFirst("###", " ");
	}

	/**
	 * Método para inserir uma vírgula em casos de números com milhares
	 * 
	 * @param numberToInsert
	 * @return
	 */
	private String insertCommas(String numberToInsert)
	{
		if (numberToInsert.contains("milhões e"))
			numberToInsert = numberToInsert.replace("milhões e", "milhões,");
		else if (numberToInsert.contains("milhão e"))
			numberToInsert = numberToInsert.replace("milhão e", "milhão,");

		return numberToInsert;
	}

	/**
	 * Description of the Method
	 * 
	 * @return Description of the Returned Value
	 */
	public String toString(NumberWordsVarsOrsegups vars, ArrayList nro)
	{
		StringBuffer buf = new StringBuffer();

		int numero = ((Integer) nro.get(0)).intValue();
		int ct;

		for (ct = nro.size() - 1; ct > 0; ct--)
		{
			// Se ja existe texto e o atual não é zero
			if (buf.length() > 0 && !ehGrupoZero(nro, ct))
			{
				buf.append(" e ");
			}
			buf.append(numToString(((Integer) nro.get(ct)).intValue(), ct, vars));
		}
		if (buf.length() > 0)
		{
			if (isSingleGroup(nro))
				buf.append(" de ");

			// Remove espaços no final do Buffer de String
			while (buf.toString().endsWith(" "))
				buf.setLength(buf.length() - 1);

			// Se número é == 1
			if (nro.size() == 2 && ((Integer) nro.get(1)).intValue() == 1)
				buf.append(" real");
			else
			{
				buf.append(" reais");
			}
			if (((Integer) nro.get(0)).intValue() != 0)
			{
				// Adiciona 'Inteiros' ao StringBuffer se houver parte decimal.
				buf.append(" "+Unidades[1][1]);
				// Adiciona ' e ' para concatenação com centavos/decimais.
				buf.append(" e ");
			}
		}
		if (((Integer) nro.get(0)).intValue() != 0)
		{
			buf.append(numToString(((Integer) nro.get(0)).intValue(), 0, vars));
		}

		// Este upFirstChar atualmente é necessário para validações no replaceUnnecessary();
		upFirstChar(buf);

		// fabio: número negativo
		if (vars.getIsNeg() == true)
			buf.append(" negativo(s)");
		return buf.toString();
	}

	private void upFirstChar(StringBuffer buf)
	{
		if (!buf.toString().isEmpty())
		{
			String firstChar = String.valueOf(buf.charAt(0));
			buf.replace(0, 1, firstChar.toUpperCase());
		}
	}

	/**
	 * Método para mudar a primeira letra do número(em String) para caixa alta.
	 * 
	 * @param numberAsString
	 * @return newNumberAsString
	 */
	private String upFirstChar(String numberAsString)
	{
		if (numberAsString != "")
		{
			String newNumberAsString = (String.valueOf((numberAsString.charAt(0))).toUpperCase())
					+ numberAsString.substring(1, numberAsString.length());
			return newNumberAsString;
		}
		else
			return "";

	}

	/**
	 * Adds a feature to the Remainder attribute of the JExtenso object
	 * 
	 * @param divisor
	 *            The feature to be added to the Remainder attribute
	 */
	private BigDecimal addRemainder(BigDecimal num, ArrayList nro, int divisor)
	{
		// Encontra newNum[0] = num modulo divisor, newNum[1] = num dividido
		// divisor
		BigDecimal[] newNum = num.divideAndRemainder(BigDecimal.valueOf(divisor));
		// Adiciona modulo
		nro.add(new Integer(newNum[1].intValue()));
		// Altera numero
		return newNum[0];
	}

	/**
	 * Description of the Method
	 * 
	 * @param ps
	 *            Description of Parameter
	 * @return Description of the Returned Value
	 */
	private boolean haveMoreGroups(int ps, ArrayList nro)
	{
		for (; ps > 0; ps--)
		{
			if (((Integer) nro.get(ps)).intValue() != 0)
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Description of the Method
	 * 
	 * @param ps
	 *            Description of Parameter
	 * @return Description of the Returned Value
	 */
	/*
	 * private boolean isLastGroup(ArrayList nro, int ps)
	 * {
	 * return (ps > 0) && ((Integer) nro.get(ps)).intValue() != 0 && !haveMoreGroups(ps - 1, nro);
	 * }
	 */

	/**
	 * Description of the Method
	 * 
	 * @return Description of the Returned Value
	 */
	private boolean isSingleGroup(ArrayList nro)
	{
		if (nro.size() <= 3)
			return false;
		if (!ehGrupoZero(nro, 1) && !ehGrupoZero(nro, 2))
			return false;
		boolean hasOne = false;
		for (int i = 3; i < nro.size(); i++)
		{
			if (((Integer) nro.get(i)).intValue() != 0)
			{
				if (hasOne)
					return false;
				hasOne = true;
			}
		}
		return true;
	}

	boolean ehGrupoZero(ArrayList nro, int ps)
	{
		if (ps <= 0 || ps >= nro.size())
			return true;
		return ((Integer) nro.get(ps)).intValue() == 0;
	}

	/**
	 * Description of the Method
	 * 
	 * @param numero
	 *            Description of Parameter
	 * @param escala
	 *            Description of Parameter
	 * @return Description of the Returned Value
	 */
	public String numToString(int numero, int escala, NumberWordsVarsOrsegups vars)
	{
		Boolean nIsMoney = vars.isMoney();
		int floatInInt = vars.getFloatInInt();

		int unidade = (numero % 10);
		int dezena = (numero % 100); // * nao pode dividir por 10 pois verifica de 0..19
		int centena = (numero / 100);
		StringBuffer buf = new StringBuffer();

		if (numero != 0)
		{
			if (centena != 0)
			{
				if (dezena == 0 && centena == 1)
				{
					buf.append(Numeros[2][0]);
				}
				else
				{
					buf.append(Numeros[2][centena]);
				}
			}

			if ((buf.length() > 0) && (dezena != 0))
			{
				buf.append(" e ");
			}

			if (dezena > 19)
			{
				dezena /= 10;
				if (!nIsMoney)
				{
					if (floatInInt != numero && escala == 0)
						buf.append(Numeros[0][dezena - 0]);
					else
						buf.append(Numeros[1][dezena - 2]);
				}
				else
					buf.append(Numeros[1][dezena - 2]);
				if (unidade != 0)
				{
					buf.append(" e ");
					buf.append(Numeros[0][unidade]);
				}
			}
			else if (centena == 0 || dezena != 0)
			{
				if (escala == 0 && dezena == 10 && floatInInt == 1 && !nIsMoney)
					buf.append(Numeros[0][dezena - 9]);
				else
					buf.append(Numeros[0][dezena]);
			}

			buf.append(" ");
			if (numero == 1)
			{
				buf.append(Qualificadores[escala][0]);
			}
			else
			{
				buf.append(Qualificadores[escala][1]);
			}

			// Adiciona Qualificador referente as casas decimais existentes StringBuffer
			if (escala == 0)
				qualificaDecimais(buf, vars, nIsMoney);
		}
		return buf.toString();
	}

	private void qualificaDecimais(StringBuffer buf, NumberWordsVarsOrsegups vars, Boolean nIsMoney)
	{
		String floatInString = vars.getFloatInString();
		int floatInInt = vars.getFloatInInt();

		// Número de casas decimais que o número contém.
		int floatLength = floatInString.length();
		if (floatLength == 3)
			if (floatInInt == 1 && !nIsMoney)
				buf.append(Unidades[0][0]);
			else
				buf.append(Unidades[0][1]);

		else if (floatLength == 4)
			if (floatInInt == 1 && !nIsMoney)
				buf.append(Unidades[2][0]);
			else
				buf.append(Unidades[2][1]);

		else if (floatLength == 5)
			if (floatInInt == 1 && !nIsMoney)
				buf.append(Unidades[3][0]);
			else
				buf.append(Unidades[3][1]);
	}
}


