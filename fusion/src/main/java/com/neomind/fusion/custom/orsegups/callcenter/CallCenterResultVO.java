package com.neomind.fusion.custom.orsegups.callcenter;

import java.util.Collection;

/**
 * @author neomind
 * 
 *         Classe criada para encapsular as informações que irão para o cliente.
 * 
 */
public class CallCenterResultVO
{
	private Collection<SigmaVO> contas;
	private Collection<CallCenterVO> contratos;
	private String externalNumber;
	private SearchObjectVO search;

	public Collection<SigmaVO> getContas()
	{
		return contas;
	}

	public void setContas(Collection<SigmaVO> contas)
	{
		this.contas = contas;
	}

	public Collection<CallCenterVO> getContratos()
	{
		return contratos;
	}

	public void setContratos(Collection<CallCenterVO> contratos)
	{
		this.contratos = contratos;
	}

	public String getExternalNumber()
	{
		return externalNumber;
	}

	public void setExternalNumber(String externalNumber)
	{
		this.externalNumber = externalNumber;
	}

	public SearchObjectVO getSearch()
	{
		return search;
	}

	public void setSearch(SearchObjectVO search)
	{
		this.search = search;
	}
}
