package com.neomind.fusion.custom.orsegups.contract.vo;

public class ContaSigmaVo
{

	String cdCliente;
	String idEmpresa;
	String idCentral;
	String particao;
	Boolean fgAtivo;
	
	public ContaSigmaVo(){
		
	}
		
	public ContaSigmaVo(String cdCliente, String idEmpresa, String idCentral, String particao, Boolean fgAtivo)
	{
		super();
		this.cdCliente = cdCliente;
		this.idEmpresa = idEmpresa;
		this.idCentral = idCentral;
		this.particao = particao;
		this.fgAtivo = fgAtivo;
	}
	
	public String getCdCliente()
	{
		return cdCliente;
	}
	public void setCdCliente(String cdCliente)
	{
		this.cdCliente = cdCliente;
	}
	public String getIdEmpresa()
	{
		return idEmpresa;
	}
	public void setIdEmpresa(String idEmpresa)
	{
		this.idEmpresa = idEmpresa;
	}
	public String getIdCentral()
	{
		return idCentral;
	}
	public void setIdCentral(String idCentral)
	{
		this.idCentral = idCentral;
	}
	public String getParticao()
	{
		return particao;
	}
	public void setParticao(String particao)
	{
		this.particao = particao;
	}
	public Boolean getFgAtivo()
	{
		return fgAtivo;
	}
	public void setFgAtivo(Boolean fgAtivo)
	{
		this.fgAtivo = fgAtivo;
	}
	@Override
	public String toString()
	{
		return "ContaSigmaVo [cdCliente=" + cdCliente + ", idEmpresa=" + idEmpresa + ", idCentral=" + idCentral + ", particao=" + particao + ", fgAtivo=" + fgAtivo + "]";
	}
	
	
	
	
	

}
