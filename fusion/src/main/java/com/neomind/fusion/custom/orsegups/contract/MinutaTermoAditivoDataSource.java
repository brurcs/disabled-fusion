package com.neomind.fusion.custom.orsegups.contract;

import java.util.ArrayList;
import java.util.Collection;

public class MinutaTermoAditivoDataSource
{
	private Collection<TermoAditivoPostosDataSource> listaAditivosPosto = new ArrayList<TermoAditivoPostosDataSource>();

	public Collection<TermoAditivoPostosDataSource> getListaAditivosPosto()
	{
		return listaAditivosPosto;
	}

	public void setListaAditivosPosto(Collection<TermoAditivoPostosDataSource> listaAditivosPosto)
	{
		this.listaAditivosPosto = listaAditivosPosto;
	}
}
