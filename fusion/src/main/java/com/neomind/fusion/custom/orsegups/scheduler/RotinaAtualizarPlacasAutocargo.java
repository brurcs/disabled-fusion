package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class RotinaAtualizarPlacasAutocargo implements CustomJobAdapter{
    
    private static final Log log = LogFactory.getLog(RotinaAtualizarPlacasAutocargo.class);	

    @Override
    public void execute(CustomJobContext arg0){
	List<NeoObject> listaViaturas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("OTSViatura"));
	if (listaViaturas != null){
	    for (NeoObject neoObject : listaViaturas) {
		EntityWrapper viatura = new EntityWrapper(neoObject);
		if (viatura.getValue("ativaNaFrota") != null && (Boolean)viatura.getValue("ativaNaFrota")){
		    Boolean ativo = (Boolean) viatura.findField("ativaNaFrota").getValue();
		    String json = "{\"registration_plate\": \""+viatura.findField("placa").getValue()+"\"}";
		    String token1 = "115fe7a39c6d1910308c335ba3f845a0f9d6495ee912068b9cf7a90f717f03a5";
		    if (ativo){
			habilitar(json, token1);
		    }else{
			desabilitar(json, token1);
		    }
		}
	    }
	}
    }
    
    public void habilitar(String json, String token){
	try {
	    
	    HttpURLConnection request = (HttpURLConnection) new URL("https://www2.autocargo.com.br/api/integration/v1/vehicles_integration").openConnection();
	    try {
		request.setRequestMethod("POST");
		request.setReadTimeout(60 * 1000);
		request.setConnectTimeout(60 * 1000);
		request.setRequestProperty("Authorization", "Token token="+token);
		 
		request.setDoOutput(true);
		
		DataOutputStream wr = new DataOutputStream(request.getOutputStream());
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(wr));
		writer.write(json);
		writer.close();
		wr.flush();
		wr.close();

			
		int code = request.getResponseCode();
		String msg = request.getResponseMessage();
		
		if (code == 200){
		    log.info("##### Placa Atualizada com sucesso: " + msg + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}else{
		    log.error("##### ERRO AO atualizar placa no Autocargo: " + msg + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
		
	    } finally {
		request.disconnect();
	    }
	} catch (Exception ex) {
	    ex.printStackTrace();
	}
	
    }
    
    public void desabilitar(String json, String token){
	try {
	    
	    HttpURLConnection request = (HttpURLConnection) new URL("https://www2.autocargo.com.br/api/integration/v1/vehicles_integration").openConnection();
	    try {
		request.setRequestMethod("DELETE");
		request.setReadTimeout(60 * 1000);
		request.setConnectTimeout(60 * 1000);
		request.setRequestProperty("Authorization", "Token token="+token);
		request.setDoOutput(true);

		request.setDoOutput(true);
		
		DataOutputStream wr = new DataOutputStream(request.getOutputStream());
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(wr));
		writer.write(json);
		writer.close();
		wr.flush();
		wr.close();

			
		int code = request.getResponseCode();
		String msg = request.getResponseMessage();
		
		if (code == 200){
		    log.info("##### Placa Desabilitada com sucesso: " + msg + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}else{
		    log.error("##### ERRO AO atualizar placa no Autocargo: " + msg + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
		
	    } finally {
		request.disconnect();
	    }
	} catch (Exception ex) {
	    ex.printStackTrace();
	}
    }
}




