package com.neomind.fusion.custom.orsegups.objRatMobile;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ObjRatMobile {
    private String informativo;
    private String resultado;
    private String resultadoDoAtendimento;
    private String atendidoPor;
    private String local;
    private String dataAtendimento;
    private String horaAtendimento;
    private String evento;
    private String observacao;
    private String lnkFotoAit;
    private String lnkFotoLocal;
    private long cgcCpf;
    private Long tipRat;
    private String empRat;
    private String neoId;
    private String lnkImg;
    private String hasId;
    private String ratingToken;

    public String getLnkImg() {
	return lnkImg;
    }

    public void setLnkImg(String lnkImg) {
	this.lnkImg = lnkImg;
    }

    public String getNeoId() {
	return neoId;
    }

    public void setNeoId(String neoId) {
	this.neoId = neoId;
    }

    public String getEmpRat() {
	return empRat;
    }

    public void setEmpRat(String empRat) {
	this.empRat = empRat;
    }

    public Long getTipRat() {
	return tipRat;
    }

    public void setTipRat(Long tipRat) {
	this.tipRat = tipRat;
    }

    public long getCgcCpf() {
	return cgcCpf;
    }

    public void setCgcCpf(long cgcCpf) {
	this.cgcCpf = cgcCpf;
    }

    public String getResultadoDoAtendimento() {
	return resultadoDoAtendimento;
    }

    public void setResultadoDoAtendimento(String resultadoDoAtendimento) {
	this.resultadoDoAtendimento = resultadoDoAtendimento;
    }

    public String getInformativo() {
	return informativo;
    }

    public void setInformativo(String informativo) {
	this.informativo = informativo;
    }

    public String getResultado() {
	return resultado;
    }

    public void setResultado(String resultado) {
	this.resultado = resultado;
    }

    public String getAtendidoPor() {
	return atendidoPor;
    }

    public void setAtendidoPor(String atendidoPor) {
	this.atendidoPor = atendidoPor;
    }

    public String getLocal() {
	return local;
    }

    public void setLocal(String local) {
	this.local = local;
    }

    public String getDataAtendimento() {
	return dataAtendimento;
    }

    public void setDataAtendimento(String dataAtendimento) {
	this.dataAtendimento = dataAtendimento;
    }

    public String getHoraAtendimento() {
	return horaAtendimento;
    }

    public void setHoraAtendimento(String horaAtendimento) {
	this.horaAtendimento = horaAtendimento;
    }

    public String getEvento() {
	return evento;
    }

    public void setEvento(String evento) {
	this.evento = evento;
    }

    public String getObservacao() {
	return observacao;
    }

    public void setObservacao(String observacao) {
	this.observacao = observacao;
    }

    public String getLnkFotoAit() {
	return lnkFotoAit;
    }

    public void setLnkFotoAit(String lnkFotoAit) {
	this.lnkFotoAit = lnkFotoAit;
    }

    public String getLnkFotoLocal() {
	return lnkFotoLocal;
    }

    public void setLnkFotoLocal(String lnkFotoLocal) {
	this.lnkFotoLocal = lnkFotoLocal;
    }

    public void setHashId(String assunto) {
	this.hasId = assunto;
    }

    public void setRatingToken(String ratingToken) {
	this.ratingToken = ratingToken;
    }

    public String getRatingToken() {
	return this.ratingToken;
    }

    private String getHashIdComoMD5() {
	try {
	    MessageDigest messageDigest = MessageDigest.getInstance("MD5");
	    messageDigest.update(hasId.getBytes(), 0, hasId.length());
	    return (new BigInteger(1, messageDigest.digest()).toString(16)).toUpperCase();
	} catch (NoSuchAlgorithmException e) {
	    return null;
	}
    }

    public String toStringServico() {
    	return "informativo=" + informativo 
    			+ "&resultado=" + resultado 
    			+ "&resultadoDoAtendimento=" + resultadoDoAtendimento 
    			+ "&atendidoPor=" + atendidoPor 
    			+ "&local=" + local 
    			+ "&dataAtendimento="+ dataAtendimento 
    			+ "&horaAtendimento="+ horaAtendimento 
    			+ "&evento="+ evento 
    			+ "&observacao="+ observacao 
    			+ "&lnkFotoAit="+ lnkFotoAit 
    			+ "&lnkFotoLocal="+ lnkFotoLocal 
    			+ "&cgcCpf=" + cgcCpf 
    			+ "&tipRat=" + tipRat 
    			+ "&empRat=" + empRat 
    			+ "&neoId=" + neoId 
    			+ "&lnkImg=" + lnkImg
    			+ "&hashId=" + this.getHashIdComoMD5()
    			+ "&ratingToken=" + ratingToken;
    }
    
    @Override
    public String toString() {
    	return "ObjRatMobile [informativo=" + informativo 
    			+ ",\n resultado=" + resultado 
    			+ ",\n resultadoDoAtendimento=" + resultadoDoAtendimento 
    			+ ",\n atendidoPor=" + atendidoPor 
    			+ ",\n local=" + local 
    			+ ",\n dataAtendimento=" + dataAtendimento 
    			+ ",\n horaAtendimento=" + horaAtendimento 
    			+ ",\n evento=" + evento 
    			+ ",\n observacao=" + observacao 
    			+ ",\n lnkFotoAit=" + lnkFotoAit 
    			+ ",\n lnkFotoLocal=" + lnkFotoLocal 
    			+ ",\n cgcCpf=" + cgcCpf 
    			+ ",\n tipRat=" + tipRat 
    			+ ",\n empRat=" + empRat 
    			+ ",\n neoId=" + neoId
    			+ ",\n lnkImg=" + lnkImg
    			+ ",\n hashId=" + hasId
    			+ ",\n hashIdComoMD5=" + this.getHashIdComoMD5() + "]";
    }

}
