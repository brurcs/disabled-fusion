package com.neomind.fusion.custom.orsegups.presenca.bioPresenca.DTO;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 * 
 * @author lucas.alison
 *
 */

public class BioPresencaDTO {
    
    @NotNull
    private Long empresa;
    
    @NotNull
    private Long matricula;
    
    @NotNull
    private String acao;
    
    @NotNull
    private String telefone;
    
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date data; 
    
    @NotNull
    private String timeZoneDispositivo;

    
    public Long getEmpresa() {
        return empresa;
    }
    public void setEmpresa(Long empresa) {
        this.empresa = empresa;
    }
    public Long getMatricula() {
        return matricula;
    }
    public void setMatricula(Long matricula) {
        this.matricula = matricula;
    }
    public String getAcao() {
        return acao;
    }
    public void setAcao(String acao) {
        this.acao = acao;
    }
    public String getTelefone() {
        return telefone;
    }
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
    public Date getData() {
	return data;
    }
    public void setData(Date data) {
	this.data = data;
    }
    public String getTimeZoneDispositivo() {
	return timeZoneDispositivo;
    }
    public void setTimeZoneDispositivo(String timeZoneDispositivo) {
	this.timeZoneDispositivo = timeZoneDispositivo;
    }
    
    
    
}
