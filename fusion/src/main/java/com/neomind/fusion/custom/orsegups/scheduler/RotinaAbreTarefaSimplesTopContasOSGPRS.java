package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.util.NeoDateUtils;

public class RotinaAbreTarefaSimplesTopContasOSGPRS implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(RotinaAbreTarefaSimplesTopContasOSGPRS.class);

	@Override
	public void execute(CustomJobContext arg0)
	{

		Long key = GregorianCalendar.getInstance().getTimeInMillis();

		String reg = "";
		Connection conn = null;
		StringBuilder sqlOSFalhaGPRSExcessiva = new StringBuilder();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		log.warn("##### INICIAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Top Contas com OS de Falha de GPRS - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		try
		{
			

			sqlOSFalhaGPRSExcessiva.append("   SELECT TOP 50 O.CD_CLIENTE, SUBSTRING(r.NM_ROTA, 1, 3) AS sigla_regional, C.ID_CENTRAL, C.PARTICAO, C.FANTASIA, C.RAZAO, COUNT(1) AS QTD_OS, GETDATE()-15 as PrimeiraData,GETDATE() as UltimaData ");
			sqlOSFalhaGPRSExcessiva.append("   FROM dbORDEM O ");
			sqlOSFalhaGPRSExcessiva.append("   INNER JOIN dbCENTRAL C ON C.CD_CLIENTE = O.CD_CLIENTE ");
			sqlOSFalhaGPRSExcessiva.append("   INNER JOIN ROTA r WITH (NOLOCK) ON  r.CD_ROTA = c.ID_ROTA ");
			sqlOSFalhaGPRSExcessiva.append("   WHERE O.DATAEXECUTADA BETWEEN   GETDATE()-15 AND  GETDATE() AND O.IDOSDEFEITO = 125 AND O.FECHADO = 1 AND O.FG_VISUALIZADO_MOBILE = 1 AND O.DATAEXECUTADA is not null AND C.ID_RAMO != 10006 ");
			sqlOSFalhaGPRSExcessiva.append("   AND c.ID_CENTRAL NOT LIKE 'AAA%' AND c.ID_CENTRAL NOT LIKE 'DDDD' 	AND c.ID_CENTRAL NOT LIKE 'FFFF' AND c.ID_CENTRAL NOT LIKE 'R%' ");
			sqlOSFalhaGPRSExcessiva.append("   GROUP BY O.CD_CLIENTE, SUBSTRING(r.NM_ROTA, 1, 3), C.ID_CENTRAL, C.PARTICAO, C.FANTASIA, C.RAZAO ");
			sqlOSFalhaGPRSExcessiva.append("   HAVING COUNT(1) > 1 ORDER BY 7 DESC ");

			conn = PersistEngine.getConnection("SIGMA90");
			pstm = conn.prepareStatement(sqlOSFalhaGPRSExcessiva.toString());

			rs = pstm.executeQuery();
			String executor = "";
			
			/**
			 * Data de corte para busca de histórico de tarefas antigas
			 */

			
			while (rs.next())
			{
 				String solicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteFalhaGPRSExcessiva");
				
				String tituloAux = "Tratar Falha de GPRS em excesso - " + rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "]% ";
								
				String titulo = "Tratar Falha de GPRS em excesso - " + rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "] "+rs.getString("RAZAO");

				GregorianCalendar prazo = new GregorianCalendar();
				prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 7L);
				prazo.set(Calendar.HOUR_OF_DAY, 23);
				prazo.set(Calendar.MINUTE, 59);
				prazo.set(Calendar.SECOND, 59);

				NeoPaper papel = new NeoPaper();
				Long codReg = OrsegupsUtils.getCodigoRegional(rs.getString("sigla_regional"));

				if (codReg != null)
				{
					papel = OrsegupsUtils.getPapelTratarDeslocamentosExcesso(codReg);
				}

				if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
				{
					for (NeoUser user : papel.getUsers())
					{
						executor = user.getCode();
						break;
					}

					String descricao = OrsegupsUtils.getHistoricoTarefaSimples(tituloAux);
					descricao += " <strong>Conta :</strong> " + rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "] " + " <br>";
					descricao = descricao + " <strong>Razão Social :</strong> " + rs.getString("RAZAO") + " <br>";
					descricao = descricao + " <strong>Nome Fantasia :</strong> " + rs.getString("FANTASIA") + " <br>";
					descricao = descricao + " <strong>Período :</strong> " + NeoDateUtils.safeDateFormat(rs.getDate("PrimeiraData"), "dd/MM/yyyy") + "<strong>  à </strong>" + NeoDateUtils.safeDateFormat(rs.getDate("UltimaData"), "dd/MM/yyyy") + "<br>";
					descricao = descricao + " <strong>Quantidade de OS :</strong> " + rs.getString("QTD_OS") + "<br>";

					IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
					String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);

					log.warn("##### EXECUTAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Top Contas com OS de Falha de GPRS - Responsavel : " + executor + " Regional: " + reg + " Tarefa: " + tarefa);
					System.out.println("##### EXECUTAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Top Contas com OS de Falha de GPRS - Responsavel : " + executor + " Regional: " + reg + " Tarefa: " + tarefa);
				}
				else
				{
					log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Top Contas com OS de Falha de GPRS - Regra: Gerente responsável não encontrado - Regional " + rs.getString("sigla_regional"));
					continue;
				}

			}

		}
		catch (Exception e)
		{
			log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Top Contas com OS de Falha de GPRS - ERRO AO GERAR TAREFA - Regional: " + reg);
			System.out.println("[" + key + "] ##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Top Contas com OS de Falha de GPRS - ERRO AO GERAR TAREFA - Regional: " + reg);
			e.printStackTrace();
			throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
		}
		finally
		{

			try
			{
				OrsegupsUtils.closeConnection(conn, pstm, rs);
			}
			catch (Exception e)
			{
				log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Top Contas com OS de Falha de GPRS ou Ordens de Serviço em Excesso - ERRO DE EXECUÇÃO - Regional: " + reg);
				e.printStackTrace();
			}

			log.warn("##### FINALIZAR  AGENDADOR DE TAREFA:Abrir Tarefa Simples Top Contas com OS de Falha de GPRS e Ordens de Serviço em Excesso - Regional: " + reg + " Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}

	}
	

}
