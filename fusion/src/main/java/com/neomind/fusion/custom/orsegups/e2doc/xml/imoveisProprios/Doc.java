package com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisProprios;

public class Doc {
    private String id;
    private int tamanho;
    private String des;
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public int getTamanho() {
        return tamanho;
    }
    public void setTamanho(int tamanho) {
        this.tamanho = tamanho;
    }
    public String getDes() {
        return des;
    }
    public void setDes(String des) {
        this.des = des;
    }
}
