package com.neomind.fusion.custom.orsegups.seventh;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class SeventhDAOImpl {
	
	private static Logger logger = LoggerFactory.getLogger(SeventhDAOImpl.class);

	public String layoutslist(String ip, String porta, String usuario, String senha, String idCentral,
		HttpServletRequest httpRequest) {

		String urlStr = "http://" + ip + ":" + porta + "/layoutslist.cgi";
		logger.info("urlList: " + urlStr);
		String retorno = "";
		try {
			TreeMap<String, String> treeMapLayout;

			treeMapLayout = returnMap(urlStr, usuario, senha);

			if (treeMapLayout != null && !treeMapLayout.isEmpty() && treeMapLayout.containsKey(idCentral)) {

				InetAddress address;
				String ipRequest = "";
				address = InetAddress.getByName(httpRequest.getRemoteHost());
				if (address != null && address.getHostName() != null && !address.getHostName().isEmpty()) {
					ipRequest = address.getHostName();
					String hostName[] = ipRequest.split("\\.");
					ipRequest = hostName[0];
				} else {
					return null;
				}

				String nomeLayout = treeMapLayout.get(idCentral);
				String portaCliente = "8080";
				retorno = setlayout(ip, portaCliente, usuario, senha, nomeLayout, ipRequest);

			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return retorno;
	}

	private TreeMap<String, String> returnMap(String urlStr, String usuario, String senha) throws IOException {

		TreeMap<String, String> map = null;

		String resposta = "";
		URL url = null;
		HttpURLConnection urlc = null;
		BufferedReader in = null;

		try {
			String userpass = usuario + ":" + senha;

			url = new URL(urlStr.trim());
			urlc = (HttpURLConnection) url.openConnection();
			logger.info("returnMap: " + urlStr.trim());
			String basicAuth = "Basic " + new String(new Base64().encode(userpass.getBytes()));
			urlc.setRequestProperty("Authorization", basicAuth);
			if (urlc != null && urlc.getResponseCode() == 200) {
				in = new BufferedReader(new InputStreamReader(urlc.getInputStream()));
				String inputLine;
				while ((inputLine = in.readLine()) != null) {
					resposta = inputLine;
				}
				in.close();
				if (resposta != null && !resposta.isEmpty() && resposta.contains("=")) {
					String array[] = resposta.split("&");
					map = new TreeMap<String, String>();
					for (String valor : array) {
						if (valor != null && !valor.isEmpty() && valor.contains("=")) {
							String parametros[] = valor.split("=");
							String key[] = parametros[1].split("-");
							if (parametros.length > 1 && key.length > 1)
								map.put((String) key[1], (String) parametros[1]);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (in != null)
				in.close();
		}

		return map;
	}

	private String setlayout(String ip, String porta, String usuario, String senha, String nome, String ipRequest)
			throws IOException {

		String resposta = "";
		URL url = null;
		URLConnection urlc = null;
		BufferedReader in = null;

		try {
			String userpass = usuario + ":" + senha;

			url = new URL("http://" + ipRequest + ":"+porta+"/setlayout.cgi?layout=" + URLEncoder.encode(nome,"UTF-8"));
			logger.info("URL layout: "+ "http://" + ipRequest + ":"+porta+"/setlayout.cgi?layout=" + URLEncoder.encode(nome,"UTF-8"));

			urlc = url.openConnection();
			String basicAuth = "Basic " + new String(new Base64().encode(userpass.getBytes()));
			urlc.setRequestProperty("Authorization", basicAuth);
			in = new BufferedReader(new InputStreamReader(urlc.getInputStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				resposta = inputLine;
			}
			in.close();
			if (resposta != null && !resposta.isEmpty())
				return resposta;

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (in != null)
				in.close();
		}

		return resposta;
	}


	public String buscaImagem(String ip, String porta, String usuario, String senha, String cam) {

		InputStream is = null;
		URL url = null;
		String retorno = "";
		try {
			String userpass = usuario + ":" + senha;
			url = new URL("http://" + ip + ":" + porta + "/camera.cgi?camera=" + cam
					+ "&resolucao=640x480&qualidade=70&formato=jpg");

			URLConnection uc = url.openConnection();
			String basicAuth = "Basic " + new String(new Base64().encode(userpass.getBytes()));
			uc.setRequestProperty("Authorization", basicAuth);
			is = uc.getInputStream();
			byte[] b = IOUtils.toByteArray(is);
			byte[] encodeBase64 = Base64.encodeBase64(b);
			retorno = new String(encodeBase64, "UTF-8");

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(is);

		}
		return retorno;
	}

}
