package com.neomind.fusion.custom.orsegups.presenca.vo;

import java.util.GregorianCalendar;
import java.util.List;

public class DocumentoTreeVO
{
	private Long id;
	private String nome;
	private String tamanho;
	private String data;
	private GregorianCalendar gData;
	private String state;
	private String link;
	private String des;
	private List<DocumentoTreeVO> children;
	
	public Long getId()
	{
		return id;
	}
	public void setId(Long id)
	{
		this.id = id;
	}
	public String getNome()
	{
		return nome;
	}
	public void setNome(String nome)
	{
		this.nome = nome;
	}
	public String getTamanho()
	{
		return tamanho;
	}
	public void setTamanho(String tamanho)
	{
		this.tamanho = tamanho;
	}
	public String getData()
	{
		return data;
	}
	public void setData(String data)
	{
		this.data = data;
	}
	public GregorianCalendar getgData()
	{
		return gData;
	}
	public void setgData(GregorianCalendar gData)
	{
		this.gData = gData;
	}
	public String getState()
	{
		return state;
	}
	public void setState(String state)
	{
		this.state = state;
	}
	public String getLink()
	{
		return link;
	}
	public void setLink(String link)
	{
		this.link = link;
	}
	public List<DocumentoTreeVO> getChildren()
	{
		return children;
	}
	public void setChildren(List<DocumentoTreeVO> children)
	{
		this.children = children;
	}
	public String getDes() {
	    return des;
	}
	public void setDes(String des) {
	    this.des = des;
	}
}
