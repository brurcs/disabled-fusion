package com.neomind.fusion.custom.orsegups.contract.converter;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.EntityConverter;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.entity.EntityWrapper;

public class PostoConverter extends EntityConverter
{
	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin)
	{
		EntityWrapper wrapper = new EntityWrapper(field.getForm().getObject());

		StringBuilder outBuilder = new StringBuilder();

		List<NeoObject> listPostos = (List<NeoObject>) field.getValue();

		sortNeoId(listPostos);

		outBuilder.append("<table  class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">");
		outBuilder.append("    <tr style=\"cursor: auto\">");
		outBuilder.append("        <th class=\"min\"><input id=\"checkbox_all\" type=\"checkbox\" name=\"checkbox2\" value=\"checkbox\" title=\"Selecionar todos\" onclick=\"\"></th>");
		outBuilder.append("        <th style=\"cursor: auto\">Placa</th>");
		outBuilder.append("        <th style=\"cursor: auto; white-space: normal\">Regional</th>");
		outBuilder.append("        <th style=\"cursor: auto\">Valor do Posto</th>");
		outBuilder.append("        <th style=\"cursor: auto\">Valor Instalação</th>");
		outBuilder.append("        <th style=\"cursor: auto\">Pert. Regional</th>");
		outBuilder.append("        <th style=\"cursor: auto\">Combos</th>");
		outBuilder.append("        <th style=\"cursor: auto\">Equipamentos</th>");
		outBuilder.append("        <th style=\"cursor: auto\">Itens de checklist</th>");
		outBuilder.append("    </tr>");
		outBuilder.append("    <tbody>");

		for (NeoObject posto : listPostos)
		{
			EntityWrapper wPosto = new EntityWrapper(posto);

			String listComboHtml = "<ul>";
			List<NeoObject> listCombos = wPosto.findGenericValue("equipamentosItens.kitsPosto");
			for (NeoObject noCombo : listCombos)
			{
				EntityWrapper wCombo = new EntityWrapper(noCombo);
				String descricao = wCombo.findGenericValue("kit.usu_deskit");

				listComboHtml += "<li>" + descricao + "</li>";
			}
			listComboHtml += "</ul>";

			String listEquipamentosHtml = "<ul>";
			List<NeoObject> listEquipamentos = wPosto.findGenericValue("equipamentosItens.equipamentosPosto");
			for (NeoObject noEquipamento : listEquipamentos)
			{
				EntityWrapper wEquipamento = new EntityWrapper(noEquipamento);
				String descricao = wEquipamento.findGenericValue("produto.despro");

				listEquipamentosHtml += "<li>" + descricao + "</li>";
			}
			listEquipamentosHtml += "</ul>";

			String listItensChecklistHtml = "<ul>";
			List<NeoObject> listItensChecklist = wPosto.findGenericValue("equipamentosItens.itensChecklist");
			for (NeoObject noItemChecklist : listItensChecklist)
			{
				EntityWrapper wItemChecklist = new EntityWrapper(noItemChecklist);
				String descricao = wItemChecklist.findGenericValue("descricao");

				listItensChecklistHtml += "<li>" + descricao + "</li>";
			}
			listItensChecklistHtml += "</ul>";

			outBuilder.append("        <tr>");
			outBuilder.append("            <td colspan=\"1\" class=\"\"><input id=\"postoEditar_"+posto.getNeoId()+"_checkbox\" type=\"checkbox\" name=\"excluir\" value=\""+posto.getNeoId()+"\"></td>");
			outBuilder.append("            <td> " + wPosto.findGenericValue("placa") + " </td>");
			outBuilder.append("            <td style=\"white-space: normal\"> " + wPosto.findGenericValue("regionalPosto.usu_nomreg") + " </td>");
			outBuilder.append("            <td> " + wPosto.findGenericValue("formacaoPreco.valorMontanteB") + " </td>");
			outBuilder.append("            <td> " + wPosto.findGenericValue("vigilanciaEletronica.valorInstalacao") + " </td>");
			outBuilder.append("            <td> " + (wPosto.findGenericValue("enderecoPosto") == null ? "Sim" : "Não") + " </td>");
			outBuilder.append("            <td> " + listComboHtml + " </td>");
			outBuilder.append("            <td> " + listEquipamentosHtml + " </td>");
			outBuilder.append("            <td> " + listItensChecklistHtml + " </td>");
			outBuilder.append("        </tr>");
		}

		outBuilder.append("	    </tbody>");
		outBuilder.append("</table>");

		return outBuilder.toString();
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}

	public static void sortNeoId(Collection<? extends NeoObject> list)
	{
		Collections.sort((List<? extends NeoObject>) list, new Comparator<NeoObject>()
		{

			public int compare(NeoObject obj1, NeoObject obj2)
			{

				return ((Long) new EntityWrapper(obj1).findGenericValue("neoId") < (Long) new EntityWrapper(obj2).findGenericValue("neoId")) ? -1 : 1;
			}
		});
	}
}
