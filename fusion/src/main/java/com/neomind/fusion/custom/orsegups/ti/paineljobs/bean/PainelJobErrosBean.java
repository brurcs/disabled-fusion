package com.neomind.fusion.custom.orsegups.ti.paineljobs.bean;


public class PainelJobErrosBean {

    private Long idJob;
    private String nomeJob;
    private String detalhes;
    private String error;
    private String dataErro;
    private String sistema;
    
    public Long getIdJob() {
        return idJob;
    }
    public void setIdJob(Long idJob) {
        this.idJob = idJob;
    }
    public String getNomeJob() {
	return nomeJob;
    }
    public void setNomeJob(String nomeJob) {
	this.nomeJob = nomeJob;
    }
    public String getDetalhes() {
        return detalhes;
    }
    public void setDetalhes(String detalhes) {
        this.detalhes = detalhes;
    }
    public String getError() {
        return error;
    }
    public void setError(String error) {
        this.error = error;
    }
    public String getDataErro() {
	return dataErro;
    }
    public void setDataErro(String dataErro) {
	this.dataErro = dataErro;
    }
    public String getSistema() {
        return sistema;
    }
    public void setSistema(String sistema) {
        this.sistema = sistema;
    }
    
    
}
