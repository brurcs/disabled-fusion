package com.neomind.fusion.custom.orsegups.adapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import org.json.JSONObject;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class OCPAbreEventoSIGMAAPI implements CustomJobAdapter {

    @Override
    public void execute(CustomJobContext ctx) {
	if (OrsegupsUtils.isWorkDay(new GregorianCalendar())) {
	    System.out.println("Inicio para gerar evento XPL1");
	    processaJob(ctx); 
	}
    }

    public static void processaJob(CustomJobContext ctx) {

	String idCliente = "232281,233702,233703,247767,249848,233469,232334,233793,232805,233784,233785,233775,236644,235873,235132,234827,232839,233006,232019,232589,233866,233727,235228,234070,231965,233017,236091,236092,236093,236094,235741,233361,232810,248533,233990,235071,235996,236372,233316,232373,233347,233850,233845,255276,255281,234709,234004,233053,233232,232376,232393,235068,235466,232844,233586,231955,248603,233058,233389,233437,248606,248607,234639,233916,233930,235462,235004,233513,247756,236008,236423,231998,232004,233905,233488,235254,235711,236006,235563,234701,233179,241546,233327,233329,233331,233355,231927,232479,232480,232481,235919,233898,232169,232388,235366,235084,234864,235966,235634,236651";

	PreparedStatement st = null;
	ResultSet rs = null;
	String nomeFonteDados = "SIGMA90";
	Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
	StringBuffer sql = new StringBuffer();

	try {

	    sql.append("SELECT ID_CENTRAL, 'XPL1' as NM_EVENTO, '000' as NU_AUXILIAR, PARTICAO, 19 as NU_PORTA, 2 as NU_PROTOCOLO, GETDATE() as DT_RECEPCAO, 1 as TP_RECEPTORA, ID_EMPRESA,  'Geração Automática' as NM_RECEPTORA, 'T' as TP_RECEPCAO FROM dbCENTRAL");
	    sql.append(" WHERE CD_CLIENTE IN (" + idCliente + ")");

	    st = connection.prepareStatement(sql.toString());
	    rs = st.executeQuery();

	    while (rs.next()) {

		String conta = rs.getString("ID_CENTRAL");
		String empresa = rs.getString("ID_EMPRESA");
		String particao = rs.getString("PARTICAO");
		String auxiliar = rs.getString("NU_AUXILIAR");
		String evento = rs.getString("NM_EVENTO");

		try {

		    Evento eventos = new Evento();

		    eventos.setConta(conta);
		    eventos.setEmpresa(empresa);
		    eventos.setParticao(particao);
		    eventos.setAuxiliar(auxiliar);
		    eventos.setEvento(evento);

		    JSONObject jsonObj = new JSONObject();

		    jsonObj.put("conta", eventos.getConta());
		    jsonObj.put("empresa", eventos.getEmpresa());
		    jsonObj.put("particao", eventos.getParticao());
		    jsonObj.put("auxiliar", eventos.getAuxiliar());
		    jsonObj.put("evento", eventos.getEvento());

		    try {
			URL url = new URL("http://apps.orsegups.com.br:8070/sigma-service/default/newEvent");
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setDoOutput(true);
			con.setRequestMethod("POST");
			con.setRequestProperty("x-auth-token", "682ee2548d4a73d8fadf72f32c52df790eb72aff");
			con.setRequestProperty("Content-Type", "application/json");
			con.setDoOutput(true);

			String input = "" + jsonObj + "";

			OutputStream OS = con.getOutputStream();
			OS.write(input.getBytes());
			OS.flush();

			if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED && con.getResponseCode() != HttpURLConnection.HTTP_OK) {

			    throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
			    System.out.println(output);
			}

			con.disconnect();

		    } catch (MalformedURLException e) {

			e.printStackTrace();

		    } catch (IOException e) {

			e.printStackTrace();

		    }
		} catch (Exception e) {
		    e.printStackTrace();
		}

	    }
	} catch (SQLException e1) {

	    e1.printStackTrace();
	    throw new WorkflowException("Não foi possivel gerar evento no SIGMA.");
	} finally {
	    System.out.println("Finalizado gerar evento XPL1.");
	    OrsegupsUtils.closeConnection(connection, st, rs);
	}
    }

    public static String retornaClientes(String qtde, String cdMunicipio, String regional) {
	String idCliente = "";

	PreparedStatement st = null;
	ResultSet rs = null;
	String nomeFonteDados = "SIGMA90";
	Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
	StringBuffer sql = new StringBuffer();

	try {

	    sql.append("select case when (cclientes.clientes <> '') then left(cclientes.clientes, len(cclientes.clientes)-1) else '-1' end  as ccclientes  ");	    
	    sql.append("  FROM ");
	    sql.append("  ( ");
	    sql.append("     SELECT COALESCE((SELECT CONVERT(varchar(10), clientes.cdCliente) + ',' AS [text()] ");
	    sql.append("       FROM  ");
	    sql.append("       ( ");
	    sql.append("           select top ");
	    sql.append(qtde);
	    sql.append("                  cdCliente as cdCliente ");
	    sql.append("             from [FSOODB04\\SQL02].TIDB.dbo.ControlePlacas a ");
	    sql.append("       inner join [FSOODB03\\SQL01].SIGMA90.DBO.DBCENTRAL c on c.CD_CLIENTE = a.cdCliente ");
	    sql.append("            where a.regional = '");
	    sql.append(regional);
	    sql.append("' ");
	    sql.append("              and not exists ( ");
	    sql.append("                             select 1 "); 
	    sql.append("                               from [FSOODB03\\SQL01].SIGMA90.DBO.VIEW_HISTORICO vh ");
	    sql.append("                         inner join [CACUPE\\SQL02].Fusion_Producao.dbo.D_OperacaoChecagemPlaca b on vh.cd_historico = b.cdHistorico ");
//	    sql.append("                              where b.visitavalidada = 1 ");
	    sql.append("                              where vh.CD_CLIENTE = a.cdCliente ");
	    sql.append("                                and vh.CD_EVENTO IN ('XPL1') "); 
	    if (regional.equals("XLN")) {
		sql.append("                                and vh.DT_RECEBIDO > '2018-01-01 00:00:00.000' ");		
	    } else {
		sql.append("                                and vh.DT_RECEBIDO > '2017-08-01 00:00:00.000' ");
	    }
	    sql.append("                                and vh.DT_FECHAMENTO IS NOT NULL ");
	    sql.append("                             ) ");
	    sql.append("              and not exists ( ");
	    sql.append("                             select 1 ");
	    sql.append("                               from [FSOODB03\\SQL01].SIGMA90.DBO.VIEW_HISTORICO vh ");
	    sql.append("                              where vh.CD_CLIENTE = a.cdCliente ");
	    sql.append("                                and vh.CD_EVENTO IN ('XPL1') "); 
	    if (regional.equals("XLN")) { 
		sql.append("                                and vh.DT_RECEBIDO > '2018-01-01 00:00:00.000' ");
	    } else {
		sql.append("                                and vh.DT_RECEBIDO > '2017-08-01 00:00:00.000' ");
	    }
	    //sql.append("                                and vh.DT_FECHAMENTO IS NULL ");
	    sql.append("                              ) ");
	    
	    if (regional.equals("SRR")) { 
		sql.append("    and a.sequencia is not null ");
	    }
	    sql.append("     and c.CTRL_CENTRAL = 1 ");
//	    if (regional.equals("XLN")) {
//		sql.append("    and a.sequencia > 975 ");
//	    }
	    sql.append("                        order by coalesce(a.sequencia,id) ");
	    sql.append(") as clientes ");
	    sql.append("  FOR XML PATH(''), TYPE).value('.[1]', 'NVARCHAR(MAX)'), '') as clientes) as cclientes");
	    
	    st = connection.prepareStatement(sql.toString());
	    rs = st.executeQuery();

	    while (rs.next()) {
		idCliente = rs.getString("ccclientes");
	    }

	} catch (SQLException e1) {
	    e1.printStackTrace();
	    throw new WorkflowException("Não foi possivel gerar evento no SIGMA.");
	} finally {
	    //System.out.println("Finalizado gerar evento XPL1.");
	    OrsegupsUtils.closeConnection(connection, st, rs);
	}
	return idCliente;
    }

    public static int processaJob(String qtde,String cdMunicipio, String regional) {

	String idCliente = retornaClientes(qtde,cdMunicipio,regional);
	int count = 0;

	PreparedStatement st = null;
	ResultSet rs = null;
	String nomeFonteDados = "SIGMA90";
	Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
	StringBuffer sql = new StringBuffer();

	try {

	    sql.append("SELECT ID_CENTRAL, 'XPL1' as NM_EVENTO, '000' as NU_AUXILIAR, PARTICAO, 19 as NU_PORTA, 2 as NU_PROTOCOLO, GETDATE() as DT_RECEPCAO, 1 as TP_RECEPTORA, ID_EMPRESA,  'Geração Automática' as NM_RECEPTORA, 'T' as TP_RECEPCAO FROM [FSOODB03\\SQL01].SIGMA90.DBO.dbCENTRAL");
	    sql.append(" WHERE CD_CLIENTE IN (" + idCliente + ")");

	    st = connection.prepareStatement(sql.toString());
	    rs = st.executeQuery();

	    while (rs.next()) {
		
		String conta = rs.getString("ID_CENTRAL");
		String empresa = rs.getString("ID_EMPRESA");
		String particao = rs.getString("PARTICAO");
		String auxiliar = rs.getString("NU_AUXILIAR");
		String evento = rs.getString("NM_EVENTO");

		try {

		    Evento eventos = new Evento();

		    eventos.setConta(conta);
		    eventos.setEmpresa(empresa);
		    eventos.setParticao(particao);
		    eventos.setAuxiliar(auxiliar);
		    eventos.setEvento(evento);

		    JSONObject jsonObj = new JSONObject();

		    jsonObj.put("conta", eventos.getConta());
		    jsonObj.put("empresa", eventos.getEmpresa());
		    jsonObj.put("particao", eventos.getParticao());
		    jsonObj.put("auxiliar", eventos.getAuxiliar());
		    jsonObj.put("evento", eventos.getEvento());

		    try {
			URL url = new URL("http://apps.orsegups.com.br:8070/sigma-service/default/newEvent"); // PRODUCAO
			//URL url = new URL("http://DSOO14:8081/default/newEvent"); // TESTE
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setDoOutput(true);
			con.setRequestMethod("POST");
			con.setRequestProperty("x-auth-token", "682ee2548d4a73d8fadf72f32c52df790eb72aff");
			con.setRequestProperty("Content-Type", "application/json");
			con.setDoOutput(true);

			String input = "" + jsonObj + "";

			OutputStream OS = con.getOutputStream();
			OS.write(input.getBytes());
			OS.flush();

			if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED && con.getResponseCode() != HttpURLConnection.HTTP_OK) {

			    throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));

			String output;
			System.out.println("Output from Server .... \n");
			count++;
			while ((output = br.readLine()) != null) {
			    System.out.println(output);
			}

			con.disconnect();

		    } catch (MalformedURLException e) {

			e.printStackTrace();

		    } catch (IOException e) {

			e.printStackTrace();

		    }
		} catch (Exception e) {
		    e.printStackTrace();
		}

	    }
	    return count;
	} catch (SQLException e1) {

	    e1.printStackTrace();
	    throw new WorkflowException("Não foi possivel gerar evento no SIGMA.");
	} finally {
	    System.out.println("Finalizado gerar evento XPL1.");
	    OrsegupsUtils.closeConnection(connection, st, rs);
	}
    }
    
}