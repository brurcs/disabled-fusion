package com.neomind.fusion.custom.orsegups.converter;

import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoUser;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class RSCHistoricoAtividadeConverter extends StringConverter
{
	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin)
	{
		
		Long idPai = field.getForm().getObjectId();
		NeoObject tarefa = PersistEngine.getNeoObject(idPai);
		EntityWrapper wrapper = new EntityWrapper(tarefa);
		
		List<NeoObject> registroAtividades = (List<NeoObject>) wrapper.findValue("registroAtividades");
		StringBuilder textoTable = new StringBuilder();
		
		if (NeoUtils.safeIsNotNull(registroAtividades) && !registroAtividades.isEmpty())
		{
			textoTable.append("		<table  class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">");
			textoTable.append("			<tr style=\"cursor: auto\">");
			textoTable.append("				<th style=\"cursor: auto\">Responsável</th>");
			textoTable.append("				<th style=\"cursor: auto; white-space: normal\">Descrição</th>");
			textoTable.append("				<th style=\"cursor: auto\">Data da Ação</th>");
			textoTable.append("				<th style=\"cursor: auto\">Atividade</th>");
			textoTable.append("				<th style=\"cursor: auto\">Prazo</th>");
			textoTable.append("			</tr>");
			textoTable.append("			<tbody>");	
			
			for (NeoObject obj : registroAtividades)
			{
				EntityWrapper registroWrapper = new EntityWrapper(obj);
				NeoUser responsavel = (NeoUser) registroWrapper.findValue("responsavel");
				String descricao = (String) registroWrapper.findValue("descricao");
				GregorianCalendar dataFinal = (GregorianCalendar) registroWrapper.findValue("dataFinalRSC");
				String dataAcao = NeoDateUtils.safeDateFormat(dataFinal, "dd/MM/yyyy HH:mm:ss");
				GregorianCalendar prazo = (GregorianCalendar) registroWrapper.findValue("prazo");
				String atividade = (String) registroWrapper.findValue("atividade");				
				
				String grupo = "";
				if (responsavel != null && responsavel.getGroup() != null && responsavel.getGroup().getName() != null)
				{
					grupo = responsavel.getGroup().getName();
				}
				
				textoTable.append("		<tr>");
				textoTable.append("			<td title='" + grupo + "'>" + responsavel.getFullName() + "</td>");
				textoTable.append("			<td style=\"white-space: normal\">" + descricao + "</td>");
				textoTable.append("			<td>" + dataAcao + "</td>");
				textoTable.append("			<td>" + atividade + "</td>");
				textoTable.append("			<td>" + NeoDateUtils.safeDateFormat(prazo, "dd/MM/yyyy HH:mm:ss") + "</td>");
				textoTable.append("		</tr>");
			}
			
			textoTable.append("			</tbody>");
			textoTable.append("		</table>");
		}
		
		return textoTable.toString();
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}
}
