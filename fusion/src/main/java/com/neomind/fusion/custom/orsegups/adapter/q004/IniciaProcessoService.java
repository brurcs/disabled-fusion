package com.neomind.fusion.custom.orsegups.adapter.q004;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;

@Path("/q004")
public class IniciaProcessoService
{
	@SuppressWarnings("deprecation")
	@POST
	@Path("/iniciarProcesso")
	public Response iniciarProcesso(NaoConformidadeDTO dto)
	{
		try
		{
			String empresa = dto.getPersonCompanyName();
			String cliente = dto.getWorkplaceName();
			String resposta = dto.getAlternativeName();
			String obs = dto.getObservation();
			String questao = dto.getQuestionName();
			String codPosto = dto.getWorkplaceExternalId();

			if ("Não".equals(resposta))
			{
				Integer codRegional = null;
				String queryRegional = "select usu_codreg from r016orn where numloc = " + codPosto;

				Connection conn = PersistEngine.getConnection("VETORH");
				PreparedStatement pstmt = conn.prepareStatement(queryRegional);
				ResultSet rs = pstmt.executeQuery();

				if (rs != null && rs.next())
					codRegional = rs.getInt("usu_codreg");

				NeoUser noCoordenador = null;
				NeoPaper npCoordenador = OrsegupsUtils.getPapelCoordenadorRegional(Long.valueOf(codRegional));
				for (NeoUser noUser : npCoordenador.getAllUsers())
				{
					noCoordenador = noUser;
					break;
				}
				
				if (!noCoordenador.getCode().equalsIgnoreCase("reginaldo.silva"))
					return Response.notModified().build();

				ProcessModel pm = (ProcessModel) PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("name", "Q004 - Checklist Supervisão"));

				NeoObject noForm = AdapterUtils.createNewEntityInstance("q004ChecklistSupervisao");
				EntityWrapper wrapper = new EntityWrapper(noForm);

				wrapper.setValue("empresa", empresa);
				wrapper.setValue("cliente", cliente);
				wrapper.setValue("resposta", false);
				wrapper.setValue("respostaObservacao", obs);
				wrapper.setValue("questao", questao);
				wrapper.setValue("responsavel", noCoordenador);

				PersistEngine.persist(noForm);

				String codTarefa = OrsegupsWorkflowHelper.iniciaProcesso(pm, noForm, false, noCoordenador, true, null);

				if (codTarefa != null)
				{
					System.out.println("Q004 - Processo iniciado: " + codTarefa);
				}
			}

			return Response.ok().build();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return Response.notModified().build();
		}
	}
}
