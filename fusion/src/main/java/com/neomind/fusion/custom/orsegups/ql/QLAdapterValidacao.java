package com.neomind.fusion.custom.orsegups.ql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.presenca.vo.EscalaPostoVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.EscalaVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.HorarioVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.QLPresencaUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class QLAdapterValidacao
{

	/**
	 * Verifica se existe Centro de Custo informado
	 * 
	 * @param obj
	 *            O parametro pode ser do tipo XVETORHUSUFUNFUSION que representa a view V034FUSION
	 */
	public static void validarCentroCustoDoPosto(NeoObject obj)
	{
		EntityWrapper ewObj = new EntityWrapper(obj);
		String codCcu = "";
		if (obj.getNeoType().equals("XVETORHUSUFUNFUSION"))
		{
			codCcu = (String) ewObj.findField("codccu").getValue();
		}
		else
		{
			codCcu = (String) ewObj.findField("usu_codccu").getValue();
		}

		if (NeoUtils.safeIsNull(codCcu))
		{
			System.out.println("Posto deve possuir um centro de custo.");
			QLAdapter.erros.add("Posto deve possuir um centro de custo.");
			QLAdapter.log.error("Posto deve possuir um centro de custo.");
			QLAdapter.validation = false;
		}
	}

	/**
	 * Verifica se o posto informado está com situação ativa
	 * O parametro é um objeto
	 * 
	 * @param obj
	 */
	public static void validarPostoAtivo(NeoObject obj)
	{
		String sqlSelect = "Select R016ORN.USU_SitAti" + " From R016ORN, R016HIE " + " Where R016ORN.TabOrg =:tabOrg " + " AND R016ORN.NumLoc =:numLoc " + " AND R016HIE.TabOrg = R016ORN.TabOrg " + " AND R016HIE.NumLoc = R016ORN.NumLoc ";

		EntityWrapper ewObj = new EntityWrapper(obj);

		Long tabOrg = (Long) ewObj.findValue("taborg");
		Long numLoc = (Long) ewObj.findValue("numloc");

		Query query = QLAdapter.entityManager.createNativeQuery(sqlSelect);
		query.setParameter("tabOrg", tabOrg);
		query.setParameter("numLoc", numLoc);
		query.setMaxResults(1);

		Object situacaoPosto = query.getSingleResult();

		if (NeoUtils.safeIsNull(situacaoPosto) || situacaoPosto.toString().equals("N"))
		{
			System.out.println("Para ser utilizado o posto deve estar ativo.");
			QLAdapter.erros.add("Para ser utilizado o posto deve estar ativo.");
			QLAdapter.log.error("Para ser utilizado o posto deve estar ativo.");
			QLAdapter.validation = false;
		}
	}

	/**
	 * Verifica se exite filial informada no cadastro de posto
	 * 
	 * @param posto
	 */
	public static void validarFilialLigadaPosto(NeoObject posto)
	{
		EntityWrapper ewPosto = new EntityWrapper(posto);

		Long codigoFilial = (Long) ewPosto.findField("usu_codfil").getValue();

		if (NeoUtils.safeIsNull(codigoFilial))
		{
			System.out.println("Posto destino deve possuir uma filial ligada.");
			QLAdapter.erros.add("Posto destino deve possuir uma filial ligada.");
			QLAdapter.log.error("Posto destino deve possuir uma filial ligada.");
			QLAdapter.validation = false;
		}
	}

	/**
	 * Verifica se existe vaga disponível no posto
	 * 
	 * @param posto
	 * @param codOpe
	 */
	public static void validarVagaNoPosto(NeoObject posto, Integer codOpe)
	{
		EntityWrapper postoWrapper = new EntityWrapper(posto);

		//busca o posto do colaborador
		Long tabOrg = (Long) postoWrapper.findValue("taborg");
		Long numLoc = (Long) postoWrapper.findValue("numloc");
		try
		{
			List registrosVagas = new ArrayList();

			StringBuilder sqlSelect = new StringBuilder();

			sqlSelect.append(" SELECT orn.USU_QtdCon FROM R034FUN fun ");
			sqlSelect.append(" INNER JOIN R038HLO hlo WITH (NOLOCK) ON hlo.NumEmp = fun.NumEmp AND hlo.TipCol = fun.TipCol AND hlo.NumCad = fun.NumCad AND hlo.DatAlt = (SELECT MAX (hlo2.DATALT) FROM R038HLO hlo2 WHERE hlo2.NUMEMP = hlo.NUMEMP AND hlo2.TIPCOL = hlo.TIPCOL AND hlo2.NUMCAD = hlo.NUMCAD AND hlo2.DATALT <= GETDATE()) ");
			sqlSelect.append(" INNER JOIN R016ORN orn WITH (NOLOCK) ON orn.NumLoc = hlo.NumLoc and orn.TabOrg = hlo.TabOrg ");
			sqlSelect.append(" LEFT JOIN R038AFA afa WITH (NOLOCK) ON (afa.NumEmp = fun.NumEmp AND afa.TipCol = fun.TipCol AND afa.NumCad = fun.NumCad AND afa.DatAfa = (SELECT MAX (afa2.DATAFA) FROM R038AFA afa2 WHERE afa2.NUMEMP = afa.NUMEMP AND afa2.TIPCOL = afa.TIPCOL AND afa2.NUMCAD = afa.NUMCAD AND afa2.DATAFA <= GETDATE() AND (afa2.DatTer = '1900-12-31 00:00:00' OR afa2.DatTer >= cast(floor(cast(GETDATE() as float)) as datetime)))) ");
			sqlSelect.append(" WHERE orn.NumLoc = :numLoc AND orn.TabOrg = :tabOrg AND fun.DatAdm <= GETDATE() ");
			sqlSelect.append(" AND (afa.SitAfa IS NULL OR afa.SitAfa <> 7 OR (afa.SitAfa = 7 AND afa.DatAfa > cast(floor(cast(GETDATE() as float)) as datetime))) ");
			sqlSelect.append(" AND fun.TipCol = 1 AND fun.NumEmp not in (5, 11, 14)");

			Query query = QLAdapter.entityManager.createNativeQuery(sqlSelect.toString());
			query.setParameter("tabOrg", tabOrg);
			query.setParameter("numLoc", numLoc);

			registrosVagas = query.getResultList();

			String sqlSelect2 = "Select R016ORN.USU_ConVag " + " From R016ORN, R016HIE " + " Where R016ORN.TabOrg =:tabOrg " + " AND R016ORN.NumLoc =:numLoc " + " AND R016HIE.TabOrg = R016ORN.TabOrg " + " AND R016HIE.NumLoc = R016ORN.NumLoc ";

			Query query2 = QLAdapter.entityManager.createNativeQuery(sqlSelect2);
			query2.setParameter("tabOrg", tabOrg);
			query2.setParameter("numLoc", numLoc);
			query2.setMaxResults(1);

			Object controlaVaga = query2.getSingleResult();

			String sqlSelect3 = "Select USU_T010MoCo.USU_ConQuaVag " + " From USU_T010MoCo " + " Where USU_T010MoCo.USU_CodMot =:codOpe ";

			Query query3 = QLAdapter.entityManager.createNativeQuery(sqlSelect3);
			query3.setParameter("codOpe", codOpe);
			query3.setMaxResults(1);

			Object controlaQuadroVaga = query3.getSingleResult();

			if (NeoUtils.safeIsNotNull(registrosVagas) && registrosVagas.size() > 0)
			{

				int qtdeRegistros = registrosVagas.size();
				Object qtdeVagas = registrosVagas.get(0);
				int qtdcon = ((Integer) qtdeVagas).intValue();

				if (NeoUtils.safeIsNotNull(qtdeRegistros) && NeoUtils.safeIsNotNull(qtdcon) && controlaVaga != null && controlaQuadroVaga != null && qtdeRegistros >= qtdcon && controlaVaga.equals("S") && controlaQuadroVaga.toString().equals("S"))
				{
					System.out.println("Posto deve possuir vaga disponível.");
					QLAdapter.erros.add("Posto deve possuir vaga disponível.");
					QLAdapter.log.error("Posto deve possuir vaga disponível.");
					QLAdapter.validation = false;
				}
			}
		}
		catch (NoResultException noResultException)
		{
			System.out.println("Posto deve possuir vaga disponível.");
			QLAdapter.erros.add("Posto deve possuir vaga disponível.");
			QLAdapter.log.error("Posto deve possuir vaga disponível.");
			QLAdapter.validation = false;
		}
	}

	/**
	 * Verifica se o posto tem a mesma empresa que a empresa do colaborador
	 * 
	 * @param colaborador, posto
	 */
	public static void validarColaboradorEmpresa(NeoObject colaborador, NeoObject posto)
	{
		EntityWrapper colaboradorWrapper = new EntityWrapper(colaborador);
		EntityWrapper postoWrapper = new EntityWrapper(posto);

		Long coNumEmp = (Long) colaboradorWrapper.findField("numemp").getValue();
		Long poNumEmp = (Long) postoWrapper.findField("usu_numemp").getValue();
		String poCodLoc = (String) postoWrapper.findField("codloc").getValue();

		if (NeoUtils.safeIsNotNull(coNumEmp) && NeoUtils.safeIsNotNull(poNumEmp) && (!poCodLoc.substring(0, 6).equals("1.1.01") && !coNumEmp.equals(poNumEmp)))
		{
			System.out.println("Posto deve ser da mesma empresa do colaborador.");
			QLAdapter.erros.add("Posto deve ser da mesma empresa do colaborador.");
			QLAdapter.log.error("Posto deve ser da mesma empresa do colaborador.");
			QLAdapter.validation = false;
		}
	}

	/**
	 * Verifica se o colaborador a ser substituído está em férias
	 * 
	 * @param colaboradorSubstituir
	 * @param dataInicial
	 * @param dataFinal
	 */
	public static void validarColaboradorEmFerias(NeoObject colaboradorSubstituir, GregorianCalendar dataInicial, GregorianCalendar dataFinal)
	{
		EntityWrapper colaboradorSubstituirWrapper = new EntityWrapper(colaboradorSubstituir);

		Long numEmp = (Long) colaboradorSubstituirWrapper.findValue("numemp");
		Long tipCol = (Long) colaboradorSubstituirWrapper.findValue("tipcol");
		Long numcad = (Long) colaboradorSubstituirWrapper.findValue("numcad");

		StringBuilder sqlSelectSitAfa = new StringBuilder();
		sqlSelectSitAfa.append(" Select * From R038Afa Where R038Afa.NumEmp =:numEmp AND R038Afa.TipCol =:tipCol AND R038Afa.NumCad =:numcad AND R038Afa.SitAfa = 2 ");
		sqlSelectSitAfa.append(" And DATEADD(dd, DATEDIFF(dd, 0, R038Afa.DatAfa), 0) <= DATEADD(dd, DATEDIFF(dd, 0, :dataInicial), 0) ");
		sqlSelectSitAfa.append(" And DATEADD(dd, DATEDIFF(dd, 0, R038Afa.DatTer), 0) >= DATEADD(dd, DATEDIFF(dd, 0, :dataFinal), 0) ");

		Query query = QLAdapter.entityManager.createNativeQuery(sqlSelectSitAfa.toString());
		query.setParameter("numEmp", numEmp);
		query.setParameter("tipCol", tipCol);
		query.setParameter("numcad", numcad);
		query.setParameter("dataInicial", dataInicial);
		query.setParameter("dataFinal", dataFinal);

		List<Object> existsAfa = query.getResultList();

		if (existsAfa == null || existsAfa.isEmpty())
		{
			System.out.println("Colaborador a substituir não está em férias no período informado.");
			QLAdapter.erros.add("Colaborador a substituir não está em férias no período informado.");
			QLAdapter.log.error("Colaborador a substituir não está em férias no período informado.");
			QLAdapter.validation = false;
		}
	}

	/**
	 * Verifica se existe afastamento do colaborador
	 * 
	 * @param colaborador
	 * @param codOpe
	 * @param dataInicial
	 * @param dataFinal
	 */
	public static void validarAfastamentoColaborador(NeoObject colaborador, Integer codOpe, GregorianCalendar dataInicial, GregorianCalendar dataFinal)
	{
		try
		{
			EntityWrapper colaboradorWrapper = new EntityWrapper(colaborador);

			Long numEmp = (Long) colaboradorWrapper.findValue("numemp");
			Long tipCol = (Long) colaboradorWrapper.findValue("tipcol");
			Long numcad = (Long) colaboradorWrapper.findValue("numcad");

			String sqlSelect = "Select R010SIT.USU_PerMov " + " From R038Afa, R010SIT " + "	Where R038Afa.NumEmp =:numEmp " + "	AND R038Afa.TipCol =:tipCol " + "	AND R038Afa.NumCad =:numcad " + " 	AND R038Afa.DatAfa <=:dataInicial " + "	AND R010SIT.CodSit <> 123 " + "	AND R038Afa.DatTer >=:dataFinal ";

			Query query = QLAdapter.entityManager.createNativeQuery(sqlSelect);
			query.setParameter("numEmp", numEmp);
			query.setParameter("tipCol", tipCol);
			query.setParameter("numcad", numcad);
			query.setParameter("dataInicial", dataInicial);
			query.setParameter("dataFinal", dataFinal);

			query.setMaxResults(1);
			Object permiteMov = query.getSingleResult();
			String sqlSelect3 = "Select USU_T010MoCo.USU_TipCob  " + " From USU_T010MoCo " + " Where USU_T010MoCo.USU_CodMot =:codOpe ";

			Query query3 = QLAdapter.entityManager.createNativeQuery(sqlSelect3);
			query3.setParameter("codOpe", codOpe);
			query3.setMaxResults(1);

			Object controlaQuadroVaga = query3.getSingleResult();

			String tipCob = controlaQuadroVaga.toString();

			//sitafa = 2 : Permitir incluir cobertura quando o mesmo esta de ferias
			String sqlSelect4 = "Select R038Afa.sitafa " + " From R038Afa, R010SIT " + "	Where R038Afa.NumEmp =:numEmp " + "	AND R038Afa.TipCol =:tipCol " + "	AND R038Afa.NumCad =:numcad " + " 	AND R038Afa.DatAfa <=:dataInicial " + "	AND R010SIT.CodSit <> 123 " + "	AND R038Afa.DatTer >=:dataFinal ";
			Query query2 = QLAdapter.entityManager.createNativeQuery(sqlSelect4);
			query2.setParameter("numEmp", numEmp);
			query2.setParameter("tipCol", tipCol);
			query2.setParameter("numcad", numcad);
			query2.setParameter("dataInicial", dataInicial);
			query2.setParameter("dataFinal", dataFinal);
			query2.setMaxResults(1);

			Object situacaoColaborador = query2.getSingleResult();

			int sitAfa = Integer.parseInt(situacaoColaborador.toString());

			//Solicitação via tarefa simples 707885
			if (tipCob.equals("P") && permiteMov.toString().equals("S") || sitAfa == 2)
			{
				//pode movimentar
			}
			else
			{
				System.out.println("O colaborador não pode possuir afastamento no período da operação.");
				QLAdapter.erros.add("O colaborador não pode possuir afastamento no período da operação.");
				QLAdapter.log.error("O colaborador não pode possuir afastamento no período da operação.");
				QLAdapter.validation = false;
			}
		}
		catch (NoResultException noResultException)
		{
		}
	}

	/**
	 * Verifica se a cobetura de hora extra está dentro de um horário normal
	 * 
	 * @param colaborador
	 * @param dataInicial
	 * @param dataFinal
	 */
	public static void validarHoraExtra(NeoObject colaborador, GregorianCalendar dataInicial, GregorianCalendar dataFinal)
	{
		try
		{
			EntityWrapper colaboradorWrapper = new EntityWrapper(colaborador);
			Long codTma = (Long) colaboradorWrapper.findValue("codtma");
			Long codEsc = (Long) colaboradorWrapper.findValue("codesc");

			int dias = (int) ((dataFinal.getTimeInMillis() - dataInicial.getTimeInMillis()) / 86400000) + 1;

			HorarioVO horarioCobertura = new HorarioVO();
			horarioCobertura.setDataInicial(dataInicial);
			horarioCobertura.setDataFinal(dataFinal);

			//percorrer o intervalo de dias entre inicio e fim
			for (int i = 0; i < dias; i++)
			{

				//a cada iteração tenho um dia do intervalo inicial e final
				GregorianCalendar dataAtualInicio = (GregorianCalendar) dataInicial.clone();
				dataAtualInicio.add(Calendar.DAY_OF_MONTH, i);
				dataAtualInicio = OrsegupsUtils.atribuiHora(dataAtualInicio);

				GregorianCalendar dataAtualFinal = (GregorianCalendar) dataAtualInicio.clone();
				dataAtualFinal.set(Calendar.HOUR_OF_DAY, dataFinal.get(dataFinal.HOUR_OF_DAY));
				dataAtualFinal.set(Calendar.MINUTE, dataFinal.get(dataFinal.MINUTE));

				List<HorarioVO> horariosCobertura = QLPresencaUtils.getHorariosOperacoesPostos(horarioCobertura, dataAtualInicio);
				//FIXME comentado pois o metodo nao esta disponivel no produto
				//List<EscalaVO> escalasColaborador = QLPresencaUtils.getEscalasColaboradores(dataAtualInicio, codEsc, codTma,false);
				List<EscalaVO> escalasColaborador = null;

				List<HorarioVO> horariosColaborador = new ArrayList<HorarioVO>();

				if (NeoUtils.safeIsNotNull(escalasColaborador) && (NeoUtils.safeIsNotNull(escalasColaborador.get(0).getHorarios())))
				{
					horariosColaborador = escalasColaborador.get(0).getHorarios();

					//percorrer a lista de horários da data atual no momento
					for (HorarioVO horarioCob : horariosCobertura)
					{
						GregorianCalendar datIniCobertura = horarioCob.getDataInicial();
						GregorianCalendar datFimCobertura = horarioCob.getDataFinal();

						for (HorarioVO horarioColab : horariosColaborador)
						{
							GregorianCalendar datIniColaborador = horarioColab.getDataInicial();
							GregorianCalendar datFimColaborador = horarioColab.getDataFinal();

							if (datIniCobertura.after(datIniColaborador) && datIniCobertura.before(datFimColaborador) || datFimCobertura.after(datIniColaborador) && datFimCobertura.before(datFimColaborador))
							{
								System.out.println("A Hora Extra lançada está em conflito com a escala do colaborador.");
								QLAdapter.erros.add("A Hora Extra lançada está em conflito com a escala do colaborador.");
								QLAdapter.log.error("A Hora Extra lançada está em conflito com a escala do colaborador.");
								QLAdapter.validation = false;
								System.out.println("TEste");
								return;
							}
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Verifica o colaborador está com o mesmo centro de custo de um local reserva técnica
	 * 
	 * @param colaborador
	 * @param colaboradorSubstituir
	 * @param posto
	 */
	public static void validarOPReservaTecnica(NeoObject colaborador, NeoObject colaboradorSubstituir, NeoObject posto, String codigoOperacao, Boolean isHorista)
	{
		try
		{
			EntityWrapper ewColaborador = new EntityWrapper(colaborador);
			String codigoPostoColaborador = ((Long) ewColaborador.findField("numloc").getValue()).toString();
			Long codRegColab = (Long) ewColaborador.findField("usu_codreg").getValue();
			String codigoPosto = null;
			String tipoRT = "reserva";
			if (isHorista)
			{
				tipoRT = "horista";
			}

			String sqlCCUReservaTecnica = " Select numloc from " + " USU_Vorg203nv8 where nomloc like '%" + tipoRT + "%' AND codloc LIKE '1.1%' " + "   and usu_codreg =:codReg " + "   and usu_sitati = 'S' " + "    order by usu_codreg";

			Query queryRT = QLAdapter.entityManager.createNativeQuery(sqlCCUReservaTecnica);
			queryRT.setParameter("codReg", codRegColab);

			List<Object> listCodigoPosto = (List<Object>) queryRT.getResultList();
			Boolean existeErro = true;
			if (NeoUtils.safeIsNotNull(listCodigoPosto) && listCodigoPosto.size() > 0)
			{
				for (Object objCodigoPosto : listCodigoPosto)
				{
					codigoPosto = objCodigoPosto.toString();

					if (codigoPosto != null && codigoPosto.equals(codigoPostoColaborador))
					{
						existeErro = false;
					}
				}
			}
			if (existeErro)
			{
				if (isHorista)
				{
					System.out.println("O colaborador não é Horista para essa Regional.");
					QLAdapter.erros.add("O colaborador não é Horista para essa Regional.");
					QLAdapter.log.error("O colaborador não é Horista para essa Regional.");
					QLAdapter.validation = false;
				}
				/*
				 * else
				 * {
				 * System.out.println("O colaborador não é Reserva Técnica para essa Regional.");
				 * QLAdapter.erros.add("O colaborador não é Reserva Técnica para essa Regional.");
				 * QLAdapter.log.error("O colaborador não é Reserva Técnica para essa Regional.");
				 * QLAdapter.validation = false;
				 * }
				 */
			}
			//Se for hosrista verifica se a escala é 999
			if (codigoOperacao.equalsIgnoreCase("0012"))
			{
				Long codesc = (Long) ewColaborador.findField("codesc").getValue();
				if (!codesc.equals(999L))
				{
					System.out.println("A escala do colaborador não é compatível com essa operação.");
					QLAdapter.erros.add("A escala do colaborador não é compatível com essa operação.");
					QLAdapter.log.error("A escala do colaborador não é compatível com essa operação.");
					QLAdapter.validation = false;
				}
			}
		}
		catch (NoResultException ex)
		{
			System.out.println("Não foi possível encontrar a Regional ou o Centro de Custo para o posto.");
			QLAdapter.erros.add("Não foi possível encontrar a Regional ou o Centro de Custo para o posto.");
			QLAdapter.log.error("Não foi possível encontrar a Regional ou o Centro de Custo para o posto.");
			QLAdapter.validation = false;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	/**
	 * Verifica se o colaborador tem restrição com o cliente, caso o colaborador não possa ver enviado a
	 * determinado cliente
	 * 
	 * @param colaborador
	 * @param codOpe
	 * @param posto
	 */
	public static void validarColaboradorRestricaoCliente(NeoObject colaborador, String tipoOperacao, NeoObject posto)
	{
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		String nivOrgColab = "";

		EntityWrapper colaboradorWrapper = new EntityWrapper(colaborador);

		Long numEmp = (Long) colaboradorWrapper.findValue("numemp");
		Long tipCol = (Long) colaboradorWrapper.findValue("tipcol");
		Long numcad = (Long) colaboradorWrapper.findValue("numcad");
		Long numlocColab = (Long) colaboradorWrapper.findValue("numloc");
		Long tabOrgColab = (Long) colaboradorWrapper.findValue("taborg");

		System.out.println("[Restricao] [" + key + "] - numEmp:" + numEmp + ", numcad: " + numcad + ", numlocColab:" + numlocColab + ", tabOrgColab:" + tabOrgColab);

		Long numLocUsuario = null;
		Long tabOrgUsuario = null;

		//De acordo com o tipo da operacao, localiza o numLoc e o tabOrg do colaborador ou do posto.
		if (tipoOperacao.contains("TP") || tipoOperacao.contains("CP"))
		{
			EntityWrapper postoWrapper = new EntityWrapper(posto);
			Long numLocPosto = (Long) postoWrapper.findValue("numloc");
			Long tabOrgPosto = (Long) postoWrapper.findValue("taborg");
			numLocUsuario = numLocPosto;
			tabOrgUsuario = tabOrgPosto;

			System.out.println("[Restricao] [" + key + "] - " + tipoOperacao + " - numLocPosto:" + numLocPosto + ", tabOrgPosto: " + tabOrgPosto);
		}
		if (tipoOperacao.contains("CC"))
		{
			numLocUsuario = numlocColab;
			tabOrgUsuario = tabOrgColab;
			System.out.println("[Restricao] [" + key + "] - CC - numLocUsuario:" + numLocUsuario + ", tabOrgUsuario: " + tabOrgUsuario);
		}

		//Verificar qual o local do cliente, seja do colaborador ou do posto de destino
		if (NeoUtils.safeIsNotNull(numLocUsuario) && NeoUtils.safeIsNotNull(tabOrgUsuario))
		{
			String sqlNumLoc = "Select R016HIE.CodLoc " + " From R016HIE " + " Where R016HIE.TabOrg =:tabOrgUsuario " + "  AND R016HIE.NumLoc =:numLocUsuario ";

			Query queryNumLoc = QLAdapter.entityManager.createNativeQuery(sqlNumLoc);
			queryNumLoc.setParameter("tabOrgUsuario", tabOrgUsuario);
			queryNumLoc.setParameter("numLocUsuario", numLocUsuario);
			queryNumLoc.setMaxResults(1);

			Object organogramaCliente = queryNumLoc.getSingleResult();
			nivOrgColab = organogramaCliente.toString().substring(0, 12);

			System.out.println("[Restricao] [" + key + "] - nivOrgColab:" + nivOrgColab);
		}

		String sqlSelect = "Select USU_T038CobFun.USU_TabOrg, " + "  USU_T038CobFun.USU_NumLoc, " + "  USU_T038CobFun.USU_TabOrgTra, " + "  USU_T038CobFun.USU_NumLocTra, " + "  USU_T010MoCo.USU_ResCli " + "  From USU_T038CobFun,USU_T010MoCo " + "   WHERE USU_T038CobFun.USU_NumEmp =:numEmp " + "    AND USU_T038CobFun.USU_TipCol =:tipCol " + "    AND USU_T038CobFun.USU_NumCad =:numcad " + "    AND USU_T010MoCo.USU_CodMot = USU_T038CobFun.USU_CodMot " + "    AND USU_T010MoCo.USU_ResCli = 'S' ";

		Query query = QLAdapter.entityManager.createNativeQuery(sqlSelect);
		query.setParameter("numEmp", numEmp);
		query.setParameter("tipCol", tipCol);
		query.setParameter("numcad", numcad);

		List<Object[]> resultRestricao = query.getResultList();

		if (NeoUtils.safeIsNotNull(resultRestricao) && resultRestricao.size() > 0)
		{
			for (int i = 0; i < resultRestricao.size(); i++)
			{
				Object[] registro = resultRestricao.get(i);
				Short tabOrg = (Short) registro[0];
				Integer numLoc = (Integer) registro[1];
				Short tabOrgTra = (Short) registro[2];
				Integer numLocTra = (Integer) registro[3];

				Long tabOrgGeral = null;
				Long numLocGeral = null;

				if (tipoOperacao.contains("TP") || tipoOperacao.contains("CP"))
				{
					tabOrgGeral = tabOrgTra.longValue();
					numLocGeral = numLocTra.longValue();
					System.out.println("[Restricao] [" + key + "] - " + tipoOperacao + " - vl2 - numLocGeral:" + numLocGeral + ", tabOrgGeral: " + tabOrgGeral);
				}
				if (tipoOperacao.contains("CC"))
				{
					tabOrgGeral = tabOrg.longValue();
					numLocGeral = numLoc.longValue();
					System.out.println("[Restricao] [" + key + "] - CC2 - vl2 - numLocGeral:" + numLocGeral + ", tabOrgGeral: " + tabOrgGeral);
				}

				if (NeoUtils.safeIsNotNull(tabOrgGeral) && NeoUtils.safeIsNotNull(numLocGeral))
				{

					String sqlNumLoc = "Select R016HIE.CodLoc " + " From R016HIE " + " Where R016HIE.TabOrg =:tabOrgGeral " + "     AND R016HIE.NumLoc =:numLocGeral ";

					Query queryNumLoc = QLAdapter.entityManager.createNativeQuery(sqlNumLoc);
					queryNumLoc.setParameter("tabOrgGeral", tabOrgGeral);
					queryNumLoc.setParameter("numLocGeral", numLocGeral);
					queryNumLoc.setMaxResults(1);

					Object controlaCC = queryNumLoc.getSingleResult();
					String nivOrgRestricao = controlaCC.toString().substring(0, 12);

					System.out.println("[Restricao] [" + key + "] - nivOrgColab:" + nivOrgColab + ", nivOrgRestricao: " + nivOrgRestricao);

					/*
					 * nivOrgColab - Localiza no colaborador o nivel no organograma
					 * nivOrgRestricao - Localiza no resultado da query acima as restrições do
					 * colaborador
					 * para verificar se eh compativel com o nivel do cliente
					 */
					if (nivOrgColab.equals(nivOrgRestricao))
					{
						System.out.println("O colaborador possui restrição para esse cliente.");
						QLAdapter.erros.add("O colaborador possui restrição para esse cliente.");
						QLAdapter.log.error("O colaborador possui restrição para esse cliente.");
						QLAdapter.validation = false;
					}
				}
			}
		}
	}

	/**
	 * Verifica se a data de cobertura é inferior a data de admissão do colaborador
	 * 
	 * @param colaborador
	 */
	public static void validarDataAdmissao(NeoObject colaborador, GregorianCalendar dataAfa)
	{
		EntityWrapper colaboradorWrapper = new EntityWrapper(colaborador);

		Long numEmp = (Long) colaboradorWrapper.findValue("numemp");
		Long tipCol = (Long) colaboradorWrapper.findValue("tipcol");
		Long numCad = (Long) colaboradorWrapper.findValue("numcad");

		StringBuilder admColaborador = new StringBuilder();
		admColaborador.append(" SELECT DATADM FROM R034FUN WHERE numemp = :numemp AND tipcol = :tipcol AND numcad = :numcad ");

		Query queryAdm = QLAdapter.entityManager.createNativeQuery(admColaborador.toString());
		queryAdm.setParameter("numemp", numEmp);
		queryAdm.setParameter("tipcol", tipCol);
		queryAdm.setParameter("numcad", numCad);
		queryAdm.setMaxResults(1);

		Timestamp timeAdm = (Timestamp) queryAdm.getSingleResult();
		GregorianCalendar dataAdm = new GregorianCalendar();
		dataAdm.setTimeInMillis(timeAdm.getTime());

		if (NeoUtils.safeIsNotNull(dataAdm) && dataAfa.before(dataAdm))
		{
			System.out.println("O colaborador à substituir ainda não estava admitido nesta data.");
			QLAdapter.erros.add("O colaborador à substituir ainda não estava admitido nesta data.");
			QLAdapter.log.error("O colaborador à substituir ainda não estava admitido nesta data.");
			QLAdapter.validation = false;
		}
	}

	/**
	 * Verifica se o horário do posto é compatível ao horário lançado para a cobertura
	 * 
	 * @param dataInicial
	 * @param dataFinal
	 * @param posto
	 */
	public static void validarHorarioCoberturaPosto(GregorianCalendar dataInicial, GregorianCalendar dataFinal, NeoObject posto)
	{
		try
		{
			int dias = (int) ((dataFinal.getTimeInMillis() - dataInicial.getTimeInMillis()) / 86400000) + 1;

			HorarioVO horarioCobertura = new HorarioVO();
			horarioCobertura.setDataInicial(dataInicial);
			horarioCobertura.setDataFinal(dataFinal);

			EntityWrapper wrapperPosto = new EntityWrapper(posto);
			String centroCustoPosto = (String) wrapperPosto.findField("usu_codccu").getValue();
			Long numeroPosto = (Long) wrapperPosto.findField("usu_numpos").getValue();

			//percorrer o intervalo de dias entre inicio e fim
			for (int i = 0; i < dias; i++)
			{

				//a cada iteração tenho um dia do intervalo inicial e final
				GregorianCalendar dataAtualInicio = (GregorianCalendar) dataInicial.clone();
				dataAtualInicio.add(Calendar.DAY_OF_MONTH, i);
				dataAtualInicio = OrsegupsUtils.atribuiHora(dataAtualInicio);

				GregorianCalendar dataAtualFinal = (GregorianCalendar) dataAtualInicio.clone();
				dataAtualFinal.set(Calendar.HOUR_OF_DAY, dataFinal.get(dataFinal.HOUR_OF_DAY));
				dataAtualFinal.set(Calendar.MINUTE, dataFinal.get(dataFinal.MINUTE));

				List<HorarioVO> horariosCobertura = QLPresencaUtils.getHorariosOperacoesPostos(horarioCobertura, dataAtualInicio);

				EscalaPostoVO escalaPosto = QLPresencaUtils.getJornadaPosto(centroCustoPosto, numeroPosto, "Ativo", dataAtualInicio);

				if (escalaPosto != null)
				{

					List<HorarioVO> horariosPosto = escalaPosto.getHorariosHoje();
					Boolean isHorarioCompativel = false;
					if (horariosPosto != null && !horariosPosto.isEmpty())
					{
						//percorrer a lista de horários da data atual no momento
						for (HorarioVO horarioCob : horariosCobertura)
						{
							GregorianCalendar datIniCobertura = horarioCob.getDataInicial();
							GregorianCalendar datFimCobertura = horarioCob.getDataFinal();

							for (HorarioVO horarioPosto : horariosPosto)
							{
								GregorianCalendar datIniPosto = horarioPosto.getDataInicial();
								GregorianCalendar datFimPosto = horarioPosto.getDataFinal();

								if ((datIniCobertura.equals(datIniPosto) || datIniCobertura.after(datIniPosto)) && (datFimCobertura.equals(datFimPosto) || datFimCobertura.before(datFimPosto)))
								{
									isHorarioCompativel = true;
								}
							}
						}
						if (!isHorarioCompativel)
						{
							System.out.println("O período utilizado não corresponde a escala do Posto.");
							QLAdapter.erros.add("O período utilizado não corresponde a escala do Posto.");
							QLAdapter.log.error("O período utilizado não corresponde a escala do Posto.");
							QLAdapter.validation = false;
						}
					}
					else
					{
						System.out.println("Não foi encontrado horários para o posto. Verifique com o suporte.");
						QLAdapter.erros.add("Não foi encontrado horários para o posto. Verifique com o suporte.");
						QLAdapter.log.error("Não foi encontrado horários para o posto. Verifique com o suporte.");
						QLAdapter.validation = false;
					}
				}
				else
				{
					System.out.println("Posto não possui escala cadastrada.");
					QLAdapter.erros.add("Posto não possui escala cadastrada.");
					QLAdapter.log.error("Posto não possui escala cadastrada.");
					QLAdapter.validation = false;
				}
			}
		}
		catch (Exception e)
		{
			System.out.println("Erro na validação de horários. Verifique com o suporte.");
			QLAdapter.erros.add("Erro na validação de horários. Verifique com o suporte.");
			QLAdapter.log.error("Erro na validação de horários. Verifique com o suporte.");
			QLAdapter.validation = false;
		}
	}

	public static String getCodCcuColaborador(NeoObject colaborador)
	{
		String codCcuRetorno = null;
		EntityWrapper colaboradorWrapper = new EntityWrapper(colaborador);

		//busca o posto do colaborador
		Long tabOrgColaborador = (Long) colaboradorWrapper.findValue("taborg");
		Long numLocColaborador = (Long) colaboradorWrapper.findValue("numloc");

		try
		{
			String sqlSelect = "SELECT usu_codccu FROM r016orn WHERE taborg = :taborg AND numloc = :numloc";
			Query query = QLAdapter.entityManager.createNativeQuery(sqlSelect);
			query.setMaxResults(1);
			query.setParameter("taborg", tabOrgColaborador);
			query.setParameter("numloc", numLocColaborador);

			Object codCcu = query.getSingleResult();

			if (codCcu != null)
			{
				codCcuRetorno = (String) codCcu;
			}
			else
			{
				QLAdapter.validation = false;
			}
		}
		catch (NoResultException noResultException)
		{
			QLAdapter.erros.add("Posto não encontrado para o colaborador '" + colaboradorWrapper.findValue("nomfun") + "'!");
			QLAdapter.log.error("Posto não encontrado para o colaborador '" + colaboradorWrapper.findValue("nomfun") + "'!");
			QLAdapter.validation = false;
		}
		catch (Exception e)
		{
			QLAdapter.erros.add("Erro ao buscar posto para o colaborador '" + colaboradorWrapper.findValue("nomfun") + "'!");
			QLAdapter.log.error("Erro ao buscar posto para o colaborador '" + colaboradorWrapper.findValue("nomfun") + "'!");
			QLAdapter.validation = false;
			e.printStackTrace();
		}

		return codCcuRetorno;
	}

	/**
	 * Busca no cadastro de Transferencia Filial se existe registro para nova filial, somente para nível
	 * administrativo (1.1)
	 * 
	 * @param numEmp
	 * @param sisCar
	 * @return codFil
	 */
	public static Long searchFilialTransferencia(Long numEmp, Long sisCar, Long codreg, String estado)
	{
		Long codFil = 0L;

		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		groupFilter.addFilter(new QLEqualsFilter("codEmp.numemp", numEmp));
		groupFilter.addFilter(new QLEqualsFilter("sisCar.siscar", sisCar));
		if (estado != null && (estado.equals("SC") || estado.equals("PR")) && (numEmp == 21L || numEmp == 29L)){
		    groupFilter.addFilter(new QLEqualsFilter("estadoFilial", estado));
		}

		List<NeoObject> listCadastroAcessoMapa = PersistEngine.getObjects(AdapterUtils.getEntityClass("QLFilialTransferencia"), groupFilter);

		if (NeoUtils.safeIsNotNull(listCadastroAcessoMapa) && listCadastroAcessoMapa.size() > 0)
		{
			NeoObject cadastroTransf = listCadastroAcessoMapa.get(0);
			EntityWrapper ewCadastroTransf = new EntityWrapper(cadastroTransf);

			// Caso Empresa Orsegups Monitoramento e regional de curitiba, seta filial 3
			if (numEmp == 18L && codreg == 13L)
			{
				codFil = 3L;
			}
			else
			{
				codFil = (Long) ewCadastroTransf.findField("filial.codfil").getValue();
			}
		}

		return codFil;
	}

	public static void validarTrocaEscala(NeoObject colaborador, NeoObject escala) throws SQLException
	{
		//Obter dados do colaborador
		EntityWrapper colaboradorWrapper = new EntityWrapper(colaborador);
		Long codEsc = (Long) colaboradorWrapper.findValue("codesc");

		//Obter dados da escala a ser alterada
		EntityWrapper escalaWrapper = new EntityWrapper(escala);
		Long gruEsc = (Long) escalaWrapper.findValue("escala.escalaHor.usu_gruesc");

		//Obter acesso a base de dados
		Connection connVetorh = null;
		ResultSet rs = null;
		java.sql.Statement stEscala = null;

		try
		{

			connVetorh = PersistEngine.getConnection("VETORH");

			String sqlSelect = "SELECT usu_gruesc FROM r006esc WHERE codesc = " + codEsc;

			stEscala = connVetorh.createStatement();
			rs = stEscala.executeQuery(sqlSelect);
			/*
			 * Verifica se a escala do coloborador é horista e gera uma exception caso ele tente trocar
			 * de
			 * escala diferente de horista
			 */

			if (rs.next())
			{

				Long usuGruEsc = rs.getLong("usu_gruesc");

				if (usuGruEsc == 8L)
				{
					if (gruEsc != 8L)
					{
						throw new WorkflowException("Não é permitida a troca de escala de colaboradores horistas. Consulte o RH.");
					}
				}

				if (gruEsc == 8L)
				{
					if (usuGruEsc != 8L)
					{
						throw new WorkflowException("Não é permitida a troca de escala de colaboradores para horistas. Consulte o RH.");
					}
				}
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			rs.close();
			connVetorh.close();
			stEscala.close();
		}

	}

	public static void validarEmpresaDiferente(NeoObject colaborador, NeoObject posto)
	{
		EntityWrapper colaboradorWrapper = new EntityWrapper(colaborador);
		EntityWrapper postoWrapper = new EntityWrapper(posto);

		Long numEmpCol = (Long) colaboradorWrapper.findValue("numemp");
		Long numEmpPos = (Long) postoWrapper.findValue("usu_numemp");
		String bloCob = (String) postoWrapper.findValue("usu_blocob");

		if (bloCob != null && bloCob.equals("S") && numEmpCol.intValue() != numEmpPos.intValue())
		{
			System.out.println("Este posto não permite lotação de colaboradores de outras empresas.");
			QLAdapter.erros.add("Este posto não permite lotação de colaboradores de outras empresas.");
			QLAdapter.log.error("Este posto não permite lotação de colaboradores de outras empresas.");
			QLAdapter.validation = false;
		}
	}

	public static void validarEmpresaDiferenteColaborador(NeoObject colaborador, NeoObject colaboradorSubstituir)
	{
		EntityWrapper colaboradorWrapper = new EntityWrapper(colaborador);
		EntityWrapper colaboradorSubstituirWrapper = new EntityWrapper(colaboradorSubstituir);

		Long numEmpSub = (Long) colaboradorSubstituirWrapper.findValue("numemp");
		Long numLoc = (Long) colaboradorWrapper.findValue("numloc");
		Long tabOrg = (Long) colaboradorWrapper.findValue("taborg");

		String sqlSearchPosto = " SELECT usu_numemp, usu_blocob FROM R016ORN WHERE numloc = :numLoc AND taborg = :tabOrg";

		Query querySqlSearchPosto = QLAdapter.entityManager.createNativeQuery(sqlSearchPosto);

		querySqlSearchPosto.setParameter("numLoc", numLoc);
		querySqlSearchPosto.setParameter("tabOrg", tabOrg);

		Short numEmp = 0;
		String bloCob = "";

		Object resultPosto = querySqlSearchPosto.getSingleResult();
		if (NeoUtils.safeIsNotNull(resultPosto))
		{
			Object[] resultado = (Object[]) resultPosto;
			numEmp = (Short) resultado[0];
			bloCob = (String) resultado[1];
		}

		if (bloCob != null && bloCob.equals("S") && numEmp.intValue() != numEmpSub.intValue())
		{
			System.out.println("Este posto não permite lotação de colaboradores de outras empresas.");
			QLAdapter.erros.add("Este posto não permite lotação de colaboradores de outras empresas.");
			QLAdapter.log.error("Este posto não permite lotação de colaboradores de outras empresas.");
			QLAdapter.validation = false;
		}

	}

	public static void validarDataTrocaEscala(GregorianCalendar dataInicial, GregorianCalendar dataFinal)
	{
		Long dias = (dataFinal.getTimeInMillis() - dataInicial.getTimeInMillis()) / 1000 / 60 / 60 / 24;
		//Alterado a validação de 7 para 8 dias - Solicitação via tarefa 866219 - Juliano Clemente
		if (dias > 8L)
		{
			System.out.println("Data final superior a 8 dias da data inicial");
			QLAdapter.erros.add("Data final superior a 8 dias da data inicial");
			QLAdapter.log.error("Data final superior a 8 dias da data inicial");
			QLAdapter.validation = false;
		}

		if (dias < 0L)
		{
			System.out.println("Data inicial superior a data final");
			QLAdapter.erros.add("Data inicial superior a data final");
			QLAdapter.log.error("Data inicial superior a data final");
			QLAdapter.validation = false;
		}
	}
}