package com.neomind.fusion.custom.orsegups.converter;

import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;

public class OsergupsDesabilitaObrigatoriedadeCampos extends StringConverter {

	@Override
	public String getHTMLInput(final EFormField field, final OriginEnum origin) {
		EntityWrapper wrapper = new EntityWrapper(field.getForm().getObject());
		List<NeoObject> lista = wrapper.findGenericValue("validacao.listaCamposAprovados");
		if(lista != null && !lista.isEmpty()) {
			for (NeoObject campo : lista) {
				EntityWrapper ew = new EntityWrapper(campo);
				String nomeCampo = ew.findGenericValue("nome");
				EFormField eformField = field.getForm().getField(nomeCampo);
				if(eformField != null)
					eformField.setMandatory(false);
			}
		}
		
		return super.getHTMLInput(field, origin);
	}
}

