package com.neomind.fusion.custom.orsegups.rsc;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.rsc.vo.ServicosVO;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class RSCAbrirTarefaSimplesCartaCancelamento implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{
			RSCUtils rscUtils = new RSCUtils();

			String code = null;
			String avanca = null;
			String solicitante = null;
			String executor = null;
			String origem = "1";
			String familia;
			String servico;

			Long qtdDiasPrazo = null;
			List<ServicosVO> listaServicos = new ArrayList();

			List<NeoObject> paramTarAnexarCartaCancelamento = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("RSCParametrizacaoTarSimplesAnexarCartaCancelamento"));
			if (paramTarAnexarCartaCancelamento != null)
			{
				EntityWrapper wrpTarAnexarCartaCancelamento = new EntityWrapper(paramTarAnexarCartaCancelamento.get(0));
				solicitante = (String) wrpTarAnexarCartaCancelamento.findValue("solicitante.code");
				qtdDiasPrazo = (Long) wrpTarAnexarCartaCancelamento.findValue("prazo");
			}

			Long regionalContrato = (Long) processEntity.findValue("contratoSapiens.usu_codreg");
			Long empresaContrato = (Long) processEntity.findValue("contratoSapiens.usu_codemp");
			Long filialContrato = (Long) processEntity.findValue("contratoSapiens.usu_codfil");
			Long Contrato = (Long) processEntity.findValue("contratoSapiens.usu_numctr");

			listaServicos = rscUtils.contratoServico(empresaContrato, filialContrato, Contrato);
			familia = rscUtils.recuperaFamilia(listaServicos);
			servico = rscUtils.recuperaServico(listaServicos);

			NeoPaper papelExecutor = rscUtils.getPapelRespAnexarCartaCancelamento(regionalContrato, familia, servico);
			executor = OrsegupsUtils.getUserNeoPaper(papelExecutor.getCode());

			QLEqualsFilter ftrUsuario = new QLEqualsFilter("code", executor);
			NeoObject oUsuario = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoUser"), ftrUsuario);
			if (oUsuario != null)
			{
				avanca = "sim";
			}
			else
			{
				avanca = "nao";
			}

			/* Título da Tarefa */
			String titulo = "RSC " + ((String) processEntity.findValue("processoRscMae")) + " - Anexar Carta de Cancelamento - Contrato: " + processEntity.findValue("contratoSapiens.usu_numctr");

			/* Descrição da Tarefa */
			String descricao = "";
			descricao = "Prezado Gestor, por favor providenciar a Carta de Cancelamento do Contrato abaixo:<br><br>";
			descricao = descricao + " <strong>Empresa:</strong> " + processEntity.findValue("contratoSapiens.usu_codemp") + "<br>";
			descricao = descricao + " <strong>Filial:</strong> " + processEntity.findValue("contratoSapiens.usu_codfil") + "<br>";
			descricao = descricao + " <strong>Contrato:</strong> " + processEntity.findValue("contratoSapiens.usu_numctr") + "<br>";
			descricao = descricao + " <strong>Cód. Cliente:</strong> " + processEntity.findValue("RscRelatorioSolicitacaoCliente.clienteSapiens.codcli") + "<br>";
			descricao = descricao + " <strong>CPF/CNPJ:</strong> " + processEntity.findValue("RscRelatorioSolicitacaoCliente.clienteSapiens.cgccpf") + "<br>";
			descricao = descricao + " <strong>Nome Cliente:</strong> " + processEntity.findValue("RscRelatorioSolicitacaoCliente.clienteSapiens.nomcli") + "<br>";

			GregorianCalendar prazo = new GregorianCalendar();
			prazo = rscUtils.addDiasPrazoRSC(prazo, Integer.parseInt(qtdDiasPrazo.toString()), false);

			IniciarTarefaSimples iniciaTarefaSimples = new IniciarTarefaSimples();
			code = iniciaTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, origem, avanca, prazo);

			String observacao = (String) processEntity.findValue("observacao");
			observacao = observacao + "<br> Aberto a Tarefa Simples " + code + " para Anexar a Carta de Cancelamento pelo Gestor da Regional";
			processEntity.setValue("observacao", observacao);

		}
		catch (Exception e)
		{
			e.printStackTrace();
			OrsegupsUtils.sendEmail2Orsegups("desenvolvimento@orsegups.com.br", "fusion@orsegups.com.br", "Erro na Atividade [Abrir Tarefa Simples - Depto Comercial] da RSC de Cancelamento ", "", "corpo do e-mail", "", "", "");
			throw new WorkflowException("Erro ao Abrir a Tarefa Simples de Anexar Carta de Cancelamento!!!");
		}
	}



	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}

}
