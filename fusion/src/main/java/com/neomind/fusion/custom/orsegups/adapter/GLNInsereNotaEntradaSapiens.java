package com.neomind.fusion.custom.orsegups.adapter;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class GLNInsereNotaEntradaSapiens implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(GLNInsereNotaEntradaSapiens.class);

	private static String nomeFonteDados = "SAPIENS";

	private String urlstr = "";
	private String NOMUSU = "";
	private String SENUSU = "";
	private String PROXACAO = "";
	private String urlSID = "";

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		String mensagem = "";
		String nomeUsuario = "";
		String senhaUsuario = "";
		String nomeUsuarioAux = "";

		String SIS = "SIS=CO";
		String LOGIN = "&LOGIN=SID";
		String ACAO = "&ACAO=EXESENHA";
		StringBuffer inputLine = null;

		try
		{
			GregorianCalendar dataemissao = (GregorianCalendar) processEntity.findValue("dataEmissao");
			GregorianCalendar dataentrada = (GregorianCalendar) processEntity.findValue("dataEntrada");
			GregorianCalendar competenciaBI = (GregorianCalendar) processEntity.findValue("competenciaBI");

			if (dataentrada.before(dataemissao))
			{
				mensagem = "Data de Entrada deve ser maior ou igual a Data de Emissão!";
				throw new WorkflowException(mensagem);
			}

			QLEqualsFilter filtroLogin = new QLEqualsFilter("nomeUsuario", PortalUtil.getCurrentUser().getCode().concat(".sid"));
			List<NeoObject> listLogin = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("GCLoginSapiens"), filtroLogin);
			if (listLogin != null)
			{
				EntityWrapper wrapperLogin = new EntityWrapper(listLogin.get(0));
				nomeUsuario = (String) wrapperLogin.findValue("nomeUsuario");
				nomeUsuarioAux = nomeUsuario.substring(0, nomeUsuario.length() - 4);

				if (nomeUsuarioAux.equals(PortalUtil.getCurrentUser().getCode()))
				{
					senhaUsuario = (String) wrapperLogin.findValue("senhaUsuario");
				}
				else
				{
					mensagem = "Por favor, acesse o Fusion com um Usuário correspondente no Sapiens!";
					throw new WorkflowException(mensagem);
				}
			}
			else
			{
				mensagem = "Não encontrado usuário para lançar a Nota de Entrada no Sapiens!";
				throw new WorkflowException(mensagem);
			}

			NeoObject noConexao = ((List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("conexaoSID"))).get(0);
			EntityWrapper ewConexao = new EntityWrapper(noConexao);
			this.NOMUSU = "&NOMUSU=" + nomeUsuario.trim();
			this.SENUSU = "&SENUSU=" + senhaUsuario.trim();
			this.urlstr = ewConexao.findField("host").getValue().toString().trim();

			/* Dados Gerais do Cabeçalho da Nota de Entrada */
			Long codigoempresa = (Long) processEntity.findValue("empresa.codemp");
			Long codigofilial = (Long) processEntity.findValue("filial.codfil");
			Long fornecedor = (Long) processEntity.findValue("codigoFornecedor.codfor");
			Long nota = (Long) processEntity.findValue("notaFiscal");
			String serie = (String) processEntity.findValue("serieNotaFiscal.codsnf");
			BigDecimal valorLiquido = (BigDecimal) processEntity.findValue("valorLiquido");
			String transacaoProduto = (String) processEntity.findValue("transacaoProduto.codtns");
			if (transacaoProduto != null && !transacaoProduto.isEmpty())
			{
				transacaoProduto = (String) processEntity.findValue("transacaoProduto.codtns");
			}
			else
			{
				transacaoProduto = "";
			}

			String transacaoServico = (String) processEntity.findValue("transacaoServico.codtns");
			if (transacaoServico != null && !transacaoServico.isEmpty())
			{
				transacaoServico = (String) processEntity.findValue("transacaoServico.codtns");
			}
			else
			{
				transacaoServico = "";
			}

			String condicaoPagamento = (String) processEntity.findValue("condicaoPagamento.codcpg");
			String estadoICMS = "";
			if (estadoICMS != null && !estadoICMS.isEmpty())
			{
				processEntity.findValue("estadoBaseCalculoICMS.sigufs");
			}
			Long fornecedorISS = (Long) processEntity.findValue("codigoFornecedorISS.codfor");
			Long formapagamento = (Long) processEntity.findValue("formaPagamento.codfpg");
			Long usuarioresponsavel = (Long) processEntity.findValue("usuarioResponsavel.codusu");

			urlSID = urlstr + SIS + LOGIN + ACAO + NOMUSU + SENUSU;
			PROXACAO = "&PROXACAO=SID.SRV.XML";
			String CodEmp = codigoempresa.toString();
			String CodFil = codigofilial.toString();
			String CodFor = fornecedor.toString();
			String NumNfc = nota.toString();
			String CodSnf = serie;
			String VlrLiq = valorLiquido.toString();
			String TnsPro = transacaoProduto;
			String TnsSer = transacaoServico;
			String CodCpg = condicaoPagamento.toString();
			String UfsCic = estadoICMS;
			String ForISS = "";
			if (fornecedorISS != null)
			{
				ForISS = fornecedorISS.toString();
			}
			String CodFpg = formapagamento.toString();
			String UsuRes = usuarioresponsavel.toString();
			String DatEnt = NeoDateUtils.safeDateFormat(dataentrada, "dd/MM/yyyy");
			String DatEmi = NeoDateUtils.safeDateFormat(dataemissao, "dd/MM/yyyy");
			String Usu_DatEnt = NeoDateUtils.safeDateFormat(competenciaBI, "dd/MM/yyyy");

			System.out.println("Iniciado o processo de montagem do XML para inserir a NF de Entrada");

			String sidxml = "";
			sidxml = "<sidxml>";
			sidxml = sidxml + "<sid acao='SID.Srv.AltEmpFil'>";
			sidxml = sidxml + "<param nome='codemp' valor='" + CodEmp + "' />";
			sidxml = sidxml + "<param nome='codfil' valor='" + CodFil + "' />";
			sidxml = sidxml + "</sid>";
			sidxml = sidxml + "<sid acao='SID.Nfc.Gravar'>";
			sidxml = sidxml + "<param nome='codfor' valor='" + CodFor + "' />";
			sidxml = sidxml + "<param nome='numnfc' valor='" + NumNfc + "' />";
			sidxml = sidxml + "<param nome='codsnf' valor='" + CodSnf + "' />";
			sidxml = sidxml + "<param nome='vlrinf' valor='" + VlrLiq + "' />";
			sidxml = sidxml + "<param nome='tnspro' valor='" + TnsPro + "' />";
			sidxml = sidxml + "<param nome='tnsser' valor='" + TnsSer + "' />";
			sidxml = sidxml + "<param nome='datent' valor='" + DatEnt + "' />";
			sidxml = sidxml + "<param nome='datemi' valor='" + DatEmi + "' />";
			sidxml = sidxml + "<param nome='codcpg' valor='" + CodCpg + "' />";
			sidxml = sidxml + "<param nome='ufscic' valor='" + UfsCic + "' />";
			sidxml = sidxml + "<param nome='foriss' valor='" + ForISS + "' />";
			sidxml = sidxml + "<param nome='codfpg' valor='" + CodFpg + "' />";
			sidxml = sidxml + "<param nome='usu_usures' valor='" + UsuRes + "' />";
			sidxml = sidxml + "<param nome='usu_datent' valor='" + Usu_DatEnt + "' />";
			sidxml = sidxml + "</sid>";

			List<String> listaItensUpdateProduto = new ArrayList<String>();
			List<String> listaItensUpdateServico = new ArrayList<String>();
			List<String> listaItensUpdateParcela = new ArrayList<String>();
			//int qtdCCProduto = 0;
			//int qtdCCServico = 0;

			/* Ler a lista de Itens de Produto do Lançamento de Nota Fiscal de Entrada */
			List<NeoObject> listaItensDeProduto = (List<NeoObject>) processEntity.findValue("listaItensDeProduto");
			if (listaItensDeProduto != null && !listaItensDeProduto.isEmpty())
			{
				Integer seqItens = 1;
				List<NeoObject> listaDeCentroDeCustoProduto = null;
				Long contafinanceira = null;
				Long contacontabil = null;

				for (NeoObject itensDeProduto : listaItensDeProduto)
				{
					EntityWrapper wrpitensDeProduto = new EntityWrapper(itensDeProduto);
					listaDeCentroDeCustoProduto = (List<NeoObject>) wrpitensDeProduto.findValue("listaCentroCusto");

					String produto = (String) wrpitensDeProduto.findValue("produto.codpro");
					BigDecimal precoUnitarioServico = (BigDecimal) wrpitensDeProduto.findValue("precoUnitarioProduto");
					String unidadeMedida = (String) wrpitensDeProduto.findValue("unidadeMedida.unimed");
					Long quantidadeRecebida = (Long) wrpitensDeProduto.findValue("quantidadeRecebida");

					BigDecimal percentualIR = new BigDecimal(0.00);
					BigDecimal percentualPIS = new BigDecimal(0.00);
					BigDecimal percentualCOFINS = new BigDecimal(0.00);
					BigDecimal percentualCSLL = new BigDecimal(0.00);
					BigDecimal percentualOutrasRetencoes = new BigDecimal(0.00);

					if (percentualIR.compareTo(new BigDecimal(0.00)) > 0)
					{
						percentualIR = (BigDecimal) wrpitensDeProduto.findValue("valorIR");
					}
					if (percentualPIS.compareTo(new BigDecimal(0.00)) > 0)
					{
						percentualPIS = (BigDecimal) wrpitensDeProduto.findValue("valorPIS");
					}
					if (percentualCOFINS.compareTo(new BigDecimal(0.00)) > 0)
					{
						percentualCOFINS = (BigDecimal) wrpitensDeProduto.findValue("valorCOFINS");
					}
					if (percentualCSLL.compareTo(new BigDecimal(0.00)) > 0)
					{
						percentualCSLL = (BigDecimal) wrpitensDeProduto.findValue("valorCSLL");
					}
					if (percentualOutrasRetencoes.compareTo(new BigDecimal(0.00)) > 0)
					{
						percentualOutrasRetencoes = (BigDecimal) wrpitensDeProduto.findValue("valorOutrasRetencoes");
					}

					contafinanceira = (Long) wrpitensDeProduto.findValue("contaFinanceira.ctafin");
					contacontabil = (Long) wrpitensDeProduto.findValue("contaContabil.ctared");
					String descricaoProduto = (String) wrpitensDeProduto.findValue("observacaoDoProduto");

					System.out.println("Montando o XML de Gravar o Produto da NF de Entrada");

					sidxml = sidxml + "<sid acao='SID.Nfc.GravarProduto'>";
					sidxml = sidxml + "<param nome='codfor' valor='" + CodFor + "' />";
					sidxml = sidxml + "<param nome='numnfc' valor='" + NumNfc + "' />";
					sidxml = sidxml + "<param nome='codsnf' valor='" + CodSnf + "' />";
					sidxml = sidxml + "<param nome='tnspro' valor='" + TnsPro + "' />";
					sidxml = sidxml + "<param nome='codpro' valor='" + produto + "' />";
					sidxml = sidxml + "<param nome='codder' valor='" + "" + "' />";
					sidxml = sidxml + "<param nome='qtdrec' valor='" + quantidadeRecebida + "' />";
					sidxml = sidxml + "<param nome='preuni' valor='" + precoUnitarioServico + "' />";
					sidxml = sidxml + "<param nome='seqipc' valor='" + seqItens.toString() + "' />";
					sidxml = sidxml + "<param nome='unimed' valor='" + unidadeMedida + "' />";
					sidxml = sidxml + "<param nome='qtdrec' valor='" + quantidadeRecebida.toString() + "' />";
					sidxml = sidxml + "<param nome='perirf' valor='" + percentualIR.toString() + "' />";
					sidxml = sidxml + "<param nome='perpit' valor='" + percentualPIS.toString() + "' />";
					sidxml = sidxml + "<param nome='percrt' valor='" + percentualCOFINS.toString() + "' />";
					sidxml = sidxml + "<param nome='percsl' valor='" + percentualCSLL.toString() + "' />";
					sidxml = sidxml + "<param nome='perour' valor='" + percentualOutrasRetencoes.toString() + "' />";
					sidxml = sidxml + "</sid>";

					//qtdCCProduto = listaDeCentroDeCustoProduto.size();
					//if(listaDeCentroDeCustoProduto.size() < 1200)
					//{
					if (listaDeCentroDeCustoProduto != null && !listaDeCentroDeCustoProduto.isEmpty())
					{

						System.out.println("Montando o XML de Gravar o Rateio do Produto da NF de Entrada");

						sidxml = sidxml + "<sid acao='SID.Nfc.GravarRateioProduto'>";
						sidxml = sidxml + "<param nome='codfor' valor='" + CodFor + "' />";
						sidxml = sidxml + "<param nome='numnfc' valor='" + NumNfc + "' />";
						sidxml = sidxml + "<param nome='codsnf' valor='" + CodSnf + "' />";
						sidxml = sidxml + "<param nome='seqipc' valor='" + seqItens + "' />";

						Integer seqCCU = 1;
						for (NeoObject itensDeCentroDeCusto : listaDeCentroDeCustoProduto)
						{
							EntityWrapper wrpitensDeCentroDeCusto = new EntityWrapper(itensDeCentroDeCusto);
							BigDecimal valorRateio = (BigDecimal) wrpitensDeCentroDeCusto.findValue("valorRateio");
							String valorRateioStr = valorRateio.toString();

							//Ler o código do Centro de Custo informado no EForm Externo (E044CCU)
							NeoObject objectCentroCustoExterno = (NeoObject) wrpitensDeCentroDeCusto.findValue("codigoCentroCusto");
							EntityWrapper wrpCentroCusto = new EntityWrapper(objectCentroCustoExterno);
							String centroCustoStr = (String) wrpCentroCusto.findValue("codccu");

							if (centroCustoStr != null && valorRateioStr != null)
							{
								sidxml = sidxml + "<param nome='ctafin" + seqCCU.toString() + "' valor='" + contafinanceira + "' />";
								sidxml = sidxml + "<param nome='ctared" + seqCCU.toString() + "' valor='" + contacontabil + "' />";
								sidxml = sidxml + "<param nome='codccu" + seqCCU.toString() + "' valor='" + centroCustoStr + "' />";
								//sidxml = sidxml + "<param nome='percta" + seqCCU.toString() + "' valor='' />";
								sidxml = sidxml + "<param nome='vlrcta" + seqCCU.toString() + "' valor='" + precoUnitarioServico.toString() + "' />";
								//sidxml = sidxml + "<param nome='perrat" + seqCCU.toString() + "' valor='' />";
								sidxml = sidxml + "<param nome='vlrrat" + seqCCU.toString() + "' valor='" + valorRateioStr + "' />";
								sidxml = sidxml + "<param nome='numprj" + seqCCU.toString() + "' valor='" + 0 + "' />";
								sidxml = sidxml + "<param nome='codfpj" + seqCCU.toString() + "' valor='" + 0 + "' />";
								sidxml = sidxml + "<param nome='obsrat" + seqCCU.toString() + "' valor='" + "" + "' />";

								seqCCU++;
							}
						}
						listaItensUpdateProduto.add(CodEmp + ";" + CodFil + ";" + CodFor + ";" + NumNfc + ";" + CodSnf + ";" + seqItens + ";" + descricaoProduto);
						seqItens++;
					}
					sidxml = sidxml + "</sid>";
					//}
					//else
					//{

					//}
				}
			}

			/* Ler a lista de Itens de Serviço do Lançamento de Nota Fiscal de Entrada */
			List<NeoObject> listaItensDeServico = (List<NeoObject>) processEntity.findValue("listaItensDeServico");
			if (listaItensDeServico != null && !listaItensDeServico.isEmpty())
			{
				Integer seqItens = 1;
				List<NeoObject> listaDeCentroDeCustoServico = null;
				Long contafinanceira = null;
				Long contacontabil = null;

				for (NeoObject itensDeServico : listaItensDeServico)
				{
					System.out.println("Montando o XML de Gravar o Serviço da NF de Entrada");

					sidxml = sidxml + "<sid acao='SID.Nfc.GravarServico'>";
					sidxml = sidxml + "<param nome='codfor' valor='" + CodFor + "' />";
					sidxml = sidxml + "<param nome='numnfc' valor='" + NumNfc + "' />";
					sidxml = sidxml + "<param nome='codsnf' valor='" + CodSnf + "' />";
					sidxml = sidxml + "<param nome='tnsser' valor='" + TnsSer + "' />";

					EntityWrapper wrpitensDeServico = new EntityWrapper(itensDeServico);
					listaDeCentroDeCustoServico = (List<NeoObject>) wrpitensDeServico.findValue("listaCentroCusto");

					String servico = (String) wrpitensDeServico.findValue("servico.codser");
					BigDecimal precoUnitarioServico = (BigDecimal) wrpitensDeServico.findValue("precoUnitarioServico");
					String unidadeMedida = (String) wrpitensDeServico.findValue("unidadeMedida.unimed");
					Long quantidadeRecebida = (Long) wrpitensDeServico.findValue("quantidadeRecebida");

					BigDecimal percentualISS = new BigDecimal(0.00);
					BigDecimal percentualIR = new BigDecimal(0.00);
					BigDecimal percentualINSS = new BigDecimal(0.00);
					BigDecimal percentualPIS = new BigDecimal(0.00);
					BigDecimal percentualCOFINS = new BigDecimal(0.00);
					BigDecimal percentualCSLL = new BigDecimal(0.00);
					BigDecimal percentualOutrasRetencoes = new BigDecimal(0.00);

					if (percentualISS.compareTo(new BigDecimal(0.00)) > 0)
					{
						percentualISS = (BigDecimal) wrpitensDeServico.findValue("valorISS");
					}
					if (percentualIR.compareTo(new BigDecimal(0.00)) > 0)
					{
						percentualIR = (BigDecimal) wrpitensDeServico.findValue("valorIR");
					}
					if (percentualINSS.compareTo(new BigDecimal(0.00)) > 0)
					{
						percentualINSS = (BigDecimal) wrpitensDeServico.findValue("valorINSS");
					}
					if (percentualPIS.compareTo(new BigDecimal(0.00)) > 0)
					{
						percentualPIS = (BigDecimal) wrpitensDeServico.findValue("valorPIS");
					}
					if (percentualCOFINS.compareTo(new BigDecimal(0.00)) > 0)
					{
						percentualCOFINS = (BigDecimal) wrpitensDeServico.findValue("valorCOFINS");
					}
					if (percentualCSLL.compareTo(new BigDecimal(0.00)) > 0)
					{
						percentualCSLL = (BigDecimal) wrpitensDeServico.findValue("valorCSLL");
					}
					if (percentualOutrasRetencoes.compareTo(new BigDecimal(0.00)) > 0)
					{
						percentualOutrasRetencoes = (BigDecimal) wrpitensDeServico.findValue("valorOutrasRetencoes");
					}

					contafinanceira = (Long) wrpitensDeServico.findValue("contaFinanceira.ctafin");
					contacontabil = (Long) wrpitensDeServico.findValue("contaContabil.ctared");
					String descricaoServico = (String) wrpitensDeServico.findValue("observacaoDoServico");

					sidxml = sidxml + "<param nome='codser' valor='" + servico + "' />";
					sidxml = sidxml + "<param nome='seqisc' valor='" + seqItens.toString() + "' />";
					sidxml = sidxml + "<param nome='preuni' valor='" + precoUnitarioServico.toString() + "' />";
					sidxml = sidxml + "<param nome='unimed' valor='" + unidadeMedida + "' />";
					sidxml = sidxml + "<param nome='qtdrec' valor='" + quantidadeRecebida.toString() + "' />";
					sidxml = sidxml + "<param nome='periss' valor='" + percentualISS.toString() + "' />";
					sidxml = sidxml + "<param nome='perirf' valor='" + percentualIR.toString() + "' />";
					sidxml = sidxml + "<param nome='perins' valor='" + percentualINSS.toString() + "' />";
					sidxml = sidxml + "<param nome='perpit' valor='" + percentualPIS.toString() + "' />";
					sidxml = sidxml + "<param nome='percrt' valor='" + percentualCOFINS.toString() + "' />";
					sidxml = sidxml + "<param nome='percsl' valor='" + percentualCSLL.toString() + "' />";
					sidxml = sidxml + "<param nome='perour' valor='" + percentualOutrasRetencoes.toString() + "' />";
					sidxml = sidxml + "</sid>";

					//qtdCCServico = listaDeCentroDeCustoServico.size();
					if (listaDeCentroDeCustoServico != null && !listaDeCentroDeCustoServico.isEmpty())
					{
						System.out.println("Montando o XML de Gravar o Rateio do Serviço da NF de Entrada");

						sidxml = sidxml + "<sid acao='SID.Nfc.GravarRateioServico'>";
						sidxml = sidxml + "<param nome='codfor' valor='" + CodFor + "' />";
						sidxml = sidxml + "<param nome='numnfc' valor='" + NumNfc + "' />";
						sidxml = sidxml + "<param nome='codsnf' valor='" + CodSnf + "' />";
						sidxml = sidxml + "<param nome='seqisc' valor='" + seqItens + "' />";

						Integer seqCCU = 1;
						for (NeoObject itensDeCentroDeCusto : listaDeCentroDeCustoServico)
						{
							EntityWrapper wrpitensDeCentroDeCusto = new EntityWrapper(itensDeCentroDeCusto);
							BigDecimal valorRateio = (BigDecimal) wrpitensDeCentroDeCusto.findValue("valorRateio");
							String valorRateioStr = valorRateio.toString();

							//Ler o código do Centro de Custo informado no EForm Externo (E044CCU)
							NeoObject objectCentroCustoExterno = (NeoObject) wrpitensDeCentroDeCusto.findValue("codigoCentroCusto");
							EntityWrapper wrpCentroCusto = new EntityWrapper(objectCentroCustoExterno);
							String centroCustoStr = (String) wrpCentroCusto.findValue("codccu");

							if (centroCustoStr != null && valorRateioStr != null)
							{
								sidxml = sidxml + "<param nome='ctafin" + seqCCU.toString() + "' valor='" + contafinanceira + "' />";
								sidxml = sidxml + "<param nome='ctared" + seqCCU.toString() + "' valor='" + contacontabil + "' />";
								sidxml = sidxml + "<param nome='codccu" + seqCCU.toString() + "' valor='" + centroCustoStr + "' />";
								//sidxml = sidxml + "<param nome='percta" + seqCCU.toString() + "' valor='' />";
								sidxml = sidxml + "<param nome='vlrcta" + seqCCU.toString() + "' valor='" + precoUnitarioServico.toString() + "' />";
								//sidxml = sidxml + "<param nome='perrat" + seqCCU.toString() + "' valor='' />";
								sidxml = sidxml + "<param nome='vlrrat" + seqCCU.toString() + "' valor='" + valorRateioStr + "' />";
								sidxml = sidxml + "<param nome='numprj" + seqCCU.toString() + "' valor='" + 0 + "' />";
								sidxml = sidxml + "<param nome='codfpj" + seqCCU.toString() + "' valor='" + 0 + "' />";
								sidxml = sidxml + "<param nome='obsrat" + seqCCU.toString() + "' valor='" + "" + "' />";

								seqCCU++;
							}
						}
						sidxml = sidxml + "</sid>";
					}
					listaItensUpdateServico.add(CodEmp + ";" + CodFil + ";" + CodFor + ";" + NumNfc + ";" + CodSnf + ";" + seqItens + ";" + descricaoServico);
					seqItens++;
				}
			}

			/* Quantidade de Parcelas para Lançamento da Nota Fiscal de Entrada */
			List<NeoObject> listaQuantidadeParcelas = (List<NeoObject>) processEntity.findValue("listaParcelas");
			if (listaQuantidadeParcelas != null && !listaQuantidadeParcelas.isEmpty())
			{
				Integer seqParcelas = 1;

				System.out.println("Montando o XML de Gravar as Parcelas da NF de Entrada");

				sidxml = sidxml + "<sid acao='SID.Nfc.GravarParcelasManuais'>";
				sidxml = sidxml + "<param nome='codfor' valor='" + CodFor + "' />";
				sidxml = sidxml + "<param nome='numnfc' valor='" + NumNfc + "' />";
				sidxml = sidxml + "<param nome='codsnf' valor='" + CodSnf + "' />";

				for (NeoObject itensDeParcela : listaQuantidadeParcelas)
				{
					EntityWrapper wrpitensDeParcela = new EntityWrapper(itensDeParcela);
					String titulo = (String) wrpitensDeParcela.findValue("titulo");
					GregorianCalendar vencimentoTitulo = (GregorianCalendar) wrpitensDeParcela.findValue("vencimentoParcela");
					BigDecimal valororiginal = (BigDecimal) wrpitensDeParcela.findValue("valorParcela");
					String observacaoParcela = (String) wrpitensDeParcela.findValue("observacaoParcela");

					sidxml = sidxml + "<param nome='vlrpar" + seqParcelas.toString() + "' valor='" + valororiginal.toString() + "' />";
					sidxml = sidxml + "<param nome='vctpar" + seqParcelas.toString() + "' valor='" + NeoUtils.safeDateFormat(vencimentoTitulo, "dd/MM/yyyy") + "' />";
					sidxml = sidxml + "<param nome='numtit" + seqParcelas.toString() + "' valor='" + titulo + "' />";

					listaItensUpdateParcela.add(CodEmp + ";" + CodFil + ";" + CodFor + ";" + NumNfc + ";" + CodSnf + ";" + titulo + ";" + observacaoParcela);

					if (OrsegupsUtils.isWorkDay(vencimentoTitulo) == false)
					{
						mensagem = "Data de vencimento do Titulo não é um dia útil!";
						throw new WorkflowException(mensagem);
					}

					seqParcelas++;
				}
				sidxml = sidxml + "</sid>";
			}
			sidxml = sidxml + "</sidxml>";

			System.out.println("Finalizando o processo de montagem do XML para inserir a NF de Entrada");

			/*
			 * String consultaSID = "SELECT COUNT(*) FROM E440RAT WHERE CODEMP = " + CodEmp +
			 * " CODFIL = " + CodFil + " AND CODFOR = " + CodFor + " AND NUMNFC = " + NumNfc +
			 * " AND CODSNF = " + CodSnf;
			 */

			mensagem = executaSidUrl(urlSID, PROXACAO, sidxml);/*
																 * , consultaSID, qtdCCProduto,
																 * qtdCCServico
																 */
			System.out.println(sidxml);
			if (!mensagem.equals("OK"))
			{
				throw new WorkflowException(mensagem);
			}

			//Processar rateio manualmente via Insert 

			System.out.println("Iniciando o processo de alteração da Observação da da Nota Fiscal de Entrada e Parcela");
			alteraObservacaoNotaFiscalEParcela(listaItensUpdateProduto, listaItensUpdateServico, listaItensUpdateParcela);
			
			
			Connection conn = null;
			PreparedStatement stmt = null;
			ResultSet rs = null;
			
			try{
			    conn = PersistEngine.getConnection("SAPIENS");
			    Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();

			    StringBuffer query = new StringBuffer();
			    
			    query.append(" select CodPar, VctPar from E440PAR where CodEmp = ? and CodFil = ? and CodFor = ? and NumNfc = ? and CodSnf = ? ");
			    
			    stmt = conn.prepareStatement(query.toString());
			    stmt.setLong(1, Long.valueOf(CodEmp));
			    stmt.setLong(2, Long.valueOf(CodFil));
			    stmt.setLong(3, Long.valueOf(CodFor));
			    stmt.setLong(4, Long.valueOf(NumNfc));
			    stmt.setString(5, CodSnf);
			    rs = stmt.executeQuery();

			    log.warn("SQL - buscarParcelaNota - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");

			    while (rs.next()){
				Date dataVct = rs.getDate("VctPar");
				Long codParcela = rs.getLong("CodPar");
				GregorianCalendar dataVencUp = new GregorianCalendar();
				if (codParcela != null){
				    GregorianCalendar dataVencimento = new GregorianCalendar();
				    dataVencimento.setTime(dataVct);
				    if (dataVct != null){
					if (dataVencimento.get(Calendar.MONTH) == dataentrada.get(Calendar.MONTH)){
					    dataVencUp.setTime(dataentrada.getTime());
					}else{
					    dataVencUp.setTime(dataVencimento.getTime());
					}
					String update = "UPDATE E440PAR SET USU_DatEnt = ? WHERE CODEMP = ? AND CODFIL = ? AND CODFOR = ? AND NUMNFC = ? AND CODSNF = ? AND CODPAR = ? ";					
					dataVencUp.set(Calendar.DAY_OF_MONTH, 1);
					stmt = conn.prepareStatement(update);
					stmt.setDate(1, new java.sql.Date(dataVencUp.getTime().getTime()));
					stmt.setLong(2, Long.valueOf(CodEmp));
					stmt.setLong(3, Long.valueOf(CodFil));
					stmt.setLong(4, Long.valueOf(CodFor));
					stmt.setLong(5, Long.valueOf(NumNfc));
					stmt.setString(6, CodSnf);
					stmt.setLong(7, codParcela);
					
					stmt.executeUpdate();
				    }
				}
			    }
			    
			}catch (Exception e){
			    e.printStackTrace();
			}finally{
			    OrsegupsUtils.closeConnection(conn, stmt, rs);
			}
			
			
			System.out.println("Finalizando o processo de alteração da Observação da da Nota Fiscal de Entrada e Parcela");

			Boolean fecharNotaFiscal = (Boolean) processEntity.findValue("fecharNotaFiscal");
			if (fecharNotaFiscal)
			{
				sidxml = "";
				sidxml = "<sidxml>";
				sidxml = sidxml + "<sid acao='SID.Nfc.Fechar'>";
				sidxml = sidxml + "<param nome='codfor' valor='" + CodFor + "' />";
				sidxml = sidxml + "<param nome='numnfc' valor='" + NumNfc + "' />";
				sidxml = sidxml + "<param nome='codsnf' valor='" + CodSnf + "' />";
				sidxml = sidxml + "<param nome='gernfe' valor='" + 0 + "' />";
				sidxml = sidxml + "</sid>";
				sidxml = sidxml + "</sidxml>";

				mensagem = executaSidUrl(urlSID, PROXACAO, sidxml);/* , "", 0, 0 */
			}

			if (!mensagem.equals("OK"))
			{
				throw new WorkflowException(mensagem);
			}

			System.out.println("Processo de Inserção de Nota Fiscal de Entrada Finalizado com Sucesso");
		}
		catch (WorkflowException e)
		{
			e.printStackTrace();
			throw new WorkflowException(mensagem);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Erro inesperado. Por favor, contate o Departamento de TI!");
		}
	}

	public static String executaSidUrl(String urlSid, String proxacao, String sidxml)
	{
		StringBuffer inputLine = null;
		String mensagem = "";

		urlSid = urlSid + proxacao;
		inputLine = callSapiensSIDViaPOST(urlSid, sidxml);
		mensagem = inputLine.toString();

		return mensagem;

	}

	public static StringBuffer callSapiensSIDViaPOST(String url, String xml)
	{
		/*
		 * Connection connBD = null;
		 * StringBuilder sqlConfereInsertSID = new StringBuilder();
		 * PreparedStatement pstm = null;
		 * ResultSet rs = null;
		 */

		System.out.println("Iniciando o processo de chamada do SID");

		String errors = "";
		StringBuffer inputLine = new StringBuffer();
		url = url.replaceAll(" ", "%20");
		try
		{
			URL murl = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) murl.openConnection();

			//add cabeçalho request
			conn.setRequestMethod("POST");
			conn.setRequestProperty("User-Agent", "Mozilla/5.0");
			conn.setRequestProperty("Accept-Language", "pt-BR,en;q=0.5");

			String urlParameters = "xml=" + xml;
			System.out.println("XML para chamada do SID: " + urlParameters);

			// Send post request
			conn.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			char letter;

			while ((letter = (char) in.read()) != '\uFFFF')
			{
				inputLine.append(letter);
			}

			in.close();
			System.out.println("Finalizando o processo de chamada do SID com o retorno: " + inputLine.toString());
		}
		catch (Exception e)
		{
			/*
			 * try
			 * {
			 * if (qtdCCProduto >= 1200 || qtdCCServico >= 1200)
			 * {
			 * connBD = PersistEngine.getConnection("SAPIENS");
			 * sqlConfereInsertSID.append(consultaSID);
			 * String a = "";
			 * pstm = connBD.prepareStatement(sqlConfereInsertSID.toString());
			 * rs = pstm.executeQuery();
			 * int qtdCC = rs.getInt(1);
			 * OrsegupsUtils.closeConnection(connBD, pstm, rs);
			 * if(qtdCC == qtdCCProduto || qtdCC == qtdCCServico)
			 * {
			 * String teste = "OK";
			 * }
			 * else
			 * {
			 * inputLine.append("ERRO: Problema de conexão com o Serviço Sapiens SID" +
			 * e.getStackTrace());
			 * e.printStackTrace();
			 * errors = "Problema de conexão com o Serviço Sapiens SID! Erro: " + inputLine +
			 * e.getStackTrace() + e.getMessage() + e.getCause();
			 * log.error(errors);
			 * throw new WorkflowException(errors);
			 * }
			 * }
			 * }
			 * catch (SQLException e1)
			 * {
			 * // TODO Auto-generated catch block
			 * e1.printStackTrace();
			 * }
			 */
			inputLine.append("ERRO: Problema de conexão com o Serviço Sapiens SID" + e.getStackTrace());
			e.printStackTrace();
			errors = "Problema de conexão com o Serviço Sapiens SID! Erro: " + inputLine + e.getStackTrace() + e.getMessage() + e.getCause();
			log.error(errors);
			throw new WorkflowException(errors);
		}
		return inputLine;
	}

	public static void alteraObservacaoNotaFiscalEParcela(List<String> lstItensProduto, List<String> lstItensServico, List<String> lstItensParcela)
	{
		Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
		PreparedStatement st = null;

		try
		{
			connection.setAutoCommit(false);

			if (lstItensProduto != null && !lstItensProduto.isEmpty())
			{
				for (String itensProduto : lstItensProduto)
				{
					String[] arrayProduto = itensProduto.split(";");
					String update = "UPDATE E440IPC SET CPLIPC ='" + arrayProduto[6] + "' WHERE CODEMP = " + arrayProduto[0] + " AND CODFIL = " + arrayProduto[1] + " AND CODFOR = " + arrayProduto[2] + " AND NUMNFC = " + arrayProduto[3] + " AND CODSNF = '" + arrayProduto[4] + "' AND SEQIPC = " + arrayProduto[5];

					st = connection.prepareStatement(update);
					st.executeUpdate();
				}
			}

			if (lstItensServico != null && !lstItensServico.isEmpty())
			{
				for (String itensServico : lstItensServico)
				{
					String[] arrayServico = itensServico.split(";");
					String update = "UPDATE E440ISC SET CPLISC ='" + arrayServico[6] + "' WHERE CODEMP = " + arrayServico[0] + " AND CODFIL = " + arrayServico[1] + " AND CODFOR = " + arrayServico[2] + " AND NUMNFC = " + arrayServico[3] + " AND CODSNF = '" + arrayServico[4] + "' AND SEQISC = " + arrayServico[5];

					st = connection.prepareStatement(update);
					st.executeUpdate();
				}
			}

			if (lstItensParcela != null && !lstItensParcela.isEmpty())
			{
				for (String itensParcela : lstItensParcela)
				{
					String[] arrayParcela = itensParcela.split(";");
					String update = "UPDATE E440PAR SET OBSPAR ='" + arrayParcela[6] + "' WHERE CODEMP = " + arrayParcela[0] + " AND CODFIL = " + arrayParcela[1] + " AND CODFOR = " + arrayParcela[2] + " AND NUMNFC = " + arrayParcela[3] + " AND CODSNF = '" + arrayParcela[4] + "' AND NUMTIT = '" + arrayParcela[5] + "'";

					st = connection.prepareStatement(update);
					st.executeUpdate();
				}
			}

			connection.commit();
			connection.setAutoCommit(true);
		}
		catch (Exception e)
		{
		    e.printStackTrace();
			try
			{
				connection.rollback();
			}
			catch (SQLException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (e instanceof WorkflowException)
			{
				throw (WorkflowException) e;
			}
			else
			{
				throw new WorkflowException("Erro!" + e);
			}
		}
		finally
		{
			if (st != null)
			{
				try
				{
					st.close();
					connection.close();
				}
				catch (SQLException e)
				{
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}
}
