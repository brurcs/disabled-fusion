package com.neomind.fusion.custom.orsegups.justificativaAfastamento.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.Days;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class JAHistoricoAfastados implements CustomJobAdapter {

    final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.justificativaAfastamento.adapter.JAHistoricoAfastados");
   
    @Override
    public void execute(CustomJobContext ctx) {
	
	QLGroupFilter groupFilter = new QLGroupFilter("AND");
	groupFilter.addFilter(new QLEqualsFilter("job", "Abrir tarefa simples afastamentos"));
	NeoObject job = PersistEngine.getObject(AdapterUtils.getEntityClass("ultimaExecucaoJob"), groupFilter);
	GregorianCalendar dataUltimaExecucao = null;
	EntityWrapper wJob = null;
	if (job != null){
	    wJob = new EntityWrapper(job);

	    dataUltimaExecucao = (GregorianCalendar) wJob.findField("dataUltimaExec").getValue();
	}
	
	DateTime datUlt = new DateTime(dataUltimaExecucao);
	DateTime datAtu = new DateTime(Calendar.getInstance());
	if (dataUltimaExecucao != null && Days.daysBetween(datUlt, datAtu).getDays() == 15){
	    
	    Long key = GregorianCalendar.getInstance().getTimeInMillis();
	    
	    log.warn("INICIAR AGENDADOR DE TAREFA: JAHistoricoAfastados -- Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	    Connection conn = null;
	    PreparedStatement pstm = null;
	    ResultSet rs = null;
	    
	    Connection connH = null;
	    PreparedStatement pstmH = null;
	    ResultSet rsH = null;
	    
	    StringBuilder sql = new StringBuilder();
	    
	    try {
		
		conn = PersistEngine.getConnection("VETORH");
		sql.append(" SELECT * FROM ( SELECT top 50 ");
		sql.append(" AFA.NumEmp,  ");
		sql.append(" AFA.NumCad,  ");
		sql.append(" FUN.nomfun, ");
		sql.append(" CAR.TitRed as CARGO, ");
		sql.append(" fun.numcpf,  ");
		sql.append(" FUN.datnas,  ");
		sql.append(" FUN.numpis,  ");
		sql.append(" FUN.datadm,  ");
		sql.append(" AFA.DatAfa  ");
		sql.append(" FROM R038AFA AFA WITH(NOLOCK) "); 
		sql.append(" INNER JOIN R034FUN FUN WITH(NOLOCK) ON FUN.numcad = AFA.NumCad AND FUN.numemp = AFA.NumEmp AND AFA.TipCol = FUN.tipcol ");
		sql.append(" INNER JOIN R038HLO HLO WITH(NOLOCK) ON HLO.NumCad = FUN.numcad AND HLO.NumEmp = FUN.numemp AND HLO.TipCol = FUN.tipcol AND HLO.DatAlt = "); 
		sql.append(" (SELECT MAX(DATALT) FROM R038HLO HLO2 WITH(NOLOCK) WHERE HLO2.numcad = HLO.NumCad AND HLO2.numemp = HLO.NumEmp AND HLO2.tipcol = HLO.TipCol) ");
		sql.append(" INNER JOIN r016orn ORN WITH(NOLOCK) ON ORN.numloc = HLO.NumLoc AND ORN.taborg = HLO.TabOrg ");
		sql.append(" INNER JOIN R038HCA HCA WITH(NOLOCK) ON HCA.NumCad = FUN.numcad AND HCA.NumEmp = FUN.numemp AND HCA.TipCol = FUN.tipcol AND HCA.DatAlt = ");  
		sql.append(" (SELECT MAX(DATALT) FROM R038HCA HCA2 WITH(NOLOCK) WHERE HCA2.numcad = HCA.NumCad AND HCA2.numemp = HCA.NumEmp AND HCA2.tipcol = HCA.TipCol) ");
		sql.append(" INNER JOIN R024CAR CAR WITH(NOLOCK) ON CAR.CodCar = HCA.CodCar AND CAR.EstCar=1 ");
		sql.append(" WHERE AFA.SitAfa in (3,4) ");
		sql.append(" AND AFA.DATAFA = (SELECT MAX(AFA2.DATAFA) FROM R038AFA AFA2 WITH(NOLOCK) WHERE AFA2.NumEmp = AFA.NUMEMP AND AFA2.TipCol = AFA.TipCol AND AFA2.NumCad = AFA.NumCad) "); 
		sql.append(" AND AFA.DatAfa <= ?  ");
		sql.append(" AND FUN.numcpf not in (select hisAfa.cpf from [CACUPE\\SQL02].Fusion_Producao.dbo.D_historicoAfastados hisAfa WITH(NOLOCK) where hisAfa.dataAfastamento = AFA.DatAfa) order by AFA.DatAfa desc, AFA.NumEmp) AS dados ");
		sql.append(" ORDER BY dados.NumEmp ");
		
		pstm = conn.prepareStatement(sql.toString());
		
		GregorianCalendar data = new GregorianCalendar();
		data.set(Calendar.DAY_OF_MONTH, 31);
		data.set(Calendar.MONTH, 11);
		data.set(Calendar.YEAR, data.get(Calendar.YEAR)-2);
		
		pstm.setTimestamp(1, new Timestamp(data.getTimeInMillis()));
		
		rs = pstm.executeQuery();
		
		while (rs.next()) {
		    Long idEmpresa = rs.getLong("NumEmp");
		    Long numCad = rs.getLong("NumCad");
		    String nomeFun = rs.getString("nomfun");
		    String cargo = rs.getString("CARGO");
		    Long cpf = rs.getLong("numcpf");
		    GregorianCalendar dataNas = new GregorianCalendar();
		    dataNas.setTime(rs.getDate("datnas"));
		    Long numPis = rs.getLong("numpis");
		    GregorianCalendar datadm = new GregorianCalendar();
		    datadm.setTime(rs.getDate("datadm"));
		    GregorianCalendar DatAfa = new GregorianCalendar();
		    DatAfa.setTime(rs.getDate("DatAfa"));
		    
		    GregorianCalendar prazo = new GregorianCalendar();
		    prazo.add(Calendar.DAY_OF_MONTH, 15);
		    while(!OrsegupsUtils.isWorkDay(prazo)){
			prazo.add(Calendar.DAY_OF_MONTH, 1);
		    }
		    prazo.set(Calendar.HOUR, 23);
		    prazo.set(Calendar.MINUTE, 59);
		    prazo.set(Calendar.SECOND, 59);
		    String titulo = "Analisar Afastamento do Colaborador - "+idEmpresa+"/"+numCad+" - "+nomeFun;
		    String descricao = "Prezada Gestora do RH, por favor analisar o afastamento do colaborador(a) com situação de Auxílio Doença e Acidente de Trabalho na Previdência Social, seguem as informações: <br/>";
		    
		    descricao = descricao + "<table class=\"gridbox\" cellpadding=\"0\" cellspacing=\"0\" style=\"empty-cells: show\">";
		    descricao = descricao + "<thead>";
		    descricao = descricao + "<tr><th colspan=9>Dados do colaborador</th></tr>";
		    descricao = descricao + "<tr>";
		    descricao = descricao + "<th>Empresa</th>";
		    descricao = descricao + "<th>Matrícula</th>";
		    descricao = descricao + "<th>Nome</th>";
		    descricao = descricao + "<th>Função ou Cargo</th>";
		    descricao = descricao + "<th>CPF</th>";
		    descricao = descricao + "<th>Dt. Nascimento</th>";
		    descricao = descricao + "<th>Núm. PIS</th>";
		    descricao = descricao + "<th>Dt. de Admissão</th>";
		    descricao = descricao + "<th>Dt. Afastamento</th>";
		    descricao = descricao + "</tr>";
		    descricao = descricao + "</thead>";
		    descricao = descricao + "<tbody>";
		    descricao = descricao + "<tr>";
		    descricao = descricao + "<td>" + idEmpresa + "</td>";
		    descricao = descricao + "<td>" + numCad + "</td>";
		    descricao = descricao + "<td>" + nomeFun + "</td>";
		    descricao = descricao + "<td>" + cargo + "</td>";
		    descricao = descricao + "<td>" + cpf + "</td>";
		    descricao = descricao + "<td>" + NeoDateUtils.safeDateFormat(dataNas, "dd/MM/yyyy") + "</td>";
		    descricao = descricao + "<td>" + numPis + "</td>";
		    descricao = descricao + "<td>" + NeoDateUtils.safeDateFormat(datadm, "dd/MM/yyyy") + "</td>";
		    descricao = descricao + "<td>" + NeoDateUtils.safeDateFormat(DatAfa, "dd/MM/yyyy") + "</td>";
		    descricao = descricao + "</tr>";
		    
		    descricao = descricao + "</tbody>";
		    descricao = descricao + "</table><br/>";
		    
		    StringBuilder sqlH = new StringBuilder();
		    
		    connH = PersistEngine.getConnection("");
		    
		    sqlH.append(" SELECT top 6 P.neoId, P.code, p.startDate FROM WFProcess P WITH(NOLOCK) ");
		    sqlH.append(" INNER JOIN D_Tarefa T WITH(NOLOCK) ON T.wfprocess_neoId = P.neoId ");
		    sqlH.append(" INNER JOIN D_historicoAfastados H WITH(NOLOCK) ON H.tarefa = P.code ");
		    sqlH.append(" where  H.matricula = ? and H.empresa = ? ");
		    sqlH.append(" order by H.dataAfastamento ");
		    
		    pstmH = connH.prepareStatement(sqlH.toString());
		    
		    pstmH.setLong(1, numCad);
		    pstmH.setLong(2, idEmpresa);
		    
		    rsH = pstmH.executeQuery();
		    
		    descricao = descricao + "<table class=\"gridbox\" cellpadding=\"0\" cellspacing=\"0\" style=\"empty-cells: show\">";
		    descricao = descricao + "<thead>";
		    descricao = descricao + "<tr><th colspan=2>Histórico tarefas</th></tr>";
		    descricao = descricao + "<tr>";
		    descricao = descricao + "<th>Tarefa</th>";
		    descricao = descricao + "<th>Data</th>";
		    descricao = descricao + "</tr>";
		    descricao = descricao + "</thead>";
		    descricao = descricao + "<tbody>";
		    while (rsH.next()) {
			Long idProcess = rsH.getLong("neoId");
			Long code = rsH.getLong("code");
			descricao += "<tr><td><a href=\"https://intranet.orsegups.com.br/fusion/bpm/workflow_search.jsp?id="+idProcess+"\" target=\"_blank\"><img title=\"Pesquisar\" src=\"http://dsoo14:8080/fusion/imagens/icones_final/visualize_blue_16x16-trans.png\" align=\"absmiddle\"> "+code+"</a></td>";
			GregorianCalendar datSta = new GregorianCalendar();
			datSta.setTime(rsH.getDate("startDate"));
			descricao += "<td>"+NeoDateUtils.safeDateFormat(datSta, "dd/MM/yyyy")+"</td></tr>";
		    }
		    descricao = descricao + "</tbody>";
		    descricao = descricao + "</table><br/>";
		    
//		descricao += "Empresa: "+idEmpresa+" / Matrícula: "+numCad+" / Nome: "+nomeFun+" / Função ou Cargo: "+cargo+" / CPF: "+cpf+" / Dt. Nascimento: "+NeoDateUtils.safeDateFormat(dataNas, "dd/MM/yyyy");
//		descricao += " / Núm. PIS: "+numPis+" / Dt. de Admissão: "+NeoDateUtils.safeDateFormat(datadm, "dd/MM/yyyy")+" / Dt. Afastamento: "+NeoDateUtils.safeDateFormat(DatAfa, "dd/MM/yyyy");
		    String solicitante = null;
		    NeoPaper papelSolicitante = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "solicitanteAfatados"));
		    if (papelSolicitante != null){
			Set<NeoUser> users = papelSolicitante.getUsers();
			if (users != null){
			    for (NeoUser neoUser : users) {
				solicitante = neoUser.getCode();
			    }
			}
		    }
		    String executor = null;
		    NeoPaper papelExecutor = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "executorAfastados"));
		    if (papelExecutor != null){
			Set<NeoUser> users = papelExecutor.getUsers();
			if (users != null){
			    for (NeoUser neoUser : users) {
				executor = neoUser.getCode();
			    }
			}
		    }
		    
		    IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		    
		    String codigoTarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "", prazo);
		    
		    if (codigoTarefa != null && !codigoTarefa.contains("Erro")){
			InstantiableEntityInfo hisAfa = AdapterUtils.getInstantiableEntityInfo("historicoAfastados");
			NeoObject objHisAfa = hisAfa.createNewInstance();
			EntityWrapper wrapperHisAfa = new EntityWrapper(objHisAfa);
			
			wrapperHisAfa.findField("empresa").setValue(idEmpresa);
			wrapperHisAfa.findField("cpf").setValue(cpf);
			wrapperHisAfa.findField("tarefa").setValue(codigoTarefa);
			wrapperHisAfa.findField("nome").setValue(nomeFun);
			wrapperHisAfa.findField("matricula").setValue(numCad);
			wrapperHisAfa.findField("dataAFastamento").setValue(DatAfa);
			PersistEngine.persist(objHisAfa);
			
		    }
		}
		if (wJob != null){
		    GregorianCalendar dataU = new GregorianCalendar();
		    dataU.setTime(new Date());
		    wJob.findField("dataUltimaExec").setValue(dataU);
		    PersistEngine.persist(job);
		}
	    } catch (Exception e) {
		e.printStackTrace();
		log.error("ERRO AGENDADOR DE TAREFA: RotinaHistoricoAfastados - ERRO AO EXECUTAR ROTINA -- Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	    } finally {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    }
	}
    }
    
}
