package com.neomind.fusion.custom.orsegups.mobile.vo;

/**
 * Created by carla.regina on 06/09/2014.
 */
public class Resposta
{
	private Integer id;
	
	private String neoIdPergunta;

	private String pergunta;

	private String resposta;

	private String observacao;
	
	private Integer pergunta_id;

	private Integer assunto_id;

	private Integer pesquisa_id;

	private String imagemInspecao;
	
	public String getImagemInspecao()
	{
		return this.imagemInspecao;
	}

	public void setImagemInspecao(String imagemInspecao)
	{
		this.imagemInspecao = imagemInspecao;
	}
	

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getPergunta()
	{
		return pergunta;
	}

	public void setPergunta(String pergunta)
	{
		this.pergunta = pergunta;
	}

	public String getResposta()
	{
		return resposta;
	}

	public void setResposta(String resposta)
	{
		this.resposta = resposta;
	}

	public String getObservacao()
	{
		return observacao;
	}

	public void setObservacao(String observacao)
	{
		this.observacao = observacao;
	}

	public String getNeoIdPergunta()
	{
		return neoIdPergunta;
	}

	public void setNeoIdPergunta(String neoIdPergunta)
	{
		this.neoIdPergunta = neoIdPergunta;
	}

	public Integer getPergunta_id() {
		return pergunta_id;
	}

	public void setPergunta_id(Integer pergunta_id) {
		this.pergunta_id = pergunta_id;
	}

	public Integer getAssunto_id() {
		return assunto_id;
	}

	public void setAssunto_id(Integer assunto_id) {
		this.assunto_id = assunto_id;
	}

	public Integer getPesquisa_id() {
		return pesquisa_id;
	}

	public void setPesquisa_id(Integer pesquisa_id) {
		this.pesquisa_id = pesquisa_id;
	}
	
	
}