package com.neomind.fusion.custom.orsegups.callcenter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.datawarehouse.ExternalEntityInfo;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

@WebServlet(name = "CallCenterServlet", urlPatterns = { "/servlet/com.neomind.fusion.custom.orsegups.callcenter.CallCenterServlet" }, asyncSupported = true)
public class CallCenterServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	private static final Log log = LogFactory.getLog(CallCenterServlet.class);

	@Override
	protected synchronized void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		this.doRequest(request, response);
	}

	@Override
	protected synchronized void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		this.doRequest(request, response);
	}

	private void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/plain");
		response.setCharacterEncoding("ISO-8859-1");
		Gson gson = new Gson();
		String action = null;
		final PrintWriter out = response.getWriter();

		if (request.getParameter("action") != null)
		{
			action = request.getParameter("action");
		}

		if (action.equalsIgnoreCase("callBack"))
		{
			this.callBack(request, response, out);
		}
		if (action.equalsIgnoreCase("buscarLigacaoRecebida"))
		{
			String callBack = request.getParameter("callBack");
			String listaClientesJson = gson.toJson(this.buscarLigacaoRecebida(callBack));
			out.print(listaClientesJson);
			out.flush();
			out.close();
		}
		if (action.equalsIgnoreCase("atualizaLigacaoRecebida"))
		{
			this.atualizaLigacaoRecebida(request, response, out);
		}
		if (action.equalsIgnoreCase("atualizaStatusAtendimento"))
		{
			String emLigacaoJson = gson.toJson(this.atualizaStatusAtendimento(request, response, out));
			out.print(emLigacaoJson);
			out.flush();
			out.close();
		}

		if (action.equalsIgnoreCase("recuperarSenha"))
		{
			recuperarSenha(request, response, out);
			out.flush();
			out.close();
		}

		if (action.equalsIgnoreCase("getSigmaOSData"))
		{
			getSigmaOSData(request, response, out);
			out.flush();
			out.close();
		}

		if (action.equalsIgnoreCase("recuperaSenhaSite"))
		{
			recuperaSenhaSite(request, response, out);
			out.flush();
			out.close();
		}

	}

	private void callBack(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws IOException
	{
		String origem = request.getParameter("origem");
		String fone = request.getParameter("fone");
		String retorno = "99";

		try
		{

			if (NeoUtils.safeIsNotNull(fone) && validarTelefone(fone) && NeoUtils.safeIsNotNull(origem))
			{

				retorno = this.inserirLigacaoRecebida(fone, origem);

			}
			else
			{
				log.error("29- Telefone nulo ou inválido!");
				retorno = "29";

			}
		}
		catch (Exception e)
		{
	
			e.printStackTrace();
			log.error("99-Erro" + e.getMessage());
		}

		out.print("{\"return\":\""+retorno+"\"}");
		out.flush();
		out.close();
	}

	private Collection<CallCenterClienteRetornoVO> buscarLigacaoRecebida(String call)
	{

		Collection<CallCenterClienteRetornoVO> clientesList = null;
		try
		{
			QLGroupFilter filter = new QLGroupFilter("OR");
			filter.addFilter(new QLEqualsFilter("atendimentoEfetuado", false));
			QLGroupFilter filterDate = new QLGroupFilter("AND");
			GregorianCalendar dataLigacaoFilter = new GregorianCalendar();
			dataLigacaoFilter.add(GregorianCalendar.HOUR_OF_DAY, -12);
			QLOpFilter filtroData = new QLOpFilter("dataLigacao", ">=", (GregorianCalendar) dataLigacaoFilter);
			filterDate.addFilter(new QLEqualsFilter("atendimentoEfetuado", true));
			QLGroupFilter filterCall = new QLGroupFilter("AND");
			if (NeoUtils.safeIsNotNull(call) && !call.isEmpty() && (call.equals("CallBack") || call.equals("Abandono")))
				filterCall.addFilter(new QLOpFilter("origem", "LIKE", "%" + call + "%"));

			filterDate.addFilter(filtroData);
			filter.addFilter(filterDate);
			filterCall.addFilter(filter);

			// View de Contas
			Collection<NeoObject> clientes = null;

			clientes = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CERECCallCenterRetornarLigacao"), filterCall, -1, -1, "atendimentoEfetuado,dataLigacao desc");
			PersistEngine.getEntityManager().flush();
			if (clientes != null && !clientes.isEmpty())
			{
				clientesList = new ArrayList<CallCenterClienteRetornoVO>();
				CallCenterClienteRetornoVO clienteRetornoVO = null;
				for (NeoObject neoObject : clientes)
				{
					clienteRetornoVO = new CallCenterClienteRetornoVO();
					EntityWrapper clienteOrigem = new EntityWrapper(neoObject);

					String descricao = (String) clienteOrigem.getValue("descricao2");
					String origem = (String) clienteOrigem.getValue("origem");
					String telefone = (String) clienteOrigem.getValue("telefoneCliente");
					GregorianCalendar dataLigacao = (GregorianCalendar) clienteOrigem.getValue("dataLigacao");
					Boolean retornouCliente = (Boolean) clienteOrigem.getValue("retornouCliente");
					GregorianCalendar dataRetorno = (GregorianCalendar) clienteOrigem.getValue("dataRetorno");
					Boolean atendimentoEfetuado = (Boolean) clienteOrigem.getValue("atendimentoEfetuado");
					Boolean clienteSapiensSigma = (Boolean) clienteOrigem.getValue("clienteSapiensSigma");
					Long id = (Long) clienteOrigem.getValue("neoId");
					Boolean emAtendimento = (Boolean) clienteOrigem.getValue("emAtendimento");
					String nomeUsuario = (String) clienteOrigem.getValue("nomeUsuario");
					GregorianCalendar dataPrimeiraTentativa = (GregorianCalendar) clienteOrigem.getValue("dataPrimeiraLigacao");

					clienteRetornoVO.setOrigem(origem);

					telefone = telefone.replace("(", "").replace(")", "").replace("-", "").trim();
					telefone = telefone.replace(" ", "");

					clienteRetornoVO.setTelefone("<a class=\"easyui-linkbutton\" data-options=\"plain:true,iconCls:'icon-back'\" href='javascript:dial(\"" + telefone + "\");'>" + telefone + "</a>");
					clienteRetornoVO.setDataLigacao(NeoUtils.safeDateFormat(dataLigacao, "dd/MM/yyyy HH:mm:ss"));
					clienteRetornoVO.setRetornouCliente(retornouCliente);
					clienteRetornoVO.setDataRetorno(NeoUtils.safeDateFormat(dataRetorno, "dd/MM/yyyy HH:mm:ss"));
					clienteRetornoVO.setAtendimentoEfetuado(atendimentoEfetuado);
					clienteRetornoVO.setCliente(clienteSapiensSigma);
					clienteRetornoVO.setId(id);
					clienteRetornoVO.setEmAtendimento(emAtendimento);
					clienteRetornoVO.setUsuario(nomeUsuario);
					clienteRetornoVO.setDataPrimeiraLigacao(NeoUtils.safeDateFormat(dataPrimeiraTentativa, "dd/MM/yyyy HH:mm:ss"));

					if (NeoUtils.safeIsNotNull(descricao) && !descricao.isEmpty())
					{
						descricao = descricao.replaceAll("\n", "<br>");
						descricao = descricao.replaceAll("---", "<br><li>");
						clienteRetornoVO.setDescricao(descricao);
					}
					else
					{
						clienteRetornoVO.setDescricao("Vazio.");
					}

					clientesList.add(clienteRetornoVO);
				}

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();

		}
		return clientesList;

	}

	private String inserirLigacaoRecebida(String telefone, String call)
	{
		String retorno = "";
		String origem = "Abandono";
		List<NeoObject> clientes = null;
		try
		{
			if (NeoUtils.safeIsNotNull(telefone))
			{
				QLGroupFilter filterAnd = new QLGroupFilter("AND");
				filterAnd.addFilter(new QLEqualsFilter("telefoneCliente", telefone));
				filterAnd.addFilter(new QLEqualsFilter("atendimentoEfetuado", false));
				clientes = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CERECCallCenterRetornarLigacao"), filterAnd);
				PersistEngine.getEntityManager().flush();

				if (clientes != null && clientes.isEmpty())
				{
					// InstantiableEntityInfo cERECCallCenterLigacaoCliente =
					// AdapterUtils.getInstantiableEntityInfo("CERECCallCenterRetornarLigacao");
					NeoObject noPS = AdapterUtils.createNewEntityInstance("CERECCallCenterRetornarLigacao");

					EntityWrapper psWrapper = new EntityWrapper(noPS);
					if (NeoUtils.safeIsNotNull(call) && call.equals("1"))
						origem = "CallBack";

					psWrapper.findField("origem").setValue(origem);
					psWrapper.findField("telefoneCliente").setValue(telefone);
					psWrapper.findField("dataLigacao").setValue(new GregorianCalendar());
					// psWrapper.findField("callBack").setValue(callBack);
					// psWrapper.findField("descricao").setValue(descricao);
					psWrapper.findField("retornouCliente").setValue(Boolean.FALSE);
					psWrapper.findField("atendimentoEfetuado").setValue(Boolean.FALSE);
					psWrapper.findField("emAtendimento").setValue(Boolean.FALSE);
					Boolean cliente = verificarTelefoneCliente(telefone);
					psWrapper.findField("clienteSapiensSigma").setValue(cliente);
					PersistEngine.persist(noPS);
					// PersistEngine.getEntityManager().flush();
					retorno = "OK";
				}
				else
				{
					retorno = "9";
					log.error("9-EXISTE UMA LIGAÇÃO ANTERIOR A ESSA COM O NUMERO, " + telefone + ", AGUARDANDO RETORNO.");
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			retorno = "99";
			log.error("99-ERRO CallCenterServlet AO INSERIR setLigacaoRecebida : " + e.getMessage());
		}
		return retorno;
	}

	// METODO
	private void atualizaLigacaoRecebida(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{
		try
		{
			String descricao = request.getParameter("descricao");
			String telefone = request.getParameter("telefone");
			String encerrar = request.getParameter("encerrar");
			String idStr = request.getParameter("id");
			String nomeStr = request.getParameter("nomeUsuario");
			Long id = Long.parseLong(idStr);
			Gson gson = new Gson();

			if (NeoUtils.safeIsNotNull(telefone) && NeoUtils.safeIsNotNull(id))
			{

				QLGroupFilter filter = new QLGroupFilter("AND");
				filter.addFilter(new QLEqualsFilter("neoId", id));
				// filter.addFilter(new QLEqualsFilter("telefoneCliente",
				// telefone));
				// filter.addFilter(new QLEqualsFilter("emAtendimento",
				// Boolean.TRUE));
				List<NeoObject> clientes = null;
				clientes = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CERECCallCenterRetornarLigacao"), filter, -1, -1, " dataLigacao desc");
				PersistEngine.getEntityManager().flush();

				if (clientes != null && !clientes.isEmpty())
				{
					NeoObject neoObject = (NeoObject) clientes.get(0);

					EntityWrapper clienteWrapper = new EntityWrapper(neoObject);

					clienteWrapper.findField("retornouCliente").setValue(Boolean.TRUE);

					clienteWrapper.findField("emAtendimento").setValue(Boolean.FALSE);

					// clienteWrapper.findField("dataRetorno").setValue(new
					// GregorianCalendar());

					clienteWrapper.findField("nomeUsuario").setValue(" ");

					String desc = "";
					if (NeoUtils.safeIsNotNull(clienteWrapper.findValue("descricao2")))
						desc = (String) clienteWrapper.findValue("descricao2");
					descricao = NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss") + " " + descricao;
					clienteWrapper.findField("descricao2").setValue("--- " + descricao + "\r\n" + desc);

					if (encerrar.equals("1"))
						clienteWrapper.findField("atendimentoEfetuado").setValue(Boolean.TRUE);

					PersistEngine.persist(neoObject);
					// PersistEngine.commit(true);

				}

			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("ERRO CallCenterServlet AO ALTERAR setLigacaoRecebida : " + e.getMessage());

		}

	}

	private Boolean verificarTelefoneCliente(String telefone)
	{
		if (telefone != null)
		{
			// busca pelo telefone
			if (telefone != null && !telefone.trim().isEmpty())
			{
				Class clazz = AdapterUtils.getEntityClass("SAPIENSUSUT500FNE");
				Collection<NeoObject> codigosTelefone = PersistEngine.getObjects(clazz, new QLEqualsFilter("usu_numfne", telefone));

				if (codigosTelefone != null && !codigosTelefone.isEmpty())
				{
					return true;
				}
				
				String telefoneRequest2 = "";
				
				if (telefone.length()>10){
				    String subFone = telefone.substring(3);
				    
				    if(subFone.startsWith("7") || subFone.startsWith("8") || subFone.startsWith("9")){
					telefoneRequest2 = telefone.substring(0, 2)+subFone;
				    }				   				    
				}
				
				codigosTelefone = PersistEngine.getObjects(clazz, new QLEqualsFilter("usu_numfne", telefoneRequest2));

				if (codigosTelefone != null && !codigosTelefone.isEmpty())
				{
					return true;
				}
				
				
			}
		}
		return false;
	}

	private Boolean atualizaStatusAtendimento(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{
		synchronized (this)
		{
			try
			{

				String idStr = request.getParameter("id");
				String nomeStr = request.getParameter("nomeUsuario");
				String ramalOrigem = request.getParameter("ramalOrigem");
				String ramalDestino = request.getParameter("ramalDestino");

				Long id = Long.parseLong(idStr);
				Gson gson = new Gson();

				if (NeoUtils.safeIsNotNull(id))
				{
					QLGroupFilter filter = new QLGroupFilter("AND");
					filter.addFilter(new QLEqualsFilter("neoId", id));
					// filter.addFilter(new QLEqualsFilter("telefoneCliente",
					// telefone));
					// filter.addFilter(new QLEqualsFilter("emAtendimento",
					// Boolean.FALSE));
					Thread.sleep(100);
					// View de Contas
					List<NeoObject> clientes = null;
					clientes = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CERECCallCenterRetornarLigacao"), filter);

					if (clientes != null && !clientes.isEmpty())
					{
						NeoObject neoObject = (NeoObject) clientes.get(0);

						EntityWrapper clienteWrapper = new EntityWrapper(neoObject);
						Boolean flag = (Boolean) clienteWrapper.findValue("emAtendimento");
						String nome = (String) clienteWrapper.findValue("nomeUsuario");

						if (!flag || (flag && nome.equals(PortalUtil.getCurrentUser().getCode())))
						{

							clienteWrapper.findField("emAtendimento").setValue(Boolean.TRUE);
							clienteWrapper.findField("nomeUsuario").setValue(nomeStr);
							clienteWrapper.findField("dataRetorno").setValue(new GregorianCalendar());
							if (clienteWrapper.findValue("dataPrimeiraLigacao") == null)
							{
								clienteWrapper.findField("dataPrimeiraLigacao").setValue(new GregorianCalendar());
							}

							PersistEngine.persist(neoObject);
							// PersistEngine.commit(true);
							PersistEngine.getEntityManager().flush();
							
							/**
							 * Adição do novo digito para telefones móveis
							 */
							List<NeoObject> listaSubstituicao = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TINonoDigito"));

							if (listaSubstituicao != null && !listaSubstituicao.isEmpty()) {

							    NeoObject objAdicionar = listaSubstituicao.get(0);

							    EntityWrapper wAdicionar = new EntityWrapper(objAdicionar);
							    
							    boolean adicionar = (boolean) wAdicionar.findField("adicionar").getValue();
							    			    
							    if(adicionar){
								if (ramalDestino.startsWith("0")){
								    ramalDestino = ramalDestino.substring(1);
								}
								if (ramalDestino.length() == 8){						    
								    
								    if(ramalDestino.startsWith("7") || ramalDestino.startsWith("8") || ramalDestino.startsWith("9")){
									ramalDestino = "9"+ramalDestino;
								    }
						    
								}else if (ramalDestino.length() == 10){
								    String subFone = ramalDestino.substring(2);
								    
								    if(subFone.startsWith("7") || subFone.startsWith("8") || subFone.startsWith("9")){
									subFone = "9"+subFone;
									ramalDestino = ramalDestino.substring(0, 2)+subFone;
								    }			
								}
								
							    }
							}
							
							String url = "http://192.168.20.241/snep/services/?service=Dial&ramal=" + ramalOrigem + "&exten=" + ramalDestino;

							if (ramalDestino != null && !ramalDestino.isEmpty() && ramalOrigem != null && !ramalOrigem.isEmpty())
							{
								String message;

								if (ramalDestino.equals(ramalOrigem))
								{
									message = "Erro ao efetuar chamada.\nRamal de origem igual ao ramal de destino.";
								}
								else
								{
									StringBuffer buffer = this.callUrl(url);

									String result = buffer.toString();
									result = result.replaceAll("\\{", "");
									result = result.replaceAll("\\}", "");
									String[] params = result.split(",");

									if (params[0] != null && params[0].contains("ok"))
									{
										message = "Chamada efetuada a partir do ramal " + ramalOrigem + " para o ramal " + ramalDestino + ".";
									}
									else
									{
										String erro[] = params[1].split(":");

										message = "Erro ao efetuar chamada.\n" + erro[1];
									}
								}
							}

							return Boolean.TRUE;
						}
					}
				}

			}
			catch (Exception e)
			{
				e.printStackTrace();
				log.error("ERRO CallCenterServlet AO ALTERAR setLigacaoRecebida : " + e.getMessage());

			}

			return Boolean.FALSE;
		}
	}

	public static boolean validarTelefone(String tel)
	{

		if ((tel == null) || (tel.length() < 8))
			return false;
		else
			return true;
	}

	private StringBuffer callUrl(String url)
	{
		StringBuffer inputLine = new StringBuffer();

		try
		{
			URL murl = new URL(url);
			URLConnection murlc = murl.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(murlc.getInputStream()));

			char letter;

			while ((letter = (char) in.read()) != '\uFFFF')
			{
				inputLine.append(letter);
			}

			in.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return inputLine;
	}

	private void recuperarSenha(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{
		Connection conn = null;
		PreparedStatement st = null;
		try
		{
			String auxCgcCpf = onlyNumbers(request.getParameter("cgccpf"));
			Long cgcCpf = null;
			if ((auxCgcCpf == null) || (auxCgcCpf.trim().equals("")))
			{
				log.error("Erro CGCCPF inválido.");
				return;
			}
			try
			{
				cgcCpf = Long.valueOf(Long.parseLong(auxCgcCpf.trim()));
			}
			catch (NumberFormatException e)
			{
				e.printStackTrace();

				OrsegupsUtils.closeConnection(conn, st, null);
				return;
			}
			String senhaAtual = String.valueOf(cgcCpf);

			String modificada = "";
			if ((senhaAtual != null) && (senhaAtual.length() > 0))
			{
				modificada = senhaAtual.substring(senhaAtual.length() - 4, senhaAtual.length());
			}
			String novaSenha = modificada.trim();

			QLGroupFilter filterCliSen = new QLGroupFilter("AND");
			filterCliSen.addFilter(new QLEqualsFilter("usu_cgccpf", cgcCpf));

			ExternalEntityInfo infoViewLoginCliente = (ExternalEntityInfo) EntityRegister.getInstance().getCache().getByString("SAPIENSUSUTUSU");
			List<NeoObject> user = PersistEngine.getObjects(infoViewLoginCliente.getEntityClass(), filterCliSen);
			if ((novaSenha != null) && (!novaSenha.equals("")))
			{
				conn = PersistEngine.getConnection("SAPIENS");

				StringBuffer sqlUpd = new StringBuffer();
				sqlUpd.append("UPDATE USU_T230USU SET USU_SenUsu = ? WHERE USU_CgcCpf = ?");

				st = conn.prepareStatement(sqlUpd.toString());
				st.setString(1, novaSenha);
				st.setLong(2, cgcCpf.longValue());
				st.executeUpdate();

				st.close();
				conn.close();
			}
			ExternalEntityInfo infoCliente = (ExternalEntityInfo) EntityRegister.getInstance().getCache().getByString("SAPIENS_Clientes");
			List<NeoObject> cliente = new ArrayList();
			if (infoCliente != null)
			{
				cliente = PersistEngine.getObjects(infoCliente.getEntityClass(), new QLEqualsFilter("cgccpf", cgcCpf));
			}
			if (!cliente.isEmpty())
			{
				Map<String, Object> paramMap = new HashMap();

				EntityWrapper wrpCliente = new EntityWrapper((NeoObject) cliente.get(0));
				String fantasia = (String) wrpCliente.getValue("apecli");
				String mailCliente = (String) wrpCliente.getValue("intnet");
				String senha = "";
				ExternalEntityInfo infoUsu = (ExternalEntityInfo) EntityRegister.getInstance().getCache().getByString("SAPIENSUSUTUSU");
				List<NeoObject> listaUsu = PersistEngine.getObjects(infoUsu.getEntityClass(), new QLEqualsFilter("usu_cgccpf", cgcCpf));
				if (!listaUsu.isEmpty())
				{
					EntityWrapper usuarioCliente = new EntityWrapper((NeoObject) listaUsu.get(0));
					senha = (String) usuarioCliente.getValue("usu_senusu");
				}
				if ((cgcCpf != null) && (!cgcCpf.equals("")) && (senha != null) && (!senha.equals("")) && (mailCliente != null) && (!mailCliente.equals("")) && (fantasia != null) && (!fantasia.equals("")))
				{
					paramMap.put("cgccpf", cgcCpf);
					paramMap.put("senha", senha);
					paramMap.put("fantasia", fantasia);

					FusionRuntime.getInstance().getMailEngine().sendEMail(mailCliente, "/portal_orsegups/areaCliente2TemplateEmailSenhaCliente.jsp", paramMap);
				}
				else
				{
					log.error("Erro ao enviar E-MAIL informações inválidas .");
				}
			}
			else
			{
				log.error("Erro cliente não encontrado .");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, st, null);
		}
	}

	public static String onlyNumbers(String str)
	{
		String sAux = "";
		if (str == null)
		{
			str = "";
		}
		for (int i = 0; i < str.length(); i++)
		{
			if ((str.charAt(i) >= '0') && (str.charAt(i) <= '9'))
			{
				sAux = sAux + str.charAt(i);
			}
		}
		return sAux;
	}

	private void getSigmaOSData(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{
		String equip = request.getParameter("codigoCliente");
		if (equip != null && !equip.equals(""))
		{
			Long cdCliente = Long.valueOf(equip);

			List<NeoObject> contasSigma = PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMACENTRAL"), new QLEqualsFilter("cd_cliente", cdCliente));
			if (contasSigma != null && !contasSigma.isEmpty())
			{
				try
				{

					Connection connSapiens = null;
					PreparedStatement st3 = null;
					ResultSet rsEquip = null;
					connSapiens = PersistEngine.getConnection("SAPIENS");
					StringBuffer sqlEquip = new StringBuffer();
					sqlEquip.append(" SELECT cvs.USU_CodSer ");
					sqlEquip.append(" FROM USU_T160SIG sig ");
					sqlEquip.append(" INNER JOIN USU_T160CVS cvs ON cvs.usu_codemp = sig.usu_codemp AND cvs.usu_numctr = sig.usu_numctr AND cvs.usu_numpos = sig.usu_numpos ");
					sqlEquip.append(" WHERE sig.usu_codcli = " + cdCliente + " ");
					sqlEquip.append(" AND cvs.usu_codser IN ('9002035','9002011', '9002004', '9002005','9002014') ");
					sqlEquip.append(" ORDER BY cvs.usu_sitcvs ");
					st3 = connSapiens.prepareStatement(sqlEquip.toString());
					rsEquip = st3.executeQuery();

					////107655 - SOO - COM CAD - SUELLEN ROSENBROCK
					//108294 - Bruno Brasil
					Integer colEquip = 108294;
					String eq = PortalUtil.getBaseURL();
					String flag_equip = "<img src='" + eq + "imagens/icones/sphere_red_16x16-trans.png'/> Cliente";

					if (rsEquip.next())
					{
						//107655 - SOO - CM - ANA PAULA
						colEquip = 89428;

						// Equipamento Orsegups
						flag_equip = "<img src='" + eq + "imagens/icones/sphere_yellow_16x16-trans.png'/> Orsegups";
					}
					OrsegupsUtils.closeConnection(connSapiens, st3, rsEquip);
					out.print(flag_equip);
				}
				catch (Exception e)
				{
					e.printStackTrace();
					System.out.println("Erro CallCenterStartProcessServlet: " + e.getMessage());
					out.print("Não encontrado, erro ao efetuar pesquisa.");
				}
			}
			else
			{
				out.print("Conta sigma não encontrada.");
			}
		}
		else
		{
			out.print("Código cliente nulo ou vazio.");
		}
	}

	private void recuperaSenhaSite(HttpServletRequest request, HttpServletResponse response, PrintWriter out)
	{
		String cgcCpf = request.getParameter("cgcCpf");
		String senhaSite = "";
		if ((cgcCpf != null) && (!cgcCpf.isEmpty()))
		{
			cgcCpf = cgcCpf.trim();
			cgcCpf = cgcCpf.replaceAll(" ", "");
			cgcCpf = cgcCpf.replaceAll("\\s+", "");
			try
			{
				ExternalEntityInfo infoUsu = (ExternalEntityInfo) EntityRegister.getInstance().getCache().getByString("SAPIENSUSUTUSU");
				NeoObject usuSenha = (NeoObject) PersistEngine.getObject(infoUsu.getEntityClass(), new QLEqualsFilter("usu_cgccpf", Long.valueOf(Long.parseLong(cgcCpf))));
				if (usuSenha != null)
				{
					EntityWrapper usuarioCliente = new EntityWrapper(usuSenha);
					senhaSite = (String) usuarioCliente.getValue("usu_senusu");
					senhaSite = senhaSite + " <a href=\"javascript:void(0)\" title=\"Clique para redefinir senha padrão.\" onclick=\"alterarSenhaParaPadrao(" + cgcCpf + ")\"><img src='" + "http://intranet.orsegups.com.br" + "/fusion/imagens/icones/key_16x16-trans.png'/></a>";
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				out.print(senhaSite);
			}
		}
		else
		{
			out.print("CNPJ ou CPF INVÁLIDO.");
		}

	}

}
