package com.neomind.fusion.custom.orsegups.maps.call.vo;

public class FilaAlertAitVO
{
	private String posicao;
	private String ait;
	private String conta;
	private String fantasia;
	private String tempo;
	private String tempoFila;
	private Boolean emAtendimento;
	private String nomeAtendente;
	private String log;
	
	
	public FilaAlertAitVO()
	{
		
	}
	public Boolean getEmAtendimento()
	{
		return emAtendimento;
	}
	public void setEmAtendimento(Boolean emAtendimento)
	{
		this.emAtendimento = emAtendimento;
	}
	public String getTempoFila()
	{
		return tempoFila;
	}
	public void setTempoFila(String tempoFila)
	{
		this.tempoFila = tempoFila;
	}
	
	public String getPosicao()
	{
		return posicao;
	}
	public void setPosicao(String posicao)
	{
		this.posicao = posicao;
	}
	public String getAit()
	{
		return ait;
	}
	public void setAit(String ait)
	{
		this.ait = ait;
	}
	public String getConta()
	{
		return conta;
	}
	public void setConta(String conta)
	{
		this.conta = conta;
	}
	public String getFantasia()
	{
		return fantasia;
	}
	public void setFantasia(String fantasia)
	{
		this.fantasia = fantasia;
	}
	public String getTempo()
	{
		return tempo;
	}
	public void setTempo(String tempo)
	{
		this.tempo = tempo;
	}
	public String getLog()
	{
		return log;
	}
	public void setLog(String log)
	{
		this.log = log;
	}
	public String getNomeAtendente()
	{
		return nomeAtendente;
	}
	public void setNomeAtendente(String nomeAtendente)
	{
		this.nomeAtendente = nomeAtendente;
	}
	
}
