package com.neomind.fusion.custom.orsegups.converter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.util.NeoUtils;


public class HistoricoBonificacaoConverter extends StringConverter
{

	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin)
	{
	    Long idPai = field.getForm().getObjectId();
	    NeoObject historico = PersistEngine.getNeoObject(idPai);
	    EntityWrapper wrapper = new EntityWrapper(historico);
	    StringBuilder textoTable = new StringBuilder();
	    NeoObject neoNF = (NeoObject)wrapper.findValue("contrato");
	    GregorianCalendar dataSolicitacao = (GregorianCalendar) wrapper.findValue("dataSolicitacao");
	    Long numCtr = null;
	    Long numEmp = null;
	    Long numFil = null;
	    String competencia = null;
	    if (neoNF != null){
		EntityWrapper wrapperNF = new EntityWrapper(neoNF);
		numCtr = (Long) wrapperNF.findValue("usu_numctr");
		numEmp = (Long) wrapperNF.findValue("usu_codemp");
		numFil = (Long) wrapperNF.findValue("usu_codfil");
	    }else{
		neoNF = (NeoObject)wrapper.findValue("nfSapiens");
		if (neoNF != null){
		    EntityWrapper wrapperNF = new EntityWrapper(neoNF);
		    numCtr = (Long) wrapperNF.findValue("numctr");
		    numEmp = (Long) wrapperNF.findValue("codemp");
		    numFil = (Long) wrapperNF.findValue("codfil");
		}
	    }
	    if (neoNF != null){
		if (numCtr != null){
		    Connection conn = null;
		    PreparedStatement pstm = null;
		    ResultSet rs = null;

		    StringBuilder sql = new StringBuilder();

		    try {
			conn = PersistEngine.getConnection("");

			sql.append(" SELECT p.code, cms.usu_numctr, cms.usu_numpos, cms.usu_datcpt, cms.usu_cplcvs, cms.usu_obscms, cms.usu_adcsub, cms.usu_preuni, ");
			sql.append(" cms.usu_codccu, cms.usu_codser, cms.usu_codsnf, cms.usu_ctafin, cms.usu_ctared, cms.usu_datfim, cms.usu_datger, cms.usu_datini, ");
			sql.append(" cms.usu_datmov, cms.usu_diatra, cms.usu_filnfv, cms.usu_horger, cms.usu_libapo, cms.usu_numnfv, cms.usu_percrt, cms.usu_percsl, ");
			sql.append(" cms.usu_perins, cms.usu_perirf, cms.usu_periss, cms.usu_perour, cms.usu_perpit, cms.usu_posbon, cms.usu_qtdfun, cms.usu_seqmov, ");
			sql.append(" cms.usu_tarfus, cms.usu_tipapo, cms.usu_tnsser, cms.usu_unimed, cms.usu_usuger, cms.usu_codlan ");
			sql.append(" FROM D_ProcessoSolicitacaoCancelamentoNFBonificacao pscb ");
			sql.append(" INNER JOIN D_PrcssSlctcCnclmntNFBnfccmvmntsa48a lig ON lig.D_ProcessoSolicitacaoCancelamentoNFBonificacao_neoId = pscb.neoId ");
			sql.append(" INNER JOIN X_SAPIENSUSUTCMS ext ON ext.neoId = lig.movimentos_neoId ");
			sql.append(" INNER JOIN [FSOODB04\\SQL02].Sapiens.dbo.USU_T160CMS cms ON cms.usu_numctr = ext.usu_numctr and cms.usu_codemp = ext.usu_codemp and cms.usu_numpos = ext.usu_numpos and cms.usu_codfil = ext.usu_codfil ");
			sql.append(" INNER JOIN WFProcess p ON p.neoId = pscb.wfprocess_neoId ");
			sql.append(" WHERE ext.usu_numctr = ? AND ext.usu_codemp = ? AND ext.usu_codfil = ? and RIGHT(CONVERT(VARCHAR(15), cms.usu_datcpt, 103), 7) <> ? ");
			sql.append(" GROUP BY p.code, cms.usu_numctr, cms.usu_numpos, cms.usu_datcpt, cms.usu_cplcvs, cms.usu_obscms, cms.usu_adcsub, cms.usu_preuni, ");
			sql.append(" cms.usu_codccu, cms.usu_codser, cms.usu_codsnf, cms.usu_ctafin, cms.usu_ctared, cms.usu_datfim, cms.usu_datger, cms.usu_datini, ");
			sql.append(" cms.usu_datmov, cms.usu_diatra, cms.usu_filnfv, cms.usu_horger, cms.usu_libapo, cms.usu_numnfv, cms.usu_percrt, cms.usu_percsl, ");
			sql.append(" cms.usu_perins, cms.usu_perirf, cms.usu_periss, cms.usu_perour, cms.usu_perpit, cms.usu_posbon, cms.usu_qtdfun, cms.usu_seqmov, ");
			sql.append(" cms.usu_tarfus, cms.usu_tipapo, cms.usu_tnsser, cms.usu_unimed, cms.usu_usuger, cms.usu_codlan ");

			pstm = conn.prepareStatement(sql.toString());

			pstm.setLong(1, numCtr);
			pstm.setLong(2, numEmp);
			pstm.setLong(3, numFil);
			pstm.setString(4, NeoUtils.safeDateFormat(dataSolicitacao, "MM/yyyy"));

			rs = pstm.executeQuery();

			textoTable.append("		<table  class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">");
			textoTable.append("			<tr style=\"cursor: auto\">");
			textoTable.append("				<th style=\"cursor: auto\">Contrato</th>");
			textoTable.append("				<th style=\"cursor: auto\">Posto</th>");
			textoTable.append("				<th style=\"cursor: auto\">Competência</th>");
			textoTable.append("				<th style=\"cursor: auto\">Serviço</th>");
			textoTable.append("				<th style=\"cursor: auto\">Motivo / Observação</th>");
			textoTable.append("				<th style=\"cursor: auto\">Operação</th>");
			textoTable.append("				<th style=\"cursor: auto\">Valor Unit.</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_codccu</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_codser</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_codsnf</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_ctafin</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_ctared</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_datfim</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_datger</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_datini</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_datmov</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_diatra</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_filnfv</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_horger</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_libapo</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_numnfv</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_perctr</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_percsl</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_perins</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_perirf</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_periss</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_perour</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_perpit</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_posbon</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_qtdfun</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_seqmov</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_tarfus</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_tipapo</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_tnsser</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_unimed</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_usuger</th>");
			textoTable.append("				<th style=\"cursor: auto\">usu_codlan</th>");
			textoTable.append("			</tr>");
			textoTable.append("			<tbody>");	
			Boolean possuiHistorico = false;
			while (rs.next()) {
			    textoTable.append("<tr>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getLong("usu_numctr")) ? rs.getLong("usu_numctr") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getLong("usu_numpos")) ? rs.getLong("usu_numpos") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getDate("usu_datcpt")) ? NeoUtils.safeDateFormat(rs.getDate("usu_datcpt"), "MM/yyyy") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getString("usu_cplcvs")) ? rs.getString("usu_cplcvs") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getString("usu_obscms")) ? rs.getString("usu_obscms") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getString("usu_adcsub")) ? rs.getString("usu_adcsub") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getFloat("usu_preuni")) ? "R$ "+rs.getFloat("usu_preuni") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getLong("usu_codccu")) ? rs.getLong("usu_codccu") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getLong("usu_codser")) ? rs.getLong("usu_codser") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getString("usu_codsnf")) ? rs.getString("usu_codsnf") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getLong("usu_ctafin")) ? rs.getLong("usu_ctafin") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getLong("usu_ctared")) ? rs.getLong("usu_ctared") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getDate("usu_datfim")) ? NeoUtils.safeDateFormat(rs.getDate("usu_datfim"), "dd/MM/yyyy") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getDate("usu_datger")) ? NeoUtils.safeDateFormat(rs.getDate("usu_datger"), "dd/MM/yyyy") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getDate("usu_datini")) ? NeoUtils.safeDateFormat(rs.getDate("usu_datini"), "dd/MM/yyyy") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getDate("usu_datmov")) ? NeoUtils.safeDateFormat(rs.getDate("usu_datmov"), "dd/MM/yyyy") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getFloat("usu_diatra")) ? rs.getFloat("usu_diatra") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getLong("usu_filnfv")) ? rs.getLong("usu_filnfv") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getLong("usu_horger")) ? rs.getLong("usu_horger") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getString("usu_libapo")) ? rs.getString("usu_libapo") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getLong("usu_numnfv")) ? rs.getLong("usu_numnfv") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getFloat("usu_percrt")) ? rs.getFloat("usu_percrt") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getFloat("usu_percsl")) ? rs.getFloat("usu_percsl") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getFloat("usu_perins")) ? rs.getFloat("usu_perins") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getFloat("usu_perirf")) ? rs.getFloat("usu_perirf") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getFloat("usu_periss")) ? rs.getFloat("usu_periss") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getFloat("usu_perour")) ? rs.getFloat("usu_perour") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getFloat("usu_perpit")) ? rs.getFloat("usu_perpit") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getString("usu_posbon")) ? rs.getString("usu_posbon") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getFloat("usu_qtdfun")) ? rs.getFloat("usu_qtdfun") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getLong("usu_seqmov")) ? rs.getLong("usu_seqmov") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getLong("usu_tarfus")) ? rs.getLong("usu_tarfus") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getString("usu_tipapo")) ? rs.getString("usu_tipapo") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getString("usu_tnsser")) ? rs.getString("usu_tnsser") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getString("usu_unimed")) ? rs.getString("usu_unimed") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getLong("usu_usuger")) ? rs.getLong("usu_usuger") : "")+"</td>");
			    textoTable.append("<td>"+(!NeoUtils.safeIsNull(rs.getLong("usu_codlan")) ? rs.getLong("usu_codlan") : "")+"</td>");
			    textoTable.append("</tr>");
			    possuiHistorico = true;
			}
			if (possuiHistorico){
			    textoTable.append("			</tbody>");
			    textoTable.append("		</table>");
			}else{
			    textoTable = new StringBuilder();
			    textoTable.append("<i>Não há histórico</i>");
			}
		    } catch (SQLException e) {
			e.printStackTrace();
		    } finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		    }
		}
	    }
	    
	    String txtFinal = textoTable.toString();

	    txtFinal = txtFinal.replaceAll("\n", " ").replaceAll("\r", " ").replaceAll("&Acirc;&#160;", " ").replaceAll("Â&nbsp;", "");
	    return txtFinal;
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return getHTMLInput(field, origin);
	}
}