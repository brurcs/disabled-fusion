package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;

public class FCNAtualizaPrazo implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		String nomeProcesso = activity.getProcessName();
		
		if(nomeProcesso.equalsIgnoreCase(OrsegupsUtils.CANCELAMENTO_CONTRATO_INICIATIVA))
		{
			GregorianCalendar dataFinalMonitoramento = (GregorianCalendar) processEntity.findValue("dataFinalMonitoramento");
			GregorianCalendar newDate  = OrsegupsUtils.getSpecificWorkDay(dataFinalMonitoramento, 15L);
			
			processEntity.setValue("dataFinalRecebimento", newDate);
			
		}
		else if(nomeProcesso.equalsIgnoreCase(OrsegupsUtils.CANCELAMENTO_CONTRATO_INADIMPLENCIA))
		{
			GregorianCalendar dataFinalMonitoramento  = OrsegupsUtils.getSpecificWorkDay(activity.getProcess().getStartDate(), 5L);
			processEntity.setValue("dataFinalMonitoramento", dataFinalMonitoramento);
								
			GregorianCalendar dataFinalRecebimento  = OrsegupsUtils.getSpecificWorkDay(activity.getProcess().getStartDate(), 20L);
			processEntity.setValue("dataFinalRecebimento", dataFinalRecebimento);
			
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}
	
}
