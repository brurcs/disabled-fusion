package com.neomind.fusion.custom.orsegups.adapter.integraRubi;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.rpc.ServiceException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.util.PDFMergerUtility;

import br.com.senior.services.vetorh.constants.WSConfigs;
import br.com.senior.services.vetorh.relatorios.G5SeniorServicesLocatorFPRel;
import br.com.senior.services.vetorh.relatorios.RelatoriosRelatoriosIn;
import br.com.senior.services.vetorh.relatorios.RelatoriosRelatoriosOut;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.servicos.helpers.WorkflowHelper;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

import edu.emory.mathcs.backport.java.util.Collections;

public class RHintegracaoRelatoriosRubi implements AdapterInterface
{

	@SuppressWarnings("unused")
	private static final Log log = LogFactory.getLog(RHintegracaoRelatoriosRubi.class);

	@SuppressWarnings("unused")
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		WSConfigs config = new WSConfigs();
		// TODO Auto-generated method stub

		Task tarefaAnterior = retornaTarefaAnterior(activity); // Tarefa anterior.

		NeoUser userOrigin = tarefaAnterior.getInstance().getOwner();

		RelatoriosRelatoriosIn integrarRelatIn = new RelatoriosRelatoriosIn();
		RelatoriosRelatoriosOut integrarRelatOut = new RelatoriosRelatoriosOut();

		String caminhoPadrao = null;
		//ALTERAR PARA rhDiretorioDocs
		List<NeoObject> listaCaminhoPadrao = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("rhDiretorioDocs"), new QLRawFilter("tipoDoc like 'padrao'"));

		if (listaCaminhoPadrao.size() > 0)
		{

			for (NeoObject dir : listaCaminhoPadrao)
			{

				EntityWrapper wDir = new EntityWrapper(dir);

				caminhoPadrao = wDir.findGenericValue("diretorio");

			}

		}

		String prDir = null;

		//GERAÃ‡ÃƒO CRACHÃ�
		List<NeoObject> diretorioCracha = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("rhDiretorioDocs"), new QLRawFilter("tipoDoc like 'cracha'"));
		for (NeoObject dir : diretorioCracha)
		{

			EntityWrapper wDir = new EntityWrapper(dir);

			prDir = wDir.findGenericValue("diretorio");

		}
		//Lista que agrupa todos os documentos do contrato

		List<String> listaDocsContratos = new ArrayList();		
		String erroCracha = null;
		String erroProtocolo = null;
		String erroCon = null;
		Long codColab = processEntity.findGenericValue("tpColaborador.codigo");
		GregorianCalendar dataAtual = new GregorianCalendar();
		dataAtual.add(Calendar.YEAR, 1);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		//String sDataAtual = sdf.format(dataAtual.getTime());
		String erroWF = null;
		Integer numRel = null;
		Long numemp = processEntity.findGenericValue("emp.numemp"); //NÃºmero Empresa

		List<NeoObject> listaCrachas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("rhListaCrachas"), new QLRawFilter("empresa like '" + numemp + "' and ativo = true"));

		if (codColab == 1)
		{

			for (NeoObject cracha : listaCrachas)
			{

				EntityWrapper wCracha = new EntityWrapper(cracha);

				Long numRelLong = wCracha.findGenericValue("modeloCracha");

				numRel = numRelLong.intValue();

			}

		}
		else if (codColab == 2)
		{

			numRel = 209;
		}

		String prRelatorio = "FPDO" + numRel + ".col";
		String prExecFmt = "tefFile";
		Long numcad = processEntity.findGenericValue("matricula"); //Matricula
		String prEntrada = "<EAbrEmp=" + numemp + "> <EAbrTcl=1> <EAbrCad=" + numcad + ">";
		String prFileName = "Cracha_" + numemp + "_" + numcad;
		String prSaveFormat = "padrao";

		integrarRelatIn.setPrRelatorio(prRelatorio);
		integrarRelatIn.setPrExecFmt(prExecFmt);
		integrarRelatIn.setPrEntrada(prEntrada);
		integrarRelatIn.setPrEntranceIsXML("F");
		integrarRelatIn.setPrSaveFormat(prSaveFormat);
		integrarRelatIn.setPrDir(prDir);
		integrarRelatIn.setPrFileName(prFileName);

		G5SeniorServicesLocatorFPRel glRel = new G5SeniorServicesLocatorFPRel();

		System.out.println("---------------------------------------------------------------------");
		System.out.println("RHIntegracaoRelatoriosRubi - Matrícula: " + numcad + " Integrando relatório: " + prRelatorio + " prFileName: " + prFileName + " - " + sdf.format(new GregorianCalendar().getTime()));
		System.out.println("---------------------------------------------------------------------");

		try
		{
			integrarRelatOut = glRel.getrubi_Synccom_senior_g5_rh_fp_relatoriosPort().relatorios(config.getUserService(), config.getPasswordService(), 0, integrarRelatIn);
		}
		catch (RemoteException | ServiceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		erroCracha = integrarRelatOut.getErroExecucao();

		if (erroCracha != null)
		{

			erroCracha = prRelatorio + " - " + erroCracha;

			log.warn("Erro na geração do crachá!");
			System.out.println("Erro na geração do crachá! - " + erroCracha);
			//contErros++;
			if (erroWF != null)
				erroWF = erroWF + " - " + erroCracha;
			else
			{
				erroWF = erroCracha;
			}

		}
		else
		{

			NeoFile neoFileCracha = null;

			log.warn("Crachá: " + prFileName + " - GERADO COM SUCESSO!");
			System.out.println("Crachá: " + prFileName + " - GERADO COM SUCESSO!");

			File fileCracha = new File(caminhoPadrao + "CRACHAS\\" + prFileName + ".IMP");

			if (fileCracha.exists())
			{

				neoFileCracha = createNeoFile(fileCracha, fileCracha.getName());

			}

			NeoObject crachaOBJ = processEntity.findGenericValue("documentacao");

			new EntityWrapper(crachaOBJ).setValue("docCracha", neoFileCracha);

			fileCracha.delete();

		}

		//GERAÃ‡ÃƒO PROTOCOLOS DE ENTREGA DOS CRACHÃ�S
		List<NeoObject> diretorioProtocoloCracha = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("rhDiretorioDocs"), new QLRawFilter("tipoDoc like 'protocolocracha'"));
		for (NeoObject dir : diretorioProtocoloCracha)
		{

			EntityWrapper wDir = new EntityWrapper(dir);

			prDir = wDir.findGenericValue("diretorio");

		}
		//CÓDIGO DO RELATÓRIO REFERENTE AO PROTOCOLO
		numRel = 195;

		prRelatorio = "FPDO" + numRel + ".col";
		prEntrada = "<EAbrEmp=" + numemp + "> <EAbrTcl=1> <EAbrCad=" + numcad + ">";
		prFileName = "Protocolo_Cracha_" + numemp + "_" + numcad;
		prSaveFormat = "tsfPDF";

		integrarRelatIn.setPrRelatorio(prRelatorio);
		integrarRelatIn.setPrExecFmt(prExecFmt);
		integrarRelatIn.setPrEntrada(prEntrada);
		integrarRelatIn.setPrEntranceIsXML("F");
		integrarRelatIn.setPrSaveFormat(prSaveFormat);
		integrarRelatIn.setPrDir(prDir);
		integrarRelatIn.setPrFileName(prFileName);

		G5SeniorServicesLocatorFPRel glProt = new G5SeniorServicesLocatorFPRel();

		System.out.println("---------------------------------------------------------------------");
		System.out.println("RHIntegracaoRelatoriosRubi - Matrícula: " + numcad + " Integrando relatório: " + prRelatorio + " prFileName: " + prFileName + " - " + sdf.format(new GregorianCalendar().getTime()));
		System.out.println("---------------------------------------------------------------------");

		try
		{
			integrarRelatOut = glProt.getrubi_Synccom_senior_g5_rh_fp_relatoriosPort().relatorios(config.getUserService(), config.getPasswordService(), 0, integrarRelatIn);
		}
		catch (RemoteException | ServiceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		erroProtocolo = integrarRelatOut.getErroExecucao();

		if (erroProtocolo != null)
		{

			erroProtocolo = prRelatorio + " - " + erroProtocolo;

			log.warn("Erro na geração do crachá!");
			System.out.println("Erro na geração do crachá!");
			//contErros++;
			if (erroWF != null)
				erroWF = erroWF + " - " + erroProtocolo;
			else
			{
				erroWF = erroProtocolo;
			}

		}
		else
		{

			NeoFile neoFileProtocolo = null;

			log.warn("Protocolo do Crachá: " + prFileName + " - GERADO COM SUCESSO!");
			System.out.println("Protocolo do Crachá: " + prFileName + " - GERADO COM SUCESSO!");

			File fileProtocolo = new File(caminhoPadrao + "PROTOCOLOCRACHA\\" + prFileName + ".PDF");

			if (fileProtocolo.exists())
			{

				neoFileProtocolo = createNeoFile(fileProtocolo, fileProtocolo.getName());

			}

			NeoObject protocoloOBJ = processEntity.findGenericValue("documentacao");

			new EntityWrapper(protocoloOBJ).setValue("docProtocoloCracha", neoFileProtocolo);

			fileProtocolo.delete();

		}

		//GERAÇÃO DOS CONTRATOS
		String cargo = processEntity.findGenericValue("cargos.titcar");

		String tipcar = processEntity.findGenericValue("cargos.usu_tipcar");

		Long codPag = processEntity.findGenericValue("tpSalario.codigo");

		Boolean possuiVT = processEntity.findGenericValue("usaVT");

		Long catsefip = processEntity.findGenericValue("catSefip.codigo");

		Long codTipAdm = processEntity.findGenericValue("tpAdmColab.codigo");

		Long codreg = processEntity.findGenericValue("regionalColab.usu_codreg");

		String codcar = processEntity.findGenericValue("cargos.codcar");

		System.out.println("Código do cargo: " + codcar + " - " + numcad + " - " + numemp);

		List<String> contrato = new ArrayList<>();

		/*
		 * 001 - Cascavel - Empresas 21(VigilÃ¢ncia) ou 29(Asseio) - Operacional
		 * 002 - Cascavel - Empresas 21(VigilÃ¢ncia) ou 29(Asseio) - Administrativo
		 * 003 - Vigilante padrÃ£o(NÃƒO CASCAVEL) - Empresas 21 e 01 - Mensalista C/VT ou Emp 18 com
		 * cargos Monitor de Acesso, AIT ou Supervisor
		 * 004 - Vigilante padrÃ£o(NÃƒO CASCAVEL) - Empresas 21 e 01 - Mensalista S/VT ou Emp 18 com
		 * cargos Monitor de Acesso, AIT ou Supervisor
		 * 005 - Vigilante padrÃ£o(NÃƒO CASCAVEL) - Empresas 21 e 01 - Horista C/VT ou Emp 18 com cargos
		 * Monitor de Acesso, AIT ou Supervisor
		 * 006 - Vigilante padrÃ£o(NÃƒO CASCAVEL) - Empresas 21 e 01 - Horista S/VT ou Emp 18 com cargos
		 * Monitor de Acesso, AIT ou Supervisor
		 * 007 - Asseio padrÃ£o - Empresas 2, 6 ou 29 - Horista - C/VT
		 * 008 - Asseio padrÃ£o - Empresas 2, 6 ou 29 - Horista - S/VT
		 * 009 - Asseio padrÃ£o - Empresas 2, 6 ou 29 - Mensalista - C/VT
		 * 010 - Asseio padrÃ£o - Empresas 2, 6 ou 29 - Mensalista - S/VT
		 * 011 - Aprendiz C/VT
		 * 012 - Aprendiz S/VT
		 */

		//APRENDIZ
		if (catsefip == 7)
		{

			if (possuiVT)
			{

				contrato.add("011");

			}
			else
			{

				contrato.add("012");

			}
			//CASCAVEL OPERACIONAL    
		}
		else if (codreg == 14 && codTipAdm == 2 && (numemp == 21 || numemp == 29))
		{

			contrato.add("001");

			//CASCAVEL ADMINISTRATIVA    
		}
		else if (codreg == 14 && codTipAdm == 1 && (numemp == 21 || numemp == 29))
		{

			contrato.add("002");

			//Vigilante padrão(NÃO CASCAVEL) - Mensalista - Empresas 21, 01 ou Emp 18 com cargos Monitor de Acesso, AIT ou Supervisor
		}
		else if (codreg != 14 && codPag == 1 && (numemp == 21 || numemp == 1 || (numemp == 18 && (cargo.contains("AIT") || cargo.contains("supervisor") || (cargo.contains("monitor") && cargo.contains("acesso"))))))
		{

			if (possuiVT)
			{

				contrato.add("003");

			}
			else
			{

				contrato.add("004");

			}

			//Vigilante padrão(NÃO CASCAVEL) - Horista - Empresas 21, 01 ou Emp 18 com cargos Monitor de Acesso, AIT ou Supervisor    
		}
		else if (codreg != 14 && codPag == 5 && (numemp == 21 || numemp == 1 || (numemp == 18 && (cargo.contains("AIT") || cargo.contains("supervisor") || (cargo.contains("monitor") && cargo.contains("acesso"))))))
		{

			if (possuiVT)
			{

				contrato.add("005");

			}
			else
			{

				contrato.add("006");

			}

			//Asseio padrão - Empresas 2, 6 ou 29 - Horista 
		}
		else if (codreg != 14 && codPag == 5 && (numemp == 2 || numemp == 6 || numemp == 29))
		{

			if (possuiVT)
			{

				contrato.add("007");

			}
			else
			{

				contrato.add("008");

			}

			//Asseio padrão - Empresas 2, 6 ou 29 - Mensalista    
		}
		else if (codreg != 14 && codPag == 1 && (numemp == 2 || numemp == 6 || numemp == 29))
		{

			if (possuiVT)
			{

				contrato.add("009");

			}
			else
			{

				contrato.add("010");

			}

		}
		else
		{

			if (possuiVT)
				contrato.add("003");
			else
				contrato.add("004");
		}

		System.out.println("Relação de Contratao Selecionado para Matricula " + numcad + ": " + contrato);
		
		Integer contErros = 0;

		//For responsável por mesclar vigilante ou asseio com documentação de horista quando for o caso.
		for (String selecao : contrato)
		{

			List<NeoObject> listaContratos = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("rhListaContratos"), new QLRawFilter("listaContrato like '" + selecao + "' and ativo = true"));

			//ordenador da lista baseada no campo de ordenação
			sortNeoId(listaContratos);

			NeoObject noParametroQtd = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("RH01ParametrizacaoAdmissao"), new QLEqualsFilter("parametro", "qtdTentativas"));
			NeoObject noParametroIntervalo = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("RH01ParametrizacaoAdmissao"), new QLEqualsFilter("parametro", "intervaloTentativas"));
			
			if (noParametroQtd == null || noParametroIntervalo == null)
				throw new WorkflowException("Os parametros de tentativas do WS não foram configurados corretamente. Favor contatar a TI.");
			
			Long limiteTentativas = Long.valueOf((String) new EntityWrapper(noParametroQtd).findGenericValue("valor"));
			Long intervalo = Long.valueOf((String) new EntityWrapper(noParametroIntervalo).findGenericValue("valor"));
			
			Boolean relatoriosOK = false;
			Long tentativas = 0l;
			retentativas: while (!relatoriosOK && tentativas < limiteTentativas)
			{
				tentativas++;
				contErros = 0;
				
				System.out.println("RHIntegracaoRelatoriosRubi - Tentativa " + tentativas + " - Tarefa: " + activity.getCode());
				
				for (NeoObject itemlista : listaContratos)
				{
				
				
					List<NeoObject> diretorioContrato = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("rhDiretorioDocs"), new QLRawFilter("tipoDoc like 'contrato'"));

					for (NeoObject dir : diretorioContrato)
					{

						EntityWrapper wDir = new EntityWrapper(dir);

						prDir = wDir.findGenericValue("diretorio");

					}

					EntityWrapper wItemLista = new EntityWrapper(itemlista);

					GregorianCalendar dtadmissao = processEntity.findGenericValue("dtAdmissao");

					String sDtAdmissao = sdf.format(dtadmissao.getTime());

					prRelatorio = wItemLista.findGenericValue("modeloRelatorio");

					prEntrada = null;

					String horseg = processEntity.findGenericValue("jornadaTrabalho.horarioSeg");

					String horter = processEntity.findGenericValue("jornadaTrabalho.horarioTerca");

					String horqua = processEntity.findGenericValue("jornadaTrabalho.horarioQua");

					String horqui = processEntity.findGenericValue("jornadaTrabalho.horarioQui");

					String horsex = processEntity.findGenericValue("jornadaTrabalho.horarioSex");

					String horsabado = processEntity.findGenericValue("jornadaTrabalho.horarioSab");

					String hordomingo = processEntity.findGenericValue("jornadaTrabalho.horarioDom");

					NeoObject oJornada = processEntity.findGenericValue("jornadaTrabalho");

					EntityWrapper wJornada = new EntityWrapper(oJornada);

					Long jornada = wJornada.findGenericValue("jornada.codigo");

					Integer anoCorrente = dtadmissao.get(Calendar.YEAR);

					String anoCorrenteS = anoCorrente.toString();

					String newdata = "31/12/";

					newdata = newdata + anoCorrenteS;

					GregorianCalendar hoje = new GregorianCalendar();

					SimpleDateFormat sdfmesano = new SimpleDateFormat("MM/yyyy");

					String mesano = sdfmesano.format(hoje.getTime());

					String sHoje = sdf.format(hoje.getTime());

					@SuppressWarnings("deprecation")
					Integer anoAdmissao = dtadmissao.get(Calendar.YEAR);

					@SuppressWarnings("deprecation")
					Integer mesAdmissao = dtadmissao.get(Calendar.MONTH);

					String mesAdmissaoS = null;

					//mesAdmissaoS deve ser sempre 1 mes antes da data de admissão
					if (mesAdmissao == 0)
						mesAdmissaoS = "12";
					else if (mesAdmissao < 10)
						mesAdmissaoS = "0" + mesAdmissao;
					else
						mesAdmissaoS = mesAdmissao.toString();

					String mesanoVT = mesAdmissaoS + "/" + anoAdmissao.toString();
					System.out.println("RHIntegracaoRelatoriosRubi - Matrícula: " + numcad + " Mês Referencia VT: " + mesanoVT + " - " + sdf.format(new GregorianCalendar().getTime()));

					//INÍCIO DA REGRAS PARA OS MAPAS DE CADA RELATÓRIO

					if (prRelatorio.equals("FPDO901.COL"))
					{

						prEntrada = "<ETipEmi=E><EDatEmi=" + sDtAdmissao + "><EDatIni=01/01/2012><EDatRef=" + sDtAdmissao + "><ESomAdm=N><EMosUsu=N><EAbrEmp=" + numemp + "><EAbrTcl=1><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPGE703.GER"))
					{

						prEntrada = "<Emesano=" + mesanoVT + "><ecodtipger=1><Eabremp=" + numemp + "><Eabrtipcol=1><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPVT708.COL"))
					{

						prEntrada = "<Emesano=" + mesanoVT + "><ecodtipger=1><Eabremp=" + numemp + "><Eabrtipcol=1><EAbrCad=" + numcad + "><gerarpendencia=S>";

					}
					else if (prRelatorio.equals("FPDO502.COL"))
					{

						prEntrada = "<EModLis=02><EDatIni=01/01/2012><EDatFim=" + newdata + "><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + "><EAbrTcl=1>";

					}
					else if (prRelatorio.equals("FPDO531.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + "><EAbrTcl=1><ESomAdm=N><EMosUsu=N>";

					}
					else if (prRelatorio.equals("FPDO532.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + "><EAbrTcl=1><ESomAdm=N><EMosUsu=S>";

					}
					else if (prRelatorio.equals("FPDO533.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + sDtAdmissao + "><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + "><EAbrTcl=1><ESomAdm=N><EMosUsu=N>";

					}
					else if (prRelatorio.equals("FPDO006.COL"))
					{

						prEntrada = "<ETipEmi=E><EDatEmi=" + sDtAdmissao + "><EDatIni=01/01/2012><EDatRef=" + sDtAdmissao + "><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + "><EAbrTcl=1><ESomAdm=N><EMosUsu=N>";

					}
					else if (prRelatorio.equals("FPDO008.COL"))
					{

						prEntrada = "<ETipEmi=A><EDatEmi=" + sDtAdmissao + "><EDatIni=01/01/2012><EDatRef=" + sDtAdmissao + "><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + "><EAbrTcl=1><ESomAdm=N><EMosUsu=S>";

					}
					else if (prRelatorio.equals("FPDO112.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + "><EAbrTcl=1><ESomAdm=N><EMosUsu=N>";

					}
					else if (prRelatorio.equals("FPDO189.COL"))
					{//OK empresa 1

						prEntrada = "<EDatRec=" + sHoje + "><EMesAno=" + mesano + "><EAbrEmp=" + numemp + "><ECodTipGer=1><EMosUsu=S><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO118.COL"))
					{//OK empresa 1

						prEntrada = "<EDatRec=" + sHoje + "><EMesAno=" + mesano + "><EAbrEmp=" + numemp + "><ECodTipGer=1><EMosUsu=S><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO116.COL"))
					{//OK empresa 1

						prEntrada = "<EAbrEmp=" + numemp + "><ECodTipGer=1><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPRE258.COL"))
					{

						prEntrada = "<EDatRef=" + sDtAdmissao + "><EMosUsu=S><EAbrEmp=" + numemp + "><EAbrTcl=1><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO009.COL"))
					{

						prEntrada = "<ETipEmi=E><EDatEmi=" + sDtAdmissao + "><EDatIni=01/01/2012><EDatRef=" + sDtAdmissao + "><ESomAdm=N><EMosUsu=N><EAbrGpe=N><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + "><EAbrTcl=1>";

					}
					else if (prRelatorio.equals("FPDO534.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><ESomAdm=N><EMosUsu=S><EAbrTcl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO194.COL"))
					{

						prEntrada = "<EDatRec=" + sHoje + "><EMesAno=" + mesano + "><EAbrEmp=" + numemp + "><ECodTipGer=1><EMosUsu=S><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO119.COL"))
					{

						prEntrada = "<EAbrEmp=" + numemp + "><EAbrTcl=1><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPFR305.COL"))
					{

						prEntrada = "<ELisDem=N><EDatIni=01/01/2012><EDatRef=" + sDtAdmissao + "><EAbrAss=S><ENumPag=N><EAbrTcl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO603.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO619.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO628.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO629.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO644.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO645.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO646.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO647.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO648.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO649.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO650.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO651.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO652.COL"))
					{

						prEntrada = "<EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO653.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO654.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO655.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO656.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO657.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO658.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO659.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO038.COL"))
					{

						prEntrada = "<ELisDem=N><EDatIni=01/01/2012><EDatRef=" + sDtAdmissao + "><EAbrAss=S><ENumPag=N><EAbrTcl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO604.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO660.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO661.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO662.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO663.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO664.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO536.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO537.COL") || prRelatorio.equals("FPDO538.COL") || prRelatorio.equals("FPDO539.COL"))
					{

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EDatEmi=" + sDtAdmissao + "><ETipEmi=E><EHorSegF=" + horseg + "><EHorTerF=" + horter + "><EHorQuaF=" + horqua + "><EHorQuiF=" + horqui + "><EHorSexF=" + horsex + "><EhorSabado=" + horsabado + "><EhorDomingo=" + hordomingo + "><EAbrTCl=1><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + ">";

					}
					else if (prRelatorio.equals("FPDO540.COL"))
					{

						System.out.println("Report FPDO540.COL: " + numemp + " - " + numcad + " - " + anoCorrente + " - " + newdata);

						prEntrada = "<EDatIni=01/01/2012><EDatFim=" + newdata + "><EAbrEmp=" + numemp + "><EAbrCad=" + numcad + "><EAbrTcl=1><ESomAdm=N><EMosUsu=S>";

					}

					prFileName = "contrato_" + prRelatorio + "_" + numemp + "_" + numcad;

					Boolean dependentes = processEntity.findGenericValue("possuiDepend");

					Boolean possuiFilho = Boolean.FALSE;

					Integer filhoSalarioFamilia = 0;

					Boolean possuiCartVT = Boolean.FALSE;

					Boolean passouFPGE703 = processEntity.findGenericValue("FPGE703");

					Boolean possuivt = processEntity.findGenericValue("usaVT");

					List<NeoObject> listaVT = null;

					if (possuivt)
					{

						listaVT = processEntity.findGenericValue("listaVT");

						for (NeoObject vt : listaVT)
						{

							EntityWrapper wVT = new EntityWrapper(vt);

							Long numVT = wVT.findGenericValue("numVT");

							if (numVT != null)
							{

								possuiCartVT = Boolean.TRUE;

							}

						}

					}

					if (dependentes)
					{

						List<NeoObject> listaFilhos = processEntity.findGenericValue("listaDepend");

						for (NeoObject dep : listaFilhos)
						{

							EntityWrapper wDep = new EntityWrapper(dep);

							Long coddep = wDep.findGenericValue("graParentesco.codigo");

							if (coddep == 1)
							{

								possuiFilho = Boolean.TRUE;

								//Cálculo da idade do dependente

								GregorianCalendar dtnascimentoDependente = wDep.findGenericValue("dtNascDepend");

								Long dt = (dtadmissao.getTimeInMillis() - dtnascimentoDependente.getTimeInMillis()) + 3600000;

								Long diasCorridosAnoLong = (dt / 86400000L);

								Integer diasDecorridosInt = Integer.valueOf(diasCorridosAnoLong.toString());

								BigDecimal dias = new BigDecimal(diasDecorridosInt);

								BigDecimal ano = new BigDecimal(365.25);

								BigDecimal idadeDependente = dias.divide(ano, 0, RoundingMode.DOWN);

								if (idadeDependente.intValue() < 14)
								{

									filhoSalarioFamilia++;

								}

							}

						}

					}

					integrarRelatIn.setPrRelatorio(prRelatorio);
					integrarRelatIn.setPrExecFmt(prExecFmt);
					integrarRelatIn.setPrEntrada(prEntrada);
					integrarRelatIn.setPrEntranceIsXML("F");
					integrarRelatIn.setPrSaveFormat(prSaveFormat);
					integrarRelatIn.setPrDir(prDir);
					integrarRelatIn.setPrFileName(prFileName);

					G5SeniorServicesLocatorFPRel glCont = new G5SeniorServicesLocatorFPRel();

					System.out.println("---------------------------------------------------------------------");
					System.out.println("RHIntegracaoRelatoriosRubi - Matrícula: " + numcad + " Integrando relatório: " + prRelatorio + " prFileName: " + prFileName + " - " + sdf.format(new GregorianCalendar().getTime()));
					System.out.println("RHIntegracaoRelatoriosRubi - Matrícula: " + numcad + " prEntrada: " + prEntrada + " - " + sdf.format(new GregorianCalendar().getTime()));
					System.out.println("---------------------------------------------------------------------");

					if (prRelatorio.equals("FPDO536.COL") || prRelatorio.equals("FPDO537.COL") || prRelatorio.equals("FPDO538.COL") || prRelatorio.equals("FPDO539.COL"))
					{

						if (jornada == 1 && prRelatorio.equals("FPDO536.COL"))
						{

							try
							{
								integrarRelatOut = glCont.getrubi_Synccom_senior_g5_rh_fp_relatoriosPort().relatorios(config.getUserService(), config.getPasswordService(), 0, integrarRelatIn);
							}
							catch (RemoteException | ServiceException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							erroCon = integrarRelatOut.getErroExecucao();

							if (erroCon != null)
							{

								erroCon = prRelatorio + " - " + erroCon;

								if (erroWF != null)
								{

									erroWF = erroWF + " - " + erroCon;

								}
								else
								{

									erroWF = erroCon;

								}
								log.warn("Erro no: " + prFileName);
								System.out.println("Erro no: " + prFileName);
								
								if (tentativas < limiteTentativas)
								{
									try
									{
										Thread.sleep(intervalo);
									}
									catch (InterruptedException e)
									{
										e.printStackTrace();
									}
									
									relatoriosOK = false;
									continue retentativas;
								}
								else contErros++;
							}
							else
							{

								log.warn("contrato_" + prRelatorio + "_" + numemp + "_" + numcad + " GERADO COM SUCESSO!");
								System.out.println("contrato_" + prRelatorio + "_" + numemp + "_" + numcad + " GERADO COM SUCESSO!");
								if (!prRelatorio.equals("FPGE703.GER"))
								{

									listaDocsContratos.add("contrato_" + prRelatorio + "_" + numemp + "_" + numcad + ".PDF");

								}

							}

						}
						else if (jornada == 2 && prRelatorio.equals("FPDO539.COL"))
						{

							try
							{
								integrarRelatOut = glCont.getrubi_Synccom_senior_g5_rh_fp_relatoriosPort().relatorios(config.getUserService(), config.getPasswordService(), 0, integrarRelatIn);
							}
							catch (RemoteException | ServiceException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							erroCon = integrarRelatOut.getErroExecucao();

							if (erroCon != null)
							{

								erroCon = prRelatorio + " - " + erroCon;

								if (erroWF != null)
								{

									erroWF = erroWF + " - " + erroCon;

								}
								else
								{

									erroWF = erroCon;

								}
								log.warn("Erro no: " + prFileName);
								System.out.println("Erro no: " + prFileName);
								contErros++;
							}
							else
							{

								log.warn("contrato_" + prRelatorio + "_" + numemp + "_" + numcad + " GERADO COM SUCESSO!");
								System.out.println("contrato_" + prRelatorio + "_" + numemp + "_" + numcad + " GERADO COM SUCESSO!");
								if (!prRelatorio.equals("FPGE703.GER"))
								{

									listaDocsContratos.add("contrato_" + prRelatorio + "_" + numemp + "_" + numcad + ".PDF");

								}

							}

						}
						else if (jornada == 3 && prRelatorio.equals("FPDO538.COL"))
						{

							try
							{
								integrarRelatOut = glCont.getrubi_Synccom_senior_g5_rh_fp_relatoriosPort().relatorios(config.getUserService(), config.getPasswordService(), 0, integrarRelatIn);
							}
							catch (RemoteException | ServiceException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							erroCon = integrarRelatOut.getErroExecucao();

							if (erroCon != null)
							{

								erroCon = prRelatorio + " - " + erroCon;

								if (erroWF != null)
								{

									erroWF = erroWF + " - " + erroCon;

								}
								else
								{

									erroWF = erroCon;

								}
								log.warn("Erro no: " + prFileName);
								System.out.println("Erro no: " + prFileName);

								if (tentativas < limiteTentativas)
								{
									try
									{
										Thread.sleep(intervalo);
									}
									catch (InterruptedException e)
									{
										e.printStackTrace();
									}
									
									relatoriosOK = false;
									continue retentativas;
								}
								else contErros++;
							}
							else
							{

								log.warn("contrato_" + prRelatorio + "_" + numemp + "_" + numcad + " GERADO COM SUCESSO!");
								System.out.println("contrato_" + prRelatorio + "_" + numemp + "_" + numcad + " GERADO COM SUCESSO!");
								if (!prRelatorio.equals("FPGE703.GER"))
								{

									listaDocsContratos.add("contrato_" + prRelatorio + "_" + numemp + "_" + numcad + ".PDF");

								}

							}

						}
						else if (jornada == 4 && prRelatorio.equals("FPDO537.COL"))
						{

							try
							{
								integrarRelatOut = glCont.getrubi_Synccom_senior_g5_rh_fp_relatoriosPort().relatorios(config.getUserService(), config.getPasswordService(), 0, integrarRelatIn);
							}
							catch (RemoteException | ServiceException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							erroCon = integrarRelatOut.getErroExecucao();

							if (erroCon != null)
							{

								erroCon = prRelatorio + " - " + erroCon;

								if (erroWF != null)
								{

									erroWF = erroWF + " - " + erroCon;

								}
								else
								{

									erroWF = erroCon;

								}
								log.warn("Erro no: " + prFileName);
								System.out.println("Erro no: " + prFileName);


								if (tentativas < limiteTentativas)
								{
									try
									{
										Thread.sleep(intervalo);
									}
									catch (InterruptedException e)
									{
										e.printStackTrace();
									}
									
									relatoriosOK = false;
									continue retentativas;
								}
								else contErros++;
							}
							else
							{

								log.warn("contrato_" + prRelatorio + "_" + numemp + "_" + numcad + " GERADO COM SUCESSO!");
								System.out.println("contrato_" + prRelatorio + "_" + numemp + "_" + numcad + " GERADO COM SUCESSO!");
								if (!prRelatorio.equals("FPGE703.GER"))
								{

									listaDocsContratos.add("contrato_" + prRelatorio + "_" + numemp + "_" + numcad + ".PDF");

								}

							}

						}

					}
					else if ((!prRelatorio.equals("FPDO533.COL") && !prRelatorio.equals("FPDO006.COL") && !prRelatorio.equals("FPRE258.COL")) && !prRelatorio.equals("FPGE703.GER") && !prRelatorio.equals("FPVT708.COL") && !prRelatorio.equals("FPDO652.COL"))
					{

						try
						{
							integrarRelatOut = glCont.getrubi_Synccom_senior_g5_rh_fp_relatoriosPort().relatorios(config.getUserService(), config.getPasswordService(), 0, integrarRelatIn);
						}
						catch (RemoteException | ServiceException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						erroCon = integrarRelatOut.getErroExecucao();

						if (erroCon != null)
						{

							erroCon = prRelatorio + " - " + erroCon;

							if (erroWF != null)
							{

								erroWF = erroWF + " - " + erroCon;

							}
							else
							{

								erroWF = erroCon;

							}
							log.warn("Erro no: " + prFileName);
							System.out.println("Erro no: " + prFileName);

							if (tentativas < limiteTentativas)
							{
								try
								{
									Thread.sleep(intervalo);
								}
								catch (InterruptedException e)
								{
									e.printStackTrace();
								}
								
								relatoriosOK = false;
								continue retentativas;
							}
							else contErros++;
						}
						else
						{

							log.warn("contrato_" + prRelatorio + "_" + numemp + "_" + numcad + " GERADO COM SUCESSO!");
							System.out.println("contrato_" + prRelatorio + "_" + numemp + "_" + numcad + " GERADO COM SUCESSO!");
							if (!prRelatorio.equals("FPGE703.GER"))
							{

								listaDocsContratos.add("contrato_" + prRelatorio + "_" + numemp + "_" + numcad + ".PDF");

							}

						}

					}
					else if ((prRelatorio.equals("FPDO533.COL") && possuiFilho && filhoSalarioFamilia > 0) || (prRelatorio.equals("FPDO006.COL") && possuiFilho && filhoSalarioFamilia > 0) || ((prRelatorio.equals("FPRE258.COL") && possuiCartVT || (prRelatorio.equals("FPVT708.COL")))))
					{

						System.out.println("INFORMAÇÕES DOS RELATÓRIOS VARIÁVEIS: Matrícula -> " + numcad + " - Empresa -> " + numemp + " - Relatório -> " + prRelatorio + "" + " - Possui Cartão de VT -> " + possuiCartVT);

						try
						{
							integrarRelatOut = glCont.getrubi_Synccom_senior_g5_rh_fp_relatoriosPort().relatorios(config.getUserService(), config.getPasswordService(), 0, integrarRelatIn);
						}
						catch (RemoteException | ServiceException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						erroCon = integrarRelatOut.getErroExecucao();

						if (erroCon != null)
						{

							erroCon = prRelatorio + " - " + erroCon;

							if (erroWF != null)
							{

								erroWF = erroWF + " - " + erroCon;

							}
							else
							{

								erroWF = erroCon;

							}
							log.warn("Erro no: " + prFileName);
							System.out.println("Erro no: " + prFileName);

							if (tentativas < limiteTentativas)
							{
								try
								{
									Thread.sleep(intervalo);
								}
								catch (InterruptedException e)
								{
									e.printStackTrace();
								}
								
								relatoriosOK = false;
								continue retentativas;
							}
							else contErros++;
						}
						else
						{

							log.warn("Contrato_" + prRelatorio + "_" + numemp + "_" + numcad + " GERADO COM SUCESSO!");
							System.out.println("Contrato_" + prRelatorio + "_" + numemp + "_" + numcad + " GERADO COM SUCESSO!");
							if (!prRelatorio.equals("FPGE703.GER"))
							{

								listaDocsContratos.add("contrato_" + prRelatorio + "_" + numemp + "_" + numcad + ".PDF");

							}

						}

					}
					else if (prRelatorio.equals("FPGE703.GER") && (passouFPGE703 == null || !passouFPGE703))
					{

						try
						{
							integrarRelatOut = glCont.getrubi_Synccom_senior_g5_rh_fp_relatoriosPort().relatorios(config.getUserService(), config.getPasswordService(), 0, integrarRelatIn);
						}
						catch (RemoteException | ServiceException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						erroCon = integrarRelatOut.getErroExecucao();

						if (erroCon != null)
						{

							erroCon = prRelatorio + " - DEFINIR PERÍODO DE CÁLCULO DO VALE TRANSPORTE DO MÊS DA ADMISSÃO! - " + erroCon;

							if (erroWF != null)
							{

								erroWF = erroWF + " - " + erroCon;

							}
							else
							{

								erroWF = erroCon;

							}
							System.out.println("Erro no: " + prFileName);

							if (tentativas < limiteTentativas)
							{
								try
								{
									Thread.sleep(intervalo);
								}
								catch (InterruptedException e)
								{
									e.printStackTrace();
								}
								
								relatoriosOK = false;
								continue retentativas;
							}
							else contErros++;
						}
						else
						{

							System.out.println("Contrato_" + prRelatorio + "_" + numemp + "_" + numcad + " GERADO COM SUCESSO!");

							processEntity.findField("FPGE703").setValue(Boolean.TRUE);

							if (!prRelatorio.equals("FPGE703.GER"))
							{

								listaDocsContratos.add("contrato_" + prRelatorio + "_" + numemp + "_" + numcad + ".PDF");

							}

						}

					}
					else
					{

						if (prRelatorio.equals("FPGE703.GER"))
						{

							System.out.println(prRelatorio + " JÁ FOI GERADO OU NÃO DEVE SER GERADO!");

						}
						else
						{

							System.out.println("Relatório: " + prRelatorio + " - NÃƒO FOI GERADO POIS NÃƒO POSSUI DEPENDENTES E/OU CARTÃƒO VT");

						}

					}

					////////////////////////////////////////////////////////////////////////////////////////////////////////

				}
				relatoriosOK = true;
			}
		}
		//DEVE ENCERRAR O FOR DA SELEÃ‡Ã•ES A SEREM IMPRESSAS

		//INICIA GERAÃ‡ÃƒO E MERGE DOS DOCUMENTOS DO CONTRATO
		PDFMergerUtility ut = new PDFMergerUtility();

		Integer sizeListaDocs = listaDocsContratos.size();

		Integer contSizeListaDocs = 1;

		String fimOS = null;

		for (String nomeArquivo : listaDocsContratos)
		{

			ut.addSource(new File(caminhoPadrao + "CONTRATOS\\" + nomeArquivo));

			if (contSizeListaDocs == sizeListaDocs)
			{

				//adciona documento da Ordem de serviÃ§o

				List<NeoObject> docsOSs = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("rhCargoSxOSs"), new QLEqualsFilter("cargo", codcar));
				for (NeoObject OS : docsOSs)
				{

					EntityWrapper wOS = new EntityWrapper(OS);

					fimOS = wOS.findGenericValue("os");

				}

				if (fimOS != null)
				{

					NeoObject docOS = AdapterUtils.createNewEntityInstance(fimOS);

					PersistEngine.persist(docOS);

					EntityWrapper wDocOS = new EntityWrapper(docOS);

					NeoFile docOSFile = wDocOS.findGenericValue("file");

					ut.addSource(docOSFile.getAsFile());

				}
				else
				{

					NeoObject docOS = AdapterUtils.createNewEntityInstance("rhTpGenerico");

					PersistEngine.persist(docOS);

					EntityWrapper wDocOS = new EntityWrapper(docOS);

					NeoFile docOSFile = wDocOS.findGenericValue("file");

					ut.addSource(docOSFile.getAsFile());

				}

				if (codTipAdm == 1)
				{//se AdmissÃ£o for administrativa adiciona documentaÃ§Ã£o de IntegraÃ§Ã£o

					//adiciona documento de integraÃ§Ã£o
					NeoObject docItegracao = AdapterUtils.createNewEntityInstance("rhIntegracao");

					PersistEngine.persist(docItegracao);

					EntityWrapper wDocIntegracao = new EntityWrapper(docItegracao);

					NeoFile docIntegracaoFile = wDocIntegracao.findGenericValue("file");

					ut.addSource(docIntegracaoFile.getAsFile());

				}

			}

			contSizeListaDocs++;

		}

		NeoFile nomeContrato = NeoStorage.createFile("Contrato_" + numemp + "_" + numcad + ".PDF");

		try
		{
			ut.setDestinationStream(new FileOutputStream(nomeContrato.getAsFile()));
			ut.mergeDocuments(); // Faz merge dos arquivos.
			processEntity.findField("documentacao.docContrato").setValue(nomeContrato);

			//exclusÃ£o dos docs antigos

			for (String arquivos : listaDocsContratos)
			{

				File tempFile = new File(caminhoPadrao + "CONTRATOS\\" + arquivos);

				tempFile.delete();

			}

		}
		catch (COSVisitorException | IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//geraÃ§Ã£o da etiqueta

		List<NeoObject> diretorioEtiqueta = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("rhDiretorioDocs"), new QLRawFilter("tipoDoc like 'etiqueta'"));
		for (NeoObject dir : diretorioEtiqueta)
		{

			EntityWrapper wDir = new EntityWrapper(dir);

			prDir = wDir.findGenericValue("diretorio");

		}

		GregorianCalendar dtAdmissao = processEntity.findGenericValue("dtAdmissao");

		String sDtAdmissao = sdf.format(dtAdmissao.getTime());

		prFileName = "Etiqueta_" + numemp + "_" + numcad;

		prEntrada = "<EMosCol=S><EDatIni=" + sDtAdmissao + "><EDatFin=" + sDtAdmissao + "><EAbrEmp=" + numemp + "><EAbrTip=1><EAbrCad=" + numcad + ">";

		integrarRelatIn.setPrRelatorio("FPEG404.EPT");
		integrarRelatIn.setPrExecFmt(prExecFmt);
		integrarRelatIn.setPrEntrada(prEntrada);
		integrarRelatIn.setPrEntranceIsXML("F");
		integrarRelatIn.setPrSaveFormat(prSaveFormat);
		integrarRelatIn.setPrDir(prDir);
		integrarRelatIn.setPrFileName(prFileName);

		G5SeniorServicesLocatorFPRel glCont = new G5SeniorServicesLocatorFPRel();

		try
		{
			integrarRelatOut = glCont.getrubi_Synccom_senior_g5_rh_fp_relatoriosPort().relatorios(config.getUserService(), config.getPasswordService(), 0, integrarRelatIn);
		}
		catch (RemoteException | ServiceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		erroCon = integrarRelatOut.getErroExecucao();

		if (erroCon != null)
		{

			erroCon = prRelatorio + " - " + erroCon;

			if (erroWF != null)
			{

				erroWF = erroWF + " - " + erroCon;

			}
			else
			{

				erroWF = erroCon;

			}
			log.warn("Erro no: " + prFileName);
			System.out.println("Erro no: " + prFileName);
			contErros++;
		}
		else
		{

			NeoFile neoFileEtiqueta = null;

			log.warn("Etiqueta: " + prFileName + " - GERADO COM SUCESSO!");
			System.out.println("Etiqueta: " + prFileName + " - GERADO COM SUCESSO!");

			File fileEtiqueta = new File(prDir + "\\" + prFileName + ".PDF");

			if (fileEtiqueta.exists())
			{

				neoFileEtiqueta = createNeoFile(fileEtiqueta, fileEtiqueta.getName());

			}

			NeoObject etiquetaOBJ = processEntity.findGenericValue("documentacao");

			new EntityWrapper(etiquetaOBJ).setValue("docEtiqueta", neoFileEtiqueta);

			fileEtiqueta.delete();

		}

		//<EMosCol=S><EDatIni=dtadmissao><EDatFin=dtfutura><EAbrEmp=empresa><EAbrTip=1><EAbrCad=numcad>

		if (contErros > 0)
		{
			Integer qtdSemInfoAListar = StringUtils.countMatches(erroWF, "listar");

			if (qtdSemInfoAListar >= 5)
			{
				processEntity.setValue("errosIntegracao", "A geração de documentos do Rubi está temporariamente indisponível. Aguarde alguns minutos e tente novamente mais tarde.");
				processEntity.setValue("converterErroIntegracao", erroWF);
			}	
			else
				processEntity.setValue("errosIntegracao", erroWF);

			processEntity.findField("contErros").setValue(new Long(contErros));

			processEntity.findField("validaIntegracao").setValue(Boolean.FALSE);

			processEntity.findField("exec2").setValue(userOrigin);

		}
		else
		{

			processEntity.findField("validaIntegracao").setValue(Boolean.TRUE);

			processEntity.findField("contErros").setValue(new Long(contErros));

			processEntity.findField("tarefaAtual").setValue("3. Imprimir Crachá - RH");

		}

	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}

	public Task retornaTarefaAnterior(Activity atividade)
	{
		Task tarefaAnterior = null;

		GregorianCalendar ultimaDataFim = null;

		List<Task> tarefasAnteriores = new WorkflowHelper().getTaskOrigins(atividade);

		if (tarefasAnteriores != null && tarefasAnteriores.size() > 0)
		{
			for (Task tarefa : tarefasAnteriores)
			{
				if (ultimaDataFim == null || tarefa.getActivity().getFinishDate().after(ultimaDataFim))
				{
					ultimaDataFim = tarefa.getActivity().getFinishDate();

					tarefaAnterior = tarefa;
				}
			}
		}

		return tarefaAnterior;
	}

	public NeoFile createNeoFile(final File file, final String name)
	{
		NeoFile neoFile = new NeoFile();
		PersistEngine.persist(neoFile);
		neoFile.setName(name);
		neoFile.setStorage(NeoStorage.getDefault());

		try
		{
			org.apache.commons.io.FileUtils.copyFile(file, neoFile.getStorage().getFile(neoFile));
			//file.delete();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		return neoFile;
	}

	public static void sortNeoId(Collection<? extends NeoObject> list)
	{
		Collections.sort((List<? extends NeoObject>) list, new Comparator<NeoObject>()
		{

			public int compare(NeoObject obj1, NeoObject obj2)
			{

				return ((Long) new EntityWrapper(obj1).findGenericValue("ordenacao") < (Long) new EntityWrapper(obj2).findGenericValue("ordenacao")) ? -1 : 1;
			}
		});
	}

}
