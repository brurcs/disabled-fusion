/**
 * ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.neomind.fusion.custom.orsegups.contract.clientws.cliente;

public class ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega  implements java.io.Serializable {
    private java.lang.String baiEnt;

    private java.lang.String cepEnt;

    private java.lang.String cgcEnt;

    private java.lang.String cidEnt;

    private java.lang.String cplEnt;

    private java.lang.String endEnt;

    private java.lang.String estEnt;

    private java.lang.String iniEnt;

    private java.lang.String insEnt;

    private java.lang.String nenEnt;

    private java.lang.String nomCli;

    private java.lang.String prxEnt;

    private java.lang.Integer seqEnt;

    private java.lang.String sitReg;

    public ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega() {
    }

    public ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega(
           java.lang.String baiEnt,
           java.lang.String cepEnt,
           java.lang.String cgcEnt,
           java.lang.String cidEnt,
           java.lang.String cplEnt,
           java.lang.String endEnt,
           java.lang.String estEnt,
           java.lang.String iniEnt,
           java.lang.String insEnt,
           java.lang.String nenEnt,
           java.lang.String nomCli,
           java.lang.String prxEnt,
           java.lang.Integer seqEnt,
           java.lang.String sitReg) {
           this.baiEnt = baiEnt;
           this.cepEnt = cepEnt;
           this.cgcEnt = cgcEnt;
           this.cidEnt = cidEnt;
           this.cplEnt = cplEnt;
           this.endEnt = endEnt;
           this.estEnt = estEnt;
           this.iniEnt = iniEnt;
           this.insEnt = insEnt;
           this.nenEnt = nenEnt;
           this.nomCli = nomCli;
           this.prxEnt = prxEnt;
           this.seqEnt = seqEnt;
           this.sitReg = sitReg;
    }


    /**
     * Gets the baiEnt value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @return baiEnt
     */
    public java.lang.String getBaiEnt() {
        return baiEnt;
    }


    /**
     * Sets the baiEnt value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @param baiEnt
     */
    public void setBaiEnt(java.lang.String baiEnt) {
        this.baiEnt = baiEnt;
    }


    /**
     * Gets the cepEnt value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @return cepEnt
     */
    public java.lang.String getCepEnt() {
        return cepEnt;
    }


    /**
     * Sets the cepEnt value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @param cepEnt
     */
    public void setCepEnt(java.lang.String cepEnt) {
        this.cepEnt = cepEnt;
    }


    /**
     * Gets the cgcEnt value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @return cgcEnt
     */
    public java.lang.String getCgcEnt() {
        return cgcEnt;
    }


    /**
     * Sets the cgcEnt value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @param cgcEnt
     */
    public void setCgcEnt(java.lang.String cgcEnt) {
        this.cgcEnt = cgcEnt;
    }


    /**
     * Gets the cidEnt value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @return cidEnt
     */
    public java.lang.String getCidEnt() {
        return cidEnt;
    }


    /**
     * Sets the cidEnt value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @param cidEnt
     */
    public void setCidEnt(java.lang.String cidEnt) {
        this.cidEnt = cidEnt;
    }


    /**
     * Gets the cplEnt value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @return cplEnt
     */
    public java.lang.String getCplEnt() {
        return cplEnt;
    }


    /**
     * Sets the cplEnt value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @param cplEnt
     */
    public void setCplEnt(java.lang.String cplEnt) {
        this.cplEnt = cplEnt;
    }


    /**
     * Gets the endEnt value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @return endEnt
     */
    public java.lang.String getEndEnt() {
        return endEnt;
    }


    /**
     * Sets the endEnt value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @param endEnt
     */
    public void setEndEnt(java.lang.String endEnt) {
        this.endEnt = endEnt;
    }


    /**
     * Gets the estEnt value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @return estEnt
     */
    public java.lang.String getEstEnt() {
        return estEnt;
    }


    /**
     * Sets the estEnt value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @param estEnt
     */
    public void setEstEnt(java.lang.String estEnt) {
        this.estEnt = estEnt;
    }


    /**
     * Gets the iniEnt value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @return iniEnt
     */
    public java.lang.String getIniEnt() {
        return iniEnt;
    }


    /**
     * Sets the iniEnt value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @param iniEnt
     */
    public void setIniEnt(java.lang.String iniEnt) {
        this.iniEnt = iniEnt;
    }


    /**
     * Gets the insEnt value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @return insEnt
     */
    public java.lang.String getInsEnt() {
        return insEnt;
    }


    /**
     * Sets the insEnt value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @param insEnt
     */
    public void setInsEnt(java.lang.String insEnt) {
        this.insEnt = insEnt;
    }


    /**
     * Gets the nenEnt value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @return nenEnt
     */
    public java.lang.String getNenEnt() {
        return nenEnt;
    }


    /**
     * Sets the nenEnt value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @param nenEnt
     */
    public void setNenEnt(java.lang.String nenEnt) {
        this.nenEnt = nenEnt;
    }


    /**
     * Gets the nomCli value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @return nomCli
     */
    public java.lang.String getNomCli() {
        return nomCli;
    }


    /**
     * Sets the nomCli value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @param nomCli
     */
    public void setNomCli(java.lang.String nomCli) {
        this.nomCli = nomCli;
    }


    /**
     * Gets the prxEnt value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @return prxEnt
     */
    public java.lang.String getPrxEnt() {
        return prxEnt;
    }


    /**
     * Sets the prxEnt value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @param prxEnt
     */
    public void setPrxEnt(java.lang.String prxEnt) {
        this.prxEnt = prxEnt;
    }


    /**
     * Gets the seqEnt value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @return seqEnt
     */
    public java.lang.Integer getSeqEnt() {
        return seqEnt;
    }


    /**
     * Sets the seqEnt value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @param seqEnt
     */
    public void setSeqEnt(java.lang.Integer seqEnt) {
        this.seqEnt = seqEnt;
    }


    /**
     * Gets the sitReg value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @return sitReg
     */
    public java.lang.String getSitReg() {
        return sitReg;
    }


    /**
     * Sets the sitReg value for this ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.
     * 
     * @param sitReg
     */
    public void setSitReg(java.lang.String sitReg) {
        this.sitReg = sitReg;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega)) return false;
        ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega other = (ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.baiEnt==null && other.getBaiEnt()==null) || 
             (this.baiEnt!=null &&
              this.baiEnt.equals(other.getBaiEnt()))) &&
            ((this.cepEnt==null && other.getCepEnt()==null) || 
             (this.cepEnt!=null &&
              this.cepEnt.equals(other.getCepEnt()))) &&
            ((this.cgcEnt==null && other.getCgcEnt()==null) || 
             (this.cgcEnt!=null &&
              this.cgcEnt.equals(other.getCgcEnt()))) &&
            ((this.cidEnt==null && other.getCidEnt()==null) || 
             (this.cidEnt!=null &&
              this.cidEnt.equals(other.getCidEnt()))) &&
            ((this.cplEnt==null && other.getCplEnt()==null) || 
             (this.cplEnt!=null &&
              this.cplEnt.equals(other.getCplEnt()))) &&
            ((this.endEnt==null && other.getEndEnt()==null) || 
             (this.endEnt!=null &&
              this.endEnt.equals(other.getEndEnt()))) &&
            ((this.estEnt==null && other.getEstEnt()==null) || 
             (this.estEnt!=null &&
              this.estEnt.equals(other.getEstEnt()))) &&
            ((this.iniEnt==null && other.getIniEnt()==null) || 
             (this.iniEnt!=null &&
              this.iniEnt.equals(other.getIniEnt()))) &&
            ((this.insEnt==null && other.getInsEnt()==null) || 
             (this.insEnt!=null &&
              this.insEnt.equals(other.getInsEnt()))) &&
            ((this.nenEnt==null && other.getNenEnt()==null) || 
             (this.nenEnt!=null &&
              this.nenEnt.equals(other.getNenEnt()))) &&
            ((this.nomCli==null && other.getNomCli()==null) || 
             (this.nomCli!=null &&
              this.nomCli.equals(other.getNomCli()))) &&
            ((this.prxEnt==null && other.getPrxEnt()==null) || 
             (this.prxEnt!=null &&
              this.prxEnt.equals(other.getPrxEnt()))) &&
            ((this.seqEnt==null && other.getSeqEnt()==null) || 
             (this.seqEnt!=null &&
              this.seqEnt.equals(other.getSeqEnt()))) &&
            ((this.sitReg==null && other.getSitReg()==null) || 
             (this.sitReg!=null &&
              this.sitReg.equals(other.getSitReg())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBaiEnt() != null) {
            _hashCode += getBaiEnt().hashCode();
        }
        if (getCepEnt() != null) {
            _hashCode += getCepEnt().hashCode();
        }
        if (getCgcEnt() != null) {
            _hashCode += getCgcEnt().hashCode();
        }
        if (getCidEnt() != null) {
            _hashCode += getCidEnt().hashCode();
        }
        if (getCplEnt() != null) {
            _hashCode += getCplEnt().hashCode();
        }
        if (getEndEnt() != null) {
            _hashCode += getEndEnt().hashCode();
        }
        if (getEstEnt() != null) {
            _hashCode += getEstEnt().hashCode();
        }
        if (getIniEnt() != null) {
            _hashCode += getIniEnt().hashCode();
        }
        if (getInsEnt() != null) {
            _hashCode += getInsEnt().hashCode();
        }
        if (getNenEnt() != null) {
            _hashCode += getNenEnt().hashCode();
        }
        if (getNomCli() != null) {
            _hashCode += getNomCli().hashCode();
        }
        if (getPrxEnt() != null) {
            _hashCode += getPrxEnt().hashCode();
        }
        if (getSeqEnt() != null) {
            _hashCode += getSeqEnt().hashCode();
        }
        if (getSitReg() != null) {
            _hashCode += getSitReg().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientesGravarClientesInDadosGeraisClienteEnderecosEntrega.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesGravarClientesInDadosGeraisClienteEnderecosEntrega"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baiEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "baiEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cepEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cgcEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cgcEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cidEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cidEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cplEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cplEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("iniEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "iniEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "insEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nenEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nenEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prxEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prxEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitReg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitReg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
