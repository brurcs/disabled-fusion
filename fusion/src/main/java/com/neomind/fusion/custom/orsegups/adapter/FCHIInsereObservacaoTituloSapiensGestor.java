package com.neomind.fusion.custom.orsegups.adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class FCHIInsereObservacaoTituloSapiensGestor implements AdapterInterface
{

	private static final Log log = LogFactory.getLog(FCHIInsereObservacaoTituloSapiensGestor.class);


	public void back(EntityWrapper entitywrapper, Activity activity1)
	{
	}

	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		System.out.println("Codigo FCIInsereObservacaoTituloSapiensGestor start Inicio: ");
		String codigo = activity.getCode();
		List<NeoObject> listaTitulos = (List<NeoObject>) processEntity.findField("listTitulos").getValue();
		String texto = "";
		if (listaTitulos != null && !listaTitulos.isEmpty()) {
			try	{
				Boolean conseguiuContato = (Boolean) processEntity.findValue("conseguiuContatoCliente");
				if (conseguiuContato) {
					texto = (String) processEntity.findValue("obsNegocGestor");
				} else {
					texto = (String) processEntity.findValue("justifiqueGestor");
				}
				NeoUser usuario = null;
				String usuarioSapiens = "";
				NeoPaper  paper = (NeoPaper) processEntity.findValue("gerenteRegional");
				for(NeoUser user : paper.getAllUsers()){
					if(NeoUtils.safeIsNotNull(user)){
						usuario = user;
						usuarioSapiens = usuario.getCode();
						break;
					}
				}
			
				if (texto.length() <= 245) {
					GregorianCalendar dataHora = new GregorianCalendar();
					GregorianCalendar data = new GregorianCalendar( dataHora.get(GregorianCalendar.YEAR), dataHora.get(GregorianCalendar.MONTH), dataHora.get(GregorianCalendar.DAY_OF_MONTH));
					
					Long codigoUsuarioSapiens = buscaUsuarioSapiensCorrespondente(usuarioSapiens, codigo);
				
					insereObservacaoTituloSapiens(listaTitulos, data, dataHora, texto, codigoUsuarioSapiens, codigo);
				} else {
					throw new WorkflowException("Os Campos Conseguiu contato com esse Cliente? /ou Justificativa Negociação Possui mais de 245 Caracteres");
				}

			} catch (WorkflowException we)	{
				System.out.println("Contrato que apresentou problema no método start. Classe FCHIInsereObservacaoTituloSapiens:");
				throw we;
			} catch (Exception e) {
				e.printStackTrace();
				throw new WorkflowException(e.getMessage());
			}						
		}
		System.out.println("Codigo FCIInsereObservacaoTituloSapiensGestor start Fim: "+codigo);
			
	}



	@SuppressWarnings("unchecked")
	public void insereObservacaoTituloSapiens(List<NeoObject> listaTitulos, GregorianCalendar data, GregorianCalendar dataHora, String texto, Long usuario, String codigo) throws Exception
	{
		try {
			System.out.println("Inicio Codigo FCIInsereObservacaoTituloSapiensGestor classe insereObservacaoTituloSapiens Inicio: "+codigo);
			for (NeoObject titulo : listaTitulos) {
				EntityWrapper wrapper = new EntityWrapper(titulo);
				Long empresa = (Long) wrapper.findValue("empresa");
				Long filial = (Long) wrapper.findValue("filial");
				String numeroTitulo = (String) wrapper.findValue("numeroTitulo");
				String tipoTitulo = (String) wrapper.findValue("tipoTitulo");
				Integer sequencial = 1;
				
				String nomeFonteDados = "SAPIENS";
				
				String sqlSelect = "SELECT (CASE WHEN max(seqmov) > 0 THEN MAX(seqmov)+1 ELSE 1 END) FROM E301MOR WHERE codemp = :codemp AND codfil = :codfil AND numtit = :numtit AND codtpt = :codtpt ";
				
				try{
					Query query = PersistEngine.getEntityManager(nomeFonteDados).createNativeQuery(sqlSelect);
					query.setParameter("codemp", empresa);
					query.setParameter("codfil", filial);
					query.setParameter("numtit", numeroTitulo);
					query.setParameter("codtpt", tipoTitulo);
					List<Object> resultList = query.getResultList();
					
					if(resultList != null && !resultList.isEmpty())	{
						sequencial = (Integer)resultList.get(0);
					}
				
				}
				catch (Exception ex)
				{
					System.out.println("Contrato que apresentou problema no Exception do Select. Classe FCHIInsereObservacaoTituloSapiens no método insereObservacaoTituloSapiens: "+numeroTitulo);
					ex.printStackTrace();
					throw new WorkflowException("Sistema Sapiens indisponível para inserir observação no título, tente novamente mais tarde!");
				}
				
				Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
				
				String sql = "INSERT INTO E301MOR (CodEmp, CodFil, NumTit, CodTpt, SeqMov, TipObs, ObsTit, UsuMov, DatMov, HorMov) ";
				sql += "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, (DATEPART(HOUR, ?) * 60) + DATEPART(MINUTE, ?)) ";
				PreparedStatement st = null;
				try
				{	
					st = connection.prepareStatement(sql);
					st.setLong(1, empresa);
					st.setLong(2, filial);
					st.setString(3, numeroTitulo);
					st.setString(4, tipoTitulo);
					st.setInt(5, sequencial);
					st.setString(6, "M");
					st.setString(7, texto);
					st.setLong(8, usuario);
					st.setTimestamp(9, new Timestamp(data.getTimeInMillis()));
					st.setTimestamp(10, new Timestamp(dataHora.getTimeInMillis()));
					st.setTimestamp(11, new Timestamp(dataHora.getTimeInMillis()));
					System.out.println("INSERT INTO E301MOR (CodEmp, CodFil, NumTit, CodTpt, SeqMov, TipObs, ObsTit, UsuMov, DatMov, HorMov) VALUES ("+empresa+", "+filial+", "+numeroTitulo+", "+tipoTitulo+", "+sequencial+", 'M', "+texto+", "+usuario+", "+new Timestamp(data.getTimeInMillis())+", (DATEPART(HOUR, "+new Timestamp(dataHora.getTimeInMillis())+") * 60) + DATEPART(MINUTE, "+new Timestamp(dataHora.getTimeInMillis())+")) ");
					
					st.executeUpdate();
				}
				catch (SQLException e)
				{ 
					System.out.println("Contrato que apresentou problema no SQLException. Classe FCHIInsereObservacaoTituloSapiens no método insereObservacaoTituloSapiens: "+numeroTitulo);
					e.printStackTrace();
					throw new WorkflowException("Sistema Sapiens indisponível para inserir observação no título, tente novamente mais tarde!");
				}
				catch (Exception ex)
				{
					System.out.println("Contrato que apresentou problema no Exception. Classe FCHIInsereObservacaoTituloSapiens no método insereObservacaoTituloSapiens: :  "+numeroTitulo);
					throw new WorkflowException("Sistema Sapiens indisponível para inserir observação no título, tente novamente mais tarde!");
				}
				finally
				{
					if (st != null)
					{
						try
						{
							st.close();
							connection.close();
						}
						catch (SQLException e)
						{
							log.error("Erro ao fechar o statement");
							e.printStackTrace();
							throw e;
						}
					}
				}
	
			}
			System.out.println("Fim Codigo FCIInsereObservacaoTituloSapiensGestor classe insereObservacaoTituloSapiens Fim: "+codigo);
		}
		catch (WorkflowException e)
		{ 
			System.out.println("Erro ao Inserir Observação no Sapiens. Classe FCHIInsereObservacaoTituloSapiens no método insereObservacaoTituloSapiens");
			e.printStackTrace();
			throw new WorkflowException("Sistema Sapiens indisponível para inserir observação no título, tente novamente mais tarde!");
		}
	}

	private Long buscaUsuarioSapiensCorrespondente(String userCode, String codigo) throws Exception
	{
		//Logica para buscar codigo do usuario do sapiens referente ao usuario fusion
		System.out.println("Codigo FCIInsereObservacaoTituloSapiensGestor classe buscaUsuarioSapiensCorrespondente Inicio: "+codigo);
		Long sapiensUserCode = 0L;
		NeoObject sapiensUser = null;
		InstantiableEntityInfo ieiEX = AdapterUtils.getInstantiableEntityInfo("SAPIENSUSUARIO");
		String login = userCode;

		QLEqualsFilter loginFilter = new QLEqualsFilter("nomusu", login);
		@SuppressWarnings("unchecked")
		ArrayList<NeoObject> sapiensUsers = (ArrayList<NeoObject>) PersistEngine.getObjects(ieiEX.getEntityClass(), loginFilter);

		if (sapiensUsers != null && sapiensUsers.size() > 0)
		{
			sapiensUser = sapiensUsers.get(0);
		}

		if (sapiensUser != null)
		{
			EntityWrapper sapiensUserEw = new EntityWrapper(sapiensUser);
			sapiensUserCode = (Long)sapiensUserEw.findValue("codusu");
		}

		if (sapiensUserCode == null || sapiensUserCode == 0L)
		{
			log.error("Não foi possivel buscar o codigo de usuario sapiens referente ao usuário logado no Fusion - " + userCode);
			throw new WorkflowException("Não foi possivel buscar o codigo de usuario sapiens referente ao usuário logado no Fusion.");
		}
		System.out.println("Codigo FCIInsereObservacaoTituloSapiensGestor classe buscaUsuarioSapiensCorrespondente Fim: "+codigo);
		return sapiensUserCode;
	}

	
}

