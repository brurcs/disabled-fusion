package com.neomind.fusion.custom.orsegups.agendavisita;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.adapter.casvig.OrsegupsContratoUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.fusion.workflow.simulation.WorkflowService;
import com.neomind.util.NeoUtils;

public class AgendaVisitaComercialJob implements CustomJobAdapter {

	@Override
	public void execute(CustomJobContext ctx) {
		if (OrsegupsUtils.isWorkDay(new GregorianCalendar())){
			System.out.println("[C036] - iniciando job de agendamento de visitas do comercial" );
			processaJob(ctx);
		}
	}
	
	public static void processaJob(CustomJobContext ctx){

		String queryGerentes = "select reg.usu_codreg, usu.NomUsu from usu_t200reg reg " +
				" inner join r999usu usu on usu.CodUsu = reg.usu_gerreg and reg.usu_codreg not in (8,9,11,3,5) " + 
				" order by reg.usu_codreg  ";
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		try
		{
			Long usu_codreg = 0L;
			String usu_login = "";

			int qtdTarefas = 4;

			/*
			 * Connection connection = OrsegupsUtils.getSqlConnection("SAPIENS") ;
			 * ResultSet rs = OrsegupsContratoUtils.getResultSet( queryGerentes , "SAPIENS");
			 * if(rs!= null){
			 * do{ // para cada gerente de regional
			 * usu_codreg = rs.getLong(1);
			 * usu_login = rs.getString(2);
			 * if (usu_codreg == 13){
			 * usu_login = "juarez.fernandes";
			 * qtdTarefas = 2;
			 * }else if (usu_codreg == 15){
			 * usu_login = "lauro.ramos";
			 * qtdTarefas = 0;
			 * }else if ( usu_codreg == 14){
			 * //usu_login = "juilson.vieira"; vai para cleci.pagnussatti. Solcitidado Sr. valtair
			 * 10/09/2014
			 * qtdTarefas = 2;
			 * }else{
			 * qtdTarefas = 2;
			 * }
			 * if (usu_login.equals("cleci.pagnussatti") ){
			 * qtdTarefas = 1;
			 * }
			 * System.out.println("[C036] + Regional: " + usu_codreg + ", Usuário: " + usu_login +
			 * ", tarefas:" + qtdTarefas);
			 * abreTarefas(usu_codreg, usu_login, qtdTarefas);
			 * }while(rs.next());
			 * }
			 */

			// Abre para os Inspetores
			ArrayList<NeoObject> listaInspetores = (ArrayList<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("AVRUsuariosAbrirTarefa"));
			qtdTarefas = 4;
			boolean isEletronica = false;
			for (NeoObject inspetor: listaInspetores)
			{
				EntityWrapper wInspetor = new EntityWrapper(inspetor);
				usu_codreg = NeoUtils.safeLong(NeoUtils.safeOutputString(wInspetor.findValue("codreg")));
				usu_login = NeoUtils.safeOutputString(wInspetor.findValue("usufus"));
				GregorianCalendar dataIniSus = new GregorianCalendar();
				GregorianCalendar dataFimSus = new GregorianCalendar();
				dataIniSus = (GregorianCalendar) wInspetor.findValue("DataSuspensaoIni");
				dataFimSus = (GregorianCalendar) wInspetor.findValue("DataSuspensaoFim");
				if(wInspetor.findValue("isEletronica") != null){
					isEletronica = (boolean) wInspetor.findValue("isEletronica");
				}
				
				GregorianCalendar dataAtual = new GregorianCalendar();
				dataAtual.set(GregorianCalendar.HOUR_OF_DAY, 0);
				dataAtual.set(GregorianCalendar.MINUTE, 0);
				dataAtual.set(GregorianCalendar.SECOND, 0);

				if ((dataAtual.compareTo(dataIniSus) < 0 && dataAtual.compareTo(dataFimSus) < 0) || (dataAtual.compareTo(dataIniSus) > 0 && dataAtual.compareTo(dataFimSus) > 0))
				{

					/*
					 * if (usu_login != null && "geraldo.santos".equals(usu_login)){
					 * qtdTarefas = 6;
					 * }
					 */

					if (wInspetor.findValue("quantidadeTarefas") != null)
					{
						qtdTarefas = NeoUtils.safeInteger(NeoUtils.safeOutputString(wInspetor.findValue("quantidadeTarefas")));
					}
					//if(!usu_login.equals("carlos.machado")){
					System.out.println(usu_login);
					abreTarefas(usu_codreg, usu_login, qtdTarefas, isEletronica);
					//}
				}

			}

			/*
			 * //Para pegar as informaçõs dos usuários de um papel
			 * usu_codreg = 0L;
			 * usu_login = "";
			 * // abrir para o segundo superintendente
			 * NeoPaper papelSuperintendente = PersistEngine.getObject(NeoPaper.class, new
			 * QLEqualsFilter("code", "Superintendente Comercial Paraná"));
			 * Set usuarios = papelSuperintendente.getUsers();
			 * Iterator ite = usuarios.iterator();
			 * while(ite!= null && ite.hasNext()){
			 * NeoUser usr = (NeoUser) ite.next();
			 * System.out.println("[C036] - usr-> " + usr.getCode() );
			 * abreTarefas(usu_codreg, usu_login, 2);
			 * }
			 */

		}
		catch (Exception e)
		{
			System.out.println("[" + key + "]Erro ao processar agendamento de visitas ");
			e.printStackTrace();
			throw new JobException("Erro ao processar agendamento de visitas. Procure no log por [" + key + "] ");
		}

	}

	/**
	 * preenche os dados do formulario e inicia a tarefa
	 * 
	 * @param usu_codreg
	 * @param usu_login
	 * @param qtdTarefas
	 * @throws Exception
	 */
	private static void abreTarefas(Long usu_codreg, String usu_login, int qtdTarefas, boolean isEletronica) throws Exception
	{

		for (int c = 0; c < qtdTarefas; c++){   
			
			NeoObject eformProcesso = eformProcesso = AdapterUtils.createNewEntityInstance("AVRPrincipal");
			EntityWrapper wEformProcesso = new EntityWrapper(eformProcesso);

			GregorianCalendar dataAgenda = new GregorianCalendar();//OrsegupsUtils.getNextWorkDay(new GregorianCalendar());
			dataAgenda.set(dataAgenda.HOUR_OF_DAY, 00);
			dataAgenda.set(dataAgenda.MINUTE, 00);
			dataAgenda.set(dataAgenda.SECOND, 00);
			dataAgenda.set(dataAgenda.MILLISECOND, 00);

			GregorianCalendar dataPrazo = new GregorianCalendar();
			dataPrazo.add(GregorianCalendar.DAY_OF_MONTH, 4);
			dataPrazo.set(dataPrazo.HOUR_OF_DAY, 23);
			dataPrazo.set(dataPrazo.MINUTE, 59);
			dataPrazo.set(dataPrazo.SECOND, 59);
			dataPrazo.set(dataPrazo.MILLISECOND, 59);
			
			wEformProcesso.setValue("isEletronica",isEletronica); 
			wEformProcesso.setValue("dataAgenda", dataAgenda);
			wEformProcesso.setValue("prazoVisita", OrsegupsUtils.getNextWorkDay(dataPrazo));

			QLGroupFilter gp = new QLGroupFilter("AND");
			gp.addFilter(new QLEqualsFilter("usu_codreg", usu_codreg));

			ArrayList<NeoObject> oRegionais = (ArrayList<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSUSUREG"), gp);
			NeoObject oRegional = null;
			if (oRegionais != null && !oRegionais.isEmpty())
			{
				oRegional = oRegionais.get(0);
				wEformProcesso.setValue("regional", oRegional);
			}

			String nomePapelSuperintendente = "";
			if (usu_codreg == 13 || usu_codreg == 14 || usu_codreg == 15)
			{
				nomePapelSuperintendente = "Superintendente Comercial Paraná";
			}
			else
			{
				//nomePapelSuperintendente = "Superintendente Regional";
				nomePapelSuperintendente = "Superintendente Comercial Paraná"; // sempre juilson
			}

			//NeoUser papelGerenteComercial = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", usu_login));
			NeoUser papelGerenteComercial = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", usu_login));
			NeoPaper papelSuperintendente = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", nomePapelSuperintendente));
			NeoPaper papelDiretorComercial = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "Diretor de Planejamento"));

			//wEformProcesso.setValue("papelGerenteComercial", papelGerenteComercial);
			wEformProcesso.setValue("usuarioGerenteComercial", papelGerenteComercial);
			wEformProcesso.setValue("papelSuperIntendente", papelSuperintendente);
			wEformProcesso.setValue("papelDirComercial", papelDiretorComercial);

			PersistEngine.persist(eformProcesso);

			NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "gilsoncesar"));
			if (solicitante == null)
			{
				System.out.println(" usuario não encontrado no fusion. " + usu_login);
			}
			else
			{
				//NeoUser responsavel = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "giliardi") );
				//NeoUser responsavel = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", "danilo.silva") );
				NeoUser responsavel = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", usu_login));

				iniciaProcessoTarefaAgendametoVisita(eformProcesso, solicitante, responsavel, dataAgenda);
				Thread.sleep(200);

				Thread.sleep(200);
			}
			//break;
		}

	}

	/**
	 * Inicia o workflow de Agendamento de Visita e retorna <b>true</b> se o workflow foi iniciado com
	 * suscesso
	 * ou <b>false</b> caso ocorra algum erro.
	 * 
	 * @param NeoObject eformProcesso Eform do processo de tarefa simples ja com seus valores peenchidos
	 * @param NeoUser responsavel Usuario responsãvel pelo workflow que estã sendo iniciado
	 * @return Boolean true se for iniciado corretamente e false caso ocorra algum erro
	 */
	public static Boolean iniciaProcessoTarefaAgendametoVisita(NeoObject eformProcesso, NeoUser solicitante, NeoUser responsavel, GregorianCalendar startDate)
	{
		Boolean result = false;
		try{
			QLEqualsFilter equal = new QLEqualsFilter("Name", "C036 - Agendamento de Visita Regional");
			ProcessModel processModel = (ProcessModel) PersistEngine.getObject(ProcessModel.class, equal);

			/**
			 * @author neomind willian.mews - Alterado para utilizar a nova arquitetura de processos do
			 *         Fusion
			 * @date 12/03/2015
			 */

			final WFProcess processo = WorkflowService.startProcess(processModel, eformProcesso, false, responsavel);

			/**
			 * FIM ALTERAÇÕES - NEOMIND
			 */

			System.out.println("[C036] + Responsavel: " + responsavel.getCode() + " Tarefa " + processo.getCode() + " Aberta.");

			result = true;
		}
		catch (Exception e)
		{
			result = false;
		}

		return result;
	}

}
