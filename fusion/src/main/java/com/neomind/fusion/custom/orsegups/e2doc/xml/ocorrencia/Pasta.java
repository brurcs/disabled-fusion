package com.neomind.fusion.custom.orsegups.e2doc.xml.ocorrencia;


public class Pasta {
    private Indices indices;
    private Doc doc;
    
    public Indices getIndices() {
        return indices;
    }
    public void setIndices(Indices indices) {
        this.indices = indices;
    }
    public Doc getDoc() {
        return doc;
    }
    public void setDoc(Doc doc) {
        this.doc = doc;
    }

    
    
}
