package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.presenca.vo.ContaSapiensVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.ContaSigmaVO;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoDateUtils;

public class RotinaAbreTarefaSimplesConciliacaoSigmaSapiens implements CustomJobAdapter {

    final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.RotinaAbreTarefaSimplesConciliacaoSigmaSapiens");

    private Long key = GregorianCalendar.getInstance().getTimeInMillis();

    public void execute(CustomJobContext ctx) {

	log.warn("INICIAR AGENDADOR DE TAREFA: RotinaCobrancaOS -- Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	ArrayList<ContaSigmaVO> contasSigma = null;
	ArrayList<ContaSapiensVO> contasSapiensPrivado = null;
	ArrayList<ContaSapiensVO> contasSapiensPublico = null;

	// Monta lista Sigma x Sapiens
	try {

	    conn = PersistEngine.getConnection("SIGMA90");

	    contasSigma = new ArrayList<ContaSigmaVO>();

	    sql.append("SELECT DISTINCT cen.ID_CENTRAL, cen.PARTICAO, cen.RAZAO,CEN.FANTASIA, cen.CGCCPF, cen.ENDERECO, cen.ID_ESTADO, cen.CEP, ");
	    sql.append("CASE CEN.TP_PESSOA ");
	    sql.append("WHEN 2 THEN 'Público' ");
	    sql.append("ELSE 'Privado' ");
	    sql.append("END AS TIPOPESSOA ");
	    sql.append("FROM dbCENTRAL AS CEN WITH(NOLOCK) ");
	    sql.append("WHERE CEN.CTRL_CENTRAL=1 ");
	    sql.append("AND CEN.ID_CENTRAL NOT IN ('AAA5', 'AAA6') ");
	    sql.append("AND NOT EXISTS ( ");
	    sql.append("SELECT SIG.USU_CODCLI AS USU_CODCLI  FROM [FSOODB04\\SQL02].Sapiens.dbo.usu_t160sig AS sig WITH(NOLOCK) ");
	    sql.append("INNER JOIN [FSOODB04\\SQL02].Sapiens.dbo.usu_t160cvs AS cvs WITH(NOLOCK) ON sig.usu_codemp = cvs.usu_codemp AND sig.usu_codfil = cvs.usu_codfil ");
	    sql.append("AND sig.usu_numpos = cvs.usu_numpos AND sig.usu_numctr = cvs.usu_numctr ");
	    sql.append("WHERE SIG.USU_CODCLI = CEN.CD_CLIENTE AND ( CVS.USU_SITCVS = 'A' OR (CVS.USU_SITCVS = 'I' AND CVS.USU_DATFIM > GETDATE())) ");
	    sql.append(") ORDER BY TIPOPESSOA, RAZAO ");

	    pstm = conn.prepareStatement(sql.toString());

	    rs = pstm.executeQuery();

	    while (rs.next()) {
		ContaSigmaVO novaConta = new ContaSigmaVO();

		novaConta.setCentral(rs.getString("ID_CENTRAL"));
		novaConta.setParticao(rs.getString("PARTICAO"));
		novaConta.setRazao(rs.getString("RAZAO"));
		novaConta.setFantasia(rs.getString("FANTASIA"));
		novaConta.setCgccpf(rs.getString("CGCCPF") == null || rs.getString("CGCCPF").isEmpty() ? "Não informado" : rs.getString("CGCCPF"));

		contasSigma.add(novaConta);

	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("ERRO AGENDADOR DE TAREFA: RotinaAbreTarefaSimplesConciliacaoSigmaSapiens - ERRO AO EXECUTAR ROTINA -- Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	    sql = null;
	}

	// Monta lista Sapiens x Sigma Privado
	try {
	    conn = PersistEngine.getConnection("SAPIENS");

	    contasSapiensPrivado = new ArrayList<ContaSapiensVO>();

	    sql = new StringBuilder();

	    sql.append("SELECT  cvs.usu_codemp, cvs.usu_codfil, cvs.usu_numctr, cvs.usu_numpos, cli.NomCli, cli.CgcCpf, ");
	    sql.append("CASE WHEN cli.TipEmc = 1 THEN 'Privado' WHEN cli.TipEmc = 2 THEN 'Público' END AS TIPOCLIENTE ");
	    sql.append("FROM dbo.usu_t160cvs AS cvs ");
	    sql.append("LEFT OUTER JOIN dbo.usu_t160sig AS sig ON sig.usu_codemp = cvs.usu_codemp AND sig.usu_codfil = cvs.usu_codfil AND sig.usu_numctr = cvs.usu_numctr AND sig.usu_numpos = cvs.usu_numpos ");
	    sql.append("INNER JOIN dbo.usu_t160ctr AS ctr ON cvs.usu_codemp = ctr.usu_codemp AND cvs.usu_codfil = ctr.usu_codfil AND cvs.usu_numctr = ctr.usu_numctr ");
	    sql.append("INNER JOIN dbo.E085CLI AS cli ON cli.CodCli = ctr.usu_codcli ");
	    sql.append("WHERE (cvs.usu_sitcvs = 'A' OR (cvs.usu_sitcvs = 'I' AND cvs.usu_datfim > cast(floor(cast(getdate() as float)) as datetime))) AND cli.TipEmc = 1 ");
	    sql.append("AND (sig.usu_codcli IS NULL) ");
	                    

	    pstm = conn.prepareStatement(sql.toString());

	    rs = pstm.executeQuery();

	    while (rs.next()) {

		ContaSapiensVO novaConta = new ContaSapiensVO();
		novaConta.setCodEmp(rs.getInt("usu_codemp"));
		novaConta.setCodFil(rs.getInt("usu_codfil"));
		novaConta.setNumCtr(rs.getInt("usu_numctr"));
		novaConta.setNumPos(rs.getInt("usu_numpos"));
		novaConta.setNomCli(rs.getString("NomCli"));
		novaConta.setCgcCpf(rs.getString("CgcCpf"));
		novaConta.setTipoCliente(rs.getString("TipoCliente"));

		contasSapiensPrivado.add(novaConta);
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("ERRO AGENDADOR DE TAREFA: RotinaAbreTarefaSimplesConciliacaoSigmaSapiens - ERRO AO EXECUTAR ROTINA -- Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	    sql = null;
	}

	// Monta lista Sapiens x Sigma Publico
	try {
	    conn = PersistEngine.getConnection("SAPIENS");

	    contasSapiensPublico = new ArrayList<ContaSapiensVO>();

	    sql = new StringBuilder();

	    sql.append("SELECT  cvs.usu_codemp, cvs.usu_codfil, cvs.usu_numctr, cvs.usu_numpos, cli.NomCli, cli.CgcCpf, ");
	    sql.append("CASE WHEN cli.TipEmc = 1 THEN 'Privado' WHEN cli.TipEmc = 2 THEN 'Público' END AS TIPOCLIENTE ");
	    sql.append("FROM dbo.usu_t160cvs AS cvs ");
	    sql.append("LEFT OUTER JOIN dbo.usu_t160sig AS sig ON sig.usu_codemp = cvs.usu_codemp AND sig.usu_codfil = cvs.usu_codfil AND sig.usu_numctr = cvs.usu_numctr AND sig.usu_numpos = cvs.usu_numpos ");
	    sql.append("INNER JOIN dbo.usu_t160ctr AS ctr ON cvs.usu_codemp = ctr.usu_codemp AND cvs.usu_codfil = ctr.usu_codfil AND cvs.usu_numctr = ctr.usu_numctr ");
	    sql.append("INNER JOIN dbo.E085CLI AS cli ON cli.CodCli = ctr.usu_codcli ");
	    sql.append("WHERE (cvs.usu_sitcvs = 'A' OR (cvs.usu_sitcvs = 'I' AND cvs.usu_datfim > cast(floor(cast(getdate() as float)) as datetime))) AND cli.TipEmc = 2 ");
	    sql.append("AND (sig.usu_codcli IS NULL) ");

	    pstm = conn.prepareStatement(sql.toString());

	    rs = pstm.executeQuery();
	    while (rs.next()) {

		ContaSapiensVO novaConta = new ContaSapiensVO();
		novaConta.setCodEmp(rs.getInt("usu_codemp"));
		novaConta.setCodFil(rs.getInt("usu_codfil"));
		novaConta.setNumCtr(rs.getInt("usu_numctr"));
		novaConta.setNumPos(rs.getInt("usu_numpos"));
		novaConta.setNomCli(rs.getString("NomCli"));
		novaConta.setCgcCpf(rs.getString("CgcCpf"));
		novaConta.setTipoCliente(rs.getString("TipoCliente"));

		contasSapiensPublico.add(novaConta);
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("ERRO AGENDADOR DE TAREFA: RotinaAbreTarefaSimplesConciliacaoSigmaSapiens - ERRO AO EXECUTAR ROTINA -- Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	    sql = null;
	}

	this.criaDiretorio();

	if (!contasSigma.isEmpty()) {
	    this.montaRelatorioSigma(contasSigma);
	}

	if (!contasSapiensPrivado.isEmpty()) {
	    this.montaRelatorioSapiens(contasSapiensPrivado, "Privado");
	}

	if (!contasSapiensPublico.isEmpty()) {
	    this.montaRelatorioSapiens(contasSapiensPublico, "Publico");
	}

	if (!contasSigma.isEmpty() && !contasSapiensPrivado.isEmpty()) {
	    String[] srcFiles = { NeoStorage.getDefault().getPath() + "/report/relatoriosConciliacao/RelatorioSapiensSigmaPrivado.pdf",
		    NeoStorage.getDefault().getPath() + "/report/relatoriosConciliacao/RelatorioSigmaSapiens.pdf" };
	    this.ziparArquivos(srcFiles);
	    File arqRelatorio = new File(NeoStorage.getDefault().getPath() + "/report/relatoriosConciliacao/relatorios.zip");
	    NeoStorage neoStorage = NeoStorage.getDefault();
	    NeoFile anexo = neoStorage.copy(arqRelatorio);
	    anexo.setName("Relatorios.zip");
	    this.abrirTarefa(anexo, 0);
	} else if (!contasSigma.isEmpty() && contasSapiensPrivado.isEmpty()) {
	    String[] srcFiles = {NeoStorage.getDefault().getPath() + "/report/relatoriosConciliacao/RelatorioSigmaSapiens.pdf"};
	    this.ziparArquivos(srcFiles);
	    File arqRelatorio = new File(NeoStorage.getDefault().getPath() + "/report/relatoriosConciliacao/relatorios.zip");
	    NeoStorage neoStorage = NeoStorage.getDefault();
	    NeoFile anexo = neoStorage.copy(arqRelatorio);
	    anexo.setName("Relatorio.zip");
	    this.abrirTarefa(anexo, 1);
	} else if (!contasSapiensPrivado.isEmpty() && contasSigma.isEmpty()) {
	    String[] srcFiles = {NeoStorage.getDefault().getPath() + "/report/relatoriosConciliacao/RelatorioSapiensSigmaPrivado.pdf"};
	    this.ziparArquivos(srcFiles);
	    File arqRelatorio = new File(NeoStorage.getDefault().getPath() + "/report/relatoriosConciliacao/relatorios.zip");
	    NeoStorage neoStorage = NeoStorage.getDefault();
	    NeoFile anexo = neoStorage.copy(arqRelatorio);
	    anexo.setName("Relatorio.zip");
	    this.abrirTarefa(anexo, 2);
	}

	if (!contasSapiensPublico.isEmpty()) {
	    String[] srcFiles = {NeoStorage.getDefault().getPath() + "/report/relatoriosConciliacao/RelatorioSapiensSigmaPublico.pdf"};
	    this.ziparArquivos(srcFiles);
	    File arqRelatorio = new File(NeoStorage.getDefault().getPath() + "/report/relatoriosConciliacao/relatorios.zip");
	    NeoStorage neoStorage = NeoStorage.getDefault();
	    NeoFile anexo = neoStorage.copy(arqRelatorio);
	    anexo.setName("Relatorio.zip");
	    this.abrirTarefa(anexo, 3);
	}

    }

    /**
     * Abre tarefa, recebe int tipo 0 para Sigma + Privado, 1 para Sigma, 2
     * Privado , 3 publico
     * 
     * @param anexo
     * @param tipo
     */
    private void abrirTarefa(NeoFile anexo, int tipo) {

	String solicitante = "";
	String executor = "";

	String titulo = "";
	String descricao = "";

	if (tipo == 0) {
	    titulo = "Conciliação de Contas Sigma x Postos Sapiens";
	    descricao = "Segue em anexo relatórios de Contas Sigma sem vinculos com Postos Sapiens e Postos Sapiens sem vinculo com conta Sigma.";
	    solicitante =  OrsegupsUtils.getUserNeoPaper("SolicitanteConciliacaoPrivado");
	    executor = OrsegupsUtils.getUserNeoPaper("ResponsavelConciliacaoPrivado");
	} else if (tipo == 1) {
	    titulo = "Conciliação de Contas Sigma x Postos Sapiens";
	    descricao = "Segue em anexo relatórios de Contas Sigma sem vinculos com Postos Sapiens.";
	    solicitante =  OrsegupsUtils.getUserNeoPaper("SolicitanteConciliacaoPrivado");
	    executor = OrsegupsUtils.getUserNeoPaper("ResponsavelConciliacaoPrivado");
	} else if (tipo == 2) {
	    titulo = "Conciliação de Postos Sapiens x Contas Sigma";
	    descricao = "Segue em anexo relatórios de Postos Sapiens sem vinculo com conta Sigma.";
	    solicitante =  OrsegupsUtils.getUserNeoPaper("SolicitanteConciliacaoPrivado");
	    executor = OrsegupsUtils.getUserNeoPaper("ResponsavelConciliacaoPrivado");
	} else {
	    titulo = "Conciliação de Postos Sapiens x Contas Sigma";
	    descricao = "Segue em anexo relatórios de Postos Sapiens sem vinculo com conta Sigma.";
	    solicitante = OrsegupsUtils.getUserNeoPaper("SolicitanteConciliacaoPublico");
	    executor = OrsegupsUtils.getUserNeoPaper("ResponsavelConciliacaoPublico");
	}

	GregorianCalendar prazo = new GregorianCalendar();
	prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 7L);
	prazo.set(Calendar.HOUR_OF_DAY, 23);
	prazo.set(Calendar.MINUTE, 59);
	prazo.set(Calendar.SECOND, 59);

	IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
	String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "hadouken", prazo, anexo);

	if (tarefa == null || tarefa.isEmpty()) {
	    log.error("ERRO AGENDADOR DE TAREFA: RotinaAbreTarefaSimplesConciliacaoSigmaSapiens - ERRO AO EXECUTAR ROTINA -- Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	}

    }

    private void montaRelatorioSigma(ArrayList<ContaSigmaVO> contasSigma) {
	try {

	    ClassLoader classLoader = getClass().getClassLoader();

	    File file = new File(classLoader.getResource("/reports/RelatorioSigmaSapiens.jrxml").getFile());
	    String pathReport = file.getAbsolutePath().toString();

	    JasperReport report = JasperCompileManager.compileReport(pathReport);

	    JasperPrint print = JasperFillManager.fillReport(report, null, new JRBeanCollectionDataSource(contasSigma));

	    String pathPdf = NeoStorage.getDefault().getPath() + "/report/relatoriosConciliacao/RelatorioSigmaSapiens.pdf";

	    JasperExportManager.exportReportToPdfFile(print, pathPdf);

	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("ERRO AGENDADOR DE TAREFA: RotinaAbreTarefaSimplesConciliacaoSigmaSapiens - ERRO AO EXECUTAR ROTINA -- Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	}
    }

    private void montaRelatorioSapiens(ArrayList<ContaSapiensVO> contasSapiens, String tipo) {
	try {

	    ClassLoader classLoader = getClass().getClassLoader();
	    File file = new File(classLoader.getResource("/reports/RelatorioSapiensSigma.jrxml").getFile());
	    String pathReport = file.getAbsolutePath().toString();

	    JasperReport report = JasperCompileManager.compileReport(pathReport);

	    JasperPrint print = JasperFillManager.fillReport(report, null, new JRBeanCollectionDataSource(contasSapiens));

	    String pathPdf = NeoStorage.getDefault().getPath() + "/report/relatoriosConciliacao/RelatorioSapiensSigma" + tipo + ".pdf";

	    JasperExportManager.exportReportToPdfFile(print, pathPdf);

	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("ERRO AGENDADOR DE TAREFA: RotinaAbreTarefaSimplesConciliacaoSigmaSapiens - ERRO AO EXECUTAR ROTINA -- Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	}
    }

    private void criaDiretorio() {

	try {

	    File dir = new File(NeoStorage.getDefault().getPath() + "/report/relatoriosConciliacao");

	    if (!dir.exists()) {
		dir.mkdir();
	    }

	} catch (SecurityException e) {
	    e.printStackTrace();
	    log.error("ERRO AGENDADOR DE TAREFA: RotinaAbreTarefaSimplesConciliacaoSigmaSapiens - ERRO AO EXECUTAR ROTINA -- Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	}
    }

    private void ziparArquivos(String[] srcFiles) {

	String zipFile = NeoStorage.getDefault().getPath() + "/report/relatoriosConciliacao/relatorios.zip";

	try {

	    // create byte buffer
	    byte[] buffer = new byte[1024];

	    FileOutputStream fos = new FileOutputStream(zipFile);

	    ZipOutputStream zos = new ZipOutputStream(fos);

	    for (int i = 0; i < srcFiles.length; i++) {

		File srcFile = new File(srcFiles[i]);

		FileInputStream fis = new FileInputStream(srcFile);

		// begin writing a new ZIP entry, positions the stream to the
		// start of the entry data
		zos.putNextEntry(new ZipEntry(srcFile.getName()));

		int length;

		while ((length = fis.read(buffer)) > 0) {
		    zos.write(buffer, 0, length);
		}

		zos.closeEntry();

		// close the InputStream
		fis.close();

	    }

	    // close the ZipOutputStream
	    zos.close();

	} catch (IOException ioe) {
	    System.out.println("Error creating zip file: " + ioe);
	    log.error("ERRO AGENDADOR DE TAREFA: RotinaAbreTarefaSimplesConciliacaoSigmaSapiens - ERRO AO EXECUTAR ROTINA -- Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	}
    }
}
