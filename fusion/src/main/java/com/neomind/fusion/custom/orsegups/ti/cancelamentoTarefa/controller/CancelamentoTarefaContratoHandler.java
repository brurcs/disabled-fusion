package com.neomind.fusion.custom.orsegups.ti.cancelamentoTarefa.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisProprios.Pasta;
import com.neomind.fusion.custom.orsegups.ti.cancelamentoTarefa.engine.CancelamentoTarefaContratoEngine;

@Path(value = "CancelarTarefas")
public class CancelamentoTarefaContratoHandler
{
	private static final Log log = LogFactory.getLog(CancelamentoTarefaContratoHandler.class);
	
	@POST
	@Path("cancelarTarefas")
	@Produces("application/json")
	public HashMap<String,String> cancelarTarefas(@MatrixParam("code") String code, @MatrixParam("motivo") String motivo) {
	        	   
	    CancelamentoTarefaContratoEngine ctce = new CancelamentoTarefaContratoEngine();
	    
	    String string = ctce.cancelaProcessos(code, motivo);	       
	    HashMap<String, String> retorno = new HashMap<String, String>();
	    retorno.put("retorno", string);
	    return retorno;

	}
	
	@POST
	@Path("buscarTarefa")
	@Produces("application/json")
	public HashMap<String,String> buscarTarefa(@MatrixParam("code") String code) {
	        	   
	    CancelamentoTarefaContratoEngine ctce = new CancelamentoTarefaContratoEngine();
	    
	    String string = ctce.buscarTarefa(code);	       
	    HashMap<String, String> retorno = new HashMap<String, String>();
	    retorno.put("cliente", string);
	    return retorno;

	}
	
	
}