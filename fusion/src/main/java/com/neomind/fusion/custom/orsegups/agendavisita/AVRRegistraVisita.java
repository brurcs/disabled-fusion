package com.neomind.fusion.custom.orsegups.agendavisita;

import java.util.Collection;
import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class AVRRegistraVisita implements AdapterInterface {

	@Override
	public void start(Task origin, EntityWrapper wrapper,	Activity activity) {
		try{
			String usuarioLogado = PortalUtil.getCurrentUser().getCode();
			
			NeoObject oRegistroAtividade = AdapterUtils.createNewEntityInstance("AVRRegistroAtividades");
			EntityWrapper wRegistroAtividade = new EntityWrapper(oRegistroAtividade);
			
			wRegistroAtividade.setValue("dataRegistro", new GregorianCalendar());
			wRegistroAtividade.setValue("usuario", usuarioLogado );
			wRegistroAtividade.setValue("acao", "Respondeu Reprovação");
			
			if (wrapper.findValue("respostaReprovacao")!= null){
				wRegistroAtividade.setValue("acao", "Respondeu Reprovação");
				wRegistroAtividade.setValue("obs2", wrapper.findValue("respostaReprovacao"));
				wrapper.setValue("respostaReprovacao",(String)null);
				wrapper.setValue("flagReprovado",0L);
			}else{
				wRegistroAtividade.setValue("acao", "Visitar");
				wRegistroAtividade.setValue("obs2", "Relatou visita");
			}
			
			Collection<NeoObject> listaRegistros = wrapper.findField("registroHistorico").getValues();
			listaRegistros.add(oRegistroAtividade);
			
			//registroHistorico
		}catch(Exception e){
			e.printStackTrace();
			throw new WorkflowException("Erro ao relatar visita. " + e.getMessage());
		}
		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity) {
		
	}

}
