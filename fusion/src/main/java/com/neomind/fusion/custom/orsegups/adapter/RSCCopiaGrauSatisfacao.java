package com.neomind.fusion.custom.orsegups.adapter;

import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.util.NeoUtils;


public class RSCCopiaGrauSatisfacao implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		List<NeoObject> oco = (List<NeoObject>) processEntity.findValue("historicoOcorrencias");
		if(NeoUtils.safeIsNotNull(oco)){
			NeoObject ultima = oco.get(oco.size()-1);
			EntityWrapper ewUltima = new EntityWrapper(ultima);
			processEntity.setValue("grauSatisfacao", ewUltima.findValue("grauSatisfacao"));
		}
	}
	
	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}
}