package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.ListIterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.FaltaEfetivoVO;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsGroupLevels;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.SchedulerUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.fusion.workflow.simulation.WorkflowService;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

@SuppressWarnings("deprecation")
public class AbreTarefaSimplesFaltaEfetivo implements CustomJobAdapter {

    private static final Log log = LogFactory.getLog(AbreTarefaSimplesFaltaEfetivo.class);

    @Override
    public void execute(CustomJobContext arg0) {

	Connection conn = null;
	StringBuilder sql = new StringBuilder();
	PreparedStatement pstm = null;
	ResultSet rs = null;
	System.out.println("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Simples Falta de Efetivo - Data: " 
		+ NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	log.warn("##### INICIO AGENDADOR DE TAREFA: Abrir Tarefa Simples Falta de Efetivo - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

	Long key = GregorianCalendar.getInstance().getTimeInMillis();

	try {

	    List<FaltaEfetivoVO> faltaEfetivoVOs = new ArrayList<FaltaEfetivoVO>(); 
	    conn = PersistEngine.getConnection("VETORH");

	    sql.append(" SELECT SUBSTRING(REG.USU_NOMREG,1,3) AS SIGLA_REGIONAL, ORN.USU_CODREG, ORN.USU_CODCCU, ORN.NUMLOC, ORN.TABORG, ORN.USU_CLIORN, ORN.USU_LOTORN, ORN.NOMLOC, ORN.USU_QTDCON, HLO.NUMCAD ");
	    sql.append(" FROM R016ORN ORN  ");
	    sql.append(" INNER JOIN R016HIE HIE ON HIE.NUMLOC = ORN.NUMLOC AND HIE.TABORG = ORN.TABORG  ");
	    sql.append(" LEFT JOIN R038HLO  HLO ON HLO.TABORG = ORN.TABORG  AND HLO.NUMLOC = ORN.NUMLOC AND HLO.DATALT = (SELECT MAX(HLO2.DATALT) ");
	    sql.append(" FROM R038HLO HLO2 WHERE HLO2.NUMEMP = HLO.NUMEMP AND HLO2.TIPCOL = HLO.TIPCOL AND HLO2.NUMCAD = HLO.NUMCAD AND HLO2.DATALT <= GETDATE()) ");
	    sql.append(" LEFT JOIN R034FUN FUN ON HLO.NUMEMP = FUN.NUMEMP AND HLO.TIPCOL = FUN.TIPCOL  AND HLO.NUMCAD = FUN.NUMCAD AND FUN.TIPCOL = 1 ");
	    sql.append(" INNER JOIN [FSOODB04\\SQL02].SAPIENS.DBO.E044CCU CCU ON ORN.USU_CODCCU = CCU.CODCCU AND ORN.USU_NUMEMP = CCU.CODEMP");
	    sql.append(" INNER JOIN [FSOODB04\\SQL02].SAPIENS.DBO.USU_T200REG REG ON ORN.USU_CODREG = REG.USU_CODREG ");
	    sql.append(" WHERE ORN.USU_SITATI = 'S' ");
	    //sql.append(" AND ORN.USU_CODREG <> 9 ");   Removido a pedido do RH - tarefa 1621066 
	    sql.append(" AND ORN.USU_CODREG <> 0 ");
	    sql.append(" AND ORN.USU_CODREG <> 8 ");
	    sql.append(" AND (LEN(HIE.CODLOC) = 27) ");
	    sql.append(" AND ORN.USU_NUMEMP IN (1, 7, 15, 17, 18, 19, 21, 22, 27, 28, 29) ");
	    sql.append(" AND (FUN.SITAFA <> 7 OR FUN.NUMCAD IS NULL) ");
	    sql.append(" AND CCU.CLACCU NOT LIKE '11%'"); // CENTRO DE CUSTO ADMINISTRATIVO
	    sql.append(" ORDER BY ORN.USU_CODREG, ORN.USU_CODCCU, HLO.NUMCAD ");

	    pstm = conn.prepareStatement(sql.toString());
	    rs = pstm.executeQuery();
	    FaltaEfetivoVO faltaEfetivoVO = null;

	    while (rs.next()) {
		faltaEfetivoVO = new FaltaEfetivoVO();
		Long codreg = rs.getLong("usu_codreg");
		Long codccu = rs.getLong("usu_codccu");
		Long numloc = rs.getLong("numloc");
		Long taborg = rs.getLong("taborg");
		Long qtdCon = rs.getLong("usu_qtdcon");

		String lotorn = rs.getString("usu_lotorn");
		String nomloc = rs.getString("nomloc");
		String nomFun = rs.getString("NumCad");
		
		String sigRegional = rs.getString("sigla_regional");

		faltaEfetivoVO.setNomReg(sigRegional);
		faltaEfetivoVO.setCodreg(codreg);
		faltaEfetivoVO.setCodccu(codccu);
		faltaEfetivoVO.setNumloc(numloc);
		faltaEfetivoVO.setTaborg(taborg);
		faltaEfetivoVO.setLotorn(lotorn);
		faltaEfetivoVO.setNomloc(nomloc);
		faltaEfetivoVO.setNomFun(nomFun);
		faltaEfetivoVO.setQtdCon(qtdCon.intValue());

		faltaEfetivoVOs.add(faltaEfetivoVO);

	    }
	    int count = 0;
	    ListIterator<FaltaEfetivoVO> faltaVOIterator = faltaEfetivoVOs.listIterator();
	    List<FaltaEfetivoVO> faltaEfetivoVOsAux = new ArrayList<FaltaEfetivoVO>();
	    while (faltaVOIterator.hasNext()) {
		FaltaEfetivoVO atual = faltaVOIterator.next();
		FaltaEfetivoVO proximo = null;

		if (faltaVOIterator.hasNext()) {
		    proximo = faltaVOIterator.next();
		    faltaVOIterator.previous();

		    if (atual.getNumloc().equals(proximo.getNumloc()) && atual.getNomFun() != null && !atual.getNomFun().isEmpty()){
			count++;
		    } else if (!proximo.getNumloc().equals(atual.getNumloc())) {
			count++;
			if (atual.getQtdCon() > count || atual.getQtdCon() == 1 
				&& ((atual.getNomFun() != null && atual.getNomFun().isEmpty()) || (atual.getNomFun() == null))) {
			    System.out.println("#Falta de Efetivo# "+proximo.getNumloc() + " - " + atual.getNumloc() + " - " + atual.getQtdCon() + " > - " + count);
			    faltaEfetivoVO = new FaltaEfetivoVO();
			    faltaEfetivoVO.setNomReg(atual.getNomReg());
			    faltaEfetivoVO.setCodreg(atual.getCodreg());
			    faltaEfetivoVO.setCodccu(atual.getCodccu());
			    faltaEfetivoVO.setNumloc(atual.getNumloc());
			    faltaEfetivoVO.setTaborg(atual.getTaborg());
			    faltaEfetivoVO.setLotorn(atual.getLotorn());
			    faltaEfetivoVO.setNomloc(atual.getNomloc());
			    faltaEfetivoVO.setQtdCon(atual.getQtdCon());
			    faltaEfetivoVO.setQtdFun(count);
			    faltaEfetivoVOsAux.add(faltaEfetivoVO);
			}
			count = 0;
		    }
		}
	    }
	    for (FaltaEfetivoVO faltaVO : faltaEfetivoVOsAux) {

		String solicitante = "beatriz.malmann";
		String titulo = faltaVO.getNomReg() + " - #Informar falta de efetivo no posto " + faltaVO.getCodccu();
		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 1L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);

		NeoPaper responsavel = new NeoPaper();
		String executorInicial = null;
		String executor = "";

		responsavel = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "AFEExecutor"));
		// ANTIGO: OrsegupsUtils.getPapelFaltaEfetivo(faltaVO.getCodreg(), faltaVO.getNomloc());

		
		NeoUser user = new NeoUser();

		if (responsavel != null && responsavel.getAllUsers() != null && !responsavel.getAllUsers().isEmpty()) {
		    for (NeoUser u : responsavel.getUsers()) {
			user = u;
			executorInicial = user.getCode();
			break;
		    }
		} else {

		    if (responsavel != null && responsavel.getCode() != null && !responsavel.getCode().isEmpty()) {
			log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Falta de Efetivo - Papel " + responsavel.getCode() 
				+ " sem usuário definido - Posto: " + faltaVO.getLotorn() + " - " + faltaVO.getNomloc() + " - " + faltaVO.getNumloc());
			StringBuilder sqlRegional = new StringBuilder();

			// Nome da regional
			sqlRegional.append(" SELECT USU_CODREG,USU_NOMREG FROM USU_T200REG WHERE USU_CODREG = ?");
			Connection connSapiens = PersistEngine.getConnection("SAPIENS");
			PreparedStatement stRegional = null;
			ResultSet rsRegional = null;

			stRegional = connSapiens.prepareStatement(sqlRegional.toString());
			stRegional.setLong(1, faltaVO.getCodreg());
			rsRegional = stRegional.executeQuery();
			String codNomRegional = "";
			Long codReg = 0L;
			if (rsRegional.next()) {
			    codReg = Long.parseLong(rsRegional.getString("USU_CODREG"));
			    codNomRegional = rsRegional.getString("USU_CODREG");
			    codNomRegional = codNomRegional + " - " + rsRegional.getString("USU_NOMREG");
			}

			OrsegupsUtils.closeConnection(connSapiens, stRegional, rsRegional);

			//Histórico de tarefas
			StringBuilder sqlHistorico = new StringBuilder();
			sqlHistorico.append(" SELECT * FROM d_faltaResponsavelFaltaEfetivo WHERE codReg = ? ");
			Connection connFusion = PersistEngine.getConnection("");
			PreparedStatement stHistorico = null;
			ResultSet rsHistorico = null;

			stHistorico = connFusion.prepareStatement(sqlHistorico.toString());
			stHistorico.setLong(1, codReg);
			rsHistorico = stHistorico.executeQuery();

			String tarefas = "";
			while (rsHistorico.next()) {
			    if (tarefas.equals("")) {
				tarefas = rsHistorico.getString("codigoTarefaSimples");
			    } else {
				tarefas = tarefas + "," + rsHistorico.getString("codigoTarefaSimples");
			    }	 
			}
			OrsegupsUtils.closeConnection(connFusion, stHistorico, rsHistorico);

			// Abre tarefa simples para informar o responsável pela regional quando o mesmo não é encontrado
			boolean abrirTarefaSimplesInformarResponsavelRegional = true;
			if (tarefas.equals("")) {
			    abrirTarefaSimplesInformarResponsavelRegional = true;
			} else {
			    //Histórico de tarefas
			    StringBuilder sqlTarefa = new StringBuilder();
			    sqlTarefa.append(" SELECT * FROM WFProcess W  ");
			    sqlTarefa.append(" INNER JOIN D_Tarefa T ON T.wfprocess_neoId = W.neoId ");
			    sqlTarefa.append(" WHERE W.processState = 0 and W.code in("+tarefas+")");

			    Connection connFusion2 = PersistEngine.getConnection("");
			    PreparedStatement stTarefa = null;
			    ResultSet rsTarefa = null;

			    stTarefa = connFusion2.prepareStatement(sqlTarefa.toString());
			    rsTarefa = stTarefa.executeQuery();

			    if (rsTarefa.next())  {
				abrirTarefaSimplesInformarResponsavelRegional = false;
			    }
			    OrsegupsUtils.closeConnection(connFusion2, stTarefa, rsTarefa);
			}

			if (abrirTarefaSimplesInformarResponsavelRegional) {

			    String descricaTarSim = "Informar o responsavel da regional " + codNomRegional + " que deverá responder as tarefas de falta de efetivo. "
				    + "Segue posto que não foi aberto tarefa por falta de responsavel:<br> "
				    + "Posto: " + faltaVO.getLotorn() + " - " + faltaVO.getNomloc() + " - " + faltaVO.getNumloc();
			    String tituloTarSim = "Informar resposanvel pelas tarefas de Falta de Efetivo da regional " + codNomRegional;
			    String code = this.iniciarTarefaSimples(descricaTarSim, tituloTarSim);
			    System.out.println("###Falta de efetivo: Aberto tarefa simples " + code + " referente a falta de resposanvel na regional " + codNomRegional);

			    InstantiableEntityInfo tarefaHistorico = AdapterUtils.getInstantiableEntityInfo("faltaResponsavelFaltaEfetivo");
			    NeoObject noHistorico = tarefaHistorico.createNewInstance();
			    EntityWrapper tarefaWrapper = new EntityWrapper(noHistorico);

			    tarefaWrapper.setValue("codigoTarefaSimples", code);
			    tarefaWrapper.setValue("codReg", codReg);
			    PersistEngine.persist(noHistorico);
			}
		    }
		    continue;
		}

		GregorianCalendar dataExecucao = new GregorianCalendar();
		dataExecucao = OrsegupsUtils.getSpecificWorkDay(dataExecucao, -12L);
		
		/* Este nível inicial não existe por padrão nos níveis de grupo da empresa. Coloquei um fixo para 
		 * poder abrir tarefa para duas pessoas que estão no mesmo grupo (mesmo level) */
		Integer nivelExecutorInicial = 50;

		QLEqualsFilter numlocFilter = new QLEqualsFilter("numloc", faltaVO.getNumloc());
		QLEqualsFilter taborgFilter = new QLEqualsFilter("taborg", faltaVO.getTaborg());
		QLEqualsFilter nivelFilter = new QLEqualsFilter("nivel", nivelExecutorInicial);
		QLOpFilter dataFilter = new QLOpFilter("dataExecucao", ">=", (GregorianCalendar) dataExecucao);

		QLGroupFilter groupFilter = new QLGroupFilter("AND");
		groupFilter.addFilter(numlocFilter);
		groupFilter.addFilter(taborgFilter);
		groupFilter.addFilter(nivelFilter);
		groupFilter.addFilter(dataFilter);

		List<NeoObject> objs = PersistEngine.getObjects(AdapterUtils.getEntityClass("TarefaFaltaEfetivo"), groupFilter);

		Integer nivelExecutorEscalada = 0;
		
		if (objs != null && objs.size() >= 3) {
		    Integer countNivel = objs.size();
		    while (countNivel >= 3) {
			//Presidencia não sobe mais nivel					
			if (nivelExecutorEscalada != OrsegupsGroupLevels.PRESIDENCIA) {			   			    
			    /* #Adaptação - Na primeira iteração a rotina irá para o responsável do grupo, ou seja
			       não vai subir 1 nível na hierarquia. O fluxo passa a partir de agora a obeceder a mesma regra de escalonamento da tarefa simples */
			    if(user.getCode() == executorInicial){
				for (NeoUser u : user.getGroup().getResponsible().getAllUsers()) {
				    user = u;
				    break;
				}
			    } else {
				for (NeoUser u : user.getGroup().getUpperLevel().getResponsible().getAllUsers()) {
				    user = u;
				    break;
				}			
			    }
			    
			    nivelExecutorEscalada = user.getGroup().getGroupLevel();
			    nivelFilter = new QLEqualsFilter("nivel", nivelExecutorEscalada);

			    groupFilter = new QLGroupFilter("AND");
			    groupFilter.addFilter(numlocFilter);
			    groupFilter.addFilter(taborgFilter);
			    groupFilter.addFilter(nivelFilter);
			    groupFilter.addFilter(dataFilter);

			    System.out.println("faltaEfetivo debug -> ini" + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm"));
			    objs = PersistEngine.getObjects(AdapterUtils.getEntityClass("TarefaFaltaEfetivo"), groupFilter);
			    System.out.println("faltaEfetivo debug -> fim" + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm"));
			    countNivel = objs.size();
			    			    
			    executor = user.getCode();
			    System.out.println(executor);
			    
			} else {
			    break;
			}
		    }
		} else {
		    executor = user.getCode();
			    System.out.println(executor);
		}

		String descricao = "";
		descricao = " Informar o andamento do preenchimento da falta de efetivo no posto "
			+ faltaVO.getLotorn() + " - " + faltaVO.getNomloc() + " - CC " + faltaVO.getCodccu() + "\n";
		String tarefa = "";
		String processo = "";
		String historico = "";
		SchedulerUtils utils = new SchedulerUtils();

		/* Diretoria não recebe G004, por regra definida eles receberão uma tarefa simples com o histório das G004 */
		if (nivelExecutorEscalada < OrsegupsGroupLevels.DIRETORIA) {
		    processo = "G004 - Acompanhamento de Falta de Efetivo";
		    tarefa = abrirTarefaFaltaEfetivo(solicitante, executor, titulo, descricao, historico, "sim", prazo);
		} else {
		    processo = "G001 - Tarefa Simples";
		    descricao += utils.getHTMLInputFaltaEfetivo(faltaVO.getNumloc(), faltaVO.getTaborg());
		    IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		    if (executor.equals("dilmoberger"))  {
			solicitante = "dilmoberger";
			NeoPaper gerente = OrsegupsUtils.getPapelGerenteRegional(faltaVO.getCodreg(), faltaVO.getNomloc());
			if (gerente != null && gerente.getAllUsers() != null && !gerente.getAllUsers().isEmpty()) {
			    for (NeoUser usuarioGerente : gerente.getUsers())  {
				executor = usuarioGerente.getCode();
				break;
			    }
			} else {
			    log.error("#####  AGENDADOR DE TAREFA: Abrir Tarefa Simples Falta de Efetivo - Responsavel " + gerente.getCode() 
				    + " sem usuário definido - Colaborador: " + rs.getInt("NumEmp") + "/" + rs.getInt("NumCad") 
				    + " - Data: " + NeoDateUtils.safeDateFormat(rs.getDate("DatAfa"), "dd/MM/yyyy"));
			    continue;
			}
		    }
		    if (descricao != null && !descricao.isEmpty() && descricao.length() <= 7500)  {
			tarefa = iniciarTarefaSimples.abrirTarefaReturnNeoId(solicitante, executor, titulo, descricao, "1", "sim", prazo, null);
		    }  else {
			log.error("#####  AGENDADOR DE TAREFA: Abrir Tarefa Simples Falta de Efetivo - Responsavel " + executor 
				+ " Titulo " + titulo + " descrição maior que 8000 caracteres - "
				+ "Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		    }

		}

		if (tarefa != null && !tarefa.isEmpty() && tarefa.contains(";")) {
		    InstantiableEntityInfo tarefaFE = AdapterUtils.getInstantiableEntityInfo("TarefaFaltaEfetivo");
		    NeoObject noFE = tarefaFE.createNewInstance();
		    EntityWrapper feWrapper = new EntityWrapper(noFE);
		    String array[] = tarefa.split(";");
		    Integer nivelAux = 0;
		    feWrapper.findField("numloc").setValue(faltaVO.getNumloc());
		    feWrapper.findField("taborg").setValue(faltaVO.getTaborg());
		    feWrapper.findField("tarefa").setValue(array[1]);
		    feWrapper.findField("neoIdTarefa").setValue(Long.parseLong(array[0]));
		    feWrapper.findField("processo").setValue(processo);		    		 		    
		    if(executor == executorInicial){
			feWrapper.findField("nivel").setValue(nivelExecutorInicial.longValue());
			nivelAux = nivelExecutorInicial;
		    } else {
			feWrapper.findField("nivel").setValue(nivelExecutorEscalada.longValue());
			nivelAux = nivelExecutorEscalada;
		    }
		    feWrapper.findField("dataExecucao").setValue(new GregorianCalendar());
		    PersistEngine.persist(noFE);

		    log.warn("##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Falta de Efetivo - Responsavel " 
			    + responsavel.getCode() + " - Nivel: " + nivelAux + " Posto: " + faltaVO.getLotorn() 
			    + " - " + faltaVO.getNomloc() + " - " + faltaVO.getNumloc());
		}
	    }
	} catch (Exception e) {

	    log.error("[" + key + "] ##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Falta de Efetivo", e);
	    System.out.println("[" + key + "] ##### AGENDADOR DE TAREFA: Abrir Tarefa Simples Falta de Efetivo");
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {

	    OrsegupsUtils.closeConnection(conn, pstm, rs);

	    System.out.println("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Simples Falta de Efetivo - Data: " 
		    + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	    log.warn("##### FIM AGENDADOR DE TAREFA: Abrir Tarefa Simples Falta de Efetivo - Data: " 
		    + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	}
    }

    public String abrirTarefaFaltaEfetivo(String solicitante, String executor, String titulo, String descricao, String historico, String origem, GregorianCalendar prazo)  {
	try {
	    NeoUser objSolicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", solicitante));
	    NeoUser objExecutor = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", executor));

	    if (NeoUtils.safeIsNull(objSolicitante)) {
		return "Erro #1 - Solicitante nÃ£o encontrado";
	    }

	    if (NeoUtils.safeIsNull(titulo))  {
		return "Erro #3 - TÃ­tulo nÃ£o informado";
	    }

	    if (NeoUtils.safeIsNull(descricao))  {
		return "Erro #4 - DescriÃ§Ã£o nÃ£o informada";
	    }

	    if (NeoUtils.safeIsNull(prazo)) {
		return "Erro #6 - Prazo nÃ£o informado";
	    }

	    //Cria Instancia do Eform Principal
	    InstantiableEntityInfo infoTarefa = AdapterUtils.getInstantiableEntityInfo("AFEAcompanhamentoFaltaEfetivo");
	    NeoObject noTarefa = infoTarefa.createNewInstance();
	    EntityWrapper tarefaWrapper = new EntityWrapper(noTarefa);

	    tarefaWrapper.findField("executor").setValue(objExecutor);
	    //tarefaWrapper.findField("historico").setValue(historico);
	    tarefaWrapper.findField("titulo").setValue(titulo);
	    tarefaWrapper.findField("solicitacao").setValue(descricao);
	    tarefaWrapper.findField("prazo").setValue(prazo);

	    //Salva o Eform
	    PersistEngine.persist(noTarefa);

	    final ProcessModel pm = (ProcessModel) PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("name", "G004 - Acompanhamento de Falta de Efetivo"));
	    final WFProcess processo = WorkflowService.startProcess(pm, noTarefa, false, objSolicitante);
	    processo.setSaved(true);

	    return processo.getNeoId() + ";" + processo.getCode();
	} catch (Exception e) {
	    e.printStackTrace();
	}

	return "";
    }

    public String iniciarTarefaSimples(String descricaoTarSim, String titulo)  {
	String solicitanteTs = "beatriz.malmann";

	String descricaoTs = descricaoTarSim;
	String tituloTs = titulo;
	String executorTs = "";
	GregorianCalendar prazoTs = new GregorianCalendar();
	prazoTs = OrsegupsUtils.getSpecificWorkDay(prazoTs, 2L);
	prazoTs.set(Calendar.HOUR_OF_DAY, 23);
	prazoTs.set(Calendar.MINUTE, 59);
	prazoTs.set(Calendar.SECOND, 59);

	IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
	String code = iniciarTarefaSimples.abrirTarefa(solicitanteTs, executorTs, tituloTs, descricaoTs, "1", "nao", prazoTs);
	return code;
    }
}
