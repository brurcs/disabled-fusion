package com.neomind.fusion.custom.orsegups.endpoint.filter;

import java.io.IOException;
import java.security.Principal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Priority;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;

import com.neomind.fusion.custom.orsegups.endpoint.annotation.Secured;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;

@Provider
@Secured
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {

	// Get the HTTP Authorization header from the request
	String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

	// Check if the HTTP Authorization header is present and formatted
	// correctly
	if (authorizationHeader == null || !authorizationHeader.startsWith("Token ")) {
	    throw new NotAuthorizedException("Authorization header must be provided");
	}

	// Extract the token from the HTTP Authorization header
	String token = authorizationHeader.substring("Token".length()).trim();

	try {
	    // Validate the token
	    final String username = this.validateToken(token);
	    
	    if (!username.isEmpty()){
		
		final SecurityContext currentSecurityContext = requestContext.getSecurityContext();
		requestContext.setSecurityContext(new SecurityContext() {

		    @Override
		    public Principal getUserPrincipal() {

		        return new Principal() {

		            @Override
		            public String getName() {
		                return username;
		            }
		        };
		    }

		    @Override
		    public boolean isUserInRole(String role) {
		        return true;
		    }

		    @Override
		    public boolean isSecure() {
		        return currentSecurityContext.isSecure();
		    }

		    @Override
		    public String getAuthenticationScheme() {
		        return "Token";
		    }
		});
		
	    }else{
		throw new Exception();
	    }

	} catch (Exception e) {
	    requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
	}
    }

    private String validateToken(String token) throws Exception {
	
	String retorno = "";
	
	if (!token.isEmpty()) {
	    Connection conn = null;
	    PreparedStatement pstm = null;
	    ResultSet rs = null;

	    StringBuilder sql = new StringBuilder();

	    sql.append("SELECT USUARIO FROM TOKEN_CONTROL WHERE TOKEN = ? AND ATIVO = 1 AND VALIDADE > GETDATE()");

	    try {
		conn = PersistEngine.getConnection("TIDB");
		pstm = conn.prepareStatement(sql.toString());

		pstm.setString(1, token);

		rs = pstm.executeQuery();

		if (rs.next()) {
		    retorno = rs.getString(1);
		}else{		    
		    throw new Exception();
		}

	    } catch (SQLException e) {
		e.printStackTrace();
	    } finally {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    }
	} else {
	    throw new Exception();
	}
	
	return retorno;

    }
}
