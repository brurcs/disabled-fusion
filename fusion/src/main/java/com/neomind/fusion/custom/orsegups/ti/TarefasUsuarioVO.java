package com.neomind.fusion.custom.orsegups.ti;

import java.util.ArrayList;
import java.util.Collection;


public class TarefasUsuarioVO
{
	private String usuario;
	private Long codigoUsuario;
	private String total;
	private Long totalExecMes;
	private Collection<TarefasVO> tarefas = new ArrayList();
	
	public String getUsuario()
	{
		return usuario;
	}
	public void setUsuario(String usuario)
	{
		this.usuario = usuario;
	}
	public Collection<TarefasVO> getTarefas()
	{
		return tarefas;
	}
	public void setTarefas(Collection<TarefasVO> tarefas)
	{
		this.tarefas = tarefas;
	}
	public String getTotal()
	{
		return total;
	}
	public void setTotal(String total)
	{
		this.total = total;
	}
	public Long getCodigoUsuario()
	{
		return codigoUsuario;
	}
	public void setCodigoUsuario(Long codigoUsuario)
	{
		this.codigoUsuario = codigoUsuario;
	}
	public Long getTotalExecMes() {
	    return totalExecMes;
	}
	public void setTotalExecMes(Long totalExecMes) {
	    this.totalExecMes = totalExecMes;
	}
	
}
