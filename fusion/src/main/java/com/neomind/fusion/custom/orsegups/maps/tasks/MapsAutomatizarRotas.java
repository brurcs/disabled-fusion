package com.neomind.fusion.custom.orsegups.maps.tasks;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.common.base.Joiner;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.common.NeoRunnable;
import com.neomind.fusion.custom.orsegups.maps.comparator.EventoFilaEsperaComparator;
import com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServletAux;
import com.neomind.fusion.custom.orsegups.maps.vo.EventoEsperaVO;
import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.maps.vo.RotaVO;
import com.neomind.fusion.custom.orsegups.maps.vo.ViaturaVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

/**
 * Classe de automatização de enfileiramento de eventos dos AITs
 * 
 * @author mateus.batista
 * @since 08/02/2017
 * @version 3.0.0
 */
public class MapsAutomatizarRotas {

    private static final Log log = LogFactory.getLog(MapsAutomatizarRotas.class);

    private static MapsAutomatizarRotas instance;

    //private int executionCount = 0;

    private MapsAutomatizarRotas() {

    }

    public static MapsAutomatizarRotas getInstance() {
	if (instance == null) {
	    MapsAutomatizarRotas.instance = new MapsAutomatizarRotas();
	}

	return MapsAutomatizarRotas.instance;
    }

    public synchronized void execute() {

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	
	Map<Integer, ViaturaVO> viaturas = new HashMap<Integer, ViaturaVO>();
	
	long inicio = Calendar.getInstance().getTimeInMillis();
	
	System.out.println("##MAPSAUTOMATIZARROTAS## CHAVE: "+inicio+" - INICIO DA EXECUCAO DA ROTINA EM "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
		
	try {

	    StringBuilder sql = new StringBuilder();
	    
	    sql.append(" SELECT motorista, codigoViatura, I.nomeRota FROM D_OTSViatura  V WITH(NOLOCK)  ");
	    sql.append(" INNER JOIN D_OTSViatura_listaRotasEfetivas LR WITH(NOLOCK) ON LR.D_OTSViatura_neoId = V.neoId  ");
	    sql.append(" INNER JOIN D_OTSItemRotaEfetiva I WITH(NOLOCK) ON I.neoId = LR.listaRotasEfetivas_neoId ");
	    sql.append(" WHERE V.emUso=1 AND V.ativaNaFrota=1 AND V.controlarRota=1 ");

	    conn = PersistEngine.getConnection("");

	    stmt = conn.createStatement();

	    rs = stmt.executeQuery(sql.toString());

	    while (rs.next()) {
		
		Integer codigoViatura = rs.getInt(2);
		
		if (viaturas.containsKey(codigoViatura)){
		    
		    List<RotaVO> rotas = viaturas.get(codigoViatura).getListaRotasEfetivas();
		    
		    RotaVO novaRota = new RotaVO();
		    
		    novaRota.setNomeRota(rs.getString(3));
		    
		    rotas.add(novaRota);
		    
		}else{
		    ViaturaVO novaViatura = new ViaturaVO();
		    novaViatura.setNomeMotorista(rs.getString(1));
		    novaViatura.setCodigoViatura(String.valueOf(codigoViatura));
		    
		    RotaVO rota = new RotaVO();
		    rota.setNomeRota(rs.getString(3));
		    
		    List<RotaVO> listaRotasEfetivas = new ArrayList<RotaVO>();
		    
		    listaRotasEfetivas.add(rota);
		    
		    novaViatura.setListaRotasEfetivas(listaRotasEfetivas);
		    
		    viaturas.put(codigoViatura, novaViatura);
		}

	    }
	    
	} catch (Exception e) {
	    e.printStackTrace();
	    Long key = new GregorianCalendar().getTimeInMillis();
	    log.error("Erro no processamento, procurar no log por [" + key + "]");
//	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}
	
	System.out.println("##MAPSAUTOMATIZARROTAS## CHAVE: "+inicio+" - BUSCOU LISTA DE VIATURAS EM "+(Calendar.getInstance().getTimeInMillis()-inicio)+" ms");
	
	try {

	    if (viaturas != null && !viaturas.isEmpty()) {

		for (Integer i: viaturas.keySet()) {
		    
		    int count = 1;
		    
		    ViaturaVO viatura = viaturas.get(i);

		    String viaturaFusion = viatura.getNomeMotorista();

		    String codigoViatura = viatura.getCodigoViatura();

		    if (!viaturaFusion.contains("- TEC") && !viaturaFusion.contains("-TEC") && !viaturaFusion.contains("-INSP") && !viaturaFusion.contains("-MOTO") && !viaturaFusion.contains("-ADM")
			    && !viaturaFusion.contains("OFFICE") && !viaturaFusion.contains("COORD") && !viaturaFusion.contains("ENG") && !codigoViatura.isEmpty() && codigoViatura.length() <= 6) {

			if (viatura.getListaRotasEfetivas() != null && !viatura.getListaRotasEfetivas().isEmpty()) {
//			    if (executionCount % 2 == 0) {
//				this.normalizeEvents(codigoViatura);
//			    }
			    long inicioMoveRota = Calendar.getInstance().getTimeInMillis();
			    System.out.println("##MAPSAUTOMATIZARROTAS## CHAVE: "+inicio+" - INICIOU AUTOMATIZAÇÃO DA ROTA "+count+"/"+viaturas.size()+" "+viatura.getListaRotasEfetivas().toString());
			    this.moveRoutesByWeight(viatura);			    
			    long tempoTotal = Calendar.getInstance().getTimeInMillis()-inicioMoveRota;
			    if (tempoTotal > 10*1000){
				System.out.println("##MAPSAUTOMATIZARROTAS## CHAVE: "+inicio+" #ALERTA#TEMPO EXCESSIVO# FINALIZOU AUTOMATIZAÇÃO DA ROTA "+count+"/"+viaturas.size()+" FINALIZOU AUTOMATIZAÇÃO DA ROTA  "+viatura.getListaRotasEfetivas().toString()+" EM "+tempoTotal+" ms");
			    }else{				
				System.out.println("##MAPSAUTOMATIZARROTAS## CHAVE: "+inicio+" - FINALIZOU AUTOMATIZAÇÃO DA ROTA "+count+"/"+viaturas.size()+" "+viatura.getListaRotasEfetivas().toString()+" EM "+tempoTotal+" ms");
			    }
			}

		    }
		    count ++;
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    Long key = new GregorianCalendar().getTimeInMillis();
	    log.error("Erro no processamento, procurar no log por [" + key + "]");
//	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	}
	
	System.out.println("##MAPSAUTOMATIZARROTAS## CHAVE: "+inicio+" - FIM DA EXECUCAO DA ROTINA EM "+NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss")+ "TEMPO TOTAL: "+(Calendar.getInstance().getTimeInMillis()-inicio));
	//this.executionCount++;
    }

    @SuppressWarnings("unused")
    private void normalizeEvents(String codigoViatura) {

	Long key = GregorianCalendar.getInstance().getTimeInMillis();

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	List<String> codigosEventosFila = new ArrayList<String>();

	try {

	    String sql = "SELECT codHistorico, neoId FROM D_SIGMAEventosFilaEspera WITH(NOLOCK) WHERE cdViaturaSigma=" + codigoViatura + " ORDER BY codigo ASC";

	    conn = PersistEngine.getConnection("");

	    stmt = conn.createStatement();

	    rs = stmt.executeQuery(sql);

	    while (rs.next()) {
		String codigoHistorico = rs.getString(1);
		codigosEventosFila.add(codigoHistorico);

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	    log.error("Erro no processamento, procurar no log por [" + key + "]");
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	List<EventoVO> listaEventosRecalculados = new ArrayList<EventoVO>();

	if (codigosEventosFila != null && !codigosEventosFila.isEmpty()) {

	    String codigosEventos = Joiner.on(",").join(codigosEventosFila);

	    StringBuilder sql = new StringBuilder();
	    sql.append(" SELECT h.CD_HISTORICO, h.CD_EVENTO, F.NM_FRASE_EVENTO, ");
	    sql.append(" c.CD_CLIENTE,C.ID_CENTRAL, C.PARTICAO, C.FANTASIA, c.ID_EMPRESA, h.TX_OBSERVACAO_FECHAMENTO, ");
	    sql.append(" CASE ");
	    sql.append("WHEN (C.RAZAO LIKE '%DILMO%BERGER%' OR C.FANTASIA LIKE '%DILMO%BERGER%') THEN 0 ");
	    sql.append(" WHEN H.CD_EVENTO IN ('XVID','XPLE') AND H.TX_OBSERVACAO_FECHAMENTO LIKE '%#ComAlteracao%'  THEN 1 ");
	    sql.append(" WHEN C.ID_EMPRESA IN (10119, 10120) THEN 2 ");
	    sql.append(" WHEN H.CD_EVENTO = 'EODM' THEN 3 ");
	    sql.append(" WHEN F.NM_FRASE_EVENTO LIKE '%PANICO%' OR F.NM_FRASE_EVENTO LIKE '%PÂNICO%' OR F.NM_FRASE_EVENTO LIKE '%COAÇÃO%' OR F.NM_FRASE_EVENTO LIKE '%COACAO%' THEN 4 ");
	    sql.append(" WHEN H.CD_EVENTO = 'XXX7' AND cuc.PRIORIDADE =1 THEN 5 ");
	    sql.append(" WHEN EXISTS (SELECT H2.CD_HISTORICO FROM HISTORICO H2 WITH(NOLOCK) WHERE H2.CD_HISTORICO_PAI = H.CD_HISTORICO) THEN 6 ");
	    sql.append(" WHEN EXISTS (SELECT RSC.CD_CLIENTE FROM [FSOODB04\\SQL02].TIDB.dbo.ClientesSigmaRSCRRC RSC WHERE RSC.CD_CLIENTE = H.CD_CLIENTE) THEN 7 ");
	    sql.append(" WHEN H.CD_EVENTO = 'X406' AND  H.TX_OBSERVACAO_FECHAMENTO LIKE '%#ComAlteracao%' THEN 8 ");
	    sql.append(" WHEN cuc.TIPO = 1 THEN 9 ");
	    sql.append(" WHEN H.CD_EVENTO IN ('XVID','XPLE') THEN 10 ");
	    sql.append("  WHEN H.CD_EVENTO = 'X406' THEN 11 ");
	    sql.append(" ELSE 12 ");
	    sql.append(" END AS PESO, ");
	    sql.append(" C.NU_LATITUDE, C.NU_LONGITUDE ");
	    sql.append(" FROM HISTORICO h WITH (NOLOCK) ");
	    sql.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
	    sql.append(" INNER JOIN rota r WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA  ");
	    sql.append(" INNER JOIN dbCODE cuc WITH (NOLOCK) ON cuc.ID_CODE = h.CD_CODE ");
	    sql.append(" INNER JOIN HISTORICO_FRASE_EVENTO F WITh(NOLOCK) ON F.CD_FRASE_EVENTO = H.CD_FRASE_EVENTO ");
	    // sql.append(" INNER JOIN VIATURA V WITH(NOLOCK) ON V.CD_VIATURA = "
	    // + codigoViatura + " ");
	    // sql.append(" INNER JOIN [CACUPE\\SQL02].Fusion_Producao.dbo.D_OTSViatura OTS WITH(NOLOCK) ON OTS.codigoViatura= CAST(V.CD_VIATURA AS BIGINT) AND OTS.ativaNaFrota = 1 AND OTS.emUso=1 ");
	    // sql.append(" LEFT JOIN HISTORICO H2 WITH(NOLOCK) ON H2.CD_VIATURA = V.CD_VIATURA AND H2.FG_STATUS=9 ");
	    // sql.append(" LEFT JOIN dbCENTRAL C2 WITH(NOLOCK) ON C2.CD_CLIENTE = H2.CD_CLIENTE  ");
	    sql.append(" WHERE H.CD_HISTORICO IN (" + codigosEventos + ")  ");	    
	    sql.append(" ORDER BY PESO, H.DT_RECEBIDO ASC ");

	    Connection connRecalc = null;

	    Statement stmtRecalc = null;

	    ResultSet rsRecalc = null;

	    try {
		connRecalc = PersistEngine.getConnection("SIGMA90");

		stmtRecalc = connRecalc.createStatement();

		rsRecalc = stmtRecalc.executeQuery(sql.toString());

		while (rsRecalc.next()) {

		    EventoVO novoEvento = new EventoVO();

		    novoEvento.setCodigoHistorico(rsRecalc.getString("CD_HISTORICO"));
		    novoEvento.setCodigoEvento(rsRecalc.getString("CD_EVENTO"));
		    novoEvento.setCodigoCliente(rsRecalc.getString("CD_CLIENTE"));
		    novoEvento.setCodigoCentral(rsRecalc.getString("ID_CENTRAL"));
		    novoEvento.setParticao(rsRecalc.getString("PARTICAO"));
		    novoEvento.setFantasia(rsRecalc.getString("FANTASIA"));
		    novoEvento.setEmpresa(rsRecalc.getString("ID_EMPRESA"));
		    novoEvento.setTextoObservacao(rsRecalc.getString("TX_OBSERVACAO_FECHAMENTO") != null ? rsRecalc.getString("TX_OBSERVACAO_FECHAMENTO") : "");
		    novoEvento.setPeso(rsRecalc.getLong("PESO"));
		    novoEvento.setLatitude(rsRecalc.getFloat("NU_LATITUDE"));
		    novoEvento.setLongitude(rsRecalc.getFloat("NU_LONGITUDE"));
		    //novoEvento.setRota(rota);
		    novoEvento.setCodigoViatura(codigoViatura);
		    // novoEvento.setDistancia(rs.getDouble("DISTANCIA"));
		    listaEventosRecalculados.add(novoEvento);

		}
	    } catch (SQLException e) {
		e.printStackTrace();
		log.error("Erro no processamento, procurar no log por [" + key + "]");
		throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	    } finally {
		OrsegupsUtils.closeConnection(connRecalc, stmtRecalc, rsRecalc);
	    }

	    if (listaEventosRecalculados != null && !listaEventosRecalculados.isEmpty()) {

		List<EventoEsperaVO> virtualQueue = this.addToVirtualQueue(listaEventosRecalculados);

		for (EventoEsperaVO evento : virtualQueue) {

		    boolean updateEvent = this.updateEventCode(evento);

		    int updateCount = 0;

		    while (!updateEvent && updateCount < 2) {
			updateEvent = this.updateEventCode(evento);
			updateCount++;
		    }

		}
	    }
	}
    }

    private List<EventoEsperaVO> addToVirtualQueue(List<EventoVO> eventos) {

	long code = 0;

	boolean foundPosition = false;

	List<EventoEsperaVO> eventosFila = new ArrayList<EventoEsperaVO>();

	for (EventoVO event : eventos) {

	    foundPosition = false;

	    EventoEsperaVO newVirtualEvent = new EventoEsperaVO();

	    Collections.sort(eventosFila, new EventoFilaEsperaComparator());

	    if (eventosFila != null && !eventosFila.isEmpty()) {

		Long cont = 1L;
		for (EventoEsperaVO v : eventosFila) {
		    v.setCodigo(cont);
		    cont++;
		}

		long biggerCode = 0L;

		EventoEsperaVO upperEvent = null;

		for (EventoEsperaVO aitEvent : eventosFila) {

		    if (!foundPosition) {

			if (event.getPeso() == aitEvent.getPeso()) {

			    BigDecimal oldEventLat = aitEvent.getLatitude();
			    BigDecimal oldEventLon = aitEvent.getLongitude();

			    double newEventLat = event.getLatitude();
			    double newEventLon = event.getLongitude();

			    if (upperEvent != null) {

				BigDecimal upperEventLat = upperEvent.getLatitude();
				BigDecimal upperEventLon = upperEvent.getLongitude();

				if (upperEventLat != null) {
				    double distanceUpperToOld = this.getDistance(upperEventLat.doubleValue(), upperEventLon.doubleValue(), oldEventLat.doubleValue(), oldEventLon.doubleValue());
				    double distanceUpperToNew = this.getDistance(upperEventLat.doubleValue(), upperEventLon.doubleValue(), newEventLat, newEventLon);

				    if (distanceUpperToNew < distanceUpperToOld) {
					foundPosition = true;
					code = aitEvent.getCodigo();
					aitEvent.setCodigo(code + 1L);
				    } else {
					if (aitEvent.getCodigo() > biggerCode) {
					    biggerCode = aitEvent.getCodigo();
					}
				    }
				}

			    } else {

				List<EventoVO> eventsOnRoad = this.getEventsOnRoad(event.getCodigoViatura());

				if (eventsOnRoad != null && !eventsOnRoad.isEmpty()) {

				    EventoVO eventOnRoad = null;

				    for (EventoVO eventOnList : eventsOnRoad) {
					if (eventOnList.getStatus() == 2) {
					    eventOnRoad = eventOnList;
					}
				    }

				    if (eventOnRoad == null) {
					eventOnRoad = eventsOnRoad.get(0);
				    }

				    double onRoadLat = eventOnRoad.getLatitude();
				    double onRoadLon = eventOnRoad.getLongitude();

				    double distanceOnRoadToOld = this.getDistance(onRoadLat, onRoadLon, oldEventLat.doubleValue(), oldEventLon.doubleValue());
				    double distanceOnRoadToNew = this.getDistance(onRoadLat, onRoadLon, newEventLat, newEventLon);

				    if (distanceOnRoadToNew < distanceOnRoadToOld) {
					foundPosition = true;
					code = aitEvent.getCodigo();
					aitEvent.setCodigo(code + 1);
				    } else {
					if (aitEvent.getCodigo() > biggerCode) {
					    biggerCode = aitEvent.getCodigo();
					}
				    }

				} else {
				    Connection connCar = null;
				    PreparedStatement pstmCar = null;
				    ResultSet rsCar = null;

				    BigDecimal carLat = null;

				    BigDecimal carLon = null;
				    try {

					String sql = "SELECT latitude, longitude FROM D_OTSViatura WITH(NOLOCK) WHERE emUso=1 AND ativaNaFrota=1 AND controlarRota=1 AND codigoViatura=?";

					connCar = PersistEngine.getConnection("");

					pstmCar = connCar.prepareStatement(sql);

					pstmCar.setString(1, event.getCodigoViatura());

					rsCar = pstmCar.executeQuery();

					if (rsCar.next()) {
					    carLat = rsCar.getBigDecimal(1);
					    carLon = rsCar.getBigDecimal(2);
					}

				    } catch (SQLException e) {
					e.printStackTrace();
				    } finally {
					OrsegupsUtils.closeConnection(connCar, pstmCar, rsCar);
				    }

				    if (carLat != null && carLon != null) {

					double distanceCarToOld = this.getDistance(carLat.doubleValue(), carLon.doubleValue(), oldEventLat.doubleValue(), oldEventLon.doubleValue());
					double distancecarToNew = this.getDistance(carLat.doubleValue(), carLon.doubleValue(), newEventLat, newEventLon);

					if (distancecarToNew < distanceCarToOld) {
					    foundPosition = true;
					    code = aitEvent.getCodigo();
					    aitEvent.setCodigo(code + 1);
					} else {
					    if (aitEvent.getCodigo() > biggerCode) {
						biggerCode = aitEvent.getCodigo();
					    }
					}
				    }

				}
			    }

			} else if (event.getPeso() < aitEvent.getPeso()) {
			    foundPosition = true;

			    code = aitEvent.getCodigo();

			    aitEvent.setCodigo(code + 1);

			} else {

			    biggerCode = aitEvent.getCodigo();

			}
		    } else {
			long novoCodigo = aitEvent.getCodigo();
			aitEvent.setCodigo(novoCodigo + 1);
		    }

		    upperEvent = aitEvent;

		    boolean updateEvent = this.updateEventCode(aitEvent);

		    int updateCount = 0;

		    while (!updateEvent && updateCount < 2) {
			updateEvent = this.updateEventCode(aitEvent);
			updateCount++;
		    }
		}

		if (!foundPosition) {
		    code = ++biggerCode;
		}

	    } else {
		code = 1;
	    }

	    newVirtualEvent.setCodigo(code);
	    newVirtualEvent.setCodHistorico(event.getCodigoHistorico());
	    newVirtualEvent.setPeso(event.getPeso());
	    newVirtualEvent.setLatitude(new BigDecimal(event.getLatitude()));
	    newVirtualEvent.setLongitude(new BigDecimal(event.getLongitude()));

	    eventosFila.add(newVirtualEvent);

	}

	return eventosFila;
    }

    private void moveRoutesByWeight(ViaturaVO viatura) {

	Long key = GregorianCalendar.getInstance().getTimeInMillis();

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	List<EventoVO> eventos = new ArrayList<EventoVO>();

	StringBuilder sql = new StringBuilder();

	sql.append(" SELECT h.CD_HISTORICO, h.CD_EVENTO, F.NM_FRASE_EVENTO, ");
	sql.append(" c.CD_CLIENTE,C.ID_CENTRAL, C.PARTICAO, C.FANTASIA, c.ID_EMPRESA, h.TX_OBSERVACAO_FECHAMENTO, ");
	sql.append(" CASE ");
	sql.append("WHEN (C.RAZAO LIKE '%DILMO%BERGER%' OR C.FANTASIA LIKE '%DILMO%BERGER%') THEN 0 ");
	sql.append(" WHEN H.CD_EVENTO IN ('XVID','XPLE') AND H.TX_OBSERVACAO_FECHAMENTO LIKE '%#ComAlteracao%'  THEN 1 ");
	sql.append(" WHEN C.ID_EMPRESA IN (10119, 10120) THEN 2 ");
	sql.append(" WHEN H.CD_EVENTO = 'EODM' THEN 3 ");
	sql.append(" WHEN F.NM_FRASE_EVENTO LIKE '%PANICO%' OR F.NM_FRASE_EVENTO LIKE '%PÂNICO%' THEN 4 ");
	sql.append(" WHEN H.CD_EVENTO = 'XXX7' AND cuc.PRIORIDADE =1 THEN 5 ");
	sql.append(" WHEN EXISTS (SELECT H2.CD_HISTORICO FROM HISTORICO H2 WITH(NOLOCK) WHERE H2.CD_HISTORICO_PAI = H.CD_HISTORICO) THEN 6 ");
	sql.append(" WHEN EXISTS (SELECT RSC.CD_CLIENTE FROM [FSOODB04\\SQL02].TIDB.dbo.ClientesSigmaRSCRRC RSC WHERE RSC.CD_CLIENTE = H.CD_CLIENTE) THEN 7 ");
	sql.append(" WHEN H.CD_EVENTO = 'X406' AND  H.TX_OBSERVACAO_FECHAMENTO LIKE '%#ComAlteracao%' THEN 8 ");
	sql.append(" WHEN cuc.TIPO = 1 THEN 9 ");
	sql.append(" WHEN H.CD_EVENTO IN ('XVID','XPLE') THEN 10 ");
	sql.append(" WHEN H.CD_EVENTO = 'X406' THEN 11 ");
	sql.append(" ELSE 12 ");
	sql.append(" END AS PESO, ");
	sql.append(" C.NU_LATITUDE, C.NU_LONGITUDE ");
	sql.append(" FROM HISTORICO h WITH (NOLOCK) ");
	sql.append(" INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE ");
	sql.append(" INNER JOIN rota r WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA  ");
	sql.append(" INNER JOIN dbCODE cuc WITH (NOLOCK) ON cuc.ID_CODE = h.CD_CODE ");
	sql.append(" INNER JOIN HISTORICO_FRASE_EVENTO F WITh(NOLOCK) ON F.CD_FRASE_EVENTO = H.CD_FRASE_EVENTO ");
	// sql.append(" INNER JOIN VIATURA V WITH(NOLOCK) ON V.CD_VIATURA = " +
	// codigoViatura + " ");
	// sql.append(" INNER JOIN [CACUPE\\SQL02].Fusion_Producao.dbo.D_OTSViatura OTS WITH(NOLOCK) ON OTS.codigoViatura= CAST(V.CD_VIATURA AS BIGINT) AND OTS.ativaNaFrota = 1 AND OTS.emUso=1 ");
	sql.append(" WHERE NOT EXISTS (SELECT codHistorico FROM [CACUPE\\SQL02].Fusion_Producao.dbo.D_SIGMAEventosFilaEspera WITH(NOLOCK) WHERE codHistorico = h.CD_HISTORICO) ");
	sql.append(" AND h.FG_STATUS IN (1) ");
	sql.append(" AND h.CD_HISTORICO_PAI IS NULL ");
	
	if (viatura.getNomeMotorista().toUpperCase().contains("COLAGEM DE PLACAS")){
	    sql.append(" AND H.CD_CODE = 'PLC' ");
	}else{
	    sql.append(" AND H.CD_CODE != 'PLC' ");	    
	}
	
	sql.append(" AND NOT EXISTS (SELECT 1 FROM DBORDEM OS WITH(NOLOCK) WHERE OS.CD_CLIENTE = H.CD_CLIENTE AND OS.FECHADO != 1 AND OS.IDOSDEFEITO = 1020 )");
	
	
	StringBuilder rotas = new StringBuilder();
	
	int count = 0;
	
	List<String> rotasViatura = new ArrayList<String>();
	
	for (RotaVO r : viatura.getListaRotasEfetivas()){
	    
	    rotasViatura.add(r.getNomeRota());
	    
	    String rotaSQL = this.fixBrackets(r.getNomeRota());
	    
	    rotaSQL += "-";
	    
	    if (count == 0){
		
		rotas.append(" r.NM_ROTA LIKE '" + rotaSQL + "%' ESCAPE '\\' " );
		
	    }else{
		rotas.append(" OR r.NM_ROTA LIKE '" + rotaSQL + "%' ESCAPE '\\' " );
	    }
	    
	    count ++;
	    
	}
	
	sql.append(" AND ( "+rotas+" ) ");
	sql.append(" AND (H.TX_OBSERVACAO_GERENTE IS NULL OR H.TX_OBSERVACAO_GERENTE NOT LIKE '%#busy%'  ) ");
	sql.append(" ORDER BY PESO, H.DT_RECEBIDO ASC ");

	try {
	    conn = PersistEngine.getConnection("SIGMA90");

	    stmt = conn.createStatement();

	    rs = stmt.executeQuery(sql.toString());

	    while (rs.next()) {
		EventoVO novoEvento = new EventoVO();

		novoEvento.setCodigoHistorico(rs.getString("CD_HISTORICO"));
		novoEvento.setCodigoEvento(rs.getString("CD_EVENTO"));
		novoEvento.setCodigoCliente(rs.getString("CD_CLIENTE"));
		novoEvento.setCodigoCentral(rs.getString("ID_CENTRAL"));
		novoEvento.setParticao(rs.getString("PARTICAO"));
		novoEvento.setFantasia(rs.getString("FANTASIA"));
		novoEvento.setEmpresa(rs.getString("ID_EMPRESA"));
		novoEvento.setTextoObservacao(rs.getString("TX_OBSERVACAO_FECHAMENTO") != null ? rs.getString("TX_OBSERVACAO_FECHAMENTO") : "");
		novoEvento.setPeso(rs.getLong("PESO"));
		novoEvento.setLatitude(rs.getFloat("NU_LATITUDE"));
		novoEvento.setLongitude(rs.getFloat("NU_LONGITUDE"));
		novoEvento.setRota(Joiner.on(";").join(rotasViatura));
		novoEvento.setCodigoViatura(viatura.getCodigoViatura());
		// novoEvento.setDistancia(rs.getDouble("DISTANCIA"));

		eventos.add(novoEvento);

	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	    log.error("Erro no processamento, procurar no log por [" + key + "]");
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	if (!eventos.isEmpty()) {
	    this.addEventByWeight(eventos, viatura.getCodigoViatura());
	}

    }

    private String fixBrackets(String rota) {

	if (rota != null && !rota.isEmpty()) {
	    rota = rota.replace("[", "\\[");
	    rota = rota.replace("]", "\\]");
	    rota = rota.trim();
	}

	return rota;
    }

    private void addEventByWeight(List<EventoVO> events, String codigoViatura) {

	Long key = GregorianCalendar.getInstance().getTimeInMillis();

	long code = 0;

	boolean foundPosition = false;

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	List<EventoEsperaVO> eventosFila = new ArrayList<EventoEsperaVO>();

	
	try {

	    String sql = "SELECT peso, latitude, longitude, codigo, codHistorico FROM D_SIGMAEventosFilaEspera WITH(NOLOCK) WHERE cdViaturaSigma=" + codigoViatura + " ORDER BY codigo ASC";

	    conn = PersistEngine.getConnection("");

	    stmt = conn.createStatement();

	    rs = stmt.executeQuery(sql);

	    while (rs.next()) {
		EventoEsperaVO novoEvento = new EventoEsperaVO();

		novoEvento.setPeso(rs.getLong(1));
		novoEvento.setLatitude(rs.getBigDecimal(2));
		novoEvento.setLongitude(rs.getBigDecimal(3));
		novoEvento.setCodigo(rs.getLong(4));
		novoEvento.setCodHistorico(rs.getString(5));

		eventosFila.add(novoEvento);

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	    log.error("Erro no processamento, procurar no log por [" + key + "]");
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	for (EventoVO event : events) {

	    foundPosition = false;

	    if (eventosFila != null && !eventosFila.isEmpty()) {

		long biggerCode = 0L;

		EventoEsperaVO upperEvent = null;

		Collections.sort(eventosFila, new EventoFilaEsperaComparator());
		
		Long cont = 1L;
		for (EventoEsperaVO v : eventosFila) {
		    v.setCodigo(cont);
		    cont++;
		}

		for (EventoEsperaVO aitEvent : eventosFila) {

		    if (!foundPosition) {

			if (event.getPeso() == aitEvent.getPeso()) {

			    BigDecimal oldEventLat = aitEvent.getLatitude();
			    BigDecimal oldEventLon = aitEvent.getLongitude();

			    double newEventLat = event.getLatitude();
			    double newEventLon = event.getLongitude();

			    if (upperEvent != null) {

				BigDecimal upperEventLat = upperEvent.getLatitude();
				BigDecimal upperEventLon = upperEvent.getLongitude();

				if (upperEventLat != null) {
				    double distanceUpperToOld = this.getDistance(upperEventLat.doubleValue(), upperEventLon.doubleValue(), oldEventLat.doubleValue(), oldEventLon.doubleValue());
				    double distanceUpperToNew = this.getDistance(upperEventLat.doubleValue(), upperEventLon.doubleValue(), newEventLat, newEventLon);

				    if (distanceUpperToNew < distanceUpperToOld) {
					foundPosition = true;
					code = aitEvent.getCodigo();
					aitEvent.setCodigo(code + 1L);
				    } else {
					if (aitEvent.getCodigo() > biggerCode) {
					    biggerCode = aitEvent.getCodigo();
					}
				    }
				}

			    } else {

				List<EventoVO> eventsOnRoad = this.getEventsOnRoad(event.getCodigoViatura());

				if (eventsOnRoad != null && !eventsOnRoad.isEmpty()) {

				    EventoVO eventOnRoad = null;

				    for (EventoVO eventOnList : eventsOnRoad) {
					if (eventOnList.getStatus() == 2) {
					    eventOnRoad = eventOnList;
					}
				    }

				    if (eventOnRoad == null) {
					eventOnRoad = eventsOnRoad.get(0);
				    }

				    double onRoadLat = eventOnRoad.getLatitude();
				    double onRoadLon = eventOnRoad.getLongitude();

				    double distanceOnRoadToOld = this.getDistance(onRoadLat, onRoadLon, oldEventLat.doubleValue(), oldEventLon.doubleValue());
				    double distanceOnRoadToNew = this.getDistance(onRoadLat, onRoadLon, newEventLat, newEventLon);

				    if (distanceOnRoadToNew < distanceOnRoadToOld) {
					foundPosition = true;
					code = aitEvent.getCodigo();
					aitEvent.setCodigo(code + 1);

				    } else {
					if (aitEvent.getCodigo() > biggerCode) {
					    biggerCode = aitEvent.getCodigo();
					}
				    }

				} else {
				    Connection connCar = null;
				    PreparedStatement pstmCar = null;
				    ResultSet rsCar = null;

				    BigDecimal carLat = null;

				    BigDecimal carLon = null;
				    try {

					String sql = "SELECT latitude, longitude FROM D_OTSViatura WITH(NOLOCK) WHERE emUso=1 AND ativaNaFrota=1 AND controlarRota=1 AND codigoViatura=?";

					connCar = PersistEngine.getConnection("");

					pstmCar = connCar.prepareStatement(sql);

					pstmCar.setString(1, event.getCodigoViatura());

					rsCar = pstmCar.executeQuery();

					if (rsCar.next()) {
					    carLat = rsCar.getBigDecimal(1);
					    carLon = rsCar.getBigDecimal(2);
					}

				    } catch (SQLException e) {
					e.printStackTrace();
				    } finally {
					OrsegupsUtils.closeConnection(connCar, pstmCar, rsCar);
				    }

				    if (carLat != null && carLon != null) {

					double distanceCarToOld = this.getDistance(carLat.doubleValue(), carLon.doubleValue(), oldEventLat.doubleValue(), oldEventLon.doubleValue());
					double distancecarToNew = this.getDistance(carLat.doubleValue(), carLon.doubleValue(), newEventLat, newEventLon);

					if (distancecarToNew < distanceCarToOld) {
					    foundPosition = true;
					    code = aitEvent.getCodigo();
					    aitEvent.setCodigo(code + 1);
					} else {
					    if (aitEvent.getCodigo() > biggerCode) {
						biggerCode = aitEvent.getCodigo();
					    }
					}
				    }

				}
			    }

			} else if (event.getPeso() < aitEvent.getPeso()) {
			    foundPosition = true;

			    code = aitEvent.getCodigo();

			    aitEvent.setCodigo(code + 1);

			} else {

			    biggerCode = aitEvent.getCodigo();

			}
		    } else {
			long novoCodigo = aitEvent.getCodigo();
			aitEvent.setCodigo(novoCodigo + 1);
		    }

		    upperEvent = aitEvent;

		    boolean updateEvent = this.updateEventCode(aitEvent);

		    int updateCount = 0;

		    while (!updateEvent && updateCount < 2) {
			updateEvent = this.updateEventCode(aitEvent);
			updateCount++;
		    }
		}

		if (!foundPosition) {
		    code = ++biggerCode;
		}

	    } else {
		code = 1;
	    }

	    String codigoCliente = event.getCodigoCentral() + "[" + event.getParticao() + "] " + event.getFantasia();

	    codigoCliente = OrsegupsMapsServletAux.retiraCaracteresAcentuados(codigoCliente);

	    event.setCodigoCliente(codigoCliente);
	    event.setCodigo(code);

	    boolean insertEvent = this.insertEventOnQueue(event);

	    int insertCount = 0;

	    while (!insertEvent && insertCount < 2) {
		insertEvent = this.insertEventOnQueue(event);
		insertCount++;
	    }

	    EventoEsperaVO newEventOnQueue = new EventoEsperaVO();

	    newEventOnQueue.setCodHistorico(event.getCodigoHistorico());
	    newEventOnQueue.setCodigo(code);
	    newEventOnQueue.setPeso(event.getPeso());
	    newEventOnQueue.setLatitude(new BigDecimal(event.getLatitude()));
	    newEventOnQueue.setLongitude(new BigDecimal(event.getLongitude()));

	    eventosFila.add(newEventOnQueue);

	}

    }

    private boolean insertEventOnQueue(EventoVO event) {
	
	try {
	    this.deleteExistentEntry(event.getCodigoHistorico());	    
	}catch (Exception e){
	    e.printStackTrace();
	}

	final NeoObject noPS = AdapterUtils.createNewEntityInstance("SIGMAEventosFilaEspera");

	EntityWrapper psWrapper = new EntityWrapper(noPS);

	psWrapper.findField("codHistorico").setValue(event.getCodigoHistorico());
	psWrapper.findField("codRota").setValue(event.getRota());
	psWrapper.findField("dataEspera").setValue(new GregorianCalendar());
	psWrapper.findField("cdCliente").setValue(event.getCodigoCliente());
	psWrapper.findField("cdViaturaSigma").setValue(event.getCodigoViatura());
	psWrapper.findField("codigo").setValue(event.getCodigo());
	psWrapper.findField("peso").setValue(event.getPeso());
	psWrapper.findField("distancia").setValue(new BigDecimal(event.getDistancia()));
	psWrapper.findField("latitude").setValue(new BigDecimal(event.getLatitude()));
	psWrapper.findField("longitude").setValue(new BigDecimal(event.getLongitude()));

	final NeoRunnable work = new NeoRunnable() {
	    public void run() throws Exception {
		PersistEngine.persist(noPS);
	    }
	};

	try {
	    PersistEngine.managedRun(work);
	    return true;
	} catch (final Exception e) {
	    log.error(e.getMessage(), e);
	}

	return false;
    }
    
    private void deleteExistentEntry(String codigoHistorico){
	
	Connection conn = null;
	PreparedStatement pstm = null;


	StringBuilder sql = new StringBuilder();

	try {
	    
	    sql.append(" DELETE D_SIGMAEventosFilaEspera WHERE codHistorico = ? ");
	    
	    conn = PersistEngine.getConnection("");
	    pstm = conn.prepareStatement(sql.toString());
	    
	    pstm.setString(1, codigoHistorico);;

	    pstm.executeUpdate();
	    
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}
    }

    /**
     * Update event code by codHistorico
     * 
     * @param event
     * @return
     */

    private boolean updateEventCode(EventoEsperaVO event) {

	Connection conn = null;
	PreparedStatement pstm = null;

	try {
	    String sql = "UPDATE D_SIGMAEventosFilaEspera SET codigo = ? WHERE codHistorico = ?";

	    conn = PersistEngine.getConnection("");

	    pstm = conn.prepareStatement(sql);

	    pstm.setLong(1, event.getCodigo());
	    pstm.setString(2, event.getCodHistorico());

	    int rs = pstm.executeUpdate();

	    if (rs > 0) {
		return true;
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}

	return false;
    }

    private List<EventoVO> getEventsOnRoad(String carCode) {

	String sql = " SELECT H.FG_STATUS, C.NU_LATITUDE, C.NU_LONGITUDE FROM HISTORICO H WITH(NOLOCK) " + " INNER JOIN DBCENTRAL C WITH(NOLOCK) ON C.CD_CLIENTE = H.CD_CLIENTE " + " WHERE H.CD_VIATURA=" + carCode
		+ " AND H.FG_STATUS IN (2,3,9) ";

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	List<EventoVO> result = null;

	try {
	    conn = PersistEngine.getConnection("SIGMA90");
	    stmt = conn.createStatement();

	    rs = stmt.executeQuery(sql);

	    result = new ArrayList<EventoVO>();

	    while (rs.next()) {
		EventoVO event = new EventoVO();

		event.setStatus(rs.getInt(1));
		event.setLatitude(rs.getFloat(2));
		event.setLongitude(rs.getFloat(3));

		result.add(event);
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	return result;
    }

    private double getDistance(double lat1, double lon1, double lat2, double lon2) {

	return this.distance(lat1, lon1, lat2, lon2, 'K');

    }

    /* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
    /* :: This function converts decimal degrees to radians : */
    /* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
    private double deg2rad(double deg) {
	return (deg * Math.PI / 180.0);
    }

    /* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
    /* :: This function converts radians to decimal degrees : */
    /* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
    private double rad2deg(double rad) {
	return (rad * 180.0 / Math.PI);
    }

    private double distance(double lat1, double lon1, double lat2, double lon2, char unit) {
	double theta = lon1 - lon2;
	double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
	dist = Math.acos(dist);
	dist = rad2deg(dist);
	dist = dist * 60 * 1.1515;
	if (unit == 'K') {
	    dist = dist * 1.609344;
	} else if (unit == 'N') {
	    dist = dist * 0.8684;
	}
	return (dist);
    }
}
