package com.neomind.fusion.custom.orsegups.maps.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.Gson;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.util.NeoUtils;

@WebServlet(name="OrsegupsMapsRouteServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsRouteServlet"})
public class OrsegupsMapsRouteServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	private static final Log log = LogFactory.getLog(OrsegupsMapsRouteServlet.class);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	private void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/plain");
		response.setCharacterEncoding("ISO-8859-1");

		final PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		String action = "";

		if (request.getParameter("action") != null)
		{
			action = request.getParameter("action");
		}
		
		if (action.equals("rotaViatura"))
		{
			String rastreador = request.getParameter("rastreador");
			String dataInical = request.getParameter("dataInical");
			String dataFinal = request.getParameter("dataFinal");
			
			this.rotaViatura(rastreador, dataInical, dataFinal, out);
		}
	}
	private void rotaViatura(String rastreador, String dataInical , String dataFinal,PrintWriter out){
		
	log.warn("##### INICIAR VERIFICAÇÃO DE ROTA DA VIATURA - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
	
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	//LISTA COMANDOS SQL
	List<String> listaCmd = null;

	try
	{	
		conn = PersistEngine.getConnection("SIGMA90");
		
		StringBuilder sql = new StringBuilder();
		sql.append(" select  REPLACE ( latitude , '.' , ',' ) AS LATITUDE, ");
		sql.append("REPLACE ( longitude , '.' , ',' ) AS LONGITUDE, ");
		sql.append(" +'Data: '+  ");
		sql.append(" convert(VARCHAR(100), ");
		sql.append(" convert(VARCHAR(30),e.time,105)+ ' '+ ");
		sql.append(" convert(VARCHAR(30),e.time,108)+'<br/>Velocidade: '+convert(VARCHAR(30), e.speed) +' Km/h')  AS DESCRICAO ");
		sql.append(" from event e where e.id_tracker = '304' ");
		sql.append(" and e.time >= '2014-01-12 00:00:00'   ");
		sql.append(" and e.time <= '2014-01-12 03:00:00'  ");
		sql.append(" order by e.time ");
		

		pstm = conn.prepareStatement(sql.toString());
//		pstm.setString(1, rastreador);
//		pstm.setString(2, dataInical);
//		pstm.setString(3, dataFinal);
	    rs = pstm.executeQuery();
	    
	    listaCmd = new ArrayList<String>();
	    
		while (rs.next())
		{
			String latitude = "";
			String longitude = "";
			String descricao = "";
			
			latitude = rs.getString("LATITUDE");
			longitude = rs.getString("LONGITUDE");
			descricao = rs.getString("DESCRICAO");
			
			listaCmd.add(latitude);
			listaCmd.add(longitude);
			listaCmd.add(descricao);
			
			//Marca evento como atendido
		
			
		}
		
		
		log.warn("##### EXECUTAR VERIFICAÇÃO DE ROTA DA VIATURA - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));

	}
	catch (Exception e)
	{
		e.printStackTrace();
		log.error("##### ERRO VERIFICAÇÃO DE ROTA DA VIATURA: " +e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));
		try
		{
			conn.rollback();
		}
		catch (SQLException e1)
		{
			
			e1.printStackTrace();
		}
	}
	finally
	{ 
		try
		{
			Gson gson = new Gson();
			String rota = gson.toJson(listaCmd);

			out.print(rota);
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO VERIFICAÇÃO DE ROTA DA VIATURA: " +e.getMessage() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));

		}
			log.warn("##### FINALIZAR VERIFICAÇÃO DE ROTA DA VIATURA - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(),"dd/MM/yyyy"));

	}
	}

		
    
}
