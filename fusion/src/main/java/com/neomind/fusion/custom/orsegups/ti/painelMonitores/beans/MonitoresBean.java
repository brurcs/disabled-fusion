package com.neomind.fusion.custom.orsegups.ti.painelMonitores.beans;

public class MonitoresBean {
    
    private int cpuUsage;
    
    private int memoryUsage;

    public int getCpuUsage() {
        return cpuUsage;
    }

    public void setCpuUsage(int cpuUsage) {
        this.cpuUsage = cpuUsage;
    }

    public int getMemoryUsage() {
        return memoryUsage;
    }

    public void setMemoryUsage(int memoryUsage) {
        this.memoryUsage = memoryUsage;
    }
    
    

}
