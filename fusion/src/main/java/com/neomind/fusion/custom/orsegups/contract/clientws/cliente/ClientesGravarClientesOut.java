/**
 * ClientesGravarClientesOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.neomind.fusion.custom.orsegups.contract.clientws.cliente;

public class ClientesGravarClientesOut  implements java.io.Serializable {
    private java.lang.String erroExecucao;

    private java.lang.String mensagemRetorno;

    private com.neomind.fusion.custom.orsegups.contract.clientws.cliente.ClientesGravarClientesOutRetornosClientes[] retornosClientes;

    private java.lang.Integer tipoRetorno;

    public ClientesGravarClientesOut() {
    }

    public ClientesGravarClientesOut(
           java.lang.String erroExecucao,
           java.lang.String mensagemRetorno,
           com.neomind.fusion.custom.orsegups.contract.clientws.cliente.ClientesGravarClientesOutRetornosClientes[] retornosClientes,
           java.lang.Integer tipoRetorno) {
           this.erroExecucao = erroExecucao;
           this.mensagemRetorno = mensagemRetorno;
           this.retornosClientes = retornosClientes;
           this.tipoRetorno = tipoRetorno;
    }


    /**
     * Gets the erroExecucao value for this ClientesGravarClientesOut.
     * 
     * @return erroExecucao
     */
    public java.lang.String getErroExecucao() {
        return erroExecucao;
    }


    /**
     * Sets the erroExecucao value for this ClientesGravarClientesOut.
     * 
     * @param erroExecucao
     */
    public void setErroExecucao(java.lang.String erroExecucao) {
        this.erroExecucao = erroExecucao;
    }


    /**
     * Gets the mensagemRetorno value for this ClientesGravarClientesOut.
     * 
     * @return mensagemRetorno
     */
    public java.lang.String getMensagemRetorno() {
        return mensagemRetorno;
    }


    /**
     * Sets the mensagemRetorno value for this ClientesGravarClientesOut.
     * 
     * @param mensagemRetorno
     */
    public void setMensagemRetorno(java.lang.String mensagemRetorno) {
        this.mensagemRetorno = mensagemRetorno;
    }


    /**
     * Gets the retornosClientes value for this ClientesGravarClientesOut.
     * 
     * @return retornosClientes
     */
    public com.neomind.fusion.custom.orsegups.contract.clientws.cliente.ClientesGravarClientesOutRetornosClientes[] getRetornosClientes() {
        return retornosClientes;
    }


    /**
     * Sets the retornosClientes value for this ClientesGravarClientesOut.
     * 
     * @param retornosClientes
     */
    public void setRetornosClientes(com.neomind.fusion.custom.orsegups.contract.clientws.cliente.ClientesGravarClientesOutRetornosClientes[] retornosClientes) {
        this.retornosClientes = retornosClientes;
    }

    public com.neomind.fusion.custom.orsegups.contract.clientws.cliente.ClientesGravarClientesOutRetornosClientes getRetornosClientes(int i) {
        return this.retornosClientes[i];
    }

    public void setRetornosClientes(int i, com.neomind.fusion.custom.orsegups.contract.clientws.cliente.ClientesGravarClientesOutRetornosClientes _value) {
        this.retornosClientes[i] = _value;
    }


    /**
     * Gets the tipoRetorno value for this ClientesGravarClientesOut.
     * 
     * @return tipoRetorno
     */
    public java.lang.Integer getTipoRetorno() {
        return tipoRetorno;
    }


    /**
     * Sets the tipoRetorno value for this ClientesGravarClientesOut.
     * 
     * @param tipoRetorno
     */
    public void setTipoRetorno(java.lang.Integer tipoRetorno) {
        this.tipoRetorno = tipoRetorno;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientesGravarClientesOut)) return false;
        ClientesGravarClientesOut other = (ClientesGravarClientesOut) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.erroExecucao==null && other.getErroExecucao()==null) || 
             (this.erroExecucao!=null &&
              this.erroExecucao.equals(other.getErroExecucao()))) &&
            ((this.mensagemRetorno==null && other.getMensagemRetorno()==null) || 
             (this.mensagemRetorno!=null &&
              this.mensagemRetorno.equals(other.getMensagemRetorno()))) &&
            ((this.retornosClientes==null && other.getRetornosClientes()==null) || 
             (this.retornosClientes!=null &&
              java.util.Arrays.equals(this.retornosClientes, other.getRetornosClientes()))) &&
            ((this.tipoRetorno==null && other.getTipoRetorno()==null) || 
             (this.tipoRetorno!=null &&
              this.tipoRetorno.equals(other.getTipoRetorno())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getErroExecucao() != null) {
            _hashCode += getErroExecucao().hashCode();
        }
        if (getMensagemRetorno() != null) {
            _hashCode += getMensagemRetorno().hashCode();
        }
        if (getRetornosClientes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRetornosClientes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRetornosClientes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTipoRetorno() != null) {
            _hashCode += getTipoRetorno().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientesGravarClientesOut.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesGravarClientesOut"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("erroExecucao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "erroExecucao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mensagemRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mensagemRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retornosClientes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retornosClientes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesGravarClientesOutRetornosClientes"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipoRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
