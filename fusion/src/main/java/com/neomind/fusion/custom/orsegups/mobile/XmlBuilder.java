package com.neomind.fusion.custom.orsegups.mobile;

import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.transform.stream.StreamResult;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.mobile.vo.AssuntoVO;
import com.neomind.fusion.custom.orsegups.mobile.vo.ClienteVO;
import com.neomind.fusion.custom.orsegups.mobile.vo.PerguntaVO;
import com.neomind.fusion.custom.orsegups.mobile.vo.PesquisaVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.datawarehouse.NeoDataSource;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;


public class XmlBuilder {

	public String clientes() throws SQLException{

		String xml = null;		
		NeoDataSource data = (NeoDataSource) PersistEngine.getObject(NeoDataSource.class,
				new QLEqualsFilter("name", "SAPIENS"));

		Connection con = OrsegupsUtils.openConnection(data);

		String SQLCliente = " SELECT * FROM USU_INSP"; //ORDER";
		//BY EMPRESA, CLIENTE, LOTACAO ";
		// Prepara o statement
		Statement stm = null;


		try {
			stm = (Statement) con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		ResultSet rs = stm.executeQuery(SQLCliente);

		while (rs.next())
		{						
			try{				
				ClienteVO cvo = new ClienteVO();
				cvo.setCodCliente(String.valueOf(rs.getRow()));
				cvo.setCodEmpresa(String.valueOf(rs.getRow()));
				cvo.setCodLotacao(String.valueOf(rs.getRow()));
				cvo.setCodPosto(String.valueOf(rs.getRow()));
				cvo.setNome_cliente(rs.getString("CLIENTE"));
				cvo.setNome_Empresa(rs.getString("EMPRESA"));
				cvo.setNomeLotacao(rs.getString("LOTACAO"));
				cvo.setNomePosto(rs.getString("POSTO"));
				System.out.println(new XmlBuilder().marshal(cvo));

				xml = new XmlBuilder().marshal(cvo);

			}catch(Exception e ){
				System.out.println(e.getMessage());


			}

		}


		return xml;
	}	



	public String Pesquisa() throws SQLException{

		InstantiableEntityInfo pesquisa = (InstantiableEntityInfo) EntityRegister.getInstance().getCache().getByType("Pesquisa");
		List<NeoObject> newObjPesq = (List<NeoObject>) PersistEngine.getObjects(pesquisa.getEntityClass());

		String xml = null;	


		Collection<AssuntoVO> assuntos = new ArrayList<AssuntoVO>();
		Collection<PerguntaVO> perguntas = new ArrayList<PerguntaVO>();

		try{
			for (NeoObject pesquisaQuery : newObjPesq)
			{

				EntityWrapper wrapperPesq = new EntityWrapper(pesquisaQuery);
				
				
			
				for(int i =0; i<= newObjPesq.size(); i++){

					PesquisaVO pvo = new PesquisaVO();
					pvo.setCod_pesq(String.valueOf(wrapperPesq.getObject().getNeoId()));
					pvo.setDesc_pesq((String) wrapperPesq.getValue("pesquisatxt"));
					//pvo.setAssuntos(assuntos);
					System.out.println(new XmlBuilder().marshal(pvo));
					xml = new XmlBuilder().marshal(pvo);
					
				}
				List<NeoObject> newObjAssun = ((List<NeoObject>) wrapperPesq.findValue("neoidAssunto"));

				for (NeoObject assuntoQuery : newObjAssun)
				{

					EntityWrapper wrapperAssun = new EntityWrapper(assuntoQuery);

					for(int i =0; i<= newObjAssun.size(); i++){
						AssuntoVO avo = new AssuntoVO();

						avo.setCod_assun(String.valueOf(wrapperAssun.getObject().getNeoId()));
						avo.setDesc_assun((String) wrapperAssun.getValue("descassun"));
						avo.setPergunta(perguntas);
						assuntos.add(avo);
					}
					List<NeoObject> newObjPerg = ((List<NeoObject>) wrapperAssun.findValue("neoidPergunta"));
					for (NeoObject perguntaQuery : newObjPerg)
					{

						EntityWrapper wrapperPerg = new EntityWrapper(perguntaQuery);

						for(int i =0; i<= newObjPerg.size(); i++){

							PerguntaVO pervo = new PerguntaVO();
							pervo.setCod_perg(String.valueOf(wrapperPerg.getObject().getNeoId()));
							pervo.setDesc_perg((String) wrapperPerg.getValue("perguntatxt"));
							pervo.setTip_resposta((String) wrapperPerg.getValue("tipresposta"));
						
						
						}

					}



	

				}
							
			
			}		  	
		}catch(Exception e ){
			System.out.println(e.getMessage());


		}
		return xml;

	}



	public String Reinspecao() throws SQLException{
		
		
		
		return null;

	}




public String marshal(Object object) {
	final StringWriter out = new StringWriter();
	JAXBContext context = null;
	try {
		context = JAXBContext.newInstance(object.getClass());
		Marshaller marshaller = context.createMarshaller();
		marshaller.setProperty(
				javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT,
				Boolean.TRUE
				);
		marshaller.marshal(object, new StreamResult(out));
	} catch (PropertyException e) {
		e.printStackTrace();
	} catch (JAXBException e) {
		e.printStackTrace();
	}
	return out.toString();
}
}


