package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.jfree.util.Log;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.contract.CadastraCentroCustoClienteContrato;
import com.neomind.fusion.custom.orsegups.contract.CentroCusto;
import com.neomind.fusion.custom.orsegups.contract.ContractSIDClient;
import com.neomind.fusion.custom.orsegups.contract.ContratoContasSIGMAWebService;
import com.neomind.fusion.custom.orsegups.contract.ContratoIntegracaoSapiens;
import com.neomind.fusion.custom.orsegups.contract.ContratoLogUtils;
import com.neomind.fusion.custom.orsegups.contract.ContratoUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class CadastraCentroCusto implements AdapterInterface
{
	
	private static String FIELDS_E044CCU = "( CodEmp,CodCcu,DesCcu,AbrCcu,CodUsu,TipCcu,IndAgr,CcuPai,AgrTax,MskCcu,ClaCcu,GruCcu,NivCcu,PosCcu,AnaSin,AceRat,CriRat,CtaRed,CtaRcr,CtaFdv,CtaFcr,ClaDes,CodLoc,LocRea, "+
			   "  PlaSeg,TaxIcu,CodTur,USU_CliNom,USU_LotNom,USU_EmpNom,USU_NumCtr,USU_GerNom,indexp,usu_codreg,datalt,horalt,usualt) ";

	private static String FIELDS_E043PCM = "( CodMpc,CtaRed,MskGcc,DefGru,ClaCta,DesCta,AbrCta,AnaSin,NatCta,NivCta,ExiRat,ForRat,ModCtb,CtaCtb,CodCcu, "+
			   "  TipCcu,ExiAux,MetCon,datalt,horalt,usualt,datger,horger,usuger,usu_codreg,valini,valfin ) ";

	/*
	 * (non-Javadoc)
	 * @see com.neomind.fusion.entity.EntityAdapter#run(java.util.Map)
	 * 
	 * - Os metodos para cadastrar devem ser isolados, pois contrato e cliente devem ser criados antes dos postos
	 * - A variável aClaCta deve perpetuar em todos os métodos
	 * - O contrato novo deve ser inserido no campo numContrato
	 * - O cliente novo deve ser inserido no campo buscaNomeCliente 
	 */
	@Override
	public void start(Task origin, EntityWrapper wrapper, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		Long key = (new GregorianCalendar()).getTimeInMillis();
		String numContrato = null;
		try{
			//long movimentacao = (Long) wrapper.findValue("movimentoContratoNovo.codTipo");
			Long codEmpresa = (Long) wrapper.findField("empresa.codemp").getValue();
			Long codFilial = (Long) wrapper.findField("empresa.codfil").getValue();
			String codCli = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.codigoCliente") != null ? wrapper.findValue("novoCliente.codigoCliente") : wrapper.findValue("buscaNomeCliente.codcli"));
			numContrato = NeoUtils.safeOutputString(wrapper.findValue("dadosGeraisContrato.numeroContratoSapiens"));
			String aCodEmp = String.valueOf(codEmpresa);
			String aCodFil = String.valueOf(codFilial);
			String aCodCli = codCli;
			String aNumeroContrato = String.valueOf(numContrato);
			NeoObject cliente = (NeoObject)  wrapper.findValue("buscaNomeCliente");
			EntityWrapper wCliente = new EntityWrapper(cliente);
			String aNomCli = NeoUtils.safeOutputString(wCliente.findValue("nomcli"));
			
			List<NeoObject> postos = (List<NeoObject>)wrapper.findField("postosContrato").getValues();
			
			String aCtared7Nivel = "";
			String aClacta7Nivel = "";
			
			ContratoLogUtils.logInfo("["+key+"] Ctr: "+numContrato + "Cadastrando Centro de custo de 7 e 8 niveis");
			for(NeoObject posto : postos)
			{
				
				EntityWrapper wPosto = new EntityWrapper(posto);
				
				NeoObject oTipoPosto = (NeoObject) wPosto.findValue("tipoPosto");
				EntityWrapper wTipoPosto = new EntityWrapper(oTipoPosto);
	
				//fernanda martins solicitou que fosse exigido pelo menos 2 contatos no posto
				Collection<NeoObject> contatos = wPosto.findField("vigilanciaEletronica.listaContatosPosto").getValues();
				if (contatos != null && contatos.size() < 2 && NeoUtils.safeOutputString(wTipoPosto.findValue("codigo")).equals("1") ){
					throw new Exception("Erro: Para cada posto, deve haver pelo menos 2 contatos.");
				}
					
				
				Long nivccu = NeoUtils.safeLong(NeoUtils.safeOutputString(wPosto.findValue("centroCusto.nivccu")));
				String codccu = NeoUtils.safeOutputString(wPosto.findValue("centroCusto.codccu"));
				String aClaCta = NeoUtils.safeOutputString(wPosto.findValue("centroCusto.claccu"));
				
				ContratoIntegracaoSapiens integracaoCCSapiens = ContratoIntegracaoSapiens.getInstance();
				
				if ( NeoUtils.safeLong(NeoUtils.safeOutputString(wrapper.findValue("movimentoContratoNovo.codTipo"))) == 5
					 && 
					 NeoUtils.safeLong(NeoUtils.safeOutputString(wrapper.findValue("movimentoAlteracaoContrato.codTipo"))) != 1
					){
					ContratoLogUtils.logInfo("["+key+"] Ctr: "+numContrato+" Centro de custo 7 e 8 não será gerado aqui, pois, será reaproveitado centro de custo do posto antigo");
				}else{
					ContratoLogUtils.logInfo("["+key+"] Ctr: "+numContrato + " Vai ser gerado centro de custo pois é um posto novo");
					/*
					 * se está no nível 6 é para cadastrar o nível 7 para baixo
					 * O nível 7 é a Sublotação
					 */
					if(nivccu != null && nivccu == 6)
					{
						ContratoLogUtils.logInfo("["+key+"] Ctr: "+numContrato + " (nivccu != null && nivccu == 6) ");
						String aCtaRed = codccu;
						ContratoLogUtils.logInfo("["+key+"] Ctr: "+numContrato + " Variável aCtared7Nivel ='" + aCtared7Nivel+"'");
						if (!aCtared7Nivel.equals("")){ // se houver 7 nível, só cadastra o 8 nivel
							 
							String codccu8 = integracaoCCSapiens.insertPosto(wrapper.getObject(), aCtared7Nivel, aClacta7Nivel);
							wPosto.findField("codccu").setValue(NeoUtils.safeString(codccu8));
							
						}else{ //se não houver centro de custo de 7 nivel, cadastra 7 e 8 niveis
							ContratoLogUtils.logInfo("["+key+"] Ctr: "+numContrato + " Posto.codccu =" + wPosto.findValue("codccu"));
							if (wPosto.findValue("codccu") == null){
								if(!aClaCta.equals("") && aClaCta != null){
									ContratoUtils.verificarNiveisCcuExistentes(aClaCta);
								}
								aCtaRed = integracaoCCSapiens.insertLotacao(wrapper.getObject(), codccu, aClaCta); // cadastra 7 nivel
								ContratoUtils.addCCToRollBack(NeoUtils.safeLong(numContrato), NeoUtils.safeLong(aCtaRed));
								ContratoLogUtils.logInfo("["+key+"] Ctr: "+numContrato + "CC inserido 7:"+aCtaRed);
							}else{
								ContratoLogUtils.logInfo("["+key+"] Ctr: aCtaRed = (String) wPosto.findValue(\"codccu\") ");
								aCtaRed = (String) wPosto.findValue("codccu");
							}
		
							aClacta7Nivel = CentroCusto.getClassificacaoPCM(aCodEmp, aCtaRed);
							aCtared7Nivel = aCtaRed;
							
							String codccu8 = integracaoCCSapiens.insertPosto(wrapper.getObject(), aCtared7Nivel, aClacta7Nivel); // cadastra 8 nivel
							if (codccu8!= null){
								wPosto.findField("codccu").setValue(NeoUtils.safeString(codccu8));
								ContratoUtils.addCCToRollBack(NeoUtils.safeLong(numContrato), NeoUtils.safeLong(codccu8));
								ContratoLogUtils.logInfo("["+key+"] Ctr: "+numContrato + " CC inserido 8: "+codccu8);
							}
							
						}
						
					}
				}
			}
			
		}catch(Exception e){
			ContratoLogUtils.logInfo("["+key+"] Ctr: "+numContrato + "Erro ao cadastrar centros de custo. "+e.getMessage());
			ContratoUtils.rollBackCC(NeoUtils.safeLong(numContrato), ContratoUtils.retornaListaCCToRollBack(NeoUtils.safeLong(numContrato)), "CADASTRACENTROCUSTO - " + origin.getCode() );
			e.printStackTrace();
			throw new WorkflowException("["+key+"] - Erro ao cadastrar centros de custo. " + e.getMessage());
		}
		
		//wrapper.findField("responsavelPelaConta").setValue(wrapper.findValue("responsavelPelaConta2"));
	}
	
	public static void atualizaContratoCodigoAgrupador(String codEmp, String codFil, String numCtr,  String codigoAgr) throws Exception {
		/*StringBuilder query = new StringBuilder();
		query.append(" UPDATE usu_T160CTR SET ");
		query.append("usu_ctragr= '"+codigoAgr+"'");       // int
		query.append(" WHERE usu_codemp = " + codEmp + " and usu_codfil = " + codFil + " and usu_numctr = " + numCtr + " and usu_sitctr = 'A'");

		Query queryExecute = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(query.toString());
		try
		{
			queryExecute.executeUpdate();
		}catch(Exception e)
		{
			System.out.println("[FLUXO CONTRATOS] - ERRO " + query);
			e.printStackTrace();
			throw new Exception("Erro ao integrar com o Sapiens. Favor tentar novamente em instantes. Oitavo nível do Centro de Custo");
		}
		*/
		String nomeFonteDados = "SAPIENS";
		
		Connection connection = OrsegupsUtils.getSqlConnection(nomeFonteDados);
		
		String empresa = codEmp;
		String filial = codFil;
		String contrato = numCtr;
		String codAgru = codigoAgr;
		StringBuffer sql = new StringBuffer();
		sql.append("UPDATE USU_T160CTR ");
		sql.append("SET usu_ctragr = ? ");
		sql.append("WHERE USU_CODEMP = ? ");
		sql.append("AND USU_CODFIL = ? ");
		sql.append("AND USU_NUMCTR = ? ");
		PreparedStatement st = null;
		try
		{
			st = connection.prepareStatement(sql.toString());
			st.setString(1, codAgru);
			st.setString(2, empresa);
			st.setString(3, filial);
			st.setString(4, contrato);

			st.executeUpdate();

		}
		catch (SQLException e)
		{
			System.out.println("[FLUXO CONTRATOS] - ERRO " + sql.toString());
			e.printStackTrace();
			throw new Exception("Erro ao integrar com o Sapiens. Favor tentar novamente em instantes. Oitavo nível do Centro de Custo");
		}
		finally{
			OrsegupsUtils.closeConnection(connection, st, null);;
		}
		
	}
	
	/**
	 * Atualiza razão social na arvore de centro de custos e altera a razão social no cadastro do cliente
	 * @param wrapper
	 */
	public static void updateRazaoSocial(EntityWrapper wrapper)
	{
		
		String codEmp = NeoUtils.safeOutputString(wrapper.findValue("empresa.codemp"));
		String codCli = NeoUtils.safeOutputString(wrapper.findValue("buscaNomeCliente.codcli"));
		String numeroContrato = NeoUtils.safeOutputString(wrapper.findValue("numContrato.usu_numctr"));
		String cgcCpf = null;
		
		
		String razaoSocialOld = "";
		String razaoSocial4 = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.razaoSocial"));
		String razaoSocial5 = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.razaoSocial"));
		String razaoSocial6 = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.razaoSocial"));
		String razaoSocial7 = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.razaoSocial"));
		String razaoSocial8 = NeoUtils.safeOutputString(wrapper.findValue("novoCliente.razaoSocial"));
		/*String razaoSocial5 = "CTR - " + NeoUtils.safeOutputString(wrapper.findValue("novoCliente.razaoSocial"));
		String razaoSocial6 = "SUB - " + NeoUtils.safeOutputString(wrapper.findValue("novoCliente.razaoSocial"));
		String razaoSocial7 = "LOT - " + NeoUtils.safeOutputString(wrapper.findValue("novoCliente.razaoSocial"));
		String razaoSocial8 = "PTO - " + NeoUtils.safeOutputString(wrapper.findValue("novoCliente.razaoSocial"));*/
		
		
		
		String queryCli = "select NomCli, cast( cgccpf as Varchar(20) ) cgccpf from e085cli where codCli = "+codCli;
		
		Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(queryCli);
		Collection<Object[]> resultList = query.getResultList();
		
		if (resultList != null && resultList.size() >0){
			for(Object[] r : resultList){
				razaoSocialOld = (String) r[0];
				cgcCpf = (String)r[1];
			}
		}else{
			Log.warn("updateRazaoSocial() - Nome antigo do cliente "+codCli+" não encontrado " );
		}
		
		// Altera razao social do cliente.
		if (codCli != null && !codCli.equals("") && razaoSocial4 != null && !razaoSocial4.equals("")){
			ContractSIDClient.alteraRazaoSocial(Long.parseLong(codCli), razaoSocialOld ,razaoSocial4);
		}
		
		String claCcu = getClassificacaoCentroCusto(codEmp, codCli, 3, numeroContrato);
		String ccu = getCentroCusto(claCcu, 3, codEmp);
		
		String classificacaoQuartoNivel = CadastraCentroCusto.getClassificacaoCentroCusto(codEmp, codCli, 4, numeroContrato);
		if (classificacaoQuartoNivel == null || "".equals(classificacaoQuartoNivel) ){ // busca nas tarefas do fusion um arvore para reutilizar
			classificacaoQuartoNivel = CadastraCentroCusto.getClassificacaoCC4NivelTarefas(codEmp, codCli);
		}
		String centroCustoQuartoNivel = CadastraCentroCusto.getCentroCusto(classificacaoQuartoNivel, 4, codEmp);

		
		System.out.println("[FLUXO CONTRATOS]-CC -  Razao: "+razaoSocial4+", codEmp= "+codEmp+ ", codCli="+ codCli+ ", contrato= "+numeroContrato+", claCcu="+ claCcu + ", ccu3niv=" + ccu);
		
		Collection<String> listaCC4 = atualizaCCQuartoNivel(ccu, razaoSocialOld, razaoSocial4, classificacaoQuartoNivel,4);
		for(String cc4 : listaCC4)
		{
			//System.out.println("4>"+cc4);
			Collection<String> listaCC5 = atualizaCCQuintoNivel(cc4, razaoSocialOld, razaoSocial5, classificacaoQuartoNivel,5);
			for(String cc5 : listaCC5)
			{
				//System.out.println("5>>>"+cc5);
				Collection<String> listaCC6 = atualizaCCSextoNivel(cc5, razaoSocialOld, razaoSocial6, classificacaoQuartoNivel,6);
				for(String cc6 : listaCC6)
				{
					//System.out.println("6>>>>>"+cc6);
					Collection<String> listaCC7 = atualizaCCSetimoNivel(cc6,  razaoSocialOld, razaoSocial7, classificacaoQuartoNivel,6);
					for(String cc7 : listaCC7)
					{
						//System.out.println("7>>>>>>>"+cc7);
						
						Collection<String> listaCC8 = atualizaCCOitavoNivel(cc7,  razaoSocialOld, razaoSocial8, classificacaoQuartoNivel,8);
						for(String cc8 : listaCC8)
						{
							ContratoContasSIGMAWebService.renomeiaRazaoClienteSigma(cgcCpf, razaoSocialOld, razaoSocial4);
							//System.out.println("8>>>>>>>"+cc8);
						}
					}
				}
			}
		}
	}
	
	

	public static Collection<String> atualizaCCQuartoNivel(String ccu, String razaoAntiga, String razaoSocialNova, String claCta, int nivelAtualizar)
	{
		Collection<String> listaCcu = atualizaCC2(ccu, razaoAntiga, razaoSocialNova, claCta, 4);
		return listaCcu;
	}
	
	public static Collection<String> atualizaCCQuintoNivel(String ccu, String razaoAntiga, String razaoSocialNova, String claCta, int nivelAtualizar)
	{
		Collection<String> listaCcu = atualizaCC2(ccu, razaoAntiga, razaoSocialNova, claCta, 5);
		
		return listaCcu;
	}
	
	public static Collection<String> atualizaCCSextoNivel(String ccu, String razaoAntiga, String razaoSocialNova, String claCta, int nivelAtualizar)
	{
		Collection<String> listaCcu = atualizaCC2(ccu, razaoAntiga, razaoSocialNova, claCta, 6);
		
		return listaCcu;
	}
	
	public static Collection<String> atualizaCCSetimoNivel(String ccu, String razaoAntiga, String razaoSocialNova, String claCta, int nivelAtualizar)
	{
		Collection<String> listaCcu = atualizaCC2(ccu, razaoAntiga, razaoSocialNova, claCta, 7);
		
		return listaCcu;
	}
	
	public static Collection<String> atualizaCCOitavoNivel(String ccu, String razaoAntiga, String razaoSocialNova, String claCta, int nivelAtualizar)
	{
		Collection<String> listaCcu = atualizaCC2(ccu, razaoAntiga, razaoSocialNova, claCta, 8);
		return listaCcu;
	}
	
	public static Collection<String> atualizaCC(String ccu, String razaoSocial, int nivel)
	{
		System.out.println("[FLUXO CONTRATOS]-Atualizando CC, ccuPai="+ccu+", Nova razao social="+ razaoSocial+", "+nivel );
		Collection<String> listaCcu = new ArrayList<String>();
		/*
		 * buscar todos o CCU de oitavo nivel para atualizar
		 * atualizar eles
		 * para cada CCU de oitavo nivel, atualizar na PCM este CCU com o oitavo nivel
		 */
		String razaoSocial20 = razaoSocial;
		String razaoSocial80 = razaoSocial;
		
		if(razaoSocial.length() > 20)
			razaoSocial20 = razaoSocial.substring(0, 20);

		if(razaoSocial.length() > 80)
			razaoSocial80 = razaoSocial.substring(0, 80);
		
		//TODO fazer o select e buscar os CC para atualizar
		StringBuilder sql = new StringBuilder();
		sql.append("select ccu.CodCcu "); 
		sql.append("from E044CCU ccu ");
		sql.append("WHERE ");
		sql.append("ccu.CcuPai = " + ccu);
		
		Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		Collection<Object> resultList = query.getResultList();
		
		String ccuControle = "";
		if (resultList != null && !resultList.isEmpty())
		{
			for (Object result : resultList)
			{
				if (result != null)
				{
					String ccuNivel = NeoUtils.safeOutputString(result);
					listaCcu.add(ccuNivel);
					
					if(ccuControle.equals(""))
						ccuControle = ccuNivel;
					
					//UPDATE E044CCU
					String update = " UPDATE E044CCU SET DESCCU = '" + razaoSocial80 + "', ABRCCU = '" + razaoSocial20 + "' WHERE CodCcu = " + ccuNivel;
					sql = new StringBuilder();
					sql.append(update);
					Query queryExecute = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
					try
					{
						queryExecute.executeUpdate();
					}catch(Exception e)
					{
						throw new WorkflowException("Erro " + e.getMessage());
					}
					
					if(ccuControle.equals("") || !ccuControle.equals(ccuNivel))
					{
						ccuControle = ccuNivel;
						
						String update2 = " UPDATE E043PCM SET DESCTA = '" + razaoSocial80 + "', ABRCTA = '" + razaoSocial20 + "' WHERE NivCta = "+ nivel +" and CtaRed = " + ccuNivel;
						sql = new StringBuilder();
						sql.append(update2);
						queryExecute = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
						try
						{
							queryExecute.executeUpdate();
						}catch(Exception e)
						{
							throw new WorkflowException("Erro:" + e.getMessage());
						}
					}
				}
			}
		}
		
		return listaCcu;
	}
	
	
	public static Collection<String> atualizaCC2(String ccu, String razaoAntiga, String razaoSocialNova, String claCta, int nivelAtualizar)
	{
		//System.out.println("Atualizando CC, ccuPai="+ccu+", Nova razao social="+ razaoSocialNova+", "+nivelAtualizar );
		Collection<String> listaCcu = new ArrayList<String>();
		
		String razaoSocial20 = razaoSocialNova;
		String razaoSocial80 = razaoSocialNova;
		
		if(razaoSocialNova.length() > 20)
			razaoSocial20 = razaoSocialNova.substring(0, 20);

		if(razaoSocialNova.length() > 80)
			razaoSocial80 = razaoSocialNova.substring(0, 80);
		
		
		StringBuilder sql = new StringBuilder();
		sql.append("select distinct ccu.CodCcu "); 
		sql.append("from E044CCU ccu ");
		sql.append("WHERE ");
		sql.append("ccu.CcuPai = " + ccu);
		sql.append(" and claCcu like '" +claCta+"%' and nivccu =" +nivelAtualizar);
		
		Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		Collection<Object> resultList = query.getResultList();
		
		//String ccuControle = "";
		if (resultList != null && !resultList.isEmpty())
		{
			for (Object result : resultList)
			{
				if (result != null)
				{
					String codccu = NeoUtils.safeOutputString(result);
					listaCcu.add(codccu);
					
					//UPDATE E044CCU
					String update = " UPDATE E044CCU SET DESCCU = replace(DESCCU, '"+ razaoAntiga + "', '"+razaoSocial80+"') , ABRCCU = '" + razaoSocial20 + "' WHERE CodCcu = " + codccu;
					sql = new StringBuilder();
					sql.append(update);
					Query queryExecute = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
					try
					{
						System.out.println("[FLUXO CONTRATOS]- "+update);
						queryExecute.executeUpdate();
					}catch(Exception e)
					{
						throw new WorkflowException("Erro atualizando e044ccu");
					}
						
					String update2 = " UPDATE E043PCM SET DESCTA = replace(DESCTA, '"+ razaoAntiga + "', '"+razaoSocial80+"')  WHERE NivCta = "+ nivelAtualizar +" and CtaRed = " + codccu;
					sql = new StringBuilder();
					sql.append(update2);
					queryExecute = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
					try
					{
						System.out.println("[FLUXO CONTRATOS]- " +update2);
						queryExecute.executeUpdate();
					}catch(Exception e)
					{
						throw new WorkflowException("Erro atualizando e043pcm");
					}

				}
			}
		}
		
		return listaCcu;
	}
	
	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		
	}
	
	public Collection<String> getEmpresasSapiens()
	{
		List<NeoObject> empresas = (List<NeoObject>)PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE070FIL"));
		Collection<String> codigos = new ArrayList<String>();
		for(NeoObject empresa : empresas)
		{
			EntityWrapper w = new EntityWrapper(empresa);
			String codigo = NeoUtils.safeOutputString(w.findValue("codemp"));
			if(!codigos.contains(codigo))
				codigos.add(codigo);
		}
		
		return codigos;
	}
	
	public static String getCentroCusto(String claCcu, int nivelCC, String codEmp)
	{
		//1.2.33.44444.555.66.7777.88
		/*
		 * 	2 nivel = 1.2 = 2
		 *  3 nivel = 1.2.33 = 4
		 *  4 nilvl = 1.2.33.44444 = 9
		 *  5 nivel = 1.2.33.44444.555 = 12
		 *  6 nivel = 1.2.33.44444.555.66 = 14
		 *  7 nivel = 1.2.33.44444.555.66.7777 = 18
		 *  8 nivel = 1.2.33.44444.555.66.7777.88 = 20
		 */
		if(claCcu.equals(""))
			return "";
		
		int nivel = 1;
		if(nivelCC == 3)
		{
			nivel = 4;
		}else if(nivelCC == 4)
		{
			nivel = 9;
		}else if(nivelCC == 5)
		{
			nivel = 12;
		}else if(nivelCC == 6)
		{
			nivel = 14;
		}else if(nivelCC == 7)
		{
			nivel = 18;
		}else
		{
			nivel = 20;
		}
		
		String claCcuNivel = claCcu.substring(0, nivel);
		
		/**
		 * @author orsegups lucas.avila - case sensitive e ValueOf para Long.
		 * @date 08/07/2015
		 */
		
		QLGroupFilter gp = new QLGroupFilter("AND");
		gp.addFilter(new QLEqualsFilter("claccu", claCcuNivel));
		gp.addFilter(new QLEqualsFilter("nivccu", nivelCC));
		gp.addFilter(new QLEqualsFilter("codemp", Long.valueOf(codEmp)));
		Collection<NeoObject> contas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), gp);
		String codCcu = "";
		for(NeoObject conta : contas)
		{
			EntityWrapper w = new EntityWrapper(conta);
			codCcu = NeoUtils.safeOutputString(w.findValue("codccu"));
			System.out.println("[FLUXO CONTRATOS]- codCcu: "+ codCcu);
		}
		
		return codCcu;
	}
	
	
	
	public static String getClassificacaoCentroCusto(String codEmp, String codCli, int nivelCC, String numeroContrato)
	{
		String classificacaoCentroCusto = "";
		String codigo = "";
		if(codEmp.equals("15"))
			codigo = "16" + codEmp;
		else
			codigo = "15" + codEmp;
		
		if(nivelCC == 5)
		{

		}
		
		//1.2.33.44444.555.66.7777.88
		/*
		 * 	2 nivel = 1.2 = 2
		 *  3 nivel = 1.2.33 = 4
		 *  4 nilvl = 1.2.33.44444 = 9
		 *  5 nivel = 1.2.33.44444.555 = 12
		 *  6 nivel = 1.2.33.44444.555.66 = 14
		 *  7 nivel = 1.2.33.44444.555.66.7777 = 18
		 *  8 nivel = 1.2.33.44444.555.66.7777.88 = 20
		 */
		int nivel = 1;
		if(nivelCC == 3)
		{
			nivel = 4;
		}else if(nivelCC == 4)
		{
			nivel = 9;
		}else if(nivelCC == 5)
		{
			nivel = 12;
		}else if(nivelCC == 6)
		{
			nivel = 14;
		}else if(nivelCC == 7)
		{
			nivel = 18;
		}else
		{
			nivel = 20;
		}
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT MIN(SUBSTRING(E044CCU.CLACCU,1,"+nivel+")) ");
		sql.append("FROM E044CCU, USU_T160CVS, USU_T160CTR, E043PCM ");
		sql.append("WHERE E044CCU.CODEMP = "+codEmp+" AND ");
		sql.append("USU_T160CTR.USU_CODCLI = "+codCli+" AND  ");
		sql.append("E044CCU.CODCCU = USU_T160CVS.USU_CODCCU AND  ");
		sql.append("E044CCU.CODEMP = USU_T160CVS.USU_CODEMP AND  ");
		sql.append("E043PCM.CLACTA = E044CCU.CLACCU AND  ");
		if(numeroContrato.equals(""))
			sql.append("E044CCU.CLACCU LIKE '"+codigo+"%' AND ");
		else
			sql.append("USU_T160CTR.USU_NUMCTR = '"+numeroContrato+"' AND ");
		
		sql.append("SUBSTRING(E044CCU.CLACCU,1,9) NOT IN ('150100060','150200051','150500015','150600032','150700039','150800025','151400039','151502660','151700002','151800002','150101549','161500148') AND   ");
		sql.append("USU_T160CTR.USU_CODEMP = USU_T160CVS.USU_CODEMP AND ");
		sql.append("USU_T160CTR.USU_CODFIL = USU_T160CVS.USU_CODFIL AND ");
		sql.append("USU_T160CTR.USU_NUMCTR = USU_T160CVS.USU_NUMCTR ");
		System.out.println("[FLUXO CONTRATOS]- "+sql.toString());
		Query query = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		query.setMaxResults(1);
		Collection<Object> resultList = query.getResultList();
		if (resultList != null && !resultList.isEmpty())
		{
			for (Object result : resultList)
			{
				if (result != null)
				{
					classificacaoCentroCusto = NeoUtils.safeOutputString(result);
				}
			}
		}
		
		return classificacaoCentroCusto;
	}
	
	/**
	 * Verifica se há centro de custo já gerado para esse cliente dentro das tarefas do fusion. 
	 * Isso é feito para o caso de contratos de cliente novo e eletrônica e contratos de CFTV que forem
	 * lançados antes de o primeiro contrato do cliente ser validado no fusion.
	 * @param codEmp
	 * @param codCli
	 * @return
	 */
	public static String getClassificacaoCC4NivelTarefas(String codEmp,String codCli) {
		ContratoLogUtils.logInfo("Consultando centro de custo na tarefa para o cliente: " + codCli + " na empresa: " + codEmp);
		
		String classificacaoCentroCusto = "";
		String sql =  "select min(classificacaoCentroCusto4Nivel)  from D_FGCPrincipal p  "+
						" inner join D_FGCClientesCadastroSapiens cli on (p.novoCliente_neoId = cli.neoId) "+
						" inner join X_SAPIENSE070FIL fil on (p.empresa_neoId = fil.neoId)" +
						" where cli.codigoCliente =  "+ codCli +
						" and fil.codemp = " + codEmp;
		
		ContratoLogUtils.logInfo("query: " + sql);
		
		Query query = PersistEngine.getEntityManager().createNativeQuery(sql);
		Collection<Object> resultList = query.getResultList();
		if (resultList != null && !resultList.isEmpty())
		{
			for (Object result : resultList)
			{
				if (result != null)
				{
					classificacaoCentroCusto = NeoUtils.safeOutputString(result);
				}
			}
		}
		
		return classificacaoCentroCusto;
	}
	
	
	
	
	
}
