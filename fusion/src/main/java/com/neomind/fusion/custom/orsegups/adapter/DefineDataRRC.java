package com.neomind.fusion.custom.orsegups.adapter;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.vetorh.TrocaEscalaVO;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class DefineDataRRC implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		String iff = "";
		try
		{
			GregorianCalendar dataResponderRRCTemp = new GregorianCalendar();
			
			iff = "1";
			// responder e responder escalada
			if (activity.getName().equals("Definir Prazo para Responder RRC") && processEntity.findValue("responderRRCEscalada") == null)
			{
				GregorianCalendar dataResponderRRC = OrsegupsUtils.getSpecificWorkDay(dataResponderRRCTemp, 1L);
				dataResponderRRC.set(Calendar.HOUR_OF_DAY, 23);
				dataResponderRRC.set(Calendar.MINUTE, 59);
				dataResponderRRC.set(Calendar.SECOND, 59);
				processEntity.setValue("dataResponderRrc", dataResponderRRC);
				processEntity.setValue("responderRRCEscalada", "Responder RRC Gerente Escalada");
				
				processEntity.setValue("aprovarRRCEscalada", "Aprovar Resposta RRC");
				PersistEngine.persist(processEntity.getObject());
			}
			
			iff = "2";
			if (activity.getName().equals("Definir Prazo para Responder RRC - Gerente Escalada") && processEntity.findValue("responderRRCEscalada").equals("Responder RRC Gerente Escalada"))
			{
				GregorianCalendar dataResponderRRC = OrsegupsUtils.getSpecificWorkDay(dataResponderRRCTemp, 1L);
				dataResponderRRC.set(Calendar.HOUR_OF_DAY, 23);
				dataResponderRRC.set(Calendar.MINUTE, 59);
				dataResponderRRC.set(Calendar.SECOND, 59);
				
				processEntity.findField("dataResponderRrcGerenteEscalada").setValue(dataResponderRRC);
				processEntity.setValue("responderRRCEscalada", "Responder RRC Superintendente Escalada");
				
				//quando for escalado pelo Tratar RRC não passa pela pela condição de cima, ou seja não for avançado pelo Tratar RRC
				processEntity.setValue("aprovarRRCEscalada", "Aprovar Resposta RRC");
				
				processEntity.setValue("recusaRRC", Boolean.FALSE);
				processEntity.setValue("esconderRecusaRRC", Boolean.TRUE);
				PersistEngine.persist(processEntity.getObject());
			}
			
			iff = "3";
			if (activity.getName().equals("Definir Prazo para Responder RRC - Superintendente Escalada") && processEntity.findValue("responderRRCEscalada").equals("Responder RRC Superintendente Escalada"))
			{
				GregorianCalendar dataResponderRRC = OrsegupsUtils.getSpecificWorkDay(dataResponderRRCTemp, 1L);
				dataResponderRRC.set(Calendar.HOUR_OF_DAY, 23);
				dataResponderRRC.set(Calendar.MINUTE, 59);
				dataResponderRRC.set(Calendar.SECOND, 59);
				processEntity.setValue("dataResponderRrcSuperintendenteEscalada", dataResponderRRC);
				processEntity.setValue("responderRRCEscalada", "Responder RRC Diretor Escalada");
				PersistEngine.persist(processEntity.getObject());
			}
			
			iff = "4";
			if (activity.getName().equals("Definir Prazo para Responder RRC - Diretor Escalada") && processEntity.findValue("responderRRCEscalada").equals("Responder RRC Diretor Escalada"))
			{
				GregorianCalendar dataResponderRRC = OrsegupsUtils.getSpecificWorkDay(dataResponderRRCTemp, 1L);
				dataResponderRRC.set(Calendar.HOUR_OF_DAY, 23);
				dataResponderRRC.set(Calendar.MINUTE, 59);
				dataResponderRRC.set(Calendar.SECOND, 59);
				
				processEntity.setValue("dataResponderRrcDiretorEscalada", dataResponderRRC);
				processEntity.setValue("responderRRCEscalada", "Responder RRC Presidente Escalada");
				PersistEngine.persist(processEntity.getObject());
			}
			
			iff = "5";
			//------------------------------------------------------------
			//quando for avançar as tarefas de responder e responder escalada
			if (activity.getName().equals("Definir Prazo Responder RRC Gerente") && processEntity.findValue("responderRRCEscalada").equals("Responder RRC Gerente Escalada"))
			{
				GregorianCalendar dataaProvarRRC = OrsegupsUtils.getSpecificWorkDay(dataResponderRRCTemp, 1L);
				dataaProvarRRC.set(Calendar.HOUR_OF_DAY, 23);
				dataaProvarRRC.set(Calendar.MINUTE, 59);
				dataaProvarRRC.set(Calendar.SECOND, 59);
				processEntity.setValue("dataAprovarRrcGerenteEscalada", dataaProvarRRC);
				PersistEngine.persist(processEntity.getObject());
			}
			
			iff = "6";
			if (activity.getName().equals("Definir Prazo para Aprovar Resposta RRC - Gerente") && processEntity.findValue("responderRRCEscalada").equals("Responder RRC Superintendente Escalada"))//aprovarRRCEscalada
			{
				GregorianCalendar dataAprovarRRC = OrsegupsUtils.getSpecificWorkDay(dataResponderRRCTemp, 1L);
				dataAprovarRRC.set(Calendar.HOUR_OF_DAY, 23);
				dataAprovarRRC.set(Calendar.MINUTE, 59);
				dataAprovarRRC.set(Calendar.SECOND, 59);
				processEntity.setValue("dataAprovarRrcGerenteEscalada", dataAprovarRRC);
				
				PersistEngine.persist(processEntity.getObject());
			}
			
			iff = "7";
			if (activity.getName().equals("Definir Prazo Responder RRC - Tarefa Escalada superintendente") && processEntity.findValue("responderRRCEscalada").equals("Responder RRC Diretor Escalada"))//aprovarRRCEscalada
			{
				GregorianCalendar dataAprovarRRC = OrsegupsUtils.getSpecificWorkDay(dataResponderRRCTemp, 1L);
				dataAprovarRRC.set(Calendar.HOUR_OF_DAY, 23);
				dataAprovarRRC.set(Calendar.MINUTE, 59);
				dataAprovarRRC.set(Calendar.SECOND, 59);
				processEntity.setValue("dataAprovarRrcGerenteEscalada", dataAprovarRRC);
				
				PersistEngine.persist(processEntity.getObject());
			}
			
			iff = "8";
			if (activity.getName().equals("Definir Responder RRC - Tarefa Escalada - Diretor") && processEntity.findValue("responderRRCEscalada").equals("Responder RRC Presidente Escalada"))
			{
				GregorianCalendar dataaProvarRRC = OrsegupsUtils.getSpecificWorkDay(dataResponderRRCTemp, 1L);
				dataaProvarRRC.set(Calendar.HOUR_OF_DAY, 23);
				dataaProvarRRC.set(Calendar.MINUTE, 59);
				dataaProvarRRC.set(Calendar.SECOND, 59);
				processEntity.setValue("dataAprovarRrcGerenteEscalada", dataaProvarRRC);
				PersistEngine.persist(processEntity.getObject());
			}
			
			iff = "9";
			if (activity.getName().equals("Definir Prazo para Responder RRC - Presidente"))
			{
				GregorianCalendar dataaProvarRRC = OrsegupsUtils.getSpecificWorkDay(dataResponderRRCTemp, 1L);
				dataaProvarRRC.set(Calendar.HOUR_OF_DAY, 23);
				dataaProvarRRC.set(Calendar.MINUTE, 59);
				dataaProvarRRC.set(Calendar.SECOND, 59);
				
				processEntity.setValue("dataAprovarRrcGerenteEscalada", dataaProvarRRC);
				PersistEngine.persist(processEntity.getObject());
			}
			
			///------------------------------------------------------------
			
			iff = "10";
			//Aprovar e Aprovar Escalada
			if (activity.getName().equals("Definir Prazo para Aprovar Resposta RRC - Superintendente Escalada") && processEntity.findValue("aprovarRRCEscalada").equals("Aprovar Resposta RRC"))
			{
				GregorianCalendar dataResponderRRC = OrsegupsUtils.getSpecificWorkDay(dataResponderRRCTemp, 1L);
				dataResponderRRC.set(Calendar.HOUR_OF_DAY, 23);
				dataResponderRRC.set(Calendar.MINUTE, 59);
				dataResponderRRC.set(Calendar.SECOND, 59);
				processEntity.setValue("dataAprovarRrcSuperintendenteEscalada", dataResponderRRC);
				processEntity.setValue("aprovarRRCEscalada", ".Aprovar Resposta RRC - Tarefa Escalada");
				
				processEntity.setValue("recusaRRC", Boolean.FALSE);
				processEntity.setValue("esconderRecusaRRC", Boolean.TRUE);
				PersistEngine.persist(processEntity.getObject());
			}
			
			iff = "11";
			if (activity.getName().equals("Definir Prazo para Aprovar Resposta RRC - Diretor Escalada") && processEntity.findValue("aprovarRRCEscalada").equals(".Aprovar Resposta RRC - Tarefa Escalada"))
			{
				GregorianCalendar dataResponderRRC = OrsegupsUtils.getSpecificWorkDay(dataResponderRRCTemp, 1L);
				dataResponderRRC.set(Calendar.HOUR_OF_DAY, 23);
				dataResponderRRC.set(Calendar.MINUTE, 59);
				dataResponderRRC.set(Calendar.SECOND, 59);
				processEntity.setValue("dataAprovarRrcDiretorEscalada", dataResponderRRC);
				processEntity.setValue("aprovarRRCEscalada", "..Aprovar Resposta RRC - Tarefa Escalada - 2");
				PersistEngine.persist(processEntity.getObject());
			}
			
			/*if (activity.getName().equals("Definir Prazo para Aprovar RRC - Presidente"))
			{
				GregorianCalendar dataaProvarRRC = OrsegupsUtils.getSpecificWorkDay(dataResponderRRCTemp, 1L);
				dataaProvarRRC.set(Calendar.HOUR_OF_DAY, 23);
				dataaProvarRRC.set(Calendar.MINUTE, 59);
				dataaProvarRRC.set(Calendar.SECOND, 59);
				processEntity.setValue("dataAprovarRrcGerenteEscalada", dataaProvarRRC);
				PersistEngine.persist(processEntity.getObject());
			}*///
			
			iff = "12";
			if (activity.getName().equals("Definir Prazo para Tratar RRC"))
			{
				GregorianCalendar dataaProvarRRC = OrsegupsUtils.getSpecificWorkDay(dataResponderRRCTemp, 2L);
				dataaProvarRRC.set(Calendar.HOUR_OF_DAY, 23);
				dataaProvarRRC.set(Calendar.MINUTE, 59);
				dataaProvarRRC.set(Calendar.SECOND, 59);
				processEntity.setValue("dataTratarRRC", dataaProvarRRC);
				PersistEngine.persist(processEntity.getObject());
			}
			
			iff = "13";
			if (activity.getName().equals("Definir Prazo para Tratar RRC Escalada") && processEntity.findValue("tratarRRCEscalada") == null)
			{
				GregorianCalendar dataaProvarRRC = OrsegupsUtils.getSpecificWorkDay(dataResponderRRCTemp, 2L);
				dataaProvarRRC.set(Calendar.HOUR_OF_DAY, 23);
				dataaProvarRRC.set(Calendar.MINUTE, 59);
				dataaProvarRRC.set(Calendar.SECOND, 59);
				processEntity.setValue("dataTratarRRCEscaladaQualidade", dataaProvarRRC);
				processEntity.findField("tratarRRCEscalada").setValue("Tratar RRC - Escalada Qualidade");
				
				//Caso escale do tratar RRC ai não deixa mais dar prazo para o Responder RRC
				if (processEntity.findValue("responderRRCEscalada") == null){
					processEntity.setValue("responderRRCEscalada", "Responder RRC Gerente Escalada");
				}
				
				//se escala da um novo prazo para a Atividade "Responder RRC"
				if(processEntity.findValue("esconderRecusaRRC") == Boolean.TRUE){
					processEntity.setValue("dataResponderRrc", dataaProvarRRC);
				}
				
				//para poder dar um novo prazo no responder RRC caso não tenha escalado do tratar RRC
				//processEntity.setValue("responderRRCEscalada", null);
			}
			
			iff = "13.1";
			if (activity.getName().equals("Definir Prazo para Tratar RRC Escalada Supervisor Call Center 2") && processEntity.findValue("tratarRRCEscalada").equals("Tratar RRC - Escalada Qualidade"))
			{
				GregorianCalendar dataaProvarRRC = OrsegupsUtils.getSpecificWorkDay(dataResponderRRCTemp, 2L);
				dataaProvarRRC.set(Calendar.HOUR_OF_DAY, 23);
				dataaProvarRRC.set(Calendar.MINUTE, 59);
				dataaProvarRRC.set(Calendar.SECOND, 59);
				processEntity.setValue("dataTratarRRCEscaladaSupervisorCallCenter", dataaProvarRRC);
				processEntity.findField("tratarRRCEscalada").setValue("Tratar RRC - Escalada Call Center 2");
				
				//Caso escale do tratar RRC ai não deixa mais dar prazo para o Responder RRC
				if (processEntity.findValue("responderRRCEscalada") == null){
					processEntity.setValue("responderRRCEscalada", "Responder RRC Gerente Escalada");
				}
				
				//se escala da um novo prazo para a Atividade "Responder RRC"
				if(processEntity.findValue("esconderRecusaRRC") == Boolean.TRUE){
					processEntity.setValue("dataResponderRrc", dataaProvarRRC);
				}
			}
			
			
			//quando é avançado da atividade "Tratar RRC - Escalada Qualidade"
			iff = "13.2";
			if (activity.getName().equals("Definir Prazo para Tratar RRC Quanlidade") && processEntity.findValue("tratarRRCEscalada").equals("Tratar RRC - Escalada Qualidade"))
			{
				GregorianCalendar dataaProvarRRC = OrsegupsUtils.getSpecificWorkDay(dataResponderRRCTemp, 2L);
				dataaProvarRRC.set(Calendar.HOUR_OF_DAY, 23);
				dataaProvarRRC.set(Calendar.MINUTE, 59);
				dataaProvarRRC.set(Calendar.SECOND, 59);
				processEntity.setValue("dataResponderRrc", dataaProvarRRC);
			}
			
			//quando é avançado da atividade "Tratar RRC - Escalada Qualidade"
			iff = "13.3";
			if (activity.getName().equals("Definir Prazo para Tratar RRC CallCenter 2") && processEntity.findValue("tratarRRCEscalada").equals("Tratar RRC - Escalada Call Center 2"))
			{
				GregorianCalendar dataaProvarRRC = OrsegupsUtils.getSpecificWorkDay(dataResponderRRCTemp, 2L);
				dataaProvarRRC.set(Calendar.HOUR_OF_DAY, 23);
				dataaProvarRRC.set(Calendar.MINUTE, 59);
				dataaProvarRRC.set(Calendar.SECOND, 59);
				processEntity.setValue("dataResponderRrc", dataaProvarRRC);
				processEntity.findField("tratarRRCEscalada").setValue("Tratar RRC - Escalada call center 2");
			}
			
			iff = "14";
			if (activity.getName().equals("Definir Prazo para Tratar RRC Qualidade Recusada"))
			{
				if (processEntity.findValue("tratarRRCEscalada") == null) {
					GregorianCalendar data1 = (GregorianCalendar) processEntity.findValue("dataResponderRrc");
					GregorianCalendar data2 = (GregorianCalendar) data1.clone();
					processEntity.findField("dataTratarRRC").setValue(data2);
					processEntity.setValue("esconderRecusaRRC", Boolean.TRUE);
					processEntity.setValue("recusaRRC", Boolean.FALSE);
					processEntity.setValue("esconderCamposResponderRRC", Boolean.FALSE);
				} else {
					if (processEntity.findValue("tratarRRCEscalada").equals("Tratar RRC - Escalada Qualidade")) {
						processEntity.findField("dataTratarRRCEscaladaQualidade").setValue(processEntity.findValue("dataResponderRrc"));
						processEntity.setValue("esconderRecusaRRC", Boolean.TRUE);
						processEntity.setValue("recusaRRC", Boolean.FALSE);
						processEntity.setValue("esconderCamposResponderRRC", Boolean.FALSE);
					} else {
						if (processEntity.findValue("tratarRRCEscalada").equals("Tratar RRC - Escalada call center 2")) {
							processEntity.findField("dataTratarRRCEscaladaSupervisorCallCenter").setValue(processEntity.findValue("dataResponderRrc"));
							processEntity.setValue("esconderRecusaRRC", Boolean.TRUE);
							processEntity.setValue("recusaRRC", Boolean.FALSE);
							processEntity.setValue("esconderCamposResponderRRC", Boolean.FALSE);
						}
					}
				}
				
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro ao definir prazo para atividade: "+activity.getCode()+", no if >>> "+iff);
			throw new WorkflowException("Erro ao definir prazo para atividade");
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
