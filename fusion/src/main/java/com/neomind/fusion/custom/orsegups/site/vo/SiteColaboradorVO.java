package com.neomind.fusion.custom.orsegups.site.vo;

import java.util.ArrayList;
import java.util.Collection;


public class SiteColaboradorVO {
	
	private String numCad;
	private String nomFun;
	private String tipCol;
	
	
	private SiteEmpresaColaboradorVO empresa;
	private Collection<SiteContraChequeColaboradorVO> contraCheque = new ArrayList();
	
	public String getNumCad() {
		return numCad;
	}
	public void setNumCad(String numCad) {
		this.numCad = numCad;
	}
	public String getNomFun() {
		return nomFun;
	}
	public void setNomFun(String nomFun) {
		this.nomFun = nomFun;
	}
	
	public Collection<SiteContraChequeColaboradorVO> getContraCheque() {
		return contraCheque;
	}
	public void setContraCheque(
			Collection<SiteContraChequeColaboradorVO> contraCheque) {
		this.contraCheque = contraCheque;
	}
	
	public SiteEmpresaColaboradorVO getEmpresa() {
		return empresa;
	}
	public void setEmpresa(SiteEmpresaColaboradorVO empresa) {
		this.empresa = empresa;
	}
	public String getTipCol() {
		return tipCol;
	}
	public void setTipCol(String tipCol) {
		this.tipCol = tipCol;
	}
	
	
	
}
