package com.neomind.fusion.custom.orsegups.adapter.r001v2;

import com.neomind.fusion.workflow.event.ActivityAvailableEventListener;
import com.neomind.fusion.workflow.event.ActivityEvent;
import com.neomind.fusion.workflow.event.ActivityStartEventListener;
import com.neomind.fusion.workflow.event.TaskAssignerEvent;
import com.neomind.fusion.workflow.event.TaskAssignerEventListener;
import com.neomind.fusion.workflow.event.WorkflowEventListener;
import com.neomind.fusion.workflow.event.WorkflowFinishEvent;
import com.neomind.fusion.workflow.event.WorkflowFinishEventListener;
import com.neomind.fusion.workflow.event.WorkflowStartEvent;
import com.neomind.fusion.workflow.event.WorkflowStartEventListener;
import com.neomind.fusion.workflow.exception.ActivityException;
import com.neomind.fusion.workflow.exception.TaskException;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class EscalaTarefa implements ActivityStartEventListener,ActivityAvailableEventListener,
                                     TaskAssignerEventListener,
                                     WorkflowEventListener,WorkflowStartEventListener,WorkflowFinishEventListener
{

	
	@Override
	public void onStart(ActivityEvent event) throws ActivityException {
		System.out.println("Entrou: ActivityStartEventListener");   //quando chegou em processar Ferias
	}

	@Override
	public void onAssign(TaskAssignerEvent event) throws TaskException {
		System.out.println("Entrou: TaskAssignerEventListener");		
	}

	@Override
	public void onAvailable(ActivityEvent event) throws ActivityException {
		System.out.println("Entrou: ActivityAvailableEventListener");	//quando chegou em Processar Ferias
	}

	@Override
	public void onFinish(WorkflowFinishEvent event) throws WorkflowException {
		System.out.println("Entrou: WorkflowFinishEventListener");		
	}

	@Override
	public void onStart(WorkflowStartEvent event) throws WorkflowException {
		System.out.println("Entrou: WorkflowStartEventListener");		
	}
}
