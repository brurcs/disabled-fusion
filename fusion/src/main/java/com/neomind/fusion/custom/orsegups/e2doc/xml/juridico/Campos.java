package com.neomind.fusion.custom.orsegups.e2doc.xml.juridico;

public class Campos {
    
    private String i73;
    private String i74;
    private String i75;
    private String i76;
    
    private String i105;
    private String i106;
    private String i107; 
    
    public String getI73() {
        return i73;
    }
    public void setI73(String i73) {
        this.i73 = i73;
    }
    public String getI74() {
        return i74;
    }
    public void setI74(String i74) {
        this.i74 = i74;
    }
    public String getI75() {
        return i75;
    }
    public void setI75(String i75) {
        this.i75 = i75;
    }
    public String getI76() {
        return i76;
    }
    public void setI76(String i76) {
        this.i76 = i76;
    }
	public String getI105() {
		return i105;
	}
	public void setI105(String i105) {
		this.i105 = i105;
	}
	public String getI106() {
		return i106;
	}
	public void setI106(String i106) {
		this.i106 = i106;
	}
	public String getI107() {
		return i107;
	}
	public void setI107(String i107) {
		this.i107 = i107;
	}    
}
