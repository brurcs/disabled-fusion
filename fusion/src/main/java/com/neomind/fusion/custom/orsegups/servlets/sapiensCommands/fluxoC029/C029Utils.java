package com.neomind.fusion.custom.orsegups.servlets.sapiensCommands.fluxoC029;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.ProcessState;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class C029Utils {

    public static boolean verificaTarefaAberta(NeoObject contrato) {

	NeoObject efProcesso = null;
	int processState = 0;

	processState = ProcessState.running.ordinal();

	QLGroupFilter groupFilter = new QLGroupFilter("AND");
	groupFilter.addFilter(new QLEqualsFilter("contratoSapiens", contrato));
	groupFilter.addFilter(new QLEqualsFilter("wfprocess.processState", processState));

	efProcesso = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FCNCancelamentoHumanaInadimplencia"), groupFilter);

	if (NeoUtils.safeIsNull(efProcesso)) {
	    return false;
	} else {
	    return true;
	}
    }

    public static NeoObject retornaObjetoContrato(Long codigoEmpresa, Long codigoFilial, Long numeroContrato) {

	NeoObject contrato = null;

	QLGroupFilter groupFilter = new QLGroupFilter("AND");
	groupFilter.addFilter(new QLEqualsFilter("usu_codemp", codigoEmpresa));
	groupFilter.addFilter(new QLEqualsFilter("usu_codfil", codigoFilial));
	groupFilter.addFilter(new QLEqualsFilter("usu_numctr", numeroContrato));
	contrato = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("VFUSCTRSAP"), groupFilter);

	return contrato;
    }

    public static NeoUser retornaUsuarioSolicitante() {

	NeoPaper papelSolicitante = null;
	NeoUser usuarioSolicitante = null;

	papelSolicitante = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "FCNSolicitanteProcessoDeCancelamento"));
	usuarioSolicitante = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelSolicitante);

	return usuarioSolicitante;
    }

    public static NeoObject insereDadosFormularioPrincipal(NeoObject contrato) {

	NeoObject eFormPrincipal = null;
	EntityWrapper ewEformPrincipal = null;

	eFormPrincipal = AdapterUtils.createNewEntityInstance("FCNCancelamentoHumanaInadimplencia");
	ewEformPrincipal = new EntityWrapper(eFormPrincipal);
	ewEformPrincipal.findField("contratoSapiens").setValue(contrato);

	PersistEngine.persist(eFormPrincipal);

	return eFormPrincipal;
    }

}
