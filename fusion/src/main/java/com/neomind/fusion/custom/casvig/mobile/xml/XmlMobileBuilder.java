package com.neomind.fusion.custom.casvig.mobile.xml;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.ejb.EntityManagerImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.casvig.mobile.entity.CasvigMobileEntityPool;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.datawarehouse.NeoDataSource;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.util.NeoUtils;

/**
 * 
 * @author Daniel Henrique Joppi
 * 
 */
public abstract class XmlMobileBuilder
{
	private static final Log log = LogFactory.getLog(XmlMobileBuilder.class);
	
	/**
	 * Constroi um XML com a Estrutura capturada do E-Form Pesquisa
	 * 
	 * @author Daniel Henrique Joppi 03/03/2008
	 * @return XML de Pesquisas.
	 */
	public static Document buildXMLReasearch()
	{
		// Criando o documento XML
		Document doc = XmlMobileConverter.newDocument();
		// Constru��o do XML de Pesquisa
		Element xmlRaiz = doc.createElement("Pesquisas");

		Element xmlPesquisa;
		Element xmlAssunto;
		Element xmlPergunta;

		InstantiableEntityInfo pesquisa = (InstantiableEntityInfo) EntityRegister.getInstance().getCache().getByType("Pesquisa");
		List<NeoObject> newObjPesq = (List<NeoObject>) PersistEngine.getObjects(pesquisa.getEntityClass());

		int iPes = 0;
		for (NeoObject pesquisaQuery : newObjPesq)
		{
			xmlPesquisa = doc.createElement("Pesquisa");
			EntityWrapper wrapperPesq = new EntityWrapper(pesquisaQuery);

			Object cod_pesq = wrapperPesq.getObject().getNeoId();
			Object desc_pesq = wrapperPesq.getValue("pesquisatxt");

			xmlPesquisa.setAttribute("cod_pesq", cod_pesq.toString());
			xmlPesquisa.setAttribute("desc_pesq", desc_pesq.toString());

			int iAss = 0;
			List<NeoObject> newObjAssun = ((List<NeoObject>) wrapperPesq.findValue("neoidAssunto"));
			for (NeoObject assuntoQuery : newObjAssun)
			{
				xmlAssunto = doc.createElement("Assunto");
				EntityWrapper wrapperAssun = new EntityWrapper(assuntoQuery);

				Object cod_assun = wrapperAssun.getObject().getNeoId();
				Object desc_assun = wrapperAssun.getValue("descassun");

				xmlAssunto.setAttribute("cod_assun", cod_assun.toString());
				xmlAssunto.setAttribute("desc_assun", desc_assun.toString());

				int iPer = 0;
				List<NeoObject> newObjPerg = ((List<NeoObject>) wrapperAssun.findValue("neoidPergunta"));
				for (NeoObject perguntaQuery : newObjPerg)
				{
					xmlPergunta = doc.createElement("Pergunta");
					EntityWrapper wrapperPerg = new EntityWrapper(perguntaQuery);

					Object cod_perg = wrapperPerg.getObject().getNeoId();
					Object desc_perg = wrapperPerg.getValue("perguntatxt");
					Object tip_resposta = wrapperPerg.getValue("tipresposta");

					xmlPergunta.setAttribute("cod_perg", cod_perg.toString());
					xmlPergunta.setAttribute("desc_perg", desc_perg.toString());
					xmlPergunta.setAttribute("tip_resposta", tip_resposta.toString());

					xmlAssunto.appendChild(xmlPergunta);
					iPer++;
				}
				xmlAssunto.setAttribute("num_perg", "" + iPer);
				xmlPesquisa.appendChild(xmlAssunto);
				iAss++;
			}
			xmlPesquisa.setAttribute("num_assun", "" + iAss);
			xmlRaiz.appendChild(xmlPesquisa);
			iPes++;
		}
		xmlRaiz.setAttribute("num_pes", "" + iPes);

		doc.appendChild(xmlRaiz);

		return XmlMobileConverter.setEnconding(doc);
	}

	/**
	 * Constroi um XML com a estrutura capturada do E-Form Exrteno Cliente.
	 * 
	 * @author Daniel Henrique Joppi 03/03/2008
	 * @return XML de Clientes.
	 */
	public static Document buildXMLClient()
	{
		// Criando o documento XML
		Document doc = XmlMobileConverter.newDocument();

		// Constru��o do XML de Pesquisa
		Element xmlRaiz = doc.createElement("Clientes");
		Element xmlEmpresa = doc.createElement("Empresa");
		Element xmlCliente = doc.createElement("Cliente");
		Element xmlLotacao = doc.createElement("Lotacao");
		Element xmlPostoTrabalho = doc.createElement("PostoTrabalho");

		try
		{
			  NeoDataSource data = (NeoDataSource) PersistEngine.getObject(NeoDataSource.class,
					    new QLEqualsFilter("name", "Sapiens"));
			  
			  Connection con = OrsegupsUtils.openConnection(data);

			String SQLCliente = " SELECT * FROM VIEW_LOC_INSP_MOBILE";
				//	+ " ORDER BY EMPRESA, CLIENTE, LOTACAO ";
			//String SQLCliente = " SELECT * FROM USU_INSP ";
				//ORDER BY EMPRESA, CLIENTE, LOTACAO ";
			 // Prepara o statement
			  Statement stm = null;
	
			
			   stm = (Statement) con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
			     ResultSet.CONCUR_READ_ONLY);
			   ResultSet rs = stm.executeQuery(SQLCliente);
		
			
			
			/*EntityManagerImpl emi = (EntityManagerImpl) PersistEngine.getEntityManager("SAPIENS");
			
			// Ap�s estabelecermos a conex�o com o banco de dados
			// Utilizamos o m�todo createStatement de con para criar o Statement
			Statement stm = emi.getSession().connection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);*/

			// Vamos executar o seguinte comando SQL:
			

			// Definido o Statement, executamos a query no banco de dados
		

			String empresa = new String("");
			String cliente = new String("");
			String lotacao = new String("");
			String codPosto = new String("");
			String posto = new String("");
			// O m�todo next () informa se houve resultados e posiciona o cursor
			// do banco na pr�xima linha dispon�vel para recupera��o
			// Como esperamos v�rias linhas utilizamos um la�o para recuperar os
			// dados
			int iEmp = 0;
			int iCli = 0;
			int iLot = 0;
			int iPost = 0;
			if (rs.next())
			{
				
				
				empresa = new String(rs.getString("EMPRESA"));
				cliente = new String(rs.getString("CLIENTE"));
				lotacao = new String(rs.getString("LOTACAO"));
				posto = new String(rs.getString("nomePosto"));
				codPosto = new String(rs.getString("codPosto"));

				xmlEmpresa = doc.createElement("Empresa");
				xmlEmpresa.setAttribute("nome_empresa", empresa);

				xmlCliente = doc.createElement("Cliente");
				xmlCliente.setAttribute("nome_cliente", cliente);
				xmlLotacao = doc.createElement("Lotacao");
				xmlLotacao.setAttribute("nome_lotacao", lotacao);
				xmlPostoTrabalho = doc.createElement("PostoTrabalho");
				xmlPostoTrabalho.setAttribute("nome_posto", posto);
				xmlPostoTrabalho.setAttribute("codigo_posto", codPosto);
				xmlLotacao.appendChild(xmlPostoTrabalho);
			}
			while (rs.next())
			{
				// Os m�todos getXXX recuperam os dados de acordo com o tipo SQL
				// do dado:
				
				String newEmpresa = new String(rs.getString("EMPRESA"));
				String newCliente = new String(rs.getString("CLIENTE"));
				String newLotacao = new String(rs.getString("LOTACAO"));
				String newPosto = new String(rs.getString("codPosto"));

				if (!empresa.equals(newEmpresa))
				{
					xmlLotacao.setAttribute("num_posto", "" + (iPost + 1));
					xmlCliente.appendChild(xmlLotacao);
					xmlCliente.setAttribute("num_lotacao", "" + (iLot + 1));
					xmlEmpresa.appendChild(xmlCliente);
					xmlEmpresa.setAttribute("num_cliente", "" + (iCli + 1));
					xmlRaiz.appendChild(xmlEmpresa);

					iEmp++;
					iCli = 0;
					iLot = 0;
					iPost = 0;

					empresa = new String(newEmpresa);
					cliente = new String(newCliente);
					lotacao = new String(newLotacao);
					posto = new String(newPosto);
					codPosto = new String(rs.getString("CODIGO"));

					xmlEmpresa = doc.createElement("Empresa");
					xmlEmpresa.setAttribute("nome_empresa", empresa);

					xmlCliente = doc.createElement("Cliente");
					xmlCliente.setAttribute("nome_cliente", cliente);
					xmlLotacao = doc.createElement("Lotacao");
					xmlLotacao.setAttribute("nome_lotacao", lotacao);
					xmlPostoTrabalho = doc.createElement("PostoTrabalho");
					xmlPostoTrabalho.setAttribute("nome_posto", posto);
					xmlPostoTrabalho.setAttribute("codigo_posto", codPosto);
					xmlLotacao.appendChild(xmlPostoTrabalho);

				}
				else if (!cliente.equals(newCliente))
				{
					xmlLotacao.setAttribute("num_posto", "" + (iPost + 1));
					xmlCliente.appendChild(xmlLotacao);
					xmlCliente.setAttribute("num_lotacao", "" + (iLot + 1));
					xmlEmpresa.appendChild(xmlCliente);

					iCli++;
					iLot = 0;
					iPost = 0;

					empresa = new String(newEmpresa);
					cliente = new String(newCliente);
					lotacao = new String(newLotacao);
					posto = new String(newPosto);
					codPosto = new String(rs.getString("codPosto"));

					xmlCliente = doc.createElement("Cliente");
					xmlCliente.setAttribute("nome_cliente", cliente);
					xmlLotacao = doc.createElement("Lotacao");
					xmlLotacao.setAttribute("nome_lotacao", lotacao);
					xmlPostoTrabalho = doc.createElement("PostoTrabalho");
					xmlPostoTrabalho.setAttribute("nome_posto", posto);
					xmlPostoTrabalho.setAttribute("codigo_posto", codPosto);
					xmlLotacao.appendChild(xmlPostoTrabalho);
				}
				else if (!lotacao.equals(newLotacao))
				{
					xmlLotacao.setAttribute("num_posto", "" + (iPost + 1));
					xmlCliente.appendChild(xmlLotacao);

					iLot++;
					iPost = 0;

					lotacao = new String(newLotacao);
					posto = new String(newPosto);
					codPosto = new String(rs.getString("codPosto"));

					xmlLotacao = doc.createElement("Lotacao");
					xmlLotacao.setAttribute("nome_lotacao", lotacao);
					xmlPostoTrabalho = doc.createElement("PostoTrabalho");
					xmlPostoTrabalho.setAttribute("nome_posto", posto);
					xmlPostoTrabalho.setAttribute("codigo_posto", codPosto);
					xmlLotacao.appendChild(xmlPostoTrabalho);
				}
				else
				{
					iPost++;

					posto = new String(newPosto);
					codPosto = new String(rs.getString("codPosto"));

					xmlPostoTrabalho = doc.createElement("PostoTrabalho");
					xmlPostoTrabalho.setAttribute("nome_posto", posto);
					xmlPostoTrabalho.setAttribute("codigo_posto", codPosto);
					xmlLotacao.appendChild(xmlPostoTrabalho);
				}
			}
			xmlLotacao.setAttribute("num_posto", "" + (iPost + 1));
			xmlCliente.appendChild(xmlLotacao);
			xmlCliente.setAttribute("num_lotacao", "" + (iLot + 1));
			xmlEmpresa.appendChild(xmlCliente);
			xmlEmpresa.setAttribute("num_cliente", "" + (iCli + 1));
			xmlRaiz.appendChild(xmlEmpresa);
			xmlRaiz.setAttribute("num_emp", "" + (iEmp + 1));

		}
		catch (SQLException se)
		{
			// se houve algum erro, uma exce��o � gerada para informar o erro
			se.printStackTrace(); // vejamos que erro foi gerado e quem o gerou
		}
		

		doc.appendChild(xmlRaiz);
		System.out.println(doc);

		return XmlMobileConverter.setEnconding(doc);
	}
	
	/**
	 * Constroi um XML com a Resultados obstivos atraves de uma lista de E-Forms
	 * PesquisaResultado.
	 * 
	 * @author Daniel Henrique Joppi 08/04/2008
	 * @param listPool
	 * lista de reinspe��es que devem ser enviadas para o mobile.
	 * @return XML de Reinspe��es e Efic�cia.
	 */
	public static Document buildXMLReinspection(List<CasvigMobileEntityPool> listPool)
	{
		// Constru��o do XML de PesquisaResultado
		Document doc = XmlMobileConverter.newDocument();
		Element xmlRaiz = doc.createElement("Resultados");
		Element xmlResultado = doc.createElement("Resultado");
		Element xmlResposta;

		int iPes = 0;
		for (CasvigMobileEntityPool pool : listPool)
		{
			NeoObject entity = pool.getActivity().getProcess().getEntity();
			try
			{
				EntityWrapper wrapper = new EntityWrapper(entity);

				NeoObject objInspetoria = (NeoObject) wrapper.getValue("relatorioInspecao");
				EntityWrapper wrapperResult = new EntityWrapper(objInspetoria);

				Object cod_postoTrabalho = wrapperResult.getValue("codposto");
				Object nome_empresa = wrapperResult.getValue("nomeempresa");
				Object nome_cliente = wrapperResult.getValue("nomecliente");
				Object nome_lotacao = wrapperResult.getValue("nomelotacao");
				Object nome_postoTrabalho = wrapperResult.getValue("nomeposto");
				Object gestor_cliente = wrapperResult.getValue("gestorcliente");
				Object nome_colaborador = wrapperResult.getValue("colaboradorentrevistado");
				Object funcao_colaborador = wrapperResult.getValue("funccolaborador");
				Object nome_inspetor = wrapperResult.getValue("nomeinspetor");

				GregorianCalendar data = (GregorianCalendar) wrapperResult.getValue("datainspecao");
				int mes = data.get(Calendar.MONTH);
				int dia = data.get(Calendar.DAY_OF_MONTH);
				int ano = data.get(Calendar.YEAR);
				Object data_inpesao = ano + " " + mes + " " + dia;
				Object descrisao_pesquisa = wrapperResult.getValue("descpesquisa");
				Object Observacao = null;// wrapperResult.getValue("Observacao");

				xmlResultado = doc.createElement("Resultado");
				xmlResultado.setAttribute("tipo_inspecao", (NeoUtils.safeIsNull(pool.getTypeInspection())) ? "" : pool.getTypeInspection());
				xmlResultado.setAttribute("cod_task", "" + pool.getTask().getNeoId());
				xmlResultado.setAttribute("mobile_neoId", "00000");
				xmlResultado.setAttribute("cod_postoTrabalho", (NeoUtils.safeIsNull(cod_postoTrabalho)) ? "" : cod_postoTrabalho.toString());
				xmlResultado.setAttribute("nome_empresa", (NeoUtils.safeIsNull(nome_empresa)) ? "" : nome_empresa.toString());
				xmlResultado.setAttribute("nome_cliente", (NeoUtils.safeIsNull(nome_cliente)) ? "" : nome_cliente.toString());
				xmlResultado.setAttribute("nome_lotacao", (NeoUtils.safeIsNull(nome_lotacao)) ? "" : nome_lotacao.toString());
				xmlResultado.setAttribute("nome_postoTrabalho", (NeoUtils.safeIsNull(nome_postoTrabalho)) ? "" : nome_postoTrabalho.toString());
				xmlResultado.setAttribute("gestor_cliente", (NeoUtils.safeIsNull(gestor_cliente)) ? "" : gestor_cliente.toString());
				xmlResultado.setAttribute("nome_colaborador", (NeoUtils.safeIsNull(nome_colaborador)) ? "" : nome_colaborador.toString());
				xmlResultado.setAttribute("funcao_colaborador", (NeoUtils.safeIsNull(funcao_colaborador)) ? "" : funcao_colaborador.toString());
				xmlResultado.setAttribute("nome_inspetor", (NeoUtils.safeIsNull(nome_inspetor)) ? "" : nome_inspetor.toString());
				xmlResultado.setAttribute("data_inpesao", (NeoUtils.safeIsNull(data_inpesao)) ? "" : data_inpesao.toString());
				xmlResultado.setAttribute("descrisao_pesquisa", (NeoUtils.safeIsNull(descrisao_pesquisa)) ? "" : descrisao_pesquisa.toString());
				xmlResultado.setAttribute("observacao", (NeoUtils.safeIsNull(Observacao)) ? "" : Observacao.toString());

				xmlResposta = doc.createElement("Resposta");
				if (pool.getTypeInspection().equals("Reinspeção") || pool.getTypeInspection().equals("Reinspecao"))
				{
					xmlResposta.setAttribute("desc_perg", "Resposta à Inconsistência");
					xmlResposta.setAttribute("resp_perg", (NeoUtils.safeIsNull(wrapper.getValue("respostaInconsistencia"))) ? "" : wrapper.getValue(
							"respostaInconsistencia").toString());
				}
				else
				{
					xmlResposta.setAttribute("desc_perg", "Resposta à Não-conformidade");
					xmlResposta.setAttribute("resp_perg", (NeoUtils.safeIsNull(wrapper.getValue("respostaNaoConformidade"))) ? "" : wrapper.getValue(
							"respostaNaoConformidade").toString());
				}

				xmlResposta.setAttribute("logical", "null");
				xmlResposta.setAttribute("code_perg", "0");

				xmlResultado.appendChild(xmlResposta);

				int iRes = 1;
				List<NeoObject> listObjResp = ((List<NeoObject>) wrapperResult.findValue("neoidResposta"));
				for (NeoObject respQuery : listObjResp)
				{
					xmlResposta = doc.createElement("Resposta");
					EntityWrapper wrapperResp = new EntityWrapper(respQuery);

					Object desc_perg = wrapperResp.getValue("descperg");
					Object resp_perg = wrapperResp.getValue("respperg");
					Object code_perg = wrapperResp.getValue("codeperg");
					Object logical = wrapperResp.getValue("booleanValor");

					if (!desc_perg.equals("Resposta à Inconsistência") || !desc_perg.equals("Resposta a Inconsistencia"))
					{
						xmlResposta.setAttribute("desc_perg", (NeoUtils.safeIsNull(desc_perg)) ? "" : desc_perg.toString());
						xmlResposta.setAttribute("resp_perg", (NeoUtils.safeIsNull(resp_perg)) ? "" : resp_perg.toString());
						xmlResposta.setAttribute("code_perg", (NeoUtils.safeIsNull(code_perg)) ? "" : code_perg.toString());
						xmlResposta.setAttribute("logical", (NeoUtils.safeIsNull(logical)) ? "" : logical.toString());

						xmlResultado.appendChild(xmlResposta);
						iRes++;
					}
				}
				xmlResultado.setAttribute("numTotalResp", "" + iRes);
				xmlRaiz.appendChild(xmlResultado);

				iPes++;
			}
			catch (Exception npe)
			{
				log.error("IM_Inspecao - neoId = " + entity.getNeoId() + " - neoIdTask = " + pool.getTask().getNeoId());
			}
		}
		xmlRaiz.setAttribute("num_reslt", "" + iPes);
		doc.appendChild(xmlRaiz);

		if (iPes == 0)
			return null;

		return XmlMobileConverter.setEnconding(doc);
	}
}