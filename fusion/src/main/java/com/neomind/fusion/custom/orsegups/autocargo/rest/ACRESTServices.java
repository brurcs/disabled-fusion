package com.neomind.fusion.custom.orsegups.autocargo.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.neomind.fusion.custom.orsegups.autocargo.engine.ACEngineIntegracao;
import com.neomind.fusion.custom.orsegups.autocargo.imports.importFromFile.ACCarsImport;
import com.neomind.fusion.custom.orsegups.autocargo.imports.importFromFile.ACClientsImport;
import com.neomind.fusion.custom.orsegups.autocargo.imports.importFromFile.ACCreateCarAccounts;
import com.neomind.fusion.custom.orsegups.autocargo.imports.importFromFile.ACCreateClientAccounts;
import com.neomind.fusion.custom.orsegups.autocargo.imports.importFromService.ACImportCarsFromService;

@Path(value = "autocargo")
public class ACRESTServices {
    
    private final ACEngineIntegracao engine = new ACEngineIntegracao();
    private final ACCreateClientAccounts accountEngine = new ACCreateClientAccounts();
    private final ACCreateCarAccounts accountCarEngine = new ACCreateCarAccounts();
    private final ACImportCarsFromService serverEngine = new ACImportCarsFromService();
    
    @GET
    @Path("runIntegration")
    @Produces(MediaType.TEXT_PLAIN)
    public String getViaturasDisponiveis() {

	return engine.runIntegration();

    }
    
    
    @GET
    @Path("runClientImport")
    @Produces(MediaType.TEXT_PLAIN)
    public String runClientImport() {

	return ACClientsImport.execute();

    }
    
    @GET
    @Path("runCarImport")
    @Produces(MediaType.TEXT_PLAIN)
    public String runCarImport() {

	return ACCarsImport.execute();

    }
    
    @GET
    @Path("createClientAccounts")
    @Produces(MediaType.TEXT_PLAIN)
    public String runClientCreate() {

	accountEngine.createClients();
	
	return "Fim da execução!";

    }
    
    @GET
    @Path("createCarAccounts")
    @Produces(MediaType.TEXT_PLAIN)
    public String runCarCreate() {

	accountCarEngine.createCars();
	
	return "Fim da execução!";

    }
    
    @GET
    @Path("createCarAccountsFromService")
    @Produces(MediaType.TEXT_PLAIN)
    public String carFromService() {

	serverEngine.runIntegration();
	
	return "Fim da execução!";

    }
    
    
}


