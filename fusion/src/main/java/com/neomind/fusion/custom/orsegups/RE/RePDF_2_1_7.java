package com.neomind.fusion.custom.orsegups.RE;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.parser.PdfTextExtractor;
import com.neomind.util.NeoUtils;

public class RePDF_2_1_7 {
		boolean separaResumoFechamento;
		boolean separaResumoFechamentoFGT;
		boolean separaResumoInfoPrevidencia;
		String path = "\\\\fsoofs01\\f$\\Site";
	
	
	public static String leadingZeros(String s, int length) {
	    	String retorno = null;
	    	try {
	    		if (s.length() >= length) 
	    			return s;
	    		else 
	    			retorno = String.format("%0" + (length-s.length()) + "d%s", 0, s);
	    	}catch(Exception ex) {
	    			ex.printStackTrace();
	    	}
			return retorno;
    }

    //Cria PDF por TOMADOR
    public static boolean criarArquivoPDF(String codEmp, String dataCpt, String inscricaoTomadorAnterior, PdfReader reader, String outFile, int paginaStart, int paginaEnd, boolean encontrado, Long empfil[]) {
		boolean retorno = true;
    	try {
    		String naoEncontrado = "";
    		if (!encontrado){
    			naoEncontrado = "\\naoEncontrado\\";
    		}
    		outFile = outFile+naoEncontrado+codEmp+"\\"+leadingZeros(NeoUtils.safeOutputString(empfil[0]),2)+"\\"+empfil[1]+"\\"+NeoUtils.safeLong(inscricaoTomadorAnterior)+"\\RE\\"+dataCpt.replace("/", "");
    		File file = new File(outFile);
    		if (!file.exists())
    			file.mkdirs();
			outFile = outFile+"\\"+NeoUtils.safeLong(inscricaoTomadorAnterior) + ".pdf";
			System.out.println(outFile);
			Document document = new Document(reader.getPageSizeWithRotation(1));
			PdfCopy writer = new PdfCopy(document, new FileOutputStream(outFile));
			document.open();
			for (int i = paginaStart; i <= paginaEnd; i++) {
				PdfImportedPage page = writer.getImportedPage(reader, i);
				writer.addPage(page);
			}
			document.close();
			writer.close();
		} catch (FileNotFoundException e) {
			retorno = false;
			e.printStackTrace();
		} catch (DocumentException e) {
			retorno = false;
			e.printStackTrace();
		} catch (IOException e) {
			retorno = false;
			e.printStackTrace();
		} catch (Exception ex){
			retorno = false;
			ex.printStackTrace();
		}
		return retorno;
	}
	    
	  //Ler PDF identificando se o proximo PDF � igual ao TOMADOR anterior caso n�o seje ele manda criar o pdf e continua num ciclo ate ler todas as paginas do PDF 
	    public String leituraPdf(InputStream input, String dataCpt, String codEmp, String usuario, boolean separaResumoFechamento, boolean separaResumoFechamentoFGT, boolean separaResumoInfoPrevidencia) throws Exception{
	    	ArrayList<String> cgccpfs = new ArrayList<String>();
	    	String arquivosGerados = " Arquivos Gerados: \r\n";
	    	try {
		    	PdfReader reader = new PdfReader(input, null);
				PdfTextExtractor text = new PdfTextExtractor(reader);
				int numPages = reader.getNumberOfPages();
				String inscricaoTomadorAnterior = null; 
				int paginaStart = 1;
		        int paginaEnd = 0;
		        String inscricaoTomador = null;
		        String cnpjPrestador = null;
		        
		        boolean isResumoFechamento = false;
				int paginaStartRFE = 0;
				int paginaEndRFE = 0;
				
				boolean isResumoFechamentoFGT = false;
				int paginaStartRFEF = 0;
				int paginaEndRFEF = 0;
				
				boolean isResumoInfoPrevidencia = false;
				int paginaStartRIPT = 0;
				int paginaEndRIPT = 0;
		        
				for (int paginaAtual = 1; paginaAtual <= numPages; paginaAtual++) {
	
					StringBuilder str = new StringBuilder(text.getTextFromPage(paginaAtual));
					
					int posicao515 = str.indexOf("515\n");
             		int posicaoInscricaoStart = posicao515 + 4;
             		int posicaoInscricaoEnd = str.indexOf("\n", posicaoInscricaoStart);
             		String inscricaoTomadorRaw = str.substring(posicaoInscricaoStart, posicaoInscricaoEnd);
             		
             		int posicaoInscricaoStartPrest = posicao515 - 19;
             		int posicaoInscricaoEndPrest = str.indexOf("\n", posicaoInscricaoStartPrest);
             		String inscricaoPrestadorRaw = str.substring(posicaoInscricaoStartPrest, posicaoInscricaoEndPrest);
					
					String[] texto = text.getTextFromPage(paginaAtual).split("\n");
					
					//ler linha a linha
					boolean isInicioTomador = false;
					
					
					
					
					//Recupera se  troca do novo tomador
					int idxModalidade = retornaIndiceCampoArray(texto,"MODALIDADE :",1);
					if (idxModalidade > -1 && texto[idxModalidade] != null && 
							(
									texto[idxModalidade].contains("MODALIDADE : \"BRANCO\"-RECOLHIMENTO AO FGTS E DECLAR")
									||
									texto[idxModalidade].contains("MODALIDADE : 9-CONFIRMA")
							)
							){
						isInicioTomador = true;
					}else if (str.toString().contains("RESUMO DO FECHAMENTO - EMPRESA") && !str.toString().contains("FGTS")){
						System.out.println("1");
						isResumoFechamento = true;
						paginaStartRFE = paginaAtual;
						paginaEndRFE = paginaAtual;
					}else if(str.toString().contains("RESUMO DO FECHAMENTO - EMPRESA")  && str.toString().contains("FGTS") ){
						System.out.println("2");
						isResumoFechamentoFGT = true;
						paginaStartRFEF = paginaAtual;
						paginaEndRFEF = paginaAtual;
													  												   
					}else if(str.toString().contains("RESUMO DAS INFORMA") && NeoUtils.safeOutputString(str.toString().substring((str.indexOf("NCIA SOCIAL CONSTANTES NO ARQUIVO SEFIP")+40),(str.indexOf("NCIA SOCIAL CONSTANTES NO ARQUIVO SEFIP")+48))).trim().equals("EMPRESA") ){
						System.out.println("3");
						isResumoInfoPrevidencia = true;
						paginaStartRIPT = paginaAtual;
						paginaEndRIPT = paginaAtual;
					}else if(str.toString().contains("RESUMO DAS INFORMA") && NeoUtils.safeOutputString(str.toString().substring((str.indexOf("NCIA SOCIAL CONSTAN")+40),(str.indexOf("NCIA SOCIAL CONSTAN")+48))).trim().equals("EMPRESA") ){
						System.out.println("3");
						isResumoInfoPrevidencia = true;
						paginaStartRIPT = paginaAtual;
						paginaEndRIPT = paginaAtual;
					}
					
					//RESUMO DO FECHAMENTO - EMPRESA
					//RESUMO DO FECHAMENTO - EMPRESA FGTS
					
					if (isInicioTomador) {
						//Recupera o CGC do TOMADOR
						cnpjPrestador = inscricaoPrestadorRaw.replaceAll("\\D+",""); //texto[10].replaceAll("\\D+","");
						inscricaoTomador = inscricaoTomadorRaw.replaceAll("\\D+",""); // texto[12].replaceAll("\\D+","");
						cgccpfs.add(inscricaoTomador);
						
						if(inscricaoTomadorAnterior == null)
							inscricaoTomadorAnterior = inscricaoTomador;
					}
					
					Boolean valid = true; // aqui irao colocar o metodo de busca do TOMADOR na base da senior se existir retorna true se n�o retorna false
					
					if (!inscricaoTomadorAnterior.equals(inscricaoTomador) || (paginaAtual == numPages)){
						paginaEnd = paginaAtual -1;
						Long emfil[] = TomadorDaoImpl.getEmpFil(cnpjPrestador);
         				criarArquivoPDF(codEmp,dataCpt,inscricaoTomadorAnterior,reader,path+(usuario.equals("danilo.silva")? "\\teste":"")+"\\",paginaStart,paginaEnd, valid, emfil );
         				arquivosGerados = arquivosGerados+ "Empresa: "+emfil[0]+", Filial: "+emfil[1]+", Pagina: " + paginaAtual + ", Inscricao Tomador: " + inscricaoTomador+"\r\n";
         				inscricaoTomadorAnterior = inscricaoTomador;
         				paginaStart = paginaAtual;
					}
					
					if (isResumoFechamento && isResumoFechamentoFGT && isResumoInfoPrevidencia){
						for (String cgccpf : cgccpfs){
							paginaEnd = paginaAtual ;
							paginaStart = paginaAtual ;
							Long emfil[] = TomadorDaoImpl.getEmpFil(cnpjPrestador);

							criarArquivoPDFNaPasta(codEmp,dataCpt,cgccpf,reader,path+(usuario.equals("danilo.silva")? "\\teste":"")+"\\",paginaStartRFE,paginaEndRFE, valid, emfil,"RFE" );
							criarArquivoPDFNaPasta(codEmp,dataCpt,cgccpf,reader,path+(usuario.equals("danilo.silva")? "\\teste":"")+"\\",paginaStartRFEF,paginaEndRFEF, valid, emfil, "RFEF" );
							criarArquivoPDFNaPasta(codEmp,dataCpt,cgccpf,reader,path+(usuario.equals("danilo.silva")? "\\teste":"")+"\\",paginaStartRIPT,paginaEndRIPT, valid, emfil, "RIPT" );
	         				
							arquivosGerados = arquivosGerados+ "Empresa: "+emfil[0]+", Filial: "+emfil[1]+", Pagina: " + paginaAtual + ", Inscricao Tomador: " + inscricaoTomador+"\r\n";
						}
					}
					
				}
				
				if (!(isResumoFechamento && isResumoFechamentoFGT && isResumoInfoPrevidencia)){
					if (cgccpfs.size() >0){
						throw new Exception("Erro ao processar os Resumos da RE.");
					}
				}
	    	
	    	 }catch (Exception e) {
	             e.printStackTrace();
	             if (true) throw new Exception("teste" + e.getMessage());
	         }
	    	 return arquivosGerados;
	     }
	    
	    
	    public static int retornaIndiceCampoArray(String array[], String campo, int ocorrencia ){
	    	int retorno = -1;
	    	
	    	int count = 0;
	    	for (int i = 0; i < array.length; i++){
	    		if (array[i].contains(campo)){
	    			count++;
	    			retorno = i;
	    		}
	    	}
	    	if (count != ocorrencia){
	    		retorno =-1;
	    	}
	    	
	    	return retorno;
	    	
	    }
	    
	    
	    public static boolean criarArquivoPDFNaPasta(String codEmp, String dataCpt, String inscricaoAnterior, PdfReader reader, String outFile, int paginaStart, int paginaEnd, boolean encontrado, Long empfil[], String pasta) {
			boolean retorno = true;
	    	try {
	    		String naoEncontrado = "";
	    		if (!encontrado){
	    			naoEncontrado = "\\naoEncontrado\\";
	    		}
	    		outFile = outFile+naoEncontrado+"\\"+codEmp+"\\"+leadingZeros(NeoUtils.safeOutputString(empfil[0]),2)+"\\"+empfil[1]+"\\"+NeoUtils.safeLong(inscricaoAnterior)+"\\"+pasta+"\\"+dataCpt.replace("/", "");
	    		File file = new File(outFile);
	    		if (!file.exists())
	    			file.mkdirs();
				outFile = outFile+"\\"+NeoUtils.safeLong(inscricaoAnterior) + ".pdf";
				System.out.println(outFile);
				Document document = new Document(reader.getPageSizeWithRotation(1));
				PdfCopy writer = new PdfCopy(document, new FileOutputStream(outFile));
				document.open();
				for (int i = paginaStart; i <= paginaEnd; i++) {
					PdfImportedPage page = writer.getImportedPage(reader, i);
					writer.addPage(page);
				}
				document.close();
				writer.close();
			} catch (FileNotFoundException e) {
				retorno = false;
				e.printStackTrace();
			} catch (DocumentException e) {
				retorno = false;
				e.printStackTrace();
			} catch (IOException e) {
				retorno = false;
				e.printStackTrace();
			} catch (Exception ex){
				retorno = false;
				ex.printStackTrace();
			}
			return retorno;
		}
	    
	   
	    
	    
	public static void main(String args[]) throws IOException {
		/*
		String inFile = "C:\\tmp\\RE\\RE 042015 Orsegups Limpeza.PDF";
		File f = new File(inFile);
		 String dataCpt = "03/2015";
         String codEmp = "Orsegups";
		RePDF_2_1_7 t = new RePDF_2_1_7();
		t.leituraPdf( new FileInputStream(f), dataCpt, codEmp,"danilo.silva");*/
		System.out.println(leadingZeros("7", 2));
	}
}