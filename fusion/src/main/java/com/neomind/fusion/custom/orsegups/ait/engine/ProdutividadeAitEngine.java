package com.neomind.fusion.custom.orsegups.ait.engine;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.ait.controller.ProdutividadeAitController;
import com.neomind.fusion.custom.orsegups.ait.vo.AtendimentoValorVO;
import com.neomind.fusion.custom.orsegups.ait.vo.AtendimentoVtrVO;
import com.neomind.fusion.custom.orsegups.ait.vo.AtendimentosAitVO;
import com.neomind.fusion.custom.orsegups.maps.vo.ViaturaVO;
import com.neomind.fusion.custom.orsegups.utils.HtmlToText;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsConnectionUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoGroup;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class ProdutividadeAitEngine {
    
    private Map<Integer, String> serverInfo = OrsegupsConnectionUtils.getServerInfo("SIGMA");
    
    /**
     * Metodo criado para retornar as 6 ultimas competencias
     * 
     * @param calendar
     * @return List<String>
     */
    public List<String> getCompetencias(GregorianCalendar calendar) {
	List<String> cptList = null;
	if (NeoUtils.safeIsNotNull(calendar)) {
	    cptList = new ArrayList<String>();

	    GregorianCalendar dataInicio = new GregorianCalendar();
	    if (dataInicio.get(Calendar.DAY_OF_MONTH) <= 15) {
		dataInicio.add(Calendar.MONTH, -1);
	    }
	    dataInicio.set(Calendar.HOUR_OF_DAY, 0);
	    dataInicio.set(Calendar.MINUTE, 0);
	    dataInicio.set(Calendar.SECOND, 0);
	    dataInicio.set(Calendar.MILLISECOND, 0);

	    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/yyyy");
	    for (int i = 0; i < 6; i++) {
		String diaStr = dateFormat.format(dataInicio.getTime());
		cptList.add(diaStr);
		dataInicio.add(Calendar.MONTH, -1);
	    }

	}
	return cptList;
    }

    /**
     * Método responsável por retornar todas as viaturas que possuem vinculo com
     * colaborador
     * 
     * @return List<AtendimentoVtrVO>
     */
    public List<AtendimentoVtrVO> getListaViatura() {
	List<AtendimentoVtrVO> vtrList = null;
	try {
	    List<ViaturaVO> aitObjs = this.getViaturaXColaborador();

	    if (aitObjs != null && !aitObjs.isEmpty()) {
		StringBuilder txtCol = new StringBuilder();
		vtrList = new ArrayList<AtendimentoVtrVO>();
		int cont = 0;
		for (ViaturaVO v: aitObjs) {
		    
		    String cdViatura = v.getCodigoViatura();

		    txtCol.append("'");
		    txtCol.append(cdViatura);
		    txtCol.append("'");
		    if (cont < aitObjs.size() - 1) {
			txtCol.append(",");
		    }
		    cont++;

		}

		if (txtCol != null) {
		    vtrList.addAll(getViatura(txtCol.toString()));

		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return vtrList;
    }

    /**
     * Método responsável por retornar informações do colaborador empresa, tipo,
     * matrícula e nome completo
     * 
     * @param cpf
     * @return String
     */
    public String getColaborador(Long cpf) {

	Connection connVetorh = null;
	PreparedStatement stColaborador = null;
	ResultSet rsColaborador = null;
	String retorno = "";
	try {
	    connVetorh = PersistEngine.getConnection("VETORH");
	    StringBuffer queryColaborador = new StringBuffer();
	    queryColaborador.append(" SELECT fun.NumEmp, fun.TipCol, fun.NumCad,  fun.NomFun, fun.SitAfa,  fun.emicar ");
	    queryColaborador.append(" FROM R034FUN fun ");
	    queryColaborador.append(" WHERE fun.DatAdm <= GETDATE() AND (fun.SitAfa <> 7 OR (fun.SitAfa = 7 AND fun.DatAfa > GETDATE())) ");
	    queryColaborador.append(" AND fun.TipCol = 1 AND fun.NumCpf = ? ");

	    stColaborador = connVetorh.prepareStatement(queryColaborador.toString());
	    stColaborador.setLong(1, cpf);

	    rsColaborador = stColaborador.executeQuery();

	    if (rsColaborador.next()) {
		retorno = rsColaborador.getLong("NumEmp") + ";" + rsColaborador.getLong("TipCol") + ";" + rsColaborador.getLong("NumCad") + ";" + rsColaborador.getString("NomFun");
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(connVetorh, stColaborador, rsColaborador);
	}

	return retorno;
    }

    /**
     * Método responsável por retornar a viatura do sigma
     * 
     * @param cdViatura
     * @return List<AtendimentoVtrVO>
     */
    private List<AtendimentoVtrVO> getViatura(String cdViatura) {
	Connection conn = null;
	PreparedStatement preparedStatementHSelect = null;
	ResultSet rsH = null;
	List<AtendimentoVtrVO> vtrList = null;

	try {
	    conn = PersistEngine.getConnection("SIGMA90");
	    StringBuilder sqlSigma = new StringBuilder();
	    sqlSigma.append("  SELECT CD_VIATURA,NM_VIATURA FROM VIATURA WHERE CD_VIATURA IN (" + cdViatura + ") ");

	    preparedStatementHSelect = conn.prepareStatement(sqlSigma.toString());
	    rsH = preparedStatementHSelect.executeQuery();
	    vtrList = new ArrayList<AtendimentoVtrVO>();

	    AtendimentoVtrVO atendimentoVtrVO = null;

	    while (rsH.next()) {
		atendimentoVtrVO = new AtendimentoVtrVO();
		atendimentoVtrVO.setCdViatura(rsH.getString("CD_VIATURA"));
		atendimentoVtrVO.setNmViatura(rsH.getString("NM_VIATURA"));

		vtrList.add(atendimentoVtrVO);
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, preparedStatementHSelect, rsH);
	}

	return vtrList;
    }

    /**
     * Método responsável por retornar a foto do AIT
     * 
     * @param cdViatura
     * @return Long
     */
    public Long getFotoAit(String cdViatura) {
	Long idAit = 0L;
	try {
	    InstantiableEntityInfo infoTec = (InstantiableEntityInfo) EntityRegister.getEntityInfo("AITEmailAutomaticoAIT");
	    @SuppressWarnings("unchecked")
	    NeoObject colaborador = PersistEngine.getObject(infoTec.getEntityClass(), new QLEqualsFilter("matricula", cdViatura));
	    if (colaborador != null) {

		EntityWrapper wpneo = new EntityWrapper(colaborador);

		idAit = ((NeoFile) wpneo.getValue("fotoTec")).getNeoId();

	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return idAit;

    }

    /**
     * Método responsável por retornar informações do colaborador empresa, tipo,
     * matrícula e nome completo
     * 
     * @param cpf
     * @return String
     */
    public String getBuscaColaborador(Long cpf) {

	Connection connVetorh = null;
	PreparedStatement stColaborador = null;
	ResultSet rsColaborador = null;
	String retorno = "";

	try {

	    StringBuffer queryColaborador = new StringBuffer();

	    queryColaborador.append("   SELECT fun.NumEmp, fun.TipCol, fun.NumCad,fun.DatAdm, fun.NomFun, fun.SitAfa, fun.DatAfa, fun.emicar,fun.NumCpf ");
	    queryColaborador.append("   FROM R034FUN fun  ");
	    queryColaborador.append("   WHERE fun.DatAdm <= GETDATE() AND (fun.SitAfa <> 7 OR (fun.SitAfa = 7 AND fun.DatAfa > GETDATE()))  ");
	    queryColaborador.append("   AND fun.TipCol = 1 AND fun.NumCpf = ? ");

	    connVetorh = PersistEngine.getConnection("VETORH");
	    stColaborador = connVetorh.prepareStatement(queryColaborador.toString());
	    stColaborador.setLong(1, cpf);

	    rsColaborador = stColaborador.executeQuery();

	    if (rsColaborador.next()) {
		Long empCol = rsColaborador.getLong("NumEmp");
		Long tipCol = rsColaborador.getLong("TipCol");
		Long numCad = rsColaborador.getLong("NumCad");
		String nomFun = rsColaborador.getString("NomFun");

		retorno = empCol + " - " + tipCol + " - " + numCad + " - " + nomFun;
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(connVetorh, stColaborador, rsColaborador);
	}

	return retorno;
    }

    /**
     * Método responsável por retorar nome da viatura e cpf
     * 
     * @param cdViatura
     * @return ViaturaVO
     */
    public ViaturaVO getDadosAit(String cdViatura) {
	Long idAit = 0L;
	ViaturaVO viaturaVO = null;
	try {

	    InstantiableEntityInfo infoTec = (InstantiableEntityInfo) EntityRegister.getEntityInfo("AITEmailAutomaticoAIT");
	    @SuppressWarnings("unchecked")
	    NeoObject colaborador = PersistEngine.getObject(infoTec.getEntityClass(), new QLEqualsFilter("matricula", cdViatura));
	    if (colaborador != null) {

		EntityWrapper wpneo = new EntityWrapper(colaborador);

		idAit = ((NeoFile) wpneo.getValue("fotoTec")).getNeoId();
		List<ViaturaVO> aitObjs = this.getViaturaXColaboradorByCDViatura(Integer.parseInt(cdViatura)); 

		if (aitObjs != null && !aitObjs.isEmpty()) {

		    for (ViaturaVO v : aitObjs) {
			viaturaVO = new ViaturaVO();
			
			String nmViatura = v.getNomeMotorista();
			Long cpf = v.getCpf();
			viaturaVO.setCodigoViatura(cdViatura);
			viaturaVO.setNomeMotorista(nmViatura);
			viaturaVO.setNomeImagem(idAit.toString());
			viaturaVO.setTitulo(getBuscaColaborador(cpf));
			break;

		    }

		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return viaturaVO;

    }

    /**
     * 
     * @param cdViatura
     * @param competencia
     * @return List<AtendimentosAitVO>
     */
    public List<AtendimentosAitVO> getAtendimentosAit(String cdViatura, String competencia) {
	Connection conn = null;
	PreparedStatement preparedStatementHSelect = null;
	ResultSet rsH = null;
	List<AtendimentosAitVO> atendimentoList = null;

	String conexao = "SIGMA90";
	
	try {
	    String[] compArray = competencia.split("/");
	    
	    if(Integer.parseInt(compArray[1]) >= 2017){
		if(Integer.parseInt(compArray[1]) == 2017 && Integer.parseInt(compArray[0]) < 9 ){
		    conexao = "SIGMA90_HIST";
		}
	    }else{
		conexao = "SIGMA90_HIST";
	    }
	    
	} catch (Exception e) {
	    e.printStackTrace();
	}
		
	try {
	    if (competencia != null && !competencia.isEmpty() && competencia.contains("/")) {

		conn = PersistEngine.getConnection(conexao);

		String array[] = competencia.split("/");

		GregorianCalendar datIni = new GregorianCalendar();
		datIni.set(Calendar.DAY_OF_MONTH, 16);
		datIni.set(Calendar.MONTH, Integer.parseInt(array[0]) - 2);
		datIni.set(Calendar.HOUR_OF_DAY, 0);
		datIni.set(Calendar.MINUTE, 0);
		datIni.set(Calendar.SECOND, 0);
		datIni.set(Calendar.MILLISECOND, 0);
		datIni.set(Calendar.YEAR, Integer.parseInt(array[1]));

		System.out.println("Data inicial " + NeoDateUtils.safeDateFormat(datIni, "dd/MM/yyyy HH:mm:ss.SSS"));

		GregorianCalendar datFim = new GregorianCalendar();
		datFim.set(Calendar.DAY_OF_MONTH, 16);
		datFim.set(Calendar.HOUR_OF_DAY, 0);
		datFim.set(Calendar.MINUTE, 0);
		datFim.set(Calendar.SECOND, 0);
		datFim.set(Calendar.MILLISECOND, 0);
		datFim.set(Calendar.MONTH, Integer.parseInt(array[0]) - 1);
		datFim.set(Calendar.YEAR, Integer.parseInt(array[1]));

		System.out.println("Data fim " + NeoDateUtils.safeDateFormat(datFim, "dd/MM/yyyy HH:mm:ss.SSS"));

		StringBuilder sqlSigma = new StringBuilder();
		sqlSigma.append("  SELECT CD_HISTORICO, SUBSTRING(vtr.NM_VIATURA, 1, 3) AS REG, vtr.NM_VIATURA AS AA,   ");
		sqlSigma.append("   c.FANTASIA + ' - ' + c.ID_CENTRAL AS CLIENTE, h.CD_EVENTO, DT_FECHAMENTO, col.NM_COLABORADOR AS OP,  ");
		sqlSigma.append("   DATEDIFF(MINUTE, h.DT_ESPERA_DESLOCAMENTO, h.DT_VIATURA_NO_LOCAL) AS DESLOCAMENTO, ");
		sqlSigma.append("   DATEDIFF(MINUTE, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) AS ATENDIMENTO, vtr.CD_VIATURA,   ");
		sqlSigma.append("   H.TX_OBSERVACAO_FECHAMENTO, H.TX_OBSERVACAO_GERENTE    ");
		sqlSigma.append("   FROM VIEW_HISTORICO h WITH (NOLOCK)   ");
		sqlSigma.append("   INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE =  h.CD_CLIENTE   ");
		sqlSigma.append("   INNER JOIN VIATURA vtr WITH (NOLOCK) ON vtr.CD_VIATURA =  h.CD_VIATURA   ");
		sqlSigma.append("   INNER JOIN USUARIO u WITH (NOLOCK) ON u.CD_USUARIO = h.CD_USUARIO_FECHAMENTO   ");
		sqlSigma.append("   INNER JOIN COLABORADOR col WITH (NOLOCK) ON col.CD_COLABORADOR = u.CD_COLABORADOR   ");
		sqlSigma.append(" 	WHERE h.DT_ESPERA_DESLOCAMENTO IS NOT NULL   ");
		sqlSigma.append("   AND h.DT_VIATURA_NO_LOCAL IS NOT NULL   ");
		sqlSigma.append("   AND h.DT_FECHAMENTO >= ?   ");
		sqlSigma.append("   AND h.DT_FECHAMENTO < ?   ");
		sqlSigma.append("   AND vtr.NM_VIATURA NOT LIKE '%PLACAS%'   ");
		sqlSigma.append("   AND DATEDIFF (MINUTE, h.DT_RECEBIDO, h.DT_VIATURA_NO_LOCAL) >= 0   ");
		sqlSigma.append("   AND vtr.CD_VIATURA = ?  ");
		sqlSigma.append("   AND h.CD_EVENTO NOT IN ('XAAE', 'XXX8', 'X8', 'XXX7', 'X406', 'XXX2', 'XXX5')   ");
		sqlSigma.append("   ORDER BY SUBSTRING(vtr.NM_VIATURA, 1, 3),vtr.NM_VIATURA   ");

		preparedStatementHSelect = conn.prepareStatement(sqlSigma.toString());

		java.sql.Date dataSqlIni = new java.sql.Date(datIni.getTimeInMillis());
		java.sql.Date dataSqlFim = new java.sql.Date(datFim.getTimeInMillis());

		preparedStatementHSelect.setDate(1, dataSqlIni);
		preparedStatementHSelect.setDate(2, dataSqlFim);
		preparedStatementHSelect.setString(3, cdViatura);

		rsH = preparedStatementHSelect.executeQuery();

		atendimentoList = new ArrayList<AtendimentosAitVO>();
		HtmlToText htmlToText = new HtmlToText();
		AtendimentosAitVO aasupfis = null;
		boolean flag = false;

		while (rsH.next()) {

		    if (!flag) {
			
			List<ViaturaVO> aitObjs = this.getViaturaXColaboradorByCDViatura(rsH.getInt("CD_VIATURA"));
			if (aitObjs != null && !aitObjs.isEmpty()) {
			    flag = true;
			} else {
			    break;
			}
		    }

		    aasupfis = new AtendimentosAitVO();
		    aasupfis.setCdHistorico(rsH.getString("CD_HISTORICO"));
		    aasupfis.setRegional(rsH.getString("REG"));
		    aasupfis.setAit(rsH.getString("AA"));
		    aasupfis.setCliente(rsH.getString("CLIENTE"));
		    aasupfis.setCdEvento(rsH.getString("CD_EVENTO"));
		    GregorianCalendar calendar = new GregorianCalendar();
		    calendar.setTime(rsH.getTimestamp("DT_FECHAMENTO"));
		    aasupfis.setDtFechamento(NeoDateUtils.safeDateFormat(calendar.getTime(), "dd/MM/yyyy HH:mm:ss"));
		    aasupfis.setOp(rsH.getString("OP"));
		    aasupfis.setDeslocamento(rsH.getString("DESLOCAMENTO"));
		    aasupfis.setAtendimento(rsH.getString("ATENDIMENTO"));
		    aasupfis.setCdViatura(rsH.getString("CD_VIATURA"));
		    String txObsevacaoFechamento = rsH.getString("TX_OBSERVACAO_FECHAMENTO");
		    String calendarDDMM = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM");
		    String calendarDM = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "d/M");
		    if (txObsevacaoFechamento != null && !txObsevacaoFechamento.isEmpty()) {
			String arrayAux[] = txObsevacaoFechamento.split("###");
			String textoFormatado = "";
			for (String string : arrayAux) {
			    textoFormatado += string + "<br/>";
			}

			if (!textoFormatado.isEmpty()) {
			    textoFormatado = textoFormatado.replaceAll("#", "");
			    textoFormatado = textoFormatado.replaceAll("\\r\\n", "");
			    textoFormatado = textoFormatado.replaceAll(calendarDDMM, "<br/><br/>" + calendarDDMM);
			    textoFormatado = textoFormatado.replaceAll(calendarDM, "<br/><br/>" + calendarDDMM);

			    textoFormatado = textoFormatado.replaceAll("Ligando para cliente!", "Ligando para cliente ");
			    String aux = "";
			    if (textoFormatado.contains("Ramal :")) {
				int number = textoFormatado.indexOf("Ramal :");
				aux = textoFormatado.substring((number + 7), (number + 12));
				textoFormatado = textoFormatado.replaceAll(aux, "");
			    }
			    textoFormatado = textoFormatado.replaceAll("Ramal:", "");
			    textoFormatado = textoFormatado.replaceAll("Ramal :", "");
			    textoFormatado = textoFormatado.replaceAll("FecharEvento", "");
			    textoFormatado = textoFormatado.replaceAll("EmEspera", "");
			    textoFormatado = textoFormatado.replaceAll("TratarSigma", "");

			    textoFormatado = textoFormatado.replaceAll("AtendimentoRealizado", "");
			    textoFormatado = textoFormatado.replaceAll("SemContato", "");
			    textoFormatado = textoFormatado.replaceAll("AtualizarCadastro", "");
			    textoFormatado = textoFormatado.replaceAll("NaoEnviaComunicado", "");
			    textoFormatado = textoFormatado.replaceAll(": - ", " - ");

			    textoFormatado = textoFormatado.replaceAll("Motivo:", "<br/><br/>");

			    txObsevacaoFechamento = textoFormatado;

			}

		    } else {
			txObsevacaoFechamento = "NADA CONSTA";
		    }

		    aasupfis.setTxFechamento(htmlToText.parse(txObsevacaoFechamento));
		    if (rsH.getString("TX_OBSERVACAO_GERENTE") != null && !rsH.getString("TX_OBSERVACAO_GERENTE").isEmpty()) {
			aasupfis.setTxGerente(htmlToText.parse(rsH.getString("TX_OBSERVACAO_GERENTE")));
		    } else {
			aasupfis.setTxGerente("NADA CONSTA");
		    }
		    atendimentoList.add(aasupfis);

		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, preparedStatementHSelect, rsH);

	}

	return atendimentoList;
    }

    /**
     * Cadastra relato na produtividade
     * 
     * @param cdViatura
     * @param competencia
     * @param relato
     * @return String
     */
    public String cadastraRelatoProdutividade(String cdViatura, String competencia, String relato) {
	String retorno = "Sucesso ao cadastrar relato";
	try {
	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("cdViatura", Long.parseLong(cdViatura)));
	    filter.addFilter(new QLEqualsFilter("competencia", competencia));
	    List<NeoObject> aitObjs = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CPAProdutividadeAIT"), filter);

	    if (aitObjs != null && !aitObjs.isEmpty()) {
		NeoObject neoObject = (NeoObject) aitObjs.get(0);

		if (neoObject != null) {
		    if ((relato != null && relato.isEmpty()) || relato == null || relato.equalsIgnoreCase("null")) {
			relato = "";
		    }
		    EntityWrapper entityWrapper = new EntityWrapper(neoObject);
		    entityWrapper.findField("relato").setValue(relato);
		    if (relato != null && !relato.isEmpty()) {

			entityWrapper.findField("situacao").setValue(retornaCPASituacao(2L));
		    } else {
			entityWrapper.findField("situacao").setValue(retornaCPASituacao(1L));
		    }
		    PersistEngine.persist(neoObject);
		}

	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    retorno = "Erro ao cadastrar relato";
	}
	return retorno;
    }

    /**
     * Método para buscar relato sobre a produtividade
     * 
     * @param cdViatura
     * @param competencia
     * @return AtendimentosAitVO
     */
    public AtendimentosAitVO buscaProdutividade(String cdViatura, String competencia) {
	AtendimentosAitVO atendimentosAitVO = null;
	try {
	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("cdViatura", Long.parseLong(cdViatura)));
	    filter.addFilter(new QLEqualsFilter("competencia", competencia));
	    List<NeoObject> aitObjs = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CPAProdutividadeAIT"), filter);

	    if (aitObjs != null && !aitObjs.isEmpty()) {
		NeoObject neoObject = (NeoObject) aitObjs.get(0);

		if (neoObject != null) {
		    atendimentosAitVO = new AtendimentosAitVO();
		    EntityWrapper entityWrapper = new EntityWrapper(neoObject);
		    atendimentosAitVO.setCdViatura(cdViatura);
		    atendimentosAitVO.setAit((String) entityWrapper.findField("nmViatura").getValue());
		    atendimentosAitVO.setRelato((String) entityWrapper.findField("relato").getValue());
		    Long wfProcess = 0L;
		    if (entityWrapper.findValue("wfprocess.neoId") != null) {
			wfProcess = (Long) entityWrapper.findField("wfprocess.neoId").getValue();
		    }
		    if (wfProcess != null && !wfProcess.equals(0L)) {
			atendimentosAitVO.setWfProcess(wfProcess);
		    }

		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return atendimentosAitVO;
    }

    /**
     * Método para verificar se existe o cpf cadastrado no ERP
     * 
     * @param cdViatura
     * @param competencia
     * @return AtendimentosAitVO
     */
    public Boolean validaVinculoViaturaColaborador(Long cdViatura) {

	Connection conn = null;
	StringBuilder queryColaborador = new StringBuilder();
	PreparedStatement pstm = null;
	ResultSet rs = null;
	Boolean flag = Boolean.FALSE;
	if (NeoUtils.safeIsNotNull(cdViatura)) {
	    try {
		Long cpf = retornaCpf(cdViatura);

		if (cpf != null) {

		    conn = PersistEngine.getConnection("VETORH");

		    queryColaborador.append("  SELECT fun.NumEmp,fun.CodFil, fun.TipCol, fun.NumCad, fun.DatAdm, fun.NomFun, fun.SitAfa, fun.DatAfa, fun.emicar,fun.NumCpf");
		    queryColaborador.append("  FROM R034FUN fun WITH(NOLOCK)  ");
		    queryColaborador.append("  WHERE fun.DatAdm <= getdate() AND (fun.SitAfa <> 7 OR (fun.SitAfa = 7 AND fun.DatAfa > getdate())) ");
		    queryColaborador.append("  AND fun.numcpf = ? ");

		    pstm = conn.prepareStatement(queryColaborador.toString());

		    pstm.setLong(1, cpf);

		    rs = pstm.executeQuery();

		    if (rs.next()) {
			flag = Boolean.TRUE;
		    }
		}
	    } catch (Exception e) {
		e.printStackTrace();
	    } finally {

		OrsegupsUtils.closeConnection(conn, pstm, rs);

	    }
	}
	return flag;
    }

    /**
     * Método para retornar código do calculo por empresa
     * 
     * @param empresa
     * @return Long
     */
    public Long retornaCodigoCalculo(Long empresa, String competencia) {

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	Long codCal = 0L;
	try {

	    String comp[] = competencia.split("/");
	    conn = PersistEngine.getConnection("VETORH");

	    StringBuilder sql = new StringBuilder();
	    sql.append("   SELECT CodCal FROM R044Cal Cal WITH(NOLOCK) WHERE TipCal = 11 AND NumEmp = ? AND MONTH(PERREF) = ? AND YEAR(PERREF) = ?");

	    pstm = conn.prepareStatement(sql.toString());
	    pstm.setLong(1, empresa);
	    pstm.setInt(2, Integer.parseInt(comp[0]));
	    pstm.setInt(3, Integer.parseInt(comp[1]));
	    rs = pstm.executeQuery();
	    if (rs.next()) {
		codCal = rs.getLong("CodCal");

	    }

	} catch (Exception e) {

	    e.printStackTrace();
	} finally {

	    OrsegupsUtils.closeConnection(conn, pstm, rs);

	}
	return codCal;
    }

    /**
     * Método para inserir o lançamento no ERP
     * 
     * @param codCal
     * @param valor
     * @return boolean
     */
    public boolean insereLancamento(Long codCal, Double valor, Long empresa, Long numCad) {

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;
	boolean flag = false;
	try {

	    conn = PersistEngine.getConnection("VETORH");

	    StringBuilder sql = new StringBuilder();
	    sql.append("INSERT INTO R044MOV (NumEmp, TipCol, NumCad, CodCal, TabEve, CodEve, CodRat, SeqEve, OriMov, RefEve, ValEve) VALUES (?, 1, ?, ?, 941, 742, 0, 2, 'I', 0, ?) ");

	    pstm = conn.prepareStatement(sql.toString());
	    pstm.setLong(1, empresa);
	    pstm.setLong(2, numCad);
	    pstm.setLong(3, codCal);
	    pstm.setDouble(4, valor);
	    int rows = pstm.executeUpdate();

	    if (rows > 0) {
		flag = true;
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {

	    OrsegupsUtils.closeConnection(conn, pstm, rs);

	}
	return flag;
    }

    /**
     * Método para retornar a empresa do colaborador
     * 
     * @param cdViatura
     * @return Long
     */
    public HashMap<String, Long> retornaDadosViaturaColaborador(Long cdViatura) {

	Connection conn = null;
	StringBuilder queryColaborador = new StringBuilder();
	PreparedStatement pstm = null;
	ResultSet rs = null;

	HashMap<String, Long> dadosColaborador = null;

	if (NeoUtils.safeIsNotNull(cdViatura)) {
	    try {
		Long cpf = retornaCpf(cdViatura);

		if (cpf != null) {

		    conn = PersistEngine.getConnection("VETORH");

		    queryColaborador.append("  SELECT fun.NumEmp, fun.numcad ");
		    queryColaborador.append("  FROM R034FUN fun WITH(NOLOCK)  ");
		    queryColaborador.append("  WHERE fun.DatAdm <= getdate() AND (fun.SitAfa <> 7 OR (fun.SitAfa = 7 AND fun.DatAfa > getdate())) ");
		    queryColaborador.append("  AND fun.numcpf = ? ");

		    pstm = conn.prepareStatement(queryColaborador.toString());

		    pstm.setLong(1, cpf);

		    rs = pstm.executeQuery();

		    if (rs.next()) {
			dadosColaborador = new HashMap<>();
			dadosColaborador.put("numEmp", rs.getLong("NumEmp"));
			dadosColaborador.put("numCad", rs.getLong("numcad"));

		    }
		}
	    } catch (Exception e) {
		e.printStackTrace();
	    } finally {

		OrsegupsUtils.closeConnection(conn, pstm, rs);

	    }
	}
	return dadosColaborador;
    }

    /**
     * Método para retornar cpf cadastrado no vinculo da viatura.
     * 
     * @param cdViatura
     * @return Long
     */
    public Long retornaCpf(Long cdViatura) {
	Long cpf = null;
	try {

	    int codigoViatura = cdViatura.intValue();
	
	    List<ViaturaVO> aitObjs = this.getViaturaXColaboradorByCDViatura(codigoViatura);

	    if (aitObjs != null && !aitObjs.isEmpty()) {

		ViaturaVO v = aitObjs.get(0);

		cpf = v.getCpf();
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return cpf;
    }

    private List<ViaturaVO> getViaturaXColaborador() {
	
	List<ViaturaVO> result = new ArrayList<ViaturaVO>();

	Connection conn = null;
	Statement stmt= null;
	ResultSet rs = null;

	try {

	    StringBuilder sql = new StringBuilder();

	    sql.append(" SELECT cpfInt, v.nm_viatura, v.cd_viatura FROM D_RHViaturaColaborador RH WITH(NOLOCK) ");
	    sql.append(" INNER JOIN X_SIGMA90VIATURA X WITH(NOLOCK) ON X.neoId = RH.viatura_neoId ");
	    sql.append(" INNER JOIN ["+this.serverInfo.get(OrsegupsConnectionUtils.NOME_SERVIDOR)+"\\"+this.serverInfo.get(OrsegupsConnectionUtils.INSTANCIA)+"].SIGMA90.dbo.VIATURA V WITH(NOLOCK) ON V.CD_VIATURA = X.cd_viatura ");


	    conn = PersistEngine.getConnection("");

	    stmt = conn.createStatement();
	    
	    rs = stmt.executeQuery(sql.toString());

	    while (rs.next()) {
		ViaturaVO v = new ViaturaVO();
		v.setCpf(rs.getLong(1));
		v.setNomeMotorista(rs.getString(2));
		v.setCodigoViatura(rs.getString(3));
		result.add(v);

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	return result;

    }
    
    private List<ViaturaVO> getViaturaXColaboradorByCDViatura(int cdViatura) {
	
	List<ViaturaVO> result = new ArrayList<ViaturaVO>();

	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	try {

	    StringBuilder sql = new StringBuilder();

	    sql.append(" SELECT cpfInt, nm_viatura FROM D_RHViaturaColaborador RH WITH(NOLOCK) ");
	    sql.append(" INNER JOIN X_SIGMA90VIATURA X WITH(NOLOCK) ON X.neoId = RH.viatura_neoId ");
	    sql.append(" INNER JOIN ["+this.serverInfo.get(OrsegupsConnectionUtils.NOME_SERVIDOR)+"\\"+this.serverInfo.get(OrsegupsConnectionUtils.INSTANCIA)+"].SIGMA90.dbo.VIATURA V WITH(NOLOCK) ON V.CD_VIATURA = X.cd_viatura ");
	    sql.append(" WHERE V.CD_VIATURA = ? ");

	    conn = PersistEngine.getConnection("");

	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setInt(1, cdViatura);

	    rs = pstm.executeQuery();

	    while (rs.next()) {
		ViaturaVO v = new ViaturaVO();
		v.setCpf(rs.getLong(1));
		v.setNomeMotorista(rs.getString(2));
		result.add(v);

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

	return result;

    }

    /**
     * Retorna situação da produtividade
     * 
     * @param codigo
     * @return NeoObject
     */
    public NeoObject retornaCPASituacao(Long codigo) {
	NeoObject neoObject = null;
	try {
	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("codigo", codigo));
	    List<NeoObject> aitObjs = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CPASituacao"), filter);

	    if (aitObjs != null && !aitObjs.isEmpty()) {

		neoObject = aitObjs.get(0);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return neoObject;
    }

    /**
     * Método retorna valores que seram lançados
     * 
     * @param competencia
     * @param cdViatura
     * @return AtendimentoValorVO
     */
    public AtendimentoValorVO getValorLancado(String competencia, Long cdViatura) {

	List<AtendimentosAitVO> produtividadeAASUPFISs = new ArrayList<AtendimentosAitVO>();
	produtividadeAASUPFISs = getListaProdutividade(competencia, cdViatura.toString());
	AtendimentoValorVO atendimentoValorVO = null;
	if (NeoUtils.safeIsNotNull(produtividadeAASUPFISs) && !produtividadeAASUPFISs.isEmpty()) {
	    atendimentoValorVO = new AtendimentoValorVO();
	    int count = 0;
	    Double totalValorDSL = 0.0;
	    Double totalValorATD = 0.0;
	    Double totalDeslocamento = 0.0;
	    Double totalAtendimento = 0.0;

	    for (AtendimentosAitVO aasupfisObj : produtividadeAASUPFISs) {
		Double valorDSL = 0.0;
		Double valorATD = 0.0;

		if (aasupfisObj.getDeslocamento() != null && !aasupfisObj.getDeslocamento().isEmpty()) {
		    if (Integer.parseInt(aasupfisObj.getDeslocamento()) >= 0 && Integer.parseInt(aasupfisObj.getDeslocamento()) <= 10) {
			valorDSL = 2.0;
		    } else if (Integer.parseInt(aasupfisObj.getDeslocamento()) > 10 && Integer.parseInt(aasupfisObj.getDeslocamento()) <= 15) {
			valorDSL = 1.0;
		    } else if (Integer.parseInt(aasupfisObj.getDeslocamento()) > 15 && Integer.parseInt(aasupfisObj.getDeslocamento()) <= 20) {
			valorDSL = 0.50;
		    }

		    if (Integer.parseInt(aasupfisObj.getAtendimento()) >= 0 && Integer.parseInt(aasupfisObj.getAtendimento()) <= 15) {
			valorATD = 1.0;
		    } else if (Integer.parseInt(aasupfisObj.getAtendimento()) > 15 && Integer.parseInt(aasupfisObj.getAtendimento()) <= 20) {
			valorATD = 0.50;
		    } else if (Integer.parseInt(aasupfisObj.getAtendimento()) > 20 && Integer.parseInt(aasupfisObj.getAtendimento()) <= 25) {
			valorATD = 0.25;
		    }
		}

		totalDeslocamento += Integer.parseInt(aasupfisObj.getDeslocamento());
		totalAtendimento += Integer.parseInt(aasupfisObj.getAtendimento());
		totalValorDSL += valorDSL;
		totalValorATD += valorATD;
		count++;

	    }

	    atendimentoValorVO.setCount(count);

	    Double subTotalDeslocamento = totalDeslocamento / count;
	    Double subTotalAtendimento = totalAtendimento / count;

	    atendimentoValorVO.setSubTotalDeslocamento(subTotalDeslocamento);
	    atendimentoValorVO.setSubTotalAtendimento(subTotalAtendimento);
	    atendimentoValorVO.setTotalValorDSL(totalValorDSL);
	    atendimentoValorVO.setTotalValorATD(totalValorATD);
	    atendimentoValorVO.setTotalValorGeral(totalValorDSL + totalValorATD);

	}

	return atendimentoValorVO;
    }

    /**
     * Retorna lista com atendimentos por AIT
     * 
     * @param competencia
     * @param cdViatura
     * @return List<AtendimentosAitVO>
     */
    protected List<AtendimentosAitVO> getListaProdutividade(String competencia, String cdViatura) {

	List<AtendimentosAitVO> atendimentosAitVOs = null;
	try {
	    ProdutividadeAitController produtividadeAitController = new ProdutividadeAitController();
	    String array[] = competencia.split("/");

	    String mes = array[0];
	    String ano = array[1];

	    atendimentosAitVOs = produtividadeAitController.listaAtendimentos(cdViatura, mes, ano);

	} catch (Exception e) {
	    e.printStackTrace();
	}
	return atendimentosAitVOs;
    }

    /**
     * Retorna os dados da produtividade de todos os AITs para a regional
     * 
     * @param regional
     * @return
     */
    public List<AtendimentoVtrVO> getRankingProdutividade(String regional) {

	GregorianCalendar dataAtual = new GregorianCalendar();

	if (dataAtual.get(Calendar.DAY_OF_MONTH) >= 16) {
	    dataAtual.add(Calendar.MONTH, +1);
	}

	String competencia = NeoDateUtils.safeDateFormat(dataAtual, "MM/yyyy");

	List<AtendimentoVtrVO> ranking = new ArrayList<AtendimentoVtrVO>();

	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;

	try {
	    conn = PersistEngine.getConnection("");

	    String SQL = "SELECT * FROM D_RHViaturaColaborador RH WITH(NOLOCK) " + " INNER JOIN X_SIGMA90VIATURA X WITH(NOLOCK) ON RH.viatura_neoId = X.neoId "
		    + " INNER JOIN ["+this.serverInfo.get(OrsegupsConnectionUtils.NOME_SERVIDOR)+"\\"+this.serverInfo.get(OrsegupsConnectionUtils.INSTANCIA)+"].SIGMA90.dbo.VIATURA V WITH(NOLOCK) ON X.cd_viatura = V.CD_VIATURA " + " WHERE V.NM_VIATURA LIKE '" + regional + "%' ";

	    stmt = conn.createStatement();

	    rs = stmt.executeQuery(SQL);

	    while (rs.next()) {
		ViaturaVO dadosViatura = null;

		long cdViatura = rs.getLong("CD_VIATURA");

		dadosViatura = this.getDadosAit(String.valueOf(cdViatura));

		if (dadosViatura != null) {
		    AtendimentoVtrVO viatura = new AtendimentoVtrVO();

		    viatura.setCdViatura(dadosViatura.getCodigoViatura());
		    viatura.setNmViatura(dadosViatura.getNomeMotorista());
		    viatura.setTitulo(dadosViatura.getTitulo());
		    viatura.setNomeImagem(dadosViatura.getNomeImagem());
		    viatura.setCpf(rs.getLong("cpfInt"));

		    AtendimentoValorVO produtividade = getValorLancado(competencia, cdViatura);

		    if (produtividade != null) {
			viatura.setProdutividade(produtividade);
			ranking.add(viatura);
		    }
		}
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, stmt, rs);
	}

	return ranking;
    }

    public Map<String, Boolean> getPermissaoExibicao() {

	Map<String, Boolean> retorno = new HashMap<>();

	NeoUser usuarioLogado = PortalUtil.getCurrentUser();

	NeoGroup grupo = usuarioLogado.getGroup();

	String nomeGrupo = grupo.getName();

	// USUARIOS QUE POSSUEM ACESSO A TODAS AS FILIAIS
	if (nomeGrupo.contains("Presidência") || nomeGrupo.contains("Diretoria") || nomeGrupo.equalsIgnoreCase("Coordenação de Sistemas") || nomeGrupo.contains("Superintendência")) {
	    for (String key : OrsegupsUtils.getRegionais().keySet()) {
		retorno.put(key, true);
	    }
	} else {

	    // VERIFICA SE USUARIO NÃO TEM ACESSO TOTAL

	    NeoPaper acessoTotal = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", "AITRankingTodos"));

	    if (acessoTotal != null) {
		Set<NeoUser> usuariosAcessoTotal = acessoTotal.getUsers();

		for (NeoUser user : usuariosAcessoTotal) {
		    if (user.equals(usuarioLogado)) {
			for (String key : OrsegupsUtils.getRegionais().keySet()) {
			    retorno.put(key, true);
			}
		    }
		}
		return retorno;
	    }

	    Map<String, Long> regionais = OrsegupsUtils.getRegionais();

	    for (String key : regionais.keySet()) {

		// VERIFICA GERENTES DE ELETRONICA DA SEDE

		if (regionais.get(key) == 1) {

		    if (usuarioLogado.getGroup().getName().equalsIgnoreCase("Gerência de Vigilância Eletrônica")) {
			retorno.put(key, true);
		    }
		}

		// VERIFICA GERENTES REGIONAIS
		NeoPaper papel = OrsegupsUtils.getPapelGerenteRegional(regionais.get(key));

		if (papel != null) {
		    Set<NeoUser> usuariosPapel = papel.getUsers();

		    for (NeoUser user : usuariosPapel) {
			if (user.equals(usuarioLogado)) {
			    retorno.put(key, true);
			}
		    }
		}

		papel = null;

		// VERIFICA COORDENADORES REGIONAIS

		papel = OrsegupsUtils.getPapelCoordenadorRegional(regionais.get(key));

		if (papel != null) {
		    Set<NeoUser> usuariosPapel = papel.getUsers();

		    for (NeoUser user : usuariosPapel) {
			if (user.equals(usuarioLogado)) {
			    retorno.put(key, true);
			}
		    }
		}

		// VERIFICA PAPEIS ESPECIFICOS DO RANKING

		papel = null;

		papel = OrsegupsUtils.getPapelRankingAIT(regionais.get(key));

		if (papel != null) {
		    Set<NeoUser> usuariosPapel = papel.getUsers();

		    for (NeoUser user : usuariosPapel) {
			if (user.equals(usuarioLogado)) {
			    retorno.put(key, true);
			}
		    }
		}
	    }
	}

	return retorno;
    }

}
