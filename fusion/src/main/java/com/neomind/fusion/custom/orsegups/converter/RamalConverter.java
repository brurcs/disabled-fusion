package com.neomind.fusion.custom.orsegups.converter;

import java.util.StringTokenizer;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.util.NeoUtils;

public class RamalConverter extends StringConverter {

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin) {
				
		String result = "";
		String ramalDestino = "";
		String ramalOrigem = "";
		String link = "";

		//pega ramal do usuário logado no sistema
		EntityWrapper usuarioOrigemWrapper = new EntityWrapper(PortalUtil.getCurrentUser());
		ramalOrigem = (String) usuarioOrigemWrapper.findValue("ramal");
		
		if(NeoUtils.safeIsNotNull(ramalOrigem)){
			
			if (field.getDeclaringEntityInfo().getSafeTypeName().equalsIgnoreCase("Tarefa")) {
				//processEntityWrapper pega o numero do ramal preenchido no cadastro do usuário do solicitante
				EntityWrapper processEntityWrapper = new EntityWrapper(field.getForm().getObject());
				ramalDestino = (String) processEntityWrapper.findValue("Solicitante.ramal");
				
				link = OrsegupsUtils.getCallLink(ramalOrigem, ramalDestino, true);
				
			} else {
				String numero = "";
				int cont = 0;
				//field.getValue() pega o valor já preenchido no campo
				ramalDestino = NeoUtils.safeOutputString(field.getValue());
				ramalDestino = ramalDestino.replaceAll("\\(|\\)|\\-|\\.|\\ ", "");
				
				StringTokenizer st = new StringTokenizer(ramalDestino, "/");
				while(st.hasMoreTokens()){
					numero = st.nextToken();
										

					link = link + OrsegupsUtils.getCallLink(ramalOrigem, numero, true);
					if (cont < st.countTokens()){
					    link = link + " / ";
					}
					
					cont++;	
				}
			}
		}
		
		if (NeoUtils.safeIsNotNull(link)) {
			result = link;
		} else {
			result = super.getHTMLView(field, origin);
		}
		
		return result;
	}	
}
