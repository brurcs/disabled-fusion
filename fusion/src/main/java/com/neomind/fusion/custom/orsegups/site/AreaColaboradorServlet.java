package com.neomind.fusion.custom.orsegups.site;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.UserWebServiceAuthentication;
import com.neomind.fusion.custom.orsegups.e2doc.controller.E2docHandler;
import com.neomind.fusion.custom.orsegups.maps.servlet.OrsegupsMapsServlet;
import com.neomind.fusion.custom.orsegups.presenca.vo.DocumentoTreeVO;
import com.neomind.fusion.custom.orsegups.presenca.vo.SaldoHorasVO;
import com.neomind.fusion.custom.orsegups.site.vo.HorasVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteAnotacaoColaboradorVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteColaboradorVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteContraChequeColaboradorVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteEmpresaColaboradorVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteEventosColaboradorVO;
import com.neomind.fusion.custom.orsegups.site.vo.SiteFilialColaboradorVO;
import com.neomind.fusion.custom.orsegups.utils.DocumentoComparator;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.datawarehouse.ExternalEntityInfo;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.dynamic.DocumentEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLNotInFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

@WebServlet(name="AreaColaboradorServlet", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.site.AreaColaboradorServlet"})
public class AreaColaboradorServlet extends HttpServlet
{

	private static final long serialVersionUID = 1L;
	private static final Log log = LogFactory.getLog(OrsegupsMapsServlet.class);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp); 
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.doRequest(req, resp);
	}

	private void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/html");
		response.setCharacterEncoding("ISO-8859-1");
		PrintWriter out = null;
		try
		{
			String action = "";

			if (request.getParameter("action") != null)
			{
				action = request.getParameter("action");
			}
			
			if(!action.equalsIgnoreCase("downloadDocPunicao")){
				out = response.getWriter();	
			}
			

			if (action.equalsIgnoreCase("login"))
			{
				this.login(request, response, out);
			}

			if (action.equalsIgnoreCase("logout"))
			{
				this.logout(request, response, out);
			}
			
			if (action.equalsIgnoreCase("listarContraCheques"))
			{
				this.listarContraCheques(request, response, out);
			}
			
			if (action.equalsIgnoreCase("listarCartaoPonto"))
			{
				this.listarCartaoPonto(request, response, out);
			}
			
			if (action.equalsIgnoreCase("alterarSenha"))
			{
				this.alterarSenha(request, response, out);
			}
			if (action.equalsIgnoreCase("alterarDados"))
			{
				this.alterarDados(request, response, out);
			}
			if (action.equalsIgnoreCase("listarDados"))
			{
				this.listarDados(request, response, out);
			}
			if (action.equalsIgnoreCase("listaHistoricoDisciplinar"))
			{
				this.listaHistoricoDisciplinar(request, response, out);
			}
			if(action.equalsIgnoreCase("listaDocumentosPunicoes")){
				this.listaDeDocumentosPunicoes(request, response, out);
			}
			
			if(action.equalsIgnoreCase("listarRegistrosPontoMobile")){
				this.pontosDia(request, response, out);
			}
			
			if(action.equalsIgnoreCase("listarRegistrosPagamentoMobile")){
				this.pagamentoMes(request, response, out);
			}
			
			
			
			if(action.equalsIgnoreCase("downloadDocPunicao")){
				String neoId = request.getParameter("id");
				if(neoId != null && !neoId.isEmpty())
				this.downloadDocPunicao(Long.parseLong(neoId), response);
			}
			
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	

	private void login(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws JSONException, ServletException
	{

		// Validar autentificacao MD5
		String key = request.getParameter("key");

		UserWebServiceAuthentication auth = new UserWebServiceAuthentication();
		if (!auth.authentication(key))
		{
			JSONObject jsonAuth = new JSONObject();

			jsonAuth.append("status", 0L);
			jsonAuth.append("msg", "Erro de autenticação");

			out.print(jsonAuth);
			return;
		}

		String cpf = request.getParameter("user");
		String pass = request.getParameter("pass");
		
		cpf = cpf.replaceAll("[^0-9]", ""); 

		if (cpf == null || cpf.trim().equals("") || pass == null || pass.trim().equals(""))
		{
			JSONObject jsonLogin = new JSONObject();

			jsonLogin.append("status", 0L);
			jsonLogin.append("msg", "Login/Senha obrigatório");

			out.print(jsonLogin);
			return;
		}
		else
		{
			Long cpfLong = Long.valueOf(cpf);
			try
			{
				String titRed = "";
				String sitAfa = "";
				String caudem = "";
				GregorianCalendar datAfa = null;
				if ((cpfLong != null) && (cpfLong > 0))
				{
					
					ArrayList sitList = new ArrayList(1);
					sitList.add(7L);
					QLGroupFilter filterCol = new QLGroupFilter("AND");
					filterCol.addFilter(new QLEqualsFilter("numcpf", cpfLong));
					//filterCol.addFilter(new QLNotInFilter("sitafa", sitList));
					List<NeoObject> listaCol = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORHUSUFUNFUSION"), filterCol, -1,-1,"datafa ASC");
					if (!listaCol.isEmpty())
					{
						EntityWrapper usuarioColaborador = new EntityWrapper(listaCol.get(0));
						sitAfa = usuarioColaborador.getValue("sitafa").toString();
						titRed = usuarioColaborador.getValue("titred").toString();
						caudem = usuarioColaborador.getValue("caudem").toString();
						
						if (sitAfa.equalsIgnoreCase("7"))
						{
							/*if ( (caudem.equalsIgnoreCase("1") || caudem.equalsIgnoreCase("2") ) && !SiteUtils.verificaPrazoAcessoSite(cpfLong, 60)){
								out.print("Verifique a situação de afastamento.");
								throw new ServletException("Situação afastamento igual a 7. ");
							}*/

						}
						
						String senhaMaster = null;
						List<NeoObject> usr = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("siteColabConfigSenhaMaster"), null);
						if (usr != null)
						{
							EntityWrapper wrapperSenhaMaster = new EntityWrapper(usr.get(0));
							senhaMaster = (String) wrapperSenhaMaster.getValue("senha");
						}

						cpfLong = Long.parseLong(cpf.trim());
						log.warn("Funcionário: " + cpf);
						log.warn("Senha: " + pass);
						QLGroupFilter filterUser = new QLGroupFilter("AND");
						filterUser.addFilter(new QLEqualsFilter("usu_numcpf", Long.valueOf(cpf)));
						if (senhaMaster == null || !senhaMaster.equals(pass)){
							filterUser.addFilter(new QLEqualsFilter("usu_senusu", pass));	
						}
						
						// View de Contas
						//List<NeoObject> users = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORHUSUVUSU"), filterUser);
						

						if (SiteUtils.loginColaboradorValido(cpfLong, pass) || senhaMaster.equals(pass))
						{
							EntityWrapper usuarioCliente = new EntityWrapper(listaCol.get(0));
							Long numCad = (Long) usuarioCliente.getValue("numcad");
							String nomfun = (String) usuarioCliente.getValue("nomfun");

							QLGroupFilter filterCliente = new QLGroupFilter("AND");
							filterCliente.addFilter(new QLEqualsFilter("cd_cliente", numCad));
							filterCliente.addFilter(new QLEqualsFilter("ctrl_central", true));

							String newKey = numCad.toString() + new GregorianCalendar().getTimeInMillis();
							GregorianCalendar dataSession = new GregorianCalendar();

							dataSession.add(Calendar.HOUR_OF_DAY, +1);

							MessageDigest md = MessageDigest.getInstance("MD5");
							md.update(newKey.getBytes());
							byte[] hashMd5 = md.digest();

							String txtMd5 = OrsegupsUtils.stringHexa(hashMd5);

							QLEqualsFilter filtroSession = new QLEqualsFilter("codigo", cpfLong);
							List<NeoObject> sessions = PersistEngine.getObjects(AdapterUtils.getEntityClass("UsuarioWebServiceSession"), filtroSession);

							/*Excluir Sessao
							if (sessions != null && !sessions.isEmpty())
							{
								PersistEngine.removeObjects(sessions);
							}
							*/

							NeoObject session = AdapterUtils.createNewEntityInstance("UsuarioWebServiceSession");

							if (session != null)
							{
								EntityWrapper sessionWrapper = new EntityWrapper(session);
								sessionWrapper.setValue("key", txtMd5);
								sessionWrapper.setValue("time", dataSession);
								sessionWrapper.setValue("codigo", cpfLong);
								sessionWrapper.setValue("ip", request.getRemoteAddr());
								QLEqualsFilter filtroService = new QLEqualsFilter("codigo", 2L);
								NeoObject sessionsServico = PersistEngine.getObject(AdapterUtils.getEntityClass("UsuarioWebServiceServico"), filtroService);
								sessionWrapper.setValue("servico", sessionsServico);

								PersistEngine.persist(session);
							}

							JSONObject jsonColaborador = new JSONObject();

							jsonColaborador.append("status", 1L);
							jsonColaborador.append("nomcli", nomfun);
							jsonColaborador.append("titred", titRed);
							jsonColaborador.append("codcli", numCad.toString());
							jsonColaborador.append("sessionId", txtMd5);
							jsonColaborador.append("cpf", cpf);

							// Gson gson = new Gson();
							// String userJSON = gson.toJson(user.get(0));

							out.print(jsonColaborador);

						}
						else
						{
							JSONObject jsonUser = new JSONObject();

							jsonUser.append("status", 0L);
							jsonUser.append("msg", "Usuário/Senha Inválidos");
							out.print(jsonUser);
							return;
						}
					}
					else
					{
						JSONObject jsonUser = new JSONObject();

						jsonUser.append("status", 0L);
						jsonUser.append("msg", "Usuário/Senha Inválidos");
						out.print(jsonUser);
						return;
					}
				}
			}
			catch (NumberFormatException e)
			{
				JSONObject jsonUser = new JSONObject();

				jsonUser.append("status", 0L);
				jsonUser.append("msg", "Usuário/Senha Inválidos");
				out.print(jsonUser);
				e.printStackTrace();
			}
			catch (NoSuchAlgorithmException e)
			{
				JSONObject jsonUser = new JSONObject();

				jsonUser.append("status", 0L);
				jsonUser.append("msg", "Usuário/Senha Inválidos");
				out.print(jsonUser);
				e.printStackTrace();
			}
			catch (Exception e)
			{
				JSONObject jsonUser = new JSONObject();

				jsonUser.append("status", 0L);
				jsonUser.append("msg", "Usuário/Senha Inválidos");
				out.print(jsonUser);
				e.printStackTrace();
			}
		}
	}

	private void logout(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws JSONException, ServletException, IOException
	{
		// verifaca id Sessão
		String sessionId = request.getParameter("sessionId");
		SiteUtils siteUtils = new SiteUtils();

		// buscar Sesssao
		NeoObject session = siteUtils.getSession(sessionId);

		JSONObject jsonCliente = new JSONObject();

		if (session != null)
		{
			//remove sessao
			PersistEngine.remove(session);
			jsonCliente.append("status", 1L);
			jsonCliente.append("msg", "Logout efetuado com sucesso.");
		}
		else
		{
			jsonCliente.append("status", 2L);
			jsonCliente.append("msg", "Sessão inexistente.");
		}
		out.print(jsonCliente);

	}

	private void alterarSenha(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws JSONException
	{

		String sessionId = request.getParameter("sessionId");
		SiteUtils siteUtils = new SiteUtils();

		Connection connVetorh = PersistEngine.getConnection("VETORH");
		
		// buscar Sesssao
		NeoObject session = siteUtils.getSession(sessionId);
		JSONObject jsonColaborador = new JSONObject();
		if (session == null)
		{

			jsonColaborador.append("status", 2L);
			jsonColaborador.append("msg", "Sessão inexistente");
			out.print(jsonColaborador);
			return;
		}
		EntityWrapper sessionWrapper = new EntityWrapper(session);
		Long numCpf = (Long) sessionWrapper.getValue("codigo");

		String senhaAtual = (String) request.getParameter("inSenhaAtual");
		String novaSenha = (String) request.getParameter("inNovaSenha");
		String novaSenha2 = (String) request.getParameter("inNovaSenha2");

		QLGroupFilter filterColSen = new QLGroupFilter("AND");
		filterColSen.addFilter(new QLEqualsFilter("usu_numcpf", numCpf));
		filterColSen.addFilter(new QLEqualsFilter("usu_senusu", senhaAtual));

		ExternalEntityInfo infoViewLoginColaborador = (ExternalEntityInfo) EntityRegister.getInstance().getCache().getByString("VETORHUSUVUSU");
		List<NeoObject> user = (List<NeoObject>) PersistEngine.getObjects(infoViewLoginColaborador.getEntityClass(), filterColSen);
		if ((!user.isEmpty()) && (novaSenha != null) && (!novaSenha.equals("")) && (novaSenha.equals(novaSenha2)))
		{
			PreparedStatement st = null;
			try
			{
				StringBuffer sqlUpd = new StringBuffer();
				sqlUpd.append("UPDATE USU_T230USU SET USU_SenUsu = ? WHERE USU_NumCpf = ?");

				st = connVetorh.prepareStatement(sqlUpd.toString());
				st.setString(1, novaSenha);
				st.setLong(2, numCpf);
				st.executeUpdate();

				jsonColaborador.append("status", 1L);
				jsonColaborador.append("msg", "Senha alterada com sucesso.");
				out.print(jsonColaborador);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				jsonColaborador.append("status", 0L);
				jsonColaborador.append("msg", "Não foi possível alterar a senha, erro interno.");
				out.print(jsonColaborador);

			}
			finally
			{
				try
				{
					connVetorh.close();
				}
				catch (SQLException e)
				{
					e.printStackTrace();
				}
			}
		}
		else
		{
			jsonColaborador.append("status", 0L);
			jsonColaborador.append("msg", "Não foi possível alterar a senha, verifique os dados informados.");
			out.print(jsonColaborador);
		}
	}

	private void listarContraCheques(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws JSONException
	{
		try{
		
			String sessionId = request.getParameter("sessionId");
			SiteUtils siteUtils = new SiteUtils();
	
			// buscar Sesssao
			NeoObject session = siteUtils.getSession(sessionId);
	
			if (session == null)
			{
				JSONObject jsonColaborador = new JSONObject();
	
				jsonColaborador.append("status", 2L);
				jsonColaborador.append("msg", "Sessão Sessão");
				out.print(jsonColaborador);
				return;
			}
	
			EntityWrapper sessionWrapper = new EntityWrapper(session);
			Long numCpf = (Long) sessionWrapper.getValue("codigo");
			String nomFun = "";
			String titRed = "";
			// /verificar com o Lucas
			if ((Long.valueOf(numCpf) != null) && (Long.valueOf(numCpf) > 0))
			{
				//ArrayList sitList = new ArrayList(1);
				//sitList.add(7L);
	
				QLGroupFilter filterCol = new QLGroupFilter("AND");
				filterCol.addFilter(new QLEqualsFilter("numcpf", Long.valueOf(numCpf)));
				//filterCol.addFilter(new QLNotInFilter("sitafa", sitList));
	
				List<NeoObject> listaCol = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORHUSUFUNFUSION"), filterCol);
	
				List<SiteColaboradorVO> listaColaboradorVO = new ArrayList<SiteColaboradorVO>();
				
				if (!listaCol.isEmpty())
				{
					SiteColaboradorVO colaboradorVO = new SiteColaboradorVO();
					EntityWrapper usuarioColaborador = new EntityWrapper(listaCol.get(0));
					nomFun = usuarioColaborador.getValue("nomfun").toString();
					titRed = usuarioColaborador.getValue("titred").toString();
	
					for (NeoObject itemCol : listaCol)
					{
						colaboradorVO = new SiteColaboradorVO();
						SiteEmpresaColaboradorVO empresaColaboradorVO = new SiteEmpresaColaboradorVO();
						EntityWrapper wrapperCol = new EntityWrapper((NeoObject) itemCol);
						String apeemp = (String) wrapperCol.getValue("apeemp");
						Long numemp = (Long) wrapperCol.getValue("numemp");
						Long tipcol = (Long) wrapperCol.getValue("tipcol");
						Long numcad = (Long) wrapperCol.getValue("numcad");
						Long codfil = (Long) wrapperCol.getValue("codfil");
				
						QLGroupFilter filterChqDisp = new QLGroupFilter("AND");
						filterChqDisp.addFilter(new QLEqualsFilter("usu_numemp", numemp));
						filterChqDisp.addFilter(new QLEqualsFilter("usu_tipcol", tipcol));
						filterChqDisp.addFilter(new QLEqualsFilter("usu_numcad", numcad));
					
						List<NeoObject> listaChqDisp = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORHUSUVCHQ"), filterChqDisp);
	
						if (!listaChqDisp.isEmpty())
						{
							
							SiteFilialColaboradorVO filialColaborador = new SiteFilialColaboradorVO();
							filialColaborador = SiteUtils.retornaDadosFilial(numemp,codfil);
							empresaColaboradorVO.setFilial(filialColaborador);
							
							empresaColaboradorVO.setCodEmp(numemp);
							empresaColaboradorVO.setNomEmp(apeemp);
							colaboradorVO.setEmpresa(empresaColaboradorVO);
							colaboradorVO.setNumCad(String.valueOf(numcad));
							colaboradorVO.setNomFun(String.valueOf(nomFun));
							colaboradorVO.setTipCol(String.valueOf(tipcol));
							
							for (NeoObject itemChqDisp : listaChqDisp)
							{
	
								EntityWrapper wrpChqDisp = new EntityWrapper((NeoObject) itemChqDisp);
								QLGroupFilter filterChqMes = new QLGroupFilter("AND");
								filterChqMes.addFilter(new QLEqualsFilter("usu_numemp", numemp));
								filterChqMes.addFilter(new QLEqualsFilter("usu_tipcol", tipcol));
								filterChqMes.addFilter(new QLEqualsFilter("usu_numcad", numcad));
								
								GregorianCalendar menorCompetencia = new GregorianCalendar();
								menorCompetencia.set(GregorianCalendar.DAY_OF_MONTH, 1);	
								menorCompetencia.set(GregorianCalendar.HOUR, 0);	
								menorCompetencia.set(GregorianCalendar.MINUTE, 0);	
								menorCompetencia.set(GregorianCalendar.SECOND, 0);	
								menorCompetencia.set(GregorianCalendar.MILLISECOND, 0);	
								menorCompetencia.add(GregorianCalendar.MONTH, -4);	
								filterChqMes.addFilter(new QLOpFilter("usu_perref", ">" , menorCompetencia));
								
								System.out.println(NeoDateUtils.safeDateFormat(menorCompetencia));
	
								//List<NeoObject> listaChqMes = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORHUSUTCHQ"), filterChqMes, -1, -1, "usu_perref DESC");
								List<SiteContraChequeColaboradorVO> listaChqMes = SiteUtils.getContraCheque(numemp, tipcol, numcad);
								
								List<SiteContraChequeColaboradorVO> listaContraChequeVO = new ArrayList<SiteContraChequeColaboradorVO>();
								if (!listaChqMes.isEmpty())
								{
									for (SiteContraChequeColaboradorVO itemChqMes : listaChqMes)
									{
										if (itemChqMes.getMostraSite() == 0L){
											continue;
										}
										//SiteContraChequeColaboradorVO contraChequeColaboradorVO = new SiteContraChequeColaboradorVO();
										
										
										colaboradorVO.setEmpresa(empresaColaboradorVO);
	
										QLGroupFilter filterEvt = new QLGroupFilter("AND");
										filterEvt.addFilter(new QLEqualsFilter("usu_numemp", numemp));
										filterEvt.addFilter(new QLEqualsFilter("usu_tipcol", tipcol));
										filterEvt.addFilter(new QLEqualsFilter("usu_numcad", numcad));
										filterEvt.addFilter(new QLEqualsFilter("usu_perref", itemChqMes.getPerRefDate()));
	
										List<NeoObject> listaEvt = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("VETORHUSUTEVT"), filterEvt, -1, -1, "usu_tipeve");
	
										List<SiteEventosColaboradorVO> eventos = new ArrayList<SiteEventosColaboradorVO>();
										if (!listaEvt.isEmpty())
										{
											for (NeoObject itemEvt : listaEvt)
											{
												SiteEventosColaboradorVO siteEventosColaboradorVO = new SiteEventosColaboradorVO();
												EntityWrapper wrpEvt = new EntityWrapper((NeoObject) itemEvt);
	
												siteEventosColaboradorVO.setCodEve(String.valueOf(wrpEvt.getValue("usu_codeve")));
												siteEventosColaboradorVO.setDesEve((String) wrpEvt.getValue("usu_deseve"));
												if (((BigDecimal) wrpEvt.getValue("usu_refeve")).doubleValue() != 0)
													siteEventosColaboradorVO.setRefEve(NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), wrpEvt.getValue("usu_refeve")));
												
												if((Long) wrpEvt.getValue("usu_tipeve") == 1L){
													siteEventosColaboradorVO.setValPro(NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), wrpEvt.getValue("usu_valeve")));
												}else if((Long) wrpEvt.getValue("usu_tipeve") == 2L){
													siteEventosColaboradorVO.setValPro(NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), wrpEvt.getValue("usu_valeve")));
												}else if((Long) wrpEvt.getValue("usu_tipeve") == 3L){
													
													if (siteEventosColaboradorVO.getCodEve().equals("163")){
														siteEventosColaboradorVO.setValPro(NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), wrpEvt.getValue("usu_valeve")));
													}else{
														siteEventosColaboradorVO.setValDes(NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), wrpEvt.getValue("usu_valeve")));
													}
													
												}else if((Long) wrpEvt.getValue("usu_tipeve") == 4L  || (Long) wrpEvt.getValue("usu_tipeve") == 6L){
													siteEventosColaboradorVO.setValDes(NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), wrpEvt.getValue("usu_valeve")));
												}else if((Long) wrpEvt.getValue("usu_tipeve") == 5L){
													siteEventosColaboradorVO.setValPro(NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), wrpEvt.getValue("usu_valeve")));
												}else{
													siteEventosColaboradorVO.setValPro(NeoUtils.safeFormat(new DecimalFormat("#,##0.00"), wrpEvt.getValue("usu_valeve")));
												}
												
												if(!((Long) wrpEvt.getValue("usu_tipeve") == 5L)){
													eventos.add(siteEventosColaboradorVO);
												}
											}
										}
	
										itemChqMes.setEventosColaboradorVOs(eventos);
										listaContraChequeVO.add(itemChqMes);
									}
								}
								if(listaContraChequeVO.size() > 0){
									colaboradorVO.setContraCheque(listaContraChequeVO);	
								}
							}
							listaColaboradorVO.add(colaboradorVO);
						}
					}
				}
	
				Gson gson = new Gson();
				String colaboradorJson = gson.toJson(listaColaboradorVO);
				System.out.println(colaboradorJson);
				out.print(colaboradorJson);
			}
		}
		catch (Exception e)
		{
			JSONObject jsonUser = new JSONObject();

			jsonUser.append("status", 0L);
			jsonUser.append("msg", "Erro inesperado.");
			out.print(jsonUser);
			e.printStackTrace();
		}
	}
	
	private void listarCartaoPonto(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws JSONException
	{
		Connection connVetorh = PersistEngine.getConnection("VETORH");
		JSONArray jsonRetorno = new JSONArray();		
		JSONArray jsonAcessos = new JSONArray();
		
		Long numcad = 0L;
		Long numemp = 0L;
		Long tipcol = 0L;
		
		try
		{
			
			String sessionId = request.getParameter("sessionId");
			SiteUtils siteUtils = new SiteUtils();
	
			// buscar Sesssao
			NeoObject session = siteUtils.getSession(sessionId);
	
			if (session == null)
			{
				JSONObject jsonColaborador = new JSONObject();
	
				jsonColaborador.append("status", 2L);
				jsonColaborador.append("msg", "Sessão Sessão");
				out.print(jsonColaborador);
				return;
			}
			
			EntityWrapper sessionWrapper = new EntityWrapper(session);
			Long numCpf = (Long) sessionWrapper.getValue("codigo");
			
			GregorianCalendar gc = new GregorianCalendar();
			gc.add(GregorianCalendar.MONTH, -3);
			gc.set(GregorianCalendar.DATE, 16);

			StringBuffer sqlAcesso = new StringBuffer();
			sqlAcesso.append(" SELECT acc.DatAcc, DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) as data, acc.dirAcc, acc.codrlg, fun.numcad, fun.numemp, fun.tipcol, jma.DesJMa, acc.usu_obsjma, acc.TipAcc, tac.DesTAc, fun.NumCra, acc.oriacc, ");
			sqlAcesso.append(" (select cal.perref from dbo.R044CAL cal where cal.NumEmp = 18 and tipcal = 11 and acc.DatAcc >= cal.iniapu and acc.DatAcc <= cal.fimapu) as cpt ");
			sqlAcesso.append(" FROM R070ACC acc ");
			sqlAcesso.append(" inner join R070TAC tac on (acc.tipacc = tac.TipAcc) ");
			sqlAcesso.append(" left join r076jma jma on jma.CodJMa = acc.usu_codjma ");
			sqlAcesso.append(" INNER JOIN R038HCH hch ON  hch.NumCra = acc.NumCra   ");
			sqlAcesso.append(" INNER JOIN R034FUN fun ON fun.NumEmp = hch.NumEmp AND fun.NumCad = hch.NumCad AND fun.TipCol = hch.TipCol  "); // and hch.numcra = fun.NumCra
			sqlAcesso.append(" WHERE fun.NumCpf = ?  AND acc.DatAcc >= ? AND acc.UsoMar = 2 AND ( (acc.CodPlt = 2 and acc.TipAcc >= 100) "); //AND (acc.TipAcc >= 100 or (acc.TipAcc = 1 and acc.oriacc = 'R')) AND acc.DatAcc >= ? ");
			sqlAcesso.append(" or (acc.codplt = 0 and oriacc = 'R')	or (acc.TipAcc = 1 and acc.oriacc = 'R'))  ") ;
			sqlAcesso.append(" GROUP BY acc.DatAcc, acc.HorAcc, acc.dirAcc, acc.codrlg, fun.numcad, fun.numemp, fun.tipcol, jma.DesJMa, acc.usu_obsjma, acc.TipAcc, tac.DesTAc, fun.NumCra, acc.oriacc  ");
			sqlAcesso.append(" ORDER BY DATEADD(MINUTE, acc.HorAcc, acc.DatAcc) desc ");
			

			PreparedStatement st = connVetorh.prepareStatement(sqlAcesso.toString());
			st.setLong(1, numCpf);
			st.setDate(2, new Date(gc.getTimeInMillis()));
			
			ResultSet rs = st.executeQuery();
			
			Date dateIter = null;
			GregorianCalendar gDateIter = null;
			JSONObject jsonAcesso = null;
			JSONArray listaMarcacoes = null;
			JSONArray listaMarcacoesAjustadas = null;
			
			
			if (rs.next())
			{
				numcad = rs.getLong("numcad");
				numemp = rs.getLong("numemp");
				tipcol = rs.getLong("tipcol");
				
				do{
					Date date = rs.getDate("DatAcc");
					if (!date.equals(dateIter)){ // se mudar o dia, reinicia o objeto acesso e recria as marcações
						
						if (dateIter != null){ // na primeira passada não deve executar isto
							jsonAcesso.put("accs", listaMarcacoes);
							jsonAcesso.put("accsCM", listaMarcacoesAjustadas);
							jsonAcessos.put(jsonAcesso);
						}
						
						dateIter = date;
						gDateIter = new GregorianCalendar();
						gDateIter.setTimeInMillis(dateIter.getTime());
						
						Timestamp tsAcc = rs.getTimestamp("data"); // hora batida 
						GregorianCalendar datacc = new GregorianCalendar();
						datacc.setTimeInMillis(tsAcc.getTime());
						jsonAcesso = new JSONObject();
						jsonAcesso.put("datacc", NeoUtils.safeDateFormat(gDateIter));
						jsonAcesso.put("diaSemana", OrsegupsUtils.getDayOfWeek(datacc));
						jsonAcesso.put("cpt", rs.getString("cpt"));
						listaMarcacoes = new JSONArray();
						listaMarcacoesAjustadas =  new JSONArray();
						
						
						JSONObject accs = new JSONObject();
						accs.put("dataacc", NeoUtils.safeDateFormat(datacc));
						accs.put("diracc", rs.getString("dirAcc"));
						accs.put("oriacc", rs.getString("oriacc"));
						accs.put("codrlg", rs.getString("codRlg"));
						accs.put("desjma", rs.getString("DesJMa"));
						accs.put("tipacc", rs.getString("TipAcc"));
						accs.put("destac", rs.getString("DesTAc"));
						accs.put("usu_obsjma", rs.getString("usu_obsjma"));
						listaMarcacoes.put(accs);
						
					}else{
						
						Timestamp tsAcc = rs.getTimestamp("data"); // hora batida 
						GregorianCalendar datacc = new GregorianCalendar();
						datacc.setTimeInMillis(tsAcc.getTime());
						
						JSONObject accs = new JSONObject();
						accs.put("dataacc", NeoUtils.safeDateFormat(datacc));
						accs.put("diracc", rs.getString("dirAcc"));
						accs.put("oriacc", rs.getString("oriacc"));
						accs.put("codrlg", rs.getString("codRlg"));
						accs.put("desjma", rs.getString("DesJMa"));
						accs.put("tipacc", rs.getString("TipAcc"));
						accs.put("destac", rs.getString("DesTAc"));
						accs.put("usu_obsjma", rs.getString("usu_obsjma"));
						accs.put("cpt", rs.getString("cpt"));
						listaMarcacoes.put(accs);
						
					}
				}while(rs.next());
			}
			jsonAcesso.put("accs", listaMarcacoes);
			jsonAcesso.put("accsCM", listaMarcacoesAjustadas);
			jsonAcessos.put(jsonAcesso);

			rs.close();
			st.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				connVetorh.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		JSONObject dadosCabecalho = SiteUtils.retornaCabecalhoCartaoPonto(numcad, numemp, tipcol);
		
		GregorianCalendar dataCorte = new GregorianCalendar();
		dataCorte.set(GregorianCalendar.MONDAY,05);
		dataCorte.set(GregorianCalendar.DATE,01);
		dataCorte.set(GregorianCalendar.YEAR,2016);
		dataCorte.set(GregorianCalendar.HOUR,0);
		dataCorte.set(GregorianCalendar.MINUTE,0);
		dataCorte.set(GregorianCalendar.SECOND,0);
		dataCorte.set(GregorianCalendar.MILLISECOND,0);
		  
		GregorianCalendar gca =  new GregorianCalendar();
		GregorianCalendar gcb =  new GregorianCalendar();
		GregorianCalendar gcaFim =  new GregorianCalendar();
		gcb.set(GregorianCalendar.DATE, 1);
		gcb.add(GregorianCalendar.DATE, -1);
		  
		gcaFim = (GregorianCalendar) gca.clone(); 
		gcaFim.add(GregorianCalendar.MONTH, 1);
		gcaFim.set(GregorianCalendar.DATE, 1);
		gcaFim.add(GregorianCalendar.DATE, -1);
		
		HorasVO horas = new HorasVO();
		HorasVO horasAnt = new HorasVO();
		SaldoHorasVO saldos = new SaldoHorasVO();
		  
		horasAnt = SiteUtils.retornaSaldoHoras(numcad, numemp, 1L, gcb , 0, true );
		horas = SiteUtils.retornaSaldoHoras(numcad, numemp, 1L, gca , 0, false );
		saldos = SiteUtils.calculaSaldoFinal(horasAnt, horas);
		
		String saldoAnterior = "000:00";
		String saldoAVencer = "000:00";
		if(gca.getInstance().after(dataCorte)){
			saldoAnterior = OrsegupsUtils.getHorarioBanco(horasAnt.getTotal()) ;
		}else{
		     if(horasAnt.getTotal() > 0 ) {
		    	 saldoAnterior = "000:00"; 
		     }else{ 
		    	 saldoAnterior = OrsegupsUtils.getHorarioBanco(horasAnt.getTotal()); 
		     }
	   }
		
		
		if(gca.getInstance().after(dataCorte)){
			saldoAVencer= OrsegupsUtils.getHorarioBanco(saldos.getSaldoVencer()); 
		}else{
			if(saldos.getSaldoVencer() > 0 ) {
				saldoAVencer = "000:00" ;
			}else{ 
				saldoAVencer = OrsegupsUtils.getHorarioBanco(saldos.getSaldoVencer()); 
			}
		}
			
		GregorianCalendar datUltApu = new GregorianCalendar();
		datUltApu.add(GregorianCalendar.DATE, -1);
		
		
		Date ultimaDataApurada = new Date(datUltApu.getTimeInMillis()); //SiteUtils.retornaUltimaDataApurada(numcad, numemp, tipcol);
		if (dadosCabecalho != null){
			dadosCabecalho.put("saldo", OrsegupsUtils.getHorarioBanco(saldos.getSaldo()));
			dadosCabecalho.put("saldoAnterior", saldoAnterior /*OrsegupsUtils.getHorario(saldos.getSaldoAnterior())*/);
			dadosCabecalho.put("saldoVencer", saldoAVencer /*OrsegupsUtils.getHorario(saldos.getSaldoVencer())*/);
			if (ultimaDataApurada != null){
				GregorianCalendar gc = new GregorianCalendar();
				gc.setTimeInMillis(ultimaDataApurada.getTime());
				//gc.add(GregorianCalendar.DATE, -1);
				dadosCabecalho.put("ultimaDataApurada", NeoUtils.safeDateFormat(gc,"dd/MM/yyyy"));
				dadosCabecalho.put("cptAtual", NeoUtils.safeDateFormat(new GregorianCalendar(),"MM/yyyy"));
				dadosCabecalho.put("cptAnterior", NeoUtils.safeDateFormat(gcb,"MM/yyyy"));
				
				GregorianCalendar dataVencimento = new GregorianCalendar();
				dataVencimento.set(GregorianCalendar.DATE, 1);
				dataVencimento.add(GregorianCalendar.MONTH,1);
				dataVencimento.add(GregorianCalendar.DATE,-1);
				
				dadosCabecalho.put("cptVencer", NeoUtils.safeDateFormat(dataVencimento,"dd/MM/yyyy"));
				
				
				
			}
		}
		jsonRetorno.put(0,jsonAcessos);
		
		jsonRetorno.put(1,dadosCabecalho);
		
		System.out.println(jsonRetorno);
		out.print(jsonRetorno);
		out.flush();
		out.close();
	}
	
	
	
	private void alterarDados(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws JSONException
	{

		String sessionId = request.getParameter("sessionId");
		SiteUtils siteUtils = new SiteUtils();

		Connection connVetorh = PersistEngine.getConnection("VETORH");
		
		// buscar Sesssao
		NeoObject session = siteUtils.getSession(sessionId);
		JSONObject jsonColaborador = new JSONObject();
		if (session == null)
		{

			jsonColaborador.append("status", 2L);
			jsonColaborador.append("msg", "Sessão inexistente");
			out.print(jsonColaborador);
			return;
		}
		EntityWrapper sessionWrapper = new EntityWrapper(session);
		Long numCpf = (Long) sessionWrapper.getValue("codigo");
		
		String novoEmail = (String) request.getParameter("novoEmail");
		

		
			PreparedStatement st = null;
			try
			{
				StringBuffer sqlUpd = new StringBuffer();
				sqlUpd.append("UPDATE R034FUN SET usu_emapon = ? WHERE numcpf = ?");

				st = connVetorh.prepareStatement(sqlUpd.toString());
				st.setString(1, novoEmail);
				st.setLong(2, numCpf);
				st.executeUpdate();

				jsonColaborador.append("status", 1L);
				jsonColaborador.append("msg", "E-mail alterado com sucesso.");
				
				out.print(jsonColaborador);
			}
			catch (Exception e)
			{
				System.out.println("[AREA COLABORADOR] - Erro ao alterar e-mail. " + e.getMessage());
				e.printStackTrace();
				jsonColaborador.append("status", 0L);
				jsonColaborador.append("msg", "Não foi possível alterar o e-mail, erro interno.");
				out.print(jsonColaborador);

			}
			finally
			{
				try
				{
					connVetorh.close();
				}
				catch (SQLException e)
				{
					e.printStackTrace();
				}
			}
		}
	
		private void listarDados(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws JSONException
		{
			String sql = "select * from r034fun where numcpf = ? and datafa = cast('1900-12-31 00:00:00' as Datetime)  and caudem <> 6";
			JSONObject jsonColaborador = new JSONObject();
			
			
			
			try{
				Long numcpf = Long.parseLong(request.getParameter("codigo"));
				
				String sessionId = request.getParameter("sessionId");
				SiteUtils siteUtils = new SiteUtils();
				
				NeoObject session = siteUtils.getSession(sessionId);
				
				if (session == null)
				{

					jsonColaborador.append("status", 2L);
					jsonColaborador.append("msg", "Sessão inexistente");
					out.print(jsonColaborador);
					return;
				}
				
				
				Connection connVetorh = PersistEngine.getConnection("VETORH");
				
				PreparedStatement pst = connVetorh.prepareStatement(sql);
				pst.setLong(1, numcpf);
				
				ResultSet rs = pst.executeQuery();
				
				String emapon = "";
				while (rs.next()){
					emapon = rs.getString("usu_emapon");
				}
				rs.close();
				rs = null;
				
				
				JSONObject jsonDados = new JSONObject();
				jsonDados.append("email", NeoUtils.safeOutputString(emapon) );
				out.print(jsonDados);
				
				
				if (connVetorh != null){
					connVetorh.close();
				}
				
			}catch(Exception e){
				
				System.out.println("[AREA COLABORADOR] - Erro ao listar e-mail. " + e.getMessage());
				e.printStackTrace();
				jsonColaborador.append("status", 0L);
				jsonColaborador.append("msg", "Não foi possível listar o e-mail do colaborador, erro interno.");
				out.print(jsonColaborador);
			}
			
		}
		
		
		private void listaHistoricoDisciplinar(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws JSONException
		{
			response.setHeader("Access-Control-Allow-Origin", "*");
	        response.setHeader("Access-Control-Allow-Methods", "POST");
	        response.setHeader("Access-Control-Allow-Headers", "Content-Type");
	        response.setHeader("Access-Control-Max-Age", "86400");
	        response.setContentType("application/json");
			JSONObject jsonRetorno = new JSONObject();
			
			
			
			try{
				String numcpf = request.getParameter("numcpf");

				String sessionId = request.getParameter("sessionId");
				SiteUtils siteUtils = new SiteUtils();
				
				NeoObject session = siteUtils.getSession(sessionId);
				
				if (session == null)
				{

					jsonRetorno.append("status", 2L);
					jsonRetorno.append("msg", "Sessão inexistente");
					out.print(jsonRetorno);
					return;
				}
				
				EntityWrapper sessionWrapper = new EntityWrapper(session);
				Long numCpf = (Long) sessionWrapper.getValue("codigo");
		        
				List<SiteAnotacaoColaboradorVO> anotacoes = new ArrayList<SiteAnotacaoColaboradorVO>();
				anotacoes = SiteUtils.listaHistoricoDisciplinar(NeoUtils.safeOutputString(numCpf));
				
				Gson gson = new Gson();
				String jsonAnotacoes = gson.toJson(anotacoes);
				System.out.println(jsonAnotacoes);
				out.print(jsonAnotacoes);
				out.flush();
				out.close();
				
				
				
			}catch(Exception e){
				
				System.out.println("[AREA COLABORADOR] - Erro ao listar e-mail. " + e.getMessage());
				e.printStackTrace();
				jsonRetorno.append("status", 0L);
				jsonRetorno.append("msg", "Não foi possível as anotações do colaborador, erro interno.");
				out.print(jsonRetorno);
			}
			
		}
		
		public static void listaDeDocumentosPunicoes(HttpServletRequest request, HttpServletResponse response, PrintWriter out){
			String sessionId = request.getParameter("sessionId");
			SiteUtils siteUtils = new SiteUtils();

			// buscar Sesssao
			NeoObject session = siteUtils.getSession(sessionId);
			EntityWrapper wSession = new EntityWrapper(session);
			
			String numcpf = String.valueOf(wSession.getValue("codigo"));
			
			List<DocumentoTreeVO> documentos = new ArrayList<DocumentoTreeVO>();
			documentos = listaDocumentoPunicoes(Long.parseLong(numcpf));
			Gson gson = new Gson();
			String jsonDocumentos = gson.toJson(documentos);
			
			System.out.println("jsonDocumentos: "+jsonDocumentos);

			out.print(jsonDocumentos);
			out.flush();
			out.close();
		}
		
		public static List<DocumentoTreeVO> listaDocumentoPunicoes(Long cpf)
		{
				List<DocumentoTreeVO> documentos = new ArrayList<DocumentoTreeVO>();
				List<NeoObject> colaboradores = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("GEDRHDADOSCOLABORADOR"), new QLEqualsFilter("numeroCPF", cpf), -1, -1, " _vo.neoObjectType.neoType, creationDate desc ");

				E2docHandler e2docHandler = new E2docHandler();
				documentos.addAll(e2docHandler.documentosFuncionarioPresenca(cpf.toString()));

				if (NeoUtils.safeIsNotNull(colaboradores))
				{
					log.debug("<br>tamanho da colaboradores: " + colaboradores.size());
					DocumentoTreeVO documentoAnt = new DocumentoTreeVO();
					for (NeoObject col : colaboradores)
					{
						log.debug("<br>neoid do colaborador:" + col.getNeoId());
						EntityWrapper colaboradorWrapper = new EntityWrapper(col);
						col = PersistEngine.reload(col);
						String tipoDoc = String.valueOf(colaboradorWrapper.getValue("code"));
							DocumentEntityInfo docInfo = (DocumentEntityInfo) colaboradorWrapper.findValue("docInfo");
							List<NeoObject> arquivos = (List<NeoObject>) colaboradorWrapper.findValue("arquivos");

							log.debug("<br>tamanho do arquivos:" + arquivos.size());
							Long neoid = (Long) colaboradorWrapper.findValue("neoId");

							DocumentoTreeVO documento = new DocumentoTreeVO();
							documento.setId(neoid);
							documento.setNome(docInfo.getTitle());
							documento.setState("closed");

							List<DocumentoTreeVO> childrens = new ArrayList<DocumentoTreeVO>();

							if (NeoUtils.safeIsNotNull(arquivos))
							{
								for (NeoObject arq : arquivos)
								{

									EntityWrapper arquivoWrapper = new EntityWrapper(arq);
									NeoFile file = (NeoFile) arquivoWrapper.findValue("arquivo");
									Long fileNeoId = (Long) arquivoWrapper.findValue("neoId");
									log.debug("<br>NeoId do arquivo: " + fileNeoId);

									Long mes = (Long) arquivoWrapper.findValue("mes");
									Long ano = (Long) arquivoWrapper.findValue("ano");
									Long id = (Long) arquivoWrapper.findValue("neoId");

									GregorianCalendar gData = new GregorianCalendar();

									String competencia = "";

									if (mes != null && mes != 0 && ano != null && ano != 0)
									{
										competencia = String.format("%02d", mes) + "/" + ano.toString();
										gData = new GregorianCalendar(ano.intValue(), mes.intValue() - 1, 1);
									}
									else
									{
										gData = new GregorianCalendar(1900, 0, 1);
									}
									DocumentoTreeVO children = new DocumentoTreeVO();
									children.setId(id);
									children.setNome(file.getName() + "." + file.getSufix().toLowerCase());
									children.setData(competencia);
									children.setgData(gData);
									children.setLink(OrsegupsUtils.getLink(file));
									childrens.add(children);
								}
							}
							documento.setChildren(childrens);
							if (documentoAnt != null && documentoAnt.getNome() != null && documentoAnt.getNome().equals(documento.getNome()))
							{
								documentoAnt.getChildren().addAll(documento.getChildren());
							}
							else
							{
								documentos.add(documento);
							}
							documentoAnt = documento;
						}
				}

				List<DocumentoTreeVO> listaVisualizar = new ArrayList<DocumentoTreeVO>();
				
				for (DocumentoTreeVO doc : documentos)
				{
					if (doc != null && doc.getNome().equalsIgnoreCase("[GED/RH] Histórico Disciplinar"))
					{
						listaVisualizar.add(doc);
					}
				}
				
				
				for (DocumentoTreeVO doc : listaVisualizar)
				{
					
					Collections.sort(doc.getChildren(), new DocumentoComparator());
				}

				return listaVisualizar;
		}
		
		public static void downloadDocPunicao(Long neoId,HttpServletResponse response){
			OutputStream out = null;
			try
			{
				String sContentType = "application/octet-stream";
				response.setHeader("Content-Disposition","inline; filename=\"myfile.txt\""); 
				response.setContentType(sContentType);
				
				InputStream in = null;
				NeoFile neoFile = (NeoFile) PersistEngine.getObject(NeoFile.class, new QLEqualsFilter("neoId", neoId));
				response.setHeader("Content-Disposition","inline; filename=\""+neoFile.getName()+"\""); 
				File fileRelatorio = neoFile.getAsFile();		
				
				in = new BufferedInputStream(new FileInputStream(fileRelatorio));
				if (fileRelatorio.exists())
				{
					response.setContentLength((int) in.available());
					out = response.getOutputStream();
					int l;
					byte b[] = new byte[1024];
					while ((l = in.read(b, 0, b.length)) != -1)
					{
						out.write(b, 0, l);
					}
					out.flush();
					out.close();
					in.close();
				}
				else
				{
					System.out.println("[Area ColaboradorServlet]-Download de Arquivo de ficha ponto invalido");
				}
			}
			catch (Exception e)
			{
				System.out.println("[Area ClientServlet]- Erro Baixando o arquivo de ficha ponto");
				e.printStackTrace();
			}
		}
		
		public void pontosDia(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws JSONException {
			Connection conn = null;
			PreparedStatement pst = null;
			ResultSet rs = null;
			StringBuilder sql = new StringBuilder();
			String retorno = "";
			String nomFun = "";
			String empresa = request.getParameter("empresa");
			String matricula = request.getParameter("matricula");
			
			
			if(empresa != null && matricula != null){
				
				if (empresa.length() < 2) {
					empresa = "0" + empresa;
				}
	
				for (int i = 0; i < 8; i++) {
					if (matricula.length() < 8) {
						matricula = "0" + matricula;
					} else {
						break;
					}
				}
				Long codEmp = Long.parseLong(empresa);
				Long numCad = Long.parseLong(matricula);
				
				sql.append(" SELECT f.nomfun,HORACC/60 AS HORA, CAST(ROUND(((HORACC/60.0) - (HORACC/60)) * 60,2 ) AS INT) AS MINUTOS "
						+ " FROM r070acc acc"
				+ "  inner join r034fun f on f.numcad = "+numCad+" and f.numemp = "+codEmp
				+ " WHERE acc.numcra =  '10" + empresa + matricula+"' "
				+ " AND acc.datacc >= (DATEADD(DAY, DATEDIFF(DAY, 0, getdate()), 0)) AND acc.tipacc >= 100 order by acc.HORACC");
				try {
					conn = PersistEngine.getConnection("VETORH");
					pst = conn.prepareStatement(sql.toString());
					rs = pst.executeQuery();
	
					while (rs.next()) {
						nomFun = rs.getString("nomfun");
						String hora = rs.getString("HORA");
						String minutos = rs.getString("MINUTOS");
	
						if (hora != null && !hora.isEmpty()) {
							if (Integer.parseInt(hora) < 10) {
								hora = "0" + hora;
							}
						} else {
							hora = "00";
						}
	
						if (minutos != null && !minutos.isEmpty()) {
							if (Integer.parseInt(minutos) < 10) {
								minutos = "0" + minutos;
							}
						} else {
							minutos = "00";
						}
	
						retorno = retorno + hora + ":" + minutos + "  ";
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if(retorno.equals("")){
				retorno = "Nenhum registro encontrado!";
			}
			JSONObject jsonRetorno = new JSONObject();
			jsonRetorno.append("retorno", retorno);
			jsonRetorno.append("nomFun", nomFun);
			System.out.println("retorno: "+jsonRetorno.toString());
			out.print(jsonRetorno.toString());
			out.flush();
			out.close();
		}
		
		public void pagamentoMes(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws JSONException {
			Connection conn = null;
			PreparedStatement pst = null;
			ResultSet rs = null;
			StringBuilder sql = new StringBuilder();
			String dataPagamento = "";
			double valorPago = 0L;
			String empresa = request.getParameter("empresa");
			String matricula = request.getParameter("matricula");
			
			
			if(empresa != null && matricula != null){
				Long codEmp = Long.parseLong(empresa);
				Long numCad = Long.parseLong(matricula);
				
				sql.append(" SELECT convert(varchar,DatPag,103) as dataPagamento,ValPag FROM R046PAG WHERE NUMCAD = ? AND NUMEMP = ? AND DatPag >= GETDATE()-26");
				try {
					conn = PersistEngine.getConnection("VETORH");
					pst = conn.prepareStatement(sql.toString());
					pst.setLong(1, numCad);
					pst.setLong(2, codEmp);
					rs = pst.executeQuery();
					
	
					while (rs.next()) {
						dataPagamento = rs.getString("dataPagamento");
						valorPago = rs.getDouble("ValPag");
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			JSONObject jsonRetorno = new JSONObject();
			jsonRetorno.append("dataPagemento", dataPagamento);
			jsonRetorno.append("valorPago", valorPago);
			out.print(jsonRetorno.toString());
			out.flush();
			out.close();
		}
}
