package com.neomind.fusion.custom.orsegups.site.vo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;



public class SiteContraChequeColaboradorVO {
	
	
	private String titCar;
	private String datAdm;
	private String perRef;
	private String txtBan;
	private String txtAge;
	private String txtCon;
	private String salBas;
	private String basFgt;
	private String salIns;
	private String basIrf;
	private String fgtMes;
	private String totVct;
	private String totDsc;
	private String vlrLiq;
	private String msgChq;
	private GregorianCalendar perRefDate;
	private Long mostraSite;
	
	private Collection<SiteEventosColaboradorVO> eventosColaboradorVOs = new ArrayList();

	public String getTitCar()
	{
		return titCar;
	}

	public void setTitCar(String titCar)
	{
		this.titCar = titCar;
	}

	public String getDatAdm()
	{
		return datAdm;
	}

	public void setDatAdm(String datAdm)
	{
		this.datAdm = datAdm;
	}

	public String getPerRef()
	{
		return perRef;
	}

	public void setPerRef(String perRef)
	{
		this.perRef = perRef;
	}

	public String getTxtBan()
	{
		return txtBan;
	}

	public void setTxtBan(String txtBan)
	{
		this.txtBan = txtBan;
	}

	public String getTxtAge()
	{
		return txtAge;
	}

	public void setTxtAge(String txtAge)
	{
		this.txtAge = txtAge;
	}

	public String getTxtCon()
	{
		return txtCon;
	}

	public void setTxtCon(String txtCon)
	{
		this.txtCon = txtCon;
	}

	public String getSalBas()
	{
		return salBas;
	}

	public void setSalBas(String salBas)
	{
		this.salBas = salBas;
	}

	public String getBasFgt()
	{
		return basFgt;
	}

	public void setBasFgt(String basFgt)
	{
		this.basFgt = basFgt;
	}

	public String getSalIns()
	{
		return salIns;
	}

	public void setSalIns(String salIns)
	{
		this.salIns = salIns;
	}

	public String getBasIrf()
	{
		return basIrf;
	}

	public void setBasIrf(String basIrf)
	{
		this.basIrf = basIrf;
	}

	public String getFgtMes()
	{
		return fgtMes;
	}

	public void setFgtMes(String fgtMes)
	{
		this.fgtMes = fgtMes;
	}

	public String getTotVct()
	{
		return totVct;
	}

	public void setTotVct(String totVct)
	{
		this.totVct = totVct;
	}

	public String getTotDsc()
	{
		return totDsc;
	}

	public void setTotDsc(String totDsc)
	{
		this.totDsc = totDsc;
	}

	public String getVlrLiq()
	{
		return vlrLiq;
	}

	public void setVlrLiq(String vlrLiq)
	{
		this.vlrLiq = vlrLiq;
	}

	public String getMsgChq()
	{
		return msgChq;
	}

	public void setMsgChq(String msgChq)
	{
		this.msgChq = msgChq;
	}

	public GregorianCalendar getPerRefDate()
	{
		return perRefDate;
	}

	public void setPerRefDate(GregorianCalendar perRefDate)
	{
		this.perRefDate = perRefDate;
	}

	public Long getMostraSite()
	{
		return mostraSite;
	}

	public void setMostraSite(Long mostraSite)
	{
		this.mostraSite = mostraSite;
	}

	public Collection<SiteEventosColaboradorVO> getEventosColaboradorVOs()
	{
		return eventosColaboradorVOs;
	}

	public void setEventosColaboradorVOs(Collection<SiteEventosColaboradorVO> eventosColaboradorVOs)
	{
		this.eventosColaboradorVOs = eventosColaboradorVOs;
	}

	@Override
	public String toString()
	{
		return "SiteContraChequeColaboradorVO [titCar=" + titCar + ", datAdm=" + datAdm + ", perRef=" + perRef + ", txtBan=" + txtBan + ", txtAge=" + txtAge + ", txtCon=" + txtCon + ", salBas=" + salBas + ", basFgt=" + basFgt + ", salIns=" + salIns + ", basIrf=" + basIrf + ", fgtMes=" + fgtMes + ", totVct=" + totVct + ", totDsc=" + totDsc + ", vlrLiq=" + vlrLiq + ", msgChq=" + msgChq + ", perRefDate=" + perRefDate + ", mostraSite=" + mostraSite + ", eventosColaboradorVOs="
				+ eventosColaboradorVOs + "]";
	}
	
	
	
	
	
	
}
