 package com.neomind.fusion.custom.orsegups.scheduler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Authenticator;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeBodyPart;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.HtmlEmail;
import org.jsoup.Jsoup;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.EmailConfigVO;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoCalendarUtils;
import com.neomind.util.NeoDateUtils;

public class RotinaAbreTarefaSimplesEmail implements CustomJobAdapter
{

	private static final Log log = LogFactory.getLog(RotinaAbreTarefaSimplesEmail.class);

	private Pattern pattern;
	private Matcher matcher;
	private final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	/**
	 * Método para abrir tarefas a partir do e-mail
	 * 
	 * quando um e-mail form enviado para tarefasimples@orsegups.com.br com o título no padrão XXXXXX
	 * [dd/MM/yyyy]
	 * 
	 * o mesmo vai pegar todos os envolvidos no e-mail pesquisar no sistema fusion os usuário do e-mail e
	 * abrir uma tarefa simples
	 * 
	 * O prazo é definido no título do e-mail
	 * 
	 * @author William Bastos
	 * 
	 * @param CustomJobContext arg0
	 * 
	 * @return void
	 */

	@Override
	public void execute(CustomJobContext arg0)
	{

		Folder folder = null;
		Store store = null;
		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		try
		{
			EmailConfigVO emailConfigVO = getConfigEmailTarefa();
			final String user = emailConfigVO.getUserName();
			final String password = emailConfigVO.getPassword();
			final String host = emailConfigVO.getHost();

			if (emailConfigVO != null)
			{
				pattern = Pattern.compile(EMAIL_PATTERN);
				Properties props = System.getProperties();

				props.setProperty("mail.pop3s.rsetbeforequit", "true");
				props.setProperty("mail.pop3.rsetbeforequit", "true");
				props.setProperty("mail.pop3.connectiontimeout", "20000");

				Session session = Session.getInstance(props, new Authenticator()
				{
					@Override
					protected PasswordAuthentication getPasswordAuthentication()
					{
						return new PasswordAuthentication(user, password);

					}
				});

				store = session.getStore("pop3s");
							
				store = this.storeConnect(store, host, user, password);
				
				while (!store.isConnected()){
				    store = this.storeConnect(store, host, user, password);
				}
			
				System.out.print("Conectado ao servidor IMAP...");
				folder = store.getDefaultFolder().getFolder("INBOX");
				folder.open(Folder.READ_WRITE);

				Message[] messages = folder.getMessages();

				System.out.println("Quantidade de E-mails: " + folder.getMessageCount());
				System.out.println("No of Unread Messages : " + folder.getUnreadMessageCount());

				Set<String> toList = null;
				Set<String> fromList = null;

				HashMap<String, Set<String>> mapGrupoEmail = verificaGrupoEmail();

				for (int i = 0; i < messages.length; ++i)
				{

					Message msg = messages[i];

					String titulo = "unknown";

					if (msg.getReplyTo() != null && msg.getReplyTo().length >= 1)
					{
						titulo = msg.getSubject();
						toList = new TreeSet<String>();
						for (int j = 0; j < msg.getReplyTo().length; j++)
						{
							if (verificaEmailMsgReplyTo(msg, j))
							{
								String email = recuperaEmail(msg.getReplyTo()[j].toString());

								matcher = pattern.matcher(email);

								if (matcher.matches())
								{

									toList.add(email);

								}
							}
						}
						if (toList == null || (toList != null && toList.isEmpty()))
						{
							continue;
						}
					}
					else
					{
						continue;
					}

					if (msg.getAllRecipients() != null && msg.getAllRecipients().length >= 1)
					{
						fromList = new TreeSet<String>();
						for (int j = 0; j < msg.getAllRecipients().length; j++)
						{
							if (verificaEmailMsgAllRecipients(msg, j))
							{

								String email = recuperaEmail(msg.getAllRecipients()[j].toString());

								matcher = pattern.matcher(email);

								if (matcher.matches())
								{

									if (mapGrupoEmail != null && !mapGrupoEmail.isEmpty() && mapGrupoEmail.containsKey(email))
									{
										fromList.addAll(mapGrupoEmail.get(email));
									}
									else
									{
										fromList.add(email);
									}
								}
							}

						}
						if (msg.getReplyTo() != null && msg.getReplyTo().length >= 1 && (fromList == null || (fromList != null && fromList.isEmpty())))
						{

							String email = recuperaEmail(msg.getReplyTo()[0].toString());

							String tituloMail = "Erro ao abrir tarefa via e-mail";
							enviaEmail("Atenção. Não foi possível abrir a tarefa <strong>" + titulo + "</strong>. Lista de destinatários contém erros ou está vazia. <br>", "fusion@orsegups.com.br", email, tituloMail);
							msg.setFlag(Flags.Flag.DELETED, true);
							continue;
						}
					}
					else
					{
						String email = recuperaEmail(msg.getReplyTo()[0].toString());

						String tituloMail = "Erro ao abrir tarefa via e-mail";
						enviaEmail("Atenção. Não foi possível abrir a tarefa <strong>" + titulo + "</strong>. Lista de destinatários contém erros ou está vazia. <br>", "fusion@orsegups.com.br", email, tituloMail);
						msg.setFlag(Flags.Flag.DELETED, true);
						continue;
					}

					String value = new String(titulo.getBytes(), "UTF-8");
					System.out.println("Titulo " + value);
					if (titulo != null && verificaTitulo(titulo) != null && !verificaTitulo(titulo).isEmpty() && !verificaTitulo(titulo).contains("Título inválido.") && verificaTitulo(titulo).contains(";") && !titulo.contains("RES:"))
					{
						String tituloAux = verificaTitulo(titulo);
						String arrayAux[] = tituloAux.split(";");
						if (arrayAux != null && arrayAux.length >= 1)
						{
							if (validaPrazo(arrayAux[1]) == null)
							{
								String email = recuperaEmail(msg.getReplyTo()[0].toString());

								String tituloMail = "Erro ao abrir tarefa via e-mail";
								enviaEmail("Atenção. Não foi possível abrir a tarefa <strong>" + titulo + "</strong>. Prazo inválido. <br>", "fusion@orsegups.com.br", email, tituloMail);
								msg.setFlag(Flags.Flag.DELETED, true);

							}
							else
							{
								String email = recuperaEmail(msg.getReplyTo()[0].toString());
								for (String emailFrom : fromList)
								{

									System.out.println("Carregando informações para abertura da tarefa ... " + titulo + " para " + emailFrom + " de " + email);
									String filename = "c:/mail/" + new GregorianCalendar().getTimeInMillis();
									String from = recuperaEmail(emailFrom);
									String to = recuperaEmail(email);

									if (from != null && !from.isEmpty() && from.contains("@orsegups.com.br") && to != null && !to.isEmpty() && !to.contains("tarefasimples@orsegups.com.br"))
									{
										String solicitanteTarefa = getCodeUsuario(to);
										String executorTarefa = getCodeUsuario(from);

										if (solicitanteTarefa != null && !solicitanteTarefa.isEmpty() && executorTarefa != null && !executorTarefa.isEmpty())
										{
											if (msg.getContent() instanceof Multipart)
											{
												Multipart multi = ((Multipart) msg.getContent());
												MimeBodyPart part = (MimeBodyPart) multi.getBodyPart(0);

												String html = getText(part);
												String txt = htmlToText(html);
												if (txt != null && !txt.isEmpty() && txt.length() <= 7500)
												{
													saveParts(msg.getContent(), filename, from, to, titulo, msg.getSentDate());
												}
												else
												{
													String tituloMail = "Erro ao abrir tarefa via e-mail";
													enviaEmail("Atenção. Não foi possível abrir a tarefa <strong>" + titulo + "</strong>, limite do campo texto para abertura da tarefa foi excedido ou não possui não possui texto.  <br>", "fusion@orsegups.com.br", to, tituloMail);
													msg.setFlag(Flags.Flag.DELETED, true);
												}
											}
											else if (msg.getContent() instanceof String)
											{
												String txt = htmlToText((String)msg.getContent());
												if (txt != null && !txt.isEmpty() && txt.length() <= 7500)
												{
													abreTarefa(to, from, titulo, txt, msg.getSentDate());
												}
												else
												{
													String tituloMail = "Erro ao abrir tarefa via e-mail";
													enviaEmail("Atenção. Não foi possível abrir a tarefa <strong>" + titulo + "</strong>, limite do campo texto para abertura da tarefa foi excedido ou não possui não possui texto.  <br>", "fusion@orsegups.com.br", to, tituloMail);
													msg.setFlag(Flags.Flag.DELETED, true);
												}
											}
											else
											{
												String tituloMail = "Erro ao abrir tarefa via e-mail";
												enviaEmail("Atenção. Não foi possível abrir a tarefa <strong>" + titulo + "</strong>, formato do e-mail é inválido.  <br>", "fusion@orsegups.com.br", to, tituloMail);
												msg.setFlag(Flags.Flag.DELETED, true);
											}
										}
										else
										{
											String tituloMail = "Erro ao abrir tarefa via e-mail";
											enviaEmail("Atenção. Não foi possível abrir a tarefa <strong>" + titulo + "</strong>, usuário ou solicitante não encontrado.  <br>", "fusion@orsegups.com.br", to, tituloMail);
											msg.setFlag(Flags.Flag.DELETED, true);
										}
									}
								}
								msg.setFlag(Flags.Flag.DELETED, true);
							}
						}
					}
					else
					{
						String email = recuperaEmail(msg.getReplyTo()[0].toString());

						String tituloMail = "Erro ao abrir tarefa via e-mail";
						enviaEmail("Atenção! não foi possível abrir a tarefa <strong>" + titulo + "</strong>. Título inválido. <br>", "fusion@orsegups.com.br", email, tituloMail);
						msg.setFlag(Flags.Flag.DELETED, true);
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("[" + key + "] Tarefa via E-Mail erro no processamento." + e.getMessage());
			log.error("Tarefa via E-Mail erro no processamento.");
			throw new JobException("Erro no processamento. Procurar no log por [" + key + "] " + e.getMessage());
		}
		finally
		{
			if (folder != null)
			{
				try
				{
					folder.close(true);
				}
				catch (MessagingException e)
				{
					log.error("MessagingException", e);
				}
			}
			if (store != null)
			{
				try
				{
					store.close();
				}
				catch (MessagingException e)
				{
					log.error("MessagingException", e);
				}
			}
		}
	}

    private Store storeConnect(Store store, String host, String user, String password) {

	try {
	    store.connect(host, user, password);
	} catch (Exception e) {
	    e.printStackTrace();
	}
	
	return store;
    }

	/**
	 * Método que trata o e-mail para transformar o mesmo em uma tarefa
	 * 
	 * @param content
	 * @param filename
	 * @param from
	 * @param to
	 * @param titulo
	 * @param date
	 * @throws IOException
	 * @throws MessagingException
	 */
	public void saveParts(Object content, String filename, String from, String to, String titulo, Date date) throws IOException, MessagingException
	{
		OutputStream out = null;
		InputStream in = null;
		try
		{
			if (content instanceof Multipart)
			{
				Multipart multi = ((Multipart) content);
				MimeBodyPart part = (MimeBodyPart) multi.getBodyPart(0);

				if (part.getContent() instanceof Multipart)
				{
					saveParts(part.getContent(), filename, from, to, titulo, date);
				}
				else
				{
					String html = getText(part);
					abreTarefa(to, from, titulo, html, date);
				}
			}
		}
		catch (Exception e)
		{
			log.error("Exception", e);
			String tituloMail = "Erro ao abrir tarefa via e-mail";
			enviaEmail("Atenção! Não foi possível abrir a tarefa <strong>" + titulo + "</strong>, ocorreu uma exceção ao abrir a tarefa.  <br>", "fusion@orsegups.com.br", to, tituloMail);
		}
		finally
		{
			if (in != null)
			{
				in.close();
			}
			if (out != null)
			{
				out.flush();
				out.close();
			}
		}
	}

	public void abreTarefa(String solicitante, String executor, String titulo, String conteudo, Date date)
	{
		try
		{
			String txt = htmlToText(conteudo);
			String emailFrom = executor;
			String emailTo = solicitante;

			matcher = pattern.matcher(emailFrom);
			matcher = pattern.matcher(emailTo);
			if (matcher.matches())
			{

				String tituloAux = verificaTitulo(titulo);
				String tituloPrazoArray[] = tituloAux.split(";");
				String solicitanteTarefa = getCodeUsuario(emailTo);
				String executorTarefa = getCodeUsuario(emailFrom);

				GregorianCalendar prazo = new GregorianCalendar();
				prazo = validaPrazo(tituloPrazoArray[1]);
				prazo.set(Calendar.HOUR_OF_DAY, 23);
				prazo.set(Calendar.MINUTE, 59);
				prazo.set(Calendar.SECOND, 59);
				IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
				String tarefa = iniciarTarefaSimples.abrirTarefa(solicitanteTarefa, executorTarefa, titulo, txt, "1", "sim", prazo);

				if (tarefa != null && !tarefa.isEmpty())
				{
					setHistorico(tarefa, titulo, new GregorianCalendar(), solicitanteTarefa, executorTarefa, emailTo, emailFrom, date);
					String tituloMail = "Sucesso ao abrir tarefa via e-mail";
					enviaEmail("Atenção. Tarefa <strong>" + titulo + "</strong> aberta com sucesso. Código: " + tarefa + " <br>", "fusion@orsegups.com.br", emailTo, tituloMail);
					System.out.print("Tarefa aberta via e-mail: " + titulo + " código: " + tarefa + " solicitante: " + solicitanteTarefa + " executor: " + executorTarefa);

				}
				else
				{
					String tituloMail = "Erro ao abrir tarefa via e-mail";
					enviaEmail("Atenção. Não foi possível abrir a tarefa <strong>" + titulo + "</strong> devido a algum problema durante a execução da mesma. <br>", emailTo, emailFrom, tituloMail);
				}
			}
		}
		catch (Exception e)
		{
			log.error("Exception", e);
			String tituloMail = "Erro ao abrir tarefa via e-mail";
			enviaEmail("Atenção! Não foi possível abrir a tarefa <strong>" + titulo + "</strong>, ocorreu uma exceção ao abrir a tarefa.  <br>", "fusion@orsegups.com.br", solicitante, tituloMail);
		}
	}

	/**
	 * Método que transforma html em texto
	 * 
	 * @param p
	 * @return
	 * @throws MessagingException
	 * @throws IOException
	 */
	public static String getText(Part p) throws MessagingException, IOException
	{
		if (p.isMimeType("text/*"))
		{
			String s = (String) p.getContent();
			//textIsHtml = p.isMimeType("text/html");
			return s;
		}

		if (p.isMimeType("multipart/alternative"))
		{
			// prefer html text over plain text
			Multipart mp = (Multipart) p.getContent();
			String text = null;
			for (int i = 0; i < mp.getCount(); i++)
			{
				Part bp = mp.getBodyPart(i);
				if (bp.isMimeType("text/plain"))
				{
					if (text == null)
						text = getText(bp);
					continue;
				}
				else if (bp.isMimeType("text/html"))
				{
					String s = getText(bp);
					if (s != null)
						return s;
				}
				else
				{
					return getText(bp);
				}
			}
			return text;
		}
		else if (p.isMimeType("multipart/*"))
		{
			Multipart mp = (Multipart) p.getContent();
			for (int i = 0; i < mp.getCount(); i++)
			{
				String s = getText(mp.getBodyPart(i));
				if (s != null)
					return s;
			}
		}

		return null;
	}

	/**
	 * Método que transforma html em texto
	 * 
	 * @param html
	 * @return
	 */
	public static String htmlToText(String html)
	{
		return html != null ? Jsoup.parse(html).text() : "";
	}

	/**
	 * Método que retorna código do usuário do sistema fusion
	 * 
	 * @param email
	 * @return
	 */
	private String getCodeUsuario(String email)
	{
		Connection conn = null;
		PreparedStatement preparedStatementHSelect = null;
		ResultSet rsH = null;
		String usuario = null;

		try
		{
			conn = PersistEngine.getConnection("FUSIONPROD");
			StringBuilder sqlSigma = new StringBuilder();
			sqlSigma.append("  SELECT S.code FROM NEOUSER U WITH(NOLOCK) ");
			sqlSigma.append("  INNER JOIN SecurityEntity S WITH(NOLOCK) ON S.neoId = U.neoId ");
			sqlSigma.append("  WHERE U.email LIKE ? AND U.email IS NOT NULL AND S.active = 1 ");

			preparedStatementHSelect = conn.prepareStatement(sqlSigma.toString());
			preparedStatementHSelect.setString(1, email);
			rsH = preparedStatementHSelect.executeQuery();

			if (rsH.next())
			{
				usuario = rsH.getString("code");
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			OrsegupsUtils.closeConnection(conn, preparedStatementHSelect, rsH);
		}

		return usuario;
	}

	/**
	 * Método para verificar se o e-mail não possue exceção
	 * 
	 * @param msg
	 * @param j
	 * @return
	 */
	private boolean verificaEmailMsgReplyTo(Message msg, int j)
	{
		boolean retorno = true;
		try
		{
			if (msg.getReplyTo()[j] == null)
				return false;
			if (msg.getReplyTo()[j].toString() == null)
				return false;
			if (msg.getReplyTo()[j].toString().isEmpty())
				return false;
			if (!msg.getReplyTo()[j].toString().contains("@orsegups.com.br"))
				return false;
			if (msg.getReplyTo()[j].toString().contains("tarefasimples@orsegups.com.br"))
				return false;

		}
		catch (Exception e)
		{
			retorno = false;
		}
		return retorno;
	}

	/**
	 * Método para verificar se o e-mail não possue exceção
	 * 
	 * @param msg
	 * @param j
	 * @return
	 */
	private boolean verificaEmailMsgAllRecipients(Message msg, int j)
	{
		boolean retorno = true;
		try
		{
			if (msg.getAllRecipients()[j] == null)
				return false;
			if (msg.getAllRecipients()[j].toString() == null)
				return false;
			if (msg.getAllRecipients()[j].toString().isEmpty())
				return false;
			if (!msg.getAllRecipients()[j].toString().contains("@orsegups.com.br"))
				return false;
			if (msg.getAllRecipients()[j].toString().contains("tarefasimples@orsegups.com.br"))
				return false;

		}
		catch (Exception e)
		{
			retorno = false;
		}
		return retorno;
	}

	/**
	 * Método para validar o título do e-mail
	 * 
	 * @param msg
	 * @return
	 */
	private String verificaTitulo(String msg)
	{
		String retorno = "";
		try
		{
			String titulo = msg.substring(0, msg.indexOf("["));
			String data = msg.substring(msg.indexOf("[") + 1, msg.indexOf("[") + 11);

			retorno = titulo + ";" + data;
		}
		catch (Exception e)
		{
			retorno = "Título inválido.";
		}
		return retorno;
	}

	/**
	 * Método recuperar e-mail do html
	 * 
	 * @param email
	 * @return
	 */
	private String recuperaEmail(String email)
	{
		String retorno = "";
		try
		{

			if (email.contains("<") && email.contains(">"))
			{
				retorno = email.toString().substring(email.indexOf("<") + 1, email.indexOf(">"));
			}
			else
			{
				retorno = email;
			}
		}
		catch (Exception e)
		{
			System.out.println("E-mail inválido");
		}
		return retorno;
	}

	/**
	 * Método para validar o prazo informado no e-mail
	 * 
	 * @param data
	 * @return
	 */
	private GregorianCalendar validaPrazo(String data)
	{
		GregorianCalendar prazo = null;
		try
		{
			GregorianCalendar dataAtual = new GregorianCalendar(new GregorianCalendar().get(1), new GregorianCalendar().get(2), new GregorianCalendar().get(5), 23, 59, 59);
			prazo = (GregorianCalendar) NeoCalendarUtils.stringToDate(data);

			if (OrsegupsUtils.isWorkDay(prazo))
			{
				prazo.set(Calendar.HOUR_OF_DAY, 23);
				prazo.set(Calendar.MINUTE, 59);
				prazo.set(Calendar.SECOND, 59);
			}
			if (!OrsegupsUtils.isWorkDay(prazo))
			{
				System.out.println("Prazo informado deve ter um dia útil!");
				prazo = null;
			}

			if ((prazo.before(dataAtual)) || (prazo.equals(dataAtual)))
			{
				System.out.println("Prazo informado deve ser de pelo menos 1 dia!");
				prazo = null;
			}

		}
		catch (Exception e)
		{
			System.out.println("Data inválida.");
		}
		return prazo;
	}

	/**
	 * Método para buscar as configurções para acessar o e-mail
	 * 
	 * @param
	 * @return
	 */
	private EmailConfigVO getConfigEmailTarefa()
	{

		EmailConfigVO emailConfigVO = null;
		try
		{
			Collection<NeoObject> configKey = null;

			configKey = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("EmailTarefaConfiguracoes"));

			if (configKey != null && !configKey.isEmpty())
			{

				for (NeoObject neoObject : configKey)
				{
					emailConfigVO = new EmailConfigVO();

					EntityWrapper keyObj = new EntityWrapper(neoObject);

					String host = (String) keyObj.getValue("host");
					String userName = (String) keyObj.getValue("userName");
					String password = (String) keyObj.getValue("password");
					String smtpport = (String) keyObj.getValue("smtpport");
					String protocol = (String) keyObj.getValue("protocol");

					emailConfigVO.setHost(host);
					emailConfigVO.setUserName(userName);
					emailConfigVO.setPassword(password);
					emailConfigVO.setSmtpport(smtpport);
					emailConfigVO.setProtocol(protocol);

				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO AO getConfigE2DOC: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}
		return emailConfigVO;

	}

	/**
	 * Método para enviar o e-mail
	 * 
	 * @param msg
	 * @param to
	 * @param from
	 * @param titulo
	 */
	private void enviaEmail(String msg, String to, String from, String titulo)
	{

		try
		{

			MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

			MailSettings mailClone = new MailSettings();
			mailClone.setMinutesInterval(settings.getMinutesInterval());
			mailClone.setFromEMail(settings.getFromEMail());
			mailClone.setFromName(settings.getFromName());
			mailClone.setRenderServer(settings.getRenderServer());
			mailClone.setSecurityFromEmail(settings.getSecurityFromEmail());
			mailClone.setSecurityCopyEmail(settings.getSecurityCopyEmail());
			mailClone.setSmtpSettings(settings.getSmtpSettings());
			mailClone.setPort(settings.getPort());
			mailClone.setSmtpServer(settings.getSmtpServer());
			mailClone.setEnabled(settings.isEnabled());

			mailClone.setFromEMail("fusion@orsegups.com.br");
			mailClone.setFromName("Orsegups Partipações S.A.");

			StringBuilder stringBuilder = new StringBuilder();
			String descricao = "";
			descricao = descricao + msg;

			stringBuilder.append("<html xmlns:v=\"urn:schemas-microsoft-com:vml\"                                                                                                                                         ");
			stringBuilder.append("xmlns:o=\"urn:schemas-microsoft-com:office:office\"                                                                                                                                     ");
			stringBuilder.append("xmlns:w=\"urn:schemas-microsoft-com:office:word\"                                                                                                                                       ");
			stringBuilder.append("xmlns:m=\"http://schemas.microsoft.com/office/2004/12/omml\"                                                                                                                            ");
			stringBuilder.append("xmlns=\"http://www.w3.org/TR/REC-html40\">                                                                                                                                              ");
			stringBuilder.append("                                                                                                                                                                                      ");
			stringBuilder.append("<head>                                                                                                                                                                                ");
			stringBuilder.append("<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">                                                                                                              ");
			stringBuilder.append("<meta name=ProgId content=Word.Document>                                                                                                                                              ");
			stringBuilder.append("<meta name=Generator content=\"Microsoft Word 14\">                                                                                                                                     ");
			stringBuilder.append("<meta name=Originator content=\"Microsoft Word 14\">                                                                                                                                    ");
			stringBuilder.append("<link rel=File-List                                                                                                                                                                   ");
			stringBuilder.append("href=\"Neomind%20Fusion%20-%20Tarefa%20em%20Aviso%20-%20C023%20-%20FCN%20-%20Cancelamento%20de%20Contrato%20Iniciativa%20eNEW%20-%20016161_arquivos/filelist.xml\">                     ");
			stringBuilder.append("<link rel=Edit-Time-Data                                                                                                                                                              ");
			stringBuilder.append("href=\"Neomind%20Fusion%20-%20Tarefa%20em%20Aviso%20-%20C023%20-%20FCN%20-%20Cancelamento%20de%20Contrato%20Iniciativa%20eNEW%20-%20016161_arquivos/editdata.mso\">                     ");
			stringBuilder.append("                                                                                                                                                                                      ");
			stringBuilder.append("<link rel=Stylesheet type=\"text/css\" media=all                                                                                                                                        ");
			stringBuilder.append("href=\"http://intranet.orsegups.com.br/fusion/css/portal-decorator.css\">                                                                                                               ");
			stringBuilder.append("<style>                                                                                                                                                                               ");
			stringBuilder.append("<!--[if gte mso 10]>                                                                                                                                                                  ");
			stringBuilder.append("<style>                                                                                                                                                                               ");
			stringBuilder.append(" /* Style Definitions */                                                                                                                                                              ");
			stringBuilder.append(" table.MsoNormalTable                                                                                                                                                                 ");
			stringBuilder.append("	{mso-style-name:\"Tabela normal\";                                                                                                                                                    ");
			stringBuilder.append("	mso-tstyle-rowband-size:0;                                                                                                                                                          ");
			stringBuilder.append("	mso-tstyle-colband-size:0;                                                                                                                                                          ");
			stringBuilder.append("	mso-style-noshow:yes;                                                                                                                                                               ");
			stringBuilder.append("	mso-style-priority:99;                                                                                                                                                              ");
			stringBuilder.append("	mso-style-parent:\"\";                                                                                                                                                                ");
			stringBuilder.append("	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;                                                                                                                                                ");
			stringBuilder.append("	mso-para-margin:0cm;                                                                                                                                                                ");
			stringBuilder.append("	mso-para-margin-bottom:.0001pt;                                                                                                                                                     ");
			stringBuilder.append("	mso-pagination:widow-orphan;                                                                                                                                                        ");
			stringBuilder.append("	font-size:10.0pt;                                                                                                                                                                   ");
			stringBuilder.append("	font-family:\"Times New Roman\",\"serif\";}                                                                                                                                             ");
			stringBuilder.append("</style>                                                                                                                                                                              ");
			stringBuilder.append("<![endif]-->                                                                                                                                                                          ");
			stringBuilder.append("</head>                                                                                                                                                                               ");
			stringBuilder.append("                                                                                                                                                                                      ");
			stringBuilder.append("<body lang=PT-BR link=blue vlink=purple style='tab-interval:35.4pt'>                                                                                                                  ");
			stringBuilder.append("                                                                                                                                                                                      ");
			stringBuilder.append("<div class=WordSection1>                                                                                                                                                              ");
			stringBuilder.append("                                                                                                                                                                                      ");
			stringBuilder.append("                                                                                                                                                                                      ");
			stringBuilder.append("                                                                                                                                                                                      ");
			stringBuilder.append("<p class=MsoNormal><o:p>&nbsp;</o:p></p>                                                                                                                                              ");
			stringBuilder.append("                                                                                                                                                                                      ");
			stringBuilder.append("<div align=center>                                                                                                                                                                    ");
			stringBuilder.append("                                                                                                                                                                                      ");
			stringBuilder.append("<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=550                                                                                                            ");
			stringBuilder.append(" style='width:412.5pt;mso-cellspacing:0cm;mso-yfti-tbllook:1184;mso-padding-alt:                                                                                                      ");
			stringBuilder.append(" 0cm 0cm 0cm 0cm'>                                                                                                                                                                    ");
			stringBuilder.append(" <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:54.0pt'>                                                                                                                     ");
			stringBuilder.append("  <td style='padding:0cm 0cm 0cm 0cm;height:54.0pt'>                                                                                                                                  ");
			stringBuilder.append("  <p class=MsoNormal align=center style='text-align:center'><span                                                                                                                     ");
			stringBuilder.append("  style='mso-fareast-font-family:\"Times New Roman\"'><img width=550 height=72                                                                                                          ");
			stringBuilder.append("  id=\"_x0000_i1025\" src=\"http://intranet.orsegups.com.br/fusion/imagens/top_mail.jpg\"                                                                                                 ");
			stringBuilder.append("  alt=\"Neomind Fusion\"><o:p></o:p></span></p>                                                                                                                                         ");
			stringBuilder.append("  </td>                                                                                                                                                                               ");
			stringBuilder.append(" </tr>                                                                                                                                                                                ");
			stringBuilder.append(" <tr style='mso-yfti-irow:1'>                                                                                                                                                         ");
			stringBuilder.append("  <td style='padding:0cm 0cm 0cm 0cm'>                                                                                                                                                ");
			stringBuilder.append("  <p class=titulo1 style='margin-bottom:12.0pt'>Olá Sistema Fusion,</p>                                                                                                               ");
			stringBuilder.append("  <p>&nbsp;</p>                                                                                                                                                                       ");
			stringBuilder.append("  <p class=mailtxt>                                                                                                                                         ");
			stringBuilder.append("  " + descricao + "                                                                                                              ");
			stringBuilder.append("  .</p>                                                                                      ");
			stringBuilder.append("  </td>                                                                                                                                                                               ");
			stringBuilder.append(" </tr>                                                                                                                                                                                ");
			stringBuilder.append(" <tr style='mso-yfti-irow:2'>                                                                                                                                                         ");
			stringBuilder.append("  <td style='padding:0cm 0cm 0cm 0cm'>                                                                                                                                                ");
			stringBuilder.append("  <p class=mailcopy align=center style='text-align:center'>Copyright ©                                                                                                                ");
			stringBuilder.append("  2006-2016&nbsp; Neomind. Todos os direitos reservados.<br>                                                                                                                          ");
			stringBuilder.append("  <a href=\"http://www.neomind.com.br\">www.neomind.com.br</a> </p>                                                                                                                     ");
			stringBuilder.append("  <p class=MsoNormal align=center style='text-align:center'><span                                                                                                                     ");
			stringBuilder.append("  style='mso-fareast-font-family:\"Times New Roman\"'><img border=0 width=114                                                                                                           ");
			stringBuilder.append("  height=33 id=\"_x0000_i1026\"                                                                                                                                                         ");
			stringBuilder.append("  src=\"http://intranet.orsegups.com.br/fusion/imagens/login/log_neomind.jpg\"                                                                                                          ");
			stringBuilder.append("  alt=\"Neomind Fusion\"><o:p></o:p></span></p>                                                                                                                                         ");
			stringBuilder.append("  </td>                                                                                                                                                                               ");
			stringBuilder.append(" </tr>                                                                                                                                                                                ");
			stringBuilder.append(" <tr style='mso-yfti-irow:3;mso-yfti-lastrow:yes;height:54.0pt'>                                                                                                                      ");
			stringBuilder.append("  <td style='padding:0cm 0cm 0cm 0cm;height:54.0pt'>                                                                                                                                  ");
			stringBuilder.append("  <p class=MsoNormal align=center style='text-align:center'><span                                                                                                                     ");
			stringBuilder.append("  style='mso-fareast-font-family:\"Times New Roman\"'><img border=0 width=550                                                                                                           ");
			stringBuilder.append("  height=72 id=\"_x0000_i1027\"                                                                                                                                                         ");
			stringBuilder.append("  src=\"http://intranet.orsegups.com.br/fusion/imagens/botom_mail.jpg\"                                                                                                                 ");
			stringBuilder.append("  alt=\"Neomind Fusion\"><o:p></o:p></span></p>                                                                                                                                         ");
			stringBuilder.append("  </td>                                                                                                                                                                               ");
			stringBuilder.append(" </tr>                                                                                                                                                                                ");
			stringBuilder.append("</table>                                                                                                                                                                              ");
			stringBuilder.append("                                                                                                                                                                                      ");
			stringBuilder.append("</div>                                                                                                                                                                                ");
			stringBuilder.append("                                                                                                                                                                                      ");
			stringBuilder.append("<p class=MsoNormal><span style='mso-fareast-font-family:\"Times New Roman\"'><o:p>&nbsp;</o:p></span></p>                                                                               ");
			stringBuilder.append("                                                                                                                                                                                      ");
			stringBuilder.append("</div>                                                                                                                                                                                ");
			stringBuilder.append("                                                                                                                                                                                      ");
			stringBuilder.append("</body>                                                                                                                                                                               ");
			stringBuilder.append("                                                                                                                                                                                      ");
			stringBuilder.append("</html>                                                                                                                                                                               ");
			stringBuilder.append("                                                                                                                                                                                      ");

			if (mailClone != null)
			{

				HtmlEmail noUserEmail = new HtmlEmail();

				noUserEmail.setCharset("ISO-8859-1");
				noUserEmail.addTo(from);
				noUserEmail.addCc("emailautomatico@orsegups.com.br");
				noUserEmail.setSubject(titulo);
				noUserEmail.setHtmlMsg(stringBuilder.toString());
				noUserEmail.setContent(stringBuilder.toString(), "text/html");
				mailClone.applyConfig(noUserEmail);

				noUserEmail.send();
			}
		}
		catch (Exception e)
		{
			log.error("##### ERRO Rotina Envia Email Abre Tarefa - Data: " + e.getMessage().toString() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}
	}

	/**
	 * Método para salvar o histórico de abertura de tarefas
	 * 
	 * @param tarefa
	 * @param titulo
	 * @param datCad
	 * @param solicitante
	 * @param executor
	 * @param emailSolicitante
	 * @param emailExecutor
	 * @param datEnvio
	 */
	public void setHistorico(String tarefa, String titulo, GregorianCalendar datCad, String solicitante, String executor, String emailSolicitante, String emailExecutor, Date datEnvio)
	{

		try
		{
			InstantiableEntityInfo infoHis = (InstantiableEntityInfo) EntityRegister.getEntityInfo("EmailTarefaSimplesHistorico");
			NeoObject emailHis = infoHis.createNewInstance();
			EntityWrapper emailHisWp = new EntityWrapper(emailHis);
			emailHisWp.findField("tarefa").setValue(tarefa);
			emailHisWp.findField("titulo").setValue(titulo);
			emailHisWp.findField("dataCadastro").setValue(datCad);
			NeoUser usuarioSolicitante = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", solicitante));
			emailHisWp.findField("solicitante").setValue(usuarioSolicitante);
			NeoUser usuarioExecutor = PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", executor));
			emailHisWp.findField("executor").setValue(usuarioExecutor);
			emailHisWp.findField("emailSolicitante").setValue(emailSolicitante);
			emailHisWp.findField("emailExecutor").setValue(emailExecutor);
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(datEnvio);
			emailHisWp.findField("dataEnvio").setValue(calendar);

			PersistEngine.persist(emailHis);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO AO getConfigE2DOC: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		}

	}

	/**
	 * Método para buscar emails do grupo
	 * 
	 * @return
	 */
	private HashMap<String, Set<String>> verificaGrupoEmail()
	{

		String csvArq = "\\\\ssoovt09\\f$\\Sistemas\\Fusion\\GrupoEmail\\grupos.o365.csv";
		BufferedReader br = null;
		String linha = "";
		String cvsSplit = ",";
		HashMap<String, Set<String>> map = null;
		Set<String> setEmails = null;
		String grupo = "";
		try
		{
			if (csvArq != null)
			{
				File folder = new File(csvArq);

				if (folder.exists())
				{

					int count = 0;
					br = new BufferedReader(new FileReader(csvArq));
					map = new HashMap<String, Set<String>>();
					setEmails = new TreeSet<String>();
					while ((linha = br.readLine()) != null)
					{
						if (linha.contains(",") && linha.split(cvsSplit).length > 2 && count > 1)
						{

							String[] country = linha.split(cvsSplit);

							if (country != null && country[2] != null && !country[2].isEmpty() && country[3] != null && !country[3].isEmpty())
							{
								if (count == 2)
								{
									grupo = country[2].replaceAll("\"", "");
								}
								if (grupo != null && !grupo.isEmpty() && !grupo.equals(country[2].replaceAll("\"", "")))
								{
									map.put(grupo, setEmails);
									grupo = country[2].replaceAll("\"", "");
									setEmails = new TreeSet<String>();
								}

								matcher = pattern.matcher(country[3].replaceAll("\"", ""));

								if (matcher.matches())
								{
									setEmails.add(country[3].replaceAll("\"", ""));
								}
							}
						}
						count++;
					}
					if (grupo != null && !grupo.isEmpty() && setEmails != null && !setEmails.isEmpty())
					{
						map.put(grupo, setEmails);
					}
				}
			}
		}
		catch (FileNotFoundException e)
		{
			System.out.println("Arquivo não encontrado.");
		}
		catch (IOException e)
		{
			System.out.println("Arquivo com erros.");
		}
		finally
		{
			if (br != null)
			{
				try
				{
					br.close();
				}
				catch (IOException e)
				{
					System.out.println("Arquivo com erros.");
				}
			}
		}
		return map;
	}
}
