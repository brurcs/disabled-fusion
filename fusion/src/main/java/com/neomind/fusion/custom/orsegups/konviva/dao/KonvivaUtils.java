package com.neomind.fusion.custom.orsegups.konviva.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.neomind.fusion.custom.orsegups.konviva.vo.ColaboradorKonvivaVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;

public class KonvivaUtils
{
	public static List<ColaboradorKonvivaVO> lstColaboradorKonviva()
	{
		List<ColaboradorKonvivaVO> lstColaboradoresKonviva = new ArrayList<ColaboradorKonvivaVO>();
		Connection conVetorh = PersistEngine.getConnection("VETORH");
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		StringBuilder sqlColaboradoresKonviva = new StringBuilder();
		
		sqlColaboradoresKonviva.append(" SELECT top 1000 FUN.numcpf, FUN.nomfun, ORN.usu_codccu, ORN.nomloc, FUN.numcad, emp.nomemp as nomoem, ORN.usu_lotorn as supervisor, orn.nomloc, reg.USU_NomReg FROM R034FUN FUN "); 
		sqlColaboradoresKonviva.append(" INNER JOIN R030EMP EMP ON EMP.NUMEMP = FUN.NUMEMP																						                                         ");
		sqlColaboradoresKonviva.append(" INNER JOIN R038HLO HLO ON HLO.NumEmp = FUN.numemp 																			                                                     ");	
		sqlColaboradoresKonviva.append("               AND HLO.TipCol = FUN.tipcol                                                                                                                                       ");
		sqlColaboradoresKonviva.append("               AND HLO.NumCad = FUN.NumCad                                                                                                                                       ");
		sqlColaboradoresKonviva.append("               AND HLO.DatAlt = (SELECT MAX(HLO1.DATALT) FROM R038HLO HLO1                                                                                                       ");
		sqlColaboradoresKonviva.append("                                 WHERE HLO1.NumEmp = HLO.NumEmp                                                                                                                  ");
		sqlColaboradoresKonviva.append("                                   AND HLO1.TipCol = HLO.TipCol                                                                                                                  ");
		sqlColaboradoresKonviva.append("                                   AND HLO1.NumCad = HLO.NumCad)                                                                                                                 ");
		sqlColaboradoresKonviva.append(" INNER JOIN R016ORN ORN ON ORN.taborg = HLO.TabOrg                                                                                                                               ");
		sqlColaboradoresKonviva.append("                       AND ORN.numloc = HLO.NumLoc                                                                                                                               ");
		sqlColaboradoresKonviva.append(" INNER JOIN R038HFI HFI on HFI.NumEmp = FUN.numemp                                                                                                                               ");
		sqlColaboradoresKonviva.append("                       AND HFI.TipCol = FUN.tipcol                                                                                                                               ");
		sqlColaboradoresKonviva.append("                       AND HFI.NumCad = FUN.numcad                                                                                                                               ");
		sqlColaboradoresKonviva.append("                       AND HFI.DatAlt = (SELECT MAX(HFI1.DATALT) FROM R038HFI HFI1                                                                                               ");
		sqlColaboradoresKonviva.append("                                          WHERE HFI1.NumEmp = HFI.NumEmp                                                                                                         ");
		sqlColaboradoresKonviva.append("                                            AND HFI1.TipCol = HFI.TipCol                                                                                                         ");
		sqlColaboradoresKonviva.append("                                            AND HFI1.NumCad = HFI.NumCad)                                                                                                        ");
		sqlColaboradoresKonviva.append(" INNER JOIN r030fil fil on fil.codfil = HFI.codfil                                                                                                                               ");
		sqlColaboradoresKonviva.append("                       AND fil.numemp = HFI.numemp                                                                                                                               ");
		sqlColaboradoresKonviva.append(" INNER JOIN r032oem oem on oem.codoem = fil.codoem                                                                                                                               ");
		sqlColaboradoresKonviva.append(" INNER JOIN USU_T200REG reg on reg.USU_CodReg = ORN.usu_codreg                                                                                                                   ");
		sqlColaboradoresKonviva.append(" /*INNER JOIN USU_T016SUP SUP ON SUP.USU_CODCCU = ORN.USU_CODCCU                                                                                                                 ");
		sqlColaboradoresKonviva.append(" INNER JOIN r016orn ORNSUP on ORNSUP.usu_codccu = SUP.usu_codccusup AND ORNSUP.usu_sitati = 'S'                                                                                  ");
		sqlColaboradoresKonviva.append(" INNER JOIN R038HLO HLOSUP on HLOSUP.TabOrg = ORNSUP.TabOrg                                                                                                                      ");
		sqlColaboradoresKonviva.append(" AND HLOSUP.NUMLOC = ORNSUP.NUmLoc                                                                                                                                               ");
		sqlColaboradoresKonviva.append(" AND HLOSUP.DatAlt = (SELECT MAX(HLO2.DatAlt) FROM R038HLO HLO2                                                                                                                  ");
		sqlColaboradoresKonviva.append(" WHERE HLO2.TabOrg = HLOSUP.TabOrg                                                                                                                                               ");
		sqlColaboradoresKonviva.append(" AND HLO2.NumLoc = HLOSUP.NumLoc)                                                                                                                                                ");
		sqlColaboradoresKonviva.append(" INNER JOIN r034fun FUNSUP ON FUNSUP.numemp = HLOSUP.NumEmp                                                                                                                      ");
		sqlColaboradoresKonviva.append(" AND FUNSUP.tipcol = HLOSUP.TipCol                                                                                                                                               ");
		sqlColaboradoresKonviva.append(" AND FUNSUP.numcad = HLOSUP.NumCad*/                                                                                                                                             ");
		sqlColaboradoresKonviva.append(" WHERE FUN.NUMEMP IN (1,2,6,7,8,15,17,18,19,21,22,24,25,26,27,28,29,30)                                                                                                          ");
		sqlColaboradoresKonviva.append(" AND FUN.SITAFA NOT IN (3,4,6,7) AND NOT EXISTS (SELECT 1 FROM [CACUPE\\SQL02].Fusion_Producao.dbo.D_usuariosIntegradosKonviva uik WHERE uik.cpf = FUN.numcpf                    ");
		sqlColaboradoresKonviva.append(" AND CONVERT(VARCHAR,dataProcessamento,103) = CONVERT(VARCHAR,GETDATE(),103))																									 ");                   
		
		try
		{
			pst = conVetorh.prepareStatement(sqlColaboradoresKonviva.toString());
			rs = pst.executeQuery();
			
			while (rs.next())
			{
				ColaboradorKonvivaVO colaborador = new ColaboradorKonvivaVO();
				
				colaborador.setDsCPF(rs.getString("numcpf"));
				colaborador.setCdCentroCusto(rs.getLong("usu_codccu"));
				colaborador.setDsPosto(rs.getString("nomloc"));
				colaborador.setCdMatricula(rs.getLong("numcad"));
				colaborador.setDsCentroCusto(rs.getString("nomloc"));
				colaborador.setDsFilial(rs.getString("nomoem"));
				colaborador.setDsRegional(rs.getString("usu_nomreg"));
				colaborador.setDsSupervisor(rs.getString("supervisor"));
				colaborador.setNomeFuncionario(rs.getString("nomfun"));
		
				lstColaboradoresKonviva.add(colaborador);
			}			
			return lstColaboradoresKonviva;
			
		}
		catch(Exception e)
		{
			System.out.println("##### Erro ao processar a consulta para Integração do Konviva!");
			e.printStackTrace();
			return lstColaboradoresKonviva;
		}
		finally
		{
			OrsegupsUtils.closeConnection(conVetorh, pst, rs);
		}
	}
}
