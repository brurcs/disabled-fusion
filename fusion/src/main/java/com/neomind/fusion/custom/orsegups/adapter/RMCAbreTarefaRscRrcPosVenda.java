package com.neomind.fusion.custom.orsegups.adapter;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.rsc.helper.RSCHelper;
import com.neomind.fusion.custom.orsegups.rsc.vo.RSCVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.fusion.workflow.simulation.WorkflowService;

public class RMCAbreTarefaRscRrcPosVenda implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{
			String observacao = "";
			String code = "";

			Boolean desejaRealizarPesquisaContato = (Boolean) processEntity.findValue("desejaRealizarPesquisaContato");
			Boolean possivelRealizarPosVenda = (Boolean) processEntity.findValue("possivelRealizarPosVenda");
			Boolean informacoesDoClienteConferem = (Boolean) processEntity.findValue("pesquisaPosVenda.informacoesDoClienteConferem");
			Boolean duvidasOuSugestoes = (Boolean) processEntity.findValue("pesquisaPosVenda.duvidasOuSugestoes");
			Long notaAtendimentoComercial = (Long) processEntity.findValue("pesquisaPosVenda.notaAtendimentoComercial.codigoNota");
			Long notaAtendimentoTecnico = (Long) processEntity.findValue("pesquisaPosVenda.notaAtendimentoTecnico.codigoNota");

			if (desejaRealizarPesquisaContato && desejaRealizarPesquisaContato != null)
			{
				List<String> lstInformacoesRRC = null;

				if (possivelRealizarPosVenda && possivelRealizarPosVenda != null && notaAtendimentoComercial != null)
				{

					if (notaAtendimentoComercial == 3L || notaAtendimentoComercial == 4L)
					{
						observacao = "RRC Aberta devido a Pesquisa de PÃ³s Venda realizada pela tarefa de RMC " + activity.getCode() + ". Seguem as reclamaÃ§Ãµes feitas pelo cliente: ";
						observacao = observacao + (String) processEntity.findValue("pesquisaPosVenda.obsNotaAtendimentoComercial");

						lstInformacoesRRC = retornaInformacoesIniciarRRC();
						code = abreRSCouRRC(lstInformacoesRRC.get(0), lstInformacoesRRC.get(1), lstInformacoesRRC.get(2), lstInformacoesRRC.get(3), lstInformacoesRRC.get(4), lstInformacoesRRC.get(5), Long.valueOf(lstInformacoesRRC.get(6)), "", processEntity, observacao);

						processEntity.findField("pesquisaPosVenda.obsNotaAtendimentoComercial").setValue(((String) processEntity.findValue("pesquisaPosVenda.obsNotaAtendimentoComercial")) + " Aberto a tarefa de RRC " + code);
					}

					if (notaAtendimentoTecnico == 3L || notaAtendimentoTecnico == 4L)
					{
						observacao = "RRC Aberta devido a Pesquisa de PÃ³s Venda realizada pela tarefa de RMC " + activity.getCode() + ". Seguem as reclamaÃ§Ãµes feitas pelo cliente: ";
						observacao = observacao + (String) processEntity.findValue("pesquisaPosVenda.obsNotaAtendimentoTecnico");

						lstInformacoesRRC = retornaInformacoesIniciarRRC();
						code = abreRSCouRRC(lstInformacoesRRC.get(0), lstInformacoesRRC.get(1), lstInformacoesRRC.get(2), lstInformacoesRRC.get(3), lstInformacoesRRC.get(4), lstInformacoesRRC.get(5), Long.valueOf(lstInformacoesRRC.get(6)), "", processEntity, observacao);

						processEntity.findField("pesquisaPosVenda.obsNotaAtendimentoTecnico").setValue(((String) processEntity.findValue("pesquisaPosVenda.obsNotaAtendimentoTecnico")) + " Aberto a tarefa de RRC " + code);
					}

					if (!informacoesDoClienteConferem)
					{
						observacao = "RSC Aberta devido a Pesquisa de PÃ³s Venda realizada pela tarefa de RMC " + activity.getCode() + ". Seguem as solicitaÃ§Ãµes feitas pelo cliente para ajustes: ";
						observacao = observacao + (String) processEntity.findValue("pesquisaPosVenda.obsInformacoesDoClienteConferem");

						List<String> lstInformacoesRSC = retornaInformacoesIniciarRSC();
						code = abreRSCouRRC(lstInformacoesRSC.get(0), lstInformacoesRSC.get(1), lstInformacoesRSC.get(2), lstInformacoesRSC.get(3), lstInformacoesRSC.get(4), lstInformacoesRSC.get(5), Long.valueOf(lstInformacoesRSC.get(6)), lstInformacoesRSC.get(7), processEntity, observacao);

						processEntity.findField("pesquisaPosVenda.obsInformacoesDoClienteConferem").setValue(((String) processEntity.findValue("pesquisaPosVenda.obsInformacoesDoClienteConferem")) + " Aberto a tarefa de RSC " + code);
					}

					if (duvidasOuSugestoes)
					{
						observacao = "RSC Aberta devido a Pesquisa de PÃ³s Venda realizada pela tarefa de RMC " + activity.getCode() + ". Seguem as solicitaÃ§Ãµes feitas pelo cliente para ajustes: ";
						observacao = observacao + (String) processEntity.findValue("pesquisaPosVenda.obsDuvidasOuSugestoes");

						List<String> lstInformacoesRSC = retornaInformacoesIniciarRSC();
						code = abreRSCouRRC(lstInformacoesRSC.get(0), lstInformacoesRSC.get(1), lstInformacoesRSC.get(2), lstInformacoesRSC.get(3), lstInformacoesRSC.get(4), lstInformacoesRSC.get(5), Long.valueOf(lstInformacoesRSC.get(6)), lstInformacoesRSC.get(7), processEntity, observacao);

						processEntity.findField("pesquisaPosVenda.obsDuvidasOuSugestoes").setValue(((String) processEntity.findValue("pesquisaPosVenda.obsDuvidasOuSugestoes")) + " Aberto a tarefa de RSC " + code);
					}
				}
			}
			/*
			 * Solicitado pela SrÂª. Fernanda Maciel via tarefa 761005 que esse processo fosse desfeito
			 * else
			 * {
			 * observacao =
			 * "RRC Aberta devido a Pesquisa de PÃ³s Venda que nÃ£o foi realizada. Tarefa de RMC " +
			 * activity.getCode() + ". Seguem as justificativas da nÃ£o realizaÃ§Ã£o da pesquisa: ";
			 * observacao = observacao + (String) processEntity.findValue("obsPossivelRealizarPosVenda");
			 * lstInformacoesRRC = retornaInformacoesIniciarRRC();
			 * code = abreRSCouRRC(lstInformacoesRRC.get(0), lstInformacoesRRC.get(1),
			 * lstInformacoesRRC.get(2), lstInformacoesRRC.get(3), lstInformacoesRRC.get(4),
			 * lstInformacoesRRC.get(5), Long.valueOf(lstInformacoesRRC.get(6)), "", processEntity,
			 * observacao);
			 * processEntity.findField("obsPossivelRealizarPosVenda").setValue(((String)
			 * processEntity.findValue("obsPossivelRealizarPosVenda")) + " Aberto a tarefa de RRC " +
			 * code);
			 * }
			 */
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Erro inesperado. Por favor, contate o Departamento de TI!");
		}
	}

	public static List<String> retornaInformacoesIniciarRRC()
	{
		String eform = "";
		String workflow = "";
		String fieldTelefone = "";
		String fieldContato = "";
		String fieldMensagem = "";
		String solicitante = "";
		Long idTipoContato = 0L;

		QLEqualsFilter filterTipoContato = new QLEqualsFilter("neoId", 11715151L);
		NeoObject tipoContatoObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("tipoContato"), filterTipoContato);
		EntityWrapper tipoContatoWrapper = new EntityWrapper(tipoContatoObj);
		idTipoContato = (Long) tipoContatoWrapper.getValue("neoId");
		String descricaoTipoContato = (String) tipoContatoWrapper.getValue("tipo");

		eform = "RRCRelatorioReclamacaoCliente";
		workflow = "Q005 - RRC - Relatório de Reclamação de Cliente";
		fieldTelefone = "telefone";
		fieldContato = "pessoaContato";
		fieldMensagem = "textoReclamacao";
		solicitante = "SITERESPONSAVELRRC";

		List<String> lstInformacoesRRC = new ArrayList<String>();
		lstInformacoesRRC.add(eform);
		lstInformacoesRRC.add(workflow);
		lstInformacoesRRC.add(fieldTelefone);
		lstInformacoesRRC.add(fieldContato);
		lstInformacoesRRC.add(fieldMensagem);
		lstInformacoesRRC.add(solicitante);
		lstInformacoesRRC.add(String.valueOf(idTipoContato));

		return lstInformacoesRRC;
	}

	public static List<String> retornaInformacoesIniciarRSC()
	{

		String eform = "";
		String workflow = "";
		String fieldTelefone = "";
		String fieldContato = "";
		String fieldMensagem = "";
		String solicitante = "";
		String valueMensagem = "";
		Long idTipoContato = 0L;

		QLEqualsFilter filterTipoContato = new QLEqualsFilter("neoId", 12430375L);
		NeoObject tipoContatoObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("tipoContato"), filterTipoContato);
		EntityWrapper tipoContatoWrapper = new EntityWrapper(tipoContatoObj);
		idTipoContato = (Long) tipoContatoWrapper.getValue("neoId");
		String descricaoTipoContato = (String) tipoContatoWrapper.getValue("tipo");

		//eform = "RSCRelatorioSolicitacaoCliente";
		eform = "RSCRelatorioSolicitacaoClienteNovo";
		//workflow = "C027 - RSC - RelatÃ³rio de SolicitaÃ§Ã£o de Cliente";
		workflow = "C027 - RSC - RelatÃ³rio de SolicitaÃ§Ã£o de Cliente Novo";
		fieldTelefone = "telefones";
		fieldContato = "contato";
		fieldMensagem = "descricao";
		valueMensagem = "Tipo do Contato: " + descricaoTipoContato + " \nDescriÃ§Ã£o: ";
		solicitante = "RMCRESPONSAVELRSC";

		List<String> lstInformacoesRSC = new ArrayList<String>();
		lstInformacoesRSC.add(eform);
		lstInformacoesRSC.add(workflow);
		lstInformacoesRSC.add(fieldTelefone);
		lstInformacoesRSC.add(fieldContato);
		lstInformacoesRSC.add(fieldMensagem);
		lstInformacoesRSC.add(solicitante);
		lstInformacoesRSC.add(String.valueOf(idTipoContato));
		lstInformacoesRSC.add(valueMensagem);

		return lstInformacoesRSC;
	}

	// TODO RSC - Dalvan testar abertura de RSC
	public static String abreRSCouRRC(String eform, String workflow, String fieldTelefone, String fieldContato, String fieldMensagem, String solRscRrc, Long idTipoContato, String valueMensagem, EntityWrapper rmc, String observacao)
	{
		NeoObject oCliente = null;
		Long codcli = (Long) rmc.findValue("codigoCliente");

		QLEqualsFilter filterCliente = new QLEqualsFilter("codcli", codcli);
		oCliente = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("SAPIENS_Clientes"), filterCliente);

		String nomCliente = "";
		String emaCliente = "";
		String telCliente = "";

		if (oCliente != null)
		{
			EntityWrapper wrpCliente = new EntityWrapper(oCliente);
			nomCliente = (String) wrpCliente.findValue("nomcli");
			emaCliente = (String) wrpCliente.findValue("intnet");
			telCliente = (String) wrpCliente.findValue("foncli");

		}

		//Cria Instancia do Eform Principal
		InstantiableEntityInfo infoSolicitacao = AdapterUtils.getInstantiableEntityInfo(eform);
		NeoObject noSolicitacao = infoSolicitacao.createNewInstance();
		EntityWrapper solicitacaoWrapper = new EntityWrapper(noSolicitacao);

		//Busca Origem da Solicitacao
		QLEqualsFilter filterOrigem = new QLEqualsFilter("sequencia", "03");
		NeoObject origemObj = (NeoObject) PersistEngine.getObject(AdapterUtils.getEntityClass("RRCOrigem"), filterOrigem);

		GregorianCalendar gcPrazo = new GregorianCalendar();
		gcPrazo.add(GregorianCalendar.DATE, 5);
		gcPrazo.set(GregorianCalendar.HOUR, 23);
		gcPrazo.set(GregorianCalendar.MINUTE, 59);
		gcPrazo.set(GregorianCalendar.SECOND, 59);

		if (idTipoContato == 11715151L)
		{
			solicitacaoWrapper.setValue("prazoAcao", OrsegupsUtils.getNextWorkDay(gcPrazo));
		}
		else
		{
			//solicitacaoWrapper.setValue("prazoAtendimento", OrsegupsUtils.getNextWorkDay(gcPrazo));

			String valueSolicitante = "SITERESPONSAVELRSC";

			RSCVO rsc = new RSCVO();
			rsc.setCodCli(codcli);
			rsc.setOrigemSolicitacao("03");
			rsc.setTipoContato("127514862");
			rsc.setNome(nomCliente);
			rsc.setEmail(emaCliente);
			rsc.setTelefone(telCliente);
			rsc.setMensagem(valueMensagem + observacao);
			rsc.setPrazo(OrsegupsUtils.getNextWorkDay(gcPrazo));

			String tarefa = null;
			try
			{
				tarefa = RSCHelper.abrirRSC(rsc, valueSolicitante, true);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			System.out.println("Tarefa RSC Aberta: " + tarefa);
			return tarefa;
		}

		//Seta os valores
		solicitacaoWrapper.setValue("origem", origemObj);
		solicitacaoWrapper.setValue("email", emaCliente);
		solicitacaoWrapper.setValue(fieldContato, nomCliente);
		solicitacaoWrapper.setValue(fieldTelefone, telCliente);
		solicitacaoWrapper.setValue(fieldMensagem, valueMensagem + observacao);

		//Caso existe o cliente
		if (oCliente != null)
		{
			solicitacaoWrapper.findField("clienteSapiens").setValue(oCliente);
		}

		//Salva o Eform
		PersistEngine.persist(solicitacaoWrapper.getObject());

		// Cria Instancia do WorkFlow
		QLEqualsFilter equal = new QLEqualsFilter("name", workflow);
		List<ProcessModel> processModelList = (List<ProcessModel>) PersistEngine.getObjects(ProcessModel.class, equal, " neoId DESC ");
		ProcessModel processModel = processModelList.get(0);

		//Busca o solicitante
		NeoPaper papel = new NeoPaper();
		String executor = "";
		NeoPaper obj = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", solRscRrc));
		papel = (NeoPaper) obj;

		if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
		{
			for (NeoUser user : papel.getUsers())
			{
				executor = user.getCode();
				break;
			}

		}
		else
		{
			String gestaoCerec = "GestÃ£o CEREC";
			NeoPaper objCerec = (NeoPaper) PersistEngine.getObject(AdapterUtils.getEntityClass("NeoPaper"), new QLEqualsFilter("code", gestaoCerec));
			papel = (NeoPaper) objCerec;
			if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
			{
				for (NeoUser user : papel.getUsers())
				{
					executor = user.getCode();
					break;
				}
			}
			else
			{
				String mensagem = "UsuÃ¡rio ou Gestor ResponsÃ¡vel nÃ£o encontrado!";
				System.out.println(mensagem);
			}

		}

		NeoUser solicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", executor));
		System.out.println("teste do Dalvan  Pos vendas solicitante: " + solicitante);

		/**
		 * @author neomind willian.mews - Alterado para utilizar a nova arquitetura de processos do
		 *         Fusion
		 * @date 11/03/2015
		 */

		final WFProcess processo = WorkflowService.startProcess(processModel, solicitacaoWrapper.getObject(), false, solicitante);
		System.out.println("SolicitaÃ§Ã£o aberta via Pesquisa de PÃ³s Venda [RMC]. " + processo.getCode());
		String mensagem = processo.getCode();
		try
		{
			new OrsegupsWorkflowHelper(processo).avancaPrimeiraAtividade(solicitante, false);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("Erro #4 - Erro ao avanÃ§ar a primeira tarefa");
			mensagem = "Erro #4 - Erro ao avanÃ§ar a primeira tarefa";
		}
		return mensagem;
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub		
	}

}
