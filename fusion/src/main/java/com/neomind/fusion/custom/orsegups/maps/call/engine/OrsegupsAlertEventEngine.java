package com.neomind.fusion.custom.orsegups.maps.call.engine;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import us.monoid.json.JSONObject;

import com.google.gson.Gson;
import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.callcenter.ProvidenciaVO;
import com.neomind.fusion.custom.orsegups.jms.NeoJMSPublisher;
import com.neomind.fusion.custom.orsegups.maps.call.vo.CallAlertEventVO;
import com.neomind.fusion.custom.orsegups.maps.call.vo.EventoAcessoClienteVO;
import com.neomind.fusion.custom.orsegups.maps.call.vo.EventoHistoricoVO;
import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.seventh.SeventhDAOImpl;
import com.neomind.fusion.custom.orsegups.sigma.RotinaCallListenerXPRE;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class OrsegupsAlertEventEngine {
    private static final Log log = LogFactory.getLog(OrsegupsAlertEventEngine.class);

    private static OrsegupsAlertEventEngine instance;

    public static final String filaOperacao = "CM_EVENTOS";

    public static Map<String, CallAlertEventVO> callingAlertEventVO;

    private static Boolean verificandoEventos = false;

    private static String linkSnep = "http://192.168.20.241";

    private static String WEB_SERVICE_SIGMA = "http://192.168.20.218:8080/SigmaWebServices/";

    public static final String USUARIO_CORPORATIVO = "11697,11707,12570,12571,11712";

    private OrsegupsAlertEventEngine() {
	OrsegupsAlertEventEngine.callingAlertEventVO = new HashMap<String, CallAlertEventVO>();
    }

    public static OrsegupsAlertEventEngine getInstance() {
	if (OrsegupsAlertEventEngine.instance == null) {
	    OrsegupsAlertEventEngine.instance = new OrsegupsAlertEventEngine();

	    CallAlertsMapsRoutine.executaRotinaAtendimentoDeEventosCorporativos();
	    CallAlertsMapsRoutine.executaRotinaAtendimentoDeEventos();
	    CallAlertsMapsRoutine.executaRotinaDeslocamentoEventoViatura();
	    CallAlertsMapsRoutine.executaRotinaDeslocarEventoViaturaLivre();
	    CallAlertsMapsRoutine.executaAutomatizarRotas();
	    try{ RotinaCallListenerXPRE.executaRotinaXPRE(); }catch(Exception e){ log.error("Erro ao executar rotina de xpre", e); e.printStackTrace(); }

	}
	return OrsegupsAlertEventEngine.instance;
    }

    /*
     * me chamado para cda alerta x2, x5 e x406 encontrado no mapa faz o
     * tratamento para realizar a ligaÃ§Ã£o para ele
     */
    public void alert(EventoVO eventoVO, List<String> telCal) {
	// realizar a chamada deste me quando os alertas ocorrerem.

	// caso o algum operador ja esteja ligando para este evento nÃ£o realiza
	// nova ligacao
	
	Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();
	
	System.out.println("#"+eventoVO.getCodigoHistorico()+"# 5.5 Iniciou metodo alerta Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");
	try {
	    if (eventoVO != null && !OrsegupsAlertEventEngine.callingAlertEventVO.containsKey(eventoVO.getCodigoHistorico())) {
		List<String> operadoresDisponiveis = telCal;
		String[] ramalOperador = null;
		String telefone = null;
//		 Descommentar
		// operadoresDisponiveis = this.getRamaisDisponiveis();
		// verifica se tem algum operador disponivel
		if (operadoresDisponiveis != null && !operadoresDisponiveis.isEmpty()) {
		    for (String valor : operadoresDisponiveis) {
			if (valor != null && valor.contains(";")) {
			    ramalOperador = valor.split(";");
			    if (ramalOperador[1] != null && eventoVO.getCodigoEvento() != null && ( ramalOperador[1].contains(eventoVO.getCodigoEvento()) || ramalOperador[1].contains(eventoVO.getCdCode()) )  ) {
				Long grupoOperador = getGrupoRamal(ramalOperador[0]);
				if (eventoVO.getGrupo() != null && !eventoVO.getGrupo().equals(0L)){
				    GregorianCalendar gc = new GregorianCalendar();
				    if (eventoVO.getGrupo() == 220L){ // Balaroti
					int hora = gc.get(Calendar.HOUR_OF_DAY);
					if (hora < 19 && hora > 7){
					    if (grupoOperador == null){
						telefone = ramalOperador[0];
						break;
					    }
					}else{
					    if (grupoOperador != null && grupoOperador.equals(eventoVO.getGrupo())){
						telefone = ramalOperador[0];
						break;
					    }
					}
				    }else if (eventoVO.getGrupo() == 221L){ // Globo
					int dia = gc.get(Calendar.DAY_OF_WEEK);
					int hora = gc.get(Calendar.HOUR_OF_DAY);
					if (dia > 1 && dia < 7){
					    if (hora < 18 && hora > 8){
						if (grupoOperador == null){
						    telefone = ramalOperador[0];
						    break;
						}
					    }else{
						if (grupoOperador != null && grupoOperador.equals(eventoVO.getGrupo())){
						    telefone = ramalOperador[0];
						    break;
						}
					    }
					}else{
					    if (grupoOperador != null && grupoOperador.equals(eventoVO.getGrupo())){
						telefone = ramalOperador[0];
						break;
					    }
					}
				    }else{
					if (grupoOperador == null){
					    telefone = ramalOperador[0];
					    break;
					}
				    }
				}else{
				    if (grupoOperador == null){
					telefone = ramalOperador[0];
					break;
				    }
				}
			    }
			}

		    }
		    if (telefone != null && !telefone.isEmpty()) {
			boolean pausaOk = false;
			
			System.out.println("#"+eventoVO.getCodigoHistorico()+"# 5.6 Iniciou busca de ramal Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");
			
			pausaOk = this.getPausaRamal(telefone.trim());
			if (pausaOk) {
			    System.out.println("#"+eventoVO.getCodigoHistorico()+"# 5.7 Iniciou busca de telefone Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");
			    LinkedHashSet<ProvidenciaVO> numerosChamar = this.telefoneCliente(eventoVO);
			    CallAlertEventVO alertEventVo = new CallAlertEventVO();
			    alertEventVo.setCallEventoVO(eventoVO);
			    alertEventVo.setNumerosChamar(numerosChamar);
			    alertEventVo.setRamalOperador(telefone.trim());
			    OrsegupsAlertEventEngine.callingAlertEventVO.put(alertEventVo.getCallEventoVO().getCodigoHistorico(), alertEventVo);
			    System.out.println("#"+eventoVO.getCodigoHistorico()+"# 5.8 Iniciou ligacao para cliente Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");
			    this.ligarParaCliente(eventoVO.getCodigoHistorico());
			    System.out.println("#"+eventoVO.getCodigoHistorico()+"# 5.9 Finalizou ligacao para cliente Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");
			    String ramalStr = "";
			    String contaStr = "";
			    if (alertEventVo.getRamalOperador() != null)
				ramalStr = alertEventVo.getRamalOperador();
			    if (alertEventVo.getCallEventoVO().getCodigoCentral() != null && alertEventVo.getCallEventoVO().getParticao() != null)
				contaStr = "Conta : " + alertEventVo.getCallEventoVO().getCodigoCentral() + "[" + alertEventVo.getCallEventoVO().getParticao() + "] Evento : "
					+ alertEventVo.getCallEventoVO().getCodigoEvento() + " Historico : " + alertEventVo.getCallEventoVO().getCodigoHistorico() + " Cliente : "
					+ alertEventVo.getCallEventoVO().getCodigoCliente();
			    System.out.println("#"+eventoVO.getCodigoHistorico()+"# 5.10 Iniciou alteracao ultima ligacao Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");
			    this.alteraDataUltimaLigacaoRamal(ramalStr, contaStr);
			    
			    System.out.println("#"+eventoVO.getCodigoHistorico()+"# 5.11 Finalizou alteracao ultima ligacao Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");
			}
		    }
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("ERRO AO RETORNAR ALERT EVENT " + e);

	}
    }

    /*
     * busca no sneps quais operadores da fila estÃ£o disponiveis
     */

//    private List<String> getOperadoresDisponiveis(String fila, String type) {
//	List<String> result = null;
//	log.debug("Buscando lista de operadores disponiveis no Snep");
//	// realizar a chamado ao snep para verificar quais operadores estÃ£o
//	// livres no momento
//	String message = "";
//	String[] params = null;
//	String url = linkSnep + "/snep/modules/services/api/?service=QueueStatus&queue=" + fila + "&type=" + type;
//	if (fila != null && !fila.isEmpty()) {
//	    StringBuilder buffer = this.callUrl(url);
//	    String resultStr = buffer.toString();
//	    resultStr = resultStr.replaceAll("\\{", "");
//	    resultStr = resultStr.replaceAll("\\}", "");
//	    resultStr = resultStr.replaceAll("\"", "");
//	    resultStr = resultStr.replaceAll(":free", "");
//	    resultStr = resultStr.replaceAll(":paused", "");
//	    resultStr = resultStr.replaceAll(":busy", "");
//	    resultStr = resultStr.replaceAll(":all", "");
//	    params = resultStr.split(",");
//	    if (params[0] != null && !params[0].isEmpty()) {
//		message = "Chamada efetuada a partir do ramal " + fila + ".";
//	    } else {
//		String erro[] = params[1].split(":");
//		message = "Erro ao efetuar chamada.\n" + erro[1];
//	    }
//	}
//	log.debug("Solicitando ligaÃ§Ã£o para Snep! Verificar status dos agentes na fila: " + fila + " mensagem : " + message);
//	// remover esta linha quando integraÃ§Ã£o estiver implementada
//	result = new ArrayList<String>();
//	for (String agente : params) {
//	    if (!agente.contains("fail") || !agente.contains("message:Nenhum membro encontrato para a relacao fila x status"))
//		result.add(agente);
//	}
//	return result;
//    }

    /*
     * realiza a pausa do operador no snep evitando assim que ele receba
     * ligaÃ§Ãµes da fila
     */
    public boolean pausarOperador(String ramal) {
	log.debug("Pausadno operador no Snep! Ramal: " + ramal);
	// realizar a chamado ao snep para pausar este operador no snep e
	// retorna se conseguiu pausar
	String message = "";
	boolean pause = false;
	String ramalDestino = ramal;
	// 192.168.20.241
	String url = linkSnep + "/snep/modules/services/api/?service=QueueMember&action=pause&pause=5&agent=" + ramal;
	if (ramalDestino != null && !ramalDestino.isEmpty() && ramalDestino.length() == 4) {
	    StringBuilder buffer = this.callUrl(url);
	    String result = buffer.toString();
	    result = result.replaceAll("\\{", "");
	    result = result.replaceAll("\\}", "");
	    String[] params = result.split(",");
	    // System.out.println("Pausa " + result);
	    if (params[0] != null && params[0].contains("Success")) {
		message = "Chamada efetuada a partir do ramal " + ramalDestino + ".";
		pause = true;
	    } else {
		String erro[] = params[1].split(":");
		message = "Erro ao efetuar chamada.\n" + erro[1];
		pause = false;
	    }
	}
	log.debug("Solicitando ligaÃ§Ã£o para Snep! Para pausaro Ramal:" + ramal + " mensagem : " + message);
	return pause;
    }

    /*
     * libera o opeador que estÃ¡ pausado no snep
     */
    public boolean liberarOperador(String ramal) {
	log.debug("Liberando operador no Snep! Ramal: " + ramal);
	// realizar a chamado ao snep para liberar o operador pausado
	String message = "";
	boolean unPause = false;
	String ramalDestino = ramal;
	String url = linkSnep + "/snep/modules/services/api/?service=QueueMember&action=unpause&agent=" + ramal;
	if (ramalDestino != null && !ramalDestino.isEmpty() && ramalDestino.length() == 4) {
	    StringBuilder buffer = this.callUrl(url);
	    String result = buffer.toString();
	    result = result.replaceAll("\\{", "");
	    result = result.replaceAll("\\}", "");
	    String[] params = result.split(",");
	    if (params[0] != null && params[0].contains("Success")) {
		message = "Chamada efetuada a partir do ramal " + ramalDestino + ".";
		unPause = true;
	    } else {
		String erro[] = params[1].split(":");
		message = "Erro ao efetuar chamada.\n" + erro[1];
		unPause = false;
	    }
	}
	log.debug("Solicitando ligaÃ§Ã£o para Snep! Para retirar a pausa do Ramal:" + ramal + " mensagem : " + message);
	return unPause;
    }

    /*
     * busca uma lista de providenciaVO que Ã© omesmo objeto utilizado pelo
     * CallCenter O objeto ProvidenciaVO contem o nome e o telefone dos contatos
     */
    private LinkedHashSet<ProvidenciaVO> telefoneCliente(EventoVO eventoVO) {
	// buscar a lista de telefones que devem ser chamados em ordem de
	// prioridade
	// LinkedHashSet mantÃ©m a ordem de inserÃ§Ã£o dos objetos
	LinkedHashSet<ProvidenciaVO> result = null;
	List<String> listaTelefone = null;
	ProvidenciaVO providenciaVO = null;
	log.debug("Buscando a lista de telefones do cliente para ligar! " + eventoVO.getCodigoEvento());
	Connection connection = null;
	PreparedStatement statement = null;
	ResultSet resultSet = null;
	try {
	    connection = PersistEngine.getConnection("SIGMA90");
	    StringBuilder sql = new StringBuilder();
	    sql.append(" SELECT DBC.FONE1 AS TELEFONE1, DBC.FONE2 AS TELEFONE2, DBP.FONE1 AS TELEFONE3, DBP.FONE2 AS TELEFONE4  ");
	    sql.append(" FROM dbPROVIDENCIA DBP WITH (NOLOCK)  ");
	    sql.append(" RIGHT JOIN dbCENTRAL DBC WITH (NOLOCK)  ON (DBC.CD_CLIENTE =  DBP.CD_CLIENTE) ");
	    sql.append(" WHERE DBC.ctrl_central = 1 AND DBC.CD_CLIENTE = " + eventoVO.getCodigoCliente() + " ORDER BY DBP.NU_PRIORIDADE_NIVEL2 ");
	    statement = connection.prepareStatement(sql.toString());
	    resultSet = statement.executeQuery();
	    providenciaVO = new ProvidenciaVO();
	    listaTelefone = new ArrayList<String>();
	    result = new LinkedHashSet<ProvidenciaVO>();
	    providenciaVO.setNome(eventoVO.getNomeEvento());
	    int contador = 0;
	    while (resultSet.next()) {
		if (NeoUtils.safeIsNotNull(resultSet.getString("TELEFONE1")) && !listaTelefone.contains(resultSet.getString("TELEFONE1"))) {
		    listaTelefone.add(new String(resultSet.getString("TELEFONE1")));
		}
		if (NeoUtils.safeIsNotNull(resultSet.getString("TELEFONE2")) && !listaTelefone.contains(resultSet.getString("TELEFONE2"))) {
		    listaTelefone.add(new String(resultSet.getString("TELEFONE2")));
		}
		if (NeoUtils.safeIsNotNull(resultSet.getString("TELEFONE3")) && !listaTelefone.contains(resultSet.getString("TELEFONE3"))) {
		    listaTelefone.add(new String(resultSet.getString("TELEFONE3")));
		}
		if (NeoUtils.safeIsNotNull(resultSet.getString("TELEFONE4")) && !listaTelefone.contains(resultSet.getString("TELEFONE4"))) {
		    listaTelefone.add(new String(resultSet.getString("TELEFONE4")));
		}
		contador++;
	    }
	    if (listaTelefone != null && !listaTelefone.isEmpty()) {
		contador = 0;
		for (String tel : listaTelefone) {
		    try {
			String formato = "([0-9]{2}?)[0-9]{4}?-[0-9]{4}?";
			if ((tel == null) || (tel.length() != 13) || (!tel.matches(formato))) {
			    tel = tel.replace("(", "").replace(")", "").replace("-", "").trim();
			    tel = tel.replaceAll(" ", "");
			    tel = tel.trim();
			    //Long numero = Long.parseLong(tel);
			    contador++;
			    if (contador == 1)
				providenciaVO.setTelefone1(tel);
			    if (contador == 2)
				providenciaVO.setTelefone2(tel);
			    if (contador == 3)
				providenciaVO.setTelefone3(tel);
			    if (contador == 4)
				providenciaVO.setTelefone4(tel);
			    if (contador == 5)
				providenciaVO.setTelefone5(tel);
			    if (contador == 6)
				providenciaVO.setTelefone6(tel);
			}
		    } catch (NumberFormatException e) {
			log.error("Erro telefone  : " + tel + " Cliente : " + eventoVO.getCodigoCliente() + " Erro: " + e.getMessage());
			continue;
		    }
		}
		providenciaVO.setCodigoCliente(Integer.parseInt(eventoVO.getCodigoCliente()));
		result.add(providenciaVO);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("Erro ao efetuar a ligaÃ§Ã£o eventos X406 XXX2 XXX5" + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(connection, statement, resultSet);
	}
	return result;
    }

    /*
     * logica que vai selecionar qual proximo numero do cliente e realiza a
     * ligacao ou finaliza o evento caso nÃ£o tenham mais ligacoes
     */
    private void ligarParaCliente(String codigoHistorico) {
	
	Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();
	
	System.out.println("#"+codigoHistorico+"# 5.8.1 Iniciou metodo ligacao para cliente Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");
	
	try {
	    CallAlertEventVO alertEventVo = OrsegupsAlertEventEngine.callingAlertEventVO.get(codigoHistorico);
	    Boolean closeEvent = false;
	    if (alertEventVo != null && alertEventVo.getNumerosChamar() != null && !alertEventVo.getNumerosChamar().isEmpty()) {
		// ProvidenciaVO providencia =
		// alertEventVo.getNumerosChamar().iterator().next();
		String numeroChamar = this.numeroChamar(alertEventVo);

		if (numeroChamar != null) {
		    if (!numeroChamar.startsWith("0")) {
			numeroChamar = verificarPrefixo(numeroChamar);
		    }

		    /**
		     * Adição do novo digito para telefones móveis
		     */
		    List<NeoObject> listaSubstituicao = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TINonoDigito"));

		    if (listaSubstituicao != null && !listaSubstituicao.isEmpty()) {

			NeoObject objAdicionar = listaSubstituicao.get(0);

			EntityWrapper wAdicionar = new EntityWrapper(objAdicionar);

			boolean adicionar = (boolean) wAdicionar.findField("adicionar").getValue();

			if (adicionar) {
			    if (numeroChamar.startsWith("0")) {
				numeroChamar = numeroChamar.substring(1);
			    }
			    if (numeroChamar.length() == 8) {
				if (numeroChamar.startsWith("7") || numeroChamar.startsWith("8") || numeroChamar.startsWith("9")) {
				    numeroChamar = "9" + numeroChamar;
				}
			    } else if (numeroChamar.length() == 10) {
				String subFone = numeroChamar.substring(2);

				if (subFone.startsWith("7") || subFone.startsWith("8") || subFone.startsWith("9")) {
				    subFone = "9" + subFone;
				    numeroChamar = numeroChamar.substring(0, 2) + subFone;
				}
			    }
			}

		    }

		    log.debug("OrsegupsAlertEventEngine SIGMA EVENTO HISTORICO getLigacaoSnep: " + alertEventVo.getRamalOperador() + " " + numeroChamar.trim());
		    
		    System.out.println("#"+codigoHistorico+"# 5.8.2 Iniciou updateInfoWindow Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");

		    // atualiza os dados do que vai ser impresso em tela
		    alertEventVo.updateInfoWindow();
		    
		    System.out.println("#"+codigoHistorico+"# 5.8.3 Finalizou updateInfoWindow Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");
		    // serializa o vo para json
		    String alertVoJSON = "";
		    Gson gson = new Gson();
		    // alertVoJSON = gson.toJson(null);
		    // dispara a mensagem JMS
		    log.debug("NeoJMSPublisher ANTES DE EXECUTAR: Agente:" + alertEventVo.getRamalOperador() + " Conta: " + alertEventVo.getCallEventoVO().getCodigoCentral() + " - Evento: "
			    + alertEventVo.getCallEventoVO().getCodigoEvento() + " Ligando para: " + numeroChamar);
		    // NeoJMSPublisher.getInstance().sendAlertEventMessage(URLEncoder.encode(alertVoJSON,
		    // "UTF-8"), alertEventVo.getRamalOperador());
		    alertVoJSON = gson.toJson(alertEventVo);
		    try {
			System.out.println("#"+codigoHistorico+"# 5.8.4 Iniciou chamada JMS Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");
			NeoJMSPublisher.getInstance().sendAlertEventMessage(URLEncoder.encode(alertVoJSON, "UTF-8"), alertEventVo.getRamalOperador());
			System.out.println("#"+codigoHistorico+"# 5.8.5 Finalizou chamada JMS Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");
		    } catch (UnsupportedEncodingException e) {
			// Auto-generated catch block
			e.printStackTrace();
			log.error("ERRO VERIFICAR EVENTOS ramalFlag: verificarLigacaoPendente - " + e.getMessage());

		    } catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		    }

		    if (!alertEventVo.getCallEventoVO().getCodigoEvento().equals("XVID") && !alertEventVo.getCallEventoVO().getCodigoEvento().equals("XVD2")) {
			System.out.println("#"+codigoHistorico+"# 5.8.6 Iniciou busca ligacao Snep Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");
			this.getLigacaoSnep(alertEventVo.getRamalOperador(), numeroChamar.trim());
			System.out.println("#"+codigoHistorico+"# 5.8.7 Finalizou busca ligacao Snep Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");
			// salva em log
			String textoLog = "Ligando para cliente " + numeroChamar + " a partir do ramal " + alertEventVo.getRamalOperador() + "\r\n";
			System.out.println("#"+codigoHistorico+"# 5.8.9 Iniciou saveLog Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");
			saveLog(alertEventVo.getCallEventoVO().getCodigoCliente(), alertEventVo.getCallEventoVO().getCodigoHistorico(), textoLog);
			System.out.println("#"+codigoHistorico+"# 5.8.9 Finalizou saveLog Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");
			if (log.isDebugEnabled())
			    log.debug("NeoJMSPublisher DEPOIS DE EXECUTAR: Ramal:" + alertEventVo.getRamalOperador() + " Conta: " + alertEventVo.getCallEventoVO().getCodigoCentral() + " - Evento: "
				    + alertEventVo.getCallEventoVO().getCodigoEvento() + " Ligado para: " + numeroChamar);
		    }

		} else {
		    closeEvent = true;
		}
	    } else {
		closeEvent = true;
	    }

	    if (closeEvent) {
		System.out.println("#"+codigoHistorico+"# 5.8.10 Iniciou fecharEvento Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");
		this.fecharEvento(false, codigoHistorico, false, null, false);
		System.out.println("#"+codigoHistorico+"# 5.8.11 Finalizou fecharEvento Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("ERRO NeoJMSPublisher DEPOIS DE EXECUTAR: " + e.getMessage());

	}
    }

    /*
     * busca por um numero para chamar na lista atÃ© encontrar, caso ocntrÃ¡rio
     * retorna null,
     */
    private String numeroChamar(CallAlertEventVO alertEventVo) {
	if (log.isDebugEnabled())
	    log.debug("Buscando proximo numero a ligar! " + alertEventVo.getCallEventoVO().getCodigoHistorico());
	String numeroChamar = null;
	Map<Integer, String> listaTelefones = null;
	ProvidenciaVO providencia = alertEventVo.getNumerosChamar().iterator().next();
	listaTelefones = new HashMap<Integer, String>();
	if (NeoUtils.safeIsNotNull(providencia.getTelefone1()) && !providencia.getTelefone1().isEmpty())
	    listaTelefones.put(0, providencia.getTelefone1().replaceAll(" ", ""));
	if (NeoUtils.safeIsNotNull(providencia.getTelefone2()) && !providencia.getTelefone2().isEmpty())
	    listaTelefones.put(1, providencia.getTelefone2().replaceAll(" ", ""));
	if (NeoUtils.safeIsNotNull(providencia.getTelefone3()) && !providencia.getTelefone3().isEmpty())
	    listaTelefones.put(2, providencia.getTelefone3().replaceAll(" ", ""));
	if (NeoUtils.safeIsNotNull(providencia.getTelefone4()) && !providencia.getTelefone4().isEmpty())
	    listaTelefones.put(3, providencia.getTelefone4().replaceAll(" ", ""));
	if (NeoUtils.safeIsNotNull(providencia.getTelefone5()) && !providencia.getTelefone5().isEmpty())
	    listaTelefones.put(4, providencia.getTelefone5().replaceAll(" ", ""));
	if (NeoUtils.safeIsNotNull(providencia.getTelefone6()) && !providencia.getTelefone6().isEmpty())
	    listaTelefones.put(5, providencia.getTelefone6().replaceAll(" ", ""));
	if (alertEventVo.getUltimoNumero() == null) {
	    numeroChamar = listaTelefones.get(0);
	    alertEventVo.setUltimoNumero(0);
	} else {

	    if (alertEventVo.getUltimoNumero() < listaTelefones.size() - 1) {
		numeroChamar = listaTelefones.get(alertEventVo.getUltimoNumero() + 1);
		alertEventVo.setUltimoNumero(alertEventVo.getUltimoNumero() + 1);
	    } else {
		numeroChamar = listaTelefones.get(0);
		alertEventVo.setUltimoNumero(0);
	    }
	}
	alertEventVo.setUltimoNumeroChamado(numeroChamar);
	alertEventVo.getNumerosJaChamados().add(numeroChamar);
	// log.debug("Numero chamada snep: " + numeroChamar);
	return numeroChamar;
    }

    /*
     * adiciona mensagem no log do evento
     */
    private void addEvetLog(EventoVO eventoVO, String message) {
	// logica para persistir a mensagem no log da viatura
	saveLog(eventoVO.getCodigoCliente(), eventoVO.getCodigoHistorico(), message);
	log.debug("Adicionando ao log ao evento! " + message);
    }

    public void cadastrarEvetLog(String codigoHistorico, String cdCliente, String message) {
	saveLog(cdCliente, codigoHistorico, message);
	log.debug("Adicionando ao log ao evento! " + message);
    }

    private void saveLog(String cdCliente, String cdHistorico, String textoLog) {
	Connection conn = null;
	PreparedStatement st = null;
	ResultSet rs = null;
	String texto = null;
	try {
	    if (NeoUtils.safeIsNotNull(textoLog) && !textoLog.isEmpty()) {
		texto = textoLog;
		if (!textoLog.contains(": Op. CM - "))
		    texto = NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss ") + ": - " + textoLog + "###";
		conn = PersistEngine.getConnection("SIGMA90");
		StringTokenizer token = new StringTokenizer(texto, ";;");
		conn.setAutoCommit(false);
		while (token.hasMoreTokens()) {
		    String textoLocal = token.nextToken();
		    StringBuilder sqlUpdate = new StringBuilder();
		    sqlUpdate
			    .append(" UPDATE HISTORICO  SET  TX_OBSERVACAO_FECHAMENTO = CASE WHEN(TX_OBSERVACAO_FECHAMENTO IS NULL) THEN '' ELSE TX_OBSERVACAO_FECHAMENTO END  + (CHAR(10) + CHAR(13)) + ?  WHERE CD_HISTORICO = ? ");
		    st = conn.prepareStatement(sqlUpdate.toString());
		    st.setString(1, textoLocal);
		    st.setString(2, cdHistorico);
		    int update = st.executeUpdate();

		    int updateCount = 0;

		    while (update < 1 && updateCount < 2) {
			update = st.executeUpdate();
			updateCount++;
		    }

		}
		conn.commit();
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("##### ERRO SALVAR LOG EVENT ALERT: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
	    try {
		conn.rollback();
	    } catch (SQLException e1) {

		e1.printStackTrace();
	    }
	} finally {
	    OrsegupsUtils.closeConnection(conn, st, rs);
	}

    }

    /*
     * logica que finailza o evento caso naÃµ tenha conseguido contato com o
     * cliente ou a palavra chave esteja ok
     */
    public void fecharEvento(Boolean contatoOk, String codigoHistorico, Boolean callAt, String ramal, Boolean alteracao) {
	if (log.isDebugEnabled())
	    log.debug("Fechar evento! ContatoOk: " + contatoOk);
	CallAlertEventVO alertEventVo = OrsegupsAlertEventEngine.callingAlertEventVO.get(codigoHistorico);
	if (alertEventVo != null) {

	    if (alteracao) {
		if (alertEventVo.getCallEventoVO().getCodigoEvento().equals("XVID") || alertEventVo.getCallEventoVO().getCodigoEvento().equals("XVD2")) {
		    this.addEvetLog(alertEventVo.getCallEventoVO(), " Evento em espera, mas não finalizado no SIGMA. #EmEspera #ComAlteracao");
		} else {
		    this.addEvetLog(alertEventVo.getCallEventoVO(),
			    " Evento em espera após ser feito o procedimento de ligação. Mas não finalizado no SIGMA. Chamada realizada a partir do ramal " + alertEventVo.getRamalOperador() + ". #EmEspera #ComAlteracao");
		}
		this.emEsperaEventoSigma(alertEventVo);
	    } else if (contatoOk && callAt) {
		if (alertEventVo.getCallEventoVO().getCodigoEvento().equals("XVID") || alertEventVo.getCallEventoVO().getCodigoEvento().equals("XVD2")) {
		    this.addEvetLog(alertEventVo.getCallEventoVO(), " Atendimento encerrado. #FecharEvento");
		} else {
		    this.addEvetLog(alertEventVo.getCallEventoVO(), " Atendimento encerrado. Chamada realizada a partir do ramal " + alertEventVo.getRamalOperador() + ". #FecharEvento");
		}

		this.fecharEventoSigma(alertEventVo);

	    } else if (contatoOk && !callAt) {
		if (alertEventVo.getCallEventoVO().getCodigoEvento().equals("XVID") || alertEventVo.getCallEventoVO().getCodigoEvento().equals("XVD2")) {
		    this.addEvetLog(alertEventVo.getCallEventoVO(), " Evento em espera, mas não finalizado no SIGMA. #EmEspera");
		} else {
		    this.addEvetLog(alertEventVo.getCallEventoVO(),
			    " Evento em espera após ser feito o procedimento de ligação. Mas não finalizado no SIGMA. Chamada realizada a partir do ramal " + alertEventVo.getRamalOperador() + ". #EmEspera");
		}
		this.emEsperaEventoSigma(alertEventVo);
	    } else if (!contatoOk && callAt) {
		if (alertEventVo.getCallEventoVO().getCodigoEvento().equals("XVID") || alertEventVo.getCallEventoVO().getCodigoEvento().equals("XVD2")) {
		    this.addEvetLog(alertEventVo.getCallEventoVO(), " Evento encerrado após tentativas sem sucesso de contato com o cliente, ou inconsistência durante o contato. #TratarSigma");
		} else {
		    this.addEvetLog(alertEventVo.getCallEventoVO(),
			    " Evento encerrado após tentativas sem sucesso de contato com o cliente, ou inconsistência durante o contato, ligação. Chamada realizada a partir do ramal " + alertEventVo.getRamalOperador()
				    + ". #TratarSigma");
		}

	    }

	    this.getRetiraPausaRamal(alertEventVo.getRamalOperador());
	    String ramalStr = "";
	    String contaStr = "";
	    if (alertEventVo.getRamalOperador() != null)
		ramalStr = alertEventVo.getRamalOperador();
	    if (alertEventVo.getCallEventoVO().getCodigoCentral() != null && alertEventVo.getCallEventoVO().getParticao() != null)
		contaStr = "Conta : " + alertEventVo.getCallEventoVO().getCodigoCentral() + "[" + alertEventVo.getCallEventoVO().getParticao() + "] Evento : " + alertEventVo.getCallEventoVO().getCodigoEvento()
			+ " Historico : " + alertEventVo.getCallEventoVO().getCodigoHistorico() + " Cliente : " + alertEventVo.getCallEventoVO().getCodigoCliente();
	    this.alteraDataUltimaLigacaoRamal(ramalStr, contaStr);
	} else if (ramal != null) {
	    this.getRetiraPausaRamal(ramal);
	    this.alteraDataUltimaLigacaoRamal(ramal, "CallAlertEventVO null: Histórico: " + codigoHistorico);
	}
	
	if (codigoHistorico != null && OrsegupsAlertEventEngine.callingAlertEventVO.containsKey(codigoHistorico)){
	    OrsegupsAlertEventEngine.callingAlertEventVO.remove(codigoHistorico);	    
	}
	
	try {
	   
	    Gson gson = new Gson();
	    String alertVoJSON = gson.toJson(null);
	    NeoJMSPublisher.getInstance().sendAlertEventMessage(URLEncoder.encode(alertVoJSON, "UTF-8"), ramal);

	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("##### ERRO NeoJMSPublisher: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
	}
    }

    /*
     * fechar o evento no banco do sigma
     */
    private void fecharEventoSigma(CallAlertEventVO alertEventVo) {
	Connection conn = null;
	PreparedStatement preparedStatementHSelect = null;
	PreparedStatement preparedStatementUpdate = null;
	ResultSet rsH = null;
	List<String> listaHitorico = null;
	try {

	    if (verificaEventoHistorico(alertEventVo.getCallEventoVO().getCodigoHistorico())) {

		String usuario = getUsuarioSigma();

		conn = PersistEngine.getConnection("SIGMA90");

		String selecHtSQL = " SELECT CD_HISTORICO FROM HISTORICO WITH (NOLOCK)  WHERE CD_HISTORICO_PAI = ? ";

		String updateSQL = "  UPDATE HISTORICO SET FG_STATUS = 4 , DT_FECHAMENTO = GETDATE() ,CD_USUARIO_FECHAMENTO = ? WHERE CD_HISTORICO = ? ";
		conn.setAutoCommit(false);
		String hPai = alertEventVo.getCallEventoVO().getCodigoHistorico();
		preparedStatementHSelect = conn.prepareStatement(selecHtSQL.toString());
		preparedStatementHSelect.setString(1, hPai);

		listaHitorico = new ArrayList<String>();
		rsH = preparedStatementHSelect.executeQuery();
		while (rsH.next()) {
		    if (NeoUtils.safeIsNotNull(rsH.getString("CD_HISTORICO")))
			listaHitorico.add(rsH.getString("CD_HISTORICO"));
		}

		for (String cdHistorico : listaHitorico) {
		    preparedStatementUpdate = conn.prepareStatement(updateSQL.toString());
		    preparedStatementUpdate.setString(1, usuario);
		    preparedStatementUpdate.setString(2, cdHistorico);
		    preparedStatementUpdate.executeUpdate();
		    

		}
		if (preparedStatementUpdate != null && !preparedStatementUpdate.isClosed()){
		    preparedStatementUpdate.close();
		}
		preparedStatementUpdate = conn.prepareStatement(updateSQL.toString());
		preparedStatementUpdate.setString(1, usuario);
		preparedStatementUpdate.setString(2, alertEventVo.getCallEventoVO().getCodigoHistorico());
		int update = preparedStatementUpdate.executeUpdate();

		int updateCount = 0;

		while (update < 1 && updateCount < 2) {
		    update = preparedStatementUpdate.executeUpdate();
		    updateCount++;
		}

		conn.commit();

		// String cdUsuario = (String) getUsuarioSigma();
		// conn = PersistEngine.getConnection("SIGMA90");
		// String updateSQLFilho =
		// " UPDATE HISTORICO SET FG_STATUS = 4 , DT_FECHAMENTO = GETDATE() ,CD_USUARIO_FECHAMENTO = ? WHERE CD_HISTORICO_PAI = ? ";
		//
		// String updateSQLPai =
		// " UPDATE HISTORICO SET FG_STATUS = 4 , DT_FECHAMENTO = GETDATE() ,CD_USUARIO_FECHAMENTO = ? WHERE CD_HISTORICO = ? ";
		//
		// conn.setAutoCommit(false);
		//
		// preparedStatementUpdate =
		// conn.prepareStatement(updateSQLFilho.toString());
		// preparedStatementUpdate.setString(1, cdUsuario);
		// preparedStatementUpdate.setString(2,
		// alertEventVo.getCallEventoVO().getCodigoHistorico());
		// preparedStatementUpdate.executeUpdate();
		//
		// preparedStatementUpdatePai =
		// conn.prepareStatement(updateSQLPai.toString());
		// preparedStatementUpdatePai.setString(1, cdUsuario);
		// preparedStatementUpdatePai.setString(2,
		// alertEventVo.getCallEventoVO().getCodigoHistorico());
		// preparedStatementUpdatePai.executeUpdate();
		// conn.setAutoCommit(true);
		// conn.commit();
		log.debug("OrsegupsAlertEventEngine fechar eventos historico :  " + alertEventVo.getCallEventoVO().getCodigoHistorico());

	    }
	} catch (Exception e) {
	    try {
		conn.rollback();
		log.debug("OrsegupsAlertEventEngine fechar eventos rollback historico :  " + alertEventVo.getCallEventoVO().getCodigoHistorico());
	    } catch (SQLException e1) {
		log.error("##### ERRO SALVAR LOG EVENT TRANSAÃ‡ÃƒO fecharEventoSigma: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		e1.printStackTrace();
	    }
	    e.printStackTrace();
	    log.error("##### ERRO SALVAR LOG EVENT ALERT fecharEventoSigma: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
	} finally {
	    OrsegupsUtils.closeConnection(null, preparedStatementHSelect, null);
	    OrsegupsUtils.closeConnection(conn, preparedStatementUpdate, rsH);
	}
	log.debug("Fechando evento no Sigma! " + alertEventVo.getCallEventoVO().getCodigoHistorico());
    }

    private void emEsperaEventoSigma(CallAlertEventVO alertEventVo) {
	// logica para finalizar o evento no sigma
	Connection conn = null;
	PreparedStatement preparedStatementHSelect = null;
	PreparedStatement preparedStatementUpdate = null;
	ResultSet rsH = null;
	List<String> listaHitorico = null;

	try {
	    if (verificaEventoHistorico(alertEventVo.getCallEventoVO().getCodigoHistorico())) {
		String usuario = getUsuarioSigma();

		conn = PersistEngine.getConnection("SIGMA90");

		String selecHtSQL = " SELECT CD_HISTORICO FROM HISTORICO WITH (NOLOCK)  WHERE CD_HISTORICO_PAI = ? ";

		String updateSQL = " UPDATE HISTORICO SET FG_STATUS = 1 , DT_ESPERA = GETDATE() , CD_USUARIO_ESPERA = ? WHERE CD_HISTORICO = ? ";
		conn.setAutoCommit(false);
		String hPai = alertEventVo.getCallEventoVO().getCodigoHistorico();
		preparedStatementHSelect = conn.prepareStatement(selecHtSQL.toString());
		preparedStatementHSelect.setString(1, hPai);

		listaHitorico = new ArrayList<String>();
		rsH = preparedStatementHSelect.executeQuery();
		while (rsH.next()) {
		    if (NeoUtils.safeIsNotNull(rsH.getString("CD_HISTORICO")))
			listaHitorico.add(rsH.getString("CD_HISTORICO"));
		}

		for (String cdHistorico : listaHitorico) {
		    preparedStatementUpdate = conn.prepareStatement(updateSQL.toString());
		    preparedStatementUpdate.setString(1, usuario);
		    preparedStatementUpdate.setString(2, cdHistorico);
		    preparedStatementUpdate.executeUpdate();

		}
		if (preparedStatementUpdate != null && !preparedStatementUpdate.isClosed()){
		    preparedStatementUpdate.close();
		}
		
		preparedStatementUpdate = conn.prepareStatement(updateSQL.toString());
		preparedStatementUpdate.setString(1, usuario);
		preparedStatementUpdate.setString(2, alertEventVo.getCallEventoVO().getCodigoHistorico());
		preparedStatementUpdate.executeUpdate();

		conn.commit();
		log.debug("OrsegupsAlertEventEngine Em Espera " + listaHitorico.size() + " Historico :  " + alertEventVo.getCallEventoVO().getCodigoHistorico());

	    }
	} catch (Exception e) {
	    try {
		conn.rollback();
		log.debug("OrsegupsAlertEventEngine Em Espera rollback " + listaHitorico.size() + " Historico :  " + alertEventVo.getCallEventoVO().getCodigoHistorico());
	    } catch (SQLException e1) {
		log.error("##### ERRO SALVAR LOG EVENT TRANSAÃ‡ÃƒO EM ESPERA emEsperaEventoSigma: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
		e1.printStackTrace();
	    }
	    e.printStackTrace();
	    log.error("##### ERRO SALVAR LOG EM ESPERA emEsperaEventoSigma: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
	} finally {
	    OrsegupsUtils.closeConnection(null, preparedStatementHSelect, rsH);
	    OrsegupsUtils.closeConnection(conn, preparedStatementUpdate, null);
	}
	//

	log.debug("deslocando evento no Sigma! " + alertEventVo.getCallEventoVO().getCodigoHistorico());
    }

    /*
     * solicita para o snep a ligaÃ§Ã£o
     */
    public void solicitarLigacaoSnep(String ramal, String numeroExterno) {
	// adicionar a logica que irÃ¡ realizar a chamada ap Snep
	String message = "";

	String ramalOrigem = null;
	String ramalDestino = null;
	ramalOrigem = ramal;
	ramalDestino = numeroExterno.trim();

	String url = linkSnep + "/snep/services/?service=Dial&agent=" + ramalOrigem + "&exten=" + ramalDestino;
	if (ramalDestino != null && !ramalDestino.isEmpty() && ramalOrigem != null && !ramalOrigem.isEmpty()) {

	    if (ramalDestino.equals(ramalOrigem)) {
		message = "Erro ao efetuar chamada.\nRamal de origem igual ao ramal de destino.";
	    } else {
		StringBuilder buffer = this.callUrl(url);

		String result = buffer.toString();
		result = result.replaceAll("\\{", "");
		result = result.replaceAll("\\}", "");
		String[] params = result.split(",");
		// System.out.println("Ligaca : " + result);
		if (params[0] != null && params[0].contains("ok")) {
		    message = "Chamada efetuada a partir do ramal " + ramalOrigem + " para o ramal " + ramalDestino + ".";

		} else {
		    String erro[] = params[1].split(":");

		    message = "Erro ao efetuar chamada.\n" + erro[1];
		}
	    }

	}
	log.debug("Solicitando ligaÃ§Ã£o para Snep! Ramal:" + ramal + " - Numero externo: " + numeroExterno + " mensagem : " + message);
    }

    /*
     * me que vai realizar o controle de acordo com a aÃ§Ã£o que o operador
     * tomar em tela ou seja se conseguiu contato ou nÃ£o com o cliente
     */
    public void contato(Boolean contatoOk, String codigoHistorico, Boolean callAt, String ramal, Boolean alteracao) {
	log.debug("Contato realizado? " + contatoOk);

	if (alteracao) {
	    this.fecharEvento(contatoOk, codigoHistorico, callAt, ramal, alteracao);
	} else if (contatoOk && callAt) {
	    this.fecharEvento(contatoOk, codigoHistorico, callAt, ramal, alteracao);
	} else if (contatoOk && !callAt) {
	    this.fecharEvento(contatoOk, codigoHistorico, callAt, ramal, alteracao);
	} else if (!contatoOk && callAt) {
	    this.fecharEvento(contatoOk, codigoHistorico, callAt, ramal, alteracao);
	} else {
	    this.ligarParaCliente(codigoHistorico);
	}
    }

    public void clearCache() {
	getCache();
	OrsegupsAlertEventEngine.callingAlertEventVO.clear();
	OrsegupsAlertEventEngine.verificandoEventos = false;
	// OrsegupsAlertEventEngine.instance = null;

    }

    public String getCache() {

	Set<String> chaves = OrsegupsAlertEventEngine.callingAlertEventVO.keySet();
	
	StringBuilder r = new StringBuilder();
	for (String chave : chaves) {
	    if (chave != null) {
		r.append("---------------------CACHE EVENTOS---------------------------------------------------------------------------------- \r\n");
		r.append("Historico: " + chave + " Ramal : " + OrsegupsAlertEventEngine.callingAlertEventVO.get(chave).getRamalOperador() + " Evento : "
			+ OrsegupsAlertEventEngine.callingAlertEventVO.get(chave).getCallEventoVO().getCodigoEvento()+" \r\n");
		r.append("--------------------------------------------------------------------------------------------------------------- \r\n");
	    }
	}
	return r.toString();
    }

    // garante que nÃ£o realizarÃ¡ duas verificaÃ§Ãµes de eventos ao mesmo tempo
    public void verificaEventos(Long tipoAtendimento) {

	Long timeExecFinal = GregorianCalendar.getInstance().getTimeInMillis();

	// medir tempo execucao
	Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();
	
	System.out.println("#"+timeExecFinal+"# 1 Iniciou busca de eventos Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");
	
	EventoVO vo = null;
	List<String> telCal = null;
	telCal = getRamaisDisponiveis(tipoAtendimento);
	
	boolean flag = false;
	if (telCal != null && !telCal.isEmpty())
	    flag = true;
	
	System.out.println("#"+timeExecFinal+"# 2 Buscou ramais disponiveis Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms Ramal livre:"+flag);
	try {

	    if (!OrsegupsAlertEventEngine.verificandoEventos && telCal != null && !telCal.isEmpty()) {
		OrsegupsAlertEventEngine.verificandoEventos = true;
		// tipoEventos = getTipoAtendidmentoEvento(ramal);
		
		System.out.println("#"+timeExecFinal+"# 3 Iniciou busca de evento para o operadores Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");

		vo = new EventoVO();

		vo = (EventoVO) getEvento(telCal, tipoAtendimento);
		
		if (vo != null){
		    System.out.println("#"+timeExecFinal+"#"+vo.getCodigoHistorico()+"# 4 Terminou busca de eventos Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");	    
		}
	
		// verificar se existem os eventos E401 ,xxx2, xxx5 e x406 e
		// chamar o me abaixo passando os eventos
		if (vo != null && !OrsegupsAlertEventEngine.callingAlertEventVO.containsKey(vo.getCodigoHistorico())) {
		    // log.debug("OrsegupsAlertEventEngine SIGMA EVENTO HISTORICO this alert : "
		    // + vo.getCodigoHistorico());
		    System.out.println("#"+timeExecFinal+"#"+vo.getCodigoHistorico()+"# 5 Iniciou alert de eventos Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");
		    this.alert(getDadosAcessoCliente(getDadosProvidencia(vo)), telCal);

		}
		if (vo != null){
		    System.out.println("#"+timeExecFinal+"#"+vo.getCodigoHistorico()+"# 6 Finalizou alert de eventos Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");		    
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("ERRO ROTINA DE DEVIO DE HABITO. " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExecFinal) + "ms Erro : " + e);

	} finally {
	    if (OrsegupsAlertEventEngine.verificandoEventos)
		OrsegupsAlertEventEngine.verificandoEventos = false;
	}
	
	if (vo != null){
	    System.out.println("#"+timeExecFinal+"#"+vo.getCodigoHistorico()+"# 7 Finalizou verificacao de eventos Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");	    
	    if (GregorianCalendar.getInstance().getTimeInMillis() - timeExec > 5000){
		System.out.println("#"+timeExecFinal+"#warning1#"+vo.getCodigoHistorico()+"# 7 Finalizou verificacao de eventos Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");
	    }
	}
	
	log.warn("FINALIZANDO ROTINA DE DEVIO DE HABITO. " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExecFinal) + "ms");
	log.debug("6Âº verificaEventos final - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExecFinal) + "ms Atendimento - " + tipoAtendimento);
    }

    // CONSULTA EVENTOS TABELA HISTORICO SIGMA
    private EventoVO getEvento(List<String> telCal, Long tipoAtendimento) {
	
	Long timeExecFinal = GregorianCalendar.getInstance().getTimeInMillis();
	
	String[] args = null;
	EventoVO resultVo = null;
	System.out.println("#"+timeExecFinal+"# 4.1 Iniciou busca de evento para o operadores Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExecFinal) + "ms - NumTel: "+telCal.size() );
	for (String valor : telCal) {

	    args = valor.split(";");
	    Long grupoCliente = null;
	    if (args != null){
		System.out.println("#"+timeExecFinal+"# 4.1.1 Iniciou a busca do grupo. Ramal: "+args[0]);
		grupoCliente = getGrupoRamal(args[0]);
		System.out.println("#"+timeExecFinal+"# 4.1.2 Buscou grupo. Ramal: "+args[0]+" - Grupo:"+grupoCliente);
		GregorianCalendar gc = new GregorianCalendar();
		if (grupoCliente != null){
		    if (grupoCliente == 220L){ // Balaroti
			System.out.println("#"+timeExecFinal+"# 4.1.3 Ramal: "+args[0]+" - Grupo:"+grupoCliente+" Balaroti");
			int hora = gc.get(Calendar.HOUR_OF_DAY);
			if (hora < 19 && hora > 7){
			    grupoCliente = null;
			}
		    }else if (grupoCliente == 221L){ // Globo
			System.out.println("#"+timeExecFinal+"# 4.1.4 Ramal: "+args[0]+" - Grupo:"+grupoCliente+" Globo");
			int dia = gc.get(Calendar.DAY_OF_WEEK);
			int hora = gc.get(Calendar.HOUR_OF_DAY);
			if (dia > 1 && dia < 7){
			    if (hora < 18 && hora > 8){
				grupoCliente = null;
			    }
			}
		    }else{
			System.out.println("#"+timeExecFinal+"# 4.1.5 Ramal: "+args[0]+" - Grupo:"+grupoCliente+" sem grupo");
			grupoCliente = null;
		    }
		}
	    }
	    
	    String eventos = "";
	    int contador = 0;
	    QLGroupFilter groupFilter = new QLGroupFilter("AND");
	    groupFilter.addFilter(new QLEqualsFilter("habilitado", Boolean.TRUE));
	    List<NeoObject> neoObjects = PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMAEventos"), groupFilter);
	    if ((neoObjects != null) && (!neoObjects.isEmpty())) {
		for (NeoObject neoObject : neoObjects) {
		    EntityWrapper neoObjectRegional = new EntityWrapper(neoObject);
		    if (contador > 0) {
			eventos = eventos + ",";
		    }
		    eventos = eventos + "'" + (String) neoObjectRegional.getValue("evento") + "'";

		    contador++;
		}
	    }
	    System.out.println("#"+timeExecFinal+"# 4.2 Iniciou busca de evento no e-form SIGMAEventos Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExecFinal) + "ms " );
	    String sqlX406 = " (h.CD_EVENTO = 'X406'  AND h.CD_HISTORICO_PAI IS NULL AND h.FG_STATUS IN (5) ) ";
	    String sqlE401 = " (h.CD_EVENTO = 'E401' AND h.CD_CODE = 'DLC' AND h.CD_HISTORICO_PAI IS NULL AND h.FG_STATUS IN (5) ) ";
	    String sqlXXX5 = " (h.CD_EVENTO = 'XXX5' AND H.CD_CODE = 'DST'  AND h.CD_HISTORICO_PAI IS NULL AND h.FG_STATUS IN (5)  ) ";
	    String sqlXXX2 = " (h.CD_EVENTO = 'XXX2' AND h.CD_CODE = 'NAR'  AND h.CD_HISTORICO_PAI IS NULL AND h.FG_STATUS IN (5) ) ";
	    String sqlXXX1 = " (h.CD_EVENTO = 'XXX1' AND (h.CD_CODE = 'X1A' OR h.CD_CODE = 'X1B' OR h.CD_CODE = 'TNR')  AND h.CD_HISTORICO_PAI IS NULL AND h.FG_STATUS IN (5) ) ";
	    String sqlXVID = " (h.CD_EVENTO = ('XVID') AND h.CD_HISTORICO_PAI IS NULL AND h.FG_STATUS IN (0) ) ";
	    String sqlXVD2 = " (h.CD_EVENTO = ('XVD2') AND h.CD_HISTORICO_PAI IS NULL AND h.FG_STATUS IN (0) ) ";
	    String sqlXRED = " (h.CD_EVENTO = 'XRED' AND h.CD_HISTORICO_PAI IS NULL AND h.FG_STATUS IN (0) ) ";
	    String sqlXPA = " (h.CD_CODE = 'XPA' AND h.CD_HISTORICO_PAI IS NULL AND h.FG_STATUS IN (0,1,5) )";

	    String sqlEventos = "";
	    Map<String, String> mapEventos = new HashMap<String, String>();
	    
	    if (args != null){
		
		if (eventos != null && eventos.contains("X406") && args[1].contains("X406")) {
		    mapEventos.put("X406", sqlX406);
		}
		if (eventos != null && eventos.contains("E401") && args[1].contains("E401")) {
		    mapEventos.put("E401", sqlE401);
		}
		if (eventos != null && eventos.contains("XXX5") && args[1].contains("XXX5")) {
		    mapEventos.put("XXX5", sqlXXX5);
		}
		if (eventos != null && eventos.contains("XXX2") && args[1].contains("XXX2")) {
		    mapEventos.put("XXX2", sqlXXX2);
		}
		if (eventos != null && eventos.contains("XXX1") && args[1].contains("XXX1")) {
		    mapEventos.put("XXX1", sqlXXX1);
		}
		if (eventos != null && eventos.contains("XVID") & args[1].contains("XVID")) {
		    mapEventos.put("XVID", sqlXVID);
		}
		if (eventos != null && eventos.contains("XRED") & args[1].contains("XRED")) {
		    mapEventos.put("XRED", sqlXRED);
		}	    
		if (eventos != null && eventos.contains("XPA") & args[1].contains("XPA")) {
		    mapEventos.put("XPA", sqlXPA);
		}
		if (eventos != null && eventos.contains("XVD2") & args[1].contains("XVD2")) {
		    mapEventos.put("XVD2", sqlXVD2);
		}

	    }


	    if (mapEventos.values() != null) {
		contador = 0;
		for (String evento : mapEventos.values()) {
		    if (contador > 0)
			sqlEventos += " OR ";

		    sqlEventos += evento;

		    contador++;
		}
	    }

	    if (sqlEventos == null || sqlEventos.equals("")) {
		return null;
	    }
	    StringBuilder sqlStr = new StringBuilder();
	    sqlStr.append(" SELECT TOP(15) h.CD_HISTORICO, h.CD_EVENTO, h.CD_CODE,h.NU_PRIORIDADE, c.CD_CLIENTE, h.FG_STATUS, fe.NM_FRASE_EVENTO,    ");
	    sqlStr.append(" h.DT_RECEBIDO, h.DT_ESPERA_DESLOCAMENTO, c.ID_CENTRAL, c.PARTICAO,   ");
	    sqlStr.append(" c.FANTASIA, c.RAZAO, c.ENDERECO, cid.NOME AS NOMECIDADE, bai.NOME AS NOMEBAIRRO, TX_OBSERVACAO_FECHAMENTO,   ");
	    sqlStr.append(" TX_OBSERVACAO_GERENTE, r.TELEFONE, c.DATANOVAX2, c.COMPLEMENTO, e.NOME AS NOMEESTADO,  r.NM_ROTA , c.ID_EMPRESA, ");
	    sqlStr.append(" c.PERGUNTA AS PERGUNTA, c.RESPOSTA AS RESPOSTA, c.RESPONSAVEL AS RESPONSAVEL, c.EMAILRESP AS EMAIL,c.FONE1, c.FONE2, ");
	    sqlStr.append(" c.cgccpf  , c.cep, em.NM_RAZAO_SOCIAL, c.PROVIDENCIA,c.OBSTEMP, c.FANTASIA, EMP.NM_COMPLEMENTO, c.CD_GRUPO_CLIENTE   ");
	    sqlStr.append(" FROM HISTORICO h  INNER JOIN dbCENTRAL c WITH (NOLOCK) ON c.CD_CLIENTE = h.CD_CLIENTE   ");
	    sqlStr.append(" INNER JOIN EMPRESA EMP ON EMP.CD_EMPRESA = c.ID_EMPRESA ");
	    sqlStr.append(" INNER JOIN HISTORICO_FRASE_EVENTO fe WITH (NOLOCK)  ON fe.CD_FRASE_EVENTO = h.CD_FRASE_EVENTO LEFT JOIN dbBAIRRO bai WITH (NOLOCK) ");
	    sqlStr.append(" ON bai.ID_BAIRRO = c.ID_BAIRRO  ");
	    sqlStr.append(" LEFT JOIN dbCIDADE cid WITH (NOLOCK)  ON cid.ID_CIDADE = c.ID_CIDADE ");
	    sqlStr.append(" INNER JOIN ROTA r WITH (NOLOCK) ON r.CD_ROTA = c.ID_ROTA ");
	    sqlStr.append(" INNER JOIN dbESTADO e WITH (NOLOCK)  ON e.ID_ESTADO = cid.ID_ESTADO INNER JOIN EMPRESA em WITH (NOLOCK) ON em.CD_EMPRESA = c.ID_EMPRESA   ");
	    sqlStr.append(" WHERE (" + sqlEventos + ")   ");
	    sqlStr.append(" AND (h.TX_OBSERVACAO_FECHAMENTO NOT LIKE '%#FecharEvento%' AND h.TX_OBSERVACAO_FECHAMENTO NOT LIKE '%#EmEspera%' AND h.TX_OBSERVACAO_FECHAMENTO NOT LIKE '%#TratarSigma%' )   ");

	    //Adicionado exceção para XVID solicitação da tarefa simples 1073918,
	    	    
	    if (tipoAtendimento != null && tipoAtendimento.equals(1L)) {
		sqlStr.append(" AND NOT EXISTS(select ass.CD_CLIENTE from ASSOC_USUARIO_CENTRAL ass where ass.CD_USUARIO IN (" + USUARIO_CORPORATIVO + ") AND ass.CD_CLIENTE = CASE WHEN (H.CD_EVENTO='XVID' OR H.CD_EVENTO='XVD2') THEN 9999999 ELSE h.CD_CLIENTE END  ) ");
	    } else if (tipoAtendimento != null && tipoAtendimento.equals(2L)) {
		sqlStr.append(" AND (EXISTS(select ass.CD_CLIENTE from ASSOC_USUARIO_CENTRAL ass where ass.CD_USUARIO IN (" + USUARIO_CORPORATIVO + ") AND h.CD_CLIENTE = ass.CD_CLIENTE  ) OR (c.CD_GRUPO_CLIENTE IN (220,221))) ");
	    }
	    
	    sqlStr.append(" AND ((c.FONE1 is not null or c.FONE2 is not null)  ");
	    sqlStr.append(" OR EXISTS(SELECT PROV.FONE1, PROV.FONE2 FROM  dbPROVIDENCIA AS PROV WITH (NOLOCK) ");
	    sqlStr.append("	WHERE PROV.CD_CLIENTE = h.CD_CLIENTE AND (PROV.FONE1 is not null or PROV.FONE2 is not null) ))	");
	    	    
	    if (grupoCliente != null){
		sqlStr.append(" AND c.CD_GRUPO_CLIENTE = "+grupoCliente);
	    }else{
		GregorianCalendar gc = new GregorianCalendar();
		int dia = gc.get(Calendar.DAY_OF_WEEK);
		int hora = gc.get(Calendar.HOUR_OF_DAY);
		String grupos = null;
		if (hora >= 19 || hora <= 7) {
		    grupos = "220";
		}
		if (dia > 1 && dia < 7){
		    if (hora >= 18 || hora <= 8){
			if (grupos != null){
			    grupos += ",221";				    
			}else{
			    grupos = "221";
			}
		    }
		}else{
		    if (grupos != null){
			grupos += ",221";				    
		    }else{
			grupos = "221";
		    }
		}
		if (grupos != null){
		    sqlStr.append(" AND (c.CD_GRUPO_CLIENTE not in ("+grupos+") OR c.CD_GRUPO_CLIENTE IS NULL ) ");
		}
	    }
	    sqlStr.append(" ORDER BY  CASE WHEN h.CD_EVENTO = 'E401' AND c.ID_EMPRESA NOT IN (10128,10129,10130,10131,10132,10133,10134,10135,10136,10137,10138,10139,10140,10141,10142,10143) THEN 1 ");
	    sqlStr.append(" WHEN h.CD_EVENTO = 'E401' AND c.ID_EMPRESA IN (10128,10129,10130,10131,10132,10133,10134,10135,10136,10137,10138,10139,10140,10141,10142,10143) THEN 2 ");
	    sqlStr.append(" WHEN h.CD_EVENTO = 'X406' AND c.ID_EMPRESA NOT IN (10128,10129,10130,10131,10132,10133,10134,10135,10136,10137,10138,10139,10140,10141,10142,10143) THEN 3 ");
	    sqlStr.append(" WHEN h.CD_EVENTO = 'X406' AND c.ID_EMPRESA IN (10128,10129,10130,10131,10132,10133,10134,10135,10136,10137,10138,10139,10140,10141,10142,10143) THEN 4   ");
	    sqlStr.append(" WHEN (h.CD_EVENTO = 'XVID' OR h.CD_EVENTO = 'XVD2') THEN 5 ");
	    sqlStr.append(" WHEN h.CD_EVENTO = 'XXX5' AND c.ID_EMPRESA NOT IN (10128,10129,10130,10131,10132,10133,10134,10135,10136,10137,10138,10139,10140,10141,10142,10143) THEN 6 ");
	    sqlStr.append(" WHEN h.CD_EVENTO = 'XXX5' AND c.ID_EMPRESA IN (10128,10129,10130,10131,10132,10133,10134,10135,10136,10137,10138,10139,10140,10141,10142,10143) THEN 7 ");
	    sqlStr.append(" WHEN h.CD_EVENTO = 'XXX2' AND c.ID_EMPRESA NOT IN (10128,10129,10130,10131,10132,10133,10134,10135,10136,10137,10138,10139,10140,10141,10142,10143) THEN  8 ");
	    sqlStr.append(" WHEN h.CD_EVENTO = 'XXX2' AND c.ID_EMPRESA IN (10128,10129,10130,10131,10132,10133,10134,10135,10136,10137,10138,10139,10140,10141,10142,10143) THEN  9 ");
	    sqlStr.append(" WHEN h.CD_EVENTO = 'XXX1' AND h.CD_CODE = 'X1A'  THEN 10 ");
	    sqlStr.append(" WHEN h.CD_EVENTO = 'XXX1' AND h.CD_CODE = 'X1B'  THEN 11 ");
	    sqlStr.append(" WHEN h.CD_EVENTO = 'XRED' THEN 12 ");
	    sqlStr.append(" ELSE 13  END, h.DT_RECEBIDO ASC ");

	    Connection connection = null;
	    PreparedStatement statement = null;
	    ResultSet resultSet = null;
	    List<EventoVO> eventList = null;

	    try {
		System.out.println("#"+timeExecFinal+"# 4.3 Iniciou execucao de consulta no sigma Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExecFinal) + "ms " );
		eventList = new ArrayList<EventoVO>();
		connection = PersistEngine.getConnection("SIGMA90");
		statement = connection.prepareStatement(sqlStr.toString());
		resultSet = statement.executeQuery();
		EventoVO eventoVO = null;
		System.out.println("#"+timeExecFinal+"# 4.4 Executou a consulta de eventos no sigma Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExecFinal) + "ms " );
		while (resultSet.next()) {
		    eventoVO = new EventoVO();
		    String codigoHistorico = resultSet.getString("CD_HISTORICO");
		    String codigoEvento = resultSet.getString("CD_EVENTO");
		    String cuc = resultSet.getString("CD_CODE");
		    Long prioridade = resultSet.getLong("NU_PRIORIDADE");
		    Integer codigoCliente = resultSet.getInt("CD_CLIENTE");
		    Integer status = resultSet.getInt("FG_STATUS");
		    String nomeEvento = resultSet.getString("NM_FRASE_EVENTO");
		    Timestamp dataRecebimento = resultSet.getTimestamp("DT_RECEBIDO");
		    String codigoCentral = resultSet.getString("ID_CENTRAL");
		    String particao = resultSet.getString("PARTICAO");
		    String fantasia = resultSet.getString("FANTASIA");
		    String razao = resultSet.getString("RAZAO");
		    String endereco = resultSet.getString("ENDERECO");
		    String nomeCidade = resultSet.getString("NOMECIDADE");
		    String bairro = resultSet.getString("NOMEBAIRRO");
		    String observacaoFechamento = resultSet.getString("TX_OBSERVACAO_FECHAMENTO"); // log
		    String observavaoGerente = resultSet.getString("TX_OBSERVACAO_GERENTE"); // log
		    Long idGrupo = resultSet.getLong("CD_GRUPO_CLIENTE");
		    System.out.println("#"+timeExecFinal+"# 4.4.4.1 Ramal: "+args[0]+" - Grupo:"+grupoCliente+" Conta:"+codigoCentral+" Particao:"+particao+" Cliente:"+codigoCliente);
											     // gerencia
		    String telefone = resultSet.getString("TELEFONE");
		    String complemento = resultSet.getString("COMPLEMENTO");
		    String nomeEstado = resultSet.getString("NOMEESTADO");
		    String nomeRota = resultSet.getString("nm_rota");
		    String empresa = resultSet.getString("ID_EMPRESA");
		    String cgccpf = resultSet.getString("cgccpf");
		    String pergunta = resultSet.getString("PERGUNTA");
		    String resposta = resultSet.getString("RESPOSTA");
		    String responsavel = resultSet.getString("RESPONSAVEL");
		    String email = resultSet.getString("EMAIL");
		    String telefone1 = resultSet.getString("FONE1");
		    String telefone2 = resultSet.getString("FONE2");
		    String cep = resultSet.getString("cep");
		    String nmEmpresa = resultSet.getString("NM_RAZAO_SOCIAL");
		    Timestamp dataNovoX2 = resultSet.getTimestamp("DATANOVAX2");
		    String obsTemp = resultSet.getString("OBSTEMP");
		    String obsProvidencia = resultSet.getString("PROVIDENCIA");
		    String complementoEmpresa = resultSet.getString("NM_COMPLEMENTO");

		    eventoVO.setCodigoHistorico((codigoHistorico != null) ? codigoHistorico.toString() : "");
		    eventoVO.setCodigoEvento(codigoEvento);
		    eventoVO.setCdCode(cuc);
		    eventoVO.setPrioridade((prioridade != null) ? prioridade.longValue() : null);
		    eventoVO.setCodigoCliente((codigoCliente != null) ? codigoCliente.toString() : "");
		    eventoVO.setDataNovoX2(NeoDateUtils.safeDateFormat(dataNovoX2, "dd/MM/yyyy HH:mm:ss"));
		    eventoVO.setStatus(status.intValue());
		    eventoVO.setNomeEvento(nomeEvento);

		    eventoVO.setEmpresa(empresa);
		    eventoVO.setNomeEmpresa(nmEmpresa);
		    String dataRecebimentoStr = NeoDateUtils.safeDateFormat(dataRecebimento, "dd/MM/yyyy HH:mm:ss");
		    eventoVO.setDataRecebimento(dataRecebimentoStr);

		    eventoVO.setObservacaoEmpresa(complementoEmpresa.toUpperCase());

		    if (NeoUtils.safeIsNull(complemento) || complemento.isEmpty())
			complemento = " ";
		    eventoVO.setCodigoCentral(codigoCentral);
		    eventoVO.setParticao(particao);
		    eventoVO.setFantasia(fantasia);
		    eventoVO.setRazao(razao);
		    eventoVO.setEndereco(endereco);
		    eventoVO.setNomeCidade(nomeCidade);
		    eventoVO.setBairro(bairro);
		    if (NeoUtils.safeIsNotNull(telefone)) {
			telefone = telefone.replace("(", "").replace(")", "").replace("-", "").trim();
			telefone = telefone.replaceAll(" ", "");
			eventoVO.setTelefone(telefone);
		    }
		    eventoVO.setRota(nomeRota);
		    eventoVO.setUf(nomeEstado);
		    eventoVO.setPergunta(pergunta);
		    eventoVO.setResposta(resposta);
		    eventoVO.setResponsavel(responsavel);
		    eventoVO.setEmailResponsavel(email);
		    eventoVO.setObsTemp(obsTemp);
		    eventoVO.setObsProvidencia(obsProvidencia);
		    if (NeoUtils.safeIsNotNull(telefone1)) {
			telefone1 = telefone1.replace("(", "").replace(")", "").replace("-", "").trim();
			telefone1 = telefone1.replaceAll(" ", "");
			eventoVO.setTelefone1("<a class=\"easyui-linkbutton\" data-options=\"plain:true,iconCls:'icon-back'\" href='javascript:dial(\"0" + telefone1 + "\");'>" + telefone1 + "</a>");
		    }
		    if (NeoUtils.safeIsNotNull(telefone2)) {
			telefone2 = telefone2.replace("(", "").replace(")", "").replace("-", "").trim();
			telefone2 = telefone2.replaceAll(" ", "");
			eventoVO.setTelefone2("<a class=\"easyui-linkbutton\" data-options=\"plain:true,iconCls:'icon-back'\" href='javascript:dial(\"0" + telefone2 + "\");'>" + telefone2 + "</a>");
		    }
		    eventoVO.setCgccpf(cgccpf);
		    eventoVO.setCep(cep);
		    eventoVO.setGrupo(idGrupo);
		    eventList.add(eventoVO);

		    String deslocamentoViatura = " ";

		    if (observacaoFechamento == null || observacaoFechamento.isEmpty()) {
			observacaoFechamento = "Vazio";
		    } else {
			observacaoFechamento = "<li>" + observacaoFechamento;
			observacaoFechamento = observacaoFechamento.replaceAll("###", "<br><li>");
		    }
		    if (observavaoGerente == null || observavaoGerente.isEmpty()) {
			observavaoGerente = "Vazio";
		    } else {
			observavaoGerente = "<li>" + observavaoGerente;
			observavaoGerente = observavaoGerente.replace("\n", "<br><li>");
		    }

		    if (NeoUtils.safeIsNull(deslocamentoViatura))
			deslocamentoViatura = " ";

		    eventoVO.setObservacaoFechamento(observacaoFechamento);
		    eventoVO.setObservavaoGerente(observavaoGerente);

		}
		System.out.println("#"+timeExecFinal+"# 4.5 Iniciou busca de evento no e-form SIGMAEventos Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExecFinal) + "ms " );
	    } catch (Exception e) {
		e.printStackTrace();
		log.error("Erro Eventos retorno: " + e.getMessage());

	    } finally {

		OrsegupsUtils.closeConnection(connection, statement, resultSet);
	    }
	    
		if (eventList == null || eventList.isEmpty())
		    continue;
		System.out.println("#"+timeExecFinal+"# 4.6 Iniciou busca de evento na fila dos operadores Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExecFinal) + "ms " );
		resultVo = new EventoVO();
		for (EventoVO vo : eventList) {
		    List<String> listaConta = null;
		    listaConta = getHistoricoFila(vo.getCodigoHistorico());
		    if (vo != null && !OrsegupsAlertEventEngine.callingAlertEventVO.containsKey(vo.getCodigoHistorico()) && (listaConta == null || listaConta.isEmpty())) {			
			if (grupoCliente != null){
			    if (vo.getGrupo().equals(grupoCliente)){
				resultVo = vo;
				break;
			    }
			}else{
			    resultVo = vo;
			    break;
			}
		    }

		}
		System.out.println("#"+timeExecFinal+"# 4.7 Finalizou busca de evento na fila dos operadores Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExecFinal) + "ms " );
		eventList = null;
		return resultVo;

	}

	return resultVo;
    }
    
    private Long getGrupoRamal(String ramal){
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();
	Long retorno = null;
	try {
	    conn = PersistEngine.getConnection("");
	    
	    sql.append(" SELECT GC.cd_grupo_cliente AS grupo FROM D_CMRamais r WITH(NOLOCK) ");
	    sql.append(" INNER JOIN X_dbGrupoCliente gc WITH(NOLOCK) ON gc.neoId = r.grupo_neoId ");
	    sql.append(" WHERE r.ramal = ? ");	    	   
	    
	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setString(1, ramal);

	    rs = pstm.executeQuery();

	    while (rs.next()) {
		retorno = rs.getLong("grupo");
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
	return retorno;
    }

    private StringBuilder callUrl(String url) {
	StringBuilder inputLine = null;

	try {
	    inputLine = new StringBuilder();
	    URL murl = new URL(url);
	    URLConnection murlc = murl.openConnection();
	    BufferedReader in = new BufferedReader(new InputStreamReader(murlc.getInputStream()));

	    char letter;

	    while ((letter = (char) in.read()) != '\uFFFF') {
		inputLine.append(letter);
	    }
	    in.close();
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("ERRO AO EXECUTAR A URL SNEP TELA EVENTOS: " + e.getMessage());
	}
	return inputLine;
    }


    private EventoVO getDadosProvidencia(EventoVO vo) {

	Connection connection = null;
	PreparedStatement statement = null;
	ResultSet resultSet = null;
	
	Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();
	
	System.out.println("#"+vo.getCodigoHistorico()+"# 5.1 Iniciou busca de dados na providencia Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");

	try {

	    if (vo != null && vo.getCodigoCliente() != null) {
		connection = PersistEngine.getConnection("SIGMA90");
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT ");
		sql.append("	PROV.CD_CLIENTE, ");
		sql.append("	CD_PROVIDENCIA, ");
		sql.append("	PROV.NOME, ");
		sql.append("	PROV.FONE1, ");
		sql.append("	PROV.FONE2, ");
		sql.append("	PROV.EMAIL, ");
		sql.append("	PROV.NU_PRIORIDADE ");
		sql.append(" FROM ");
		sql.append("	dbo.dbPROVIDENCIA AS PROV WITH (NOLOCK) ");
		sql.append("	INNER JOIN dbo.dbCENTRAL as CEN WITH (NOLOCK)  ON CEN.CD_CLIENTE = PROV.CD_CLIENTE ");
		sql.append(" WHERE  ");
		sql.append("	CEN.CD_CLIENTE = " + vo.getCodigoCliente() + " ");
		sql.append(" ORDER BY PROV.NU_PRIORIDADE_NIVEL2 ");

		statement = connection.prepareStatement(sql.toString());
		resultSet = statement.executeQuery();

		ProvidenciaVO providenciaVO = null;
		while (resultSet.next()) {

		    providenciaVO = new ProvidenciaVO();
		    String cliente = resultSet.getString("CD_CLIENTE");
		    String providencia = resultSet.getString("CD_PROVIDENCIA");
		    String nome = resultSet.getString("NOME");
		    String telefone1 = resultSet.getString("FONE1");
		    String telefone2 = resultSet.getString("FONE2");
		    String email = resultSet.getString("EMAIL");
		    String prioridade = resultSet.getString("NU_PRIORIDADE");

		    if (NeoUtils.safeIsNotNull(cliente)) {
			providenciaVO.setCodigoCliente(Integer.parseInt(cliente));
		    } else {
			providenciaVO.setCodigoCliente(0);
		    }

		    if (NeoUtils.safeIsNotNull(providencia)) {
			providenciaVO.setCodigoProvidencia(Integer.parseInt(providencia));
		    } else {
			providenciaVO.setCodigoProvidencia(0);
		    }

		    if (NeoUtils.safeIsNotNull(nome)) {
			providenciaVO.setNome(nome);
		    } else {
			providenciaVO.setCodigoCliente(0);
		    }

		    if (NeoUtils.safeIsNotNull(telefone1) && !telefone1.isEmpty()) {
			telefone1 = telefone1.replace("(", "").replace(")", "").replace("-", "").trim();
			telefone1 = telefone1.replace(" ", "");
			providenciaVO.setTelefone1("<a class=\"easyui-linkbutton\" data-options=\"plain:true,iconCls:'icon-back'\" href='javascript:dial(\"0" + telefone1 + "\");'>" + telefone1 + "</a>");
		    } else {
			providenciaVO.setTelefone1("");
		    }
		    if (NeoUtils.safeIsNotNull(telefone2) && !telefone2.isEmpty()) {
			telefone2 = telefone2.replace("(", "").replace(")", "").replace("-", "").trim();
			telefone2 = telefone2.replace(" ", "");
			providenciaVO.setTelefone2("<a class=\"easyui-linkbutton\" data-options=\"plain:true,iconCls:'icon-back'\" href='javascript:dial(\"0" + telefone2 + "\");'>" + telefone2 + "</a>");
		    } else {
			providenciaVO.setTelefone2("");
		    }
		    if (NeoUtils.safeIsNotNull(email)) {
			providenciaVO.setEmail(email);
		    } else {
			providenciaVO.setEmail("");
		    }
		    if (NeoUtils.safeIsNotNull(prioridade)) {
			providenciaVO.setPrioridade(Integer.parseInt(prioridade));
		    } else {
			providenciaVO.setPrioridade(0);
		    }
		    vo.addProvidencia(providenciaVO);
		}

	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("ERRO OrsegupsAlertEventEngine SIGMA PROVIDENCIAS : " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(connection, statement, resultSet);
	}
	System.out.println("#"+vo.getCodigoHistorico()+"# 5.2 Finalizou busca de dados na providencia Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");
	return vo;

    }

    public void verificarLigacaoPendente(String ramal) {

	boolean ramalFlag = false;
	CallAlertEventVO callAlertEventVOs = null;
	Collection<CallAlertEventVO> alertEventEngines = null;
	alertEventEngines = OrsegupsAlertEventEngine.callingAlertEventVO.values();
	//TODO INSTANCIA ESTATICA NÃO VINDO DA INSTANCE ESTÁ GERANDO PROBLEMAS
	if (NeoUtils.safeIsNotNull(alertEventEngines) && !alertEventEngines.isEmpty()) {
	    for (CallAlertEventVO callAlertEventVO : alertEventEngines) {
		callAlertEventVOs = (CallAlertEventVO) callAlertEventVO;
		if (callAlertEventVO.getRamalOperador().equals(ramal) && verificaEventoHistorico(callAlertEventVO.getCallEventoVO().getCodigoHistorico())) {
		    ramalFlag = true;
		    break;
		}
	    }
	    if (ramalFlag) {

		CallAlertEventVO alertEventVo = OrsegupsAlertEventEngine.callingAlertEventVO.get(callAlertEventVOs.getCallEventoVO().getCodigoHistorico());

		if (NeoUtils.safeIsNotNull(callAlertEventVOs)) {
		    // serializa o vo para json
		    Gson gson = new Gson();
		    // dispara a mensagem JMS
		    try {
			String alertVoJSON = gson.toJson(alertEventVo);
			NeoJMSPublisher.getInstance().sendAlertEventMessage(URLEncoder.encode(alertVoJSON, "UTF-8"), alertEventVo.getRamalOperador());
		    } catch (UnsupportedEncodingException e) {
			// Auto-generated catch block
			e.printStackTrace();
			log.error("ERRO VERIFICAR EVENTOS ramalFlag: verificarLigacaoPendente - " + e.getMessage());

		    } catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		    }

		}
	    }

	}
	if (!ramalFlag) {
	    if (callAlertEventVOs != null && !verificaEventoHistorico(callAlertEventVOs.getCallEventoVO().getCodigoHistorico())) {
		OrsegupsAlertEventEngine.callingAlertEventVO.remove(callAlertEventVOs.getCallEventoVO().getCodigoHistorico());

		Gson gson = new Gson();
		String alertVoJSON = gson.toJson(null);
		try {
		    NeoJMSPublisher.getInstance().sendAlertEventMessage(URLEncoder.encode(alertVoJSON, "UTF-8"), ramal);
		} catch (UnsupportedEncodingException e) {
		    // Auto-generated catch block
		    e.printStackTrace();
		    log.error("ERRO VERIFICAR EVENTOS ramalFlag: verificarLigacaoPendente - " + e.getMessage());

		} catch (Exception e) {
		    e.printStackTrace();
		    log.error(e.getMessage());
		}finally{
		    getRetiraPausaRamal(ramal);		    
		}

	    }

	}
    }

//    // verifica log do evento
//    private String nomeFila() {
//
//	QLGroupFilter filter = new QLGroupFilter("AND");
//	filter.addFilter(new QLEqualsFilter("codigo", 1L));
//
//	// Formatar CPF/CNPJ
//	String descFila = null;
//	List<NeoObject> filas = null;
//	filas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CMFilaOperacao"), filter);
//	if (filas != null && !filas.isEmpty()) {
//	    EntityWrapper entityWrapper = new EntityWrapper(filas.get(0));
//	    descFila = (String) entityWrapper.getValue("descricao");
//	}
//
//	return descFila;
//    }

    // verifica log do evento
    public String getLog(String cdCliente, String cdHistorico) {
	Connection conn = null;
	PreparedStatement st = null;
	ResultSet rs = null;
	String texto = "";
	try {
	    conn = PersistEngine.getConnection("SIGMA90");
	    StringBuilder sql = new StringBuilder();
	    sql.append(" SELECT H.TX_OBSERVACAO_FECHAMENTO FROM HISTORICO H WITH (NOLOCK)  WHERE H.CD_CLIENTE = ? AND H.CD_HISTORICO = ? ");
	    sql.append(" AND  h.FG_STATUS IN (5) AND (( H.CD_EVENTO LIKE 'X406')  OR (H.CD_EVENTO LIKE 'E401' AND H.CD_CODE LIKE 'DLC' ) OR (H.CD_EVENTO LIKE 'XXX2' AND H.CD_CODE LIKE 'NAR') OR (H.CD_EVENTO LIKE 'XXX5' AND H.CD_CODE LIKE 'DST'))  ");
	    st = conn.prepareStatement(sql.toString());
	    st.setString(1, cdCliente);
	    st.setString(2, cdHistorico);
	    rs = st.executeQuery();

	    while (rs.next()) {
		if (NeoUtils.safeIsNotNull(rs.getString("TX_OBSERVACAO_FECHAMENTO")))
		    texto = rs.getString("TX_OBSERVACAO_FECHAMENTO");
	    }

	    if (texto == null || texto.isEmpty()) {
		texto = "Vazio";
	    } else {
		texto = "<li>" + texto;
		texto = texto.replaceAll("##", "<br><li>");
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("##### ERRO SALVAR LOG EVENT ALERT: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
	} finally {
	    OrsegupsUtils.closeConnection(conn, st, rs);
	}
	return texto;

    }

//    // verificar o ramal do usuÃ¡rio logado
//    private String getRamal() {
//	NeoUser neoUser = (NeoUser) PortalUtil.getCurrentUser();
//	EntityWrapper entityWrapper = new EntityWrapper(neoUser);
//	String ramal = (String) entityWrapper.findField("ramal").getValue();
//	return ramal;
//    }

    // Corrige telefone com prefixo local
    private String verificarPrefixo(String destino) {

	List<String> prefix_list = new ArrayList<String>();
	String[] arr = { "3276", "3274", "3256", "3272", "3243", "3285", "3264", "3025", "3028", "3216", "3217", "3221", "3222", "3223", "3224", "3225", "3226", "3228", "3229", "3231", "3232", "3233", "3234", "3235",
		"3236", "3237", "3238", "3239", "3240", "3244", "3248", "3249", "3251", "3261", "3266", "3269", "3281", "3282", "3284", "3322", "3323", "3324", "3325", "3326", "3329", "3331", "3333", "3334", "3335",
		"3337", "3338", "3348", "3368", "3369", "3422", "3451", "3700", "3254", "3354", "3262", "3268", "3273", "3267", "3033", "3242", "3283", "3286", "3341", "3342", "3344", "3253", "3275", "3292", "3245",
		"3265", "3241", "3246", "3247", "3257", "3258", "3259", "3271", "3278", "3343", "3346", "3357", "3381", "3972", "3277", "3378", "3263", "3279", "3206" };

	for (String string : arr) {
	    prefix_list.add(string);
	}
	if (destino != null && destino.isEmpty())
	    log.error("Telefone destino nulo ou vazio!");

	if (destino.length() == 10 && destino.substring(0, 2) == "48") {
	    destino = destino.substring(-8);
	} else if (destino.length() == 10) {
	    destino = "0" + destino;
	}
	String prefix = null;
	/*
	 * Fernando.rebelo erro aqui, pois destino veio com tamanho 4, portanto
	 * o substring abaixo vai dar erro de index 06:14:25,840 ERROR
	 * OrsegupsAlertEventEngine:497 - ERRO NeoJMSPublisher DEPOIS DE
	 * EXECUTAR: String index out of range: 4
	 */
	try {
	    prefix = destino.substring(3, 4);

	    if (prefix_list.contains(prefix) && destino.substring(1, 2) == "48")
		destino = destino.substring(-8);
	} catch (Exception e) {
	    e.getStackTrace();
	    log.error("Erro no NeoJMSPublisher ao tratar o telefone de destino cujo prefixo = " + prefix);
	}

	return destino;
    }

    // verifica historico de eventos anteriores
    public EventoVO getDadosEventosHistorico(EventoVO vo, String codigoHistorico, String valor) {

	CallAlertEventVO alertEventVo = null;
	if (NeoUtils.safeIsNotNull(codigoHistorico))
	    alertEventVo = OrsegupsAlertEventEngine.callingAlertEventVO.get(codigoHistorico);

	Connection connection = null;
	CallableStatement callableStatement = null;
	ResultSet resultSet = null;

	try {

	    if ((vo != null && vo.getCodigoCliente() != null) || (alertEventVo != null && valor != null)) {
		connection = PersistEngine.getConnection("SIGMA90");
		StringBuilder sql = new StringBuilder();
		sql.append(" EXEC USP_HISTORICO_EVENTOS @COD_CLIENTE = ?, @DATA = ?  ");

		if (NeoUtils.safeIsNotNull(vo)) {
		    codigoHistorico = vo.getCodigoCliente();
		} else {
		    codigoHistorico = alertEventVo.getCallEventoVO().getCodigoCliente();
		    vo = new EventoVO();
		}

		if (valor == null)
		    valor = "1";
		callableStatement = connection.prepareCall(sql.toString());
		callableStatement.setInt(1, Integer.parseInt(codigoHistorico));
		callableStatement.setInt(2, Integer.parseInt(valor));
		resultSet = callableStatement.executeQuery();

		EventoHistoricoVO eventoVO = null;
		while (resultSet.next()) {

		    eventoVO = new EventoHistoricoVO();
		    String dtRecebido = NeoDateUtils.safeDateFormat(resultSet.getTimestamp("DT_RECEBIDO"), "dd/MM/yyyy HH:mm:ss ");
		    String cdEvento = resultSet.getString("CD_EVENTO");
		    String tipoEvento = resultSet.getString("NM_FRASE_EVENTO");
		    String dtEspera = NeoDateUtils.safeDateFormat(resultSet.getTimestamp("DT_ESPERA"), "dd/MM/yyyy HH:mm:ss ");
		    String dtVtrDeslocamento = NeoDateUtils.safeDateFormat(resultSet.getTimestamp("DT_VIATURA_DESLOCAMENTO"), "dd/MM/yyyy HH:mm:ss ");
		    String dtVtrLocal = NeoDateUtils.safeDateFormat(resultSet.getTimestamp("DT_VIATURA_NO_LOCAL"), "dd/MM/yyyy HH:mm:ss ");
		    String dtFechamento = NeoDateUtils.safeDateFormat(resultSet.getTimestamp("DT_FECHAMENTO"), "dd/MM/yyyy HH:mm:ss ");
		    String nuFechamento = resultSet.getString("NU_AUXILIAR");
		    String obsFechamento = resultSet.getString("TX_OBSERVACAO_FECHAMENTO");
		    String prioridade = resultSet.getString("NU_PRIORIDADE");

		    if (obsFechamento == null || obsFechamento.isEmpty()) {
			obsFechamento = "Vazio";
		    } else {
			// obsFechamento = "<li>" + obsFechamento;
			obsFechamento = obsFechamento.replace("\n", "<br><li>");
		    }

		    if (NeoUtils.safeIsNotNull(dtRecebido))
			eventoVO.setDtRecebido(dtRecebido);
		    else
			eventoVO.setDtRecebido("");
		    if (NeoUtils.safeIsNotNull(cdEvento))
			eventoVO.setCdEvento(cdEvento);
		    else
			eventoVO.setCdEvento("");
		    if (NeoUtils.safeIsNotNull(tipoEvento))
			eventoVO.setTipoEvento(tipoEvento);
		    else
			eventoVO.setTipoEvento("");
		    if (NeoUtils.safeIsNotNull(dtEspera))
			eventoVO.setDtEspera(dtEspera);
		    else
			eventoVO.setDtEspera("");
		    if (NeoUtils.safeIsNotNull(dtVtrDeslocamento))
			eventoVO.setDtVtrDeslocamento(dtVtrDeslocamento);
		    else
			eventoVO.setDtVtrDeslocamento("");
		    if (NeoUtils.safeIsNotNull(dtVtrLocal))
			eventoVO.setDtVtrLocal(dtVtrLocal);
		    else
			eventoVO.setDtVtrLocal("");
		    if (NeoUtils.safeIsNotNull(dtFechamento))
			eventoVO.setDtFechamento(dtFechamento);
		    else
			eventoVO.setDtFechamento("");
		    if (NeoUtils.safeIsNotNull(nuFechamento))
			eventoVO.setNuFechamento(nuFechamento);
		    else
			eventoVO.setNuFechamento("");
		    if (NeoUtils.safeIsNotNull(obsFechamento))
			eventoVO.setObsFechamento(obsFechamento);
		    else
			eventoVO.setObsFechamento("");
		    if (NeoUtils.safeIsNotNull(prioridade))
			eventoVO.setPrioridade(prioridade);
		    else
			eventoVO.setPrioridade("");

		    vo.addEventoHistorico(eventoVO);

		}

	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("ERRO OrsegupsAlertEventEngine SIGMA EVENTO HISTORICO : " + e.getMessage());
	} finally {
	    try {
		if (resultSet != null)
		    resultSet.close();
		if (callableStatement != null)
		    callableStatement.close();
		if (connection != null)
		    connection.close();

	    } catch (Exception e2) {
		e2.printStackTrace();
	    }
	}
	return vo;

    }

    // verifica se o evento ainda existe no historico
    private Boolean verificaEventoHistorico(String cdHistorico) {
	Connection conn = null;
	PreparedStatement st = null;
	ResultSet rs = null;
	Boolean flag = Boolean.FALSE;
	try {
	    conn = PersistEngine.getConnection("SIGMA90");
	    StringBuilder sql = new StringBuilder();
	    sql.append(" SELECT H.CD_HISTORICO FROM HISTORICO H WITH (NOLOCK)  WHERE H.CD_HISTORICO = ? ");
	    sql.append(" AND  h.FG_STATUS IN (5,0) AND (( H.CD_EVENTO LIKE 'X406')  OR (H.CD_EVENTO LIKE 'E401' AND H.CD_CODE LIKE 'DLC' ) OR (H.CD_EVENTO LIKE 'XXX2' AND H.CD_CODE LIKE 'NAR') OR (H.CD_EVENTO LIKE 'XXX5' AND H.CD_CODE LIKE 'DST') OR (h.CD_EVENTO = 'XVID') OR (h.CD_EVENTO = 'XVD2') OR (h.CD_EVENTO = 'XRED') OR (h.CD_CODE = 'XPA' AND h.CD_HISTORICO_PAI IS NULL AND h.FG_STATUS IN (0,1,5)) )  ");
	    st = conn.prepareStatement(sql.toString());
	    st.setString(1, cdHistorico);
	    rs = st.executeQuery();

	    if (rs.next())
		flag = Boolean.TRUE;

	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("##### ERRO SALVAR LOG EVENT ALERT verificaEventoHistorico: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
	} finally {
	    OrsegupsUtils.closeConnection(conn, st, rs);
	}
	return flag;

    }

    // verifica qual ramal esta disponivel a mais tempo
    private List<String> getRamaisDisponiveis(Long tipoAtendimento) {

	List<String> ramaisAtivos = null;
	
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();
	
	sql.append(" SELECT E.descricao, R.ramal FROM D_CMRamais R WITH(NOLOCK) ");
	sql.append(" INNER JOIN D_CMTipoAtendimentoDesvioDeHabito T WITH(NOLOCK) ON R.tipoAtendimento_neoId = T.neoId ");
	sql.append(" INNER JOIN D_CMTipoEventoDesvioDeHabito E WITH(NOLOCK) ON R.tipo_neoId = E.neoId ");
	sql.append(" WHERE R.situacao = 1  ");
	sql.append(" AND R.disponivelAtendimento = 1 ");
	sql.append(" AND T.codigo = ? ");
	sql.append(" ORDER BY dataLigacao ASC ");

	try {
	    
	    ramaisAtivos = new ArrayList<String>();
	    
	    conn = PersistEngine.getConnection("");
	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setLong(1, tipoAtendimento);

	    rs = pstm.executeQuery();
	    
	    while (rs.next()) {
		
		String descricao = rs.getString("descricao");
		
		if (descricao != null && !descricao.isEmpty()){
		    String ramal = rs.getString("ramal");
		    
		    if (ramal != null && !ramal.isEmpty()){
			ramal = ramal+";"+descricao;
			ramaisAtivos.add(ramal);
		    }
		}
		
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	    log.error("##### ERRO AO PESQUISAR getRamaisDisponiveis: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
	
	
//	
//	try {
//	    QLGroupFilter filter = new QLGroupFilter("AND");
//	    filter.addFilter(new QLEqualsFilter("situacao", true));
//	    filter.addFilter(new QLEqualsFilter("disponivelAtendimento", true));
//	    filter.addFilter(new QLEqualsFilter("tipoAtendimento.codigo", tipoAtendimento));
//
//	    List<NeoObject> ramais = null;
//
//	    ramais = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CMRamais"), filter, -1, -1, "dataLigacao asc");
//	    PersistEngine.getEntityManager().flush();
//	    if (ramais != null && !ramais.isEmpty()) {
//		
//		for (NeoObject neoObject : ramais) {
//		    EntityWrapper ramalUsuario = new EntityWrapper(neoObject);
//		    String ramal = null;
//		    if ((String) ramalUsuario.findValue("tipo.descricao") != null)
//			ramal = (String) ramalUsuario.getValue("ramal") + ";" + (String) ramalUsuario.findValue("tipo.descricao");
//		    if (ramal != null && !ramal.isEmpty())
//			ramaisAtivos.add(ramal);
//		}
//
//	    }
//	} catch (Exception e) {
//	    e.printStackTrace();
//	    log.error("##### ERRO AO PESQUISAR getRamaisDisponiveis: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
//	}
	return ramaisAtivos;

    }


    // adicionar pausa no ramal
    private boolean getPausaRamal(String ramal) {
	Boolean ramaisAtivos = Boolean.FALSE;
	
	Connection conn = null;
	PreparedStatement pstm = null;
	
	StringBuilder sql = new StringBuilder();
	sql.append(" UPDATE D_CMRamais SET situacao=0 ");
	sql.append(" WHERE situacao=1 AND ramal=? ");

	try {
	    conn = PersistEngine.getConnection("");
	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setString(1, ramal);

	    int rs = pstm.executeUpdate();

	    if (rs > 0){
		ramaisAtivos = Boolean.TRUE;
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	    log.error("##### ERRO SALVAR LOG EVENT ALERT getPausaRamal: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}
	

	return ramaisAtivos;
    }

    // adicionar pausa no ramal
    private boolean getRetiraPausaRamal(String ramal) {
	
	Boolean ramaisAtivos = Boolean.FALSE;
	
	Connection conn = null;
	PreparedStatement pstm = null;
	
	StringBuilder sql = new StringBuilder();
	sql.append(" UPDATE D_CMRamais SET situacao=1 ");
	sql.append(" WHERE situacao=0 AND ramal=? ");

	try {
	    conn = PersistEngine.getConnection("");
	    pstm = conn.prepareStatement(sql.toString());

	    pstm.setString(1, ramal);

	    int rs = pstm.executeUpdate();

	    if (rs > 0){
		ramaisAtivos = Boolean.TRUE;
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	    log.error("##### OrsegupsAlertEventEngine getRetiraPausaRamal: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}

//	Boolean ramaisAtivos = Boolean.FALSE;
//	List<NeoObject> ramais = null;
//	try {
//	    QLGroupFilter filter = new QLGroupFilter("AND");
//	    filter.addFilter(new QLEqualsFilter("situacao", false));
//	    filter.addFilter(new QLEqualsFilter("ramal", ramal));
//
//	    ramais = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CMRamais"), filter);
//	    if (ramais != null && !ramais.isEmpty()) {
//		NeoObject neoObject = (NeoObject) ramais.get(0);
//
//		EntityWrapper ramalUsuario = new EntityWrapper(neoObject);
//		ramalUsuario.setValue("situacao", true);
//		ramalUsuario.setValue("ramal", ramal);
//		PersistEngine.persist(neoObject);
//
//		ramaisAtivos = Boolean.TRUE;
//		PersistEngine.getEntityManager().flush();
//	    }
//	} catch (Exception e) {
//	    e.printStackTrace();
//	   
//	}
//	ramais = null;
	return ramaisAtivos;
    }

    // adicionar a logica que irÃ¡ realizar a chamada ap Snep
    public void getLigacaoSnep(String ramal, String numeroExterno) {
	String message = "";
	String ramalOrigem = null;
	String ramalDestino = null;
	ramalOrigem = ramal;
	ramalDestino = numeroExterno.trim();
	String url = linkSnep + "/snep/services/?service=Dial&ramal=" + ramalOrigem + "&exten=" + ramalDestino;
	if (ramalDestino != null && !ramalDestino.isEmpty() && ramalOrigem != null && !ramalOrigem.isEmpty()) {
	    if (ramalDestino.equals(ramalOrigem)) {
		message = "Erro ao efetuar chamada.\nRamal de origem igual ao ramal de destino.";
	    } else {
		StringBuilder buffer = this.callUrl(url);
		String result = buffer.toString();
		result = result.replaceAll("\\{", "");
		result = result.replaceAll("\\}", "");
		String[] params = result.split(",");
		// System.out.println("Ligaca : " + result);
		if (params[0] != null && params[0].contains("ok")) {
		    message = "Chamada efetuada a partir do ramal " + ramalOrigem + " para o ramal " + ramalDestino + ".";

		} else {
		    String erro[] = params[1].split(":");
		    message = "Erro ao efetuar chamada.\n" + erro[1];
		}
	    }
	}
	log.debug("Solicitando ligaÃ§Ã£o para Snep! Ramal:" + ramal + " - Numero externo: " + numeroExterno + " mensagem : " + message);
    }

    // ALTERA DATA ULTIMA LIGACAO PARA
    private void alteraDataUltimaLigacaoRamal(String ramal, String contaParticao) {
	
	    Connection conn = null;
	    PreparedStatement pstm = null;

	    StringBuilder sql = new StringBuilder();
	    
	    sql.append(" UPDATE D_CMRamais SET dataLigacao = GETDATE(), conta = ?  ");
	    sql.append(" WHERE ramal = ? ");

	    try {
		conn = PersistEngine.getConnection("");
		pstm = conn.prepareStatement(sql.toString());
		
		pstm.setString(1, contaParticao);
		pstm.setString(2, ramal);

		pstm.executeUpdate();

	    } catch (SQLException e) {
		e.printStackTrace();
		log.error("##### ERRO SALVAR LOG EVENT ALERT alteraDataUltimaLigacaoRamal: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
	    } finally {
		OrsegupsUtils.closeConnection(conn, pstm, null);
	    }
	
//	try {
//	    QLGroupFilter filter = new QLGroupFilter("AND");
//	    filter.addFilter(new QLEqualsFilter("ramal", ramal));
//	    // View de Contas
//	    List<NeoObject> ramais = null;
//	    ramais = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CMRamais"), filter);
//	    PersistEngine.getEntityManager().flush();
//	    if (ramais != null && !ramais.isEmpty()) {
//		NeoObject neoObject = (NeoObject) ramais.get(0);
//		EntityWrapper ramalUsuario = new EntityWrapper(neoObject);
//		ramalUsuario.setValue("dataLigacao", new GregorianCalendar());
//		if (contaParticao != null)
//		    ramalUsuario.setValue("conta", contaParticao);
//		ramalUsuario.setValue("ramal", ramal);
//		PersistEngine.persist(neoObject);
//
//	    }
//	} catch (Exception e) {
//	    e.printStackTrace();
//	    
//	}
    }

    // VERIFICAR SE O EVENTO HISTORICO JA ESTA EM ATENDIMENTO
    private List<String> getHistoricoFila(String historico) {

	List<String> ramaisAtivos = null;
	
	Connection conn = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	StringBuilder sql = new StringBuilder();

	sql.append(" SELECT conta FROM D_CMRamais R WITH(NOLOCK) ");
	sql.append(" WHERE conta LIKE ? ");
	
	try {
	    conn = PersistEngine.getConnection("");
	    pstm = conn.prepareStatement(sql.toString());
	    
	    if (!historico.isEmpty()){
		historico = "%"+historico+"%";		
	    }
	    
	    pstm.setString(1, historico);
	    
	    rs = pstm.executeQuery();
	    
	    ramaisAtivos = new ArrayList<String>();
	    
	    while (rs.next()) {
		String h = rs.getString(1);
				
		if (h != null && !h.isEmpty()){
		    ramaisAtivos.add(h);
		}

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	    log.error("##### ERRO SALVAR LOG EVENT ALERT getHistoricoFila: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}
	
//	try {
//	    QLGroupFilter filter = new QLGroupFilter("AND");
//	    filter.addFilter(new QLOpFilter("conta", "LIKE", "%" + historico + "%"));
//	    List<NeoObject> ramais = null;
//	    ramais = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CMRamais"), filter);
//	    PersistEngine.getEntityManager().flush();
//	    if (ramais != null && !ramais.isEmpty()) {
//		ramaisAtivos = new ArrayList<String>();
//		for (NeoObject neoObject : ramais) {
//		    EntityWrapper ramalUsuario = new EntityWrapper(neoObject);
//		    String hist = (String) ramalUsuario.getValue("conta");
//		    ramaisAtivos.add(hist);
//		}
//	    }
//	} catch (Exception e) {
//	    e.printStackTrace();
//	    log.error("##### ERRO SALVAR LOG EVENT ALERT getHistoricoFila: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
//	}
	return ramaisAtivos;
    }

    // EVENTOS XXX2 e XXX5 PRECISA SER INSERIDO O DATANOVAX2 PARA ABRIR O
    // PROXIMO EVENTO AUTOMATICO
    public void proximoEventoCliente(String codHistorico, String dataEventoCliente) {
	Connection conn = null;
	PreparedStatement preparedStatementUpdate = null;
	ResultSet rsH = null;
	try {
	    CallAlertEventVO alertEventVo = OrsegupsAlertEventEngine.callingAlertEventVO.get(codHistorico);
	    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	    java.sql.Timestamp data = new java.sql.Timestamp(formatter.parse(dataEventoCliente).getTime());

	    conn = PersistEngine.getConnection("SIGMA90");
	    String updateSQL = " UPDATE dbCENTRAL SET DATANOVAX2 = ? WHERE CD_CLIENTE = ? ";
	    preparedStatementUpdate = conn.prepareStatement(updateSQL.toString());
	    preparedStatementUpdate.setTimestamp(1, new Timestamp(data.getTime()));
	    preparedStatementUpdate.setString(2, alertEventVo.getCallEventoVO().getCodigoCliente());
	    preparedStatementUpdate.executeUpdate();
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("##### ERRO SALVAR LOG EVENT proximoEventoCliente: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
	} finally {
	    OrsegupsUtils.closeConnection(conn, preparedStatementUpdate, rsH);
	}
	log.debug("Fechando evento no Sigma! " + codHistorico);
    }

    // verificar se existem os usuÃ¡rios que estÃ£o autorizados para a atender
    // pelo cliente
    private EventoVO getDadosAcessoCliente(EventoVO vo) {
	Connection connection = null;
	PreparedStatement statement = null;
	ResultSet resultSet = null;
	
	Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();
	
	System.out.println("#"+vo.getCodigoHistorico()+"# 5.3 Iniciou busca de dados na providencia Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");
	
	try {
	    if (vo != null && vo.getCodigoCliente() != null) {
		connection = PersistEngine.getConnection("SIGMA90");
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT A.ID_ACESSO,A.NOME,A.NU_REGISTRO_GERAL,A.NU_CPF,A.SENHA,A.OBS,A.NU_TELEFONE  FROM dbACESSO AS A WITH (NOLOCK) WHERE A.CD_CLIENTE = ? ");
		statement = connection.prepareStatement(sql.toString());
		statement.setString(1, vo.getCodigoCliente());
		resultSet = statement.executeQuery();
		EventoAcessoClienteVO eventoAcessoCliente = null;
		while (resultSet.next()) {
		    eventoAcessoCliente = new EventoAcessoClienteVO();
		    String idAcesso = resultSet.getString("ID_ACESSO");
		    String nome = resultSet.getString("NOME");
		    String nuRegistroGeral = resultSet.getString("NU_REGISTRO_GERAL");
		    String nuCpf = resultSet.getString("NU_CPF");
		    String senha = resultSet.getString("SENHA");
		    String obs = resultSet.getString("OBS");
		    String telefone = resultSet.getString("NU_TELEFONE");
		    if (obs == null || obs.isEmpty()) {
			obs = "Vazio";
		    } else {
			// obsFechamento = "<li>" + obsFechamento;
			obs = obs.replace("\n", "<br><li>");
		    }

		    if (NeoUtils.safeIsNotNull(idAcesso)) {
			eventoAcessoCliente.setIdAcesso(idAcesso);
		    } else {
			eventoAcessoCliente.setIdAcesso("");
		    }

		    if (NeoUtils.safeIsNotNull(nome)) {
			eventoAcessoCliente.setNome(nome);
		    } else {
			eventoAcessoCliente.setNome("");
		    }

		    if (NeoUtils.safeIsNotNull(nuRegistroGeral)) {
			eventoAcessoCliente.setNuRegistroGeral(nuRegistroGeral);
		    } else {
			eventoAcessoCliente.setNuRegistroGeral("");
		    }

		    if (NeoUtils.safeIsNotNull(nuCpf)) {
			eventoAcessoCliente.setNuCpf(nuCpf);
		    } else {
			eventoAcessoCliente.setNuCpf("");
		    }
		    if (NeoUtils.safeIsNotNull(senha)) {
			eventoAcessoCliente.setSenha(senha);
		    } else {
			eventoAcessoCliente.setSenha("");
		    }
		    if (NeoUtils.safeIsNotNull(obs)) {
			eventoAcessoCliente.setObs(obs);
		    } else {
			eventoAcessoCliente.setObs("");
		    }
		    if (NeoUtils.safeIsNotNull(telefone)) {
			telefone = telefone.replace("(", "").replace(")", "").replace("-", "").trim();
			telefone = telefone.replace(" ", "");
			eventoAcessoCliente.setTelefone("<a class=\"easyui-linkbutton\" data-options=\"plain:true,iconCls:'icon-back'\" href='javascript:dial(\"0" + telefone + "\");'>" + telefone + "</a>");
		    } else {
			eventoAcessoCliente.setTelefone("");
		    }
		    vo.addEventoAcessoCliente(eventoAcessoCliente);
		}

	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("ERRO OrsegupsAlertEventEngine getDadosAcessoCliente : " + e.getMessage());
	} finally {
	    OrsegupsUtils.closeConnection(connection, statement, resultSet);
	}
	
	System.out.println("#"+vo.getCodigoHistorico()+"# 5.4 Iniciou busca de dados na providencia Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms ");
	return vo;
    }

    private String getUsuarioSigma() {
	NeoUser neoUser = null;
	if (PortalUtil.getCurrentUser() != null)
	    neoUser = (NeoUser) PortalUtil.getCurrentUser();

	String cdUsuario = "11010";
	try {
	    if (neoUser != null) {
		//NeoUser user = (NeoUser) neoUser;
		QLGroupFilter filterAnd = new QLGroupFilter("AND");
		filterAnd.addFilter(new QLEqualsFilter("usuarioFusion", neoUser));
		ArrayList<NeoObject> neoObject = (ArrayList<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SIGMAUsuarioSigmaFusion"), filterAnd);
		PersistEngine.getEntityManager().flush();
		if (neoObject != null && !neoObject.isEmpty()) {
		    EntityWrapper entityWrapper = new EntityWrapper(neoObject.get(0));
		    Long cdUsuarioL = (Long) entityWrapper.findValue("usuarioSigma.cd_usuario");
		    cdUsuario = String.valueOf(cdUsuarioL);
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("ERRO OrsegupsAlertEventEngine getUsuarioSigma : " + e.getMessage());
	}
	return cdUsuario;

    }

    public void abrirOSSigma(String idCliente, String particao, String idCentral, String cdEmpresa, String solicitante, String defeito, String respTecnico, String descricao) {
	Map<String, String> map = new HashMap<String, String>();

	map.put("clientId", idCliente);
	map.put("partition", particao);
	map.put("centralId", idCentral);
	map.put("companyId", cdEmpresa);
	map.put("scheduledTime", "");
	map.put("estimatedTime", "");
	map.put("description", (descricao.length() >= 500 ? descricao.substring(0, 499) : descricao));
	map.put("requester", solicitante);
	map.put("defect", defeito);
	map.put("responsibleTechnician", respTecnico);

	String param2 = montaParametroOSString(map);
	try {
	    String urlParameters = param2;
	    String request = WEB_SERVICE_SIGMA + "ReceptorOSWebService";
	    URL url = new URL(request);
	    URLConnection conn = url.openConnection();

	    conn.setDoOutput(true);

	    OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());

	    writer.write(urlParameters);
	    writer.flush();

	    String line;
	    BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

	    while ((line = reader.readLine()) != null) {
		JSONObject json = new JSONObject(line);
		if (json.getString("status").equals("success")) {
		    log.debug("Sucesso ao abrir OS tratamento de desvio de habito.");
		} else {
		    log.debug("ExceÃ§Ã£o ao abrir OS tratamento de desvio de habito.");
		}
	    }
	    writer.close();
	    reader.close();
	} catch (Exception e) {
	    e.printStackTrace();
	}

    }

    public static String montaParametroOSString(Map<String, String> map) {
	String parametro = "";

	/*
	 * clientId=?* &centralId=?* &partition=?* &companyId=?*
	 * &responsibleTechnician=?* &defect=?* &requester=?* &description=?
	 * &estimatedTime=? &scheduledTime=?
	 */
	if (map.get("clientId") != null && !map.get("clientId").equals("")) {
	    parametro += "clientId" + "=" + map.get("clientId");
	}

	if (map.get("centralId") != null && !map.get("centralId").equals("")) {
	    parametro += "&" + "centralId" + "=" + map.get("centralId");
	}

	if (map.get("partition") != null && !map.get("partition").equals("")) {
	    parametro += "&" + "partition" + "=" + map.get("partition");
	}

	if (map.get("companyId") != null && !map.get("companyId").equals("")) {
	    parametro += "&" + "companyId" + "=" + map.get("companyId");
	}

	if (map.get("responsibleTechnician") != null && !map.get("responsibleTechnician").equals("")) {
	    parametro += "&" + "responsibleTechnician" + "=" + map.get("responsibleTechnician");
	}

	if (map.get("defect") != null && !map.get("defect").equals("")) {
	    parametro += "&" + "defect" + "=" + map.get("defect");
	}

	if (map.get("requester") != null && !map.get("requester").equals("")) {
	    parametro += "&" + "requester" + "=" + map.get("requester");
	}

	if (map.get("description") != null && !map.get("description").equals("")) {
	    parametro += "&" + "description" + "=" + map.get("description");
	}

	if (map.get("estimatedTime") != null && !map.get("estimatedTime").equals("")) {
	    parametro += "&" + "estimatedTime" + "=" + map.get("estimatedTime");
	}

	if (map.get("scheduledTime") != null && !map.get("scheduledTime").equals("")) {
	    parametro += "&" + "scheduledTime" + "=" + map.get("scheduledTime");
	}

	try {
	    parametro = URLEncoder.encode(parametro,"UTF-8").replace("%3D", "=").replace("%26", "&");
	} catch (UnsupportedEncodingException e) {
	    e.printStackTrace();
	}
	return parametro;
    }

    private String getTecnicoResponsavel(String cdCliente) {
	Connection conn = null;
	PreparedStatement stCol = null;
	PreparedStatement stTec = null;
	ResultSet rsCol = null;
	ResultSet rsTec = null;
	String cdTecnico = null;
	String tecResp = "";
	String nmRota = "";
	String insResp = "";
	Connection connSapiens = null;
	PreparedStatement st3 = null;
	ResultSet rsEquip = null;

	try {

	    connSapiens = PersistEngine.getConnection("SAPIENS");
	    StringBuffer sqlEquip = new StringBuffer();
	    sqlEquip.append(" SELECT cvs.USU_CodSer ");
	    sqlEquip.append(" FROM USU_T160SIG sig ");
	    sqlEquip.append(" INNER JOIN USU_T160CVS cvs ON cvs.usu_codemp = sig.usu_codemp AND cvs.usu_numctr = sig.usu_numctr AND cvs.usu_numpos = sig.usu_numpos ");
	    sqlEquip.append(" WHERE sig.usu_codcli = ? ");
	    sqlEquip.append(" AND cvs.usu_codser IN ('9002011', '9002004', '9002005', '9002014') ");
	    sqlEquip.append(" ORDER BY cvs.usu_sitcvs ");
	    st3 = connSapiens.prepareStatement(sqlEquip.toString());
	    st3.setString(1, cdCliente);
	    rsEquip = st3.executeQuery();

	    // SOO - COM CAD - SUELLEN ROSENBROCK
	    Integer colEquip = 107655;
	    if (rsEquip.next()) {
		colEquip = null;
	    }
	    conn = PersistEngine.getConnection("SIGMA90");

	    StringBuilder sqlTec = new StringBuilder();
	    sqlTec.append("    SELECT C.CD_TECNICO_RESPONSAVEL, R.NM_ROTA FROM dbCENTRAL C WITH (NOLOCK)  ");
	    sqlTec.append("    LEFT JOIN ROTA R ON R.CD_ROTA = C.ID_ROTA  ");
	    sqlTec.append("    WHERE C.CD_CLIENTE = ?  ");
	    stTec = conn.prepareStatement(sqlTec.toString());
	    stTec.setString(1, cdCliente);
	    rsTec = stTec.executeQuery();

	    while (rsTec.next()) {
		nmRota = rsTec.getString("NM_ROTA");
		tecResp = rsTec.getString("CD_TECNICO_RESPONSAVEL");
		break;
	    }
	    if (nmRota != null && !nmRota.isEmpty()) {
		StringBuilder sqlCol = new StringBuilder();
		sqlCol.append("  SELECT CD_COLABORADOR FROM COLABORADOR WITH (NOLOCK) WHERE NM_COLABORADOR LIKE '" + nmRota.substring(0, 3) + "%ANL%' AND FG_ATIVO_COLABORADOR = 1  ");
		stCol = conn.prepareStatement(sqlCol.toString());
		rsCol = stCol.executeQuery();
		while (rsCol.next()) {
		    insResp = rsCol.getString("CD_COLABORADOR");
		    break;
		}

	    }

	    if (colEquip != null) {
		cdTecnico = colEquip.toString();
	    } else if (insResp != null && !insResp.isEmpty()) {
		cdTecnico = insResp;
	    } else if (tecResp != null && !tecResp.isEmpty()) {
		cdTecnico = tecResp;
	    } else if (cdTecnico == null || cdTecnico.isEmpty()) {
		cdTecnico = "9999";
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    log.error("##### ERRO PESQUISAR RESP TEC EVENT ALERT verificaEventoHistorico: " + e.getMessage() + " - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss "));
	} finally {
	    OrsegupsUtils.closeConnection(connSapiens, st3, rsEquip);
	    OrsegupsUtils.closeConnection(null, stCol, rsCol);
	    OrsegupsUtils.closeConnection(conn, stTec, rsTec);
	}
	return cdTecnico;

    }

    public void abreOsSigma(String codigoHistorico, String descricao) {
	String complemento = " Operador : " + PortalUtil.getCurrentUser().getFullName() + " - ";
	complemento += " " + descricao;
	try {
	    if (codigoHistorico != null && !codigoHistorico.isEmpty()) {
		CallAlertEventVO alertEventVo = OrsegupsAlertEventEngine.callingAlertEventVO.get(codigoHistorico);
		if (alertEventVo.getCallEventoVO().getCodigoEvento().equals("XXX1")) {
		    //String cdUsuario = (String) getUsuarioSigma();
		    String tecResp = (String) getTecnicoResponsavel(alertEventVo.getCallEventoVO().getCodigoCliente());
		    this.abrirOSSigma(alertEventVo.getCallEventoVO().getCodigoCliente(), alertEventVo.getCallEventoVO().getParticao(), alertEventVo.getCallEventoVO().getCodigoCentral(), alertEventVo.getCallEventoVO()
			    .getEmpresa(), "10010", "122", tecResp, "VERIFICAR FALHA X1. " + complemento);
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}

    }

//    /**
//     * Me para geraÃ§Ã£o de XVID para os cÃ³digos de eventos especificados no
//     * e-form X, onde a conta do cliente possua a flag STSERVIDORCFTV=1
//     * (indicando que o mesmo possui monitoramento de imagens), para eventos em
//     * espera (FG_STATUS=1). ApÃ³s gerar o XVID o evento inicial Ã© finalizado.
//     * 
//     * @return String indicando status da operaÃ§Ã£o.
//     */
//    public synchronized String substituirEventosPorXVID() {
//
//	String returnFromAccess = "";
//
//	// sigmaSubstituirEvento
//
//	List<NeoObject> listaSubstituicao = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("sigmaSubstituirEvento"));
//
//	if (listaSubstituicao != null && !listaSubstituicao.isEmpty()) {
//
//	    for (NeoObject objSubs : listaSubstituicao) {
//
//		EntityWrapper wSubstituicao = new EntityWrapper(objSubs);
//		boolean substituir = (boolean) wSubstituicao.findField("substituir").getValue();
//
//		if (substituir) {
//		    Connection conn = null;
//		    Statement stmt = null;
//		    ResultSet rs = null;
//
//		    Map<Long, EventoRecebido> listaEventosRecebidos = null;
//
//		    String eventos = "";
//
//		    List<NeoObject> listaEventos = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("sigmaCodigoEventosSubstituidos"));
//
//		    if (listaEventos != null && !listaEventos.isEmpty()) {
//
//			int count = 0;
//
//			for (NeoObject objEvento : listaEventos) {
//
//			    EntityWrapper wEvento = new EntityWrapper(objEvento);
//			    String cdEvento = (String) wEvento.findField("codigoEvento").getValue();
//
//			    if (count == 0) {
//				eventos += " H.CD_EVENTO='" + cdEvento + "' ";
//			    } else {
//				eventos += " OR H.CD_EVENTO='" + cdEvento + "' ";
//			    }
//			    count++;
//
//			}
//		    }
//
//		    // sigmaCodigoEventosSubstituidos
//
//		    try {
//
//			String sql = "SELECT H.CD_HISTORICO, H.CD_EVENTO, H.DT_RECEBIDO, C.CD_CLIENTE, C.ID_CENTRAL, C.PARTICAO, C.ID_EMPRESA FROM HISTORICO H WITH(NOLOCK) "
//				+ " INNER JOIN dbCENTRAL C WITH(NOLOCK) ON H.CD_CLIENTE = C.CD_CLIENTE " 
//				+ " WHERE H.FG_STATUS IN (0,1) " 
//				+ " AND (" + eventos + ") AND C.STSERVIDORCFTV=1 AND H.CD_CODE NOT IN ('MPI') ";
//
//			conn = PersistEngine.getConnection("SIGMA90");
//			stmt = conn.createStatement();
//			rs = stmt.executeQuery(sql);
//
//			listaEventosRecebidos = new HashMap<Long, EventoRecebido>();
//
//			while (rs.next()) {
//
//			    EventoRecebido eventoRecebido = new EventoRecebido();
//
//			    eventoRecebido.setCodigo("XVID");
//			    eventoRecebido.setData(new GregorianCalendar());
//			    eventoRecebido.setEmpresa(rs.getLong("ID_EMPRESA"));
//			    eventoRecebido.setIdCentral(rs.getString("ID_CENTRAL"));
//			    eventoRecebido.setTipoIntegracao(Byte.parseByte("2"));
//			    eventoRecebido.setParticao(rs.getString("PARTICAO"));
//			    eventoRecebido.setProtocolo(Byte.parseByte("2"));
//			    eventoRecebido.setCdCliente(rs.getInt("CD_CLIENTE"));
//
//			    listaEventosRecebidos.put(rs.getLong("CD_HISTORICO"), eventoRecebido);
//
//			}
//
//		    } catch (Exception e) {
//			e.printStackTrace();
//		    } finally {
//			OrsegupsUtils.closeConnection(conn, stmt, rs);
//		    }
//
//		    for (Long key : listaEventosRecebidos.keySet()) {
//
//			ReceptorEventosWebServiceProxy webServiceProxy = new ReceptorEventosWebServiceProxy();
//
//			long cdEventoXVID = this.verificaEventoAberto(listaEventosRecebidos.get(key));
//
//			if (cdEventoXVID == 0) {
//			    try {
//				returnFromAccess = webServiceProxy.receberEvento(listaEventosRecebidos.get(key));
//			    } catch (RemoteException e) {
//				e.printStackTrace();
//			    }
//			} else {
//			    returnFromAccess = "ACK";
//			}
//
//			if (returnFromAccess.equalsIgnoreCase("ACK")) {
//			    this.fechaEvento(key, cdEventoXVID);
//			}
//
//		    }
//		}
//
//	    }
//
//	}
//
//	return returnFromAccess;
//    }

//    /**
//     * Verifica se existe evento aberto na conta do cliente em situaÃ§Ã£o 0
//     * (nÃ£o atendido)
//     * 
//     * @param eventoRecebido
//     * @return boolean true se existe evento; false se nÃ£o
//     */
//    private long verificaEventoAberto(EventoRecebido eventoRecebido) {
//
//	Connection conn = null;
//	Statement stmt = null;
//	ResultSet rs = null;
//
//	try {
//
//	    String sql = "SELECT H.CD_HISTORICO FROM HISTORICO H WITH(NOLOCK) WHERE H.FG_STATUS IN (0, 5) AND H.CD_EVENTO = '" + eventoRecebido.getCodigo() + "' AND H.CD_CLIENTE = " + eventoRecebido.getCdCliente();
//
//	    conn = PersistEngine.getConnection("SIGMA90");
//	    stmt = conn.createStatement();
//	    rs = stmt.executeQuery(sql);
//
//	    if (rs.next()) {
//
//		return rs.getLong("CD_HISTORICO");
//
//	    } else {
//
//		return 0;
//	    }
//
//	} catch (Exception e) {
//	    e.printStackTrace();
//	} finally {
//	    OrsegupsUtils.closeConnection(conn, stmt, rs);
//	}
//
//	return 0;
//
//    }

    public void abrirLayout(String codigoCentral, String codigoCliente, HttpServletRequest request) {

	String usuario = "admin";
	String senha = "seventh";

	InetAddress address;
	String ip = "";
	String porta = "8080";

	System.out.println(request.getRemoteAddr());
	try {
	    address = InetAddress.getByName(request.getRemoteHost());
	    if (address != null && address.getHostName() != null && !address.getHostName().isEmpty()) {
		ip = address.getHostName();
		String hostName[] = ip.split("\\.");
		ip = hostName[0];

		if (ip != null && !ip.isEmpty()) {
		    SeventhDAOImpl dguard = new SeventhDAOImpl();
		    try{
			dguard.layoutslist(ip, porta, usuario, senha, codigoCentral, request);
		    }catch (Exception e){
			log.error("OrsegupsAlertEventEngine Falha ao abrir layout em "+ip+":"+porta+" u: "+usuario+" s: "+senha+" central: "+codigoCentral);
		    }
		}

	    }
	} catch (UnknownHostException e1) {
	    e1.printStackTrace();
	}

    }

//    private boolean fechaEvento(long cdHistorico, long cdHistoricoXVID) {
//
//	Connection conn = null;
//	PreparedStatement ps = null;
//
//	String historico = "";
//
//	if (cdHistoricoXVID != 0) {
//	    historico = String.valueOf(cdHistoricoXVID);
//	}
//
//	try {
//
//	    String sql = " UPDATE HISTORICO SET FG_STATUS = 4, TX_OBSERVACAO_FECHAMENTO = 'Fechamento automático. Evento sendo tratado via XVID " + historico + "',  "
//		    + " DT_FECHAMENTO = GETDATE(), CD_USUARIO_FECHAMENTO = 11010 " + " WHERE ( CD_HISTORICO_PAI = ? OR CD_HISTORICO = ? ) ";
//
//	    conn = PersistEngine.getConnection("SIGMA90");
//	    ps = conn.prepareStatement(sql);
//	    ps.setLong(1, cdHistorico);
//	    ps.setLong(2, cdHistorico);
//
//	    int rs = ps.executeUpdate();
//
//	    int updateCount = 0;
//
//	    while (rs < 1 && updateCount < 2) {
//		rs = ps.executeUpdate();
//		updateCount++;
//	    }
//
//	    if (rs > 0) {
//		return true;
//	    }
//
//	} catch (Exception e) {
//	    e.printStackTrace();
//	    throw new JobException("Erro no processamento, procurar no log por [ERRO_FECHAMENTO_EVENTO_ALARME]");
//	} finally {
//	    OrsegupsUtils.closeConnection(conn, ps, null);
//	}
//
//	return false;
//    }

}
