package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.HtmlEmail;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.integracaoMobile.IntegracaoPortalMobile;
import com.neomind.fusion.custom.orsegups.objRatMobile.ObjRatMobile;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsEmailUtils;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

/**
 * Método para envio de e-mail para os clientes
 * 
 * quando a ordem de serviço for finalizada
 * 
 * no e-mail será enviado um breve texto contendo o Nº da OS junto com os dados
 * do cliente e a foto do técnico
 * 
 * com a descrição do fechamento da ordem de serviço feito pelo técnico.
 * 
 * @author William Bastos
 * 
 * @param CustomJobContext
 *            arg0
 * 
 * @return void
 */

public class EmailDeliveryOSFechamento implements CustomJobAdapter {
    private static final Log log = LogFactory.getLog(EmailDeliveryOSFechamento.class);

    @SuppressWarnings("unchecked")
    @Override
    public void execute(CustomJobContext arg0) {

	int adicionados = 0;

	log.warn("E-Mail OS executado em: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	Long key = GregorianCalendar.getInstance().getTimeInMillis();

	Set<Integer> empresasNotificadas = new HashSet<Integer>();

	Connection conn = null;
	StringBuilder strSigma = null;
	PreparedStatement pstm = null;
	ResultSet rs = null;

	try {

	    conn = PersistEngine.getConnection("SIGMA90");

	    strSigma = new StringBuilder();

	    InstantiableEntityInfo infoEnvioEmail = (InstantiableEntityInfo) EntityRegister.getEntityInfo("EmailDelivery");

	    InstantiableEntityInfo infoHis = (InstantiableEntityInfo) EntityRegister.getEntityInfo("OSEmailAutomaticoAtraso");
	    InstantiableEntityInfo infoTec = (InstantiableEntityInfo) EntityRegister.getEntityInfo("OSEmailAutomaticoTecnicos");
	    InstantiableEntityInfo infoCadTec = (InstantiableEntityInfo) EntityRegister.getEntityInfo("OSEmaiCadastrolAutomaticoTecnicosCM");

	    String ultimaExecucaoRotina = ultimaExecucaoRotina();
	    String ultimaExecucaoRotinaAux = ultimaExecucaoRotinaRange();

	    strSigma.append(" 	SELECT c.OBSERVACAO, c.CGCCPF, he.IDOSHISTORICO, he.ID_ORDEM, c.RESPONSAVEL, c.EMAILRESP, ");
	    strSigma.append(" 	c.RAZAO, c.ENDERECO, bai.NOME AS NOMEBAIRRO, cid.NOME AS NOMECIDADE, ");
	    strSigma.append(" 	col.CD_COLABORADOR, c.CD_CLIENTE, col.NM_COLABORADOR, d.DESCRICAODEFEITO,os.DEFEITO,os.EXECUTADO,ISNULL(p.TX_OBSERVACAO,'Vazio') AS TEXTO_PAUSA, c.ID_EMPRESA, OS.FECHAMENTO  ");
	    strSigma.append(" 	FROM OSHISTORICO he ");
	    strSigma.append(" 	INNER JOIN dbORDEM os ON os.ID_ORDEM = he.ID_ORDEM ");
	    strSigma.append(" 	INNER JOIN dbo.OSDEFEITO d on d.IDOSDEFEITO = os.IDOSDEFEITO ");
	    strSigma.append(" 	INNER JOIN COLABORADOR col ON col.CD_COLABORADOR = os.ID_INSTALADOR	");
	    strSigma.append(" 	INNER JOIN dbCENTRAL c ON c.CD_CLIENTE = os.CD_CLIENTE ");
	    strSigma.append(" 	INNER JOIN dbCIDADE cid ON c.ID_CIDADE = cid.ID_CIDADE ");
	    strSigma.append(" 	INNER JOIN dbBAIRRO bai ON c.ID_CIDADE = bai.ID_CIDADE AND c.ID_BAIRRO = bai.ID_BAIRRO ");
	    strSigma.append(" 	LEFT JOIN ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p WITH (NOLOCK)	  ");
	    strSigma.append(" 	ON p.CD_ORDEM_SERVICO = os.ID_ORDEM  ");
	    strSigma.append(" 	AND p.CD_ORDEM_SERVICO_MOTIVO_PAUSA = (SELECT MAX(p2.CD_ORDEM_SERVICO_MOTIVO_PAUSA)  ");
	    strSigma.append(" 	FROM ASSOC_ORDEM_SERVICO_MOTIVO_PAUSA p2 WITH (NOLOCK)	 WHERE p2.CD_ORDEM_SERVICO = p.CD_ORDEM_SERVICO ) ");
	    strSigma.append("   WHERE C.TP_PESSOA != 2 AND ( OS.FECHAMENTO > '" + ultimaExecucaoRotina + "' OR  OS.FECHAMENTO > '" + ultimaExecucaoRotinaAux + "' )  ");
	    strSigma.append(" 	AND os.DATAEXECUTADA_MOB IS NULL ");
	    strSigma.append(" 	AND c.TP_PESSOA in (0,1) ");
	    strSigma.append(" 	AND os.FECHADO = 1 ");
	    strSigma.append("   AND ( COL.NM_COLABORADOR LIKE '%TERC%MANUT%' OR COL.NM_COLABORADOR NOT LIKE '%TERC%' ) ");
	    strSigma.append("   AND col.nm_colaborador not like 'cm%' ");
	    strSigma.append("   AND OPFECHOU NOT IN (11010, 9999) ");
	    strSigma.append(" 	ORDER BY OS.FECHAMENTO ");

	    pstm = conn.prepareStatement(strSigma.toString());

	    inserirFimRotina();

	    rs = pstm.executeQuery();

	    while (rs.next()) {
		boolean clienteComExcecao = OrsegupsEmailUtils.verificarExcecaoCNPJ(rs.getInt("CD_CLIENTE"));
		String observacao = rs.getString("OBSERVACAO") == null ? "" : rs.getString("OBSERVACAO");
		if (!clienteComExcecao) {

		    QLGroupFilter queryGroup = new QLGroupFilter("AND");
		    queryGroup.addFilter(new QLEqualsFilter("idOrdem", rs.getString("ID_ORDEM")));
		    queryGroup.addFilter(new QLEqualsFilter("cdTecnico", rs.getString("CD_COLABORADOR")));

		    NeoObject openOS = PersistEngine.getObject(infoHis.getEntityClass(), queryGroup);
		    if (openOS == null) {

			List<NeoObject> listaEmails = PersistEngine.getObjects(infoTec.getEntityClass(), new QLEqualsFilter("codigo", rs.getString("CD_COLABORADOR")), -1, -1);

			if (listaEmails != null && !listaEmails.isEmpty()) {
			    EntityWrapper wpneo = new EntityWrapper(listaEmails.get(0));

			    String cgcCpf = rs.getString("CGCCPF") == null ? "" : rs.getString("CGCCPF").replaceAll("([.\\-/])", "").trim();
			    String idOrdem = rs.getString("ID_ORDEM");
			    String responsavel = (rs.getString("RESPONSAVEL") == null ? "" : rs.getString("RESPONSAVEL"));
			    String email = (rs.getString("EMAILRESP") == null ? "" : rs.getString("EMAILRESP"));
			    String razao = (rs.getString("RAZAO") == null ? "" : rs.getString("RAZAO"));
			    String endereco = (rs.getString("ENDERECO") == null ? "" : rs.getString("ENDERECO"));
			    String cidade = (rs.getString("NOMECIDADE") == null ? "" : rs.getString("NOMECIDADE"));
			    String bairro = (rs.getString("NOMEBAIRRO") == null ? "" : rs.getString("NOMEBAIRRO"));
			    String cdCliente = rs.getString("CD_CLIENTE");
			    String matriculaTecnico = (String) wpneo.findValue("matricula");
			    String nomeTecnico = wpneo.findValue("nome").toString().toUpperCase();
			    String idOsHistorico = rs.getString("IDOSHISTORICO");
			    String cdTecnico = rs.getString("CD_COLABORADOR");
			    // String tipoOS = rs.getString("DESCRICAODEFEITO");
			    String defeito = (rs.getString("DEFEITO") != null ? rs.getString("DEFEITO") : "");
			    // Retirado - analisar
			    // String defeito = NeoUtils.encodeHTMLValue(rs.getString("DEFEITO").replaceAll("#", ""));
			    String executado = rs.getString("EXECUTADO");
			    String textoPausa = rs.getString("TEXTO_PAUSA");

			    int empresa = rs.getInt("ID_EMPRESA");

			    // TODO UTILIZAR DATA E HORA
			    Date dtFechamento = rs.getDate("FECHAMENTO");
			    String dataAtendimento = NeoDateUtils.safeDateFormat(dtFechamento, "dd/MM/yyyy");
			    String horaAtendimento = rs.getString("FECHAMENTO").substring(11, 16);

			    String descricao = "Vazio";

			    if (executado != null && !executado.isEmpty()) {
				descricao = executado;
			    } else if (textoPausa != null && !textoPausa.isEmpty() && !textoPausa.equals("Vazio")) {
				descricao = textoPausa;
			    }
			    boolean flagAtualizadoNAC = observacao.contains("#AC");
			    // ESTA É MINHA ADAPTAÇÃO
			    List<String> emailClie = OrsegupsEmailUtils.validarEmail(email, cdCliente, flagAtualizadoNAC);

			    if ((emailClie != null) && (!emailClie.isEmpty())) {

				StringBuilder noUserMsg = new StringBuilder();

				final String tipo = OrsegupsEmailUtils.TIPO_OS;

				Map<Integer, String> params = OrsegupsEmailUtils.getConfiguracaoEmpresa(empresa);

				String pasta = null;
				String remetente = null;
				String grupo = null;
				if (params != null) {

				    pasta = params.get(OrsegupsEmailUtils.RAT_PASTA);
				    remetente = params.get(OrsegupsEmailUtils.RAT_REMETENTE);
				    grupo = params.get(OrsegupsEmailUtils.RAT_GRUPO);
				    String neoId = params.get(OrsegupsEmailUtils.RAT_NEOID);
				    Long tipRat = 5L;
				    String ratingToken = DigestUtils.sha256Hex(cgcCpf + Long.toString(tipRat) + Long.toString(new java.util.Date().getTime()));
				    
				    for (String emailFor : emailClie) {

					noUserMsg = new StringBuilder();

					noUserMsg.append(OrsegupsEmailUtils.getCabecalhoEmail(tipo, pasta, ratingToken));

					noUserMsg.append(" <!--- main content table begins--->");
					noUserMsg.append(" <table width=\"600\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\">");
					noUserMsg.append(" <tbody><tr><td colspan=\"3\"><img width=\"1\" height=\"15\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif\" alt=\"\"></td></tr>");
					noUserMsg.append(" <tr><td width=\"10\"></td>");
					noUserMsg.append(" <td align=\"left\" style=\"FONT-WEIGHT: regular; FONT-SIZE: 18px; COLOR: #0298d5; FONT-FAMILY: Helvetica, Arial, sans-serif;\">");
					noUserMsg.append(" <font style=\"FONT-WEIGHT: regular; FONT-SIZE: 18px; COLOR: #0298d5; FONT-FAMILY: Helvetica, Arial, sans-serif;\">Prezado Cliente,</font>");
					noUserMsg.append(" <br>");
					noUserMsg.append(" </td><td width=\"10\"></td>");
					noUserMsg.append(" </tr>");
					noUserMsg.append(" <tr><td width=\"10\"></td>");
					noUserMsg.append(" <td align=\"left\">");
					noUserMsg.append("  <img width=\"10\" height=\"22\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif\" alt=\"\"></td><td width=\"10\"></td>");
					noUserMsg.append(" </tr>");
					noUserMsg.append(" <tr><td width=\"10\"><img width=\"10\" height=\"15\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif\" alt=\"\"></td>");
					noUserMsg.append(" <td style=\"FONT-WEIGHT: regular; FONT-SIZE: 18px; FONT-FAMILY: Helvetica, Arial, sans-serif;\">");
					noUserMsg.append(" Informamos que sua ordem de serviço " + idOrdem + " foi atendida. Abaixo segue o relato do técnico " + nomeTecnico + ".<br>");
					noUserMsg.append(" <br>");
					noUserMsg.append(" <font style=\"FONT-WEIGHT: regular; FONT-SIZE: 18px;FONT-FAMILY: Helvetica, Arial, sans-serif;\">Relato: </font><br>");
					noUserMsg.append(" <br>");
					noUserMsg.append(" <font style=\"FONT-WEIGHT: regular; FONT-SIZE: 18px; FONT-FAMILY: Helvetica, Arial, sans-serif;\">" + descricao.toUpperCase() + "</font>");
					noUserMsg.append(" <br>");
					noUserMsg.append(" <br>");
					noUserMsg.append(" <div align=\"center\">");

					Long neoIdFoto = ((NeoFile) wpneo.getValue("fotoTec")).getNeoId();

					if (neoIdFoto == null || neoIdFoto.equals(0L)) {
					    noUserMsg.append(" <img style=\"max-height:226px\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/default-user.png\" border=\"0\" alt=\"Técnico Responsável\"></div>");
					} else {
					    noUserMsg.append(" <img style=\"max-height:226px\" src=\"http://intranet.orsegups.com.br/fusion/file/download/" + ((NeoFile) wpneo.getValue("fotoTec")).getNeoId() + "\" border=\"0\" alt=\"Técnico Responsável\"></div>");
					}

					noUserMsg.append(" <div align=\"center\" style=\"font-size:14px;;line-height:18px; color:#0298d5; font-weight:bold; font-family:Helvetica, Arial, sans-serif\">" + nomeTecnico + "</div> ");
					noUserMsg.append(" <div align=\"center\" style=\"FONT-WEIGHT: normal; FONT-SIZE: 10px; COLOR: #999999; font-family:Arial;valign:top;\">Matrícula: " + matriculaTecnico + "&nbsp;*</div><br>");
					noUserMsg.append(" <br>");
					noUserMsg.append(" <div align=\"left\" style=\"FONT-WEIGHT: normal; FONT-SIZE: 10px; COLOR: #999999; font-family:Arial;valign:top;\">*&nbsp;Para sua segurança, solicite ao nosso técnico o crachá de identificação.</div>");
					noUserMsg.append(" <br>");
					noUserMsg.append(" O atendimento refere-se ao seu sistema de segurança instalado no endereço abaixo:<br><br>");
					noUserMsg.append(" <strong>Nome/Razão Social: </strong> " + razao + "<br>");
					noUserMsg.append(" <strong>Endereço: </strong>" + endereco + "<br>");
					noUserMsg.append(" <strong>Bairro: </strong>" + bairro + "<br>");
					noUserMsg.append(" <strong>Cidade: </strong>" + cidade);
					noUserMsg.append(" <br>");
					noUserMsg.append(" <strong>Descrição da OS: </strong>" + defeito.toUpperCase());

					noUserMsg.append(" </td><td width=\"10\"><img width=\"10\" height=\"15\" src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/spacer_clear.gif\" alt=\"\"></td>");
					noUserMsg.append(" </tr>");
					// noUserMsg.append(" <td align=\"left\" style=\"FONT-WEIGHT: regular; FONT-SIZE: 18px; COLOR: #0298d5; FONT-FAMILY: Helvetica, Arial, sans-serif;\">");
					// noUserMsg.append(" <font style=\"FONT-WEIGHT: regular; FONT-SIZE: 18px; COLOR: #0298d5; FONT-FAMILY: Helvetica, Arial, sans-serif;\">Prezado Cliente,</font>");
					// noUserMsg.append(" <br>");
					// noUserMsg.append(" </td><td width=\"10\"></td>");
					noUserMsg.append(" </tbody></table>");
					noUserMsg.append(" <!--- main content table ends--->");

					noUserMsg.append(OrsegupsEmailUtils.getRodapeEmail(tipo, pasta, grupo, email, remetente, ratingToken));

					NeoObject emailHis = infoHis.createNewInstance();
					EntityWrapper emailHisWp = new EntityWrapper(emailHis);
					adicionados = adicionados + 1;
					emailHisWp.findField("idOrdem").setValue(idOrdem);
					emailHisWp.findField("responsavel").setValue(responsavel);
					emailHisWp.findField("email").setValue(emailClie.toString());
					emailHisWp.findField("razao").setValue(razao);
					emailHisWp.findField("endereco").setValue(endereco);
					emailHisWp.findField("cidade").setValue(cidade);
					emailHisWp.findField("bairro").setValue(bairro);
					emailHisWp.findField("cdCliente").setValue(cdCliente);
					emailHisWp.findField("enviadoPara").setValue(emailClie.toString());
					emailHisWp.findField("idHistorico").setValue(idOsHistorico);
					emailHisWp.findField("cdTecnico").setValue(cdTecnico);
					PersistEngine.persist(emailHis);

					// TESTE E-MAIL UTILIZANDO RECURSO FUSION
					//String subject = "Atendimento concluído: " + idOrdem;
					//OrsegupsEmailUtils.sendTestEmail(noUserMsg, subject);

					GregorianCalendar dataCad = (GregorianCalendar) GregorianCalendar.getInstance();
					NeoObject emaiEnvio = infoEnvioEmail.createNewInstance();
					EntityWrapper emailEnvioWp = new EntityWrapper(emaiEnvio);
					emailEnvioWp.findField("de").setValue("os" + remetente);
					emailEnvioWp.findField("para").setValue(emailFor + ";emailautomatico@orsegups.com.br;dirceu.costa@orsegups.com.br");
					emailEnvioWp.findField("assunto").setValue("Atendimento concluído: " + idOrdem);
					emailEnvioWp.findField("Mensagem").setValue(noUserMsg.toString());
					emailEnvioWp.findField("datCad").setValue(dataCad);
					PersistEngine.persist(emaiEnvio);

					try {
					    // Montando informações para serviço Mobile - Inicio
					    ObjRatMobile objRat = new ObjRatMobile();
					    objRat.setTipRat(tipRat);
					    objRat.setRatingToken(ratingToken);
					    objRat.setInformativo("Prezado Cliente, Informamos que sua ordem de serviço " + idOrdem + " foi atendida. Abaixo segue o relato do técnico " + nomeTecnico);
					    objRat.setEvento("Atendimento concluído: " + idOrdem);
					    objRat.setResultadoDoAtendimento(executado);
					    objRat.setHashId("Atendimento concluído: " + idOrdem);
					    objRat.setResultado("O atendimento refere-se ao seu sistema de segurança instalado no endereço abaixo:");
					    objRat.setAtendidoPor(nomeTecnico);
					    objRat.setLocal(razao + " - " + endereco + " - " + bairro + " - " + cidade);
					    objRat.setDataAtendimento(dataAtendimento);
					    objRat.setHoraAtendimento(horaAtendimento);
					    objRat.setLnkFotoAit("http://intranet.orsegups.com.br/fusion/file/download/" + ((NeoFile) wpneo.getValue("fotoTec")).getNeoId());
					    objRat.setLnkFotoLocal("");
					    objRat.setObservacao(defeito.toUpperCase());
					    objRat.setEmpRat(grupo);
					    objRat.setNeoId(neoId);

					    if (!cgcCpf.equals("")) {
						objRat.setCgcCpf(Long.parseLong(cgcCpf));
						IntegracaoPortalMobile integracao = new IntegracaoPortalMobile();
						integracao.inserirInformacoesPush(objRat);
					    }
					} catch (Exception e) {
					    log.error("Erro IntegracaoPortalMobile EmailDeliveryOSFechamento", e);

					}
				    }

				} else {
				    if (!empresasNotificadas.contains(empresa)) {
					OrsegupsEmailUtils.enviarNotificacaoEmpresaSemConfig(empresa);
					empresasNotificadas.add(empresa);
				    }
				}

			    }

			} else {
			    // envio email adastro tecnico

			    List<NeoObject> listaEmailsEnviadosCM = PersistEngine.getObjects(infoCadTec.getEntityClass(), new QLEqualsFilter("cdTecnico", rs.getString("CD_COLABORADOR")), -1, -1);

			    if (listaEmailsEnviadosCM.isEmpty()) {
				MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

				MailSettings mailClone = new MailSettings();
				mailClone.setMinutesInterval(settings.getMinutesInterval());
				mailClone.setRenderServer(settings.getRenderServer());
				mailClone.setSecurityFromEmail(settings.getSecurityFromEmail());
				mailClone.setSecurityCopyEmail(settings.getSecurityCopyEmail());
				mailClone.setSmtpSettings(settings.getSmtpSettings());
				mailClone.setPort(settings.getPort());
				mailClone.setSmtpServer(settings.getSmtpServer());
				mailClone.setEnabled(settings.isEnabled());

				mailClone.setFromEMail("os@orsegups.com.br");
				mailClone.setFromName("Orsegups Partipações S.A.");
				
				if (mailClone != null && mailClone.isEnabled()) {

				    HtmlEmail noUserEmail = new HtmlEmail();
				    StringBuilder noUserMsg = new StringBuilder();

				    Calendar saudacao = Calendar.getInstance();
				    String saudacaoEMail = "";
				    if (saudacao.get(Calendar.HOUR_OF_DAY) >= 6 && saudacao.get(Calendar.HOUR_OF_DAY) <= 12) {
					saudacaoEMail = "Bom dia, ";
				    } else if (saudacao.get(Calendar.HOUR_OF_DAY) > 12 && saudacao.get(Calendar.HOUR_OF_DAY) <= 18) {
					saudacaoEMail = "Boa tarde, ";
				    } else {
					saudacaoEMail = "Boa noite, ";
				    }

				    noUserEmail.setCharset("ISO-8859-1");
				    noUserEmail.addTo("cm@orsegups.com.br");

				    noUserEmail.setFrom("os@orsegups.com.br");
				    noUserEmail.setSubject("Não Responda - Cadastro de Técnico " + rs.getString("NM_COLABORADOR"));

				    noUserMsg.append("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></head><body>");
				    noUserMsg.append("<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#ffffff\">");
				    noUserMsg.append("          <tbody>");
				    noUserMsg.append("		    <td align=\"left\" valign=\"top\" >");
				    noUserMsg.append("               <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/logoORS.jpg\" >");
				    noUserMsg.append("            </td>");
				    noUserMsg.append("			<td>");
				    noUserMsg.append("			<strong>" + saudacaoEMail + " favor providenciar o cadastro do técnico para execução de ordens de serviço.</strong><br>");
				    noUserMsg.append("			<strong>Código: </strong> " + rs.getString("CD_COLABORADOR") + " <br>");
				    noUserMsg.append("			<strong>Nome: </strong>" + rs.getString("NM_COLABORADOR") + " <br>");
				    noUserMsg.append("			</td>");
				    noUserMsg.append("     </tbody></table>");
				    noUserMsg.append("</body></html>");
				    noUserEmail.setHtmlMsg(noUserMsg.toString());
				    noUserEmail.setContent(noUserMsg.toString(), "text/html");
				    mailClone.applyConfig(noUserEmail);

				    noUserEmail.send();

				    NeoObject emailCadTec = infoCadTec.createNewInstance();
				    EntityWrapper emailHisWp = new EntityWrapper(emailCadTec);
				    emailHisWp.findField("cdTecnico").setValue(rs.getString("CD_COLABORADOR"));
				    PersistEngine.persist(emailCadTec);

				}
			    }
			}
		    }
		}
	    }
	}

	catch (Exception e) {
	    log.error("E-Mail OS erro no processamento:" + e.getMessage());
	    System.out.println("[" + key + "] E-Mail OS erro no processamento: " + e.getMessage());
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, rs);
	}

    }

    private String ultimaExecucaoRotina() {

	String retorno = "";
	try {
	    Collection<NeoObject> monitoraAgendador = null;

	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("nomeRotina", "EmailDeliveryOSFechamento"));

	    monitoraAgendador = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);

	    if (monitoraAgendador != null && !monitoraAgendador.isEmpty()) {
		for (NeoObject neoObject : monitoraAgendador) {
		    EntityWrapper monitoraAgendadorObj = new EntityWrapper(neoObject);

		    GregorianCalendar dataFinalAgendador = (GregorianCalendar) monitoraAgendadorObj.getValue("dataFinalAgendador");

		    retorno = NeoDateUtils.safeDateFormat(dataFinalAgendador, "yyyy-MM-dd HH:mm:ss");
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {

	}
	return retorno;

    }

    private String ultimaExecucaoRotinaRange() {

	String retorno = "";
	try {
	    Collection<NeoObject> monitoraAgendador = null;

	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("nomeRotina", "EmailDeliveryOSFechamento"));

	    monitoraAgendador = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);

	    if (monitoraAgendador != null && !monitoraAgendador.isEmpty()) {
		for (NeoObject neoObject : monitoraAgendador) {
		    EntityWrapper monitoraAgendadorObj = new EntityWrapper(neoObject);

		    GregorianCalendar dataFinalAgendador = (GregorianCalendar) monitoraAgendadorObj.getValue("dataFinalAgendador");
		    GregorianCalendar dataFinalAgendadorAux = (GregorianCalendar) dataFinalAgendador.clone();

		    dataFinalAgendadorAux.add(Calendar.MINUTE, -15);

		    retorno = NeoDateUtils.safeDateFormat(dataFinalAgendadorAux, "yyyy-MM-dd HH:mm:ss");
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {

	}
	return retorno;

    }

    private void inserirFimRotina() {

	try {
	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("nomeRotina", "EmailDeliveryOSFechamento"));

	    List<NeoObject> monitoraAgendador = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);
	    PersistEngine.getEntityManager().flush();

	    if (monitoraAgendador != null && !monitoraAgendador.isEmpty()) {
		NeoObject neoObject = (NeoObject) monitoraAgendador.get(0);

		EntityWrapper monitoraAgendadorWrapper = new EntityWrapper(neoObject);

		monitoraAgendadorWrapper.findField("dataFinalAgendador").setValue(new GregorianCalendar());

		PersistEngine.persist(neoObject);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}

    }
    
}