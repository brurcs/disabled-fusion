package com.neomind.fusion.custom.orsegups.autocargo.importacao;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;

public class ACClientsImport {

    public static String execute() {

	String retorno = "Resultado da execução: ";
	
	Connection conn = null;

	PreparedStatement pstm = null;

	//						 1 	2 		3	 4		 5 		6		 7 		8 			9 	10		 11
	String sql = "INSERT INTO CELTEC_CLIENTE (ID_LEGADO, OPERADORA, NOME_CLIENTE, TELEFONE_CLIENTE, CEP_CLIENTE, RUA_CLIENTE, NUMERO_CLIENTE, COMPLEMENTO_CLIENTE, BAIRRO_CLIENTE, CIDADE_CLIENTE, ESTADO_CLIENTE) "
		+ " VALUES (?,?,?,?,?,?,?,?,?,?,?)";

	FileInputStream file = null;
	
	try {
	    file = new FileInputStream(new File("c:\\Importacao\\ClientesAutoCargo.xlsx"));
	    // Create Workbook instance holding reference to .xlsx file
	    XSSFWorkbook workbook = new XSSFWorkbook(file);

	    // Get first/desired sheet from the workbook
	    XSSFSheet sheet = workbook.getSheetAt(1);

	    // Iterate through each rows one by one
	    Iterator<Row> rowIterator = sheet.iterator();
	    
	    //PULAR PRIMERA LINHA
	    rowIterator.next();

	    conn = PersistEngine.getConnection("IMPORTACOES");

	    pstm = conn.prepareStatement(sql);

	    while (rowIterator.hasNext()) {

		Row row = rowIterator.next();

		Cell cellOperadora = row.getCell(0);
		Cell cellIdLegado = row.getCell(1);
		Cell cellCliente = row.getCell(2);
		Cell cellTelefone = row.getCell(3);
		Cell cellCep = row.getCell(4);
		Cell cellRua = row.getCell(5);
		Cell cellNumero = row.getCell(6);
		Cell cellComplemento = row.getCell(7);
		Cell cellBairro = row.getCell(8);
		Cell cellCidade = row.getCell(9);
		Cell cellEstado = row.getCell(10);

		Double idLegado = cellIdLegado.getNumericCellValue();

		pstm.setInt(1, idLegado.intValue());
		pstm.setString(2, cellOperadora != null ? cellOperadora.getStringCellValue() : "");
		
		String cliente = "";
		
		if (cellCliente != null){
		    cliente = cellCliente.getStringCellValue().toUpperCase();
		}
		
		pstm.setString(3, cliente);
		
		String telefone = "";
		if (cellTelefone != null){
		    telefone = cellTelefone.getStringCellValue();
		    telefone = telefone.replaceAll("[^0-9/() ]", "");
		    telefone = telefone.replace("/", ";");
		}
		
		pstm.setString(4, telefone);

		String cep = "88103400";
		
		if (cellCep != null){
		    switch (cellCep.getCellType()) {
		    case Cell.CELL_TYPE_NUMERIC:
			Double preCep = cellCep.getNumericCellValue();
			int intCep = preCep.intValue();
			cep = String.valueOf(intCep);
			cep = cep.replace("-","");
			cep = cep.replace(".", "");			
			break;
		    case Cell.CELL_TYPE_STRING:
			cep = cellCep.getStringCellValue();
			break;
		    }		    
		}
		pstm.setString(5, cep);
		
		pstm.setString(6, cellRua != null ? cellRua.getStringCellValue().toUpperCase() : "");
		
		
		String numero = "";
		
		if (cellNumero != null){
		    switch (cellNumero.getCellType()) {
		    case Cell.CELL_TYPE_NUMERIC:
			
			Double preNumero = cellNumero.getNumericCellValue();
			int intNumero = preNumero.intValue();
			numero = String.valueOf(intNumero);
			break;
		    case Cell.CELL_TYPE_STRING:
			numero = cellNumero.getStringCellValue();
			break;
		    }		    
		}
		
		pstm.setString(7, numero);
		
		String complemento = "";
		
		if (cellComplemento != null){
		    if (cellComplemento != null){
			switch (cellComplemento.getCellType()) {
			case Cell.CELL_TYPE_NUMERIC:
			    complemento += cellComplemento.getNumericCellValue();
			    break;
			case Cell.CELL_TYPE_STRING:
			    complemento = cellComplemento.getStringCellValue();
			    complemento = complemento.replace("-", "");
			    complemento = complemento.toUpperCase();
			    break;
			}		    
		    }	    
		}
		
		
		pstm.setString(8, complemento);
		
		String bairro = "CENTRO";
		
		if (cellBairro != null){
		    bairro = cellBairro.getStringCellValue().toUpperCase();
		}

		pstm.setString(9, bairro);
		
		String cidade = "SÃO JOSÉ";
		
		if (cellCidade != null){
		    cidade = cellCidade.getStringCellValue().toUpperCase();
		}
		
		pstm.setString(10, cidade);
		
		String estado = "SC";
		if (cellEstado != null){
		    estado = cellEstado.getStringCellValue();
		    
		    if (estado != null && !estado.isEmpty()){
			if(estado.equalsIgnoreCase("SANTA CATARINA")){
			    estado = "SC";
			}else if (estado.equalsIgnoreCase("PARANÁ") || estado.equalsIgnoreCase("PARANA")){
			    estado = "PR";
			}else if(estado.equalsIgnoreCase("SÃO PAULO")){
			    estado = "SP";
			}else if(estado.equalsIgnoreCase("RIO DE JANEIRO")){
			    estado = "RJ";
			}
		    }		    
		}
		
		
		
		pstm.setString(11, estado);

		pstm.addBatch();
		pstm.clearParameters();

		// For each row, iterate through all the columns
		// Iterator<Cell> cellIterator = row.cellIterator();

		// while (cellIterator.hasNext())
		// {
		// Cell cell = cellIterator.next();
		// //Check the cell type and format accordingly
		// switch (cell.getCellType())
		// {
		// case Cell.CELL_TYPE_NUMERIC:
		// System.out.print(cell.getNumericCellValue() + "t");
		// break;
		// case Cell.CELL_TYPE_STRING:
		// System.out.print(cell.getStringCellValue() + "t");
		// break;
		// }
		// }

	    }

	    int[] results = pstm.executeBatch();

	    retorno += results.toString();

	    System.out.println(results);
 
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    try {
		file.close();
	    } catch (IOException e) {
		e.printStackTrace();
	    }
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}
	return retorno;
    }

}
