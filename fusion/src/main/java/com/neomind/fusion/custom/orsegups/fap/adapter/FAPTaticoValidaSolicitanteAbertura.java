package com.neomind.fusion.custom.orsegups.fap.adapter;
import com.neomind.fusion.custom.orsegups.fap.utils.FAPParametrizacao;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.event.TaskFinishEvent;
import com.neomind.fusion.workflow.event.TaskFinishEventListener;
import com.neomind.fusion.workflow.exception.TaskException;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class FAPTaticoValidaSolicitanteAbertura implements TaskFinishEventListener {

	@Override
	public void onFinish(TaskFinishEvent event) throws TaskException {

		EntityWrapper processEntity = new EntityWrapper(event.getProcess().getEntity());

		Long codigoAplicacao = Long.parseLong(processEntity.findField("aplicacaoPagamento.codigo").getValueAsString());
		NeoUser solicitante = (NeoUser) processEntity.findField("solicitanteOrcamento").getValue();
		String nomeSolicitante = solicitante.getCode();
		boolean debug = (FAPParametrizacao.findParameter("debug").equals("1") ? true : false);

		if(codigoAplicacao == 11 && !nomeSolicitante.equalsIgnoreCase("rosileny") && debug == false) {
			throw new WorkflowException("Usuário sem permissão para abrir FAP Tático.");
		}		
	}    
}







