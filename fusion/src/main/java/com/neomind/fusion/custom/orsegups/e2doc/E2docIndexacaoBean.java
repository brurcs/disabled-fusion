package com.neomind.fusion.custom.orsegups.e2doc;

import java.util.Date;
import java.util.Map;

public class E2docIndexacaoBean {
    private String tipo;
    private String modelo; 
    private String classe;
    private String formato; 
    private Long cpf;
    private String tipoDocumento; 
    private String matricula;
    private String colaborador; 
    private String empresa; 
    private String filial;
    private Date data;
    private String observacao; 
    private byte[] documento;
    
    private String nrProcesso;
    private String tipoProcesso;
    private Long cdClienteSapiens;
    private String unidadeFederativa;
    private String cliente;
    
    private String indices;
    
    public String getTipo() {
        return tipo;
    }
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    public String getModelo() {
        return modelo;
    }
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
    public String getClasse() {
        return classe;
    }
    public void setClasse(String classe) {
        this.classe = classe;
    }
    public String getFormato() {
        return formato;
    }
    public void setFormato(String formato) {
        this.formato = formato;
    }
    public Long getCpf() {
        return cpf;
    }
    public void setCpf(Long cpf) {
        this.cpf = cpf;
    }
    public String getTipoDocumento() {
        return tipoDocumento;
    }
    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }
    public String getMatricula() {
        return matricula;
    }
    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
    public String getColaborador() {
        return colaborador;
    }
    public void setColaborador(String colaborador) {
        this.colaborador = colaborador;
    }
    public String getEmpresa() {
        return empresa;
    }
    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }
    public Date getData() {
        return data;
    }
    public void setData(Date data) {
        this.data = data;
    }
    public String getObservacao() {
        return observacao;
    }
    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
    public byte[] getDocumento() {
        return documento;
    }
    public void setDocumento(byte[] documento) {
        this.documento = documento;
    }
    public String getTipoProcesso() {
        return tipoProcesso;
    }
    public void setTipoProcesso(String tipoProcesso) {
        this.tipoProcesso = tipoProcesso;
    }
    public Long getCdClienteSapiens() {
        return cdClienteSapiens;
    }
    public void setCdClienteSapiens(Long cdClienteSapiens) {
        this.cdClienteSapiens = cdClienteSapiens;
    }
    public String getUnidadeFederativa() {
        return unidadeFederativa;
    }
    public void setUnidadeFederativa(String unidadeFederativa) {
        this.unidadeFederativa = unidadeFederativa;
    }
    public String getNrProcesso() {
        return nrProcesso;
    }
    public void setNrProcesso(String nrProcesso) {
        this.nrProcesso = nrProcesso;
    }
    public String getFilial() {
        return filial;
    }
    public void setFilial(String filial) {
        this.filial = filial;
    }
	public String getCliente()
	{
		return cliente;
	}
	public void setCliente(String cliente)
	{
		this.cliente = cliente;
	}
	public String getIndices() {
	    return indices;
	}
	public void setIndices(String indices) {
	    this.indices = indices;
	}    
    
}
