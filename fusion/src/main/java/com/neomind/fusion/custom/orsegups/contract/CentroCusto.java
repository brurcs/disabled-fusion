package com.neomind.fusion.custom.orsegups.contract;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.Query;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.adapter.CadastraCentroCusto;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.adapter.casvig.OrsegupsContratoUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

/** 
 * Classe para criação e manipulação de centro de custos. Foi escrita para remover a dependencia direta dos campos de eform do fluxo de contrato, assim, fica mais maleável.
 * @author danilo.silva
 *
 */
public class CentroCusto {
	
	private static String FIELDS_E044CCU = "( CodEmp,CodCcu,DesCcu,AbrCcu,CodUsu,TipCcu,IndAgr,CcuPai,AgrTax,MskCcu,ClaCcu,GruCcu,NivCcu,PosCcu,AnaSin,AceRat,CriRat,CtaRed,CtaRcr,CtaFdv,CtaFcr,ClaDes,CodLoc,LocRea, "+
			   							   "  PlaSeg,TaxIcu,CodTur,USU_CliNom,USU_LotNom,USU_EmpNom,USU_NumCtr,USU_GerNom,indexp,usu_codreg,datalt,horalt,usualt) ";

	private static String FIELDS_E043PCM = "( CodMpc,CtaRed,MskGcc,DefGru,ClaCta,DesCta,AbrCta,AnaSin,NatCta,NivCta,ExiRat,ForRat,ModCtb,CtaCtb,CodCcu, "+
					   					   "  TipCcu,ExiAux,MetCon,datalt,horalt,usualt,datger,horger,usuger,usu_codreg,valini,valfin ) ";
	
	
	/*
	 * cria o quarto nível de centro de custo, nivel do cliente
	 */
	public static String cadastraClienteCentroCusto(String codEmp, String codFil, String codCli, String nomCli ) throws Exception
	{
		
		String codEmpCC = "";
		if(codEmp.equals("15"))
			codEmpCC = "16" + codEmp;
		else
			codEmpCC = "15" + codEmp;
		
		/*
		 * codcli é encontrado usando a query
		 * SELECT max(ClaCcu) from E044ccu where codemp = '18' and ClaCcu like '1518%' and nivccu = 4 
		 * Resultado = 151828153
		 * remover os 4 primeiros digitos, e o restante adicionar +1
		 * Ex: 151828153 = 1518 28153 = 28153 + 1 = 28154
		 */
		String codigo = "";
		
		Long nCtaRed = 0L;
		String aClaCta = "";
		String aCtaRed = "";
		
		QLGroupFilter gp = new QLGroupFilter("AND");
		
		gp.addFilter(new QLRawFilter("codemp = "+codEmp+" and nivccu = 4 and claccu like '"+codEmpCC+"%'"));
		Collection<NeoObject> centroCustoEmpresa = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE044CCU"), gp, 0,1, "claccu DESC");
		Long maxclaccu = 0L;
		for(NeoObject cc : centroCustoEmpresa)
		{
			EntityWrapper w = new EntityWrapper(cc);
			Long claccu = NeoUtils.safeLong(NeoUtils.safeOutputString(w.findValue("claccu")));
			if(claccu > maxclaccu)
				maxclaccu = claccu;
		}
		
		{
			String valor = NeoUtils.safeOutputString(maxclaccu);
			valor = valor.substring(4, valor.length());
			long valorL = NeoUtils.safeLong(valor) + 1;
			codigo = NeoUtils.safeOutputString(valorL);
			while(codigo.length() < 5)
				codigo = "0" + codigo;
			
			codigo = codEmpCC + codigo;
		}
		
		gp.clearFilterList();
		gp.addFilter(new QLEqualsFilter("clacta", codigo));
		gp.addFilter(new QLEqualsFilter("codmpc", 203));
		Collection<NeoObject> listaPlanoContas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE043PCM"), gp);
		if(listaPlanoContas.size() > 0)
		{
			/*
			 * esse metodo só será chamado para criar um cc novo para um cliente novo
			 * logo, essa consulta sempre será vazia
			 */
			for(NeoObject obj : listaPlanoContas)
			{
				EntityWrapper w = new EntityWrapper(obj);
				nCtaRed = NeoUtils.safeLong(NeoUtils.safeOutputString(w.findValue("ctared")));
				aClaCta = codEmpCC + codigo;
				aCtaRed = NeoUtils.safeOutputString(nCtaRed);
			}
		}else
		{
			nCtaRed = getProximaChaveE043PCM();
			aCtaRed = NeoUtils.safeOutputString(nCtaRed);
			aClaCta = codigo;
			
		}
		
		String aNomCli = NeoUtils.safeOutputString(nomCli);
		
		String aDesCta = aNomCli.substring(0, (aNomCli.length()>80?80:aNomCli.length()));
		String aAbrCta = aNomCli.substring(0, (aNomCli.length()>20?20:aNomCli.length()));
		String dDatHoj = new SimpleDateFormat("yyyy-MM-dd").format(new GregorianCalendar().getTime());
		String aCtaPai = CadastraCentroCusto.getCentroCusto(codEmpCC, 3, codEmp);

		String query = " INSERT INTO E043PCM "+FIELDS_E043PCM+" VALUES (203, :chave, '1.2.33.44444.555.66.7777.88', 'X', '"+aClaCta+"', '"+aDesCta+"', '" +
				aAbrCta+"', 'S', 'D', 4, 'N', 0, 0, 0, ':chave', 3, 'N', 0, '1900-12-31', 0, 0, '"+dDatHoj+"', 0, 1, NULL, '1900-12-31', " +
				"'1900-12-31') ";
		
		aCtaRed = NeoUtils.safeOutputString(OrsegupsContratoUtils.safeInsert("SAPIENS", "SELECT MAX(ctared)+10 FROM E043PCM WHERE codmpc = 203", query, 10, 5000));
		if ("0".equals(aCtaRed)){
			throw new Exception("Não foi possível cadastrar o centro de custo no Modelo de Plano. Tente Novamente.");
		}
		
		Collection<String> empresas = getEmpresasSapiens();
		
		for(String empresa : empresas)
		{
			String query2 = "INSERT INTO E044CCU "+FIELDS_E044CCU+" VALUES ("+empresa+", "+aCtaRed+", '"+aDesCta+"', '"+aAbrCta+"', 0, 3, '', '"+aCtaPai+"', '', " +
					"'1.2.33.44444.555.66.7777.88', '"+aClaCta+"', 1, 4, 5, 'S', 'N', 5, 0, 0, 0, 0, '', '', '', '', 0, 0, " +
					"NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL) ";
			OrsegupsContratoUtils.safeInsert("SAPIENS", Long.parseLong(aCtaRed), query2);
		}
		
		return aCtaRed;
		
//		wForm.setValue("aClaCta", aClaCta);
//		
//		//aCtaRed é o código do centro de custo criado, logo o código do pai para o próximo centro de custo
//		gp.clearFilterList();
//		
//		String numeroContrato = NeoUtils.safeOutputString(wForm.findValue("dadosGeraisContrato.numeroContratoSapiens"));
//		
//		NeoObject contrato = (NeoObject)wForm.findValue("numContrato");
//		if(contrato != null)
//		{
//			EntityWrapper wContrato = new EntityWrapper(contrato);
//			numeroContrato = NeoUtils.safeOutputString(wContrato.findValue("usu_numctr"));
//		}
//		
		
//		/*
//		 * validar o cadastro do quinto nível como sendo o agrupador
//		 * validar se foi escolhido um agrupador, senão criar um novo
//		 */
//		
//		if(wForm.findValue("codigoAgrupador") != null)
//		{
//			/*
//			 * Esse campo está em desuso
//			 * 
//			 * NeoObject agrupador = (NeoObject) wForm.findValue("codigoAgrupador");
//			EntityWrapper wAgr = new EntityWrapper(agrupador);
//			String numeroContratoAgr = NeoUtils.safeOutputString(wAgr.findValue("usu_ctragr")).replace("AGR", "");
//			String classificacaoQuintoNivel = CadastraCentroCusto.getClassificacaoCentroCusto(codEmp, codCli, 5, numeroContratoAgr);
//			if(classificacaoQuintoNivel.equals(""))
//				classificacaoQuintoNivel = NeoUtils.safeOutputString(wForm.findValue("aClaCta"));
//			String centroCustoQuintoNivel = CadastraCentroCusto.getCentroCusto(classificacaoQuintoNivel, 5, codEmp);
//			//String aClaCta = NeoUtils.safeOutputString(processEntity.findValue("aClaCta"));
//			//cadastraSubContratoCentroCusto(form, centroCustoQuintoNivel, aClaCta, false);*/
//		}else
//		{
//			String codigoAgr = "AGR" + numeroContrato;
//			CadastraCentroCustoClienteContrato.insereRegistroAgrupador(codEmp, codFil, codCli, codigoAgr);
//			
//			atualizaContratoCodigoAgrupador(codEmp, codFil, numeroContrato, codigoAgr);
//			
//			cadastraContratoCentroCusto(form, aCtaRed, codigoAgr);
//			
//		}
	}
	
	
	/*
	 * cria o quinto nível do centro de custo, nível do agrupador
	 */
	public static String cadastraContratoCentroCusto(String codEmp, String codFil, String codCli, String centroCustoPai, String claCtaPai, String numeroContrato) throws Exception
	{
		
		if(numeroContrato.contains("AGR"))	numeroContrato = numeroContrato.replace("AGR", "");
		
		String aDesCta = "AGRUPADOR - " + numeroContrato.substring(0, (numeroContrato.length()>20?20:numeroContrato.length()));;
		String aAbrCta = aDesCta.substring(0, (aDesCta.length()>20?20:aDesCta.length()));
		
		Long nCtaRed = 0L;
		String aClaCta = "";
		String aCtaRed = "";
		
		QLGroupFilter gp = new QLGroupFilter("AND");
		gp.addFilter(new QLEqualsFilter("codmpc", 203));
		gp.addFilter(new QLEqualsFilter("descta", aDesCta));
		Collection<NeoObject> listaPlanoContas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE043PCM"), gp);
		if(listaPlanoContas.size() > 0)
		{
			for(NeoObject obj : listaPlanoContas)
			{
				EntityWrapper w = new EntityWrapper(obj);
				nCtaRed = NeoUtils.safeLong(NeoUtils.safeOutputString(w.findValue("ctared")));
				aCtaRed = NeoUtils.safeOutputString(w.findValue("ctared"));
				aClaCta = NeoUtils.safeOutputString(w.findValue("clacta"));
			}
		}else
		{
			nCtaRed = getProximaChaveE043PCM();
			aCtaRed = NeoUtils.safeOutputString(nCtaRed);
			
			aClaCta = claCtaPai;
			if(aClaCta.equals(""))
			{
				System.out.println("[FLUXO CONTRATOS]-Integracao contrato - cadastraContratoCentroCusto() codEmp: "+ codEmp+ ",codCli: "+ codCli+", nivcc: "+ 4 +", numCtr: "+numeroContrato);
				aClaCta = CadastraCentroCusto.getClassificacaoCentroCusto(codEmp, codCli, 4, numeroContrato); 
				if(aClaCta.equals(""))
				{
					aClaCta = CadastraCentroCusto.getClassificacaoCentroCusto(codEmp, codCli, 4, "");
					//String centroCustoQuartoNivel = CadastraCentroCusto.getCentroCusto(classificacaoQuartoNivel, 4, codEmp);
					//CadastraCentroCusto cadCC = new CadastraCentroCusto();
					//cadCC.cadastraClienteCentroCusto(form);
					//return;
				}
			}
		}
		
		String aCliCcu = aClaCta + "%";
		gp.clearFilterList();
		gp.addFilter(new QLEqualsFilter("codmpc", 203));
		gp.addFilter(new QLEqualsFilter("nivcta", 5));
		gp.addFilter(new QLRawFilter("clacta like '" + aCliCcu +"'"));
		
		listaPlanoContas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE043PCM"), gp);
		int qtd = listaPlanoContas.size() + 1;
		String nQtdCtr = NeoUtils.safeOutputString(qtd);
		while(nQtdCtr.length() < 3)
			nQtdCtr = "0" + nQtdCtr;
		
		String aCtaPai = centroCustoPai;
		aClaCta = aClaCta + nQtdCtr;
		String dDatHoj = new SimpleDateFormat("yyyy-MM-dd").format(new GregorianCalendar().getTime());
		
		String query = " INSERT INTO E043PCM "+FIELDS_E043PCM+" VALUES (203, :chave, '1.2.33.44444.555.66.7777.88', 'X', '"+aClaCta+"', '"+aDesCta+"', '" +
				aAbrCta+"', 'S', 'D', 5, 'N', 0, 0, 0, ':chave', 3, 'N', 0, '1900-12-31', 0, 0, '"+dDatHoj+"', 0, 1, NULL, '1900-12-31', " +
				"'1900-12-31') ";
		aCtaRed = NeoUtils.safeOutputString(OrsegupsContratoUtils.safeInsert("SAPIENS", "SELECT MAX(ctared)+10 FROM E043PCM WHERE codmpc = 203", query, 10, 5000));
		if ("0".equals(aCtaRed)){
			throw new Exception("Não foi possível cadastrar o centro de custo no Modelo de Plano. Tente Novamente.");
		}
		
		Collection<String> empresas = getEmpresasSapiens();
		
		for(String empresa : empresas)
		{
			String query2 = "INSERT INTO E044CCU "+FIELDS_E044CCU+" VALUES ("+empresa+", "+aCtaRed+", '"+aDesCta+"', '"+aAbrCta+"', 0, 3, '', '"+aCtaPai+"', '', " +
					"'1.2.33.44444.555.66.7777.88', '"+aClaCta+"', 1, 5, 3, 'S', 'N', 5, 0, 0, 0, 0, '', '', '', '', 0, 0, " +
					"NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL) ";
			OrsegupsContratoUtils.safeInsert("SAPIENS", Long.parseLong(aCtaRed), query2);
		}
		
		return aCtaRed;
		
//		wForm.setValue("aClaCta", aClaCta);
//		wForm.setValue("aCtaRed", aCtaRed);
//		wForm.setValue("centroCustoContrato", aCtaRed);
//		
//		cadastraSubContratoCentroCusto(form, aCtaRed, aClaCta, false);
	}
	
	
	/*
	 * cadastra o sexto nível do centro de custo, nível do contrato
	 */
	public static String cadastraSubContratoCentroCusto(String codEmp, String codFil, String codCli, String centroCustoPai, String aClaCtaPai, String numeroContrato, boolean cadastraTodaArvore) throws Exception
	{
		
//		String numeroContrato = NeoUtils.safeOutputString(wForm.findValue("numContrato.usu_numctr"));
//		if(numeroContrato.equals(""))
//			numeroContrato = NeoUtils.safeOutputString(wForm.findValue("dadosGeraisContrato.numeroContratoSapiens"));
		//6 nivel - buscaNomeCliente.codcli
		String codccuPai = NeoUtils.safeOutputString(centroCustoPai);
		Long nCtaRed = 0L;
		String aCtaRed = "";
		String aClaCta = "";
		
		String aDesCta = "CONTRATO - " + numeroContrato;//+ codccuPai.substring(0, (codccuPai.length()>80?80:codccuPai.length()));;
		String aAbrCta = aDesCta.substring(0, (aDesCta.length()>20?20:aDesCta.length()));
		
		if(aClaCta == null || aClaCta.equals(""))
		{
			//aClaCta = CadastraCentroCusto.getClassificacaoCentroCusto(codEmp, codCli, 5, numeroContrato);
			aClaCta = CentroCusto.getClassificacaoPCM(codEmp, centroCustoPai);
		}
		
		aClaCta = aClaCtaPai + "01";
		
		nCtaRed = getProximaChaveE043PCM();
		aCtaRed = NeoUtils.safeOutputString(nCtaRed);	
		
		String dDatHoj = new SimpleDateFormat("yyyy-MM-dd").format(new GregorianCalendar().getTime());
		String aCtaPai = codccuPai;		
		
		String query = " INSERT INTO E043PCM "+FIELDS_E043PCM+" VALUES (203, :chave, '1.2.33.44444.555.66.7777.88', 'X', '"+aClaCta+"', '"+aDesCta+"', '" +
				aAbrCta+"', 'S', 'D', 6, 'N', 0, 0, 0, ':chave', 3, 'N', 0, '1900-12-31', 0, 0, '"+dDatHoj+"', 0, 1, NULL, '1900-12-31', " +
				"'1900-12-31') ";
		aCtaRed = NeoUtils.safeOutputString(OrsegupsContratoUtils.safeInsert("SAPIENS", "SELECT MAX(ctared)+10 FROM E043PCM WHERE codmpc = 203", query, 10, 5000));
		if ("0".equals(aCtaRed)){
			throw new Exception("Não foi possível cadastrar o centro de custo no Modelo de Plano. Tente Novamente.");
		}
		
		Collection<String> empresas = getEmpresasSapiens();
		
		for(String empresa : empresas)
		{
			
			String query2 = "INSERT INTO E044CCU "+FIELDS_E044CCU+" VALUES ("+empresa+", "+aCtaRed+", '"+aDesCta+"', '"+aAbrCta+"', 0, 3, '', '"+aCtaPai+"', '', " +
					"'1.2.33.44444.555.66.7777.88', '"+aClaCta+"', 1, 6, 2, 'S', 'N', 5, 0, 0, 0, 0, '', '', '', '', 0, 0, " +
					"NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL) ";
			OrsegupsContratoUtils.safeInsert("SAPIENS", Long.parseLong(aCtaRed), query2);
			
		}
		
		return aCtaRed;
		
		//wForm.setValue("aClaCta6", aClaCta);
		//wForm.setValue("aCtaRed", aCtaRed);
		//wForm.setValue("centroCusto6", aCtaRed);
		
//		if(cadastraTodaArvore)
//			cadastraLotacaoCentroCusto(form, aCtaRed, aClaCta, cadastraTodaArvore);
	}
	
	
	/*
	 * cadastra o sétimo nível do centro de custo, nível da lotação
	 */
	public static String cadastraLotacaoCentroCusto(String codEmp, String codFil, String codCli, String centroCustoPai, String aClaCtaPai, String numeroContrato, String nomCli) throws Exception 
	{
		
		String retorno = null;
		Long numCtr = new Long(numeroContrato);
								
		String codccuPai = NeoUtils.safeOutputString(centroCustoPai);
		String aNomCli = NeoUtils.safeOutputString(nomCli);
		
		String aDesCta = aNomCli.substring(0, (aNomCli.length()>80?80:aNomCli.length()));
		String aAbrCta = aNomCli.substring(0, (aNomCli.length()>20?20:aNomCli.length()));
		Long nCtaRed = 0L;
		String aCtaRed = "";
		String aClaCta = "";
		
		aClaCta = aClaCtaPai + "0001"; 
		// Busca Informações na E043PCM - Plano de Contas
		QLGroupFilter filter = new QLGroupFilter("AND");
		filter.addFilter(new QLEqualsFilter("codmpc", 203));
		filter.addFilter(new QLEqualsFilter("nivcta", 7));
		filter.addFilter(new QLRawFilter("clacta like " + aClaCta));
		List<NeoObject> noListaPlanoConta = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE043PCM"), filter);
		if(noListaPlanoConta.size() > 0)
		{
			for(NeoObject noPlanoConta:noListaPlanoConta)
			{
				EntityWrapper ewPlanoConta = new EntityWrapper(noPlanoConta);
				nCtaRed = NeoUtils.safeLong(NeoUtils.safeOutputString(ewPlanoConta.findValue("ctared")));
				aClaCta = NeoUtils.safeOutputString(ewPlanoConta.findValue("clacta"));
				aCtaRed = NeoUtils.safeOutputString(nCtaRed);
			}
		}else
		{
			nCtaRed = getProximaChaveE043PCM();
			aCtaRed = NeoUtils.safeOutputString(nCtaRed);
		}
		
		String aCtaPai = codccuPai;	
		String data = new SimpleDateFormat("yyyy-MM-dd").format(new GregorianCalendar().getTime());
		
		String query = " INSERT INTO E043PCM "+FIELDS_E043PCM+" VALUES (203, :chave, '1.2.33.44444.555.66.7777.88', 'X', '"+aClaCta+"', '"+aDesCta+"', '" +
				aAbrCta+"', 'S', 'D', 7, 'N', 0, 0, 0, ':chave', 3, 'N', 0, '1900-12-31', 0, 0, '"+data+"', 0, 1, NULL, '1900-12-31', " +
				"'1900-12-31') ";
		
		aCtaRed = NeoUtils.safeOutputString(OrsegupsContratoUtils.safeInsert("SAPIENS", "SELECT MAX(ctared)+10 FROM E043PCM WHERE codmpc = 203", query, 10, 5000));
		retorno = aCtaRed;
		if ("0".equals(aCtaRed)){
			throw new Exception("Não foi possível cadastrar o centro de custo no Modelo de Plano. Tente Novamente.");
		}
		
		Collection<String> empresas = getEmpresasSapiens();
		
		for(String empresa : empresas)
		{
			
			String query2 = "INSERT INTO E044CCU "+FIELDS_E044CCU+" VALUES ("+empresa+", "+aCtaRed+", '"+aDesCta+"', '"+aAbrCta+"', 0, 3, '', '"+aCtaPai+"', '', " +
					"'1.2.33.44444.555.66.7777.88', '"+aClaCta+"', 1, 7, 4, 'S', 'N', 5, 0, 0, 0, 0, '', '', '', '', 0, 0, " +
					"NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL) ";
			OrsegupsContratoUtils.safeInsert("SAPIENS", Long.parseLong(aCtaRed), query2);
			
		}
		
		ContratoUtils.addCCToRollBack(Long.parseLong(numeroContrato), Long.parseLong(aCtaRed));
		
		
		String codccu8 = cadastraPostoCentroCusto(codEmp, codFil, codCli, aCtaRed, aClaCta, numeroContrato, nomCli);
		if (codccu8!= null){
			retorno = codccu8;
			ContratoUtils.addCCToRollBack(Long.parseLong(numeroContrato), Long.parseLong(codccu8));
		}
		return retorno;
		
	}
	
	
	/*
	 * cadastra o último nível do centro de custo, o nível do posto
	 */
	private static String cadastraPostoCentroCusto(String codEmp, String codFil, String codCli, String centroCustoPai, String aClaCtaPai, String numeroContrato, String nomCli) throws Exception
	{
		String retorno = null;
		String codccuPai = NeoUtils.safeOutputString(centroCustoPai);
		String aNomCli = NeoUtils.safeOutputString(nomCli);
		String aClaCta = "";
		
		// Início do processo para inserção 		
		QLGroupFilter filter = new QLGroupFilter("AND");
		filter.addFilter(new QLEqualsFilter("nivcta", 8));
		filter.addFilter(new QLEqualsFilter("codmpc", 203));
		filter.addFilter(new QLRawFilter("clacta LIKE '" + aClaCtaPai  +"%'"));
		
		List<NeoObject> noListaPlanoConta = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE043PCM"), filter);
		Long nCtaRed = null;
		String aCtaRed = "";
		int qtd = noListaPlanoConta != null ?  noListaPlanoConta.size() : 0;
		String aPosCcu = qtd < 10 ? ("0"+NeoUtils.safeOutputString(noListaPlanoConta.size()==0 ? 1:noListaPlanoConta.size())) : NeoUtils.safeOutputString( noListaPlanoConta.size()==0 ? 1:noListaPlanoConta.size() );
		aClaCta = aClaCtaPai + aPosCcu;
		
		/*filter.clearFilterList();
		filter.addFilter(new QLEqualsFilter("codmpc", 203));
		//filter.addFilter(new QLRawFilter("ctared in (SELECT MAX(ctared) FROM E043PCM WHERE codmpc = 203)"));
		noListaPlanoConta = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE043PCM"), filter, 0, 1, "ctared DESC");
		for(NeoObject no : noListaPlanoConta)
		{
			EntityWrapper wNo = new EntityWrapper(no);
			nCtaRed = NeoUtils.safeLong(NeoUtils.safeOutputString(wNo.findValue("ctared"))) + 10;
			aCtaRed = NeoUtils.safeOutputString(nCtaRed);
			retorno = aCtaRed;
		}*/
		
		nCtaRed = getProximaChaveE043PCM();
		aCtaRed = NeoUtils.safeOutputString(nCtaRed);
		
		String aDesCta = "P" + aPosCcu + " - " + aNomCli.substring(0, (aNomCli.length()>80?80:aNomCli.length()));
		String aAbrCta = aDesCta.substring(0, (aDesCta.length()>20?20:aDesCta.length()));
		String aCtaPai = codccuPai;		
		String data = new SimpleDateFormat("yyyy-MM-dd").format(new GregorianCalendar().getTime());
		
		String query = " INSERT INTO E043PCM "+FIELDS_E043PCM+" VALUES (203, :chave, '1.2.33.44444.555.66.7777.88', 'X', '"+aClaCta+"', '"+aDesCta+"', '" +
				aAbrCta+"', 'A', 'D', 8, 'N', 0, 0, 0, ':chave', 3, 'N', 0, '1900-12-31', 0, 0, '"+data+"', 0, 1, NULL, '1900-12-31', " +
				"'1900-12-31') ";
		
		aCtaRed = NeoUtils.safeOutputString(OrsegupsContratoUtils.safeInsert("SAPIENS", "SELECT MAX(ctared)+10 FROM E043PCM WHERE codmpc = 203", query, 10, 5000));
		retorno = aCtaRed;
		if ("0".equals(aCtaRed)){
			throw new Exception("Não foi possível cadastrar o centro de custo no Modelo de Plano. Tente Novamente.");
		}
		
		Collection<String> empresas = getEmpresasSapiens();
		
		for(String empresa : empresas)
		{
			
			String query2 = "INSERT INTO E044CCU "+FIELDS_E044CCU+" VALUES ("+empresa+", "+aCtaRed+", '"+aDesCta+"', '"+aAbrCta+"', 0, 3, '', '"+aCtaPai+"', '', " +
					"'1.2.33.44444.555.66.7777.88', '"+aClaCta+"', 1, 8, 2, 'A', 'S', 5, 0, 0, 0, 0, '', '', '', '', 0, 0, " +
					"NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL) ";
			
			
			String.valueOf(OrsegupsContratoUtils.safeInsert("SAPIENS", Long.parseLong(aCtaRed), query2));
		}
		
		return retorno;
	}
	
	
	
	
	
	
	/**
	 * 
	 * @param eformExterno - eform que será consultado para gerar 
	 * @return
	 */
	private static long getProximaChaveE043PCM()
	{
		long chave = 0;
		QLGroupFilter filter =  new QLGroupFilter("AND");
		filter.addFilter(new QLEqualsFilter("codmpc", 203));
		//filter.addFilter(new QLRawFilter("ctared in (SELECT MAX(ctared) FROM E043PCM WHERE codmpc = 203) "));
		List<NeoObject> noListaPlanoConta = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE043PCM"), filter, 0, 1, "ctared DESC");
		for(NeoObject noPlanoConta:noListaPlanoConta)
		{
			EntityWrapper ewPlanoConta = new EntityWrapper(noPlanoConta);
			chave = NeoUtils.safeLong(NeoUtils.safeOutputString(ewPlanoConta.findValue("ctared"))) + 10;
			//aCtaRed = NeoUtils.safeOutputString(nCtaRed);
		}
		
		return chave;
	}
	
	public static Collection<String> getEmpresasSapiens()
	{
		List<NeoObject> empresas = (List<NeoObject>)PersistEngine.getObjects(AdapterUtils.getEntityClass("SAPIENSE070FIL"));
		Collection<String> codigos = new ArrayList<String>();
		for(NeoObject empresa : empresas)
		{
			EntityWrapper w = new EntityWrapper(empresa);
			String codigo = NeoUtils.safeOutputString(w.findValue("codemp"));
			if(!codigos.contains(codigo))
				codigos.add(codigo);
		}
		
		return codigos;
	}
	
	
	public static int insereRegistroAgrupador(String codemp, String codfil, String codcli, String codagr)
	{
		int retorno = -1;
		
		//UPDATE E044CCU
		String update = "INSERT INTO USU_T160AGR (USU_CTRAGR, USU_CODEMP, USU_CODFIL, USU_CODCLI) VALUES ('" + codagr + "'," + codemp + "," + codfil + "," + codcli + ")";
		StringBuilder sql = new StringBuilder();
		sql.append(update);
		Query queryExecute = PersistEngine.getEntityManager("SAPIENS").createNativeQuery(sql.toString());
		try
		{
			retorno = queryExecute.executeUpdate();
		}catch(Exception e)
		{
			retorno = -1;
			//se ja existe, n precisa tratar
			//throw new WorkflowException("Erro: já existe agrupador " + codagr );
		}finally{
			return retorno;
		}
		
	}
	
	/**
	 * Retorna a classificação pelo código do centro de custo 
	 * @param ctaRed
	 * @return
	 */
	public static String getClassificacaoPCM(String codEmp, String ctaRed){
		
		String query = "select claccu from e044ccu where codEmp = "+codEmp+" and  codccu = " + ctaRed;
		String retorno = null;
		ResultSet rs = OrsegupsContratoUtils.getResultSet(query, "SAPIENS");
		if (rs != null){
			try {
				retorno = rs.getString(1);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return retorno;
	}
	
	public static void main(String args[]) throws Exception{
		String codEmp = "18";
		String codFil = "1";
		String codCli = "2232";
		String numeroContrato = "31556";
		String nomCli = "testes";
		String aCtaRed = "";
		String aClaCta = "";
			
		// insere 4 nivel
		aCtaRed = CentroCusto.cadastraClienteCentroCusto(codEmp, codFil, codCli, nomCli);
		aClaCta = CentroCusto.getClassificacaoPCM(codEmp, aCtaRed);
		System.out.println("[FLUXO CONTRATOS]-ctaRed_4-> " + aCtaRed + ", claCta: " +aClaCta);
		
		// insere 5 nivel
		aCtaRed =  CentroCusto.cadastraContratoCentroCusto(codEmp, codFil, codCli, aCtaRed, aClaCta, numeroContrato);
		aClaCta = CentroCusto.getClassificacaoPCM(codEmp, aCtaRed);
		System.out.println("[FLUXO CONTRATOS]-ctaRed_5-> " + aCtaRed + ", claCta: " +aClaCta);
		
		// insere 6 nivel
		aCtaRed =  CentroCusto.cadastraSubContratoCentroCusto(codEmp, codFil, codCli, aCtaRed, aClaCta, numeroContrato, false);
		aClaCta = CentroCusto.getClassificacaoPCM(codEmp, aCtaRed);
		System.out.println("[FLUXO CONTRATOS]-ctaRed_6-> " + aCtaRed + ", claCta: " +aClaCta);
		
		//insere 7 e 8 nivel e retorna codigo do 8 nivel 
		aCtaRed = CentroCusto.cadastraLotacaoCentroCusto(codEmp, codFil, codCli, aCtaRed, aClaCta, numeroContrato, nomCli);
		aClaCta = CentroCusto.getClassificacaoPCM(codEmp, aCtaRed);
		
		
	}

}
