package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;


/**
 * 
 * Adapter responsável por gerar as parcelas da nota fiscal, o cálculo é gerado a partir do valor líquido com a quantidade de parcelas definidas na condição de pagamento, 
 * o vencimento é cálculado de três maneiras, sendo que a padrão é 30 dias após a data de emissão a primeira parcela e as demais 30 dias após a anterior, outra maneira seria
 * atráves do campo dia de vencimento que é informado o dia do mês que vencerá a parcela e por último o campo quantidade de dias de prazo para o próximo vencimento.
 * 
 * @author lucas.alison
 *
 */
public class GLNInsereParcelasNotaFiscalSapiens implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(GLNInsereParcelasNotaFiscalSapiens.class);

	private static String nomeFonteDados = "SAPIENS";

	private String urlstr = "";
	private String NOMUSU = "";
	private String SENUSU = "";
	private String PROXACAO = "";
	private String urlSID = "";

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
	    try{
		BigDecimal valorLiquido = (BigDecimal) processEntity.findValue("valorLiquido");
		Long qtdParcelas = (Long) processEntity.findValue("condicaoPagamento.qtdpar");
		Long nota = (Long) processEntity.findValue("notaFiscal");
		Long codigoempresa = (Long) processEntity.findValue("empresa.codemp");
		String serie = (String) processEntity.findValue("serieNotaFiscal.codsnf");
		Long diaVencimento = (Long) processEntity.findValue("dataVencimento");
		Long qtdDiasPrazo = (Long) processEntity.findValue("qtdDiasPrazo");
		String titulo = buscarSeparadorSerieNota(codigoempresa, serie);
		if (titulo == null){
		    titulo = "";
		}
		GregorianCalendar dataVenc = (GregorianCalendar) processEntity.findValue("dataVencPrimParcela");
		if (qtdParcelas != null){
		    BigDecimal vlParcela = valorLiquido.divide(new BigDecimal(qtdParcelas), 2, RoundingMode.HALF_UP);
		    int i = 1;
		    int periodo = 30;
		    if (qtdDiasPrazo != null){
			periodo = qtdDiasPrazo.intValue();
		    }
		    List<NeoObject> listaParcelas = new LinkedList<NeoObject>();
		    int mesReferencia = 1;
		    while (i <= qtdParcelas){
			GregorianCalendar dataVencClone2 = (GregorianCalendar) dataVenc.clone();
			InstantiableEntityInfo parcelas = AdapterUtils.getInstantiableEntityInfo("GLNParcelas");
			NeoObject objParcelas = parcelas.createNewInstance();
			EntityWrapper wrapperErrosVerificador = new EntityWrapper(objParcelas);
			if (i > 1){
			    if (diaVencimento != null){
				dataVencClone2.set(Calendar.DAY_OF_MONTH, diaVencimento.intValue());
				dataVencClone2.add(Calendar.MONTH, mesReferencia);
			    }else{
				dataVencClone2.add(Calendar.DAY_OF_MONTH, periodo);
			    }
			    mesReferencia++;
			}
			while(!OrsegupsUtils.isWorkDay(dataVencClone2)){
			    dataVencClone2.add(Calendar.DAY_OF_MONTH, 1);
			}
			wrapperErrosVerificador.findField("titulo").setValue(nota+titulo+i);
			wrapperErrosVerificador.findField("vencimentoParcela").setValue(dataVencClone2);
			wrapperErrosVerificador.findField("valorParcela").setValue(vlParcela);
			
			String observacaoParcela = "";
			
			List<NeoObject> listaItensDeProduto = (List<NeoObject>) processEntity.findValue("listaItensDeProduto");			
			if (listaItensDeProduto != null && !listaItensDeProduto.isEmpty()){
			    for (NeoObject itensDeProduto : listaItensDeProduto){
				EntityWrapper wrpitensDeProduto = new EntityWrapper(itensDeProduto);
				String descricaoProduto = (String) wrpitensDeProduto.findValue("observacaoDoProduto");
				if (descricaoProduto != null){
				    observacaoParcela += descricaoProduto+" - ";
				}
			    }
			}
			List<NeoObject> listaItensDeServico = (List<NeoObject>) processEntity.findValue("listaItensDeServico");
			if (listaItensDeServico != null && !listaItensDeServico.isEmpty()){
			    for (NeoObject itensDeServico : listaItensDeServico){
				EntityWrapper wrpitensDeServico = new EntityWrapper(itensDeServico);
				String descricaoServico = (String) wrpitensDeServico.findValue("observacaoDoServico");
				if (descricaoServico != null){
				    observacaoParcela += descricaoServico+" - ";
				}
			    }
			}
			
			
			wrapperErrosVerificador.findField("observacaoParcela").setValue(observacaoParcela+" parcela "+i+"/"+qtdParcelas);
			
			listaParcelas.add(objParcelas);
			PersistEngine.persist(objParcelas);
			if (qtdDiasPrazo != null){
			    periodo += qtdDiasPrazo.intValue();
			}else{
			    periodo += 30;
			}
			i++;
		    }
		    processEntity.setValue("listaParcelas", listaParcelas);
		}
		
	    }catch (Exception e){
		e.printStackTrace();
		System.out.println(e);
		throw new WorkflowException("Erro inesperado. Por favor, contate o Departamento de TI!");
	    }
	}
	
	/**
	 * Método para buscar o separador da série da cadastrado no Sapiens
	 * @param codEmp - codigo da empresa da nota fiscal
	 * @param codSnf - codigo da serie da nota fiscal
	 */
	public String buscarSeparadorSerieNota(Long codEmp, String codSnf){
	    Connection conn = null;
	    PreparedStatement stmt = null;
	    ResultSet rs = null;
	    String retorno = null;
	    try{
		conn = PersistEngine.getConnection("SAPIENS");
		Long timeExec = GregorianCalendar.getInstance().getTimeInMillis();
		
		StringBuffer queryJobs = new StringBuffer();
		    
		queryJobs.append(" select top 1 DesPar from E020PAR where CodEmp = ? and CodSnf = ? ");
		
		stmt = conn.prepareStatement(queryJobs.toString());
		stmt.setLong(1, codEmp);
		stmt.setString(2, codSnf);
		rs = stmt.executeQuery();

		log.warn("SQL - buscarSeparadorSerieNota - Time spent: " + (GregorianCalendar.getInstance().getTimeInMillis() - timeExec) + "ms");

		while (rs.next()){
		    String desPar = rs.getString("DesPar");
		    if (desPar != null){
			retorno = desPar.substring(0,1);
		    }
		}
		    
	    }catch (Exception e){
		e.printStackTrace();
	    }finally{
		OrsegupsUtils.closeConnection(conn, stmt, rs);
	    }

	    return retorno;
	}
	
	
	@Override
	public void back(EntityWrapper processEntity, Activity activity){
	}
}
