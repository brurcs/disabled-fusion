package com.neomind.fusion.custom.orsegups.converter;

import java.util.GregorianCalendar;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.util.NeoUtils;

public class QLImprimeComunicadoConverter extends StringConverter
{

	@Override
	protected String getHTMLInput(EFormField field, OriginEnum origin)
	{
		
		Long idPai = field.getForm().getObjectId();
		NeoObject ficha = PersistEngine.getNeoObject(idPai);
		EntityWrapper wrapper = new EntityWrapper(ficha);
		
		NeoObject colaborador = (NeoObject) wrapper.findValue("colaborador");
		EntityWrapper colaboradorWrapper = new EntityWrapper(colaborador);
		
		Long numEmp = (Long) colaboradorWrapper.findValue("numemp");
		Long tipCol = (Long) colaboradorWrapper.findValue("tipcol");
		Long numCad = (Long) colaboradorWrapper.findValue("numcad");
		GregorianCalendar data = (GregorianCalendar) wrapper.getValue("data");
		GregorianCalendar dataZero = new GregorianCalendar( data.get(GregorianCalendar.YEAR), data.get(GregorianCalendar.MONTH), data.get(GregorianCalendar.DAY_OF_MONTH));
		
		String emp = numEmp.toString();
		String tip = tipCol.toString();
		String cad = numCad.toString();
		
	
		String dat = NeoUtils.safeDateFormat(dataZero, "dd/MM/yyyy");
		
		StringBuilder button = new StringBuilder();
		
		button.append(" <div align='left'> ");
		button.append("		<a target '_blank' href='servlet/com.neomind.fusion.custom.orsegups.servlets.RelatoriosServlet?action=coberturaFerias&numemp=" + emp + "&tipcol=" + tip + "&numcad=" + cad + "&datalt=" + dat + "' value='Imprimir Comunicado de Férias' title='Imprimir Comunicado de Férias'><img src='imagens/custom/report_pdf_16x16.png' align='middle' title='Imprimir relatório de cobertura' /></a>");
		button.append(" </div>");
		
		return button.toString();
	}

	@Override
	protected String getHTMLView(EFormField field, OriginEnum origin)
	{
		return "";
	}
}