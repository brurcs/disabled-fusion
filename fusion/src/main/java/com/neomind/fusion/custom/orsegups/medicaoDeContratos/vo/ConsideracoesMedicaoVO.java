package com.neomind.fusion.custom.orsegups.medicaoDeContratos.vo;

public class ConsideracoesMedicaoVO
{
	public String descricao;
	public boolean isFalta;
	public String isfaltaString;
	public long neoId;
	public String linkExcluir;
	public String linkEdirar;

	public final String getLinkEdirar()
	{
		return linkEdirar;
	}

	public final void setLinkEdirar(String linkEdirar)
	{
		this.linkEdirar = linkEdirar;
	}

	public final String getLinkExcluir()
	{
		return linkExcluir;
	}

	public final void setLinkExcluir(String linkExcluir)
	{
		this.linkExcluir = linkExcluir;
	}

	public String getIsfaltaString()
	{
		return isfaltaString;
	}

	public void setIsfaltaString(String isfaltaString)
	{
		this.isfaltaString = isfaltaString;
	}

	public String getDescricao()
	{
		return descricao;
	}

	public void setDescricao(String descricao)
	{
		this.descricao = descricao;
	}

	public boolean isFalta()
	{
		return isFalta;
	}

	public void setFalta(boolean isFalta)
	{
		this.isFalta = isFalta;
	}

	public long getNeoId()
	{
		return neoId;
	}

	public void setNeoId(long neoId)
	{
		this.neoId = neoId;
	}

}
