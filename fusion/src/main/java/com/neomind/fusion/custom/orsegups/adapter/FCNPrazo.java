package com.neomind.fusion.custom.orsegups.adapter;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class FCNPrazo implements AdapterInterface
{

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try
		{
			GregorianCalendar prazo = new GregorianCalendar();
			GregorianCalendar prazoAtu = new GregorianCalendar();
			
			if (activity.getProcess().getModel().getName().equals("C023 - FCN - Cancelamento de Contrato Iniciativa eNEW"))
			{
				prazo = (GregorianCalendar) processEntity.findValue("dataFinalFaturamento");
				if(prazo != null)
				{					
					prazo.add(Calendar.DAY_OF_MONTH, 30);
					processEntity.setValue("dataInicioAcaoJudicial", OrsegupsUtils.getNextWorkDay(prazo));
				}
				else
				{
					prazoAtu.add(Calendar.DAY_OF_MONTH, 30);
					processEntity.setValue("dataInicioAcaoJudicial", OrsegupsUtils.getNextWorkDay(prazoAtu));
				}
			}
			if (activity.getProcess().getModel().getName().equals("C025 - FCN - Cancelamento de Contrato Inadimplência eNEW"))
			{
				prazo = (GregorianCalendar) processEntity.findValue("dataFinalFat");
				if(prazo != null)
				{
					prazo.add(Calendar.DAY_OF_MONTH, 30);
					processEntity.setValue("dataInicioAcaoJudicial", OrsegupsUtils.getNextWorkDay(prazo));
				}
				else
				{
					prazoAtu.add(Calendar.DAY_OF_MONTH, 30);
					processEntity.setValue("dataInicioAcaoJudicial", OrsegupsUtils.getNextWorkDay(prazoAtu));
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException("Não foi possível definir a data de Início da Atividade de Ação Judicial.");
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}

}
