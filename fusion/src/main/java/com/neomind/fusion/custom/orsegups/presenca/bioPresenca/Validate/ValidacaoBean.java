package com.neomind.fusion.custom.orsegups.presenca.bioPresenca.Validate;

import java.util.ArrayList;
import java.util.List;

import com.neomind.fusion.custom.orsegups.presenca.bioPresenca.DTO.BioPresencaDTO;
import com.neomind.fusion.custom.orsegups.presenca.bioPresenca.DTO.ErroDTO;
import com.neomind.fusion.custom.orsegups.presenca.bioPresenca.DTO.ErroDTOPai;

/**
 * 
 * @author lucas.alison
 *
 */

public class ValidacaoBean {
    
    public static List<ErroDTOPai> validar(BioPresencaDTO b){
	List<ErroDTOPai> retorno = new ArrayList<ErroDTOPai>();
	if (b.getAcao() == null){
	    ErroDTO e = new ErroDTO();
	    e.setCampo("acao");
	    e.setMsg("Não pode ser nulo");
	    retorno.add(e);
	}
	if (b.getData() == null){
	    ErroDTO e = new ErroDTO();
	    e.setCampo("data");
	    e.setMsg("Não pode ser nula");
	    retorno.add(e);
	}
	if (b.getEmpresa() == null){
	    ErroDTO e = new ErroDTO();
	    e.setCampo("empresa");
	    e.setMsg("Não pode ser nula");
	    retorno.add(e);
	}
	if (b.getMatricula() == null){
	    ErroDTO e = new ErroDTO();
	    e.setCampo("matricula");
	    e.setMsg("Não pode ser nula");
	    retorno.add(e);
	}
	if (b.getTelefone() == null){
	    ErroDTO e = new ErroDTO();
	    e.setCampo("telefone");
	    e.setMsg("Não pode ser nulo");
	    retorno.add(e);
	}
	if (b.getTimeZoneDispositivo() == null){
	    ErroDTO e = new ErroDTO();
	    e.setCampo("timezonedispositivo");
	    e.setMsg("Não pode ser nulo");
	    retorno.add(e);
	}
	return retorno;
    }
}
