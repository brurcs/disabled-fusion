package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoDateUtils;

public class RotinaAvancaQLP implements CustomJobAdapter{

	@Override
	public void execute(CustomJobContext ctx) {
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();

		try {
			conn = PersistEngine.getConnection("");

			GregorianCalendar gc = new GregorianCalendar();
			gc.set(GregorianCalendar.MONTH, -3);

			String dataCorte = NeoDateUtils.safeDateFormat(gc, "dd/MM/yyyy");

			sql.append(" SELECT TOP 50 WF.CODE, WF.STARTDATE, WF.NEOID, A.* FROM WFPROCESS WF ");
			sql.append(" INNER JOIN ACTIVITY A ON A.PROCESS_NEOID = WF.NEOID ");
			sql.append(" INNER JOIN ACTIVITYMODEL AM ON AM.NEOID = A.MODEL_NEOID ");
			sql.append(" INNER JOIN D_QLEXECUCAO D ON WF.ENTITY_NEOID = D.NEOID ");
			sql.append(" WHERE ");
			sql.append(" AM.NAME = 'DEFINIR OPERAÇÃO' AND ");
			sql.append(" A.FINISHDATE IS NULL AND ");
			sql.append(" WF.PROCESSSTATE = 0 AND ");
			sql.append(" NOT EXISTS (SELECT NEOID FROM TASK T WHERE T.ACTIVITY_NEOID = A.NEOID) ");
			sql.append(" AND WF.STARTDATE >= " + dataCorte );
			sql.append(" AND D.operacao_neoid IS NOT NULL");
			sql.append(" AND D.ISPROCESSOAUTOMATICO IS NOT NULL");

			pstm = conn.prepareStatement(sql.toString());

			rs = pstm.executeQuery();

			while(rs.next()){

				String code = rs.getString("code");
				Long neoIdWF = rs.getLong("neoid");
				System.out.println("NEOID: " + neoIdWF.toString());
				NeoUser solicitante = PersistEngine.getNeoObject(NeoUser.class, new QLEqualsFilter("code", "beatriz.malmann"));

				WFProcess processoQLP = PersistEngine.getNeoObject(WFProcess.class, neoIdWF);	

				try {
					new OrsegupsWorkflowHelper(processoQLP).avancaPrimeiraAtividadePresencaEngine(solicitante, true);
					System.out.println("[AVANCAR QLP AUTOMATICO] TAREFA: " + code + "AVANÇADA COM SUCESSO!");
				} catch (WorkflowException e) {					
					e.printStackTrace();
					System.out.println("[AVANCAR QLP AUTOMATICO] TAREFA:" + code + " NÃO PODE SER AVANÇADA. MOTIVO: " + (e.getErrorList().get(0).getI18nMessage()));					
				}
				catch (Exception e) {
					e.printStackTrace();
					System.out.println("[AVANCAR QLP AUTOMATICO] TAREFA:" + neoIdWF + "NÃO PODE SER AVANÇADA. MOTIVO: " + e.getMessage());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			OrsegupsUtils.closeConnection(conn, pstm, rs);
		}


	}

}
