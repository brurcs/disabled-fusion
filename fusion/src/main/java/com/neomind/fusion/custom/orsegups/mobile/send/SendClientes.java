package com.neomind.fusion.custom.orsegups.mobile.send;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.neomind.fusion.custom.orsegups.mobile.vo.ClienteVO;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.datawarehouse.NeoDataSource;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.util.NeoUtils;

@WebServlet(name="SendClientes", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.mobile.send.SendClientes"})
public class SendClientes extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	XmlBuilder xml = new XmlBuilder();

	public String clientes() throws SQLException {
		String json = null;
		NeoDataSource data = (NeoDataSource) PersistEngine.getObject( NeoDataSource.class, new QLEqualsFilter("name", "SAPIENS"));

		Connection con =  OrsegupsUtils.openConnection(data);

		String SQLCliente = " SELECT * FROM VIEW_LOC_INSP_MOBILE "; // ORDER";
		// BY EMPRESA, CLIENTE, LOTACAO ";
		// Prepara o statement
		Statement stm = null;

		ArrayList<ClienteVO> list = new ArrayList<ClienteVO>();
		try {
			stm = (Statement) con.createStatement(	ResultSet.TYPE_SCROLL_INSENSITIVE,	ResultSet.CONCUR_READ_ONLY);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		ResultSet rs = stm.executeQuery(SQLCliente);
		ArrayList<String> teste = new ArrayList<String>();

		int cont = 0;

		while (rs.next() /* && cont < 10 */) {
			cont++;
			ClienteVO cvo = new ClienteVO();
			try {

				cvo = new ClienteVO();
				cvo.setNome_cliente(NeoUtils.safeOutputString( rs.getString("CLIENTE") ).replace("'", "''") );
				cvo.setNome_Empresa( NeoUtils.safeOutputString( rs.getString("EMPRESA") ).replace("'", "''") );
				cvo.setNomeLotacao( NeoUtils.safeOutputString( rs.getString("LOTACAO") ).replace("'", "''")  );
				cvo.setNomePosto( NeoUtils.safeOutputString( rs.getString("POSTO") ).replace("'", "''"));
				cvo.setCodCcu(rs.getString("CODIGO"));
				list.add(cvo);

			} catch (Exception e) {
				System.out.println("[MOBILE] - Cliente com cadastro incompleto ->" + cvo);
				//e.printStackTrace();
			}

		}
		Gson gson = new Gson();
		json = gson.toJson(list);

		return json;

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("application/json");

		PrintWriter out = response.getWriter();

		try {
			System.out.println( "Enviando Clientes[com.neomind.fusion.custom.orsegups.mobile.send.SendClientes]: " + clientes());
			out.print(clientes());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();

		try {
			System.out.println( "Enviando Clientes[com.neomind.fusion.custom.orsegups.mobile.send.SendClientes]: " + clientes());
			out.print(clientes());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
