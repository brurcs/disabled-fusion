package com.neomind.fusion.custom.orsegups.fap.scheduler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.fap.dao.JobTecnicoTaticoDAO;
import com.neomind.fusion.custom.orsegups.fap.dto.FornecedorDTO;
import com.neomind.fusion.custom.orsegups.fap.exception.NaoEncontradoException;
import com.neomind.fusion.custom.orsegups.fap.factory.FapFactory;
import com.neomind.fusion.custom.orsegups.fap.utils.FapUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

/**
 * JOB que abre as FAP's para o tipo de aplicação técnico.
 * 
 * @author diogo.silva
 *
 */
public class RotinaAbreFapTecnico implements CustomJobAdapter
{
	/* private static final Log log = LogFactory.getLog(RotinaAbreFapTecnico.class); */
	@Override
	public void execute(CustomJobContext ctx)
	{

		NeoUser solicitante = null;
		NeoUser aprovador = null;
		Long codigoTecnico = null;
		String regional = null;
		String nomeTecnico = null;

		List<FornecedorDTO> listaFornecedores = null;
		List<FornecedorDTO> listaNaoGerouTarefa = new ArrayList<>();
		List<FornecedorDTO> listaNaoAvancouAtividade = new ArrayList<>();

		int totalTarefas = 0;
		int abertas = 0;

		try
		{

			System.out.println("##### INICIAR AGENDADOR DE TAREFA: Abrir Fap Aplicação Técnico -- Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy hh:mm:ss"));

			listaFornecedores = JobTecnicoTaticoDAO.getFornecedoresTecnicos();
			totalTarefas = listaFornecedores.size();

			System.out.println("RotinaAbreFapTecnico - Total de fornecedores: " + totalTarefas);

			if (NeoUtils.safeIsNotNull(listaFornecedores))
			{
				for (FornecedorDTO fornecedor : listaFornecedores)
				{
					try
					{
						System.out.println("RotinaAbreFapTecnico - 1");

						// Sigla da regional está no início do nome do técnico
						nomeTecnico = fornecedor.getNomeTecnico();
						regional = FapUtils.getSiglaRegional(nomeTecnico);

						//E-form externo - campo da aplicação técnico
						codigoTecnico = fornecedor.getCodigoTecnico();
						NeoObject sigmaColaborador = FapFactory.neoObjectFactory("SIGMACOLABORADOR", "cd_colaborador", codigoTecnico);
						//E-form aplicação técnico
						NeoObject aplicacaoTecnico = FapFactory.neoObjectFactory("FAPAplicacaoTecnico", "tecnico", sigmaColaborador);
						//E-form aplicação de pagamento
						NeoObject aplicacaoPagamento = FapFactory.neoObjectFactory("FAPAplicacaoDoPagamento", "codigo", 10L);
						//E-form principal da Fap
						NeoObject wfFap = AdapterUtils.createNewEntityInstance("FAPAutorizacaoDePagamento");
						EntityWrapper eWFap = new EntityWrapper(wfFap);

						NeoObject noRegional = PersistEngine.getNeoObject(AdapterUtils.getEntityClass("GCEscritorioRegional"), new QLEqualsFilter("siglaRegional", regional));
						NeoObject usuariosRegional = noRegional != null ? PersistEngine.getNeoObject(AdapterUtils.getEntityClass("FAPTecnicoUsuarios"), new QLEqualsFilter("regional.neoId", noRegional.getNeoId())) : null;
						if (usuariosRegional == null)
							continue;

						System.out.println("RotinaAbreFapTecnico - 2");

						solicitante = FapUtils.getSolicitanteTecnico(regional);
						aprovador = FapUtils.getAprovadorTecnico(regional);

						//Campos obrigatórios na atividade Solicitar Orçamento
						eWFap.findField("solicitanteOrcamento").setValue(solicitante);
						eWFap.findField("tecnicoSigma").setValue(aplicacaoTecnico);
						eWFap.findField("aplicacaoPagamento").setValue(aplicacaoPagamento);

						//Preenche o aprovador da FAP
						eWFap.findField("aprovador").setValue(aprovador);

						// Controla a competência do pagamento que será sempre referente ao mês anterior
						GregorianCalendar competencia = new GregorianCalendar();
						competencia.add(Calendar.MONTH, -1);
						competencia.set(Calendar.DAY_OF_MONTH, 1);
						competencia.set(Calendar.HOUR, 0);
						competencia.set(Calendar.MINUTE, 0);
						competencia.set(Calendar.SECOND, 0);
						competencia.set(Calendar.MILLISECOND, 0);
						eWFap.findField("competenciaPagamento").setValue(competencia);

						//Process model
						final ProcessModel pm = PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("name", "F001 - FAP - AutorizacaoDePagamento"));

						System.out.println("RotinaAbreFapTecnico - 3");

						PersistEngine.persist(wfFap);

						// Inicia a FAP
						String code = OrsegupsWorkflowHelper.iniciaProcesso(pm, wfFap, false, solicitante, true, null);

						System.out.println("RotinaAbreFapTecnico - 4");

						if (NeoUtils.safeIsNotNull(code))
						{
							abertas++;

							System.out.println("RotinaAbreFapTecnico - 5");

							// Com a inserção na tabela de controle, não haverá abertura de tarefas em duplicidade (Rota x Compentecia)
							NeoObject controleDeAbertura = AdapterUtils.createNewEntityInstance("FAPTecnicoControleAberturaTarefaViaJob");
							EntityWrapper ewControleDeAbertura = new EntityWrapper(controleDeAbertura);
							ewControleDeAbertura.setValue("codigoTecnico", fornecedor.getCodigoTecnico());
							ewControleDeAbertura.setValue("dataAbertura", new GregorianCalendar());
							ewControleDeAbertura.setValue("competencia", competencia);

							PersistEngine.persist(controleDeAbertura);

							try
							{
								FapUtils.avancaAtividade(code);
							}
							catch (WorkflowException e)
							{
								List<String> listaErros = FapUtils.getErrosEform(e);
								if (NeoUtils.safeIsNotNull(listaErros))
								{
									fornecedor.setListaErros(listaErros);
								}
								listaNaoAvancouAtividade.add(fornecedor);
							}
						}
						else
						{
							listaNaoGerouTarefa.add(fornecedor);
						}
					}
					catch (NaoEncontradoException e)
					{
						List<String> listaErros = new ArrayList<>();
						listaErros.add(e.getMessage());
						if (NeoUtils.safeIsNotNull(listaErros))
						{
							fornecedor.setListaErros(listaErros);
							listaNaoGerouTarefa.add(fornecedor);
						}
					}
					catch (WorkflowException e)
					{
						List<String> listaErros = FapUtils.getErrosEform(e);
						if (NeoUtils.safeIsNotNull(listaErros))
						{
							fornecedor.setListaErros(listaErros);
							listaNaoGerouTarefa.add(fornecedor);
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			// Tratar o que não foi gerado ou não avançou atividade
			FapUtils.naoGerouTarefaTecnicos(listaNaoGerouTarefa);
			FapUtils.naoAvancouAtividadeTecnicos(listaNaoAvancouAtividade);
			FapUtils.enviaResumoJobTecnico(totalTarefas, abertas, listaNaoGerouTarefa.size(), listaNaoAvancouAtividade.size());
		}
	}
}
