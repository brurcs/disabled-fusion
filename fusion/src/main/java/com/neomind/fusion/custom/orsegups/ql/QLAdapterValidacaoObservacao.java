package com.neomind.fusion.custom.orsegups.ql;

import javax.persistence.EntityTransaction;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

/**
 * Verifica se o Centro de Custo informado faz parte das exceções
 * 
 * @param obj
 *            O parametro pode ser do tipo XVETORHUSUFUNFUSION que representa a
 *            view V034FUSION
 */
public class QLAdapterValidacaoObservacao implements AdapterInterface {
    public static final Log log = LogFactory.getLog(QLAdapterValidacaoObservacao.class);
    EntityTransaction transaction;

    @Override
    public void start(Task origin, EntityWrapper processEntity, Activity activity) {
	try {
	    NeoObject posto = (NeoObject) processEntity.findValue("posto.nivel8");
	    if (posto != null) {
		EntityWrapper ewPosto = new EntityWrapper(posto);
		String codCcu = "";
		if (posto.getNeoType().equals("XVETORHUSUFUNFUSION")) {
		    codCcu = (String) ewPosto.findField("codccu").getValue();
		} else {
		    codCcu = (String) ewPosto.findField("usu_codccu").getValue();
		}

		if (codCcu != null) {
		    Boolean isValidaObs = false;
		    if (codCcu.equals("381085")) {
			isValidaObs = true;
		    } else if (codCcu.equals("7107820")) {
			isValidaObs = true;
		    } else if (codCcu.equals("7107790")) {
			isValidaObs = true;
		    } else if (codCcu.equals("7107780")) {
			isValidaObs = true;
		    } else if (codCcu.equals("7107850")) {
			isValidaObs = true;
		    } else if (codCcu.equals("7107860")) {
			isValidaObs = true;
		    } else if (codCcu.equals("7107810")) {
			isValidaObs = true;
		    } else if (codCcu.equals("7107800")) {
			isValidaObs = true;
		    } else if (codCcu.equals("7107870")) {
			isValidaObs = true;
		    } else if (codCcu.equals("7107840")) {
			isValidaObs = true;
		    } else if (codCcu.equals("7742730")) {
			isValidaObs = true;
		    } else if (codCcu.equals("7897090")) {
			isValidaObs = true;
		    }
		    if (isValidaObs) {
			String observacao = (String) processEntity.findValue("observacao");
			if (observacao.length() < 20) {
			    throw new Exception("A observação deve conter 20 caracteres ou mais!");
			}
		    }
		}
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    String msg = e.getMessage();
	    
	    if (msg == null){
		msg = "Erro ao validar observação, por favor verifique  os campos e tente novamente!";
	    }
	    throw new WorkflowException(msg);
	}
    }

    @Override
    public void back(EntityWrapper processEntity, Activity activity) {
    }
}