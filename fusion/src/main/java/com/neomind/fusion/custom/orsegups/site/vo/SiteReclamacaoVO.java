package com.neomind.fusion.custom.orsegups.site.vo;

import java.util.ArrayList;
import java.util.Collection;

import com.neomind.fusion.custom.orsegups.presenca.vo.ReclamacaoVO;

public class SiteReclamacaoVO
{
	private String codigoCliente;
	private Collection<ReclamacaoVO> reclamacoes = new ArrayList();
	
	public Collection<ReclamacaoVO> getReclamacoes()
	{
		return reclamacoes;
	}
	public void setReclamacoes(Collection<ReclamacaoVO> reclamacoes)
	{
		this.reclamacoes = reclamacoes;
	}
	public String getCodigoCliente()
	{
		return codigoCliente;
	}
	public void setCodigoCliente(String codigoCliente)
	{
		this.codigoCliente = codigoCliente;
	}
}
