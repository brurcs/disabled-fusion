package com.neomind.fusion.custom.orsegups.converter;

import com.neomind.fusion.custom.orsegups.fap.utils.FAPParametrizacao;
import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.fusion.eform.converter.StringConverter;
import com.neomind.fusion.entity.EntityWrapper;

public class FAPTaticoHistoricoEventosConverter extends StringConverter {
    
    @Override  
    public String getHTMLInput(EFormField field, OriginEnum origin) {
	
	EntityWrapper ew = new EntityWrapper(field.getForm().getObject());
	String code = ew.findGenericValue("wfprocess.code");
	StringBuilder historico = new StringBuilder();
	String link = null;
	boolean debug = (FAPParametrizacao.findParameter("debug").equals("1") ? true : false);
	
	if(debug){
	    link = "http://localhost:9090/fusion/portal/render/FormList?type=FAPHistoricoEventos&info=861908049&edit=false&_fl_set_dyn_codigoTarefa=";
	} else {
	    link = "https://intranet.orsegups.com.br/fusion/portal/render/FormList?type=FAPHistoricoEventos&info=861908049&edit=false&_fl_set_dyn_codigoTarefa=";	
	}
	historico.append(" <table class='gridbox gridboxNoHover' cellpadding='0' cellspacing='0' width='100%'> ");
	historico.append(" <tr style='cursor: auto'> ");
	historico.append(" <th style='cursor: auto; white-space: normal; text-align: center; width=\"10%\"'>HISTÓRICO DE EVENTOS</th> ");	
	historico.append(" </tr> ");
	historico.append(" <tbody> ");
	historico.append(" <td style='white-space: normal; text-align: center'> ");
	historico.append(" <a target =\"_blank\" href=\"" + link + code + "\" align=\"absmiddle\">"
		+ "<img class=\"tableIcon\" src=\"imagens/icones_final/properties_16x16-trans.png\" title=\"Visualizar\" align=\"absmiddle\"></a>Visualizar</td> ");
	historico.append(" </tbody> ");
	historico.append(" </table> ");
	
	return historico.toString();
    }

    @Override
    protected String getHTMLView(EFormField field, OriginEnum origin) {
	return getHTMLInput(field, origin);
    }
}
