package com.neomind.fusion.custom.orsegups.eform.vo;

public class ClienteVO extends EformVO {

    public ClienteVO() {
	super();
    }
    
    Integer cdCliente;
    String cpfCnpj;
    String razaoSocial;
    String idEmpresa;
    String particao;
    String idCentral;
    String empresaParticaoRazao;
    String endereco;
    
    public Integer getCdCliente() {
        return cdCliente;
    }
    public void setCdCliente(Integer cdCliente) {
        this.cdCliente = cdCliente;
    }
    public String getCpfCnpj() {
        return cpfCnpj;
    }
    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }
    public String getRazaoSocial() {
        return razaoSocial;
    }
    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }
    public String getIdEmpresa() {
        return idEmpresa;
    }
    public void setIdEmpresa(String idEmpresa) {
        this.idEmpresa = idEmpresa;
    }
    public String getParticao() {
        return particao;
    }
    public void setParticao(String particao) {
        this.particao = particao;
    }
    public String getIdCentral() {
        return idCentral;
    }
    public void setIdCentral(String idCentral) {
        this.idCentral = idCentral;
    }
    public String getEmpresaParticaoRazao() {
        return empresaParticaoRazao;
    }
    public void setEmpresaParticaoRazao(String empresaParticaoRazao) {
        this.empresaParticaoRazao = empresaParticaoRazao;
    }
    public String getEndereco() {
        return endereco;
    }
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }
}