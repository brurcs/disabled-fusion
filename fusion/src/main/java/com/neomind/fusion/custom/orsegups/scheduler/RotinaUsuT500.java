package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsConnectionUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;

public class RotinaUsuT500 implements CustomJobAdapter
{

    private Map<Integer, String> serverInfo = OrsegupsConnectionUtils.getServerInfo("SIGMA");
    
	@Override
	public void execute(CustomJobContext arg0)
	{
		System.out.println("RUN ROTINA TELEFONES usu_t500fne");
		//CONEXOES
		Connection connSigma = null, connSapiens = null;
		PreparedStatement pstmSapiens = null, pstmSigma = null, pstmCdm160 = null, pstmSel160 = null;

		//LISTA NOVA
		List<String> listaTelefones = new ArrayList<String>();

		//LISTA ANTIGA
		List<String> listaTelefonesUSU_T160FNE = new ArrayList<String>();

		//LISTA COMANDOS SQL
		List<String> listaCmd = new ArrayList<String>();

		try
		{
			
			//TODO verificar a possibilidade de utilizar esta query unificada futuramente.
			/*query.append(" ( ");
			query.append(" 		SELECT cli.foncli ,"); 
			query.append(" 		        cli.foncl2,"); 
			query.append(" 		        cli.foncl3,"); 
			query.append(" 		        cli.foncl4,"); 
			query.append(" 		        cli.foncl5,"); 
			query.append(" 		        ctr.usu_clifon,"); 
			query.append(" 		        cli.codcli,"); 
			query.append(" 		        0 cd_cliente");  
			query.append(" 		FROM e085cli cli"); 
			query.append(" 		INNER JOIN  usu_t160ctr ctr with(nolock) ON (cli.codcli  = ctr.usu_codcli)");  
			query.append(" 		INNER JOIN usu_t160cvs cvs  with(nolock) ON (cvs.usu_numctr = ctr.usu_numctr and"); 
			query.append(" 		cvs.usu_codfil = ctr.usu_codfil and cvs.usu_codemp = ctr.usu_codemp)"); 
			query.append(" 		WHERE ((cvs.usu_sitcvs = 'A') OR (cvs.usu_sitcvs = 'I' and cvs.usu_datfim >=  GETDATE()))"); 
			query.append(" 		)");
			query.append(" 		union");
			query.append(" 		(");
			query.append(" 		 SELECT DBC.FONE1 collate SQL_Latin1_General_CP1_CI_AS foncli,"); 
			query.append(" 		        DBC.FONE2 collate SQL_Latin1_General_CP1_CI_AS foncl2,"); 
			query.append(" 		        DBP.FONE1 collate SQL_Latin1_General_CP1_CI_AS foncl3,"); 
			query.append(" 		        DBP.FONE2 collate SQL_Latin1_General_CP1_CI_AS foncl4,"); 
			query.append(" 		        '0' foncl5  ,"); 
			query.append(" 		        '0' usu_clifon   ,"); 
			query.append(" 		        '0' codcli  , ");
			query.append(" 		        DBC.cd_cliente"); 
			query.append(" 		         FROM ["+this.serverInfo.get(OrsegupsConnectionUtils.NOME_SERVIDOR)+"\\"+this.serverInfo.get(OrsegupsConnectionUtils.INSTANCIA)+"].SIGMA90.dbo.dbPROVIDENCIA DBP   with(nolock)");  
			query.append(" 		         RIGHT JOIN ["+this.serverInfo.get(OrsegupsConnectionUtils.NOME_SERVIDOR)+"\\"+this.serverInfo.get(OrsegupsConnectionUtils.INSTANCIA)+"].SIGMA90.dbo.dbCENTRAL DBC  with(nolock)  ON (DBC.CD_CLIENTE =  DBP.CD_CLIENTE)"); 
			query.append(" 		         WHERE DBC.ctrl_central = 1"); 
			query.append(" 		)");
	        query.append("");*/ 
	        
	        
			connSigma = PersistEngine.getConnection("SIGMA90");
			connSapiens = PersistEngine.getConnection("SAPIENS");

			StringBuilder sqlSapiens = new StringBuilder();
			sqlSapiens.append(" SELECT cli.foncli, cli.foncl2, cli.foncl3, cli.foncl4, cli.foncl5, ctr.usu_clifon, cli.codcli  ");
			sqlSapiens.append(" FROM e085cli cli with(nolock)  ");
			sqlSapiens.append(" INNER JOIN  usu_t160ctr ctr  with(nolock)  ON (cli.codcli  = ctr.usu_codcli)  ");
			sqlSapiens.append(" INNER JOIN usu_t160cvs cvs  with(nolock)  ON (cvs.usu_numctr = ctr.usu_numctr and ");
			sqlSapiens.append(" cvs.usu_codfil = ctr.usu_codfil and cvs.usu_codemp = ctr.usu_codemp) ");
			sqlSapiens.append(" WHERE ((cvs.usu_sitcvs = 'A') OR (cvs.usu_sitcvs = 'I' and cvs.usu_datfim >=  GETDATE())) ");

			StringBuilder sqlSigma = new StringBuilder();
			sqlSigma.append(" SELECT DBC.FONE1, DBC.FONE2, DBP.FONE1, DBP.FONE2, DBC.CD_CLIENTE  ");
			sqlSigma.append(" FROM dbPROVIDENCIA DBP  with(nolock)  ");
			sqlSigma.append(" RIGHT JOIN dbCENTRAL DBC  with(nolock)  ON (DBC.CD_CLIENTE =  DBP.CD_CLIENTE) ");
			sqlSigma.append(" WHERE DBC.ctrl_central = 1 ");

			String sqlSapiens160 = " select usu_numfne, usu_cd_clie, usu_codcli from usu_t500fne  with(nolock) ";

			//BUSCA-SE REGISTROS NA BASE DO SAPIENS
			pstmSapiens = connSapiens.prepareStatement(sqlSapiens.toString());
			ResultSet rsSapiens = pstmSapiens.executeQuery();

			while (rsSapiens.next())
			{
				String str = this.formatarNumero(rsSapiens.getString(1));
				if (!str.equals("not"))
				{
					if (!listaTelefones.contains(str + ".0." + rsSapiens.getString(7)))
					{
						listaTelefones.add(str + ".0." + rsSapiens.getString(7));
					}
				}

				String str2 = this.formatarNumero(rsSapiens.getString(2));
				if (!str2.equals("not"))
				{
					if (!listaTelefones.contains(str2 + ".0." + rsSapiens.getString(7)))
					{
						listaTelefones.add(str2 + ".0." + rsSapiens.getString(7));
					}
				}

				String str3 = this.formatarNumero(rsSapiens.getString(3));
				if (!str3.equals("not"))
				{
					if (!listaTelefones.contains(str3 + ".0." + rsSapiens.getString(7)))
					{
						listaTelefones.add(str3 + ".0." + rsSapiens.getString(7));
					}
				}

				String str4 = this.formatarNumero(rsSapiens.getString(4));
				if (!str4.equals("not"))
				{
					if (!listaTelefones.contains(str4 + ".0." + rsSapiens.getString(7)))
					{
						listaTelefones.add(str4 + ".0." + rsSapiens.getString(7));
					}
				}

				String str5 = this.formatarNumero(rsSapiens.getString(5));
				if (!str5.equals("not"))
				{
					if (!listaTelefones.contains(str5 + ".0." + rsSapiens.getString(7)))
					{
						listaTelefones.add(str5 + ".0." + rsSapiens.getString(7));
					}
				}

				String str6 = this.formatarNumero(rsSapiens.getString(6));
				if (!str6.equals("not"))
				{
					if (!listaTelefones.contains(str6 + ".0." + rsSapiens.getString(7)))
					{
						listaTelefones.add(str6 + ".0." + rsSapiens.getString(7));
					}
				}

			}

			//BUSCA-SE REGISTROS NA BASE DO SIGMA
			pstmSigma = connSigma.prepareStatement(sqlSigma.toString());
			ResultSet rsSigma = pstmSigma.executeQuery();
			while (rsSigma.next())
			{
				String str1 = this.formatarNumero(rsSigma.getString(1));
				if (!str1.equals("not"))
				{
					if (!listaTelefones.contains(str1 + "." + rsSigma.getString(5) + ".0"))
					{
						listaTelefones.add(str1 + "." + rsSigma.getString(5) + ".0");
					}
				}

				String str2 = this.formatarNumero(rsSigma.getString(2));
				if (!str2.equals("not"))
				{
					if (!listaTelefones.contains(str2 + "." + rsSigma.getString(5) + ".0"))
					{
						listaTelefones.add(str2 + "." + rsSigma.getString(5) + ".0");
					}
				}

				String str3 = this.formatarNumero(rsSigma.getString(3));
				if (!str3.equals("not"))
				{
					if (!listaTelefones.contains(str3 + "." + rsSigma.getString(5) + ".0"))
					{
						listaTelefones.add(str3 + "." + rsSigma.getString(5) + ".0");
					}
				}

				String str4 = this.formatarNumero(rsSigma.getString(4));
				if (!str4.equals("not"))
				{
					if (!listaTelefones.contains(str4 + "." + rsSigma.getString(5) + ".0"))
					{
						listaTelefones.add(str4 + "." + rsSigma.getString(5) + ".0");
					}
				}

			}

			//BUSCA-SE REGISTROS NA BASE DO SAPIENS USU_T160FNE
			pstmSel160 = connSapiens.prepareStatement(sqlSapiens160);
			ResultSet rsSel160 = pstmSel160.executeQuery();
			while (rsSel160.next())
			{
				listaTelefonesUSU_T160FNE.add(rsSel160.getString(1) + "." + rsSel160.getString(2) + "." + rsSel160.getString(3));
			}
			
			//REGISTROS A DELETAR
			for (String deletado : listaTelefonesUSU_T160FNE)
			{
				if (!(listaTelefones.contains(deletado)))
				{
					String operadores[] = deletado.split("\\."); 
					listaCmd.add("delete from usu_t500fne where usu_numfne = '"+operadores[0]+"' and usu_cd_clie ='"+operadores[1]+"' and usu_codcli ='"+operadores[2]+"'");
				}
			}
			//REGISTROS A INSERIR
			for (String inserido : listaTelefones)
			{
				if (!(listaTelefonesUSU_T160FNE.contains(inserido)))
				{
					String operadores[] = inserido.split("\\."); 
					listaCmd.add("insert into usu_t500fne (usu_numfne, usu_cd_clie, usu_codcli) values ('"+operadores[0]+"' ,'"+operadores[1]+"','"+operadores[2]+"')");
				}
			}

			//EXECUTA-SE OS COMANDOS
			for (String cmd : listaCmd)
			{
				pstmCdm160 = connSapiens.prepareStatement(cmd);
				pstmCdm160.execute();
				
			}

			/*
			 * String url =
			 * "http://www.orsegups.com.br/fusion/custom/jsp/orsegups/snep/isCliente.jsp?foneOrigem=reload"
			 * ;
			 * try
			 * {
			 * URLEncoder.encode(url, "ISO-8859-1");
			 * URL urlAux = new URL(url);
			 * URLConnection uc = urlAux.openConnection();
			 * BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
			 * String teste = in.readLine();
			 * Log.warn(teste);
			 * }
			 * catch (IOException e)
			 * {
			 * // TODO Auto-generated catch block
			 * e.printStackTrace();
			 * }
			 */

		}
		catch (SQLException e)
		{
			System.out.println("ROTINA TELEFONES usu_t500 ERROR: " + e.getMessage());
		}
		finally
		{
			try
			{
				if (connSapiens != null)
				{
					connSapiens.close();
				}
				if (connSigma != null)
				{
					connSigma.close();
				}
				if (pstmCdm160 != null)
				{
					pstmCdm160.close();
				}

				if (pstmSapiens != null)
				{
					pstmSapiens.close();
				}

				if (pstmSigma != null)
				{
					pstmSigma.close();
				}

				if (pstmSel160 != null)
				{
					pstmSel160.close();
				}
			}
			catch (SQLException e)
			{

				System.out.println("ROTINA TELEFONES usu_t500 ERROR: " + e.getMessage());
			}
		}
	}

	public String formatarNumero(String auxFoneStr)
	{
		//VALIDACAO DA STRING
		if (auxFoneStr != null)
		{
			if (!(auxFoneStr.equals(" ") || auxFoneStr.isEmpty()))
			{
				auxFoneStr = auxFoneStr.replaceAll("[^0-9]", "");
				if (auxFoneStr.length() == 10 && !(auxFoneStr.equals("0000000000"))
											  && !(auxFoneStr.equals("1111111111"))
											  && !(auxFoneStr.equals("2222222222"))
											  && !(auxFoneStr.equals("3333333333"))
											  && !(auxFoneStr.equals("4444444444"))
											  && !(auxFoneStr.equals("5555555555"))
											  && !(auxFoneStr.equals("6666666666"))
											  && !(auxFoneStr.equals("7777777777"))
											  && !(auxFoneStr.equals("8888888888"))
											  && !(auxFoneStr.equals("9999999999")))
				{
					return auxFoneStr;
				}
			}
		}
		return "not";
	}
}

