package com.neomind.fusion.custom.orsegups.utils;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.HtmlEmail;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.engine.FusionRuntime;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.mail.MailSettings;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLOpFilter;
import com.neomind.fusion.workflow.ProcessState;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoUtils;

public class SchedulerUtils
{
	
	private static final Log log = LogFactory.getLog(SchedulerUtils.class);

	public String getHTMLInputFaltaEfetivo(Long numloc, Long taborg)
	{
		StringBuilder textoTable = new StringBuilder();
		try
		{
		
			if (numloc != null && taborg != null)
			{
				
				GregorianCalendar dataExecucao = new GregorianCalendar();
				dataExecucao = OrsegupsUtils.getSpecificWorkDay(dataExecucao, -12L);
				
				QLEqualsFilter numlocFilter = new QLEqualsFilter("numloc", numloc);
				QLEqualsFilter taborgFilter = new QLEqualsFilter("taborg", taborg);
				QLOpFilter dataFilter = new QLOpFilter("dataExecucao", ">=", (GregorianCalendar) dataExecucao);
				QLEqualsFilter processFilter = new QLEqualsFilter("processo", "G004 - Acompanhamento de Falta de Efetivo");
				
				QLGroupFilter groupFilter = new QLGroupFilter("AND");
				groupFilter.addFilter(numlocFilter);
				groupFilter.addFilter(taborgFilter);
				groupFilter.addFilter(dataFilter);
				groupFilter.addFilter(processFilter);
				
				List<NeoObject> faltasEfetivo = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TarefaFaltaEfetivo"), groupFilter, -1, -1, "dataExecucao");
				
				if (faltasEfetivo != null && !faltasEfetivo.isEmpty())
				{
					
					textoTable.append("	<table  class=\"gridbox gridboxNoHover\" cellpadding=\"0\" cellspacing=\"0\" width=\"80%\">");
					textoTable.append("		<tr style=\"cursor: auto\">");
					//textoTable.append("			<th style=\"cursor: auto\">Processo</th>");
					textoTable.append("			<th style=\"cursor: auto\">Tarefa</th>");
					textoTable.append("			<th style=\"cursor: auto\">Data Abertura</th>");
					textoTable.append("			<th style=\"cursor: auto; white-space: normal\">Resposta</th>");
					textoTable.append("			<th style=\"cursor: auto\">Executor</th>");
					textoTable.append("			<th style=\"cursor: auto\">Status</th>");
					textoTable.append("			<th style=\"cursor: auto\">Data Execução</th>");
					textoTable.append("		</tr>");
					textoTable.append("		<tbody>");	
					
					for (NeoObject obj : faltasEfetivo)
					{
						EntityWrapper wrapperObj = new EntityWrapper(obj);
						String tar = (String) wrapperObj.findValue("tarefa");
						String proc = (String) wrapperObj.findValue("processo");
						
						NeoObject tarEfetivo = PersistEngine.getObject(AdapterUtils.getEntityClass("AFEAcompanhamentoFaltaEfetivo"), new QLEqualsFilter("wfprocess.code", tar));
						EntityWrapper tarWrapper = new EntityWrapper(tarEfetivo);
						
						String user = (String) tarWrapper.findValue("executor.fullName");
						String descricao = (String) tarWrapper.findValue("resposta");
						ProcessState state = (ProcessState) tarWrapper.findValue("wfprocess.processState");
						String estado = "";
						
						switch (state.ordinal())
						{
							case 0:
								estado = "Em andamento";
								break;

							case 1:
								estado = "Finalizada";
								break;

							case 2:
								estado = "Cancelada";
								break;
						}
						
						if (descricao == null || descricao.equals("null"))
						{
							descricao = "";
						}
						
						GregorianCalendar dataInicio = (GregorianCalendar) tarWrapper.findValue("wfprocess.startDate");
						GregorianCalendar dataFinal = (GregorianCalendar) tarWrapper.findValue("wfprocess.finishDate");
						String datini = "";
						String datfim = "";
						
						if (dataInicio != null)
						{
							datini = NeoUtils.safeDateFormat(dataInicio);
						}
						
						if (dataFinal != null)
						{
							datfim = NeoUtils.safeDateFormat(dataFinal);
						}
						//Tarefa | Data Abertura | Resposta | Usuário | Status | Data Resposta
						textoTable.append("		<tr>");
						//textoTable.append("			<td>" + proc + "</td>");
						textoTable.append("			<td>" + tar + "</td>");
						textoTable.append("			<td>" + datini + "</td>");
						textoTable.append("			<td style=\"white-space: normal\">" + descricao + "</td>");
						textoTable.append("			<td>" + user + "</td>");
						textoTable.append("			<td>" + estado + "</td>");	
						textoTable.append("			<td>" + datfim + "</td>");
						
					}
					textoTable.append("			</tr>");
					textoTable.append("		</tbody>");
					textoTable.append("	</table>");
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return textoTable.toString();
	}
	
	public void enviaExecucaoRotina(String titulo, String corpo)
	{

		try
		{
			MailSettings settings = (MailSettings) PersistEngine.reload(FusionRuntime.getInstance().getSettings().getMailSettings());

			MailSettings mailClone = new MailSettings();
			mailClone.setMinutesInterval(settings.getMinutesInterval());
			mailClone.setFromEMail(settings.getFromEMail());
			mailClone.setFromName(settings.getFromName());
			mailClone.setRenderServer(settings.getRenderServer());
			mailClone.setSecurityFromEmail(settings.getSecurityFromEmail());
			mailClone.setSecurityCopyEmail(settings.getSecurityCopyEmail());
			mailClone.setSmtpSettings(settings.getSmtpSettings());
			mailClone.setPort(settings.getPort());
			mailClone.setSmtpServer(settings.getSmtpServer());
			mailClone.setEnabled(settings.isEnabled());

			mailClone.setFromEMail("fusion@orsegups.com.br");
			mailClone.setFromName("Orsegups Partipações S.A.");
			if (mailClone != null && corpo != null && !corpo.isEmpty()  && titulo != null && !titulo.isEmpty())
			{

				
				String informacao = "";
				HtmlEmail noUserEmail = new HtmlEmail();
				StringBuilder noUserMsg = new StringBuilder();

				Calendar saudacao = Calendar.getInstance();
				String saudacaoEMail = "";
				if (saudacao.get(Calendar.HOUR_OF_DAY) >= 6 && saudacao.get(Calendar.HOUR_OF_DAY) < 12)
				{
					saudacaoEMail = "Bom dia, ";
				}
				else if (saudacao.get(Calendar.HOUR_OF_DAY) >= 12 && saudacao.get(Calendar.HOUR_OF_DAY) <= 18)
				{
					saudacaoEMail = "Boa tarde, ";
				}
				else
				{
					saudacaoEMail = "Boa noite, ";
				}

				String subject = "Não Responda - E-mail referente a execução de rotina. ";
				String addTo = "emailautomatico@orsegups.com.br";
				String from = "fusion@orsegups.com.br";
				noUserEmail.setCharset("ISO-8859-1");
				noUserEmail.addTo("emailautomatico@orsegups.com.br");
				noUserEmail.addTo("cm@orsegups.com.br");

				noUserEmail.setSubject(subject);
				noUserMsg.append("<!doctype html>");
				noUserMsg.append("<html>");
				noUserMsg.append("<head>");
				noUserMsg.append("<meta charset=\"utf-8\">");
				noUserMsg.append("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
				noUserMsg.append("</head>");
				noUserMsg.append("<body>");
				noUserMsg.append("<table width=\"640\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#ffffff\">");
				noUserMsg.append("          <tbody>");
				noUserMsg.append("		    <td align=\"left\" valign=\"top\" >");
				noUserMsg.append("               <img src=\"http://intranet.orsegups.com.br/fusion/portal_orsegups/images/delivery/logoORS.jpg\" >");
				noUserMsg.append("            </td>");
				noUserMsg.append("			<td>");
				noUserMsg.append("			<strong>" + saudacaoEMail + " este e-mail se refere a execução de rotina de "+titulo+"</strong><br>");
				if (NeoUtils.safeIsNotNull(corpo) )
					noUserMsg.append("			<strong>Informação: </strong> " + corpo+ " <br>");
				
				noUserMsg.append("			</td>");
				noUserMsg.append("     </tbody></table>");
				noUserMsg.append("</body></html>");
				noUserEmail.setHtmlMsg(noUserMsg.toString());
				noUserEmail.setContent(noUserMsg.toString(), "text/html");
				mailClone.applyConfig(noUserEmail);

				noUserEmail.send();
				//OrsegupsUtils.sendEmail2SendGrid(addTo, from, subject, "", noUserMsg.toString());

				
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error("##### ERRO ENVIA E-MAIL ROTINA  - Data: " + e.getMessage().toString() + " - Data: " + NeoUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

		}

	}
}
