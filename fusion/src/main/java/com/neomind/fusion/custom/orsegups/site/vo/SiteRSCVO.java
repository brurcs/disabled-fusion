package com.neomind.fusion.custom.orsegups.site.vo;

import java.util.GregorianCalendar;

/**
 * Utilizar classe RSCVO
 * @author danilo.silva
 *
 */
@Deprecated
public class SiteRSCVO
{
	Long codCli;
	String tarefa;
	String nome;
	String email;
	String telefone;
	String cidade;
	String bairro;
	String mensagem;
	Long tipoContato;
	Long origemSolicitacao;
	GregorianCalendar prazo;
	
	public String getNome()
	{
		return nome;
	}
	public void setNome(String nome)
	{
		this.nome = nome;
	}
	public String getEmail()
	{
		return email;
	}
	public void setEmail(String email)
	{
		this.email = email;
	}
	public String getTelefone()
	{
		return telefone;
	}
	public void setTelefone(String telefone)
	{
		this.telefone = telefone;
	}
	public String getCidade()
	{
		return cidade;
	}
	public void setCidade(String cidade)
	{
		this.cidade = cidade;
	}
	public String getBairro()
	{
		return bairro;
	}
	public void setBairro(String bairro)
	{
		this.bairro = bairro;
	}
	public String getMensagem()
	{
		return mensagem;
	}
	public void setMensagem(String mensagem)
	{
		this.mensagem = mensagem;
	}
	public Long getTipoContato()
	{
		return tipoContato;
	}
	public void setTipoContato(Long tipoContato)
	{
		this.tipoContato = tipoContato;
	}
	public Long getOrigemSolicitacao()
	{
		return origemSolicitacao;
	}
	public void setOrigemSolicitacao(Long origemSolicitacao)
	{
		this.origemSolicitacao = origemSolicitacao;
	}
	public String getTarefa()
	{
		return tarefa;
	}
	public void setTarefa(String tarefa)
	{
		this.tarefa = tarefa;
	}
	public GregorianCalendar getPrazo()
	{
		return prazo;
	}
	public void setPrazo(GregorianCalendar prazo)
	{
		this.prazo = prazo;
	}
	
	
	public Long getCodCli()
	{
		return codCli;
	}
	public void setCodCli(Long codCli)
	{
		this.codCli = codCli;
	}
	@Override
	public String toString()
	{
		return "SiteRSCVO [codCli=" + codCli + ", tarefa=" + tarefa + ", nome=" + nome + ", email=" + email + ", telefone=" + telefone + ", cidade=" + cidade + ", bairro=" + bairro + ", mensagem=" + mensagem + ", tipoContato=" + tipoContato + ", origemSolicitacao=" + origemSolicitacao + ", prazo=" + prazo + "]";
	}
	
	
	
	
}
