package com.neomind.fusion.custom.orsegups.e2doc.xml.imoveisProprios;

public class Campos {
    
    private String i19;
    private String i20;
    private String i22;
    private String i23;
    private String i24;
    private String i26;
    private String i30;
    private String i31;
    private String i36;
    private String i37;
    private String i38;
    private String i55;
    private String i56;
    private String i58;
    
    public String getI19() {
        return i19;
    }
    public void setI19(String i19) {
        this.i19 = i19;
    }
    public String getI20() {
        return i20;
    }
    public void setI20(String i20) {
        this.i20 = i20;
    }
    public String getI22() {
        return i22;
    }
    public void setI22(String i22) {
        this.i22 = i22;
    }
    public String getI23() {
        return i23;
    }
    public void setI23(String i23) {
        this.i23 = i23;
    }
    public String getI24() {
        return i24;
    }
    public void setI24(String i24) {
        this.i24 = i24;
    }
    public String getI26() {
        return i26;
    }
    public void setI26(String i26) {
        this.i26 = i26;
    }
    public String getI30() {
        return i30;
    }
    public void setI30(String i30) {
        this.i30 = i30;
    }
    public String getI31() {
        return i31;
    }
    public void setI31(String i31) {
        this.i31 = i31;
    }
    public String getI36() {
        return i36;
    }
    public void setI36(String i36) {
        this.i36 = i36;
    }
    public String getI37() {
        return i37;
    }
    public void setI37(String i37) {
        this.i37 = i37;
    }
    public String getI38() {
        return i38;
    }
    public void setI38(String i38) {
        this.i38 = i38;
    }
    public String getI55() {
        return i55;
    }
    public void setI55(String i55) {
        this.i55 = i55;
    }
    public String getI56() {
        return i56;
    }
    public void setI56(String i56) {
        this.i56 = i56;
    }
    public String getI58() {
        return i58;
    }
    public void setI58(String i58) {
        this.i58 = i58;
    }
    
}
