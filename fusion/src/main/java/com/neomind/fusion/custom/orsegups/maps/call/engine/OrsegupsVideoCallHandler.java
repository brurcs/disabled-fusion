package com.neomind.fusion.custom.orsegups.maps.call.engine;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.neomind.fusion.custom.orsegups.maps.vo.EventoVO;
import com.neomind.fusion.custom.orsegups.maps.vo.ViaturaVO;

@Path(value = "eventosVideo")
public class OrsegupsVideoCallHandler
{
	

	@GET
	@Path("contatoCliente/{codigoHistorico}/{callOk}/{callAt}/{ramal}/{alteracao}")
	public void contatoCliente(@PathParam("codigoHistorico") String codigoHistorico, @PathParam("callOk") Boolean callOk, @PathParam("callAt") Boolean callAt, @PathParam("ramal") String ramal, @PathParam("alteracao") Boolean alteracao)
	{
		OrsegupsAlertEventVideoEngine.getInstance().contato(callOk, codigoHistorico, callAt, ramal, alteracao);
	}

	@GET
	@Path("abreOs/{codigoHistorico}/{descricao}")
	public void abreOs(@PathParam("codigoHistorico") String codigoHistorico, @PathParam("descricao") String descricao)
	{
		OrsegupsAlertEventVideoEngine.getInstance().abreOsSigma(codigoHistorico, descricao); 
	}

	@GET
	@Path("verificaEventos")
	public void verificaEventos()
	{
		OrsegupsAlertEventVideoEngine.getInstance().verificaEventos();
	}
	
	@GET
	@Path("verificarLigacaoPendente/{ramal}")
	public void verificaLigacaoPendente(@PathParam("ramal") String ramal)
	{
		OrsegupsAlertEventVideoEngine.getInstance().verificarLigacaoPendente(ramal);
	}
	
	@GET
	@Path("verificarLigacaoPendenteHistorico/{ramal}")
	public void verificarLigacaoPendenteHistorico(@PathParam("ramal") String ramal)
	{
		OrsegupsAlertEventVideoEngine.getInstance().verificarLigacaoPendenteHistorico(ramal);
	}

	@GET
	@Path("cadastrarEventLog/{codigoHistorico}/{callOk}")
	public void cadastrarEvetLog(@PathParam("codigoHistorico") String codigoHistorico, @PathParam("callOk") Boolean callOk, @PathParam("texto") String texto)
	{
		OrsegupsAlertEventVideoEngine.getInstance().cadastrarEvetLog(codigoHistorico, texto, texto);
	}

	@GET
	@Path("getLog/{codigoHistorico}/{callOk}/{callAt}/{texto}/{cliente}")
	public void getLog(@PathParam("codigoHistorico") String codigoHistorico, @PathParam("callOk") Boolean callOk, @PathParam("callAt") Boolean callAt, @PathParam("texto") String texto, @PathParam("cliente") String cliente)
	{
		OrsegupsAlertEventVideoEngine.getInstance().getLog(cliente, codigoHistorico);
	}

	@GET
	@Path("getAtualizaEventos/{codigoHistorico}/{texto}")
	public void getEventos(@PathParam("codigoHistorico") String codigoHistorico, @PathParam("texto") String valor)
	{
		OrsegupsAlertEventVideoEngine.getInstance().getDadosEventosHistorico(codigoHistorico, valor);
	}

	@GET
	@Path("alertAit")
	public void alertAit(ViaturaVO viaturaVO, EventoVO eventoVO)
	{
		//OrsegupsAlertEventVideoEngine.getInstance().alert(eventoVO);
	}

	
	@GET
	@Path("getLigacaoSnep/{numeroInterno}/{numeroExterno}/{codHistorico}/{cdCliente}")
	public void getLigacaoSnep(@PathParam("numeroInterno") String numeroInterno, @PathParam("numeroExterno") String numeroExterno,@PathParam("codHistorico") String codHistorico, @PathParam("cdCliente") String cdCliente )
	{	
		
	
		try
		{
			OrsegupsAlertEventVideoEngine.getInstance().getLigacaoSnep(numeroInterno, numeroExterno);
			String textoLog = "Ligando para cliente! " + numeroExterno + " Ramal "+numeroInterno+"###";
			
			OrsegupsAlertEventVideoEngine.getInstance().cadastrarEvetLog(codHistorico, cdCliente, textoLog);
		
		}
		catch (Exception e)
		{
			e.printStackTrace();
			
		}

	}
	
	@GET
	@Path("clearCache")
	public void clearCache()
	{
		OrsegupsAlertEventVideoEngine.getInstance().clearCache();
	}

}