package com.neomind.fusion.custom.orsegups.contract;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.eform.EFormField;
import com.neomind.fusion.eform.converter.DefaultConverter;
import com.neomind.fusion.eform.converter.OriginEnum;
import com.neomind.util.NeoUtils;

public class CancelaTarefaConverter extends DefaultConverter {

	private static final Log log = LogFactory.getLog(ContractCEPConverter.class);

	@Override
	public String getHTMLInput(EFormField field, OriginEnum origin)
	{
		final String id = getFieldId(field);
		
		// busca eformPai aos dados gerais
		Long eform = field.getForm().getCaller().getObjectId();
		
		String botao = "<input type=\"button\" class=\"input_button\" id=\"bt_cancela_fluxo\" value=\"Cancelar Fluxo de Contrato\" onClick = \"javascript:cancelTaskFluxoContratos( '"+eform+"' );\"/>";
		
		StringBuilder outBuilder = new StringBuilder();
		
		//outBuilder.append(super.getHTMLInput(field, origin));
		
		outBuilder.append(botao);
		
		return outBuilder.toString();
	}
	
	@Override
	// otavio.campos - Pode suportar qualquer tipo de campo, mas a mascara tem que ser retirada do valor aqui.
	public Object getValueFromHTMLString(EFormField field, Object value)
	{
		String valor = "";
		return valor;
	}

}
