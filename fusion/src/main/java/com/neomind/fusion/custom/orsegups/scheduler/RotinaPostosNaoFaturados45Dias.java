package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class RotinaPostosNaoFaturados45Dias implements CustomJobAdapter {

    @SuppressWarnings("unused")
    private static final Log log = LogFactory.getLog(RotinaPostosNaoFaturados45Dias.class);

    @Override
    public void execute(CustomJobContext arg0) {
	
	Connection conn = PersistEngine.getConnection("TIDB"); 
	PreparedStatement pstm = null;
	ResultSet rs = null;
	final Log log = LogFactory.getLog("com.neomind.fusion.custom.orsegups.scheduler.RotinaPostosNaoFaturados45Dias");
	log.warn("##### INICIO AGENDADOR DE TAREFA: RotinaPostosNaoFaturados45Dias - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	System.out.println("##### INICIO AGENDADOR DE TAREFA: RotinaPostosNaoFaturados45Dias - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	Long key = GregorianCalendar.getInstance().getTimeInMillis();
	try
	{
	    Long codNumCtrAtual = 0L;
	    
	    StringBuilder descricaoTarefa = new StringBuilder();
	    
	    StringBuffer  varname1 = new StringBuffer();
	    
	    varname1.append("SELECT * ");
	    varname1.append("           FROM ( ");
	    varname1.append("               select cli.NomCli as cliente, ");
	    varname1.append("	                  cvs.usu_codemp as empresa, ");
	    varname1.append("	    	    	  cvs.usu_codfil as filial, ");
	    varname1.append("	                  cvs.usu_numctr as contrato, ");
	    varname1.append("	                  cvs.usu_numpos as posto, ");
	    varname1.append("	                  cvs.usu_numctr, ");
	    varname1.append("	                  CONVERT(varchar(10),cvs.usu_datini,103) as dtIniPosto, ");
	    varname1.append("	    	    	  case when (CONVERT(varchar(10),coalesce(cvs.usu_datfim,'1900-12-31 00:00:00.000'),103) = '31/12/1900') then '' else CONVERT(varchar(10),cvs.usu_datfim,103) end as dtFimPosto, ");
	    varname1.append("	                  cvs.usu_numpos, ");
	    varname1.append("	    	    	  cvs.usu_codemp, ");
	    varname1.append("	    	    	  cvs.usu_codfil, ");
	    varname1.append("	    	    	  convert(varchar(4),year(DATEADD(DAY,-45,GETDATE())))+convert(varchar(2),month(DATEADD(DAY,-45,GETDATE()))) as competencia, ");
	    varname1.append("	    	    	  cvs.usu_qtdcvs*cvs.usu_preuni as valorPosto, ");
	    varname1.append("	    	    	  (select (COALESCE(SUM((case when (cms.usu_adcsub = '-') then -1*(cms.usu_qtdcvs*cms.usu_preuni) else (cms.usu_qtdcvs*cms.usu_preuni) end)),0)*1.05) as total ");
	    varname1.append("	    				 from [FSOODB04\\SQL02].Sapiens.dbo.usu_t160cms cms with(nolock) ");
	    varname1.append("	    				where convert(varchar(4),year(cms.usu_datcpt))+convert(varchar(2),month(cms.usu_datcpt)) = convert(varchar(4),year(DATEADD(DAY,-45,GETDATE())))+convert(varchar(2),month(DATEADD(DAY,-45,GETDATE()))) ");
	    varname1.append("	    				  and cms.usu_numpos = cvs.usu_numpos ");
	    varname1.append("	    				  and cms.usu_numctr = cvs.usu_numctr ");
	    varname1.append("	    				  and cms.usu_codfil = cvs.usu_codfil ");
	    varname1.append("	    				  and cms.usu_codemp = cvs.usu_codemp ");
	    varname1.append("	    	    	  ) as qtdeApontamentos ");
	    varname1.append("	             from [FSOODB04\\SQL02].Sapiens.dbo.usu_t160cvs cvs with(nolock) ");
	    varname1.append("	       inner join [FSOODB04\\SQL02].Sapiens.dbo.usu_t160ctr ctr with(nolock) ");
	    varname1.append("	                                                        on ctr.usu_codemp = cvs.usu_codemp ");
	    varname1.append("	    	    	    	                               and ctr.usu_codfil = cvs.usu_codfil ");
	    varname1.append("	    	    	    	                               and ctr.usu_numctr = cvs.usu_numctr ");
	    varname1.append("	       inner join [FSOODB04\\SQL02].Sapiens.dbo.E085CLI cli with(nolock) ");
	    varname1.append("	    	    	    	                            on ctr.usu_codcli = cli.CodCli ");
	    varname1.append("	            where cvs.usu_codemp not in (8,25,26) ");
	    varname1.append("	    	      and cvs.usu_numctr not in (1,65069) ");
	    varname1.append("	    	      and cvs.usu_codfil not in (0,1000,1001,1002) ");
	    varname1.append("	    	      and cli.tipemc in (1) ");
	    varname1.append("	    	      and cvs.usu_datini < DATEADD(DAY,-44,GETDATE()) ");
	    varname1.append("	    	      and cvs.usu_datini > DATEADD(DAY,-45,GETDATE()) ");
	    varname1.append("	              and not exists ( ");
	    varname1.append("	    	    			select 1 ");
	    varname1.append("	    	    			  from [FSOODB04\\SQL02].Sapiens.dbo.e160cvs ecvs  with(nolock) ");
	    varname1.append("	    	    			 where ecvs.codemp = cvs.usu_codemp ");
	    varname1.append("	    	    			   and ecvs.codfil = cvs.usu_codfil ");
	    varname1.append("	    	    			   and ecvs.numctr = cvs.usu_numctr ");
	    varname1.append("	    	    			   and ecvs.usu_seqagr = cvs.usu_numpos ");
	    varname1.append("	    	    			   and ecvs.numnfv is not null ");
	    varname1.append("	    	     ) ");
	    varname1.append("	    ) as tabela3 ");
	    varname1.append("	    WHERE (tabela3.valorPosto-case when (tabela3.qtdeApontamentos<0) then tabela3.qtdeApontamentos*-1 else tabela3.qtdeApontamentos end) > 0 ");
	    varname1.append("	 ORDER BY tabela3.usu_numctr,tabela3.usu_numpos");
	    
	    pstm = conn.prepareStatement(varname1.toString(),ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
	    rs = pstm.executeQuery();
	    
	    String titulo = "";
	    String scodEmp = "";
	    String scodFil = "";
	    
	    double contratoValorPosto = 0;
	    double contratoQtdeApontamento = 0;
	    double contratoSaldo = 0;
	    
	    int totalRegistros = 0;

	    while (rs.next())
	    {
	    	
	    	totalRegistros++;
			Long codEmp = rs.getLong("empresa");
			Long codFil = rs.getLong("filial");
			Long numCtr = rs.getLong("contrato");
			Long numPos = rs.getLong("posto");
			String cliente = rs.getString("cliente");
			String dtInicioPosto = rs.getString("dtIniPosto");
			String dtFimPosto = rs.getString("dtFimPosto");		
			
			double valorPosto = rs.getDouble("valorPosto");
			
			DecimalFormat df = new DecimalFormat("#,###.00");
			String sValorPosto = df.format(valorPosto); 
			
			if (codNumCtrAtual == 0L) {
			    
			    titulo = "Cliente: "+cliente+" | Empresa: " + codEmp + " Filial: " + codFil + " Contrato: " + numCtr + " - Postos Sem Faturamento à 45 dias.";
			    scodEmp = ""+codEmp;
			    scodFil = ""+codFil;
			    
			    codNumCtrAtual = numCtr;
			    descricaoTarefa = new StringBuilder();
			    descricaoTarefa.append("<table style=\"width:100%\">");
			    descricaoTarefa.append("   <tr>");
			    descricaoTarefa.append("      <td colspan=\"8\">Empresa: <b>"+codEmp+"</b> | Filial: <b>"+codFil+"</b> | Contrato: <b>"+numCtr+"</b><br></td>");
			    descricaoTarefa.append("   </tr>");
			    descricaoTarefa.append("   <tr>");
			    descricaoTarefa.append("      <td colspan=\"8\">Cliente: <b>"+cliente+"</b><br></td>");
			    descricaoTarefa.append("   </tr>");
			    descricaoTarefa.append("   <tr>");
			    descricaoTarefa.append("      <td colspan=\"8\">O contrato abaixo não foi faturado na(s) competência(s) listada(s):<br></td>");
			    descricaoTarefa.append("   </tr>");
			    descricaoTarefa.append("   <tr>");
			    descricaoTarefa.append("      <td width=\"20%\"><b>Posto</b></td>");
			    descricaoTarefa.append("      <td width=\"25%\"><b>Dt.Inicio Posto</b></td>");
			    descricaoTarefa.append("      <td width=\"25%\"><b>Dt.Fim Posto</b></td>");
			    descricaoTarefa.append("      <td width=\"30%\"><b>Valor Posto</b></td>");
			    descricaoTarefa.append("   </tr>"); 
			    
			}
			
			if (numCtr.equals(codNumCtrAtual) && descricaoTarefa.length() < 7000) {
			    descricaoTarefa.append("  <tr>");
			    descricaoTarefa.append("    <td>");
			    descricaoTarefa.append(numPos);
			    descricaoTarefa.append("    </td>");	
			    descricaoTarefa.append("    <td>");
			    descricaoTarefa.append(dtInicioPosto);
			    descricaoTarefa.append("    </td>");	
			    descricaoTarefa.append("    <td>");
			    descricaoTarefa.append(dtFimPosto);
			    descricaoTarefa.append("    </td>");
			    descricaoTarefa.append("    <td>");
			    descricaoTarefa.append(sValorPosto);
			    descricaoTarefa.append("    </td>");
			    descricaoTarefa.append("  </tr>");
			    codNumCtrAtual = numCtr;
			    contratoValorPosto += valorPosto;
			} else {
			    if (descricaoTarefa.length() >= 7000) {
				descricaoTarefa.append("  <tr>");
				descricaoTarefa.append("    <td colspan=\"8\" align=\"right\">");
				descricaoTarefa.append("      <br>Incompleto devido ao tamanho máximo de caracteres");
				descricaoTarefa.append("    </td>");
				descricaoTarefa.append("  </tr>"); 
			    }
			    descricaoTarefa.append("  <tr>");
			    descricaoTarefa.append("    <td colspan=\"8\" align=\"right\">");
			    descricaoTarefa.append("      <br>Total Posto(s): R$ "+ df.format(contratoValorPosto)+ " | Total Apontamentos Lançados: R$ "+ ((df.format(contratoQtdeApontamento) == ",00") ? "0,00" : df.format(contratoQtdeApontamento)) +" | Saldo Restante: R$ "+df.format(contratoSaldo));
			    descricaoTarefa.append("    </td>");
			    descricaoTarefa.append("  </tr>"); 
			    descricaoTarefa.append("  <tr>");
			    descricaoTarefa.append("    <td colspan=\"8\">");
			    descricaoTarefa.append("      <br>Verificar/Resolver e/ou liberar o faturamento.");
			    descricaoTarefa.append("    </td>");
			    descricaoTarefa.append("  </tr>"); 
			    descricaoTarefa.append("</table>");
			    rs.previous();
			    
			    abrirTarefa(scodEmp, scodFil, titulo, descricaoTarefa.toString());
			    
			    codNumCtrAtual = 0L;
			    contratoValorPosto = 0;
			    contratoQtdeApontamento = 0;
			    contratoSaldo = 0;
			}
	    }
	    if (totalRegistros == 0) {
			//abrirTarefaCiencia(); //Comentado para não abrir mais tarefa quando não houver registro retornado da QUERY acima a pedidos de Carol - TAREFA: 1732005
		}
	    
	}catch (Exception e) {
	    log.error("##### AGENDADOR DE TAREFA: Abrir Tarefa Validacao OS Chp1");
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por ["+key+"]");
	}
	finally {
	    try {
		rs.close();
		pstm.close();
		conn.close();
	    } catch (SQLException e) {
		e.printStackTrace();
	    }

	    log.warn("##### FIM AGENDADOR DE TAREFA: RotinaPostosNaoFaturados45Dias - - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	    System.out.println("##### FIM AGENDADOR DE TAREFA: RotinaPostosNaoFaturados45Dias - - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy HH:mm:ss"));
	}
    }
    
    /**
     * Metodo de abertura de tarefas
     * 
     * @param codEmp
     * @param codFil
     * @param numCtr
     * @param numPos
     * @param numeroOS
     * @return numero da Tarefa
     */
    private void abrirTarefa(String _codEmp, String _codFil, String _titulo, String _descricaoTarefa) throws Exception {
	
	String solicitante = "charlot.andrade";
	
	if (_codEmp.equals("1") && _codFil.equals("1")) {
	    solicitante = "taise.soares";
	} else if (_codEmp.equals("6") && _codFil.equals("1")) {
	    solicitante = "taise.soares";
	} else if (_codEmp.equals("17") && _codFil.equals("1")) {
	    solicitante = "amanda.rossi";
	} else if (_codEmp.equals("18") && _codFil.equals("1")) {
	    solicitante = "francielle.neckel";
	} else if (_codEmp.equals("18") && _codFil.equals("2")) {
	    solicitante = "taise.soares";
	} else if (_codEmp.equals("18") && _codFil.equals("3")) {
	    solicitante = "taise.soares";
	} else if (_codEmp.equals("18") && _codFil.equals("4")) {
	    solicitante = "francielle.neckel";
	} else if (_codEmp.equals("21") && _codFil.equals("1")) {
	    solicitante = "gileno.junior";
	} else if (_codEmp.equals("21") && _codFil.equals("2")) {
	    solicitante = "gileno.junior";
	} else if (_codEmp.equals("21") && _codFil.equals("3")) {
	    solicitante = "gileno.junior";
	} else if (_codEmp.equals("21") && _codFil.equals("4")) {
	    solicitante = "amanda.rossi";
	} else if (_codEmp.equals("24") && _codFil.equals("2")) {
	    solicitante = "gileno.junior";
	} else if (_codEmp.equals("29") && _codFil.equals("1")) {
	    solicitante = "gileno.junior";
	} else if (_codEmp.equals("29") && _codFil.equals("2")) {
	    solicitante = "gileno.junior";
	} else if (_codEmp.equals("30") && _codFil.equals("1")) {
	    solicitante = "charlot.andrade";
	}
	
	String executor = "";

	NeoPaper papelExecutor = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "ExecutorTarefaVarreduraPostoNaoFaturado"));

	NeoUser usuarioExecutor = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelExecutor);

	if(NeoUtils.safeIsNotNull(usuarioExecutor)){
	    executor = usuarioExecutor.getCode();
	}
	
	GregorianCalendar prazo = new GregorianCalendar();
	int dia = 0;
	while (dia <= 10) {	    	
	    if (OrsegupsUtils.isWorkDay(prazo)) {		
		dia++;
	    }
	    prazo.add(GregorianCalendar.DATE, 1);
	}
	while (!OrsegupsUtils.isWorkDay(prazo)) 
	{
		prazo = OrsegupsUtils.getNextWorkDay(prazo);
	}
	
	prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
	prazo.set(GregorianCalendar.MINUTE, 59);
	prazo.set(GregorianCalendar.SECOND, 59);
	
	

	IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
	
	iniciarTarefaSimples.abrirTarefa(solicitante, executor, _titulo, _descricaoTarefa, "1", "hadouken", prazo); 
	//iniciarTarefaSimples.abrirTarefa(solicitante, "lucas.alison", _titulo, _descricaoTarefa, "1", "hadouken", prazo);
	
    }
    
    /**
     * Metodo de abertura de tarefas para Ciência que na Data (45 dias atrás) não há postos não faturados
     * 
     * @param codEmp
     * @param codFil
     * @param numCtr
     * @param numPos
     * @param numeroOS
     * @return numero da Tarefa
     */
    private void abrirTarefaCiencia() throws Exception {
	
	String solicitante = "caroline.wrunski";
	
	String executor = "";

	NeoPaper papelExecutor = PersistEngine.getObject(NeoPaper.class, new QLEqualsFilter("code", "ExecutorTarefaVarreduraPostoNaoFaturado"));

	NeoUser usuarioExecutor = OrsegupsUtils.retornaPrimeiroUsuarioNoPapel(papelExecutor);

	if(NeoUtils.safeIsNotNull(usuarioExecutor)){
	    executor = usuarioExecutor.getCode();
	}
	
	GregorianCalendar prazo = new GregorianCalendar();
	int dia = 0;
	while (dia <= 15) {	    	
	    if (OrsegupsUtils.isWorkDay(prazo)) {		
		dia++;
	    }
	    prazo.add(GregorianCalendar.DATE, 2);
	}
	while (!OrsegupsUtils.isWorkDay(prazo)) 
	{
		prazo = OrsegupsUtils.getNextWorkDay(prazo);
	}
	
	StringBuilder descricaoTarefa = new StringBuilder();
    descricaoTarefa.append("<table style=\"width:100%\">");
    descricaoTarefa.append("   <tr>");
    descricaoTarefa.append("      <td>Tomar ciência. Na presente data não constam Postos não faturados à 45 dias</td>");
    descricaoTarefa.append("   </tr>");
    descricaoTarefa.append("</table>");
	
	prazo.set(GregorianCalendar.HOUR_OF_DAY, 23);
	prazo.set(GregorianCalendar.MINUTE, 59);
	prazo.set(GregorianCalendar.SECOND, 59);
	
	

	IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
	
	iniciarTarefaSimples.abrirTarefa(solicitante, executor, "Tomar Ciência. Postos Sem Faturamento à 45 dias.", descricaoTarefa.toString(), "1", "hadouken", prazo); 
	
    }
}
