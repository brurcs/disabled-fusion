package com.neomind.fusion.custom.orsegups.adapter.regrasSupPastas;

import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.TaskSummary;
import com.neomind.fusion.workflow.task.rule.TaskRuleAdapter;
import com.neomind.fusion.workflow.task.rule.TaskRuleContext;

public class AdapterRegraRSCDilmo implements TaskRuleAdapter{

	@Override
	public boolean isTriggered(TaskRuleContext context){
	    	
	    boolean retorno = false;
	    
	    Task t = null;
	    TaskSummary	ts = null;
	    try{
		
		if(context.getTask() instanceof  Task){
		    t = (Task) context.getTask();
		    if (t != null && t.getActivity() != null && t.getActivity().getModel() != null){
			switch (t.getActivity().getModel().getName()) {
			case "Registrar RSC - Escalada Presidência":
			    retorno = true;
			    break;
			case "Atender Solicitação - Escalada Presidência":
			    retorno = true;
			    break;
			case "Agendar Visita Escalada - Presidência":
			    retorno = true;
			    break; 
			case "Visitar Cliente Escalada - Presidência":
			    retorno = true;
			    break;    
			case "Aprovar Bonificação - Escalada Presidência":
			    retorno = true;
			    break;   
			case "Visitar Cliente - Escalada Presidência":
			    retorno = true;
			    break;  
			case "Tentar Reversão - Escalada Presidência":
			    retorno = true;
			    break; 
			case "Efetivar Cancelamento do Contrato - Escalada Presidência":
			    retorno = true;
			    break;  
			case "Verificação Eficácia - Sol. Cliente - Escalada Presidência":
			    retorno = true;
			    break;  
			default:
			    retorno = false;
			    break;
			}
		    }
		    if (t.getActivity().getModel().getName().equals("Registrar RSC - Escalada Presidência")){
			retorno = true;
		    }
		}else{
		    ts = (TaskSummary) context.getTask();
		    if (ts != null){
			switch (ts.getTaskName()) {
			case "Registrar RSC - Escalada Presidência":
			    retorno = true;
			    break;
			case "Atender Solicitação - Escalada Presidência":
			    retorno = true;
			    break;
			case "Agendar Visita Escalada - Presidência":
			    retorno = true;
			    break; 
			case "Visitar Cliente Escalada - Presidência":
			    retorno = true;
			    break;    
			case "Aprovar Bonificação - Escalada Presidência":
			    retorno = true;
			    break;   
			case "Visitar Cliente - Escalada Presidência":
			    retorno = true;
			    break;  
			case "Tentar Reversão - Escalada Presidência":
			    retorno = true;
			    break; 
			case "Efetivar Cancelamento do Contrato - Escalada Presidência":
			    retorno = true;
			    break;  
			case "Verificação Eficácia - Sol. Cliente - Escalada Presidência":
			    retorno = true;
			    break;  
			default:
			    retorno = false;
			    break;
			}
		    }
		}
	    }catch(Exception e){
		System.out.println("Erro ao direcionar tarefa RSC para subpastas. Mensagem: "+e.getMessage());
		e.printStackTrace();
	    }

	    return retorno;

	}
}

