package com.neomind.fusion.custom.orsegups.adapter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bouncycastle.asn1.tsp.TSTInfo;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;

public class FAPRegistroAtividades implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(FAPRegistroAtividades.class);

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		origin = OrsegupsWorkflowHelper.getTaskOrigin(activity);
		
		String mensagem = "Por favor, contatar o administrador do sistema!";
		WorkflowException exception = null;
		try
		{
			InstantiableEntityInfo registroAtividade = AdapterUtils.getInstantiableEntityInfo("FAPRegistroAtividade");
			NeoObject registro = registroAtividade.createNewInstance();
			EntityWrapper wRegistro = new EntityWrapper(registro);

			wRegistro.findField("responsavel").setValue(origin.getUser());
			
			wRegistro.findField("dataInicial").setValue(origin.getStartDate());
			wRegistro.findField("dataFinal").setValue(origin.getFinishDate());

			if (origin.getActivityName().equalsIgnoreCase("Aprovar Orçamento"))
			{
				String txtDescricao = (String) processEntity.findValue("observacaoAprovacao");
				Boolean aprovado = (Boolean) processEntity.findValue("aprovado");
				if(!aprovado){
					if(origin.getFinishByUser() == null)
					{
						processEntity.findField("proximaAtividadePosAprovacao").setValue("Desaprovou o FAP - Escalada");
						String des = "FAP não aprovada pelo responsável " + origin.getUser().getFullName() + " no prazo de 3 dias!";
						wRegistro.findField("descricao").setValue(des);
						wRegistro.findField("atividade").setValue("Desaprovou o FAP - Escalada");
					}
					else
					{
						processEntity.findField("proximaAtividadePosAprovacao").setValue("Desaprovou o FAP");
						wRegistro.findField("atividade").setValue("Desaprovou o FAP");
						wRegistro.findField("descricao").setValue(txtDescricao);						
					}
				}
				else
				{
					wRegistro.findField("atividade").setValue("Aprovou o FAP");
					wRegistro.findField("descricao").setValue(txtDescricao);
				}				
				processEntity.setValue("observacaoAprovacao", null);
			}
			else if (origin.getActivityName().equalsIgnoreCase("Revisar Orçamento Reprovado"))
			{
				wRegistro.findField("atividade").setValue("Reenviou o FAP para Aprovação");
				
				String txtDescricao = (String) processEntity.findValue("justificarDesaprovacao");
				wRegistro.findField("descricao").setValue(txtDescricao);
				processEntity.setValue("justificarDesaprovacao", null);
				processEntity.setValue("proximaAtividadePosAprovacao", null);
			}
			/*Início novo if*/
			else if (origin.getActivityName().equalsIgnoreCase("Revisar Orçamento Reprovado - Escalado"))
			{
				wRegistro.findField("atividade").setValue("Reenviou o FAP para Aprovação - Escalada");
				
				String txtDescricao = (String) processEntity.findValue("justificarDesaprovacao");
				wRegistro.findField("descricao").setValue(txtDescricao);
				processEntity.setValue("justificarDesaprovacao", null);
				processEntity.setValue("proximaAtividadePosAprovacao", null);
			}
			/*fim novo if*/
			else if (origin.getActivityName().equalsIgnoreCase("Slipar Pagamento"))
			{
				Boolean aceitaNotaFiscal = (Boolean) processEntity.findValue("aceitaNotaFiscal");
				if(!aceitaNotaFiscal)
				{
					wRegistro.findField("atividade").setValue("Desaprovou o FAP para Revisar a Nota Fiscal em Anexo.");
				}
				else
				{
					wRegistro.findField("atividade").setValue("Aprovou o FAP para Lançamento do Título");
				}
				
				String txtDescricao = (String) processEntity.findValue("observacaoNotaFiscal");
				
				if(txtDescricao != null && !txtDescricao.isEmpty()){
					wRegistro.findField("descricao").setValue(txtDescricao);
				}
				else
				{
					wRegistro.findField("descricao").setValue("Nenhuma informação a listar");
				}
				
				processEntity.setValue("observacaoNotaFiscal", null);
			}
			else if (origin.getActivityName().equalsIgnoreCase("Revisar Nota Fiscal"))
			{
				wRegistro.findField("atividade").setValue("Reenviou o FAP para Slipar Pagamento");
				String txtDescricao = (String) processEntity.findValue("justificarDesaprovacaoNotaFiscal");
				
				if(txtDescricao != null && !txtDescricao.isEmpty())
				{
					wRegistro.findField("descricao").setValue(txtDescricao);
				}
				else
				{
					wRegistro.findField("descricao").setValue("Nenhuma informação a listar");
				}
				processEntity.setValue("justificarDesaprovacaoNotaFiscal", null);
			}
			
			PersistEngine.persist(registro);
			processEntity.findField("registroAtividades").addValue(registro);
		}
		catch(Exception e)
		{
			log.error(mensagem);
			e.printStackTrace();
			throw new WorkflowException(mensagem);
		}
		
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}

}
