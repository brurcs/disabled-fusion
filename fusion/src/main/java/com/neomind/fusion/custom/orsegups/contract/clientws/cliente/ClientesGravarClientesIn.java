/**
 * ClientesGravarClientesIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.neomind.fusion.custom.orsegups.contract.clientws.cliente;

public class ClientesGravarClientesIn  implements java.io.Serializable {
    private com.neomind.fusion.custom.orsegups.contract.clientws.cliente.ClientesGravarClientesInDadosGeraisCliente[] dadosGeraisCliente;

    private java.lang.String dataBuild;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String idtReq;

    private java.lang.String sigInt;

    public ClientesGravarClientesIn() {
    }

    public ClientesGravarClientesIn(
    		com.neomind.fusion.custom.orsegups.contract.clientws.cliente.ClientesGravarClientesInDadosGeraisCliente[] dadosGeraisCliente,
           java.lang.String dataBuild,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String idtReq,
           java.lang.String sigInt) {
           this.dadosGeraisCliente = dadosGeraisCliente;
           this.dataBuild = dataBuild;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.idtReq = idtReq;
           this.sigInt = sigInt;
    }


    /**
     * Gets the dadosGeraisCliente value for this ClientesGravarClientesIn.
     * 
     * @return dadosGeraisCliente
     */
    public com.neomind.fusion.custom.orsegups.contract.clientws.cliente.ClientesGravarClientesInDadosGeraisCliente[] getDadosGeraisCliente() {
        return dadosGeraisCliente;
    }


    /**
     * Sets the dadosGeraisCliente value for this ClientesGravarClientesIn.
     * 
     * @param dadosGeraisCliente
     */
    public void setDadosGeraisCliente(com.neomind.fusion.custom.orsegups.contract.clientws.cliente.ClientesGravarClientesInDadosGeraisCliente[] dadosGeraisCliente) {
        this.dadosGeraisCliente = dadosGeraisCliente;
    }

    public com.neomind.fusion.custom.orsegups.contract.clientws.cliente.ClientesGravarClientesInDadosGeraisCliente getDadosGeraisCliente(int i) {
        return this.dadosGeraisCliente[i];
    }

    public void setDadosGeraisCliente(int i, com.neomind.fusion.custom.orsegups.contract.clientws.cliente.ClientesGravarClientesInDadosGeraisCliente _value) {
        this.dadosGeraisCliente[i] = _value;
    }


    /**
     * Gets the dataBuild value for this ClientesGravarClientesIn.
     * 
     * @return dataBuild
     */
    public java.lang.String getDataBuild() {
        return dataBuild;
    }


    /**
     * Sets the dataBuild value for this ClientesGravarClientesIn.
     * 
     * @param dataBuild
     */
    public void setDataBuild(java.lang.String dataBuild) {
        this.dataBuild = dataBuild;
    }


    /**
     * Gets the flowInstanceID value for this ClientesGravarClientesIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this ClientesGravarClientesIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this ClientesGravarClientesIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this ClientesGravarClientesIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the idtReq value for this ClientesGravarClientesIn.
     * 
     * @return idtReq
     */
    public java.lang.String getIdtReq() {
        return idtReq;
    }


    /**
     * Sets the idtReq value for this ClientesGravarClientesIn.
     * 
     * @param idtReq
     */
    public void setIdtReq(java.lang.String idtReq) {
        this.idtReq = idtReq;
    }


    /**
     * Gets the sigInt value for this ClientesGravarClientesIn.
     * 
     * @return sigInt
     */
    public java.lang.String getSigInt() {
        return sigInt;
    }


    /**
     * Sets the sigInt value for this ClientesGravarClientesIn.
     * 
     * @param sigInt
     */
    public void setSigInt(java.lang.String sigInt) {
        this.sigInt = sigInt;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientesGravarClientesIn)) return false;
        ClientesGravarClientesIn other = (ClientesGravarClientesIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.dadosGeraisCliente==null && other.getDadosGeraisCliente()==null) || 
             (this.dadosGeraisCliente!=null &&
              java.util.Arrays.equals(this.dadosGeraisCliente, other.getDadosGeraisCliente()))) &&
            ((this.dataBuild==null && other.getDataBuild()==null) || 
             (this.dataBuild!=null &&
              this.dataBuild.equals(other.getDataBuild()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.idtReq==null && other.getIdtReq()==null) || 
             (this.idtReq!=null &&
              this.idtReq.equals(other.getIdtReq()))) &&
            ((this.sigInt==null && other.getSigInt()==null) || 
             (this.sigInt!=null &&
              this.sigInt.equals(other.getSigInt())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDadosGeraisCliente() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDadosGeraisCliente());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDadosGeraisCliente(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getDataBuild() != null) {
            _hashCode += getDataBuild().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getIdtReq() != null) {
            _hashCode += getIdtReq().hashCode();
        }
        if (getSigInt() != null) {
            _hashCode += getSigInt().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientesGravarClientesIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesGravarClientesIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dadosGeraisCliente");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dadosGeraisCliente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesGravarClientesInDadosGeraisCliente"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataBuild");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dataBuild"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idtReq");
        elemField.setXmlName(new javax.xml.namespace.QName("", "idtReq"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sigInt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sigInt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
