package com.neomind.fusion.custom.orsegups.fap.scheduler;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.neomind.fusion.common.NeoRunnable;
import com.neomind.fusion.custom.orsegups.fap.dao.AplicacaoTaticoDAO;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.util.NeoUtils;


/**
 * @author diogo.silva
 * Esta classe tem por objetivo abrir tarefas simples quando forem encontradas duplicidades nas associações de Rota x Contas. 
 * */
public class RotinaAbreTarefaSimplesValidaContasVsRota implements CustomJobAdapter {

    @Override
    public void execute(CustomJobContext ctx) {
	try {
	    List<String> contasEmMaisDeUmaRota = AplicacaoTaticoDAO.verificarContasEmMaisDeUmaRota();

	    if(NeoUtils.safeIsNotNull(contasEmMaisDeUmaRota)){
		for(final String conta : contasEmMaisDeUmaRota){		 
		    final NeoRunnable work = new NeoRunnable(){
			@Override
			public void run() throws Exception {
			    String executor = "laercio.leite";
			    String solicitante = "rosileny";
			    String titulo = "FAP Tático - Verificar associação Rota x Conta";
			    String[] arr = conta.split("-");
			    String descricao = "Conta " + arr[1] + " da empresa " + arr[0] +  " está sem vinculada em mais de rota. Favor verificar o cadastro.";
			    GregorianCalendar prazo = new GregorianCalendar();
			    prazo.add(Calendar.DAY_OF_MONTH, 2);
			    while(!OrsegupsUtils.isWorkDay(prazo)){
				prazo.add(Calendar.DAY_OF_MONTH, 1);
			    }
			    prazo.set(Calendar.HOUR, 23);
			    prazo.set(Calendar.MINUTE, 59);
			    prazo.set(Calendar.SECOND, 59);

			    IniciarTarefaSimples tarefa = new IniciarTarefaSimples();
			    tarefa.abrirTarefa(solicitante, executor, titulo, descricao, "1", "hadouken", prazo);
			}
		    };
		    try {
			PersistEngine.managedRun(work);
		    } catch (final Exception e){
			e.printStackTrace();
			continue;
		    }		    
		}		
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
}
