package com.neomind.fusion.custom.orsegups.callcenter;

public class CallCenterClienteRetornoVO
{
	private String descricao;
	private String dataLigacao;
	private String origem;
	private String callBack;
	private Boolean retornouCliente;
	private String telefone;
	private String dataRetorno;
	private Boolean atendimentoEfetuado;
	private Boolean cliente;
	private Long id;
	private Boolean emAtendimento;
	private String usuario;
	private String dataPrimeiraLigacao;
	
	public String getDescricao()
	{
		return descricao;
	}
	public void setDescricao(String descricao)
	{
		this.descricao = descricao;
	}
	public String getDataLigacao()
	{
		return dataLigacao;
	}
	public void setDataLigacao(String dataLigacao)
	{
		this.dataLigacao = dataLigacao;
	}
	public String getOrigem()
	{
		return origem;
	}
	public void setOrigem(String origem)
	{
		this.origem = origem;
	}
	public String getCallBack()
	{
		return callBack;
	}
	public void setCallBack(String callBack)
	{
		this.callBack = callBack;
	}
	public Boolean getRetornouCliente()
	{
		return retornouCliente;
	}
	public void setRetornouCliente(Boolean retornouCliente)
	{
		this.retornouCliente = retornouCliente;
	}
	public String getTelefone()
	{
		return telefone;
	}
	public void setTelefone(String telefone)
	{
		this.telefone = telefone;
	}
	public String getDataRetorno()
	{
		return dataRetorno;
	}
	public void setDataRetorno(String dataRetorno)
	{
		this.dataRetorno = dataRetorno;
	}
	public Boolean getAtendimentoEfetuado()
	{
		return atendimentoEfetuado;
	}
	public void setAtendimentoEfetuado(Boolean atendimentoEfetuado)
	{
		this.atendimentoEfetuado = atendimentoEfetuado;
	}
	public Boolean getCliente()
	{
		return cliente;
	}
	public void setCliente(Boolean cliente)
	{
		this.cliente = cliente;
	}
	public Long getId()
	{
		return id;
	}
	public void setId(Long id)
	{
		this.id = id;
	}
	public Boolean getEmAtendimento()
	{
		return emAtendimento;
	}
	public void setEmAtendimento(Boolean emAtendimento)
	{
		this.emAtendimento = emAtendimento;
	}
	public String getUsuario()
	{
		return usuario;
	}
	public void setUsuario(String usuario)
	{
		this.usuario = usuario;
	}
	public String getDataPrimeiraLigacao() {
		return dataPrimeiraLigacao;
	}
	public void setDataPrimeiraLigacao(String dataPrimeiraLigacao) {
		this.dataPrimeiraLigacao = dataPrimeiraLigacao;
	}
	

}
