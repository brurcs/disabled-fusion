package com.neomind.fusion.custom.orsegups.site.vo;

public class SiteDocsMostrarVO
{
	private Long codDoc;
	private Long codEmp;
	private Long codCli;
	
	public SiteDocsMostrarVO(Long codDoc, Long codEmp, Long codCli)
	{
		super();
		this.codDoc = codDoc;
		this.codEmp = codEmp;
		this.codCli = codCli;
	}
	public Long getCodDoc()
	{
		return codDoc;
	}
	public void setCodDoc(Long codDoc)
	{
		this.codDoc = codDoc;
	}
	public Long getCodEmp()
	{
		return codEmp;
	}
	public void setCodEmp(Long codEmp)
	{
		this.codEmp = codEmp;
	}
	public Long getCodCli()
	{
		return codCli;
	}
	public void setCodCli(Long codCli)
	{
		this.codCli = codCli;
	}
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codCli == null) ? 0 : codCli.hashCode());
		result = prime * result + ((codDoc == null) ? 0 : codDoc.hashCode());
		result = prime * result + ((codEmp == null) ? 0 : codEmp.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SiteDocsMostrarVO other = (SiteDocsMostrarVO) obj;
		if (codCli == null)
		{
			if (other.codCli != null)
				return false;
		}
		else if (!codCli.equals(other.codCli))
			return false;
		if (codDoc == null)
		{
			if (other.codDoc != null)
				return false;
		}
		else if (!codDoc.equals(other.codDoc))
			return false;
		if (codEmp == null)
		{
			if (other.codEmp != null)
				return false;
		}
		else if (!codEmp.equals(other.codEmp))
			return false;
		return true;
	}

	
}
