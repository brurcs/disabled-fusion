package com.neomind.fusion.custom.orsegups.rsc;

import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.custom.orsegups.utils.novo.OrsegupsWorkflowHelper;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.InstantiableEntityInfo;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class RSCMontarPreenchimentoAutomaticoTS implements AdapterInterface
{
	private static final Log log = LogFactory.getLog(RSCMontarPreenchimentoAutomaticoTS.class);

	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
	    try{
		RSCUtils rscUtils = new RSCUtils();
		rscUtils.registrarTarefaSimples(processEntity, activity);
	    }catch(Exception e){
		System.out.println("Erro na classe RSCMontarPreenchimentoAutomaticoTS do fluxo C027.099 - RSC - Verificação de Eficácia.");
		e.printStackTrace();
		throw new WorkflowException(e.getMessage());
	    }
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}
}
