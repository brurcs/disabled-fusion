package com.neomind.fusion.custom.orsegups.mobile;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name="OsergupsLogin", urlPatterns={"/servlet/com.neomind.fusion.custom.orsegups.mobile.OsergupsLogin"})
public class OsergupsLogin extends HttpServlet
{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
			IOException
	{
		this.doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
			IOException
	{
		this.doRequest(req, resp);
	}

	private void doRequest(HttpServletRequest req, HttpServletResponse resp)
	{

		String action = req.getParameter("action");

		PrintWriter out = null;
		try
		{
			if (action.equalsIgnoreCase("login"))
			{

				String username = req.getParameter("username");
				String password = req.getParameter("password");

				System.out.println(username + " - " + password);

				out = resp.getWriter();

				out.print("123456789");
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			out.close();
		}
	}

}
