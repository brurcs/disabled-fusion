package com.neomind.fusion.custom.orsegups.adapter;

import java.util.GregorianCalendar;

import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.event.TaskAssignerEvent;
import com.neomind.fusion.workflow.event.TaskAssignerEventListener;
import com.neomind.fusion.workflow.event.TaskStartEvent;
import com.neomind.fusion.workflow.event.TaskStartEventListener;
import com.neomind.fusion.workflow.exception.TaskException;

public class DefinePrazoConfirmarCobrancaCSE  implements AdapterInterface{
 
    @Override
    public void start(Task origin, EntityWrapper processEntity, Activity activity) {
	// TODO Auto-generated method stub
	
	GregorianCalendar dueDate = processEntity.findGenericValue("qlExecucao.dataFinal");
		
	dueDate = OrsegupsUtils.getSpecificWorkDay(dueDate, 2L);

	
	processEntity.findField("prazoConfirmarCobranca").setValue(dueDate);
    }

    @Override
    public void back(EntityWrapper processEntity, Activity activity) {
	// TODO Auto-generated method stub
	
    }
}
