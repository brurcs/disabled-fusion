package com.neomind.fusion.custom.orsegups.scheduler;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.IniciarTarefaSimples;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.util.NeoDateUtils;

public class RotinaAbreTarefaSimplesCobrancaDeslocamentoExclusivo implements CustomJobAdapter {
    private static final Log log = LogFactory.getLog(RotinaAbreTarefaSimplesCobrancaDeslocamentoExclusivo.class);

    @Override
    public void execute(CustomJobContext arg0) {

	Long key = GregorianCalendar.getInstance().getTimeInMillis();

	Connection conn = null;
	StringBuilder sql = new StringBuilder();
	PreparedStatement pstm = null;
	ResultSet rs = null;
	log.warn("##### INICIAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Cobrança Deslocamento Exclusivo - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));

	String ultimaExecucaoRotina = ultimaExecucaoRotina();
	String ultimaExecucaoRotinaAux = ultimaExecucaoRotinaRange(); 
	
	try {

	    sql.append(" SELECT C.ID_CENTRAL, C.PARTICAO, C.RAZAO, C.FANTASIA, C.CD_CLIENTE, H.TX_OBSERVACAO_FECHAMENTO, H.DT_RECEBIDO, ");
	    sql.append(" H.CD_EVENTO, REPLACE(REPLACE(REPLACE(REPLACE(hfe.NM_FRASE_EVENTO, 'NÃO LIGAR', ''), 'ROUBO', ''), '()', ''), '  ', ' ') as NM_FRASE_EVENTO, h.CD_HISTORICO ");
	    sql.append(" FROM VIEW_HISTORICO H WITH(NOLOCK) ");
	    sql.append(" INNER JOIN dbCENTRAL C WITH(NOLOCK) ON C.CD_CLIENTE = H.CD_CLIENTE ");
	    sql.append(" INNER JOIN HISTORICO_FRASE_EVENTO hfe with (nolock) ON hfe.CD_FRASE_EVENTO = H.CD_FRASE_EVENTO ");
	    sql.append(" INNER JOIN [CACUPE\\SQL02].fusion_producao.DBO.X_dbGrupoCliente GX ON GX.cd_grupo_cliente = C.CD_GRUPO_CLIENTE ");
	    sql.append(" INNER JOIN [CACUPE\\SQL02].fusion_producao.DBO.D_gruposCobrancaDeslocamentoExclusivo G ON GX.neoId = G.grupoCliente_neoId ");
	    sql.append(" WHERE H.FG_STATUS = 4 ");
	    sql.append(" AND H.DT_VIATURA_DESLOCAMENTO IS NOT NULL ");
	    sql.append(" AND H.DT_ESPERA_DESLOCAMENTO IS NOT NULL ");
	    sql.append(" AND H.DT_VIATURA_DESLOCAMENTO != h.DT_ESPERA_DESLOCAMENTO ");
	    sql.append(" AND NOT EXISTS (SELECT 1 FROM [FSOODB04\\SQL02].TIDB.DBO.HISTORICO_TAREFAS_COBRANCA_DESLOCAMENTO HT WHERE HT.HISTORICO = H.CD_HISTORICO) ");
	    sql.append(" AND (H.DT_FECHAMENTO > '" + ultimaExecucaoRotina + "' OR H.DT_FECHAMENTO > '" + ultimaExecucaoRotinaAux + "') ");
	    
	    conn = PersistEngine.getConnection("SIGMA90");
	    pstm = conn.prepareStatement(sql.toString());

	    rs = pstm.executeQuery();
	    inserirFimRotina();
	    /**
	     * Data de corte para busca de histórico de tarefas antigas
	     */

	    while (rs.next()) {
		String solicitante = OrsegupsUtils.getUserNeoPaper("solicitanteCobrancaDeslocamentoExclusivo");
		if (solicitante == null){
		    log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Cobrança Deslocamento Exclusivo - Regra: solicitante não encontrado");
		    continue;
		}
		String executor = OrsegupsUtils.getUserNeoPaper("executorCobrancaDeslocamentoExclusivo");
		if (executor == null){
		    log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Cobrança Deslocamento Exclusivo - Regra: executor não encontrado");
		    continue;
		}
		String tituloAux = "Cobrar Deslocamento - "+rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "]% ";

		String titulo = "Cobrar Deslocamento - "+rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "] "+rs.getString("FANTASIA")+"("+rs.getString("RAZAO")+")";

		GregorianCalendar prazo = new GregorianCalendar();
		prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
		prazo.set(Calendar.HOUR_OF_DAY, 23);
		prazo.set(Calendar.MINUTE, 59);
		prazo.set(Calendar.SECOND, 59);

		String descricao = OrsegupsUtils.getHistoricoTarefaSimples(tituloAux);
		descricao += " <strong>Conta :</strong> " + rs.getString("ID_CENTRAL") + "[" + rs.getString("PARTICAO") + "] " + " <br>";
		descricao = descricao + " <strong>Razão Social :</strong> " + rs.getString("RAZAO") + " <br>";
		descricao = descricao + " <strong>Nome Fantasia :</strong> " + rs.getString("FANTASIA") + " <br>";
		descricao = descricao + " <strong>Evento :</strong> " + rs.getString("CD_EVENTO") + " - " + rs.getString("NM_FRASE_EVENTO") + " <br>";
		Date dtRecebido = rs.getDate("DT_RECEBIDO");
		String dataRecebido = NeoDateUtils.safeDateFormat(dtRecebido, "dd/MM/yyyy");
		String horaRecebido = rs.getString("DT_RECEBIDO").substring(11, 16);
		descricao = descricao + " <strong>Recebido :</strong> " + dataRecebido+" "+horaRecebido + " <br>";
		descricao = descricao + " <strong>Log evento :</strong> <br>" + rs.getString("TX_OBSERVACAO_FECHAMENTO") + "<br>";

		IniciarTarefaSimples iniciarTarefaSimples = new IniciarTarefaSimples();
		String tarefa = iniciarTarefaSimples.abrirTarefa(solicitante, executor, titulo, descricao, "1", "sim", prazo);
		
		if (tarefa != null && !tarefa.isEmpty() && !tarefa.contains("Erro")){
		    gravarHistoricoTarefa(rs.getLong("CD_HISTORICO"), tarefa);
		}
		
		log.warn("##### EXECUTAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Cobrança Deslocamento Exclusivo - Responsavel : " + executor + " Tarefa: " + tarefa);
		System.out.println("##### EXECUTAR AGENDADOR DE TAREFA: Abrir Tarefa Simples Cobrança Deslocamento Exclusivo - Responsavel : " + executor + " Tarefa: " + tarefa);

	    }

	} catch (Exception e) {
	    log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Cobrança Deslocamento Exclusivo - ERRO AO GERAR TAREFA");
	    System.out.println("[" + key + "] ##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Cobrança Deslocamento Exclusivo - ERRO AO GERAR TAREFA");
	    e.printStackTrace();
	    throw new JobException("Erro no processamento, procurar no log por [" + key + "]");
	} finally {

	    try {
		OrsegupsUtils.closeConnection(conn, pstm, rs);
	    } catch (Exception e) {
		log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Cobrança Deslocamento Exclusivo - ERRO DE EXECUÇÃO");
		e.printStackTrace();
	    }

	    log.warn("##### FINALIZAR  AGENDADOR DE TAREFA:Abrir Tarefa Simples Cobrança Deslocamento Exclusivo - Data: "
		    + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
	}

    }
    
    private void inserirFimRotina() {

	try {
	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("nomeRotina", "RotinaAbreTarefaSimplesCobrancaDeslocamentoExclusivo"));

	    List<NeoObject> monitoraAgendador = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);
	    PersistEngine.getEntityManager().flush();

	    if (monitoraAgendador != null && !monitoraAgendador.isEmpty()) {
		NeoObject neoObject = (NeoObject) monitoraAgendador.get(0);

		EntityWrapper monitoraAgendadorWrapper = new EntityWrapper(neoObject);

		monitoraAgendadorWrapper.findField("dataFinalAgendador").setValue(new GregorianCalendar());

		PersistEngine.persist(neoObject);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
    
    private String ultimaExecucaoRotina() {

	String retorno = "";
	try {
	    Collection<NeoObject> monitoraAgendador = null;

	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("nomeRotina", "RotinaAbreTarefaSimplesCobrancaDeslocamentoExclusivo"));

	    monitoraAgendador = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);

	    if (monitoraAgendador != null && !monitoraAgendador.isEmpty()) {
		for (NeoObject neoObject : monitoraAgendador) {
		    EntityWrapper monitoraAgendadorObj = new EntityWrapper(neoObject);

		    GregorianCalendar dataFinalAgendador = (GregorianCalendar) monitoraAgendadorObj.getValue("dataFinalAgendador");

		    retorno = NeoDateUtils.safeDateFormat(dataFinalAgendador, "yyyy-MM-dd HH:mm:ss");
		}
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	}

	return retorno;
    }

    private String ultimaExecucaoRotinaRange() {

	String retorno = "";
	try {
	    Collection<NeoObject> monitoraAgendador = null;

	    QLGroupFilter filter = new QLGroupFilter("AND");
	    filter.addFilter(new QLEqualsFilter("nomeRotina", "RotinaAbreTarefaSimplesCobrancaDeslocamentoExclusivo"));

	    monitoraAgendador = (Collection<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("TIMonitoraAgendador"), filter);

	    if (monitoraAgendador != null && !monitoraAgendador.isEmpty()) {
		for (NeoObject neoObject : monitoraAgendador) {
		    EntityWrapper monitoraAgendadorObj = new EntityWrapper(neoObject);

		    GregorianCalendar dataFinalAgendador = (GregorianCalendar) monitoraAgendadorObj.getValue("dataFinalAgendador");

		    GregorianCalendar dataFinalAgendadorAux = (GregorianCalendar) dataFinalAgendador.clone();

		    dataFinalAgendadorAux.add(Calendar.MINUTE, -15);

		    retorno = NeoDateUtils.safeDateFormat(dataFinalAgendadorAux, "yyyy-MM-dd HH:mm:ss");
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return retorno;
    }

    private void gravarHistoricoTarefa(Long historico, String tarefa){
	Connection conn = null;
	PreparedStatement pstm = null;

	StringBuilder sql = new StringBuilder();

	try {
	    conn = PersistEngine.getConnection("TIDB");
	    
	    sql.append(" INSERT INTO HISTORICO_TAREFAS_COBRANCA_DESLOCAMENTO (HISTORICO, TAREFA) VALUES (?,?) ");
	    
	    pstm = conn.prepareStatement(sql.toString());
	    
	    pstm.setLong(1, historico);
	    pstm.setLong(2, new Long(tarefa));
	    
	    pstm.executeUpdate();

	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    OrsegupsUtils.closeConnection(conn, pstm, null);
	}
    }
    
}
