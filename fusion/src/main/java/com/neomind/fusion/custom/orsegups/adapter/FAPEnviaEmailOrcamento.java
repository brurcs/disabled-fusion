package com.neomind.fusion.custom.orsegups.adapter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.StringTokenizer;

import com.neomind.fusion.common.NeoObject; 
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class FAPEnviaEmailOrcamento implements AdapterInterface
{
	public void back(EntityWrapper processEntity, Activity activity)
	{
	}

	@SuppressWarnings("unchecked")
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		ArrayList<String> listaEmails = new ArrayList<String>();
		ArrayList<String> listaEmailsCopia = new ArrayList<String>();
		try
		{ 
			String code = (String)processEntity.findValue("wfprocess.code");
			String emailRemetente = (String)processEntity.findValue("solicitanteOrcamento.email");
			List<NeoObject> listaDestinatarios = (List<NeoObject>) processEntity.findValue("destinatarioOrcamento");
			if (listaDestinatarios != null && !listaDestinatarios.isEmpty()) 	{
				for (NeoObject dst : listaDestinatarios) {
					EntityWrapper wrpItem = new EntityWrapper(dst);
					String emailsStr = (String)wrpItem.findValue("emails");
					if (emailsStr != null ) {
						StringTokenizer tokenEmailStr = new StringTokenizer(emailsStr, ";");
						while (tokenEmailStr.hasMoreElements())
						{
							String email = (String) tokenEmailStr.nextElement();
							email = email.trim();
							if (email.contains("@")) {
								listaEmails.add(email);
							}
						}
					}
					String emailForn = (String)wrpItem.findValue("fornecedor.intnet");
					if (emailForn != null ) {
						StringTokenizer tokenEmailStr = new StringTokenizer(emailForn, ";");
						while (tokenEmailStr.hasMoreElements())
						{
							String email = (String) tokenEmailStr.nextElement();
							email = email.trim();
							if (email.contains("@")) {
								listaEmails.add(email);
							}
						}
					}
				}
			}
			
			Long aplicacaoPagamento = (Long) (Long) processEntity.findValue("aplicacaoPagamento.codigo");
			
			String emailCopia = "";
			if (aplicacaoPagamento == 1L)
			{
				emailCopia = (String) processEntity.findValue("aplicacaoPagamento.destinatarioDaCopiaOculta");
			}
			if (aplicacaoPagamento == 2L)
			{
				emailCopia = (String) processEntity.findValue("aplicacaoPagamento.destinatarioDaCopiaOculta");
			}
			if (aplicacaoPagamento == 3L)
			{
				emailCopia = (String) processEntity.findValue("aplicacaoPagamento.destinatarioDaCopiaOculta");
			}	
			if (aplicacaoPagamento == 4L)
			{
				emailCopia = (String) processEntity.findValue("aplicacaoPagamento.destinatarioDaCopiaOculta");
			}	
			if (aplicacaoPagamento == 5L)
			{
				emailCopia = (String) processEntity.findValue("aplicacaoPagamento.destinatarioDaCopiaOculta");
			}	
			if (aplicacaoPagamento == 6L)
			{
				emailCopia = (String) processEntity.findValue("aplicacaoPagamento.destinatarioDaCopiaOculta");
			}	
			if (aplicacaoPagamento == 7L)
			{
				emailCopia = (String) processEntity.findValue("aplicacaoPagamento.destinatarioDaCopiaOculta");
			}	
			if (aplicacaoPagamento == 8L)
			{
				emailCopia = (String) processEntity.findValue("aplicacaoPagamento.destinatarioDaCopiaOculta");
			}
			if (aplicacaoPagamento == 9L)
			{
				emailCopia = (String) processEntity.findValue("aplicacaoPagamento.destinatarioDaCopiaOculta");
			}
			
			if (emailCopia != null ) {
				StringTokenizer tokenEmailStr = new StringTokenizer(emailCopia, ";");
				while (tokenEmailStr.hasMoreElements())
				{
					String email = (String) tokenEmailStr.nextElement();
					email = email.trim();
					if (email.contains("@")) {
						listaEmailsCopia.add(email);
					}
				}
			}
			
			String textoObjeto = "";
			String textoRelato = "";
			// Manutencao Frota
			if (aplicacaoPagamento == 1L) {
				textoObjeto = "<p><strong>Viatura :</strong> " + (String) processEntity.findValue("viatura.placa");
				textoObjeto = textoObjeto + " - " + (String) processEntity.findValue("viatura.tipoViatura.nome");
				textoObjeto = textoObjeto + " - ";
				if(((String) processEntity.findValue("viatura.anoViatura")) == null)
				{
					textoObjeto = textoObjeto + "Ano não informado";
				}
				else
				{
					textoObjeto = textoObjeto + ((String) processEntity.findValue("viatura.anoViatura"));
				}
				textoObjeto = textoObjeto + " - " + (String) processEntity.findValue("viatura.descricao");
				textoObjeto = textoObjeto + " - " + String.valueOf((Long) processEntity.findValue("laudo.kilometragem")) + "KM";
				textoRelato = (String) processEntity.findValue("laudo.relato") + "</p>";
			} // Manutencao Eletronica  
			else if ((aplicacaoPagamento == 2L) || (aplicacaoPagamento == 3L) || (aplicacaoPagamento == 4L) || 
					 (aplicacaoPagamento == 5L) || (aplicacaoPagamento == 6L) || (aplicacaoPagamento == 7L) ||
					 (aplicacaoPagamento == 8L) || (aplicacaoPagamento == 9L))
			{
				textoRelato = (String) processEntity.findValue("laudoEletronica.relato");					
				textoObjeto = "<p><strong>Conta/Cliente:</strong> " + (String) processEntity.findValue("contaSigma.id_central");
				textoObjeto = textoObjeto + "[" + (String) processEntity.findValue("contaSigma.particao") + "]";
				textoObjeto = textoObjeto + " - " + (String) processEntity.findValue("contaSigma.razao");
				textoObjeto = textoObjeto + " - " + (String) processEntity.findValue("contaSigma.fantasia") + "</p>";
			}
			
			NeoUser solicitante = (NeoUser) processEntity.findValue("solicitanteOrcamento");
			GregorianCalendar dataRaw = (GregorianCalendar) processEntity.findValue("dataSolicitacao");
			StringBuilder html = new StringBuilder();
			html.append("<html>");
			html.append("<center><h1>" + "Solicitação de Orçamento - " + code + "</h1></center>");
			html.append("<br/><br/>");
			html.append("<p><strong>Data:</strong> " +  NeoUtils.safeDateFormat(dataRaw, "dd/MM/yyyy") + "</p>");
			html.append("<p><strong>Solicitante:</strong> " +  solicitante.getFullName() + " (" + solicitante.getEmail() + ")</p>");
			html.append(textoObjeto);
			html.append("<p><strong>Solicitação/Serviço:</strong><br/> " + textoRelato  + "</p>");
			html.append("<br/><br/>");
			html.append("<p>Favor responder este email com o orçamento desta solicitação.</p>");
			html.append("</html>");
			
			List<String> copiaOculta = new ArrayList<String>();
			copiaOculta.add("emailautomatico@orsegups.com.br");
			
			if (!listaEmails.isEmpty())	{
				for (String emailDst : listaEmails)
				{
					OrsegupsUtils.sendEmail2Orsegups(emailDst, "fusion@orsegups.com.br", "Solicitação de Orçamento - " + code, "", html.toString(), emailRemetente, listaEmailsCopia, copiaOculta);
				}
			}			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new WorkflowException(e.getMessage());
		}
	}
}