package com.neomind.fusion.custom.orsegups.eform.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.neomind.fusion.custom.orsegups.eform.vo.Chp1ClienteVO;
import com.neomind.fusion.custom.orsegups.eform.vo.Chp1VO;
import com.neomind.fusion.custom.orsegups.eform.vo.ClienteVO;
import com.neomind.fusion.custom.orsegups.eform.vo.EformVO;

@Path(value = "eform")
public class EformHandler {

    @POST
    @Path("getChp1Empresa")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public List<EformVO> getChp1Empresas() {

	EformController e = new EformController();

	return e.getEmpresas(); 

    } 
    
    @POST
    @Path("getChp1Operadora")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public List<EformVO> getChp1Operadoras() {

	EformController e = new EformController();

	return e.getOperadoras();

    }
    
    @POST
    @Path("contaChp1")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaChp1s() {

	EformController e = new EformController();

	return e.contaChipes();

    }
    
    @POST
    @Path("getChp1")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public List<Chp1VO> getChp1s(@MatrixParam("inicio") String inicio,
	    @MatrixParam("fim") String fim) {

	EformController e = new EformController();

	return e.getChipes(inicio,fim);

    }
    
    @POST
    @Path("getRegional")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public List<EformVO> getRegionais() {

	EformController e = new EformController();

	return e.getRegionais();

    }

    @POST
    @Path("countChips")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public int countChips(@MatrixParam("empresa") String _empresa,
	    @MatrixParam("operadora") String _operadora,
	    @MatrixParam("operadora2") String _operadora2,
	    @MatrixParam("regional") String _regional,
	    @MatrixParam("linha") String _linha,
	    @MatrixParam("emCliente") String _emCliente,
	    @MatrixParam("cancelado") String _cancelado,	    
	    @MatrixParam("emEstoque") String _emEstoque,
	    @MatrixParam("habilitado") String _habilitado,	    
	    @MatrixParam("iccId") String _iccId,
	    @MatrixParam("dataCancelamento") String _dataCancelamento)
    {
	
	EformController e = new EformController();
	
	return e.countChips(!_iccId.equals("undefined")?_iccId:"", 
		!_empresa.equals("undefined")?_empresa:"", 
			!_operadora.equals("undefined")?_operadora:"", 
				!_operadora2.equals("undefined")?_operadora2:"", 
					!_regional.equals("undefined")?_regional:"", 
						!_linha.equals("undefined")?_linha:"",
							!_cancelado.equals("undefined")?_cancelado:"",
								!_habilitado.equals("undefined")?_habilitado:"",
									!_dataCancelamento.equals("undefined")?_dataCancelamento:"",
										!_emCliente.equals("undefined")?_emCliente:"",						
											!_emEstoque.equals("undefined")?_emEstoque:"");						
    }
    
    @POST
    @Path("getChips")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public List<Chp1VO> getChip(
	    @MatrixParam("empresa") String _empresa,
	    @MatrixParam("operadora") String _operadora,
	    @MatrixParam("operadora2") String _operadora2,
	    @MatrixParam("regional") String _regional,
	    @MatrixParam("linha") String _linha,
	    @MatrixParam("emCliente") String _emCliente,
	    @MatrixParam("emEstoque") String _emEstoque,
	    @MatrixParam("dataCancelamento") String _dataCancelamento,
	    @MatrixParam("cancelado") String _cancelado,
	    @MatrixParam("habilitado") String _habilitado,
	    @MatrixParam("iccId") String _iccId,
	    @MatrixParam("pageNumber") String _pageNumber,
	    @MatrixParam("pageResults") String _pageResults) {
	
	EformController e = new EformController();
	
	return e.getChip(!_iccId.equals("undefined")?_iccId:"", 
		!_empresa.equals("undefined")?_empresa:"", 
			!_operadora.equals("undefined")?_operadora:"", 
				!_operadora2.equals("undefined")?_operadora2:"", 
					!_regional.equals("undefined")?_regional:"", 
						!_linha.equals("undefined")?_linha:"",
							!_dataCancelamento.equals("undefined")?_dataCancelamento:"",
								!_cancelado.equals("undefined")?_cancelado:"",
									!_habilitado.equals("undefined")?_habilitado:"",
										!_emCliente.equals("undefined")?_emCliente:"",
											!_emEstoque.equals("undefined")?_emEstoque:"",						
												_pageNumber, _pageResults);

    }
    
    @POST
    @Path("getChipsCliente")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public List<Chp1ClienteVO> getChipCliente(@MatrixParam("empresa") String _empresa,
	    @MatrixParam("instalacao") String _instalacao,
	    @MatrixParam("remocao") String _remocao,
	    @MatrixParam("iccId") String _iccId) {
	
	EformController e = new EformController();
	
	return e.getChipCliente(!_iccId.equals("undefined")?_iccId:"", 
		!_empresa.equals("undefined")?_empresa:"", 
			!_instalacao.equals("undefined")?_instalacao:"", 
				!_remocao.equals("undefined")?_remocao:"");

    }
    
    @POST
    @Path("setChip")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public String setChip(@MatrixParam("empresa") String _empresa,
	    @MatrixParam("operadora") String _operadora,
	    @MatrixParam("operadora2") String _operadora2,
	    @MatrixParam("ativo") String _ativo,
	    @MatrixParam("regional") String _regional,
	    @MatrixParam("linha") String _linha,
	    @MatrixParam("iccId") String _iccId,
	    @MatrixParam("neoId") String _neoId,
	    @MatrixParam("dataCancelamentoFormatada") String _dataCancelamentoFormatada) {
	
	EformController e = new EformController();
	
	return e.setChip(_iccId, _empresa, _operadora, !_operadora2.equals("undefined")?_operadora2:"", !_regional.equals("undefined")?_regional:"", !_linha.equals("undefined")?_linha:"", !_ativo.equals("undefined")?_ativo:"", !_neoId.equals("undefined")?_neoId:"",!_dataCancelamentoFormatada.equals("undefined")?_dataCancelamentoFormatada:"");

    }
    
    @POST
    @Path("getCdCliente")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public List<ClienteVO> getCdCliente(@MatrixParam("CdCliente") String _cliente) {

	EformController e = new EformController();

	return e.getCdCliente(_cliente);

    }
    
    @POST
    @Path("getCliente")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public List<ClienteVO> getCliente(@MatrixParam("cliente") String _cliente) {

	EformController e = new EformController();

	return e.getCliente(_cliente);

    }
    
    @POST
    @Path("verificaChipExistente")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public boolean verificaChipExistente(@MatrixParam("IccId") String _iccId) {

	EformController e = new EformController();

	return e.verificaIccIdExistente(_iccId);

    }    
    
    @POST
    @Path("getChip")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public List<Chp1VO> getChip(@MatrixParam("chip") String _chip) {

	EformController e = new EformController();

	return e.getChip(_chip);

    }
    
    @POST
    @Path("contaChp1Cliente")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaChp1Clientes() {

	EformController e = new EformController();

	return e.contaChipClientes();

    }
    
    @POST
    @Path("getChp1Cliente")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public List<Chp1ClienteVO> getChp1Clientes() {

	EformController e = new EformController();

	return e.getChp1Clientes();

    }
    
    @POST
    @Path("setChp1Cliente")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public String setChp1Cliente(@MatrixParam("cdCLiente") String _cdCliente,
	    @MatrixParam("iccId") String _iccId,
	    @MatrixParam("replicar") String _replicar) {
	
	EformController e = new EformController();
	
	return e.setChp1Cliente(_cdCliente,_iccId,_replicar);

    }
    
    @POST
    @Path("getRegionalRota")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public List<EformVO> getRegionalRota(@MatrixParam("regionalRota") String _regionalRota) {

	EformController e = new EformController();
	return e.getRegionalRota(_regionalRota);
    }
    
    @POST
    @Path("getTecnicoTercerizado")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public List<EformVO> getTecnicoTercerizado(@MatrixParam("codRegionalRota") String _codRegionalRota,
	    @MatrixParam("regionalRota") String _regionalRota,
	    @MatrixParam("codTecnicoTercerizado") String _codTecnicoTercerizado,
	    @MatrixParam("tecnicoTercerizado") String _tecnicoTercerizado) {

	EformController e = new EformController();
	return e.getTecnicoTercerizado(_codRegionalRota,_regionalRota,_codTecnicoTercerizado,_tecnicoTercerizado);
    }
    
    @POST
    @Path("contaTecnicoTercerizado")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaTecnicoTercerizado(@MatrixParam("colaboradores") String _colaboradores,
	    @MatrixParam("textoColaboradores") String _textoColaboradores,
	    @MatrixParam("rotas") String _rotas,
	    @MatrixParam("textoRotas") String _textoRotas) {

	EformController e = new EformController();
	return e.contaTecnicoTercerizado(_colaboradores,_textoColaboradores,_rotas,_textoRotas);
    }
    
    @POST
    @Path("gerarOS")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer gerarOS(@MatrixParam("colaboradores") String _colaboradores,
	    @MatrixParam("textoColaboradores") String _textoColaboradores,
	    @MatrixParam("rotas") String _rotas,
	    @MatrixParam("textoRotas") String _textoRotas,
	    @MatrixParam("qtdeGerar") String _qtdeGerar) {

	EformController e = new EformController();
	return e.gerarOS(_colaboradores,_textoColaboradores,_rotas,_textoRotas,_qtdeGerar);
    }
    
    @POST
    @Path("contaTotalContratado")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaTotalContratado() {

	EformController e = new EformController();

	return e.contaTotalContratado();

    }
    
    @POST
    @Path("contaTotalCancelado")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaTotalCancelado() {

	EformController e = new EformController();

	return e.contaTotalCancelado();

    }
    
    @POST
    @Path("contaTotalAtivo")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaTotalAtivo() {

	EformController e = new EformController();

	return e.contaTotalAtivo();

    }
    
    @POST
    @Path("contaTotalInativo")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaTotalInativo() {

	EformController e = new EformController();

	return e.contaTotalInativo();

    }
    
    @POST
    @Path("contaTotalGprs")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaTotalGprs() {

	EformController e = new EformController();

	return e.contaTotalGprs();

    }
    
    @POST
    @Path("contaTotalContaGprs")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaTotalContaGprs() {

	EformController e = new EformController();

	return e.contaTotalContaGprs();

    }
    
    @POST
    @Path("contaIdentificados")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaIdentificados() {

	EformController e = new EformController();

	return e.contaIdentificados();

    }
    
    @POST
    @Path("contaAguardandoIdentificacao")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaAguardandoIdentificacao() {

	EformController e = new EformController();

	return e.contaAguardandoIdentificacao();

    }
    
    @POST
    @Path("contaNaoIdentificado")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaNaoIdentificado() {

	EformController e = new EformController();

	return e.contaNaoIdentificado();

    }
    
    @POST
    @Path("contaTotalNexti")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaTotalNexti() {

	EformController e = new EformController();

	return e.contaTotalNexti();

    }
    
    @POST
    @Path("contaTotalRastreamento")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaTotalRastreamento() {

	EformController e = new EformController();

	return e.contaTotalRastreamento();

    }
    
    @POST
    @Path("contaTotalDeggy")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaTotalDeggy() {

	EformController e = new EformController();

	return e.contaTotalDeggy();

    }
    
    @POST
    @Path("contaTotalTI")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaTotalTI() {

	EformController e = new EformController();

	return e.contaTotalTI();

    }
    
    @POST
    @Path("contaTotalTIaCancelar")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaTotalTIaCancelar() {

	EformController e = new EformController();

	return e.contaTotalTIaCancelar();

    }
    
    @POST
    @Path("contaTotalRegionais")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaTotalRegionais() {

	EformController e = new EformController();

	return e.contaTotalRegionais();

    }
    
    @POST
    @Path("contaNaoIdentificados")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaNaoIdentificados() {

	EformController e = new EformController();

	return e.contaNaoIdentificados();

    }
    
    @POST
    @Path("totalIdentificado")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer totalIdentificado() {

	EformController e = new EformController();

	return e.totalIdentificado();

    }
    
    @POST
    @Path("totalNaoIdentificado")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer totalNaoIdentificado() {

	EformController e = new EformController();

	return e.totalNaoIdentificado();

    }
    
    @POST
    @Path("contaTotalEncaronada")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaTotalEncaronada() {

	EformController e = new EformController();

	return e.contaTotalEncaronada();

    }
    
    @POST
    @Path("contaTotalVisuVTR")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaTotalVisuVTR() {

	EformController e = new EformController();

	return e.contaTotalVisuVTR();

    }
    
    @POST
    @Path("contaTotalVisuVTRImagem")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaTotalVisuVTRImagem() {

	EformController e = new EformController();

	return e.contaTotalVisuVTRImagem();

    }
    
    @POST
    @Path("contaTotalAC")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaTotalAC() {

	EformController e = new EformController();

	return e.contaTotalAC();

    }
    
    @POST
    @Path("contaTotalFT")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaTotalFT() {

	EformController e = new EformController();

	return e.contaTotalFT();

    }
    
    @POST
    @Path("contaValidacao")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaValidacao() {

	EformController e = new EformController();

	return e.contaValidacao();

    }
    
    @POST
    @Path("contaValidacaoPositiva")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaValidacaoPositiva() {

	EformController e = new EformController();

	return e.contaValidacaoPositiva();

    }
    
    @POST
    @Path("contaValidacaoNegativa")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaValidacaoNegativa() {

	EformController e = new EformController();

	return e.contaValidacaoNegativa();

    }
    
    @POST
    @Path("contaValidacaoAguardando")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaValidacaoAguardando() {

	EformController e = new EformController();

	return e.contaValidacaoAguardando();

    }
    
    @POST
    @Path("contaOSComplementar")
    @Produces("application/json")
    @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Integer contaOSComplementar() {

	EformController e = new EformController();

	return e.contaOSComplementar();

    }
}
