package com.neomind.fusion.custom.orsegups.scheduler;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLGroupFilter;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.scheduler.JobException;
import com.neomind.fusion.scheduler.job.CustomJobAdapter;
import com.neomind.fusion.scheduler.job.CustomJobContext;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.model.ProcessModel;
import com.neomind.fusion.workflow.simulation.WorkflowService;
import com.neomind.util.NeoDateUtils;
import com.neomind.util.NeoUtils;

public class RotinaIniciaFluxoProdutividadeAIT implements CustomJobAdapter
{
	private static final Log log = LogFactory.getLog(RotinaIniciaFluxoProdutividadeAIT.class);

	@Override
	public void execute(CustomJobContext arg0)
	{

		Long key = GregorianCalendar.getInstance().getTimeInMillis();
		try
		{
		    GregorianCalendar dataInicio = new GregorianCalendar();
		    dataInicio.add(Calendar.MONTH, -1); //-1
		    dataInicio.set(Calendar.HOUR_OF_DAY, 0);
		    dataInicio.set(Calendar.MINUTE, 0);
		    dataInicio.set(Calendar.SECOND, 0);
		    dataInicio.set(Calendar.MILLISECOND, 0);
			
			String competencia = NeoDateUtils.safeDateFormat(dataInicio, "MM/yyyy");

			GregorianCalendar prazo = new GregorianCalendar();
			prazo = OrsegupsUtils.getSpecificWorkDay(prazo, 2L);
			prazo.set(Calendar.HOUR_OF_DAY, 23);
			prazo.set(Calendar.MINUTE, 59);
			prazo.set(Calendar.SECOND, 59);

			Map<String, Long> mapReg = OrsegupsUtils.getRegionais();

			String solicitante = OrsegupsUtils.getUserNeoPaper("CPASolicitante");

			for (Map.Entry<String, Long> entry : mapReg.entrySet())
			{
				NeoPaper papel = new NeoPaper();
				Long codReg = OrsegupsUtils.getCodigoRegional(entry.getKey());
				String executor = "";

				papel = OrsegupsUtils.getPapelGerenteRegionalProdutividadeAit(codReg);

				if (papel != null && papel.getAllUsers() != null && !papel.getAllUsers().isEmpty())
				{
					for (NeoUser user : papel.getUsers())
					{
						executor = user.getCode();
						abrirFluxoProdutividade(solicitante, executor, prazo, competencia, entry.getKey());
						break;
					}
				}
				else
				{
					log.error("##### ERRO AGENDADOR DE TAREFA: Abrir Tarefa Simples Produtividade IA2 - Regra: Coordenador responsável não encontrado - Papel " + papel.getName());
					continue;
				}

			}

		}
		catch (Exception e)
		{
			log.error("##### AGENDADOR DE TAREFA: PRODUTIVIDADE [" + key + "]" + e.getMessage());
			e.printStackTrace();
			throw new JobException("Erro ao processar job, procurar no log por:" + key);
		}
		finally
		{

			log.warn("##### FINALIZAR  AGENDADOR DE TAREFA: PRODUTIVIDADE AA SUP FIS e Ordens de Serviço em Excesso - Data: " + NeoDateUtils.safeDateFormat(new GregorianCalendar(), "dd/MM/yyyy"));
		}

	}

	public String abrirFluxoProdutividade(String solicitante, String executor, GregorianCalendar prazo, String competencia, String regional)
	{
		try
		{
			NeoUser objSolicitante = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", solicitante));
			NeoUser objExecutor = (NeoUser) PersistEngine.getObject(NeoUser.class, new QLEqualsFilter("code", executor));

			if (NeoUtils.safeIsNull(objSolicitante))
			{
				return "Erro #1 - Solicitante não encontrado";
			}

			if (NeoUtils.safeIsNull(prazo))
			{
				return "Erro #2 - Prazo não informado";
			}

			QLGroupFilter filter = new QLGroupFilter("AND");
			//TESTAR ESTA CONSULTA PARA VER SE TRAZ APENAS O COLABORADOR DA REGIONAL
			QLRawFilter filterCliente = new QLRawFilter("nmViatura LIKE '" + regional + "%'");
			filter.addFilter(filterCliente);
			QLRawFilter filterTitulo= new QLRawFilter("titulo IS NULL");
			filter.addFilter(filterTitulo);
			filter.addFilter(new QLEqualsFilter("competencia", competencia));
			List<NeoObject> aitObjs = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("CPAProdutividadeAIT"), filter);

			if (aitObjs != null && !aitObjs.isEmpty())
			{
				for (NeoObject neoObject : aitObjs)
				{

					NeoObject noPS = (NeoObject) neoObject;
					EntityWrapper noPSWrapper = new EntityWrapper(noPS);
					noPSWrapper.findField("solicitante").setValue(objSolicitante);
					noPSWrapper.findField("executor").setValue(objExecutor);
					noPSWrapper.findField("titulo").setValue("Produtividade AIT ");
					noPSWrapper.findField("prazo").setValue(prazo);
					
					PersistEngine.persist(noPS);

					final ProcessModel pm = (ProcessModel) PersistEngine.getObject(ProcessModel.class, new QLEqualsFilter("name", "R007 - CPA - Cálculo de Produtividade de AIT"));
					final WFProcess processo = WorkflowService.startProcess(pm, noPS, false, objSolicitante);
					processo.setSaved(true);
					System.out.println(processo.getCode() +" - "+processo.getTitle() +" - "+objExecutor.getCode()); 

				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return "";
	}
}
