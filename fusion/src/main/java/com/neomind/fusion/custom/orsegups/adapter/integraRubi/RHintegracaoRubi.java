package com.neomind.fusion.custom.orsegups.adapter.integraRubi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.rpc.ServiceException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import br.com.senior.services.sm.asoExterno.AsoexternoASOExterno2In;
import br.com.senior.services.sm.asoExterno.AsoexternoASOExterno2Out;
import br.com.senior.services.sm.asoExterno.G5SeniorServicesLocatorAso;
import br.com.senior.services.sm.resultadosExames.G5SeniorServicesLocatorResultExam;
import br.com.senior.services.sm.resultadosExames.ResultadosExamesResultados3In;
import br.com.senior.services.sm.resultadosExames.ResultadosExamesResultados3Out;
import br.com.senior.services.vetorh.constants.WSConfigs;
import br.com.senior.services.vetorh.dependentes.DependentesDependente2In;
import br.com.senior.services.vetorh.dependentes.DependentesDependente2Out;
import br.com.senior.services.vetorh.dependentes.DependentesNascimentoIn;
import br.com.senior.services.vetorh.dependentes.DependentesNascimentoOut;
import br.com.senior.services.vetorh.dependentes.G5SeniorServicesLocatorDep;
import br.com.senior.services.vetorh.ficha.basica.FichaBasicaFichaBasica5In;
import br.com.senior.services.vetorh.ficha.basica.FichaBasicaFichaBasica5Out;
import br.com.senior.services.vetorh.ficha.basica.G5SeniorServicesLocatorFBas;
import br.com.senior.services.vetorh.ficha.complementar.FichaComplementarFichaComplementar7In;
import br.com.senior.services.vetorh.ficha.complementar.FichaComplementarFichaComplementar7Out;
import br.com.senior.services.vetorh.ficha.complementar.G5SeniorServicesLocatorFCom;
import br.com.senior.services.vetorh.historico.G5SeniorServicesLocatorFPHis;
import br.com.senior.services.vetorh.historico.HistoricosAdicional2In;
import br.com.senior.services.vetorh.historico.HistoricosAdicional2Out;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.custom.orsegups.utils.OrsegupsUtils;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoStorage;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.adapter.AdapterUtils;
import com.neomind.fusion.workflow.event.ActivityEvent;
import com.neomind.fusion.workflow.event.ActivityStartEventListener;
import com.neomind.fusion.workflow.event.TaskAssignerEvent;
import com.neomind.fusion.workflow.event.TaskAssignerEventListener;
import com.neomind.fusion.workflow.event.TaskTransferEvent;
import com.neomind.fusion.workflow.event.TaskTransferEventListener;
import com.neomind.fusion.workflow.exception.ActivityException;
import com.neomind.fusion.workflow.exception.TaskException;
import com.neomind.fusion.workflow.exception.WorkflowException;
import com.neomind.util.NeoUtils;

public class RHintegracaoRubi implements AdapterInterface, ActivityStartEventListener, TaskAssignerEventListener, TaskTransferEventListener
{
	@SuppressWarnings("unused")
	private static final Log log = LogFactory.getLog(RHintegracaoRubi.class);

	public void start(Task origin, EntityWrapper processEntity, Activity activity) //throws RemoteException, ServiceException
	{
		WSConfigs config = new WSConfigs();
		EntityWrapper pEntity = processEntity;

		NeoObject eform = pEntity.getObject();

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYY hh:mm:ss");

		///////////////////////////////////////////////////////////////////////////////////////////////////////

		FichaBasicaFichaBasica5Out integrarFBasOut = new FichaBasicaFichaBasica5Out();
		FichaBasicaFichaBasica5In integrarFBasIn = new FichaBasicaFichaBasica5In();

		//Criando Entradas e Saídas da Ficha Complementar
		FichaComplementarFichaComplementar7Out integrarFComOut = new FichaComplementarFichaComplementar7Out();
		FichaComplementarFichaComplementar7In integrarFComIn = new FichaComplementarFichaComplementar7In();

		//Criando Entradas e Saídas dos Adicionais
		HistoricosAdicional2Out integrarAdiOut = new HistoricosAdicional2Out();
		HistoricosAdicional2In integrarAdiIn = new HistoricosAdicional2In();

		//Criando Entradas e Saídas dos Dependentes
		DependentesDependente2Out integrarDepOut = new DependentesDependente2Out();
		DependentesDependente2In integrarDepIn = new DependentesDependente2In();

		DependentesNascimentoOut integrarDepNasOut = new DependentesNascimentoOut();
		DependentesNascimentoIn integrarDepNasIn = new DependentesNascimentoIn();

		AsoexternoASOExterno2Out integrarASOOut = new AsoexternoASOExterno2Out();
		AsoexternoASOExterno2In integrarASOIn = new AsoexternoASOExterno2In();

		ResultadosExamesResultados3Out integrarResultExamOut = new ResultadosExamesResultados3Out();
		ResultadosExamesResultados3In integrarResultExamIn = new ResultadosExamesResultados3In();

		///////////////////////////////////////////////////////////////////////////////////////////////////////

		NeoUser userOrigin = origin.getActivity().getInstance().getOwner();

		String caminhoPadrao = null;
		//$%
		List<NeoObject> listaCaminhoPadrao = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("rhDiretorioDocs"), new QLRawFilter("tipoDoc like 'padrao'"));

		if (listaCaminhoPadrao.size() > 0)
		{

			for (NeoObject dir : listaCaminhoPadrao)
			{

				EntityWrapper wDir = new EntityWrapper(dir);

				caminhoPadrao = wDir.findGenericValue("diretorio");

			}

		}

		String codeUsu = userOrigin.getCode();

		String erroWF = null;

		Integer contErros = 0;

		Boolean integra = Boolean.FALSE;

		String erroBas = null;

		String erroCom = null;

		String erroAdi = null;

		String erroDep = null;

		String erroDepNas = null;

		String erroASO = null;

		String erroResultExam = null;

		Integer contFBas = 0;

		Integer contFCom = 0;

		Integer contFHis = 0;

		Integer numcad = null;

		Long numL = pEntity.findGenericValue("matricula");

		String chaveCodigo = activity.getProcess().getCode();

		//Chaves de Registro das Integrações
		String chaveFBas = "FBas" + chaveCodigo; //chave da Ficha Básica

		String chaveFCom = "FCom" + chaveCodigo; //chave da Ficha Complementar

		String chaveAdic = "Adic" + chaveCodigo; //chave do Adicionais

		String chaveDepe = "Depe" + chaveCodigo; //chave do Dependentes

		String chaveDNas = "DNas" + chaveCodigo; //chave do Dependentes Nascimento

		String chaveASO = "ASO" + chaveCodigo; //chave da ASO

		String chaveResultExam = "ResultExam" + chaveCodigo; //chave do Resultado do Exame

		GregorianCalendar dataAtual = new GregorianCalendar();

		dataAtual.add(Calendar.MONTH, -1);

		String apefun = "#I" + chaveCodigo;

		pEntity.findField("apefun").setValue(apefun);

		String tipope = "I"; //Tipo Operação - SEMPRE "I"

		SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy");

		SimpleDateFormat horaDF = new SimpleDateFormat("HH:mm");

		String sHoraAtual = horaDF.format(dataAtual.getTime());

		String sHora = sHoraAtual.substring(0, 2);

		String sMinuto = sHoraAtual.substring(3, 5);

		Integer iHora = Integer.parseInt(sHora);

		Integer iMinuto = Integer.parseInt(sMinuto);

		Integer iHorInc = (iHora * 60) + iMinuto;

		//String horinc = iHorInc.toString().trim();

		String horinc = "534";

		Long tipadmcolab = pEntity.findGenericValue("tpAdmColab.codigo");

		Long depemp = null;

		if (tipadmcolab == 2)
		{
			depemp = 1l;
		}
		else
		{
			depemp = pEntity.findGenericValue("departamento.codigo");
		}

		String colabsede = pEntity.findGenericValue("colabSede.codigo");

		String valcom = pEntity.findGenericValue("auxVC.codigo");

		Long numemp = pEntity.findGenericValue("emp.numemp"); //Número Empresa

		Long tipcol = pEntity.findGenericValue("tpColaborador.codigo"); //Tipo de Colaborador

		String numcpf = pEntity.findGenericValue("numcpf"); //Número do CPF

		Long estciv = pEntity.findGenericValue("estadoCivil.codigo"); //Estado Civil

		if (NeoUtils.safeIsNotNull(pEntity))
		{

			/**
			 * INÍCIO FichaBasicaFichaBasica5.
			 */

			//Início Customizados
			tipadmcolab = pEntity.findGenericValue("tpAdmColab.codigo");

			if (tipadmcolab == 2)
			{
				depemp = 1l;
			}
			else
			{
				depemp = pEntity.findGenericValue("departamento.codigo");
			}

			colabsede = pEntity.findGenericValue("colabSede.codigo");

			valcom = pEntity.findGenericValue("auxVC.codigo");
			//Fim Customizados

			numemp = pEntity.findGenericValue("emp.numemp"); //Número Empresa

			String nomfun = pEntity.findGenericValue("colaborador");

			String nomcom = pEntity.findGenericValue("nomCompletoFC"); //Nome Completo

			/*
			 * Integer caractNomCompleto = nomcom.length();
			 * if(caractNomCompleto > 40 && caractNomCompleto <= 80){
			 * nomfun = nomcom.replace(' ',';');
			 * String nomePedacos[] = nomfun.split(";");
			 * int k;
			 * String nomSaida="";
			 * for(k=0 ; k<nomePedacos.length ; k++)
			 * {
			 * if(k == 0 || k == (nomePedacos.length-1))
			 * nomSaida = nomSaida + nomePedacos[k];
			 * else
			 * nomSaida = nomSaida + " " + nomePedacos[k].charAt(0) + " ";
			 * }
			 * nomfun = nomSaida;
			 * }else if(caractNomCompleto <= 40){
			 * nomfun = nomcom;
			 * }
			 */

			String nomcra = pEntity.findGenericValue("nomeCracha"); //Nome do crachá

			List<NeoObject> listaVT = pEntity.findGenericValue("listaVT");

			GregorianCalendar datadmCalendar = pEntity.findGenericValue("dtAdmissao");

			String dtFimEvt = "31/12/1900";

			String dtadmissao = SDF.format(datadmCalendar.getTime()); //Data de Admissão

			Long tipadm = pEntity.findGenericValue("tpAdmissao.codigo"); //Tipo de Admissão

			Long apupon = pEntity.findGenericValue("tpApuracao.codigo"); //Tipo de Apuração

			Long codvinhvi = pEntity.findGenericValue("tpVinculo.codvin"); //Tipo de Vínculo

			//Regra do sindicato para colaboradores administrativos ou operacionais
			Long codsin = null;

			Long codfil = null;

			String numloc = null;

			if (tipadmcolab == 1)
			{

				if (depemp == 2)
				{

					codsin = pEntity.findGenericValue("sindiAdmSede.sindicato.codsin"); //Código do Sindicato

					codfil = pEntity.findGenericValue("codfil.codfil"); //Código da Filial

					numloc = pEntity.findGenericValue("localadm.codloc");

				}
				else if (depemp > 0 && depemp < 31)
				{

					codsin = pEntity.findGenericValue("sindicatoAdm.sindicato.codsin"); //Código do Sindicato

					codfil = pEntity.findGenericValue("codfilOperacional.codfil"); //Código da Filial

					numloc = pEntity.findGenericValue("localadm.codloc");

				}
				else
				{

					codsin = pEntity.findGenericValue("sindicato.sindicato.codsin"); //Código do Sindicato

					codfil = pEntity.findGenericValue("codfilOperacional.codfil"); //Código da Filial

					numloc = pEntity.findGenericValue("localadm.codloc");

				}

			}
			else if (tipadmcolab == 2)
			{

				codsin = pEntity.findGenericValue("sindicato.sindicato.codsin"); //Código do Sindicato

				if (codsin == null)
				{

					codsin = pEntity.findGenericValue("sindicatoAdm.sindicato.codsin");

					if (codsin == null)
					{

						codsin = pEntity.findGenericValue("sindicato1.sindicato.codsin");

						if (codsin == null)
						{

							codsin = pEntity.findGenericValue("sindicatoAdm1.sindicato.codsin");

						}

					}

				}

				codfil = pEntity.findGenericValue("codfilOperacional.codfil"); //Código da Filial

				numloc = pEntity.findGenericValue("loca.codloc");

			}

			if (codfil == null)

				if (numloc == null)

					numloc.trim();

			Long sitafa = pEntity.findGenericValue("situacao"); //Situação

			String codcar = pEntity.findGenericValue("cargos.codcar"); //Código do Cargo

			String tipcar = pEntity.findGenericValue("cargos.usu_tipcar");

			Long tipcon = pEntity.findGenericValue("tpContrato.codigo"); //Tipo de Contrato

			Long tipsal = pEntity.findGenericValue("tpSalario.codigo"); //Tipo de Salário

			Boolean editarSalario = pEntity.findGenericValue("editsalario");

			BigDecimal valsal = pEntity.findGenericValue("valorSalario.valor"); //Valor do Salário

			if (valsal == null || editarSalario)
			{

				valsal = pEntity.findGenericValue("valorSalarioPerso"); //Valor do Salário

			}

			Long indadm = pEntity.findGenericValue("indAdmissao"); //Indicador da Admissão

			Long admeso = pEntity.findGenericValue("tpAdmeSocial"); //Tipo de Admissão eSocial

			Long cateso = pEntity.findGenericValue("cateSocial.codigo"); //Categoria do eSocial 

			String codccu = null;

			//busca de centro de custo depende do tipo de admissão do colaborador
			if (tipadmcolab == 1)
			{

				codccu = pEntity.findGenericValue("ccuAdm.codccu");

			}
			else if (tipadmcolab == 2)
			{

				codccu = pEntity.findGenericValue("ccuOpe.codccu");

			}

			Long numctp = pEntity.findGenericValue("numCartTrabalho"); //Número da Carteira de Trabalho

			String digcar = pEntity.findGenericValue("digCartTrabalho"); //Número da Carteira de Trabalho

			GregorianCalendar gcDtCartTrabalho = pEntity.findGenericValue("dtCartTrabalho");

			String dexctp = SDF.format(gcDtCartTrabalho.getTime()); //Data da Carteira de Trabalho

			String pagsin = pEntity.findGenericValue("contribSindical.codigo"); //Contribuição Sindical

			String serctp = pEntity.findGenericValue("serCartTrabalho"); //Série da Carteira de Trabalho

			Long codmothca = pEntity.findGenericValue("motivo.codmot"); //Código do Motivo de Alteração

			String modpag = pEntity.findGenericValue("modPagamento.codigo"); //Modo de Pagamento

			Long codage = null;

			Long codban = null;

			Long conban = null;

			String digconS = null;

			Long tpctba = null;

			if (modpag.equals("R"))
			{

				codage = pEntity.findGenericValue("agenciaConta.codage");

				codban = pEntity.findGenericValue("bancoConta.codban");

				conban = pEntity.findGenericValue("conban");

				digconS = pEntity.findGenericValue("digcontaS");

				tpctba = pEntity.findGenericValue("tpConta.codigo");

			}

			String socsinhsi = pEntity.findGenericValue("sindicalizados.codigo"); //Sindicalizados

			String estctp = pEntity.findGenericValue("ufCarteTrabalho.codest"); //UF da carteira de trabalho

			numcpf = numcpf.replace(".", "");

			numcpf = numcpf.replace("-", "");

			String tipsex = pEntity.findGenericValue("sexo.sigla"); //Sexo

			Long numpis = pEntity.findGenericValue("pis"); //Número do PIS

			Long grains = pEntity.findGenericValue("grauInstrucao.grains"); //Grau de Instrução

			String tipopc = pEntity.findGenericValue("optFGTS.codigo"); //Optante FGTS

			GregorianCalendar datnasCalendar = pEntity.findGenericValue("dtNascimento");

			String datnas = SDF.format(datnasCalendar.getTime()); //Data de Nascimento

			String perpag = pEntity.findGenericValue("periodoPgto.cd"); //Período de Pagamento

			Long codnac = 10l; //Código da Nacionalidade

			String recadi = pEntity.findGenericValue("recebAdtoSal.codigo"); //Recebe Adiantamento do Salário

			String rec13s = pEntity.findGenericValue("receb13Sal.codigo"); //Recebe 13º

			String lisrai = pEntity.findGenericValue("constaRais.codigo"); //Consta na RAIS

			Long codesc = pEntity.findGenericValue("escalaTrabalho.codesc"); //Código da Escala

			String emicar = pEntity.findGenericValue("emitirPonto.codigo"); //Emitir Cartão Ponto

			String deffis = pEntity.findGenericValue("deficiente.codigo"); //Deficiente Físico

			String benrea = pEntity.findGenericValue("benrea.codigo"); //Beneficiente Reabilitado

			Long raccor = pEntity.findGenericValue("racaCor.codigo"); //Raça Cor

			Long catsef = pEntity.findGenericValue("catSefip.codigo"); //Categoria do Sefip

			Long coddef = null;

			Boolean usavt = pEntity.findGenericValue("usaVT");

			Long escvtr = null;

			String deficiente = pEntity.findGenericValue("deficiente.codigo");

			if (deficiente != null && deficiente.equals("S"))
				coddef = pEntity.findGenericValue("deficiencia.codigo");

			Long fisres = pEntity.findGenericValue("fiscRespon.usu_fisres");

			Boolean existeSelecao = pEntity.findGenericValue("existeSelecao");

			if (valcom != null && valcom.equals("S"))
			{

				escvtr = pEntity.findGenericValue("valauxVT.escvtr"); //Valor para Auxílio Combustível		

			}
			else if (usavt)
			{

				escvtr = pEntity.findGenericValue("listaVT.escalaEVT.escvtr"); //Escala do Vale Transporte

			}

			//INÍCIO DO IN					
			integrarFBasIn.setTipOpe(tipope.toString());
			integrarFBasIn.setNumEmp(numemp.intValue());
			integrarFBasIn.setAciTraAfa(null);
			integrarFBasIn.setAdmAnt(null);
			integrarFBasIn.setAdmeSo(admeso.intValue());
			integrarFBasIn.setApeFun(apefun.toString());
			integrarFBasIn.setTipCol(tipcol.intValue());
			integrarFBasIn.setApuPon(apupon.intValue());
			integrarFBasIn.setApuPonApu(apupon.intValue());
			integrarFBasIn.setAssPpr(null);
			integrarFBasIn.setBenRea(benrea);
			integrarFBasIn.setBusHor(null);
			integrarFBasIn.setCadFol(null);
			integrarFBasIn.setCarVagHca(null);
			integrarFBasIn.setCatSef(catsef.intValue());
			integrarFBasIn.setCateSo(cateso.intValue());
			integrarFBasIn.setCauDemAfa(null);
			integrarFBasIn.setClaSalHsa(null);
			integrarFBasIn.setCnpjAn(null);
			if (modpag.equals("R"))
			{

				integrarFBasIn.setCodAge(codage.intValue());
				integrarFBasIn.setCodBan(codban.intValue());
				integrarFBasIn.setConBan(conban.doubleValue());
				integrarFBasIn.setTpCtBa(tpctba.intValue());
				integrarFBasIn.setDigBan(digconS);

			}
			else
			{

				integrarFBasIn.setCodAge(null);
				integrarFBasIn.setCodBan(null);
				integrarFBasIn.setTpCtBa(null);
				integrarFBasIn.setConBan(null);
				integrarFBasIn.setDigBan(null);

			}

			integrarFBasIn.setCodAteAfa(null);
			integrarFBasIn.setCodCar(codcar);
			integrarFBasIn.setCodCat(null);
			integrarFBasIn.setCodCcu(codccu);
			integrarFBasIn.setCodCha(null);

			integrarFBasIn.setCodDoeAfa(null);
			//integrarFBasIn.setCodEqp(null);
			integrarFBasIn.setCodEsc(codesc.intValue());
			//integrarFBasIn.setCodEstHsa(1);
			//integrarFBasIn.setCodEtb(null);					
			integrarFBasIn.setCodFicFmd(null);
			integrarFBasIn.setCodFil(codfil.intValue());
			integrarFBasIn.setCodFor(null);
			integrarFBasIn.setCodIdn(null);
			integrarFBasIn.setCodMotHca((codmothca != null) ? codmothca.intValue() : null);
			integrarFBasIn.setCodMotHsa((codmothca != null) ? codmothca.intValue() : null);
			integrarFBasIn.setCodNac(codnac.intValue());
			integrarFBasIn.setCodPro(null);
			if (codsin == null)
			{

				new WorkflowException("Código do Sindicato Nulo!");

			}
			else
			{

				integrarFBasIn.setCodSin(codsin.intValue());
				integrarFBasIn.setCodSinHsi(codsin.intValue());

			}
			integrarFBasIn.setCodTap(null);
			integrarFBasIn.setCodTma(1);
			integrarFBasIn.setCodTmaHes(1);
			integrarFBasIn.setCodVinHvi(codvinhvi.intValue());
			integrarFBasIn.setConFgt(null);
			integrarFBasIn.setConFinCcu(null);
			//integrarFBasIn.setConRho(conrho);
			integrarFBasIn.setConTosHlo(null);
			integrarFBasIn.setConTovAfa(null);
			integrarFBasIn.setConTovHfi(null);
			integrarFBasIn.setConTovHlo(null);
			integrarFBasIn.setCotDef(deffis);
			integrarFBasIn.setCplEstHsa(null);
			integrarFBasIn.setCplSalHsa(null);
			integrarFBasIn.setDatAdm(dtadmissao);
			integrarFBasIn.setDatAfaAfa(null);
			integrarFBasIn.setDatAltCcu(null);
			integrarFBasIn.setDatAltHca(null);
			integrarFBasIn.setDatAltHcs(null);
			integrarFBasIn.setDatAltHes(null);
			integrarFBasIn.setDatAltHfi(null);
			integrarFBasIn.setDatAltHlo(null);
			integrarFBasIn.setDatAltHor(null);
			integrarFBasIn.setDatApo(null);
			integrarFBasIn.setDatInc(null);
			integrarFBasIn.setDatNas(datnas);
			integrarFBasIn.setDatOpc(null);
			integrarFBasIn.setDatTerAfa(null);
			integrarFBasIn.setDcdPis(null);
			integrarFBasIn.setDepIrf(null);
			integrarFBasIn.setDepSaf(null);
			integrarFBasIn.setDexCtp(dexctp.toString());
			integrarFBasIn.setDiaJusAfa(null);
			;
			if (digcar == null)
				integrarFBasIn.setDigCar(null);
			else
				integrarFBasIn.setDigCar(digcar.toString());

			integrarFBasIn.setDocEst(null);
			integrarFBasIn.setEmiCar(emicar);

			if (escvtr != null)
				integrarFBasIn.setEscVtr(escvtr.intValue());
			else
				integrarFBasIn.setEscVtr(null);

			integrarFBasIn.setEstCiv((estciv != null) ? estciv.intValue() : null);
			integrarFBasIn.setEstConAfa(null);
			integrarFBasIn.setEstCtp(estctp);
			integrarFBasIn.setExmRetAfa(null);
			integrarFBasIn.setFimEtbHeb(null);
			integrarFBasIn.setFimEvt(dtFimEvt);
			integrarFBasIn.setFlowInstanceID(null);
			integrarFBasIn.setFlowName(null);
			integrarFBasIn.setGraIns(grains.intValue());
			integrarFBasIn.setHorAfaAfa(null);
			integrarFBasIn.setHorBas(null);
			integrarFBasIn.setHorDsrHor(null);
			integrarFBasIn.setHorInc(horinc);
			integrarFBasIn.setHorSabHor(null);
			integrarFBasIn.setHorSemHor(null);
			integrarFBasIn.setHorTerAfa(null);
			integrarFBasIn.setIndAdm((indadm != null) ? indadm.intValue() : null);
			integrarFBasIn.setIniAtu(null);
			integrarFBasIn.setIniEtbHeb(null);
			integrarFBasIn.setIniEvt(null);
			integrarFBasIn.setInsCur(null);
			integrarFBasIn.setIrrIse(null);
			integrarFBasIn.setLisRai(lisrai);
			integrarFBasIn.setLocTraHlo(null);
			integrarFBasIn.setMatAnt(null);
			integrarFBasIn.setModPag(modpag);
			integrarFBasIn.setMoeEstHsa(null);
			integrarFBasIn.setMotPos(null);
			integrarFBasIn.setMovSef(null);
			integrarFBasIn.setNatDesHna(null);
			integrarFBasIn.setNivSalHsa(null);
			integrarFBasIn.setNomAteAfa(null);
			integrarFBasIn.setNomFun(nomfun.toString());
			integrarFBasIn.setNumCad(0);
			integrarFBasIn.setNumCpf(numcpf);
			integrarFBasIn.setNumCtp(numctp.intValue());
			integrarFBasIn.setNumLoc(numloc);
			integrarFBasIn.setNumPis(numpis.doubleValue());
			integrarFBasIn.setObsAfaAfa(null);
			integrarFBasIn.setObsFimHeb(null);
			integrarFBasIn.setObsIniHeb(null);
			integrarFBasIn.setOnuSce(null);
			integrarFBasIn.setOpcCes(null);
			integrarFBasIn.setOrgClaAfa(null);
			integrarFBasIn.setPagSin(pagsin);
			integrarFBasIn.setPerJur(null);
			integrarFBasIn.setPerPag(perpag);
			integrarFBasIn.setPonEmb(null);
			integrarFBasIn.setPosObs(null);
			integrarFBasIn.setPosTra(null);
			integrarFBasIn.setPrvTerAfa(null);
			integrarFBasIn.setQhrAfaAfa(null);
			integrarFBasIn.setRacCor(raccor.intValue());
			integrarFBasIn.setRatEve("S");
			integrarFBasIn.setReaRes(null);
			integrarFBasIn.setRec13S(rec13s);
			integrarFBasIn.setRecAdi(recadi);
			integrarFBasIn.setRecGra(null);
			integrarFBasIn.setRegConAfa(null);
			integrarFBasIn.setResOnu(null);
			integrarFBasIn.setSalEstHsa(null);
			integrarFBasIn.setSerCtp(serctp);
			integrarFBasIn.setSisCes(null);
			integrarFBasIn.setSitAfa(sitafa.intValue());
			integrarFBasIn.setSocSinHsi(socsinhsi);
			integrarFBasIn.setTipAdmHfi(tipadm.intValue());
			//integrarFBasIn.setTipAdm(tipadm);
			integrarFBasIn.setTipApo(0);
			integrarFBasIn.setTipCon(tipcon.intValue());
			integrarFBasIn.setTipOpc(tipopc);
			//integrarFBasIn.setTipSal(tipsal.intValue());
			integrarFBasIn.setTipSalHsa(tipsal.intValue());
			integrarFBasIn.setTipSex(tipsex);
			integrarFBasIn.setTurInt(null);
			integrarFBasIn.setValSalHsa(valsal.doubleValue());

			integrarFBasIn.setVerInt(null);

			if (deficiente != null && deficiente.equals("S"))
			{

				integrarFBasIn.setCodDef(coddef.intValue());

				integrarFBasIn.setDefFis(deffis.toString());

			}
			else
			{

				integrarFBasIn.setCodDef(null);

				integrarFBasIn.setDefFis((deffis != null) ? deffis.toString() : null);
			}

			//Início customizados
			integrarFBasIn.setUSU_ColSde(colabsede.toString());
			integrarFBasIn.setUSU_CsgBrc("N");//Evandro mencionou que na contratação sempre é "N"
			integrarFBasIn.setUSU_DepEmp(depemp.intValue());
			integrarFBasIn.setUSU_TipAdm(tipadmcolab.intValue());
			integrarFBasIn.setUSU_ValCom((valcom != null) ? valcom.toString() : null);
			integrarFBasIn.setUSU_FisRes(fisres.intValue());
			//codusu deverá ser inserido um usuário específico pra classificar como integração
			//fim customizados

			/**
			 * TÉRMINO FichaBasicaFichaBasica5.
			 */

			/**
			 * INÍCIO FichaComplementar6.
			 */

			//numemp já tem na FBas
			//tipope já tem na FBas
			//tipcol já tem na FBas
			//numcad gerado no ato da FBas

			String tiplgr = null;

			String endrua = null;

			Long endnum = null;

			String endcpl = null;

			Long codpai = 1l;

			String codest = null;

			Long codcid = null;

			Long codbai = null;

			Long endcep = pEntity.findGenericValue("cep");

			Boolean endManual = pEntity.findGenericValue("informaendereco");

			if (!endManual)
			{//AUTOMÁTICO

				tiplgr = pEntity.findGenericValue("tpLogradouro.codigo");

				endrua = pEntity.findGenericValue("ruacolab.nomrua");

				endnum = pEntity.findGenericValue("endnum");

				endcpl = pEntity.findGenericValue("complEnd");

				codest = pEntity.findGenericValue("estado.codest");

				codcid = pEntity.findGenericValue("cidcolab.codcid");

				codbai = pEntity.findGenericValue("bairro.codbai");

			}
			else
			{//MANUAL

				tiplgr = pEntity.findGenericValue("logracolab.codigo");

				endrua = pEntity.findGenericValue("ruacolabo");

				endnum = pEntity.findGenericValue("endnum");

				endcpl = pEntity.findGenericValue("complEnd");

				codest = pEntity.findGenericValue("estadocolab.codest");

				codcid = pEntity.findGenericValue("cidade.codcid");

				codbai = pEntity.findGenericValue("bairrocolab.codbai");

			}

			Long dditel = 55l;

			Long numddi2 = 55l;

			String numtel = pEntity.findGenericValue("telefone");

			Integer dddtel = Integer.parseInt(numtel.substring(1, 3));

			String primDig01 = numtel.substring(5, 6);

			String numtel2 = null;

			Integer numddd2 = null;

			String primDig02 = null;

			if (primDig01.equals("9") && numtel != null)
				numtel = numtel.substring(4, 15).replaceAll("-", "").trim();
			else if (!primDig01.equals("9") && numtel != null)
			{
				numtel = numtel.substring(4, 14).replaceAll("-", "").trim();

				numtel2 = pEntity.findGenericValue("telefone2");

				if (numtel2 != null && !numtel2.equals(""))
				{

					numddd2 = Integer.parseInt(numtel2.substring(1, 3));

					primDig02 = numtel2.substring(5, 6);

					if (primDig02.equals("9") && numtel2 != null)
						numtel2 = numtel2.substring(4, 15).replaceAll("-", "").trim();
					else if (!primDig02.equals("9") && numtel2 != null)
						numtel2 = numtel2.substring(4, 14).replaceAll("-", "").trim();

				}
			}

			Long painas = 1l;

			String estnas = pEntity.findGenericValue("estNasc.codest");

			Long ccinas = pEntity.findGenericValue("cidNasc.codcid");

			String numele = "" + pEntity.findGenericValue("numEleitorS");

			String zonele = pEntity.findGenericValue("zonaEleitorS");

			String secele = pEntity.findGenericValue("sessaoEleitorS");

			//VALIDAÇÃO CNH
			String exigeCNH = pEntity.findGenericValue("exigCNH.codigo");

			String numcnh = null;

			String orgcnh = null;

			String estcnh = null;

			String catcnh = null;

			GregorianCalendar GCdatcnh = null;

			String datcnh = null;

			GregorianCalendar GCvencnh = null;

			String vencnh = null;

			if (exigeCNH != null && exigeCNH.equals("S"))
			{

				Long numcnhL = pEntity.findGenericValue("numHabilitacao");

				numcnh = String.valueOf(numcnhL);

				orgcnh = pEntity.findGenericValue("orgHabilitacao.codigo");

				estcnh = pEntity.findGenericValue("estCNH.codest");

				catcnh = pEntity.findGenericValue("catHabilitacao.codigo");

				GCdatcnh = pEntity.findGenericValue("dtExpedicao");

				datcnh = SDF.format(GCdatcnh.getTime());

				GCvencnh = pEntity.findGenericValue("dtValidHabilit");

				vencnh = SDF.format(GCvencnh.getTime());

			}

			String numres = pEntity.findGenericValue("numres");

			String catres = pEntity.findGenericValue("catres");

			Long durcon = pEntity.findGenericValue("duracaoContrato");

			Long procon = pEntity.findGenericValue("prorrogContrato");

			GregorianCalendar GCultexm = pEntity.findGenericValue("ultExameMedic");

			String ultexm = SDF.format(GCultexm.getTime());

			//GregorianCalendar gDexCid = pEntity.findGenericValue("dtExp");

			//String dexCid = SDF.format(gDexCid.getTime());

			//String estCid = pEntity.findGenericValue("estExp.codest");

			//String emiCid = pEntity.findGenericValue("orgExp.descricao");

			String numCid = pEntity.findGenericValue("numIdentRG");

			//INÍCIO In
			integrarFComIn.setNumEmp(numemp.intValue());
			integrarFComIn.setTipOpe(tipope);
			integrarFComIn.setTipCol(tipcol.intValue());
			integrarFComIn.setEndCep(endcep.intValue());
			integrarFComIn.setTipLgr(tiplgr);
			integrarFComIn.setEndRua(endrua);
			integrarFComIn.setEndNum(endnum.intValue());
			integrarFComIn.setEndRua(endrua);
			integrarFComIn.setEndCpl(endcpl);
			integrarFComIn.setCodPai(codpai.intValue());
			integrarFComIn.setCodEst(codest);
			integrarFComIn.setCodCid(codcid.intValue());
			integrarFComIn.setCodBai(codbai.intValue());
			integrarFComIn.setDdiTel(dditel.intValue());
			integrarFComIn.setDddTel(dddtel);
			integrarFComIn.setNumTel(numtel);

			//RG
			integrarFComIn.setNumCid(numCid);
			//integrarFComIn.setEmiCid(emiCid);
			//integrarFComIn.setEstCid(estCid);
			//integrarFComIn.setDexCid(dexCid);

			integrarFComIn.setNumDdi2(numddi2.intValue());
			integrarFComIn.setNumDdd2(numddd2);
			integrarFComIn.setNumTel2(numtel2);
			integrarFComIn.setPaiNas(painas.intValue());
			integrarFComIn.setEstNas(estnas);
			integrarFComIn.setCciNas(ccinas.intValue());
			integrarFComIn.setNumEle(numele);
			integrarFComIn.setZonEle(zonele);
			integrarFComIn.setSecEle(secele);
			integrarFComIn.setNumCnh(numcnh);
			integrarFComIn.setOrgCnh(orgcnh);
			integrarFComIn.setEstCnh(estcnh);
			integrarFComIn.setCatCnh(catcnh);
			integrarFComIn.setDatCnh(datcnh);
			integrarFComIn.setVenCnh(vencnh);
			integrarFComIn.setNumRes(numres);
			integrarFComIn.setCatRes(catres);
			integrarFComIn.setDurCon(durcon.intValue());
			integrarFComIn.setProCon(procon.intValue());
			integrarFComIn.setUltExm(ultexm);
			integrarFComIn.setNomCom(nomcom);
			//integrarFComIn.setNifExt("1");
			integrarFComIn.setClaAss("N");

			/**
			 * TÉRMINO FichaComplementar6.
			 */

			/**
			 * INÍCIO Adicionais.
			 */

			Boolean existeAdicional = pEntity.findGenericValue("exisAdicional");

			BigDecimal perins = null;

			BigDecimal perper = null;

			if (existeAdicional)
			{

				perins = pEntity.findGenericValue("perInsalubridade");

				perper = pEntity.findGenericValue("perPericulosidade");

			}
			if (existeSelecao)
			{

				perins = pEntity.findGenericValue("perInsalubridadeSele");

			}

			Boolean InsereAdicional = Boolean.FALSE;

			if (perins != null && perins.doubleValue() > 0)
				InsereAdicional = Boolean.TRUE;

			integrarAdiIn.setNumEmp(numemp.intValue());
			integrarAdiIn.setTipCol(tipcol.intValue());
			integrarAdiIn.setTipOpe(tipope);
			integrarAdiIn.setDatAlt(dtadmissao);

			if (perins != null)
				integrarAdiIn.setPerIns(perins.doubleValue());
			else
				integrarAdiIn.setPerIns(null);

			if (perper != null)
				integrarAdiIn.setPerPer(perper.doubleValue());

			/**
			 * TÉRMINO Adicionais.
			 */

			//INTEGRAÇÃO FBas
			List<NeoObject> registroFBas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("rhRegistroIntegracao"), new QLRawFilter("registro like '" + chaveFBas + "'"));

			if (registroFBas.size() == 0)
			{

				//LOCATOR FBas
				G5SeniorServicesLocatorFBas glBas = new G5SeniorServicesLocatorFBas();

				try
				{
					log.info("INTEGRACAO FichaMedica :" + chaveCodigo + " - INICIO: " + sdf.format(new GregorianCalendar().getTime()));
					integrarFBasOut = glBas.getrubi_Synccom_senior_g5_rh_fp_fichaBasicaPort().fichaBasica_5(config.getUserService(), config.getPasswordService(), 0, integrarFBasIn);
					log.info("INTEGRACAO FichaMedica :" + chaveCodigo + " - FIM: " + sdf.format(new GregorianCalendar().getTime()));
				}
				catch (RemoteException | ServiceException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				erroBas = integrarFBasOut.getErroExecucao();

				log.info(erroBas + " - " + nomfun);

				if (erroBas != null && !erroBas.contains("ficha médica") && !erroBas.contains("Deseja inserir demais informações do estagiário"))
				{

					log.warn("NÃO GEROU FICHA MÉDICA!");

					contErros++;

					erroWF = erroBas;

				}
				else if (erroBas == null || erroBas.contains("ficha médica") || erroBas.contains("Deseja inserir demais informações do estagiário"))
				{
					erroBas = erroBas != null ? erroBas.replace("Deseja inserir demais informações do estagiário?", "") : erroBas;

					adicionaRegistroIntegracao(chaveFBas);

					//RESGATA NUMCAD QUE ACABOU DE SER REGISTRADO
					numcad = resgataNumCad(apefun);

					if (tipsal == 5)
					{

						Boolean validaEventoHorista = insereEventoHorista(numcad.intValue(), numemp.intValue());

						if (validaEventoHorista)
						{

							log.info("EVENTOS DE HORISTA LANÇADOS COM SUCESSO - " + numcad + " - " + numemp);

						}
						else
						{

							contErros++;

							erroWF = erroBas + " - ERRO NA INSERÇÃO DOS EVENTOS DE HORISTA!";

						}
					}

					if (tipcon == 5l)
						insereDadosEstagio(processEntity, numcad.intValue(), numemp.intValue());

					if (apupon != 1l || tipsal == 5l)
						insereHistoricoDispensaPonto(processEntity, numcad.intValue(), numemp.intValue());

					String fichaMedica = getFichaMedica(numcad.intValue(), numemp.intValue()).toString();

					pEntity.findField("documentacao.fichamedica").setValue(fichaMedica);

					PersistEngine.persist(eform);

					log.warn("FICHA BÁSICA GERADA COM SUCESSO!");
					log.warn("FICHA MÉDICA GERADA: " + fichaMedica);
					contFBas++;

					log.info("NumCad resgatado: " + numcad);

					numL = new Long(numcad);

					log.info("Convertida Matricula para Long: " + numL);

					pEntity.findField("matricula").setValue(numL);

					Long newMatricula = pEntity.findGenericValue("matricula");

					log.info("Matricula setada no formulário: " + newMatricula);

					log.warn("MATRÍCULA: " + numcad);
					integrarFComIn.setNumCad(numcad);
					integrarFComIn.setFicReg(numcad.intValue());
					integrarAdiIn.setNumCad(numcad);

					SimpleDateFormat SDFHis = new SimpleDateFormat("yyyy-MM-dd");

					String dtAdmissaoS = SDFHis.format(datadmCalendar.getTime());

					Boolean validaHistoricoCracha = insereHistoricoCracha(numcad, numemp.intValue(), dtAdmissaoS);

					if (validaHistoricoCracha != null && !validaHistoricoCracha)
					{

						contErros++;

						erroWF = erroBas + " - ERRO NA INSERÇÃO DO HISTÓRICO DO CRACHÁ";

						log.info(numcad + " - " + numemp + "ERRO NO HISTÓRICO DO CRACHÁ!");

					}
					else
					{

						log.info(numcad + " - " + numemp + "HISTÓRICO DO CRACHÁ INSERIDO COM SUCESSO!");

						pEntity.findField("validaHistoricoCracha").setValue(Boolean.TRUE);

					}

					Boolean validaPersonalizados = inserePersonalizados(numcad.intValue(), tipadmcolab.intValue(), colabsede.toString(), "N", ((valcom == null) ? "" : valcom.toString()), depemp.intValue(), fisres.intValue(), nomcra);

					if (validaPersonalizados != null && !validaPersonalizados)
					{

						contErros++;

						erroWF = erroBas + " - ERRO NA INSERÇÃO DOS CAMPOS PERSONALIZADOS";

					}
					else
					{

						log.warn("CAMPOS PERSONALIZADOS INSERIDOS COM SUCESSO!");

						numL = new Long(numcad);

						pEntity.findField("matricula").setValue(numL);

					}

					Boolean validaDadosVigilantes = null;

					log.info("vigilante: " + pEntity.findGenericValue("dadosVigilancia.aitVigilante"));

					Boolean aitVigilante = pEntity.findGenericValue("dadosVigilancia.aitVigilante");

					String cargoDescricao = pEntity.findGenericValue("cargos.titcar");

					if ((tipcar != null && tipcar.equals("VIG") && cargoDescricao.contains("VIGILANTE")) || (tipcar != null && tipcar.equals("VIG") && aitVigilante))
					{

						GregorianCalendar gDtformacao = pEntity.findGenericValue("dadosVigilancia.dtFormacCNV");

						SimpleDateFormat SDFVig = new SimpleDateFormat("yyyy-MM-dd");

						String dtformacao = null;

						if (gDtformacao != null)
						{

							dtformacao = SDFVig.format(gDtformacao.getTime());

						}
						else
						{

							dtformacao = "1900-12-31";

						}

						GregorianCalendar gDtreciclagem = pEntity.findGenericValue("dadosVigilancia.dtReciclagemCNV");

						Boolean cadastroCNV = pEntity.findGenericValue("dadosVigilancia.cadastroCNV");

						String dtreciclagem = null;

						if (gDtreciclagem != null)
						{

							dtreciclagem = SDFVig.format(gDtreciclagem.getTime());

						}
						else
						{

							dtreciclagem = "1900-12-31";

						}

						String orgaovig = pEntity.findGenericValue("dadosVigilancia.orgaoExpCNV");

						String numdrt = pEntity.findGenericValue("dadosVigilancia.numDRT");

						GregorianCalendar gDtvalidadecnv = pEntity.findGenericValue("dadosVigilancia.dtvalidadeCNV");

						log.info("Data Validade CNV: " + gDtvalidadecnv + " - " + numemp + " - " + numcad);

						String dtvalidadecnv = null;

						if (gDtvalidadecnv != null)
						{

							dtvalidadecnv = SDFVig.format(gDtvalidadecnv.getTime());

						}
						else
						{

							dtvalidadecnv = "1900-12-31";

						}

						String numcnv = pEntity.findGenericValue("dadosVigilancia.numCNV");

						Boolean extraval = pEntity.findGenericValue("dadosVigilancia.extTranspVal");

						Boolean extsegpes = pEntity.findGenericValue("dadosVigilancia.extSeguPesso");

						Long codoem = pEntity.findGenericValue("dadosVigilancia.escolaVig.codoem");

						validaDadosVigilantes = insereDadosVigilante(numcad, numemp.intValue(), dtformacao, dtreciclagem, orgaovig, numdrt, dtvalidadecnv, numcnv, extraval, extsegpes, cadastroCNV, codoem);

						if (!validaDadosVigilantes)
						{

							contErros++;

							erroWF = erroBas + " - ERRO NA INSERÇÃO DOS DADOS DO VIGILANTE";

							log.info("ERRO NA INSERÇÃO DOS DADOS DO VIGILANTE - " + numcad);

						}
						else
						{

							log.info("DADOS DO VIGILANTE INSERIDOS COM SUCESSO - " + numcad);

						}

					}

					NeoFile fotoColabNF = pEntity.findGenericValue("fotocracha");

					if (fotoColabNF != null)
					{

						log.info("fotoColabNF: " + fotoColabNF);

						GregorianCalendar agora = new GregorianCalendar();

						SimpleDateFormat sdfatual = new SimpleDateFormat("yyyyMMddHHmmss");

						String caminho = sdfatual.format(agora.getTime());

						log.info("caminho: " + caminho);

						try
						{

							File diretorio = new File(caminhoPadrao + caminho);

							log.info("diretorio: " + diretorio);

							diretorio.mkdir();

						}
						catch (Exception ex)
						{

							ex.printStackTrace();
						}

						File tempDir = new File(caminhoPadrao + caminho);

						log.info("tempDir: " + tempDir);

						File tempFile = new File(tempDir + "\\" + fotoColabNF.getName());

						log.info("tempFile: " + tempFile);

						try
						{
							NeoStorage.copy(fotoColabNF.getInputStream(), new FileOutputStream(tempFile));

							log.info("fotoColabNF: " + fotoColabNF);

						}
						catch (FileNotFoundException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						//CRIAR INSERT NO BANCO DO IMAGEM_HEX

						Boolean validaInsertFotoCracha = insereFotoCracha(numcad, numemp.intValue(), tempFile.getAbsolutePath());

						log.info("validaInsertFotoCracha: " + validaInsertFotoCracha);

						tempFile.delete();
						tempDir.delete();

						if (validaInsertFotoCracha != null && !validaInsertFotoCracha)
						{

							contErros++;
							erroWF = erroBas + " - ERRO NA INSERÇÃO DA FOTO DO USUÁRIO";

						}
						else
						{

							log.warn("FOTO DO USUÁRIO INSERIDA COM SUCESSO!");

							numL = new Long(numcad);

							pEntity.findField("matricula").setValue(numL);
							pEntity.findField("validaFoto").setValue(Boolean.TRUE);
							pEntity.findField("errosIntegracao").setValue("");
							pEntity.findField("contErros").setValue(0l);

						}

					}
					else
					{

						numL = new Long(numcad);

						pEntity.findField("matricula").setValue(numL);
						pEntity.findField("validaFoto").setValue(Boolean.TRUE);
						pEntity.findField("errosIntegracao").setValue("");
						pEntity.findField("contErros").setValue(0l);

					}

					Boolean validaSolicitante = resgataSolicitante(codeUsu, numcad.intValue());

					if (validaSolicitante != null && !validaSolicitante)
					{

						contErros++;

						erroWF = erroBas + " - ERRO NA INSERÇÃO DO USUÁRIO SOLICITANTE";

					}
					else
					{

						log.warn("USUÁRIO SOLICITANTE INSERIDO COM SUCESSO!");

						numL = new Long(numcad);

						pEntity.findField("matricula").setValue(numL);

					}

					if (existeSelecao)
					{

						Boolean insereSelecao = insereSelecao(numcad, numemp.intValue(), tipcol.intValue());

						if (insereSelecao != null && !insereSelecao)
						{

							contErros++;

							erroWF = erroBas + " - ERRO NA INSERÇÃO DA SELEÇÃO";

						}
						else
						{

							numL = new Long(numcad);

							pEntity.findField("matricula").setValue(numL);

						}

					}

					log.info("ANTES ENTRAR NA LISTA VT");

					if (listaVT.size() > 0)
					{

						log.info("ENTROU NA LISTA VT");

						for (NeoObject itemVT : listaVT)
						{

							EntityWrapper wItemVT = new EntityWrapper(itemVT);

							Long empresaVT = wItemVT.findGenericValue("empresaVT.codigo");

							log.info("LISTA VT - EMPRESAVT - " + empresaVT);

							Integer empVT = null;

							if (empresaVT != null)
								empVT = empresaVT.intValue();

							Long numeroVT = wItemVT.findGenericValue("numVT");

							log.info("LISTA VT - NUMEROVT - " + numeroVT);

							Double numVT = null;

							if (numeroVT != null)
								numVT = numeroVT.doubleValue();

							if (empresaVT != null && numeroVT != null)
							{

								Boolean validaInsertVT = insereListaVT(numcad, empVT, numVT);

								log.info("VALIDAINSERTVT - " + validaInsertVT);

								if (!validaInsertVT)
								{

									log.info("ERRO NA INSERÇÃO DOS CARTÕES DE VT DO COLABORADOR DE MATRICULA: " + numcad);

									contErros++;
								}

							}

						}

					}

					numL = new Long(numcad);

					pEntity.findField("matricula").setValue(numL);

				}
				else
				{

					new WorkflowException("ERRO NA INTEGRAÇÃO");

				}

			}
			else
			{

				Long matricula = pEntity.findGenericValue("matricula");

				String apefunS = pEntity.findGenericValue("apefun");

				Integer cadastroResgate = null;

				if (matricula == null)
				{

					cadastroResgate = resgataNumCad(apefunS);

				}
				else
				{

					cadastroResgate = matricula.intValue();

				}

				log.info("Resgate passagem Matrícula: " + cadastroResgate);

				numcad = cadastroResgate.intValue();

				integrarFComIn.setNumCad(numcad);
				integrarFComIn.setFicReg(numcad.intValue());
				integrarAdiIn.setNumCad(numcad);

				Boolean existeHistoricoCracha = pEntity.findGenericValue("validaHistoricoCracha");

				if (!existeHistoricoCracha)
				{

					SimpleDateFormat SDFHis = new SimpleDateFormat("yyyy-MM-dd");

					String dtAdmissaoS = SDFHis.format(datadmCalendar.getTime());

					Boolean validaHistoricoCracha = insereHistoricoCracha(numcad, numemp.intValue(), dtAdmissaoS);

					if (validaHistoricoCracha != null && !validaHistoricoCracha)
					{

						contErros++;

						erroWF = erroBas + " - ERRO NA INSERÇÃO DO HISTÓRICO DO CRACHÁ";

						log.info(numcad + " - " + numemp + "ERRO NO HISTÓRICO DO CRACHÁ!");

					}
					else
					{

						log.info(numcad + " - " + numemp + "HISTÓRICO DO CRACHÁ INSERIDO COM SUCESSO!");

						pEntity.findField("validaHistoricoCracha").setValue(Boolean.TRUE);

					}

				}

				Boolean validaFoto = pEntity.findGenericValue("validaFoto");

				if (!validaFoto)
				{

					/////////////////////////////////////////////////////////////////////////////////////////////
					/////INSERIDO O CÓDIGO DE INSERÇÃO DA FOTO FORA DA FICHA BÁSICA PARA ACUSAR O ERRO MESMO APÓS
					/////JÁ GERADA A FB.

					NeoFile fotoColabNF = pEntity.findGenericValue("fotocracha");

					if (fotoColabNF != null)
					{

						log.info("fotoColabNF: " + fotoColabNF);

						GregorianCalendar agora = new GregorianCalendar();

						SimpleDateFormat sdfatual = new SimpleDateFormat("yyyyMMddHHmmss");

						String caminho = sdfatual.format(agora.getTime());

						try
						{

							File diretorio = new File(caminhoPadrao + caminho);

							log.info("diretorio: " + diretorio);

							diretorio.mkdir();

						}
						catch (Exception ex)
						{

							ex.printStackTrace();
						}

						File tempDir = new File(caminhoPadrao + caminho);

						log.info("tempDir: " + tempDir);

						File tempFile = new File(tempDir + "\\" + fotoColabNF.getName());

						log.info("tempFile: " + tempFile);

						try
						{
							NeoStorage.copy(fotoColabNF.getInputStream(), new FileOutputStream(tempFile));

							log.info("fotoColabNF: " + fotoColabNF);

						}
						catch (FileNotFoundException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						//CRIAR INSERT NO BANCO DO IMAGEM_HEX

						Boolean validaInsertFotoCracha = insereFotoCracha(numcad, numemp.intValue(), tempFile.getAbsolutePath());

						log.info("validaInsertFotoCracha: " + validaInsertFotoCracha);

						tempFile.delete();
						tempDir.delete();

						if (validaInsertFotoCracha != null && !validaInsertFotoCracha)
						{

							contErros++;
							erroWF = erroBas + " - ERRO NA INSERÇÃO DA FOTO DO USUÁRIO";

						}
						else
						{

							log.warn("FOTO DO USUÁRIO INSERIDA COM SUCESSO!");
							log.info("FOTO DO USUÁRIO INSERIDA COM SUCESSO!");

							numL = new Long(numcad);

							log.info("numL: " + numL);

							pEntity.findField("matricula").setValue(numL);

							pEntity.findField("validaFoto").setValue(Boolean.TRUE);

							pEntity.findField("errosIntegracao").setValue("");

							pEntity.findField("contErros").setValue(0);

						}

					}
					else
					{

						numL = new Long(numcad);

						log.info("numL: " + numL);

						pEntity.findField("matricula").setValue(numL);

						pEntity.findField("validaFoto").setValue(Boolean.TRUE);

						pEntity.findField("errosIntegracao").setValue("");

						pEntity.findField("contErros").setValue(0);

					}

				}

				log.info("FICHA BÁSICA JÁ FOI GERADA");
				contFBas++;

			}

			if (contErros > 0)
			{

				log.info("NÃO ENTROU NA FICHA COMPLEMENTAR!");

			}
			else
			{

				List<NeoObject> registroFCom = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("rhRegistroIntegracao"), new QLRawFilter("registro like '" + chaveFCom + "'"));

				if (registroFCom.size() == 0)
				{

					//LOCATOR FCom
					G5SeniorServicesLocatorFCom glCom = new G5SeniorServicesLocatorFCom();

					//INTEGRAÇÃO FCom
					try
					{
						log.info("INTEGRACAO FichaComplementar :" + chaveCodigo + " - INICIO: " + sdf.format(new GregorianCalendar().getTime()));
						integrarFComOut = glCom.getrubi_Synccom_senior_g5_rh_fp_fichaComplementarPort().fichaComplementar_7(config.getUserService(), config.getPasswordService(), 0, integrarFComIn);
						log.info("INTEGRACAO FichaComplementar :" + chaveCodigo + " - FIM: " + sdf.format(new GregorianCalendar().getTime()));
					}
					catch (RemoteException | ServiceException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					erroCom = integrarFComOut.getErroExecucao();

					log.info("Mensagem da ficha complementar da matrícula: " + numcad + " - " + erroCom);

					if (erroCom != null)
					{

						if (erroCom.contains("O campo \"DENifExt1\" não está disponível na tela \"FR034CPL\""))
						{

							log.warn("FICHA COMPLEMENTAR GERADA COM SUCESSO!");
							log.info("FICHA COMPLEMENTAR GERADA COM SUCESSO!");

							adicionaRegistroIntegracao(chaveFCom);
							contFCom++;

						}
						else
						{

							log.warn("ERRO NA FICHA COMPLEMENTAR!");

							contErros++;

							erroWF = erroBas + " - " + erroCom;

						}

					}
					else if (erroCom == null)
					{

						log.info("FICHA COMPLEMENTAR GERADA COM SUCESSO!");

						adicionaRegistroIntegracao(chaveFCom);
						contFCom++;

					}

				}
				else
				{

					log.info("FICHA COMPLEMENTAR JÁ FOI GERADA!");
					contFCom++;

				}

			}

			if (erroCom != null || contFBas > 0 || contFCom > 0)
			{

				if (erroCom != null)
				{

					if (erroCom.contains("erro") && !erroCom.contains("O campo \"DENifExt1\" não está disponível na tela \"FR034CPL\""))
					{

					}
					else
					{

						List<NeoObject> registroFPHis = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("rhRegistroIntegracao"), new QLRawFilter("registro like '" + chaveAdic + "'"));

						if (registroFPHis.size() == 0 && InsereAdicional)
						{

							//LOCATOR Adi
							G5SeniorServicesLocatorFPHis glAdi = new G5SeniorServicesLocatorFPHis();

							//INTEGRAÇÃO Adi
							try
							{
								log.info("INTEGRACAO Adicionais :" + chaveCodigo + " - INICIO: " + sdf.format(new GregorianCalendar().getTime()));
								integrarAdiOut = glAdi.getrubi_Synccom_senior_g5_rh_fp_historicosPort().adicional_2(config.getUserService(), config.getPasswordService(), 0, integrarAdiIn);
								log.info("INTEGRACAO Adicionais :" + chaveCodigo + " - FIM: " + sdf.format(new GregorianCalendar().getTime()));
							}
							catch (RemoteException | ServiceException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}

						erroAdi = integrarAdiOut.getErroExecucao();

						if ((erroAdi != null && erroAdi.contains("erro")) && !erroAdi.contains("Registro já existe"))
						{

							contErros++;

							erroWF = erroAdi + " - " + erroCom + " - " + erroBas;

						}
						else
						{

							log.info("ADICIONAIS GERADOS COM SUCESSO!");

							adicionaRegistroIntegracao(chaveAdic);

						}

					}

				}
				else
				{

					List<NeoObject> registroFPHis = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("rhRegistroIntegracao"), new QLRawFilter("registro like '" + chaveAdic + "'"));

					if (registroFPHis.size() == 0)
					{

						//LOCATOR Adi
						G5SeniorServicesLocatorFPHis glAdi = new G5SeniorServicesLocatorFPHis();

						//INTEGRAÇÃO Adi
						try
						{
							log.info("INTEGRACAO Adicionais :" + chaveCodigo + " - INICIO: " + sdf.format(new GregorianCalendar().getTime()));
							integrarAdiOut = glAdi.getrubi_Synccom_senior_g5_rh_fp_historicosPort().adicional_2(config.getUserService(), config.getPasswordService(), 0, integrarAdiIn);
							log.info("INTEGRACAO Adicionais :" + chaveCodigo + " - FIM: " + sdf.format(new GregorianCalendar().getTime()));
						}
						catch (RemoteException | ServiceException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

					erroAdi = integrarAdiOut.getErroExecucao();

					if ((erroAdi != null && erroAdi.contains("erro")) && !erroAdi.contains("Registro já existe"))
					{

						log.info("ERRO NOS ADICIONAIS!");

						contErros++;

						erroWF = erroAdi + " - " + erroCom + " - " + erroBas;

					}
					else
					{

						log.info("ADICIONAIS GERADOS COM SUCESSO!");

						adicionaRegistroIntegracao(chaveAdic);

					}

				}

			}

			//cadastrarAnotacao(numemp,tipcol,numcad,dtAdmissaoS,1,);

			if ((erroAdi != null && erroAdi.contains("erro")) && !erroAdi.contains("Registro já existe"))
			{
				log.info("NÃO ENTROU NOS DEPENDENTES!");
			}
			else
			{

				Boolean possuiDepe = pEntity.findGenericValue("possuiDepend");

				//INTEGRAÇÃO Dep e DepNas
				List<NeoObject> listaDependentes = pEntity.findGenericValue("listaDepend");

				List<NeoObject> registroFPDepe = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("rhRegistroIntegracao"), new QLRawFilter("registro like '" + chaveDepe + "'"));

				if (registroFPDepe.size() == 0 && possuiDepe)
				{

					Integer coddep = 1;

					for (NeoObject dependente : listaDependentes)
					{

						EntityWrapper wDependente = new EntityWrapper(dependente);

						String nomdep = wDependente.findGenericValue("nomDependente");

						String datnasDep = null;

						Long grapar = wDependente.findGenericValue("graParentesco.codigo");

						if (grapar == 3)
						{

							datnasDep = wDependente.findGenericValue("dtNasciParentPaiMae");

						}
						else
						{

							GregorianCalendar GCdatnas = wDependente.findGenericValue("dtNascDepend");

							datnasDep = SDF.format(GCdatnas.getTime());

						}

						String tipsexDep = wDependente.findGenericValue("sexoDependente.sigla");

						String nomcomDep = wDependente.findGenericValue("nomCompletoDepend");

						Long tipdep = wDependente.findGenericValue("tpDependESoci.codigo"); //não mapeado na webservice

						Long estcivDep = wDependente.findGenericValue("estCivilDependente.codigo");

						String numcpfDep = wDependente.findGenericValue("cpfDependente");

						String entcer = null;

						if (grapar == 1)
						{

							GregorianCalendar GCentcer = wDependente.findGenericValue("dtEntregaCertidao");

							entcer = SDF.format(GCentcer.getTime());

							integrarDepIn.setAviImp("N");

							integrarDepNasIn.setEntCer(entcer);

						}
						else
						{

							integrarDepIn.setAviImp(null);

							integrarDepNasIn.setEntCer(null);
						}

						integrarDepIn.setTipOpe(tipope);
						integrarDepIn.setNumEmp(numemp.intValue());
						integrarDepIn.setTipCol(tipcol.intValue());
						integrarDepIn.setNumCad(numcad);
						integrarDepIn.setCodDep(coddep);
						integrarDepIn.setNomDep(nomdep);
						integrarDepIn.setDatNas(datnasDep);
						integrarDepIn.setGraPar(grapar.intValue());
						integrarDepIn.setTipSex(tipsexDep);
						integrarDepIn.setNomCom(nomcomDep);
						integrarDepIn.setTipDep(tipdep.intValue());
						integrarDepIn.setEstCiv(estcivDep.intValue());
						integrarDepIn.setNumCpf(numcpfDep);
						integrarDepIn.setPenJud("N");

						integrarDepNasIn.setNumEmp(numemp.intValue());
						integrarDepNasIn.setTipCol(tipcol.intValue());
						integrarDepNasIn.setNumCad(numcad);
						integrarDepNasIn.setCodDep(coddep);

						List<NeoObject> registroFDep = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("rhRegistroIntegracao"), new QLRawFilter("registro like '" + chaveDepe + coddep.toString() + "'"));

						if (registroFDep.size() == 0)
						{

							//LOCATOR Dep
							G5SeniorServicesLocatorDep glDep = new G5SeniorServicesLocatorDep();

							try
							{
								log.info("INTEGRACAO Dependentes :" + chaveCodigo + " - codDep:" + coddep + " - INICIO: " + sdf.format(new GregorianCalendar().getTime()));
								integrarDepOut = glDep.getrubi_Synccom_senior_g5_rh_fp_dependentesPort().dependente_2(config.getUserService(), config.getPasswordService(), 0, integrarDepIn);
								log.info("INTEGRACAO Dependentes :" + chaveCodigo + " - codDep:" + coddep + " - FIM: " + sdf.format(new GregorianCalendar().getTime()));
							}
							catch (RemoteException | ServiceException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}

						erroDep = integrarDepOut.getErroExecucao();

						if (erroDep != null && !erroDep.contains("Registro já existe."))
						{

							log.info("ERRO NOS DEPENDENTES!");

							contErros++;

							erroWF = erroDep + " - " + erroAdi + " - " + erroBas + " - " + erroCom;

						}
						else
						{

							log.info("DEPENDENTE " + coddep + " INSERIDO COM SUCESSO!");

							adicionaRegistroIntegracao(chaveDepe);

							adicionaRegistroIntegracao(chaveDepe + coddep.toString());

							if (erroDep != null && erroDep.contains("erro"))
								log.info("NÃO ENTROU NO NASCIMENTO DOS DEPENDENTES!");
							else
							{

								integrarDepNasIn.setNumEmp(numemp.intValue());
								integrarDepNasIn.setTipCol(tipcol.intValue());
								integrarDepNasIn.setNumCad(numcad.intValue());
								integrarDepNasIn.setCodDep(coddep);

								if (grapar == 1)
								{

									GregorianCalendar GCentcer = wDependente.findGenericValue("dtEntregaCertidao");

									entcer = SDF.format(GCentcer.getTime());

									integrarDepNasIn.setEntCer(entcer);

								}
								else
								{

									integrarDepNasIn.setEntCer(null);

								}

								List<NeoObject> registroFDepNas = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("rhRegistroIntegracao"), new QLRawFilter("registro like '" + chaveDNas + coddep.toString() + "'"));

								if (registroFDepNas.size() == 0)
								{

									//LOCATOR DepNas
									G5SeniorServicesLocatorDep glDepNas = new G5SeniorServicesLocatorDep();

									//Integrar Entrega da Certidão
									try
									{
										log.info("INTEGRACAO NascDependente :" + chaveCodigo + " - " + coddep + "/" + chaveDNas + " - INICIO: " + sdf.format(new GregorianCalendar().getTime()));
										integrarDepNasOut = glDepNas.getrubi_Synccom_senior_g5_rh_fp_dependentesPort().nascimento(config.getUserService(), config.getPasswordService(), 0, integrarDepNasIn);
										log.info("INTEGRACAO NascDependente :" + chaveCodigo + " - " + coddep + "/" + chaveDNas + " - FIM: " + sdf.format(new GregorianCalendar().getTime()));
									}
									catch (RemoteException | ServiceException e)
									{
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

								}

								erroDepNas = integrarDepNasOut.getErroExecucao();

								if (erroDepNas != null && erroDepNas.contains("erro"))
								{

									contErros++;

									erroWF = erroDepNas + " - " + erroDep + " - " + erroAdi + " - " + erroCom + " - " + erroBas;

								}
								else
								{

									log.info("NASCIMENTO DO DEPENDENTE INSERIDO COM SUCESSO!");

									adicionaRegistroIntegracao(chaveDNas + coddep.toString());

								}

							}

						}

						coddep++;

					}

				}

			}

		}

		//RESGATA SOLICITANTE E VERIFICA SE POSSUI O PAPEL REFERENTE AS ADMISSÕES DA SEDE
		//CASO SEJA DA SEDE NÃO IRÁ EFETUAR ESSAS INTEGRAÇÕES

		/*
		 * NeoUser solicitante = pEntity.findGenericValue("exec1");
		 * Set<NeoPaper> solicPapers = solicitante.getPapers();
		 * Boolean eSede = Boolean.FALSE;
		 * for (NeoPaper papel : solicPapers)
		 * {
		 * if (papel.getCode().contains("rhAdmissaoSede"))
		 * {
		 * eSede = Boolean.TRUE;
		 * }
		 * }
		 */

		Boolean eSede = false;
		Long codRegional = pEntity.findGenericValue("regionalColab.usu_codreg");
		if (codRegional != null && (codRegional == 0l || codRegional == 1l || codRegional == 9l))
			eSede = true;

		if (!eSede)
		{

			//BUSCA CAMPOS ASO E RESULTADOS
			GregorianCalendar GCultexm = pEntity.findGenericValue("ultExameMedic");
			String ultexm = SDF.format(GCultexm.getTime());
			//String ultexm = SDF.format(new GregorianCalendar().getTime());
			//Long codAte = pEntity.findGenericValue("atendente.codate");

			//Integer numAte = numAte();
			String StcodFic = pEntity.findGenericValue("documentacao.fichamedica");
			if (StcodFic == null)
			{
				new WorkflowException("ERRO NA INTEGRAÇÃO");
			}
			Integer codFic;

			if (ParametrizacaoAdmissao.findParameter("efetuaIntegracaoASO") != null && !ParametrizacaoAdmissao.findParameter("efetuaIntegracaoASO").equals("0"))
			{

				// INÍCIO INTEGRAÇÃO ASO

				//IN ASO EXTERNO

				codFic = Integer.parseInt(StcodFic);
				log.info("INTEGRACAO AsoExterna : codFic=" + codFic);

				Long codMed = pEntity.findGenericValue("medico.codate");

				integrarASOIn.setTipOpe("i"); //FIXO COMO INCLUSÃO
				log.info("INTEGRACAO AsoExterna : tipOpe=" + integrarASOIn.getTipOpe());

				integrarASOIn.setNumEmp(numemp != null ? numemp.intValue() : null);
				log.info("INTEGRACAO AsoExterna : numemp=" + integrarASOIn.getNumEmp());

				integrarASOIn.setCodAte(codMed != null ? codMed.intValue() : null); //criar campo
				log.info("INTEGRACAO AsoExterna : codMed=" + integrarASOIn.getCodAte());

				integrarASOIn.setNumAte(numAte(numemp, codMed));
				log.info("INTEGRACAO AsoExterna : numAte=" + integrarASOIn.getNumAte());

				integrarASOIn.setDatAte(ultexm);
				log.info("INTEGRACAO AsoExterna : ultexm=" + integrarASOIn.getDatAte());

				integrarASOIn.setTipAso(1);
				log.info("INTEGRACAO AsoExterna : tipAso=" + integrarASOIn.getTipAso());

				integrarASOIn.setCodFic(codFic);
				log.info("INTEGRACAO AsoExterna : codFic=" + integrarASOIn.getCodFic());

				integrarASOIn.setCodPar(40);
				log.info("INTEGRACAO AsoExterna : codPar=" + integrarASOIn.getCodPar());

				List<NeoObject> registroASO = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("rhRegistroIntegracao"), new QLRawFilter("registro like '" + chaveASO + "'"));

				if (registroASO.size() == 0)
				{

					// LOCATOR ASO
					G5SeniorServicesLocatorAso glASO = new G5SeniorServicesLocatorAso();

					// INTEGRAÇÃO ASO
					try
					{
						log.info("INTEGRACAO AsoExterna :" + chaveCodigo + " - INICIO: " + new GregorianCalendar());
						integrarASOOut = glASO.getsm_Synccom_senior_g5_rh_sm_asoexternoPort().ASOExterno_2(config.getUserService(), config.getPasswordService(), 0, integrarASOIn);
						log.info("INTEGRACAO AsoExterna :" + chaveCodigo + " - FIM: " + new GregorianCalendar() + " - " + integrarASOOut.getErroExecucao());
					}
					catch (RemoteException | ServiceException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				erroASO = integrarASOOut.getErroExecucao();

				if ((erroASO != null && erroASO.contains("erro")) && !erroASO.contains("Registro já existe"))
				{

					contErros++;

					erroWF = erroASO + ": ERRO NA CRIAÇÃO DA ASO EXTERNA - " + erroCom + " - " + erroBas;

				}
				else
				{

					log.info("Registro da ASO inserido com sucesso!");

					adicionaRegistroIntegracao(chaveASO);

				}
			}

			if (ParametrizacaoAdmissao.findParameter("efetuaIntegracaoResultadoExame") != null && !ParametrizacaoAdmissao.findParameter("efetuaIntegracaoResultadoExame").equals("0"))
			{

				// INÍCIO INTEGRAÇÃO RESULTADO DO EXAME

				//IN RESULTADO EXAMES
				codFic = Integer.parseInt(StcodFic);
				log.info("INTEGRACAO ResultadoExame : codFic=" + codFic);

				integrarResultExamIn.setNumEmp(numemp != null ? numemp.intValue() : null);
				log.info("INTEGRACAO ResultadoExame : numemp=" + numemp != null ? numemp.intValue() : null);

				integrarResultExamIn.setCodFic(codFic);
				log.info("INTEGRACAO ResultadoExame : codFic=" + codFic);

				integrarResultExamIn.setCodExa(2);
				log.info("INTEGRACAO ResultadoExame : codExa=2");

				integrarResultExamIn.setDatSol(ultexm);
				log.info("INTEGRACAO ResultadoExame : datSol=" + ultexm);

				integrarResultExamIn.setTipOpe("i");
				log.info("INTEGRACAO ResultadoExame : tipOpe=i");

				integrarResultExamIn.setOriExa(1);
				log.info("INTEGRACAO ResultadoExame : oriExa=1");

				integrarResultExamIn.setCodAte(132); //código da Thays fixamente
				log.info("INTEGRACAO ResultadoExame : codAte=132");

				integrarResultExamIn.setConPPP("S");
				log.info("INTEGRACAO ResultadoExame : conPPP=S");

				integrarResultExamIn.setSitExa("R");
				log.info("INTEGRACAO ResultadoExame : SitExa=R");

				integrarResultExamIn.setTipExa("R");
				log.info("INTEGRACAO ResultadoExame : TipExa=R");

				integrarResultExamIn.setCodPar(40);
				log.info("INTEGRACAO ResultadoExame : codPar=40");

				integrarResultExamIn.setNorAlt("N");
				log.info("INTEGRACAO ResultadoExame : norAlt=N");

				integrarResultExamIn.setEvoRes("E");
				log.info("INTEGRACAO ResultadoExame : evoRes=E");

				integrarResultExamIn.setSitRes("O");
				log.info("INTEGRACAO ResultadoExame : sitRes=O");

				List<NeoObject> registroResultExam = (List<NeoObject>) PersistEngine.getObjects(AdapterUtils.getEntityClass("rhRegistroIntegracao"), new QLRawFilter("registro like '" + chaveResultExam + "'"));

				if (registroResultExam.size() == 0)
				{

					// LOCATOR RESULTADO DO EXAME
					G5SeniorServicesLocatorResultExam glResultExam = new G5SeniorServicesLocatorResultExam();

					// INTEGRAÇÃO RESULTADO DO EXAME
					try
					{
						log.info("INTEGRACAO ExameAso :" + chaveCodigo + " - INICIO: " + new GregorianCalendar());
						integrarResultExamOut = glResultExam.getsm_Synccom_senior_g5_rh_sm_ResultadosExamesPort().resultados_3(config.getUserService(), config.getPasswordService(), 0, integrarResultExamIn);
						log.info("INTEGRACAO ExameAso :" + chaveCodigo + " - FIM: " + new GregorianCalendar() + " - " + integrarResultExamOut.getErroExecucao());
					}
					catch (RemoteException | ServiceException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				erroResultExam = integrarResultExamOut.getErroExecucao();

				if ((erroResultExam != null && erroResultExam.contains("erro")) && !erroResultExam.contains("Registro já existe"))
				{

					contErros++;

					erroWF = erroResultExam + ": ERRO NA CRIAÇÃO DO RESULTADO DO EXAME DA ASO - " + erroCom + " - " + erroBas;

				}
				else
				{

					log.info("Registro do Resultado do Exame inserido com sucesso!");

					adicionaRegistroIntegracao(chaveResultExam);

				}
			}

		}

		//if(contErros > 0 && !(boolean) pEntity.findField("flagErro").getValue()){
		if (contErros > 0)
		{

			pEntity.findField("errosIntegracao").setValue(erroWF);

			pEntity.findField("contErros").setValue(new Long(contErros));

			pEntity.findField("exec2").setValue(userOrigin);

		}
		else
		{

			pEntity.findField("validaIntegracao").setValue(Boolean.TRUE);

			pEntity.findField("contErros").setValue(new Long(contErros));

		}

	}

	@Override
	public void onTransfer(TaskTransferEvent event)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onAssign(TaskAssignerEvent event) throws TaskException
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onStart(ActivityEvent event) throws ActivityException
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{
		// TODO Auto-generated method stub

	}

	public static Integer resgataNumCad(String chave)
	{

		Connection connSapiens = null;

		PreparedStatement pstData = null;

		Integer numcad = null;

		try
		{

			connSapiens = PersistEngine.getConnection("VETORH");

			String query = " select numcad from r034fun where apefun like '" + chave + "' ";

			pstData = connSapiens.prepareStatement(query);

			java.sql.ResultSet rs = pstData.executeQuery();

			if (rs.next())
			{

				numcad = rs.getInt("numcad");

			}
			else
			{

				log.warn("NÃO EXISTE REGISTRO");

				return null;

			}

			return numcad;

		}
		catch (Exception e)
		{

			log.warn("");
			e.printStackTrace();
			return null;

		}
		finally
		{

			OrsegupsUtils.closeConnection(connSapiens, pstData, null);
		}

	}

	public static Integer getFichaMedica(Integer nrCadastro, Integer nrEmpresa)
	{
		Connection connSapiens = null;
		PreparedStatement pstmt = null;

		Integer nrFicha = null;

		try
		{
			connSapiens = PersistEngine.getConnection("VETORH");

			String query = " select codfic from R110FIC where numemp = " + nrEmpresa + " and numcad = " + nrCadastro;

			pstmt = connSapiens.prepareStatement(query);

			ResultSet rs = pstmt.executeQuery();

			if (rs.next())
				nrFicha = rs.getInt("codfic");
			else
			{
				log.warn("NÃO EXISTE REGISTRO");

				return null;
			}

			return nrFicha;
		}
		catch (Exception e)
		{

			log.warn("");
			e.printStackTrace();
			return null;

		}
		finally
		{

			OrsegupsUtils.closeConnection(connSapiens, pstmt, null);
		}
	}

	public static Integer numAte(Long numemp, Long codmed)
	{

		Connection connSapiens = null;
		PreparedStatement pstData = null;
		Integer numAte = null;

		try
		{

			connSapiens = PersistEngine.getConnection("VETORH");

			StringBuffer varname1 = new StringBuffer();

			varname1.append("  SELECT top 1 numate + 1 AS numate ");
			varname1.append("    FROM r110mam AS A ");
			varname1.append("   WHERE A.numemp = ");
			varname1.append(numemp);
			varname1.append("     AND A.codate = ");
			varname1.append(codmed);
			varname1.append("     AND NOT EXISTS (SELECT * ");
			varname1.append("                       FROM r110mam AS B ");
			varname1.append("                      WHERE B.numemp = ");
			varname1.append(numemp);
			varname1.append("                        AND B.codate = ");
			varname1.append(codmed);
			varname1.append("                        AND B.numate = A.numate + 1) ");
			varname1.append("     AND numate < (SELECT MAX(numate) FROM r110mam) ");
			varname1.append("order by 1");

			pstData = connSapiens.prepareStatement(varname1.toString());
			java.sql.ResultSet rs = pstData.executeQuery();

			if (rs.next())
			{
				numAte = rs.getInt("numate");
			}
			else
			{
				return 1;
			}

			return numAte;

		}
		catch (Exception e)
		{

			log.warn("");
			e.printStackTrace();
			return 0;

		}
		finally
		{

			OrsegupsUtils.closeConnection(connSapiens, pstData, null);
		}

	}

	public static Boolean resgataSolicitante(String codeUsu, Integer numcad)
	{

		Connection connSapiens = null;

		PreparedStatement pstData = null;

		PreparedStatement pstData2 = null;

		if (codeUsu.equals("thays.santos"))
			codeUsu = "thays.passos";

		Integer codusu = null;

		try
		{

			connSapiens = PersistEngine.getConnection("VETORH");

			String query = " select codusu from r999usu where nomusu like '" + codeUsu + "' ";

			String query2 = null;

			pstData = connSapiens.prepareStatement(query);

			java.sql.ResultSet rs = pstData.executeQuery();

			if (rs.next())
			{

				codusu = rs.getInt("codusu");

				query2 = " update r034fun set usu_codusu = " + codusu + " where numcad = " + numcad;

			}
			else
			{

				log.warn("NÃO EXISTE REGISTRO");

				return Boolean.FALSE;

			}

			if (codusu != null)
			{

				pstData2 = connSapiens.prepareStatement(query2);

				int ret = pstData2.executeUpdate();

				if (ret == 0)
				{

					log.warn("NÃO LIMPOU O APELIDO");

					return Boolean.FALSE;

				}
				else
				{

					return Boolean.TRUE;

				}

			}
			else
			{

				return Boolean.FALSE;

			}

		}
		catch (Exception e)
		{

			log.warn("");
			e.printStackTrace();
			return null;

		}
		finally
		{

			OrsegupsUtils.closeConnection(connSapiens, pstData, null);
		}

	}

	public static Boolean inserePersonalizados(Integer NumCad, Integer USU_TipAdm, String USU_ColSde, String USU_CsgBrc, String USU_ValCom, Integer USU_DepEmp, Integer USU_FisRes, String USU_NomCra)
	{

		Connection connSapiens = null;

		PreparedStatement pstData = null;

		Boolean result = null;

		try
		{

			connSapiens = PersistEngine.getConnection("VETORH");

			String query = " update r034fun set usu_tipadm = " + USU_TipAdm + ", usu_colsde = '" + USU_ColSde + "', usu_csgbrc = '" + USU_CsgBrc + "', usu_valcom = '" + USU_ValCom + "', usu_depemp = " + USU_DepEmp + ", usu_fisres = " + USU_FisRes + ", usu_nomcra = '" + USU_NomCra + "'  where numcad = " + NumCad;

			if (NumCad != null)
			{

				pstData = connSapiens.prepareStatement(query);

				int ret = pstData.executeUpdate();

				if (ret == 0)
				{

					log.warn("ERRO NO UPDATE DO COLABORADOR DE MATRÍCULA: " + NumCad);
					log.info("ERRO NO UPDATE DO COLABORADOR DE MATRÍCULA: " + NumCad);

					return Boolean.FALSE;

				}
				else
				{

					return Boolean.TRUE;

				}

			}

			return result;

		}
		catch (Exception e)
		{

			log.warn("ERRO NO MÉTODO: \"inserePersonalizados\"");
			log.info("ERRO NO MÉTODO: \"inserePersonalizados\"");
			e.printStackTrace();
			return Boolean.FALSE;

		}
		finally
		{

			OrsegupsUtils.closeConnection(connSapiens, pstData, null);
		}

	}

	public static Boolean insereEventoHorista(Integer NumCad, Integer NumEmp)
	{

		Connection connSapiens = null;

		PreparedStatement pstData = null;

		PreparedStatement pstData2 = null;

		Boolean result = null;

		try
		{

			connSapiens = PersistEngine.getConnection("VETORH");

			String query = " insert into r044fix (NumEmp, TipCol, NumCad, TabEve, CodEve, CodRat, DatIni, DatFim) " + "values (" + NumEmp + ", 1, " + NumCad + ", 941, 1, 0, '1900-12-31 00:00:00.0', '1900-12-31 00:00:00.0')";

			log.info(NumEmp + " - " + NumCad + " - 1: " + query);

			String query2 = " insert into r044fix (NumEmp, TipCol, NumCad, TabEve, CodEve, CodRat, DatIni, DatFim) " + "values (" + NumEmp + ", 1, " + NumCad + ", 941, 2, 0, '1900-12-31 00:00:00.0', '1900-12-31 00:00:00.0')";

			log.info(NumEmp + " - " + NumCad + " - 2: " + query2);

			if (NumCad != null)
			{

				pstData = connSapiens.prepareStatement(query);

				pstData2 = connSapiens.prepareStatement(query2);

				int ret = pstData.executeUpdate();

				int ret2 = pstData2.executeUpdate();

				if (ret == 0 || ret2 == 0)
				{

					log.warn("ERRO NO INSERT DO EVENTO DO HORISTA: " + NumCad);
					log.info("ERRO NO INSERT DO EVENTO DO HORISTA: " + NumCad);

					return Boolean.FALSE;

				}
				else
				{

					return Boolean.TRUE;

				}

			}

			return result;

		}
		catch (Exception e)
		{

			log.warn("ERRO NO MÉTODO: \"insereEventoHorista\"");
			log.info("ERRO NO MÉTODO: \"insereEventoHorista\"");
			e.printStackTrace();
			return Boolean.FALSE;

		}
		finally
		{

			OrsegupsUtils.closeConnection(connSapiens, pstData, null);
		}

	}

	public static Boolean insereDadosEstagio(EntityWrapper wForm, Integer nrCadastro, Integer nrEmpresa)
	{
		Connection connSapiens = null;
		PreparedStatement pstmt = null;
		Boolean result = null;

		try
		{
			connSapiens = PersistEngine.getConnection("VETORH");

			BigDecimal valorBolsa = wForm.findGenericValue("estagiario.valBol");
			Long tipoCol = wForm.findGenericValue("tpColaborador.codigo");
			String codNatEstagio = wForm.findGenericValue("estagiario.naturezaEstagio.codigo");
			Long codNivelEstagio = wForm.findGenericValue("estagiario.nivelEstagio.codigo");
			Long codInstEnsino = wForm.findGenericValue("estagiario.insEns.codoem");
			Long codAgenteIntegracao = wForm.findGenericValue("estagiario.ageInt.codoem");
			Long codEmpresaSupervisor = wForm.findGenericValue("estagiario.empCoo.numemp");
			Long codTipoSupervisor = wForm.findGenericValue("estagiario.tipCoo.codigo");
			Long codSupervisor = wForm.findGenericValue("estagiario.numCoo.numcad");
			Long cpfSupervisor = wForm.findGenericValue("estagiario.cpfCoo");
			GregorianCalendar dtPrevistaTermino = wForm.findGenericValue("estagiario.preTer");
			String dtPrevistaStr = NeoUtils.safeOutputString(dtPrevistaTermino != null ? new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(dtPrevistaTermino.getTime()) : null);
			String nomeSupervisor = wForm.findGenericValue("estagiario.nomCoo");
			String areaAtuacao = wForm.findGenericValue("estagiario.areAtu");
			String apoliceSeguro = wForm.findGenericValue("estagiario.apoSeg");

			String query = "insert into R034ETG (NumEmp, TipCol, NumCad, natetg, nivetg, areatu, aposeg, valbol, preter, insens, ageint, empcoo, tipcoo, numcoo, nomcoo, cpfcoo) " + "values (" + nrEmpresa + ", " + tipoCol + ", " + nrCadastro + ", '" + codNatEstagio + "', " + codNivelEstagio + ", '" + areaAtuacao + "', '" + apoliceSeguro + "', " + valorBolsa + ", '" + dtPrevistaStr + "', " + codInstEnsino + ", " + codAgenteIntegracao + ", " + codEmpresaSupervisor + ", " + codTipoSupervisor + ", "
					+ codSupervisor + ", '" + nomeSupervisor + "', '" + cpfSupervisor + "')";

			log.info(nrEmpresa + " - " + nrCadastro + " - " + query);

			if (nrCadastro != null)
			{
				pstmt = connSapiens.prepareStatement(query);

				int ret = pstmt.executeUpdate();

				if (ret == 0)
				{
					log.warn("ERRO NO INSERT DO ESTAGIARIO: " + nrCadastro);
					log.info("ERRO NO ESTAGIARIO: " + nrCadastro);

					return Boolean.FALSE;

				}
				else
					return Boolean.TRUE;
			}

			return result;
		}
		catch (Exception e)
		{
			log.warn("ERRO NO MÉTODO: \"insereEstagiario\"");
			log.info("ERRO NO MÉTODO: \"insereEstagiario\"");
			e.printStackTrace();
			return Boolean.FALSE;
		}
		finally
		{
			OrsegupsUtils.closeConnection(connSapiens, pstmt, null);
		}
	}

	public static Boolean insereHistoricoDispensaPonto(EntityWrapper wForm, Integer nrCadastro, Integer nrEmpresa)
	{
		Connection connSapiens = null;
		PreparedStatement pstmt = null;
		Boolean result = null;

		try
		{
			connSapiens = PersistEngine.getConnection("VETORH");

			GregorianCalendar dtAdmissao = wForm.findGenericValue("dtAdmissao");
			String dtAdmissaoStr = NeoUtils.safeOutputString(dtAdmissao != null ? new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(dtAdmissao.getTime()) : null);

			Long tipoCol = wForm.findGenericValue("tpColaborador.codigo");

			String query = "insert into R038NOT (NumEmp, TipCol, NumCad, DatNot, SeqNot, TipNot) " + "values (" + nrEmpresa + ", " + tipoCol + ", " + nrCadastro + ", '" + dtAdmissaoStr + "', 1, 11)";

			log.info(nrEmpresa + " - " + nrCadastro + " - " + query);

			if (nrCadastro != null)
			{
				pstmt = connSapiens.prepareStatement(query);

				int ret = pstmt.executeUpdate();

				if (ret == 0)
				{
					log.error("ERRO NO INSERT DO HISTORICO DE DISPENSA DE PONTO: " + nrCadastro);

					return Boolean.FALSE;

				}
				else
					return Boolean.TRUE;
			}

			return result;
		}
		catch (Exception e)
		{
			log.warn("ERRO NO MÉTODO: \"insereHistoricoDispensaPonto\"");
			log.info("ERRO NO MÉTODO: \"insereHistoricoDispensaPonto\"");
			e.printStackTrace();
			return Boolean.FALSE;
		}
		finally
		{
			OrsegupsUtils.closeConnection(connSapiens, pstmt, null);
		}
	}

	public static Boolean insereHistoricoCracha(Integer NumCad, Integer NumEmp, String DatIni)
	{

		Connection connSapiens = null;

		PreparedStatement pstData = null;

		Boolean result = null;

		try
		{

			connSapiens = PersistEngine.getConnection("VETORH");

			String NumCadS = NumCad.toString();

			String Zeros = "";

			Integer contNumCad = 8 - NumCadS.length();

			for (Integer i = 0; i < contNumCad; i++)
			{

				Zeros = Zeros + "0";

			}

			NumCadS = Zeros + NumCadS;

			String NumEmpS = NumEmp.toString();

			if (NumEmpS.length() == 1)
			{

				NumEmpS = "0" + NumEmpS;

			}

			String NumCra = "10" + NumEmpS + NumCadS;

			log.info("Cont NumCra - " + NumCad + ": " + NumCra.length());

			String query = " insert into R038HCH (TipCra, NumEmp, TipCol, NumCad, DatIni, NumCra, DatFim, StaAtu, StaAcc, " + "HorIni, ViaCra, HorFim, RecEPr) " + "values (1, " + NumEmp + ", 1, " + NumCad + ", '" + DatIni + "', " + NumCra + ", '1900-12-31', 1, 1, " + "0, 0, 0, '')";

			log.info("Query inserção histórico do crachá: " + query);

			pstData = connSapiens.prepareStatement(query);

			int ret = pstData.executeUpdate();

			log.info("Retorno :" + ret);

			if (ret == 0)
			{

				log.info("ERRO NO INSERT DO R038HCH DO COLABORADOR DE MATRÍCULA: " + NumCad);

				result = Boolean.FALSE;

			}
			else
			{

				result = Boolean.TRUE;

				log.info("INSERÇÃO DE HISTÓRICO OK - " + NumCad);

			}

			return result;

		}
		catch (Exception e)
		{

			log.warn("ERRO NO MÉTODO: \"insereHistoricoCracha\"");
			log.info("ERRO NO MÉTODO: \"insereHistoricoCracha\"");
			e.printStackTrace();
			return Boolean.FALSE;

		}
		finally
		{

			OrsegupsUtils.closeConnection(connSapiens, pstData, null);
		}

	}

	public static Boolean insereFotoCracha(Integer numcad, Integer numemp, String fotemp)
	{

		Connection connSapiens = null;

		PreparedStatement pstData = null;

		Boolean result = null;

		try
		{

			connSapiens = PersistEngine.getConnection("VETORH");

			String query = " INSERT INTO R034FOT (NumEmp, TipCol, NumCad, FotEmp) SELECT " + numemp + ", 1, " + numcad + "," + "BulkColumn FROM OPENROWSET( Bulk '" + fotemp + "', SINGLE_BLOB) AS BLOB";

			log.info("queryFoto: " + query);

			if (numcad != null)
			{

				pstData = connSapiens.prepareStatement(query);

				int ret = pstData.executeUpdate();

				if (ret == 0)
				{

					log.warn("ERRO NO INSERT DA FOTO DO COLABORADOR DE MATRÍCULA: " + numcad);

					log.info("ERRO NO INSERT DA FOTO DO COLABORADOR DE MATRÍCULA: " + numcad);

					return Boolean.FALSE;

				}
				else
				{

					return Boolean.TRUE;

				}

			}

			return result;

		}
		catch (Exception e)
		{

			log.warn("ERRO NO MÉTODO: \"inserePersonalizados\"");
			e.printStackTrace();
			return Boolean.FALSE;

		}
		finally
		{

			OrsegupsUtils.closeConnection(connSapiens, pstData, null);
		}

	}

	public static Boolean insereListaVT(Integer NumCad, Integer codEmpVT, Double numVT)
	{

		Connection connSapiens = null;

		PreparedStatement pstData = null;

		Boolean result = null;

		try
		{

			connSapiens = PersistEngine.getConnection("VETORH");

			if (codEmpVT != null && codEmpVT == 10)
			{

				String query = " update r034fun set usu_matjotur = " + numVT + " where numcad = " + NumCad;

				log.info("query VT: " + query);

				if (numVT != null)
				{

					pstData = connSapiens.prepareStatement(query);

					int ret = pstData.executeUpdate();

					if (ret == 0)
					{

						log.warn("ERRO NO UPDATE DO COLABORADOR DE MATRÍCULA: " + NumCad);
						log.info("ERRO NO UPDATE DO COLABORADOR DE MATRÍCULA: " + NumCad);

						return Boolean.FALSE;

					}
					else
					{

						return Boolean.TRUE;

					}

				}
				else
				{

					return Boolean.FALSE;

				}

			}
			else if (codEmpVT != null && codEmpVT == 20)
			{

				String query = " update r034fun set usu_matbiguacu = " + numVT + "where numcad = " + NumCad;

				if (numVT != null)
				{

					pstData = connSapiens.prepareStatement(query);

					int ret = pstData.executeUpdate();

					if (ret == 0)
					{

						log.warn("ERRO NO UPDATE DO COLABORADOR DE MATRÍCULA: " + NumCad);
						log.info("ERRO NO UPDATE DO COLABORADOR DE MATRÍCULA: " + NumCad);

						return Boolean.FALSE;

					}
					else
					{

						return Boolean.TRUE;

					}

				}
				else
				{

					return Boolean.FALSE;

				}

			}
			else if (codEmpVT != null && codEmpVT == 30)
			{

				String query = " update r034fun set usu_matestrela = " + numVT + "where numcad = " + NumCad;

				if (numVT != null)
				{

					pstData = connSapiens.prepareStatement(query);

					int ret = pstData.executeUpdate();

					if (ret == 0)
					{

						log.warn("ERRO NO UPDATE DO COLABORADOR DE MATRÍCULA: " + NumCad);
						log.info("ERRO NO UPDATE DO COLABORADOR DE MATRÍCULA: " + NumCad);

						return Boolean.FALSE;

					}
					else
					{

						return Boolean.TRUE;

					}

				}
				else
				{

					return Boolean.FALSE;

				}

			}
			else if (codEmpVT != null && codEmpVT == 40)
			{

				String query = " update r034fun set usu_matimp = " + numVT + "where numcad = " + NumCad;

				if (numVT != null)
				{

					pstData = connSapiens.prepareStatement(query);

					int ret = pstData.executeUpdate();

					if (ret == 0)
					{

						log.warn("ERRO NO UPDATE DO COLABORADOR DE MATRÍCULA: " + NumCad);
						log.info("ERRO NO UPDATE DO COLABORADOR DE MATRÍCULA: " + NumCad);

						return Boolean.FALSE;

					}
					else
					{

						return Boolean.TRUE;

					}

				}
				else
				{

					return Boolean.FALSE;

				}

			}
			else if (codEmpVT != null && codEmpVT == 50)
			{

				String query = " update r034fun set usu_matpasrap = " + numVT + "where numcad = " + NumCad;

				if (numVT != null)
				{

					pstData = connSapiens.prepareStatement(query);

					int ret = pstData.executeUpdate();

					if (ret == 0)
					{

						log.warn("ERRO NO UPDATE DO COLABORADOR DE MATRÍCULA: " + NumCad);
						log.info("ERRO NO UPDATE DO COLABORADOR DE MATRÍCULA: " + NumCad);

						return Boolean.FALSE;

					}
					else
					{

						return Boolean.TRUE;

					}

				}
				else
				{

					return Boolean.FALSE;

				}

			}
			else if (codEmpVT != null && codEmpVT == 110)
			{
				String query = " update r034fun set usu_codsic = " + numVT + "where numcad = " + NumCad;

				if (numVT != null)
				{

					pstData = connSapiens.prepareStatement(query);

					int ret = pstData.executeUpdate();

					if (ret == 0)
					{

						log.warn("ERRO NO UPDATE DO COLABORADOR DE MATRÍCULA: " + NumCad);
						log.info("ERRO NO UPDATE DO COLABORADOR DE MATRÍCULA: " + NumCad);

						return Boolean.FALSE;

					}
					else
					{

						return Boolean.TRUE;

					}

				}
				else
				{

					return Boolean.FALSE;

				}
			}
			else
			{

				return Boolean.FALSE;

			}

		}
		catch (Exception e)
		{

			log.warn("ERRO NO MÉTODO: \"inserePersonalizados\"");
			e.printStackTrace();
			return Boolean.FALSE;

		}
		finally
		{

			OrsegupsUtils.closeConnection(connSapiens, pstData, null);
		}

	}

	public NeoObject adicionaRegistroIntegracao(String registro)
	{

		NeoObject oRegistrosIntegracao = AdapterUtils.createNewEntityInstance("rhRegistroIntegracao");

		EntityWrapper wRegistrosIntegracao = new EntityWrapper(oRegistrosIntegracao);

		if (registro == null)
		{
			log.warn("REGISTRO DE INTEGRAÇÃO VEIO VAZIO");
			log.info("REGISTRO DE INTEGRAÇÃO VEIO VAZIO");
		}
		else
		{
			wRegistrosIntegracao.setValue("registro", registro);
			log.info("REGISTRO DE INTEGRAÇÃO CORRETO: " + registro);
		}

		PersistEngine.persist(oRegistrosIntegracao);

		return oRegistrosIntegracao;

	}

	public static Boolean insereSelecao(Integer numcad, Integer numemp, Integer tipcol)
	{

		Connection connSapiens = null;

		PreparedStatement pstData = null;

		Boolean result = null;

		try
		{

			connSapiens = PersistEngine.getConnection("VETORH");

			String query = " insert into r034sel (numemp, tipcol, numcad, codsel) values (" + numemp + ", " + tipcol + ", " + numcad + ", 2)";

			if (numcad != null)
			{

				pstData = connSapiens.prepareStatement(query);

				int ret = pstData.executeUpdate();

				if (ret == 0)
				{

					log.warn("ERRO NO UPDATE DO COLABORADOR DE MATRÍCULA: " + numcad);
					log.info("ERRO NO UPDATE DO COLABORADOR DE MATRÍCULA: " + numcad);

					return Boolean.FALSE;

				}
				else
				{

					return Boolean.TRUE;

				}

			}

			return result;

		}
		catch (Exception e)
		{

			log.warn("ERRO NO MÉTODO: \"insereSelecao\"");
			e.printStackTrace();
			return Boolean.FALSE;

		}
		finally
		{

			OrsegupsUtils.closeConnection(connSapiens, pstData, null);
		}

	}

	public byte[] ler_ficheiro(File fotoColab)
	{
		if (fotoColab.exists())
		{
			FileInputStream fis = null;
			try
			{
				fis = new FileInputStream(fotoColab);
			}
			catch (FileNotFoundException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			byte[] data = new byte[(int) fotoColab.length()];
			try
			{
				fis.read(data);
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try
			{
				fis.close();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return data;

		}
		else
		{
			return null;
		}
	}

	private String bytesToHex(byte[] bytes)
	{
		StringBuffer sb = new StringBuffer();

		for (byte b : bytes)
		{
			sb.append(String.format("%02X", b));
		}

		return sb.toString();
	}

	public static Boolean insereDadosVigilante(Integer NumCad, Integer NumEmp, String DtFormacao, String DtReciclagem, String OrgaoVig, String NumDrt, String DtValidadeCnv, String NumCnv, Boolean ExtTrans, Boolean ExtSegPes, Boolean cadastroCNV, Long codoem)
	{

		Connection connSapiens = null;

		PreparedStatement pstData = null;

		if (codoem == null)
		{

			codoem = 0l;
		}

		if (DtReciclagem == null)
			DtReciclagem = "1900-12-31";

		String USU_ExtTraVal = null;

		String USU_ExtSegPes = null;

		if (ExtSegPes)
		{

			USU_ExtSegPes = "S";

		}
		else
		{

			USU_ExtSegPes = "N";

		}

		if (ExtTrans)
		{

			USU_ExtTraVal = "S";

		}
		else
		{

			USU_ExtTraVal = "N";

		}

		String query = null;

		try
		{

			connSapiens = PersistEngine.getConnection("VETORH");

			if (cadastroCNV)
			{

				query = " INSERT INTO USU_TCadCur (USU_NumEmp, USU_TipCol, USU_NumCad, USU_OrgExp, USU_NumDip, USU_NumDrt, USU_ExtTraVal, " + "USU_ExtSegPes, USU_DatFor, USU_DatRec, USU_DatValCnv, USU_NuCaNaVi, USU_NumOem)" + "VALUES (" + NumEmp + ", 1, " + NumCad + ", '" + OrgaoVig + "', '" + NumDrt + "', '" + NumDrt + "', '" + USU_ExtTraVal + "', '" + USU_ExtSegPes + "', '" + DtFormacao + "', '" + DtReciclagem + "', '" + DtValidadeCnv + "', '" + NumCnv + "', " + codoem + ")";

			}
			else
			{

				query = " INSERT INTO USU_TCadCur (USU_NumEmp, USU_TipCol, USU_NumCad, USU_OrgExp, USU_NumDip, USU_NumDrt, USU_ExtTraVal, " + "USU_ExtSegPes, USU_DatFor, USU_DatRec, USU_NumOem)" + "VALUES (" + NumEmp + ", 1, " + NumCad + ", '" + OrgaoVig + "', '" + NumDrt + "', '" + NumDrt + "', '" + USU_ExtTraVal + "', '" + USU_ExtSegPes + "', '" + DtFormacao + "', '" + DtReciclagem + "', " + codoem + ")";

			}

			log.info(NumEmp + " - " + NumCad + " - Query: " + query);

			pstData = connSapiens.prepareStatement(query);

			int ret = pstData.executeUpdate();

			if (ret == 0)
			{

				log.warn("ERRO NO UPDATE DOS DADOS DO VIGILANTE: " + NumCad);
				log.info("ERRO NO UPDATE DOS DADOS DO VIGILANTE: " + NumCad);

				return Boolean.FALSE;

			}
			else
			{

				return Boolean.TRUE;

			}

		}
		catch (Exception e)
		{

			log.warn("ERRO NO MÉTODO: \"insereDadosVigilante\"");
			log.info("ERRO NO MÉTODO: \"insereDadosVigilante\"");
			e.printStackTrace();
			return Boolean.FALSE;

		}
		finally
		{

			OrsegupsUtils.closeConnection(connSapiens, pstData, null);
		}

	}

}