package com.neomind.fusion.custom.orsegups.e2doc.xml.compras;

public class Campos {
    
    private String i46;
    private String i47;
    private String i103;
    
    public String getI46() {
        return i46;
    }
    public void setI46(String i46) {
        this.i46 = i46;
    }
    public String getI47() {
        return i47;
    }
    public void setI47(String i47) {
        this.i47 = i47;
    }
    public String getI103() {
        return i103;
    }
    public void setI103(String i103) {
        this.i103 = i103;
    }
}
