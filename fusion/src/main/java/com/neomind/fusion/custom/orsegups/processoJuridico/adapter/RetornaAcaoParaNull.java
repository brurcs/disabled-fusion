package com.neomind.fusion.custom.orsegups.processoJuridico.adapter;

import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.adapter.AdapterInterface;
import com.neomind.fusion.workflow.exception.WorkflowException;

//com.neomind.fusion.custom.orsegups.processoJuridico.adapter.RetornaAcaoParaNull

public class RetornaAcaoParaNull implements AdapterInterface
{
	@Override
	public void start(Task origin, EntityWrapper processEntity, Activity activity)
	{
		try{
		    processEntity.setValue("listaAcaoPro", null);
		}catch(Exception e){
		    System.out.println("Erro na classe RetornaAcaoParaNull do fluxo J002.");
		    e.printStackTrace();
		    throw new WorkflowException(e.getMessage());
		}
	}

	@Override
	public void back(EntityWrapper processEntity, Activity activity)
	{

	}

}
