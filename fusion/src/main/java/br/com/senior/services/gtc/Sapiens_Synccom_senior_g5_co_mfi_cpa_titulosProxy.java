package br.com.senior.services.gtc;


public class Sapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy implements br.com.senior.services.gtc.Sapiens_Synccom_senior_g5_co_mfi_cpa_titulos {
  private String _endpoint = null;
  private br.com.senior.services.gtc.Sapiens_Synccom_senior_g5_co_mfi_cpa_titulos sapiens_Synccom_senior_g5_co_mfi_cpa_titulos = null;
  
  public Sapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy() {
    _initSapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy();
  }
  
  public Sapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy(String endpoint) {
    _endpoint = endpoint;
    _initSapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy();
  }
  
  private void _initSapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy() {
    try {
      sapiens_Synccom_senior_g5_co_mfi_cpa_titulos = (new br.com.senior.services.gtc.G5SeniorServicesLocator()).getsapiens_Synccom_senior_g5_co_mfi_cpa_titulosPort();
      if (sapiens_Synccom_senior_g5_co_mfi_cpa_titulos != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)sapiens_Synccom_senior_g5_co_mfi_cpa_titulos)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)sapiens_Synccom_senior_g5_co_mfi_cpa_titulos)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (sapiens_Synccom_senior_g5_co_mfi_cpa_titulos != null)
      ((javax.xml.rpc.Stub)sapiens_Synccom_senior_g5_co_mfi_cpa_titulos)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public br.com.senior.services.gtc.Sapiens_Synccom_senior_g5_co_mfi_cpa_titulos getSapiens_Synccom_senior_g5_co_mfi_cpa_titulos() {
    if (sapiens_Synccom_senior_g5_co_mfi_cpa_titulos == null)
      _initSapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy();
    return sapiens_Synccom_senior_g5_co_mfi_cpa_titulos;
  }
  
  public br.com.senior.services.gtc.TitulosConsultarTitulosAbertosCPOut consultarTitulosAbertosCP(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.gtc.TitulosConsultarTitulosAbertosCPIn parameters) throws java.rmi.RemoteException{
    if (sapiens_Synccom_senior_g5_co_mfi_cpa_titulos == null)
      _initSapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy();
    return sapiens_Synccom_senior_g5_co_mfi_cpa_titulos.consultarTitulosAbertosCP(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.gtc.TitulosConsultarTitulosAbertosCP2Out consultarTitulosAbertosCP_2(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.gtc.TitulosConsultarTitulosAbertosCP2In parameters) throws java.rmi.RemoteException{
    if (sapiens_Synccom_senior_g5_co_mfi_cpa_titulos == null)
      _initSapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy();
    return sapiens_Synccom_senior_g5_co_mfi_cpa_titulos.consultarTitulosAbertosCP_2(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.gtc.TitulosGravarTitulosCPOut gravarTitulosCP(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.gtc.TitulosGravarTitulosCPIn parameters) throws java.rmi.RemoteException{
    if (sapiens_Synccom_senior_g5_co_mfi_cpa_titulos == null)
      _initSapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy();
    return sapiens_Synccom_senior_g5_co_mfi_cpa_titulos.gravarTitulosCP(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.gtc.TitulosEntradaTitulosLoteCPOut entradaTitulosLoteCP(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.gtc.TitulosEntradaTitulosLoteCPIn parameters) throws java.rmi.RemoteException{
    if (sapiens_Synccom_senior_g5_co_mfi_cpa_titulos == null)
      _initSapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy();
    return sapiens_Synccom_senior_g5_co_mfi_cpa_titulos.entradaTitulosLoteCP(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.gtc.TitulosBaixarTitulosCPOut baixarTitulosCP(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.gtc.TitulosBaixarTitulosCPIn parameters) throws java.rmi.RemoteException{
    if (sapiens_Synccom_senior_g5_co_mfi_cpa_titulos == null)
      _initSapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy();
    return sapiens_Synccom_senior_g5_co_mfi_cpa_titulos.baixarTitulosCP(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.gtc.TitulosExcluirTitulosCPOut excluirTitulosCP(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.gtc.TitulosExcluirTitulosCPIn parameters) throws java.rmi.RemoteException{
    if (sapiens_Synccom_senior_g5_co_mfi_cpa_titulos == null)
      _initSapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy();
    return sapiens_Synccom_senior_g5_co_mfi_cpa_titulos.excluirTitulosCP(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.gtc.TitulosSubstituirTitulosCPOut substituirTitulosCP(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.gtc.TitulosSubstituirTitulosCPIn parameters) throws java.rmi.RemoteException{
    if (sapiens_Synccom_senior_g5_co_mfi_cpa_titulos == null)
      _initSapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy();
    return sapiens_Synccom_senior_g5_co_mfi_cpa_titulos.substituirTitulosCP(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.gtc.TitulosGerarBaixaAproveitamentoCPOut gerarBaixaAproveitamentoCP(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.gtc.TitulosGerarBaixaAproveitamentoCPIn parameters) throws java.rmi.RemoteException{
    if (sapiens_Synccom_senior_g5_co_mfi_cpa_titulos == null)
      _initSapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy();
    return sapiens_Synccom_senior_g5_co_mfi_cpa_titulos.gerarBaixaAproveitamentoCP(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.gtc.TitulosGerarBaixaPorLoteCPOut gerarBaixaPorLoteCP(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.gtc.TitulosGerarBaixaPorLoteCPIn parameters) throws java.rmi.RemoteException{
    if (sapiens_Synccom_senior_g5_co_mfi_cpa_titulos == null)
      _initSapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy();
    return sapiens_Synccom_senior_g5_co_mfi_cpa_titulos.gerarBaixaPorLoteCP(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.gtc.TitulosEstornoBaixaTitulosCPOut estornoBaixaTitulosCP(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.gtc.TitulosEstornoBaixaTitulosCPIn parameters) throws java.rmi.RemoteException{
    if (sapiens_Synccom_senior_g5_co_mfi_cpa_titulos == null)
      _initSapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy();
    return sapiens_Synccom_senior_g5_co_mfi_cpa_titulos.estornoBaixaTitulosCP(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.gtc.TitulosEstornoBaixaTitulosCP2Out estornoBaixaTitulosCP_2(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.gtc.TitulosEstornoBaixaTitulosCP2In parameters) throws java.rmi.RemoteException{
    if (sapiens_Synccom_senior_g5_co_mfi_cpa_titulos == null)
      _initSapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy();
    return sapiens_Synccom_senior_g5_co_mfi_cpa_titulos.estornoBaixaTitulosCP_2(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.gtc.TitulosbuscarPendentesCPOut buscarPendentesCP(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.gtc.TitulosbuscarPendentesCPIn parameters) throws java.rmi.RemoteException{
    if (sapiens_Synccom_senior_g5_co_mfi_cpa_titulos == null)
      _initSapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy();
    return sapiens_Synccom_senior_g5_co_mfi_cpa_titulos.buscarPendentesCP(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.gtc.TitulosProcessarVariacaoCambialOut processarVariacaoCambial(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.gtc.TitulosProcessarVariacaoCambialIn parameters) throws java.rmi.RemoteException{
    if (sapiens_Synccom_senior_g5_co_mfi_cpa_titulos == null)
      _initSapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy();
    return sapiens_Synccom_senior_g5_co_mfi_cpa_titulos.processarVariacaoCambial(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.gtc.TitulosConsultarTitulosCPOut consultarTitulosCP(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.gtc.TitulosConsultarTitulosCPIn parameters) throws java.rmi.RemoteException{
    if (sapiens_Synccom_senior_g5_co_mfi_cpa_titulos == null)
      _initSapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy();
    return sapiens_Synccom_senior_g5_co_mfi_cpa_titulos.consultarTitulosCP(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.gtc.TitulosConsultarTitulosCP2Out consultarTitulosCP_2(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.gtc.TitulosConsultarTitulosCP2In parameters) throws java.rmi.RemoteException{
    if (sapiens_Synccom_senior_g5_co_mfi_cpa_titulos == null)
      _initSapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy();
    return sapiens_Synccom_senior_g5_co_mfi_cpa_titulos.consultarTitulosCP_2(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.gtc.TitulosProcessarAVMOut processarAVM(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.gtc.TitulosProcessarAVMIn parameters) throws java.rmi.RemoteException{
    if (sapiens_Synccom_senior_g5_co_mfi_cpa_titulos == null)
      _initSapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy();
    return sapiens_Synccom_senior_g5_co_mfi_cpa_titulos.processarAVM(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.gtc.TitulosreprovarCPOut reprovarCP(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.gtc.TitulosreprovarCPIn parameters) throws java.rmi.RemoteException{
    if (sapiens_Synccom_senior_g5_co_mfi_cpa_titulos == null)
      _initSapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy();
    return sapiens_Synccom_senior_g5_co_mfi_cpa_titulos.reprovarCP(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.gtc.TitulosConsultarCPOut consultarCP(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.gtc.TitulosConsultarCPIn parameters) throws java.rmi.RemoteException{
    if (sapiens_Synccom_senior_g5_co_mfi_cpa_titulos == null)
      _initSapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy();
    return sapiens_Synccom_senior_g5_co_mfi_cpa_titulos.consultarCP(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.gtc.TitulosaprovarCPOut aprovarCP(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.gtc.TitulosaprovarCPIn parameters) throws java.rmi.RemoteException{
    if (sapiens_Synccom_senior_g5_co_mfi_cpa_titulos == null)
      _initSapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy();
    return sapiens_Synccom_senior_g5_co_mfi_cpa_titulos.aprovarCP(user, password, encryption, parameters);
  }
  
  public br.com.senior.services.gtc.TitulosConsultarCP2Out consultarCP_2(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.gtc.TitulosConsultarCP2In parameters) throws java.rmi.RemoteException{
    if (sapiens_Synccom_senior_g5_co_mfi_cpa_titulos == null)
      _initSapiens_Synccom_senior_g5_co_mfi_cpa_titulosProxy();
    return sapiens_Synccom_senior_g5_co_mfi_cpa_titulos.consultarCP_2(user, password, encryption, parameters);
  }
  
  
}