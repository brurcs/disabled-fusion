/**
 * ClientesCadastrarCartaoClienteOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package br.com.senior.services.co.cad.clientes;

public class ClientesCadastrarCartaoClienteOut  implements java.io.Serializable {
    private java.lang.String codBan;

    private java.lang.String codSeg;

    private java.lang.String datVal;

    private java.lang.String erroExecucao;

    private java.lang.String mensagemRetorno;

    private java.lang.String nomBan;

    private java.lang.String nomcar;

    private java.lang.String numCar;

    private java.lang.Integer tipoRetorno;

    public ClientesCadastrarCartaoClienteOut() {
    }

    public ClientesCadastrarCartaoClienteOut(
           java.lang.String codBan,
           java.lang.String codSeg,
           java.lang.String datVal,
           java.lang.String erroExecucao,
           java.lang.String mensagemRetorno,
           java.lang.String nomBan,
           java.lang.String nomcar,
           java.lang.String numCar,
           java.lang.Integer tipoRetorno) {
           this.codBan = codBan;
           this.codSeg = codSeg;
           this.datVal = datVal;
           this.erroExecucao = erroExecucao;
           this.mensagemRetorno = mensagemRetorno;
           this.nomBan = nomBan;
           this.nomcar = nomcar;
           this.numCar = numCar;
           this.tipoRetorno = tipoRetorno;
    }


    /**
     * Gets the codBan value for this ClientesCadastrarCartaoClienteOut.
     * 
     * @return codBan
     */
    public java.lang.String getCodBan() {
        return codBan;
    }


    /**
     * Sets the codBan value for this ClientesCadastrarCartaoClienteOut.
     * 
     * @param codBan
     */
    public void setCodBan(java.lang.String codBan) {
        this.codBan = codBan;
    }


    /**
     * Gets the codSeg value for this ClientesCadastrarCartaoClienteOut.
     * 
     * @return codSeg
     */
    public java.lang.String getCodSeg() {
        return codSeg;
    }


    /**
     * Sets the codSeg value for this ClientesCadastrarCartaoClienteOut.
     * 
     * @param codSeg
     */
    public void setCodSeg(java.lang.String codSeg) {
        this.codSeg = codSeg;
    }


    /**
     * Gets the datVal value for this ClientesCadastrarCartaoClienteOut.
     * 
     * @return datVal
     */
    public java.lang.String getDatVal() {
        return datVal;
    }


    /**
     * Sets the datVal value for this ClientesCadastrarCartaoClienteOut.
     * 
     * @param datVal
     */
    public void setDatVal(java.lang.String datVal) {
        this.datVal = datVal;
    }


    /**
     * Gets the erroExecucao value for this ClientesCadastrarCartaoClienteOut.
     * 
     * @return erroExecucao
     */
    public java.lang.String getErroExecucao() {
        return erroExecucao;
    }


    /**
     * Sets the erroExecucao value for this ClientesCadastrarCartaoClienteOut.
     * 
     * @param erroExecucao
     */
    public void setErroExecucao(java.lang.String erroExecucao) {
        this.erroExecucao = erroExecucao;
    }


    /**
     * Gets the mensagemRetorno value for this ClientesCadastrarCartaoClienteOut.
     * 
     * @return mensagemRetorno
     */
    public java.lang.String getMensagemRetorno() {
        return mensagemRetorno;
    }


    /**
     * Sets the mensagemRetorno value for this ClientesCadastrarCartaoClienteOut.
     * 
     * @param mensagemRetorno
     */
    public void setMensagemRetorno(java.lang.String mensagemRetorno) {
        this.mensagemRetorno = mensagemRetorno;
    }


    /**
     * Gets the nomBan value for this ClientesCadastrarCartaoClienteOut.
     * 
     * @return nomBan
     */
    public java.lang.String getNomBan() {
        return nomBan;
    }


    /**
     * Sets the nomBan value for this ClientesCadastrarCartaoClienteOut.
     * 
     * @param nomBan
     */
    public void setNomBan(java.lang.String nomBan) {
        this.nomBan = nomBan;
    }


    /**
     * Gets the nomcar value for this ClientesCadastrarCartaoClienteOut.
     * 
     * @return nomcar
     */
    public java.lang.String getNomcar() {
        return nomcar;
    }


    /**
     * Sets the nomcar value for this ClientesCadastrarCartaoClienteOut.
     * 
     * @param nomcar
     */
    public void setNomcar(java.lang.String nomcar) {
        this.nomcar = nomcar;
    }


    /**
     * Gets the numCar value for this ClientesCadastrarCartaoClienteOut.
     * 
     * @return numCar
     */
    public java.lang.String getNumCar() {
        return numCar;
    }


    /**
     * Sets the numCar value for this ClientesCadastrarCartaoClienteOut.
     * 
     * @param numCar
     */
    public void setNumCar(java.lang.String numCar) {
        this.numCar = numCar;
    }


    /**
     * Gets the tipoRetorno value for this ClientesCadastrarCartaoClienteOut.
     * 
     * @return tipoRetorno
     */
    public java.lang.Integer getTipoRetorno() {
        return tipoRetorno;
    }


    /**
     * Sets the tipoRetorno value for this ClientesCadastrarCartaoClienteOut.
     * 
     * @param tipoRetorno
     */
    public void setTipoRetorno(java.lang.Integer tipoRetorno) {
        this.tipoRetorno = tipoRetorno;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientesCadastrarCartaoClienteOut)) return false;
        ClientesCadastrarCartaoClienteOut other = (ClientesCadastrarCartaoClienteOut) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codBan==null && other.getCodBan()==null) || 
             (this.codBan!=null &&
              this.codBan.equals(other.getCodBan()))) &&
            ((this.codSeg==null && other.getCodSeg()==null) || 
             (this.codSeg!=null &&
              this.codSeg.equals(other.getCodSeg()))) &&
            ((this.datVal==null && other.getDatVal()==null) || 
             (this.datVal!=null &&
              this.datVal.equals(other.getDatVal()))) &&
            ((this.erroExecucao==null && other.getErroExecucao()==null) || 
             (this.erroExecucao!=null &&
              this.erroExecucao.equals(other.getErroExecucao()))) &&
            ((this.mensagemRetorno==null && other.getMensagemRetorno()==null) || 
             (this.mensagemRetorno!=null &&
              this.mensagemRetorno.equals(other.getMensagemRetorno()))) &&
            ((this.nomBan==null && other.getNomBan()==null) || 
             (this.nomBan!=null &&
              this.nomBan.equals(other.getNomBan()))) &&
            ((this.nomcar==null && other.getNomcar()==null) || 
             (this.nomcar!=null &&
              this.nomcar.equals(other.getNomcar()))) &&
            ((this.numCar==null && other.getNumCar()==null) || 
             (this.numCar!=null &&
              this.numCar.equals(other.getNumCar()))) &&
            ((this.tipoRetorno==null && other.getTipoRetorno()==null) || 
             (this.tipoRetorno!=null &&
              this.tipoRetorno.equals(other.getTipoRetorno())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodBan() != null) {
            _hashCode += getCodBan().hashCode();
        }
        if (getCodSeg() != null) {
            _hashCode += getCodSeg().hashCode();
        }
        if (getDatVal() != null) {
            _hashCode += getDatVal().hashCode();
        }
        if (getErroExecucao() != null) {
            _hashCode += getErroExecucao().hashCode();
        }
        if (getMensagemRetorno() != null) {
            _hashCode += getMensagemRetorno().hashCode();
        }
        if (getNomBan() != null) {
            _hashCode += getNomBan().hashCode();
        }
        if (getNomcar() != null) {
            _hashCode += getNomcar().hashCode();
        }
        if (getNumCar() != null) {
            _hashCode += getNumCar().hashCode();
        }
        if (getTipoRetorno() != null) {
            _hashCode += getTipoRetorno().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientesCadastrarCartaoClienteOut.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesCadastrarCartaoClienteOut"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codSeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codSeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datVal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datVal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("erroExecucao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "erroExecucao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mensagemRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mensagemRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomcar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomcar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipoRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
