/**
 * DependentesPeriodosIRIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.dependentes;

public class DependentesPeriodosIRIn  implements java.io.Serializable {
    private java.lang.Integer codDep;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.Integer numCad;

    private java.lang.Integer numEmp;

    private br.com.senior.services.vetorh.dependentes.DependentesPeriodosIRInPeriodosIr[] periodosIr;

    private java.lang.Integer tipCol;

    public DependentesPeriodosIRIn() {
    }

    public DependentesPeriodosIRIn(
           java.lang.Integer codDep,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.Integer numCad,
           java.lang.Integer numEmp,
           br.com.senior.services.vetorh.dependentes.DependentesPeriodosIRInPeriodosIr[] periodosIr,
           java.lang.Integer tipCol) {
           this.codDep = codDep;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.numCad = numCad;
           this.numEmp = numEmp;
           this.periodosIr = periodosIr;
           this.tipCol = tipCol;
    }


    /**
     * Gets the codDep value for this DependentesPeriodosIRIn.
     * 
     * @return codDep
     */
    public java.lang.Integer getCodDep() {
        return codDep;
    }


    /**
     * Sets the codDep value for this DependentesPeriodosIRIn.
     * 
     * @param codDep
     */
    public void setCodDep(java.lang.Integer codDep) {
        this.codDep = codDep;
    }


    /**
     * Gets the flowInstanceID value for this DependentesPeriodosIRIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this DependentesPeriodosIRIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this DependentesPeriodosIRIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this DependentesPeriodosIRIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the numCad value for this DependentesPeriodosIRIn.
     * 
     * @return numCad
     */
    public java.lang.Integer getNumCad() {
        return numCad;
    }


    /**
     * Sets the numCad value for this DependentesPeriodosIRIn.
     * 
     * @param numCad
     */
    public void setNumCad(java.lang.Integer numCad) {
        this.numCad = numCad;
    }


    /**
     * Gets the numEmp value for this DependentesPeriodosIRIn.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this DependentesPeriodosIRIn.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the periodosIr value for this DependentesPeriodosIRIn.
     * 
     * @return periodosIr
     */
    public br.com.senior.services.vetorh.dependentes.DependentesPeriodosIRInPeriodosIr[] getPeriodosIr() {
        return periodosIr;
    }


    /**
     * Sets the periodosIr value for this DependentesPeriodosIRIn.
     * 
     * @param periodosIr
     */
    public void setPeriodosIr(br.com.senior.services.vetorh.dependentes.DependentesPeriodosIRInPeriodosIr[] periodosIr) {
        this.periodosIr = periodosIr;
    }

    public br.com.senior.services.vetorh.dependentes.DependentesPeriodosIRInPeriodosIr getPeriodosIr(int i) {
        return this.periodosIr[i];
    }

    public void setPeriodosIr(int i, br.com.senior.services.vetorh.dependentes.DependentesPeriodosIRInPeriodosIr _value) {
        this.periodosIr[i] = _value;
    }


    /**
     * Gets the tipCol value for this DependentesPeriodosIRIn.
     * 
     * @return tipCol
     */
    public java.lang.Integer getTipCol() {
        return tipCol;
    }


    /**
     * Sets the tipCol value for this DependentesPeriodosIRIn.
     * 
     * @param tipCol
     */
    public void setTipCol(java.lang.Integer tipCol) {
        this.tipCol = tipCol;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DependentesPeriodosIRIn)) return false;
        DependentesPeriodosIRIn other = (DependentesPeriodosIRIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codDep==null && other.getCodDep()==null) || 
             (this.codDep!=null &&
              this.codDep.equals(other.getCodDep()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.numCad==null && other.getNumCad()==null) || 
             (this.numCad!=null &&
              this.numCad.equals(other.getNumCad()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.periodosIr==null && other.getPeriodosIr()==null) || 
             (this.periodosIr!=null &&
              java.util.Arrays.equals(this.periodosIr, other.getPeriodosIr()))) &&
            ((this.tipCol==null && other.getTipCol()==null) || 
             (this.tipCol!=null &&
              this.tipCol.equals(other.getTipCol())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodDep() != null) {
            _hashCode += getCodDep().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getNumCad() != null) {
            _hashCode += getNumCad().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getPeriodosIr() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPeriodosIr());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPeriodosIr(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTipCol() != null) {
            _hashCode += getTipCol().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DependentesPeriodosIRIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "dependentesPeriodosIRIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codDep");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codDep"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("periodosIr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "periodosIr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "dependentesPeriodosIRInPeriodosIr"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
