/**
 * HistoricosContratoIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.historico;

public class HistoricosContratoIn  implements java.io.Serializable {
    private java.lang.Integer codOem;

    private java.lang.String datAlt;

    private java.lang.String datFim;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.Integer numCad;

    private java.lang.String numCon;

    private java.lang.Integer numEmp;

    private java.lang.Integer tipCol;

    private java.lang.String tipOpe;

    public HistoricosContratoIn() {
    }

    public HistoricosContratoIn(
           java.lang.Integer codOem,
           java.lang.String datAlt,
           java.lang.String datFim,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.Integer numCad,
           java.lang.String numCon,
           java.lang.Integer numEmp,
           java.lang.Integer tipCol,
           java.lang.String tipOpe) {
           this.codOem = codOem;
           this.datAlt = datAlt;
           this.datFim = datFim;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.numCad = numCad;
           this.numCon = numCon;
           this.numEmp = numEmp;
           this.tipCol = tipCol;
           this.tipOpe = tipOpe;
    }


    /**
     * Gets the codOem value for this HistoricosContratoIn.
     * 
     * @return codOem
     */
    public java.lang.Integer getCodOem() {
        return codOem;
    }


    /**
     * Sets the codOem value for this HistoricosContratoIn.
     * 
     * @param codOem
     */
    public void setCodOem(java.lang.Integer codOem) {
        this.codOem = codOem;
    }


    /**
     * Gets the datAlt value for this HistoricosContratoIn.
     * 
     * @return datAlt
     */
    public java.lang.String getDatAlt() {
        return datAlt;
    }


    /**
     * Sets the datAlt value for this HistoricosContratoIn.
     * 
     * @param datAlt
     */
    public void setDatAlt(java.lang.String datAlt) {
        this.datAlt = datAlt;
    }


    /**
     * Gets the datFim value for this HistoricosContratoIn.
     * 
     * @return datFim
     */
    public java.lang.String getDatFim() {
        return datFim;
    }


    /**
     * Sets the datFim value for this HistoricosContratoIn.
     * 
     * @param datFim
     */
    public void setDatFim(java.lang.String datFim) {
        this.datFim = datFim;
    }


    /**
     * Gets the flowInstanceID value for this HistoricosContratoIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this HistoricosContratoIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this HistoricosContratoIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this HistoricosContratoIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the numCad value for this HistoricosContratoIn.
     * 
     * @return numCad
     */
    public java.lang.Integer getNumCad() {
        return numCad;
    }


    /**
     * Sets the numCad value for this HistoricosContratoIn.
     * 
     * @param numCad
     */
    public void setNumCad(java.lang.Integer numCad) {
        this.numCad = numCad;
    }


    /**
     * Gets the numCon value for this HistoricosContratoIn.
     * 
     * @return numCon
     */
    public java.lang.String getNumCon() {
        return numCon;
    }


    /**
     * Sets the numCon value for this HistoricosContratoIn.
     * 
     * @param numCon
     */
    public void setNumCon(java.lang.String numCon) {
        this.numCon = numCon;
    }


    /**
     * Gets the numEmp value for this HistoricosContratoIn.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this HistoricosContratoIn.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the tipCol value for this HistoricosContratoIn.
     * 
     * @return tipCol
     */
    public java.lang.Integer getTipCol() {
        return tipCol;
    }


    /**
     * Sets the tipCol value for this HistoricosContratoIn.
     * 
     * @param tipCol
     */
    public void setTipCol(java.lang.Integer tipCol) {
        this.tipCol = tipCol;
    }


    /**
     * Gets the tipOpe value for this HistoricosContratoIn.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this HistoricosContratoIn.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HistoricosContratoIn)) return false;
        HistoricosContratoIn other = (HistoricosContratoIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codOem==null && other.getCodOem()==null) || 
             (this.codOem!=null &&
              this.codOem.equals(other.getCodOem()))) &&
            ((this.datAlt==null && other.getDatAlt()==null) || 
             (this.datAlt!=null &&
              this.datAlt.equals(other.getDatAlt()))) &&
            ((this.datFim==null && other.getDatFim()==null) || 
             (this.datFim!=null &&
              this.datFim.equals(other.getDatFim()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.numCad==null && other.getNumCad()==null) || 
             (this.numCad!=null &&
              this.numCad.equals(other.getNumCad()))) &&
            ((this.numCon==null && other.getNumCon()==null) || 
             (this.numCon!=null &&
              this.numCon.equals(other.getNumCon()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.tipCol==null && other.getTipCol()==null) || 
             (this.tipCol!=null &&
              this.tipCol.equals(other.getTipCol()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodOem() != null) {
            _hashCode += getCodOem().hashCode();
        }
        if (getDatAlt() != null) {
            _hashCode += getDatAlt().hashCode();
        }
        if (getDatFim() != null) {
            _hashCode += getDatFim().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getNumCad() != null) {
            _hashCode += getNumCad().hashCode();
        }
        if (getNumCon() != null) {
            _hashCode += getNumCon().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getTipCol() != null) {
            _hashCode += getTipCol().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HistoricosContratoIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "historicosContratoIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codOem");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codOem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datFim");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datFim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
