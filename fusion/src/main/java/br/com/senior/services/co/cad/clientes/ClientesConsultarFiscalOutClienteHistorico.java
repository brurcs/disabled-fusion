/**
 * ClientesConsultarFiscalOutClienteHistorico.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package br.com.senior.services.co.cad.clientes;

public class ClientesConsultarFiscalOutClienteHistorico  implements java.io.Serializable {
    private br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutClienteHistoricoCampoUsuarioDefinicoes[] campoUsuarioDefinicoes;

    private java.lang.String cifFob;

    private java.lang.Integer codCli;

    private java.lang.Integer codEmp;

    private java.lang.Integer codFil;

    private java.lang.String codStr;

    private java.lang.String codTic;

    private java.lang.String codTrd;

    private java.lang.String conFin;

    private java.lang.String indPre;

    private java.lang.Integer motDes;

    private java.lang.Double perDif;

    private java.lang.Double perIss;

    public ClientesConsultarFiscalOutClienteHistorico() {
    }

    public ClientesConsultarFiscalOutClienteHistorico(
           br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutClienteHistoricoCampoUsuarioDefinicoes[] campoUsuarioDefinicoes,
           java.lang.String cifFob,
           java.lang.Integer codCli,
           java.lang.Integer codEmp,
           java.lang.Integer codFil,
           java.lang.String codStr,
           java.lang.String codTic,
           java.lang.String codTrd,
           java.lang.String conFin,
           java.lang.String indPre,
           java.lang.Integer motDes,
           java.lang.Double perDif,
           java.lang.Double perIss) {
           this.campoUsuarioDefinicoes = campoUsuarioDefinicoes;
           this.cifFob = cifFob;
           this.codCli = codCli;
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.codStr = codStr;
           this.codTic = codTic;
           this.codTrd = codTrd;
           this.conFin = conFin;
           this.indPre = indPre;
           this.motDes = motDes;
           this.perDif = perDif;
           this.perIss = perIss;
    }


    /**
     * Gets the campoUsuarioDefinicoes value for this ClientesConsultarFiscalOutClienteHistorico.
     * 
     * @return campoUsuarioDefinicoes
     */
    public br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutClienteHistoricoCampoUsuarioDefinicoes[] getCampoUsuarioDefinicoes() {
        return campoUsuarioDefinicoes;
    }


    /**
     * Sets the campoUsuarioDefinicoes value for this ClientesConsultarFiscalOutClienteHistorico.
     * 
     * @param campoUsuarioDefinicoes
     */
    public void setCampoUsuarioDefinicoes(br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutClienteHistoricoCampoUsuarioDefinicoes[] campoUsuarioDefinicoes) {
        this.campoUsuarioDefinicoes = campoUsuarioDefinicoes;
    }

    public br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutClienteHistoricoCampoUsuarioDefinicoes getCampoUsuarioDefinicoes(int i) {
        return this.campoUsuarioDefinicoes[i];
    }

    public void setCampoUsuarioDefinicoes(int i, br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutClienteHistoricoCampoUsuarioDefinicoes _value) {
        this.campoUsuarioDefinicoes[i] = _value;
    }


    /**
     * Gets the cifFob value for this ClientesConsultarFiscalOutClienteHistorico.
     * 
     * @return cifFob
     */
    public java.lang.String getCifFob() {
        return cifFob;
    }


    /**
     * Sets the cifFob value for this ClientesConsultarFiscalOutClienteHistorico.
     * 
     * @param cifFob
     */
    public void setCifFob(java.lang.String cifFob) {
        this.cifFob = cifFob;
    }


    /**
     * Gets the codCli value for this ClientesConsultarFiscalOutClienteHistorico.
     * 
     * @return codCli
     */
    public java.lang.Integer getCodCli() {
        return codCli;
    }


    /**
     * Sets the codCli value for this ClientesConsultarFiscalOutClienteHistorico.
     * 
     * @param codCli
     */
    public void setCodCli(java.lang.Integer codCli) {
        this.codCli = codCli;
    }


    /**
     * Gets the codEmp value for this ClientesConsultarFiscalOutClienteHistorico.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this ClientesConsultarFiscalOutClienteHistorico.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this ClientesConsultarFiscalOutClienteHistorico.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this ClientesConsultarFiscalOutClienteHistorico.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codStr value for this ClientesConsultarFiscalOutClienteHistorico.
     * 
     * @return codStr
     */
    public java.lang.String getCodStr() {
        return codStr;
    }


    /**
     * Sets the codStr value for this ClientesConsultarFiscalOutClienteHistorico.
     * 
     * @param codStr
     */
    public void setCodStr(java.lang.String codStr) {
        this.codStr = codStr;
    }


    /**
     * Gets the codTic value for this ClientesConsultarFiscalOutClienteHistorico.
     * 
     * @return codTic
     */
    public java.lang.String getCodTic() {
        return codTic;
    }


    /**
     * Sets the codTic value for this ClientesConsultarFiscalOutClienteHistorico.
     * 
     * @param codTic
     */
    public void setCodTic(java.lang.String codTic) {
        this.codTic = codTic;
    }


    /**
     * Gets the codTrd value for this ClientesConsultarFiscalOutClienteHistorico.
     * 
     * @return codTrd
     */
    public java.lang.String getCodTrd() {
        return codTrd;
    }


    /**
     * Sets the codTrd value for this ClientesConsultarFiscalOutClienteHistorico.
     * 
     * @param codTrd
     */
    public void setCodTrd(java.lang.String codTrd) {
        this.codTrd = codTrd;
    }


    /**
     * Gets the conFin value for this ClientesConsultarFiscalOutClienteHistorico.
     * 
     * @return conFin
     */
    public java.lang.String getConFin() {
        return conFin;
    }


    /**
     * Sets the conFin value for this ClientesConsultarFiscalOutClienteHistorico.
     * 
     * @param conFin
     */
    public void setConFin(java.lang.String conFin) {
        this.conFin = conFin;
    }


    /**
     * Gets the indPre value for this ClientesConsultarFiscalOutClienteHistorico.
     * 
     * @return indPre
     */
    public java.lang.String getIndPre() {
        return indPre;
    }


    /**
     * Sets the indPre value for this ClientesConsultarFiscalOutClienteHistorico.
     * 
     * @param indPre
     */
    public void setIndPre(java.lang.String indPre) {
        this.indPre = indPre;
    }


    /**
     * Gets the motDes value for this ClientesConsultarFiscalOutClienteHistorico.
     * 
     * @return motDes
     */
    public java.lang.Integer getMotDes() {
        return motDes;
    }


    /**
     * Sets the motDes value for this ClientesConsultarFiscalOutClienteHistorico.
     * 
     * @param motDes
     */
    public void setMotDes(java.lang.Integer motDes) {
        this.motDes = motDes;
    }


    /**
     * Gets the perDif value for this ClientesConsultarFiscalOutClienteHistorico.
     * 
     * @return perDif
     */
    public java.lang.Double getPerDif() {
        return perDif;
    }


    /**
     * Sets the perDif value for this ClientesConsultarFiscalOutClienteHistorico.
     * 
     * @param perDif
     */
    public void setPerDif(java.lang.Double perDif) {
        this.perDif = perDif;
    }


    /**
     * Gets the perIss value for this ClientesConsultarFiscalOutClienteHistorico.
     * 
     * @return perIss
     */
    public java.lang.Double getPerIss() {
        return perIss;
    }


    /**
     * Sets the perIss value for this ClientesConsultarFiscalOutClienteHistorico.
     * 
     * @param perIss
     */
    public void setPerIss(java.lang.Double perIss) {
        this.perIss = perIss;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientesConsultarFiscalOutClienteHistorico)) return false;
        ClientesConsultarFiscalOutClienteHistorico other = (ClientesConsultarFiscalOutClienteHistorico) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.campoUsuarioDefinicoes==null && other.getCampoUsuarioDefinicoes()==null) || 
             (this.campoUsuarioDefinicoes!=null &&
              java.util.Arrays.equals(this.campoUsuarioDefinicoes, other.getCampoUsuarioDefinicoes()))) &&
            ((this.cifFob==null && other.getCifFob()==null) || 
             (this.cifFob!=null &&
              this.cifFob.equals(other.getCifFob()))) &&
            ((this.codCli==null && other.getCodCli()==null) || 
             (this.codCli!=null &&
              this.codCli.equals(other.getCodCli()))) &&
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codStr==null && other.getCodStr()==null) || 
             (this.codStr!=null &&
              this.codStr.equals(other.getCodStr()))) &&
            ((this.codTic==null && other.getCodTic()==null) || 
             (this.codTic!=null &&
              this.codTic.equals(other.getCodTic()))) &&
            ((this.codTrd==null && other.getCodTrd()==null) || 
             (this.codTrd!=null &&
              this.codTrd.equals(other.getCodTrd()))) &&
            ((this.conFin==null && other.getConFin()==null) || 
             (this.conFin!=null &&
              this.conFin.equals(other.getConFin()))) &&
            ((this.indPre==null && other.getIndPre()==null) || 
             (this.indPre!=null &&
              this.indPre.equals(other.getIndPre()))) &&
            ((this.motDes==null && other.getMotDes()==null) || 
             (this.motDes!=null &&
              this.motDes.equals(other.getMotDes()))) &&
            ((this.perDif==null && other.getPerDif()==null) || 
             (this.perDif!=null &&
              this.perDif.equals(other.getPerDif()))) &&
            ((this.perIss==null && other.getPerIss()==null) || 
             (this.perIss!=null &&
              this.perIss.equals(other.getPerIss())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCampoUsuarioDefinicoes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCampoUsuarioDefinicoes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCampoUsuarioDefinicoes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCifFob() != null) {
            _hashCode += getCifFob().hashCode();
        }
        if (getCodCli() != null) {
            _hashCode += getCodCli().hashCode();
        }
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodStr() != null) {
            _hashCode += getCodStr().hashCode();
        }
        if (getCodTic() != null) {
            _hashCode += getCodTic().hashCode();
        }
        if (getCodTrd() != null) {
            _hashCode += getCodTrd().hashCode();
        }
        if (getConFin() != null) {
            _hashCode += getConFin().hashCode();
        }
        if (getIndPre() != null) {
            _hashCode += getIndPre().hashCode();
        }
        if (getMotDes() != null) {
            _hashCode += getMotDes().hashCode();
        }
        if (getPerDif() != null) {
            _hashCode += getPerDif().hashCode();
        }
        if (getPerIss() != null) {
            _hashCode += getPerIss().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientesConsultarFiscalOutClienteHistorico.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesConsultarFiscalOutClienteHistorico"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("campoUsuarioDefinicoes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "campoUsuarioDefinicoes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesConsultarFiscalOutClienteHistoricoCampoUsuarioDefinicoes"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cifFob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cifFob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codStr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codStr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTic");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTic"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTrd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTrd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conFin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "conFin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indPre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indPre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("motDes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "motDes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDif");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDif"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perIss");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perIss"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
