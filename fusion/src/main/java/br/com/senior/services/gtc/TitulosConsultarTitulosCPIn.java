/**
 * TitulosConsultarTitulosCPIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.gtc;

public class TitulosConsultarTitulosCPIn  implements java.io.Serializable {
    private java.lang.String codEmp;

    private java.lang.String codFil;

    private java.lang.String codFor;

    private java.lang.String dataBuild;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    public TitulosConsultarTitulosCPIn() {
    }

    public TitulosConsultarTitulosCPIn(
           java.lang.String codEmp,
           java.lang.String codFil,
           java.lang.String codFor,
           java.lang.String dataBuild,
           java.lang.String flowInstanceID,
           java.lang.String flowName) {
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.codFor = codFor;
           this.dataBuild = dataBuild;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
    }


    /**
     * Gets the codEmp value for this TitulosConsultarTitulosCPIn.
     * 
     * @return codEmp
     */
    public java.lang.String getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this TitulosConsultarTitulosCPIn.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.String codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this TitulosConsultarTitulosCPIn.
     * 
     * @return codFil
     */
    public java.lang.String getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this TitulosConsultarTitulosCPIn.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.String codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codFor value for this TitulosConsultarTitulosCPIn.
     * 
     * @return codFor
     */
    public java.lang.String getCodFor() {
        return codFor;
    }


    /**
     * Sets the codFor value for this TitulosConsultarTitulosCPIn.
     * 
     * @param codFor
     */
    public void setCodFor(java.lang.String codFor) {
        this.codFor = codFor;
    }


    /**
     * Gets the dataBuild value for this TitulosConsultarTitulosCPIn.
     * 
     * @return dataBuild
     */
    public java.lang.String getDataBuild() {
        return dataBuild;
    }


    /**
     * Sets the dataBuild value for this TitulosConsultarTitulosCPIn.
     * 
     * @param dataBuild
     */
    public void setDataBuild(java.lang.String dataBuild) {
        this.dataBuild = dataBuild;
    }


    /**
     * Gets the flowInstanceID value for this TitulosConsultarTitulosCPIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this TitulosConsultarTitulosCPIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this TitulosConsultarTitulosCPIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this TitulosConsultarTitulosCPIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosConsultarTitulosCPIn)) return false;
        TitulosConsultarTitulosCPIn other = (TitulosConsultarTitulosCPIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codFor==null && other.getCodFor()==null) || 
             (this.codFor!=null &&
              this.codFor.equals(other.getCodFor()))) &&
            ((this.dataBuild==null && other.getDataBuild()==null) || 
             (this.dataBuild!=null &&
              this.dataBuild.equals(other.getDataBuild()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodFor() != null) {
            _hashCode += getCodFor().hashCode();
        }
        if (getDataBuild() != null) {
            _hashCode += getDataBuild().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosConsultarTitulosCPIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosConsultarTitulosCPIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataBuild");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dataBuild"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
