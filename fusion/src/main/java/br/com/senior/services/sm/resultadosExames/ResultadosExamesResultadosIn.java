/**
 * ResultadosExamesResultadosIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.sm.resultadosExames;

public class ResultadosExamesResultadosIn  implements java.io.Serializable {
    private java.lang.Integer codAte;

    private java.lang.Integer codExa;

    private java.lang.Integer codFic;

    private java.lang.Integer codOem;

    private java.lang.Integer codPar;

    private java.lang.String conPPP;

    private java.lang.String datRes;

    private java.lang.String datSol;

    private java.lang.String evoRes;

    private java.lang.String expExc;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String norAlt;

    private java.lang.Integer numEmp;

    private java.lang.Integer oemExa;

    private java.lang.Integer oriExa;

    private java.lang.String sitExa;

    private java.lang.String sitRes;

    private java.lang.String sobAve;

    private java.lang.String tipExa;

    private java.lang.String tipOpe;

    private br.com.senior.services.sm.resultadosExames.ResultadosExamesResultadosInWGD108RER[] WGD108RER;

    public ResultadosExamesResultadosIn() {
    }

    public ResultadosExamesResultadosIn(
           java.lang.Integer codAte,
           java.lang.Integer codExa,
           java.lang.Integer codFic,
           java.lang.Integer codOem,
           java.lang.Integer codPar,
           java.lang.String conPPP,
           java.lang.String datRes,
           java.lang.String datSol,
           java.lang.String evoRes,
           java.lang.String expExc,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String norAlt,
           java.lang.Integer numEmp,
           java.lang.Integer oemExa,
           java.lang.Integer oriExa,
           java.lang.String sitExa,
           java.lang.String sitRes,
           java.lang.String sobAve,
           java.lang.String tipExa,
           java.lang.String tipOpe,
           br.com.senior.services.sm.resultadosExames.ResultadosExamesResultadosInWGD108RER[] WGD108RER) {
           this.codAte = codAte;
           this.codExa = codExa;
           this.codFic = codFic;
           this.codOem = codOem;
           this.codPar = codPar;
           this.conPPP = conPPP;
           this.datRes = datRes;
           this.datSol = datSol;
           this.evoRes = evoRes;
           this.expExc = expExc;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.norAlt = norAlt;
           this.numEmp = numEmp;
           this.oemExa = oemExa;
           this.oriExa = oriExa;
           this.sitExa = sitExa;
           this.sitRes = sitRes;
           this.sobAve = sobAve;
           this.tipExa = tipExa;
           this.tipOpe = tipOpe;
           this.WGD108RER = WGD108RER;
    }


    /**
     * Gets the codAte value for this ResultadosExamesResultadosIn.
     * 
     * @return codAte
     */
    public java.lang.Integer getCodAte() {
        return codAte;
    }


    /**
     * Sets the codAte value for this ResultadosExamesResultadosIn.
     * 
     * @param codAte
     */
    public void setCodAte(java.lang.Integer codAte) {
        this.codAte = codAte;
    }


    /**
     * Gets the codExa value for this ResultadosExamesResultadosIn.
     * 
     * @return codExa
     */
    public java.lang.Integer getCodExa() {
        return codExa;
    }


    /**
     * Sets the codExa value for this ResultadosExamesResultadosIn.
     * 
     * @param codExa
     */
    public void setCodExa(java.lang.Integer codExa) {
        this.codExa = codExa;
    }


    /**
     * Gets the codFic value for this ResultadosExamesResultadosIn.
     * 
     * @return codFic
     */
    public java.lang.Integer getCodFic() {
        return codFic;
    }


    /**
     * Sets the codFic value for this ResultadosExamesResultadosIn.
     * 
     * @param codFic
     */
    public void setCodFic(java.lang.Integer codFic) {
        this.codFic = codFic;
    }


    /**
     * Gets the codOem value for this ResultadosExamesResultadosIn.
     * 
     * @return codOem
     */
    public java.lang.Integer getCodOem() {
        return codOem;
    }


    /**
     * Sets the codOem value for this ResultadosExamesResultadosIn.
     * 
     * @param codOem
     */
    public void setCodOem(java.lang.Integer codOem) {
        this.codOem = codOem;
    }


    /**
     * Gets the codPar value for this ResultadosExamesResultadosIn.
     * 
     * @return codPar
     */
    public java.lang.Integer getCodPar() {
        return codPar;
    }


    /**
     * Sets the codPar value for this ResultadosExamesResultadosIn.
     * 
     * @param codPar
     */
    public void setCodPar(java.lang.Integer codPar) {
        this.codPar = codPar;
    }


    /**
     * Gets the conPPP value for this ResultadosExamesResultadosIn.
     * 
     * @return conPPP
     */
    public java.lang.String getConPPP() {
        return conPPP;
    }


    /**
     * Sets the conPPP value for this ResultadosExamesResultadosIn.
     * 
     * @param conPPP
     */
    public void setConPPP(java.lang.String conPPP) {
        this.conPPP = conPPP;
    }


    /**
     * Gets the datRes value for this ResultadosExamesResultadosIn.
     * 
     * @return datRes
     */
    public java.lang.String getDatRes() {
        return datRes;
    }


    /**
     * Sets the datRes value for this ResultadosExamesResultadosIn.
     * 
     * @param datRes
     */
    public void setDatRes(java.lang.String datRes) {
        this.datRes = datRes;
    }


    /**
     * Gets the datSol value for this ResultadosExamesResultadosIn.
     * 
     * @return datSol
     */
    public java.lang.String getDatSol() {
        return datSol;
    }


    /**
     * Sets the datSol value for this ResultadosExamesResultadosIn.
     * 
     * @param datSol
     */
    public void setDatSol(java.lang.String datSol) {
        this.datSol = datSol;
    }


    /**
     * Gets the evoRes value for this ResultadosExamesResultadosIn.
     * 
     * @return evoRes
     */
    public java.lang.String getEvoRes() {
        return evoRes;
    }


    /**
     * Sets the evoRes value for this ResultadosExamesResultadosIn.
     * 
     * @param evoRes
     */
    public void setEvoRes(java.lang.String evoRes) {
        this.evoRes = evoRes;
    }


    /**
     * Gets the expExc value for this ResultadosExamesResultadosIn.
     * 
     * @return expExc
     */
    public java.lang.String getExpExc() {
        return expExc;
    }


    /**
     * Sets the expExc value for this ResultadosExamesResultadosIn.
     * 
     * @param expExc
     */
    public void setExpExc(java.lang.String expExc) {
        this.expExc = expExc;
    }


    /**
     * Gets the flowInstanceID value for this ResultadosExamesResultadosIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this ResultadosExamesResultadosIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this ResultadosExamesResultadosIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this ResultadosExamesResultadosIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the norAlt value for this ResultadosExamesResultadosIn.
     * 
     * @return norAlt
     */
    public java.lang.String getNorAlt() {
        return norAlt;
    }


    /**
     * Sets the norAlt value for this ResultadosExamesResultadosIn.
     * 
     * @param norAlt
     */
    public void setNorAlt(java.lang.String norAlt) {
        this.norAlt = norAlt;
    }


    /**
     * Gets the numEmp value for this ResultadosExamesResultadosIn.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this ResultadosExamesResultadosIn.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the oemExa value for this ResultadosExamesResultadosIn.
     * 
     * @return oemExa
     */
    public java.lang.Integer getOemExa() {
        return oemExa;
    }


    /**
     * Sets the oemExa value for this ResultadosExamesResultadosIn.
     * 
     * @param oemExa
     */
    public void setOemExa(java.lang.Integer oemExa) {
        this.oemExa = oemExa;
    }


    /**
     * Gets the oriExa value for this ResultadosExamesResultadosIn.
     * 
     * @return oriExa
     */
    public java.lang.Integer getOriExa() {
        return oriExa;
    }


    /**
     * Sets the oriExa value for this ResultadosExamesResultadosIn.
     * 
     * @param oriExa
     */
    public void setOriExa(java.lang.Integer oriExa) {
        this.oriExa = oriExa;
    }


    /**
     * Gets the sitExa value for this ResultadosExamesResultadosIn.
     * 
     * @return sitExa
     */
    public java.lang.String getSitExa() {
        return sitExa;
    }


    /**
     * Sets the sitExa value for this ResultadosExamesResultadosIn.
     * 
     * @param sitExa
     */
    public void setSitExa(java.lang.String sitExa) {
        this.sitExa = sitExa;
    }


    /**
     * Gets the sitRes value for this ResultadosExamesResultadosIn.
     * 
     * @return sitRes
     */
    public java.lang.String getSitRes() {
        return sitRes;
    }


    /**
     * Sets the sitRes value for this ResultadosExamesResultadosIn.
     * 
     * @param sitRes
     */
    public void setSitRes(java.lang.String sitRes) {
        this.sitRes = sitRes;
    }


    /**
     * Gets the sobAve value for this ResultadosExamesResultadosIn.
     * 
     * @return sobAve
     */
    public java.lang.String getSobAve() {
        return sobAve;
    }


    /**
     * Sets the sobAve value for this ResultadosExamesResultadosIn.
     * 
     * @param sobAve
     */
    public void setSobAve(java.lang.String sobAve) {
        this.sobAve = sobAve;
    }


    /**
     * Gets the tipExa value for this ResultadosExamesResultadosIn.
     * 
     * @return tipExa
     */
    public java.lang.String getTipExa() {
        return tipExa;
    }


    /**
     * Sets the tipExa value for this ResultadosExamesResultadosIn.
     * 
     * @param tipExa
     */
    public void setTipExa(java.lang.String tipExa) {
        this.tipExa = tipExa;
    }


    /**
     * Gets the tipOpe value for this ResultadosExamesResultadosIn.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this ResultadosExamesResultadosIn.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }


    /**
     * Gets the WGD108RER value for this ResultadosExamesResultadosIn.
     * 
     * @return WGD108RER
     */
    public br.com.senior.services.sm.resultadosExames.ResultadosExamesResultadosInWGD108RER[] getWGD108RER() {
        return WGD108RER;
    }


    /**
     * Sets the WGD108RER value for this ResultadosExamesResultadosIn.
     * 
     * @param WGD108RER
     */
    public void setWGD108RER(br.com.senior.services.sm.resultadosExames.ResultadosExamesResultadosInWGD108RER[] WGD108RER) {
        this.WGD108RER = WGD108RER;
    }

    public br.com.senior.services.sm.resultadosExames.ResultadosExamesResultadosInWGD108RER getWGD108RER(int i) {
        return this.WGD108RER[i];
    }

    public void setWGD108RER(int i, br.com.senior.services.sm.resultadosExames.ResultadosExamesResultadosInWGD108RER _value) {
        this.WGD108RER[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ResultadosExamesResultadosIn)) return false;
        ResultadosExamesResultadosIn other = (ResultadosExamesResultadosIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codAte==null && other.getCodAte()==null) || 
             (this.codAte!=null &&
              this.codAte.equals(other.getCodAte()))) &&
            ((this.codExa==null && other.getCodExa()==null) || 
             (this.codExa!=null &&
              this.codExa.equals(other.getCodExa()))) &&
            ((this.codFic==null && other.getCodFic()==null) || 
             (this.codFic!=null &&
              this.codFic.equals(other.getCodFic()))) &&
            ((this.codOem==null && other.getCodOem()==null) || 
             (this.codOem!=null &&
              this.codOem.equals(other.getCodOem()))) &&
            ((this.codPar==null && other.getCodPar()==null) || 
             (this.codPar!=null &&
              this.codPar.equals(other.getCodPar()))) &&
            ((this.conPPP==null && other.getConPPP()==null) || 
             (this.conPPP!=null &&
              this.conPPP.equals(other.getConPPP()))) &&
            ((this.datRes==null && other.getDatRes()==null) || 
             (this.datRes!=null &&
              this.datRes.equals(other.getDatRes()))) &&
            ((this.datSol==null && other.getDatSol()==null) || 
             (this.datSol!=null &&
              this.datSol.equals(other.getDatSol()))) &&
            ((this.evoRes==null && other.getEvoRes()==null) || 
             (this.evoRes!=null &&
              this.evoRes.equals(other.getEvoRes()))) &&
            ((this.expExc==null && other.getExpExc()==null) || 
             (this.expExc!=null &&
              this.expExc.equals(other.getExpExc()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.norAlt==null && other.getNorAlt()==null) || 
             (this.norAlt!=null &&
              this.norAlt.equals(other.getNorAlt()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.oemExa==null && other.getOemExa()==null) || 
             (this.oemExa!=null &&
              this.oemExa.equals(other.getOemExa()))) &&
            ((this.oriExa==null && other.getOriExa()==null) || 
             (this.oriExa!=null &&
              this.oriExa.equals(other.getOriExa()))) &&
            ((this.sitExa==null && other.getSitExa()==null) || 
             (this.sitExa!=null &&
              this.sitExa.equals(other.getSitExa()))) &&
            ((this.sitRes==null && other.getSitRes()==null) || 
             (this.sitRes!=null &&
              this.sitRes.equals(other.getSitRes()))) &&
            ((this.sobAve==null && other.getSobAve()==null) || 
             (this.sobAve!=null &&
              this.sobAve.equals(other.getSobAve()))) &&
            ((this.tipExa==null && other.getTipExa()==null) || 
             (this.tipExa!=null &&
              this.tipExa.equals(other.getTipExa()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe()))) &&
            ((this.WGD108RER==null && other.getWGD108RER()==null) || 
             (this.WGD108RER!=null &&
              java.util.Arrays.equals(this.WGD108RER, other.getWGD108RER())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodAte() != null) {
            _hashCode += getCodAte().hashCode();
        }
        if (getCodExa() != null) {
            _hashCode += getCodExa().hashCode();
        }
        if (getCodFic() != null) {
            _hashCode += getCodFic().hashCode();
        }
        if (getCodOem() != null) {
            _hashCode += getCodOem().hashCode();
        }
        if (getCodPar() != null) {
            _hashCode += getCodPar().hashCode();
        }
        if (getConPPP() != null) {
            _hashCode += getConPPP().hashCode();
        }
        if (getDatRes() != null) {
            _hashCode += getDatRes().hashCode();
        }
        if (getDatSol() != null) {
            _hashCode += getDatSol().hashCode();
        }
        if (getEvoRes() != null) {
            _hashCode += getEvoRes().hashCode();
        }
        if (getExpExc() != null) {
            _hashCode += getExpExc().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getNorAlt() != null) {
            _hashCode += getNorAlt().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getOemExa() != null) {
            _hashCode += getOemExa().hashCode();
        }
        if (getOriExa() != null) {
            _hashCode += getOriExa().hashCode();
        }
        if (getSitExa() != null) {
            _hashCode += getSitExa().hashCode();
        }
        if (getSitRes() != null) {
            _hashCode += getSitRes().hashCode();
        }
        if (getSobAve() != null) {
            _hashCode += getSobAve().hashCode();
        }
        if (getTipExa() != null) {
            _hashCode += getTipExa().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        if (getWGD108RER() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getWGD108RER());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getWGD108RER(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ResultadosExamesResultadosIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "resultadosExamesResultadosIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codAte");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codAte"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codExa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codExa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFic");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFic"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codOem");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codOem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conPPP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "conPPP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datRes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datRes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datSol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datSol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("evoRes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "evoRes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expExc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "expExc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("norAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "norAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("oemExa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "oemExa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("oriExa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "oriExa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitExa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitExa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitRes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitRes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sobAve");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sobAve"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipExa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipExa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("WGD108RER");
        elemField.setXmlName(new javax.xml.namespace.QName("", "WGD108RER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "resultadosExamesResultadosInWGD108RER"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
