/**
 * CalculoApuracaoEscalonarIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class CalculoApuracaoEscalonarIn  implements java.io.Serializable {
    private java.lang.String abrCad;

    private java.lang.String abrCar;

    private java.lang.String abrCcu;

    private java.lang.String abrCes;

    private java.lang.String abrEmp;

    private java.lang.String abrEsc;

    private java.lang.String abrFil;

    private java.lang.String abrLoc;

    private java.lang.String abrOem;

    private java.lang.String abrSin;

    private java.lang.String abrTcl;

    private java.lang.String abrTco;

    private java.lang.String abrTma;

    private java.lang.String abrTrb;

    private java.lang.String abrTsa;

    private java.lang.String abrVin;

    private java.lang.Integer codCal;

    private java.lang.Integer codLot;

    private java.lang.String datFim;

    private java.lang.String datIni;

    private java.lang.String desAce;

    private java.lang.Integer diaDes;

    private java.lang.Integer diaRec;

    private java.lang.String diaVer;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String gerCal;

    private java.lang.Integer gruEmp;

    private java.lang.String leiMar;

    private java.lang.String motDRe;

    private java.lang.Integer numEmp;

    private java.lang.String proApu;

    private java.lang.String qtdIte;

    private java.lang.String reaMar;

    public CalculoApuracaoEscalonarIn() {
    }

    public CalculoApuracaoEscalonarIn(
           java.lang.String abrCad,
           java.lang.String abrCar,
           java.lang.String abrCcu,
           java.lang.String abrCes,
           java.lang.String abrEmp,
           java.lang.String abrEsc,
           java.lang.String abrFil,
           java.lang.String abrLoc,
           java.lang.String abrOem,
           java.lang.String abrSin,
           java.lang.String abrTcl,
           java.lang.String abrTco,
           java.lang.String abrTma,
           java.lang.String abrTrb,
           java.lang.String abrTsa,
           java.lang.String abrVin,
           java.lang.Integer codCal,
           java.lang.Integer codLot,
           java.lang.String datFim,
           java.lang.String datIni,
           java.lang.String desAce,
           java.lang.Integer diaDes,
           java.lang.Integer diaRec,
           java.lang.String diaVer,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String gerCal,
           java.lang.Integer gruEmp,
           java.lang.String leiMar,
           java.lang.String motDRe,
           java.lang.Integer numEmp,
           java.lang.String proApu,
           java.lang.String qtdIte,
           java.lang.String reaMar) {
           this.abrCad = abrCad;
           this.abrCar = abrCar;
           this.abrCcu = abrCcu;
           this.abrCes = abrCes;
           this.abrEmp = abrEmp;
           this.abrEsc = abrEsc;
           this.abrFil = abrFil;
           this.abrLoc = abrLoc;
           this.abrOem = abrOem;
           this.abrSin = abrSin;
           this.abrTcl = abrTcl;
           this.abrTco = abrTco;
           this.abrTma = abrTma;
           this.abrTrb = abrTrb;
           this.abrTsa = abrTsa;
           this.abrVin = abrVin;
           this.codCal = codCal;
           this.codLot = codLot;
           this.datFim = datFim;
           this.datIni = datIni;
           this.desAce = desAce;
           this.diaDes = diaDes;
           this.diaRec = diaRec;
           this.diaVer = diaVer;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.gerCal = gerCal;
           this.gruEmp = gruEmp;
           this.leiMar = leiMar;
           this.motDRe = motDRe;
           this.numEmp = numEmp;
           this.proApu = proApu;
           this.qtdIte = qtdIte;
           this.reaMar = reaMar;
    }


    /**
     * Gets the abrCad value for this CalculoApuracaoEscalonarIn.
     * 
     * @return abrCad
     */
    public java.lang.String getAbrCad() {
        return abrCad;
    }


    /**
     * Sets the abrCad value for this CalculoApuracaoEscalonarIn.
     * 
     * @param abrCad
     */
    public void setAbrCad(java.lang.String abrCad) {
        this.abrCad = abrCad;
    }


    /**
     * Gets the abrCar value for this CalculoApuracaoEscalonarIn.
     * 
     * @return abrCar
     */
    public java.lang.String getAbrCar() {
        return abrCar;
    }


    /**
     * Sets the abrCar value for this CalculoApuracaoEscalonarIn.
     * 
     * @param abrCar
     */
    public void setAbrCar(java.lang.String abrCar) {
        this.abrCar = abrCar;
    }


    /**
     * Gets the abrCcu value for this CalculoApuracaoEscalonarIn.
     * 
     * @return abrCcu
     */
    public java.lang.String getAbrCcu() {
        return abrCcu;
    }


    /**
     * Sets the abrCcu value for this CalculoApuracaoEscalonarIn.
     * 
     * @param abrCcu
     */
    public void setAbrCcu(java.lang.String abrCcu) {
        this.abrCcu = abrCcu;
    }


    /**
     * Gets the abrCes value for this CalculoApuracaoEscalonarIn.
     * 
     * @return abrCes
     */
    public java.lang.String getAbrCes() {
        return abrCes;
    }


    /**
     * Sets the abrCes value for this CalculoApuracaoEscalonarIn.
     * 
     * @param abrCes
     */
    public void setAbrCes(java.lang.String abrCes) {
        this.abrCes = abrCes;
    }


    /**
     * Gets the abrEmp value for this CalculoApuracaoEscalonarIn.
     * 
     * @return abrEmp
     */
    public java.lang.String getAbrEmp() {
        return abrEmp;
    }


    /**
     * Sets the abrEmp value for this CalculoApuracaoEscalonarIn.
     * 
     * @param abrEmp
     */
    public void setAbrEmp(java.lang.String abrEmp) {
        this.abrEmp = abrEmp;
    }


    /**
     * Gets the abrEsc value for this CalculoApuracaoEscalonarIn.
     * 
     * @return abrEsc
     */
    public java.lang.String getAbrEsc() {
        return abrEsc;
    }


    /**
     * Sets the abrEsc value for this CalculoApuracaoEscalonarIn.
     * 
     * @param abrEsc
     */
    public void setAbrEsc(java.lang.String abrEsc) {
        this.abrEsc = abrEsc;
    }


    /**
     * Gets the abrFil value for this CalculoApuracaoEscalonarIn.
     * 
     * @return abrFil
     */
    public java.lang.String getAbrFil() {
        return abrFil;
    }


    /**
     * Sets the abrFil value for this CalculoApuracaoEscalonarIn.
     * 
     * @param abrFil
     */
    public void setAbrFil(java.lang.String abrFil) {
        this.abrFil = abrFil;
    }


    /**
     * Gets the abrLoc value for this CalculoApuracaoEscalonarIn.
     * 
     * @return abrLoc
     */
    public java.lang.String getAbrLoc() {
        return abrLoc;
    }


    /**
     * Sets the abrLoc value for this CalculoApuracaoEscalonarIn.
     * 
     * @param abrLoc
     */
    public void setAbrLoc(java.lang.String abrLoc) {
        this.abrLoc = abrLoc;
    }


    /**
     * Gets the abrOem value for this CalculoApuracaoEscalonarIn.
     * 
     * @return abrOem
     */
    public java.lang.String getAbrOem() {
        return abrOem;
    }


    /**
     * Sets the abrOem value for this CalculoApuracaoEscalonarIn.
     * 
     * @param abrOem
     */
    public void setAbrOem(java.lang.String abrOem) {
        this.abrOem = abrOem;
    }


    /**
     * Gets the abrSin value for this CalculoApuracaoEscalonarIn.
     * 
     * @return abrSin
     */
    public java.lang.String getAbrSin() {
        return abrSin;
    }


    /**
     * Sets the abrSin value for this CalculoApuracaoEscalonarIn.
     * 
     * @param abrSin
     */
    public void setAbrSin(java.lang.String abrSin) {
        this.abrSin = abrSin;
    }


    /**
     * Gets the abrTcl value for this CalculoApuracaoEscalonarIn.
     * 
     * @return abrTcl
     */
    public java.lang.String getAbrTcl() {
        return abrTcl;
    }


    /**
     * Sets the abrTcl value for this CalculoApuracaoEscalonarIn.
     * 
     * @param abrTcl
     */
    public void setAbrTcl(java.lang.String abrTcl) {
        this.abrTcl = abrTcl;
    }


    /**
     * Gets the abrTco value for this CalculoApuracaoEscalonarIn.
     * 
     * @return abrTco
     */
    public java.lang.String getAbrTco() {
        return abrTco;
    }


    /**
     * Sets the abrTco value for this CalculoApuracaoEscalonarIn.
     * 
     * @param abrTco
     */
    public void setAbrTco(java.lang.String abrTco) {
        this.abrTco = abrTco;
    }


    /**
     * Gets the abrTma value for this CalculoApuracaoEscalonarIn.
     * 
     * @return abrTma
     */
    public java.lang.String getAbrTma() {
        return abrTma;
    }


    /**
     * Sets the abrTma value for this CalculoApuracaoEscalonarIn.
     * 
     * @param abrTma
     */
    public void setAbrTma(java.lang.String abrTma) {
        this.abrTma = abrTma;
    }


    /**
     * Gets the abrTrb value for this CalculoApuracaoEscalonarIn.
     * 
     * @return abrTrb
     */
    public java.lang.String getAbrTrb() {
        return abrTrb;
    }


    /**
     * Sets the abrTrb value for this CalculoApuracaoEscalonarIn.
     * 
     * @param abrTrb
     */
    public void setAbrTrb(java.lang.String abrTrb) {
        this.abrTrb = abrTrb;
    }


    /**
     * Gets the abrTsa value for this CalculoApuracaoEscalonarIn.
     * 
     * @return abrTsa
     */
    public java.lang.String getAbrTsa() {
        return abrTsa;
    }


    /**
     * Sets the abrTsa value for this CalculoApuracaoEscalonarIn.
     * 
     * @param abrTsa
     */
    public void setAbrTsa(java.lang.String abrTsa) {
        this.abrTsa = abrTsa;
    }


    /**
     * Gets the abrVin value for this CalculoApuracaoEscalonarIn.
     * 
     * @return abrVin
     */
    public java.lang.String getAbrVin() {
        return abrVin;
    }


    /**
     * Sets the abrVin value for this CalculoApuracaoEscalonarIn.
     * 
     * @param abrVin
     */
    public void setAbrVin(java.lang.String abrVin) {
        this.abrVin = abrVin;
    }


    /**
     * Gets the codCal value for this CalculoApuracaoEscalonarIn.
     * 
     * @return codCal
     */
    public java.lang.Integer getCodCal() {
        return codCal;
    }


    /**
     * Sets the codCal value for this CalculoApuracaoEscalonarIn.
     * 
     * @param codCal
     */
    public void setCodCal(java.lang.Integer codCal) {
        this.codCal = codCal;
    }


    /**
     * Gets the codLot value for this CalculoApuracaoEscalonarIn.
     * 
     * @return codLot
     */
    public java.lang.Integer getCodLot() {
        return codLot;
    }


    /**
     * Sets the codLot value for this CalculoApuracaoEscalonarIn.
     * 
     * @param codLot
     */
    public void setCodLot(java.lang.Integer codLot) {
        this.codLot = codLot;
    }


    /**
     * Gets the datFim value for this CalculoApuracaoEscalonarIn.
     * 
     * @return datFim
     */
    public java.lang.String getDatFim() {
        return datFim;
    }


    /**
     * Sets the datFim value for this CalculoApuracaoEscalonarIn.
     * 
     * @param datFim
     */
    public void setDatFim(java.lang.String datFim) {
        this.datFim = datFim;
    }


    /**
     * Gets the datIni value for this CalculoApuracaoEscalonarIn.
     * 
     * @return datIni
     */
    public java.lang.String getDatIni() {
        return datIni;
    }


    /**
     * Sets the datIni value for this CalculoApuracaoEscalonarIn.
     * 
     * @param datIni
     */
    public void setDatIni(java.lang.String datIni) {
        this.datIni = datIni;
    }


    /**
     * Gets the desAce value for this CalculoApuracaoEscalonarIn.
     * 
     * @return desAce
     */
    public java.lang.String getDesAce() {
        return desAce;
    }


    /**
     * Sets the desAce value for this CalculoApuracaoEscalonarIn.
     * 
     * @param desAce
     */
    public void setDesAce(java.lang.String desAce) {
        this.desAce = desAce;
    }


    /**
     * Gets the diaDes value for this CalculoApuracaoEscalonarIn.
     * 
     * @return diaDes
     */
    public java.lang.Integer getDiaDes() {
        return diaDes;
    }


    /**
     * Sets the diaDes value for this CalculoApuracaoEscalonarIn.
     * 
     * @param diaDes
     */
    public void setDiaDes(java.lang.Integer diaDes) {
        this.diaDes = diaDes;
    }


    /**
     * Gets the diaRec value for this CalculoApuracaoEscalonarIn.
     * 
     * @return diaRec
     */
    public java.lang.Integer getDiaRec() {
        return diaRec;
    }


    /**
     * Sets the diaRec value for this CalculoApuracaoEscalonarIn.
     * 
     * @param diaRec
     */
    public void setDiaRec(java.lang.Integer diaRec) {
        this.diaRec = diaRec;
    }


    /**
     * Gets the diaVer value for this CalculoApuracaoEscalonarIn.
     * 
     * @return diaVer
     */
    public java.lang.String getDiaVer() {
        return diaVer;
    }


    /**
     * Sets the diaVer value for this CalculoApuracaoEscalonarIn.
     * 
     * @param diaVer
     */
    public void setDiaVer(java.lang.String diaVer) {
        this.diaVer = diaVer;
    }


    /**
     * Gets the flowInstanceID value for this CalculoApuracaoEscalonarIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this CalculoApuracaoEscalonarIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this CalculoApuracaoEscalonarIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this CalculoApuracaoEscalonarIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the gerCal value for this CalculoApuracaoEscalonarIn.
     * 
     * @return gerCal
     */
    public java.lang.String getGerCal() {
        return gerCal;
    }


    /**
     * Sets the gerCal value for this CalculoApuracaoEscalonarIn.
     * 
     * @param gerCal
     */
    public void setGerCal(java.lang.String gerCal) {
        this.gerCal = gerCal;
    }


    /**
     * Gets the gruEmp value for this CalculoApuracaoEscalonarIn.
     * 
     * @return gruEmp
     */
    public java.lang.Integer getGruEmp() {
        return gruEmp;
    }


    /**
     * Sets the gruEmp value for this CalculoApuracaoEscalonarIn.
     * 
     * @param gruEmp
     */
    public void setGruEmp(java.lang.Integer gruEmp) {
        this.gruEmp = gruEmp;
    }


    /**
     * Gets the leiMar value for this CalculoApuracaoEscalonarIn.
     * 
     * @return leiMar
     */
    public java.lang.String getLeiMar() {
        return leiMar;
    }


    /**
     * Sets the leiMar value for this CalculoApuracaoEscalonarIn.
     * 
     * @param leiMar
     */
    public void setLeiMar(java.lang.String leiMar) {
        this.leiMar = leiMar;
    }


    /**
     * Gets the motDRe value for this CalculoApuracaoEscalonarIn.
     * 
     * @return motDRe
     */
    public java.lang.String getMotDRe() {
        return motDRe;
    }


    /**
     * Sets the motDRe value for this CalculoApuracaoEscalonarIn.
     * 
     * @param motDRe
     */
    public void setMotDRe(java.lang.String motDRe) {
        this.motDRe = motDRe;
    }


    /**
     * Gets the numEmp value for this CalculoApuracaoEscalonarIn.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this CalculoApuracaoEscalonarIn.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the proApu value for this CalculoApuracaoEscalonarIn.
     * 
     * @return proApu
     */
    public java.lang.String getProApu() {
        return proApu;
    }


    /**
     * Sets the proApu value for this CalculoApuracaoEscalonarIn.
     * 
     * @param proApu
     */
    public void setProApu(java.lang.String proApu) {
        this.proApu = proApu;
    }


    /**
     * Gets the qtdIte value for this CalculoApuracaoEscalonarIn.
     * 
     * @return qtdIte
     */
    public java.lang.String getQtdIte() {
        return qtdIte;
    }


    /**
     * Sets the qtdIte value for this CalculoApuracaoEscalonarIn.
     * 
     * @param qtdIte
     */
    public void setQtdIte(java.lang.String qtdIte) {
        this.qtdIte = qtdIte;
    }


    /**
     * Gets the reaMar value for this CalculoApuracaoEscalonarIn.
     * 
     * @return reaMar
     */
    public java.lang.String getReaMar() {
        return reaMar;
    }


    /**
     * Sets the reaMar value for this CalculoApuracaoEscalonarIn.
     * 
     * @param reaMar
     */
    public void setReaMar(java.lang.String reaMar) {
        this.reaMar = reaMar;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CalculoApuracaoEscalonarIn)) return false;
        CalculoApuracaoEscalonarIn other = (CalculoApuracaoEscalonarIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.abrCad==null && other.getAbrCad()==null) || 
             (this.abrCad!=null &&
              this.abrCad.equals(other.getAbrCad()))) &&
            ((this.abrCar==null && other.getAbrCar()==null) || 
             (this.abrCar!=null &&
              this.abrCar.equals(other.getAbrCar()))) &&
            ((this.abrCcu==null && other.getAbrCcu()==null) || 
             (this.abrCcu!=null &&
              this.abrCcu.equals(other.getAbrCcu()))) &&
            ((this.abrCes==null && other.getAbrCes()==null) || 
             (this.abrCes!=null &&
              this.abrCes.equals(other.getAbrCes()))) &&
            ((this.abrEmp==null && other.getAbrEmp()==null) || 
             (this.abrEmp!=null &&
              this.abrEmp.equals(other.getAbrEmp()))) &&
            ((this.abrEsc==null && other.getAbrEsc()==null) || 
             (this.abrEsc!=null &&
              this.abrEsc.equals(other.getAbrEsc()))) &&
            ((this.abrFil==null && other.getAbrFil()==null) || 
             (this.abrFil!=null &&
              this.abrFil.equals(other.getAbrFil()))) &&
            ((this.abrLoc==null && other.getAbrLoc()==null) || 
             (this.abrLoc!=null &&
              this.abrLoc.equals(other.getAbrLoc()))) &&
            ((this.abrOem==null && other.getAbrOem()==null) || 
             (this.abrOem!=null &&
              this.abrOem.equals(other.getAbrOem()))) &&
            ((this.abrSin==null && other.getAbrSin()==null) || 
             (this.abrSin!=null &&
              this.abrSin.equals(other.getAbrSin()))) &&
            ((this.abrTcl==null && other.getAbrTcl()==null) || 
             (this.abrTcl!=null &&
              this.abrTcl.equals(other.getAbrTcl()))) &&
            ((this.abrTco==null && other.getAbrTco()==null) || 
             (this.abrTco!=null &&
              this.abrTco.equals(other.getAbrTco()))) &&
            ((this.abrTma==null && other.getAbrTma()==null) || 
             (this.abrTma!=null &&
              this.abrTma.equals(other.getAbrTma()))) &&
            ((this.abrTrb==null && other.getAbrTrb()==null) || 
             (this.abrTrb!=null &&
              this.abrTrb.equals(other.getAbrTrb()))) &&
            ((this.abrTsa==null && other.getAbrTsa()==null) || 
             (this.abrTsa!=null &&
              this.abrTsa.equals(other.getAbrTsa()))) &&
            ((this.abrVin==null && other.getAbrVin()==null) || 
             (this.abrVin!=null &&
              this.abrVin.equals(other.getAbrVin()))) &&
            ((this.codCal==null && other.getCodCal()==null) || 
             (this.codCal!=null &&
              this.codCal.equals(other.getCodCal()))) &&
            ((this.codLot==null && other.getCodLot()==null) || 
             (this.codLot!=null &&
              this.codLot.equals(other.getCodLot()))) &&
            ((this.datFim==null && other.getDatFim()==null) || 
             (this.datFim!=null &&
              this.datFim.equals(other.getDatFim()))) &&
            ((this.datIni==null && other.getDatIni()==null) || 
             (this.datIni!=null &&
              this.datIni.equals(other.getDatIni()))) &&
            ((this.desAce==null && other.getDesAce()==null) || 
             (this.desAce!=null &&
              this.desAce.equals(other.getDesAce()))) &&
            ((this.diaDes==null && other.getDiaDes()==null) || 
             (this.diaDes!=null &&
              this.diaDes.equals(other.getDiaDes()))) &&
            ((this.diaRec==null && other.getDiaRec()==null) || 
             (this.diaRec!=null &&
              this.diaRec.equals(other.getDiaRec()))) &&
            ((this.diaVer==null && other.getDiaVer()==null) || 
             (this.diaVer!=null &&
              this.diaVer.equals(other.getDiaVer()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.gerCal==null && other.getGerCal()==null) || 
             (this.gerCal!=null &&
              this.gerCal.equals(other.getGerCal()))) &&
            ((this.gruEmp==null && other.getGruEmp()==null) || 
             (this.gruEmp!=null &&
              this.gruEmp.equals(other.getGruEmp()))) &&
            ((this.leiMar==null && other.getLeiMar()==null) || 
             (this.leiMar!=null &&
              this.leiMar.equals(other.getLeiMar()))) &&
            ((this.motDRe==null && other.getMotDRe()==null) || 
             (this.motDRe!=null &&
              this.motDRe.equals(other.getMotDRe()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.proApu==null && other.getProApu()==null) || 
             (this.proApu!=null &&
              this.proApu.equals(other.getProApu()))) &&
            ((this.qtdIte==null && other.getQtdIte()==null) || 
             (this.qtdIte!=null &&
              this.qtdIte.equals(other.getQtdIte()))) &&
            ((this.reaMar==null && other.getReaMar()==null) || 
             (this.reaMar!=null &&
              this.reaMar.equals(other.getReaMar())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAbrCad() != null) {
            _hashCode += getAbrCad().hashCode();
        }
        if (getAbrCar() != null) {
            _hashCode += getAbrCar().hashCode();
        }
        if (getAbrCcu() != null) {
            _hashCode += getAbrCcu().hashCode();
        }
        if (getAbrCes() != null) {
            _hashCode += getAbrCes().hashCode();
        }
        if (getAbrEmp() != null) {
            _hashCode += getAbrEmp().hashCode();
        }
        if (getAbrEsc() != null) {
            _hashCode += getAbrEsc().hashCode();
        }
        if (getAbrFil() != null) {
            _hashCode += getAbrFil().hashCode();
        }
        if (getAbrLoc() != null) {
            _hashCode += getAbrLoc().hashCode();
        }
        if (getAbrOem() != null) {
            _hashCode += getAbrOem().hashCode();
        }
        if (getAbrSin() != null) {
            _hashCode += getAbrSin().hashCode();
        }
        if (getAbrTcl() != null) {
            _hashCode += getAbrTcl().hashCode();
        }
        if (getAbrTco() != null) {
            _hashCode += getAbrTco().hashCode();
        }
        if (getAbrTma() != null) {
            _hashCode += getAbrTma().hashCode();
        }
        if (getAbrTrb() != null) {
            _hashCode += getAbrTrb().hashCode();
        }
        if (getAbrTsa() != null) {
            _hashCode += getAbrTsa().hashCode();
        }
        if (getAbrVin() != null) {
            _hashCode += getAbrVin().hashCode();
        }
        if (getCodCal() != null) {
            _hashCode += getCodCal().hashCode();
        }
        if (getCodLot() != null) {
            _hashCode += getCodLot().hashCode();
        }
        if (getDatFim() != null) {
            _hashCode += getDatFim().hashCode();
        }
        if (getDatIni() != null) {
            _hashCode += getDatIni().hashCode();
        }
        if (getDesAce() != null) {
            _hashCode += getDesAce().hashCode();
        }
        if (getDiaDes() != null) {
            _hashCode += getDiaDes().hashCode();
        }
        if (getDiaRec() != null) {
            _hashCode += getDiaRec().hashCode();
        }
        if (getDiaVer() != null) {
            _hashCode += getDiaVer().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getGerCal() != null) {
            _hashCode += getGerCal().hashCode();
        }
        if (getGruEmp() != null) {
            _hashCode += getGruEmp().hashCode();
        }
        if (getLeiMar() != null) {
            _hashCode += getLeiMar().hashCode();
        }
        if (getMotDRe() != null) {
            _hashCode += getMotDRe().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getProApu() != null) {
            _hashCode += getProApu().hashCode();
        }
        if (getQtdIte() != null) {
            _hashCode += getQtdIte().hashCode();
        }
        if (getReaMar() != null) {
            _hashCode += getReaMar().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CalculoApuracaoEscalonarIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "calculoApuracaoEscalonarIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abrCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "abrCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abrCar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "abrCar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abrCcu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "abrCcu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abrCes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "abrCes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abrEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "abrEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abrEsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "abrEsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abrFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "abrFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abrLoc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "abrLoc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abrOem");
        elemField.setXmlName(new javax.xml.namespace.QName("", "abrOem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abrSin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "abrSin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abrTcl");
        elemField.setXmlName(new javax.xml.namespace.QName("", "abrTcl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abrTco");
        elemField.setXmlName(new javax.xml.namespace.QName("", "abrTco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abrTma");
        elemField.setXmlName(new javax.xml.namespace.QName("", "abrTma"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abrTrb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "abrTrb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abrTsa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "abrTsa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abrVin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "abrVin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codLot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codLot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datFim");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datFim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datIni");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datIni"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("desAce");
        elemField.setXmlName(new javax.xml.namespace.QName("", "desAce"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diaDes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "diaDes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diaRec");
        elemField.setXmlName(new javax.xml.namespace.QName("", "diaRec"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diaVer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "diaVer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gerCal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "gerCal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gruEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "gruEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("leiMar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "leiMar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("motDRe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "motDRe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("proApu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "proApu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qtdIte");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qtdIte"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reaMar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "reaMar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
