/**
 * ClientesExportarClientesCompletoOutClientes.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package br.com.senior.services.co.cad.clientes;

public class ClientesExportarClientesCompletoOutClientes  implements java.io.Serializable {
    private java.lang.String apeCli;

    private java.lang.String baiCli;

    private java.lang.String baiCob;

    private java.lang.Integer cepCli;

    private java.lang.Integer cepCob;

    private java.lang.Integer cepIni;

    private java.lang.String cgcCob;

    private java.lang.String cgcCpf;

    private java.lang.String cidCli;

    private java.lang.String cidCob;

    private java.lang.String cliPrx;

    private java.lang.Integer cliRep;

    private java.lang.Integer codCli;

    private java.lang.String codCpg;

    private java.lang.String codCrp;

    private java.lang.String codCrt;

    private java.lang.Integer codFor;

    private java.lang.Integer codFpg;

    private java.lang.Integer codGre;

    private java.lang.Integer codMot;

    private java.lang.Integer codMs2;

    private java.lang.Integer codMs3;

    private java.lang.Integer codMs4;

    private java.lang.Integer codMsg;

    private java.lang.String codPai;

    private java.lang.Integer codRep;

    private java.lang.String codSuf;

    private java.lang.String cplCob;

    private java.lang.String cplEnd;

    private java.lang.Integer ctrPad;

    private java.lang.Integer cxaPst;

    private java.lang.String datAtu;

    private java.lang.String datMot;

    private java.lang.String emaNfe;

    private java.lang.String endCli;

    private java.lang.String endCob;

    private java.lang.String estCob;

    private java.lang.String faxCli;

    private java.lang.String fonCl2;

    private java.lang.String fonCl3;

    private java.lang.String fonCl4;

    private java.lang.String fonCl5;

    private java.lang.String fonCli;

    private java.lang.Integer horAtu;

    private java.lang.Integer horMot;

    private java.lang.String ideCli;

    private java.lang.Integer iniCob;

    private java.lang.String insEst;

    private java.lang.String insMun;

    private java.lang.String intNet;

    private java.lang.Integer maiAtr;

    private java.lang.Integer medAtr;

    private java.lang.String nenCli;

    private java.lang.String nenCob;

    private java.lang.String nomCli;

    private java.lang.String obsMot;

    private java.lang.Double perDsc;

    private java.lang.Integer qtdChs;

    private java.lang.Integer recDtj;

    private java.lang.Integer recDtm;

    private java.lang.Double recJmm;

    private java.lang.Double recMul;

    private java.lang.String recTjr;

    private java.lang.Double salCre;

    private java.lang.Double salDup;

    private java.lang.Double salOut;

    private java.lang.Integer seqCob;

    private java.lang.String sigUfs;

    private java.lang.String sitCli;

    private java.lang.String tipCli;

    private java.lang.Integer tipEmc;

    private java.lang.String tipMer;

    private java.lang.Double tolDsc;

    private java.lang.Integer usuAtu;

    private java.lang.String zipCod;

    public ClientesExportarClientesCompletoOutClientes() {
    }

    public ClientesExportarClientesCompletoOutClientes(
           java.lang.String apeCli,
           java.lang.String baiCli,
           java.lang.String baiCob,
           java.lang.Integer cepCli,
           java.lang.Integer cepCob,
           java.lang.Integer cepIni,
           java.lang.String cgcCob,
           java.lang.String cgcCpf,
           java.lang.String cidCli,
           java.lang.String cidCob,
           java.lang.String cliPrx,
           java.lang.Integer cliRep,
           java.lang.Integer codCli,
           java.lang.String codCpg,
           java.lang.String codCrp,
           java.lang.String codCrt,
           java.lang.Integer codFor,
           java.lang.Integer codFpg,
           java.lang.Integer codGre,
           java.lang.Integer codMot,
           java.lang.Integer codMs2,
           java.lang.Integer codMs3,
           java.lang.Integer codMs4,
           java.lang.Integer codMsg,
           java.lang.String codPai,
           java.lang.Integer codRep,
           java.lang.String codSuf,
           java.lang.String cplCob,
           java.lang.String cplEnd,
           java.lang.Integer ctrPad,
           java.lang.Integer cxaPst,
           java.lang.String datAtu,
           java.lang.String datMot,
           java.lang.String emaNfe,
           java.lang.String endCli,
           java.lang.String endCob,
           java.lang.String estCob,
           java.lang.String faxCli,
           java.lang.String fonCl2,
           java.lang.String fonCl3,
           java.lang.String fonCl4,
           java.lang.String fonCl5,
           java.lang.String fonCli,
           java.lang.Integer horAtu,
           java.lang.Integer horMot,
           java.lang.String ideCli,
           java.lang.Integer iniCob,
           java.lang.String insEst,
           java.lang.String insMun,
           java.lang.String intNet,
           java.lang.Integer maiAtr,
           java.lang.Integer medAtr,
           java.lang.String nenCli,
           java.lang.String nenCob,
           java.lang.String nomCli,
           java.lang.String obsMot,
           java.lang.Double perDsc,
           java.lang.Integer qtdChs,
           java.lang.Integer recDtj,
           java.lang.Integer recDtm,
           java.lang.Double recJmm,
           java.lang.Double recMul,
           java.lang.String recTjr,
           java.lang.Double salCre,
           java.lang.Double salDup,
           java.lang.Double salOut,
           java.lang.Integer seqCob,
           java.lang.String sigUfs,
           java.lang.String sitCli,
           java.lang.String tipCli,
           java.lang.Integer tipEmc,
           java.lang.String tipMer,
           java.lang.Double tolDsc,
           java.lang.Integer usuAtu,
           java.lang.String zipCod) {
           this.apeCli = apeCli;
           this.baiCli = baiCli;
           this.baiCob = baiCob;
           this.cepCli = cepCli;
           this.cepCob = cepCob;
           this.cepIni = cepIni;
           this.cgcCob = cgcCob;
           this.cgcCpf = cgcCpf;
           this.cidCli = cidCli;
           this.cidCob = cidCob;
           this.cliPrx = cliPrx;
           this.cliRep = cliRep;
           this.codCli = codCli;
           this.codCpg = codCpg;
           this.codCrp = codCrp;
           this.codCrt = codCrt;
           this.codFor = codFor;
           this.codFpg = codFpg;
           this.codGre = codGre;
           this.codMot = codMot;
           this.codMs2 = codMs2;
           this.codMs3 = codMs3;
           this.codMs4 = codMs4;
           this.codMsg = codMsg;
           this.codPai = codPai;
           this.codRep = codRep;
           this.codSuf = codSuf;
           this.cplCob = cplCob;
           this.cplEnd = cplEnd;
           this.ctrPad = ctrPad;
           this.cxaPst = cxaPst;
           this.datAtu = datAtu;
           this.datMot = datMot;
           this.emaNfe = emaNfe;
           this.endCli = endCli;
           this.endCob = endCob;
           this.estCob = estCob;
           this.faxCli = faxCli;
           this.fonCl2 = fonCl2;
           this.fonCl3 = fonCl3;
           this.fonCl4 = fonCl4;
           this.fonCl5 = fonCl5;
           this.fonCli = fonCli;
           this.horAtu = horAtu;
           this.horMot = horMot;
           this.ideCli = ideCli;
           this.iniCob = iniCob;
           this.insEst = insEst;
           this.insMun = insMun;
           this.intNet = intNet;
           this.maiAtr = maiAtr;
           this.medAtr = medAtr;
           this.nenCli = nenCli;
           this.nenCob = nenCob;
           this.nomCli = nomCli;
           this.obsMot = obsMot;
           this.perDsc = perDsc;
           this.qtdChs = qtdChs;
           this.recDtj = recDtj;
           this.recDtm = recDtm;
           this.recJmm = recJmm;
           this.recMul = recMul;
           this.recTjr = recTjr;
           this.salCre = salCre;
           this.salDup = salDup;
           this.salOut = salOut;
           this.seqCob = seqCob;
           this.sigUfs = sigUfs;
           this.sitCli = sitCli;
           this.tipCli = tipCli;
           this.tipEmc = tipEmc;
           this.tipMer = tipMer;
           this.tolDsc = tolDsc;
           this.usuAtu = usuAtu;
           this.zipCod = zipCod;
    }


    /**
     * Gets the apeCli value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return apeCli
     */
    public java.lang.String getApeCli() {
        return apeCli;
    }


    /**
     * Sets the apeCli value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param apeCli
     */
    public void setApeCli(java.lang.String apeCli) {
        this.apeCli = apeCli;
    }


    /**
     * Gets the baiCli value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return baiCli
     */
    public java.lang.String getBaiCli() {
        return baiCli;
    }


    /**
     * Sets the baiCli value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param baiCli
     */
    public void setBaiCli(java.lang.String baiCli) {
        this.baiCli = baiCli;
    }


    /**
     * Gets the baiCob value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return baiCob
     */
    public java.lang.String getBaiCob() {
        return baiCob;
    }


    /**
     * Sets the baiCob value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param baiCob
     */
    public void setBaiCob(java.lang.String baiCob) {
        this.baiCob = baiCob;
    }


    /**
     * Gets the cepCli value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return cepCli
     */
    public java.lang.Integer getCepCli() {
        return cepCli;
    }


    /**
     * Sets the cepCli value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param cepCli
     */
    public void setCepCli(java.lang.Integer cepCli) {
        this.cepCli = cepCli;
    }


    /**
     * Gets the cepCob value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return cepCob
     */
    public java.lang.Integer getCepCob() {
        return cepCob;
    }


    /**
     * Sets the cepCob value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param cepCob
     */
    public void setCepCob(java.lang.Integer cepCob) {
        this.cepCob = cepCob;
    }


    /**
     * Gets the cepIni value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return cepIni
     */
    public java.lang.Integer getCepIni() {
        return cepIni;
    }


    /**
     * Sets the cepIni value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param cepIni
     */
    public void setCepIni(java.lang.Integer cepIni) {
        this.cepIni = cepIni;
    }


    /**
     * Gets the cgcCob value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return cgcCob
     */
    public java.lang.String getCgcCob() {
        return cgcCob;
    }


    /**
     * Sets the cgcCob value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param cgcCob
     */
    public void setCgcCob(java.lang.String cgcCob) {
        this.cgcCob = cgcCob;
    }


    /**
     * Gets the cgcCpf value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return cgcCpf
     */
    public java.lang.String getCgcCpf() {
        return cgcCpf;
    }


    /**
     * Sets the cgcCpf value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param cgcCpf
     */
    public void setCgcCpf(java.lang.String cgcCpf) {
        this.cgcCpf = cgcCpf;
    }


    /**
     * Gets the cidCli value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return cidCli
     */
    public java.lang.String getCidCli() {
        return cidCli;
    }


    /**
     * Sets the cidCli value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param cidCli
     */
    public void setCidCli(java.lang.String cidCli) {
        this.cidCli = cidCli;
    }


    /**
     * Gets the cidCob value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return cidCob
     */
    public java.lang.String getCidCob() {
        return cidCob;
    }


    /**
     * Sets the cidCob value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param cidCob
     */
    public void setCidCob(java.lang.String cidCob) {
        this.cidCob = cidCob;
    }


    /**
     * Gets the cliPrx value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return cliPrx
     */
    public java.lang.String getCliPrx() {
        return cliPrx;
    }


    /**
     * Sets the cliPrx value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param cliPrx
     */
    public void setCliPrx(java.lang.String cliPrx) {
        this.cliPrx = cliPrx;
    }


    /**
     * Gets the cliRep value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return cliRep
     */
    public java.lang.Integer getCliRep() {
        return cliRep;
    }


    /**
     * Sets the cliRep value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param cliRep
     */
    public void setCliRep(java.lang.Integer cliRep) {
        this.cliRep = cliRep;
    }


    /**
     * Gets the codCli value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return codCli
     */
    public java.lang.Integer getCodCli() {
        return codCli;
    }


    /**
     * Sets the codCli value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param codCli
     */
    public void setCodCli(java.lang.Integer codCli) {
        this.codCli = codCli;
    }


    /**
     * Gets the codCpg value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return codCpg
     */
    public java.lang.String getCodCpg() {
        return codCpg;
    }


    /**
     * Sets the codCpg value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param codCpg
     */
    public void setCodCpg(java.lang.String codCpg) {
        this.codCpg = codCpg;
    }


    /**
     * Gets the codCrp value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return codCrp
     */
    public java.lang.String getCodCrp() {
        return codCrp;
    }


    /**
     * Sets the codCrp value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param codCrp
     */
    public void setCodCrp(java.lang.String codCrp) {
        this.codCrp = codCrp;
    }


    /**
     * Gets the codCrt value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return codCrt
     */
    public java.lang.String getCodCrt() {
        return codCrt;
    }


    /**
     * Sets the codCrt value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param codCrt
     */
    public void setCodCrt(java.lang.String codCrt) {
        this.codCrt = codCrt;
    }


    /**
     * Gets the codFor value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return codFor
     */
    public java.lang.Integer getCodFor() {
        return codFor;
    }


    /**
     * Sets the codFor value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param codFor
     */
    public void setCodFor(java.lang.Integer codFor) {
        this.codFor = codFor;
    }


    /**
     * Gets the codFpg value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return codFpg
     */
    public java.lang.Integer getCodFpg() {
        return codFpg;
    }


    /**
     * Sets the codFpg value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param codFpg
     */
    public void setCodFpg(java.lang.Integer codFpg) {
        this.codFpg = codFpg;
    }


    /**
     * Gets the codGre value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return codGre
     */
    public java.lang.Integer getCodGre() {
        return codGre;
    }


    /**
     * Sets the codGre value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param codGre
     */
    public void setCodGre(java.lang.Integer codGre) {
        this.codGre = codGre;
    }


    /**
     * Gets the codMot value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return codMot
     */
    public java.lang.Integer getCodMot() {
        return codMot;
    }


    /**
     * Sets the codMot value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param codMot
     */
    public void setCodMot(java.lang.Integer codMot) {
        this.codMot = codMot;
    }


    /**
     * Gets the codMs2 value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return codMs2
     */
    public java.lang.Integer getCodMs2() {
        return codMs2;
    }


    /**
     * Sets the codMs2 value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param codMs2
     */
    public void setCodMs2(java.lang.Integer codMs2) {
        this.codMs2 = codMs2;
    }


    /**
     * Gets the codMs3 value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return codMs3
     */
    public java.lang.Integer getCodMs3() {
        return codMs3;
    }


    /**
     * Sets the codMs3 value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param codMs3
     */
    public void setCodMs3(java.lang.Integer codMs3) {
        this.codMs3 = codMs3;
    }


    /**
     * Gets the codMs4 value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return codMs4
     */
    public java.lang.Integer getCodMs4() {
        return codMs4;
    }


    /**
     * Sets the codMs4 value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param codMs4
     */
    public void setCodMs4(java.lang.Integer codMs4) {
        this.codMs4 = codMs4;
    }


    /**
     * Gets the codMsg value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return codMsg
     */
    public java.lang.Integer getCodMsg() {
        return codMsg;
    }


    /**
     * Sets the codMsg value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param codMsg
     */
    public void setCodMsg(java.lang.Integer codMsg) {
        this.codMsg = codMsg;
    }


    /**
     * Gets the codPai value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return codPai
     */
    public java.lang.String getCodPai() {
        return codPai;
    }


    /**
     * Sets the codPai value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param codPai
     */
    public void setCodPai(java.lang.String codPai) {
        this.codPai = codPai;
    }


    /**
     * Gets the codRep value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return codRep
     */
    public java.lang.Integer getCodRep() {
        return codRep;
    }


    /**
     * Sets the codRep value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param codRep
     */
    public void setCodRep(java.lang.Integer codRep) {
        this.codRep = codRep;
    }


    /**
     * Gets the codSuf value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return codSuf
     */
    public java.lang.String getCodSuf() {
        return codSuf;
    }


    /**
     * Sets the codSuf value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param codSuf
     */
    public void setCodSuf(java.lang.String codSuf) {
        this.codSuf = codSuf;
    }


    /**
     * Gets the cplCob value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return cplCob
     */
    public java.lang.String getCplCob() {
        return cplCob;
    }


    /**
     * Sets the cplCob value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param cplCob
     */
    public void setCplCob(java.lang.String cplCob) {
        this.cplCob = cplCob;
    }


    /**
     * Gets the cplEnd value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return cplEnd
     */
    public java.lang.String getCplEnd() {
        return cplEnd;
    }


    /**
     * Sets the cplEnd value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param cplEnd
     */
    public void setCplEnd(java.lang.String cplEnd) {
        this.cplEnd = cplEnd;
    }


    /**
     * Gets the ctrPad value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return ctrPad
     */
    public java.lang.Integer getCtrPad() {
        return ctrPad;
    }


    /**
     * Sets the ctrPad value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param ctrPad
     */
    public void setCtrPad(java.lang.Integer ctrPad) {
        this.ctrPad = ctrPad;
    }


    /**
     * Gets the cxaPst value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return cxaPst
     */
    public java.lang.Integer getCxaPst() {
        return cxaPst;
    }


    /**
     * Sets the cxaPst value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param cxaPst
     */
    public void setCxaPst(java.lang.Integer cxaPst) {
        this.cxaPst = cxaPst;
    }


    /**
     * Gets the datAtu value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return datAtu
     */
    public java.lang.String getDatAtu() {
        return datAtu;
    }


    /**
     * Sets the datAtu value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param datAtu
     */
    public void setDatAtu(java.lang.String datAtu) {
        this.datAtu = datAtu;
    }


    /**
     * Gets the datMot value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return datMot
     */
    public java.lang.String getDatMot() {
        return datMot;
    }


    /**
     * Sets the datMot value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param datMot
     */
    public void setDatMot(java.lang.String datMot) {
        this.datMot = datMot;
    }


    /**
     * Gets the emaNfe value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return emaNfe
     */
    public java.lang.String getEmaNfe() {
        return emaNfe;
    }


    /**
     * Sets the emaNfe value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param emaNfe
     */
    public void setEmaNfe(java.lang.String emaNfe) {
        this.emaNfe = emaNfe;
    }


    /**
     * Gets the endCli value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return endCli
     */
    public java.lang.String getEndCli() {
        return endCli;
    }


    /**
     * Sets the endCli value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param endCli
     */
    public void setEndCli(java.lang.String endCli) {
        this.endCli = endCli;
    }


    /**
     * Gets the endCob value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return endCob
     */
    public java.lang.String getEndCob() {
        return endCob;
    }


    /**
     * Sets the endCob value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param endCob
     */
    public void setEndCob(java.lang.String endCob) {
        this.endCob = endCob;
    }


    /**
     * Gets the estCob value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return estCob
     */
    public java.lang.String getEstCob() {
        return estCob;
    }


    /**
     * Sets the estCob value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param estCob
     */
    public void setEstCob(java.lang.String estCob) {
        this.estCob = estCob;
    }


    /**
     * Gets the faxCli value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return faxCli
     */
    public java.lang.String getFaxCli() {
        return faxCli;
    }


    /**
     * Sets the faxCli value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param faxCli
     */
    public void setFaxCli(java.lang.String faxCli) {
        this.faxCli = faxCli;
    }


    /**
     * Gets the fonCl2 value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return fonCl2
     */
    public java.lang.String getFonCl2() {
        return fonCl2;
    }


    /**
     * Sets the fonCl2 value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param fonCl2
     */
    public void setFonCl2(java.lang.String fonCl2) {
        this.fonCl2 = fonCl2;
    }


    /**
     * Gets the fonCl3 value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return fonCl3
     */
    public java.lang.String getFonCl3() {
        return fonCl3;
    }


    /**
     * Sets the fonCl3 value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param fonCl3
     */
    public void setFonCl3(java.lang.String fonCl3) {
        this.fonCl3 = fonCl3;
    }


    /**
     * Gets the fonCl4 value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return fonCl4
     */
    public java.lang.String getFonCl4() {
        return fonCl4;
    }


    /**
     * Sets the fonCl4 value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param fonCl4
     */
    public void setFonCl4(java.lang.String fonCl4) {
        this.fonCl4 = fonCl4;
    }


    /**
     * Gets the fonCl5 value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return fonCl5
     */
    public java.lang.String getFonCl5() {
        return fonCl5;
    }


    /**
     * Sets the fonCl5 value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param fonCl5
     */
    public void setFonCl5(java.lang.String fonCl5) {
        this.fonCl5 = fonCl5;
    }


    /**
     * Gets the fonCli value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return fonCli
     */
    public java.lang.String getFonCli() {
        return fonCli;
    }


    /**
     * Sets the fonCli value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param fonCli
     */
    public void setFonCli(java.lang.String fonCli) {
        this.fonCli = fonCli;
    }


    /**
     * Gets the horAtu value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return horAtu
     */
    public java.lang.Integer getHorAtu() {
        return horAtu;
    }


    /**
     * Sets the horAtu value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param horAtu
     */
    public void setHorAtu(java.lang.Integer horAtu) {
        this.horAtu = horAtu;
    }


    /**
     * Gets the horMot value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return horMot
     */
    public java.lang.Integer getHorMot() {
        return horMot;
    }


    /**
     * Sets the horMot value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param horMot
     */
    public void setHorMot(java.lang.Integer horMot) {
        this.horMot = horMot;
    }


    /**
     * Gets the ideCli value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return ideCli
     */
    public java.lang.String getIdeCli() {
        return ideCli;
    }


    /**
     * Sets the ideCli value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param ideCli
     */
    public void setIdeCli(java.lang.String ideCli) {
        this.ideCli = ideCli;
    }


    /**
     * Gets the iniCob value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return iniCob
     */
    public java.lang.Integer getIniCob() {
        return iniCob;
    }


    /**
     * Sets the iniCob value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param iniCob
     */
    public void setIniCob(java.lang.Integer iniCob) {
        this.iniCob = iniCob;
    }


    /**
     * Gets the insEst value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return insEst
     */
    public java.lang.String getInsEst() {
        return insEst;
    }


    /**
     * Sets the insEst value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param insEst
     */
    public void setInsEst(java.lang.String insEst) {
        this.insEst = insEst;
    }


    /**
     * Gets the insMun value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return insMun
     */
    public java.lang.String getInsMun() {
        return insMun;
    }


    /**
     * Sets the insMun value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param insMun
     */
    public void setInsMun(java.lang.String insMun) {
        this.insMun = insMun;
    }


    /**
     * Gets the intNet value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return intNet
     */
    public java.lang.String getIntNet() {
        return intNet;
    }


    /**
     * Sets the intNet value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param intNet
     */
    public void setIntNet(java.lang.String intNet) {
        this.intNet = intNet;
    }


    /**
     * Gets the maiAtr value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return maiAtr
     */
    public java.lang.Integer getMaiAtr() {
        return maiAtr;
    }


    /**
     * Sets the maiAtr value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param maiAtr
     */
    public void setMaiAtr(java.lang.Integer maiAtr) {
        this.maiAtr = maiAtr;
    }


    /**
     * Gets the medAtr value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return medAtr
     */
    public java.lang.Integer getMedAtr() {
        return medAtr;
    }


    /**
     * Sets the medAtr value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param medAtr
     */
    public void setMedAtr(java.lang.Integer medAtr) {
        this.medAtr = medAtr;
    }


    /**
     * Gets the nenCli value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return nenCli
     */
    public java.lang.String getNenCli() {
        return nenCli;
    }


    /**
     * Sets the nenCli value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param nenCli
     */
    public void setNenCli(java.lang.String nenCli) {
        this.nenCli = nenCli;
    }


    /**
     * Gets the nenCob value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return nenCob
     */
    public java.lang.String getNenCob() {
        return nenCob;
    }


    /**
     * Sets the nenCob value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param nenCob
     */
    public void setNenCob(java.lang.String nenCob) {
        this.nenCob = nenCob;
    }


    /**
     * Gets the nomCli value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return nomCli
     */
    public java.lang.String getNomCli() {
        return nomCli;
    }


    /**
     * Sets the nomCli value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param nomCli
     */
    public void setNomCli(java.lang.String nomCli) {
        this.nomCli = nomCli;
    }


    /**
     * Gets the obsMot value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return obsMot
     */
    public java.lang.String getObsMot() {
        return obsMot;
    }


    /**
     * Sets the obsMot value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param obsMot
     */
    public void setObsMot(java.lang.String obsMot) {
        this.obsMot = obsMot;
    }


    /**
     * Gets the perDsc value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return perDsc
     */
    public java.lang.Double getPerDsc() {
        return perDsc;
    }


    /**
     * Sets the perDsc value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param perDsc
     */
    public void setPerDsc(java.lang.Double perDsc) {
        this.perDsc = perDsc;
    }


    /**
     * Gets the qtdChs value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return qtdChs
     */
    public java.lang.Integer getQtdChs() {
        return qtdChs;
    }


    /**
     * Sets the qtdChs value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param qtdChs
     */
    public void setQtdChs(java.lang.Integer qtdChs) {
        this.qtdChs = qtdChs;
    }


    /**
     * Gets the recDtj value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return recDtj
     */
    public java.lang.Integer getRecDtj() {
        return recDtj;
    }


    /**
     * Sets the recDtj value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param recDtj
     */
    public void setRecDtj(java.lang.Integer recDtj) {
        this.recDtj = recDtj;
    }


    /**
     * Gets the recDtm value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return recDtm
     */
    public java.lang.Integer getRecDtm() {
        return recDtm;
    }


    /**
     * Sets the recDtm value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param recDtm
     */
    public void setRecDtm(java.lang.Integer recDtm) {
        this.recDtm = recDtm;
    }


    /**
     * Gets the recJmm value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return recJmm
     */
    public java.lang.Double getRecJmm() {
        return recJmm;
    }


    /**
     * Sets the recJmm value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param recJmm
     */
    public void setRecJmm(java.lang.Double recJmm) {
        this.recJmm = recJmm;
    }


    /**
     * Gets the recMul value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return recMul
     */
    public java.lang.Double getRecMul() {
        return recMul;
    }


    /**
     * Sets the recMul value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param recMul
     */
    public void setRecMul(java.lang.Double recMul) {
        this.recMul = recMul;
    }


    /**
     * Gets the recTjr value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return recTjr
     */
    public java.lang.String getRecTjr() {
        return recTjr;
    }


    /**
     * Sets the recTjr value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param recTjr
     */
    public void setRecTjr(java.lang.String recTjr) {
        this.recTjr = recTjr;
    }


    /**
     * Gets the salCre value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return salCre
     */
    public java.lang.Double getSalCre() {
        return salCre;
    }


    /**
     * Sets the salCre value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param salCre
     */
    public void setSalCre(java.lang.Double salCre) {
        this.salCre = salCre;
    }


    /**
     * Gets the salDup value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return salDup
     */
    public java.lang.Double getSalDup() {
        return salDup;
    }


    /**
     * Sets the salDup value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param salDup
     */
    public void setSalDup(java.lang.Double salDup) {
        this.salDup = salDup;
    }


    /**
     * Gets the salOut value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return salOut
     */
    public java.lang.Double getSalOut() {
        return salOut;
    }


    /**
     * Sets the salOut value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param salOut
     */
    public void setSalOut(java.lang.Double salOut) {
        this.salOut = salOut;
    }


    /**
     * Gets the seqCob value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return seqCob
     */
    public java.lang.Integer getSeqCob() {
        return seqCob;
    }


    /**
     * Sets the seqCob value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param seqCob
     */
    public void setSeqCob(java.lang.Integer seqCob) {
        this.seqCob = seqCob;
    }


    /**
     * Gets the sigUfs value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return sigUfs
     */
    public java.lang.String getSigUfs() {
        return sigUfs;
    }


    /**
     * Sets the sigUfs value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param sigUfs
     */
    public void setSigUfs(java.lang.String sigUfs) {
        this.sigUfs = sigUfs;
    }


    /**
     * Gets the sitCli value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return sitCli
     */
    public java.lang.String getSitCli() {
        return sitCli;
    }


    /**
     * Sets the sitCli value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param sitCli
     */
    public void setSitCli(java.lang.String sitCli) {
        this.sitCli = sitCli;
    }


    /**
     * Gets the tipCli value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return tipCli
     */
    public java.lang.String getTipCli() {
        return tipCli;
    }


    /**
     * Sets the tipCli value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param tipCli
     */
    public void setTipCli(java.lang.String tipCli) {
        this.tipCli = tipCli;
    }


    /**
     * Gets the tipEmc value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return tipEmc
     */
    public java.lang.Integer getTipEmc() {
        return tipEmc;
    }


    /**
     * Sets the tipEmc value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param tipEmc
     */
    public void setTipEmc(java.lang.Integer tipEmc) {
        this.tipEmc = tipEmc;
    }


    /**
     * Gets the tipMer value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return tipMer
     */
    public java.lang.String getTipMer() {
        return tipMer;
    }


    /**
     * Sets the tipMer value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param tipMer
     */
    public void setTipMer(java.lang.String tipMer) {
        this.tipMer = tipMer;
    }


    /**
     * Gets the tolDsc value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return tolDsc
     */
    public java.lang.Double getTolDsc() {
        return tolDsc;
    }


    /**
     * Sets the tolDsc value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param tolDsc
     */
    public void setTolDsc(java.lang.Double tolDsc) {
        this.tolDsc = tolDsc;
    }


    /**
     * Gets the usuAtu value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return usuAtu
     */
    public java.lang.Integer getUsuAtu() {
        return usuAtu;
    }


    /**
     * Sets the usuAtu value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param usuAtu
     */
    public void setUsuAtu(java.lang.Integer usuAtu) {
        this.usuAtu = usuAtu;
    }


    /**
     * Gets the zipCod value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @return zipCod
     */
    public java.lang.String getZipCod() {
        return zipCod;
    }


    /**
     * Sets the zipCod value for this ClientesExportarClientesCompletoOutClientes.
     * 
     * @param zipCod
     */
    public void setZipCod(java.lang.String zipCod) {
        this.zipCod = zipCod;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientesExportarClientesCompletoOutClientes)) return false;
        ClientesExportarClientesCompletoOutClientes other = (ClientesExportarClientesCompletoOutClientes) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.apeCli==null && other.getApeCli()==null) || 
             (this.apeCli!=null &&
              this.apeCli.equals(other.getApeCli()))) &&
            ((this.baiCli==null && other.getBaiCli()==null) || 
             (this.baiCli!=null &&
              this.baiCli.equals(other.getBaiCli()))) &&
            ((this.baiCob==null && other.getBaiCob()==null) || 
             (this.baiCob!=null &&
              this.baiCob.equals(other.getBaiCob()))) &&
            ((this.cepCli==null && other.getCepCli()==null) || 
             (this.cepCli!=null &&
              this.cepCli.equals(other.getCepCli()))) &&
            ((this.cepCob==null && other.getCepCob()==null) || 
             (this.cepCob!=null &&
              this.cepCob.equals(other.getCepCob()))) &&
            ((this.cepIni==null && other.getCepIni()==null) || 
             (this.cepIni!=null &&
              this.cepIni.equals(other.getCepIni()))) &&
            ((this.cgcCob==null && other.getCgcCob()==null) || 
             (this.cgcCob!=null &&
              this.cgcCob.equals(other.getCgcCob()))) &&
            ((this.cgcCpf==null && other.getCgcCpf()==null) || 
             (this.cgcCpf!=null &&
              this.cgcCpf.equals(other.getCgcCpf()))) &&
            ((this.cidCli==null && other.getCidCli()==null) || 
             (this.cidCli!=null &&
              this.cidCli.equals(other.getCidCli()))) &&
            ((this.cidCob==null && other.getCidCob()==null) || 
             (this.cidCob!=null &&
              this.cidCob.equals(other.getCidCob()))) &&
            ((this.cliPrx==null && other.getCliPrx()==null) || 
             (this.cliPrx!=null &&
              this.cliPrx.equals(other.getCliPrx()))) &&
            ((this.cliRep==null && other.getCliRep()==null) || 
             (this.cliRep!=null &&
              this.cliRep.equals(other.getCliRep()))) &&
            ((this.codCli==null && other.getCodCli()==null) || 
             (this.codCli!=null &&
              this.codCli.equals(other.getCodCli()))) &&
            ((this.codCpg==null && other.getCodCpg()==null) || 
             (this.codCpg!=null &&
              this.codCpg.equals(other.getCodCpg()))) &&
            ((this.codCrp==null && other.getCodCrp()==null) || 
             (this.codCrp!=null &&
              this.codCrp.equals(other.getCodCrp()))) &&
            ((this.codCrt==null && other.getCodCrt()==null) || 
             (this.codCrt!=null &&
              this.codCrt.equals(other.getCodCrt()))) &&
            ((this.codFor==null && other.getCodFor()==null) || 
             (this.codFor!=null &&
              this.codFor.equals(other.getCodFor()))) &&
            ((this.codFpg==null && other.getCodFpg()==null) || 
             (this.codFpg!=null &&
              this.codFpg.equals(other.getCodFpg()))) &&
            ((this.codGre==null && other.getCodGre()==null) || 
             (this.codGre!=null &&
              this.codGre.equals(other.getCodGre()))) &&
            ((this.codMot==null && other.getCodMot()==null) || 
             (this.codMot!=null &&
              this.codMot.equals(other.getCodMot()))) &&
            ((this.codMs2==null && other.getCodMs2()==null) || 
             (this.codMs2!=null &&
              this.codMs2.equals(other.getCodMs2()))) &&
            ((this.codMs3==null && other.getCodMs3()==null) || 
             (this.codMs3!=null &&
              this.codMs3.equals(other.getCodMs3()))) &&
            ((this.codMs4==null && other.getCodMs4()==null) || 
             (this.codMs4!=null &&
              this.codMs4.equals(other.getCodMs4()))) &&
            ((this.codMsg==null && other.getCodMsg()==null) || 
             (this.codMsg!=null &&
              this.codMsg.equals(other.getCodMsg()))) &&
            ((this.codPai==null && other.getCodPai()==null) || 
             (this.codPai!=null &&
              this.codPai.equals(other.getCodPai()))) &&
            ((this.codRep==null && other.getCodRep()==null) || 
             (this.codRep!=null &&
              this.codRep.equals(other.getCodRep()))) &&
            ((this.codSuf==null && other.getCodSuf()==null) || 
             (this.codSuf!=null &&
              this.codSuf.equals(other.getCodSuf()))) &&
            ((this.cplCob==null && other.getCplCob()==null) || 
             (this.cplCob!=null &&
              this.cplCob.equals(other.getCplCob()))) &&
            ((this.cplEnd==null && other.getCplEnd()==null) || 
             (this.cplEnd!=null &&
              this.cplEnd.equals(other.getCplEnd()))) &&
            ((this.ctrPad==null && other.getCtrPad()==null) || 
             (this.ctrPad!=null &&
              this.ctrPad.equals(other.getCtrPad()))) &&
            ((this.cxaPst==null && other.getCxaPst()==null) || 
             (this.cxaPst!=null &&
              this.cxaPst.equals(other.getCxaPst()))) &&
            ((this.datAtu==null && other.getDatAtu()==null) || 
             (this.datAtu!=null &&
              this.datAtu.equals(other.getDatAtu()))) &&
            ((this.datMot==null && other.getDatMot()==null) || 
             (this.datMot!=null &&
              this.datMot.equals(other.getDatMot()))) &&
            ((this.emaNfe==null && other.getEmaNfe()==null) || 
             (this.emaNfe!=null &&
              this.emaNfe.equals(other.getEmaNfe()))) &&
            ((this.endCli==null && other.getEndCli()==null) || 
             (this.endCli!=null &&
              this.endCli.equals(other.getEndCli()))) &&
            ((this.endCob==null && other.getEndCob()==null) || 
             (this.endCob!=null &&
              this.endCob.equals(other.getEndCob()))) &&
            ((this.estCob==null && other.getEstCob()==null) || 
             (this.estCob!=null &&
              this.estCob.equals(other.getEstCob()))) &&
            ((this.faxCli==null && other.getFaxCli()==null) || 
             (this.faxCli!=null &&
              this.faxCli.equals(other.getFaxCli()))) &&
            ((this.fonCl2==null && other.getFonCl2()==null) || 
             (this.fonCl2!=null &&
              this.fonCl2.equals(other.getFonCl2()))) &&
            ((this.fonCl3==null && other.getFonCl3()==null) || 
             (this.fonCl3!=null &&
              this.fonCl3.equals(other.getFonCl3()))) &&
            ((this.fonCl4==null && other.getFonCl4()==null) || 
             (this.fonCl4!=null &&
              this.fonCl4.equals(other.getFonCl4()))) &&
            ((this.fonCl5==null && other.getFonCl5()==null) || 
             (this.fonCl5!=null &&
              this.fonCl5.equals(other.getFonCl5()))) &&
            ((this.fonCli==null && other.getFonCli()==null) || 
             (this.fonCli!=null &&
              this.fonCli.equals(other.getFonCli()))) &&
            ((this.horAtu==null && other.getHorAtu()==null) || 
             (this.horAtu!=null &&
              this.horAtu.equals(other.getHorAtu()))) &&
            ((this.horMot==null && other.getHorMot()==null) || 
             (this.horMot!=null &&
              this.horMot.equals(other.getHorMot()))) &&
            ((this.ideCli==null && other.getIdeCli()==null) || 
             (this.ideCli!=null &&
              this.ideCli.equals(other.getIdeCli()))) &&
            ((this.iniCob==null && other.getIniCob()==null) || 
             (this.iniCob!=null &&
              this.iniCob.equals(other.getIniCob()))) &&
            ((this.insEst==null && other.getInsEst()==null) || 
             (this.insEst!=null &&
              this.insEst.equals(other.getInsEst()))) &&
            ((this.insMun==null && other.getInsMun()==null) || 
             (this.insMun!=null &&
              this.insMun.equals(other.getInsMun()))) &&
            ((this.intNet==null && other.getIntNet()==null) || 
             (this.intNet!=null &&
              this.intNet.equals(other.getIntNet()))) &&
            ((this.maiAtr==null && other.getMaiAtr()==null) || 
             (this.maiAtr!=null &&
              this.maiAtr.equals(other.getMaiAtr()))) &&
            ((this.medAtr==null && other.getMedAtr()==null) || 
             (this.medAtr!=null &&
              this.medAtr.equals(other.getMedAtr()))) &&
            ((this.nenCli==null && other.getNenCli()==null) || 
             (this.nenCli!=null &&
              this.nenCli.equals(other.getNenCli()))) &&
            ((this.nenCob==null && other.getNenCob()==null) || 
             (this.nenCob!=null &&
              this.nenCob.equals(other.getNenCob()))) &&
            ((this.nomCli==null && other.getNomCli()==null) || 
             (this.nomCli!=null &&
              this.nomCli.equals(other.getNomCli()))) &&
            ((this.obsMot==null && other.getObsMot()==null) || 
             (this.obsMot!=null &&
              this.obsMot.equals(other.getObsMot()))) &&
            ((this.perDsc==null && other.getPerDsc()==null) || 
             (this.perDsc!=null &&
              this.perDsc.equals(other.getPerDsc()))) &&
            ((this.qtdChs==null && other.getQtdChs()==null) || 
             (this.qtdChs!=null &&
              this.qtdChs.equals(other.getQtdChs()))) &&
            ((this.recDtj==null && other.getRecDtj()==null) || 
             (this.recDtj!=null &&
              this.recDtj.equals(other.getRecDtj()))) &&
            ((this.recDtm==null && other.getRecDtm()==null) || 
             (this.recDtm!=null &&
              this.recDtm.equals(other.getRecDtm()))) &&
            ((this.recJmm==null && other.getRecJmm()==null) || 
             (this.recJmm!=null &&
              this.recJmm.equals(other.getRecJmm()))) &&
            ((this.recMul==null && other.getRecMul()==null) || 
             (this.recMul!=null &&
              this.recMul.equals(other.getRecMul()))) &&
            ((this.recTjr==null && other.getRecTjr()==null) || 
             (this.recTjr!=null &&
              this.recTjr.equals(other.getRecTjr()))) &&
            ((this.salCre==null && other.getSalCre()==null) || 
             (this.salCre!=null &&
              this.salCre.equals(other.getSalCre()))) &&
            ((this.salDup==null && other.getSalDup()==null) || 
             (this.salDup!=null &&
              this.salDup.equals(other.getSalDup()))) &&
            ((this.salOut==null && other.getSalOut()==null) || 
             (this.salOut!=null &&
              this.salOut.equals(other.getSalOut()))) &&
            ((this.seqCob==null && other.getSeqCob()==null) || 
             (this.seqCob!=null &&
              this.seqCob.equals(other.getSeqCob()))) &&
            ((this.sigUfs==null && other.getSigUfs()==null) || 
             (this.sigUfs!=null &&
              this.sigUfs.equals(other.getSigUfs()))) &&
            ((this.sitCli==null && other.getSitCli()==null) || 
             (this.sitCli!=null &&
              this.sitCli.equals(other.getSitCli()))) &&
            ((this.tipCli==null && other.getTipCli()==null) || 
             (this.tipCli!=null &&
              this.tipCli.equals(other.getTipCli()))) &&
            ((this.tipEmc==null && other.getTipEmc()==null) || 
             (this.tipEmc!=null &&
              this.tipEmc.equals(other.getTipEmc()))) &&
            ((this.tipMer==null && other.getTipMer()==null) || 
             (this.tipMer!=null &&
              this.tipMer.equals(other.getTipMer()))) &&
            ((this.tolDsc==null && other.getTolDsc()==null) || 
             (this.tolDsc!=null &&
              this.tolDsc.equals(other.getTolDsc()))) &&
            ((this.usuAtu==null && other.getUsuAtu()==null) || 
             (this.usuAtu!=null &&
              this.usuAtu.equals(other.getUsuAtu()))) &&
            ((this.zipCod==null && other.getZipCod()==null) || 
             (this.zipCod!=null &&
              this.zipCod.equals(other.getZipCod())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getApeCli() != null) {
            _hashCode += getApeCli().hashCode();
        }
        if (getBaiCli() != null) {
            _hashCode += getBaiCli().hashCode();
        }
        if (getBaiCob() != null) {
            _hashCode += getBaiCob().hashCode();
        }
        if (getCepCli() != null) {
            _hashCode += getCepCli().hashCode();
        }
        if (getCepCob() != null) {
            _hashCode += getCepCob().hashCode();
        }
        if (getCepIni() != null) {
            _hashCode += getCepIni().hashCode();
        }
        if (getCgcCob() != null) {
            _hashCode += getCgcCob().hashCode();
        }
        if (getCgcCpf() != null) {
            _hashCode += getCgcCpf().hashCode();
        }
        if (getCidCli() != null) {
            _hashCode += getCidCli().hashCode();
        }
        if (getCidCob() != null) {
            _hashCode += getCidCob().hashCode();
        }
        if (getCliPrx() != null) {
            _hashCode += getCliPrx().hashCode();
        }
        if (getCliRep() != null) {
            _hashCode += getCliRep().hashCode();
        }
        if (getCodCli() != null) {
            _hashCode += getCodCli().hashCode();
        }
        if (getCodCpg() != null) {
            _hashCode += getCodCpg().hashCode();
        }
        if (getCodCrp() != null) {
            _hashCode += getCodCrp().hashCode();
        }
        if (getCodCrt() != null) {
            _hashCode += getCodCrt().hashCode();
        }
        if (getCodFor() != null) {
            _hashCode += getCodFor().hashCode();
        }
        if (getCodFpg() != null) {
            _hashCode += getCodFpg().hashCode();
        }
        if (getCodGre() != null) {
            _hashCode += getCodGre().hashCode();
        }
        if (getCodMot() != null) {
            _hashCode += getCodMot().hashCode();
        }
        if (getCodMs2() != null) {
            _hashCode += getCodMs2().hashCode();
        }
        if (getCodMs3() != null) {
            _hashCode += getCodMs3().hashCode();
        }
        if (getCodMs4() != null) {
            _hashCode += getCodMs4().hashCode();
        }
        if (getCodMsg() != null) {
            _hashCode += getCodMsg().hashCode();
        }
        if (getCodPai() != null) {
            _hashCode += getCodPai().hashCode();
        }
        if (getCodRep() != null) {
            _hashCode += getCodRep().hashCode();
        }
        if (getCodSuf() != null) {
            _hashCode += getCodSuf().hashCode();
        }
        if (getCplCob() != null) {
            _hashCode += getCplCob().hashCode();
        }
        if (getCplEnd() != null) {
            _hashCode += getCplEnd().hashCode();
        }
        if (getCtrPad() != null) {
            _hashCode += getCtrPad().hashCode();
        }
        if (getCxaPst() != null) {
            _hashCode += getCxaPst().hashCode();
        }
        if (getDatAtu() != null) {
            _hashCode += getDatAtu().hashCode();
        }
        if (getDatMot() != null) {
            _hashCode += getDatMot().hashCode();
        }
        if (getEmaNfe() != null) {
            _hashCode += getEmaNfe().hashCode();
        }
        if (getEndCli() != null) {
            _hashCode += getEndCli().hashCode();
        }
        if (getEndCob() != null) {
            _hashCode += getEndCob().hashCode();
        }
        if (getEstCob() != null) {
            _hashCode += getEstCob().hashCode();
        }
        if (getFaxCli() != null) {
            _hashCode += getFaxCli().hashCode();
        }
        if (getFonCl2() != null) {
            _hashCode += getFonCl2().hashCode();
        }
        if (getFonCl3() != null) {
            _hashCode += getFonCl3().hashCode();
        }
        if (getFonCl4() != null) {
            _hashCode += getFonCl4().hashCode();
        }
        if (getFonCl5() != null) {
            _hashCode += getFonCl5().hashCode();
        }
        if (getFonCli() != null) {
            _hashCode += getFonCli().hashCode();
        }
        if (getHorAtu() != null) {
            _hashCode += getHorAtu().hashCode();
        }
        if (getHorMot() != null) {
            _hashCode += getHorMot().hashCode();
        }
        if (getIdeCli() != null) {
            _hashCode += getIdeCli().hashCode();
        }
        if (getIniCob() != null) {
            _hashCode += getIniCob().hashCode();
        }
        if (getInsEst() != null) {
            _hashCode += getInsEst().hashCode();
        }
        if (getInsMun() != null) {
            _hashCode += getInsMun().hashCode();
        }
        if (getIntNet() != null) {
            _hashCode += getIntNet().hashCode();
        }
        if (getMaiAtr() != null) {
            _hashCode += getMaiAtr().hashCode();
        }
        if (getMedAtr() != null) {
            _hashCode += getMedAtr().hashCode();
        }
        if (getNenCli() != null) {
            _hashCode += getNenCli().hashCode();
        }
        if (getNenCob() != null) {
            _hashCode += getNenCob().hashCode();
        }
        if (getNomCli() != null) {
            _hashCode += getNomCli().hashCode();
        }
        if (getObsMot() != null) {
            _hashCode += getObsMot().hashCode();
        }
        if (getPerDsc() != null) {
            _hashCode += getPerDsc().hashCode();
        }
        if (getQtdChs() != null) {
            _hashCode += getQtdChs().hashCode();
        }
        if (getRecDtj() != null) {
            _hashCode += getRecDtj().hashCode();
        }
        if (getRecDtm() != null) {
            _hashCode += getRecDtm().hashCode();
        }
        if (getRecJmm() != null) {
            _hashCode += getRecJmm().hashCode();
        }
        if (getRecMul() != null) {
            _hashCode += getRecMul().hashCode();
        }
        if (getRecTjr() != null) {
            _hashCode += getRecTjr().hashCode();
        }
        if (getSalCre() != null) {
            _hashCode += getSalCre().hashCode();
        }
        if (getSalDup() != null) {
            _hashCode += getSalDup().hashCode();
        }
        if (getSalOut() != null) {
            _hashCode += getSalOut().hashCode();
        }
        if (getSeqCob() != null) {
            _hashCode += getSeqCob().hashCode();
        }
        if (getSigUfs() != null) {
            _hashCode += getSigUfs().hashCode();
        }
        if (getSitCli() != null) {
            _hashCode += getSitCli().hashCode();
        }
        if (getTipCli() != null) {
            _hashCode += getTipCli().hashCode();
        }
        if (getTipEmc() != null) {
            _hashCode += getTipEmc().hashCode();
        }
        if (getTipMer() != null) {
            _hashCode += getTipMer().hashCode();
        }
        if (getTolDsc() != null) {
            _hashCode += getTolDsc().hashCode();
        }
        if (getUsuAtu() != null) {
            _hashCode += getUsuAtu().hashCode();
        }
        if (getZipCod() != null) {
            _hashCode += getZipCod().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientesExportarClientesCompletoOutClientes.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesExportarClientesCompletoOutClientes"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("apeCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "apeCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baiCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "baiCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baiCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "baiCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cepCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cepCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepIni");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cepIni"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cgcCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cgcCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cgcCpf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cgcCpf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cidCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cidCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cidCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cidCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cliPrx");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cliPrx"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cliRep");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cliRep"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCpg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCpg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCrp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCrp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCrt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCrt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFpg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFpg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codGre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codGre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMs2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMs2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMs3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMs3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMs4");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMs4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMsg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMsg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPai");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPai"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRep");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRep"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codSuf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codSuf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cplCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cplCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cplEnd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cplEnd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctrPad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctrPad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cxaPst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cxaPst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAtu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAtu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datMot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datMot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emaNfe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "emaNfe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("faxCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "faxCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonCl2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonCl2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonCl3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonCl3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonCl4");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonCl4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonCl5");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonCl5"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horAtu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horAtu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horMot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horMot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ideCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ideCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("iniCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "iniCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insEst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "insEst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insMun");
        elemField.setXmlName(new javax.xml.namespace.QName("", "insMun"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("intNet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "intNet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maiAtr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "maiAtr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("medAtr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "medAtr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nenCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nenCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nenCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nenCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsMot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsMot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qtdChs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qtdChs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recDtj");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recDtj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recDtm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recDtm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recJmm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recJmm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recMul");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recMul"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recTjr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recTjr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("salCre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "salCre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("salDup");
        elemField.setXmlName(new javax.xml.namespace.QName("", "salDup"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("salOut");
        elemField.setXmlName(new javax.xml.namespace.QName("", "salOut"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sigUfs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sigUfs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipEmc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipEmc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipMer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipMer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tolDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tolDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuAtu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuAtu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zipCod");
        elemField.setXmlName(new javax.xml.namespace.QName("", "zipCod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
