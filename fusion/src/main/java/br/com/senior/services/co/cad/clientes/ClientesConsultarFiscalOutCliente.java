/**
 * ClientesConsultarFiscalOutCliente.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package br.com.senior.services.co.cad.clientes;

public class ClientesConsultarFiscalOutCliente  implements java.io.Serializable {
    private java.lang.String calFun;

    private java.lang.String calSen;

    private br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutClienteCampoUsuarioCliente[] campoUsuarioCliente;

    private java.lang.Integer codCli;

    private java.lang.Integer codCnv;

    private java.lang.Integer codMs2;

    private java.lang.Integer codMs3;

    private java.lang.Integer codMs4;

    private java.lang.Integer codMsg;

    private java.lang.Integer codRtr;

    private java.lang.String conFin;

    private java.lang.String datSuf;

    private java.lang.String emaNfe;

    private br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutClienteHistorico[] historico;

    private java.lang.String limRet;

    private java.lang.Integer natCof;

    private java.lang.Integer natPis;

    private java.lang.Integer natRet;

    private java.lang.String nomCli;

    private java.lang.String numIdf;

    private java.lang.Double perAin;

    private java.lang.Integer regEst;

    private java.lang.String retCof;

    private java.lang.String retCsl;

    private java.lang.String retIrf;

    private java.lang.String retOur;

    private java.lang.String retPis;

    private java.lang.String retPro;

    private java.lang.String triCof;

    private java.lang.String triPis;

    public ClientesConsultarFiscalOutCliente() {
    }

    public ClientesConsultarFiscalOutCliente(
           java.lang.String calFun,
           java.lang.String calSen,
           br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutClienteCampoUsuarioCliente[] campoUsuarioCliente,
           java.lang.Integer codCli,
           java.lang.Integer codCnv,
           java.lang.Integer codMs2,
           java.lang.Integer codMs3,
           java.lang.Integer codMs4,
           java.lang.Integer codMsg,
           java.lang.Integer codRtr,
           java.lang.String conFin,
           java.lang.String datSuf,
           java.lang.String emaNfe,
           br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutClienteHistorico[] historico,
           java.lang.String limRet,
           java.lang.Integer natCof,
           java.lang.Integer natPis,
           java.lang.Integer natRet,
           java.lang.String nomCli,
           java.lang.String numIdf,
           java.lang.Double perAin,
           java.lang.Integer regEst,
           java.lang.String retCof,
           java.lang.String retCsl,
           java.lang.String retIrf,
           java.lang.String retOur,
           java.lang.String retPis,
           java.lang.String retPro,
           java.lang.String triCof,
           java.lang.String triPis) {
           this.calFun = calFun;
           this.calSen = calSen;
           this.campoUsuarioCliente = campoUsuarioCliente;
           this.codCli = codCli;
           this.codCnv = codCnv;
           this.codMs2 = codMs2;
           this.codMs3 = codMs3;
           this.codMs4 = codMs4;
           this.codMsg = codMsg;
           this.codRtr = codRtr;
           this.conFin = conFin;
           this.datSuf = datSuf;
           this.emaNfe = emaNfe;
           this.historico = historico;
           this.limRet = limRet;
           this.natCof = natCof;
           this.natPis = natPis;
           this.natRet = natRet;
           this.nomCli = nomCli;
           this.numIdf = numIdf;
           this.perAin = perAin;
           this.regEst = regEst;
           this.retCof = retCof;
           this.retCsl = retCsl;
           this.retIrf = retIrf;
           this.retOur = retOur;
           this.retPis = retPis;
           this.retPro = retPro;
           this.triCof = triCof;
           this.triPis = triPis;
    }


    /**
     * Gets the calFun value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return calFun
     */
    public java.lang.String getCalFun() {
        return calFun;
    }


    /**
     * Sets the calFun value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param calFun
     */
    public void setCalFun(java.lang.String calFun) {
        this.calFun = calFun;
    }


    /**
     * Gets the calSen value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return calSen
     */
    public java.lang.String getCalSen() {
        return calSen;
    }


    /**
     * Sets the calSen value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param calSen
     */
    public void setCalSen(java.lang.String calSen) {
        this.calSen = calSen;
    }


    /**
     * Gets the campoUsuarioCliente value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return campoUsuarioCliente
     */
    public br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutClienteCampoUsuarioCliente[] getCampoUsuarioCliente() {
        return campoUsuarioCliente;
    }


    /**
     * Sets the campoUsuarioCliente value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param campoUsuarioCliente
     */
    public void setCampoUsuarioCliente(br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutClienteCampoUsuarioCliente[] campoUsuarioCliente) {
        this.campoUsuarioCliente = campoUsuarioCliente;
    }

    public br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutClienteCampoUsuarioCliente getCampoUsuarioCliente(int i) {
        return this.campoUsuarioCliente[i];
    }

    public void setCampoUsuarioCliente(int i, br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutClienteCampoUsuarioCliente _value) {
        this.campoUsuarioCliente[i] = _value;
    }


    /**
     * Gets the codCli value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return codCli
     */
    public java.lang.Integer getCodCli() {
        return codCli;
    }


    /**
     * Sets the codCli value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param codCli
     */
    public void setCodCli(java.lang.Integer codCli) {
        this.codCli = codCli;
    }


    /**
     * Gets the codCnv value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return codCnv
     */
    public java.lang.Integer getCodCnv() {
        return codCnv;
    }


    /**
     * Sets the codCnv value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param codCnv
     */
    public void setCodCnv(java.lang.Integer codCnv) {
        this.codCnv = codCnv;
    }


    /**
     * Gets the codMs2 value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return codMs2
     */
    public java.lang.Integer getCodMs2() {
        return codMs2;
    }


    /**
     * Sets the codMs2 value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param codMs2
     */
    public void setCodMs2(java.lang.Integer codMs2) {
        this.codMs2 = codMs2;
    }


    /**
     * Gets the codMs3 value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return codMs3
     */
    public java.lang.Integer getCodMs3() {
        return codMs3;
    }


    /**
     * Sets the codMs3 value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param codMs3
     */
    public void setCodMs3(java.lang.Integer codMs3) {
        this.codMs3 = codMs3;
    }


    /**
     * Gets the codMs4 value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return codMs4
     */
    public java.lang.Integer getCodMs4() {
        return codMs4;
    }


    /**
     * Sets the codMs4 value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param codMs4
     */
    public void setCodMs4(java.lang.Integer codMs4) {
        this.codMs4 = codMs4;
    }


    /**
     * Gets the codMsg value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return codMsg
     */
    public java.lang.Integer getCodMsg() {
        return codMsg;
    }


    /**
     * Sets the codMsg value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param codMsg
     */
    public void setCodMsg(java.lang.Integer codMsg) {
        this.codMsg = codMsg;
    }


    /**
     * Gets the codRtr value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return codRtr
     */
    public java.lang.Integer getCodRtr() {
        return codRtr;
    }


    /**
     * Sets the codRtr value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param codRtr
     */
    public void setCodRtr(java.lang.Integer codRtr) {
        this.codRtr = codRtr;
    }


    /**
     * Gets the conFin value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return conFin
     */
    public java.lang.String getConFin() {
        return conFin;
    }


    /**
     * Sets the conFin value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param conFin
     */
    public void setConFin(java.lang.String conFin) {
        this.conFin = conFin;
    }


    /**
     * Gets the datSuf value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return datSuf
     */
    public java.lang.String getDatSuf() {
        return datSuf;
    }


    /**
     * Sets the datSuf value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param datSuf
     */
    public void setDatSuf(java.lang.String datSuf) {
        this.datSuf = datSuf;
    }


    /**
     * Gets the emaNfe value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return emaNfe
     */
    public java.lang.String getEmaNfe() {
        return emaNfe;
    }


    /**
     * Sets the emaNfe value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param emaNfe
     */
    public void setEmaNfe(java.lang.String emaNfe) {
        this.emaNfe = emaNfe;
    }


    /**
     * Gets the historico value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return historico
     */
    public br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutClienteHistorico[] getHistorico() {
        return historico;
    }


    /**
     * Sets the historico value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param historico
     */
    public void setHistorico(br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutClienteHistorico[] historico) {
        this.historico = historico;
    }

    public br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutClienteHistorico getHistorico(int i) {
        return this.historico[i];
    }

    public void setHistorico(int i, br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalOutClienteHistorico _value) {
        this.historico[i] = _value;
    }


    /**
     * Gets the limRet value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return limRet
     */
    public java.lang.String getLimRet() {
        return limRet;
    }


    /**
     * Sets the limRet value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param limRet
     */
    public void setLimRet(java.lang.String limRet) {
        this.limRet = limRet;
    }


    /**
     * Gets the natCof value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return natCof
     */
    public java.lang.Integer getNatCof() {
        return natCof;
    }


    /**
     * Sets the natCof value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param natCof
     */
    public void setNatCof(java.lang.Integer natCof) {
        this.natCof = natCof;
    }


    /**
     * Gets the natPis value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return natPis
     */
    public java.lang.Integer getNatPis() {
        return natPis;
    }


    /**
     * Sets the natPis value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param natPis
     */
    public void setNatPis(java.lang.Integer natPis) {
        this.natPis = natPis;
    }


    /**
     * Gets the natRet value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return natRet
     */
    public java.lang.Integer getNatRet() {
        return natRet;
    }


    /**
     * Sets the natRet value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param natRet
     */
    public void setNatRet(java.lang.Integer natRet) {
        this.natRet = natRet;
    }


    /**
     * Gets the nomCli value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return nomCli
     */
    public java.lang.String getNomCli() {
        return nomCli;
    }


    /**
     * Sets the nomCli value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param nomCli
     */
    public void setNomCli(java.lang.String nomCli) {
        this.nomCli = nomCli;
    }


    /**
     * Gets the numIdf value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return numIdf
     */
    public java.lang.String getNumIdf() {
        return numIdf;
    }


    /**
     * Sets the numIdf value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param numIdf
     */
    public void setNumIdf(java.lang.String numIdf) {
        this.numIdf = numIdf;
    }


    /**
     * Gets the perAin value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return perAin
     */
    public java.lang.Double getPerAin() {
        return perAin;
    }


    /**
     * Sets the perAin value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param perAin
     */
    public void setPerAin(java.lang.Double perAin) {
        this.perAin = perAin;
    }


    /**
     * Gets the regEst value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return regEst
     */
    public java.lang.Integer getRegEst() {
        return regEst;
    }


    /**
     * Sets the regEst value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param regEst
     */
    public void setRegEst(java.lang.Integer regEst) {
        this.regEst = regEst;
    }


    /**
     * Gets the retCof value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return retCof
     */
    public java.lang.String getRetCof() {
        return retCof;
    }


    /**
     * Sets the retCof value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param retCof
     */
    public void setRetCof(java.lang.String retCof) {
        this.retCof = retCof;
    }


    /**
     * Gets the retCsl value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return retCsl
     */
    public java.lang.String getRetCsl() {
        return retCsl;
    }


    /**
     * Sets the retCsl value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param retCsl
     */
    public void setRetCsl(java.lang.String retCsl) {
        this.retCsl = retCsl;
    }


    /**
     * Gets the retIrf value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return retIrf
     */
    public java.lang.String getRetIrf() {
        return retIrf;
    }


    /**
     * Sets the retIrf value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param retIrf
     */
    public void setRetIrf(java.lang.String retIrf) {
        this.retIrf = retIrf;
    }


    /**
     * Gets the retOur value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return retOur
     */
    public java.lang.String getRetOur() {
        return retOur;
    }


    /**
     * Sets the retOur value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param retOur
     */
    public void setRetOur(java.lang.String retOur) {
        this.retOur = retOur;
    }


    /**
     * Gets the retPis value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return retPis
     */
    public java.lang.String getRetPis() {
        return retPis;
    }


    /**
     * Sets the retPis value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param retPis
     */
    public void setRetPis(java.lang.String retPis) {
        this.retPis = retPis;
    }


    /**
     * Gets the retPro value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return retPro
     */
    public java.lang.String getRetPro() {
        return retPro;
    }


    /**
     * Sets the retPro value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param retPro
     */
    public void setRetPro(java.lang.String retPro) {
        this.retPro = retPro;
    }


    /**
     * Gets the triCof value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return triCof
     */
    public java.lang.String getTriCof() {
        return triCof;
    }


    /**
     * Sets the triCof value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param triCof
     */
    public void setTriCof(java.lang.String triCof) {
        this.triCof = triCof;
    }


    /**
     * Gets the triPis value for this ClientesConsultarFiscalOutCliente.
     * 
     * @return triPis
     */
    public java.lang.String getTriPis() {
        return triPis;
    }


    /**
     * Sets the triPis value for this ClientesConsultarFiscalOutCliente.
     * 
     * @param triPis
     */
    public void setTriPis(java.lang.String triPis) {
        this.triPis = triPis;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientesConsultarFiscalOutCliente)) return false;
        ClientesConsultarFiscalOutCliente other = (ClientesConsultarFiscalOutCliente) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.calFun==null && other.getCalFun()==null) || 
             (this.calFun!=null &&
              this.calFun.equals(other.getCalFun()))) &&
            ((this.calSen==null && other.getCalSen()==null) || 
             (this.calSen!=null &&
              this.calSen.equals(other.getCalSen()))) &&
            ((this.campoUsuarioCliente==null && other.getCampoUsuarioCliente()==null) || 
             (this.campoUsuarioCliente!=null &&
              java.util.Arrays.equals(this.campoUsuarioCliente, other.getCampoUsuarioCliente()))) &&
            ((this.codCli==null && other.getCodCli()==null) || 
             (this.codCli!=null &&
              this.codCli.equals(other.getCodCli()))) &&
            ((this.codCnv==null && other.getCodCnv()==null) || 
             (this.codCnv!=null &&
              this.codCnv.equals(other.getCodCnv()))) &&
            ((this.codMs2==null && other.getCodMs2()==null) || 
             (this.codMs2!=null &&
              this.codMs2.equals(other.getCodMs2()))) &&
            ((this.codMs3==null && other.getCodMs3()==null) || 
             (this.codMs3!=null &&
              this.codMs3.equals(other.getCodMs3()))) &&
            ((this.codMs4==null && other.getCodMs4()==null) || 
             (this.codMs4!=null &&
              this.codMs4.equals(other.getCodMs4()))) &&
            ((this.codMsg==null && other.getCodMsg()==null) || 
             (this.codMsg!=null &&
              this.codMsg.equals(other.getCodMsg()))) &&
            ((this.codRtr==null && other.getCodRtr()==null) || 
             (this.codRtr!=null &&
              this.codRtr.equals(other.getCodRtr()))) &&
            ((this.conFin==null && other.getConFin()==null) || 
             (this.conFin!=null &&
              this.conFin.equals(other.getConFin()))) &&
            ((this.datSuf==null && other.getDatSuf()==null) || 
             (this.datSuf!=null &&
              this.datSuf.equals(other.getDatSuf()))) &&
            ((this.emaNfe==null && other.getEmaNfe()==null) || 
             (this.emaNfe!=null &&
              this.emaNfe.equals(other.getEmaNfe()))) &&
            ((this.historico==null && other.getHistorico()==null) || 
             (this.historico!=null &&
              java.util.Arrays.equals(this.historico, other.getHistorico()))) &&
            ((this.limRet==null && other.getLimRet()==null) || 
             (this.limRet!=null &&
              this.limRet.equals(other.getLimRet()))) &&
            ((this.natCof==null && other.getNatCof()==null) || 
             (this.natCof!=null &&
              this.natCof.equals(other.getNatCof()))) &&
            ((this.natPis==null && other.getNatPis()==null) || 
             (this.natPis!=null &&
              this.natPis.equals(other.getNatPis()))) &&
            ((this.natRet==null && other.getNatRet()==null) || 
             (this.natRet!=null &&
              this.natRet.equals(other.getNatRet()))) &&
            ((this.nomCli==null && other.getNomCli()==null) || 
             (this.nomCli!=null &&
              this.nomCli.equals(other.getNomCli()))) &&
            ((this.numIdf==null && other.getNumIdf()==null) || 
             (this.numIdf!=null &&
              this.numIdf.equals(other.getNumIdf()))) &&
            ((this.perAin==null && other.getPerAin()==null) || 
             (this.perAin!=null &&
              this.perAin.equals(other.getPerAin()))) &&
            ((this.regEst==null && other.getRegEst()==null) || 
             (this.regEst!=null &&
              this.regEst.equals(other.getRegEst()))) &&
            ((this.retCof==null && other.getRetCof()==null) || 
             (this.retCof!=null &&
              this.retCof.equals(other.getRetCof()))) &&
            ((this.retCsl==null && other.getRetCsl()==null) || 
             (this.retCsl!=null &&
              this.retCsl.equals(other.getRetCsl()))) &&
            ((this.retIrf==null && other.getRetIrf()==null) || 
             (this.retIrf!=null &&
              this.retIrf.equals(other.getRetIrf()))) &&
            ((this.retOur==null && other.getRetOur()==null) || 
             (this.retOur!=null &&
              this.retOur.equals(other.getRetOur()))) &&
            ((this.retPis==null && other.getRetPis()==null) || 
             (this.retPis!=null &&
              this.retPis.equals(other.getRetPis()))) &&
            ((this.retPro==null && other.getRetPro()==null) || 
             (this.retPro!=null &&
              this.retPro.equals(other.getRetPro()))) &&
            ((this.triCof==null && other.getTriCof()==null) || 
             (this.triCof!=null &&
              this.triCof.equals(other.getTriCof()))) &&
            ((this.triPis==null && other.getTriPis()==null) || 
             (this.triPis!=null &&
              this.triPis.equals(other.getTriPis())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCalFun() != null) {
            _hashCode += getCalFun().hashCode();
        }
        if (getCalSen() != null) {
            _hashCode += getCalSen().hashCode();
        }
        if (getCampoUsuarioCliente() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCampoUsuarioCliente());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCampoUsuarioCliente(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCodCli() != null) {
            _hashCode += getCodCli().hashCode();
        }
        if (getCodCnv() != null) {
            _hashCode += getCodCnv().hashCode();
        }
        if (getCodMs2() != null) {
            _hashCode += getCodMs2().hashCode();
        }
        if (getCodMs3() != null) {
            _hashCode += getCodMs3().hashCode();
        }
        if (getCodMs4() != null) {
            _hashCode += getCodMs4().hashCode();
        }
        if (getCodMsg() != null) {
            _hashCode += getCodMsg().hashCode();
        }
        if (getCodRtr() != null) {
            _hashCode += getCodRtr().hashCode();
        }
        if (getConFin() != null) {
            _hashCode += getConFin().hashCode();
        }
        if (getDatSuf() != null) {
            _hashCode += getDatSuf().hashCode();
        }
        if (getEmaNfe() != null) {
            _hashCode += getEmaNfe().hashCode();
        }
        if (getHistorico() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getHistorico());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getHistorico(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getLimRet() != null) {
            _hashCode += getLimRet().hashCode();
        }
        if (getNatCof() != null) {
            _hashCode += getNatCof().hashCode();
        }
        if (getNatPis() != null) {
            _hashCode += getNatPis().hashCode();
        }
        if (getNatRet() != null) {
            _hashCode += getNatRet().hashCode();
        }
        if (getNomCli() != null) {
            _hashCode += getNomCli().hashCode();
        }
        if (getNumIdf() != null) {
            _hashCode += getNumIdf().hashCode();
        }
        if (getPerAin() != null) {
            _hashCode += getPerAin().hashCode();
        }
        if (getRegEst() != null) {
            _hashCode += getRegEst().hashCode();
        }
        if (getRetCof() != null) {
            _hashCode += getRetCof().hashCode();
        }
        if (getRetCsl() != null) {
            _hashCode += getRetCsl().hashCode();
        }
        if (getRetIrf() != null) {
            _hashCode += getRetIrf().hashCode();
        }
        if (getRetOur() != null) {
            _hashCode += getRetOur().hashCode();
        }
        if (getRetPis() != null) {
            _hashCode += getRetPis().hashCode();
        }
        if (getRetPro() != null) {
            _hashCode += getRetPro().hashCode();
        }
        if (getTriCof() != null) {
            _hashCode += getTriCof().hashCode();
        }
        if (getTriPis() != null) {
            _hashCode += getTriPis().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientesConsultarFiscalOutCliente.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesConsultarFiscalOutCliente"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("calFun");
        elemField.setXmlName(new javax.xml.namespace.QName("", "calFun"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("calSen");
        elemField.setXmlName(new javax.xml.namespace.QName("", "calSen"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("campoUsuarioCliente");
        elemField.setXmlName(new javax.xml.namespace.QName("", "campoUsuarioCliente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesConsultarFiscalOutClienteCampoUsuarioCliente"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCnv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCnv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMs2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMs2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMs3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMs3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMs4");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMs4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMsg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMsg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRtr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRtr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conFin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "conFin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datSuf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datSuf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emaNfe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "emaNfe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("historico");
        elemField.setXmlName(new javax.xml.namespace.QName("", "historico"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesConsultarFiscalOutClienteHistorico"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "limRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("natCof");
        elemField.setXmlName(new javax.xml.namespace.QName("", "natCof"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("natPis");
        elemField.setXmlName(new javax.xml.namespace.QName("", "natPis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("natRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "natRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numIdf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numIdf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perAin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perAin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("regEst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "regEst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retCof");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retCof"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retCsl");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retCsl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retIrf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retIrf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retOur");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retOur"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retPis");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retPis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retPro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retPro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triCof");
        elemField.setXmlName(new javax.xml.namespace.QName("", "triCof"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triPis");
        elemField.setXmlName(new javax.xml.namespace.QName("", "triPis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
