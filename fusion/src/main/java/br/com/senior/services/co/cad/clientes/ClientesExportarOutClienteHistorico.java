/**
 * ClientesExportarOutClienteHistorico.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package br.com.senior.services.co.cad.clientes;

public class ClientesExportarOutClienteHistorico  implements java.io.Serializable {
    private java.lang.String acePar;

    private java.lang.Double acrDia;

    private java.lang.String antDsc;

    private java.lang.String apmDen;

    private java.lang.String avaAti;

    private java.lang.Integer avaMot;

    private java.lang.String avaObs;

    private java.lang.Double avaVlr;

    private java.lang.Double avaVls;

    private java.lang.Double avaVlu;

    private java.lang.String avdAlt;

    private java.lang.String avdGer;

    private java.lang.Integer avhAlt;

    private java.lang.Integer avhGer;

    private java.lang.Double avuAlt;

    private java.lang.Double avuGer;

    private br.com.senior.services.co.cad.clientes.ClientesExportarOutClienteHistoricoCampoUsuarioClienteDefinicoes[] campoUsuarioClienteDefinicoes;

    private java.lang.Integer catCli;

    private java.lang.String ccbCli;

    private java.lang.String cifFob;

    private java.lang.String codAge;

    private java.lang.String codBan;

    private java.lang.Integer codCli;

    private java.lang.String codCpg;

    private java.lang.String codCrp;

    private java.lang.String codCrt;

    private java.lang.String codFcr;

    private java.lang.Integer codFin;

    private java.lang.Integer codFpg;

    private java.lang.String codFrj;

    private java.lang.String codIn1;

    private java.lang.String codIn2;

    private java.lang.String codMar;

    private java.lang.Integer codPdv;

    private java.lang.Integer codRed;

    private java.lang.Integer codRep;

    private java.lang.String codRve;

    private java.lang.String codStr;

    private java.lang.String codTab;

    private java.lang.String codTic;

    private java.lang.String codTpr;

    private java.lang.Integer codTra;

    private java.lang.String codTrd;

    private java.lang.Integer codVen;

    private java.lang.String conFin;

    private java.lang.String criEdv;

    private java.lang.Integer criRat;

    private java.lang.Integer ctaAad;

    private java.lang.Integer ctaAux;

    private java.lang.Integer ctaFcr;

    private java.lang.Integer ctaFdv;

    private java.lang.Integer ctaRcr;

    private java.lang.Integer ctaRed;

    private java.lang.Integer ctrPad;

    private java.lang.String datAlt;

    private java.lang.String datAtr;

    private java.lang.String datGer;

    private java.lang.String datLim;

    private java.lang.String datMac;

    private java.lang.String datMfa;

    private java.lang.String datPal;

    private java.lang.String datPmr;

    private java.lang.String datUfa;

    private java.lang.String datUpc;

    private java.lang.String datUpe;

    private java.lang.String datUpg;

    private java.lang.String diaEsp;

    private java.lang.Integer diaMe1;

    private java.lang.Integer diaMe2;

    private java.lang.Integer diaMe3;

    private java.lang.Double dscAnt;

    private java.lang.Double dscPon;

    private java.lang.String dscPrd;

    private java.lang.String ecpCnp;

    private java.lang.String epcPed;

    private java.lang.String exiAge;

    private java.lang.String exiLcp;

    private java.lang.String fveCpg;

    private java.lang.String fveFpg;

    private java.lang.String fveTns;

    private java.lang.String gerTcc;

    private java.lang.Integer horAlt;

    private java.lang.Integer horGer;

    private java.lang.Integer horPal;

    private java.lang.String indAgr;

    private java.lang.Integer indExp;

    private java.lang.String indOrf;

    private java.lang.String indPre;

    private java.lang.String junPed;

    private java.lang.String limApr;

    private java.lang.Integer maiAtr;

    private java.lang.Integer medAtr;

    private java.lang.Integer motDes;

    private java.lang.Double perAqa;

    private java.lang.String perCcr;

    private java.lang.Double perCom;

    private java.lang.Double perDif;

    private java.lang.Double perDs1;

    private java.lang.Double perDs2;

    private java.lang.Double perDs3;

    private java.lang.Double perDs4;

    private java.lang.Double perDs5;

    private java.lang.Double perDsc;

    private java.lang.Double perEmb;

    private java.lang.Double perEnc;

    private java.lang.Double perFre;

    private java.lang.Double perIss;

    private java.lang.Double perOf1;

    private java.lang.Double perOf2;

    private java.lang.Double perOut;

    private java.lang.Double perSeg;

    private java.lang.String porNa1;

    private java.lang.String porNa2;

    private java.lang.String porSi1;

    private java.lang.String porSi2;

    private java.lang.String prdDsc;

    private java.lang.Integer przMrt;

    private java.lang.Integer qdiPrt;

    private java.lang.Integer qtdChs;

    private java.lang.Integer qtdDcv;

    private java.lang.Integer qtdMfp;

    private java.lang.Integer qtdPgt;

    private java.lang.Integer qtdPrt;

    private java.lang.Integer qtdRpm;

    private java.lang.Integer qtdTpc;

    private java.lang.Integer recDtj;

    private java.lang.Integer recDtm;

    private java.lang.Double recJmm;

    private java.lang.Double recMul;

    private java.lang.String recTjr;

    private java.lang.Double salCre;

    private java.lang.Double salDup;

    private java.lang.Double salOut;

    private java.lang.Integer seqCob;

    private java.lang.Integer seqEnt;

    private java.lang.Integer tolDsc;

    private java.lang.Integer ultNfv;

    private java.lang.String ultSnf;

    private java.lang.Double usuAge;

    private java.lang.Double usuAlt;

    private java.lang.Double usuGer;

    private java.lang.Double vlrAcr;

    private java.lang.Double vlrAtr;

    private java.lang.Double vlrLim;

    private java.lang.Double vlrMac;

    private java.lang.Double vlrMfa;

    private java.lang.Double vlrPfa;

    private java.lang.Double vlrPrt;

    private java.lang.Double vlrUfa;

    private java.lang.Double vlrUpc;

    private java.lang.Double vlrUpe;

    private java.lang.Double vlrUpg;

    private java.lang.String volSep;

    public ClientesExportarOutClienteHistorico() {
    }

    public ClientesExportarOutClienteHistorico(
           java.lang.String acePar,
           java.lang.Double acrDia,
           java.lang.String antDsc,
           java.lang.String apmDen,
           java.lang.String avaAti,
           java.lang.Integer avaMot,
           java.lang.String avaObs,
           java.lang.Double avaVlr,
           java.lang.Double avaVls,
           java.lang.Double avaVlu,
           java.lang.String avdAlt,
           java.lang.String avdGer,
           java.lang.Integer avhAlt,
           java.lang.Integer avhGer,
           java.lang.Double avuAlt,
           java.lang.Double avuGer,
           br.com.senior.services.co.cad.clientes.ClientesExportarOutClienteHistoricoCampoUsuarioClienteDefinicoes[] campoUsuarioClienteDefinicoes,
           java.lang.Integer catCli,
           java.lang.String ccbCli,
           java.lang.String cifFob,
           java.lang.String codAge,
           java.lang.String codBan,
           java.lang.Integer codCli,
           java.lang.String codCpg,
           java.lang.String codCrp,
           java.lang.String codCrt,
           java.lang.String codFcr,
           java.lang.Integer codFin,
           java.lang.Integer codFpg,
           java.lang.String codFrj,
           java.lang.String codIn1,
           java.lang.String codIn2,
           java.lang.String codMar,
           java.lang.Integer codPdv,
           java.lang.Integer codRed,
           java.lang.Integer codRep,
           java.lang.String codRve,
           java.lang.String codStr,
           java.lang.String codTab,
           java.lang.String codTic,
           java.lang.String codTpr,
           java.lang.Integer codTra,
           java.lang.String codTrd,
           java.lang.Integer codVen,
           java.lang.String conFin,
           java.lang.String criEdv,
           java.lang.Integer criRat,
           java.lang.Integer ctaAad,
           java.lang.Integer ctaAux,
           java.lang.Integer ctaFcr,
           java.lang.Integer ctaFdv,
           java.lang.Integer ctaRcr,
           java.lang.Integer ctaRed,
           java.lang.Integer ctrPad,
           java.lang.String datAlt,
           java.lang.String datAtr,
           java.lang.String datGer,
           java.lang.String datLim,
           java.lang.String datMac,
           java.lang.String datMfa,
           java.lang.String datPal,
           java.lang.String datPmr,
           java.lang.String datUfa,
           java.lang.String datUpc,
           java.lang.String datUpe,
           java.lang.String datUpg,
           java.lang.String diaEsp,
           java.lang.Integer diaMe1,
           java.lang.Integer diaMe2,
           java.lang.Integer diaMe3,
           java.lang.Double dscAnt,
           java.lang.Double dscPon,
           java.lang.String dscPrd,
           java.lang.String ecpCnp,
           java.lang.String epcPed,
           java.lang.String exiAge,
           java.lang.String exiLcp,
           java.lang.String fveCpg,
           java.lang.String fveFpg,
           java.lang.String fveTns,
           java.lang.String gerTcc,
           java.lang.Integer horAlt,
           java.lang.Integer horGer,
           java.lang.Integer horPal,
           java.lang.String indAgr,
           java.lang.Integer indExp,
           java.lang.String indOrf,
           java.lang.String indPre,
           java.lang.String junPed,
           java.lang.String limApr,
           java.lang.Integer maiAtr,
           java.lang.Integer medAtr,
           java.lang.Integer motDes,
           java.lang.Double perAqa,
           java.lang.String perCcr,
           java.lang.Double perCom,
           java.lang.Double perDif,
           java.lang.Double perDs1,
           java.lang.Double perDs2,
           java.lang.Double perDs3,
           java.lang.Double perDs4,
           java.lang.Double perDs5,
           java.lang.Double perDsc,
           java.lang.Double perEmb,
           java.lang.Double perEnc,
           java.lang.Double perFre,
           java.lang.Double perIss,
           java.lang.Double perOf1,
           java.lang.Double perOf2,
           java.lang.Double perOut,
           java.lang.Double perSeg,
           java.lang.String porNa1,
           java.lang.String porNa2,
           java.lang.String porSi1,
           java.lang.String porSi2,
           java.lang.String prdDsc,
           java.lang.Integer przMrt,
           java.lang.Integer qdiPrt,
           java.lang.Integer qtdChs,
           java.lang.Integer qtdDcv,
           java.lang.Integer qtdMfp,
           java.lang.Integer qtdPgt,
           java.lang.Integer qtdPrt,
           java.lang.Integer qtdRpm,
           java.lang.Integer qtdTpc,
           java.lang.Integer recDtj,
           java.lang.Integer recDtm,
           java.lang.Double recJmm,
           java.lang.Double recMul,
           java.lang.String recTjr,
           java.lang.Double salCre,
           java.lang.Double salDup,
           java.lang.Double salOut,
           java.lang.Integer seqCob,
           java.lang.Integer seqEnt,
           java.lang.Integer tolDsc,
           java.lang.Integer ultNfv,
           java.lang.String ultSnf,
           java.lang.Double usuAge,
           java.lang.Double usuAlt,
           java.lang.Double usuGer,
           java.lang.Double vlrAcr,
           java.lang.Double vlrAtr,
           java.lang.Double vlrLim,
           java.lang.Double vlrMac,
           java.lang.Double vlrMfa,
           java.lang.Double vlrPfa,
           java.lang.Double vlrPrt,
           java.lang.Double vlrUfa,
           java.lang.Double vlrUpc,
           java.lang.Double vlrUpe,
           java.lang.Double vlrUpg,
           java.lang.String volSep) {
           this.acePar = acePar;
           this.acrDia = acrDia;
           this.antDsc = antDsc;
           this.apmDen = apmDen;
           this.avaAti = avaAti;
           this.avaMot = avaMot;
           this.avaObs = avaObs;
           this.avaVlr = avaVlr;
           this.avaVls = avaVls;
           this.avaVlu = avaVlu;
           this.avdAlt = avdAlt;
           this.avdGer = avdGer;
           this.avhAlt = avhAlt;
           this.avhGer = avhGer;
           this.avuAlt = avuAlt;
           this.avuGer = avuGer;
           this.campoUsuarioClienteDefinicoes = campoUsuarioClienteDefinicoes;
           this.catCli = catCli;
           this.ccbCli = ccbCli;
           this.cifFob = cifFob;
           this.codAge = codAge;
           this.codBan = codBan;
           this.codCli = codCli;
           this.codCpg = codCpg;
           this.codCrp = codCrp;
           this.codCrt = codCrt;
           this.codFcr = codFcr;
           this.codFin = codFin;
           this.codFpg = codFpg;
           this.codFrj = codFrj;
           this.codIn1 = codIn1;
           this.codIn2 = codIn2;
           this.codMar = codMar;
           this.codPdv = codPdv;
           this.codRed = codRed;
           this.codRep = codRep;
           this.codRve = codRve;
           this.codStr = codStr;
           this.codTab = codTab;
           this.codTic = codTic;
           this.codTpr = codTpr;
           this.codTra = codTra;
           this.codTrd = codTrd;
           this.codVen = codVen;
           this.conFin = conFin;
           this.criEdv = criEdv;
           this.criRat = criRat;
           this.ctaAad = ctaAad;
           this.ctaAux = ctaAux;
           this.ctaFcr = ctaFcr;
           this.ctaFdv = ctaFdv;
           this.ctaRcr = ctaRcr;
           this.ctaRed = ctaRed;
           this.ctrPad = ctrPad;
           this.datAlt = datAlt;
           this.datAtr = datAtr;
           this.datGer = datGer;
           this.datLim = datLim;
           this.datMac = datMac;
           this.datMfa = datMfa;
           this.datPal = datPal;
           this.datPmr = datPmr;
           this.datUfa = datUfa;
           this.datUpc = datUpc;
           this.datUpe = datUpe;
           this.datUpg = datUpg;
           this.diaEsp = diaEsp;
           this.diaMe1 = diaMe1;
           this.diaMe2 = diaMe2;
           this.diaMe3 = diaMe3;
           this.dscAnt = dscAnt;
           this.dscPon = dscPon;
           this.dscPrd = dscPrd;
           this.ecpCnp = ecpCnp;
           this.epcPed = epcPed;
           this.exiAge = exiAge;
           this.exiLcp = exiLcp;
           this.fveCpg = fveCpg;
           this.fveFpg = fveFpg;
           this.fveTns = fveTns;
           this.gerTcc = gerTcc;
           this.horAlt = horAlt;
           this.horGer = horGer;
           this.horPal = horPal;
           this.indAgr = indAgr;
           this.indExp = indExp;
           this.indOrf = indOrf;
           this.indPre = indPre;
           this.junPed = junPed;
           this.limApr = limApr;
           this.maiAtr = maiAtr;
           this.medAtr = medAtr;
           this.motDes = motDes;
           this.perAqa = perAqa;
           this.perCcr = perCcr;
           this.perCom = perCom;
           this.perDif = perDif;
           this.perDs1 = perDs1;
           this.perDs2 = perDs2;
           this.perDs3 = perDs3;
           this.perDs4 = perDs4;
           this.perDs5 = perDs5;
           this.perDsc = perDsc;
           this.perEmb = perEmb;
           this.perEnc = perEnc;
           this.perFre = perFre;
           this.perIss = perIss;
           this.perOf1 = perOf1;
           this.perOf2 = perOf2;
           this.perOut = perOut;
           this.perSeg = perSeg;
           this.porNa1 = porNa1;
           this.porNa2 = porNa2;
           this.porSi1 = porSi1;
           this.porSi2 = porSi2;
           this.prdDsc = prdDsc;
           this.przMrt = przMrt;
           this.qdiPrt = qdiPrt;
           this.qtdChs = qtdChs;
           this.qtdDcv = qtdDcv;
           this.qtdMfp = qtdMfp;
           this.qtdPgt = qtdPgt;
           this.qtdPrt = qtdPrt;
           this.qtdRpm = qtdRpm;
           this.qtdTpc = qtdTpc;
           this.recDtj = recDtj;
           this.recDtm = recDtm;
           this.recJmm = recJmm;
           this.recMul = recMul;
           this.recTjr = recTjr;
           this.salCre = salCre;
           this.salDup = salDup;
           this.salOut = salOut;
           this.seqCob = seqCob;
           this.seqEnt = seqEnt;
           this.tolDsc = tolDsc;
           this.ultNfv = ultNfv;
           this.ultSnf = ultSnf;
           this.usuAge = usuAge;
           this.usuAlt = usuAlt;
           this.usuGer = usuGer;
           this.vlrAcr = vlrAcr;
           this.vlrAtr = vlrAtr;
           this.vlrLim = vlrLim;
           this.vlrMac = vlrMac;
           this.vlrMfa = vlrMfa;
           this.vlrPfa = vlrPfa;
           this.vlrPrt = vlrPrt;
           this.vlrUfa = vlrUfa;
           this.vlrUpc = vlrUpc;
           this.vlrUpe = vlrUpe;
           this.vlrUpg = vlrUpg;
           this.volSep = volSep;
    }


    /**
     * Gets the acePar value for this ClientesExportarOutClienteHistorico.
     * 
     * @return acePar
     */
    public java.lang.String getAcePar() {
        return acePar;
    }


    /**
     * Sets the acePar value for this ClientesExportarOutClienteHistorico.
     * 
     * @param acePar
     */
    public void setAcePar(java.lang.String acePar) {
        this.acePar = acePar;
    }


    /**
     * Gets the acrDia value for this ClientesExportarOutClienteHistorico.
     * 
     * @return acrDia
     */
    public java.lang.Double getAcrDia() {
        return acrDia;
    }


    /**
     * Sets the acrDia value for this ClientesExportarOutClienteHistorico.
     * 
     * @param acrDia
     */
    public void setAcrDia(java.lang.Double acrDia) {
        this.acrDia = acrDia;
    }


    /**
     * Gets the antDsc value for this ClientesExportarOutClienteHistorico.
     * 
     * @return antDsc
     */
    public java.lang.String getAntDsc() {
        return antDsc;
    }


    /**
     * Sets the antDsc value for this ClientesExportarOutClienteHistorico.
     * 
     * @param antDsc
     */
    public void setAntDsc(java.lang.String antDsc) {
        this.antDsc = antDsc;
    }


    /**
     * Gets the apmDen value for this ClientesExportarOutClienteHistorico.
     * 
     * @return apmDen
     */
    public java.lang.String getApmDen() {
        return apmDen;
    }


    /**
     * Sets the apmDen value for this ClientesExportarOutClienteHistorico.
     * 
     * @param apmDen
     */
    public void setApmDen(java.lang.String apmDen) {
        this.apmDen = apmDen;
    }


    /**
     * Gets the avaAti value for this ClientesExportarOutClienteHistorico.
     * 
     * @return avaAti
     */
    public java.lang.String getAvaAti() {
        return avaAti;
    }


    /**
     * Sets the avaAti value for this ClientesExportarOutClienteHistorico.
     * 
     * @param avaAti
     */
    public void setAvaAti(java.lang.String avaAti) {
        this.avaAti = avaAti;
    }


    /**
     * Gets the avaMot value for this ClientesExportarOutClienteHistorico.
     * 
     * @return avaMot
     */
    public java.lang.Integer getAvaMot() {
        return avaMot;
    }


    /**
     * Sets the avaMot value for this ClientesExportarOutClienteHistorico.
     * 
     * @param avaMot
     */
    public void setAvaMot(java.lang.Integer avaMot) {
        this.avaMot = avaMot;
    }


    /**
     * Gets the avaObs value for this ClientesExportarOutClienteHistorico.
     * 
     * @return avaObs
     */
    public java.lang.String getAvaObs() {
        return avaObs;
    }


    /**
     * Sets the avaObs value for this ClientesExportarOutClienteHistorico.
     * 
     * @param avaObs
     */
    public void setAvaObs(java.lang.String avaObs) {
        this.avaObs = avaObs;
    }


    /**
     * Gets the avaVlr value for this ClientesExportarOutClienteHistorico.
     * 
     * @return avaVlr
     */
    public java.lang.Double getAvaVlr() {
        return avaVlr;
    }


    /**
     * Sets the avaVlr value for this ClientesExportarOutClienteHistorico.
     * 
     * @param avaVlr
     */
    public void setAvaVlr(java.lang.Double avaVlr) {
        this.avaVlr = avaVlr;
    }


    /**
     * Gets the avaVls value for this ClientesExportarOutClienteHistorico.
     * 
     * @return avaVls
     */
    public java.lang.Double getAvaVls() {
        return avaVls;
    }


    /**
     * Sets the avaVls value for this ClientesExportarOutClienteHistorico.
     * 
     * @param avaVls
     */
    public void setAvaVls(java.lang.Double avaVls) {
        this.avaVls = avaVls;
    }


    /**
     * Gets the avaVlu value for this ClientesExportarOutClienteHistorico.
     * 
     * @return avaVlu
     */
    public java.lang.Double getAvaVlu() {
        return avaVlu;
    }


    /**
     * Sets the avaVlu value for this ClientesExportarOutClienteHistorico.
     * 
     * @param avaVlu
     */
    public void setAvaVlu(java.lang.Double avaVlu) {
        this.avaVlu = avaVlu;
    }


    /**
     * Gets the avdAlt value for this ClientesExportarOutClienteHistorico.
     * 
     * @return avdAlt
     */
    public java.lang.String getAvdAlt() {
        return avdAlt;
    }


    /**
     * Sets the avdAlt value for this ClientesExportarOutClienteHistorico.
     * 
     * @param avdAlt
     */
    public void setAvdAlt(java.lang.String avdAlt) {
        this.avdAlt = avdAlt;
    }


    /**
     * Gets the avdGer value for this ClientesExportarOutClienteHistorico.
     * 
     * @return avdGer
     */
    public java.lang.String getAvdGer() {
        return avdGer;
    }


    /**
     * Sets the avdGer value for this ClientesExportarOutClienteHistorico.
     * 
     * @param avdGer
     */
    public void setAvdGer(java.lang.String avdGer) {
        this.avdGer = avdGer;
    }


    /**
     * Gets the avhAlt value for this ClientesExportarOutClienteHistorico.
     * 
     * @return avhAlt
     */
    public java.lang.Integer getAvhAlt() {
        return avhAlt;
    }


    /**
     * Sets the avhAlt value for this ClientesExportarOutClienteHistorico.
     * 
     * @param avhAlt
     */
    public void setAvhAlt(java.lang.Integer avhAlt) {
        this.avhAlt = avhAlt;
    }


    /**
     * Gets the avhGer value for this ClientesExportarOutClienteHistorico.
     * 
     * @return avhGer
     */
    public java.lang.Integer getAvhGer() {
        return avhGer;
    }


    /**
     * Sets the avhGer value for this ClientesExportarOutClienteHistorico.
     * 
     * @param avhGer
     */
    public void setAvhGer(java.lang.Integer avhGer) {
        this.avhGer = avhGer;
    }


    /**
     * Gets the avuAlt value for this ClientesExportarOutClienteHistorico.
     * 
     * @return avuAlt
     */
    public java.lang.Double getAvuAlt() {
        return avuAlt;
    }


    /**
     * Sets the avuAlt value for this ClientesExportarOutClienteHistorico.
     * 
     * @param avuAlt
     */
    public void setAvuAlt(java.lang.Double avuAlt) {
        this.avuAlt = avuAlt;
    }


    /**
     * Gets the avuGer value for this ClientesExportarOutClienteHistorico.
     * 
     * @return avuGer
     */
    public java.lang.Double getAvuGer() {
        return avuGer;
    }


    /**
     * Sets the avuGer value for this ClientesExportarOutClienteHistorico.
     * 
     * @param avuGer
     */
    public void setAvuGer(java.lang.Double avuGer) {
        this.avuGer = avuGer;
    }


    /**
     * Gets the campoUsuarioClienteDefinicoes value for this ClientesExportarOutClienteHistorico.
     * 
     * @return campoUsuarioClienteDefinicoes
     */
    public br.com.senior.services.co.cad.clientes.ClientesExportarOutClienteHistoricoCampoUsuarioClienteDefinicoes[] getCampoUsuarioClienteDefinicoes() {
        return campoUsuarioClienteDefinicoes;
    }


    /**
     * Sets the campoUsuarioClienteDefinicoes value for this ClientesExportarOutClienteHistorico.
     * 
     * @param campoUsuarioClienteDefinicoes
     */
    public void setCampoUsuarioClienteDefinicoes(br.com.senior.services.co.cad.clientes.ClientesExportarOutClienteHistoricoCampoUsuarioClienteDefinicoes[] campoUsuarioClienteDefinicoes) {
        this.campoUsuarioClienteDefinicoes = campoUsuarioClienteDefinicoes;
    }

    public br.com.senior.services.co.cad.clientes.ClientesExportarOutClienteHistoricoCampoUsuarioClienteDefinicoes getCampoUsuarioClienteDefinicoes(int i) {
        return this.campoUsuarioClienteDefinicoes[i];
    }

    public void setCampoUsuarioClienteDefinicoes(int i, br.com.senior.services.co.cad.clientes.ClientesExportarOutClienteHistoricoCampoUsuarioClienteDefinicoes _value) {
        this.campoUsuarioClienteDefinicoes[i] = _value;
    }


    /**
     * Gets the catCli value for this ClientesExportarOutClienteHistorico.
     * 
     * @return catCli
     */
    public java.lang.Integer getCatCli() {
        return catCli;
    }


    /**
     * Sets the catCli value for this ClientesExportarOutClienteHistorico.
     * 
     * @param catCli
     */
    public void setCatCli(java.lang.Integer catCli) {
        this.catCli = catCli;
    }


    /**
     * Gets the ccbCli value for this ClientesExportarOutClienteHistorico.
     * 
     * @return ccbCli
     */
    public java.lang.String getCcbCli() {
        return ccbCli;
    }


    /**
     * Sets the ccbCli value for this ClientesExportarOutClienteHistorico.
     * 
     * @param ccbCli
     */
    public void setCcbCli(java.lang.String ccbCli) {
        this.ccbCli = ccbCli;
    }


    /**
     * Gets the cifFob value for this ClientesExportarOutClienteHistorico.
     * 
     * @return cifFob
     */
    public java.lang.String getCifFob() {
        return cifFob;
    }


    /**
     * Sets the cifFob value for this ClientesExportarOutClienteHistorico.
     * 
     * @param cifFob
     */
    public void setCifFob(java.lang.String cifFob) {
        this.cifFob = cifFob;
    }


    /**
     * Gets the codAge value for this ClientesExportarOutClienteHistorico.
     * 
     * @return codAge
     */
    public java.lang.String getCodAge() {
        return codAge;
    }


    /**
     * Sets the codAge value for this ClientesExportarOutClienteHistorico.
     * 
     * @param codAge
     */
    public void setCodAge(java.lang.String codAge) {
        this.codAge = codAge;
    }


    /**
     * Gets the codBan value for this ClientesExportarOutClienteHistorico.
     * 
     * @return codBan
     */
    public java.lang.String getCodBan() {
        return codBan;
    }


    /**
     * Sets the codBan value for this ClientesExportarOutClienteHistorico.
     * 
     * @param codBan
     */
    public void setCodBan(java.lang.String codBan) {
        this.codBan = codBan;
    }


    /**
     * Gets the codCli value for this ClientesExportarOutClienteHistorico.
     * 
     * @return codCli
     */
    public java.lang.Integer getCodCli() {
        return codCli;
    }


    /**
     * Sets the codCli value for this ClientesExportarOutClienteHistorico.
     * 
     * @param codCli
     */
    public void setCodCli(java.lang.Integer codCli) {
        this.codCli = codCli;
    }


    /**
     * Gets the codCpg value for this ClientesExportarOutClienteHistorico.
     * 
     * @return codCpg
     */
    public java.lang.String getCodCpg() {
        return codCpg;
    }


    /**
     * Sets the codCpg value for this ClientesExportarOutClienteHistorico.
     * 
     * @param codCpg
     */
    public void setCodCpg(java.lang.String codCpg) {
        this.codCpg = codCpg;
    }


    /**
     * Gets the codCrp value for this ClientesExportarOutClienteHistorico.
     * 
     * @return codCrp
     */
    public java.lang.String getCodCrp() {
        return codCrp;
    }


    /**
     * Sets the codCrp value for this ClientesExportarOutClienteHistorico.
     * 
     * @param codCrp
     */
    public void setCodCrp(java.lang.String codCrp) {
        this.codCrp = codCrp;
    }


    /**
     * Gets the codCrt value for this ClientesExportarOutClienteHistorico.
     * 
     * @return codCrt
     */
    public java.lang.String getCodCrt() {
        return codCrt;
    }


    /**
     * Sets the codCrt value for this ClientesExportarOutClienteHistorico.
     * 
     * @param codCrt
     */
    public void setCodCrt(java.lang.String codCrt) {
        this.codCrt = codCrt;
    }


    /**
     * Gets the codFcr value for this ClientesExportarOutClienteHistorico.
     * 
     * @return codFcr
     */
    public java.lang.String getCodFcr() {
        return codFcr;
    }


    /**
     * Sets the codFcr value for this ClientesExportarOutClienteHistorico.
     * 
     * @param codFcr
     */
    public void setCodFcr(java.lang.String codFcr) {
        this.codFcr = codFcr;
    }


    /**
     * Gets the codFin value for this ClientesExportarOutClienteHistorico.
     * 
     * @return codFin
     */
    public java.lang.Integer getCodFin() {
        return codFin;
    }


    /**
     * Sets the codFin value for this ClientesExportarOutClienteHistorico.
     * 
     * @param codFin
     */
    public void setCodFin(java.lang.Integer codFin) {
        this.codFin = codFin;
    }


    /**
     * Gets the codFpg value for this ClientesExportarOutClienteHistorico.
     * 
     * @return codFpg
     */
    public java.lang.Integer getCodFpg() {
        return codFpg;
    }


    /**
     * Sets the codFpg value for this ClientesExportarOutClienteHistorico.
     * 
     * @param codFpg
     */
    public void setCodFpg(java.lang.Integer codFpg) {
        this.codFpg = codFpg;
    }


    /**
     * Gets the codFrj value for this ClientesExportarOutClienteHistorico.
     * 
     * @return codFrj
     */
    public java.lang.String getCodFrj() {
        return codFrj;
    }


    /**
     * Sets the codFrj value for this ClientesExportarOutClienteHistorico.
     * 
     * @param codFrj
     */
    public void setCodFrj(java.lang.String codFrj) {
        this.codFrj = codFrj;
    }


    /**
     * Gets the codIn1 value for this ClientesExportarOutClienteHistorico.
     * 
     * @return codIn1
     */
    public java.lang.String getCodIn1() {
        return codIn1;
    }


    /**
     * Sets the codIn1 value for this ClientesExportarOutClienteHistorico.
     * 
     * @param codIn1
     */
    public void setCodIn1(java.lang.String codIn1) {
        this.codIn1 = codIn1;
    }


    /**
     * Gets the codIn2 value for this ClientesExportarOutClienteHistorico.
     * 
     * @return codIn2
     */
    public java.lang.String getCodIn2() {
        return codIn2;
    }


    /**
     * Sets the codIn2 value for this ClientesExportarOutClienteHistorico.
     * 
     * @param codIn2
     */
    public void setCodIn2(java.lang.String codIn2) {
        this.codIn2 = codIn2;
    }


    /**
     * Gets the codMar value for this ClientesExportarOutClienteHistorico.
     * 
     * @return codMar
     */
    public java.lang.String getCodMar() {
        return codMar;
    }


    /**
     * Sets the codMar value for this ClientesExportarOutClienteHistorico.
     * 
     * @param codMar
     */
    public void setCodMar(java.lang.String codMar) {
        this.codMar = codMar;
    }


    /**
     * Gets the codPdv value for this ClientesExportarOutClienteHistorico.
     * 
     * @return codPdv
     */
    public java.lang.Integer getCodPdv() {
        return codPdv;
    }


    /**
     * Sets the codPdv value for this ClientesExportarOutClienteHistorico.
     * 
     * @param codPdv
     */
    public void setCodPdv(java.lang.Integer codPdv) {
        this.codPdv = codPdv;
    }


    /**
     * Gets the codRed value for this ClientesExportarOutClienteHistorico.
     * 
     * @return codRed
     */
    public java.lang.Integer getCodRed() {
        return codRed;
    }


    /**
     * Sets the codRed value for this ClientesExportarOutClienteHistorico.
     * 
     * @param codRed
     */
    public void setCodRed(java.lang.Integer codRed) {
        this.codRed = codRed;
    }


    /**
     * Gets the codRep value for this ClientesExportarOutClienteHistorico.
     * 
     * @return codRep
     */
    public java.lang.Integer getCodRep() {
        return codRep;
    }


    /**
     * Sets the codRep value for this ClientesExportarOutClienteHistorico.
     * 
     * @param codRep
     */
    public void setCodRep(java.lang.Integer codRep) {
        this.codRep = codRep;
    }


    /**
     * Gets the codRve value for this ClientesExportarOutClienteHistorico.
     * 
     * @return codRve
     */
    public java.lang.String getCodRve() {
        return codRve;
    }


    /**
     * Sets the codRve value for this ClientesExportarOutClienteHistorico.
     * 
     * @param codRve
     */
    public void setCodRve(java.lang.String codRve) {
        this.codRve = codRve;
    }


    /**
     * Gets the codStr value for this ClientesExportarOutClienteHistorico.
     * 
     * @return codStr
     */
    public java.lang.String getCodStr() {
        return codStr;
    }


    /**
     * Sets the codStr value for this ClientesExportarOutClienteHistorico.
     * 
     * @param codStr
     */
    public void setCodStr(java.lang.String codStr) {
        this.codStr = codStr;
    }


    /**
     * Gets the codTab value for this ClientesExportarOutClienteHistorico.
     * 
     * @return codTab
     */
    public java.lang.String getCodTab() {
        return codTab;
    }


    /**
     * Sets the codTab value for this ClientesExportarOutClienteHistorico.
     * 
     * @param codTab
     */
    public void setCodTab(java.lang.String codTab) {
        this.codTab = codTab;
    }


    /**
     * Gets the codTic value for this ClientesExportarOutClienteHistorico.
     * 
     * @return codTic
     */
    public java.lang.String getCodTic() {
        return codTic;
    }


    /**
     * Sets the codTic value for this ClientesExportarOutClienteHistorico.
     * 
     * @param codTic
     */
    public void setCodTic(java.lang.String codTic) {
        this.codTic = codTic;
    }


    /**
     * Gets the codTpr value for this ClientesExportarOutClienteHistorico.
     * 
     * @return codTpr
     */
    public java.lang.String getCodTpr() {
        return codTpr;
    }


    /**
     * Sets the codTpr value for this ClientesExportarOutClienteHistorico.
     * 
     * @param codTpr
     */
    public void setCodTpr(java.lang.String codTpr) {
        this.codTpr = codTpr;
    }


    /**
     * Gets the codTra value for this ClientesExportarOutClienteHistorico.
     * 
     * @return codTra
     */
    public java.lang.Integer getCodTra() {
        return codTra;
    }


    /**
     * Sets the codTra value for this ClientesExportarOutClienteHistorico.
     * 
     * @param codTra
     */
    public void setCodTra(java.lang.Integer codTra) {
        this.codTra = codTra;
    }


    /**
     * Gets the codTrd value for this ClientesExportarOutClienteHistorico.
     * 
     * @return codTrd
     */
    public java.lang.String getCodTrd() {
        return codTrd;
    }


    /**
     * Sets the codTrd value for this ClientesExportarOutClienteHistorico.
     * 
     * @param codTrd
     */
    public void setCodTrd(java.lang.String codTrd) {
        this.codTrd = codTrd;
    }


    /**
     * Gets the codVen value for this ClientesExportarOutClienteHistorico.
     * 
     * @return codVen
     */
    public java.lang.Integer getCodVen() {
        return codVen;
    }


    /**
     * Sets the codVen value for this ClientesExportarOutClienteHistorico.
     * 
     * @param codVen
     */
    public void setCodVen(java.lang.Integer codVen) {
        this.codVen = codVen;
    }


    /**
     * Gets the conFin value for this ClientesExportarOutClienteHistorico.
     * 
     * @return conFin
     */
    public java.lang.String getConFin() {
        return conFin;
    }


    /**
     * Sets the conFin value for this ClientesExportarOutClienteHistorico.
     * 
     * @param conFin
     */
    public void setConFin(java.lang.String conFin) {
        this.conFin = conFin;
    }


    /**
     * Gets the criEdv value for this ClientesExportarOutClienteHistorico.
     * 
     * @return criEdv
     */
    public java.lang.String getCriEdv() {
        return criEdv;
    }


    /**
     * Sets the criEdv value for this ClientesExportarOutClienteHistorico.
     * 
     * @param criEdv
     */
    public void setCriEdv(java.lang.String criEdv) {
        this.criEdv = criEdv;
    }


    /**
     * Gets the criRat value for this ClientesExportarOutClienteHistorico.
     * 
     * @return criRat
     */
    public java.lang.Integer getCriRat() {
        return criRat;
    }


    /**
     * Sets the criRat value for this ClientesExportarOutClienteHistorico.
     * 
     * @param criRat
     */
    public void setCriRat(java.lang.Integer criRat) {
        this.criRat = criRat;
    }


    /**
     * Gets the ctaAad value for this ClientesExportarOutClienteHistorico.
     * 
     * @return ctaAad
     */
    public java.lang.Integer getCtaAad() {
        return ctaAad;
    }


    /**
     * Sets the ctaAad value for this ClientesExportarOutClienteHistorico.
     * 
     * @param ctaAad
     */
    public void setCtaAad(java.lang.Integer ctaAad) {
        this.ctaAad = ctaAad;
    }


    /**
     * Gets the ctaAux value for this ClientesExportarOutClienteHistorico.
     * 
     * @return ctaAux
     */
    public java.lang.Integer getCtaAux() {
        return ctaAux;
    }


    /**
     * Sets the ctaAux value for this ClientesExportarOutClienteHistorico.
     * 
     * @param ctaAux
     */
    public void setCtaAux(java.lang.Integer ctaAux) {
        this.ctaAux = ctaAux;
    }


    /**
     * Gets the ctaFcr value for this ClientesExportarOutClienteHistorico.
     * 
     * @return ctaFcr
     */
    public java.lang.Integer getCtaFcr() {
        return ctaFcr;
    }


    /**
     * Sets the ctaFcr value for this ClientesExportarOutClienteHistorico.
     * 
     * @param ctaFcr
     */
    public void setCtaFcr(java.lang.Integer ctaFcr) {
        this.ctaFcr = ctaFcr;
    }


    /**
     * Gets the ctaFdv value for this ClientesExportarOutClienteHistorico.
     * 
     * @return ctaFdv
     */
    public java.lang.Integer getCtaFdv() {
        return ctaFdv;
    }


    /**
     * Sets the ctaFdv value for this ClientesExportarOutClienteHistorico.
     * 
     * @param ctaFdv
     */
    public void setCtaFdv(java.lang.Integer ctaFdv) {
        this.ctaFdv = ctaFdv;
    }


    /**
     * Gets the ctaRcr value for this ClientesExportarOutClienteHistorico.
     * 
     * @return ctaRcr
     */
    public java.lang.Integer getCtaRcr() {
        return ctaRcr;
    }


    /**
     * Sets the ctaRcr value for this ClientesExportarOutClienteHistorico.
     * 
     * @param ctaRcr
     */
    public void setCtaRcr(java.lang.Integer ctaRcr) {
        this.ctaRcr = ctaRcr;
    }


    /**
     * Gets the ctaRed value for this ClientesExportarOutClienteHistorico.
     * 
     * @return ctaRed
     */
    public java.lang.Integer getCtaRed() {
        return ctaRed;
    }


    /**
     * Sets the ctaRed value for this ClientesExportarOutClienteHistorico.
     * 
     * @param ctaRed
     */
    public void setCtaRed(java.lang.Integer ctaRed) {
        this.ctaRed = ctaRed;
    }


    /**
     * Gets the ctrPad value for this ClientesExportarOutClienteHistorico.
     * 
     * @return ctrPad
     */
    public java.lang.Integer getCtrPad() {
        return ctrPad;
    }


    /**
     * Sets the ctrPad value for this ClientesExportarOutClienteHistorico.
     * 
     * @param ctrPad
     */
    public void setCtrPad(java.lang.Integer ctrPad) {
        this.ctrPad = ctrPad;
    }


    /**
     * Gets the datAlt value for this ClientesExportarOutClienteHistorico.
     * 
     * @return datAlt
     */
    public java.lang.String getDatAlt() {
        return datAlt;
    }


    /**
     * Sets the datAlt value for this ClientesExportarOutClienteHistorico.
     * 
     * @param datAlt
     */
    public void setDatAlt(java.lang.String datAlt) {
        this.datAlt = datAlt;
    }


    /**
     * Gets the datAtr value for this ClientesExportarOutClienteHistorico.
     * 
     * @return datAtr
     */
    public java.lang.String getDatAtr() {
        return datAtr;
    }


    /**
     * Sets the datAtr value for this ClientesExportarOutClienteHistorico.
     * 
     * @param datAtr
     */
    public void setDatAtr(java.lang.String datAtr) {
        this.datAtr = datAtr;
    }


    /**
     * Gets the datGer value for this ClientesExportarOutClienteHistorico.
     * 
     * @return datGer
     */
    public java.lang.String getDatGer() {
        return datGer;
    }


    /**
     * Sets the datGer value for this ClientesExportarOutClienteHistorico.
     * 
     * @param datGer
     */
    public void setDatGer(java.lang.String datGer) {
        this.datGer = datGer;
    }


    /**
     * Gets the datLim value for this ClientesExportarOutClienteHistorico.
     * 
     * @return datLim
     */
    public java.lang.String getDatLim() {
        return datLim;
    }


    /**
     * Sets the datLim value for this ClientesExportarOutClienteHistorico.
     * 
     * @param datLim
     */
    public void setDatLim(java.lang.String datLim) {
        this.datLim = datLim;
    }


    /**
     * Gets the datMac value for this ClientesExportarOutClienteHistorico.
     * 
     * @return datMac
     */
    public java.lang.String getDatMac() {
        return datMac;
    }


    /**
     * Sets the datMac value for this ClientesExportarOutClienteHistorico.
     * 
     * @param datMac
     */
    public void setDatMac(java.lang.String datMac) {
        this.datMac = datMac;
    }


    /**
     * Gets the datMfa value for this ClientesExportarOutClienteHistorico.
     * 
     * @return datMfa
     */
    public java.lang.String getDatMfa() {
        return datMfa;
    }


    /**
     * Sets the datMfa value for this ClientesExportarOutClienteHistorico.
     * 
     * @param datMfa
     */
    public void setDatMfa(java.lang.String datMfa) {
        this.datMfa = datMfa;
    }


    /**
     * Gets the datPal value for this ClientesExportarOutClienteHistorico.
     * 
     * @return datPal
     */
    public java.lang.String getDatPal() {
        return datPal;
    }


    /**
     * Sets the datPal value for this ClientesExportarOutClienteHistorico.
     * 
     * @param datPal
     */
    public void setDatPal(java.lang.String datPal) {
        this.datPal = datPal;
    }


    /**
     * Gets the datPmr value for this ClientesExportarOutClienteHistorico.
     * 
     * @return datPmr
     */
    public java.lang.String getDatPmr() {
        return datPmr;
    }


    /**
     * Sets the datPmr value for this ClientesExportarOutClienteHistorico.
     * 
     * @param datPmr
     */
    public void setDatPmr(java.lang.String datPmr) {
        this.datPmr = datPmr;
    }


    /**
     * Gets the datUfa value for this ClientesExportarOutClienteHistorico.
     * 
     * @return datUfa
     */
    public java.lang.String getDatUfa() {
        return datUfa;
    }


    /**
     * Sets the datUfa value for this ClientesExportarOutClienteHistorico.
     * 
     * @param datUfa
     */
    public void setDatUfa(java.lang.String datUfa) {
        this.datUfa = datUfa;
    }


    /**
     * Gets the datUpc value for this ClientesExportarOutClienteHistorico.
     * 
     * @return datUpc
     */
    public java.lang.String getDatUpc() {
        return datUpc;
    }


    /**
     * Sets the datUpc value for this ClientesExportarOutClienteHistorico.
     * 
     * @param datUpc
     */
    public void setDatUpc(java.lang.String datUpc) {
        this.datUpc = datUpc;
    }


    /**
     * Gets the datUpe value for this ClientesExportarOutClienteHistorico.
     * 
     * @return datUpe
     */
    public java.lang.String getDatUpe() {
        return datUpe;
    }


    /**
     * Sets the datUpe value for this ClientesExportarOutClienteHistorico.
     * 
     * @param datUpe
     */
    public void setDatUpe(java.lang.String datUpe) {
        this.datUpe = datUpe;
    }


    /**
     * Gets the datUpg value for this ClientesExportarOutClienteHistorico.
     * 
     * @return datUpg
     */
    public java.lang.String getDatUpg() {
        return datUpg;
    }


    /**
     * Sets the datUpg value for this ClientesExportarOutClienteHistorico.
     * 
     * @param datUpg
     */
    public void setDatUpg(java.lang.String datUpg) {
        this.datUpg = datUpg;
    }


    /**
     * Gets the diaEsp value for this ClientesExportarOutClienteHistorico.
     * 
     * @return diaEsp
     */
    public java.lang.String getDiaEsp() {
        return diaEsp;
    }


    /**
     * Sets the diaEsp value for this ClientesExportarOutClienteHistorico.
     * 
     * @param diaEsp
     */
    public void setDiaEsp(java.lang.String diaEsp) {
        this.diaEsp = diaEsp;
    }


    /**
     * Gets the diaMe1 value for this ClientesExportarOutClienteHistorico.
     * 
     * @return diaMe1
     */
    public java.lang.Integer getDiaMe1() {
        return diaMe1;
    }


    /**
     * Sets the diaMe1 value for this ClientesExportarOutClienteHistorico.
     * 
     * @param diaMe1
     */
    public void setDiaMe1(java.lang.Integer diaMe1) {
        this.diaMe1 = diaMe1;
    }


    /**
     * Gets the diaMe2 value for this ClientesExportarOutClienteHistorico.
     * 
     * @return diaMe2
     */
    public java.lang.Integer getDiaMe2() {
        return diaMe2;
    }


    /**
     * Sets the diaMe2 value for this ClientesExportarOutClienteHistorico.
     * 
     * @param diaMe2
     */
    public void setDiaMe2(java.lang.Integer diaMe2) {
        this.diaMe2 = diaMe2;
    }


    /**
     * Gets the diaMe3 value for this ClientesExportarOutClienteHistorico.
     * 
     * @return diaMe3
     */
    public java.lang.Integer getDiaMe3() {
        return diaMe3;
    }


    /**
     * Sets the diaMe3 value for this ClientesExportarOutClienteHistorico.
     * 
     * @param diaMe3
     */
    public void setDiaMe3(java.lang.Integer diaMe3) {
        this.diaMe3 = diaMe3;
    }


    /**
     * Gets the dscAnt value for this ClientesExportarOutClienteHistorico.
     * 
     * @return dscAnt
     */
    public java.lang.Double getDscAnt() {
        return dscAnt;
    }


    /**
     * Sets the dscAnt value for this ClientesExportarOutClienteHistorico.
     * 
     * @param dscAnt
     */
    public void setDscAnt(java.lang.Double dscAnt) {
        this.dscAnt = dscAnt;
    }


    /**
     * Gets the dscPon value for this ClientesExportarOutClienteHistorico.
     * 
     * @return dscPon
     */
    public java.lang.Double getDscPon() {
        return dscPon;
    }


    /**
     * Sets the dscPon value for this ClientesExportarOutClienteHistorico.
     * 
     * @param dscPon
     */
    public void setDscPon(java.lang.Double dscPon) {
        this.dscPon = dscPon;
    }


    /**
     * Gets the dscPrd value for this ClientesExportarOutClienteHistorico.
     * 
     * @return dscPrd
     */
    public java.lang.String getDscPrd() {
        return dscPrd;
    }


    /**
     * Sets the dscPrd value for this ClientesExportarOutClienteHistorico.
     * 
     * @param dscPrd
     */
    public void setDscPrd(java.lang.String dscPrd) {
        this.dscPrd = dscPrd;
    }


    /**
     * Gets the ecpCnp value for this ClientesExportarOutClienteHistorico.
     * 
     * @return ecpCnp
     */
    public java.lang.String getEcpCnp() {
        return ecpCnp;
    }


    /**
     * Sets the ecpCnp value for this ClientesExportarOutClienteHistorico.
     * 
     * @param ecpCnp
     */
    public void setEcpCnp(java.lang.String ecpCnp) {
        this.ecpCnp = ecpCnp;
    }


    /**
     * Gets the epcPed value for this ClientesExportarOutClienteHistorico.
     * 
     * @return epcPed
     */
    public java.lang.String getEpcPed() {
        return epcPed;
    }


    /**
     * Sets the epcPed value for this ClientesExportarOutClienteHistorico.
     * 
     * @param epcPed
     */
    public void setEpcPed(java.lang.String epcPed) {
        this.epcPed = epcPed;
    }


    /**
     * Gets the exiAge value for this ClientesExportarOutClienteHistorico.
     * 
     * @return exiAge
     */
    public java.lang.String getExiAge() {
        return exiAge;
    }


    /**
     * Sets the exiAge value for this ClientesExportarOutClienteHistorico.
     * 
     * @param exiAge
     */
    public void setExiAge(java.lang.String exiAge) {
        this.exiAge = exiAge;
    }


    /**
     * Gets the exiLcp value for this ClientesExportarOutClienteHistorico.
     * 
     * @return exiLcp
     */
    public java.lang.String getExiLcp() {
        return exiLcp;
    }


    /**
     * Sets the exiLcp value for this ClientesExportarOutClienteHistorico.
     * 
     * @param exiLcp
     */
    public void setExiLcp(java.lang.String exiLcp) {
        this.exiLcp = exiLcp;
    }


    /**
     * Gets the fveCpg value for this ClientesExportarOutClienteHistorico.
     * 
     * @return fveCpg
     */
    public java.lang.String getFveCpg() {
        return fveCpg;
    }


    /**
     * Sets the fveCpg value for this ClientesExportarOutClienteHistorico.
     * 
     * @param fveCpg
     */
    public void setFveCpg(java.lang.String fveCpg) {
        this.fveCpg = fveCpg;
    }


    /**
     * Gets the fveFpg value for this ClientesExportarOutClienteHistorico.
     * 
     * @return fveFpg
     */
    public java.lang.String getFveFpg() {
        return fveFpg;
    }


    /**
     * Sets the fveFpg value for this ClientesExportarOutClienteHistorico.
     * 
     * @param fveFpg
     */
    public void setFveFpg(java.lang.String fveFpg) {
        this.fveFpg = fveFpg;
    }


    /**
     * Gets the fveTns value for this ClientesExportarOutClienteHistorico.
     * 
     * @return fveTns
     */
    public java.lang.String getFveTns() {
        return fveTns;
    }


    /**
     * Sets the fveTns value for this ClientesExportarOutClienteHistorico.
     * 
     * @param fveTns
     */
    public void setFveTns(java.lang.String fveTns) {
        this.fveTns = fveTns;
    }


    /**
     * Gets the gerTcc value for this ClientesExportarOutClienteHistorico.
     * 
     * @return gerTcc
     */
    public java.lang.String getGerTcc() {
        return gerTcc;
    }


    /**
     * Sets the gerTcc value for this ClientesExportarOutClienteHistorico.
     * 
     * @param gerTcc
     */
    public void setGerTcc(java.lang.String gerTcc) {
        this.gerTcc = gerTcc;
    }


    /**
     * Gets the horAlt value for this ClientesExportarOutClienteHistorico.
     * 
     * @return horAlt
     */
    public java.lang.Integer getHorAlt() {
        return horAlt;
    }


    /**
     * Sets the horAlt value for this ClientesExportarOutClienteHistorico.
     * 
     * @param horAlt
     */
    public void setHorAlt(java.lang.Integer horAlt) {
        this.horAlt = horAlt;
    }


    /**
     * Gets the horGer value for this ClientesExportarOutClienteHistorico.
     * 
     * @return horGer
     */
    public java.lang.Integer getHorGer() {
        return horGer;
    }


    /**
     * Sets the horGer value for this ClientesExportarOutClienteHistorico.
     * 
     * @param horGer
     */
    public void setHorGer(java.lang.Integer horGer) {
        this.horGer = horGer;
    }


    /**
     * Gets the horPal value for this ClientesExportarOutClienteHistorico.
     * 
     * @return horPal
     */
    public java.lang.Integer getHorPal() {
        return horPal;
    }


    /**
     * Sets the horPal value for this ClientesExportarOutClienteHistorico.
     * 
     * @param horPal
     */
    public void setHorPal(java.lang.Integer horPal) {
        this.horPal = horPal;
    }


    /**
     * Gets the indAgr value for this ClientesExportarOutClienteHistorico.
     * 
     * @return indAgr
     */
    public java.lang.String getIndAgr() {
        return indAgr;
    }


    /**
     * Sets the indAgr value for this ClientesExportarOutClienteHistorico.
     * 
     * @param indAgr
     */
    public void setIndAgr(java.lang.String indAgr) {
        this.indAgr = indAgr;
    }


    /**
     * Gets the indExp value for this ClientesExportarOutClienteHistorico.
     * 
     * @return indExp
     */
    public java.lang.Integer getIndExp() {
        return indExp;
    }


    /**
     * Sets the indExp value for this ClientesExportarOutClienteHistorico.
     * 
     * @param indExp
     */
    public void setIndExp(java.lang.Integer indExp) {
        this.indExp = indExp;
    }


    /**
     * Gets the indOrf value for this ClientesExportarOutClienteHistorico.
     * 
     * @return indOrf
     */
    public java.lang.String getIndOrf() {
        return indOrf;
    }


    /**
     * Sets the indOrf value for this ClientesExportarOutClienteHistorico.
     * 
     * @param indOrf
     */
    public void setIndOrf(java.lang.String indOrf) {
        this.indOrf = indOrf;
    }


    /**
     * Gets the indPre value for this ClientesExportarOutClienteHistorico.
     * 
     * @return indPre
     */
    public java.lang.String getIndPre() {
        return indPre;
    }


    /**
     * Sets the indPre value for this ClientesExportarOutClienteHistorico.
     * 
     * @param indPre
     */
    public void setIndPre(java.lang.String indPre) {
        this.indPre = indPre;
    }


    /**
     * Gets the junPed value for this ClientesExportarOutClienteHistorico.
     * 
     * @return junPed
     */
    public java.lang.String getJunPed() {
        return junPed;
    }


    /**
     * Sets the junPed value for this ClientesExportarOutClienteHistorico.
     * 
     * @param junPed
     */
    public void setJunPed(java.lang.String junPed) {
        this.junPed = junPed;
    }


    /**
     * Gets the limApr value for this ClientesExportarOutClienteHistorico.
     * 
     * @return limApr
     */
    public java.lang.String getLimApr() {
        return limApr;
    }


    /**
     * Sets the limApr value for this ClientesExportarOutClienteHistorico.
     * 
     * @param limApr
     */
    public void setLimApr(java.lang.String limApr) {
        this.limApr = limApr;
    }


    /**
     * Gets the maiAtr value for this ClientesExportarOutClienteHistorico.
     * 
     * @return maiAtr
     */
    public java.lang.Integer getMaiAtr() {
        return maiAtr;
    }


    /**
     * Sets the maiAtr value for this ClientesExportarOutClienteHistorico.
     * 
     * @param maiAtr
     */
    public void setMaiAtr(java.lang.Integer maiAtr) {
        this.maiAtr = maiAtr;
    }


    /**
     * Gets the medAtr value for this ClientesExportarOutClienteHistorico.
     * 
     * @return medAtr
     */
    public java.lang.Integer getMedAtr() {
        return medAtr;
    }


    /**
     * Sets the medAtr value for this ClientesExportarOutClienteHistorico.
     * 
     * @param medAtr
     */
    public void setMedAtr(java.lang.Integer medAtr) {
        this.medAtr = medAtr;
    }


    /**
     * Gets the motDes value for this ClientesExportarOutClienteHistorico.
     * 
     * @return motDes
     */
    public java.lang.Integer getMotDes() {
        return motDes;
    }


    /**
     * Sets the motDes value for this ClientesExportarOutClienteHistorico.
     * 
     * @param motDes
     */
    public void setMotDes(java.lang.Integer motDes) {
        this.motDes = motDes;
    }


    /**
     * Gets the perAqa value for this ClientesExportarOutClienteHistorico.
     * 
     * @return perAqa
     */
    public java.lang.Double getPerAqa() {
        return perAqa;
    }


    /**
     * Sets the perAqa value for this ClientesExportarOutClienteHistorico.
     * 
     * @param perAqa
     */
    public void setPerAqa(java.lang.Double perAqa) {
        this.perAqa = perAqa;
    }


    /**
     * Gets the perCcr value for this ClientesExportarOutClienteHistorico.
     * 
     * @return perCcr
     */
    public java.lang.String getPerCcr() {
        return perCcr;
    }


    /**
     * Sets the perCcr value for this ClientesExportarOutClienteHistorico.
     * 
     * @param perCcr
     */
    public void setPerCcr(java.lang.String perCcr) {
        this.perCcr = perCcr;
    }


    /**
     * Gets the perCom value for this ClientesExportarOutClienteHistorico.
     * 
     * @return perCom
     */
    public java.lang.Double getPerCom() {
        return perCom;
    }


    /**
     * Sets the perCom value for this ClientesExportarOutClienteHistorico.
     * 
     * @param perCom
     */
    public void setPerCom(java.lang.Double perCom) {
        this.perCom = perCom;
    }


    /**
     * Gets the perDif value for this ClientesExportarOutClienteHistorico.
     * 
     * @return perDif
     */
    public java.lang.Double getPerDif() {
        return perDif;
    }


    /**
     * Sets the perDif value for this ClientesExportarOutClienteHistorico.
     * 
     * @param perDif
     */
    public void setPerDif(java.lang.Double perDif) {
        this.perDif = perDif;
    }


    /**
     * Gets the perDs1 value for this ClientesExportarOutClienteHistorico.
     * 
     * @return perDs1
     */
    public java.lang.Double getPerDs1() {
        return perDs1;
    }


    /**
     * Sets the perDs1 value for this ClientesExportarOutClienteHistorico.
     * 
     * @param perDs1
     */
    public void setPerDs1(java.lang.Double perDs1) {
        this.perDs1 = perDs1;
    }


    /**
     * Gets the perDs2 value for this ClientesExportarOutClienteHistorico.
     * 
     * @return perDs2
     */
    public java.lang.Double getPerDs2() {
        return perDs2;
    }


    /**
     * Sets the perDs2 value for this ClientesExportarOutClienteHistorico.
     * 
     * @param perDs2
     */
    public void setPerDs2(java.lang.Double perDs2) {
        this.perDs2 = perDs2;
    }


    /**
     * Gets the perDs3 value for this ClientesExportarOutClienteHistorico.
     * 
     * @return perDs3
     */
    public java.lang.Double getPerDs3() {
        return perDs3;
    }


    /**
     * Sets the perDs3 value for this ClientesExportarOutClienteHistorico.
     * 
     * @param perDs3
     */
    public void setPerDs3(java.lang.Double perDs3) {
        this.perDs3 = perDs3;
    }


    /**
     * Gets the perDs4 value for this ClientesExportarOutClienteHistorico.
     * 
     * @return perDs4
     */
    public java.lang.Double getPerDs4() {
        return perDs4;
    }


    /**
     * Sets the perDs4 value for this ClientesExportarOutClienteHistorico.
     * 
     * @param perDs4
     */
    public void setPerDs4(java.lang.Double perDs4) {
        this.perDs4 = perDs4;
    }


    /**
     * Gets the perDs5 value for this ClientesExportarOutClienteHistorico.
     * 
     * @return perDs5
     */
    public java.lang.Double getPerDs5() {
        return perDs5;
    }


    /**
     * Sets the perDs5 value for this ClientesExportarOutClienteHistorico.
     * 
     * @param perDs5
     */
    public void setPerDs5(java.lang.Double perDs5) {
        this.perDs5 = perDs5;
    }


    /**
     * Gets the perDsc value for this ClientesExportarOutClienteHistorico.
     * 
     * @return perDsc
     */
    public java.lang.Double getPerDsc() {
        return perDsc;
    }


    /**
     * Sets the perDsc value for this ClientesExportarOutClienteHistorico.
     * 
     * @param perDsc
     */
    public void setPerDsc(java.lang.Double perDsc) {
        this.perDsc = perDsc;
    }


    /**
     * Gets the perEmb value for this ClientesExportarOutClienteHistorico.
     * 
     * @return perEmb
     */
    public java.lang.Double getPerEmb() {
        return perEmb;
    }


    /**
     * Sets the perEmb value for this ClientesExportarOutClienteHistorico.
     * 
     * @param perEmb
     */
    public void setPerEmb(java.lang.Double perEmb) {
        this.perEmb = perEmb;
    }


    /**
     * Gets the perEnc value for this ClientesExportarOutClienteHistorico.
     * 
     * @return perEnc
     */
    public java.lang.Double getPerEnc() {
        return perEnc;
    }


    /**
     * Sets the perEnc value for this ClientesExportarOutClienteHistorico.
     * 
     * @param perEnc
     */
    public void setPerEnc(java.lang.Double perEnc) {
        this.perEnc = perEnc;
    }


    /**
     * Gets the perFre value for this ClientesExportarOutClienteHistorico.
     * 
     * @return perFre
     */
    public java.lang.Double getPerFre() {
        return perFre;
    }


    /**
     * Sets the perFre value for this ClientesExportarOutClienteHistorico.
     * 
     * @param perFre
     */
    public void setPerFre(java.lang.Double perFre) {
        this.perFre = perFre;
    }


    /**
     * Gets the perIss value for this ClientesExportarOutClienteHistorico.
     * 
     * @return perIss
     */
    public java.lang.Double getPerIss() {
        return perIss;
    }


    /**
     * Sets the perIss value for this ClientesExportarOutClienteHistorico.
     * 
     * @param perIss
     */
    public void setPerIss(java.lang.Double perIss) {
        this.perIss = perIss;
    }


    /**
     * Gets the perOf1 value for this ClientesExportarOutClienteHistorico.
     * 
     * @return perOf1
     */
    public java.lang.Double getPerOf1() {
        return perOf1;
    }


    /**
     * Sets the perOf1 value for this ClientesExportarOutClienteHistorico.
     * 
     * @param perOf1
     */
    public void setPerOf1(java.lang.Double perOf1) {
        this.perOf1 = perOf1;
    }


    /**
     * Gets the perOf2 value for this ClientesExportarOutClienteHistorico.
     * 
     * @return perOf2
     */
    public java.lang.Double getPerOf2() {
        return perOf2;
    }


    /**
     * Sets the perOf2 value for this ClientesExportarOutClienteHistorico.
     * 
     * @param perOf2
     */
    public void setPerOf2(java.lang.Double perOf2) {
        this.perOf2 = perOf2;
    }


    /**
     * Gets the perOut value for this ClientesExportarOutClienteHistorico.
     * 
     * @return perOut
     */
    public java.lang.Double getPerOut() {
        return perOut;
    }


    /**
     * Sets the perOut value for this ClientesExportarOutClienteHistorico.
     * 
     * @param perOut
     */
    public void setPerOut(java.lang.Double perOut) {
        this.perOut = perOut;
    }


    /**
     * Gets the perSeg value for this ClientesExportarOutClienteHistorico.
     * 
     * @return perSeg
     */
    public java.lang.Double getPerSeg() {
        return perSeg;
    }


    /**
     * Sets the perSeg value for this ClientesExportarOutClienteHistorico.
     * 
     * @param perSeg
     */
    public void setPerSeg(java.lang.Double perSeg) {
        this.perSeg = perSeg;
    }


    /**
     * Gets the porNa1 value for this ClientesExportarOutClienteHistorico.
     * 
     * @return porNa1
     */
    public java.lang.String getPorNa1() {
        return porNa1;
    }


    /**
     * Sets the porNa1 value for this ClientesExportarOutClienteHistorico.
     * 
     * @param porNa1
     */
    public void setPorNa1(java.lang.String porNa1) {
        this.porNa1 = porNa1;
    }


    /**
     * Gets the porNa2 value for this ClientesExportarOutClienteHistorico.
     * 
     * @return porNa2
     */
    public java.lang.String getPorNa2() {
        return porNa2;
    }


    /**
     * Sets the porNa2 value for this ClientesExportarOutClienteHistorico.
     * 
     * @param porNa2
     */
    public void setPorNa2(java.lang.String porNa2) {
        this.porNa2 = porNa2;
    }


    /**
     * Gets the porSi1 value for this ClientesExportarOutClienteHistorico.
     * 
     * @return porSi1
     */
    public java.lang.String getPorSi1() {
        return porSi1;
    }


    /**
     * Sets the porSi1 value for this ClientesExportarOutClienteHistorico.
     * 
     * @param porSi1
     */
    public void setPorSi1(java.lang.String porSi1) {
        this.porSi1 = porSi1;
    }


    /**
     * Gets the porSi2 value for this ClientesExportarOutClienteHistorico.
     * 
     * @return porSi2
     */
    public java.lang.String getPorSi2() {
        return porSi2;
    }


    /**
     * Sets the porSi2 value for this ClientesExportarOutClienteHistorico.
     * 
     * @param porSi2
     */
    public void setPorSi2(java.lang.String porSi2) {
        this.porSi2 = porSi2;
    }


    /**
     * Gets the prdDsc value for this ClientesExportarOutClienteHistorico.
     * 
     * @return prdDsc
     */
    public java.lang.String getPrdDsc() {
        return prdDsc;
    }


    /**
     * Sets the prdDsc value for this ClientesExportarOutClienteHistorico.
     * 
     * @param prdDsc
     */
    public void setPrdDsc(java.lang.String prdDsc) {
        this.prdDsc = prdDsc;
    }


    /**
     * Gets the przMrt value for this ClientesExportarOutClienteHistorico.
     * 
     * @return przMrt
     */
    public java.lang.Integer getPrzMrt() {
        return przMrt;
    }


    /**
     * Sets the przMrt value for this ClientesExportarOutClienteHistorico.
     * 
     * @param przMrt
     */
    public void setPrzMrt(java.lang.Integer przMrt) {
        this.przMrt = przMrt;
    }


    /**
     * Gets the qdiPrt value for this ClientesExportarOutClienteHistorico.
     * 
     * @return qdiPrt
     */
    public java.lang.Integer getQdiPrt() {
        return qdiPrt;
    }


    /**
     * Sets the qdiPrt value for this ClientesExportarOutClienteHistorico.
     * 
     * @param qdiPrt
     */
    public void setQdiPrt(java.lang.Integer qdiPrt) {
        this.qdiPrt = qdiPrt;
    }


    /**
     * Gets the qtdChs value for this ClientesExportarOutClienteHistorico.
     * 
     * @return qtdChs
     */
    public java.lang.Integer getQtdChs() {
        return qtdChs;
    }


    /**
     * Sets the qtdChs value for this ClientesExportarOutClienteHistorico.
     * 
     * @param qtdChs
     */
    public void setQtdChs(java.lang.Integer qtdChs) {
        this.qtdChs = qtdChs;
    }


    /**
     * Gets the qtdDcv value for this ClientesExportarOutClienteHistorico.
     * 
     * @return qtdDcv
     */
    public java.lang.Integer getQtdDcv() {
        return qtdDcv;
    }


    /**
     * Sets the qtdDcv value for this ClientesExportarOutClienteHistorico.
     * 
     * @param qtdDcv
     */
    public void setQtdDcv(java.lang.Integer qtdDcv) {
        this.qtdDcv = qtdDcv;
    }


    /**
     * Gets the qtdMfp value for this ClientesExportarOutClienteHistorico.
     * 
     * @return qtdMfp
     */
    public java.lang.Integer getQtdMfp() {
        return qtdMfp;
    }


    /**
     * Sets the qtdMfp value for this ClientesExportarOutClienteHistorico.
     * 
     * @param qtdMfp
     */
    public void setQtdMfp(java.lang.Integer qtdMfp) {
        this.qtdMfp = qtdMfp;
    }


    /**
     * Gets the qtdPgt value for this ClientesExportarOutClienteHistorico.
     * 
     * @return qtdPgt
     */
    public java.lang.Integer getQtdPgt() {
        return qtdPgt;
    }


    /**
     * Sets the qtdPgt value for this ClientesExportarOutClienteHistorico.
     * 
     * @param qtdPgt
     */
    public void setQtdPgt(java.lang.Integer qtdPgt) {
        this.qtdPgt = qtdPgt;
    }


    /**
     * Gets the qtdPrt value for this ClientesExportarOutClienteHistorico.
     * 
     * @return qtdPrt
     */
    public java.lang.Integer getQtdPrt() {
        return qtdPrt;
    }


    /**
     * Sets the qtdPrt value for this ClientesExportarOutClienteHistorico.
     * 
     * @param qtdPrt
     */
    public void setQtdPrt(java.lang.Integer qtdPrt) {
        this.qtdPrt = qtdPrt;
    }


    /**
     * Gets the qtdRpm value for this ClientesExportarOutClienteHistorico.
     * 
     * @return qtdRpm
     */
    public java.lang.Integer getQtdRpm() {
        return qtdRpm;
    }


    /**
     * Sets the qtdRpm value for this ClientesExportarOutClienteHistorico.
     * 
     * @param qtdRpm
     */
    public void setQtdRpm(java.lang.Integer qtdRpm) {
        this.qtdRpm = qtdRpm;
    }


    /**
     * Gets the qtdTpc value for this ClientesExportarOutClienteHistorico.
     * 
     * @return qtdTpc
     */
    public java.lang.Integer getQtdTpc() {
        return qtdTpc;
    }


    /**
     * Sets the qtdTpc value for this ClientesExportarOutClienteHistorico.
     * 
     * @param qtdTpc
     */
    public void setQtdTpc(java.lang.Integer qtdTpc) {
        this.qtdTpc = qtdTpc;
    }


    /**
     * Gets the recDtj value for this ClientesExportarOutClienteHistorico.
     * 
     * @return recDtj
     */
    public java.lang.Integer getRecDtj() {
        return recDtj;
    }


    /**
     * Sets the recDtj value for this ClientesExportarOutClienteHistorico.
     * 
     * @param recDtj
     */
    public void setRecDtj(java.lang.Integer recDtj) {
        this.recDtj = recDtj;
    }


    /**
     * Gets the recDtm value for this ClientesExportarOutClienteHistorico.
     * 
     * @return recDtm
     */
    public java.lang.Integer getRecDtm() {
        return recDtm;
    }


    /**
     * Sets the recDtm value for this ClientesExportarOutClienteHistorico.
     * 
     * @param recDtm
     */
    public void setRecDtm(java.lang.Integer recDtm) {
        this.recDtm = recDtm;
    }


    /**
     * Gets the recJmm value for this ClientesExportarOutClienteHistorico.
     * 
     * @return recJmm
     */
    public java.lang.Double getRecJmm() {
        return recJmm;
    }


    /**
     * Sets the recJmm value for this ClientesExportarOutClienteHistorico.
     * 
     * @param recJmm
     */
    public void setRecJmm(java.lang.Double recJmm) {
        this.recJmm = recJmm;
    }


    /**
     * Gets the recMul value for this ClientesExportarOutClienteHistorico.
     * 
     * @return recMul
     */
    public java.lang.Double getRecMul() {
        return recMul;
    }


    /**
     * Sets the recMul value for this ClientesExportarOutClienteHistorico.
     * 
     * @param recMul
     */
    public void setRecMul(java.lang.Double recMul) {
        this.recMul = recMul;
    }


    /**
     * Gets the recTjr value for this ClientesExportarOutClienteHistorico.
     * 
     * @return recTjr
     */
    public java.lang.String getRecTjr() {
        return recTjr;
    }


    /**
     * Sets the recTjr value for this ClientesExportarOutClienteHistorico.
     * 
     * @param recTjr
     */
    public void setRecTjr(java.lang.String recTjr) {
        this.recTjr = recTjr;
    }


    /**
     * Gets the salCre value for this ClientesExportarOutClienteHistorico.
     * 
     * @return salCre
     */
    public java.lang.Double getSalCre() {
        return salCre;
    }


    /**
     * Sets the salCre value for this ClientesExportarOutClienteHistorico.
     * 
     * @param salCre
     */
    public void setSalCre(java.lang.Double salCre) {
        this.salCre = salCre;
    }


    /**
     * Gets the salDup value for this ClientesExportarOutClienteHistorico.
     * 
     * @return salDup
     */
    public java.lang.Double getSalDup() {
        return salDup;
    }


    /**
     * Sets the salDup value for this ClientesExportarOutClienteHistorico.
     * 
     * @param salDup
     */
    public void setSalDup(java.lang.Double salDup) {
        this.salDup = salDup;
    }


    /**
     * Gets the salOut value for this ClientesExportarOutClienteHistorico.
     * 
     * @return salOut
     */
    public java.lang.Double getSalOut() {
        return salOut;
    }


    /**
     * Sets the salOut value for this ClientesExportarOutClienteHistorico.
     * 
     * @param salOut
     */
    public void setSalOut(java.lang.Double salOut) {
        this.salOut = salOut;
    }


    /**
     * Gets the seqCob value for this ClientesExportarOutClienteHistorico.
     * 
     * @return seqCob
     */
    public java.lang.Integer getSeqCob() {
        return seqCob;
    }


    /**
     * Sets the seqCob value for this ClientesExportarOutClienteHistorico.
     * 
     * @param seqCob
     */
    public void setSeqCob(java.lang.Integer seqCob) {
        this.seqCob = seqCob;
    }


    /**
     * Gets the seqEnt value for this ClientesExportarOutClienteHistorico.
     * 
     * @return seqEnt
     */
    public java.lang.Integer getSeqEnt() {
        return seqEnt;
    }


    /**
     * Sets the seqEnt value for this ClientesExportarOutClienteHistorico.
     * 
     * @param seqEnt
     */
    public void setSeqEnt(java.lang.Integer seqEnt) {
        this.seqEnt = seqEnt;
    }


    /**
     * Gets the tolDsc value for this ClientesExportarOutClienteHistorico.
     * 
     * @return tolDsc
     */
    public java.lang.Integer getTolDsc() {
        return tolDsc;
    }


    /**
     * Sets the tolDsc value for this ClientesExportarOutClienteHistorico.
     * 
     * @param tolDsc
     */
    public void setTolDsc(java.lang.Integer tolDsc) {
        this.tolDsc = tolDsc;
    }


    /**
     * Gets the ultNfv value for this ClientesExportarOutClienteHistorico.
     * 
     * @return ultNfv
     */
    public java.lang.Integer getUltNfv() {
        return ultNfv;
    }


    /**
     * Sets the ultNfv value for this ClientesExportarOutClienteHistorico.
     * 
     * @param ultNfv
     */
    public void setUltNfv(java.lang.Integer ultNfv) {
        this.ultNfv = ultNfv;
    }


    /**
     * Gets the ultSnf value for this ClientesExportarOutClienteHistorico.
     * 
     * @return ultSnf
     */
    public java.lang.String getUltSnf() {
        return ultSnf;
    }


    /**
     * Sets the ultSnf value for this ClientesExportarOutClienteHistorico.
     * 
     * @param ultSnf
     */
    public void setUltSnf(java.lang.String ultSnf) {
        this.ultSnf = ultSnf;
    }


    /**
     * Gets the usuAge value for this ClientesExportarOutClienteHistorico.
     * 
     * @return usuAge
     */
    public java.lang.Double getUsuAge() {
        return usuAge;
    }


    /**
     * Sets the usuAge value for this ClientesExportarOutClienteHistorico.
     * 
     * @param usuAge
     */
    public void setUsuAge(java.lang.Double usuAge) {
        this.usuAge = usuAge;
    }


    /**
     * Gets the usuAlt value for this ClientesExportarOutClienteHistorico.
     * 
     * @return usuAlt
     */
    public java.lang.Double getUsuAlt() {
        return usuAlt;
    }


    /**
     * Sets the usuAlt value for this ClientesExportarOutClienteHistorico.
     * 
     * @param usuAlt
     */
    public void setUsuAlt(java.lang.Double usuAlt) {
        this.usuAlt = usuAlt;
    }


    /**
     * Gets the usuGer value for this ClientesExportarOutClienteHistorico.
     * 
     * @return usuGer
     */
    public java.lang.Double getUsuGer() {
        return usuGer;
    }


    /**
     * Sets the usuGer value for this ClientesExportarOutClienteHistorico.
     * 
     * @param usuGer
     */
    public void setUsuGer(java.lang.Double usuGer) {
        this.usuGer = usuGer;
    }


    /**
     * Gets the vlrAcr value for this ClientesExportarOutClienteHistorico.
     * 
     * @return vlrAcr
     */
    public java.lang.Double getVlrAcr() {
        return vlrAcr;
    }


    /**
     * Sets the vlrAcr value for this ClientesExportarOutClienteHistorico.
     * 
     * @param vlrAcr
     */
    public void setVlrAcr(java.lang.Double vlrAcr) {
        this.vlrAcr = vlrAcr;
    }


    /**
     * Gets the vlrAtr value for this ClientesExportarOutClienteHistorico.
     * 
     * @return vlrAtr
     */
    public java.lang.Double getVlrAtr() {
        return vlrAtr;
    }


    /**
     * Sets the vlrAtr value for this ClientesExportarOutClienteHistorico.
     * 
     * @param vlrAtr
     */
    public void setVlrAtr(java.lang.Double vlrAtr) {
        this.vlrAtr = vlrAtr;
    }


    /**
     * Gets the vlrLim value for this ClientesExportarOutClienteHistorico.
     * 
     * @return vlrLim
     */
    public java.lang.Double getVlrLim() {
        return vlrLim;
    }


    /**
     * Sets the vlrLim value for this ClientesExportarOutClienteHistorico.
     * 
     * @param vlrLim
     */
    public void setVlrLim(java.lang.Double vlrLim) {
        this.vlrLim = vlrLim;
    }


    /**
     * Gets the vlrMac value for this ClientesExportarOutClienteHistorico.
     * 
     * @return vlrMac
     */
    public java.lang.Double getVlrMac() {
        return vlrMac;
    }


    /**
     * Sets the vlrMac value for this ClientesExportarOutClienteHistorico.
     * 
     * @param vlrMac
     */
    public void setVlrMac(java.lang.Double vlrMac) {
        this.vlrMac = vlrMac;
    }


    /**
     * Gets the vlrMfa value for this ClientesExportarOutClienteHistorico.
     * 
     * @return vlrMfa
     */
    public java.lang.Double getVlrMfa() {
        return vlrMfa;
    }


    /**
     * Sets the vlrMfa value for this ClientesExportarOutClienteHistorico.
     * 
     * @param vlrMfa
     */
    public void setVlrMfa(java.lang.Double vlrMfa) {
        this.vlrMfa = vlrMfa;
    }


    /**
     * Gets the vlrPfa value for this ClientesExportarOutClienteHistorico.
     * 
     * @return vlrPfa
     */
    public java.lang.Double getVlrPfa() {
        return vlrPfa;
    }


    /**
     * Sets the vlrPfa value for this ClientesExportarOutClienteHistorico.
     * 
     * @param vlrPfa
     */
    public void setVlrPfa(java.lang.Double vlrPfa) {
        this.vlrPfa = vlrPfa;
    }


    /**
     * Gets the vlrPrt value for this ClientesExportarOutClienteHistorico.
     * 
     * @return vlrPrt
     */
    public java.lang.Double getVlrPrt() {
        return vlrPrt;
    }


    /**
     * Sets the vlrPrt value for this ClientesExportarOutClienteHistorico.
     * 
     * @param vlrPrt
     */
    public void setVlrPrt(java.lang.Double vlrPrt) {
        this.vlrPrt = vlrPrt;
    }


    /**
     * Gets the vlrUfa value for this ClientesExportarOutClienteHistorico.
     * 
     * @return vlrUfa
     */
    public java.lang.Double getVlrUfa() {
        return vlrUfa;
    }


    /**
     * Sets the vlrUfa value for this ClientesExportarOutClienteHistorico.
     * 
     * @param vlrUfa
     */
    public void setVlrUfa(java.lang.Double vlrUfa) {
        this.vlrUfa = vlrUfa;
    }


    /**
     * Gets the vlrUpc value for this ClientesExportarOutClienteHistorico.
     * 
     * @return vlrUpc
     */
    public java.lang.Double getVlrUpc() {
        return vlrUpc;
    }


    /**
     * Sets the vlrUpc value for this ClientesExportarOutClienteHistorico.
     * 
     * @param vlrUpc
     */
    public void setVlrUpc(java.lang.Double vlrUpc) {
        this.vlrUpc = vlrUpc;
    }


    /**
     * Gets the vlrUpe value for this ClientesExportarOutClienteHistorico.
     * 
     * @return vlrUpe
     */
    public java.lang.Double getVlrUpe() {
        return vlrUpe;
    }


    /**
     * Sets the vlrUpe value for this ClientesExportarOutClienteHistorico.
     * 
     * @param vlrUpe
     */
    public void setVlrUpe(java.lang.Double vlrUpe) {
        this.vlrUpe = vlrUpe;
    }


    /**
     * Gets the vlrUpg value for this ClientesExportarOutClienteHistorico.
     * 
     * @return vlrUpg
     */
    public java.lang.Double getVlrUpg() {
        return vlrUpg;
    }


    /**
     * Sets the vlrUpg value for this ClientesExportarOutClienteHistorico.
     * 
     * @param vlrUpg
     */
    public void setVlrUpg(java.lang.Double vlrUpg) {
        this.vlrUpg = vlrUpg;
    }


    /**
     * Gets the volSep value for this ClientesExportarOutClienteHistorico.
     * 
     * @return volSep
     */
    public java.lang.String getVolSep() {
        return volSep;
    }


    /**
     * Sets the volSep value for this ClientesExportarOutClienteHistorico.
     * 
     * @param volSep
     */
    public void setVolSep(java.lang.String volSep) {
        this.volSep = volSep;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientesExportarOutClienteHistorico)) return false;
        ClientesExportarOutClienteHistorico other = (ClientesExportarOutClienteHistorico) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.acePar==null && other.getAcePar()==null) || 
             (this.acePar!=null &&
              this.acePar.equals(other.getAcePar()))) &&
            ((this.acrDia==null && other.getAcrDia()==null) || 
             (this.acrDia!=null &&
              this.acrDia.equals(other.getAcrDia()))) &&
            ((this.antDsc==null && other.getAntDsc()==null) || 
             (this.antDsc!=null &&
              this.antDsc.equals(other.getAntDsc()))) &&
            ((this.apmDen==null && other.getApmDen()==null) || 
             (this.apmDen!=null &&
              this.apmDen.equals(other.getApmDen()))) &&
            ((this.avaAti==null && other.getAvaAti()==null) || 
             (this.avaAti!=null &&
              this.avaAti.equals(other.getAvaAti()))) &&
            ((this.avaMot==null && other.getAvaMot()==null) || 
             (this.avaMot!=null &&
              this.avaMot.equals(other.getAvaMot()))) &&
            ((this.avaObs==null && other.getAvaObs()==null) || 
             (this.avaObs!=null &&
              this.avaObs.equals(other.getAvaObs()))) &&
            ((this.avaVlr==null && other.getAvaVlr()==null) || 
             (this.avaVlr!=null &&
              this.avaVlr.equals(other.getAvaVlr()))) &&
            ((this.avaVls==null && other.getAvaVls()==null) || 
             (this.avaVls!=null &&
              this.avaVls.equals(other.getAvaVls()))) &&
            ((this.avaVlu==null && other.getAvaVlu()==null) || 
             (this.avaVlu!=null &&
              this.avaVlu.equals(other.getAvaVlu()))) &&
            ((this.avdAlt==null && other.getAvdAlt()==null) || 
             (this.avdAlt!=null &&
              this.avdAlt.equals(other.getAvdAlt()))) &&
            ((this.avdGer==null && other.getAvdGer()==null) || 
             (this.avdGer!=null &&
              this.avdGer.equals(other.getAvdGer()))) &&
            ((this.avhAlt==null && other.getAvhAlt()==null) || 
             (this.avhAlt!=null &&
              this.avhAlt.equals(other.getAvhAlt()))) &&
            ((this.avhGer==null && other.getAvhGer()==null) || 
             (this.avhGer!=null &&
              this.avhGer.equals(other.getAvhGer()))) &&
            ((this.avuAlt==null && other.getAvuAlt()==null) || 
             (this.avuAlt!=null &&
              this.avuAlt.equals(other.getAvuAlt()))) &&
            ((this.avuGer==null && other.getAvuGer()==null) || 
             (this.avuGer!=null &&
              this.avuGer.equals(other.getAvuGer()))) &&
            ((this.campoUsuarioClienteDefinicoes==null && other.getCampoUsuarioClienteDefinicoes()==null) || 
             (this.campoUsuarioClienteDefinicoes!=null &&
              java.util.Arrays.equals(this.campoUsuarioClienteDefinicoes, other.getCampoUsuarioClienteDefinicoes()))) &&
            ((this.catCli==null && other.getCatCli()==null) || 
             (this.catCli!=null &&
              this.catCli.equals(other.getCatCli()))) &&
            ((this.ccbCli==null && other.getCcbCli()==null) || 
             (this.ccbCli!=null &&
              this.ccbCli.equals(other.getCcbCli()))) &&
            ((this.cifFob==null && other.getCifFob()==null) || 
             (this.cifFob!=null &&
              this.cifFob.equals(other.getCifFob()))) &&
            ((this.codAge==null && other.getCodAge()==null) || 
             (this.codAge!=null &&
              this.codAge.equals(other.getCodAge()))) &&
            ((this.codBan==null && other.getCodBan()==null) || 
             (this.codBan!=null &&
              this.codBan.equals(other.getCodBan()))) &&
            ((this.codCli==null && other.getCodCli()==null) || 
             (this.codCli!=null &&
              this.codCli.equals(other.getCodCli()))) &&
            ((this.codCpg==null && other.getCodCpg()==null) || 
             (this.codCpg!=null &&
              this.codCpg.equals(other.getCodCpg()))) &&
            ((this.codCrp==null && other.getCodCrp()==null) || 
             (this.codCrp!=null &&
              this.codCrp.equals(other.getCodCrp()))) &&
            ((this.codCrt==null && other.getCodCrt()==null) || 
             (this.codCrt!=null &&
              this.codCrt.equals(other.getCodCrt()))) &&
            ((this.codFcr==null && other.getCodFcr()==null) || 
             (this.codFcr!=null &&
              this.codFcr.equals(other.getCodFcr()))) &&
            ((this.codFin==null && other.getCodFin()==null) || 
             (this.codFin!=null &&
              this.codFin.equals(other.getCodFin()))) &&
            ((this.codFpg==null && other.getCodFpg()==null) || 
             (this.codFpg!=null &&
              this.codFpg.equals(other.getCodFpg()))) &&
            ((this.codFrj==null && other.getCodFrj()==null) || 
             (this.codFrj!=null &&
              this.codFrj.equals(other.getCodFrj()))) &&
            ((this.codIn1==null && other.getCodIn1()==null) || 
             (this.codIn1!=null &&
              this.codIn1.equals(other.getCodIn1()))) &&
            ((this.codIn2==null && other.getCodIn2()==null) || 
             (this.codIn2!=null &&
              this.codIn2.equals(other.getCodIn2()))) &&
            ((this.codMar==null && other.getCodMar()==null) || 
             (this.codMar!=null &&
              this.codMar.equals(other.getCodMar()))) &&
            ((this.codPdv==null && other.getCodPdv()==null) || 
             (this.codPdv!=null &&
              this.codPdv.equals(other.getCodPdv()))) &&
            ((this.codRed==null && other.getCodRed()==null) || 
             (this.codRed!=null &&
              this.codRed.equals(other.getCodRed()))) &&
            ((this.codRep==null && other.getCodRep()==null) || 
             (this.codRep!=null &&
              this.codRep.equals(other.getCodRep()))) &&
            ((this.codRve==null && other.getCodRve()==null) || 
             (this.codRve!=null &&
              this.codRve.equals(other.getCodRve()))) &&
            ((this.codStr==null && other.getCodStr()==null) || 
             (this.codStr!=null &&
              this.codStr.equals(other.getCodStr()))) &&
            ((this.codTab==null && other.getCodTab()==null) || 
             (this.codTab!=null &&
              this.codTab.equals(other.getCodTab()))) &&
            ((this.codTic==null && other.getCodTic()==null) || 
             (this.codTic!=null &&
              this.codTic.equals(other.getCodTic()))) &&
            ((this.codTpr==null && other.getCodTpr()==null) || 
             (this.codTpr!=null &&
              this.codTpr.equals(other.getCodTpr()))) &&
            ((this.codTra==null && other.getCodTra()==null) || 
             (this.codTra!=null &&
              this.codTra.equals(other.getCodTra()))) &&
            ((this.codTrd==null && other.getCodTrd()==null) || 
             (this.codTrd!=null &&
              this.codTrd.equals(other.getCodTrd()))) &&
            ((this.codVen==null && other.getCodVen()==null) || 
             (this.codVen!=null &&
              this.codVen.equals(other.getCodVen()))) &&
            ((this.conFin==null && other.getConFin()==null) || 
             (this.conFin!=null &&
              this.conFin.equals(other.getConFin()))) &&
            ((this.criEdv==null && other.getCriEdv()==null) || 
             (this.criEdv!=null &&
              this.criEdv.equals(other.getCriEdv()))) &&
            ((this.criRat==null && other.getCriRat()==null) || 
             (this.criRat!=null &&
              this.criRat.equals(other.getCriRat()))) &&
            ((this.ctaAad==null && other.getCtaAad()==null) || 
             (this.ctaAad!=null &&
              this.ctaAad.equals(other.getCtaAad()))) &&
            ((this.ctaAux==null && other.getCtaAux()==null) || 
             (this.ctaAux!=null &&
              this.ctaAux.equals(other.getCtaAux()))) &&
            ((this.ctaFcr==null && other.getCtaFcr()==null) || 
             (this.ctaFcr!=null &&
              this.ctaFcr.equals(other.getCtaFcr()))) &&
            ((this.ctaFdv==null && other.getCtaFdv()==null) || 
             (this.ctaFdv!=null &&
              this.ctaFdv.equals(other.getCtaFdv()))) &&
            ((this.ctaRcr==null && other.getCtaRcr()==null) || 
             (this.ctaRcr!=null &&
              this.ctaRcr.equals(other.getCtaRcr()))) &&
            ((this.ctaRed==null && other.getCtaRed()==null) || 
             (this.ctaRed!=null &&
              this.ctaRed.equals(other.getCtaRed()))) &&
            ((this.ctrPad==null && other.getCtrPad()==null) || 
             (this.ctrPad!=null &&
              this.ctrPad.equals(other.getCtrPad()))) &&
            ((this.datAlt==null && other.getDatAlt()==null) || 
             (this.datAlt!=null &&
              this.datAlt.equals(other.getDatAlt()))) &&
            ((this.datAtr==null && other.getDatAtr()==null) || 
             (this.datAtr!=null &&
              this.datAtr.equals(other.getDatAtr()))) &&
            ((this.datGer==null && other.getDatGer()==null) || 
             (this.datGer!=null &&
              this.datGer.equals(other.getDatGer()))) &&
            ((this.datLim==null && other.getDatLim()==null) || 
             (this.datLim!=null &&
              this.datLim.equals(other.getDatLim()))) &&
            ((this.datMac==null && other.getDatMac()==null) || 
             (this.datMac!=null &&
              this.datMac.equals(other.getDatMac()))) &&
            ((this.datMfa==null && other.getDatMfa()==null) || 
             (this.datMfa!=null &&
              this.datMfa.equals(other.getDatMfa()))) &&
            ((this.datPal==null && other.getDatPal()==null) || 
             (this.datPal!=null &&
              this.datPal.equals(other.getDatPal()))) &&
            ((this.datPmr==null && other.getDatPmr()==null) || 
             (this.datPmr!=null &&
              this.datPmr.equals(other.getDatPmr()))) &&
            ((this.datUfa==null && other.getDatUfa()==null) || 
             (this.datUfa!=null &&
              this.datUfa.equals(other.getDatUfa()))) &&
            ((this.datUpc==null && other.getDatUpc()==null) || 
             (this.datUpc!=null &&
              this.datUpc.equals(other.getDatUpc()))) &&
            ((this.datUpe==null && other.getDatUpe()==null) || 
             (this.datUpe!=null &&
              this.datUpe.equals(other.getDatUpe()))) &&
            ((this.datUpg==null && other.getDatUpg()==null) || 
             (this.datUpg!=null &&
              this.datUpg.equals(other.getDatUpg()))) &&
            ((this.diaEsp==null && other.getDiaEsp()==null) || 
             (this.diaEsp!=null &&
              this.diaEsp.equals(other.getDiaEsp()))) &&
            ((this.diaMe1==null && other.getDiaMe1()==null) || 
             (this.diaMe1!=null &&
              this.diaMe1.equals(other.getDiaMe1()))) &&
            ((this.diaMe2==null && other.getDiaMe2()==null) || 
             (this.diaMe2!=null &&
              this.diaMe2.equals(other.getDiaMe2()))) &&
            ((this.diaMe3==null && other.getDiaMe3()==null) || 
             (this.diaMe3!=null &&
              this.diaMe3.equals(other.getDiaMe3()))) &&
            ((this.dscAnt==null && other.getDscAnt()==null) || 
             (this.dscAnt!=null &&
              this.dscAnt.equals(other.getDscAnt()))) &&
            ((this.dscPon==null && other.getDscPon()==null) || 
             (this.dscPon!=null &&
              this.dscPon.equals(other.getDscPon()))) &&
            ((this.dscPrd==null && other.getDscPrd()==null) || 
             (this.dscPrd!=null &&
              this.dscPrd.equals(other.getDscPrd()))) &&
            ((this.ecpCnp==null && other.getEcpCnp()==null) || 
             (this.ecpCnp!=null &&
              this.ecpCnp.equals(other.getEcpCnp()))) &&
            ((this.epcPed==null && other.getEpcPed()==null) || 
             (this.epcPed!=null &&
              this.epcPed.equals(other.getEpcPed()))) &&
            ((this.exiAge==null && other.getExiAge()==null) || 
             (this.exiAge!=null &&
              this.exiAge.equals(other.getExiAge()))) &&
            ((this.exiLcp==null && other.getExiLcp()==null) || 
             (this.exiLcp!=null &&
              this.exiLcp.equals(other.getExiLcp()))) &&
            ((this.fveCpg==null && other.getFveCpg()==null) || 
             (this.fveCpg!=null &&
              this.fveCpg.equals(other.getFveCpg()))) &&
            ((this.fveFpg==null && other.getFveFpg()==null) || 
             (this.fveFpg!=null &&
              this.fveFpg.equals(other.getFveFpg()))) &&
            ((this.fveTns==null && other.getFveTns()==null) || 
             (this.fveTns!=null &&
              this.fveTns.equals(other.getFveTns()))) &&
            ((this.gerTcc==null && other.getGerTcc()==null) || 
             (this.gerTcc!=null &&
              this.gerTcc.equals(other.getGerTcc()))) &&
            ((this.horAlt==null && other.getHorAlt()==null) || 
             (this.horAlt!=null &&
              this.horAlt.equals(other.getHorAlt()))) &&
            ((this.horGer==null && other.getHorGer()==null) || 
             (this.horGer!=null &&
              this.horGer.equals(other.getHorGer()))) &&
            ((this.horPal==null && other.getHorPal()==null) || 
             (this.horPal!=null &&
              this.horPal.equals(other.getHorPal()))) &&
            ((this.indAgr==null && other.getIndAgr()==null) || 
             (this.indAgr!=null &&
              this.indAgr.equals(other.getIndAgr()))) &&
            ((this.indExp==null && other.getIndExp()==null) || 
             (this.indExp!=null &&
              this.indExp.equals(other.getIndExp()))) &&
            ((this.indOrf==null && other.getIndOrf()==null) || 
             (this.indOrf!=null &&
              this.indOrf.equals(other.getIndOrf()))) &&
            ((this.indPre==null && other.getIndPre()==null) || 
             (this.indPre!=null &&
              this.indPre.equals(other.getIndPre()))) &&
            ((this.junPed==null && other.getJunPed()==null) || 
             (this.junPed!=null &&
              this.junPed.equals(other.getJunPed()))) &&
            ((this.limApr==null && other.getLimApr()==null) || 
             (this.limApr!=null &&
              this.limApr.equals(other.getLimApr()))) &&
            ((this.maiAtr==null && other.getMaiAtr()==null) || 
             (this.maiAtr!=null &&
              this.maiAtr.equals(other.getMaiAtr()))) &&
            ((this.medAtr==null && other.getMedAtr()==null) || 
             (this.medAtr!=null &&
              this.medAtr.equals(other.getMedAtr()))) &&
            ((this.motDes==null && other.getMotDes()==null) || 
             (this.motDes!=null &&
              this.motDes.equals(other.getMotDes()))) &&
            ((this.perAqa==null && other.getPerAqa()==null) || 
             (this.perAqa!=null &&
              this.perAqa.equals(other.getPerAqa()))) &&
            ((this.perCcr==null && other.getPerCcr()==null) || 
             (this.perCcr!=null &&
              this.perCcr.equals(other.getPerCcr()))) &&
            ((this.perCom==null && other.getPerCom()==null) || 
             (this.perCom!=null &&
              this.perCom.equals(other.getPerCom()))) &&
            ((this.perDif==null && other.getPerDif()==null) || 
             (this.perDif!=null &&
              this.perDif.equals(other.getPerDif()))) &&
            ((this.perDs1==null && other.getPerDs1()==null) || 
             (this.perDs1!=null &&
              this.perDs1.equals(other.getPerDs1()))) &&
            ((this.perDs2==null && other.getPerDs2()==null) || 
             (this.perDs2!=null &&
              this.perDs2.equals(other.getPerDs2()))) &&
            ((this.perDs3==null && other.getPerDs3()==null) || 
             (this.perDs3!=null &&
              this.perDs3.equals(other.getPerDs3()))) &&
            ((this.perDs4==null && other.getPerDs4()==null) || 
             (this.perDs4!=null &&
              this.perDs4.equals(other.getPerDs4()))) &&
            ((this.perDs5==null && other.getPerDs5()==null) || 
             (this.perDs5!=null &&
              this.perDs5.equals(other.getPerDs5()))) &&
            ((this.perDsc==null && other.getPerDsc()==null) || 
             (this.perDsc!=null &&
              this.perDsc.equals(other.getPerDsc()))) &&
            ((this.perEmb==null && other.getPerEmb()==null) || 
             (this.perEmb!=null &&
              this.perEmb.equals(other.getPerEmb()))) &&
            ((this.perEnc==null && other.getPerEnc()==null) || 
             (this.perEnc!=null &&
              this.perEnc.equals(other.getPerEnc()))) &&
            ((this.perFre==null && other.getPerFre()==null) || 
             (this.perFre!=null &&
              this.perFre.equals(other.getPerFre()))) &&
            ((this.perIss==null && other.getPerIss()==null) || 
             (this.perIss!=null &&
              this.perIss.equals(other.getPerIss()))) &&
            ((this.perOf1==null && other.getPerOf1()==null) || 
             (this.perOf1!=null &&
              this.perOf1.equals(other.getPerOf1()))) &&
            ((this.perOf2==null && other.getPerOf2()==null) || 
             (this.perOf2!=null &&
              this.perOf2.equals(other.getPerOf2()))) &&
            ((this.perOut==null && other.getPerOut()==null) || 
             (this.perOut!=null &&
              this.perOut.equals(other.getPerOut()))) &&
            ((this.perSeg==null && other.getPerSeg()==null) || 
             (this.perSeg!=null &&
              this.perSeg.equals(other.getPerSeg()))) &&
            ((this.porNa1==null && other.getPorNa1()==null) || 
             (this.porNa1!=null &&
              this.porNa1.equals(other.getPorNa1()))) &&
            ((this.porNa2==null && other.getPorNa2()==null) || 
             (this.porNa2!=null &&
              this.porNa2.equals(other.getPorNa2()))) &&
            ((this.porSi1==null && other.getPorSi1()==null) || 
             (this.porSi1!=null &&
              this.porSi1.equals(other.getPorSi1()))) &&
            ((this.porSi2==null && other.getPorSi2()==null) || 
             (this.porSi2!=null &&
              this.porSi2.equals(other.getPorSi2()))) &&
            ((this.prdDsc==null && other.getPrdDsc()==null) || 
             (this.prdDsc!=null &&
              this.prdDsc.equals(other.getPrdDsc()))) &&
            ((this.przMrt==null && other.getPrzMrt()==null) || 
             (this.przMrt!=null &&
              this.przMrt.equals(other.getPrzMrt()))) &&
            ((this.qdiPrt==null && other.getQdiPrt()==null) || 
             (this.qdiPrt!=null &&
              this.qdiPrt.equals(other.getQdiPrt()))) &&
            ((this.qtdChs==null && other.getQtdChs()==null) || 
             (this.qtdChs!=null &&
              this.qtdChs.equals(other.getQtdChs()))) &&
            ((this.qtdDcv==null && other.getQtdDcv()==null) || 
             (this.qtdDcv!=null &&
              this.qtdDcv.equals(other.getQtdDcv()))) &&
            ((this.qtdMfp==null && other.getQtdMfp()==null) || 
             (this.qtdMfp!=null &&
              this.qtdMfp.equals(other.getQtdMfp()))) &&
            ((this.qtdPgt==null && other.getQtdPgt()==null) || 
             (this.qtdPgt!=null &&
              this.qtdPgt.equals(other.getQtdPgt()))) &&
            ((this.qtdPrt==null && other.getQtdPrt()==null) || 
             (this.qtdPrt!=null &&
              this.qtdPrt.equals(other.getQtdPrt()))) &&
            ((this.qtdRpm==null && other.getQtdRpm()==null) || 
             (this.qtdRpm!=null &&
              this.qtdRpm.equals(other.getQtdRpm()))) &&
            ((this.qtdTpc==null && other.getQtdTpc()==null) || 
             (this.qtdTpc!=null &&
              this.qtdTpc.equals(other.getQtdTpc()))) &&
            ((this.recDtj==null && other.getRecDtj()==null) || 
             (this.recDtj!=null &&
              this.recDtj.equals(other.getRecDtj()))) &&
            ((this.recDtm==null && other.getRecDtm()==null) || 
             (this.recDtm!=null &&
              this.recDtm.equals(other.getRecDtm()))) &&
            ((this.recJmm==null && other.getRecJmm()==null) || 
             (this.recJmm!=null &&
              this.recJmm.equals(other.getRecJmm()))) &&
            ((this.recMul==null && other.getRecMul()==null) || 
             (this.recMul!=null &&
              this.recMul.equals(other.getRecMul()))) &&
            ((this.recTjr==null && other.getRecTjr()==null) || 
             (this.recTjr!=null &&
              this.recTjr.equals(other.getRecTjr()))) &&
            ((this.salCre==null && other.getSalCre()==null) || 
             (this.salCre!=null &&
              this.salCre.equals(other.getSalCre()))) &&
            ((this.salDup==null && other.getSalDup()==null) || 
             (this.salDup!=null &&
              this.salDup.equals(other.getSalDup()))) &&
            ((this.salOut==null && other.getSalOut()==null) || 
             (this.salOut!=null &&
              this.salOut.equals(other.getSalOut()))) &&
            ((this.seqCob==null && other.getSeqCob()==null) || 
             (this.seqCob!=null &&
              this.seqCob.equals(other.getSeqCob()))) &&
            ((this.seqEnt==null && other.getSeqEnt()==null) || 
             (this.seqEnt!=null &&
              this.seqEnt.equals(other.getSeqEnt()))) &&
            ((this.tolDsc==null && other.getTolDsc()==null) || 
             (this.tolDsc!=null &&
              this.tolDsc.equals(other.getTolDsc()))) &&
            ((this.ultNfv==null && other.getUltNfv()==null) || 
             (this.ultNfv!=null &&
              this.ultNfv.equals(other.getUltNfv()))) &&
            ((this.ultSnf==null && other.getUltSnf()==null) || 
             (this.ultSnf!=null &&
              this.ultSnf.equals(other.getUltSnf()))) &&
            ((this.usuAge==null && other.getUsuAge()==null) || 
             (this.usuAge!=null &&
              this.usuAge.equals(other.getUsuAge()))) &&
            ((this.usuAlt==null && other.getUsuAlt()==null) || 
             (this.usuAlt!=null &&
              this.usuAlt.equals(other.getUsuAlt()))) &&
            ((this.usuGer==null && other.getUsuGer()==null) || 
             (this.usuGer!=null &&
              this.usuGer.equals(other.getUsuGer()))) &&
            ((this.vlrAcr==null && other.getVlrAcr()==null) || 
             (this.vlrAcr!=null &&
              this.vlrAcr.equals(other.getVlrAcr()))) &&
            ((this.vlrAtr==null && other.getVlrAtr()==null) || 
             (this.vlrAtr!=null &&
              this.vlrAtr.equals(other.getVlrAtr()))) &&
            ((this.vlrLim==null && other.getVlrLim()==null) || 
             (this.vlrLim!=null &&
              this.vlrLim.equals(other.getVlrLim()))) &&
            ((this.vlrMac==null && other.getVlrMac()==null) || 
             (this.vlrMac!=null &&
              this.vlrMac.equals(other.getVlrMac()))) &&
            ((this.vlrMfa==null && other.getVlrMfa()==null) || 
             (this.vlrMfa!=null &&
              this.vlrMfa.equals(other.getVlrMfa()))) &&
            ((this.vlrPfa==null && other.getVlrPfa()==null) || 
             (this.vlrPfa!=null &&
              this.vlrPfa.equals(other.getVlrPfa()))) &&
            ((this.vlrPrt==null && other.getVlrPrt()==null) || 
             (this.vlrPrt!=null &&
              this.vlrPrt.equals(other.getVlrPrt()))) &&
            ((this.vlrUfa==null && other.getVlrUfa()==null) || 
             (this.vlrUfa!=null &&
              this.vlrUfa.equals(other.getVlrUfa()))) &&
            ((this.vlrUpc==null && other.getVlrUpc()==null) || 
             (this.vlrUpc!=null &&
              this.vlrUpc.equals(other.getVlrUpc()))) &&
            ((this.vlrUpe==null && other.getVlrUpe()==null) || 
             (this.vlrUpe!=null &&
              this.vlrUpe.equals(other.getVlrUpe()))) &&
            ((this.vlrUpg==null && other.getVlrUpg()==null) || 
             (this.vlrUpg!=null &&
              this.vlrUpg.equals(other.getVlrUpg()))) &&
            ((this.volSep==null && other.getVolSep()==null) || 
             (this.volSep!=null &&
              this.volSep.equals(other.getVolSep())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAcePar() != null) {
            _hashCode += getAcePar().hashCode();
        }
        if (getAcrDia() != null) {
            _hashCode += getAcrDia().hashCode();
        }
        if (getAntDsc() != null) {
            _hashCode += getAntDsc().hashCode();
        }
        if (getApmDen() != null) {
            _hashCode += getApmDen().hashCode();
        }
        if (getAvaAti() != null) {
            _hashCode += getAvaAti().hashCode();
        }
        if (getAvaMot() != null) {
            _hashCode += getAvaMot().hashCode();
        }
        if (getAvaObs() != null) {
            _hashCode += getAvaObs().hashCode();
        }
        if (getAvaVlr() != null) {
            _hashCode += getAvaVlr().hashCode();
        }
        if (getAvaVls() != null) {
            _hashCode += getAvaVls().hashCode();
        }
        if (getAvaVlu() != null) {
            _hashCode += getAvaVlu().hashCode();
        }
        if (getAvdAlt() != null) {
            _hashCode += getAvdAlt().hashCode();
        }
        if (getAvdGer() != null) {
            _hashCode += getAvdGer().hashCode();
        }
        if (getAvhAlt() != null) {
            _hashCode += getAvhAlt().hashCode();
        }
        if (getAvhGer() != null) {
            _hashCode += getAvhGer().hashCode();
        }
        if (getAvuAlt() != null) {
            _hashCode += getAvuAlt().hashCode();
        }
        if (getAvuGer() != null) {
            _hashCode += getAvuGer().hashCode();
        }
        if (getCampoUsuarioClienteDefinicoes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCampoUsuarioClienteDefinicoes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCampoUsuarioClienteDefinicoes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCatCli() != null) {
            _hashCode += getCatCli().hashCode();
        }
        if (getCcbCli() != null) {
            _hashCode += getCcbCli().hashCode();
        }
        if (getCifFob() != null) {
            _hashCode += getCifFob().hashCode();
        }
        if (getCodAge() != null) {
            _hashCode += getCodAge().hashCode();
        }
        if (getCodBan() != null) {
            _hashCode += getCodBan().hashCode();
        }
        if (getCodCli() != null) {
            _hashCode += getCodCli().hashCode();
        }
        if (getCodCpg() != null) {
            _hashCode += getCodCpg().hashCode();
        }
        if (getCodCrp() != null) {
            _hashCode += getCodCrp().hashCode();
        }
        if (getCodCrt() != null) {
            _hashCode += getCodCrt().hashCode();
        }
        if (getCodFcr() != null) {
            _hashCode += getCodFcr().hashCode();
        }
        if (getCodFin() != null) {
            _hashCode += getCodFin().hashCode();
        }
        if (getCodFpg() != null) {
            _hashCode += getCodFpg().hashCode();
        }
        if (getCodFrj() != null) {
            _hashCode += getCodFrj().hashCode();
        }
        if (getCodIn1() != null) {
            _hashCode += getCodIn1().hashCode();
        }
        if (getCodIn2() != null) {
            _hashCode += getCodIn2().hashCode();
        }
        if (getCodMar() != null) {
            _hashCode += getCodMar().hashCode();
        }
        if (getCodPdv() != null) {
            _hashCode += getCodPdv().hashCode();
        }
        if (getCodRed() != null) {
            _hashCode += getCodRed().hashCode();
        }
        if (getCodRep() != null) {
            _hashCode += getCodRep().hashCode();
        }
        if (getCodRve() != null) {
            _hashCode += getCodRve().hashCode();
        }
        if (getCodStr() != null) {
            _hashCode += getCodStr().hashCode();
        }
        if (getCodTab() != null) {
            _hashCode += getCodTab().hashCode();
        }
        if (getCodTic() != null) {
            _hashCode += getCodTic().hashCode();
        }
        if (getCodTpr() != null) {
            _hashCode += getCodTpr().hashCode();
        }
        if (getCodTra() != null) {
            _hashCode += getCodTra().hashCode();
        }
        if (getCodTrd() != null) {
            _hashCode += getCodTrd().hashCode();
        }
        if (getCodVen() != null) {
            _hashCode += getCodVen().hashCode();
        }
        if (getConFin() != null) {
            _hashCode += getConFin().hashCode();
        }
        if (getCriEdv() != null) {
            _hashCode += getCriEdv().hashCode();
        }
        if (getCriRat() != null) {
            _hashCode += getCriRat().hashCode();
        }
        if (getCtaAad() != null) {
            _hashCode += getCtaAad().hashCode();
        }
        if (getCtaAux() != null) {
            _hashCode += getCtaAux().hashCode();
        }
        if (getCtaFcr() != null) {
            _hashCode += getCtaFcr().hashCode();
        }
        if (getCtaFdv() != null) {
            _hashCode += getCtaFdv().hashCode();
        }
        if (getCtaRcr() != null) {
            _hashCode += getCtaRcr().hashCode();
        }
        if (getCtaRed() != null) {
            _hashCode += getCtaRed().hashCode();
        }
        if (getCtrPad() != null) {
            _hashCode += getCtrPad().hashCode();
        }
        if (getDatAlt() != null) {
            _hashCode += getDatAlt().hashCode();
        }
        if (getDatAtr() != null) {
            _hashCode += getDatAtr().hashCode();
        }
        if (getDatGer() != null) {
            _hashCode += getDatGer().hashCode();
        }
        if (getDatLim() != null) {
            _hashCode += getDatLim().hashCode();
        }
        if (getDatMac() != null) {
            _hashCode += getDatMac().hashCode();
        }
        if (getDatMfa() != null) {
            _hashCode += getDatMfa().hashCode();
        }
        if (getDatPal() != null) {
            _hashCode += getDatPal().hashCode();
        }
        if (getDatPmr() != null) {
            _hashCode += getDatPmr().hashCode();
        }
        if (getDatUfa() != null) {
            _hashCode += getDatUfa().hashCode();
        }
        if (getDatUpc() != null) {
            _hashCode += getDatUpc().hashCode();
        }
        if (getDatUpe() != null) {
            _hashCode += getDatUpe().hashCode();
        }
        if (getDatUpg() != null) {
            _hashCode += getDatUpg().hashCode();
        }
        if (getDiaEsp() != null) {
            _hashCode += getDiaEsp().hashCode();
        }
        if (getDiaMe1() != null) {
            _hashCode += getDiaMe1().hashCode();
        }
        if (getDiaMe2() != null) {
            _hashCode += getDiaMe2().hashCode();
        }
        if (getDiaMe3() != null) {
            _hashCode += getDiaMe3().hashCode();
        }
        if (getDscAnt() != null) {
            _hashCode += getDscAnt().hashCode();
        }
        if (getDscPon() != null) {
            _hashCode += getDscPon().hashCode();
        }
        if (getDscPrd() != null) {
            _hashCode += getDscPrd().hashCode();
        }
        if (getEcpCnp() != null) {
            _hashCode += getEcpCnp().hashCode();
        }
        if (getEpcPed() != null) {
            _hashCode += getEpcPed().hashCode();
        }
        if (getExiAge() != null) {
            _hashCode += getExiAge().hashCode();
        }
        if (getExiLcp() != null) {
            _hashCode += getExiLcp().hashCode();
        }
        if (getFveCpg() != null) {
            _hashCode += getFveCpg().hashCode();
        }
        if (getFveFpg() != null) {
            _hashCode += getFveFpg().hashCode();
        }
        if (getFveTns() != null) {
            _hashCode += getFveTns().hashCode();
        }
        if (getGerTcc() != null) {
            _hashCode += getGerTcc().hashCode();
        }
        if (getHorAlt() != null) {
            _hashCode += getHorAlt().hashCode();
        }
        if (getHorGer() != null) {
            _hashCode += getHorGer().hashCode();
        }
        if (getHorPal() != null) {
            _hashCode += getHorPal().hashCode();
        }
        if (getIndAgr() != null) {
            _hashCode += getIndAgr().hashCode();
        }
        if (getIndExp() != null) {
            _hashCode += getIndExp().hashCode();
        }
        if (getIndOrf() != null) {
            _hashCode += getIndOrf().hashCode();
        }
        if (getIndPre() != null) {
            _hashCode += getIndPre().hashCode();
        }
        if (getJunPed() != null) {
            _hashCode += getJunPed().hashCode();
        }
        if (getLimApr() != null) {
            _hashCode += getLimApr().hashCode();
        }
        if (getMaiAtr() != null) {
            _hashCode += getMaiAtr().hashCode();
        }
        if (getMedAtr() != null) {
            _hashCode += getMedAtr().hashCode();
        }
        if (getMotDes() != null) {
            _hashCode += getMotDes().hashCode();
        }
        if (getPerAqa() != null) {
            _hashCode += getPerAqa().hashCode();
        }
        if (getPerCcr() != null) {
            _hashCode += getPerCcr().hashCode();
        }
        if (getPerCom() != null) {
            _hashCode += getPerCom().hashCode();
        }
        if (getPerDif() != null) {
            _hashCode += getPerDif().hashCode();
        }
        if (getPerDs1() != null) {
            _hashCode += getPerDs1().hashCode();
        }
        if (getPerDs2() != null) {
            _hashCode += getPerDs2().hashCode();
        }
        if (getPerDs3() != null) {
            _hashCode += getPerDs3().hashCode();
        }
        if (getPerDs4() != null) {
            _hashCode += getPerDs4().hashCode();
        }
        if (getPerDs5() != null) {
            _hashCode += getPerDs5().hashCode();
        }
        if (getPerDsc() != null) {
            _hashCode += getPerDsc().hashCode();
        }
        if (getPerEmb() != null) {
            _hashCode += getPerEmb().hashCode();
        }
        if (getPerEnc() != null) {
            _hashCode += getPerEnc().hashCode();
        }
        if (getPerFre() != null) {
            _hashCode += getPerFre().hashCode();
        }
        if (getPerIss() != null) {
            _hashCode += getPerIss().hashCode();
        }
        if (getPerOf1() != null) {
            _hashCode += getPerOf1().hashCode();
        }
        if (getPerOf2() != null) {
            _hashCode += getPerOf2().hashCode();
        }
        if (getPerOut() != null) {
            _hashCode += getPerOut().hashCode();
        }
        if (getPerSeg() != null) {
            _hashCode += getPerSeg().hashCode();
        }
        if (getPorNa1() != null) {
            _hashCode += getPorNa1().hashCode();
        }
        if (getPorNa2() != null) {
            _hashCode += getPorNa2().hashCode();
        }
        if (getPorSi1() != null) {
            _hashCode += getPorSi1().hashCode();
        }
        if (getPorSi2() != null) {
            _hashCode += getPorSi2().hashCode();
        }
        if (getPrdDsc() != null) {
            _hashCode += getPrdDsc().hashCode();
        }
        if (getPrzMrt() != null) {
            _hashCode += getPrzMrt().hashCode();
        }
        if (getQdiPrt() != null) {
            _hashCode += getQdiPrt().hashCode();
        }
        if (getQtdChs() != null) {
            _hashCode += getQtdChs().hashCode();
        }
        if (getQtdDcv() != null) {
            _hashCode += getQtdDcv().hashCode();
        }
        if (getQtdMfp() != null) {
            _hashCode += getQtdMfp().hashCode();
        }
        if (getQtdPgt() != null) {
            _hashCode += getQtdPgt().hashCode();
        }
        if (getQtdPrt() != null) {
            _hashCode += getQtdPrt().hashCode();
        }
        if (getQtdRpm() != null) {
            _hashCode += getQtdRpm().hashCode();
        }
        if (getQtdTpc() != null) {
            _hashCode += getQtdTpc().hashCode();
        }
        if (getRecDtj() != null) {
            _hashCode += getRecDtj().hashCode();
        }
        if (getRecDtm() != null) {
            _hashCode += getRecDtm().hashCode();
        }
        if (getRecJmm() != null) {
            _hashCode += getRecJmm().hashCode();
        }
        if (getRecMul() != null) {
            _hashCode += getRecMul().hashCode();
        }
        if (getRecTjr() != null) {
            _hashCode += getRecTjr().hashCode();
        }
        if (getSalCre() != null) {
            _hashCode += getSalCre().hashCode();
        }
        if (getSalDup() != null) {
            _hashCode += getSalDup().hashCode();
        }
        if (getSalOut() != null) {
            _hashCode += getSalOut().hashCode();
        }
        if (getSeqCob() != null) {
            _hashCode += getSeqCob().hashCode();
        }
        if (getSeqEnt() != null) {
            _hashCode += getSeqEnt().hashCode();
        }
        if (getTolDsc() != null) {
            _hashCode += getTolDsc().hashCode();
        }
        if (getUltNfv() != null) {
            _hashCode += getUltNfv().hashCode();
        }
        if (getUltSnf() != null) {
            _hashCode += getUltSnf().hashCode();
        }
        if (getUsuAge() != null) {
            _hashCode += getUsuAge().hashCode();
        }
        if (getUsuAlt() != null) {
            _hashCode += getUsuAlt().hashCode();
        }
        if (getUsuGer() != null) {
            _hashCode += getUsuGer().hashCode();
        }
        if (getVlrAcr() != null) {
            _hashCode += getVlrAcr().hashCode();
        }
        if (getVlrAtr() != null) {
            _hashCode += getVlrAtr().hashCode();
        }
        if (getVlrLim() != null) {
            _hashCode += getVlrLim().hashCode();
        }
        if (getVlrMac() != null) {
            _hashCode += getVlrMac().hashCode();
        }
        if (getVlrMfa() != null) {
            _hashCode += getVlrMfa().hashCode();
        }
        if (getVlrPfa() != null) {
            _hashCode += getVlrPfa().hashCode();
        }
        if (getVlrPrt() != null) {
            _hashCode += getVlrPrt().hashCode();
        }
        if (getVlrUfa() != null) {
            _hashCode += getVlrUfa().hashCode();
        }
        if (getVlrUpc() != null) {
            _hashCode += getVlrUpc().hashCode();
        }
        if (getVlrUpe() != null) {
            _hashCode += getVlrUpe().hashCode();
        }
        if (getVlrUpg() != null) {
            _hashCode += getVlrUpg().hashCode();
        }
        if (getVolSep() != null) {
            _hashCode += getVolSep().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientesExportarOutClienteHistorico.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesExportarOutClienteHistorico"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("acePar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "acePar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("acrDia");
        elemField.setXmlName(new javax.xml.namespace.QName("", "acrDia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("antDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "antDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("apmDen");
        elemField.setXmlName(new javax.xml.namespace.QName("", "apmDen"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avaAti");
        elemField.setXmlName(new javax.xml.namespace.QName("", "avaAti"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avaMot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "avaMot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avaObs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "avaObs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avaVlr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "avaVlr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avaVls");
        elemField.setXmlName(new javax.xml.namespace.QName("", "avaVls"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avaVlu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "avaVlu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avdAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "avdAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avdGer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "avdGer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avhAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "avhAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avhGer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "avhGer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avuAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "avuAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avuGer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "avuGer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("campoUsuarioClienteDefinicoes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "campoUsuarioClienteDefinicoes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesExportarOutClienteHistoricoCampoUsuarioClienteDefinicoes"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("catCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "catCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ccbCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ccbCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cifFob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cifFob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codAge");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codAge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCpg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCpg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCrp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCrp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCrt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCrt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFcr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFcr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFpg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFpg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFrj");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFrj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codIn1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codIn1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codIn2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codIn2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPdv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPdv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRed");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRep");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRep"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRve");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRve"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codStr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codStr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTab");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTab"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTic");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTic"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTpr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTpr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTra");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTrd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTrd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codVen");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codVen"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conFin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "conFin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("criEdv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "criEdv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("criRat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "criRat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaAad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaAad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaAux");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaAux"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaFcr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaFcr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaFdv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaFdv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaRcr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaRcr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaRed");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaRed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctrPad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctrPad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAtr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAtr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datGer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datGer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datLim");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datLim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datMac");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datMac"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datMfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datMfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datPal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datPal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datPmr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datPmr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datUfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datUfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datUpc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datUpc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datUpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datUpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datUpg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datUpg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diaEsp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "diaEsp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diaMe1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "diaMe1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diaMe2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "diaMe2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diaMe3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "diaMe3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dscAnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dscAnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dscPon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dscPon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dscPrd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dscPrd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ecpCnp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ecpCnp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("epcPed");
        elemField.setXmlName(new javax.xml.namespace.QName("", "epcPed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("exiAge");
        elemField.setXmlName(new javax.xml.namespace.QName("", "exiAge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("exiLcp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "exiLcp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fveCpg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fveCpg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fveFpg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fveFpg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fveTns");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fveTns"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gerTcc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "gerTcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horGer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horGer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horPal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horPal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indAgr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indAgr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indExp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indExp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indOrf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indOrf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indPre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indPre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("junPed");
        elemField.setXmlName(new javax.xml.namespace.QName("", "junPed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limApr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "limApr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maiAtr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "maiAtr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("medAtr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "medAtr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("motDes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "motDes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perAqa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perAqa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perCcr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perCcr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perCom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perCom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDif");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDif"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDs1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDs1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDs2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDs2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDs3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDs3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDs4");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDs4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDs5");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDs5"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perEmb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perEmb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perEnc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perEnc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perFre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perFre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perIss");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perIss"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perOf1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perOf1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perOf2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perOf2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perOut");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perOut"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perSeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perSeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("porNa1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "porNa1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("porNa2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "porNa2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("porSi1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "porSi1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("porSi2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "porSi2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prdDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prdDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("przMrt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "przMrt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qdiPrt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qdiPrt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qtdChs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qtdChs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qtdDcv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qtdDcv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qtdMfp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qtdMfp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qtdPgt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qtdPgt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qtdPrt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qtdPrt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qtdRpm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qtdRpm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qtdTpc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qtdTpc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recDtj");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recDtj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recDtm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recDtm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recJmm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recJmm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recMul");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recMul"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recTjr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recTjr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("salCre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "salCre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("salDup");
        elemField.setXmlName(new javax.xml.namespace.QName("", "salDup"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("salOut");
        elemField.setXmlName(new javax.xml.namespace.QName("", "salOut"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tolDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tolDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ultNfv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ultNfv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ultSnf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ultSnf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuAge");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuAge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuGer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuGer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrAcr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrAcr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrAtr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrAtr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrLim");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrLim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrMac");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrMac"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrMfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrMfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrPfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrPfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrPrt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrPrt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrUfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrUfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrUpc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrUpc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrUpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrUpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrUpg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrUpg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("volSep");
        elemField.setXmlName(new javax.xml.namespace.QName("", "volSep"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
