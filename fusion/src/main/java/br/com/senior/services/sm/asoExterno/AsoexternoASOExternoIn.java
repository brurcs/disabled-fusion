/**
 * AsoexternoASOExternoIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.sm.asoExterno;

public class AsoexternoASOExternoIn  implements java.io.Serializable {
    private java.lang.Integer codAte;

    private java.lang.Integer codFic;

    private java.lang.Integer codPar;

    private java.lang.String datAte;

    private java.lang.String desAte;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.Integer numAte;

    private java.lang.Integer numEmp;

    private java.lang.String obsAte;

    private java.lang.Integer tipAso;

    private java.lang.String tipOpe;

    public AsoexternoASOExternoIn() {
    }

    public AsoexternoASOExternoIn(
           java.lang.Integer codAte,
           java.lang.Integer codFic,
           java.lang.Integer codPar,
           java.lang.String datAte,
           java.lang.String desAte,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.Integer numAte,
           java.lang.Integer numEmp,
           java.lang.String obsAte,
           java.lang.Integer tipAso,
           java.lang.String tipOpe) {
           this.codAte = codAte;
           this.codFic = codFic;
           this.codPar = codPar;
           this.datAte = datAte;
           this.desAte = desAte;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.numAte = numAte;
           this.numEmp = numEmp;
           this.obsAte = obsAte;
           this.tipAso = tipAso;
           this.tipOpe = tipOpe;
    }


    /**
     * Gets the codAte value for this AsoexternoASOExternoIn.
     * 
     * @return codAte
     */
    public java.lang.Integer getCodAte() {
        return codAte;
    }


    /**
     * Sets the codAte value for this AsoexternoASOExternoIn.
     * 
     * @param codAte
     */
    public void setCodAte(java.lang.Integer codAte) {
        this.codAte = codAte;
    }


    /**
     * Gets the codFic value for this AsoexternoASOExternoIn.
     * 
     * @return codFic
     */
    public java.lang.Integer getCodFic() {
        return codFic;
    }


    /**
     * Sets the codFic value for this AsoexternoASOExternoIn.
     * 
     * @param codFic
     */
    public void setCodFic(java.lang.Integer codFic) {
        this.codFic = codFic;
    }


    /**
     * Gets the codPar value for this AsoexternoASOExternoIn.
     * 
     * @return codPar
     */
    public java.lang.Integer getCodPar() {
        return codPar;
    }


    /**
     * Sets the codPar value for this AsoexternoASOExternoIn.
     * 
     * @param codPar
     */
    public void setCodPar(java.lang.Integer codPar) {
        this.codPar = codPar;
    }


    /**
     * Gets the datAte value for this AsoexternoASOExternoIn.
     * 
     * @return datAte
     */
    public java.lang.String getDatAte() {
        return datAte;
    }


    /**
     * Sets the datAte value for this AsoexternoASOExternoIn.
     * 
     * @param datAte
     */
    public void setDatAte(java.lang.String datAte) {
        this.datAte = datAte;
    }


    /**
     * Gets the desAte value for this AsoexternoASOExternoIn.
     * 
     * @return desAte
     */
    public java.lang.String getDesAte() {
        return desAte;
    }


    /**
     * Sets the desAte value for this AsoexternoASOExternoIn.
     * 
     * @param desAte
     */
    public void setDesAte(java.lang.String desAte) {
        this.desAte = desAte;
    }


    /**
     * Gets the flowInstanceID value for this AsoexternoASOExternoIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this AsoexternoASOExternoIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this AsoexternoASOExternoIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this AsoexternoASOExternoIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the numAte value for this AsoexternoASOExternoIn.
     * 
     * @return numAte
     */
    public java.lang.Integer getNumAte() {
        return numAte;
    }


    /**
     * Sets the numAte value for this AsoexternoASOExternoIn.
     * 
     * @param numAte
     */
    public void setNumAte(java.lang.Integer numAte) {
        this.numAte = numAte;
    }


    /**
     * Gets the numEmp value for this AsoexternoASOExternoIn.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this AsoexternoASOExternoIn.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the obsAte value for this AsoexternoASOExternoIn.
     * 
     * @return obsAte
     */
    public java.lang.String getObsAte() {
        return obsAte;
    }


    /**
     * Sets the obsAte value for this AsoexternoASOExternoIn.
     * 
     * @param obsAte
     */
    public void setObsAte(java.lang.String obsAte) {
        this.obsAte = obsAte;
    }


    /**
     * Gets the tipAso value for this AsoexternoASOExternoIn.
     * 
     * @return tipAso
     */
    public java.lang.Integer getTipAso() {
        return tipAso;
    }


    /**
     * Sets the tipAso value for this AsoexternoASOExternoIn.
     * 
     * @param tipAso
     */
    public void setTipAso(java.lang.Integer tipAso) {
        this.tipAso = tipAso;
    }


    /**
     * Gets the tipOpe value for this AsoexternoASOExternoIn.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this AsoexternoASOExternoIn.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AsoexternoASOExternoIn)) return false;
        AsoexternoASOExternoIn other = (AsoexternoASOExternoIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codAte==null && other.getCodAte()==null) || 
             (this.codAte!=null &&
              this.codAte.equals(other.getCodAte()))) &&
            ((this.codFic==null && other.getCodFic()==null) || 
             (this.codFic!=null &&
              this.codFic.equals(other.getCodFic()))) &&
            ((this.codPar==null && other.getCodPar()==null) || 
             (this.codPar!=null &&
              this.codPar.equals(other.getCodPar()))) &&
            ((this.datAte==null && other.getDatAte()==null) || 
             (this.datAte!=null &&
              this.datAte.equals(other.getDatAte()))) &&
            ((this.desAte==null && other.getDesAte()==null) || 
             (this.desAte!=null &&
              this.desAte.equals(other.getDesAte()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.numAte==null && other.getNumAte()==null) || 
             (this.numAte!=null &&
              this.numAte.equals(other.getNumAte()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.obsAte==null && other.getObsAte()==null) || 
             (this.obsAte!=null &&
              this.obsAte.equals(other.getObsAte()))) &&
            ((this.tipAso==null && other.getTipAso()==null) || 
             (this.tipAso!=null &&
              this.tipAso.equals(other.getTipAso()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodAte() != null) {
            _hashCode += getCodAte().hashCode();
        }
        if (getCodFic() != null) {
            _hashCode += getCodFic().hashCode();
        }
        if (getCodPar() != null) {
            _hashCode += getCodPar().hashCode();
        }
        if (getDatAte() != null) {
            _hashCode += getDatAte().hashCode();
        }
        if (getDesAte() != null) {
            _hashCode += getDesAte().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getNumAte() != null) {
            _hashCode += getNumAte().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getObsAte() != null) {
            _hashCode += getObsAte().hashCode();
        }
        if (getTipAso() != null) {
            _hashCode += getTipAso().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AsoexternoASOExternoIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "asoexternoASOExternoIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codAte");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codAte"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFic");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFic"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAte");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAte"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("desAte");
        elemField.setXmlName(new javax.xml.namespace.QName("", "desAte"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numAte");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numAte"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsAte");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsAte"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipAso");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipAso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
