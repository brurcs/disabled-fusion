/**
 * HistoricosAfastamento2In.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.historico;

public class HistoricosAfastamento2In  implements java.io.Serializable {
    private java.lang.Integer aciTra;

    private java.lang.Integer cauDem;

    private java.lang.String cgcSuc;

    private java.lang.Integer codAte;

    private java.lang.String codDoe;

    private java.lang.Integer codOem;

    private java.lang.String codSub;

    private java.lang.String conTov;

    private java.lang.String datAfa;

    private java.lang.String datAlt;

    private java.lang.String datNex;

    private java.lang.String datPar;

    private java.lang.Integer datPer;

    private java.lang.String datTer;

    private java.lang.Integer diaJus;

    private java.lang.Integer diaPrv;

    private java.lang.String estCon;

    private java.lang.String exmRet;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String horAfa;

    private java.lang.String horTer;

    private java.lang.Integer motRai;

    private java.lang.String msmMot;

    private java.lang.String nomAte;

    private java.lang.String nroAut;

    private java.lang.Integer numCad;

    private java.lang.Integer numEmp;

    private java.lang.String numPro;

    private java.lang.String obsAfa;

    private java.lang.Integer orgCla;

    private java.lang.String oriMot;

    private java.lang.String prvTer;

    private java.lang.String qhrAfa;

    private java.lang.String regCon;

    private java.lang.Integer risNex;

    private java.lang.Integer sitAfa;

    private java.lang.Integer sitIni;

    private java.lang.Integer tipCol;

    private java.lang.String tipOpe;

    public HistoricosAfastamento2In() {
    }

    public HistoricosAfastamento2In(
           java.lang.Integer aciTra,
           java.lang.Integer cauDem,
           java.lang.String cgcSuc,
           java.lang.Integer codAte,
           java.lang.String codDoe,
           java.lang.Integer codOem,
           java.lang.String codSub,
           java.lang.String conTov,
           java.lang.String datAfa,
           java.lang.String datAlt,
           java.lang.String datNex,
           java.lang.String datPar,
           java.lang.Integer datPer,
           java.lang.String datTer,
           java.lang.Integer diaJus,
           java.lang.Integer diaPrv,
           java.lang.String estCon,
           java.lang.String exmRet,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String horAfa,
           java.lang.String horTer,
           java.lang.Integer motRai,
           java.lang.String msmMot,
           java.lang.String nomAte,
           java.lang.String nroAut,
           java.lang.Integer numCad,
           java.lang.Integer numEmp,
           java.lang.String numPro,
           java.lang.String obsAfa,
           java.lang.Integer orgCla,
           java.lang.String oriMot,
           java.lang.String prvTer,
           java.lang.String qhrAfa,
           java.lang.String regCon,
           java.lang.Integer risNex,
           java.lang.Integer sitAfa,
           java.lang.Integer sitIni,
           java.lang.Integer tipCol,
           java.lang.String tipOpe) {
           this.aciTra = aciTra;
           this.cauDem = cauDem;
           this.cgcSuc = cgcSuc;
           this.codAte = codAte;
           this.codDoe = codDoe;
           this.codOem = codOem;
           this.codSub = codSub;
           this.conTov = conTov;
           this.datAfa = datAfa;
           this.datAlt = datAlt;
           this.datNex = datNex;
           this.datPar = datPar;
           this.datPer = datPer;
           this.datTer = datTer;
           this.diaJus = diaJus;
           this.diaPrv = diaPrv;
           this.estCon = estCon;
           this.exmRet = exmRet;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.horAfa = horAfa;
           this.horTer = horTer;
           this.motRai = motRai;
           this.msmMot = msmMot;
           this.nomAte = nomAte;
           this.nroAut = nroAut;
           this.numCad = numCad;
           this.numEmp = numEmp;
           this.numPro = numPro;
           this.obsAfa = obsAfa;
           this.orgCla = orgCla;
           this.oriMot = oriMot;
           this.prvTer = prvTer;
           this.qhrAfa = qhrAfa;
           this.regCon = regCon;
           this.risNex = risNex;
           this.sitAfa = sitAfa;
           this.sitIni = sitIni;
           this.tipCol = tipCol;
           this.tipOpe = tipOpe;
    }


    /**
     * Gets the aciTra value for this HistoricosAfastamento2In.
     * 
     * @return aciTra
     */
    public java.lang.Integer getAciTra() {
        return aciTra;
    }


    /**
     * Sets the aciTra value for this HistoricosAfastamento2In.
     * 
     * @param aciTra
     */
    public void setAciTra(java.lang.Integer aciTra) {
        this.aciTra = aciTra;
    }


    /**
     * Gets the cauDem value for this HistoricosAfastamento2In.
     * 
     * @return cauDem
     */
    public java.lang.Integer getCauDem() {
        return cauDem;
    }


    /**
     * Sets the cauDem value for this HistoricosAfastamento2In.
     * 
     * @param cauDem
     */
    public void setCauDem(java.lang.Integer cauDem) {
        this.cauDem = cauDem;
    }


    /**
     * Gets the cgcSuc value for this HistoricosAfastamento2In.
     * 
     * @return cgcSuc
     */
    public java.lang.String getCgcSuc() {
        return cgcSuc;
    }


    /**
     * Sets the cgcSuc value for this HistoricosAfastamento2In.
     * 
     * @param cgcSuc
     */
    public void setCgcSuc(java.lang.String cgcSuc) {
        this.cgcSuc = cgcSuc;
    }


    /**
     * Gets the codAte value for this HistoricosAfastamento2In.
     * 
     * @return codAte
     */
    public java.lang.Integer getCodAte() {
        return codAte;
    }


    /**
     * Sets the codAte value for this HistoricosAfastamento2In.
     * 
     * @param codAte
     */
    public void setCodAte(java.lang.Integer codAte) {
        this.codAte = codAte;
    }


    /**
     * Gets the codDoe value for this HistoricosAfastamento2In.
     * 
     * @return codDoe
     */
    public java.lang.String getCodDoe() {
        return codDoe;
    }


    /**
     * Sets the codDoe value for this HistoricosAfastamento2In.
     * 
     * @param codDoe
     */
    public void setCodDoe(java.lang.String codDoe) {
        this.codDoe = codDoe;
    }


    /**
     * Gets the codOem value for this HistoricosAfastamento2In.
     * 
     * @return codOem
     */
    public java.lang.Integer getCodOem() {
        return codOem;
    }


    /**
     * Sets the codOem value for this HistoricosAfastamento2In.
     * 
     * @param codOem
     */
    public void setCodOem(java.lang.Integer codOem) {
        this.codOem = codOem;
    }


    /**
     * Gets the codSub value for this HistoricosAfastamento2In.
     * 
     * @return codSub
     */
    public java.lang.String getCodSub() {
        return codSub;
    }


    /**
     * Sets the codSub value for this HistoricosAfastamento2In.
     * 
     * @param codSub
     */
    public void setCodSub(java.lang.String codSub) {
        this.codSub = codSub;
    }


    /**
     * Gets the conTov value for this HistoricosAfastamento2In.
     * 
     * @return conTov
     */
    public java.lang.String getConTov() {
        return conTov;
    }


    /**
     * Sets the conTov value for this HistoricosAfastamento2In.
     * 
     * @param conTov
     */
    public void setConTov(java.lang.String conTov) {
        this.conTov = conTov;
    }


    /**
     * Gets the datAfa value for this HistoricosAfastamento2In.
     * 
     * @return datAfa
     */
    public java.lang.String getDatAfa() {
        return datAfa;
    }


    /**
     * Sets the datAfa value for this HistoricosAfastamento2In.
     * 
     * @param datAfa
     */
    public void setDatAfa(java.lang.String datAfa) {
        this.datAfa = datAfa;
    }


    /**
     * Gets the datAlt value for this HistoricosAfastamento2In.
     * 
     * @return datAlt
     */
    public java.lang.String getDatAlt() {
        return datAlt;
    }


    /**
     * Sets the datAlt value for this HistoricosAfastamento2In.
     * 
     * @param datAlt
     */
    public void setDatAlt(java.lang.String datAlt) {
        this.datAlt = datAlt;
    }


    /**
     * Gets the datNex value for this HistoricosAfastamento2In.
     * 
     * @return datNex
     */
    public java.lang.String getDatNex() {
        return datNex;
    }


    /**
     * Sets the datNex value for this HistoricosAfastamento2In.
     * 
     * @param datNex
     */
    public void setDatNex(java.lang.String datNex) {
        this.datNex = datNex;
    }


    /**
     * Gets the datPar value for this HistoricosAfastamento2In.
     * 
     * @return datPar
     */
    public java.lang.String getDatPar() {
        return datPar;
    }


    /**
     * Sets the datPar value for this HistoricosAfastamento2In.
     * 
     * @param datPar
     */
    public void setDatPar(java.lang.String datPar) {
        this.datPar = datPar;
    }


    /**
     * Gets the datPer value for this HistoricosAfastamento2In.
     * 
     * @return datPer
     */
    public java.lang.Integer getDatPer() {
        return datPer;
    }


    /**
     * Sets the datPer value for this HistoricosAfastamento2In.
     * 
     * @param datPer
     */
    public void setDatPer(java.lang.Integer datPer) {
        this.datPer = datPer;
    }


    /**
     * Gets the datTer value for this HistoricosAfastamento2In.
     * 
     * @return datTer
     */
    public java.lang.String getDatTer() {
        return datTer;
    }


    /**
     * Sets the datTer value for this HistoricosAfastamento2In.
     * 
     * @param datTer
     */
    public void setDatTer(java.lang.String datTer) {
        this.datTer = datTer;
    }


    /**
     * Gets the diaJus value for this HistoricosAfastamento2In.
     * 
     * @return diaJus
     */
    public java.lang.Integer getDiaJus() {
        return diaJus;
    }


    /**
     * Sets the diaJus value for this HistoricosAfastamento2In.
     * 
     * @param diaJus
     */
    public void setDiaJus(java.lang.Integer diaJus) {
        this.diaJus = diaJus;
    }


    /**
     * Gets the diaPrv value for this HistoricosAfastamento2In.
     * 
     * @return diaPrv
     */
    public java.lang.Integer getDiaPrv() {
        return diaPrv;
    }


    /**
     * Sets the diaPrv value for this HistoricosAfastamento2In.
     * 
     * @param diaPrv
     */
    public void setDiaPrv(java.lang.Integer diaPrv) {
        this.diaPrv = diaPrv;
    }


    /**
     * Gets the estCon value for this HistoricosAfastamento2In.
     * 
     * @return estCon
     */
    public java.lang.String getEstCon() {
        return estCon;
    }


    /**
     * Sets the estCon value for this HistoricosAfastamento2In.
     * 
     * @param estCon
     */
    public void setEstCon(java.lang.String estCon) {
        this.estCon = estCon;
    }


    /**
     * Gets the exmRet value for this HistoricosAfastamento2In.
     * 
     * @return exmRet
     */
    public java.lang.String getExmRet() {
        return exmRet;
    }


    /**
     * Sets the exmRet value for this HistoricosAfastamento2In.
     * 
     * @param exmRet
     */
    public void setExmRet(java.lang.String exmRet) {
        this.exmRet = exmRet;
    }


    /**
     * Gets the flowInstanceID value for this HistoricosAfastamento2In.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this HistoricosAfastamento2In.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this HistoricosAfastamento2In.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this HistoricosAfastamento2In.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the horAfa value for this HistoricosAfastamento2In.
     * 
     * @return horAfa
     */
    public java.lang.String getHorAfa() {
        return horAfa;
    }


    /**
     * Sets the horAfa value for this HistoricosAfastamento2In.
     * 
     * @param horAfa
     */
    public void setHorAfa(java.lang.String horAfa) {
        this.horAfa = horAfa;
    }


    /**
     * Gets the horTer value for this HistoricosAfastamento2In.
     * 
     * @return horTer
     */
    public java.lang.String getHorTer() {
        return horTer;
    }


    /**
     * Sets the horTer value for this HistoricosAfastamento2In.
     * 
     * @param horTer
     */
    public void setHorTer(java.lang.String horTer) {
        this.horTer = horTer;
    }


    /**
     * Gets the motRai value for this HistoricosAfastamento2In.
     * 
     * @return motRai
     */
    public java.lang.Integer getMotRai() {
        return motRai;
    }


    /**
     * Sets the motRai value for this HistoricosAfastamento2In.
     * 
     * @param motRai
     */
    public void setMotRai(java.lang.Integer motRai) {
        this.motRai = motRai;
    }


    /**
     * Gets the msmMot value for this HistoricosAfastamento2In.
     * 
     * @return msmMot
     */
    public java.lang.String getMsmMot() {
        return msmMot;
    }


    /**
     * Sets the msmMot value for this HistoricosAfastamento2In.
     * 
     * @param msmMot
     */
    public void setMsmMot(java.lang.String msmMot) {
        this.msmMot = msmMot;
    }


    /**
     * Gets the nomAte value for this HistoricosAfastamento2In.
     * 
     * @return nomAte
     */
    public java.lang.String getNomAte() {
        return nomAte;
    }


    /**
     * Sets the nomAte value for this HistoricosAfastamento2In.
     * 
     * @param nomAte
     */
    public void setNomAte(java.lang.String nomAte) {
        this.nomAte = nomAte;
    }


    /**
     * Gets the nroAut value for this HistoricosAfastamento2In.
     * 
     * @return nroAut
     */
    public java.lang.String getNroAut() {
        return nroAut;
    }


    /**
     * Sets the nroAut value for this HistoricosAfastamento2In.
     * 
     * @param nroAut
     */
    public void setNroAut(java.lang.String nroAut) {
        this.nroAut = nroAut;
    }


    /**
     * Gets the numCad value for this HistoricosAfastamento2In.
     * 
     * @return numCad
     */
    public java.lang.Integer getNumCad() {
        return numCad;
    }


    /**
     * Sets the numCad value for this HistoricosAfastamento2In.
     * 
     * @param numCad
     */
    public void setNumCad(java.lang.Integer numCad) {
        this.numCad = numCad;
    }


    /**
     * Gets the numEmp value for this HistoricosAfastamento2In.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this HistoricosAfastamento2In.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the numPro value for this HistoricosAfastamento2In.
     * 
     * @return numPro
     */
    public java.lang.String getNumPro() {
        return numPro;
    }


    /**
     * Sets the numPro value for this HistoricosAfastamento2In.
     * 
     * @param numPro
     */
    public void setNumPro(java.lang.String numPro) {
        this.numPro = numPro;
    }


    /**
     * Gets the obsAfa value for this HistoricosAfastamento2In.
     * 
     * @return obsAfa
     */
    public java.lang.String getObsAfa() {
        return obsAfa;
    }


    /**
     * Sets the obsAfa value for this HistoricosAfastamento2In.
     * 
     * @param obsAfa
     */
    public void setObsAfa(java.lang.String obsAfa) {
        this.obsAfa = obsAfa;
    }


    /**
     * Gets the orgCla value for this HistoricosAfastamento2In.
     * 
     * @return orgCla
     */
    public java.lang.Integer getOrgCla() {
        return orgCla;
    }


    /**
     * Sets the orgCla value for this HistoricosAfastamento2In.
     * 
     * @param orgCla
     */
    public void setOrgCla(java.lang.Integer orgCla) {
        this.orgCla = orgCla;
    }


    /**
     * Gets the oriMot value for this HistoricosAfastamento2In.
     * 
     * @return oriMot
     */
    public java.lang.String getOriMot() {
        return oriMot;
    }


    /**
     * Sets the oriMot value for this HistoricosAfastamento2In.
     * 
     * @param oriMot
     */
    public void setOriMot(java.lang.String oriMot) {
        this.oriMot = oriMot;
    }


    /**
     * Gets the prvTer value for this HistoricosAfastamento2In.
     * 
     * @return prvTer
     */
    public java.lang.String getPrvTer() {
        return prvTer;
    }


    /**
     * Sets the prvTer value for this HistoricosAfastamento2In.
     * 
     * @param prvTer
     */
    public void setPrvTer(java.lang.String prvTer) {
        this.prvTer = prvTer;
    }


    /**
     * Gets the qhrAfa value for this HistoricosAfastamento2In.
     * 
     * @return qhrAfa
     */
    public java.lang.String getQhrAfa() {
        return qhrAfa;
    }


    /**
     * Sets the qhrAfa value for this HistoricosAfastamento2In.
     * 
     * @param qhrAfa
     */
    public void setQhrAfa(java.lang.String qhrAfa) {
        this.qhrAfa = qhrAfa;
    }


    /**
     * Gets the regCon value for this HistoricosAfastamento2In.
     * 
     * @return regCon
     */
    public java.lang.String getRegCon() {
        return regCon;
    }


    /**
     * Sets the regCon value for this HistoricosAfastamento2In.
     * 
     * @param regCon
     */
    public void setRegCon(java.lang.String regCon) {
        this.regCon = regCon;
    }


    /**
     * Gets the risNex value for this HistoricosAfastamento2In.
     * 
     * @return risNex
     */
    public java.lang.Integer getRisNex() {
        return risNex;
    }


    /**
     * Sets the risNex value for this HistoricosAfastamento2In.
     * 
     * @param risNex
     */
    public void setRisNex(java.lang.Integer risNex) {
        this.risNex = risNex;
    }


    /**
     * Gets the sitAfa value for this HistoricosAfastamento2In.
     * 
     * @return sitAfa
     */
    public java.lang.Integer getSitAfa() {
        return sitAfa;
    }


    /**
     * Sets the sitAfa value for this HistoricosAfastamento2In.
     * 
     * @param sitAfa
     */
    public void setSitAfa(java.lang.Integer sitAfa) {
        this.sitAfa = sitAfa;
    }


    /**
     * Gets the sitIni value for this HistoricosAfastamento2In.
     * 
     * @return sitIni
     */
    public java.lang.Integer getSitIni() {
        return sitIni;
    }


    /**
     * Sets the sitIni value for this HistoricosAfastamento2In.
     * 
     * @param sitIni
     */
    public void setSitIni(java.lang.Integer sitIni) {
        this.sitIni = sitIni;
    }


    /**
     * Gets the tipCol value for this HistoricosAfastamento2In.
     * 
     * @return tipCol
     */
    public java.lang.Integer getTipCol() {
        return tipCol;
    }


    /**
     * Sets the tipCol value for this HistoricosAfastamento2In.
     * 
     * @param tipCol
     */
    public void setTipCol(java.lang.Integer tipCol) {
        this.tipCol = tipCol;
    }


    /**
     * Gets the tipOpe value for this HistoricosAfastamento2In.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this HistoricosAfastamento2In.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HistoricosAfastamento2In)) return false;
        HistoricosAfastamento2In other = (HistoricosAfastamento2In) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.aciTra==null && other.getAciTra()==null) || 
             (this.aciTra!=null &&
              this.aciTra.equals(other.getAciTra()))) &&
            ((this.cauDem==null && other.getCauDem()==null) || 
             (this.cauDem!=null &&
              this.cauDem.equals(other.getCauDem()))) &&
            ((this.cgcSuc==null && other.getCgcSuc()==null) || 
             (this.cgcSuc!=null &&
              this.cgcSuc.equals(other.getCgcSuc()))) &&
            ((this.codAte==null && other.getCodAte()==null) || 
             (this.codAte!=null &&
              this.codAte.equals(other.getCodAte()))) &&
            ((this.codDoe==null && other.getCodDoe()==null) || 
             (this.codDoe!=null &&
              this.codDoe.equals(other.getCodDoe()))) &&
            ((this.codOem==null && other.getCodOem()==null) || 
             (this.codOem!=null &&
              this.codOem.equals(other.getCodOem()))) &&
            ((this.codSub==null && other.getCodSub()==null) || 
             (this.codSub!=null &&
              this.codSub.equals(other.getCodSub()))) &&
            ((this.conTov==null && other.getConTov()==null) || 
             (this.conTov!=null &&
              this.conTov.equals(other.getConTov()))) &&
            ((this.datAfa==null && other.getDatAfa()==null) || 
             (this.datAfa!=null &&
              this.datAfa.equals(other.getDatAfa()))) &&
            ((this.datAlt==null && other.getDatAlt()==null) || 
             (this.datAlt!=null &&
              this.datAlt.equals(other.getDatAlt()))) &&
            ((this.datNex==null && other.getDatNex()==null) || 
             (this.datNex!=null &&
              this.datNex.equals(other.getDatNex()))) &&
            ((this.datPar==null && other.getDatPar()==null) || 
             (this.datPar!=null &&
              this.datPar.equals(other.getDatPar()))) &&
            ((this.datPer==null && other.getDatPer()==null) || 
             (this.datPer!=null &&
              this.datPer.equals(other.getDatPer()))) &&
            ((this.datTer==null && other.getDatTer()==null) || 
             (this.datTer!=null &&
              this.datTer.equals(other.getDatTer()))) &&
            ((this.diaJus==null && other.getDiaJus()==null) || 
             (this.diaJus!=null &&
              this.diaJus.equals(other.getDiaJus()))) &&
            ((this.diaPrv==null && other.getDiaPrv()==null) || 
             (this.diaPrv!=null &&
              this.diaPrv.equals(other.getDiaPrv()))) &&
            ((this.estCon==null && other.getEstCon()==null) || 
             (this.estCon!=null &&
              this.estCon.equals(other.getEstCon()))) &&
            ((this.exmRet==null && other.getExmRet()==null) || 
             (this.exmRet!=null &&
              this.exmRet.equals(other.getExmRet()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.horAfa==null && other.getHorAfa()==null) || 
             (this.horAfa!=null &&
              this.horAfa.equals(other.getHorAfa()))) &&
            ((this.horTer==null && other.getHorTer()==null) || 
             (this.horTer!=null &&
              this.horTer.equals(other.getHorTer()))) &&
            ((this.motRai==null && other.getMotRai()==null) || 
             (this.motRai!=null &&
              this.motRai.equals(other.getMotRai()))) &&
            ((this.msmMot==null && other.getMsmMot()==null) || 
             (this.msmMot!=null &&
              this.msmMot.equals(other.getMsmMot()))) &&
            ((this.nomAte==null && other.getNomAte()==null) || 
             (this.nomAte!=null &&
              this.nomAte.equals(other.getNomAte()))) &&
            ((this.nroAut==null && other.getNroAut()==null) || 
             (this.nroAut!=null &&
              this.nroAut.equals(other.getNroAut()))) &&
            ((this.numCad==null && other.getNumCad()==null) || 
             (this.numCad!=null &&
              this.numCad.equals(other.getNumCad()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.numPro==null && other.getNumPro()==null) || 
             (this.numPro!=null &&
              this.numPro.equals(other.getNumPro()))) &&
            ((this.obsAfa==null && other.getObsAfa()==null) || 
             (this.obsAfa!=null &&
              this.obsAfa.equals(other.getObsAfa()))) &&
            ((this.orgCla==null && other.getOrgCla()==null) || 
             (this.orgCla!=null &&
              this.orgCla.equals(other.getOrgCla()))) &&
            ((this.oriMot==null && other.getOriMot()==null) || 
             (this.oriMot!=null &&
              this.oriMot.equals(other.getOriMot()))) &&
            ((this.prvTer==null && other.getPrvTer()==null) || 
             (this.prvTer!=null &&
              this.prvTer.equals(other.getPrvTer()))) &&
            ((this.qhrAfa==null && other.getQhrAfa()==null) || 
             (this.qhrAfa!=null &&
              this.qhrAfa.equals(other.getQhrAfa()))) &&
            ((this.regCon==null && other.getRegCon()==null) || 
             (this.regCon!=null &&
              this.regCon.equals(other.getRegCon()))) &&
            ((this.risNex==null && other.getRisNex()==null) || 
             (this.risNex!=null &&
              this.risNex.equals(other.getRisNex()))) &&
            ((this.sitAfa==null && other.getSitAfa()==null) || 
             (this.sitAfa!=null &&
              this.sitAfa.equals(other.getSitAfa()))) &&
            ((this.sitIni==null && other.getSitIni()==null) || 
             (this.sitIni!=null &&
              this.sitIni.equals(other.getSitIni()))) &&
            ((this.tipCol==null && other.getTipCol()==null) || 
             (this.tipCol!=null &&
              this.tipCol.equals(other.getTipCol()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAciTra() != null) {
            _hashCode += getAciTra().hashCode();
        }
        if (getCauDem() != null) {
            _hashCode += getCauDem().hashCode();
        }
        if (getCgcSuc() != null) {
            _hashCode += getCgcSuc().hashCode();
        }
        if (getCodAte() != null) {
            _hashCode += getCodAte().hashCode();
        }
        if (getCodDoe() != null) {
            _hashCode += getCodDoe().hashCode();
        }
        if (getCodOem() != null) {
            _hashCode += getCodOem().hashCode();
        }
        if (getCodSub() != null) {
            _hashCode += getCodSub().hashCode();
        }
        if (getConTov() != null) {
            _hashCode += getConTov().hashCode();
        }
        if (getDatAfa() != null) {
            _hashCode += getDatAfa().hashCode();
        }
        if (getDatAlt() != null) {
            _hashCode += getDatAlt().hashCode();
        }
        if (getDatNex() != null) {
            _hashCode += getDatNex().hashCode();
        }
        if (getDatPar() != null) {
            _hashCode += getDatPar().hashCode();
        }
        if (getDatPer() != null) {
            _hashCode += getDatPer().hashCode();
        }
        if (getDatTer() != null) {
            _hashCode += getDatTer().hashCode();
        }
        if (getDiaJus() != null) {
            _hashCode += getDiaJus().hashCode();
        }
        if (getDiaPrv() != null) {
            _hashCode += getDiaPrv().hashCode();
        }
        if (getEstCon() != null) {
            _hashCode += getEstCon().hashCode();
        }
        if (getExmRet() != null) {
            _hashCode += getExmRet().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getHorAfa() != null) {
            _hashCode += getHorAfa().hashCode();
        }
        if (getHorTer() != null) {
            _hashCode += getHorTer().hashCode();
        }
        if (getMotRai() != null) {
            _hashCode += getMotRai().hashCode();
        }
        if (getMsmMot() != null) {
            _hashCode += getMsmMot().hashCode();
        }
        if (getNomAte() != null) {
            _hashCode += getNomAte().hashCode();
        }
        if (getNroAut() != null) {
            _hashCode += getNroAut().hashCode();
        }
        if (getNumCad() != null) {
            _hashCode += getNumCad().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getNumPro() != null) {
            _hashCode += getNumPro().hashCode();
        }
        if (getObsAfa() != null) {
            _hashCode += getObsAfa().hashCode();
        }
        if (getOrgCla() != null) {
            _hashCode += getOrgCla().hashCode();
        }
        if (getOriMot() != null) {
            _hashCode += getOriMot().hashCode();
        }
        if (getPrvTer() != null) {
            _hashCode += getPrvTer().hashCode();
        }
        if (getQhrAfa() != null) {
            _hashCode += getQhrAfa().hashCode();
        }
        if (getRegCon() != null) {
            _hashCode += getRegCon().hashCode();
        }
        if (getRisNex() != null) {
            _hashCode += getRisNex().hashCode();
        }
        if (getSitAfa() != null) {
            _hashCode += getSitAfa().hashCode();
        }
        if (getSitIni() != null) {
            _hashCode += getSitIni().hashCode();
        }
        if (getTipCol() != null) {
            _hashCode += getTipCol().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HistoricosAfastamento2In.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "historicosAfastamento2In"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("aciTra");
        elemField.setXmlName(new javax.xml.namespace.QName("", "aciTra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cauDem");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cauDem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cgcSuc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cgcSuc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codAte");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codAte"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codDoe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codDoe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codOem");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codOem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codSub");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codSub"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conTov");
        elemField.setXmlName(new javax.xml.namespace.QName("", "conTov"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datNex");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datNex"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datPar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datPar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datPer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datPer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datTer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datTer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diaJus");
        elemField.setXmlName(new javax.xml.namespace.QName("", "diaJus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diaPrv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "diaPrv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estCon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estCon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("exmRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "exmRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horAfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horAfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horTer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horTer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("motRai");
        elemField.setXmlName(new javax.xml.namespace.QName("", "motRai"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msmMot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msmMot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomAte");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomAte"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nroAut");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nroAut"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numPro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numPro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsAfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsAfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orgCla");
        elemField.setXmlName(new javax.xml.namespace.QName("", "orgCla"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("oriMot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "oriMot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prvTer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prvTer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qhrAfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qhrAfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("regCon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "regCon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("risNex");
        elemField.setXmlName(new javax.xml.namespace.QName("", "risNex"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitAfa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitAfa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitIni");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitIni"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
