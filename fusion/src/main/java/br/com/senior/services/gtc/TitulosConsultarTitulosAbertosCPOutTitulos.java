/**
 * TitulosConsultarTitulosAbertosCPOutTitulos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.gtc;

public class TitulosConsultarTitulosAbertosCPOutTitulos  implements java.io.Serializable {
    private java.lang.String antDsc;

    private java.lang.String codCcu;

    private java.lang.String codCrt;

    private java.lang.Integer codEmp;

    private java.lang.Double codFav;

    private java.lang.Integer codFil;

    private java.lang.Integer codFor;

    private java.lang.Integer codFpg;

    private java.lang.Integer codFpj;

    private java.lang.String codFrj;

    private java.lang.String codMoe;

    private java.lang.String codMpt;

    private java.lang.Integer codNtg;

    private java.lang.String codPor;

    private java.lang.String codTns;

    private java.lang.String codTpt;

    private java.lang.String cpgSub;

    private java.lang.Integer ctaFin;

    private java.lang.Integer ctaRed;

    private java.lang.Integer ctrFre;

    private java.lang.Integer ctrNre;

    private java.lang.String datDsc;

    private java.lang.String datEmi;

    private java.lang.String datEnt;

    private java.lang.String datNeg;

    private java.lang.String datPpt;

    private java.lang.Double dscNeg;

    private java.lang.Integer filCcr;

    private java.lang.Integer filCtr;

    private java.lang.Integer filNfc;

    private java.lang.Integer filNff;

    private java.lang.Integer filNfv;

    private java.lang.Integer filOcp;

    private java.lang.Integer forNfc;

    private java.lang.Integer forNff;

    private java.lang.String gerTep;

    private java.lang.Double jrsDia;

    private java.lang.Double jrsNeg;

    private java.lang.Double mulNeg;

    private java.lang.Integer numCcr;

    private java.lang.Integer numCtr;

    private java.lang.Integer numNfc;

    private java.lang.Integer numNff;

    private java.lang.Integer numNfv;

    private java.lang.Integer numOcp;

    private java.lang.Integer numPrj;

    private java.lang.String numTit;

    private java.lang.String obsTcp;

    private java.lang.Integer ocpFre;

    private java.lang.Integer ocpNre;

    private java.lang.Double outNeg;

    private java.lang.Integer perDsc;

    private java.lang.Integer perJrs;

    private java.lang.Integer perMul;

    private java.lang.String proJrs;

    private TitulosConsultarTitulosAbertosCPOutTitulosRateios[] rateios;

    private java.lang.Integer seqCgt;

    private java.lang.Integer seqImo;

    private java.lang.String sitTit;

    private java.lang.String snfNfc;

    private java.lang.String snfNfv;

    private java.lang.String tipEfe;

    private java.lang.String tipJrs;

    private java.lang.Integer tolDsc;

    private java.lang.Integer tolJrs;

    private java.lang.Integer tolMul;

    private java.lang.String ultPgt;

    private java.lang.String vctOri;

    private java.lang.String vctPro;

    private java.lang.Double vlrAbe;

    private java.lang.Double vlrDsc;

    private java.lang.Double vlrOri;

    public TitulosConsultarTitulosAbertosCPOutTitulos() {
    }

    public TitulosConsultarTitulosAbertosCPOutTitulos(
           java.lang.String antDsc,
           java.lang.String codCcu,
           java.lang.String codCrt,
           java.lang.Integer codEmp,
           java.lang.Double codFav,
           java.lang.Integer codFil,
           java.lang.Integer codFor,
           java.lang.Integer codFpg,
           java.lang.Integer codFpj,
           java.lang.String codFrj,
           java.lang.String codMoe,
           java.lang.String codMpt,
           java.lang.Integer codNtg,
           java.lang.String codPor,
           java.lang.String codTns,
           java.lang.String codTpt,
           java.lang.String cpgSub,
           java.lang.Integer ctaFin,
           java.lang.Integer ctaRed,
           java.lang.Integer ctrFre,
           java.lang.Integer ctrNre,
           java.lang.String datDsc,
           java.lang.String datEmi,
           java.lang.String datEnt,
           java.lang.String datNeg,
           java.lang.String datPpt,
           java.lang.Double dscNeg,
           java.lang.Integer filCcr,
           java.lang.Integer filCtr,
           java.lang.Integer filNfc,
           java.lang.Integer filNff,
           java.lang.Integer filNfv,
           java.lang.Integer filOcp,
           java.lang.Integer forNfc,
           java.lang.Integer forNff,
           java.lang.String gerTep,
           java.lang.Double jrsDia,
           java.lang.Double jrsNeg,
           java.lang.Double mulNeg,
           java.lang.Integer numCcr,
           java.lang.Integer numCtr,
           java.lang.Integer numNfc,
           java.lang.Integer numNff,
           java.lang.Integer numNfv,
           java.lang.Integer numOcp,
           java.lang.Integer numPrj,
           java.lang.String numTit,
           java.lang.String obsTcp,
           java.lang.Integer ocpFre,
           java.lang.Integer ocpNre,
           java.lang.Double outNeg,
           java.lang.Integer perDsc,
           java.lang.Integer perJrs,
           java.lang.Integer perMul,
           java.lang.String proJrs,
           TitulosConsultarTitulosAbertosCPOutTitulosRateios[] rateios,
           java.lang.Integer seqCgt,
           java.lang.Integer seqImo,
           java.lang.String sitTit,
           java.lang.String snfNfc,
           java.lang.String snfNfv,
           java.lang.String tipEfe,
           java.lang.String tipJrs,
           java.lang.Integer tolDsc,
           java.lang.Integer tolJrs,
           java.lang.Integer tolMul,
           java.lang.String ultPgt,
           java.lang.String vctOri,
           java.lang.String vctPro,
           java.lang.Double vlrAbe,
           java.lang.Double vlrDsc,
           java.lang.Double vlrOri) {
           this.antDsc = antDsc;
           this.codCcu = codCcu;
           this.codCrt = codCrt;
           this.codEmp = codEmp;
           this.codFav = codFav;
           this.codFil = codFil;
           this.codFor = codFor;
           this.codFpg = codFpg;
           this.codFpj = codFpj;
           this.codFrj = codFrj;
           this.codMoe = codMoe;
           this.codMpt = codMpt;
           this.codNtg = codNtg;
           this.codPor = codPor;
           this.codTns = codTns;
           this.codTpt = codTpt;
           this.cpgSub = cpgSub;
           this.ctaFin = ctaFin;
           this.ctaRed = ctaRed;
           this.ctrFre = ctrFre;
           this.ctrNre = ctrNre;
           this.datDsc = datDsc;
           this.datEmi = datEmi;
           this.datEnt = datEnt;
           this.datNeg = datNeg;
           this.datPpt = datPpt;
           this.dscNeg = dscNeg;
           this.filCcr = filCcr;
           this.filCtr = filCtr;
           this.filNfc = filNfc;
           this.filNff = filNff;
           this.filNfv = filNfv;
           this.filOcp = filOcp;
           this.forNfc = forNfc;
           this.forNff = forNff;
           this.gerTep = gerTep;
           this.jrsDia = jrsDia;
           this.jrsNeg = jrsNeg;
           this.mulNeg = mulNeg;
           this.numCcr = numCcr;
           this.numCtr = numCtr;
           this.numNfc = numNfc;
           this.numNff = numNff;
           this.numNfv = numNfv;
           this.numOcp = numOcp;
           this.numPrj = numPrj;
           this.numTit = numTit;
           this.obsTcp = obsTcp;
           this.ocpFre = ocpFre;
           this.ocpNre = ocpNre;
           this.outNeg = outNeg;
           this.perDsc = perDsc;
           this.perJrs = perJrs;
           this.perMul = perMul;
           this.proJrs = proJrs;
           this.rateios = rateios;
           this.seqCgt = seqCgt;
           this.seqImo = seqImo;
           this.sitTit = sitTit;
           this.snfNfc = snfNfc;
           this.snfNfv = snfNfv;
           this.tipEfe = tipEfe;
           this.tipJrs = tipJrs;
           this.tolDsc = tolDsc;
           this.tolJrs = tolJrs;
           this.tolMul = tolMul;
           this.ultPgt = ultPgt;
           this.vctOri = vctOri;
           this.vctPro = vctPro;
           this.vlrAbe = vlrAbe;
           this.vlrDsc = vlrDsc;
           this.vlrOri = vlrOri;
    }


    /**
     * Gets the antDsc value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return antDsc
     */
    public java.lang.String getAntDsc() {
        return antDsc;
    }


    /**
     * Sets the antDsc value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param antDsc
     */
    public void setAntDsc(java.lang.String antDsc) {
        this.antDsc = antDsc;
    }


    /**
     * Gets the codCcu value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return codCcu
     */
    public java.lang.String getCodCcu() {
        return codCcu;
    }


    /**
     * Sets the codCcu value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param codCcu
     */
    public void setCodCcu(java.lang.String codCcu) {
        this.codCcu = codCcu;
    }


    /**
     * Gets the codCrt value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return codCrt
     */
    public java.lang.String getCodCrt() {
        return codCrt;
    }


    /**
     * Sets the codCrt value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param codCrt
     */
    public void setCodCrt(java.lang.String codCrt) {
        this.codCrt = codCrt;
    }


    /**
     * Gets the codEmp value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFav value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return codFav
     */
    public java.lang.Double getCodFav() {
        return codFav;
    }


    /**
     * Sets the codFav value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param codFav
     */
    public void setCodFav(java.lang.Double codFav) {
        this.codFav = codFav;
    }


    /**
     * Gets the codFil value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codFor value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return codFor
     */
    public java.lang.Integer getCodFor() {
        return codFor;
    }


    /**
     * Sets the codFor value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param codFor
     */
    public void setCodFor(java.lang.Integer codFor) {
        this.codFor = codFor;
    }


    /**
     * Gets the codFpg value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return codFpg
     */
    public java.lang.Integer getCodFpg() {
        return codFpg;
    }


    /**
     * Sets the codFpg value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param codFpg
     */
    public void setCodFpg(java.lang.Integer codFpg) {
        this.codFpg = codFpg;
    }


    /**
     * Gets the codFpj value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return codFpj
     */
    public java.lang.Integer getCodFpj() {
        return codFpj;
    }


    /**
     * Sets the codFpj value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param codFpj
     */
    public void setCodFpj(java.lang.Integer codFpj) {
        this.codFpj = codFpj;
    }


    /**
     * Gets the codFrj value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return codFrj
     */
    public java.lang.String getCodFrj() {
        return codFrj;
    }


    /**
     * Sets the codFrj value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param codFrj
     */
    public void setCodFrj(java.lang.String codFrj) {
        this.codFrj = codFrj;
    }


    /**
     * Gets the codMoe value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return codMoe
     */
    public java.lang.String getCodMoe() {
        return codMoe;
    }


    /**
     * Sets the codMoe value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param codMoe
     */
    public void setCodMoe(java.lang.String codMoe) {
        this.codMoe = codMoe;
    }


    /**
     * Gets the codMpt value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return codMpt
     */
    public java.lang.String getCodMpt() {
        return codMpt;
    }


    /**
     * Sets the codMpt value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param codMpt
     */
    public void setCodMpt(java.lang.String codMpt) {
        this.codMpt = codMpt;
    }


    /**
     * Gets the codNtg value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return codNtg
     */
    public java.lang.Integer getCodNtg() {
        return codNtg;
    }


    /**
     * Sets the codNtg value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param codNtg
     */
    public void setCodNtg(java.lang.Integer codNtg) {
        this.codNtg = codNtg;
    }


    /**
     * Gets the codPor value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return codPor
     */
    public java.lang.String getCodPor() {
        return codPor;
    }


    /**
     * Sets the codPor value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param codPor
     */
    public void setCodPor(java.lang.String codPor) {
        this.codPor = codPor;
    }


    /**
     * Gets the codTns value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return codTns
     */
    public java.lang.String getCodTns() {
        return codTns;
    }


    /**
     * Sets the codTns value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param codTns
     */
    public void setCodTns(java.lang.String codTns) {
        this.codTns = codTns;
    }


    /**
     * Gets the codTpt value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return codTpt
     */
    public java.lang.String getCodTpt() {
        return codTpt;
    }


    /**
     * Sets the codTpt value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param codTpt
     */
    public void setCodTpt(java.lang.String codTpt) {
        this.codTpt = codTpt;
    }


    /**
     * Gets the cpgSub value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return cpgSub
     */
    public java.lang.String getCpgSub() {
        return cpgSub;
    }


    /**
     * Sets the cpgSub value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param cpgSub
     */
    public void setCpgSub(java.lang.String cpgSub) {
        this.cpgSub = cpgSub;
    }


    /**
     * Gets the ctaFin value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return ctaFin
     */
    public java.lang.Integer getCtaFin() {
        return ctaFin;
    }


    /**
     * Sets the ctaFin value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param ctaFin
     */
    public void setCtaFin(java.lang.Integer ctaFin) {
        this.ctaFin = ctaFin;
    }


    /**
     * Gets the ctaRed value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return ctaRed
     */
    public java.lang.Integer getCtaRed() {
        return ctaRed;
    }


    /**
     * Sets the ctaRed value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param ctaRed
     */
    public void setCtaRed(java.lang.Integer ctaRed) {
        this.ctaRed = ctaRed;
    }


    /**
     * Gets the ctrFre value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return ctrFre
     */
    public java.lang.Integer getCtrFre() {
        return ctrFre;
    }


    /**
     * Sets the ctrFre value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param ctrFre
     */
    public void setCtrFre(java.lang.Integer ctrFre) {
        this.ctrFre = ctrFre;
    }


    /**
     * Gets the ctrNre value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return ctrNre
     */
    public java.lang.Integer getCtrNre() {
        return ctrNre;
    }


    /**
     * Sets the ctrNre value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param ctrNre
     */
    public void setCtrNre(java.lang.Integer ctrNre) {
        this.ctrNre = ctrNre;
    }


    /**
     * Gets the datDsc value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return datDsc
     */
    public java.lang.String getDatDsc() {
        return datDsc;
    }


    /**
     * Sets the datDsc value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param datDsc
     */
    public void setDatDsc(java.lang.String datDsc) {
        this.datDsc = datDsc;
    }


    /**
     * Gets the datEmi value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return datEmi
     */
    public java.lang.String getDatEmi() {
        return datEmi;
    }


    /**
     * Sets the datEmi value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param datEmi
     */
    public void setDatEmi(java.lang.String datEmi) {
        this.datEmi = datEmi;
    }


    /**
     * Gets the datEnt value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return datEnt
     */
    public java.lang.String getDatEnt() {
        return datEnt;
    }


    /**
     * Sets the datEnt value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param datEnt
     */
    public void setDatEnt(java.lang.String datEnt) {
        this.datEnt = datEnt;
    }


    /**
     * Gets the datNeg value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return datNeg
     */
    public java.lang.String getDatNeg() {
        return datNeg;
    }


    /**
     * Sets the datNeg value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param datNeg
     */
    public void setDatNeg(java.lang.String datNeg) {
        this.datNeg = datNeg;
    }


    /**
     * Gets the datPpt value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return datPpt
     */
    public java.lang.String getDatPpt() {
        return datPpt;
    }


    /**
     * Sets the datPpt value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param datPpt
     */
    public void setDatPpt(java.lang.String datPpt) {
        this.datPpt = datPpt;
    }


    /**
     * Gets the dscNeg value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return dscNeg
     */
    public java.lang.Double getDscNeg() {
        return dscNeg;
    }


    /**
     * Sets the dscNeg value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param dscNeg
     */
    public void setDscNeg(java.lang.Double dscNeg) {
        this.dscNeg = dscNeg;
    }


    /**
     * Gets the filCcr value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return filCcr
     */
    public java.lang.Integer getFilCcr() {
        return filCcr;
    }


    /**
     * Sets the filCcr value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param filCcr
     */
    public void setFilCcr(java.lang.Integer filCcr) {
        this.filCcr = filCcr;
    }


    /**
     * Gets the filCtr value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return filCtr
     */
    public java.lang.Integer getFilCtr() {
        return filCtr;
    }


    /**
     * Sets the filCtr value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param filCtr
     */
    public void setFilCtr(java.lang.Integer filCtr) {
        this.filCtr = filCtr;
    }


    /**
     * Gets the filNfc value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return filNfc
     */
    public java.lang.Integer getFilNfc() {
        return filNfc;
    }


    /**
     * Sets the filNfc value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param filNfc
     */
    public void setFilNfc(java.lang.Integer filNfc) {
        this.filNfc = filNfc;
    }


    /**
     * Gets the filNff value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return filNff
     */
    public java.lang.Integer getFilNff() {
        return filNff;
    }


    /**
     * Sets the filNff value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param filNff
     */
    public void setFilNff(java.lang.Integer filNff) {
        this.filNff = filNff;
    }


    /**
     * Gets the filNfv value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return filNfv
     */
    public java.lang.Integer getFilNfv() {
        return filNfv;
    }


    /**
     * Sets the filNfv value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param filNfv
     */
    public void setFilNfv(java.lang.Integer filNfv) {
        this.filNfv = filNfv;
    }


    /**
     * Gets the filOcp value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return filOcp
     */
    public java.lang.Integer getFilOcp() {
        return filOcp;
    }


    /**
     * Sets the filOcp value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param filOcp
     */
    public void setFilOcp(java.lang.Integer filOcp) {
        this.filOcp = filOcp;
    }


    /**
     * Gets the forNfc value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return forNfc
     */
    public java.lang.Integer getForNfc() {
        return forNfc;
    }


    /**
     * Sets the forNfc value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param forNfc
     */
    public void setForNfc(java.lang.Integer forNfc) {
        this.forNfc = forNfc;
    }


    /**
     * Gets the forNff value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return forNff
     */
    public java.lang.Integer getForNff() {
        return forNff;
    }


    /**
     * Sets the forNff value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param forNff
     */
    public void setForNff(java.lang.Integer forNff) {
        this.forNff = forNff;
    }


    /**
     * Gets the gerTep value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return gerTep
     */
    public java.lang.String getGerTep() {
        return gerTep;
    }


    /**
     * Sets the gerTep value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param gerTep
     */
    public void setGerTep(java.lang.String gerTep) {
        this.gerTep = gerTep;
    }


    /**
     * Gets the jrsDia value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return jrsDia
     */
    public java.lang.Double getJrsDia() {
        return jrsDia;
    }


    /**
     * Sets the jrsDia value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param jrsDia
     */
    public void setJrsDia(java.lang.Double jrsDia) {
        this.jrsDia = jrsDia;
    }


    /**
     * Gets the jrsNeg value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return jrsNeg
     */
    public java.lang.Double getJrsNeg() {
        return jrsNeg;
    }


    /**
     * Sets the jrsNeg value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param jrsNeg
     */
    public void setJrsNeg(java.lang.Double jrsNeg) {
        this.jrsNeg = jrsNeg;
    }


    /**
     * Gets the mulNeg value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return mulNeg
     */
    public java.lang.Double getMulNeg() {
        return mulNeg;
    }


    /**
     * Sets the mulNeg value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param mulNeg
     */
    public void setMulNeg(java.lang.Double mulNeg) {
        this.mulNeg = mulNeg;
    }


    /**
     * Gets the numCcr value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return numCcr
     */
    public java.lang.Integer getNumCcr() {
        return numCcr;
    }


    /**
     * Sets the numCcr value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param numCcr
     */
    public void setNumCcr(java.lang.Integer numCcr) {
        this.numCcr = numCcr;
    }


    /**
     * Gets the numCtr value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return numCtr
     */
    public java.lang.Integer getNumCtr() {
        return numCtr;
    }


    /**
     * Sets the numCtr value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param numCtr
     */
    public void setNumCtr(java.lang.Integer numCtr) {
        this.numCtr = numCtr;
    }


    /**
     * Gets the numNfc value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return numNfc
     */
    public java.lang.Integer getNumNfc() {
        return numNfc;
    }


    /**
     * Sets the numNfc value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param numNfc
     */
    public void setNumNfc(java.lang.Integer numNfc) {
        this.numNfc = numNfc;
    }


    /**
     * Gets the numNff value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return numNff
     */
    public java.lang.Integer getNumNff() {
        return numNff;
    }


    /**
     * Sets the numNff value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param numNff
     */
    public void setNumNff(java.lang.Integer numNff) {
        this.numNff = numNff;
    }


    /**
     * Gets the numNfv value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return numNfv
     */
    public java.lang.Integer getNumNfv() {
        return numNfv;
    }


    /**
     * Sets the numNfv value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param numNfv
     */
    public void setNumNfv(java.lang.Integer numNfv) {
        this.numNfv = numNfv;
    }


    /**
     * Gets the numOcp value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return numOcp
     */
    public java.lang.Integer getNumOcp() {
        return numOcp;
    }


    /**
     * Sets the numOcp value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param numOcp
     */
    public void setNumOcp(java.lang.Integer numOcp) {
        this.numOcp = numOcp;
    }


    /**
     * Gets the numPrj value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return numPrj
     */
    public java.lang.Integer getNumPrj() {
        return numPrj;
    }


    /**
     * Sets the numPrj value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param numPrj
     */
    public void setNumPrj(java.lang.Integer numPrj) {
        this.numPrj = numPrj;
    }


    /**
     * Gets the numTit value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return numTit
     */
    public java.lang.String getNumTit() {
        return numTit;
    }


    /**
     * Sets the numTit value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param numTit
     */
    public void setNumTit(java.lang.String numTit) {
        this.numTit = numTit;
    }


    /**
     * Gets the obsTcp value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return obsTcp
     */
    public java.lang.String getObsTcp() {
        return obsTcp;
    }


    /**
     * Sets the obsTcp value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param obsTcp
     */
    public void setObsTcp(java.lang.String obsTcp) {
        this.obsTcp = obsTcp;
    }


    /**
     * Gets the ocpFre value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return ocpFre
     */
    public java.lang.Integer getOcpFre() {
        return ocpFre;
    }


    /**
     * Sets the ocpFre value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param ocpFre
     */
    public void setOcpFre(java.lang.Integer ocpFre) {
        this.ocpFre = ocpFre;
    }


    /**
     * Gets the ocpNre value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return ocpNre
     */
    public java.lang.Integer getOcpNre() {
        return ocpNre;
    }


    /**
     * Sets the ocpNre value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param ocpNre
     */
    public void setOcpNre(java.lang.Integer ocpNre) {
        this.ocpNre = ocpNre;
    }


    /**
     * Gets the outNeg value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return outNeg
     */
    public java.lang.Double getOutNeg() {
        return outNeg;
    }


    /**
     * Sets the outNeg value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param outNeg
     */
    public void setOutNeg(java.lang.Double outNeg) {
        this.outNeg = outNeg;
    }


    /**
     * Gets the perDsc value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return perDsc
     */
    public java.lang.Integer getPerDsc() {
        return perDsc;
    }


    /**
     * Sets the perDsc value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param perDsc
     */
    public void setPerDsc(java.lang.Integer perDsc) {
        this.perDsc = perDsc;
    }


    /**
     * Gets the perJrs value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return perJrs
     */
    public java.lang.Integer getPerJrs() {
        return perJrs;
    }


    /**
     * Sets the perJrs value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param perJrs
     */
    public void setPerJrs(java.lang.Integer perJrs) {
        this.perJrs = perJrs;
    }


    /**
     * Gets the perMul value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return perMul
     */
    public java.lang.Integer getPerMul() {
        return perMul;
    }


    /**
     * Sets the perMul value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param perMul
     */
    public void setPerMul(java.lang.Integer perMul) {
        this.perMul = perMul;
    }


    /**
     * Gets the proJrs value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return proJrs
     */
    public java.lang.String getProJrs() {
        return proJrs;
    }


    /**
     * Sets the proJrs value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param proJrs
     */
    public void setProJrs(java.lang.String proJrs) {
        this.proJrs = proJrs;
    }


    /**
     * Gets the rateios value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return rateios
     */
    public TitulosConsultarTitulosAbertosCPOutTitulosRateios[] getRateios() {
        return rateios;
    }


    /**
     * Sets the rateios value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param rateios
     */
    public void setRateios(TitulosConsultarTitulosAbertosCPOutTitulosRateios[] rateios) {
        this.rateios = rateios;
    }

    public TitulosConsultarTitulosAbertosCPOutTitulosRateios getRateios(int i) {
        return this.rateios[i];
    }

    public void setRateios(int i, TitulosConsultarTitulosAbertosCPOutTitulosRateios _value) {
        this.rateios[i] = _value;
    }


    /**
     * Gets the seqCgt value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return seqCgt
     */
    public java.lang.Integer getSeqCgt() {
        return seqCgt;
    }


    /**
     * Sets the seqCgt value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param seqCgt
     */
    public void setSeqCgt(java.lang.Integer seqCgt) {
        this.seqCgt = seqCgt;
    }


    /**
     * Gets the seqImo value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return seqImo
     */
    public java.lang.Integer getSeqImo() {
        return seqImo;
    }


    /**
     * Sets the seqImo value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param seqImo
     */
    public void setSeqImo(java.lang.Integer seqImo) {
        this.seqImo = seqImo;
    }


    /**
     * Gets the sitTit value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return sitTit
     */
    public java.lang.String getSitTit() {
        return sitTit;
    }


    /**
     * Sets the sitTit value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param sitTit
     */
    public void setSitTit(java.lang.String sitTit) {
        this.sitTit = sitTit;
    }


    /**
     * Gets the snfNfc value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return snfNfc
     */
    public java.lang.String getSnfNfc() {
        return snfNfc;
    }


    /**
     * Sets the snfNfc value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param snfNfc
     */
    public void setSnfNfc(java.lang.String snfNfc) {
        this.snfNfc = snfNfc;
    }


    /**
     * Gets the snfNfv value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return snfNfv
     */
    public java.lang.String getSnfNfv() {
        return snfNfv;
    }


    /**
     * Sets the snfNfv value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param snfNfv
     */
    public void setSnfNfv(java.lang.String snfNfv) {
        this.snfNfv = snfNfv;
    }


    /**
     * Gets the tipEfe value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return tipEfe
     */
    public java.lang.String getTipEfe() {
        return tipEfe;
    }


    /**
     * Sets the tipEfe value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param tipEfe
     */
    public void setTipEfe(java.lang.String tipEfe) {
        this.tipEfe = tipEfe;
    }


    /**
     * Gets the tipJrs value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return tipJrs
     */
    public java.lang.String getTipJrs() {
        return tipJrs;
    }


    /**
     * Sets the tipJrs value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param tipJrs
     */
    public void setTipJrs(java.lang.String tipJrs) {
        this.tipJrs = tipJrs;
    }


    /**
     * Gets the tolDsc value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return tolDsc
     */
    public java.lang.Integer getTolDsc() {
        return tolDsc;
    }


    /**
     * Sets the tolDsc value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param tolDsc
     */
    public void setTolDsc(java.lang.Integer tolDsc) {
        this.tolDsc = tolDsc;
    }


    /**
     * Gets the tolJrs value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return tolJrs
     */
    public java.lang.Integer getTolJrs() {
        return tolJrs;
    }


    /**
     * Sets the tolJrs value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param tolJrs
     */
    public void setTolJrs(java.lang.Integer tolJrs) {
        this.tolJrs = tolJrs;
    }


    /**
     * Gets the tolMul value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return tolMul
     */
    public java.lang.Integer getTolMul() {
        return tolMul;
    }


    /**
     * Sets the tolMul value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param tolMul
     */
    public void setTolMul(java.lang.Integer tolMul) {
        this.tolMul = tolMul;
    }


    /**
     * Gets the ultPgt value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return ultPgt
     */
    public java.lang.String getUltPgt() {
        return ultPgt;
    }


    /**
     * Sets the ultPgt value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param ultPgt
     */
    public void setUltPgt(java.lang.String ultPgt) {
        this.ultPgt = ultPgt;
    }


    /**
     * Gets the vctOri value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return vctOri
     */
    public java.lang.String getVctOri() {
        return vctOri;
    }


    /**
     * Sets the vctOri value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param vctOri
     */
    public void setVctOri(java.lang.String vctOri) {
        this.vctOri = vctOri;
    }


    /**
     * Gets the vctPro value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return vctPro
     */
    public java.lang.String getVctPro() {
        return vctPro;
    }


    /**
     * Sets the vctPro value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param vctPro
     */
    public void setVctPro(java.lang.String vctPro) {
        this.vctPro = vctPro;
    }


    /**
     * Gets the vlrAbe value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return vlrAbe
     */
    public java.lang.Double getVlrAbe() {
        return vlrAbe;
    }


    /**
     * Sets the vlrAbe value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param vlrAbe
     */
    public void setVlrAbe(java.lang.Double vlrAbe) {
        this.vlrAbe = vlrAbe;
    }


    /**
     * Gets the vlrDsc value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return vlrDsc
     */
    public java.lang.Double getVlrDsc() {
        return vlrDsc;
    }


    /**
     * Sets the vlrDsc value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param vlrDsc
     */
    public void setVlrDsc(java.lang.Double vlrDsc) {
        this.vlrDsc = vlrDsc;
    }


    /**
     * Gets the vlrOri value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @return vlrOri
     */
    public java.lang.Double getVlrOri() {
        return vlrOri;
    }


    /**
     * Sets the vlrOri value for this TitulosConsultarTitulosAbertosCPOutTitulos.
     * 
     * @param vlrOri
     */
    public void setVlrOri(java.lang.Double vlrOri) {
        this.vlrOri = vlrOri;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosConsultarTitulosAbertosCPOutTitulos)) return false;
        TitulosConsultarTitulosAbertosCPOutTitulos other = (TitulosConsultarTitulosAbertosCPOutTitulos) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.antDsc==null && other.getAntDsc()==null) || 
             (this.antDsc!=null &&
              this.antDsc.equals(other.getAntDsc()))) &&
            ((this.codCcu==null && other.getCodCcu()==null) || 
             (this.codCcu!=null &&
              this.codCcu.equals(other.getCodCcu()))) &&
            ((this.codCrt==null && other.getCodCrt()==null) || 
             (this.codCrt!=null &&
              this.codCrt.equals(other.getCodCrt()))) &&
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFav==null && other.getCodFav()==null) || 
             (this.codFav!=null &&
              this.codFav.equals(other.getCodFav()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codFor==null && other.getCodFor()==null) || 
             (this.codFor!=null &&
              this.codFor.equals(other.getCodFor()))) &&
            ((this.codFpg==null && other.getCodFpg()==null) || 
             (this.codFpg!=null &&
              this.codFpg.equals(other.getCodFpg()))) &&
            ((this.codFpj==null && other.getCodFpj()==null) || 
             (this.codFpj!=null &&
              this.codFpj.equals(other.getCodFpj()))) &&
            ((this.codFrj==null && other.getCodFrj()==null) || 
             (this.codFrj!=null &&
              this.codFrj.equals(other.getCodFrj()))) &&
            ((this.codMoe==null && other.getCodMoe()==null) || 
             (this.codMoe!=null &&
              this.codMoe.equals(other.getCodMoe()))) &&
            ((this.codMpt==null && other.getCodMpt()==null) || 
             (this.codMpt!=null &&
              this.codMpt.equals(other.getCodMpt()))) &&
            ((this.codNtg==null && other.getCodNtg()==null) || 
             (this.codNtg!=null &&
              this.codNtg.equals(other.getCodNtg()))) &&
            ((this.codPor==null && other.getCodPor()==null) || 
             (this.codPor!=null &&
              this.codPor.equals(other.getCodPor()))) &&
            ((this.codTns==null && other.getCodTns()==null) || 
             (this.codTns!=null &&
              this.codTns.equals(other.getCodTns()))) &&
            ((this.codTpt==null && other.getCodTpt()==null) || 
             (this.codTpt!=null &&
              this.codTpt.equals(other.getCodTpt()))) &&
            ((this.cpgSub==null && other.getCpgSub()==null) || 
             (this.cpgSub!=null &&
              this.cpgSub.equals(other.getCpgSub()))) &&
            ((this.ctaFin==null && other.getCtaFin()==null) || 
             (this.ctaFin!=null &&
              this.ctaFin.equals(other.getCtaFin()))) &&
            ((this.ctaRed==null && other.getCtaRed()==null) || 
             (this.ctaRed!=null &&
              this.ctaRed.equals(other.getCtaRed()))) &&
            ((this.ctrFre==null && other.getCtrFre()==null) || 
             (this.ctrFre!=null &&
              this.ctrFre.equals(other.getCtrFre()))) &&
            ((this.ctrNre==null && other.getCtrNre()==null) || 
             (this.ctrNre!=null &&
              this.ctrNre.equals(other.getCtrNre()))) &&
            ((this.datDsc==null && other.getDatDsc()==null) || 
             (this.datDsc!=null &&
              this.datDsc.equals(other.getDatDsc()))) &&
            ((this.datEmi==null && other.getDatEmi()==null) || 
             (this.datEmi!=null &&
              this.datEmi.equals(other.getDatEmi()))) &&
            ((this.datEnt==null && other.getDatEnt()==null) || 
             (this.datEnt!=null &&
              this.datEnt.equals(other.getDatEnt()))) &&
            ((this.datNeg==null && other.getDatNeg()==null) || 
             (this.datNeg!=null &&
              this.datNeg.equals(other.getDatNeg()))) &&
            ((this.datPpt==null && other.getDatPpt()==null) || 
             (this.datPpt!=null &&
              this.datPpt.equals(other.getDatPpt()))) &&
            ((this.dscNeg==null && other.getDscNeg()==null) || 
             (this.dscNeg!=null &&
              this.dscNeg.equals(other.getDscNeg()))) &&
            ((this.filCcr==null && other.getFilCcr()==null) || 
             (this.filCcr!=null &&
              this.filCcr.equals(other.getFilCcr()))) &&
            ((this.filCtr==null && other.getFilCtr()==null) || 
             (this.filCtr!=null &&
              this.filCtr.equals(other.getFilCtr()))) &&
            ((this.filNfc==null && other.getFilNfc()==null) || 
             (this.filNfc!=null &&
              this.filNfc.equals(other.getFilNfc()))) &&
            ((this.filNff==null && other.getFilNff()==null) || 
             (this.filNff!=null &&
              this.filNff.equals(other.getFilNff()))) &&
            ((this.filNfv==null && other.getFilNfv()==null) || 
             (this.filNfv!=null &&
              this.filNfv.equals(other.getFilNfv()))) &&
            ((this.filOcp==null && other.getFilOcp()==null) || 
             (this.filOcp!=null &&
              this.filOcp.equals(other.getFilOcp()))) &&
            ((this.forNfc==null && other.getForNfc()==null) || 
             (this.forNfc!=null &&
              this.forNfc.equals(other.getForNfc()))) &&
            ((this.forNff==null && other.getForNff()==null) || 
             (this.forNff!=null &&
              this.forNff.equals(other.getForNff()))) &&
            ((this.gerTep==null && other.getGerTep()==null) || 
             (this.gerTep!=null &&
              this.gerTep.equals(other.getGerTep()))) &&
            ((this.jrsDia==null && other.getJrsDia()==null) || 
             (this.jrsDia!=null &&
              this.jrsDia.equals(other.getJrsDia()))) &&
            ((this.jrsNeg==null && other.getJrsNeg()==null) || 
             (this.jrsNeg!=null &&
              this.jrsNeg.equals(other.getJrsNeg()))) &&
            ((this.mulNeg==null && other.getMulNeg()==null) || 
             (this.mulNeg!=null &&
              this.mulNeg.equals(other.getMulNeg()))) &&
            ((this.numCcr==null && other.getNumCcr()==null) || 
             (this.numCcr!=null &&
              this.numCcr.equals(other.getNumCcr()))) &&
            ((this.numCtr==null && other.getNumCtr()==null) || 
             (this.numCtr!=null &&
              this.numCtr.equals(other.getNumCtr()))) &&
            ((this.numNfc==null && other.getNumNfc()==null) || 
             (this.numNfc!=null &&
              this.numNfc.equals(other.getNumNfc()))) &&
            ((this.numNff==null && other.getNumNff()==null) || 
             (this.numNff!=null &&
              this.numNff.equals(other.getNumNff()))) &&
            ((this.numNfv==null && other.getNumNfv()==null) || 
             (this.numNfv!=null &&
              this.numNfv.equals(other.getNumNfv()))) &&
            ((this.numOcp==null && other.getNumOcp()==null) || 
             (this.numOcp!=null &&
              this.numOcp.equals(other.getNumOcp()))) &&
            ((this.numPrj==null && other.getNumPrj()==null) || 
             (this.numPrj!=null &&
              this.numPrj.equals(other.getNumPrj()))) &&
            ((this.numTit==null && other.getNumTit()==null) || 
             (this.numTit!=null &&
              this.numTit.equals(other.getNumTit()))) &&
            ((this.obsTcp==null && other.getObsTcp()==null) || 
             (this.obsTcp!=null &&
              this.obsTcp.equals(other.getObsTcp()))) &&
            ((this.ocpFre==null && other.getOcpFre()==null) || 
             (this.ocpFre!=null &&
              this.ocpFre.equals(other.getOcpFre()))) &&
            ((this.ocpNre==null && other.getOcpNre()==null) || 
             (this.ocpNre!=null &&
              this.ocpNre.equals(other.getOcpNre()))) &&
            ((this.outNeg==null && other.getOutNeg()==null) || 
             (this.outNeg!=null &&
              this.outNeg.equals(other.getOutNeg()))) &&
            ((this.perDsc==null && other.getPerDsc()==null) || 
             (this.perDsc!=null &&
              this.perDsc.equals(other.getPerDsc()))) &&
            ((this.perJrs==null && other.getPerJrs()==null) || 
             (this.perJrs!=null &&
              this.perJrs.equals(other.getPerJrs()))) &&
            ((this.perMul==null && other.getPerMul()==null) || 
             (this.perMul!=null &&
              this.perMul.equals(other.getPerMul()))) &&
            ((this.proJrs==null && other.getProJrs()==null) || 
             (this.proJrs!=null &&
              this.proJrs.equals(other.getProJrs()))) &&
            ((this.rateios==null && other.getRateios()==null) || 
             (this.rateios!=null &&
              java.util.Arrays.equals(this.rateios, other.getRateios()))) &&
            ((this.seqCgt==null && other.getSeqCgt()==null) || 
             (this.seqCgt!=null &&
              this.seqCgt.equals(other.getSeqCgt()))) &&
            ((this.seqImo==null && other.getSeqImo()==null) || 
             (this.seqImo!=null &&
              this.seqImo.equals(other.getSeqImo()))) &&
            ((this.sitTit==null && other.getSitTit()==null) || 
             (this.sitTit!=null &&
              this.sitTit.equals(other.getSitTit()))) &&
            ((this.snfNfc==null && other.getSnfNfc()==null) || 
             (this.snfNfc!=null &&
              this.snfNfc.equals(other.getSnfNfc()))) &&
            ((this.snfNfv==null && other.getSnfNfv()==null) || 
             (this.snfNfv!=null &&
              this.snfNfv.equals(other.getSnfNfv()))) &&
            ((this.tipEfe==null && other.getTipEfe()==null) || 
             (this.tipEfe!=null &&
              this.tipEfe.equals(other.getTipEfe()))) &&
            ((this.tipJrs==null && other.getTipJrs()==null) || 
             (this.tipJrs!=null &&
              this.tipJrs.equals(other.getTipJrs()))) &&
            ((this.tolDsc==null && other.getTolDsc()==null) || 
             (this.tolDsc!=null &&
              this.tolDsc.equals(other.getTolDsc()))) &&
            ((this.tolJrs==null && other.getTolJrs()==null) || 
             (this.tolJrs!=null &&
              this.tolJrs.equals(other.getTolJrs()))) &&
            ((this.tolMul==null && other.getTolMul()==null) || 
             (this.tolMul!=null &&
              this.tolMul.equals(other.getTolMul()))) &&
            ((this.ultPgt==null && other.getUltPgt()==null) || 
             (this.ultPgt!=null &&
              this.ultPgt.equals(other.getUltPgt()))) &&
            ((this.vctOri==null && other.getVctOri()==null) || 
             (this.vctOri!=null &&
              this.vctOri.equals(other.getVctOri()))) &&
            ((this.vctPro==null && other.getVctPro()==null) || 
             (this.vctPro!=null &&
              this.vctPro.equals(other.getVctPro()))) &&
            ((this.vlrAbe==null && other.getVlrAbe()==null) || 
             (this.vlrAbe!=null &&
              this.vlrAbe.equals(other.getVlrAbe()))) &&
            ((this.vlrDsc==null && other.getVlrDsc()==null) || 
             (this.vlrDsc!=null &&
              this.vlrDsc.equals(other.getVlrDsc()))) &&
            ((this.vlrOri==null && other.getVlrOri()==null) || 
             (this.vlrOri!=null &&
              this.vlrOri.equals(other.getVlrOri())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAntDsc() != null) {
            _hashCode += getAntDsc().hashCode();
        }
        if (getCodCcu() != null) {
            _hashCode += getCodCcu().hashCode();
        }
        if (getCodCrt() != null) {
            _hashCode += getCodCrt().hashCode();
        }
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFav() != null) {
            _hashCode += getCodFav().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodFor() != null) {
            _hashCode += getCodFor().hashCode();
        }
        if (getCodFpg() != null) {
            _hashCode += getCodFpg().hashCode();
        }
        if (getCodFpj() != null) {
            _hashCode += getCodFpj().hashCode();
        }
        if (getCodFrj() != null) {
            _hashCode += getCodFrj().hashCode();
        }
        if (getCodMoe() != null) {
            _hashCode += getCodMoe().hashCode();
        }
        if (getCodMpt() != null) {
            _hashCode += getCodMpt().hashCode();
        }
        if (getCodNtg() != null) {
            _hashCode += getCodNtg().hashCode();
        }
        if (getCodPor() != null) {
            _hashCode += getCodPor().hashCode();
        }
        if (getCodTns() != null) {
            _hashCode += getCodTns().hashCode();
        }
        if (getCodTpt() != null) {
            _hashCode += getCodTpt().hashCode();
        }
        if (getCpgSub() != null) {
            _hashCode += getCpgSub().hashCode();
        }
        if (getCtaFin() != null) {
            _hashCode += getCtaFin().hashCode();
        }
        if (getCtaRed() != null) {
            _hashCode += getCtaRed().hashCode();
        }
        if (getCtrFre() != null) {
            _hashCode += getCtrFre().hashCode();
        }
        if (getCtrNre() != null) {
            _hashCode += getCtrNre().hashCode();
        }
        if (getDatDsc() != null) {
            _hashCode += getDatDsc().hashCode();
        }
        if (getDatEmi() != null) {
            _hashCode += getDatEmi().hashCode();
        }
        if (getDatEnt() != null) {
            _hashCode += getDatEnt().hashCode();
        }
        if (getDatNeg() != null) {
            _hashCode += getDatNeg().hashCode();
        }
        if (getDatPpt() != null) {
            _hashCode += getDatPpt().hashCode();
        }
        if (getDscNeg() != null) {
            _hashCode += getDscNeg().hashCode();
        }
        if (getFilCcr() != null) {
            _hashCode += getFilCcr().hashCode();
        }
        if (getFilCtr() != null) {
            _hashCode += getFilCtr().hashCode();
        }
        if (getFilNfc() != null) {
            _hashCode += getFilNfc().hashCode();
        }
        if (getFilNff() != null) {
            _hashCode += getFilNff().hashCode();
        }
        if (getFilNfv() != null) {
            _hashCode += getFilNfv().hashCode();
        }
        if (getFilOcp() != null) {
            _hashCode += getFilOcp().hashCode();
        }
        if (getForNfc() != null) {
            _hashCode += getForNfc().hashCode();
        }
        if (getForNff() != null) {
            _hashCode += getForNff().hashCode();
        }
        if (getGerTep() != null) {
            _hashCode += getGerTep().hashCode();
        }
        if (getJrsDia() != null) {
            _hashCode += getJrsDia().hashCode();
        }
        if (getJrsNeg() != null) {
            _hashCode += getJrsNeg().hashCode();
        }
        if (getMulNeg() != null) {
            _hashCode += getMulNeg().hashCode();
        }
        if (getNumCcr() != null) {
            _hashCode += getNumCcr().hashCode();
        }
        if (getNumCtr() != null) {
            _hashCode += getNumCtr().hashCode();
        }
        if (getNumNfc() != null) {
            _hashCode += getNumNfc().hashCode();
        }
        if (getNumNff() != null) {
            _hashCode += getNumNff().hashCode();
        }
        if (getNumNfv() != null) {
            _hashCode += getNumNfv().hashCode();
        }
        if (getNumOcp() != null) {
            _hashCode += getNumOcp().hashCode();
        }
        if (getNumPrj() != null) {
            _hashCode += getNumPrj().hashCode();
        }
        if (getNumTit() != null) {
            _hashCode += getNumTit().hashCode();
        }
        if (getObsTcp() != null) {
            _hashCode += getObsTcp().hashCode();
        }
        if (getOcpFre() != null) {
            _hashCode += getOcpFre().hashCode();
        }
        if (getOcpNre() != null) {
            _hashCode += getOcpNre().hashCode();
        }
        if (getOutNeg() != null) {
            _hashCode += getOutNeg().hashCode();
        }
        if (getPerDsc() != null) {
            _hashCode += getPerDsc().hashCode();
        }
        if (getPerJrs() != null) {
            _hashCode += getPerJrs().hashCode();
        }
        if (getPerMul() != null) {
            _hashCode += getPerMul().hashCode();
        }
        if (getProJrs() != null) {
            _hashCode += getProJrs().hashCode();
        }
        if (getRateios() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRateios());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRateios(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSeqCgt() != null) {
            _hashCode += getSeqCgt().hashCode();
        }
        if (getSeqImo() != null) {
            _hashCode += getSeqImo().hashCode();
        }
        if (getSitTit() != null) {
            _hashCode += getSitTit().hashCode();
        }
        if (getSnfNfc() != null) {
            _hashCode += getSnfNfc().hashCode();
        }
        if (getSnfNfv() != null) {
            _hashCode += getSnfNfv().hashCode();
        }
        if (getTipEfe() != null) {
            _hashCode += getTipEfe().hashCode();
        }
        if (getTipJrs() != null) {
            _hashCode += getTipJrs().hashCode();
        }
        if (getTolDsc() != null) {
            _hashCode += getTolDsc().hashCode();
        }
        if (getTolJrs() != null) {
            _hashCode += getTolJrs().hashCode();
        }
        if (getTolMul() != null) {
            _hashCode += getTolMul().hashCode();
        }
        if (getUltPgt() != null) {
            _hashCode += getUltPgt().hashCode();
        }
        if (getVctOri() != null) {
            _hashCode += getVctOri().hashCode();
        }
        if (getVctPro() != null) {
            _hashCode += getVctPro().hashCode();
        }
        if (getVlrAbe() != null) {
            _hashCode += getVlrAbe().hashCode();
        }
        if (getVlrDsc() != null) {
            _hashCode += getVlrDsc().hashCode();
        }
        if (getVlrOri() != null) {
            _hashCode += getVlrOri().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosConsultarTitulosAbertosCPOutTitulos.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosConsultarTitulosAbertosCPOutTitulos"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("antDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "antDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCcu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCcu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCrt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCrt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFav");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFav"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFpg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFpg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFpj");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFpj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFrj");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFrj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMoe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMoe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codNtg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codNtg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTns");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTns"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cpgSub");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cpgSub"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaFin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaFin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaRed");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaRed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctrFre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctrFre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctrNre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctrNre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datEmi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datEmi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datPpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datPpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dscNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dscNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filCcr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "filCcr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filCtr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "filCtr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filNfc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "filNfc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filNff");
        elemField.setXmlName(new javax.xml.namespace.QName("", "filNff"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filNfv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "filNfv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filOcp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "filOcp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("forNfc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "forNfc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("forNff");
        elemField.setXmlName(new javax.xml.namespace.QName("", "forNff"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gerTep");
        elemField.setXmlName(new javax.xml.namespace.QName("", "gerTep"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("jrsDia");
        elemField.setXmlName(new javax.xml.namespace.QName("", "jrsDia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("jrsNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "jrsNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mulNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mulNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCcr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCcr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCtr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCtr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numNfc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numNfc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numNff");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numNff"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numNfv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numNfv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numOcp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numOcp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numPrj");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numPrj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numTit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numTit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsTcp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsTcp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ocpFre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ocpFre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ocpNre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ocpNre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "outNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perMul");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perMul"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("proJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "proJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rateios");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rateios"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosConsultarTitulosAbertosCPOutTitulosRateios"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqCgt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqCgt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqImo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqImo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitTit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitTit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("snfNfc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "snfNfc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("snfNfv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "snfNfv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipEfe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipEfe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tolDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tolDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tolJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tolJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tolMul");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tolMul"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ultPgt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ultPgt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vctOri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vctOri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vctPro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vctPro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrAbe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrAbe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrOri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrOri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
