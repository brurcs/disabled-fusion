/**
 * FichaBasicaIndicacaoModulosInIndicacaoModulos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.ficha.basica;

public class FichaBasicaIndicacaoModulosInIndicacaoModulos  implements java.io.Serializable {
    private java.lang.String codMod;

    private java.lang.String modSel;

    public FichaBasicaIndicacaoModulosInIndicacaoModulos() {
    }

    public FichaBasicaIndicacaoModulosInIndicacaoModulos(
           java.lang.String codMod,
           java.lang.String modSel) {
           this.codMod = codMod;
           this.modSel = modSel;
    }


    /**
     * Gets the codMod value for this FichaBasicaIndicacaoModulosInIndicacaoModulos.
     * 
     * @return codMod
     */
    public java.lang.String getCodMod() {
        return codMod;
    }


    /**
     * Sets the codMod value for this FichaBasicaIndicacaoModulosInIndicacaoModulos.
     * 
     * @param codMod
     */
    public void setCodMod(java.lang.String codMod) {
        this.codMod = codMod;
    }


    /**
     * Gets the modSel value for this FichaBasicaIndicacaoModulosInIndicacaoModulos.
     * 
     * @return modSel
     */
    public java.lang.String getModSel() {
        return modSel;
    }


    /**
     * Sets the modSel value for this FichaBasicaIndicacaoModulosInIndicacaoModulos.
     * 
     * @param modSel
     */
    public void setModSel(java.lang.String modSel) {
        this.modSel = modSel;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FichaBasicaIndicacaoModulosInIndicacaoModulos)) return false;
        FichaBasicaIndicacaoModulosInIndicacaoModulos other = (FichaBasicaIndicacaoModulosInIndicacaoModulos) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codMod==null && other.getCodMod()==null) || 
             (this.codMod!=null &&
              this.codMod.equals(other.getCodMod()))) &&
            ((this.modSel==null && other.getModSel()==null) || 
             (this.modSel!=null &&
              this.modSel.equals(other.getModSel())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodMod() != null) {
            _hashCode += getCodMod().hashCode();
        }
        if (getModSel() != null) {
            _hashCode += getModSel().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FichaBasicaIndicacaoModulosInIndicacaoModulos.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fichaBasicaIndicacaoModulosInIndicacaoModulos"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMod");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modSel");
        elemField.setXmlName(new javax.xml.namespace.QName("", "modSel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
