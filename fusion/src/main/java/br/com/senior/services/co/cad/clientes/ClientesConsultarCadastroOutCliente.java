/**
 * ClientesConsultarCadastroOutCliente.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package br.com.senior.services.co.cad.clientes;

public class ClientesConsultarCadastroOutCliente  implements java.io.Serializable {
    private java.lang.String apeCli;

    private java.lang.String baiCli;

    private java.lang.String baiCob;

    private java.lang.String baiEnt;

    private br.com.senior.services.co.cad.clientes.ClientesConsultarCadastroOutClienteCampoUsuarioCliente[] campoUsuarioCliente;

    private java.lang.Integer cepCli;

    private java.lang.Integer cepCob;

    private java.lang.Integer cepEnt;

    private java.lang.Double cgcCob;

    private java.lang.String cgcCpf;

    private java.lang.Double cgcEnt;

    private java.lang.String cidCli;

    private java.lang.String cidCob;

    private java.lang.String cidEnt;

    private java.lang.String cliPrx;

    private java.lang.Integer codCli;

    private java.lang.Integer codGre;

    private java.lang.String codPai;

    private java.lang.String codRam;

    private java.lang.String codRoe;

    private java.lang.String codSuf;

    private java.lang.String cplCob;

    private java.lang.String cplEnd;

    private java.lang.String cplEnt;

    private java.lang.Integer cxaPst;

    private java.lang.String eenCli;

    private java.lang.String eenCob;

    private java.lang.String eenEnt;

    private java.lang.String emaNfe;

    private java.lang.String endCli;

    private java.lang.String endCob;

    private java.lang.String endEnt;

    private java.lang.String entCor;

    private java.lang.String estCob;

    private java.lang.String estEnt;

    private java.lang.String faxCli;

    private java.lang.String fonCl2;

    private java.lang.String fonCl3;

    private java.lang.String fonCl4;

    private java.lang.String fonCl5;

    private java.lang.String fonCli;

    private br.com.senior.services.co.cad.clientes.ClientesConsultarCadastroOutClienteHistorico[] historico;

    private java.lang.Integer iniCob;

    private java.lang.Integer insAnp;

    private java.lang.String insEnt;

    private java.lang.String insEst;

    private java.lang.String insMun;

    private java.lang.String intNet;

    private java.lang.String marCli;

    private java.lang.String nenCli;

    private java.lang.String nenCob;

    private java.lang.String nenEnt;

    private java.lang.String nomCli;

    private java.lang.Integer seqRoe;

    private java.lang.String sigUfs;

    private java.lang.String sitCli;

    private java.lang.String temCob;

    private java.lang.String temEnt;

    private java.lang.String tipCli;

    private java.lang.Integer tipEmc;

    private java.lang.String tipMer;

    private java.lang.String zipCod;

    private java.lang.Integer zonFra;

    public ClientesConsultarCadastroOutCliente() {
    }

    public ClientesConsultarCadastroOutCliente(
           java.lang.String apeCli,
           java.lang.String baiCli,
           java.lang.String baiCob,
           java.lang.String baiEnt,
           br.com.senior.services.co.cad.clientes.ClientesConsultarCadastroOutClienteCampoUsuarioCliente[] campoUsuarioCliente,
           java.lang.Integer cepCli,
           java.lang.Integer cepCob,
           java.lang.Integer cepEnt,
           java.lang.Double cgcCob,
           java.lang.String cgcCpf,
           java.lang.Double cgcEnt,
           java.lang.String cidCli,
           java.lang.String cidCob,
           java.lang.String cidEnt,
           java.lang.String cliPrx,
           java.lang.Integer codCli,
           java.lang.Integer codGre,
           java.lang.String codPai,
           java.lang.String codRam,
           java.lang.String codRoe,
           java.lang.String codSuf,
           java.lang.String cplCob,
           java.lang.String cplEnd,
           java.lang.String cplEnt,
           java.lang.Integer cxaPst,
           java.lang.String eenCli,
           java.lang.String eenCob,
           java.lang.String eenEnt,
           java.lang.String emaNfe,
           java.lang.String endCli,
           java.lang.String endCob,
           java.lang.String endEnt,
           java.lang.String entCor,
           java.lang.String estCob,
           java.lang.String estEnt,
           java.lang.String faxCli,
           java.lang.String fonCl2,
           java.lang.String fonCl3,
           java.lang.String fonCl4,
           java.lang.String fonCl5,
           java.lang.String fonCli,
           br.com.senior.services.co.cad.clientes.ClientesConsultarCadastroOutClienteHistorico[] historico,
           java.lang.Integer iniCob,
           java.lang.Integer insAnp,
           java.lang.String insEnt,
           java.lang.String insEst,
           java.lang.String insMun,
           java.lang.String intNet,
           java.lang.String marCli,
           java.lang.String nenCli,
           java.lang.String nenCob,
           java.lang.String nenEnt,
           java.lang.String nomCli,
           java.lang.Integer seqRoe,
           java.lang.String sigUfs,
           java.lang.String sitCli,
           java.lang.String temCob,
           java.lang.String temEnt,
           java.lang.String tipCli,
           java.lang.Integer tipEmc,
           java.lang.String tipMer,
           java.lang.String zipCod,
           java.lang.Integer zonFra) {
           this.apeCli = apeCli;
           this.baiCli = baiCli;
           this.baiCob = baiCob;
           this.baiEnt = baiEnt;
           this.campoUsuarioCliente = campoUsuarioCliente;
           this.cepCli = cepCli;
           this.cepCob = cepCob;
           this.cepEnt = cepEnt;
           this.cgcCob = cgcCob;
           this.cgcCpf = cgcCpf;
           this.cgcEnt = cgcEnt;
           this.cidCli = cidCli;
           this.cidCob = cidCob;
           this.cidEnt = cidEnt;
           this.cliPrx = cliPrx;
           this.codCli = codCli;
           this.codGre = codGre;
           this.codPai = codPai;
           this.codRam = codRam;
           this.codRoe = codRoe;
           this.codSuf = codSuf;
           this.cplCob = cplCob;
           this.cplEnd = cplEnd;
           this.cplEnt = cplEnt;
           this.cxaPst = cxaPst;
           this.eenCli = eenCli;
           this.eenCob = eenCob;
           this.eenEnt = eenEnt;
           this.emaNfe = emaNfe;
           this.endCli = endCli;
           this.endCob = endCob;
           this.endEnt = endEnt;
           this.entCor = entCor;
           this.estCob = estCob;
           this.estEnt = estEnt;
           this.faxCli = faxCli;
           this.fonCl2 = fonCl2;
           this.fonCl3 = fonCl3;
           this.fonCl4 = fonCl4;
           this.fonCl5 = fonCl5;
           this.fonCli = fonCli;
           this.historico = historico;
           this.iniCob = iniCob;
           this.insAnp = insAnp;
           this.insEnt = insEnt;
           this.insEst = insEst;
           this.insMun = insMun;
           this.intNet = intNet;
           this.marCli = marCli;
           this.nenCli = nenCli;
           this.nenCob = nenCob;
           this.nenEnt = nenEnt;
           this.nomCli = nomCli;
           this.seqRoe = seqRoe;
           this.sigUfs = sigUfs;
           this.sitCli = sitCli;
           this.temCob = temCob;
           this.temEnt = temEnt;
           this.tipCli = tipCli;
           this.tipEmc = tipEmc;
           this.tipMer = tipMer;
           this.zipCod = zipCod;
           this.zonFra = zonFra;
    }


    /**
     * Gets the apeCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return apeCli
     */
    public java.lang.String getApeCli() {
        return apeCli;
    }


    /**
     * Sets the apeCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param apeCli
     */
    public void setApeCli(java.lang.String apeCli) {
        this.apeCli = apeCli;
    }


    /**
     * Gets the baiCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return baiCli
     */
    public java.lang.String getBaiCli() {
        return baiCli;
    }


    /**
     * Sets the baiCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param baiCli
     */
    public void setBaiCli(java.lang.String baiCli) {
        this.baiCli = baiCli;
    }


    /**
     * Gets the baiCob value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return baiCob
     */
    public java.lang.String getBaiCob() {
        return baiCob;
    }


    /**
     * Sets the baiCob value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param baiCob
     */
    public void setBaiCob(java.lang.String baiCob) {
        this.baiCob = baiCob;
    }


    /**
     * Gets the baiEnt value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return baiEnt
     */
    public java.lang.String getBaiEnt() {
        return baiEnt;
    }


    /**
     * Sets the baiEnt value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param baiEnt
     */
    public void setBaiEnt(java.lang.String baiEnt) {
        this.baiEnt = baiEnt;
    }


    /**
     * Gets the campoUsuarioCliente value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return campoUsuarioCliente
     */
    public br.com.senior.services.co.cad.clientes.ClientesConsultarCadastroOutClienteCampoUsuarioCliente[] getCampoUsuarioCliente() {
        return campoUsuarioCliente;
    }


    /**
     * Sets the campoUsuarioCliente value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param campoUsuarioCliente
     */
    public void setCampoUsuarioCliente(br.com.senior.services.co.cad.clientes.ClientesConsultarCadastroOutClienteCampoUsuarioCliente[] campoUsuarioCliente) {
        this.campoUsuarioCliente = campoUsuarioCliente;
    }

    public br.com.senior.services.co.cad.clientes.ClientesConsultarCadastroOutClienteCampoUsuarioCliente getCampoUsuarioCliente(int i) {
        return this.campoUsuarioCliente[i];
    }

    public void setCampoUsuarioCliente(int i, br.com.senior.services.co.cad.clientes.ClientesConsultarCadastroOutClienteCampoUsuarioCliente _value) {
        this.campoUsuarioCliente[i] = _value;
    }


    /**
     * Gets the cepCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return cepCli
     */
    public java.lang.Integer getCepCli() {
        return cepCli;
    }


    /**
     * Sets the cepCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param cepCli
     */
    public void setCepCli(java.lang.Integer cepCli) {
        this.cepCli = cepCli;
    }


    /**
     * Gets the cepCob value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return cepCob
     */
    public java.lang.Integer getCepCob() {
        return cepCob;
    }


    /**
     * Sets the cepCob value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param cepCob
     */
    public void setCepCob(java.lang.Integer cepCob) {
        this.cepCob = cepCob;
    }


    /**
     * Gets the cepEnt value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return cepEnt
     */
    public java.lang.Integer getCepEnt() {
        return cepEnt;
    }


    /**
     * Sets the cepEnt value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param cepEnt
     */
    public void setCepEnt(java.lang.Integer cepEnt) {
        this.cepEnt = cepEnt;
    }


    /**
     * Gets the cgcCob value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return cgcCob
     */
    public java.lang.Double getCgcCob() {
        return cgcCob;
    }


    /**
     * Sets the cgcCob value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param cgcCob
     */
    public void setCgcCob(java.lang.Double cgcCob) {
        this.cgcCob = cgcCob;
    }


    /**
     * Gets the cgcCpf value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return cgcCpf
     */
    public java.lang.String getCgcCpf() {
        return cgcCpf;
    }


    /**
     * Sets the cgcCpf value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param cgcCpf
     */
    public void setCgcCpf(java.lang.String cgcCpf) {
        this.cgcCpf = cgcCpf;
    }


    /**
     * Gets the cgcEnt value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return cgcEnt
     */
    public java.lang.Double getCgcEnt() {
        return cgcEnt;
    }


    /**
     * Sets the cgcEnt value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param cgcEnt
     */
    public void setCgcEnt(java.lang.Double cgcEnt) {
        this.cgcEnt = cgcEnt;
    }


    /**
     * Gets the cidCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return cidCli
     */
    public java.lang.String getCidCli() {
        return cidCli;
    }


    /**
     * Sets the cidCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param cidCli
     */
    public void setCidCli(java.lang.String cidCli) {
        this.cidCli = cidCli;
    }


    /**
     * Gets the cidCob value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return cidCob
     */
    public java.lang.String getCidCob() {
        return cidCob;
    }


    /**
     * Sets the cidCob value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param cidCob
     */
    public void setCidCob(java.lang.String cidCob) {
        this.cidCob = cidCob;
    }


    /**
     * Gets the cidEnt value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return cidEnt
     */
    public java.lang.String getCidEnt() {
        return cidEnt;
    }


    /**
     * Sets the cidEnt value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param cidEnt
     */
    public void setCidEnt(java.lang.String cidEnt) {
        this.cidEnt = cidEnt;
    }


    /**
     * Gets the cliPrx value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return cliPrx
     */
    public java.lang.String getCliPrx() {
        return cliPrx;
    }


    /**
     * Sets the cliPrx value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param cliPrx
     */
    public void setCliPrx(java.lang.String cliPrx) {
        this.cliPrx = cliPrx;
    }


    /**
     * Gets the codCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return codCli
     */
    public java.lang.Integer getCodCli() {
        return codCli;
    }


    /**
     * Sets the codCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param codCli
     */
    public void setCodCli(java.lang.Integer codCli) {
        this.codCli = codCli;
    }


    /**
     * Gets the codGre value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return codGre
     */
    public java.lang.Integer getCodGre() {
        return codGre;
    }


    /**
     * Sets the codGre value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param codGre
     */
    public void setCodGre(java.lang.Integer codGre) {
        this.codGre = codGre;
    }


    /**
     * Gets the codPai value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return codPai
     */
    public java.lang.String getCodPai() {
        return codPai;
    }


    /**
     * Sets the codPai value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param codPai
     */
    public void setCodPai(java.lang.String codPai) {
        this.codPai = codPai;
    }


    /**
     * Gets the codRam value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return codRam
     */
    public java.lang.String getCodRam() {
        return codRam;
    }


    /**
     * Sets the codRam value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param codRam
     */
    public void setCodRam(java.lang.String codRam) {
        this.codRam = codRam;
    }


    /**
     * Gets the codRoe value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return codRoe
     */
    public java.lang.String getCodRoe() {
        return codRoe;
    }


    /**
     * Sets the codRoe value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param codRoe
     */
    public void setCodRoe(java.lang.String codRoe) {
        this.codRoe = codRoe;
    }


    /**
     * Gets the codSuf value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return codSuf
     */
    public java.lang.String getCodSuf() {
        return codSuf;
    }


    /**
     * Sets the codSuf value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param codSuf
     */
    public void setCodSuf(java.lang.String codSuf) {
        this.codSuf = codSuf;
    }


    /**
     * Gets the cplCob value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return cplCob
     */
    public java.lang.String getCplCob() {
        return cplCob;
    }


    /**
     * Sets the cplCob value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param cplCob
     */
    public void setCplCob(java.lang.String cplCob) {
        this.cplCob = cplCob;
    }


    /**
     * Gets the cplEnd value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return cplEnd
     */
    public java.lang.String getCplEnd() {
        return cplEnd;
    }


    /**
     * Sets the cplEnd value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param cplEnd
     */
    public void setCplEnd(java.lang.String cplEnd) {
        this.cplEnd = cplEnd;
    }


    /**
     * Gets the cplEnt value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return cplEnt
     */
    public java.lang.String getCplEnt() {
        return cplEnt;
    }


    /**
     * Sets the cplEnt value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param cplEnt
     */
    public void setCplEnt(java.lang.String cplEnt) {
        this.cplEnt = cplEnt;
    }


    /**
     * Gets the cxaPst value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return cxaPst
     */
    public java.lang.Integer getCxaPst() {
        return cxaPst;
    }


    /**
     * Sets the cxaPst value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param cxaPst
     */
    public void setCxaPst(java.lang.Integer cxaPst) {
        this.cxaPst = cxaPst;
    }


    /**
     * Gets the eenCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return eenCli
     */
    public java.lang.String getEenCli() {
        return eenCli;
    }


    /**
     * Sets the eenCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param eenCli
     */
    public void setEenCli(java.lang.String eenCli) {
        this.eenCli = eenCli;
    }


    /**
     * Gets the eenCob value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return eenCob
     */
    public java.lang.String getEenCob() {
        return eenCob;
    }


    /**
     * Sets the eenCob value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param eenCob
     */
    public void setEenCob(java.lang.String eenCob) {
        this.eenCob = eenCob;
    }


    /**
     * Gets the eenEnt value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return eenEnt
     */
    public java.lang.String getEenEnt() {
        return eenEnt;
    }


    /**
     * Sets the eenEnt value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param eenEnt
     */
    public void setEenEnt(java.lang.String eenEnt) {
        this.eenEnt = eenEnt;
    }


    /**
     * Gets the emaNfe value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return emaNfe
     */
    public java.lang.String getEmaNfe() {
        return emaNfe;
    }


    /**
     * Sets the emaNfe value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param emaNfe
     */
    public void setEmaNfe(java.lang.String emaNfe) {
        this.emaNfe = emaNfe;
    }


    /**
     * Gets the endCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return endCli
     */
    public java.lang.String getEndCli() {
        return endCli;
    }


    /**
     * Sets the endCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param endCli
     */
    public void setEndCli(java.lang.String endCli) {
        this.endCli = endCli;
    }


    /**
     * Gets the endCob value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return endCob
     */
    public java.lang.String getEndCob() {
        return endCob;
    }


    /**
     * Sets the endCob value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param endCob
     */
    public void setEndCob(java.lang.String endCob) {
        this.endCob = endCob;
    }


    /**
     * Gets the endEnt value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return endEnt
     */
    public java.lang.String getEndEnt() {
        return endEnt;
    }


    /**
     * Sets the endEnt value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param endEnt
     */
    public void setEndEnt(java.lang.String endEnt) {
        this.endEnt = endEnt;
    }


    /**
     * Gets the entCor value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return entCor
     */
    public java.lang.String getEntCor() {
        return entCor;
    }


    /**
     * Sets the entCor value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param entCor
     */
    public void setEntCor(java.lang.String entCor) {
        this.entCor = entCor;
    }


    /**
     * Gets the estCob value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return estCob
     */
    public java.lang.String getEstCob() {
        return estCob;
    }


    /**
     * Sets the estCob value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param estCob
     */
    public void setEstCob(java.lang.String estCob) {
        this.estCob = estCob;
    }


    /**
     * Gets the estEnt value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return estEnt
     */
    public java.lang.String getEstEnt() {
        return estEnt;
    }


    /**
     * Sets the estEnt value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param estEnt
     */
    public void setEstEnt(java.lang.String estEnt) {
        this.estEnt = estEnt;
    }


    /**
     * Gets the faxCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return faxCli
     */
    public java.lang.String getFaxCli() {
        return faxCli;
    }


    /**
     * Sets the faxCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param faxCli
     */
    public void setFaxCli(java.lang.String faxCli) {
        this.faxCli = faxCli;
    }


    /**
     * Gets the fonCl2 value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return fonCl2
     */
    public java.lang.String getFonCl2() {
        return fonCl2;
    }


    /**
     * Sets the fonCl2 value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param fonCl2
     */
    public void setFonCl2(java.lang.String fonCl2) {
        this.fonCl2 = fonCl2;
    }


    /**
     * Gets the fonCl3 value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return fonCl3
     */
    public java.lang.String getFonCl3() {
        return fonCl3;
    }


    /**
     * Sets the fonCl3 value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param fonCl3
     */
    public void setFonCl3(java.lang.String fonCl3) {
        this.fonCl3 = fonCl3;
    }


    /**
     * Gets the fonCl4 value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return fonCl4
     */
    public java.lang.String getFonCl4() {
        return fonCl4;
    }


    /**
     * Sets the fonCl4 value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param fonCl4
     */
    public void setFonCl4(java.lang.String fonCl4) {
        this.fonCl4 = fonCl4;
    }


    /**
     * Gets the fonCl5 value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return fonCl5
     */
    public java.lang.String getFonCl5() {
        return fonCl5;
    }


    /**
     * Sets the fonCl5 value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param fonCl5
     */
    public void setFonCl5(java.lang.String fonCl5) {
        this.fonCl5 = fonCl5;
    }


    /**
     * Gets the fonCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return fonCli
     */
    public java.lang.String getFonCli() {
        return fonCli;
    }


    /**
     * Sets the fonCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param fonCli
     */
    public void setFonCli(java.lang.String fonCli) {
        this.fonCli = fonCli;
    }


    /**
     * Gets the historico value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return historico
     */
    public br.com.senior.services.co.cad.clientes.ClientesConsultarCadastroOutClienteHistorico[] getHistorico() {
        return historico;
    }


    /**
     * Sets the historico value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param historico
     */
    public void setHistorico(br.com.senior.services.co.cad.clientes.ClientesConsultarCadastroOutClienteHistorico[] historico) {
        this.historico = historico;
    }

    public br.com.senior.services.co.cad.clientes.ClientesConsultarCadastroOutClienteHistorico getHistorico(int i) {
        return this.historico[i];
    }

    public void setHistorico(int i, br.com.senior.services.co.cad.clientes.ClientesConsultarCadastroOutClienteHistorico _value) {
        this.historico[i] = _value;
    }


    /**
     * Gets the iniCob value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return iniCob
     */
    public java.lang.Integer getIniCob() {
        return iniCob;
    }


    /**
     * Sets the iniCob value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param iniCob
     */
    public void setIniCob(java.lang.Integer iniCob) {
        this.iniCob = iniCob;
    }


    /**
     * Gets the insAnp value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return insAnp
     */
    public java.lang.Integer getInsAnp() {
        return insAnp;
    }


    /**
     * Sets the insAnp value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param insAnp
     */
    public void setInsAnp(java.lang.Integer insAnp) {
        this.insAnp = insAnp;
    }


    /**
     * Gets the insEnt value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return insEnt
     */
    public java.lang.String getInsEnt() {
        return insEnt;
    }


    /**
     * Sets the insEnt value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param insEnt
     */
    public void setInsEnt(java.lang.String insEnt) {
        this.insEnt = insEnt;
    }


    /**
     * Gets the insEst value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return insEst
     */
    public java.lang.String getInsEst() {
        return insEst;
    }


    /**
     * Sets the insEst value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param insEst
     */
    public void setInsEst(java.lang.String insEst) {
        this.insEst = insEst;
    }


    /**
     * Gets the insMun value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return insMun
     */
    public java.lang.String getInsMun() {
        return insMun;
    }


    /**
     * Sets the insMun value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param insMun
     */
    public void setInsMun(java.lang.String insMun) {
        this.insMun = insMun;
    }


    /**
     * Gets the intNet value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return intNet
     */
    public java.lang.String getIntNet() {
        return intNet;
    }


    /**
     * Sets the intNet value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param intNet
     */
    public void setIntNet(java.lang.String intNet) {
        this.intNet = intNet;
    }


    /**
     * Gets the marCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return marCli
     */
    public java.lang.String getMarCli() {
        return marCli;
    }


    /**
     * Sets the marCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param marCli
     */
    public void setMarCli(java.lang.String marCli) {
        this.marCli = marCli;
    }


    /**
     * Gets the nenCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return nenCli
     */
    public java.lang.String getNenCli() {
        return nenCli;
    }


    /**
     * Sets the nenCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param nenCli
     */
    public void setNenCli(java.lang.String nenCli) {
        this.nenCli = nenCli;
    }


    /**
     * Gets the nenCob value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return nenCob
     */
    public java.lang.String getNenCob() {
        return nenCob;
    }


    /**
     * Sets the nenCob value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param nenCob
     */
    public void setNenCob(java.lang.String nenCob) {
        this.nenCob = nenCob;
    }


    /**
     * Gets the nenEnt value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return nenEnt
     */
    public java.lang.String getNenEnt() {
        return nenEnt;
    }


    /**
     * Sets the nenEnt value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param nenEnt
     */
    public void setNenEnt(java.lang.String nenEnt) {
        this.nenEnt = nenEnt;
    }


    /**
     * Gets the nomCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return nomCli
     */
    public java.lang.String getNomCli() {
        return nomCli;
    }


    /**
     * Sets the nomCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param nomCli
     */
    public void setNomCli(java.lang.String nomCli) {
        this.nomCli = nomCli;
    }


    /**
     * Gets the seqRoe value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return seqRoe
     */
    public java.lang.Integer getSeqRoe() {
        return seqRoe;
    }


    /**
     * Sets the seqRoe value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param seqRoe
     */
    public void setSeqRoe(java.lang.Integer seqRoe) {
        this.seqRoe = seqRoe;
    }


    /**
     * Gets the sigUfs value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return sigUfs
     */
    public java.lang.String getSigUfs() {
        return sigUfs;
    }


    /**
     * Sets the sigUfs value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param sigUfs
     */
    public void setSigUfs(java.lang.String sigUfs) {
        this.sigUfs = sigUfs;
    }


    /**
     * Gets the sitCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return sitCli
     */
    public java.lang.String getSitCli() {
        return sitCli;
    }


    /**
     * Sets the sitCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param sitCli
     */
    public void setSitCli(java.lang.String sitCli) {
        this.sitCli = sitCli;
    }


    /**
     * Gets the temCob value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return temCob
     */
    public java.lang.String getTemCob() {
        return temCob;
    }


    /**
     * Sets the temCob value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param temCob
     */
    public void setTemCob(java.lang.String temCob) {
        this.temCob = temCob;
    }


    /**
     * Gets the temEnt value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return temEnt
     */
    public java.lang.String getTemEnt() {
        return temEnt;
    }


    /**
     * Sets the temEnt value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param temEnt
     */
    public void setTemEnt(java.lang.String temEnt) {
        this.temEnt = temEnt;
    }


    /**
     * Gets the tipCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return tipCli
     */
    public java.lang.String getTipCli() {
        return tipCli;
    }


    /**
     * Sets the tipCli value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param tipCli
     */
    public void setTipCli(java.lang.String tipCli) {
        this.tipCli = tipCli;
    }


    /**
     * Gets the tipEmc value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return tipEmc
     */
    public java.lang.Integer getTipEmc() {
        return tipEmc;
    }


    /**
     * Sets the tipEmc value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param tipEmc
     */
    public void setTipEmc(java.lang.Integer tipEmc) {
        this.tipEmc = tipEmc;
    }


    /**
     * Gets the tipMer value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return tipMer
     */
    public java.lang.String getTipMer() {
        return tipMer;
    }


    /**
     * Sets the tipMer value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param tipMer
     */
    public void setTipMer(java.lang.String tipMer) {
        this.tipMer = tipMer;
    }


    /**
     * Gets the zipCod value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return zipCod
     */
    public java.lang.String getZipCod() {
        return zipCod;
    }


    /**
     * Sets the zipCod value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param zipCod
     */
    public void setZipCod(java.lang.String zipCod) {
        this.zipCod = zipCod;
    }


    /**
     * Gets the zonFra value for this ClientesConsultarCadastroOutCliente.
     * 
     * @return zonFra
     */
    public java.lang.Integer getZonFra() {
        return zonFra;
    }


    /**
     * Sets the zonFra value for this ClientesConsultarCadastroOutCliente.
     * 
     * @param zonFra
     */
    public void setZonFra(java.lang.Integer zonFra) {
        this.zonFra = zonFra;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientesConsultarCadastroOutCliente)) return false;
        ClientesConsultarCadastroOutCliente other = (ClientesConsultarCadastroOutCliente) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.apeCli==null && other.getApeCli()==null) || 
             (this.apeCli!=null &&
              this.apeCli.equals(other.getApeCli()))) &&
            ((this.baiCli==null && other.getBaiCli()==null) || 
             (this.baiCli!=null &&
              this.baiCli.equals(other.getBaiCli()))) &&
            ((this.baiCob==null && other.getBaiCob()==null) || 
             (this.baiCob!=null &&
              this.baiCob.equals(other.getBaiCob()))) &&
            ((this.baiEnt==null && other.getBaiEnt()==null) || 
             (this.baiEnt!=null &&
              this.baiEnt.equals(other.getBaiEnt()))) &&
            ((this.campoUsuarioCliente==null && other.getCampoUsuarioCliente()==null) || 
             (this.campoUsuarioCliente!=null &&
              java.util.Arrays.equals(this.campoUsuarioCliente, other.getCampoUsuarioCliente()))) &&
            ((this.cepCli==null && other.getCepCli()==null) || 
             (this.cepCli!=null &&
              this.cepCli.equals(other.getCepCli()))) &&
            ((this.cepCob==null && other.getCepCob()==null) || 
             (this.cepCob!=null &&
              this.cepCob.equals(other.getCepCob()))) &&
            ((this.cepEnt==null && other.getCepEnt()==null) || 
             (this.cepEnt!=null &&
              this.cepEnt.equals(other.getCepEnt()))) &&
            ((this.cgcCob==null && other.getCgcCob()==null) || 
             (this.cgcCob!=null &&
              this.cgcCob.equals(other.getCgcCob()))) &&
            ((this.cgcCpf==null && other.getCgcCpf()==null) || 
             (this.cgcCpf!=null &&
              this.cgcCpf.equals(other.getCgcCpf()))) &&
            ((this.cgcEnt==null && other.getCgcEnt()==null) || 
             (this.cgcEnt!=null &&
              this.cgcEnt.equals(other.getCgcEnt()))) &&
            ((this.cidCli==null && other.getCidCli()==null) || 
             (this.cidCli!=null &&
              this.cidCli.equals(other.getCidCli()))) &&
            ((this.cidCob==null && other.getCidCob()==null) || 
             (this.cidCob!=null &&
              this.cidCob.equals(other.getCidCob()))) &&
            ((this.cidEnt==null && other.getCidEnt()==null) || 
             (this.cidEnt!=null &&
              this.cidEnt.equals(other.getCidEnt()))) &&
            ((this.cliPrx==null && other.getCliPrx()==null) || 
             (this.cliPrx!=null &&
              this.cliPrx.equals(other.getCliPrx()))) &&
            ((this.codCli==null && other.getCodCli()==null) || 
             (this.codCli!=null &&
              this.codCli.equals(other.getCodCli()))) &&
            ((this.codGre==null && other.getCodGre()==null) || 
             (this.codGre!=null &&
              this.codGre.equals(other.getCodGre()))) &&
            ((this.codPai==null && other.getCodPai()==null) || 
             (this.codPai!=null &&
              this.codPai.equals(other.getCodPai()))) &&
            ((this.codRam==null && other.getCodRam()==null) || 
             (this.codRam!=null &&
              this.codRam.equals(other.getCodRam()))) &&
            ((this.codRoe==null && other.getCodRoe()==null) || 
             (this.codRoe!=null &&
              this.codRoe.equals(other.getCodRoe()))) &&
            ((this.codSuf==null && other.getCodSuf()==null) || 
             (this.codSuf!=null &&
              this.codSuf.equals(other.getCodSuf()))) &&
            ((this.cplCob==null && other.getCplCob()==null) || 
             (this.cplCob!=null &&
              this.cplCob.equals(other.getCplCob()))) &&
            ((this.cplEnd==null && other.getCplEnd()==null) || 
             (this.cplEnd!=null &&
              this.cplEnd.equals(other.getCplEnd()))) &&
            ((this.cplEnt==null && other.getCplEnt()==null) || 
             (this.cplEnt!=null &&
              this.cplEnt.equals(other.getCplEnt()))) &&
            ((this.cxaPst==null && other.getCxaPst()==null) || 
             (this.cxaPst!=null &&
              this.cxaPst.equals(other.getCxaPst()))) &&
            ((this.eenCli==null && other.getEenCli()==null) || 
             (this.eenCli!=null &&
              this.eenCli.equals(other.getEenCli()))) &&
            ((this.eenCob==null && other.getEenCob()==null) || 
             (this.eenCob!=null &&
              this.eenCob.equals(other.getEenCob()))) &&
            ((this.eenEnt==null && other.getEenEnt()==null) || 
             (this.eenEnt!=null &&
              this.eenEnt.equals(other.getEenEnt()))) &&
            ((this.emaNfe==null && other.getEmaNfe()==null) || 
             (this.emaNfe!=null &&
              this.emaNfe.equals(other.getEmaNfe()))) &&
            ((this.endCli==null && other.getEndCli()==null) || 
             (this.endCli!=null &&
              this.endCli.equals(other.getEndCli()))) &&
            ((this.endCob==null && other.getEndCob()==null) || 
             (this.endCob!=null &&
              this.endCob.equals(other.getEndCob()))) &&
            ((this.endEnt==null && other.getEndEnt()==null) || 
             (this.endEnt!=null &&
              this.endEnt.equals(other.getEndEnt()))) &&
            ((this.entCor==null && other.getEntCor()==null) || 
             (this.entCor!=null &&
              this.entCor.equals(other.getEntCor()))) &&
            ((this.estCob==null && other.getEstCob()==null) || 
             (this.estCob!=null &&
              this.estCob.equals(other.getEstCob()))) &&
            ((this.estEnt==null && other.getEstEnt()==null) || 
             (this.estEnt!=null &&
              this.estEnt.equals(other.getEstEnt()))) &&
            ((this.faxCli==null && other.getFaxCli()==null) || 
             (this.faxCli!=null &&
              this.faxCli.equals(other.getFaxCli()))) &&
            ((this.fonCl2==null && other.getFonCl2()==null) || 
             (this.fonCl2!=null &&
              this.fonCl2.equals(other.getFonCl2()))) &&
            ((this.fonCl3==null && other.getFonCl3()==null) || 
             (this.fonCl3!=null &&
              this.fonCl3.equals(other.getFonCl3()))) &&
            ((this.fonCl4==null && other.getFonCl4()==null) || 
             (this.fonCl4!=null &&
              this.fonCl4.equals(other.getFonCl4()))) &&
            ((this.fonCl5==null && other.getFonCl5()==null) || 
             (this.fonCl5!=null &&
              this.fonCl5.equals(other.getFonCl5()))) &&
            ((this.fonCli==null && other.getFonCli()==null) || 
             (this.fonCli!=null &&
              this.fonCli.equals(other.getFonCli()))) &&
            ((this.historico==null && other.getHistorico()==null) || 
             (this.historico!=null &&
              java.util.Arrays.equals(this.historico, other.getHistorico()))) &&
            ((this.iniCob==null && other.getIniCob()==null) || 
             (this.iniCob!=null &&
              this.iniCob.equals(other.getIniCob()))) &&
            ((this.insAnp==null && other.getInsAnp()==null) || 
             (this.insAnp!=null &&
              this.insAnp.equals(other.getInsAnp()))) &&
            ((this.insEnt==null && other.getInsEnt()==null) || 
             (this.insEnt!=null &&
              this.insEnt.equals(other.getInsEnt()))) &&
            ((this.insEst==null && other.getInsEst()==null) || 
             (this.insEst!=null &&
              this.insEst.equals(other.getInsEst()))) &&
            ((this.insMun==null && other.getInsMun()==null) || 
             (this.insMun!=null &&
              this.insMun.equals(other.getInsMun()))) &&
            ((this.intNet==null && other.getIntNet()==null) || 
             (this.intNet!=null &&
              this.intNet.equals(other.getIntNet()))) &&
            ((this.marCli==null && other.getMarCli()==null) || 
             (this.marCli!=null &&
              this.marCli.equals(other.getMarCli()))) &&
            ((this.nenCli==null && other.getNenCli()==null) || 
             (this.nenCli!=null &&
              this.nenCli.equals(other.getNenCli()))) &&
            ((this.nenCob==null && other.getNenCob()==null) || 
             (this.nenCob!=null &&
              this.nenCob.equals(other.getNenCob()))) &&
            ((this.nenEnt==null && other.getNenEnt()==null) || 
             (this.nenEnt!=null &&
              this.nenEnt.equals(other.getNenEnt()))) &&
            ((this.nomCli==null && other.getNomCli()==null) || 
             (this.nomCli!=null &&
              this.nomCli.equals(other.getNomCli()))) &&
            ((this.seqRoe==null && other.getSeqRoe()==null) || 
             (this.seqRoe!=null &&
              this.seqRoe.equals(other.getSeqRoe()))) &&
            ((this.sigUfs==null && other.getSigUfs()==null) || 
             (this.sigUfs!=null &&
              this.sigUfs.equals(other.getSigUfs()))) &&
            ((this.sitCli==null && other.getSitCli()==null) || 
             (this.sitCli!=null &&
              this.sitCli.equals(other.getSitCli()))) &&
            ((this.temCob==null && other.getTemCob()==null) || 
             (this.temCob!=null &&
              this.temCob.equals(other.getTemCob()))) &&
            ((this.temEnt==null && other.getTemEnt()==null) || 
             (this.temEnt!=null &&
              this.temEnt.equals(other.getTemEnt()))) &&
            ((this.tipCli==null && other.getTipCli()==null) || 
             (this.tipCli!=null &&
              this.tipCli.equals(other.getTipCli()))) &&
            ((this.tipEmc==null && other.getTipEmc()==null) || 
             (this.tipEmc!=null &&
              this.tipEmc.equals(other.getTipEmc()))) &&
            ((this.tipMer==null && other.getTipMer()==null) || 
             (this.tipMer!=null &&
              this.tipMer.equals(other.getTipMer()))) &&
            ((this.zipCod==null && other.getZipCod()==null) || 
             (this.zipCod!=null &&
              this.zipCod.equals(other.getZipCod()))) &&
            ((this.zonFra==null && other.getZonFra()==null) || 
             (this.zonFra!=null &&
              this.zonFra.equals(other.getZonFra())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getApeCli() != null) {
            _hashCode += getApeCli().hashCode();
        }
        if (getBaiCli() != null) {
            _hashCode += getBaiCli().hashCode();
        }
        if (getBaiCob() != null) {
            _hashCode += getBaiCob().hashCode();
        }
        if (getBaiEnt() != null) {
            _hashCode += getBaiEnt().hashCode();
        }
        if (getCampoUsuarioCliente() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCampoUsuarioCliente());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCampoUsuarioCliente(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCepCli() != null) {
            _hashCode += getCepCli().hashCode();
        }
        if (getCepCob() != null) {
            _hashCode += getCepCob().hashCode();
        }
        if (getCepEnt() != null) {
            _hashCode += getCepEnt().hashCode();
        }
        if (getCgcCob() != null) {
            _hashCode += getCgcCob().hashCode();
        }
        if (getCgcCpf() != null) {
            _hashCode += getCgcCpf().hashCode();
        }
        if (getCgcEnt() != null) {
            _hashCode += getCgcEnt().hashCode();
        }
        if (getCidCli() != null) {
            _hashCode += getCidCli().hashCode();
        }
        if (getCidCob() != null) {
            _hashCode += getCidCob().hashCode();
        }
        if (getCidEnt() != null) {
            _hashCode += getCidEnt().hashCode();
        }
        if (getCliPrx() != null) {
            _hashCode += getCliPrx().hashCode();
        }
        if (getCodCli() != null) {
            _hashCode += getCodCli().hashCode();
        }
        if (getCodGre() != null) {
            _hashCode += getCodGre().hashCode();
        }
        if (getCodPai() != null) {
            _hashCode += getCodPai().hashCode();
        }
        if (getCodRam() != null) {
            _hashCode += getCodRam().hashCode();
        }
        if (getCodRoe() != null) {
            _hashCode += getCodRoe().hashCode();
        }
        if (getCodSuf() != null) {
            _hashCode += getCodSuf().hashCode();
        }
        if (getCplCob() != null) {
            _hashCode += getCplCob().hashCode();
        }
        if (getCplEnd() != null) {
            _hashCode += getCplEnd().hashCode();
        }
        if (getCplEnt() != null) {
            _hashCode += getCplEnt().hashCode();
        }
        if (getCxaPst() != null) {
            _hashCode += getCxaPst().hashCode();
        }
        if (getEenCli() != null) {
            _hashCode += getEenCli().hashCode();
        }
        if (getEenCob() != null) {
            _hashCode += getEenCob().hashCode();
        }
        if (getEenEnt() != null) {
            _hashCode += getEenEnt().hashCode();
        }
        if (getEmaNfe() != null) {
            _hashCode += getEmaNfe().hashCode();
        }
        if (getEndCli() != null) {
            _hashCode += getEndCli().hashCode();
        }
        if (getEndCob() != null) {
            _hashCode += getEndCob().hashCode();
        }
        if (getEndEnt() != null) {
            _hashCode += getEndEnt().hashCode();
        }
        if (getEntCor() != null) {
            _hashCode += getEntCor().hashCode();
        }
        if (getEstCob() != null) {
            _hashCode += getEstCob().hashCode();
        }
        if (getEstEnt() != null) {
            _hashCode += getEstEnt().hashCode();
        }
        if (getFaxCli() != null) {
            _hashCode += getFaxCli().hashCode();
        }
        if (getFonCl2() != null) {
            _hashCode += getFonCl2().hashCode();
        }
        if (getFonCl3() != null) {
            _hashCode += getFonCl3().hashCode();
        }
        if (getFonCl4() != null) {
            _hashCode += getFonCl4().hashCode();
        }
        if (getFonCl5() != null) {
            _hashCode += getFonCl5().hashCode();
        }
        if (getFonCli() != null) {
            _hashCode += getFonCli().hashCode();
        }
        if (getHistorico() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getHistorico());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getHistorico(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getIniCob() != null) {
            _hashCode += getIniCob().hashCode();
        }
        if (getInsAnp() != null) {
            _hashCode += getInsAnp().hashCode();
        }
        if (getInsEnt() != null) {
            _hashCode += getInsEnt().hashCode();
        }
        if (getInsEst() != null) {
            _hashCode += getInsEst().hashCode();
        }
        if (getInsMun() != null) {
            _hashCode += getInsMun().hashCode();
        }
        if (getIntNet() != null) {
            _hashCode += getIntNet().hashCode();
        }
        if (getMarCli() != null) {
            _hashCode += getMarCli().hashCode();
        }
        if (getNenCli() != null) {
            _hashCode += getNenCli().hashCode();
        }
        if (getNenCob() != null) {
            _hashCode += getNenCob().hashCode();
        }
        if (getNenEnt() != null) {
            _hashCode += getNenEnt().hashCode();
        }
        if (getNomCli() != null) {
            _hashCode += getNomCli().hashCode();
        }
        if (getSeqRoe() != null) {
            _hashCode += getSeqRoe().hashCode();
        }
        if (getSigUfs() != null) {
            _hashCode += getSigUfs().hashCode();
        }
        if (getSitCli() != null) {
            _hashCode += getSitCli().hashCode();
        }
        if (getTemCob() != null) {
            _hashCode += getTemCob().hashCode();
        }
        if (getTemEnt() != null) {
            _hashCode += getTemEnt().hashCode();
        }
        if (getTipCli() != null) {
            _hashCode += getTipCli().hashCode();
        }
        if (getTipEmc() != null) {
            _hashCode += getTipEmc().hashCode();
        }
        if (getTipMer() != null) {
            _hashCode += getTipMer().hashCode();
        }
        if (getZipCod() != null) {
            _hashCode += getZipCod().hashCode();
        }
        if (getZonFra() != null) {
            _hashCode += getZonFra().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientesConsultarCadastroOutCliente.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesConsultarCadastroOutCliente"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("apeCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "apeCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baiCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "baiCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baiCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "baiCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baiEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "baiEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("campoUsuarioCliente");
        elemField.setXmlName(new javax.xml.namespace.QName("", "campoUsuarioCliente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesConsultarCadastroOutClienteCampoUsuarioCliente"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cepCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cepCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cepEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cgcCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cgcCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cgcCpf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cgcCpf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cgcEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cgcEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cidCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cidCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cidCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cidCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cidEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cidEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cliPrx");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cliPrx"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codGre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codGre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPai");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPai"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRam");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRam"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRoe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRoe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codSuf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codSuf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cplCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cplCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cplEnd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cplEnd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cplEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cplEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cxaPst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cxaPst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eenCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "eenCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eenCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "eenCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eenEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "eenEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emaNfe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "emaNfe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("entCor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "entCor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("faxCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "faxCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonCl2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonCl2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonCl3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonCl3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonCl4");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonCl4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonCl5");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonCl5"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("historico");
        elemField.setXmlName(new javax.xml.namespace.QName("", "historico"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesConsultarCadastroOutClienteHistorico"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("iniCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "iniCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insAnp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "insAnp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "insEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insEst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "insEst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insMun");
        elemField.setXmlName(new javax.xml.namespace.QName("", "insMun"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("intNet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "intNet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("marCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "marCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nenCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nenCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nenCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nenCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nenEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nenEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqRoe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqRoe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sigUfs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sigUfs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("temCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "temCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("temEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "temEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipEmc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipEmc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipMer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipMer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zipCod");
        elemField.setXmlName(new javax.xml.namespace.QName("", "zipCod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zonFra");
        elemField.setXmlName(new javax.xml.namespace.QName("", "zonFra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
