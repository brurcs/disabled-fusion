/**
 * FichaComplementarFichaComplementar6In.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.ficha.complementar;

public class FichaComplementarFichaComplementar6In  implements java.io.Serializable {
    private java.lang.String altIpe;

    private java.lang.String andTrb;

    private java.lang.String carCiv;

    private java.lang.String carSus;

    private java.lang.String catCnh;

    private java.lang.String catRes;

    private java.lang.Integer catVei;

    private java.lang.Integer cciNas;

    private java.lang.Integer cidCiv;

    private java.lang.Integer cidEmi;

    private java.lang.Integer cidRic;

    private java.lang.String claAss;

    private java.lang.String claSal;

    private java.lang.Integer codBai;

    private java.lang.Integer codCid;

    private java.lang.String codEst;

    private java.lang.Integer codPai;

    private java.lang.Integer codPrf;

    private java.lang.String codPsa;

    private java.lang.Integer codQua;

    private java.lang.Integer codRad;

    private java.lang.Integer codRlr;

    private java.lang.String datCnh;

    private java.lang.String datExp;

    private java.lang.Integer dddTel;

    private java.lang.Integer ddiTel;

    private java.lang.String demCiv;

    private java.lang.String demPsp;

    private java.lang.String dexCid;

    private java.lang.String dexRic;

    private java.lang.Integer docIdn;

    private java.lang.Integer durCon;

    private java.lang.String dvaPsp;

    private java.lang.String emaCom;

    private java.lang.String emaPar;

    private java.lang.String emiCid;

    private java.lang.String emiPsp;

    private java.lang.String emiRic;

    private java.lang.Integer endCep;

    private java.lang.String endCpl;

    private java.lang.Integer endNum;

    private java.lang.String endRua;

    private java.lang.String estCid;

    private java.lang.String estCiv;

    private java.lang.String estCnh;

    private java.lang.String estNas;

    private java.lang.Integer estPad;

    private java.lang.String estPsp;

    private java.lang.String estRic;

    private java.lang.String excPsa;

    private java.lang.Integer ficReg;

    private java.lang.String fimArm;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String folCiv;

    private java.lang.String incPsa;

    private java.lang.String iniArm;

    private java.lang.String livCiv;

    private java.lang.String maSPsa;

    private java.lang.String matCCv;

    private java.lang.String matCiv;

    private java.lang.Integer matIpe;

    private java.lang.Double matPsa;

    private java.lang.String nasViv;

    private java.lang.String nifExt;

    private java.lang.String nivSal;

    private java.lang.String nomCom;

    private java.lang.String nomRad;

    private java.lang.String nomSoc;

    private java.lang.String nroArm;

    private java.lang.Integer numCad;

    private java.lang.String numCid;

    private java.lang.String numCnh;

    private java.lang.Integer numDdd2;

    private java.lang.Integer numDdi2;

    private java.lang.String numEle;

    private java.lang.Integer numEmp;

    private java.lang.String numPsp;

    private java.lang.String numRam;

    private java.lang.String numRam2;

    private java.lang.String numRes;

    private java.lang.String numRic;

    private java.lang.String numTel;

    private java.lang.String numTel2;

    private java.lang.String orgCnh;

    private java.lang.Integer paiNas;

    private java.lang.Integer paiPsp;

    private java.lang.String preTrb;

    private java.lang.String priCnh;

    private java.lang.Integer proCon;

    private java.lang.String proExm;

    private java.lang.String regCon;

    private java.lang.String regMte;

    private java.lang.String secEle;

    private java.lang.Integer sitIpe;

    private java.lang.String sitReg;

    private java.lang.String tipCer;

    private java.lang.Integer tipCol;

    private java.lang.String tipLgr;

    private java.lang.String tipOpe;

    private java.lang.String tipPsa;

    private java.lang.String ultExm;

    private java.lang.String venCnh;

    private java.lang.String zonEle;

    public FichaComplementarFichaComplementar6In() {
    }

    public FichaComplementarFichaComplementar6In(
           java.lang.String altIpe,
           java.lang.String andTrb,
           java.lang.String carCiv,
           java.lang.String carSus,
           java.lang.String catCnh,
           java.lang.String catRes,
           java.lang.Integer catVei,
           java.lang.Integer cciNas,
           java.lang.Integer cidCiv,
           java.lang.Integer cidEmi,
           java.lang.Integer cidRic,
           java.lang.String claAss,
           java.lang.String claSal,
           java.lang.Integer codBai,
           java.lang.Integer codCid,
           java.lang.String codEst,
           java.lang.Integer codPai,
           java.lang.Integer codPrf,
           java.lang.String codPsa,
           java.lang.Integer codQua,
           java.lang.Integer codRad,
           java.lang.Integer codRlr,
           java.lang.String datCnh,
           java.lang.String datExp,
           java.lang.Integer dddTel,
           java.lang.Integer ddiTel,
           java.lang.String demCiv,
           java.lang.String demPsp,
           java.lang.String dexCid,
           java.lang.String dexRic,
           java.lang.Integer docIdn,
           java.lang.Integer durCon,
           java.lang.String dvaPsp,
           java.lang.String emaCom,
           java.lang.String emaPar,
           java.lang.String emiCid,
           java.lang.String emiPsp,
           java.lang.String emiRic,
           java.lang.Integer endCep,
           java.lang.String endCpl,
           java.lang.Integer endNum,
           java.lang.String endRua,
           java.lang.String estCid,
           java.lang.String estCiv,
           java.lang.String estCnh,
           java.lang.String estNas,
           java.lang.Integer estPad,
           java.lang.String estPsp,
           java.lang.String estRic,
           java.lang.String excPsa,
           java.lang.Integer ficReg,
           java.lang.String fimArm,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String folCiv,
           java.lang.String incPsa,
           java.lang.String iniArm,
           java.lang.String livCiv,
           java.lang.String maSPsa,
           java.lang.String matCCv,
           java.lang.String matCiv,
           java.lang.Integer matIpe,
           java.lang.Double matPsa,
           java.lang.String nasViv,
           java.lang.String nifExt,
           java.lang.String nivSal,
           java.lang.String nomCom,
           java.lang.String nomRad,
           java.lang.String nomSoc,
           java.lang.String nroArm,
           java.lang.Integer numCad,
           java.lang.String numCid,
           java.lang.String numCnh,
           java.lang.Integer numDdd2,
           java.lang.Integer numDdi2,
           java.lang.String numEle,
           java.lang.Integer numEmp,
           java.lang.String numPsp,
           java.lang.String numRam,
           java.lang.String numRam2,
           java.lang.String numRes,
           java.lang.String numRic,
           java.lang.String numTel,
           java.lang.String numTel2,
           java.lang.String orgCnh,
           java.lang.Integer paiNas,
           java.lang.Integer paiPsp,
           java.lang.String preTrb,
           java.lang.String priCnh,
           java.lang.Integer proCon,
           java.lang.String proExm,
           java.lang.String regCon,
           java.lang.String regMte,
           java.lang.String secEle,
           java.lang.Integer sitIpe,
           java.lang.String sitReg,
           java.lang.String tipCer,
           java.lang.Integer tipCol,
           java.lang.String tipLgr,
           java.lang.String tipOpe,
           java.lang.String tipPsa,
           java.lang.String ultExm,
           java.lang.String venCnh,
           java.lang.String zonEle) {
           this.altIpe = altIpe;
           this.andTrb = andTrb;
           this.carCiv = carCiv;
           this.carSus = carSus;
           this.catCnh = catCnh;
           this.catRes = catRes;
           this.catVei = catVei;
           this.cciNas = cciNas;
           this.cidCiv = cidCiv;
           this.cidEmi = cidEmi;
           this.cidRic = cidRic;
           this.claAss = claAss;
           this.claSal = claSal;
           this.codBai = codBai;
           this.codCid = codCid;
           this.codEst = codEst;
           this.codPai = codPai;
           this.codPrf = codPrf;
           this.codPsa = codPsa;
           this.codQua = codQua;
           this.codRad = codRad;
           this.codRlr = codRlr;
           this.datCnh = datCnh;
           this.datExp = datExp;
           this.dddTel = dddTel;
           this.ddiTel = ddiTel;
           this.demCiv = demCiv;
           this.demPsp = demPsp;
           this.dexCid = dexCid;
           this.dexRic = dexRic;
           this.docIdn = docIdn;
           this.durCon = durCon;
           this.dvaPsp = dvaPsp;
           this.emaCom = emaCom;
           this.emaPar = emaPar;
           this.emiCid = emiCid;
           this.emiPsp = emiPsp;
           this.emiRic = emiRic;
           this.endCep = endCep;
           this.endCpl = endCpl;
           this.endNum = endNum;
           this.endRua = endRua;
           this.estCid = estCid;
           this.estCiv = estCiv;
           this.estCnh = estCnh;
           this.estNas = estNas;
           this.estPad = estPad;
           this.estPsp = estPsp;
           this.estRic = estRic;
           this.excPsa = excPsa;
           this.ficReg = ficReg;
           this.fimArm = fimArm;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.folCiv = folCiv;
           this.incPsa = incPsa;
           this.iniArm = iniArm;
           this.livCiv = livCiv;
           this.maSPsa = maSPsa;
           this.matCCv = matCCv;
           this.matCiv = matCiv;
           this.matIpe = matIpe;
           this.matPsa = matPsa;
           this.nasViv = nasViv;
           this.nifExt = nifExt;
           this.nivSal = nivSal;
           this.nomCom = nomCom;
           this.nomRad = nomRad;
           this.nomSoc = nomSoc;
           this.nroArm = nroArm;
           this.numCad = numCad;
           this.numCid = numCid;
           this.numCnh = numCnh;
           this.numDdd2 = numDdd2;
           this.numDdi2 = numDdi2;
           this.numEle = numEle;
           this.numEmp = numEmp;
           this.numPsp = numPsp;
           this.numRam = numRam;
           this.numRam2 = numRam2;
           this.numRes = numRes;
           this.numRic = numRic;
           this.numTel = numTel;
           this.numTel2 = numTel2;
           this.orgCnh = orgCnh;
           this.paiNas = paiNas;
           this.paiPsp = paiPsp;
           this.preTrb = preTrb;
           this.priCnh = priCnh;
           this.proCon = proCon;
           this.proExm = proExm;
           this.regCon = regCon;
           this.regMte = regMte;
           this.secEle = secEle;
           this.sitIpe = sitIpe;
           this.sitReg = sitReg;
           this.tipCer = tipCer;
           this.tipCol = tipCol;
           this.tipLgr = tipLgr;
           this.tipOpe = tipOpe;
           this.tipPsa = tipPsa;
           this.ultExm = ultExm;
           this.venCnh = venCnh;
           this.zonEle = zonEle;
    }


    /**
     * Gets the altIpe value for this FichaComplementarFichaComplementar6In.
     * 
     * @return altIpe
     */
    public java.lang.String getAltIpe() {
        return altIpe;
    }


    /**
     * Sets the altIpe value for this FichaComplementarFichaComplementar6In.
     * 
     * @param altIpe
     */
    public void setAltIpe(java.lang.String altIpe) {
        this.altIpe = altIpe;
    }


    /**
     * Gets the andTrb value for this FichaComplementarFichaComplementar6In.
     * 
     * @return andTrb
     */
    public java.lang.String getAndTrb() {
        return andTrb;
    }


    /**
     * Sets the andTrb value for this FichaComplementarFichaComplementar6In.
     * 
     * @param andTrb
     */
    public void setAndTrb(java.lang.String andTrb) {
        this.andTrb = andTrb;
    }


    /**
     * Gets the carCiv value for this FichaComplementarFichaComplementar6In.
     * 
     * @return carCiv
     */
    public java.lang.String getCarCiv() {
        return carCiv;
    }


    /**
     * Sets the carCiv value for this FichaComplementarFichaComplementar6In.
     * 
     * @param carCiv
     */
    public void setCarCiv(java.lang.String carCiv) {
        this.carCiv = carCiv;
    }


    /**
     * Gets the carSus value for this FichaComplementarFichaComplementar6In.
     * 
     * @return carSus
     */
    public java.lang.String getCarSus() {
        return carSus;
    }


    /**
     * Sets the carSus value for this FichaComplementarFichaComplementar6In.
     * 
     * @param carSus
     */
    public void setCarSus(java.lang.String carSus) {
        this.carSus = carSus;
    }


    /**
     * Gets the catCnh value for this FichaComplementarFichaComplementar6In.
     * 
     * @return catCnh
     */
    public java.lang.String getCatCnh() {
        return catCnh;
    }


    /**
     * Sets the catCnh value for this FichaComplementarFichaComplementar6In.
     * 
     * @param catCnh
     */
    public void setCatCnh(java.lang.String catCnh) {
        this.catCnh = catCnh;
    }


    /**
     * Gets the catRes value for this FichaComplementarFichaComplementar6In.
     * 
     * @return catRes
     */
    public java.lang.String getCatRes() {
        return catRes;
    }


    /**
     * Sets the catRes value for this FichaComplementarFichaComplementar6In.
     * 
     * @param catRes
     */
    public void setCatRes(java.lang.String catRes) {
        this.catRes = catRes;
    }


    /**
     * Gets the catVei value for this FichaComplementarFichaComplementar6In.
     * 
     * @return catVei
     */
    public java.lang.Integer getCatVei() {
        return catVei;
    }


    /**
     * Sets the catVei value for this FichaComplementarFichaComplementar6In.
     * 
     * @param catVei
     */
    public void setCatVei(java.lang.Integer catVei) {
        this.catVei = catVei;
    }


    /**
     * Gets the cciNas value for this FichaComplementarFichaComplementar6In.
     * 
     * @return cciNas
     */
    public java.lang.Integer getCciNas() {
        return cciNas;
    }


    /**
     * Sets the cciNas value for this FichaComplementarFichaComplementar6In.
     * 
     * @param cciNas
     */
    public void setCciNas(java.lang.Integer cciNas) {
        this.cciNas = cciNas;
    }


    /**
     * Gets the cidCiv value for this FichaComplementarFichaComplementar6In.
     * 
     * @return cidCiv
     */
    public java.lang.Integer getCidCiv() {
        return cidCiv;
    }


    /**
     * Sets the cidCiv value for this FichaComplementarFichaComplementar6In.
     * 
     * @param cidCiv
     */
    public void setCidCiv(java.lang.Integer cidCiv) {
        this.cidCiv = cidCiv;
    }


    /**
     * Gets the cidEmi value for this FichaComplementarFichaComplementar6In.
     * 
     * @return cidEmi
     */
    public java.lang.Integer getCidEmi() {
        return cidEmi;
    }


    /**
     * Sets the cidEmi value for this FichaComplementarFichaComplementar6In.
     * 
     * @param cidEmi
     */
    public void setCidEmi(java.lang.Integer cidEmi) {
        this.cidEmi = cidEmi;
    }


    /**
     * Gets the cidRic value for this FichaComplementarFichaComplementar6In.
     * 
     * @return cidRic
     */
    public java.lang.Integer getCidRic() {
        return cidRic;
    }


    /**
     * Sets the cidRic value for this FichaComplementarFichaComplementar6In.
     * 
     * @param cidRic
     */
    public void setCidRic(java.lang.Integer cidRic) {
        this.cidRic = cidRic;
    }


    /**
     * Gets the claAss value for this FichaComplementarFichaComplementar6In.
     * 
     * @return claAss
     */
    public java.lang.String getClaAss() {
        return claAss;
    }


    /**
     * Sets the claAss value for this FichaComplementarFichaComplementar6In.
     * 
     * @param claAss
     */
    public void setClaAss(java.lang.String claAss) {
        this.claAss = claAss;
    }


    /**
     * Gets the claSal value for this FichaComplementarFichaComplementar6In.
     * 
     * @return claSal
     */
    public java.lang.String getClaSal() {
        return claSal;
    }


    /**
     * Sets the claSal value for this FichaComplementarFichaComplementar6In.
     * 
     * @param claSal
     */
    public void setClaSal(java.lang.String claSal) {
        this.claSal = claSal;
    }


    /**
     * Gets the codBai value for this FichaComplementarFichaComplementar6In.
     * 
     * @return codBai
     */
    public java.lang.Integer getCodBai() {
        return codBai;
    }


    /**
     * Sets the codBai value for this FichaComplementarFichaComplementar6In.
     * 
     * @param codBai
     */
    public void setCodBai(java.lang.Integer codBai) {
        this.codBai = codBai;
    }


    /**
     * Gets the codCid value for this FichaComplementarFichaComplementar6In.
     * 
     * @return codCid
     */
    public java.lang.Integer getCodCid() {
        return codCid;
    }


    /**
     * Sets the codCid value for this FichaComplementarFichaComplementar6In.
     * 
     * @param codCid
     */
    public void setCodCid(java.lang.Integer codCid) {
        this.codCid = codCid;
    }


    /**
     * Gets the codEst value for this FichaComplementarFichaComplementar6In.
     * 
     * @return codEst
     */
    public java.lang.String getCodEst() {
        return codEst;
    }


    /**
     * Sets the codEst value for this FichaComplementarFichaComplementar6In.
     * 
     * @param codEst
     */
    public void setCodEst(java.lang.String codEst) {
        this.codEst = codEst;
    }


    /**
     * Gets the codPai value for this FichaComplementarFichaComplementar6In.
     * 
     * @return codPai
     */
    public java.lang.Integer getCodPai() {
        return codPai;
    }


    /**
     * Sets the codPai value for this FichaComplementarFichaComplementar6In.
     * 
     * @param codPai
     */
    public void setCodPai(java.lang.Integer codPai) {
        this.codPai = codPai;
    }


    /**
     * Gets the codPrf value for this FichaComplementarFichaComplementar6In.
     * 
     * @return codPrf
     */
    public java.lang.Integer getCodPrf() {
        return codPrf;
    }


    /**
     * Sets the codPrf value for this FichaComplementarFichaComplementar6In.
     * 
     * @param codPrf
     */
    public void setCodPrf(java.lang.Integer codPrf) {
        this.codPrf = codPrf;
    }


    /**
     * Gets the codPsa value for this FichaComplementarFichaComplementar6In.
     * 
     * @return codPsa
     */
    public java.lang.String getCodPsa() {
        return codPsa;
    }


    /**
     * Sets the codPsa value for this FichaComplementarFichaComplementar6In.
     * 
     * @param codPsa
     */
    public void setCodPsa(java.lang.String codPsa) {
        this.codPsa = codPsa;
    }


    /**
     * Gets the codQua value for this FichaComplementarFichaComplementar6In.
     * 
     * @return codQua
     */
    public java.lang.Integer getCodQua() {
        return codQua;
    }


    /**
     * Sets the codQua value for this FichaComplementarFichaComplementar6In.
     * 
     * @param codQua
     */
    public void setCodQua(java.lang.Integer codQua) {
        this.codQua = codQua;
    }


    /**
     * Gets the codRad value for this FichaComplementarFichaComplementar6In.
     * 
     * @return codRad
     */
    public java.lang.Integer getCodRad() {
        return codRad;
    }


    /**
     * Sets the codRad value for this FichaComplementarFichaComplementar6In.
     * 
     * @param codRad
     */
    public void setCodRad(java.lang.Integer codRad) {
        this.codRad = codRad;
    }


    /**
     * Gets the codRlr value for this FichaComplementarFichaComplementar6In.
     * 
     * @return codRlr
     */
    public java.lang.Integer getCodRlr() {
        return codRlr;
    }


    /**
     * Sets the codRlr value for this FichaComplementarFichaComplementar6In.
     * 
     * @param codRlr
     */
    public void setCodRlr(java.lang.Integer codRlr) {
        this.codRlr = codRlr;
    }


    /**
     * Gets the datCnh value for this FichaComplementarFichaComplementar6In.
     * 
     * @return datCnh
     */
    public java.lang.String getDatCnh() {
        return datCnh;
    }


    /**
     * Sets the datCnh value for this FichaComplementarFichaComplementar6In.
     * 
     * @param datCnh
     */
    public void setDatCnh(java.lang.String datCnh) {
        this.datCnh = datCnh;
    }


    /**
     * Gets the datExp value for this FichaComplementarFichaComplementar6In.
     * 
     * @return datExp
     */
    public java.lang.String getDatExp() {
        return datExp;
    }


    /**
     * Sets the datExp value for this FichaComplementarFichaComplementar6In.
     * 
     * @param datExp
     */
    public void setDatExp(java.lang.String datExp) {
        this.datExp = datExp;
    }


    /**
     * Gets the dddTel value for this FichaComplementarFichaComplementar6In.
     * 
     * @return dddTel
     */
    public java.lang.Integer getDddTel() {
        return dddTel;
    }


    /**
     * Sets the dddTel value for this FichaComplementarFichaComplementar6In.
     * 
     * @param dddTel
     */
    public void setDddTel(java.lang.Integer dddTel) {
        this.dddTel = dddTel;
    }


    /**
     * Gets the ddiTel value for this FichaComplementarFichaComplementar6In.
     * 
     * @return ddiTel
     */
    public java.lang.Integer getDdiTel() {
        return ddiTel;
    }


    /**
     * Sets the ddiTel value for this FichaComplementarFichaComplementar6In.
     * 
     * @param ddiTel
     */
    public void setDdiTel(java.lang.Integer ddiTel) {
        this.ddiTel = ddiTel;
    }


    /**
     * Gets the demCiv value for this FichaComplementarFichaComplementar6In.
     * 
     * @return demCiv
     */
    public java.lang.String getDemCiv() {
        return demCiv;
    }


    /**
     * Sets the demCiv value for this FichaComplementarFichaComplementar6In.
     * 
     * @param demCiv
     */
    public void setDemCiv(java.lang.String demCiv) {
        this.demCiv = demCiv;
    }


    /**
     * Gets the demPsp value for this FichaComplementarFichaComplementar6In.
     * 
     * @return demPsp
     */
    public java.lang.String getDemPsp() {
        return demPsp;
    }


    /**
     * Sets the demPsp value for this FichaComplementarFichaComplementar6In.
     * 
     * @param demPsp
     */
    public void setDemPsp(java.lang.String demPsp) {
        this.demPsp = demPsp;
    }


    /**
     * Gets the dexCid value for this FichaComplementarFichaComplementar6In.
     * 
     * @return dexCid
     */
    public java.lang.String getDexCid() {
        return dexCid;
    }


    /**
     * Sets the dexCid value for this FichaComplementarFichaComplementar6In.
     * 
     * @param dexCid
     */
    public void setDexCid(java.lang.String dexCid) {
        this.dexCid = dexCid;
    }


    /**
     * Gets the dexRic value for this FichaComplementarFichaComplementar6In.
     * 
     * @return dexRic
     */
    public java.lang.String getDexRic() {
        return dexRic;
    }


    /**
     * Sets the dexRic value for this FichaComplementarFichaComplementar6In.
     * 
     * @param dexRic
     */
    public void setDexRic(java.lang.String dexRic) {
        this.dexRic = dexRic;
    }


    /**
     * Gets the docIdn value for this FichaComplementarFichaComplementar6In.
     * 
     * @return docIdn
     */
    public java.lang.Integer getDocIdn() {
        return docIdn;
    }


    /**
     * Sets the docIdn value for this FichaComplementarFichaComplementar6In.
     * 
     * @param docIdn
     */
    public void setDocIdn(java.lang.Integer docIdn) {
        this.docIdn = docIdn;
    }


    /**
     * Gets the durCon value for this FichaComplementarFichaComplementar6In.
     * 
     * @return durCon
     */
    public java.lang.Integer getDurCon() {
        return durCon;
    }


    /**
     * Sets the durCon value for this FichaComplementarFichaComplementar6In.
     * 
     * @param durCon
     */
    public void setDurCon(java.lang.Integer durCon) {
        this.durCon = durCon;
    }


    /**
     * Gets the dvaPsp value for this FichaComplementarFichaComplementar6In.
     * 
     * @return dvaPsp
     */
    public java.lang.String getDvaPsp() {
        return dvaPsp;
    }


    /**
     * Sets the dvaPsp value for this FichaComplementarFichaComplementar6In.
     * 
     * @param dvaPsp
     */
    public void setDvaPsp(java.lang.String dvaPsp) {
        this.dvaPsp = dvaPsp;
    }


    /**
     * Gets the emaCom value for this FichaComplementarFichaComplementar6In.
     * 
     * @return emaCom
     */
    public java.lang.String getEmaCom() {
        return emaCom;
    }


    /**
     * Sets the emaCom value for this FichaComplementarFichaComplementar6In.
     * 
     * @param emaCom
     */
    public void setEmaCom(java.lang.String emaCom) {
        this.emaCom = emaCom;
    }


    /**
     * Gets the emaPar value for this FichaComplementarFichaComplementar6In.
     * 
     * @return emaPar
     */
    public java.lang.String getEmaPar() {
        return emaPar;
    }


    /**
     * Sets the emaPar value for this FichaComplementarFichaComplementar6In.
     * 
     * @param emaPar
     */
    public void setEmaPar(java.lang.String emaPar) {
        this.emaPar = emaPar;
    }


    /**
     * Gets the emiCid value for this FichaComplementarFichaComplementar6In.
     * 
     * @return emiCid
     */
    public java.lang.String getEmiCid() {
        return emiCid;
    }


    /**
     * Sets the emiCid value for this FichaComplementarFichaComplementar6In.
     * 
     * @param emiCid
     */
    public void setEmiCid(java.lang.String emiCid) {
        this.emiCid = emiCid;
    }


    /**
     * Gets the emiPsp value for this FichaComplementarFichaComplementar6In.
     * 
     * @return emiPsp
     */
    public java.lang.String getEmiPsp() {
        return emiPsp;
    }


    /**
     * Sets the emiPsp value for this FichaComplementarFichaComplementar6In.
     * 
     * @param emiPsp
     */
    public void setEmiPsp(java.lang.String emiPsp) {
        this.emiPsp = emiPsp;
    }


    /**
     * Gets the emiRic value for this FichaComplementarFichaComplementar6In.
     * 
     * @return emiRic
     */
    public java.lang.String getEmiRic() {
        return emiRic;
    }


    /**
     * Sets the emiRic value for this FichaComplementarFichaComplementar6In.
     * 
     * @param emiRic
     */
    public void setEmiRic(java.lang.String emiRic) {
        this.emiRic = emiRic;
    }


    /**
     * Gets the endCep value for this FichaComplementarFichaComplementar6In.
     * 
     * @return endCep
     */
    public java.lang.Integer getEndCep() {
        return endCep;
    }


    /**
     * Sets the endCep value for this FichaComplementarFichaComplementar6In.
     * 
     * @param endCep
     */
    public void setEndCep(java.lang.Integer endCep) {
        this.endCep = endCep;
    }


    /**
     * Gets the endCpl value for this FichaComplementarFichaComplementar6In.
     * 
     * @return endCpl
     */
    public java.lang.String getEndCpl() {
        return endCpl;
    }


    /**
     * Sets the endCpl value for this FichaComplementarFichaComplementar6In.
     * 
     * @param endCpl
     */
    public void setEndCpl(java.lang.String endCpl) {
        this.endCpl = endCpl;
    }


    /**
     * Gets the endNum value for this FichaComplementarFichaComplementar6In.
     * 
     * @return endNum
     */
    public java.lang.Integer getEndNum() {
        return endNum;
    }


    /**
     * Sets the endNum value for this FichaComplementarFichaComplementar6In.
     * 
     * @param endNum
     */
    public void setEndNum(java.lang.Integer endNum) {
        this.endNum = endNum;
    }


    /**
     * Gets the endRua value for this FichaComplementarFichaComplementar6In.
     * 
     * @return endRua
     */
    public java.lang.String getEndRua() {
        return endRua;
    }


    /**
     * Sets the endRua value for this FichaComplementarFichaComplementar6In.
     * 
     * @param endRua
     */
    public void setEndRua(java.lang.String endRua) {
        this.endRua = endRua;
    }


    /**
     * Gets the estCid value for this FichaComplementarFichaComplementar6In.
     * 
     * @return estCid
     */
    public java.lang.String getEstCid() {
        return estCid;
    }


    /**
     * Sets the estCid value for this FichaComplementarFichaComplementar6In.
     * 
     * @param estCid
     */
    public void setEstCid(java.lang.String estCid) {
        this.estCid = estCid;
    }


    /**
     * Gets the estCiv value for this FichaComplementarFichaComplementar6In.
     * 
     * @return estCiv
     */
    public java.lang.String getEstCiv() {
        return estCiv;
    }


    /**
     * Sets the estCiv value for this FichaComplementarFichaComplementar6In.
     * 
     * @param estCiv
     */
    public void setEstCiv(java.lang.String estCiv) {
        this.estCiv = estCiv;
    }


    /**
     * Gets the estCnh value for this FichaComplementarFichaComplementar6In.
     * 
     * @return estCnh
     */
    public java.lang.String getEstCnh() {
        return estCnh;
    }


    /**
     * Sets the estCnh value for this FichaComplementarFichaComplementar6In.
     * 
     * @param estCnh
     */
    public void setEstCnh(java.lang.String estCnh) {
        this.estCnh = estCnh;
    }


    /**
     * Gets the estNas value for this FichaComplementarFichaComplementar6In.
     * 
     * @return estNas
     */
    public java.lang.String getEstNas() {
        return estNas;
    }


    /**
     * Sets the estNas value for this FichaComplementarFichaComplementar6In.
     * 
     * @param estNas
     */
    public void setEstNas(java.lang.String estNas) {
        this.estNas = estNas;
    }


    /**
     * Gets the estPad value for this FichaComplementarFichaComplementar6In.
     * 
     * @return estPad
     */
    public java.lang.Integer getEstPad() {
        return estPad;
    }


    /**
     * Sets the estPad value for this FichaComplementarFichaComplementar6In.
     * 
     * @param estPad
     */
    public void setEstPad(java.lang.Integer estPad) {
        this.estPad = estPad;
    }


    /**
     * Gets the estPsp value for this FichaComplementarFichaComplementar6In.
     * 
     * @return estPsp
     */
    public java.lang.String getEstPsp() {
        return estPsp;
    }


    /**
     * Sets the estPsp value for this FichaComplementarFichaComplementar6In.
     * 
     * @param estPsp
     */
    public void setEstPsp(java.lang.String estPsp) {
        this.estPsp = estPsp;
    }


    /**
     * Gets the estRic value for this FichaComplementarFichaComplementar6In.
     * 
     * @return estRic
     */
    public java.lang.String getEstRic() {
        return estRic;
    }


    /**
     * Sets the estRic value for this FichaComplementarFichaComplementar6In.
     * 
     * @param estRic
     */
    public void setEstRic(java.lang.String estRic) {
        this.estRic = estRic;
    }


    /**
     * Gets the excPsa value for this FichaComplementarFichaComplementar6In.
     * 
     * @return excPsa
     */
    public java.lang.String getExcPsa() {
        return excPsa;
    }


    /**
     * Sets the excPsa value for this FichaComplementarFichaComplementar6In.
     * 
     * @param excPsa
     */
    public void setExcPsa(java.lang.String excPsa) {
        this.excPsa = excPsa;
    }


    /**
     * Gets the ficReg value for this FichaComplementarFichaComplementar6In.
     * 
     * @return ficReg
     */
    public java.lang.Integer getFicReg() {
        return ficReg;
    }


    /**
     * Sets the ficReg value for this FichaComplementarFichaComplementar6In.
     * 
     * @param ficReg
     */
    public void setFicReg(java.lang.Integer ficReg) {
        this.ficReg = ficReg;
    }


    /**
     * Gets the fimArm value for this FichaComplementarFichaComplementar6In.
     * 
     * @return fimArm
     */
    public java.lang.String getFimArm() {
        return fimArm;
    }


    /**
     * Sets the fimArm value for this FichaComplementarFichaComplementar6In.
     * 
     * @param fimArm
     */
    public void setFimArm(java.lang.String fimArm) {
        this.fimArm = fimArm;
    }


    /**
     * Gets the flowInstanceID value for this FichaComplementarFichaComplementar6In.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this FichaComplementarFichaComplementar6In.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this FichaComplementarFichaComplementar6In.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this FichaComplementarFichaComplementar6In.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the folCiv value for this FichaComplementarFichaComplementar6In.
     * 
     * @return folCiv
     */
    public java.lang.String getFolCiv() {
        return folCiv;
    }


    /**
     * Sets the folCiv value for this FichaComplementarFichaComplementar6In.
     * 
     * @param folCiv
     */
    public void setFolCiv(java.lang.String folCiv) {
        this.folCiv = folCiv;
    }


    /**
     * Gets the incPsa value for this FichaComplementarFichaComplementar6In.
     * 
     * @return incPsa
     */
    public java.lang.String getIncPsa() {
        return incPsa;
    }


    /**
     * Sets the incPsa value for this FichaComplementarFichaComplementar6In.
     * 
     * @param incPsa
     */
    public void setIncPsa(java.lang.String incPsa) {
        this.incPsa = incPsa;
    }


    /**
     * Gets the iniArm value for this FichaComplementarFichaComplementar6In.
     * 
     * @return iniArm
     */
    public java.lang.String getIniArm() {
        return iniArm;
    }


    /**
     * Sets the iniArm value for this FichaComplementarFichaComplementar6In.
     * 
     * @param iniArm
     */
    public void setIniArm(java.lang.String iniArm) {
        this.iniArm = iniArm;
    }


    /**
     * Gets the livCiv value for this FichaComplementarFichaComplementar6In.
     * 
     * @return livCiv
     */
    public java.lang.String getLivCiv() {
        return livCiv;
    }


    /**
     * Sets the livCiv value for this FichaComplementarFichaComplementar6In.
     * 
     * @param livCiv
     */
    public void setLivCiv(java.lang.String livCiv) {
        this.livCiv = livCiv;
    }


    /**
     * Gets the maSPsa value for this FichaComplementarFichaComplementar6In.
     * 
     * @return maSPsa
     */
    public java.lang.String getMaSPsa() {
        return maSPsa;
    }


    /**
     * Sets the maSPsa value for this FichaComplementarFichaComplementar6In.
     * 
     * @param maSPsa
     */
    public void setMaSPsa(java.lang.String maSPsa) {
        this.maSPsa = maSPsa;
    }


    /**
     * Gets the matCCv value for this FichaComplementarFichaComplementar6In.
     * 
     * @return matCCv
     */
    public java.lang.String getMatCCv() {
        return matCCv;
    }


    /**
     * Sets the matCCv value for this FichaComplementarFichaComplementar6In.
     * 
     * @param matCCv
     */
    public void setMatCCv(java.lang.String matCCv) {
        this.matCCv = matCCv;
    }


    /**
     * Gets the matCiv value for this FichaComplementarFichaComplementar6In.
     * 
     * @return matCiv
     */
    public java.lang.String getMatCiv() {
        return matCiv;
    }


    /**
     * Sets the matCiv value for this FichaComplementarFichaComplementar6In.
     * 
     * @param matCiv
     */
    public void setMatCiv(java.lang.String matCiv) {
        this.matCiv = matCiv;
    }


    /**
     * Gets the matIpe value for this FichaComplementarFichaComplementar6In.
     * 
     * @return matIpe
     */
    public java.lang.Integer getMatIpe() {
        return matIpe;
    }


    /**
     * Sets the matIpe value for this FichaComplementarFichaComplementar6In.
     * 
     * @param matIpe
     */
    public void setMatIpe(java.lang.Integer matIpe) {
        this.matIpe = matIpe;
    }


    /**
     * Gets the matPsa value for this FichaComplementarFichaComplementar6In.
     * 
     * @return matPsa
     */
    public java.lang.Double getMatPsa() {
        return matPsa;
    }


    /**
     * Sets the matPsa value for this FichaComplementarFichaComplementar6In.
     * 
     * @param matPsa
     */
    public void setMatPsa(java.lang.Double matPsa) {
        this.matPsa = matPsa;
    }


    /**
     * Gets the nasViv value for this FichaComplementarFichaComplementar6In.
     * 
     * @return nasViv
     */
    public java.lang.String getNasViv() {
        return nasViv;
    }


    /**
     * Sets the nasViv value for this FichaComplementarFichaComplementar6In.
     * 
     * @param nasViv
     */
    public void setNasViv(java.lang.String nasViv) {
        this.nasViv = nasViv;
    }


    /**
     * Gets the nifExt value for this FichaComplementarFichaComplementar6In.
     * 
     * @return nifExt
     */
    public java.lang.String getNifExt() {
        return nifExt;
    }


    /**
     * Sets the nifExt value for this FichaComplementarFichaComplementar6In.
     * 
     * @param nifExt
     */
    public void setNifExt(java.lang.String nifExt) {
        this.nifExt = nifExt;
    }


    /**
     * Gets the nivSal value for this FichaComplementarFichaComplementar6In.
     * 
     * @return nivSal
     */
    public java.lang.String getNivSal() {
        return nivSal;
    }


    /**
     * Sets the nivSal value for this FichaComplementarFichaComplementar6In.
     * 
     * @param nivSal
     */
    public void setNivSal(java.lang.String nivSal) {
        this.nivSal = nivSal;
    }


    /**
     * Gets the nomCom value for this FichaComplementarFichaComplementar6In.
     * 
     * @return nomCom
     */
    public java.lang.String getNomCom() {
        return nomCom;
    }


    /**
     * Sets the nomCom value for this FichaComplementarFichaComplementar6In.
     * 
     * @param nomCom
     */
    public void setNomCom(java.lang.String nomCom) {
        this.nomCom = nomCom;
    }


    /**
     * Gets the nomRad value for this FichaComplementarFichaComplementar6In.
     * 
     * @return nomRad
     */
    public java.lang.String getNomRad() {
        return nomRad;
    }


    /**
     * Sets the nomRad value for this FichaComplementarFichaComplementar6In.
     * 
     * @param nomRad
     */
    public void setNomRad(java.lang.String nomRad) {
        this.nomRad = nomRad;
    }


    /**
     * Gets the nomSoc value for this FichaComplementarFichaComplementar6In.
     * 
     * @return nomSoc
     */
    public java.lang.String getNomSoc() {
        return nomSoc;
    }


    /**
     * Sets the nomSoc value for this FichaComplementarFichaComplementar6In.
     * 
     * @param nomSoc
     */
    public void setNomSoc(java.lang.String nomSoc) {
        this.nomSoc = nomSoc;
    }


    /**
     * Gets the nroArm value for this FichaComplementarFichaComplementar6In.
     * 
     * @return nroArm
     */
    public java.lang.String getNroArm() {
        return nroArm;
    }


    /**
     * Sets the nroArm value for this FichaComplementarFichaComplementar6In.
     * 
     * @param nroArm
     */
    public void setNroArm(java.lang.String nroArm) {
        this.nroArm = nroArm;
    }


    /**
     * Gets the numCad value for this FichaComplementarFichaComplementar6In.
     * 
     * @return numCad
     */
    public java.lang.Integer getNumCad() {
        return numCad;
    }


    /**
     * Sets the numCad value for this FichaComplementarFichaComplementar6In.
     * 
     * @param numCad
     */
    public void setNumCad(java.lang.Integer numCad) {
        this.numCad = numCad;
    }


    /**
     * Gets the numCid value for this FichaComplementarFichaComplementar6In.
     * 
     * @return numCid
     */
    public java.lang.String getNumCid() {
        return numCid;
    }


    /**
     * Sets the numCid value for this FichaComplementarFichaComplementar6In.
     * 
     * @param numCid
     */
    public void setNumCid(java.lang.String numCid) {
        this.numCid = numCid;
    }


    /**
     * Gets the numCnh value for this FichaComplementarFichaComplementar6In.
     * 
     * @return numCnh
     */
    public java.lang.String getNumCnh() {
        return numCnh;
    }


    /**
     * Sets the numCnh value for this FichaComplementarFichaComplementar6In.
     * 
     * @param numCnh
     */
    public void setNumCnh(java.lang.String numCnh) {
        this.numCnh = numCnh;
    }


    /**
     * Gets the numDdd2 value for this FichaComplementarFichaComplementar6In.
     * 
     * @return numDdd2
     */
    public java.lang.Integer getNumDdd2() {
        return numDdd2;
    }


    /**
     * Sets the numDdd2 value for this FichaComplementarFichaComplementar6In.
     * 
     * @param numDdd2
     */
    public void setNumDdd2(java.lang.Integer numDdd2) {
        this.numDdd2 = numDdd2;
    }


    /**
     * Gets the numDdi2 value for this FichaComplementarFichaComplementar6In.
     * 
     * @return numDdi2
     */
    public java.lang.Integer getNumDdi2() {
        return numDdi2;
    }


    /**
     * Sets the numDdi2 value for this FichaComplementarFichaComplementar6In.
     * 
     * @param numDdi2
     */
    public void setNumDdi2(java.lang.Integer numDdi2) {
        this.numDdi2 = numDdi2;
    }


    /**
     * Gets the numEle value for this FichaComplementarFichaComplementar6In.
     * 
     * @return numEle
     */
    public java.lang.String getNumEle() {
        return numEle;
    }


    /**
     * Sets the numEle value for this FichaComplementarFichaComplementar6In.
     * 
     * @param numEle
     */
    public void setNumEle(java.lang.String numEle) {
        this.numEle = numEle;
    }


    /**
     * Gets the numEmp value for this FichaComplementarFichaComplementar6In.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this FichaComplementarFichaComplementar6In.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the numPsp value for this FichaComplementarFichaComplementar6In.
     * 
     * @return numPsp
     */
    public java.lang.String getNumPsp() {
        return numPsp;
    }


    /**
     * Sets the numPsp value for this FichaComplementarFichaComplementar6In.
     * 
     * @param numPsp
     */
    public void setNumPsp(java.lang.String numPsp) {
        this.numPsp = numPsp;
    }


    /**
     * Gets the numRam value for this FichaComplementarFichaComplementar6In.
     * 
     * @return numRam
     */
    public java.lang.String getNumRam() {
        return numRam;
    }


    /**
     * Sets the numRam value for this FichaComplementarFichaComplementar6In.
     * 
     * @param numRam
     */
    public void setNumRam(java.lang.String numRam) {
        this.numRam = numRam;
    }


    /**
     * Gets the numRam2 value for this FichaComplementarFichaComplementar6In.
     * 
     * @return numRam2
     */
    public java.lang.String getNumRam2() {
        return numRam2;
    }


    /**
     * Sets the numRam2 value for this FichaComplementarFichaComplementar6In.
     * 
     * @param numRam2
     */
    public void setNumRam2(java.lang.String numRam2) {
        this.numRam2 = numRam2;
    }


    /**
     * Gets the numRes value for this FichaComplementarFichaComplementar6In.
     * 
     * @return numRes
     */
    public java.lang.String getNumRes() {
        return numRes;
    }


    /**
     * Sets the numRes value for this FichaComplementarFichaComplementar6In.
     * 
     * @param numRes
     */
    public void setNumRes(java.lang.String numRes) {
        this.numRes = numRes;
    }


    /**
     * Gets the numRic value for this FichaComplementarFichaComplementar6In.
     * 
     * @return numRic
     */
    public java.lang.String getNumRic() {
        return numRic;
    }


    /**
     * Sets the numRic value for this FichaComplementarFichaComplementar6In.
     * 
     * @param numRic
     */
    public void setNumRic(java.lang.String numRic) {
        this.numRic = numRic;
    }


    /**
     * Gets the numTel value for this FichaComplementarFichaComplementar6In.
     * 
     * @return numTel
     */
    public java.lang.String getNumTel() {
        return numTel;
    }


    /**
     * Sets the numTel value for this FichaComplementarFichaComplementar6In.
     * 
     * @param numTel
     */
    public void setNumTel(java.lang.String numTel) {
        this.numTel = numTel;
    }


    /**
     * Gets the numTel2 value for this FichaComplementarFichaComplementar6In.
     * 
     * @return numTel2
     */
    public java.lang.String getNumTel2() {
        return numTel2;
    }


    /**
     * Sets the numTel2 value for this FichaComplementarFichaComplementar6In.
     * 
     * @param numTel2
     */
    public void setNumTel2(java.lang.String numTel2) {
        this.numTel2 = numTel2;
    }


    /**
     * Gets the orgCnh value for this FichaComplementarFichaComplementar6In.
     * 
     * @return orgCnh
     */
    public java.lang.String getOrgCnh() {
        return orgCnh;
    }


    /**
     * Sets the orgCnh value for this FichaComplementarFichaComplementar6In.
     * 
     * @param orgCnh
     */
    public void setOrgCnh(java.lang.String orgCnh) {
        this.orgCnh = orgCnh;
    }


    /**
     * Gets the paiNas value for this FichaComplementarFichaComplementar6In.
     * 
     * @return paiNas
     */
    public java.lang.Integer getPaiNas() {
        return paiNas;
    }


    /**
     * Sets the paiNas value for this FichaComplementarFichaComplementar6In.
     * 
     * @param paiNas
     */
    public void setPaiNas(java.lang.Integer paiNas) {
        this.paiNas = paiNas;
    }


    /**
     * Gets the paiPsp value for this FichaComplementarFichaComplementar6In.
     * 
     * @return paiPsp
     */
    public java.lang.Integer getPaiPsp() {
        return paiPsp;
    }


    /**
     * Sets the paiPsp value for this FichaComplementarFichaComplementar6In.
     * 
     * @param paiPsp
     */
    public void setPaiPsp(java.lang.Integer paiPsp) {
        this.paiPsp = paiPsp;
    }


    /**
     * Gets the preTrb value for this FichaComplementarFichaComplementar6In.
     * 
     * @return preTrb
     */
    public java.lang.String getPreTrb() {
        return preTrb;
    }


    /**
     * Sets the preTrb value for this FichaComplementarFichaComplementar6In.
     * 
     * @param preTrb
     */
    public void setPreTrb(java.lang.String preTrb) {
        this.preTrb = preTrb;
    }


    /**
     * Gets the priCnh value for this FichaComplementarFichaComplementar6In.
     * 
     * @return priCnh
     */
    public java.lang.String getPriCnh() {
        return priCnh;
    }


    /**
     * Sets the priCnh value for this FichaComplementarFichaComplementar6In.
     * 
     * @param priCnh
     */
    public void setPriCnh(java.lang.String priCnh) {
        this.priCnh = priCnh;
    }


    /**
     * Gets the proCon value for this FichaComplementarFichaComplementar6In.
     * 
     * @return proCon
     */
    public java.lang.Integer getProCon() {
        return proCon;
    }


    /**
     * Sets the proCon value for this FichaComplementarFichaComplementar6In.
     * 
     * @param proCon
     */
    public void setProCon(java.lang.Integer proCon) {
        this.proCon = proCon;
    }


    /**
     * Gets the proExm value for this FichaComplementarFichaComplementar6In.
     * 
     * @return proExm
     */
    public java.lang.String getProExm() {
        return proExm;
    }


    /**
     * Sets the proExm value for this FichaComplementarFichaComplementar6In.
     * 
     * @param proExm
     */
    public void setProExm(java.lang.String proExm) {
        this.proExm = proExm;
    }


    /**
     * Gets the regCon value for this FichaComplementarFichaComplementar6In.
     * 
     * @return regCon
     */
    public java.lang.String getRegCon() {
        return regCon;
    }


    /**
     * Sets the regCon value for this FichaComplementarFichaComplementar6In.
     * 
     * @param regCon
     */
    public void setRegCon(java.lang.String regCon) {
        this.regCon = regCon;
    }


    /**
     * Gets the regMte value for this FichaComplementarFichaComplementar6In.
     * 
     * @return regMte
     */
    public java.lang.String getRegMte() {
        return regMte;
    }


    /**
     * Sets the regMte value for this FichaComplementarFichaComplementar6In.
     * 
     * @param regMte
     */
    public void setRegMte(java.lang.String regMte) {
        this.regMte = regMte;
    }


    /**
     * Gets the secEle value for this FichaComplementarFichaComplementar6In.
     * 
     * @return secEle
     */
    public java.lang.String getSecEle() {
        return secEle;
    }


    /**
     * Sets the secEle value for this FichaComplementarFichaComplementar6In.
     * 
     * @param secEle
     */
    public void setSecEle(java.lang.String secEle) {
        this.secEle = secEle;
    }


    /**
     * Gets the sitIpe value for this FichaComplementarFichaComplementar6In.
     * 
     * @return sitIpe
     */
    public java.lang.Integer getSitIpe() {
        return sitIpe;
    }


    /**
     * Sets the sitIpe value for this FichaComplementarFichaComplementar6In.
     * 
     * @param sitIpe
     */
    public void setSitIpe(java.lang.Integer sitIpe) {
        this.sitIpe = sitIpe;
    }


    /**
     * Gets the sitReg value for this FichaComplementarFichaComplementar6In.
     * 
     * @return sitReg
     */
    public java.lang.String getSitReg() {
        return sitReg;
    }


    /**
     * Sets the sitReg value for this FichaComplementarFichaComplementar6In.
     * 
     * @param sitReg
     */
    public void setSitReg(java.lang.String sitReg) {
        this.sitReg = sitReg;
    }


    /**
     * Gets the tipCer value for this FichaComplementarFichaComplementar6In.
     * 
     * @return tipCer
     */
    public java.lang.String getTipCer() {
        return tipCer;
    }


    /**
     * Sets the tipCer value for this FichaComplementarFichaComplementar6In.
     * 
     * @param tipCer
     */
    public void setTipCer(java.lang.String tipCer) {
        this.tipCer = tipCer;
    }


    /**
     * Gets the tipCol value for this FichaComplementarFichaComplementar6In.
     * 
     * @return tipCol
     */
    public java.lang.Integer getTipCol() {
        return tipCol;
    }


    /**
     * Sets the tipCol value for this FichaComplementarFichaComplementar6In.
     * 
     * @param tipCol
     */
    public void setTipCol(java.lang.Integer tipCol) {
        this.tipCol = tipCol;
    }


    /**
     * Gets the tipLgr value for this FichaComplementarFichaComplementar6In.
     * 
     * @return tipLgr
     */
    public java.lang.String getTipLgr() {
        return tipLgr;
    }


    /**
     * Sets the tipLgr value for this FichaComplementarFichaComplementar6In.
     * 
     * @param tipLgr
     */
    public void setTipLgr(java.lang.String tipLgr) {
        this.tipLgr = tipLgr;
    }


    /**
     * Gets the tipOpe value for this FichaComplementarFichaComplementar6In.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this FichaComplementarFichaComplementar6In.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }


    /**
     * Gets the tipPsa value for this FichaComplementarFichaComplementar6In.
     * 
     * @return tipPsa
     */
    public java.lang.String getTipPsa() {
        return tipPsa;
    }


    /**
     * Sets the tipPsa value for this FichaComplementarFichaComplementar6In.
     * 
     * @param tipPsa
     */
    public void setTipPsa(java.lang.String tipPsa) {
        this.tipPsa = tipPsa;
    }


    /**
     * Gets the ultExm value for this FichaComplementarFichaComplementar6In.
     * 
     * @return ultExm
     */
    public java.lang.String getUltExm() {
        return ultExm;
    }


    /**
     * Sets the ultExm value for this FichaComplementarFichaComplementar6In.
     * 
     * @param ultExm
     */
    public void setUltExm(java.lang.String ultExm) {
        this.ultExm = ultExm;
    }


    /**
     * Gets the venCnh value for this FichaComplementarFichaComplementar6In.
     * 
     * @return venCnh
     */
    public java.lang.String getVenCnh() {
        return venCnh;
    }


    /**
     * Sets the venCnh value for this FichaComplementarFichaComplementar6In.
     * 
     * @param venCnh
     */
    public void setVenCnh(java.lang.String venCnh) {
        this.venCnh = venCnh;
    }


    /**
     * Gets the zonEle value for this FichaComplementarFichaComplementar6In.
     * 
     * @return zonEle
     */
    public java.lang.String getZonEle() {
        return zonEle;
    }


    /**
     * Sets the zonEle value for this FichaComplementarFichaComplementar6In.
     * 
     * @param zonEle
     */
    public void setZonEle(java.lang.String zonEle) {
        this.zonEle = zonEle;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FichaComplementarFichaComplementar6In)) return false;
        FichaComplementarFichaComplementar6In other = (FichaComplementarFichaComplementar6In) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.altIpe==null && other.getAltIpe()==null) || 
             (this.altIpe!=null &&
              this.altIpe.equals(other.getAltIpe()))) &&
            ((this.andTrb==null && other.getAndTrb()==null) || 
             (this.andTrb!=null &&
              this.andTrb.equals(other.getAndTrb()))) &&
            ((this.carCiv==null && other.getCarCiv()==null) || 
             (this.carCiv!=null &&
              this.carCiv.equals(other.getCarCiv()))) &&
            ((this.carSus==null && other.getCarSus()==null) || 
             (this.carSus!=null &&
              this.carSus.equals(other.getCarSus()))) &&
            ((this.catCnh==null && other.getCatCnh()==null) || 
             (this.catCnh!=null &&
              this.catCnh.equals(other.getCatCnh()))) &&
            ((this.catRes==null && other.getCatRes()==null) || 
             (this.catRes!=null &&
              this.catRes.equals(other.getCatRes()))) &&
            ((this.catVei==null && other.getCatVei()==null) || 
             (this.catVei!=null &&
              this.catVei.equals(other.getCatVei()))) &&
            ((this.cciNas==null && other.getCciNas()==null) || 
             (this.cciNas!=null &&
              this.cciNas.equals(other.getCciNas()))) &&
            ((this.cidCiv==null && other.getCidCiv()==null) || 
             (this.cidCiv!=null &&
              this.cidCiv.equals(other.getCidCiv()))) &&
            ((this.cidEmi==null && other.getCidEmi()==null) || 
             (this.cidEmi!=null &&
              this.cidEmi.equals(other.getCidEmi()))) &&
            ((this.cidRic==null && other.getCidRic()==null) || 
             (this.cidRic!=null &&
              this.cidRic.equals(other.getCidRic()))) &&
            ((this.claAss==null && other.getClaAss()==null) || 
             (this.claAss!=null &&
              this.claAss.equals(other.getClaAss()))) &&
            ((this.claSal==null && other.getClaSal()==null) || 
             (this.claSal!=null &&
              this.claSal.equals(other.getClaSal()))) &&
            ((this.codBai==null && other.getCodBai()==null) || 
             (this.codBai!=null &&
              this.codBai.equals(other.getCodBai()))) &&
            ((this.codCid==null && other.getCodCid()==null) || 
             (this.codCid!=null &&
              this.codCid.equals(other.getCodCid()))) &&
            ((this.codEst==null && other.getCodEst()==null) || 
             (this.codEst!=null &&
              this.codEst.equals(other.getCodEst()))) &&
            ((this.codPai==null && other.getCodPai()==null) || 
             (this.codPai!=null &&
              this.codPai.equals(other.getCodPai()))) &&
            ((this.codPrf==null && other.getCodPrf()==null) || 
             (this.codPrf!=null &&
              this.codPrf.equals(other.getCodPrf()))) &&
            ((this.codPsa==null && other.getCodPsa()==null) || 
             (this.codPsa!=null &&
              this.codPsa.equals(other.getCodPsa()))) &&
            ((this.codQua==null && other.getCodQua()==null) || 
             (this.codQua!=null &&
              this.codQua.equals(other.getCodQua()))) &&
            ((this.codRad==null && other.getCodRad()==null) || 
             (this.codRad!=null &&
              this.codRad.equals(other.getCodRad()))) &&
            ((this.codRlr==null && other.getCodRlr()==null) || 
             (this.codRlr!=null &&
              this.codRlr.equals(other.getCodRlr()))) &&
            ((this.datCnh==null && other.getDatCnh()==null) || 
             (this.datCnh!=null &&
              this.datCnh.equals(other.getDatCnh()))) &&
            ((this.datExp==null && other.getDatExp()==null) || 
             (this.datExp!=null &&
              this.datExp.equals(other.getDatExp()))) &&
            ((this.dddTel==null && other.getDddTel()==null) || 
             (this.dddTel!=null &&
              this.dddTel.equals(other.getDddTel()))) &&
            ((this.ddiTel==null && other.getDdiTel()==null) || 
             (this.ddiTel!=null &&
              this.ddiTel.equals(other.getDdiTel()))) &&
            ((this.demCiv==null && other.getDemCiv()==null) || 
             (this.demCiv!=null &&
              this.demCiv.equals(other.getDemCiv()))) &&
            ((this.demPsp==null && other.getDemPsp()==null) || 
             (this.demPsp!=null &&
              this.demPsp.equals(other.getDemPsp()))) &&
            ((this.dexCid==null && other.getDexCid()==null) || 
             (this.dexCid!=null &&
              this.dexCid.equals(other.getDexCid()))) &&
            ((this.dexRic==null && other.getDexRic()==null) || 
             (this.dexRic!=null &&
              this.dexRic.equals(other.getDexRic()))) &&
            ((this.docIdn==null && other.getDocIdn()==null) || 
             (this.docIdn!=null &&
              this.docIdn.equals(other.getDocIdn()))) &&
            ((this.durCon==null && other.getDurCon()==null) || 
             (this.durCon!=null &&
              this.durCon.equals(other.getDurCon()))) &&
            ((this.dvaPsp==null && other.getDvaPsp()==null) || 
             (this.dvaPsp!=null &&
              this.dvaPsp.equals(other.getDvaPsp()))) &&
            ((this.emaCom==null && other.getEmaCom()==null) || 
             (this.emaCom!=null &&
              this.emaCom.equals(other.getEmaCom()))) &&
            ((this.emaPar==null && other.getEmaPar()==null) || 
             (this.emaPar!=null &&
              this.emaPar.equals(other.getEmaPar()))) &&
            ((this.emiCid==null && other.getEmiCid()==null) || 
             (this.emiCid!=null &&
              this.emiCid.equals(other.getEmiCid()))) &&
            ((this.emiPsp==null && other.getEmiPsp()==null) || 
             (this.emiPsp!=null &&
              this.emiPsp.equals(other.getEmiPsp()))) &&
            ((this.emiRic==null && other.getEmiRic()==null) || 
             (this.emiRic!=null &&
              this.emiRic.equals(other.getEmiRic()))) &&
            ((this.endCep==null && other.getEndCep()==null) || 
             (this.endCep!=null &&
              this.endCep.equals(other.getEndCep()))) &&
            ((this.endCpl==null && other.getEndCpl()==null) || 
             (this.endCpl!=null &&
              this.endCpl.equals(other.getEndCpl()))) &&
            ((this.endNum==null && other.getEndNum()==null) || 
             (this.endNum!=null &&
              this.endNum.equals(other.getEndNum()))) &&
            ((this.endRua==null && other.getEndRua()==null) || 
             (this.endRua!=null &&
              this.endRua.equals(other.getEndRua()))) &&
            ((this.estCid==null && other.getEstCid()==null) || 
             (this.estCid!=null &&
              this.estCid.equals(other.getEstCid()))) &&
            ((this.estCiv==null && other.getEstCiv()==null) || 
             (this.estCiv!=null &&
              this.estCiv.equals(other.getEstCiv()))) &&
            ((this.estCnh==null && other.getEstCnh()==null) || 
             (this.estCnh!=null &&
              this.estCnh.equals(other.getEstCnh()))) &&
            ((this.estNas==null && other.getEstNas()==null) || 
             (this.estNas!=null &&
              this.estNas.equals(other.getEstNas()))) &&
            ((this.estPad==null && other.getEstPad()==null) || 
             (this.estPad!=null &&
              this.estPad.equals(other.getEstPad()))) &&
            ((this.estPsp==null && other.getEstPsp()==null) || 
             (this.estPsp!=null &&
              this.estPsp.equals(other.getEstPsp()))) &&
            ((this.estRic==null && other.getEstRic()==null) || 
             (this.estRic!=null &&
              this.estRic.equals(other.getEstRic()))) &&
            ((this.excPsa==null && other.getExcPsa()==null) || 
             (this.excPsa!=null &&
              this.excPsa.equals(other.getExcPsa()))) &&
            ((this.ficReg==null && other.getFicReg()==null) || 
             (this.ficReg!=null &&
              this.ficReg.equals(other.getFicReg()))) &&
            ((this.fimArm==null && other.getFimArm()==null) || 
             (this.fimArm!=null &&
              this.fimArm.equals(other.getFimArm()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.folCiv==null && other.getFolCiv()==null) || 
             (this.folCiv!=null &&
              this.folCiv.equals(other.getFolCiv()))) &&
            ((this.incPsa==null && other.getIncPsa()==null) || 
             (this.incPsa!=null &&
              this.incPsa.equals(other.getIncPsa()))) &&
            ((this.iniArm==null && other.getIniArm()==null) || 
             (this.iniArm!=null &&
              this.iniArm.equals(other.getIniArm()))) &&
            ((this.livCiv==null && other.getLivCiv()==null) || 
             (this.livCiv!=null &&
              this.livCiv.equals(other.getLivCiv()))) &&
            ((this.maSPsa==null && other.getMaSPsa()==null) || 
             (this.maSPsa!=null &&
              this.maSPsa.equals(other.getMaSPsa()))) &&
            ((this.matCCv==null && other.getMatCCv()==null) || 
             (this.matCCv!=null &&
              this.matCCv.equals(other.getMatCCv()))) &&
            ((this.matCiv==null && other.getMatCiv()==null) || 
             (this.matCiv!=null &&
              this.matCiv.equals(other.getMatCiv()))) &&
            ((this.matIpe==null && other.getMatIpe()==null) || 
             (this.matIpe!=null &&
              this.matIpe.equals(other.getMatIpe()))) &&
            ((this.matPsa==null && other.getMatPsa()==null) || 
             (this.matPsa!=null &&
              this.matPsa.equals(other.getMatPsa()))) &&
            ((this.nasViv==null && other.getNasViv()==null) || 
             (this.nasViv!=null &&
              this.nasViv.equals(other.getNasViv()))) &&
            ((this.nifExt==null && other.getNifExt()==null) || 
             (this.nifExt!=null &&
              this.nifExt.equals(other.getNifExt()))) &&
            ((this.nivSal==null && other.getNivSal()==null) || 
             (this.nivSal!=null &&
              this.nivSal.equals(other.getNivSal()))) &&
            ((this.nomCom==null && other.getNomCom()==null) || 
             (this.nomCom!=null &&
              this.nomCom.equals(other.getNomCom()))) &&
            ((this.nomRad==null && other.getNomRad()==null) || 
             (this.nomRad!=null &&
              this.nomRad.equals(other.getNomRad()))) &&
            ((this.nomSoc==null && other.getNomSoc()==null) || 
             (this.nomSoc!=null &&
              this.nomSoc.equals(other.getNomSoc()))) &&
            ((this.nroArm==null && other.getNroArm()==null) || 
             (this.nroArm!=null &&
              this.nroArm.equals(other.getNroArm()))) &&
            ((this.numCad==null && other.getNumCad()==null) || 
             (this.numCad!=null &&
              this.numCad.equals(other.getNumCad()))) &&
            ((this.numCid==null && other.getNumCid()==null) || 
             (this.numCid!=null &&
              this.numCid.equals(other.getNumCid()))) &&
            ((this.numCnh==null && other.getNumCnh()==null) || 
             (this.numCnh!=null &&
              this.numCnh.equals(other.getNumCnh()))) &&
            ((this.numDdd2==null && other.getNumDdd2()==null) || 
             (this.numDdd2!=null &&
              this.numDdd2.equals(other.getNumDdd2()))) &&
            ((this.numDdi2==null && other.getNumDdi2()==null) || 
             (this.numDdi2!=null &&
              this.numDdi2.equals(other.getNumDdi2()))) &&
            ((this.numEle==null && other.getNumEle()==null) || 
             (this.numEle!=null &&
              this.numEle.equals(other.getNumEle()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.numPsp==null && other.getNumPsp()==null) || 
             (this.numPsp!=null &&
              this.numPsp.equals(other.getNumPsp()))) &&
            ((this.numRam==null && other.getNumRam()==null) || 
             (this.numRam!=null &&
              this.numRam.equals(other.getNumRam()))) &&
            ((this.numRam2==null && other.getNumRam2()==null) || 
             (this.numRam2!=null &&
              this.numRam2.equals(other.getNumRam2()))) &&
            ((this.numRes==null && other.getNumRes()==null) || 
             (this.numRes!=null &&
              this.numRes.equals(other.getNumRes()))) &&
            ((this.numRic==null && other.getNumRic()==null) || 
             (this.numRic!=null &&
              this.numRic.equals(other.getNumRic()))) &&
            ((this.numTel==null && other.getNumTel()==null) || 
             (this.numTel!=null &&
              this.numTel.equals(other.getNumTel()))) &&
            ((this.numTel2==null && other.getNumTel2()==null) || 
             (this.numTel2!=null &&
              this.numTel2.equals(other.getNumTel2()))) &&
            ((this.orgCnh==null && other.getOrgCnh()==null) || 
             (this.orgCnh!=null &&
              this.orgCnh.equals(other.getOrgCnh()))) &&
            ((this.paiNas==null && other.getPaiNas()==null) || 
             (this.paiNas!=null &&
              this.paiNas.equals(other.getPaiNas()))) &&
            ((this.paiPsp==null && other.getPaiPsp()==null) || 
             (this.paiPsp!=null &&
              this.paiPsp.equals(other.getPaiPsp()))) &&
            ((this.preTrb==null && other.getPreTrb()==null) || 
             (this.preTrb!=null &&
              this.preTrb.equals(other.getPreTrb()))) &&
            ((this.priCnh==null && other.getPriCnh()==null) || 
             (this.priCnh!=null &&
              this.priCnh.equals(other.getPriCnh()))) &&
            ((this.proCon==null && other.getProCon()==null) || 
             (this.proCon!=null &&
              this.proCon.equals(other.getProCon()))) &&
            ((this.proExm==null && other.getProExm()==null) || 
             (this.proExm!=null &&
              this.proExm.equals(other.getProExm()))) &&
            ((this.regCon==null && other.getRegCon()==null) || 
             (this.regCon!=null &&
              this.regCon.equals(other.getRegCon()))) &&
            ((this.regMte==null && other.getRegMte()==null) || 
             (this.regMte!=null &&
              this.regMte.equals(other.getRegMte()))) &&
            ((this.secEle==null && other.getSecEle()==null) || 
             (this.secEle!=null &&
              this.secEle.equals(other.getSecEle()))) &&
            ((this.sitIpe==null && other.getSitIpe()==null) || 
             (this.sitIpe!=null &&
              this.sitIpe.equals(other.getSitIpe()))) &&
            ((this.sitReg==null && other.getSitReg()==null) || 
             (this.sitReg!=null &&
              this.sitReg.equals(other.getSitReg()))) &&
            ((this.tipCer==null && other.getTipCer()==null) || 
             (this.tipCer!=null &&
              this.tipCer.equals(other.getTipCer()))) &&
            ((this.tipCol==null && other.getTipCol()==null) || 
             (this.tipCol!=null &&
              this.tipCol.equals(other.getTipCol()))) &&
            ((this.tipLgr==null && other.getTipLgr()==null) || 
             (this.tipLgr!=null &&
              this.tipLgr.equals(other.getTipLgr()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe()))) &&
            ((this.tipPsa==null && other.getTipPsa()==null) || 
             (this.tipPsa!=null &&
              this.tipPsa.equals(other.getTipPsa()))) &&
            ((this.ultExm==null && other.getUltExm()==null) || 
             (this.ultExm!=null &&
              this.ultExm.equals(other.getUltExm()))) &&
            ((this.venCnh==null && other.getVenCnh()==null) || 
             (this.venCnh!=null &&
              this.venCnh.equals(other.getVenCnh()))) &&
            ((this.zonEle==null && other.getZonEle()==null) || 
             (this.zonEle!=null &&
              this.zonEle.equals(other.getZonEle())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAltIpe() != null) {
            _hashCode += getAltIpe().hashCode();
        }
        if (getAndTrb() != null) {
            _hashCode += getAndTrb().hashCode();
        }
        if (getCarCiv() != null) {
            _hashCode += getCarCiv().hashCode();
        }
        if (getCarSus() != null) {
            _hashCode += getCarSus().hashCode();
        }
        if (getCatCnh() != null) {
            _hashCode += getCatCnh().hashCode();
        }
        if (getCatRes() != null) {
            _hashCode += getCatRes().hashCode();
        }
        if (getCatVei() != null) {
            _hashCode += getCatVei().hashCode();
        }
        if (getCciNas() != null) {
            _hashCode += getCciNas().hashCode();
        }
        if (getCidCiv() != null) {
            _hashCode += getCidCiv().hashCode();
        }
        if (getCidEmi() != null) {
            _hashCode += getCidEmi().hashCode();
        }
        if (getCidRic() != null) {
            _hashCode += getCidRic().hashCode();
        }
        if (getClaAss() != null) {
            _hashCode += getClaAss().hashCode();
        }
        if (getClaSal() != null) {
            _hashCode += getClaSal().hashCode();
        }
        if (getCodBai() != null) {
            _hashCode += getCodBai().hashCode();
        }
        if (getCodCid() != null) {
            _hashCode += getCodCid().hashCode();
        }
        if (getCodEst() != null) {
            _hashCode += getCodEst().hashCode();
        }
        if (getCodPai() != null) {
            _hashCode += getCodPai().hashCode();
        }
        if (getCodPrf() != null) {
            _hashCode += getCodPrf().hashCode();
        }
        if (getCodPsa() != null) {
            _hashCode += getCodPsa().hashCode();
        }
        if (getCodQua() != null) {
            _hashCode += getCodQua().hashCode();
        }
        if (getCodRad() != null) {
            _hashCode += getCodRad().hashCode();
        }
        if (getCodRlr() != null) {
            _hashCode += getCodRlr().hashCode();
        }
        if (getDatCnh() != null) {
            _hashCode += getDatCnh().hashCode();
        }
        if (getDatExp() != null) {
            _hashCode += getDatExp().hashCode();
        }
        if (getDddTel() != null) {
            _hashCode += getDddTel().hashCode();
        }
        if (getDdiTel() != null) {
            _hashCode += getDdiTel().hashCode();
        }
        if (getDemCiv() != null) {
            _hashCode += getDemCiv().hashCode();
        }
        if (getDemPsp() != null) {
            _hashCode += getDemPsp().hashCode();
        }
        if (getDexCid() != null) {
            _hashCode += getDexCid().hashCode();
        }
        if (getDexRic() != null) {
            _hashCode += getDexRic().hashCode();
        }
        if (getDocIdn() != null) {
            _hashCode += getDocIdn().hashCode();
        }
        if (getDurCon() != null) {
            _hashCode += getDurCon().hashCode();
        }
        if (getDvaPsp() != null) {
            _hashCode += getDvaPsp().hashCode();
        }
        if (getEmaCom() != null) {
            _hashCode += getEmaCom().hashCode();
        }
        if (getEmaPar() != null) {
            _hashCode += getEmaPar().hashCode();
        }
        if (getEmiCid() != null) {
            _hashCode += getEmiCid().hashCode();
        }
        if (getEmiPsp() != null) {
            _hashCode += getEmiPsp().hashCode();
        }
        if (getEmiRic() != null) {
            _hashCode += getEmiRic().hashCode();
        }
        if (getEndCep() != null) {
            _hashCode += getEndCep().hashCode();
        }
        if (getEndCpl() != null) {
            _hashCode += getEndCpl().hashCode();
        }
        if (getEndNum() != null) {
            _hashCode += getEndNum().hashCode();
        }
        if (getEndRua() != null) {
            _hashCode += getEndRua().hashCode();
        }
        if (getEstCid() != null) {
            _hashCode += getEstCid().hashCode();
        }
        if (getEstCiv() != null) {
            _hashCode += getEstCiv().hashCode();
        }
        if (getEstCnh() != null) {
            _hashCode += getEstCnh().hashCode();
        }
        if (getEstNas() != null) {
            _hashCode += getEstNas().hashCode();
        }
        if (getEstPad() != null) {
            _hashCode += getEstPad().hashCode();
        }
        if (getEstPsp() != null) {
            _hashCode += getEstPsp().hashCode();
        }
        if (getEstRic() != null) {
            _hashCode += getEstRic().hashCode();
        }
        if (getExcPsa() != null) {
            _hashCode += getExcPsa().hashCode();
        }
        if (getFicReg() != null) {
            _hashCode += getFicReg().hashCode();
        }
        if (getFimArm() != null) {
            _hashCode += getFimArm().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getFolCiv() != null) {
            _hashCode += getFolCiv().hashCode();
        }
        if (getIncPsa() != null) {
            _hashCode += getIncPsa().hashCode();
        }
        if (getIniArm() != null) {
            _hashCode += getIniArm().hashCode();
        }
        if (getLivCiv() != null) {
            _hashCode += getLivCiv().hashCode();
        }
        if (getMaSPsa() != null) {
            _hashCode += getMaSPsa().hashCode();
        }
        if (getMatCCv() != null) {
            _hashCode += getMatCCv().hashCode();
        }
        if (getMatCiv() != null) {
            _hashCode += getMatCiv().hashCode();
        }
        if (getMatIpe() != null) {
            _hashCode += getMatIpe().hashCode();
        }
        if (getMatPsa() != null) {
            _hashCode += getMatPsa().hashCode();
        }
        if (getNasViv() != null) {
            _hashCode += getNasViv().hashCode();
        }
        if (getNifExt() != null) {
            _hashCode += getNifExt().hashCode();
        }
        if (getNivSal() != null) {
            _hashCode += getNivSal().hashCode();
        }
        if (getNomCom() != null) {
            _hashCode += getNomCom().hashCode();
        }
        if (getNomRad() != null) {
            _hashCode += getNomRad().hashCode();
        }
        if (getNomSoc() != null) {
            _hashCode += getNomSoc().hashCode();
        }
        if (getNroArm() != null) {
            _hashCode += getNroArm().hashCode();
        }
        if (getNumCad() != null) {
            _hashCode += getNumCad().hashCode();
        }
        if (getNumCid() != null) {
            _hashCode += getNumCid().hashCode();
        }
        if (getNumCnh() != null) {
            _hashCode += getNumCnh().hashCode();
        }
        if (getNumDdd2() != null) {
            _hashCode += getNumDdd2().hashCode();
        }
        if (getNumDdi2() != null) {
            _hashCode += getNumDdi2().hashCode();
        }
        if (getNumEle() != null) {
            _hashCode += getNumEle().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getNumPsp() != null) {
            _hashCode += getNumPsp().hashCode();
        }
        if (getNumRam() != null) {
            _hashCode += getNumRam().hashCode();
        }
        if (getNumRam2() != null) {
            _hashCode += getNumRam2().hashCode();
        }
        if (getNumRes() != null) {
            _hashCode += getNumRes().hashCode();
        }
        if (getNumRic() != null) {
            _hashCode += getNumRic().hashCode();
        }
        if (getNumTel() != null) {
            _hashCode += getNumTel().hashCode();
        }
        if (getNumTel2() != null) {
            _hashCode += getNumTel2().hashCode();
        }
        if (getOrgCnh() != null) {
            _hashCode += getOrgCnh().hashCode();
        }
        if (getPaiNas() != null) {
            _hashCode += getPaiNas().hashCode();
        }
        if (getPaiPsp() != null) {
            _hashCode += getPaiPsp().hashCode();
        }
        if (getPreTrb() != null) {
            _hashCode += getPreTrb().hashCode();
        }
        if (getPriCnh() != null) {
            _hashCode += getPriCnh().hashCode();
        }
        if (getProCon() != null) {
            _hashCode += getProCon().hashCode();
        }
        if (getProExm() != null) {
            _hashCode += getProExm().hashCode();
        }
        if (getRegCon() != null) {
            _hashCode += getRegCon().hashCode();
        }
        if (getRegMte() != null) {
            _hashCode += getRegMte().hashCode();
        }
        if (getSecEle() != null) {
            _hashCode += getSecEle().hashCode();
        }
        if (getSitIpe() != null) {
            _hashCode += getSitIpe().hashCode();
        }
        if (getSitReg() != null) {
            _hashCode += getSitReg().hashCode();
        }
        if (getTipCer() != null) {
            _hashCode += getTipCer().hashCode();
        }
        if (getTipCol() != null) {
            _hashCode += getTipCol().hashCode();
        }
        if (getTipLgr() != null) {
            _hashCode += getTipLgr().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        if (getTipPsa() != null) {
            _hashCode += getTipPsa().hashCode();
        }
        if (getUltExm() != null) {
            _hashCode += getUltExm().hashCode();
        }
        if (getVenCnh() != null) {
            _hashCode += getVenCnh().hashCode();
        }
        if (getZonEle() != null) {
            _hashCode += getZonEle().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FichaComplementarFichaComplementar6In.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fichaComplementarFichaComplementar6In"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("altIpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "altIpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("andTrb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "andTrb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("carCiv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "carCiv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("carSus");
        elemField.setXmlName(new javax.xml.namespace.QName("", "carSus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("catCnh");
        elemField.setXmlName(new javax.xml.namespace.QName("", "catCnh"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("catRes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "catRes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("catVei");
        elemField.setXmlName(new javax.xml.namespace.QName("", "catVei"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cciNas");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cciNas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cidCiv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cidCiv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cidEmi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cidEmi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cidRic");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cidRic"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("claAss");
        elemField.setXmlName(new javax.xml.namespace.QName("", "claAss"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("claSal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "claSal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codBai");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codBai"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPai");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPai"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPrf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPrf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPsa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPsa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codQua");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codQua"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRlr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRlr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datCnh");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datCnh"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datExp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datExp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dddTel");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dddTel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ddiTel");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ddiTel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("demCiv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "demCiv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("demPsp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "demPsp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dexCid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dexCid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dexRic");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dexRic"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("docIdn");
        elemField.setXmlName(new javax.xml.namespace.QName("", "docIdn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("durCon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "durCon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dvaPsp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dvaPsp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emaCom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "emaCom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emaPar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "emaPar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emiCid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "emiCid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emiPsp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "emiPsp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emiRic");
        elemField.setXmlName(new javax.xml.namespace.QName("", "emiRic"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endCep");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endCep"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endCpl");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endCpl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endNum");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endNum"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endRua");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endRua"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estCid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estCid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estCiv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estCiv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estCnh");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estCnh"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estNas");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estNas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estPad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estPad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estPsp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estPsp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estRic");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estRic"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("excPsa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "excPsa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ficReg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ficReg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fimArm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fimArm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("folCiv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "folCiv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("incPsa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "incPsa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("iniArm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "iniArm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("livCiv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "livCiv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maSPsa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "maSPsa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("matCCv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "matCCv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("matCiv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "matCiv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("matIpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "matIpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("matPsa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "matPsa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nasViv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nasViv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nifExt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nifExt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nivSal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nivSal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomCom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomCom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomRad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomRad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomSoc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomSoc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nroArm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nroArm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCnh");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCnh"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numDdd2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numDdd2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numDdi2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numDdi2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEle");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numPsp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numPsp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numRam");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numRam"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numRam2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numRam2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numRes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numRes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numRic");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numRic"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numTel");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numTel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numTel2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numTel2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orgCnh");
        elemField.setXmlName(new javax.xml.namespace.QName("", "orgCnh"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paiNas");
        elemField.setXmlName(new javax.xml.namespace.QName("", "paiNas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paiPsp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "paiPsp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("preTrb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "preTrb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("priCnh");
        elemField.setXmlName(new javax.xml.namespace.QName("", "priCnh"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("proCon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "proCon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("proExm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "proExm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("regCon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "regCon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("regMte");
        elemField.setXmlName(new javax.xml.namespace.QName("", "regMte"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("secEle");
        elemField.setXmlName(new javax.xml.namespace.QName("", "secEle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitIpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitIpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitReg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitReg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipLgr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipLgr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipPsa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipPsa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ultExm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ultExm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("venCnh");
        elemField.setXmlName(new javax.xml.namespace.QName("", "venCnh"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zonEle");
        elemField.setXmlName(new javax.xml.namespace.QName("", "zonEle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
