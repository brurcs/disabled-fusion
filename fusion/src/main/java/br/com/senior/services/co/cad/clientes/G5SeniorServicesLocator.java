/**
 * G5SeniorServicesLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package br.com.senior.services.co.cad.clientes;

import br.com.senior.services.vetorh.constants.WSConfigs;

public class G5SeniorServicesLocator extends org.apache.axis.client.Service implements br.com.senior.services.co.cad.clientes.G5SeniorServices {

    public G5SeniorServicesLocator() {
    }


    public G5SeniorServicesLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public G5SeniorServicesLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for sapiens_Synccom_senior_g5_co_cad_clientesPort
    private java.lang.String sapiens_Synccom_senior_g5_co_cad_clientesPort_address = "http://"+(new WSConfigs()).getHostService()+"/g5-senior-services/sapiens_Synccom_senior_g5_co_cad_clientes";

    public java.lang.String getsapiens_Synccom_senior_g5_co_cad_clientesPortAddress() {
        return sapiens_Synccom_senior_g5_co_cad_clientesPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String sapiens_Synccom_senior_g5_co_cad_clientesPortWSDDServiceName = "sapiens_Synccom_senior_g5_co_cad_clientesPort";

    public java.lang.String getsapiens_Synccom_senior_g5_co_cad_clientesPortWSDDServiceName() {
        return sapiens_Synccom_senior_g5_co_cad_clientesPortWSDDServiceName;
    }

    public void setsapiens_Synccom_senior_g5_co_cad_clientesPortWSDDServiceName(java.lang.String name) {
        sapiens_Synccom_senior_g5_co_cad_clientesPortWSDDServiceName = name;
    }

    public br.com.senior.services.co.cad.clientes.Sapiens_Synccom_senior_g5_co_cad_clientes getsapiens_Synccom_senior_g5_co_cad_clientesPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(sapiens_Synccom_senior_g5_co_cad_clientesPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getsapiens_Synccom_senior_g5_co_cad_clientesPort(endpoint);
    }

    public br.com.senior.services.co.cad.clientes.Sapiens_Synccom_senior_g5_co_cad_clientes getsapiens_Synccom_senior_g5_co_cad_clientesPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            br.com.senior.services.co.cad.clientes.Sapiens_Synccom_senior_g5_co_cad_clientesPortBindingStub _stub = new br.com.senior.services.co.cad.clientes.Sapiens_Synccom_senior_g5_co_cad_clientesPortBindingStub(portAddress, this);
            _stub.setPortName(getsapiens_Synccom_senior_g5_co_cad_clientesPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setsapiens_Synccom_senior_g5_co_cad_clientesPortEndpointAddress(java.lang.String address) {
        sapiens_Synccom_senior_g5_co_cad_clientesPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (br.com.senior.services.co.cad.clientes.Sapiens_Synccom_senior_g5_co_cad_clientes.class.isAssignableFrom(serviceEndpointInterface)) {
                br.com.senior.services.co.cad.clientes.Sapiens_Synccom_senior_g5_co_cad_clientesPortBindingStub _stub = new br.com.senior.services.co.cad.clientes.Sapiens_Synccom_senior_g5_co_cad_clientesPortBindingStub(new java.net.URL(sapiens_Synccom_senior_g5_co_cad_clientesPort_address), this);
                _stub.setPortName(getsapiens_Synccom_senior_g5_co_cad_clientesPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("sapiens_Synccom_senior_g5_co_cad_clientesPort".equals(inputPortName)) {
            return getsapiens_Synccom_senior_g5_co_cad_clientesPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://services.senior.com.br", "g5-senior-services");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://services.senior.com.br", "sapiens_Synccom_senior_g5_co_cad_clientesPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("sapiens_Synccom_senior_g5_co_cad_clientesPort".equals(portName)) {
            setsapiens_Synccom_senior_g5_co_cad_clientesPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
