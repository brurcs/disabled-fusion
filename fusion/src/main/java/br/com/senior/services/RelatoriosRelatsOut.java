/**
 * RelatoriosRelatoriosOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class RelatoriosRelatsOut  implements java.io.Serializable {
    private java.lang.String erroExecucao;

    private java.lang.String prLOG;

    private java.lang.String prRetorno;

    public RelatoriosRelatsOut() {
    }

    public RelatoriosRelatsOut(
           java.lang.String erroExecucao,
           java.lang.String prLOG,
           java.lang.String prRetorno) {
           this.erroExecucao = erroExecucao;
           this.prLOG = prLOG;
           this.prRetorno = prRetorno;
    }


    /**
     * Gets the erroExecucao value for this RelatoriosRelatoriosOut.
     * 
     * @return erroExecucao
     */
    public java.lang.String getErroExecucao() {
        return erroExecucao;
    }


    /**
     * Sets the erroExecucao value for this RelatoriosRelatoriosOut.
     * 
     * @param erroExecucao
     */
    public void setErroExecucao(java.lang.String erroExecucao) {
        this.erroExecucao = erroExecucao;
    }


    /**
     * Gets the prLOG value for this RelatoriosRelatoriosOut.
     * 
     * @return prLOG
     */
    public java.lang.String getPrLOG() {
        return prLOG;
    }


    /**
     * Sets the prLOG value for this RelatoriosRelatoriosOut.
     * 
     * @param prLOG
     */
    public void setPrLOG(java.lang.String prLOG) {
        this.prLOG = prLOG;
    }


    /**
     * Gets the prRetorno value for this RelatoriosRelatoriosOut.
     * 
     * @return prRetorno
     */
    public java.lang.String getPrRetorno() {
        return prRetorno;
    }


    /**
     * Sets the prRetorno value for this RelatoriosRelatoriosOut.
     * 
     * @param prRetorno
     */
    public void setPrRetorno(java.lang.String prRetorno) {
        this.prRetorno = prRetorno;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RelatoriosRelatsOut)) return false;
        RelatoriosRelatsOut other = (RelatoriosRelatsOut) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.erroExecucao==null && other.getErroExecucao()==null) || 
             (this.erroExecucao!=null &&
              this.erroExecucao.equals(other.getErroExecucao()))) &&
            ((this.prLOG==null && other.getPrLOG()==null) || 
             (this.prLOG!=null &&
              this.prLOG.equals(other.getPrLOG()))) &&
            ((this.prRetorno==null && other.getPrRetorno()==null) || 
             (this.prRetorno!=null &&
              this.prRetorno.equals(other.getPrRetorno())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getErroExecucao() != null) {
            _hashCode += getErroExecucao().hashCode();
        }
        if (getPrLOG() != null) {
            _hashCode += getPrLOG().hashCode();
        }
        if (getPrRetorno() != null) {
            _hashCode += getPrRetorno().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RelatoriosRelatsOut.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "relatoriosRelatoriosOut"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("erroExecucao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "erroExecucao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prLOG");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prLOG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
