/**
 * CalculoApuracaoAcertarIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class CalculoApuracaoAcertarIn  implements java.io.Serializable {
    private java.lang.String conVer;

    private java.lang.String datAce;

    private java.lang.Integer expPes;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String marGer;

    private java.lang.Integer numEmp;

    private br.com.senior.services.CalculoApuracaoAcertarInTApuDat[] TApuDat;

    private br.com.senior.services.CalculoApuracaoAcertarInTMarDat[] TMarDat;

    private java.lang.Integer tipCol;

    private java.lang.String tipOpe;

    public CalculoApuracaoAcertarIn() {
    }

    public CalculoApuracaoAcertarIn(
           java.lang.String conVer,
           java.lang.String datAce,
           java.lang.Integer expPes,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String marGer,
           java.lang.Integer numEmp,
           br.com.senior.services.CalculoApuracaoAcertarInTApuDat[] TApuDat,
           br.com.senior.services.CalculoApuracaoAcertarInTMarDat[] TMarDat,
           java.lang.Integer tipCol,
           java.lang.String tipOpe) {
           this.conVer = conVer;
           this.datAce = datAce;
           this.expPes = expPes;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.marGer = marGer;
           this.numEmp = numEmp;
           this.TApuDat = TApuDat;
           this.TMarDat = TMarDat;
           this.tipCol = tipCol;
           this.tipOpe = tipOpe;
    }


    /**
     * Gets the conVer value for this CalculoApuracaoAcertarIn.
     * 
     * @return conVer
     */
    public java.lang.String getConVer() {
        return conVer;
    }


    /**
     * Sets the conVer value for this CalculoApuracaoAcertarIn.
     * 
     * @param conVer
     */
    public void setConVer(java.lang.String conVer) {
        this.conVer = conVer;
    }


    /**
     * Gets the datAce value for this CalculoApuracaoAcertarIn.
     * 
     * @return datAce
     */
    public java.lang.String getDatAce() {
        return datAce;
    }


    /**
     * Sets the datAce value for this CalculoApuracaoAcertarIn.
     * 
     * @param datAce
     */
    public void setDatAce(java.lang.String datAce) {
        this.datAce = datAce;
    }


    /**
     * Gets the expPes value for this CalculoApuracaoAcertarIn.
     * 
     * @return expPes
     */
    public java.lang.Integer getExpPes() {
        return expPes;
    }


    /**
     * Sets the expPes value for this CalculoApuracaoAcertarIn.
     * 
     * @param expPes
     */
    public void setExpPes(java.lang.Integer expPes) {
        this.expPes = expPes;
    }


    /**
     * Gets the flowInstanceID value for this CalculoApuracaoAcertarIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this CalculoApuracaoAcertarIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this CalculoApuracaoAcertarIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this CalculoApuracaoAcertarIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the marGer value for this CalculoApuracaoAcertarIn.
     * 
     * @return marGer
     */
    public java.lang.String getMarGer() {
        return marGer;
    }


    /**
     * Sets the marGer value for this CalculoApuracaoAcertarIn.
     * 
     * @param marGer
     */
    public void setMarGer(java.lang.String marGer) {
        this.marGer = marGer;
    }


    /**
     * Gets the numEmp value for this CalculoApuracaoAcertarIn.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this CalculoApuracaoAcertarIn.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the TApuDat value for this CalculoApuracaoAcertarIn.
     * 
     * @return TApuDat
     */
    public br.com.senior.services.CalculoApuracaoAcertarInTApuDat[] getTApuDat() {
        return TApuDat;
    }


    /**
     * Sets the TApuDat value for this CalculoApuracaoAcertarIn.
     * 
     * @param TApuDat
     */
    public void setTApuDat(br.com.senior.services.CalculoApuracaoAcertarInTApuDat[] TApuDat) {
        this.TApuDat = TApuDat;
    }

    public br.com.senior.services.CalculoApuracaoAcertarInTApuDat getTApuDat(int i) {
        return this.TApuDat[i];
    }

    public void setTApuDat(int i, br.com.senior.services.CalculoApuracaoAcertarInTApuDat _value) {
        this.TApuDat[i] = _value;
    }


    /**
     * Gets the TMarDat value for this CalculoApuracaoAcertarIn.
     * 
     * @return TMarDat
     */
    public br.com.senior.services.CalculoApuracaoAcertarInTMarDat[] getTMarDat() {
        return TMarDat;
    }


    /**
     * Sets the TMarDat value for this CalculoApuracaoAcertarIn.
     * 
     * @param TMarDat
     */
    public void setTMarDat(br.com.senior.services.CalculoApuracaoAcertarInTMarDat[] TMarDat) {
        this.TMarDat = TMarDat;
    }

    public br.com.senior.services.CalculoApuracaoAcertarInTMarDat getTMarDat(int i) {
        return this.TMarDat[i];
    }

    public void setTMarDat(int i, br.com.senior.services.CalculoApuracaoAcertarInTMarDat _value) {
        this.TMarDat[i] = _value;
    }


    /**
     * Gets the tipCol value for this CalculoApuracaoAcertarIn.
     * 
     * @return tipCol
     */
    public java.lang.Integer getTipCol() {
        return tipCol;
    }


    /**
     * Sets the tipCol value for this CalculoApuracaoAcertarIn.
     * 
     * @param tipCol
     */
    public void setTipCol(java.lang.Integer tipCol) {
        this.tipCol = tipCol;
    }


    /**
     * Gets the tipOpe value for this CalculoApuracaoAcertarIn.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this CalculoApuracaoAcertarIn.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CalculoApuracaoAcertarIn)) return false;
        CalculoApuracaoAcertarIn other = (CalculoApuracaoAcertarIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.conVer==null && other.getConVer()==null) || 
             (this.conVer!=null &&
              this.conVer.equals(other.getConVer()))) &&
            ((this.datAce==null && other.getDatAce()==null) || 
             (this.datAce!=null &&
              this.datAce.equals(other.getDatAce()))) &&
            ((this.expPes==null && other.getExpPes()==null) || 
             (this.expPes!=null &&
              this.expPes.equals(other.getExpPes()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.marGer==null && other.getMarGer()==null) || 
             (this.marGer!=null &&
              this.marGer.equals(other.getMarGer()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.TApuDat==null && other.getTApuDat()==null) || 
             (this.TApuDat!=null &&
              java.util.Arrays.equals(this.TApuDat, other.getTApuDat()))) &&
            ((this.TMarDat==null && other.getTMarDat()==null) || 
             (this.TMarDat!=null &&
              java.util.Arrays.equals(this.TMarDat, other.getTMarDat()))) &&
            ((this.tipCol==null && other.getTipCol()==null) || 
             (this.tipCol!=null &&
              this.tipCol.equals(other.getTipCol()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getConVer() != null) {
            _hashCode += getConVer().hashCode();
        }
        if (getDatAce() != null) {
            _hashCode += getDatAce().hashCode();
        }
        if (getExpPes() != null) {
            _hashCode += getExpPes().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getMarGer() != null) {
            _hashCode += getMarGer().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getTApuDat() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTApuDat());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTApuDat(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTMarDat() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTMarDat());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTMarDat(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTipCol() != null) {
            _hashCode += getTipCol().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CalculoApuracaoAcertarIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "calculoApuracaoAcertarIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conVer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "conVer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAce");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAce"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expPes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "expPes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("marGer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "marGer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TApuDat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TApuDat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "calculoApuracaoAcertarInTApuDat"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TMarDat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TMarDat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "calculoApuracaoAcertarInTMarDat"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
