/**
 * ClientesExportar2OutClienteContatos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package br.com.senior.services.co.cad.clientes;

public class ClientesExportar2OutClienteContatos  implements java.io.Serializable {
    private java.lang.String carCto;

    private java.lang.Integer codCli;

    private java.lang.Integer codNiv;

    private java.lang.String datAlt;

    private java.lang.String datGer;

    private java.lang.String datNas;

    private java.lang.String datPal;

    private java.lang.String faxCto;

    private java.lang.String fonCt2;

    private java.lang.String fonCt3;

    private java.lang.String fonCto;

    private java.lang.String hobCon;

    private java.lang.Integer horAlt;

    private java.lang.Integer horGer;

    private java.lang.Integer horPal;

    private java.lang.Integer indExp;

    private java.lang.String intNet;

    private java.lang.String nivCto;

    private java.lang.String nomCto;

    private java.lang.Integer ramCt2;

    private java.lang.Integer ramCt3;

    private java.lang.Integer ramCto;

    private java.lang.Integer seqCto;

    private java.lang.String setCto;

    private java.lang.String sitCto;

    private java.lang.String timCon;

    private java.lang.Double usuAlt;

    private java.lang.Double usuGer;

    public ClientesExportar2OutClienteContatos() {
    }

    public ClientesExportar2OutClienteContatos(
           java.lang.String carCto,
           java.lang.Integer codCli,
           java.lang.Integer codNiv,
           java.lang.String datAlt,
           java.lang.String datGer,
           java.lang.String datNas,
           java.lang.String datPal,
           java.lang.String faxCto,
           java.lang.String fonCt2,
           java.lang.String fonCt3,
           java.lang.String fonCto,
           java.lang.String hobCon,
           java.lang.Integer horAlt,
           java.lang.Integer horGer,
           java.lang.Integer horPal,
           java.lang.Integer indExp,
           java.lang.String intNet,
           java.lang.String nivCto,
           java.lang.String nomCto,
           java.lang.Integer ramCt2,
           java.lang.Integer ramCt3,
           java.lang.Integer ramCto,
           java.lang.Integer seqCto,
           java.lang.String setCto,
           java.lang.String sitCto,
           java.lang.String timCon,
           java.lang.Double usuAlt,
           java.lang.Double usuGer) {
           this.carCto = carCto;
           this.codCli = codCli;
           this.codNiv = codNiv;
           this.datAlt = datAlt;
           this.datGer = datGer;
           this.datNas = datNas;
           this.datPal = datPal;
           this.faxCto = faxCto;
           this.fonCt2 = fonCt2;
           this.fonCt3 = fonCt3;
           this.fonCto = fonCto;
           this.hobCon = hobCon;
           this.horAlt = horAlt;
           this.horGer = horGer;
           this.horPal = horPal;
           this.indExp = indExp;
           this.intNet = intNet;
           this.nivCto = nivCto;
           this.nomCto = nomCto;
           this.ramCt2 = ramCt2;
           this.ramCt3 = ramCt3;
           this.ramCto = ramCto;
           this.seqCto = seqCto;
           this.setCto = setCto;
           this.sitCto = sitCto;
           this.timCon = timCon;
           this.usuAlt = usuAlt;
           this.usuGer = usuGer;
    }


    /**
     * Gets the carCto value for this ClientesExportar2OutClienteContatos.
     * 
     * @return carCto
     */
    public java.lang.String getCarCto() {
        return carCto;
    }


    /**
     * Sets the carCto value for this ClientesExportar2OutClienteContatos.
     * 
     * @param carCto
     */
    public void setCarCto(java.lang.String carCto) {
        this.carCto = carCto;
    }


    /**
     * Gets the codCli value for this ClientesExportar2OutClienteContatos.
     * 
     * @return codCli
     */
    public java.lang.Integer getCodCli() {
        return codCli;
    }


    /**
     * Sets the codCli value for this ClientesExportar2OutClienteContatos.
     * 
     * @param codCli
     */
    public void setCodCli(java.lang.Integer codCli) {
        this.codCli = codCli;
    }


    /**
     * Gets the codNiv value for this ClientesExportar2OutClienteContatos.
     * 
     * @return codNiv
     */
    public java.lang.Integer getCodNiv() {
        return codNiv;
    }


    /**
     * Sets the codNiv value for this ClientesExportar2OutClienteContatos.
     * 
     * @param codNiv
     */
    public void setCodNiv(java.lang.Integer codNiv) {
        this.codNiv = codNiv;
    }


    /**
     * Gets the datAlt value for this ClientesExportar2OutClienteContatos.
     * 
     * @return datAlt
     */
    public java.lang.String getDatAlt() {
        return datAlt;
    }


    /**
     * Sets the datAlt value for this ClientesExportar2OutClienteContatos.
     * 
     * @param datAlt
     */
    public void setDatAlt(java.lang.String datAlt) {
        this.datAlt = datAlt;
    }


    /**
     * Gets the datGer value for this ClientesExportar2OutClienteContatos.
     * 
     * @return datGer
     */
    public java.lang.String getDatGer() {
        return datGer;
    }


    /**
     * Sets the datGer value for this ClientesExportar2OutClienteContatos.
     * 
     * @param datGer
     */
    public void setDatGer(java.lang.String datGer) {
        this.datGer = datGer;
    }


    /**
     * Gets the datNas value for this ClientesExportar2OutClienteContatos.
     * 
     * @return datNas
     */
    public java.lang.String getDatNas() {
        return datNas;
    }


    /**
     * Sets the datNas value for this ClientesExportar2OutClienteContatos.
     * 
     * @param datNas
     */
    public void setDatNas(java.lang.String datNas) {
        this.datNas = datNas;
    }


    /**
     * Gets the datPal value for this ClientesExportar2OutClienteContatos.
     * 
     * @return datPal
     */
    public java.lang.String getDatPal() {
        return datPal;
    }


    /**
     * Sets the datPal value for this ClientesExportar2OutClienteContatos.
     * 
     * @param datPal
     */
    public void setDatPal(java.lang.String datPal) {
        this.datPal = datPal;
    }


    /**
     * Gets the faxCto value for this ClientesExportar2OutClienteContatos.
     * 
     * @return faxCto
     */
    public java.lang.String getFaxCto() {
        return faxCto;
    }


    /**
     * Sets the faxCto value for this ClientesExportar2OutClienteContatos.
     * 
     * @param faxCto
     */
    public void setFaxCto(java.lang.String faxCto) {
        this.faxCto = faxCto;
    }


    /**
     * Gets the fonCt2 value for this ClientesExportar2OutClienteContatos.
     * 
     * @return fonCt2
     */
    public java.lang.String getFonCt2() {
        return fonCt2;
    }


    /**
     * Sets the fonCt2 value for this ClientesExportar2OutClienteContatos.
     * 
     * @param fonCt2
     */
    public void setFonCt2(java.lang.String fonCt2) {
        this.fonCt2 = fonCt2;
    }


    /**
     * Gets the fonCt3 value for this ClientesExportar2OutClienteContatos.
     * 
     * @return fonCt3
     */
    public java.lang.String getFonCt3() {
        return fonCt3;
    }


    /**
     * Sets the fonCt3 value for this ClientesExportar2OutClienteContatos.
     * 
     * @param fonCt3
     */
    public void setFonCt3(java.lang.String fonCt3) {
        this.fonCt3 = fonCt3;
    }


    /**
     * Gets the fonCto value for this ClientesExportar2OutClienteContatos.
     * 
     * @return fonCto
     */
    public java.lang.String getFonCto() {
        return fonCto;
    }


    /**
     * Sets the fonCto value for this ClientesExportar2OutClienteContatos.
     * 
     * @param fonCto
     */
    public void setFonCto(java.lang.String fonCto) {
        this.fonCto = fonCto;
    }


    /**
     * Gets the hobCon value for this ClientesExportar2OutClienteContatos.
     * 
     * @return hobCon
     */
    public java.lang.String getHobCon() {
        return hobCon;
    }


    /**
     * Sets the hobCon value for this ClientesExportar2OutClienteContatos.
     * 
     * @param hobCon
     */
    public void setHobCon(java.lang.String hobCon) {
        this.hobCon = hobCon;
    }


    /**
     * Gets the horAlt value for this ClientesExportar2OutClienteContatos.
     * 
     * @return horAlt
     */
    public java.lang.Integer getHorAlt() {
        return horAlt;
    }


    /**
     * Sets the horAlt value for this ClientesExportar2OutClienteContatos.
     * 
     * @param horAlt
     */
    public void setHorAlt(java.lang.Integer horAlt) {
        this.horAlt = horAlt;
    }


    /**
     * Gets the horGer value for this ClientesExportar2OutClienteContatos.
     * 
     * @return horGer
     */
    public java.lang.Integer getHorGer() {
        return horGer;
    }


    /**
     * Sets the horGer value for this ClientesExportar2OutClienteContatos.
     * 
     * @param horGer
     */
    public void setHorGer(java.lang.Integer horGer) {
        this.horGer = horGer;
    }


    /**
     * Gets the horPal value for this ClientesExportar2OutClienteContatos.
     * 
     * @return horPal
     */
    public java.lang.Integer getHorPal() {
        return horPal;
    }


    /**
     * Sets the horPal value for this ClientesExportar2OutClienteContatos.
     * 
     * @param horPal
     */
    public void setHorPal(java.lang.Integer horPal) {
        this.horPal = horPal;
    }


    /**
     * Gets the indExp value for this ClientesExportar2OutClienteContatos.
     * 
     * @return indExp
     */
    public java.lang.Integer getIndExp() {
        return indExp;
    }


    /**
     * Sets the indExp value for this ClientesExportar2OutClienteContatos.
     * 
     * @param indExp
     */
    public void setIndExp(java.lang.Integer indExp) {
        this.indExp = indExp;
    }


    /**
     * Gets the intNet value for this ClientesExportar2OutClienteContatos.
     * 
     * @return intNet
     */
    public java.lang.String getIntNet() {
        return intNet;
    }


    /**
     * Sets the intNet value for this ClientesExportar2OutClienteContatos.
     * 
     * @param intNet
     */
    public void setIntNet(java.lang.String intNet) {
        this.intNet = intNet;
    }


    /**
     * Gets the nivCto value for this ClientesExportar2OutClienteContatos.
     * 
     * @return nivCto
     */
    public java.lang.String getNivCto() {
        return nivCto;
    }


    /**
     * Sets the nivCto value for this ClientesExportar2OutClienteContatos.
     * 
     * @param nivCto
     */
    public void setNivCto(java.lang.String nivCto) {
        this.nivCto = nivCto;
    }


    /**
     * Gets the nomCto value for this ClientesExportar2OutClienteContatos.
     * 
     * @return nomCto
     */
    public java.lang.String getNomCto() {
        return nomCto;
    }


    /**
     * Sets the nomCto value for this ClientesExportar2OutClienteContatos.
     * 
     * @param nomCto
     */
    public void setNomCto(java.lang.String nomCto) {
        this.nomCto = nomCto;
    }


    /**
     * Gets the ramCt2 value for this ClientesExportar2OutClienteContatos.
     * 
     * @return ramCt2
     */
    public java.lang.Integer getRamCt2() {
        return ramCt2;
    }


    /**
     * Sets the ramCt2 value for this ClientesExportar2OutClienteContatos.
     * 
     * @param ramCt2
     */
    public void setRamCt2(java.lang.Integer ramCt2) {
        this.ramCt2 = ramCt2;
    }


    /**
     * Gets the ramCt3 value for this ClientesExportar2OutClienteContatos.
     * 
     * @return ramCt3
     */
    public java.lang.Integer getRamCt3() {
        return ramCt3;
    }


    /**
     * Sets the ramCt3 value for this ClientesExportar2OutClienteContatos.
     * 
     * @param ramCt3
     */
    public void setRamCt3(java.lang.Integer ramCt3) {
        this.ramCt3 = ramCt3;
    }


    /**
     * Gets the ramCto value for this ClientesExportar2OutClienteContatos.
     * 
     * @return ramCto
     */
    public java.lang.Integer getRamCto() {
        return ramCto;
    }


    /**
     * Sets the ramCto value for this ClientesExportar2OutClienteContatos.
     * 
     * @param ramCto
     */
    public void setRamCto(java.lang.Integer ramCto) {
        this.ramCto = ramCto;
    }


    /**
     * Gets the seqCto value for this ClientesExportar2OutClienteContatos.
     * 
     * @return seqCto
     */
    public java.lang.Integer getSeqCto() {
        return seqCto;
    }


    /**
     * Sets the seqCto value for this ClientesExportar2OutClienteContatos.
     * 
     * @param seqCto
     */
    public void setSeqCto(java.lang.Integer seqCto) {
        this.seqCto = seqCto;
    }


    /**
     * Gets the setCto value for this ClientesExportar2OutClienteContatos.
     * 
     * @return setCto
     */
    public java.lang.String getSetCto() {
        return setCto;
    }


    /**
     * Sets the setCto value for this ClientesExportar2OutClienteContatos.
     * 
     * @param setCto
     */
    public void setSetCto(java.lang.String setCto) {
        this.setCto = setCto;
    }


    /**
     * Gets the sitCto value for this ClientesExportar2OutClienteContatos.
     * 
     * @return sitCto
     */
    public java.lang.String getSitCto() {
        return sitCto;
    }


    /**
     * Sets the sitCto value for this ClientesExportar2OutClienteContatos.
     * 
     * @param sitCto
     */
    public void setSitCto(java.lang.String sitCto) {
        this.sitCto = sitCto;
    }


    /**
     * Gets the timCon value for this ClientesExportar2OutClienteContatos.
     * 
     * @return timCon
     */
    public java.lang.String getTimCon() {
        return timCon;
    }


    /**
     * Sets the timCon value for this ClientesExportar2OutClienteContatos.
     * 
     * @param timCon
     */
    public void setTimCon(java.lang.String timCon) {
        this.timCon = timCon;
    }


    /**
     * Gets the usuAlt value for this ClientesExportar2OutClienteContatos.
     * 
     * @return usuAlt
     */
    public java.lang.Double getUsuAlt() {
        return usuAlt;
    }


    /**
     * Sets the usuAlt value for this ClientesExportar2OutClienteContatos.
     * 
     * @param usuAlt
     */
    public void setUsuAlt(java.lang.Double usuAlt) {
        this.usuAlt = usuAlt;
    }


    /**
     * Gets the usuGer value for this ClientesExportar2OutClienteContatos.
     * 
     * @return usuGer
     */
    public java.lang.Double getUsuGer() {
        return usuGer;
    }


    /**
     * Sets the usuGer value for this ClientesExportar2OutClienteContatos.
     * 
     * @param usuGer
     */
    public void setUsuGer(java.lang.Double usuGer) {
        this.usuGer = usuGer;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientesExportar2OutClienteContatos)) return false;
        ClientesExportar2OutClienteContatos other = (ClientesExportar2OutClienteContatos) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.carCto==null && other.getCarCto()==null) || 
             (this.carCto!=null &&
              this.carCto.equals(other.getCarCto()))) &&
            ((this.codCli==null && other.getCodCli()==null) || 
             (this.codCli!=null &&
              this.codCli.equals(other.getCodCli()))) &&
            ((this.codNiv==null && other.getCodNiv()==null) || 
             (this.codNiv!=null &&
              this.codNiv.equals(other.getCodNiv()))) &&
            ((this.datAlt==null && other.getDatAlt()==null) || 
             (this.datAlt!=null &&
              this.datAlt.equals(other.getDatAlt()))) &&
            ((this.datGer==null && other.getDatGer()==null) || 
             (this.datGer!=null &&
              this.datGer.equals(other.getDatGer()))) &&
            ((this.datNas==null && other.getDatNas()==null) || 
             (this.datNas!=null &&
              this.datNas.equals(other.getDatNas()))) &&
            ((this.datPal==null && other.getDatPal()==null) || 
             (this.datPal!=null &&
              this.datPal.equals(other.getDatPal()))) &&
            ((this.faxCto==null && other.getFaxCto()==null) || 
             (this.faxCto!=null &&
              this.faxCto.equals(other.getFaxCto()))) &&
            ((this.fonCt2==null && other.getFonCt2()==null) || 
             (this.fonCt2!=null &&
              this.fonCt2.equals(other.getFonCt2()))) &&
            ((this.fonCt3==null && other.getFonCt3()==null) || 
             (this.fonCt3!=null &&
              this.fonCt3.equals(other.getFonCt3()))) &&
            ((this.fonCto==null && other.getFonCto()==null) || 
             (this.fonCto!=null &&
              this.fonCto.equals(other.getFonCto()))) &&
            ((this.hobCon==null && other.getHobCon()==null) || 
             (this.hobCon!=null &&
              this.hobCon.equals(other.getHobCon()))) &&
            ((this.horAlt==null && other.getHorAlt()==null) || 
             (this.horAlt!=null &&
              this.horAlt.equals(other.getHorAlt()))) &&
            ((this.horGer==null && other.getHorGer()==null) || 
             (this.horGer!=null &&
              this.horGer.equals(other.getHorGer()))) &&
            ((this.horPal==null && other.getHorPal()==null) || 
             (this.horPal!=null &&
              this.horPal.equals(other.getHorPal()))) &&
            ((this.indExp==null && other.getIndExp()==null) || 
             (this.indExp!=null &&
              this.indExp.equals(other.getIndExp()))) &&
            ((this.intNet==null && other.getIntNet()==null) || 
             (this.intNet!=null &&
              this.intNet.equals(other.getIntNet()))) &&
            ((this.nivCto==null && other.getNivCto()==null) || 
             (this.nivCto!=null &&
              this.nivCto.equals(other.getNivCto()))) &&
            ((this.nomCto==null && other.getNomCto()==null) || 
             (this.nomCto!=null &&
              this.nomCto.equals(other.getNomCto()))) &&
            ((this.ramCt2==null && other.getRamCt2()==null) || 
             (this.ramCt2!=null &&
              this.ramCt2.equals(other.getRamCt2()))) &&
            ((this.ramCt3==null && other.getRamCt3()==null) || 
             (this.ramCt3!=null &&
              this.ramCt3.equals(other.getRamCt3()))) &&
            ((this.ramCto==null && other.getRamCto()==null) || 
             (this.ramCto!=null &&
              this.ramCto.equals(other.getRamCto()))) &&
            ((this.seqCto==null && other.getSeqCto()==null) || 
             (this.seqCto!=null &&
              this.seqCto.equals(other.getSeqCto()))) &&
            ((this.setCto==null && other.getSetCto()==null) || 
             (this.setCto!=null &&
              this.setCto.equals(other.getSetCto()))) &&
            ((this.sitCto==null && other.getSitCto()==null) || 
             (this.sitCto!=null &&
              this.sitCto.equals(other.getSitCto()))) &&
            ((this.timCon==null && other.getTimCon()==null) || 
             (this.timCon!=null &&
              this.timCon.equals(other.getTimCon()))) &&
            ((this.usuAlt==null && other.getUsuAlt()==null) || 
             (this.usuAlt!=null &&
              this.usuAlt.equals(other.getUsuAlt()))) &&
            ((this.usuGer==null && other.getUsuGer()==null) || 
             (this.usuGer!=null &&
              this.usuGer.equals(other.getUsuGer())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCarCto() != null) {
            _hashCode += getCarCto().hashCode();
        }
        if (getCodCli() != null) {
            _hashCode += getCodCli().hashCode();
        }
        if (getCodNiv() != null) {
            _hashCode += getCodNiv().hashCode();
        }
        if (getDatAlt() != null) {
            _hashCode += getDatAlt().hashCode();
        }
        if (getDatGer() != null) {
            _hashCode += getDatGer().hashCode();
        }
        if (getDatNas() != null) {
            _hashCode += getDatNas().hashCode();
        }
        if (getDatPal() != null) {
            _hashCode += getDatPal().hashCode();
        }
        if (getFaxCto() != null) {
            _hashCode += getFaxCto().hashCode();
        }
        if (getFonCt2() != null) {
            _hashCode += getFonCt2().hashCode();
        }
        if (getFonCt3() != null) {
            _hashCode += getFonCt3().hashCode();
        }
        if (getFonCto() != null) {
            _hashCode += getFonCto().hashCode();
        }
        if (getHobCon() != null) {
            _hashCode += getHobCon().hashCode();
        }
        if (getHorAlt() != null) {
            _hashCode += getHorAlt().hashCode();
        }
        if (getHorGer() != null) {
            _hashCode += getHorGer().hashCode();
        }
        if (getHorPal() != null) {
            _hashCode += getHorPal().hashCode();
        }
        if (getIndExp() != null) {
            _hashCode += getIndExp().hashCode();
        }
        if (getIntNet() != null) {
            _hashCode += getIntNet().hashCode();
        }
        if (getNivCto() != null) {
            _hashCode += getNivCto().hashCode();
        }
        if (getNomCto() != null) {
            _hashCode += getNomCto().hashCode();
        }
        if (getRamCt2() != null) {
            _hashCode += getRamCt2().hashCode();
        }
        if (getRamCt3() != null) {
            _hashCode += getRamCt3().hashCode();
        }
        if (getRamCto() != null) {
            _hashCode += getRamCto().hashCode();
        }
        if (getSeqCto() != null) {
            _hashCode += getSeqCto().hashCode();
        }
        if (getSetCto() != null) {
            _hashCode += getSetCto().hashCode();
        }
        if (getSitCto() != null) {
            _hashCode += getSitCto().hashCode();
        }
        if (getTimCon() != null) {
            _hashCode += getTimCon().hashCode();
        }
        if (getUsuAlt() != null) {
            _hashCode += getUsuAlt().hashCode();
        }
        if (getUsuGer() != null) {
            _hashCode += getUsuGer().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientesExportar2OutClienteContatos.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesExportar2OutClienteContatos"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("carCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "carCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codNiv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codNiv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datGer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datGer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datNas");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datNas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datPal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datPal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("faxCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "faxCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonCt2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonCt2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonCt3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonCt3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hobCon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "hobCon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horGer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horGer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horPal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horPal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indExp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indExp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("intNet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "intNet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nivCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nivCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ramCt2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ramCt2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ramCt3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ramCt3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ramCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ramCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("setCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "setCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timCon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "timCon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuGer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuGer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
