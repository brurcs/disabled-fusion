/**
 * FichaBasicaAnuidadesInAnuidades.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.ficha.basica;

public class FichaBasicaAnuidadesInAnuidades  implements java.io.Serializable {
    private java.lang.String datAnu;

    private java.lang.Integer numAnu;

    private java.lang.Double perAnu;

    private java.lang.String tipAnu;

    private java.lang.String tipOpe;

    private java.lang.Double valAnu;

    public FichaBasicaAnuidadesInAnuidades() {
    }

    public FichaBasicaAnuidadesInAnuidades(
           java.lang.String datAnu,
           java.lang.Integer numAnu,
           java.lang.Double perAnu,
           java.lang.String tipAnu,
           java.lang.String tipOpe,
           java.lang.Double valAnu) {
           this.datAnu = datAnu;
           this.numAnu = numAnu;
           this.perAnu = perAnu;
           this.tipAnu = tipAnu;
           this.tipOpe = tipOpe;
           this.valAnu = valAnu;
    }


    /**
     * Gets the datAnu value for this FichaBasicaAnuidadesInAnuidades.
     * 
     * @return datAnu
     */
    public java.lang.String getDatAnu() {
        return datAnu;
    }


    /**
     * Sets the datAnu value for this FichaBasicaAnuidadesInAnuidades.
     * 
     * @param datAnu
     */
    public void setDatAnu(java.lang.String datAnu) {
        this.datAnu = datAnu;
    }


    /**
     * Gets the numAnu value for this FichaBasicaAnuidadesInAnuidades.
     * 
     * @return numAnu
     */
    public java.lang.Integer getNumAnu() {
        return numAnu;
    }


    /**
     * Sets the numAnu value for this FichaBasicaAnuidadesInAnuidades.
     * 
     * @param numAnu
     */
    public void setNumAnu(java.lang.Integer numAnu) {
        this.numAnu = numAnu;
    }


    /**
     * Gets the perAnu value for this FichaBasicaAnuidadesInAnuidades.
     * 
     * @return perAnu
     */
    public java.lang.Double getPerAnu() {
        return perAnu;
    }


    /**
     * Sets the perAnu value for this FichaBasicaAnuidadesInAnuidades.
     * 
     * @param perAnu
     */
    public void setPerAnu(java.lang.Double perAnu) {
        this.perAnu = perAnu;
    }


    /**
     * Gets the tipAnu value for this FichaBasicaAnuidadesInAnuidades.
     * 
     * @return tipAnu
     */
    public java.lang.String getTipAnu() {
        return tipAnu;
    }


    /**
     * Sets the tipAnu value for this FichaBasicaAnuidadesInAnuidades.
     * 
     * @param tipAnu
     */
    public void setTipAnu(java.lang.String tipAnu) {
        this.tipAnu = tipAnu;
    }


    /**
     * Gets the tipOpe value for this FichaBasicaAnuidadesInAnuidades.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this FichaBasicaAnuidadesInAnuidades.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }


    /**
     * Gets the valAnu value for this FichaBasicaAnuidadesInAnuidades.
     * 
     * @return valAnu
     */
    public java.lang.Double getValAnu() {
        return valAnu;
    }


    /**
     * Sets the valAnu value for this FichaBasicaAnuidadesInAnuidades.
     * 
     * @param valAnu
     */
    public void setValAnu(java.lang.Double valAnu) {
        this.valAnu = valAnu;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FichaBasicaAnuidadesInAnuidades)) return false;
        FichaBasicaAnuidadesInAnuidades other = (FichaBasicaAnuidadesInAnuidades) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.datAnu==null && other.getDatAnu()==null) || 
             (this.datAnu!=null &&
              this.datAnu.equals(other.getDatAnu()))) &&
            ((this.numAnu==null && other.getNumAnu()==null) || 
             (this.numAnu!=null &&
              this.numAnu.equals(other.getNumAnu()))) &&
            ((this.perAnu==null && other.getPerAnu()==null) || 
             (this.perAnu!=null &&
              this.perAnu.equals(other.getPerAnu()))) &&
            ((this.tipAnu==null && other.getTipAnu()==null) || 
             (this.tipAnu!=null &&
              this.tipAnu.equals(other.getTipAnu()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe()))) &&
            ((this.valAnu==null && other.getValAnu()==null) || 
             (this.valAnu!=null &&
              this.valAnu.equals(other.getValAnu())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDatAnu() != null) {
            _hashCode += getDatAnu().hashCode();
        }
        if (getNumAnu() != null) {
            _hashCode += getNumAnu().hashCode();
        }
        if (getPerAnu() != null) {
            _hashCode += getPerAnu().hashCode();
        }
        if (getTipAnu() != null) {
            _hashCode += getTipAnu().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        if (getValAnu() != null) {
            _hashCode += getValAnu().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FichaBasicaAnuidadesInAnuidades.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fichaBasicaAnuidadesInAnuidades"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAnu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAnu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numAnu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numAnu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perAnu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perAnu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipAnu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipAnu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valAnu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "valAnu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
