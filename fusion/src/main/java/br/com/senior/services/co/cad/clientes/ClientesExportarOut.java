/**
 * ClientesExportarOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package br.com.senior.services.co.cad.clientes;

public class ClientesExportarOut  implements java.io.Serializable {
    private br.com.senior.services.co.cad.clientes.ClientesExportarOutCliente[] cliente;

    private java.lang.String erroExecucao;

    private br.com.senior.services.co.cad.clientes.ClientesExportarOutErros[] erros;

    private java.lang.String finalizaramRegistros;

    private java.lang.String mensagemRetorno;

    private java.lang.Integer numeroLote;

    private java.lang.Integer tipoRetorno;

    public ClientesExportarOut() {
    }

    public ClientesExportarOut(
           br.com.senior.services.co.cad.clientes.ClientesExportarOutCliente[] cliente,
           java.lang.String erroExecucao,
           br.com.senior.services.co.cad.clientes.ClientesExportarOutErros[] erros,
           java.lang.String finalizaramRegistros,
           java.lang.String mensagemRetorno,
           java.lang.Integer numeroLote,
           java.lang.Integer tipoRetorno) {
           this.cliente = cliente;
           this.erroExecucao = erroExecucao;
           this.erros = erros;
           this.finalizaramRegistros = finalizaramRegistros;
           this.mensagemRetorno = mensagemRetorno;
           this.numeroLote = numeroLote;
           this.tipoRetorno = tipoRetorno;
    }


    /**
     * Gets the cliente value for this ClientesExportarOut.
     * 
     * @return cliente
     */
    public br.com.senior.services.co.cad.clientes.ClientesExportarOutCliente[] getCliente() {
        return cliente;
    }


    /**
     * Sets the cliente value for this ClientesExportarOut.
     * 
     * @param cliente
     */
    public void setCliente(br.com.senior.services.co.cad.clientes.ClientesExportarOutCliente[] cliente) {
        this.cliente = cliente;
    }

    public br.com.senior.services.co.cad.clientes.ClientesExportarOutCliente getCliente(int i) {
        return this.cliente[i];
    }

    public void setCliente(int i, br.com.senior.services.co.cad.clientes.ClientesExportarOutCliente _value) {
        this.cliente[i] = _value;
    }


    /**
     * Gets the erroExecucao value for this ClientesExportarOut.
     * 
     * @return erroExecucao
     */
    public java.lang.String getErroExecucao() {
        return erroExecucao;
    }


    /**
     * Sets the erroExecucao value for this ClientesExportarOut.
     * 
     * @param erroExecucao
     */
    public void setErroExecucao(java.lang.String erroExecucao) {
        this.erroExecucao = erroExecucao;
    }


    /**
     * Gets the erros value for this ClientesExportarOut.
     * 
     * @return erros
     */
    public br.com.senior.services.co.cad.clientes.ClientesExportarOutErros[] getErros() {
        return erros;
    }


    /**
     * Sets the erros value for this ClientesExportarOut.
     * 
     * @param erros
     */
    public void setErros(br.com.senior.services.co.cad.clientes.ClientesExportarOutErros[] erros) {
        this.erros = erros;
    }

    public br.com.senior.services.co.cad.clientes.ClientesExportarOutErros getErros(int i) {
        return this.erros[i];
    }

    public void setErros(int i, br.com.senior.services.co.cad.clientes.ClientesExportarOutErros _value) {
        this.erros[i] = _value;
    }


    /**
     * Gets the finalizaramRegistros value for this ClientesExportarOut.
     * 
     * @return finalizaramRegistros
     */
    public java.lang.String getFinalizaramRegistros() {
        return finalizaramRegistros;
    }


    /**
     * Sets the finalizaramRegistros value for this ClientesExportarOut.
     * 
     * @param finalizaramRegistros
     */
    public void setFinalizaramRegistros(java.lang.String finalizaramRegistros) {
        this.finalizaramRegistros = finalizaramRegistros;
    }


    /**
     * Gets the mensagemRetorno value for this ClientesExportarOut.
     * 
     * @return mensagemRetorno
     */
    public java.lang.String getMensagemRetorno() {
        return mensagemRetorno;
    }


    /**
     * Sets the mensagemRetorno value for this ClientesExportarOut.
     * 
     * @param mensagemRetorno
     */
    public void setMensagemRetorno(java.lang.String mensagemRetorno) {
        this.mensagemRetorno = mensagemRetorno;
    }


    /**
     * Gets the numeroLote value for this ClientesExportarOut.
     * 
     * @return numeroLote
     */
    public java.lang.Integer getNumeroLote() {
        return numeroLote;
    }


    /**
     * Sets the numeroLote value for this ClientesExportarOut.
     * 
     * @param numeroLote
     */
    public void setNumeroLote(java.lang.Integer numeroLote) {
        this.numeroLote = numeroLote;
    }


    /**
     * Gets the tipoRetorno value for this ClientesExportarOut.
     * 
     * @return tipoRetorno
     */
    public java.lang.Integer getTipoRetorno() {
        return tipoRetorno;
    }


    /**
     * Sets the tipoRetorno value for this ClientesExportarOut.
     * 
     * @param tipoRetorno
     */
    public void setTipoRetorno(java.lang.Integer tipoRetorno) {
        this.tipoRetorno = tipoRetorno;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientesExportarOut)) return false;
        ClientesExportarOut other = (ClientesExportarOut) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cliente==null && other.getCliente()==null) || 
             (this.cliente!=null &&
              java.util.Arrays.equals(this.cliente, other.getCliente()))) &&
            ((this.erroExecucao==null && other.getErroExecucao()==null) || 
             (this.erroExecucao!=null &&
              this.erroExecucao.equals(other.getErroExecucao()))) &&
            ((this.erros==null && other.getErros()==null) || 
             (this.erros!=null &&
              java.util.Arrays.equals(this.erros, other.getErros()))) &&
            ((this.finalizaramRegistros==null && other.getFinalizaramRegistros()==null) || 
             (this.finalizaramRegistros!=null &&
              this.finalizaramRegistros.equals(other.getFinalizaramRegistros()))) &&
            ((this.mensagemRetorno==null && other.getMensagemRetorno()==null) || 
             (this.mensagemRetorno!=null &&
              this.mensagemRetorno.equals(other.getMensagemRetorno()))) &&
            ((this.numeroLote==null && other.getNumeroLote()==null) || 
             (this.numeroLote!=null &&
              this.numeroLote.equals(other.getNumeroLote()))) &&
            ((this.tipoRetorno==null && other.getTipoRetorno()==null) || 
             (this.tipoRetorno!=null &&
              this.tipoRetorno.equals(other.getTipoRetorno())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCliente() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCliente());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCliente(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getErroExecucao() != null) {
            _hashCode += getErroExecucao().hashCode();
        }
        if (getErros() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getErros());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getErros(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getFinalizaramRegistros() != null) {
            _hashCode += getFinalizaramRegistros().hashCode();
        }
        if (getMensagemRetorno() != null) {
            _hashCode += getMensagemRetorno().hashCode();
        }
        if (getNumeroLote() != null) {
            _hashCode += getNumeroLote().hashCode();
        }
        if (getTipoRetorno() != null) {
            _hashCode += getTipoRetorno().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientesExportarOut.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesExportarOut"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cliente");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cliente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesExportarOutCliente"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("erroExecucao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "erroExecucao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("erros");
        elemField.setXmlName(new javax.xml.namespace.QName("", "erros"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesExportarOutErros"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("finalizaramRegistros");
        elemField.setXmlName(new javax.xml.namespace.QName("", "finalizaramRegistros"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mensagemRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mensagemRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroLote");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numeroLote"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipoRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
