/**
 * G5SeniorServices.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.gtc;

public interface G5SeniorServices extends javax.xml.rpc.Service
{
	public java.lang.String getsapiens_Synccom_senior_g5_co_mfi_cpa_titulosPortAddress();

	public Sapiens_Synccom_senior_g5_co_mfi_cpa_titulos getsapiens_Synccom_senior_g5_co_mfi_cpa_titulosPort() throws javax.xml.rpc.ServiceException;

	public Sapiens_Synccom_senior_g5_co_mfi_cpa_titulos getsapiens_Synccom_senior_g5_co_mfi_cpa_titulosPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
