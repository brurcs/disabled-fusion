/**
 * FichaBasicaContaBancariaInContasBancarias.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.ficha.basica;

public class FichaBasicaContaBancariaInContasBancarias  implements java.io.Serializable {
    private java.lang.Integer codAge;

    private java.lang.Integer codBan;

    private java.lang.Double conBan;

    private java.lang.String digBan;

    private java.lang.Double perCon;

    private java.lang.Integer tipBan;

    private java.lang.String tipOpe;

    public FichaBasicaContaBancariaInContasBancarias() {
    }

    public FichaBasicaContaBancariaInContasBancarias(
           java.lang.Integer codAge,
           java.lang.Integer codBan,
           java.lang.Double conBan,
           java.lang.String digBan,
           java.lang.Double perCon,
           java.lang.Integer tipBan,
           java.lang.String tipOpe) {
           this.codAge = codAge;
           this.codBan = codBan;
           this.conBan = conBan;
           this.digBan = digBan;
           this.perCon = perCon;
           this.tipBan = tipBan;
           this.tipOpe = tipOpe;
    }


    /**
     * Gets the codAge value for this FichaBasicaContaBancariaInContasBancarias.
     * 
     * @return codAge
     */
    public java.lang.Integer getCodAge() {
        return codAge;
    }


    /**
     * Sets the codAge value for this FichaBasicaContaBancariaInContasBancarias.
     * 
     * @param codAge
     */
    public void setCodAge(java.lang.Integer codAge) {
        this.codAge = codAge;
    }


    /**
     * Gets the codBan value for this FichaBasicaContaBancariaInContasBancarias.
     * 
     * @return codBan
     */
    public java.lang.Integer getCodBan() {
        return codBan;
    }


    /**
     * Sets the codBan value for this FichaBasicaContaBancariaInContasBancarias.
     * 
     * @param codBan
     */
    public void setCodBan(java.lang.Integer codBan) {
        this.codBan = codBan;
    }


    /**
     * Gets the conBan value for this FichaBasicaContaBancariaInContasBancarias.
     * 
     * @return conBan
     */
    public java.lang.Double getConBan() {
        return conBan;
    }


    /**
     * Sets the conBan value for this FichaBasicaContaBancariaInContasBancarias.
     * 
     * @param conBan
     */
    public void setConBan(java.lang.Double conBan) {
        this.conBan = conBan;
    }


    /**
     * Gets the digBan value for this FichaBasicaContaBancariaInContasBancarias.
     * 
     * @return digBan
     */
    public java.lang.String getDigBan() {
        return digBan;
    }


    /**
     * Sets the digBan value for this FichaBasicaContaBancariaInContasBancarias.
     * 
     * @param digBan
     */
    public void setDigBan(java.lang.String digBan) {
        this.digBan = digBan;
    }


    /**
     * Gets the perCon value for this FichaBasicaContaBancariaInContasBancarias.
     * 
     * @return perCon
     */
    public java.lang.Double getPerCon() {
        return perCon;
    }


    /**
     * Sets the perCon value for this FichaBasicaContaBancariaInContasBancarias.
     * 
     * @param perCon
     */
    public void setPerCon(java.lang.Double perCon) {
        this.perCon = perCon;
    }


    /**
     * Gets the tipBan value for this FichaBasicaContaBancariaInContasBancarias.
     * 
     * @return tipBan
     */
    public java.lang.Integer getTipBan() {
        return tipBan;
    }


    /**
     * Sets the tipBan value for this FichaBasicaContaBancariaInContasBancarias.
     * 
     * @param tipBan
     */
    public void setTipBan(java.lang.Integer tipBan) {
        this.tipBan = tipBan;
    }


    /**
     * Gets the tipOpe value for this FichaBasicaContaBancariaInContasBancarias.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this FichaBasicaContaBancariaInContasBancarias.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FichaBasicaContaBancariaInContasBancarias)) return false;
        FichaBasicaContaBancariaInContasBancarias other = (FichaBasicaContaBancariaInContasBancarias) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codAge==null && other.getCodAge()==null) || 
             (this.codAge!=null &&
              this.codAge.equals(other.getCodAge()))) &&
            ((this.codBan==null && other.getCodBan()==null) || 
             (this.codBan!=null &&
              this.codBan.equals(other.getCodBan()))) &&
            ((this.conBan==null && other.getConBan()==null) || 
             (this.conBan!=null &&
              this.conBan.equals(other.getConBan()))) &&
            ((this.digBan==null && other.getDigBan()==null) || 
             (this.digBan!=null &&
              this.digBan.equals(other.getDigBan()))) &&
            ((this.perCon==null && other.getPerCon()==null) || 
             (this.perCon!=null &&
              this.perCon.equals(other.getPerCon()))) &&
            ((this.tipBan==null && other.getTipBan()==null) || 
             (this.tipBan!=null &&
              this.tipBan.equals(other.getTipBan()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodAge() != null) {
            _hashCode += getCodAge().hashCode();
        }
        if (getCodBan() != null) {
            _hashCode += getCodBan().hashCode();
        }
        if (getConBan() != null) {
            _hashCode += getConBan().hashCode();
        }
        if (getDigBan() != null) {
            _hashCode += getDigBan().hashCode();
        }
        if (getPerCon() != null) {
            _hashCode += getPerCon().hashCode();
        }
        if (getTipBan() != null) {
            _hashCode += getTipBan().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FichaBasicaContaBancariaInContasBancarias.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fichaBasicaContaBancariaInContasBancarias"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codAge");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codAge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "conBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("digBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "digBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perCon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perCon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
