/**
 * ClientesConsultarFiscalIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package br.com.senior.services.co.cad.clientes;

public class ClientesConsultarFiscalIn  implements java.io.Serializable {
    private br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalInCgcCpf[] cgcCpf;

    private br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalInCodCli[] codCli;

    private java.lang.Integer codEmp;

    private java.lang.Integer codFil;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String identificadorSistema;

    private java.lang.Integer indicePagina;

    private java.lang.Integer limitePagina;

    private br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalInSigUfs[] sigUfs;

    private java.lang.String sitCli;

    private java.lang.String tipCli;

    private br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalInTipMer[] tipMer;

    public ClientesConsultarFiscalIn() {
    }

    public ClientesConsultarFiscalIn(
           br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalInCgcCpf[] cgcCpf,
           br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalInCodCli[] codCli,
           java.lang.Integer codEmp,
           java.lang.Integer codFil,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String identificadorSistema,
           java.lang.Integer indicePagina,
           java.lang.Integer limitePagina,
           br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalInSigUfs[] sigUfs,
           java.lang.String sitCli,
           java.lang.String tipCli,
           br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalInTipMer[] tipMer) {
           this.cgcCpf = cgcCpf;
           this.codCli = codCli;
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.identificadorSistema = identificadorSistema;
           this.indicePagina = indicePagina;
           this.limitePagina = limitePagina;
           this.sigUfs = sigUfs;
           this.sitCli = sitCli;
           this.tipCli = tipCli;
           this.tipMer = tipMer;
    }


    /**
     * Gets the cgcCpf value for this ClientesConsultarFiscalIn.
     * 
     * @return cgcCpf
     */
    public br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalInCgcCpf[] getCgcCpf() {
        return cgcCpf;
    }


    /**
     * Sets the cgcCpf value for this ClientesConsultarFiscalIn.
     * 
     * @param cgcCpf
     */
    public void setCgcCpf(br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalInCgcCpf[] cgcCpf) {
        this.cgcCpf = cgcCpf;
    }

    public br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalInCgcCpf getCgcCpf(int i) {
        return this.cgcCpf[i];
    }

    public void setCgcCpf(int i, br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalInCgcCpf _value) {
        this.cgcCpf[i] = _value;
    }


    /**
     * Gets the codCli value for this ClientesConsultarFiscalIn.
     * 
     * @return codCli
     */
    public br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalInCodCli[] getCodCli() {
        return codCli;
    }


    /**
     * Sets the codCli value for this ClientesConsultarFiscalIn.
     * 
     * @param codCli
     */
    public void setCodCli(br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalInCodCli[] codCli) {
        this.codCli = codCli;
    }

    public br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalInCodCli getCodCli(int i) {
        return this.codCli[i];
    }

    public void setCodCli(int i, br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalInCodCli _value) {
        this.codCli[i] = _value;
    }


    /**
     * Gets the codEmp value for this ClientesConsultarFiscalIn.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this ClientesConsultarFiscalIn.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this ClientesConsultarFiscalIn.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this ClientesConsultarFiscalIn.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the flowInstanceID value for this ClientesConsultarFiscalIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this ClientesConsultarFiscalIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this ClientesConsultarFiscalIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this ClientesConsultarFiscalIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the identificadorSistema value for this ClientesConsultarFiscalIn.
     * 
     * @return identificadorSistema
     */
    public java.lang.String getIdentificadorSistema() {
        return identificadorSistema;
    }


    /**
     * Sets the identificadorSistema value for this ClientesConsultarFiscalIn.
     * 
     * @param identificadorSistema
     */
    public void setIdentificadorSistema(java.lang.String identificadorSistema) {
        this.identificadorSistema = identificadorSistema;
    }


    /**
     * Gets the indicePagina value for this ClientesConsultarFiscalIn.
     * 
     * @return indicePagina
     */
    public java.lang.Integer getIndicePagina() {
        return indicePagina;
    }


    /**
     * Sets the indicePagina value for this ClientesConsultarFiscalIn.
     * 
     * @param indicePagina
     */
    public void setIndicePagina(java.lang.Integer indicePagina) {
        this.indicePagina = indicePagina;
    }


    /**
     * Gets the limitePagina value for this ClientesConsultarFiscalIn.
     * 
     * @return limitePagina
     */
    public java.lang.Integer getLimitePagina() {
        return limitePagina;
    }


    /**
     * Sets the limitePagina value for this ClientesConsultarFiscalIn.
     * 
     * @param limitePagina
     */
    public void setLimitePagina(java.lang.Integer limitePagina) {
        this.limitePagina = limitePagina;
    }


    /**
     * Gets the sigUfs value for this ClientesConsultarFiscalIn.
     * 
     * @return sigUfs
     */
    public br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalInSigUfs[] getSigUfs() {
        return sigUfs;
    }


    /**
     * Sets the sigUfs value for this ClientesConsultarFiscalIn.
     * 
     * @param sigUfs
     */
    public void setSigUfs(br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalInSigUfs[] sigUfs) {
        this.sigUfs = sigUfs;
    }

    public br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalInSigUfs getSigUfs(int i) {
        return this.sigUfs[i];
    }

    public void setSigUfs(int i, br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalInSigUfs _value) {
        this.sigUfs[i] = _value;
    }


    /**
     * Gets the sitCli value for this ClientesConsultarFiscalIn.
     * 
     * @return sitCli
     */
    public java.lang.String getSitCli() {
        return sitCli;
    }


    /**
     * Sets the sitCli value for this ClientesConsultarFiscalIn.
     * 
     * @param sitCli
     */
    public void setSitCli(java.lang.String sitCli) {
        this.sitCli = sitCli;
    }


    /**
     * Gets the tipCli value for this ClientesConsultarFiscalIn.
     * 
     * @return tipCli
     */
    public java.lang.String getTipCli() {
        return tipCli;
    }


    /**
     * Sets the tipCli value for this ClientesConsultarFiscalIn.
     * 
     * @param tipCli
     */
    public void setTipCli(java.lang.String tipCli) {
        this.tipCli = tipCli;
    }


    /**
     * Gets the tipMer value for this ClientesConsultarFiscalIn.
     * 
     * @return tipMer
     */
    public br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalInTipMer[] getTipMer() {
        return tipMer;
    }


    /**
     * Sets the tipMer value for this ClientesConsultarFiscalIn.
     * 
     * @param tipMer
     */
    public void setTipMer(br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalInTipMer[] tipMer) {
        this.tipMer = tipMer;
    }

    public br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalInTipMer getTipMer(int i) {
        return this.tipMer[i];
    }

    public void setTipMer(int i, br.com.senior.services.co.cad.clientes.ClientesConsultarFiscalInTipMer _value) {
        this.tipMer[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientesConsultarFiscalIn)) return false;
        ClientesConsultarFiscalIn other = (ClientesConsultarFiscalIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cgcCpf==null && other.getCgcCpf()==null) || 
             (this.cgcCpf!=null &&
              java.util.Arrays.equals(this.cgcCpf, other.getCgcCpf()))) &&
            ((this.codCli==null && other.getCodCli()==null) || 
             (this.codCli!=null &&
              java.util.Arrays.equals(this.codCli, other.getCodCli()))) &&
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.identificadorSistema==null && other.getIdentificadorSistema()==null) || 
             (this.identificadorSistema!=null &&
              this.identificadorSistema.equals(other.getIdentificadorSistema()))) &&
            ((this.indicePagina==null && other.getIndicePagina()==null) || 
             (this.indicePagina!=null &&
              this.indicePagina.equals(other.getIndicePagina()))) &&
            ((this.limitePagina==null && other.getLimitePagina()==null) || 
             (this.limitePagina!=null &&
              this.limitePagina.equals(other.getLimitePagina()))) &&
            ((this.sigUfs==null && other.getSigUfs()==null) || 
             (this.sigUfs!=null &&
              java.util.Arrays.equals(this.sigUfs, other.getSigUfs()))) &&
            ((this.sitCli==null && other.getSitCli()==null) || 
             (this.sitCli!=null &&
              this.sitCli.equals(other.getSitCli()))) &&
            ((this.tipCli==null && other.getTipCli()==null) || 
             (this.tipCli!=null &&
              this.tipCli.equals(other.getTipCli()))) &&
            ((this.tipMer==null && other.getTipMer()==null) || 
             (this.tipMer!=null &&
              java.util.Arrays.equals(this.tipMer, other.getTipMer())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCgcCpf() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCgcCpf());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCgcCpf(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCodCli() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCodCli());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCodCli(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getIdentificadorSistema() != null) {
            _hashCode += getIdentificadorSistema().hashCode();
        }
        if (getIndicePagina() != null) {
            _hashCode += getIndicePagina().hashCode();
        }
        if (getLimitePagina() != null) {
            _hashCode += getLimitePagina().hashCode();
        }
        if (getSigUfs() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSigUfs());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSigUfs(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSitCli() != null) {
            _hashCode += getSitCli().hashCode();
        }
        if (getTipCli() != null) {
            _hashCode += getTipCli().hashCode();
        }
        if (getTipMer() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTipMer());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTipMer(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientesConsultarFiscalIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesConsultarFiscalIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cgcCpf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cgcCpf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesConsultarFiscalInCgcCpf"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesConsultarFiscalInCodCli"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identificadorSistema");
        elemField.setXmlName(new javax.xml.namespace.QName("", "identificadorSistema"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indicePagina");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indicePagina"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limitePagina");
        elemField.setXmlName(new javax.xml.namespace.QName("", "limitePagina"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sigUfs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sigUfs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesConsultarFiscalInSigUfs"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipMer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipMer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesConsultarFiscalInTipMer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
