/**
 * TitulosGerarBaixaPorLoteCPIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.gtc;

public class TitulosGerarBaixaPorLoteCPIn  implements java.io.Serializable {
    private java.lang.Integer codEmp;

    private java.lang.Integer codFil;

    private java.lang.String datBai;

    private java.lang.String datCxb;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private TitulosGerarBaixaPorLoteCPInGridTitulosBaixar[] gridTitulosBaixar;

    private java.lang.String numCco;

    private java.lang.String tnsBai;

    private java.lang.String tnsCxb;

    public TitulosGerarBaixaPorLoteCPIn() {
    }

    public TitulosGerarBaixaPorLoteCPIn(
           java.lang.Integer codEmp,
           java.lang.Integer codFil,
           java.lang.String datBai,
           java.lang.String datCxb,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           TitulosGerarBaixaPorLoteCPInGridTitulosBaixar[] gridTitulosBaixar,
           java.lang.String numCco,
           java.lang.String tnsBai,
           java.lang.String tnsCxb) {
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.datBai = datBai;
           this.datCxb = datCxb;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.gridTitulosBaixar = gridTitulosBaixar;
           this.numCco = numCco;
           this.tnsBai = tnsBai;
           this.tnsCxb = tnsCxb;
    }


    /**
     * Gets the codEmp value for this TitulosGerarBaixaPorLoteCPIn.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this TitulosGerarBaixaPorLoteCPIn.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this TitulosGerarBaixaPorLoteCPIn.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this TitulosGerarBaixaPorLoteCPIn.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the datBai value for this TitulosGerarBaixaPorLoteCPIn.
     * 
     * @return datBai
     */
    public java.lang.String getDatBai() {
        return datBai;
    }


    /**
     * Sets the datBai value for this TitulosGerarBaixaPorLoteCPIn.
     * 
     * @param datBai
     */
    public void setDatBai(java.lang.String datBai) {
        this.datBai = datBai;
    }


    /**
     * Gets the datCxb value for this TitulosGerarBaixaPorLoteCPIn.
     * 
     * @return datCxb
     */
    public java.lang.String getDatCxb() {
        return datCxb;
    }


    /**
     * Sets the datCxb value for this TitulosGerarBaixaPorLoteCPIn.
     * 
     * @param datCxb
     */
    public void setDatCxb(java.lang.String datCxb) {
        this.datCxb = datCxb;
    }


    /**
     * Gets the flowInstanceID value for this TitulosGerarBaixaPorLoteCPIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this TitulosGerarBaixaPorLoteCPIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this TitulosGerarBaixaPorLoteCPIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this TitulosGerarBaixaPorLoteCPIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the gridTitulosBaixar value for this TitulosGerarBaixaPorLoteCPIn.
     * 
     * @return gridTitulosBaixar
     */
    public TitulosGerarBaixaPorLoteCPInGridTitulosBaixar[] getGridTitulosBaixar() {
        return gridTitulosBaixar;
    }


    /**
     * Sets the gridTitulosBaixar value for this TitulosGerarBaixaPorLoteCPIn.
     * 
     * @param gridTitulosBaixar
     */
    public void setGridTitulosBaixar(TitulosGerarBaixaPorLoteCPInGridTitulosBaixar[] gridTitulosBaixar) {
        this.gridTitulosBaixar = gridTitulosBaixar;
    }

    public TitulosGerarBaixaPorLoteCPInGridTitulosBaixar getGridTitulosBaixar(int i) {
        return this.gridTitulosBaixar[i];
    }

    public void setGridTitulosBaixar(int i, TitulosGerarBaixaPorLoteCPInGridTitulosBaixar _value) {
        this.gridTitulosBaixar[i] = _value;
    }


    /**
     * Gets the numCco value for this TitulosGerarBaixaPorLoteCPIn.
     * 
     * @return numCco
     */
    public java.lang.String getNumCco() {
        return numCco;
    }


    /**
     * Sets the numCco value for this TitulosGerarBaixaPorLoteCPIn.
     * 
     * @param numCco
     */
    public void setNumCco(java.lang.String numCco) {
        this.numCco = numCco;
    }


    /**
     * Gets the tnsBai value for this TitulosGerarBaixaPorLoteCPIn.
     * 
     * @return tnsBai
     */
    public java.lang.String getTnsBai() {
        return tnsBai;
    }


    /**
     * Sets the tnsBai value for this TitulosGerarBaixaPorLoteCPIn.
     * 
     * @param tnsBai
     */
    public void setTnsBai(java.lang.String tnsBai) {
        this.tnsBai = tnsBai;
    }


    /**
     * Gets the tnsCxb value for this TitulosGerarBaixaPorLoteCPIn.
     * 
     * @return tnsCxb
     */
    public java.lang.String getTnsCxb() {
        return tnsCxb;
    }


    /**
     * Sets the tnsCxb value for this TitulosGerarBaixaPorLoteCPIn.
     * 
     * @param tnsCxb
     */
    public void setTnsCxb(java.lang.String tnsCxb) {
        this.tnsCxb = tnsCxb;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosGerarBaixaPorLoteCPIn)) return false;
        TitulosGerarBaixaPorLoteCPIn other = (TitulosGerarBaixaPorLoteCPIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.datBai==null && other.getDatBai()==null) || 
             (this.datBai!=null &&
              this.datBai.equals(other.getDatBai()))) &&
            ((this.datCxb==null && other.getDatCxb()==null) || 
             (this.datCxb!=null &&
              this.datCxb.equals(other.getDatCxb()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.gridTitulosBaixar==null && other.getGridTitulosBaixar()==null) || 
             (this.gridTitulosBaixar!=null &&
              java.util.Arrays.equals(this.gridTitulosBaixar, other.getGridTitulosBaixar()))) &&
            ((this.numCco==null && other.getNumCco()==null) || 
             (this.numCco!=null &&
              this.numCco.equals(other.getNumCco()))) &&
            ((this.tnsBai==null && other.getTnsBai()==null) || 
             (this.tnsBai!=null &&
              this.tnsBai.equals(other.getTnsBai()))) &&
            ((this.tnsCxb==null && other.getTnsCxb()==null) || 
             (this.tnsCxb!=null &&
              this.tnsCxb.equals(other.getTnsCxb())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getDatBai() != null) {
            _hashCode += getDatBai().hashCode();
        }
        if (getDatCxb() != null) {
            _hashCode += getDatCxb().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getGridTitulosBaixar() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGridTitulosBaixar());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGridTitulosBaixar(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getNumCco() != null) {
            _hashCode += getNumCco().hashCode();
        }
        if (getTnsBai() != null) {
            _hashCode += getTnsBai().hashCode();
        }
        if (getTnsCxb() != null) {
            _hashCode += getTnsCxb().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosGerarBaixaPorLoteCPIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosGerarBaixaPorLoteCPIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datBai");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datBai"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datCxb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datCxb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gridTitulosBaixar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "gridTitulosBaixar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosGerarBaixaPorLoteCPInGridTitulosBaixar"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCco");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsBai");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsBai"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsCxb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsCxb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
