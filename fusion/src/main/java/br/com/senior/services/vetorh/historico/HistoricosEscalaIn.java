/**
 * HistoricosEscalaIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.historico;

public class HistoricosEscalaIn  implements java.io.Serializable {
    private java.lang.Integer codCat;

    private java.lang.Integer codEqp;

    private java.lang.Integer codEsc;

    private java.lang.Integer codTma;

    private java.lang.String datAlt;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.Integer numCad;

    private java.lang.Integer numEmp;

    private java.lang.Integer tipCol;

    private java.lang.String tipOpe;

    private java.lang.Integer turInt;

    public HistoricosEscalaIn() {
    }

    public HistoricosEscalaIn(
           java.lang.Integer codCat,
           java.lang.Integer codEqp,
           java.lang.Integer codEsc,
           java.lang.Integer codTma,
           java.lang.String datAlt,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.Integer numCad,
           java.lang.Integer numEmp,
           java.lang.Integer tipCol,
           java.lang.String tipOpe,
           java.lang.Integer turInt) {
           this.codCat = codCat;
           this.codEqp = codEqp;
           this.codEsc = codEsc;
           this.codTma = codTma;
           this.datAlt = datAlt;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.numCad = numCad;
           this.numEmp = numEmp;
           this.tipCol = tipCol;
           this.tipOpe = tipOpe;
           this.turInt = turInt;
    }


    /**
     * Gets the codCat value for this HistoricosEscalaIn.
     * 
     * @return codCat
     */
    public java.lang.Integer getCodCat() {
        return codCat;
    }


    /**
     * Sets the codCat value for this HistoricosEscalaIn.
     * 
     * @param codCat
     */
    public void setCodCat(java.lang.Integer codCat) {
        this.codCat = codCat;
    }


    /**
     * Gets the codEqp value for this HistoricosEscalaIn.
     * 
     * @return codEqp
     */
    public java.lang.Integer getCodEqp() {
        return codEqp;
    }


    /**
     * Sets the codEqp value for this HistoricosEscalaIn.
     * 
     * @param codEqp
     */
    public void setCodEqp(java.lang.Integer codEqp) {
        this.codEqp = codEqp;
    }


    /**
     * Gets the codEsc value for this HistoricosEscalaIn.
     * 
     * @return codEsc
     */
    public java.lang.Integer getCodEsc() {
        return codEsc;
    }


    /**
     * Sets the codEsc value for this HistoricosEscalaIn.
     * 
     * @param codEsc
     */
    public void setCodEsc(java.lang.Integer codEsc) {
        this.codEsc = codEsc;
    }


    /**
     * Gets the codTma value for this HistoricosEscalaIn.
     * 
     * @return codTma
     */
    public java.lang.Integer getCodTma() {
        return codTma;
    }


    /**
     * Sets the codTma value for this HistoricosEscalaIn.
     * 
     * @param codTma
     */
    public void setCodTma(java.lang.Integer codTma) {
        this.codTma = codTma;
    }


    /**
     * Gets the datAlt value for this HistoricosEscalaIn.
     * 
     * @return datAlt
     */
    public java.lang.String getDatAlt() {
        return datAlt;
    }


    /**
     * Sets the datAlt value for this HistoricosEscalaIn.
     * 
     * @param datAlt
     */
    public void setDatAlt(java.lang.String datAlt) {
        this.datAlt = datAlt;
    }


    /**
     * Gets the flowInstanceID value for this HistoricosEscalaIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this HistoricosEscalaIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this HistoricosEscalaIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this HistoricosEscalaIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the numCad value for this HistoricosEscalaIn.
     * 
     * @return numCad
     */
    public java.lang.Integer getNumCad() {
        return numCad;
    }


    /**
     * Sets the numCad value for this HistoricosEscalaIn.
     * 
     * @param numCad
     */
    public void setNumCad(java.lang.Integer numCad) {
        this.numCad = numCad;
    }


    /**
     * Gets the numEmp value for this HistoricosEscalaIn.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this HistoricosEscalaIn.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the tipCol value for this HistoricosEscalaIn.
     * 
     * @return tipCol
     */
    public java.lang.Integer getTipCol() {
        return tipCol;
    }


    /**
     * Sets the tipCol value for this HistoricosEscalaIn.
     * 
     * @param tipCol
     */
    public void setTipCol(java.lang.Integer tipCol) {
        this.tipCol = tipCol;
    }


    /**
     * Gets the tipOpe value for this HistoricosEscalaIn.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this HistoricosEscalaIn.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }


    /**
     * Gets the turInt value for this HistoricosEscalaIn.
     * 
     * @return turInt
     */
    public java.lang.Integer getTurInt() {
        return turInt;
    }


    /**
     * Sets the turInt value for this HistoricosEscalaIn.
     * 
     * @param turInt
     */
    public void setTurInt(java.lang.Integer turInt) {
        this.turInt = turInt;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HistoricosEscalaIn)) return false;
        HistoricosEscalaIn other = (HistoricosEscalaIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codCat==null && other.getCodCat()==null) || 
             (this.codCat!=null &&
              this.codCat.equals(other.getCodCat()))) &&
            ((this.codEqp==null && other.getCodEqp()==null) || 
             (this.codEqp!=null &&
              this.codEqp.equals(other.getCodEqp()))) &&
            ((this.codEsc==null && other.getCodEsc()==null) || 
             (this.codEsc!=null &&
              this.codEsc.equals(other.getCodEsc()))) &&
            ((this.codTma==null && other.getCodTma()==null) || 
             (this.codTma!=null &&
              this.codTma.equals(other.getCodTma()))) &&
            ((this.datAlt==null && other.getDatAlt()==null) || 
             (this.datAlt!=null &&
              this.datAlt.equals(other.getDatAlt()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.numCad==null && other.getNumCad()==null) || 
             (this.numCad!=null &&
              this.numCad.equals(other.getNumCad()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.tipCol==null && other.getTipCol()==null) || 
             (this.tipCol!=null &&
              this.tipCol.equals(other.getTipCol()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe()))) &&
            ((this.turInt==null && other.getTurInt()==null) || 
             (this.turInt!=null &&
              this.turInt.equals(other.getTurInt())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodCat() != null) {
            _hashCode += getCodCat().hashCode();
        }
        if (getCodEqp() != null) {
            _hashCode += getCodEqp().hashCode();
        }
        if (getCodEsc() != null) {
            _hashCode += getCodEsc().hashCode();
        }
        if (getCodTma() != null) {
            _hashCode += getCodTma().hashCode();
        }
        if (getDatAlt() != null) {
            _hashCode += getDatAlt().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getNumCad() != null) {
            _hashCode += getNumCad().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getTipCol() != null) {
            _hashCode += getTipCol().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        if (getTurInt() != null) {
            _hashCode += getTurInt().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HistoricosEscalaIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "historicosEscalaIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEqp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEqp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTma");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTma"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("turInt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "turInt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
