/**
 * HistoricosEstabilidadeIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.historico;

public class HistoricosEstabilidadeIn  implements java.io.Serializable {
    private java.lang.Integer codEtb;

    private java.lang.String fimEtb;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String iniEtb;

    private java.lang.Integer numCad;

    private java.lang.Integer numEmp;

    private java.lang.String obsFim;

    private java.lang.String obsIni;

    private java.lang.Integer tipCol;

    private java.lang.String tipOpe;

    public HistoricosEstabilidadeIn() {
    }

    public HistoricosEstabilidadeIn(
           java.lang.Integer codEtb,
           java.lang.String fimEtb,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String iniEtb,
           java.lang.Integer numCad,
           java.lang.Integer numEmp,
           java.lang.String obsFim,
           java.lang.String obsIni,
           java.lang.Integer tipCol,
           java.lang.String tipOpe) {
           this.codEtb = codEtb;
           this.fimEtb = fimEtb;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.iniEtb = iniEtb;
           this.numCad = numCad;
           this.numEmp = numEmp;
           this.obsFim = obsFim;
           this.obsIni = obsIni;
           this.tipCol = tipCol;
           this.tipOpe = tipOpe;
    }


    /**
     * Gets the codEtb value for this HistoricosEstabilidadeIn.
     * 
     * @return codEtb
     */
    public java.lang.Integer getCodEtb() {
        return codEtb;
    }


    /**
     * Sets the codEtb value for this HistoricosEstabilidadeIn.
     * 
     * @param codEtb
     */
    public void setCodEtb(java.lang.Integer codEtb) {
        this.codEtb = codEtb;
    }


    /**
     * Gets the fimEtb value for this HistoricosEstabilidadeIn.
     * 
     * @return fimEtb
     */
    public java.lang.String getFimEtb() {
        return fimEtb;
    }


    /**
     * Sets the fimEtb value for this HistoricosEstabilidadeIn.
     * 
     * @param fimEtb
     */
    public void setFimEtb(java.lang.String fimEtb) {
        this.fimEtb = fimEtb;
    }


    /**
     * Gets the flowInstanceID value for this HistoricosEstabilidadeIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this HistoricosEstabilidadeIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this HistoricosEstabilidadeIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this HistoricosEstabilidadeIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the iniEtb value for this HistoricosEstabilidadeIn.
     * 
     * @return iniEtb
     */
    public java.lang.String getIniEtb() {
        return iniEtb;
    }


    /**
     * Sets the iniEtb value for this HistoricosEstabilidadeIn.
     * 
     * @param iniEtb
     */
    public void setIniEtb(java.lang.String iniEtb) {
        this.iniEtb = iniEtb;
    }


    /**
     * Gets the numCad value for this HistoricosEstabilidadeIn.
     * 
     * @return numCad
     */
    public java.lang.Integer getNumCad() {
        return numCad;
    }


    /**
     * Sets the numCad value for this HistoricosEstabilidadeIn.
     * 
     * @param numCad
     */
    public void setNumCad(java.lang.Integer numCad) {
        this.numCad = numCad;
    }


    /**
     * Gets the numEmp value for this HistoricosEstabilidadeIn.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this HistoricosEstabilidadeIn.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the obsFim value for this HistoricosEstabilidadeIn.
     * 
     * @return obsFim
     */
    public java.lang.String getObsFim() {
        return obsFim;
    }


    /**
     * Sets the obsFim value for this HistoricosEstabilidadeIn.
     * 
     * @param obsFim
     */
    public void setObsFim(java.lang.String obsFim) {
        this.obsFim = obsFim;
    }


    /**
     * Gets the obsIni value for this HistoricosEstabilidadeIn.
     * 
     * @return obsIni
     */
    public java.lang.String getObsIni() {
        return obsIni;
    }


    /**
     * Sets the obsIni value for this HistoricosEstabilidadeIn.
     * 
     * @param obsIni
     */
    public void setObsIni(java.lang.String obsIni) {
        this.obsIni = obsIni;
    }


    /**
     * Gets the tipCol value for this HistoricosEstabilidadeIn.
     * 
     * @return tipCol
     */
    public java.lang.Integer getTipCol() {
        return tipCol;
    }


    /**
     * Sets the tipCol value for this HistoricosEstabilidadeIn.
     * 
     * @param tipCol
     */
    public void setTipCol(java.lang.Integer tipCol) {
        this.tipCol = tipCol;
    }


    /**
     * Gets the tipOpe value for this HistoricosEstabilidadeIn.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this HistoricosEstabilidadeIn.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HistoricosEstabilidadeIn)) return false;
        HistoricosEstabilidadeIn other = (HistoricosEstabilidadeIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codEtb==null && other.getCodEtb()==null) || 
             (this.codEtb!=null &&
              this.codEtb.equals(other.getCodEtb()))) &&
            ((this.fimEtb==null && other.getFimEtb()==null) || 
             (this.fimEtb!=null &&
              this.fimEtb.equals(other.getFimEtb()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.iniEtb==null && other.getIniEtb()==null) || 
             (this.iniEtb!=null &&
              this.iniEtb.equals(other.getIniEtb()))) &&
            ((this.numCad==null && other.getNumCad()==null) || 
             (this.numCad!=null &&
              this.numCad.equals(other.getNumCad()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.obsFim==null && other.getObsFim()==null) || 
             (this.obsFim!=null &&
              this.obsFim.equals(other.getObsFim()))) &&
            ((this.obsIni==null && other.getObsIni()==null) || 
             (this.obsIni!=null &&
              this.obsIni.equals(other.getObsIni()))) &&
            ((this.tipCol==null && other.getTipCol()==null) || 
             (this.tipCol!=null &&
              this.tipCol.equals(other.getTipCol()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodEtb() != null) {
            _hashCode += getCodEtb().hashCode();
        }
        if (getFimEtb() != null) {
            _hashCode += getFimEtb().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getIniEtb() != null) {
            _hashCode += getIniEtb().hashCode();
        }
        if (getNumCad() != null) {
            _hashCode += getNumCad().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getObsFim() != null) {
            _hashCode += getObsFim().hashCode();
        }
        if (getObsIni() != null) {
            _hashCode += getObsIni().hashCode();
        }
        if (getTipCol() != null) {
            _hashCode += getTipCol().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HistoricosEstabilidadeIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "historicosEstabilidadeIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEtb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEtb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fimEtb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fimEtb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("iniEtb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "iniEtb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsFim");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsFim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsIni");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsIni"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
