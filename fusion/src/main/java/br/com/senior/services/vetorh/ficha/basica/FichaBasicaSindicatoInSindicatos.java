/**
 * FichaBasicaSindicatoInSindicatos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.ficha.basica;

public class FichaBasicaSindicatoInSindicatos  implements java.io.Serializable {
    private java.lang.Integer codSin;

    private java.lang.String cusSin;

    private java.lang.String datFim;

    private java.lang.String datIni;

    private java.lang.String pagSin;

    private java.lang.String revSin;

    private java.lang.String socSin;

    private java.lang.String tasSin;

    private java.lang.String tipOpe;

    public FichaBasicaSindicatoInSindicatos() {
    }

    public FichaBasicaSindicatoInSindicatos(
           java.lang.Integer codSin,
           java.lang.String cusSin,
           java.lang.String datFim,
           java.lang.String datIni,
           java.lang.String pagSin,
           java.lang.String revSin,
           java.lang.String socSin,
           java.lang.String tasSin,
           java.lang.String tipOpe) {
           this.codSin = codSin;
           this.cusSin = cusSin;
           this.datFim = datFim;
           this.datIni = datIni;
           this.pagSin = pagSin;
           this.revSin = revSin;
           this.socSin = socSin;
           this.tasSin = tasSin;
           this.tipOpe = tipOpe;
    }


    /**
     * Gets the codSin value for this FichaBasicaSindicatoInSindicatos.
     * 
     * @return codSin
     */
    public java.lang.Integer getCodSin() {
        return codSin;
    }


    /**
     * Sets the codSin value for this FichaBasicaSindicatoInSindicatos.
     * 
     * @param codSin
     */
    public void setCodSin(java.lang.Integer codSin) {
        this.codSin = codSin;
    }


    /**
     * Gets the cusSin value for this FichaBasicaSindicatoInSindicatos.
     * 
     * @return cusSin
     */
    public java.lang.String getCusSin() {
        return cusSin;
    }


    /**
     * Sets the cusSin value for this FichaBasicaSindicatoInSindicatos.
     * 
     * @param cusSin
     */
    public void setCusSin(java.lang.String cusSin) {
        this.cusSin = cusSin;
    }


    /**
     * Gets the datFim value for this FichaBasicaSindicatoInSindicatos.
     * 
     * @return datFim
     */
    public java.lang.String getDatFim() {
        return datFim;
    }


    /**
     * Sets the datFim value for this FichaBasicaSindicatoInSindicatos.
     * 
     * @param datFim
     */
    public void setDatFim(java.lang.String datFim) {
        this.datFim = datFim;
    }


    /**
     * Gets the datIni value for this FichaBasicaSindicatoInSindicatos.
     * 
     * @return datIni
     */
    public java.lang.String getDatIni() {
        return datIni;
    }


    /**
     * Sets the datIni value for this FichaBasicaSindicatoInSindicatos.
     * 
     * @param datIni
     */
    public void setDatIni(java.lang.String datIni) {
        this.datIni = datIni;
    }


    /**
     * Gets the pagSin value for this FichaBasicaSindicatoInSindicatos.
     * 
     * @return pagSin
     */
    public java.lang.String getPagSin() {
        return pagSin;
    }


    /**
     * Sets the pagSin value for this FichaBasicaSindicatoInSindicatos.
     * 
     * @param pagSin
     */
    public void setPagSin(java.lang.String pagSin) {
        this.pagSin = pagSin;
    }


    /**
     * Gets the revSin value for this FichaBasicaSindicatoInSindicatos.
     * 
     * @return revSin
     */
    public java.lang.String getRevSin() {
        return revSin;
    }


    /**
     * Sets the revSin value for this FichaBasicaSindicatoInSindicatos.
     * 
     * @param revSin
     */
    public void setRevSin(java.lang.String revSin) {
        this.revSin = revSin;
    }


    /**
     * Gets the socSin value for this FichaBasicaSindicatoInSindicatos.
     * 
     * @return socSin
     */
    public java.lang.String getSocSin() {
        return socSin;
    }


    /**
     * Sets the socSin value for this FichaBasicaSindicatoInSindicatos.
     * 
     * @param socSin
     */
    public void setSocSin(java.lang.String socSin) {
        this.socSin = socSin;
    }


    /**
     * Gets the tasSin value for this FichaBasicaSindicatoInSindicatos.
     * 
     * @return tasSin
     */
    public java.lang.String getTasSin() {
        return tasSin;
    }


    /**
     * Sets the tasSin value for this FichaBasicaSindicatoInSindicatos.
     * 
     * @param tasSin
     */
    public void setTasSin(java.lang.String tasSin) {
        this.tasSin = tasSin;
    }


    /**
     * Gets the tipOpe value for this FichaBasicaSindicatoInSindicatos.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this FichaBasicaSindicatoInSindicatos.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FichaBasicaSindicatoInSindicatos)) return false;
        FichaBasicaSindicatoInSindicatos other = (FichaBasicaSindicatoInSindicatos) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codSin==null && other.getCodSin()==null) || 
             (this.codSin!=null &&
              this.codSin.equals(other.getCodSin()))) &&
            ((this.cusSin==null && other.getCusSin()==null) || 
             (this.cusSin!=null &&
              this.cusSin.equals(other.getCusSin()))) &&
            ((this.datFim==null && other.getDatFim()==null) || 
             (this.datFim!=null &&
              this.datFim.equals(other.getDatFim()))) &&
            ((this.datIni==null && other.getDatIni()==null) || 
             (this.datIni!=null &&
              this.datIni.equals(other.getDatIni()))) &&
            ((this.pagSin==null && other.getPagSin()==null) || 
             (this.pagSin!=null &&
              this.pagSin.equals(other.getPagSin()))) &&
            ((this.revSin==null && other.getRevSin()==null) || 
             (this.revSin!=null &&
              this.revSin.equals(other.getRevSin()))) &&
            ((this.socSin==null && other.getSocSin()==null) || 
             (this.socSin!=null &&
              this.socSin.equals(other.getSocSin()))) &&
            ((this.tasSin==null && other.getTasSin()==null) || 
             (this.tasSin!=null &&
              this.tasSin.equals(other.getTasSin()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodSin() != null) {
            _hashCode += getCodSin().hashCode();
        }
        if (getCusSin() != null) {
            _hashCode += getCusSin().hashCode();
        }
        if (getDatFim() != null) {
            _hashCode += getDatFim().hashCode();
        }
        if (getDatIni() != null) {
            _hashCode += getDatIni().hashCode();
        }
        if (getPagSin() != null) {
            _hashCode += getPagSin().hashCode();
        }
        if (getRevSin() != null) {
            _hashCode += getRevSin().hashCode();
        }
        if (getSocSin() != null) {
            _hashCode += getSocSin().hashCode();
        }
        if (getTasSin() != null) {
            _hashCode += getTasSin().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FichaBasicaSindicatoInSindicatos.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fichaBasicaSindicatoInSindicatos"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codSin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codSin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cusSin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cusSin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datFim");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datFim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datIni");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datIni"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pagSin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pagSin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("revSin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "revSin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("socSin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "socSin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tasSin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tasSin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
