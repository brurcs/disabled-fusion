/**
 * DependentesPeriodosIRInPeriodosIr.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.dependentes;

public class DependentesPeriodosIRInPeriodosIr  implements java.io.Serializable {
    private java.lang.String cmpFim;

    private java.lang.String cmpIni;

    private java.lang.String tipOpe;

    public DependentesPeriodosIRInPeriodosIr() {
    }

    public DependentesPeriodosIRInPeriodosIr(
           java.lang.String cmpFim,
           java.lang.String cmpIni,
           java.lang.String tipOpe) {
           this.cmpFim = cmpFim;
           this.cmpIni = cmpIni;
           this.tipOpe = tipOpe;
    }


    /**
     * Gets the cmpFim value for this DependentesPeriodosIRInPeriodosIr.
     * 
     * @return cmpFim
     */
    public java.lang.String getCmpFim() {
        return cmpFim;
    }


    /**
     * Sets the cmpFim value for this DependentesPeriodosIRInPeriodosIr.
     * 
     * @param cmpFim
     */
    public void setCmpFim(java.lang.String cmpFim) {
        this.cmpFim = cmpFim;
    }


    /**
     * Gets the cmpIni value for this DependentesPeriodosIRInPeriodosIr.
     * 
     * @return cmpIni
     */
    public java.lang.String getCmpIni() {
        return cmpIni;
    }


    /**
     * Sets the cmpIni value for this DependentesPeriodosIRInPeriodosIr.
     * 
     * @param cmpIni
     */
    public void setCmpIni(java.lang.String cmpIni) {
        this.cmpIni = cmpIni;
    }


    /**
     * Gets the tipOpe value for this DependentesPeriodosIRInPeriodosIr.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this DependentesPeriodosIRInPeriodosIr.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DependentesPeriodosIRInPeriodosIr)) return false;
        DependentesPeriodosIRInPeriodosIr other = (DependentesPeriodosIRInPeriodosIr) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cmpFim==null && other.getCmpFim()==null) || 
             (this.cmpFim!=null &&
              this.cmpFim.equals(other.getCmpFim()))) &&
            ((this.cmpIni==null && other.getCmpIni()==null) || 
             (this.cmpIni!=null &&
              this.cmpIni.equals(other.getCmpIni()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCmpFim() != null) {
            _hashCode += getCmpFim().hashCode();
        }
        if (getCmpIni() != null) {
            _hashCode += getCmpIni().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DependentesPeriodosIRInPeriodosIr.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "dependentesPeriodosIRInPeriodosIr"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cmpFim");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cmpFim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cmpIni");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cmpIni"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
