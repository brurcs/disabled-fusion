/**
 * TitulosbuscarPendentesCPOutTitulo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.gtc;

public class TitulosbuscarPendentesCPOutTitulo  implements java.io.Serializable {
    private java.lang.Integer codigoEmpresa;

    private java.lang.Integer codigoFilial;

    private java.lang.Integer codigoFornecedor;

    private java.lang.String codigoTipoTitulo;

    private java.lang.String dataEntrada;

    private java.lang.String dataPagamento;

    private java.lang.String dataVencimento;

    private java.lang.String descricaoMoeda;

    private java.lang.String descricaoTipoTitulo;

    private java.lang.String nomeEmpresa;

    private java.lang.String nomeFilial;

    private java.lang.String nomeFornecedor;

    private java.lang.String numero;

    private java.lang.String observacao;

    private java.lang.String siglaMoeda;

    private java.lang.Double valorLiquido;

    private java.lang.Double valorOriginal;

    public TitulosbuscarPendentesCPOutTitulo() {
    }

    public TitulosbuscarPendentesCPOutTitulo(
           java.lang.Integer codigoEmpresa,
           java.lang.Integer codigoFilial,
           java.lang.Integer codigoFornecedor,
           java.lang.String codigoTipoTitulo,
           java.lang.String dataEntrada,
           java.lang.String dataPagamento,
           java.lang.String dataVencimento,
           java.lang.String descricaoMoeda,
           java.lang.String descricaoTipoTitulo,
           java.lang.String nomeEmpresa,
           java.lang.String nomeFilial,
           java.lang.String nomeFornecedor,
           java.lang.String numero,
           java.lang.String observacao,
           java.lang.String siglaMoeda,
           java.lang.Double valorLiquido,
           java.lang.Double valorOriginal) {
           this.codigoEmpresa = codigoEmpresa;
           this.codigoFilial = codigoFilial;
           this.codigoFornecedor = codigoFornecedor;
           this.codigoTipoTitulo = codigoTipoTitulo;
           this.dataEntrada = dataEntrada;
           this.dataPagamento = dataPagamento;
           this.dataVencimento = dataVencimento;
           this.descricaoMoeda = descricaoMoeda;
           this.descricaoTipoTitulo = descricaoTipoTitulo;
           this.nomeEmpresa = nomeEmpresa;
           this.nomeFilial = nomeFilial;
           this.nomeFornecedor = nomeFornecedor;
           this.numero = numero;
           this.observacao = observacao;
           this.siglaMoeda = siglaMoeda;
           this.valorLiquido = valorLiquido;
           this.valorOriginal = valorOriginal;
    }


    /**
     * Gets the codigoEmpresa value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @return codigoEmpresa
     */
    public java.lang.Integer getCodigoEmpresa() {
        return codigoEmpresa;
    }


    /**
     * Sets the codigoEmpresa value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @param codigoEmpresa
     */
    public void setCodigoEmpresa(java.lang.Integer codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }


    /**
     * Gets the codigoFilial value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @return codigoFilial
     */
    public java.lang.Integer getCodigoFilial() {
        return codigoFilial;
    }


    /**
     * Sets the codigoFilial value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @param codigoFilial
     */
    public void setCodigoFilial(java.lang.Integer codigoFilial) {
        this.codigoFilial = codigoFilial;
    }


    /**
     * Gets the codigoFornecedor value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @return codigoFornecedor
     */
    public java.lang.Integer getCodigoFornecedor() {
        return codigoFornecedor;
    }


    /**
     * Sets the codigoFornecedor value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @param codigoFornecedor
     */
    public void setCodigoFornecedor(java.lang.Integer codigoFornecedor) {
        this.codigoFornecedor = codigoFornecedor;
    }


    /**
     * Gets the codigoTipoTitulo value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @return codigoTipoTitulo
     */
    public java.lang.String getCodigoTipoTitulo() {
        return codigoTipoTitulo;
    }


    /**
     * Sets the codigoTipoTitulo value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @param codigoTipoTitulo
     */
    public void setCodigoTipoTitulo(java.lang.String codigoTipoTitulo) {
        this.codigoTipoTitulo = codigoTipoTitulo;
    }


    /**
     * Gets the dataEntrada value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @return dataEntrada
     */
    public java.lang.String getDataEntrada() {
        return dataEntrada;
    }


    /**
     * Sets the dataEntrada value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @param dataEntrada
     */
    public void setDataEntrada(java.lang.String dataEntrada) {
        this.dataEntrada = dataEntrada;
    }


    /**
     * Gets the dataPagamento value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @return dataPagamento
     */
    public java.lang.String getDataPagamento() {
        return dataPagamento;
    }


    /**
     * Sets the dataPagamento value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @param dataPagamento
     */
    public void setDataPagamento(java.lang.String dataPagamento) {
        this.dataPagamento = dataPagamento;
    }


    /**
     * Gets the dataVencimento value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @return dataVencimento
     */
    public java.lang.String getDataVencimento() {
        return dataVencimento;
    }


    /**
     * Sets the dataVencimento value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @param dataVencimento
     */
    public void setDataVencimento(java.lang.String dataVencimento) {
        this.dataVencimento = dataVencimento;
    }


    /**
     * Gets the descricaoMoeda value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @return descricaoMoeda
     */
    public java.lang.String getDescricaoMoeda() {
        return descricaoMoeda;
    }


    /**
     * Sets the descricaoMoeda value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @param descricaoMoeda
     */
    public void setDescricaoMoeda(java.lang.String descricaoMoeda) {
        this.descricaoMoeda = descricaoMoeda;
    }


    /**
     * Gets the descricaoTipoTitulo value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @return descricaoTipoTitulo
     */
    public java.lang.String getDescricaoTipoTitulo() {
        return descricaoTipoTitulo;
    }


    /**
     * Sets the descricaoTipoTitulo value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @param descricaoTipoTitulo
     */
    public void setDescricaoTipoTitulo(java.lang.String descricaoTipoTitulo) {
        this.descricaoTipoTitulo = descricaoTipoTitulo;
    }


    /**
     * Gets the nomeEmpresa value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @return nomeEmpresa
     */
    public java.lang.String getNomeEmpresa() {
        return nomeEmpresa;
    }


    /**
     * Sets the nomeEmpresa value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @param nomeEmpresa
     */
    public void setNomeEmpresa(java.lang.String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }


    /**
     * Gets the nomeFilial value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @return nomeFilial
     */
    public java.lang.String getNomeFilial() {
        return nomeFilial;
    }


    /**
     * Sets the nomeFilial value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @param nomeFilial
     */
    public void setNomeFilial(java.lang.String nomeFilial) {
        this.nomeFilial = nomeFilial;
    }


    /**
     * Gets the nomeFornecedor value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @return nomeFornecedor
     */
    public java.lang.String getNomeFornecedor() {
        return nomeFornecedor;
    }


    /**
     * Sets the nomeFornecedor value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @param nomeFornecedor
     */
    public void setNomeFornecedor(java.lang.String nomeFornecedor) {
        this.nomeFornecedor = nomeFornecedor;
    }


    /**
     * Gets the numero value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @return numero
     */
    public java.lang.String getNumero() {
        return numero;
    }


    /**
     * Sets the numero value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @param numero
     */
    public void setNumero(java.lang.String numero) {
        this.numero = numero;
    }


    /**
     * Gets the observacao value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @return observacao
     */
    public java.lang.String getObservacao() {
        return observacao;
    }


    /**
     * Sets the observacao value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @param observacao
     */
    public void setObservacao(java.lang.String observacao) {
        this.observacao = observacao;
    }


    /**
     * Gets the siglaMoeda value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @return siglaMoeda
     */
    public java.lang.String getSiglaMoeda() {
        return siglaMoeda;
    }


    /**
     * Sets the siglaMoeda value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @param siglaMoeda
     */
    public void setSiglaMoeda(java.lang.String siglaMoeda) {
        this.siglaMoeda = siglaMoeda;
    }


    /**
     * Gets the valorLiquido value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @return valorLiquido
     */
    public java.lang.Double getValorLiquido() {
        return valorLiquido;
    }


    /**
     * Sets the valorLiquido value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @param valorLiquido
     */
    public void setValorLiquido(java.lang.Double valorLiquido) {
        this.valorLiquido = valorLiquido;
    }


    /**
     * Gets the valorOriginal value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @return valorOriginal
     */
    public java.lang.Double getValorOriginal() {
        return valorOriginal;
    }


    /**
     * Sets the valorOriginal value for this TitulosbuscarPendentesCPOutTitulo.
     * 
     * @param valorOriginal
     */
    public void setValorOriginal(java.lang.Double valorOriginal) {
        this.valorOriginal = valorOriginal;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosbuscarPendentesCPOutTitulo)) return false;
        TitulosbuscarPendentesCPOutTitulo other = (TitulosbuscarPendentesCPOutTitulo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codigoEmpresa==null && other.getCodigoEmpresa()==null) || 
             (this.codigoEmpresa!=null &&
              this.codigoEmpresa.equals(other.getCodigoEmpresa()))) &&
            ((this.codigoFilial==null && other.getCodigoFilial()==null) || 
             (this.codigoFilial!=null &&
              this.codigoFilial.equals(other.getCodigoFilial()))) &&
            ((this.codigoFornecedor==null && other.getCodigoFornecedor()==null) || 
             (this.codigoFornecedor!=null &&
              this.codigoFornecedor.equals(other.getCodigoFornecedor()))) &&
            ((this.codigoTipoTitulo==null && other.getCodigoTipoTitulo()==null) || 
             (this.codigoTipoTitulo!=null &&
              this.codigoTipoTitulo.equals(other.getCodigoTipoTitulo()))) &&
            ((this.dataEntrada==null && other.getDataEntrada()==null) || 
             (this.dataEntrada!=null &&
              this.dataEntrada.equals(other.getDataEntrada()))) &&
            ((this.dataPagamento==null && other.getDataPagamento()==null) || 
             (this.dataPagamento!=null &&
              this.dataPagamento.equals(other.getDataPagamento()))) &&
            ((this.dataVencimento==null && other.getDataVencimento()==null) || 
             (this.dataVencimento!=null &&
              this.dataVencimento.equals(other.getDataVencimento()))) &&
            ((this.descricaoMoeda==null && other.getDescricaoMoeda()==null) || 
             (this.descricaoMoeda!=null &&
              this.descricaoMoeda.equals(other.getDescricaoMoeda()))) &&
            ((this.descricaoTipoTitulo==null && other.getDescricaoTipoTitulo()==null) || 
             (this.descricaoTipoTitulo!=null &&
              this.descricaoTipoTitulo.equals(other.getDescricaoTipoTitulo()))) &&
            ((this.nomeEmpresa==null && other.getNomeEmpresa()==null) || 
             (this.nomeEmpresa!=null &&
              this.nomeEmpresa.equals(other.getNomeEmpresa()))) &&
            ((this.nomeFilial==null && other.getNomeFilial()==null) || 
             (this.nomeFilial!=null &&
              this.nomeFilial.equals(other.getNomeFilial()))) &&
            ((this.nomeFornecedor==null && other.getNomeFornecedor()==null) || 
             (this.nomeFornecedor!=null &&
              this.nomeFornecedor.equals(other.getNomeFornecedor()))) &&
            ((this.numero==null && other.getNumero()==null) || 
             (this.numero!=null &&
              this.numero.equals(other.getNumero()))) &&
            ((this.observacao==null && other.getObservacao()==null) || 
             (this.observacao!=null &&
              this.observacao.equals(other.getObservacao()))) &&
            ((this.siglaMoeda==null && other.getSiglaMoeda()==null) || 
             (this.siglaMoeda!=null &&
              this.siglaMoeda.equals(other.getSiglaMoeda()))) &&
            ((this.valorLiquido==null && other.getValorLiquido()==null) || 
             (this.valorLiquido!=null &&
              this.valorLiquido.equals(other.getValorLiquido()))) &&
            ((this.valorOriginal==null && other.getValorOriginal()==null) || 
             (this.valorOriginal!=null &&
              this.valorOriginal.equals(other.getValorOriginal())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodigoEmpresa() != null) {
            _hashCode += getCodigoEmpresa().hashCode();
        }
        if (getCodigoFilial() != null) {
            _hashCode += getCodigoFilial().hashCode();
        }
        if (getCodigoFornecedor() != null) {
            _hashCode += getCodigoFornecedor().hashCode();
        }
        if (getCodigoTipoTitulo() != null) {
            _hashCode += getCodigoTipoTitulo().hashCode();
        }
        if (getDataEntrada() != null) {
            _hashCode += getDataEntrada().hashCode();
        }
        if (getDataPagamento() != null) {
            _hashCode += getDataPagamento().hashCode();
        }
        if (getDataVencimento() != null) {
            _hashCode += getDataVencimento().hashCode();
        }
        if (getDescricaoMoeda() != null) {
            _hashCode += getDescricaoMoeda().hashCode();
        }
        if (getDescricaoTipoTitulo() != null) {
            _hashCode += getDescricaoTipoTitulo().hashCode();
        }
        if (getNomeEmpresa() != null) {
            _hashCode += getNomeEmpresa().hashCode();
        }
        if (getNomeFilial() != null) {
            _hashCode += getNomeFilial().hashCode();
        }
        if (getNomeFornecedor() != null) {
            _hashCode += getNomeFornecedor().hashCode();
        }
        if (getNumero() != null) {
            _hashCode += getNumero().hashCode();
        }
        if (getObservacao() != null) {
            _hashCode += getObservacao().hashCode();
        }
        if (getSiglaMoeda() != null) {
            _hashCode += getSiglaMoeda().hashCode();
        }
        if (getValorLiquido() != null) {
            _hashCode += getValorLiquido().hashCode();
        }
        if (getValorOriginal() != null) {
            _hashCode += getValorOriginal().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosbuscarPendentesCPOutTitulo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosbuscarPendentesCPOutTitulo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoEmpresa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codigoEmpresa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoFilial");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codigoFilial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoFornecedor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codigoFornecedor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoTipoTitulo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codigoTipoTitulo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataEntrada");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dataEntrada"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataPagamento");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dataPagamento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataVencimento");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dataVencimento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descricaoMoeda");
        elemField.setXmlName(new javax.xml.namespace.QName("", "descricaoMoeda"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descricaoTipoTitulo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "descricaoTipoTitulo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomeEmpresa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomeEmpresa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomeFilial");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomeFilial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomeFornecedor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomeFornecedor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numero");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numero"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("observacao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "observacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("siglaMoeda");
        elemField.setXmlName(new javax.xml.namespace.QName("", "siglaMoeda"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorLiquido");
        elemField.setXmlName(new javax.xml.namespace.QName("", "valorLiquido"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorOriginal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "valorOriginal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
