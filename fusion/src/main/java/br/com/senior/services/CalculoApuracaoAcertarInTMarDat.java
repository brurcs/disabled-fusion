/**
 * CalculoApuracaoAcertarInTMarDat.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class CalculoApuracaoAcertarInTMarDat  implements java.io.Serializable {
    private java.lang.Integer codFnc;

    private java.lang.Integer codJMa;

    private java.lang.Integer codPlt;

    private java.lang.Integer codRlg;

    private java.lang.String datMar;

    private java.lang.String dirMar;

    private java.lang.String horMar;

    private java.lang.Integer linha;

    private java.lang.String marcOri;

    private java.lang.String obsJMa;

    private java.lang.String oriMar;

    private java.lang.Integer seqAcc;

    private java.lang.String tipOpe;

    private java.lang.Integer usoMar;

    public CalculoApuracaoAcertarInTMarDat() {
    }

    public CalculoApuracaoAcertarInTMarDat(
           java.lang.Integer codFnc,
           java.lang.Integer codJMa,
           java.lang.Integer codPlt,
           java.lang.Integer codRlg,
           java.lang.String datMar,
           java.lang.String dirMar,
           java.lang.String horMar,
           java.lang.Integer linha,
           java.lang.String marcOri,
           java.lang.String obsJMa,
           java.lang.String oriMar,
           java.lang.Integer seqAcc,
           java.lang.String tipOpe,
           java.lang.Integer usoMar) {
           this.codFnc = codFnc;
           this.codJMa = codJMa;
           this.codPlt = codPlt;
           this.codRlg = codRlg;
           this.datMar = datMar;
           this.dirMar = dirMar;
           this.horMar = horMar;
           this.linha = linha;
           this.marcOri = marcOri;
           this.obsJMa = obsJMa;
           this.oriMar = oriMar;
           this.seqAcc = seqAcc;
           this.tipOpe = tipOpe;
           this.usoMar = usoMar;
    }


    /**
     * Gets the codFnc value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @return codFnc
     */
    public java.lang.Integer getCodFnc() {
        return codFnc;
    }


    /**
     * Sets the codFnc value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @param codFnc
     */
    public void setCodFnc(java.lang.Integer codFnc) {
        this.codFnc = codFnc;
    }


    /**
     * Gets the codJMa value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @return codJMa
     */
    public java.lang.Integer getCodJMa() {
        return codJMa;
    }


    /**
     * Sets the codJMa value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @param codJMa
     */
    public void setCodJMa(java.lang.Integer codJMa) {
        this.codJMa = codJMa;
    }


    /**
     * Gets the codPlt value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @return codPlt
     */
    public java.lang.Integer getCodPlt() {
        return codPlt;
    }


    /**
     * Sets the codPlt value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @param codPlt
     */
    public void setCodPlt(java.lang.Integer codPlt) {
        this.codPlt = codPlt;
    }


    /**
     * Gets the codRlg value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @return codRlg
     */
    public java.lang.Integer getCodRlg() {
        return codRlg;
    }


    /**
     * Sets the codRlg value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @param codRlg
     */
    public void setCodRlg(java.lang.Integer codRlg) {
        this.codRlg = codRlg;
    }


    /**
     * Gets the datMar value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @return datMar
     */
    public java.lang.String getDatMar() {
        return datMar;
    }


    /**
     * Sets the datMar value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @param datMar
     */
    public void setDatMar(java.lang.String datMar) {
        this.datMar = datMar;
    }


    /**
     * Gets the dirMar value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @return dirMar
     */
    public java.lang.String getDirMar() {
        return dirMar;
    }


    /**
     * Sets the dirMar value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @param dirMar
     */
    public void setDirMar(java.lang.String dirMar) {
        this.dirMar = dirMar;
    }


    /**
     * Gets the horMar value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @return horMar
     */
    public java.lang.String getHorMar() {
        return horMar;
    }


    /**
     * Sets the horMar value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @param horMar
     */
    public void setHorMar(java.lang.String horMar) {
        this.horMar = horMar;
    }


    /**
     * Gets the linha value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @return linha
     */
    public java.lang.Integer getLinha() {
        return linha;
    }


    /**
     * Sets the linha value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @param linha
     */
    public void setLinha(java.lang.Integer linha) {
        this.linha = linha;
    }


    /**
     * Gets the marcOri value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @return marcOri
     */
    public java.lang.String getMarcOri() {
        return marcOri;
    }


    /**
     * Sets the marcOri value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @param marcOri
     */
    public void setMarcOri(java.lang.String marcOri) {
        this.marcOri = marcOri;
    }


    /**
     * Gets the obsJMa value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @return obsJMa
     */
    public java.lang.String getObsJMa() {
        return obsJMa;
    }


    /**
     * Sets the obsJMa value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @param obsJMa
     */
    public void setObsJMa(java.lang.String obsJMa) {
        this.obsJMa = obsJMa;
    }


    /**
     * Gets the oriMar value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @return oriMar
     */
    public java.lang.String getOriMar() {
        return oriMar;
    }


    /**
     * Sets the oriMar value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @param oriMar
     */
    public void setOriMar(java.lang.String oriMar) {
        this.oriMar = oriMar;
    }


    /**
     * Gets the seqAcc value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @return seqAcc
     */
    public java.lang.Integer getSeqAcc() {
        return seqAcc;
    }


    /**
     * Sets the seqAcc value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @param seqAcc
     */
    public void setSeqAcc(java.lang.Integer seqAcc) {
        this.seqAcc = seqAcc;
    }


    /**
     * Gets the tipOpe value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }


    /**
     * Gets the usoMar value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @return usoMar
     */
    public java.lang.Integer getUsoMar() {
        return usoMar;
    }


    /**
     * Sets the usoMar value for this CalculoApuracaoAcertarInTMarDat.
     * 
     * @param usoMar
     */
    public void setUsoMar(java.lang.Integer usoMar) {
        this.usoMar = usoMar;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CalculoApuracaoAcertarInTMarDat)) return false;
        CalculoApuracaoAcertarInTMarDat other = (CalculoApuracaoAcertarInTMarDat) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codFnc==null && other.getCodFnc()==null) || 
             (this.codFnc!=null &&
              this.codFnc.equals(other.getCodFnc()))) &&
            ((this.codJMa==null && other.getCodJMa()==null) || 
             (this.codJMa!=null &&
              this.codJMa.equals(other.getCodJMa()))) &&
            ((this.codPlt==null && other.getCodPlt()==null) || 
             (this.codPlt!=null &&
              this.codPlt.equals(other.getCodPlt()))) &&
            ((this.codRlg==null && other.getCodRlg()==null) || 
             (this.codRlg!=null &&
              this.codRlg.equals(other.getCodRlg()))) &&
            ((this.datMar==null && other.getDatMar()==null) || 
             (this.datMar!=null &&
              this.datMar.equals(other.getDatMar()))) &&
            ((this.dirMar==null && other.getDirMar()==null) || 
             (this.dirMar!=null &&
              this.dirMar.equals(other.getDirMar()))) &&
            ((this.horMar==null && other.getHorMar()==null) || 
             (this.horMar!=null &&
              this.horMar.equals(other.getHorMar()))) &&
            ((this.linha==null && other.getLinha()==null) || 
             (this.linha!=null &&
              this.linha.equals(other.getLinha()))) &&
            ((this.marcOri==null && other.getMarcOri()==null) || 
             (this.marcOri!=null &&
              this.marcOri.equals(other.getMarcOri()))) &&
            ((this.obsJMa==null && other.getObsJMa()==null) || 
             (this.obsJMa!=null &&
              this.obsJMa.equals(other.getObsJMa()))) &&
            ((this.oriMar==null && other.getOriMar()==null) || 
             (this.oriMar!=null &&
              this.oriMar.equals(other.getOriMar()))) &&
            ((this.seqAcc==null && other.getSeqAcc()==null) || 
             (this.seqAcc!=null &&
              this.seqAcc.equals(other.getSeqAcc()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe()))) &&
            ((this.usoMar==null && other.getUsoMar()==null) || 
             (this.usoMar!=null &&
              this.usoMar.equals(other.getUsoMar())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodFnc() != null) {
            _hashCode += getCodFnc().hashCode();
        }
        if (getCodJMa() != null) {
            _hashCode += getCodJMa().hashCode();
        }
        if (getCodPlt() != null) {
            _hashCode += getCodPlt().hashCode();
        }
        if (getCodRlg() != null) {
            _hashCode += getCodRlg().hashCode();
        }
        if (getDatMar() != null) {
            _hashCode += getDatMar().hashCode();
        }
        if (getDirMar() != null) {
            _hashCode += getDirMar().hashCode();
        }
        if (getHorMar() != null) {
            _hashCode += getHorMar().hashCode();
        }
        if (getLinha() != null) {
            _hashCode += getLinha().hashCode();
        }
        if (getMarcOri() != null) {
            _hashCode += getMarcOri().hashCode();
        }
        if (getObsJMa() != null) {
            _hashCode += getObsJMa().hashCode();
        }
        if (getOriMar() != null) {
            _hashCode += getOriMar().hashCode();
        }
        if (getSeqAcc() != null) {
            _hashCode += getSeqAcc().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        if (getUsoMar() != null) {
            _hashCode += getUsoMar().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CalculoApuracaoAcertarInTMarDat.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "calculoApuracaoAcertarInTMarDat"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFnc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFnc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codJMa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codJMa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRlg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRlg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datMar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datMar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dirMar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dirMar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horMar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horMar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("linha");
        elemField.setXmlName(new javax.xml.namespace.QName("", "linha"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("marcOri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "marcOri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsJMa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsJMa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("oriMar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "oriMar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqAcc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqAcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usoMar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usoMar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
