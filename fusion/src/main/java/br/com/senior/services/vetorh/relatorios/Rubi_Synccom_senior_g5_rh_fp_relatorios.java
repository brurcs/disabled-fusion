/**
 * Rubi_Synccom_senior_g5_rh_fp_relatorios.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.relatorios;

public interface Rubi_Synccom_senior_g5_rh_fp_relatorios extends java.rmi.Remote {
    public br.com.senior.services.vetorh.relatorios.RelatoriosRelatoriosOut relatorios(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.relatorios.RelatoriosRelatoriosIn parameters) throws java.rmi.RemoteException;
}
