/**
 * Sm_Synccom_senior_g5_rh_sm_asoexterno.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.sm.asoExterno;

public interface Sm_Synccom_senior_g5_rh_sm_asoexterno extends java.rmi.Remote {
    public br.com.senior.services.sm.asoExterno.AsoexternoASOExterno2Out ASOExterno_2(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.sm.asoExterno.AsoexternoASOExterno2In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.sm.asoExterno.AsoexternoASOExternoOut ASOExterno(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.sm.asoExterno.AsoexternoASOExternoIn parameters) throws java.rmi.RemoteException;
}
