/**
 * G5SeniorServicesLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.sm.asoExterno;

import java.net.URL;

import javax.xml.rpc.ServiceException;

import br.com.senior.services.Ronda_Synccom_senior_g5_rh_hr_calculoApuracao;
import br.com.senior.services.vetorh.constants.WSConfigs;

public class G5SeniorServicesLocatorAso extends org.apache.axis.client.Service implements br.com.senior.services.G5SeniorServices {

    public G5SeniorServicesLocatorAso() {
    }


    public G5SeniorServicesLocatorAso(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public G5SeniorServicesLocatorAso(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for sm_Synccom_senior_g5_rh_sm_asoexternoPort
    private java.lang.String sm_Synccom_senior_g5_rh_sm_asoexternoPort_address = "http://"+(new WSConfigs()).getHostService()+"/g5-senior-services/sm_Synccom_senior_g5_rh_sm_asoexterno"; 

    public java.lang.String getsm_Synccom_senior_g5_rh_sm_asoexternoPortAddress() {
        return sm_Synccom_senior_g5_rh_sm_asoexternoPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String sm_Synccom_senior_g5_rh_sm_asoexternoPortWSDDServiceName = "sm_Synccom_senior_g5_rh_sm_asoexternoPort";

    public java.lang.String getsm_Synccom_senior_g5_rh_sm_asoexternoPortWSDDServiceName() {
        return sm_Synccom_senior_g5_rh_sm_asoexternoPortWSDDServiceName;
    }

    public void setsm_Synccom_senior_g5_rh_sm_asoexternoPortWSDDServiceName(java.lang.String name) {
        sm_Synccom_senior_g5_rh_sm_asoexternoPortWSDDServiceName = name;
    }

    public br.com.senior.services.sm.asoExterno.Sm_Synccom_senior_g5_rh_sm_asoexterno getsm_Synccom_senior_g5_rh_sm_asoexternoPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(sm_Synccom_senior_g5_rh_sm_asoexternoPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getsm_Synccom_senior_g5_rh_sm_asoexternoPort(endpoint);
    }

    public br.com.senior.services.sm.asoExterno.Sm_Synccom_senior_g5_rh_sm_asoexterno getsm_Synccom_senior_g5_rh_sm_asoexternoPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            br.com.senior.services.sm.asoExterno.Sm_Synccom_senior_g5_rh_sm_asoexternoPortBindingStub _stub = new br.com.senior.services.sm.asoExterno.Sm_Synccom_senior_g5_rh_sm_asoexternoPortBindingStub(portAddress, this);
            _stub.setPortName(getsm_Synccom_senior_g5_rh_sm_asoexternoPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setsm_Synccom_senior_g5_rh_sm_asoexternoPortEndpointAddress(java.lang.String address) {
        sm_Synccom_senior_g5_rh_sm_asoexternoPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (br.com.senior.services.sm.asoExterno.Sm_Synccom_senior_g5_rh_sm_asoexterno.class.isAssignableFrom(serviceEndpointInterface)) {
                br.com.senior.services.sm.asoExterno.Sm_Synccom_senior_g5_rh_sm_asoexternoPortBindingStub _stub = new br.com.senior.services.sm.asoExterno.Sm_Synccom_senior_g5_rh_sm_asoexternoPortBindingStub(new java.net.URL(sm_Synccom_senior_g5_rh_sm_asoexternoPort_address), this);
                _stub.setPortName(getsm_Synccom_senior_g5_rh_sm_asoexternoPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("sm_Synccom_senior_g5_rh_sm_asoexternoPort".equals(inputPortName)) {
            return getsm_Synccom_senior_g5_rh_sm_asoexternoPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://services.senior.com.br", "g5-senior-services");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://services.senior.com.br", "sm_Synccom_senior_g5_rh_sm_asoexternoPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("sm_Synccom_senior_g5_rh_sm_asoexternoPort".equals(portName)) {
            setsm_Synccom_senior_g5_rh_sm_asoexternoPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }


    @Override
    public String getronda_Synccom_senior_g5_rh_hr_calculoApuracaoPortAddress() {
	// TODO Auto-generated method stub
	return null;
    }


    @Override
    public Ronda_Synccom_senior_g5_rh_hr_calculoApuracao getronda_Synccom_senior_g5_rh_hr_calculoApuracaoPort() throws ServiceException {
	// TODO Auto-generated method stub
	return null;
    }


    @Override
    public Ronda_Synccom_senior_g5_rh_hr_calculoApuracao getronda_Synccom_senior_g5_rh_hr_calculoApuracaoPort(URL portAddress) throws ServiceException {
	// TODO Auto-generated method stub
	return null;
    }

}
