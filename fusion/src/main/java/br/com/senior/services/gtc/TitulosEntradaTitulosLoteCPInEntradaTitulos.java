/**
 * TitulosEntradaTitulosLoteCPInEntradaTitulos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.gtc;

public class TitulosEntradaTitulosLoteCPInEntradaTitulos implements java.io.Serializable
{
	private java.lang.String antDsc;

	private java.lang.String ccbFor;

	private java.lang.String codAge;

	private java.lang.String codBan;

	private java.lang.String codCcu;

	private java.lang.String codCrp;

	private java.lang.String codCrt;

	private java.lang.Double codFav;

	private java.lang.Integer codFil;

	private java.lang.Integer codFor;

	private java.lang.Integer codFpg;

	private java.lang.Integer codFpj;

	private java.lang.String codMoe;

	private java.lang.String codMpt;

	private java.lang.Integer codNtg;

	private java.lang.String codPor;

	private java.lang.String codTns;

	private java.lang.String codTpt;

	private java.lang.Double cotEmi;

	private java.lang.Double cotNeg;

	private java.lang.Integer ctaFin;

	private java.lang.Integer ctaRed;

	private java.lang.String datDsc;

	private java.lang.String datEmi;

	private java.lang.String datEnt;

	private java.lang.String datNeg;

	private java.lang.String datPpt;

	private java.lang.Double dscNeg;

	private java.lang.Double jrsDia;

	private java.lang.Double jrsNeg;

	private java.lang.Double mulNeg;

	private java.lang.Integer numPrj;

	private java.lang.String numTit;

	private java.lang.String obsTcp;

	private java.lang.Double outNeg;

	private java.lang.Integer perDsc;

	private java.lang.Integer perJrs;

	private java.lang.Integer perMul;

	private java.lang.Integer priPgt;

	private java.lang.String proJrs;

	private TitulosEntradaTitulosLoteCPInEntradaTitulosRateio[] rateio;

	private java.lang.String tipJrs;

	private java.lang.Integer tipTcc;

	private java.lang.String titBan;

	private java.lang.Integer tolDsc;

	private java.lang.Integer tolJrs;

	private java.lang.Integer tolMul;

	private java.lang.String vctOri;

	private java.lang.String vctPro;

	private java.lang.Double vlrDsc;

	private java.lang.Double vlrOri;

	private java.lang.Double vlrRba;

	public TitulosEntradaTitulosLoteCPInEntradaTitulos()
	{
	}

	public TitulosEntradaTitulosLoteCPInEntradaTitulos(java.lang.String antDsc, java.lang.String ccbFor, java.lang.String codAge, java.lang.String codBan, java.lang.String codCcu, java.lang.String codCrp, java.lang.String codCrt, java.lang.Double codFav, java.lang.Integer codFil, java.lang.Integer codFor, java.lang.Integer codFpg, java.lang.Integer codFpj, java.lang.String codMoe, java.lang.String codMpt, java.lang.Integer codNtg, java.lang.String codPor, java.lang.String codTns,
			java.lang.String codTpt, java.lang.Double cotEmi, java.lang.Double cotNeg, java.lang.Integer ctaFin, java.lang.Integer ctaRed, java.lang.String datDsc, java.lang.String datEmi, java.lang.String datEnt, java.lang.String datNeg, java.lang.String datPpt, java.lang.Double dscNeg, java.lang.Double jrsDia, java.lang.Double jrsNeg, java.lang.Double mulNeg, java.lang.Integer numPrj, java.lang.String numTit, java.lang.String obsTcp, java.lang.Double outNeg, java.lang.Integer perDsc,
			java.lang.Integer perJrs, java.lang.Integer perMul, java.lang.Integer priPgt, java.lang.String proJrs, TitulosEntradaTitulosLoteCPInEntradaTitulosRateio[] rateio, java.lang.String tipJrs, java.lang.Integer tipTcc, java.lang.String titBan, java.lang.Integer tolDsc, java.lang.Integer tolJrs, java.lang.Integer tolMul, java.lang.String vctOri, java.lang.String vctPro, java.lang.Double vlrDsc, java.lang.Double vlrOri, java.lang.Double vlrRba)
	{
		this.antDsc = antDsc;
		this.ccbFor = ccbFor;
		this.codAge = codAge;
		this.codBan = codBan;
		this.codCcu = codCcu;
		this.codCrp = codCrp;
		this.codCrt = codCrt;
		this.codFav = codFav;
		this.codFil = codFil;
		this.codFor = codFor;
		this.codFpg = codFpg;
		this.codFpj = codFpj;
		this.codMoe = codMoe;
		this.codMpt = codMpt;
		this.codNtg = codNtg;
		this.codPor = codPor;
		this.codTns = codTns;
		this.codTpt = codTpt;
		this.cotEmi = cotEmi;
		this.cotNeg = cotNeg;
		this.ctaFin = ctaFin;
		this.ctaRed = ctaRed;
		this.datDsc = datDsc;
		this.datEmi = datEmi;
		this.datEnt = datEnt;
		this.datNeg = datNeg;
		this.datPpt = datPpt;
		this.dscNeg = dscNeg;
		this.jrsDia = jrsDia;
		this.jrsNeg = jrsNeg;
		this.mulNeg = mulNeg;
		this.numPrj = numPrj;
		this.numTit = numTit;
		this.obsTcp = obsTcp;
		this.outNeg = outNeg;
		this.perDsc = perDsc;
		this.perJrs = perJrs;
		this.perMul = perMul;
		this.priPgt = priPgt;
		this.proJrs = proJrs;
		this.rateio = rateio;
		this.tipJrs = tipJrs;
		this.tipTcc = tipTcc;
		this.titBan = titBan;
		this.tolDsc = tolDsc;
		this.tolJrs = tolJrs;
		this.tolMul = tolMul;
		this.vctOri = vctOri;
		this.vctPro = vctPro;
		this.vlrDsc = vlrDsc;
		this.vlrOri = vlrOri;
		this.vlrRba = vlrRba;
	}

	/**
	 * Gets the antDsc value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return antDsc
	 */
	public java.lang.String getAntDsc()
	{
		return antDsc;
	}

	/**
	 * Sets the antDsc value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param antDsc
	 */
	public void setAntDsc(java.lang.String antDsc)
	{
		this.antDsc = antDsc;
	}

	/**
	 * Gets the ccbFor value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return ccbFor
	 */
	public java.lang.String getCcbFor()
	{
		return ccbFor;
	}

	/**
	 * Sets the ccbFor value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param ccbFor
	 */
	public void setCcbFor(java.lang.String ccbFor)
	{
		this.ccbFor = ccbFor;
	}

	/**
	 * Gets the codAge value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return codAge
	 */
	public java.lang.String getCodAge()
	{
		return codAge;
	}

	/**
	 * Sets the codAge value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param codAge
	 */
	public void setCodAge(java.lang.String codAge)
	{
		this.codAge = codAge;
	}

	/**
	 * Gets the codBan value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return codBan
	 */
	public java.lang.String getCodBan()
	{
		return codBan;
	}

	/**
	 * Sets the codBan value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param codBan
	 */
	public void setCodBan(java.lang.String codBan)
	{
		this.codBan = codBan;
	}

	/**
	 * Gets the codCcu value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return codCcu
	 */
	public java.lang.String getCodCcu()
	{
		return codCcu;
	}

	/**
	 * Sets the codCcu value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param codCcu
	 */
	public void setCodCcu(java.lang.String codCcu)
	{
		this.codCcu = codCcu;
	}

	/**
	 * Gets the codCrp value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return codCrp
	 */
	public java.lang.String getCodCrp()
	{
		return codCrp;
	}

	/**
	 * Sets the codCrp value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param codCrp
	 */
	public void setCodCrp(java.lang.String codCrp)
	{
		this.codCrp = codCrp;
	}

	/**
	 * Gets the codCrt value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return codCrt
	 */
	public java.lang.String getCodCrt()
	{
		return codCrt;
	}

	/**
	 * Sets the codCrt value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param codCrt
	 */
	public void setCodCrt(java.lang.String codCrt)
	{
		this.codCrt = codCrt;
	}

	/**
	 * Gets the codFav value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return codFav
	 */
	public java.lang.Double getCodFav()
	{
		return codFav;
	}

	/**
	 * Sets the codFav value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param codFav
	 */
	public void setCodFav(java.lang.Double codFav)
	{
		this.codFav = codFav;
	}

	/**
	 * Gets the codFil value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return codFil
	 */
	public java.lang.Integer getCodFil()
	{
		return codFil;
	}

	/**
	 * Sets the codFil value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param codFil
	 */
	public void setCodFil(java.lang.Integer codFil)
	{
		this.codFil = codFil;
	}

	/**
	 * Gets the codFor value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return codFor
	 */
	public java.lang.Integer getCodFor()
	{
		return codFor;
	}

	/**
	 * Sets the codFor value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param codFor
	 */
	public void setCodFor(java.lang.Integer codFor)
	{
		this.codFor = codFor;
	}

	/**
	 * Gets the codFpg value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return codFpg
	 */
	public java.lang.Integer getCodFpg()
	{
		return codFpg;
	}

	/**
	 * Sets the codFpg value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param codFpg
	 */
	public void setCodFpg(java.lang.Integer codFpg)
	{
		this.codFpg = codFpg;
	}

	/**
	 * Gets the codFpj value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return codFpj
	 */
	public java.lang.Integer getCodFpj()
	{
		return codFpj;
	}

	/**
	 * Sets the codFpj value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param codFpj
	 */
	public void setCodFpj(java.lang.Integer codFpj)
	{
		this.codFpj = codFpj;
	}

	/**
	 * Gets the codMoe value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return codMoe
	 */
	public java.lang.String getCodMoe()
	{
		return codMoe;
	}

	/**
	 * Sets the codMoe value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param codMoe
	 */
	public void setCodMoe(java.lang.String codMoe)
	{
		this.codMoe = codMoe;
	}

	/**
	 * Gets the codMpt value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return codMpt
	 */
	public java.lang.String getCodMpt()
	{
		return codMpt;
	}

	/**
	 * Sets the codMpt value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param codMpt
	 */
	public void setCodMpt(java.lang.String codMpt)
	{
		this.codMpt = codMpt;
	}

	/**
	 * Gets the codNtg value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return codNtg
	 */
	public java.lang.Integer getCodNtg()
	{
		return codNtg;
	}

	/**
	 * Sets the codNtg value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param codNtg
	 */
	public void setCodNtg(java.lang.Integer codNtg)
	{
		this.codNtg = codNtg;
	}

	/**
	 * Gets the codPor value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return codPor
	 */
	public java.lang.String getCodPor()
	{
		return codPor;
	}

	/**
	 * Sets the codPor value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param codPor
	 */
	public void setCodPor(java.lang.String codPor)
	{
		this.codPor = codPor;
	}

	/**
	 * Gets the codTns value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return codTns
	 */
	public java.lang.String getCodTns()
	{
		return codTns;
	}

	/**
	 * Sets the codTns value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param codTns
	 */
	public void setCodTns(java.lang.String codTns)
	{
		this.codTns = codTns;
	}

	/**
	 * Gets the codTpt value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return codTpt
	 */
	public java.lang.String getCodTpt()
	{
		return codTpt;
	}

	/**
	 * Sets the codTpt value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param codTpt
	 */
	public void setCodTpt(java.lang.String codTpt)
	{
		this.codTpt = codTpt;
	}

	/**
	 * Gets the cotEmi value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return cotEmi
	 */
	public java.lang.Double getCotEmi()
	{
		return cotEmi;
	}

	/**
	 * Sets the cotEmi value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param cotEmi
	 */
	public void setCotEmi(java.lang.Double cotEmi)
	{
		this.cotEmi = cotEmi;
	}

	/**
	 * Gets the cotNeg value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return cotNeg
	 */
	public java.lang.Double getCotNeg()
	{
		return cotNeg;
	}

	/**
	 * Sets the cotNeg value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param cotNeg
	 */
	public void setCotNeg(java.lang.Double cotNeg)
	{
		this.cotNeg = cotNeg;
	}

	/**
	 * Gets the ctaFin value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return ctaFin
	 */
	public java.lang.Integer getCtaFin()
	{
		return ctaFin;
	}

	/**
	 * Sets the ctaFin value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param ctaFin
	 */
	public void setCtaFin(java.lang.Integer ctaFin)
	{
		this.ctaFin = ctaFin;
	}

	/**
	 * Gets the ctaRed value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return ctaRed
	 */
	public java.lang.Integer getCtaRed()
	{
		return ctaRed;
	}

	/**
	 * Sets the ctaRed value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param ctaRed
	 */
	public void setCtaRed(java.lang.Integer ctaRed)
	{
		this.ctaRed = ctaRed;
	}

	/**
	 * Gets the datDsc value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return datDsc
	 */
	public java.lang.String getDatDsc()
	{
		return datDsc;
	}

	/**
	 * Sets the datDsc value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param datDsc
	 */
	public void setDatDsc(java.lang.String datDsc)
	{
		this.datDsc = datDsc;
	}

	/**
	 * Gets the datEmi value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return datEmi
	 */
	public java.lang.String getDatEmi()
	{
		return datEmi;
	}

	/**
	 * Sets the datEmi value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param datEmi
	 */
	public void setDatEmi(java.lang.String datEmi)
	{
		this.datEmi = datEmi;
	}

	/**
	 * Gets the datEnt value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return datEnt
	 */
	public java.lang.String getDatEnt()
	{
		return datEnt;
	}

	/**
	 * Sets the datEnt value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param datEnt
	 */
	public void setDatEnt(java.lang.String datEnt)
	{
		this.datEnt = datEnt;
	}

	/**
	 * Gets the datNeg value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return datNeg
	 */
	public java.lang.String getDatNeg()
	{
		return datNeg;
	}

	/**
	 * Sets the datNeg value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param datNeg
	 */
	public void setDatNeg(java.lang.String datNeg)
	{
		this.datNeg = datNeg;
	}

	/**
	 * Gets the datPpt value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return datPpt
	 */
	public java.lang.String getDatPpt()
	{
		return datPpt;
	}

	/**
	 * Sets the datPpt value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param datPpt
	 */
	public void setDatPpt(java.lang.String datPpt)
	{
		this.datPpt = datPpt;
	}

	/**
	 * Gets the dscNeg value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return dscNeg
	 */
	public java.lang.Double getDscNeg()
	{
		return dscNeg;
	}

	/**
	 * Sets the dscNeg value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param dscNeg
	 */
	public void setDscNeg(java.lang.Double dscNeg)
	{
		this.dscNeg = dscNeg;
	}

	/**
	 * Gets the jrsDia value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return jrsDia
	 */
	public java.lang.Double getJrsDia()
	{
		return jrsDia;
	}

	/**
	 * Sets the jrsDia value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param jrsDia
	 */
	public void setJrsDia(java.lang.Double jrsDia)
	{
		this.jrsDia = jrsDia;
	}

	/**
	 * Gets the jrsNeg value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return jrsNeg
	 */
	public java.lang.Double getJrsNeg()
	{
		return jrsNeg;
	}

	/**
	 * Sets the jrsNeg value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param jrsNeg
	 */
	public void setJrsNeg(java.lang.Double jrsNeg)
	{
		this.jrsNeg = jrsNeg;
	}

	/**
	 * Gets the mulNeg value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return mulNeg
	 */
	public java.lang.Double getMulNeg()
	{
		return mulNeg;
	}

	/**
	 * Sets the mulNeg value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param mulNeg
	 */
	public void setMulNeg(java.lang.Double mulNeg)
	{
		this.mulNeg = mulNeg;
	}

	/**
	 * Gets the numPrj value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return numPrj
	 */
	public java.lang.Integer getNumPrj()
	{
		return numPrj;
	}

	/**
	 * Sets the numPrj value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param numPrj
	 */
	public void setNumPrj(java.lang.Integer numPrj)
	{
		this.numPrj = numPrj;
	}

	/**
	 * Gets the numTit value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return numTit
	 */
	public java.lang.String getNumTit()
	{
		return numTit;
	}

	/**
	 * Sets the numTit value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param numTit
	 */
	public void setNumTit(java.lang.String numTit)
	{
		this.numTit = numTit;
	}

	/**
	 * Gets the obsTcp value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return obsTcp
	 */
	public java.lang.String getObsTcp()
	{
		return obsTcp;
	}

	/**
	 * Sets the obsTcp value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param obsTcp
	 */
	public void setObsTcp(java.lang.String obsTcp)
	{
		this.obsTcp = obsTcp;
	}

	/**
	 * Gets the outNeg value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return outNeg
	 */
	public java.lang.Double getOutNeg()
	{
		return outNeg;
	}

	/**
	 * Sets the outNeg value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param outNeg
	 */
	public void setOutNeg(java.lang.Double outNeg)
	{
		this.outNeg = outNeg;
	}

	/**
	 * Gets the perDsc value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return perDsc
	 */
	public java.lang.Integer getPerDsc()
	{
		return perDsc;
	}

	/**
	 * Sets the perDsc value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param perDsc
	 */
	public void setPerDsc(java.lang.Integer perDsc)
	{
		this.perDsc = perDsc;
	}

	/**
	 * Gets the perJrs value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return perJrs
	 */
	public java.lang.Integer getPerJrs()
	{
		return perJrs;
	}

	/**
	 * Sets the perJrs value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param perJrs
	 */
	public void setPerJrs(java.lang.Integer perJrs)
	{
		this.perJrs = perJrs;
	}

	/**
	 * Gets the perMul value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return perMul
	 */
	public java.lang.Integer getPerMul()
	{
		return perMul;
	}

	/**
	 * Sets the perMul value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param perMul
	 */
	public void setPerMul(java.lang.Integer perMul)
	{
		this.perMul = perMul;
	}

	/**
	 * Gets the priPgt value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return priPgt
	 */
	public java.lang.Integer getPriPgt()
	{
		return priPgt;
	}

	/**
	 * Sets the priPgt value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param priPgt
	 */
	public void setPriPgt(java.lang.Integer priPgt)
	{
		this.priPgt = priPgt;
	}

	/**
	 * Gets the proJrs value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return proJrs
	 */
	public java.lang.String getProJrs()
	{
		return proJrs;
	}

	/**
	 * Sets the proJrs value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param proJrs
	 */
	public void setProJrs(java.lang.String proJrs)
	{
		this.proJrs = proJrs;
	}

	/**
	 * Gets the rateio value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return rateio
	 */
	public TitulosEntradaTitulosLoteCPInEntradaTitulosRateio[] getRateio()
	{
		return rateio;
	}

	/**
	 * Sets the rateio value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param rateioArray
	 */
	public void setRateio(TitulosEntradaTitulosLoteCPInEntradaTitulosRateio[] rateioArray)
	{
		this.rateio = rateioArray;
	}

	public TitulosEntradaTitulosLoteCPInEntradaTitulosRateio getRateio(int i)
	{
		return this.rateio[i];
	}

	public void setRateio(int i, TitulosEntradaTitulosLoteCPInEntradaTitulosRateio _value)
	{
		this.rateio[i] = _value;
	}

	/**
	 * Gets the tipJrs value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return tipJrs
	 */
	public java.lang.String getTipJrs()
	{
		return tipJrs;
	}

	/**
	 * Sets the tipJrs value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param tipJrs
	 */
	public void setTipJrs(java.lang.String tipJrs)
	{
		this.tipJrs = tipJrs;
	}

	/**
	 * Gets the tipTcc value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return tipTcc
	 */
	public java.lang.Integer getTipTcc()
	{
		return tipTcc;
	}

	/**
	 * Sets the tipTcc value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param tipTcc
	 */
	public void setTipTcc(java.lang.Integer tipTcc)
	{
		this.tipTcc = tipTcc;
	}

	/**
	 * Gets the titBan value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return titBan
	 */
	public java.lang.String getTitBan()
	{
		return titBan;
	}

	/**
	 * Sets the titBan value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param titBan
	 */
	public void setTitBan(java.lang.String titBan)
	{
		this.titBan = titBan;
	}

	/**
	 * Gets the tolDsc value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return tolDsc
	 */
	public java.lang.Integer getTolDsc()
	{
		return tolDsc;
	}

	/**
	 * Sets the tolDsc value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param tolDsc
	 */
	public void setTolDsc(java.lang.Integer tolDsc)
	{
		this.tolDsc = tolDsc;
	}

	/**
	 * Gets the tolJrs value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return tolJrs
	 */
	public java.lang.Integer getTolJrs()
	{
		return tolJrs;
	}

	/**
	 * Sets the tolJrs value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param tolJrs
	 */
	public void setTolJrs(java.lang.Integer tolJrs)
	{
		this.tolJrs = tolJrs;
	}

	/**
	 * Gets the tolMul value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return tolMul
	 */
	public java.lang.Integer getTolMul()
	{
		return tolMul;
	}

	/**
	 * Sets the tolMul value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param tolMul
	 */
	public void setTolMul(java.lang.Integer tolMul)
	{
		this.tolMul = tolMul;
	}

	/**
	 * Gets the vctOri value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return vctOri
	 */
	public java.lang.String getVctOri()
	{
		return vctOri;
	}

	/**
	 * Sets the vctOri value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param vctOri
	 */
	public void setVctOri(java.lang.String vctOri)
	{
		this.vctOri = vctOri;
	}

	/**
	 * Gets the vctPro value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return vctPro
	 */
	public java.lang.String getVctPro()
	{
		return vctPro;
	}

	/**
	 * Sets the vctPro value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param vctPro
	 */
	public void setVctPro(java.lang.String vctPro)
	{
		this.vctPro = vctPro;
	}

	/**
	 * Gets the vlrDsc value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return vlrDsc
	 */
	public java.lang.Double getVlrDsc()
	{
		return vlrDsc;
	}

	/**
	 * Sets the vlrDsc value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param vlrDsc
	 */
	public void setVlrDsc(java.lang.Double vlrDsc)
	{
		this.vlrDsc = vlrDsc;
	}

	/**
	 * Gets the vlrOri value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return vlrOri
	 */
	public java.lang.Double getVlrOri()
	{
		return vlrOri;
	}

	/**
	 * Sets the vlrOri value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param vlrOri
	 */
	public void setVlrOri(java.lang.Double vlrOri)
	{
		this.vlrOri = vlrOri;
	}

	/**
	 * Gets the vlrRba value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @return vlrRba
	 */
	public java.lang.Double getVlrRba()
	{
		return vlrRba;
	}

	/**
	 * Sets the vlrRba value for this TitulosEntradaTitulosLoteCPInEntradaTitulos.
	 * 
	 * @param vlrRba
	 */
	public void setVlrRba(java.lang.Double vlrRba)
	{
		this.vlrRba = vlrRba;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj)
	{
		if (!(obj instanceof TitulosEntradaTitulosLoteCPInEntradaTitulos))
			return false;
		TitulosEntradaTitulosLoteCPInEntradaTitulos other = (TitulosEntradaTitulosLoteCPInEntradaTitulos) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null)
		{
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true && ((this.antDsc == null && other.getAntDsc() == null) || (this.antDsc != null && this.antDsc.equals(other.getAntDsc()))) && ((this.ccbFor == null && other.getCcbFor() == null) || (this.ccbFor != null && this.ccbFor.equals(other.getCcbFor()))) && ((this.codAge == null && other.getCodAge() == null) || (this.codAge != null && this.codAge.equals(other.getCodAge())))
				&& ((this.codBan == null && other.getCodBan() == null) || (this.codBan != null && this.codBan.equals(other.getCodBan()))) && ((this.codCcu == null && other.getCodCcu() == null) || (this.codCcu != null && this.codCcu.equals(other.getCodCcu()))) && ((this.codCrp == null && other.getCodCrp() == null) || (this.codCrp != null && this.codCrp.equals(other.getCodCrp())))
				&& ((this.codCrt == null && other.getCodCrt() == null) || (this.codCrt != null && this.codCrt.equals(other.getCodCrt()))) && ((this.codFav == null && other.getCodFav() == null) || (this.codFav != null && this.codFav.equals(other.getCodFav()))) && ((this.codFil == null && other.getCodFil() == null) || (this.codFil != null && this.codFil.equals(other.getCodFil())))
				&& ((this.codFor == null && other.getCodFor() == null) || (this.codFor != null && this.codFor.equals(other.getCodFor()))) && ((this.codFpg == null && other.getCodFpg() == null) || (this.codFpg != null && this.codFpg.equals(other.getCodFpg()))) && ((this.codFpj == null && other.getCodFpj() == null) || (this.codFpj != null && this.codFpj.equals(other.getCodFpj())))
				&& ((this.codMoe == null && other.getCodMoe() == null) || (this.codMoe != null && this.codMoe.equals(other.getCodMoe()))) && ((this.codMpt == null && other.getCodMpt() == null) || (this.codMpt != null && this.codMpt.equals(other.getCodMpt()))) && ((this.codNtg == null && other.getCodNtg() == null) || (this.codNtg != null && this.codNtg.equals(other.getCodNtg())))
				&& ((this.codPor == null && other.getCodPor() == null) || (this.codPor != null && this.codPor.equals(other.getCodPor()))) && ((this.codTns == null && other.getCodTns() == null) || (this.codTns != null && this.codTns.equals(other.getCodTns()))) && ((this.codTpt == null && other.getCodTpt() == null) || (this.codTpt != null && this.codTpt.equals(other.getCodTpt())))
				&& ((this.cotEmi == null && other.getCotEmi() == null) || (this.cotEmi != null && this.cotEmi.equals(other.getCotEmi()))) && ((this.cotNeg == null && other.getCotNeg() == null) || (this.cotNeg != null && this.cotNeg.equals(other.getCotNeg()))) && ((this.ctaFin == null && other.getCtaFin() == null) || (this.ctaFin != null && this.ctaFin.equals(other.getCtaFin())))
				&& ((this.ctaRed == null && other.getCtaRed() == null) || (this.ctaRed != null && this.ctaRed.equals(other.getCtaRed()))) && ((this.datDsc == null && other.getDatDsc() == null) || (this.datDsc != null && this.datDsc.equals(other.getDatDsc()))) && ((this.datEmi == null && other.getDatEmi() == null) || (this.datEmi != null && this.datEmi.equals(other.getDatEmi())))
				&& ((this.datEnt == null && other.getDatEnt() == null) || (this.datEnt != null && this.datEnt.equals(other.getDatEnt()))) && ((this.datNeg == null && other.getDatNeg() == null) || (this.datNeg != null && this.datNeg.equals(other.getDatNeg()))) && ((this.datPpt == null && other.getDatPpt() == null) || (this.datPpt != null && this.datPpt.equals(other.getDatPpt())))
				&& ((this.dscNeg == null && other.getDscNeg() == null) || (this.dscNeg != null && this.dscNeg.equals(other.getDscNeg()))) && ((this.jrsDia == null && other.getJrsDia() == null) || (this.jrsDia != null && this.jrsDia.equals(other.getJrsDia()))) && ((this.jrsNeg == null && other.getJrsNeg() == null) || (this.jrsNeg != null && this.jrsNeg.equals(other.getJrsNeg())))
				&& ((this.mulNeg == null && other.getMulNeg() == null) || (this.mulNeg != null && this.mulNeg.equals(other.getMulNeg()))) && ((this.numPrj == null && other.getNumPrj() == null) || (this.numPrj != null && this.numPrj.equals(other.getNumPrj()))) && ((this.numTit == null && other.getNumTit() == null) || (this.numTit != null && this.numTit.equals(other.getNumTit())))
				&& ((this.obsTcp == null && other.getObsTcp() == null) || (this.obsTcp != null && this.obsTcp.equals(other.getObsTcp()))) && ((this.outNeg == null && other.getOutNeg() == null) || (this.outNeg != null && this.outNeg.equals(other.getOutNeg()))) && ((this.perDsc == null && other.getPerDsc() == null) || (this.perDsc != null && this.perDsc.equals(other.getPerDsc())))
				&& ((this.perJrs == null && other.getPerJrs() == null) || (this.perJrs != null && this.perJrs.equals(other.getPerJrs()))) && ((this.perMul == null && other.getPerMul() == null) || (this.perMul != null && this.perMul.equals(other.getPerMul()))) && ((this.priPgt == null && other.getPriPgt() == null) || (this.priPgt != null && this.priPgt.equals(other.getPriPgt())))
				&& ((this.proJrs == null && other.getProJrs() == null) || (this.proJrs != null && this.proJrs.equals(other.getProJrs()))) && ((this.rateio == null && other.getRateio() == null) || (this.rateio != null && java.util.Arrays.equals(this.rateio, other.getRateio()))) && ((this.tipJrs == null && other.getTipJrs() == null) || (this.tipJrs != null && this.tipJrs.equals(other.getTipJrs())))
				&& ((this.tipTcc == null && other.getTipTcc() == null) || (this.tipTcc != null && this.tipTcc.equals(other.getTipTcc()))) && ((this.titBan == null && other.getTitBan() == null) || (this.titBan != null && this.titBan.equals(other.getTitBan()))) && ((this.tolDsc == null && other.getTolDsc() == null) || (this.tolDsc != null && this.tolDsc.equals(other.getTolDsc())))
				&& ((this.tolJrs == null && other.getTolJrs() == null) || (this.tolJrs != null && this.tolJrs.equals(other.getTolJrs()))) && ((this.tolMul == null && other.getTolMul() == null) || (this.tolMul != null && this.tolMul.equals(other.getTolMul()))) && ((this.vctOri == null && other.getVctOri() == null) || (this.vctOri != null && this.vctOri.equals(other.getVctOri())))
				&& ((this.vctPro == null && other.getVctPro() == null) || (this.vctPro != null && this.vctPro.equals(other.getVctPro()))) && ((this.vlrDsc == null && other.getVlrDsc() == null) || (this.vlrDsc != null && this.vlrDsc.equals(other.getVlrDsc()))) && ((this.vlrOri == null && other.getVlrOri() == null) || (this.vlrOri != null && this.vlrOri.equals(other.getVlrOri())))
				&& ((this.vlrRba == null && other.getVlrRba() == null) || (this.vlrRba != null && this.vlrRba.equals(other.getVlrRba())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode()
	{
		if (__hashCodeCalc)
		{
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getAntDsc() != null)
		{
			_hashCode += getAntDsc().hashCode();
		}
		if (getCcbFor() != null)
		{
			_hashCode += getCcbFor().hashCode();
		}
		if (getCodAge() != null)
		{
			_hashCode += getCodAge().hashCode();
		}
		if (getCodBan() != null)
		{
			_hashCode += getCodBan().hashCode();
		}
		if (getCodCcu() != null)
		{
			_hashCode += getCodCcu().hashCode();
		}
		if (getCodCrp() != null)
		{
			_hashCode += getCodCrp().hashCode();
		}
		if (getCodCrt() != null)
		{
			_hashCode += getCodCrt().hashCode();
		}
		if (getCodFav() != null)
		{
			_hashCode += getCodFav().hashCode();
		}
		if (getCodFil() != null)
		{
			_hashCode += getCodFil().hashCode();
		}
		if (getCodFor() != null)
		{
			_hashCode += getCodFor().hashCode();
		}
		if (getCodFpg() != null)
		{
			_hashCode += getCodFpg().hashCode();
		}
		if (getCodFpj() != null)
		{
			_hashCode += getCodFpj().hashCode();
		}
		if (getCodMoe() != null)
		{
			_hashCode += getCodMoe().hashCode();
		}
		if (getCodMpt() != null)
		{
			_hashCode += getCodMpt().hashCode();
		}
		if (getCodNtg() != null)
		{
			_hashCode += getCodNtg().hashCode();
		}
		if (getCodPor() != null)
		{
			_hashCode += getCodPor().hashCode();
		}
		if (getCodTns() != null)
		{
			_hashCode += getCodTns().hashCode();
		}
		if (getCodTpt() != null)
		{
			_hashCode += getCodTpt().hashCode();
		}
		if (getCotEmi() != null)
		{
			_hashCode += getCotEmi().hashCode();
		}
		if (getCotNeg() != null)
		{
			_hashCode += getCotNeg().hashCode();
		}
		if (getCtaFin() != null)
		{
			_hashCode += getCtaFin().hashCode();
		}
		if (getCtaRed() != null)
		{
			_hashCode += getCtaRed().hashCode();
		}
		if (getDatDsc() != null)
		{
			_hashCode += getDatDsc().hashCode();
		}
		if (getDatEmi() != null)
		{
			_hashCode += getDatEmi().hashCode();
		}
		if (getDatEnt() != null)
		{
			_hashCode += getDatEnt().hashCode();
		}
		if (getDatNeg() != null)
		{
			_hashCode += getDatNeg().hashCode();
		}
		if (getDatPpt() != null)
		{
			_hashCode += getDatPpt().hashCode();
		}
		if (getDscNeg() != null)
		{
			_hashCode += getDscNeg().hashCode();
		}
		if (getJrsDia() != null)
		{
			_hashCode += getJrsDia().hashCode();
		}
		if (getJrsNeg() != null)
		{
			_hashCode += getJrsNeg().hashCode();
		}
		if (getMulNeg() != null)
		{
			_hashCode += getMulNeg().hashCode();
		}
		if (getNumPrj() != null)
		{
			_hashCode += getNumPrj().hashCode();
		}
		if (getNumTit() != null)
		{
			_hashCode += getNumTit().hashCode();
		}
		if (getObsTcp() != null)
		{
			_hashCode += getObsTcp().hashCode();
		}
		if (getOutNeg() != null)
		{
			_hashCode += getOutNeg().hashCode();
		}
		if (getPerDsc() != null)
		{
			_hashCode += getPerDsc().hashCode();
		}
		if (getPerJrs() != null)
		{
			_hashCode += getPerJrs().hashCode();
		}
		if (getPerMul() != null)
		{
			_hashCode += getPerMul().hashCode();
		}
		if (getPriPgt() != null)
		{
			_hashCode += getPriPgt().hashCode();
		}
		if (getProJrs() != null)
		{
			_hashCode += getProJrs().hashCode();
		}
		if (getRateio() != null)
		{
			for (int i = 0; i < java.lang.reflect.Array.getLength(getRateio()); i++)
			{
				java.lang.Object obj = java.lang.reflect.Array.get(getRateio(), i);
				if (obj != null && !obj.getClass().isArray())
				{
					_hashCode += obj.hashCode();
				}
			}
		}
		if (getTipJrs() != null)
		{
			_hashCode += getTipJrs().hashCode();
		}
		if (getTipTcc() != null)
		{
			_hashCode += getTipTcc().hashCode();
		}
		if (getTitBan() != null)
		{
			_hashCode += getTitBan().hashCode();
		}
		if (getTolDsc() != null)
		{
			_hashCode += getTolDsc().hashCode();
		}
		if (getTolJrs() != null)
		{
			_hashCode += getTolJrs().hashCode();
		}
		if (getTolMul() != null)
		{
			_hashCode += getTolMul().hashCode();
		}
		if (getVctOri() != null)
		{
			_hashCode += getVctOri().hashCode();
		}
		if (getVctPro() != null)
		{
			_hashCode += getVctPro().hashCode();
		}
		if (getVlrDsc() != null)
		{
			_hashCode += getVlrDsc().hashCode();
		}
		if (getVlrOri() != null)
		{
			_hashCode += getVlrOri().hashCode();
		}
		if (getVlrRba() != null)
		{
			_hashCode += getVlrRba().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(TitulosEntradaTitulosLoteCPInEntradaTitulos.class, true);

	static
	{
		typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosEntradaTitulosLoteCPInEntradaTitulos"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("antDsc");
		elemField.setXmlName(new javax.xml.namespace.QName("", "antDsc"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ccbFor");
		elemField.setXmlName(new javax.xml.namespace.QName("", "ccbFor"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("codAge");
		elemField.setXmlName(new javax.xml.namespace.QName("", "codAge"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("codBan");
		elemField.setXmlName(new javax.xml.namespace.QName("", "codBan"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("codCcu");
		elemField.setXmlName(new javax.xml.namespace.QName("", "codCcu"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("codCrp");
		elemField.setXmlName(new javax.xml.namespace.QName("", "codCrp"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("codCrt");
		elemField.setXmlName(new javax.xml.namespace.QName("", "codCrt"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("codFav");
		elemField.setXmlName(new javax.xml.namespace.QName("", "codFav"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("codFil");
		elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("codFor");
		elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("codFpg");
		elemField.setXmlName(new javax.xml.namespace.QName("", "codFpg"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("codFpj");
		elemField.setXmlName(new javax.xml.namespace.QName("", "codFpj"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("codMoe");
		elemField.setXmlName(new javax.xml.namespace.QName("", "codMoe"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("codMpt");
		elemField.setXmlName(new javax.xml.namespace.QName("", "codMpt"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("codNtg");
		elemField.setXmlName(new javax.xml.namespace.QName("", "codNtg"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("codPor");
		elemField.setXmlName(new javax.xml.namespace.QName("", "codPor"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("codTns");
		elemField.setXmlName(new javax.xml.namespace.QName("", "codTns"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("codTpt");
		elemField.setXmlName(new javax.xml.namespace.QName("", "codTpt"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("cotEmi");
		elemField.setXmlName(new javax.xml.namespace.QName("", "cotEmi"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("cotNeg");
		elemField.setXmlName(new javax.xml.namespace.QName("", "cotNeg"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ctaFin");
		elemField.setXmlName(new javax.xml.namespace.QName("", "ctaFin"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("ctaRed");
		elemField.setXmlName(new javax.xml.namespace.QName("", "ctaRed"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("datDsc");
		elemField.setXmlName(new javax.xml.namespace.QName("", "datDsc"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("datEmi");
		elemField.setXmlName(new javax.xml.namespace.QName("", "datEmi"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("datEnt");
		elemField.setXmlName(new javax.xml.namespace.QName("", "datEnt"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("datNeg");
		elemField.setXmlName(new javax.xml.namespace.QName("", "datNeg"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("datPpt");
		elemField.setXmlName(new javax.xml.namespace.QName("", "datPpt"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("dscNeg");
		elemField.setXmlName(new javax.xml.namespace.QName("", "dscNeg"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("jrsDia");
		elemField.setXmlName(new javax.xml.namespace.QName("", "jrsDia"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("jrsNeg");
		elemField.setXmlName(new javax.xml.namespace.QName("", "jrsNeg"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("mulNeg");
		elemField.setXmlName(new javax.xml.namespace.QName("", "mulNeg"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("numPrj");
		elemField.setXmlName(new javax.xml.namespace.QName("", "numPrj"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("numTit");
		elemField.setXmlName(new javax.xml.namespace.QName("", "numTit"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("obsTcp");
		elemField.setXmlName(new javax.xml.namespace.QName("", "obsTcp"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("outNeg");
		elemField.setXmlName(new javax.xml.namespace.QName("", "outNeg"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("perDsc");
		elemField.setXmlName(new javax.xml.namespace.QName("", "perDsc"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("perJrs");
		elemField.setXmlName(new javax.xml.namespace.QName("", "perJrs"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("perMul");
		elemField.setXmlName(new javax.xml.namespace.QName("", "perMul"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("priPgt");
		elemField.setXmlName(new javax.xml.namespace.QName("", "priPgt"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("proJrs");
		elemField.setXmlName(new javax.xml.namespace.QName("", "proJrs"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("rateio");
		elemField.setXmlName(new javax.xml.namespace.QName("", "rateio"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosEntradaTitulosLoteCPInEntradaTitulosRateio"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		elemField.setMaxOccursUnbounded(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("tipJrs");
		elemField.setXmlName(new javax.xml.namespace.QName("", "tipJrs"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("tipTcc");
		elemField.setXmlName(new javax.xml.namespace.QName("", "tipTcc"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("titBan");
		elemField.setXmlName(new javax.xml.namespace.QName("", "titBan"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("tolDsc");
		elemField.setXmlName(new javax.xml.namespace.QName("", "tolDsc"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("tolJrs");
		elemField.setXmlName(new javax.xml.namespace.QName("", "tolJrs"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("tolMul");
		elemField.setXmlName(new javax.xml.namespace.QName("", "tolMul"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("vctOri");
		elemField.setXmlName(new javax.xml.namespace.QName("", "vctOri"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("vctPro");
		elemField.setXmlName(new javax.xml.namespace.QName("", "vctPro"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("vlrDsc");
		elemField.setXmlName(new javax.xml.namespace.QName("", "vlrDsc"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("vlrOri");
		elemField.setXmlName(new javax.xml.namespace.QName("", "vlrOri"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("vlrRba");
		elemField.setXmlName(new javax.xml.namespace.QName("", "vlrRba"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc()
	{
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType, java.lang.Class _javaType, javax.xml.namespace.QName _xmlType)
	{
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType, java.lang.Class _javaType, javax.xml.namespace.QName _xmlType)
	{
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
