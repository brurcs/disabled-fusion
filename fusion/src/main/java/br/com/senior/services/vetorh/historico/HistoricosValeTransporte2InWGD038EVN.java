/**
 * HistoricosValeTransporte2InWGD038EVN.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.historico;

public class HistoricosValeTransporte2InWGD038EVN  implements java.io.Serializable {
    private java.lang.Integer codLin;

    private java.lang.String numCar;

    private java.lang.String tipOpe;

    public HistoricosValeTransporte2InWGD038EVN() {
    }

    public HistoricosValeTransporte2InWGD038EVN(
           java.lang.Integer codLin,
           java.lang.String numCar,
           java.lang.String tipOpe) {
           this.codLin = codLin;
           this.numCar = numCar;
           this.tipOpe = tipOpe;
    }


    /**
     * Gets the codLin value for this HistoricosValeTransporte2InWGD038EVN.
     * 
     * @return codLin
     */
    public java.lang.Integer getCodLin() {
        return codLin;
    }


    /**
     * Sets the codLin value for this HistoricosValeTransporte2InWGD038EVN.
     * 
     * @param codLin
     */
    public void setCodLin(java.lang.Integer codLin) {
        this.codLin = codLin;
    }


    /**
     * Gets the numCar value for this HistoricosValeTransporte2InWGD038EVN.
     * 
     * @return numCar
     */
    public java.lang.String getNumCar() {
        return numCar;
    }


    /**
     * Sets the numCar value for this HistoricosValeTransporte2InWGD038EVN.
     * 
     * @param numCar
     */
    public void setNumCar(java.lang.String numCar) {
        this.numCar = numCar;
    }


    /**
     * Gets the tipOpe value for this HistoricosValeTransporte2InWGD038EVN.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this HistoricosValeTransporte2InWGD038EVN.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HistoricosValeTransporte2InWGD038EVN)) return false;
        HistoricosValeTransporte2InWGD038EVN other = (HistoricosValeTransporte2InWGD038EVN) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codLin==null && other.getCodLin()==null) || 
             (this.codLin!=null &&
              this.codLin.equals(other.getCodLin()))) &&
            ((this.numCar==null && other.getNumCar()==null) || 
             (this.numCar!=null &&
              this.numCar.equals(other.getNumCar()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodLin() != null) {
            _hashCode += getCodLin().hashCode();
        }
        if (getNumCar() != null) {
            _hashCode += getNumCar().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HistoricosValeTransporte2InWGD038EVN.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "historicosValeTransporte2InWGD038EVN"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codLin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codLin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
