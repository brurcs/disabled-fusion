/**
 * G5SeniorServicesLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.gtc;

import br.com.senior.services.vetorh.constants.WSConfigs;


public class G5SeniorServicesLocator extends org.apache.axis.client.Service implements br.com.senior.services.gtc.G5SeniorServices {

    public G5SeniorServicesLocator() {
    }


    public G5SeniorServicesLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public G5SeniorServicesLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for sapiens_Synccom_senior_g5_co_mfi_cpa_titulosPort
    private java.lang.String sapiens_Synccom_senior_g5_co_mfi_cpa_titulosPort_address = "http://"+(new WSConfigs()).getHostService()+"/g5-senior-services/sapiens_Synccom_senior_g5_co_mfi_cpa_titulos";

    public java.lang.String getsapiens_Synccom_senior_g5_co_mfi_cpa_titulosPortAddress() {
        return sapiens_Synccom_senior_g5_co_mfi_cpa_titulosPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String sapiens_Synccom_senior_g5_co_mfi_cpa_titulosPortWSDDServiceName = "sapiens_Synccom_senior_g5_co_mfi_cpa_titulosPort";

    public java.lang.String getsapiens_Synccom_senior_g5_co_mfi_cpa_titulosPortWSDDServiceName() {
        return sapiens_Synccom_senior_g5_co_mfi_cpa_titulosPortWSDDServiceName;
    }

    public void setsapiens_Synccom_senior_g5_co_mfi_cpa_titulosPortWSDDServiceName(java.lang.String name) {
        sapiens_Synccom_senior_g5_co_mfi_cpa_titulosPortWSDDServiceName = name;
    }

    public br.com.senior.services.gtc.Sapiens_Synccom_senior_g5_co_mfi_cpa_titulos getsapiens_Synccom_senior_g5_co_mfi_cpa_titulosPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(sapiens_Synccom_senior_g5_co_mfi_cpa_titulosPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getsapiens_Synccom_senior_g5_co_mfi_cpa_titulosPort(endpoint);
    }

    public br.com.senior.services.gtc.Sapiens_Synccom_senior_g5_co_mfi_cpa_titulos getsapiens_Synccom_senior_g5_co_mfi_cpa_titulosPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            br.com.senior.services.gtc.Sapiens_Synccom_senior_g5_co_mfi_cpa_titulosPortBindingStub _stub = new br.com.senior.services.gtc.Sapiens_Synccom_senior_g5_co_mfi_cpa_titulosPortBindingStub(portAddress, this);
            _stub.setPortName(getsapiens_Synccom_senior_g5_co_mfi_cpa_titulosPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setsapiens_Synccom_senior_g5_co_mfi_cpa_titulosPortEndpointAddress(java.lang.String address) {
        sapiens_Synccom_senior_g5_co_mfi_cpa_titulosPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (br.com.senior.services.gtc.Sapiens_Synccom_senior_g5_co_mfi_cpa_titulos.class.isAssignableFrom(serviceEndpointInterface)) {
                br.com.senior.services.gtc.Sapiens_Synccom_senior_g5_co_mfi_cpa_titulosPortBindingStub _stub = new br.com.senior.services.gtc.Sapiens_Synccom_senior_g5_co_mfi_cpa_titulosPortBindingStub(new java.net.URL(sapiens_Synccom_senior_g5_co_mfi_cpa_titulosPort_address), this);
                _stub.setPortName(getsapiens_Synccom_senior_g5_co_mfi_cpa_titulosPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("sapiens_Synccom_senior_g5_co_mfi_cpa_titulosPort".equals(inputPortName)) {
            return getsapiens_Synccom_senior_g5_co_mfi_cpa_titulosPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://services.senior.com.br", "g5-senior-services");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://services.senior.com.br", "sapiens_Synccom_senior_g5_co_mfi_cpa_titulosPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("sapiens_Synccom_senior_g5_co_mfi_cpa_titulosPort".equals(portName)) {
            setsapiens_Synccom_senior_g5_co_mfi_cpa_titulosPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
