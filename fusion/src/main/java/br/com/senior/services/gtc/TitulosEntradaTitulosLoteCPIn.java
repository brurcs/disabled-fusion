/**
 * TitulosEntradaTitulosLoteCPIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.gtc;

public class TitulosEntradaTitulosLoteCPIn implements java.io.Serializable
{
	private java.lang.Integer codEmp;

	private java.lang.Integer codFil;

	private TitulosEntradaTitulosLoteCPInEntradaTitulos[] entradaTitulos;

	private java.lang.String flowInstanceID;

	private java.lang.String flowName;

	private java.lang.String forCli;

	public TitulosEntradaTitulosLoteCPIn()
	{
	}

	public TitulosEntradaTitulosLoteCPIn(java.lang.Integer codEmp, java.lang.Integer codFil, TitulosEntradaTitulosLoteCPInEntradaTitulos[] entradaTitulos, java.lang.String flowInstanceID, java.lang.String flowName, java.lang.String forCli)
	{
		this.codEmp = codEmp;
		this.codFil = codFil;
		this.entradaTitulos = entradaTitulos;
		this.flowInstanceID = flowInstanceID;
		this.flowName = flowName;
		this.forCli = forCli;
	}

	/**
	 * Gets the codEmp value for this TitulosEntradaTitulosLoteCPIn.
	 * 
	 * @return codEmp
	 */
	public java.lang.Integer getCodEmp()
	{
		return codEmp;
	}

	/**
	 * Sets the codEmp value for this TitulosEntradaTitulosLoteCPIn.
	 * 
	 * @param codEmp
	 */
	public void setCodEmp(java.lang.Integer codEmp)
	{
		this.codEmp = codEmp;
	}

	/**
	 * Gets the codFil value for this TitulosEntradaTitulosLoteCPIn.
	 * 
	 * @return codFil
	 */
	public java.lang.Integer getCodFil()
	{
		return codFil;
	}

	/**
	 * Sets the codFil value for this TitulosEntradaTitulosLoteCPIn.
	 * 
	 * @param codFil
	 */
	public void setCodFil(java.lang.Integer codFil)
	{
		this.codFil = codFil;
	}

	/**
	 * Gets the entradaTitulos value for this TitulosEntradaTitulosLoteCPIn.
	 * 
	 * @return entradaTitulos
	 */
	public TitulosEntradaTitulosLoteCPInEntradaTitulos[] getEntradaTitulos()
	{
		return entradaTitulos;
	}

	/**
	 * Sets the entradaTitulos value for this TitulosEntradaTitulosLoteCPIn.
	 * 
	 * @param tituloEntradaArray
	 */
	public void setEntradaTitulos(TitulosEntradaTitulosLoteCPInEntradaTitulos[] tituloEntradaArray)
	{
		this.entradaTitulos = tituloEntradaArray;
	}

	public TitulosEntradaTitulosLoteCPInEntradaTitulos getEntradaTitulos(int i)
	{
		return this.entradaTitulos[i];
	}

	public void setEntradaTitulos(int i, TitulosEntradaTitulosLoteCPInEntradaTitulos _value)
	{
		this.entradaTitulos[i] = _value;
	}

	/**
	 * Gets the flowInstanceID value for this TitulosEntradaTitulosLoteCPIn.
	 * 
	 * @return flowInstanceID
	 */
	public java.lang.String getFlowInstanceID()
	{
		return flowInstanceID;
	}

	/**
	 * Sets the flowInstanceID value for this TitulosEntradaTitulosLoteCPIn.
	 * 
	 * @param flowInstanceID
	 */
	public void setFlowInstanceID(java.lang.String flowInstanceID)
	{
		this.flowInstanceID = flowInstanceID;
	}

	/**
	 * Gets the flowName value for this TitulosEntradaTitulosLoteCPIn.
	 * 
	 * @return flowName
	 */
	public java.lang.String getFlowName()
	{
		return flowName;
	}

	/**
	 * Sets the flowName value for this TitulosEntradaTitulosLoteCPIn.
	 * 
	 * @param flowName
	 */
	public void setFlowName(java.lang.String flowName)
	{
		this.flowName = flowName;
	}

	/**
	 * Gets the forCli value for this TitulosEntradaTitulosLoteCPIn.
	 * 
	 * @return forCli
	 */
	public java.lang.String getForCli()
	{
		return forCli;
	}

	/**
	 * Sets the forCli value for this TitulosEntradaTitulosLoteCPIn.
	 * 
	 * @param forCli
	 */
	public void setForCli(java.lang.String forCli)
	{
		this.forCli = forCli;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj)
	{
		if (!(obj instanceof TitulosEntradaTitulosLoteCPIn))
			return false;
		TitulosEntradaTitulosLoteCPIn other = (TitulosEntradaTitulosLoteCPIn) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null)
		{
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true && ((this.codEmp == null && other.getCodEmp() == null) || (this.codEmp != null && this.codEmp.equals(other.getCodEmp()))) && ((this.codFil == null && other.getCodFil() == null) || (this.codFil != null && this.codFil.equals(other.getCodFil()))) && ((this.entradaTitulos == null && other.getEntradaTitulos() == null) || (this.entradaTitulos != null && java.util.Arrays.equals(this.entradaTitulos, other.getEntradaTitulos())))
				&& ((this.flowInstanceID == null && other.getFlowInstanceID() == null) || (this.flowInstanceID != null && this.flowInstanceID.equals(other.getFlowInstanceID()))) && ((this.flowName == null && other.getFlowName() == null) || (this.flowName != null && this.flowName.equals(other.getFlowName()))) && ((this.forCli == null && other.getForCli() == null) || (this.forCli != null && this.forCli.equals(other.getForCli())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode()
	{
		if (__hashCodeCalc)
		{
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getCodEmp() != null)
		{
			_hashCode += getCodEmp().hashCode();
		}
		if (getCodFil() != null)
		{
			_hashCode += getCodFil().hashCode();
		}
		if (getEntradaTitulos() != null)
		{
			for (int i = 0; i < java.lang.reflect.Array.getLength(getEntradaTitulos()); i++)
			{
				java.lang.Object obj = java.lang.reflect.Array.get(getEntradaTitulos(), i);
				if (obj != null && !obj.getClass().isArray())
				{
					_hashCode += obj.hashCode();
				}
			}
		}
		if (getFlowInstanceID() != null)
		{
			_hashCode += getFlowInstanceID().hashCode();
		}
		if (getFlowName() != null)
		{
			_hashCode += getFlowName().hashCode();
		}
		if (getForCli() != null)
		{
			_hashCode += getForCli().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(TitulosEntradaTitulosLoteCPIn.class, true);

	static
	{
		typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosEntradaTitulosLoteCPIn"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("codEmp");
		elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("codFil");
		elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("entradaTitulos");
		elemField.setXmlName(new javax.xml.namespace.QName("", "entradaTitulos"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosEntradaTitulosLoteCPInEntradaTitulos"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		elemField.setMaxOccursUnbounded(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("flowInstanceID");
		elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("flowName");
		elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("forCli");
		elemField.setXmlName(new javax.xml.namespace.QName("", "forCli"));
		elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(true);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc()
	{
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType, java.lang.Class _javaType, javax.xml.namespace.QName _xmlType)
	{
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType, java.lang.Class _javaType, javax.xml.namespace.QName _xmlType)
	{
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
	}

}
