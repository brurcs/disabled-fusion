/**
 * HistoricosHorarioIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.historico;

public class HistoricosHorarioIn  implements java.io.Serializable {
    private java.lang.String datAlt;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.Integer horBas;

    private java.lang.String horDsr;

    private java.lang.Integer horSab;

    private java.lang.String horSem;

    private java.lang.Integer numCad;

    private java.lang.Integer numEmp;

    private java.lang.Integer tipCol;

    private java.lang.String tipOpe;

    public HistoricosHorarioIn() {
    }

    public HistoricosHorarioIn(
           java.lang.String datAlt,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.Integer horBas,
           java.lang.String horDsr,
           java.lang.Integer horSab,
           java.lang.String horSem,
           java.lang.Integer numCad,
           java.lang.Integer numEmp,
           java.lang.Integer tipCol,
           java.lang.String tipOpe) {
           this.datAlt = datAlt;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.horBas = horBas;
           this.horDsr = horDsr;
           this.horSab = horSab;
           this.horSem = horSem;
           this.numCad = numCad;
           this.numEmp = numEmp;
           this.tipCol = tipCol;
           this.tipOpe = tipOpe;
    }


    /**
     * Gets the datAlt value for this HistoricosHorarioIn.
     * 
     * @return datAlt
     */
    public java.lang.String getDatAlt() {
        return datAlt;
    }


    /**
     * Sets the datAlt value for this HistoricosHorarioIn.
     * 
     * @param datAlt
     */
    public void setDatAlt(java.lang.String datAlt) {
        this.datAlt = datAlt;
    }


    /**
     * Gets the flowInstanceID value for this HistoricosHorarioIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this HistoricosHorarioIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this HistoricosHorarioIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this HistoricosHorarioIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the horBas value for this HistoricosHorarioIn.
     * 
     * @return horBas
     */
    public java.lang.Integer getHorBas() {
        return horBas;
    }


    /**
     * Sets the horBas value for this HistoricosHorarioIn.
     * 
     * @param horBas
     */
    public void setHorBas(java.lang.Integer horBas) {
        this.horBas = horBas;
    }


    /**
     * Gets the horDsr value for this HistoricosHorarioIn.
     * 
     * @return horDsr
     */
    public java.lang.String getHorDsr() {
        return horDsr;
    }


    /**
     * Sets the horDsr value for this HistoricosHorarioIn.
     * 
     * @param horDsr
     */
    public void setHorDsr(java.lang.String horDsr) {
        this.horDsr = horDsr;
    }


    /**
     * Gets the horSab value for this HistoricosHorarioIn.
     * 
     * @return horSab
     */
    public java.lang.Integer getHorSab() {
        return horSab;
    }


    /**
     * Sets the horSab value for this HistoricosHorarioIn.
     * 
     * @param horSab
     */
    public void setHorSab(java.lang.Integer horSab) {
        this.horSab = horSab;
    }


    /**
     * Gets the horSem value for this HistoricosHorarioIn.
     * 
     * @return horSem
     */
    public java.lang.String getHorSem() {
        return horSem;
    }


    /**
     * Sets the horSem value for this HistoricosHorarioIn.
     * 
     * @param horSem
     */
    public void setHorSem(java.lang.String horSem) {
        this.horSem = horSem;
    }


    /**
     * Gets the numCad value for this HistoricosHorarioIn.
     * 
     * @return numCad
     */
    public java.lang.Integer getNumCad() {
        return numCad;
    }


    /**
     * Sets the numCad value for this HistoricosHorarioIn.
     * 
     * @param numCad
     */
    public void setNumCad(java.lang.Integer numCad) {
        this.numCad = numCad;
    }


    /**
     * Gets the numEmp value for this HistoricosHorarioIn.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this HistoricosHorarioIn.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the tipCol value for this HistoricosHorarioIn.
     * 
     * @return tipCol
     */
    public java.lang.Integer getTipCol() {
        return tipCol;
    }


    /**
     * Sets the tipCol value for this HistoricosHorarioIn.
     * 
     * @param tipCol
     */
    public void setTipCol(java.lang.Integer tipCol) {
        this.tipCol = tipCol;
    }


    /**
     * Gets the tipOpe value for this HistoricosHorarioIn.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this HistoricosHorarioIn.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HistoricosHorarioIn)) return false;
        HistoricosHorarioIn other = (HistoricosHorarioIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.datAlt==null && other.getDatAlt()==null) || 
             (this.datAlt!=null &&
              this.datAlt.equals(other.getDatAlt()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.horBas==null && other.getHorBas()==null) || 
             (this.horBas!=null &&
              this.horBas.equals(other.getHorBas()))) &&
            ((this.horDsr==null && other.getHorDsr()==null) || 
             (this.horDsr!=null &&
              this.horDsr.equals(other.getHorDsr()))) &&
            ((this.horSab==null && other.getHorSab()==null) || 
             (this.horSab!=null &&
              this.horSab.equals(other.getHorSab()))) &&
            ((this.horSem==null && other.getHorSem()==null) || 
             (this.horSem!=null &&
              this.horSem.equals(other.getHorSem()))) &&
            ((this.numCad==null && other.getNumCad()==null) || 
             (this.numCad!=null &&
              this.numCad.equals(other.getNumCad()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.tipCol==null && other.getTipCol()==null) || 
             (this.tipCol!=null &&
              this.tipCol.equals(other.getTipCol()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDatAlt() != null) {
            _hashCode += getDatAlt().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getHorBas() != null) {
            _hashCode += getHorBas().hashCode();
        }
        if (getHorDsr() != null) {
            _hashCode += getHorDsr().hashCode();
        }
        if (getHorSab() != null) {
            _hashCode += getHorSab().hashCode();
        }
        if (getHorSem() != null) {
            _hashCode += getHorSem().hashCode();
        }
        if (getNumCad() != null) {
            _hashCode += getNumCad().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getTipCol() != null) {
            _hashCode += getTipCol().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HistoricosHorarioIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "historicosHorarioIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horBas");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horBas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horDsr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horDsr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horSab");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horSab"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horSem");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horSem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
