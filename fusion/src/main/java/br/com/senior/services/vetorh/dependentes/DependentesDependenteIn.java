/**
 * DependentesDependenteIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.dependentes;

public class DependentesDependenteIn  implements java.io.Serializable {
    private java.lang.String auxCre;

    private java.lang.String aviImp;

    private java.lang.Integer codDep;

    private java.lang.String codFic;

    private java.lang.Integer codFor;

    private java.lang.String datInv;

    private java.lang.String datNas;

    private java.lang.String datTut;

    private java.lang.String depRPP;

    private java.lang.Integer estCiv;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.Integer graIns;

    private java.lang.Integer graPar;

    private java.lang.String iniTut;

    private java.lang.Integer limIrf;

    private java.lang.Integer limSaf;

    private java.lang.String nomCom;

    private java.lang.Integer nomCre;

    private java.lang.String nomDep;

    private java.lang.String nomMae;

    private java.lang.Integer numCad;

    private java.lang.String numCpf;

    private java.lang.Integer numEmp;

    private java.lang.String penJud;

    private java.lang.Integer tipCol;

    private java.lang.Integer tipDep;

    private java.lang.String tipOpe;

    private java.lang.String tipSex;

    public DependentesDependenteIn() {
    }

    public DependentesDependenteIn(
           java.lang.String auxCre,
           java.lang.String aviImp,
           java.lang.Integer codDep,
           java.lang.String codFic,
           java.lang.Integer codFor,
           java.lang.String datInv,
           java.lang.String datNas,
           java.lang.String datTut,
           java.lang.String depRPP,
           java.lang.Integer estCiv,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.Integer graIns,
           java.lang.Integer graPar,
           java.lang.String iniTut,
           java.lang.Integer limIrf,
           java.lang.Integer limSaf,
           java.lang.String nomCom,
           java.lang.Integer nomCre,
           java.lang.String nomDep,
           java.lang.String nomMae,
           java.lang.Integer numCad,
           java.lang.String numCpf,
           java.lang.Integer numEmp,
           java.lang.String penJud,
           java.lang.Integer tipCol,
           java.lang.Integer tipDep,
           java.lang.String tipOpe,
           java.lang.String tipSex) {
           this.auxCre = auxCre;
           this.aviImp = aviImp;
           this.codDep = codDep;
           this.codFic = codFic;
           this.codFor = codFor;
           this.datInv = datInv;
           this.datNas = datNas;
           this.datTut = datTut;
           this.depRPP = depRPP;
           this.estCiv = estCiv;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.graIns = graIns;
           this.graPar = graPar;
           this.iniTut = iniTut;
           this.limIrf = limIrf;
           this.limSaf = limSaf;
           this.nomCom = nomCom;
           this.nomCre = nomCre;
           this.nomDep = nomDep;
           this.nomMae = nomMae;
           this.numCad = numCad;
           this.numCpf = numCpf;
           this.numEmp = numEmp;
           this.penJud = penJud;
           this.tipCol = tipCol;
           this.tipDep = tipDep;
           this.tipOpe = tipOpe;
           this.tipSex = tipSex;
    }


    /**
     * Gets the auxCre value for this DependentesDependenteIn.
     * 
     * @return auxCre
     */
    public java.lang.String getAuxCre() {
        return auxCre;
    }


    /**
     * Sets the auxCre value for this DependentesDependenteIn.
     * 
     * @param auxCre
     */
    public void setAuxCre(java.lang.String auxCre) {
        this.auxCre = auxCre;
    }


    /**
     * Gets the aviImp value for this DependentesDependenteIn.
     * 
     * @return aviImp
     */
    public java.lang.String getAviImp() {
        return aviImp;
    }


    /**
     * Sets the aviImp value for this DependentesDependenteIn.
     * 
     * @param aviImp
     */
    public void setAviImp(java.lang.String aviImp) {
        this.aviImp = aviImp;
    }


    /**
     * Gets the codDep value for this DependentesDependenteIn.
     * 
     * @return codDep
     */
    public java.lang.Integer getCodDep() {
        return codDep;
    }


    /**
     * Sets the codDep value for this DependentesDependenteIn.
     * 
     * @param codDep
     */
    public void setCodDep(java.lang.Integer codDep) {
        this.codDep = codDep;
    }


    /**
     * Gets the codFic value for this DependentesDependenteIn.
     * 
     * @return codFic
     */
    public java.lang.String getCodFic() {
        return codFic;
    }


    /**
     * Sets the codFic value for this DependentesDependenteIn.
     * 
     * @param codFic
     */
    public void setCodFic(java.lang.String codFic) {
        this.codFic = codFic;
    }


    /**
     * Gets the codFor value for this DependentesDependenteIn.
     * 
     * @return codFor
     */
    public java.lang.Integer getCodFor() {
        return codFor;
    }


    /**
     * Sets the codFor value for this DependentesDependenteIn.
     * 
     * @param codFor
     */
    public void setCodFor(java.lang.Integer codFor) {
        this.codFor = codFor;
    }


    /**
     * Gets the datInv value for this DependentesDependenteIn.
     * 
     * @return datInv
     */
    public java.lang.String getDatInv() {
        return datInv;
    }


    /**
     * Sets the datInv value for this DependentesDependenteIn.
     * 
     * @param datInv
     */
    public void setDatInv(java.lang.String datInv) {
        this.datInv = datInv;
    }


    /**
     * Gets the datNas value for this DependentesDependenteIn.
     * 
     * @return datNas
     */
    public java.lang.String getDatNas() {
        return datNas;
    }


    /**
     * Sets the datNas value for this DependentesDependenteIn.
     * 
     * @param datNas
     */
    public void setDatNas(java.lang.String datNas) {
        this.datNas = datNas;
    }


    /**
     * Gets the datTut value for this DependentesDependenteIn.
     * 
     * @return datTut
     */
    public java.lang.String getDatTut() {
        return datTut;
    }


    /**
     * Sets the datTut value for this DependentesDependenteIn.
     * 
     * @param datTut
     */
    public void setDatTut(java.lang.String datTut) {
        this.datTut = datTut;
    }


    /**
     * Gets the depRPP value for this DependentesDependenteIn.
     * 
     * @return depRPP
     */
    public java.lang.String getDepRPP() {
        return depRPP;
    }


    /**
     * Sets the depRPP value for this DependentesDependenteIn.
     * 
     * @param depRPP
     */
    public void setDepRPP(java.lang.String depRPP) {
        this.depRPP = depRPP;
    }


    /**
     * Gets the estCiv value for this DependentesDependenteIn.
     * 
     * @return estCiv
     */
    public java.lang.Integer getEstCiv() {
        return estCiv;
    }


    /**
     * Sets the estCiv value for this DependentesDependenteIn.
     * 
     * @param estCiv
     */
    public void setEstCiv(java.lang.Integer estCiv) {
        this.estCiv = estCiv;
    }


    /**
     * Gets the flowInstanceID value for this DependentesDependenteIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this DependentesDependenteIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this DependentesDependenteIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this DependentesDependenteIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the graIns value for this DependentesDependenteIn.
     * 
     * @return graIns
     */
    public java.lang.Integer getGraIns() {
        return graIns;
    }


    /**
     * Sets the graIns value for this DependentesDependenteIn.
     * 
     * @param graIns
     */
    public void setGraIns(java.lang.Integer graIns) {
        this.graIns = graIns;
    }


    /**
     * Gets the graPar value for this DependentesDependenteIn.
     * 
     * @return graPar
     */
    public java.lang.Integer getGraPar() {
        return graPar;
    }


    /**
     * Sets the graPar value for this DependentesDependenteIn.
     * 
     * @param graPar
     */
    public void setGraPar(java.lang.Integer graPar) {
        this.graPar = graPar;
    }


    /**
     * Gets the iniTut value for this DependentesDependenteIn.
     * 
     * @return iniTut
     */
    public java.lang.String getIniTut() {
        return iniTut;
    }


    /**
     * Sets the iniTut value for this DependentesDependenteIn.
     * 
     * @param iniTut
     */
    public void setIniTut(java.lang.String iniTut) {
        this.iniTut = iniTut;
    }


    /**
     * Gets the limIrf value for this DependentesDependenteIn.
     * 
     * @return limIrf
     */
    public java.lang.Integer getLimIrf() {
        return limIrf;
    }


    /**
     * Sets the limIrf value for this DependentesDependenteIn.
     * 
     * @param limIrf
     */
    public void setLimIrf(java.lang.Integer limIrf) {
        this.limIrf = limIrf;
    }


    /**
     * Gets the limSaf value for this DependentesDependenteIn.
     * 
     * @return limSaf
     */
    public java.lang.Integer getLimSaf() {
        return limSaf;
    }


    /**
     * Sets the limSaf value for this DependentesDependenteIn.
     * 
     * @param limSaf
     */
    public void setLimSaf(java.lang.Integer limSaf) {
        this.limSaf = limSaf;
    }


    /**
     * Gets the nomCom value for this DependentesDependenteIn.
     * 
     * @return nomCom
     */
    public java.lang.String getNomCom() {
        return nomCom;
    }


    /**
     * Sets the nomCom value for this DependentesDependenteIn.
     * 
     * @param nomCom
     */
    public void setNomCom(java.lang.String nomCom) {
        this.nomCom = nomCom;
    }


    /**
     * Gets the nomCre value for this DependentesDependenteIn.
     * 
     * @return nomCre
     */
    public java.lang.Integer getNomCre() {
        return nomCre;
    }


    /**
     * Sets the nomCre value for this DependentesDependenteIn.
     * 
     * @param nomCre
     */
    public void setNomCre(java.lang.Integer nomCre) {
        this.nomCre = nomCre;
    }


    /**
     * Gets the nomDep value for this DependentesDependenteIn.
     * 
     * @return nomDep
     */
    public java.lang.String getNomDep() {
        return nomDep;
    }


    /**
     * Sets the nomDep value for this DependentesDependenteIn.
     * 
     * @param nomDep
     */
    public void setNomDep(java.lang.String nomDep) {
        this.nomDep = nomDep;
    }


    /**
     * Gets the nomMae value for this DependentesDependenteIn.
     * 
     * @return nomMae
     */
    public java.lang.String getNomMae() {
        return nomMae;
    }


    /**
     * Sets the nomMae value for this DependentesDependenteIn.
     * 
     * @param nomMae
     */
    public void setNomMae(java.lang.String nomMae) {
        this.nomMae = nomMae;
    }


    /**
     * Gets the numCad value for this DependentesDependenteIn.
     * 
     * @return numCad
     */
    public java.lang.Integer getNumCad() {
        return numCad;
    }


    /**
     * Sets the numCad value for this DependentesDependenteIn.
     * 
     * @param numCad
     */
    public void setNumCad(java.lang.Integer numCad) {
        this.numCad = numCad;
    }


    /**
     * Gets the numCpf value for this DependentesDependenteIn.
     * 
     * @return numCpf
     */
    public java.lang.String getNumCpf() {
        return numCpf;
    }


    /**
     * Sets the numCpf value for this DependentesDependenteIn.
     * 
     * @param numCpf
     */
    public void setNumCpf(java.lang.String numCpf) {
        this.numCpf = numCpf;
    }


    /**
     * Gets the numEmp value for this DependentesDependenteIn.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this DependentesDependenteIn.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the penJud value for this DependentesDependenteIn.
     * 
     * @return penJud
     */
    public java.lang.String getPenJud() {
        return penJud;
    }


    /**
     * Sets the penJud value for this DependentesDependenteIn.
     * 
     * @param penJud
     */
    public void setPenJud(java.lang.String penJud) {
        this.penJud = penJud;
    }


    /**
     * Gets the tipCol value for this DependentesDependenteIn.
     * 
     * @return tipCol
     */
    public java.lang.Integer getTipCol() {
        return tipCol;
    }


    /**
     * Sets the tipCol value for this DependentesDependenteIn.
     * 
     * @param tipCol
     */
    public void setTipCol(java.lang.Integer tipCol) {
        this.tipCol = tipCol;
    }


    /**
     * Gets the tipDep value for this DependentesDependenteIn.
     * 
     * @return tipDep
     */
    public java.lang.Integer getTipDep() {
        return tipDep;
    }


    /**
     * Sets the tipDep value for this DependentesDependenteIn.
     * 
     * @param tipDep
     */
    public void setTipDep(java.lang.Integer tipDep) {
        this.tipDep = tipDep;
    }


    /**
     * Gets the tipOpe value for this DependentesDependenteIn.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this DependentesDependenteIn.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }


    /**
     * Gets the tipSex value for this DependentesDependenteIn.
     * 
     * @return tipSex
     */
    public java.lang.String getTipSex() {
        return tipSex;
    }


    /**
     * Sets the tipSex value for this DependentesDependenteIn.
     * 
     * @param tipSex
     */
    public void setTipSex(java.lang.String tipSex) {
        this.tipSex = tipSex;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DependentesDependenteIn)) return false;
        DependentesDependenteIn other = (DependentesDependenteIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.auxCre==null && other.getAuxCre()==null) || 
             (this.auxCre!=null &&
              this.auxCre.equals(other.getAuxCre()))) &&
            ((this.aviImp==null && other.getAviImp()==null) || 
             (this.aviImp!=null &&
              this.aviImp.equals(other.getAviImp()))) &&
            ((this.codDep==null && other.getCodDep()==null) || 
             (this.codDep!=null &&
              this.codDep.equals(other.getCodDep()))) &&
            ((this.codFic==null && other.getCodFic()==null) || 
             (this.codFic!=null &&
              this.codFic.equals(other.getCodFic()))) &&
            ((this.codFor==null && other.getCodFor()==null) || 
             (this.codFor!=null &&
              this.codFor.equals(other.getCodFor()))) &&
            ((this.datInv==null && other.getDatInv()==null) || 
             (this.datInv!=null &&
              this.datInv.equals(other.getDatInv()))) &&
            ((this.datNas==null && other.getDatNas()==null) || 
             (this.datNas!=null &&
              this.datNas.equals(other.getDatNas()))) &&
            ((this.datTut==null && other.getDatTut()==null) || 
             (this.datTut!=null &&
              this.datTut.equals(other.getDatTut()))) &&
            ((this.depRPP==null && other.getDepRPP()==null) || 
             (this.depRPP!=null &&
              this.depRPP.equals(other.getDepRPP()))) &&
            ((this.estCiv==null && other.getEstCiv()==null) || 
             (this.estCiv!=null &&
              this.estCiv.equals(other.getEstCiv()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.graIns==null && other.getGraIns()==null) || 
             (this.graIns!=null &&
              this.graIns.equals(other.getGraIns()))) &&
            ((this.graPar==null && other.getGraPar()==null) || 
             (this.graPar!=null &&
              this.graPar.equals(other.getGraPar()))) &&
            ((this.iniTut==null && other.getIniTut()==null) || 
             (this.iniTut!=null &&
              this.iniTut.equals(other.getIniTut()))) &&
            ((this.limIrf==null && other.getLimIrf()==null) || 
             (this.limIrf!=null &&
              this.limIrf.equals(other.getLimIrf()))) &&
            ((this.limSaf==null && other.getLimSaf()==null) || 
             (this.limSaf!=null &&
              this.limSaf.equals(other.getLimSaf()))) &&
            ((this.nomCom==null && other.getNomCom()==null) || 
             (this.nomCom!=null &&
              this.nomCom.equals(other.getNomCom()))) &&
            ((this.nomCre==null && other.getNomCre()==null) || 
             (this.nomCre!=null &&
              this.nomCre.equals(other.getNomCre()))) &&
            ((this.nomDep==null && other.getNomDep()==null) || 
             (this.nomDep!=null &&
              this.nomDep.equals(other.getNomDep()))) &&
            ((this.nomMae==null && other.getNomMae()==null) || 
             (this.nomMae!=null &&
              this.nomMae.equals(other.getNomMae()))) &&
            ((this.numCad==null && other.getNumCad()==null) || 
             (this.numCad!=null &&
              this.numCad.equals(other.getNumCad()))) &&
            ((this.numCpf==null && other.getNumCpf()==null) || 
             (this.numCpf!=null &&
              this.numCpf.equals(other.getNumCpf()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.penJud==null && other.getPenJud()==null) || 
             (this.penJud!=null &&
              this.penJud.equals(other.getPenJud()))) &&
            ((this.tipCol==null && other.getTipCol()==null) || 
             (this.tipCol!=null &&
              this.tipCol.equals(other.getTipCol()))) &&
            ((this.tipDep==null && other.getTipDep()==null) || 
             (this.tipDep!=null &&
              this.tipDep.equals(other.getTipDep()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe()))) &&
            ((this.tipSex==null && other.getTipSex()==null) || 
             (this.tipSex!=null &&
              this.tipSex.equals(other.getTipSex())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAuxCre() != null) {
            _hashCode += getAuxCre().hashCode();
        }
        if (getAviImp() != null) {
            _hashCode += getAviImp().hashCode();
        }
        if (getCodDep() != null) {
            _hashCode += getCodDep().hashCode();
        }
        if (getCodFic() != null) {
            _hashCode += getCodFic().hashCode();
        }
        if (getCodFor() != null) {
            _hashCode += getCodFor().hashCode();
        }
        if (getDatInv() != null) {
            _hashCode += getDatInv().hashCode();
        }
        if (getDatNas() != null) {
            _hashCode += getDatNas().hashCode();
        }
        if (getDatTut() != null) {
            _hashCode += getDatTut().hashCode();
        }
        if (getDepRPP() != null) {
            _hashCode += getDepRPP().hashCode();
        }
        if (getEstCiv() != null) {
            _hashCode += getEstCiv().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getGraIns() != null) {
            _hashCode += getGraIns().hashCode();
        }
        if (getGraPar() != null) {
            _hashCode += getGraPar().hashCode();
        }
        if (getIniTut() != null) {
            _hashCode += getIniTut().hashCode();
        }
        if (getLimIrf() != null) {
            _hashCode += getLimIrf().hashCode();
        }
        if (getLimSaf() != null) {
            _hashCode += getLimSaf().hashCode();
        }
        if (getNomCom() != null) {
            _hashCode += getNomCom().hashCode();
        }
        if (getNomCre() != null) {
            _hashCode += getNomCre().hashCode();
        }
        if (getNomDep() != null) {
            _hashCode += getNomDep().hashCode();
        }
        if (getNomMae() != null) {
            _hashCode += getNomMae().hashCode();
        }
        if (getNumCad() != null) {
            _hashCode += getNumCad().hashCode();
        }
        if (getNumCpf() != null) {
            _hashCode += getNumCpf().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getPenJud() != null) {
            _hashCode += getPenJud().hashCode();
        }
        if (getTipCol() != null) {
            _hashCode += getTipCol().hashCode();
        }
        if (getTipDep() != null) {
            _hashCode += getTipDep().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        if (getTipSex() != null) {
            _hashCode += getTipSex().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DependentesDependenteIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "dependentesDependenteIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("auxCre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "auxCre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("aviImp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "aviImp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codDep");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codDep"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFic");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFic"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datInv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datInv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datNas");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datNas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datTut");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datTut"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("depRPP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "depRPP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estCiv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estCiv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("graIns");
        elemField.setXmlName(new javax.xml.namespace.QName("", "graIns"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("graPar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "graPar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("iniTut");
        elemField.setXmlName(new javax.xml.namespace.QName("", "iniTut"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limIrf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "limIrf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limSaf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "limSaf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomCom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomCom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomCre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomCre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomDep");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomDep"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomMae");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomMae"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCpf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCpf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("penJud");
        elemField.setXmlName(new javax.xml.namespace.QName("", "penJud"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipDep");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipDep"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipSex");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipSex"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
