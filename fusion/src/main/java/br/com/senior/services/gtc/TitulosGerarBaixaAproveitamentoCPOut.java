/**
 * TitulosGerarBaixaAproveitamentoCPOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.gtc;

public class TitulosGerarBaixaAproveitamentoCPOut  implements java.io.Serializable {
    private java.lang.String erroExecucao;

    private TitulosGerarBaixaAproveitamentoCPOutGridRetorno[] gridRetorno;

    private java.lang.String resultado;

    public TitulosGerarBaixaAproveitamentoCPOut() {
    }

    public TitulosGerarBaixaAproveitamentoCPOut(
           java.lang.String erroExecucao,
           TitulosGerarBaixaAproveitamentoCPOutGridRetorno[] gridRetorno,
           java.lang.String resultado) {
           this.erroExecucao = erroExecucao;
           this.gridRetorno = gridRetorno;
           this.resultado = resultado;
    }


    /**
     * Gets the erroExecucao value for this TitulosGerarBaixaAproveitamentoCPOut.
     * 
     * @return erroExecucao
     */
    public java.lang.String getErroExecucao() {
        return erroExecucao;
    }


    /**
     * Sets the erroExecucao value for this TitulosGerarBaixaAproveitamentoCPOut.
     * 
     * @param erroExecucao
     */
    public void setErroExecucao(java.lang.String erroExecucao) {
        this.erroExecucao = erroExecucao;
    }


    /**
     * Gets the gridRetorno value for this TitulosGerarBaixaAproveitamentoCPOut.
     * 
     * @return gridRetorno
     */
    public TitulosGerarBaixaAproveitamentoCPOutGridRetorno[] getGridRetorno() {
        return gridRetorno;
    }


    /**
     * Sets the gridRetorno value for this TitulosGerarBaixaAproveitamentoCPOut.
     * 
     * @param gridRetorno
     */
    public void setGridRetorno(TitulosGerarBaixaAproveitamentoCPOutGridRetorno[] gridRetorno) {
        this.gridRetorno = gridRetorno;
    }

    public TitulosGerarBaixaAproveitamentoCPOutGridRetorno getGridRetorno(int i) {
        return this.gridRetorno[i];
    }

    public void setGridRetorno(int i, TitulosGerarBaixaAproveitamentoCPOutGridRetorno _value) {
        this.gridRetorno[i] = _value;
    }


    /**
     * Gets the resultado value for this TitulosGerarBaixaAproveitamentoCPOut.
     * 
     * @return resultado
     */
    public java.lang.String getResultado() {
        return resultado;
    }


    /**
     * Sets the resultado value for this TitulosGerarBaixaAproveitamentoCPOut.
     * 
     * @param resultado
     */
    public void setResultado(java.lang.String resultado) {
        this.resultado = resultado;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosGerarBaixaAproveitamentoCPOut)) return false;
        TitulosGerarBaixaAproveitamentoCPOut other = (TitulosGerarBaixaAproveitamentoCPOut) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.erroExecucao==null && other.getErroExecucao()==null) || 
             (this.erroExecucao!=null &&
              this.erroExecucao.equals(other.getErroExecucao()))) &&
            ((this.gridRetorno==null && other.getGridRetorno()==null) || 
             (this.gridRetorno!=null &&
              java.util.Arrays.equals(this.gridRetorno, other.getGridRetorno()))) &&
            ((this.resultado==null && other.getResultado()==null) || 
             (this.resultado!=null &&
              this.resultado.equals(other.getResultado())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getErroExecucao() != null) {
            _hashCode += getErroExecucao().hashCode();
        }
        if (getGridRetorno() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGridRetorno());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGridRetorno(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getResultado() != null) {
            _hashCode += getResultado().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosGerarBaixaAproveitamentoCPOut.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosGerarBaixaAproveitamentoCPOut"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("erroExecucao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "erroExecucao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gridRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "gridRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosGerarBaixaAproveitamentoCPOutGridRetorno"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultado");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resultado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
