/**
 * FichaBasicaDeficienciasInDeficiencias.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.ficha.basica;

public class FichaBasicaDeficienciasInDeficiencias  implements java.io.Serializable {
    private java.lang.Integer codDef;

    private java.lang.String datDef;

    private java.lang.String defPri;

    private java.lang.String obsDef;

    private java.lang.String tipOpe;

    public FichaBasicaDeficienciasInDeficiencias() {
    }

    public FichaBasicaDeficienciasInDeficiencias(
           java.lang.Integer codDef,
           java.lang.String datDef,
           java.lang.String defPri,
           java.lang.String obsDef,
           java.lang.String tipOpe) {
           this.codDef = codDef;
           this.datDef = datDef;
           this.defPri = defPri;
           this.obsDef = obsDef;
           this.tipOpe = tipOpe;
    }


    /**
     * Gets the codDef value for this FichaBasicaDeficienciasInDeficiencias.
     * 
     * @return codDef
     */
    public java.lang.Integer getCodDef() {
        return codDef;
    }


    /**
     * Sets the codDef value for this FichaBasicaDeficienciasInDeficiencias.
     * 
     * @param codDef
     */
    public void setCodDef(java.lang.Integer codDef) {
        this.codDef = codDef;
    }


    /**
     * Gets the datDef value for this FichaBasicaDeficienciasInDeficiencias.
     * 
     * @return datDef
     */
    public java.lang.String getDatDef() {
        return datDef;
    }


    /**
     * Sets the datDef value for this FichaBasicaDeficienciasInDeficiencias.
     * 
     * @param datDef
     */
    public void setDatDef(java.lang.String datDef) {
        this.datDef = datDef;
    }


    /**
     * Gets the defPri value for this FichaBasicaDeficienciasInDeficiencias.
     * 
     * @return defPri
     */
    public java.lang.String getDefPri() {
        return defPri;
    }


    /**
     * Sets the defPri value for this FichaBasicaDeficienciasInDeficiencias.
     * 
     * @param defPri
     */
    public void setDefPri(java.lang.String defPri) {
        this.defPri = defPri;
    }


    /**
     * Gets the obsDef value for this FichaBasicaDeficienciasInDeficiencias.
     * 
     * @return obsDef
     */
    public java.lang.String getObsDef() {
        return obsDef;
    }


    /**
     * Sets the obsDef value for this FichaBasicaDeficienciasInDeficiencias.
     * 
     * @param obsDef
     */
    public void setObsDef(java.lang.String obsDef) {
        this.obsDef = obsDef;
    }


    /**
     * Gets the tipOpe value for this FichaBasicaDeficienciasInDeficiencias.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this FichaBasicaDeficienciasInDeficiencias.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FichaBasicaDeficienciasInDeficiencias)) return false;
        FichaBasicaDeficienciasInDeficiencias other = (FichaBasicaDeficienciasInDeficiencias) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codDef==null && other.getCodDef()==null) || 
             (this.codDef!=null &&
              this.codDef.equals(other.getCodDef()))) &&
            ((this.datDef==null && other.getDatDef()==null) || 
             (this.datDef!=null &&
              this.datDef.equals(other.getDatDef()))) &&
            ((this.defPri==null && other.getDefPri()==null) || 
             (this.defPri!=null &&
              this.defPri.equals(other.getDefPri()))) &&
            ((this.obsDef==null && other.getObsDef()==null) || 
             (this.obsDef!=null &&
              this.obsDef.equals(other.getObsDef()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodDef() != null) {
            _hashCode += getCodDef().hashCode();
        }
        if (getDatDef() != null) {
            _hashCode += getDatDef().hashCode();
        }
        if (getDefPri() != null) {
            _hashCode += getDefPri().hashCode();
        }
        if (getObsDef() != null) {
            _hashCode += getObsDef().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FichaBasicaDeficienciasInDeficiencias.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fichaBasicaDeficienciasInDeficiencias"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codDef");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codDef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datDef");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datDef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("defPri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "defPri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsDef");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsDef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
