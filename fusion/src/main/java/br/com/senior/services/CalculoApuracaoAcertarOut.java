/**
 * CalculoApuracaoAcertarOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class CalculoApuracaoAcertarOut  implements java.io.Serializable {
    private java.lang.String erroExecucao;

    private java.lang.String msgRet;

    private br.com.senior.services.CalculoApuracaoAcertarOutTApuDat[] TApuDat;

    private br.com.senior.services.CalculoApuracaoAcertarOutTMarDat[] TMarDat;

    public CalculoApuracaoAcertarOut() {
    }

    public CalculoApuracaoAcertarOut(
           java.lang.String erroExecucao,
           java.lang.String msgRet,
           br.com.senior.services.CalculoApuracaoAcertarOutTApuDat[] TApuDat,
           br.com.senior.services.CalculoApuracaoAcertarOutTMarDat[] TMarDat) {
           this.erroExecucao = erroExecucao;
           this.msgRet = msgRet;
           this.TApuDat = TApuDat;
           this.TMarDat = TMarDat;
    }


    /**
     * Gets the erroExecucao value for this CalculoApuracaoAcertarOut.
     * 
     * @return erroExecucao
     */
    public java.lang.String getErroExecucao() {
        return erroExecucao;
    }


    /**
     * Sets the erroExecucao value for this CalculoApuracaoAcertarOut.
     * 
     * @param erroExecucao
     */
    public void setErroExecucao(java.lang.String erroExecucao) {
        this.erroExecucao = erroExecucao;
    }


    /**
     * Gets the msgRet value for this CalculoApuracaoAcertarOut.
     * 
     * @return msgRet
     */
    public java.lang.String getMsgRet() {
        return msgRet;
    }


    /**
     * Sets the msgRet value for this CalculoApuracaoAcertarOut.
     * 
     * @param msgRet
     */
    public void setMsgRet(java.lang.String msgRet) {
        this.msgRet = msgRet;
    }


    /**
     * Gets the TApuDat value for this CalculoApuracaoAcertarOut.
     * 
     * @return TApuDat
     */
    public br.com.senior.services.CalculoApuracaoAcertarOutTApuDat[] getTApuDat() {
        return TApuDat;
    }


    /**
     * Sets the TApuDat value for this CalculoApuracaoAcertarOut.
     * 
     * @param TApuDat
     */
    public void setTApuDat(br.com.senior.services.CalculoApuracaoAcertarOutTApuDat[] TApuDat) {
        this.TApuDat = TApuDat;
    }

    public br.com.senior.services.CalculoApuracaoAcertarOutTApuDat getTApuDat(int i) {
        return this.TApuDat[i];
    }

    public void setTApuDat(int i, br.com.senior.services.CalculoApuracaoAcertarOutTApuDat _value) {
        this.TApuDat[i] = _value;
    }


    /**
     * Gets the TMarDat value for this CalculoApuracaoAcertarOut.
     * 
     * @return TMarDat
     */
    public br.com.senior.services.CalculoApuracaoAcertarOutTMarDat[] getTMarDat() {
        return TMarDat;
    }


    /**
     * Sets the TMarDat value for this CalculoApuracaoAcertarOut.
     * 
     * @param TMarDat
     */
    public void setTMarDat(br.com.senior.services.CalculoApuracaoAcertarOutTMarDat[] TMarDat) {
        this.TMarDat = TMarDat;
    }

    public br.com.senior.services.CalculoApuracaoAcertarOutTMarDat getTMarDat(int i) {
        return this.TMarDat[i];
    }

    public void setTMarDat(int i, br.com.senior.services.CalculoApuracaoAcertarOutTMarDat _value) {
        this.TMarDat[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CalculoApuracaoAcertarOut)) return false;
        CalculoApuracaoAcertarOut other = (CalculoApuracaoAcertarOut) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.erroExecucao==null && other.getErroExecucao()==null) || 
             (this.erroExecucao!=null &&
              this.erroExecucao.equals(other.getErroExecucao()))) &&
            ((this.msgRet==null && other.getMsgRet()==null) || 
             (this.msgRet!=null &&
              this.msgRet.equals(other.getMsgRet()))) &&
            ((this.TApuDat==null && other.getTApuDat()==null) || 
             (this.TApuDat!=null &&
              java.util.Arrays.equals(this.TApuDat, other.getTApuDat()))) &&
            ((this.TMarDat==null && other.getTMarDat()==null) || 
             (this.TMarDat!=null &&
              java.util.Arrays.equals(this.TMarDat, other.getTMarDat())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getErroExecucao() != null) {
            _hashCode += getErroExecucao().hashCode();
        }
        if (getMsgRet() != null) {
            _hashCode += getMsgRet().hashCode();
        }
        if (getTApuDat() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTApuDat());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTApuDat(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTMarDat() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTMarDat());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTMarDat(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CalculoApuracaoAcertarOut.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "calculoApuracaoAcertarOut"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("erroExecucao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "erroExecucao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msgRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msgRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TApuDat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TApuDat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "calculoApuracaoAcertarOutTApuDat"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TMarDat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TMarDat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "calculoApuracaoAcertarOutTMarDat"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
