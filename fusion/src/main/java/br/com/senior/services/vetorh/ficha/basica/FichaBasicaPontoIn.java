/**
 * FichaBasicaPontoIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.ficha.basica;

public class FichaBasicaPontoIn  implements java.io.Serializable {
    private java.lang.String datFim;

    private java.lang.String datIni;

    private java.lang.Integer eliMar;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String horFim;

    private java.lang.String horIni;

    private java.lang.Integer numCad;

    private java.lang.String numCra;

    private java.lang.Integer numEmp;

    private java.lang.Integer viaCra;

    public FichaBasicaPontoIn() {
    }

    public FichaBasicaPontoIn(
           java.lang.String datFim,
           java.lang.String datIni,
           java.lang.Integer eliMar,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String horFim,
           java.lang.String horIni,
           java.lang.Integer numCad,
           java.lang.String numCra,
           java.lang.Integer numEmp,
           java.lang.Integer viaCra) {
           this.datFim = datFim;
           this.datIni = datIni;
           this.eliMar = eliMar;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.horFim = horFim;
           this.horIni = horIni;
           this.numCad = numCad;
           this.numCra = numCra;
           this.numEmp = numEmp;
           this.viaCra = viaCra;
    }


    /**
     * Gets the datFim value for this FichaBasicaPontoIn.
     * 
     * @return datFim
     */
    public java.lang.String getDatFim() {
        return datFim;
    }


    /**
     * Sets the datFim value for this FichaBasicaPontoIn.
     * 
     * @param datFim
     */
    public void setDatFim(java.lang.String datFim) {
        this.datFim = datFim;
    }


    /**
     * Gets the datIni value for this FichaBasicaPontoIn.
     * 
     * @return datIni
     */
    public java.lang.String getDatIni() {
        return datIni;
    }


    /**
     * Sets the datIni value for this FichaBasicaPontoIn.
     * 
     * @param datIni
     */
    public void setDatIni(java.lang.String datIni) {
        this.datIni = datIni;
    }


    /**
     * Gets the eliMar value for this FichaBasicaPontoIn.
     * 
     * @return eliMar
     */
    public java.lang.Integer getEliMar() {
        return eliMar;
    }


    /**
     * Sets the eliMar value for this FichaBasicaPontoIn.
     * 
     * @param eliMar
     */
    public void setEliMar(java.lang.Integer eliMar) {
        this.eliMar = eliMar;
    }


    /**
     * Gets the flowInstanceID value for this FichaBasicaPontoIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this FichaBasicaPontoIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this FichaBasicaPontoIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this FichaBasicaPontoIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the horFim value for this FichaBasicaPontoIn.
     * 
     * @return horFim
     */
    public java.lang.String getHorFim() {
        return horFim;
    }


    /**
     * Sets the horFim value for this FichaBasicaPontoIn.
     * 
     * @param horFim
     */
    public void setHorFim(java.lang.String horFim) {
        this.horFim = horFim;
    }


    /**
     * Gets the horIni value for this FichaBasicaPontoIn.
     * 
     * @return horIni
     */
    public java.lang.String getHorIni() {
        return horIni;
    }


    /**
     * Sets the horIni value for this FichaBasicaPontoIn.
     * 
     * @param horIni
     */
    public void setHorIni(java.lang.String horIni) {
        this.horIni = horIni;
    }


    /**
     * Gets the numCad value for this FichaBasicaPontoIn.
     * 
     * @return numCad
     */
    public java.lang.Integer getNumCad() {
        return numCad;
    }


    /**
     * Sets the numCad value for this FichaBasicaPontoIn.
     * 
     * @param numCad
     */
    public void setNumCad(java.lang.Integer numCad) {
        this.numCad = numCad;
    }


    /**
     * Gets the numCra value for this FichaBasicaPontoIn.
     * 
     * @return numCra
     */
    public java.lang.String getNumCra() {
        return numCra;
    }


    /**
     * Sets the numCra value for this FichaBasicaPontoIn.
     * 
     * @param numCra
     */
    public void setNumCra(java.lang.String numCra) {
        this.numCra = numCra;
    }


    /**
     * Gets the numEmp value for this FichaBasicaPontoIn.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this FichaBasicaPontoIn.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the viaCra value for this FichaBasicaPontoIn.
     * 
     * @return viaCra
     */
    public java.lang.Integer getViaCra() {
        return viaCra;
    }


    /**
     * Sets the viaCra value for this FichaBasicaPontoIn.
     * 
     * @param viaCra
     */
    public void setViaCra(java.lang.Integer viaCra) {
        this.viaCra = viaCra;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FichaBasicaPontoIn)) return false;
        FichaBasicaPontoIn other = (FichaBasicaPontoIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.datFim==null && other.getDatFim()==null) || 
             (this.datFim!=null &&
              this.datFim.equals(other.getDatFim()))) &&
            ((this.datIni==null && other.getDatIni()==null) || 
             (this.datIni!=null &&
              this.datIni.equals(other.getDatIni()))) &&
            ((this.eliMar==null && other.getEliMar()==null) || 
             (this.eliMar!=null &&
              this.eliMar.equals(other.getEliMar()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.horFim==null && other.getHorFim()==null) || 
             (this.horFim!=null &&
              this.horFim.equals(other.getHorFim()))) &&
            ((this.horIni==null && other.getHorIni()==null) || 
             (this.horIni!=null &&
              this.horIni.equals(other.getHorIni()))) &&
            ((this.numCad==null && other.getNumCad()==null) || 
             (this.numCad!=null &&
              this.numCad.equals(other.getNumCad()))) &&
            ((this.numCra==null && other.getNumCra()==null) || 
             (this.numCra!=null &&
              this.numCra.equals(other.getNumCra()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.viaCra==null && other.getViaCra()==null) || 
             (this.viaCra!=null &&
              this.viaCra.equals(other.getViaCra())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDatFim() != null) {
            _hashCode += getDatFim().hashCode();
        }
        if (getDatIni() != null) {
            _hashCode += getDatIni().hashCode();
        }
        if (getEliMar() != null) {
            _hashCode += getEliMar().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getHorFim() != null) {
            _hashCode += getHorFim().hashCode();
        }
        if (getHorIni() != null) {
            _hashCode += getHorIni().hashCode();
        }
        if (getNumCad() != null) {
            _hashCode += getNumCad().hashCode();
        }
        if (getNumCra() != null) {
            _hashCode += getNumCra().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getViaCra() != null) {
            _hashCode += getViaCra().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FichaBasicaPontoIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fichaBasicaPontoIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datFim");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datFim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datIni");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datIni"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eliMar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "eliMar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horFim");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horFim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horIni");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horIni"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCra");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("viaCra");
        elemField.setXmlName(new javax.xml.namespace.QName("", "viaCra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
