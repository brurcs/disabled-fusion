/**
 * ClientesExportar2OutCliente.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter.
 */

package br.com.senior.services.co.cad.clientes;

public class ClientesExportar2OutCliente  implements java.io.Serializable {
    private java.lang.String apeCli;

    private java.lang.String baiCli;

    private java.lang.String baiCob;

    private java.lang.String baiEnt;

    private java.lang.String bloCre;

    private java.lang.String calFun;

    private java.lang.String calSen;

    private br.com.senior.services.co.cad.clientes.ClientesExportar2OutClienteCampoUsuarioCliente[] campoUsuarioCliente;

    private java.lang.Integer cepCli;

    private java.lang.Integer cepCob;

    private java.lang.Integer cepEnt;

    private java.lang.Integer cepFre;

    private java.lang.Integer cepIni;

    private java.lang.Double cgcCob;

    private java.lang.String cgcCpf;

    private java.lang.Double cgcEnt;

    private java.lang.String cidCli;

    private java.lang.String cidCob;

    private java.lang.String cidEnt;

    private java.lang.String cliCon;

    private java.lang.String cliFor;

    private java.lang.String cliPrx;

    private java.lang.Integer cliRep;

    private java.lang.Integer cliTra;

    private java.lang.String codAma;

    private java.lang.Integer codCli;

    private java.lang.Integer codCnv;

    private java.lang.Integer codFor;

    private java.lang.String codGal;

    private java.lang.Integer codGre;

    private java.lang.Integer codMot;

    private java.lang.Integer codMs2;

    private java.lang.Integer codMs3;

    private java.lang.Integer codMs4;

    private java.lang.Integer codMsg;

    private java.lang.String codPai;

    private java.lang.Integer codPdv;

    private java.lang.String codRam;

    private java.lang.String codRoe;

    private java.lang.Integer codRtr;

    private java.lang.String codSab;

    private java.lang.String codSro;

    private java.lang.String codSuf;

    private java.lang.String conFin;

    private br.com.senior.services.co.cad.clientes.ClientesExportar2OutClienteContatos[] contatos;

    private java.lang.String cplCob;

    private java.lang.String cplEnd;

    private java.lang.String cplEnt;

    private java.lang.Integer cxaPst;

    private java.lang.String datAtu;

    private java.lang.String datCad;

    private java.lang.String datFim;

    private java.lang.String datIcv;

    private java.lang.String datMot;

    private java.lang.String datPal;

    private java.lang.String datPdv;

    private java.lang.String datSuf;

    private java.lang.String datVct;

    private java.lang.String eenCli;

    private java.lang.String eenCob;

    private java.lang.String eenEnt;

    private java.lang.String emaNfe;

    private java.lang.String endCli;

    private java.lang.String endCob;

    private java.lang.String endEnt;

    private java.lang.String entCor;

    private java.lang.String estCob;

    private java.lang.String estEnt;

    private java.lang.String faxCli;

    private java.lang.String fonCl2;

    private java.lang.String fonCl3;

    private java.lang.String fonCl4;

    private java.lang.String fonCl5;

    private java.lang.String fonCli;

    private br.com.senior.services.co.cad.clientes.ClientesExportar2OutClienteHistorico[] historico;

    private java.lang.Integer horAtu;

    private java.lang.Integer horCad;

    private java.lang.Integer horFim;

    private java.lang.Integer horMot;

    private java.lang.Integer horPal;

    private java.lang.Integer horPdv;

    private java.lang.String ideCli;

    private java.lang.String indCoo;

    private java.lang.Integer indExp;

    private java.lang.Integer iniCob;

    private java.lang.Integer iniEnt;

    private java.lang.Integer insAnp;

    private java.lang.String insEnt;

    private java.lang.String insEst;

    private java.lang.String insMun;

    private java.lang.String intNet;

    private java.lang.String limRet;

    private java.lang.String marCli;

    private java.lang.Integer natCof;

    private java.lang.Integer natPis;

    private java.lang.Integer natRet;

    private java.lang.String nenCli;

    private java.lang.String nenCob;

    private java.lang.String nenEnt;

    private java.lang.String nomCli;

    private java.lang.Double numAnx;

    private java.lang.String numIdf;

    private java.lang.String obsMot;

    private java.lang.Double perAin;

    private java.lang.Integer qtdAtu;

    private java.lang.Integer regEst;

    private java.lang.String retCof;

    private java.lang.String retCsl;

    private java.lang.String retIrf;

    private java.lang.String retOur;

    private java.lang.String retPis;

    private java.lang.String retPro;

    private java.lang.Integer rotAnx;

    private java.lang.String senCli;

    private java.lang.Integer seqInt;

    private java.lang.Integer seqRoe;

    private java.lang.String sigUfs;

    private java.lang.String sitCli;

    private java.lang.String temCob;

    private java.lang.String temEnt;

    private java.lang.Integer tipAce;

    private java.lang.String tipCli;

    private java.lang.Integer tipEmc;

    private java.lang.Integer tipEmp;

    private java.lang.String tipMer;

    private java.lang.String triCof;

    private java.lang.String triIcm;

    private java.lang.String triIpi;

    private java.lang.String triPis;

    private java.lang.Double usuAtu;

    private java.lang.Double usuCad;

    private java.lang.Double usuMot;

    private java.lang.Double usuOpe;

    private java.lang.String zipCod;

    private java.lang.Integer zonFra;

    public ClientesExportar2OutCliente() {
    }

    public ClientesExportar2OutCliente(
           java.lang.String apeCli,
           java.lang.String baiCli,
           java.lang.String baiCob,
           java.lang.String baiEnt,
           java.lang.String bloCre,
           java.lang.String calFun,
           java.lang.String calSen,
           br.com.senior.services.co.cad.clientes.ClientesExportar2OutClienteCampoUsuarioCliente[] campoUsuarioCliente,
           java.lang.Integer cepCli,
           java.lang.Integer cepCob,
           java.lang.Integer cepEnt,
           java.lang.Integer cepFre,
           java.lang.Integer cepIni,
           java.lang.Double cgcCob,
           java.lang.String cgcCpf,
           java.lang.Double cgcEnt,
           java.lang.String cidCli,
           java.lang.String cidCob,
           java.lang.String cidEnt,
           java.lang.String cliCon,
           java.lang.String cliFor,
           java.lang.String cliPrx,
           java.lang.Integer cliRep,
           java.lang.Integer cliTra,
           java.lang.String codAma,
           java.lang.Integer codCli,
           java.lang.Integer codCnv,
           java.lang.Integer codFor,
           java.lang.String codGal,
           java.lang.Integer codGre,
           java.lang.Integer codMot,
           java.lang.Integer codMs2,
           java.lang.Integer codMs3,
           java.lang.Integer codMs4,
           java.lang.Integer codMsg,
           java.lang.String codPai,
           java.lang.Integer codPdv,
           java.lang.String codRam,
           java.lang.String codRoe,
           java.lang.Integer codRtr,
           java.lang.String codSab,
           java.lang.String codSro,
           java.lang.String codSuf,
           java.lang.String conFin,
           br.com.senior.services.co.cad.clientes.ClientesExportar2OutClienteContatos[] contatos,
           java.lang.String cplCob,
           java.lang.String cplEnd,
           java.lang.String cplEnt,
           java.lang.Integer cxaPst,
           java.lang.String datAtu,
           java.lang.String datCad,
           java.lang.String datFim,
           java.lang.String datIcv,
           java.lang.String datMot,
           java.lang.String datPal,
           java.lang.String datPdv,
           java.lang.String datSuf,
           java.lang.String datVct,
           java.lang.String eenCli,
           java.lang.String eenCob,
           java.lang.String eenEnt,
           java.lang.String emaNfe,
           java.lang.String endCli,
           java.lang.String endCob,
           java.lang.String endEnt,
           java.lang.String entCor,
           java.lang.String estCob,
           java.lang.String estEnt,
           java.lang.String faxCli,
           java.lang.String fonCl2,
           java.lang.String fonCl3,
           java.lang.String fonCl4,
           java.lang.String fonCl5,
           java.lang.String fonCli,
           br.com.senior.services.co.cad.clientes.ClientesExportar2OutClienteHistorico[] historico,
           java.lang.Integer horAtu,
           java.lang.Integer horCad,
           java.lang.Integer horFim,
           java.lang.Integer horMot,
           java.lang.Integer horPal,
           java.lang.Integer horPdv,
           java.lang.String ideCli,
           java.lang.String indCoo,
           java.lang.Integer indExp,
           java.lang.Integer iniCob,
           java.lang.Integer iniEnt,
           java.lang.Integer insAnp,
           java.lang.String insEnt,
           java.lang.String insEst,
           java.lang.String insMun,
           java.lang.String intNet,
           java.lang.String limRet,
           java.lang.String marCli,
           java.lang.Integer natCof,
           java.lang.Integer natPis,
           java.lang.Integer natRet,
           java.lang.String nenCli,
           java.lang.String nenCob,
           java.lang.String nenEnt,
           java.lang.String nomCli,
           java.lang.Double numAnx,
           java.lang.String numIdf,
           java.lang.String obsMot,
           java.lang.Double perAin,
           java.lang.Integer qtdAtu,
           java.lang.Integer regEst,
           java.lang.String retCof,
           java.lang.String retCsl,
           java.lang.String retIrf,
           java.lang.String retOur,
           java.lang.String retPis,
           java.lang.String retPro,
           java.lang.Integer rotAnx,
           java.lang.String senCli,
           java.lang.Integer seqInt,
           java.lang.Integer seqRoe,
           java.lang.String sigUfs,
           java.lang.String sitCli,
           java.lang.String temCob,
           java.lang.String temEnt,
           java.lang.Integer tipAce,
           java.lang.String tipCli,
           java.lang.Integer tipEmc,
           java.lang.Integer tipEmp,
           java.lang.String tipMer,
           java.lang.String triCof,
           java.lang.String triIcm,
           java.lang.String triIpi,
           java.lang.String triPis,
           java.lang.Double usuAtu,
           java.lang.Double usuCad,
           java.lang.Double usuMot,
           java.lang.Double usuOpe,
           java.lang.String zipCod,
           java.lang.Integer zonFra) {
           this.apeCli = apeCli;
           this.baiCli = baiCli;
           this.baiCob = baiCob;
           this.baiEnt = baiEnt;
           this.bloCre = bloCre;
           this.calFun = calFun;
           this.calSen = calSen;
           this.campoUsuarioCliente = campoUsuarioCliente;
           this.cepCli = cepCli;
           this.cepCob = cepCob;
           this.cepEnt = cepEnt;
           this.cepFre = cepFre;
           this.cepIni = cepIni;
           this.cgcCob = cgcCob;
           this.cgcCpf = cgcCpf;
           this.cgcEnt = cgcEnt;
           this.cidCli = cidCli;
           this.cidCob = cidCob;
           this.cidEnt = cidEnt;
           this.cliCon = cliCon;
           this.cliFor = cliFor;
           this.cliPrx = cliPrx;
           this.cliRep = cliRep;
           this.cliTra = cliTra;
           this.codAma = codAma;
           this.codCli = codCli;
           this.codCnv = codCnv;
           this.codFor = codFor;
           this.codGal = codGal;
           this.codGre = codGre;
           this.codMot = codMot;
           this.codMs2 = codMs2;
           this.codMs3 = codMs3;
           this.codMs4 = codMs4;
           this.codMsg = codMsg;
           this.codPai = codPai;
           this.codPdv = codPdv;
           this.codRam = codRam;
           this.codRoe = codRoe;
           this.codRtr = codRtr;
           this.codSab = codSab;
           this.codSro = codSro;
           this.codSuf = codSuf;
           this.conFin = conFin;
           this.contatos = contatos;
           this.cplCob = cplCob;
           this.cplEnd = cplEnd;
           this.cplEnt = cplEnt;
           this.cxaPst = cxaPst;
           this.datAtu = datAtu;
           this.datCad = datCad;
           this.datFim = datFim;
           this.datIcv = datIcv;
           this.datMot = datMot;
           this.datPal = datPal;
           this.datPdv = datPdv;
           this.datSuf = datSuf;
           this.datVct = datVct;
           this.eenCli = eenCli;
           this.eenCob = eenCob;
           this.eenEnt = eenEnt;
           this.emaNfe = emaNfe;
           this.endCli = endCli;
           this.endCob = endCob;
           this.endEnt = endEnt;
           this.entCor = entCor;
           this.estCob = estCob;
           this.estEnt = estEnt;
           this.faxCli = faxCli;
           this.fonCl2 = fonCl2;
           this.fonCl3 = fonCl3;
           this.fonCl4 = fonCl4;
           this.fonCl5 = fonCl5;
           this.fonCli = fonCli;
           this.historico = historico;
           this.horAtu = horAtu;
           this.horCad = horCad;
           this.horFim = horFim;
           this.horMot = horMot;
           this.horPal = horPal;
           this.horPdv = horPdv;
           this.ideCli = ideCli;
           this.indCoo = indCoo;
           this.indExp = indExp;
           this.iniCob = iniCob;
           this.iniEnt = iniEnt;
           this.insAnp = insAnp;
           this.insEnt = insEnt;
           this.insEst = insEst;
           this.insMun = insMun;
           this.intNet = intNet;
           this.limRet = limRet;
           this.marCli = marCli;
           this.natCof = natCof;
           this.natPis = natPis;
           this.natRet = natRet;
           this.nenCli = nenCli;
           this.nenCob = nenCob;
           this.nenEnt = nenEnt;
           this.nomCli = nomCli;
           this.numAnx = numAnx;
           this.numIdf = numIdf;
           this.obsMot = obsMot;
           this.perAin = perAin;
           this.qtdAtu = qtdAtu;
           this.regEst = regEst;
           this.retCof = retCof;
           this.retCsl = retCsl;
           this.retIrf = retIrf;
           this.retOur = retOur;
           this.retPis = retPis;
           this.retPro = retPro;
           this.rotAnx = rotAnx;
           this.senCli = senCli;
           this.seqInt = seqInt;
           this.seqRoe = seqRoe;
           this.sigUfs = sigUfs;
           this.sitCli = sitCli;
           this.temCob = temCob;
           this.temEnt = temEnt;
           this.tipAce = tipAce;
           this.tipCli = tipCli;
           this.tipEmc = tipEmc;
           this.tipEmp = tipEmp;
           this.tipMer = tipMer;
           this.triCof = triCof;
           this.triIcm = triIcm;
           this.triIpi = triIpi;
           this.triPis = triPis;
           this.usuAtu = usuAtu;
           this.usuCad = usuCad;
           this.usuMot = usuMot;
           this.usuOpe = usuOpe;
           this.zipCod = zipCod;
           this.zonFra = zonFra;
    }


    /**
     * Gets the apeCli value for this ClientesExportar2OutCliente.
     * 
     * @return apeCli
     */
    public java.lang.String getApeCli() {
        return apeCli;
    }


    /**
     * Sets the apeCli value for this ClientesExportar2OutCliente.
     * 
     * @param apeCli
     */
    public void setApeCli(java.lang.String apeCli) {
        this.apeCli = apeCli;
    }


    /**
     * Gets the baiCli value for this ClientesExportar2OutCliente.
     * 
     * @return baiCli
     */
    public java.lang.String getBaiCli() {
        return baiCli;
    }


    /**
     * Sets the baiCli value for this ClientesExportar2OutCliente.
     * 
     * @param baiCli
     */
    public void setBaiCli(java.lang.String baiCli) {
        this.baiCli = baiCli;
    }


    /**
     * Gets the baiCob value for this ClientesExportar2OutCliente.
     * 
     * @return baiCob
     */
    public java.lang.String getBaiCob() {
        return baiCob;
    }


    /**
     * Sets the baiCob value for this ClientesExportar2OutCliente.
     * 
     * @param baiCob
     */
    public void setBaiCob(java.lang.String baiCob) {
        this.baiCob = baiCob;
    }


    /**
     * Gets the baiEnt value for this ClientesExportar2OutCliente.
     * 
     * @return baiEnt
     */
    public java.lang.String getBaiEnt() {
        return baiEnt;
    }


    /**
     * Sets the baiEnt value for this ClientesExportar2OutCliente.
     * 
     * @param baiEnt
     */
    public void setBaiEnt(java.lang.String baiEnt) {
        this.baiEnt = baiEnt;
    }


    /**
     * Gets the bloCre value for this ClientesExportar2OutCliente.
     * 
     * @return bloCre
     */
    public java.lang.String getBloCre() {
        return bloCre;
    }


    /**
     * Sets the bloCre value for this ClientesExportar2OutCliente.
     * 
     * @param bloCre
     */
    public void setBloCre(java.lang.String bloCre) {
        this.bloCre = bloCre;
    }


    /**
     * Gets the calFun value for this ClientesExportar2OutCliente.
     * 
     * @return calFun
     */
    public java.lang.String getCalFun() {
        return calFun;
    }


    /**
     * Sets the calFun value for this ClientesExportar2OutCliente.
     * 
     * @param calFun
     */
    public void setCalFun(java.lang.String calFun) {
        this.calFun = calFun;
    }


    /**
     * Gets the calSen value for this ClientesExportar2OutCliente.
     * 
     * @return calSen
     */
    public java.lang.String getCalSen() {
        return calSen;
    }


    /**
     * Sets the calSen value for this ClientesExportar2OutCliente.
     * 
     * @param calSen
     */
    public void setCalSen(java.lang.String calSen) {
        this.calSen = calSen;
    }


    /**
     * Gets the campoUsuarioCliente value for this ClientesExportar2OutCliente.
     * 
     * @return campoUsuarioCliente
     */
    public br.com.senior.services.co.cad.clientes.ClientesExportar2OutClienteCampoUsuarioCliente[] getCampoUsuarioCliente() {
        return campoUsuarioCliente;
    }


    /**
     * Sets the campoUsuarioCliente value for this ClientesExportar2OutCliente.
     * 
     * @param campoUsuarioCliente
     */
    public void setCampoUsuarioCliente(br.com.senior.services.co.cad.clientes.ClientesExportar2OutClienteCampoUsuarioCliente[] campoUsuarioCliente) {
        this.campoUsuarioCliente = campoUsuarioCliente;
    }

    public br.com.senior.services.co.cad.clientes.ClientesExportar2OutClienteCampoUsuarioCliente getCampoUsuarioCliente(int i) {
        return this.campoUsuarioCliente[i];
    }

    public void setCampoUsuarioCliente(int i, br.com.senior.services.co.cad.clientes.ClientesExportar2OutClienteCampoUsuarioCliente _value) {
        this.campoUsuarioCliente[i] = _value;
    }


    /**
     * Gets the cepCli value for this ClientesExportar2OutCliente.
     * 
     * @return cepCli
     */
    public java.lang.Integer getCepCli() {
        return cepCli;
    }


    /**
     * Sets the cepCli value for this ClientesExportar2OutCliente.
     * 
     * @param cepCli
     */
    public void setCepCli(java.lang.Integer cepCli) {
        this.cepCli = cepCli;
    }


    /**
     * Gets the cepCob value for this ClientesExportar2OutCliente.
     * 
     * @return cepCob
     */
    public java.lang.Integer getCepCob() {
        return cepCob;
    }


    /**
     * Sets the cepCob value for this ClientesExportar2OutCliente.
     * 
     * @param cepCob
     */
    public void setCepCob(java.lang.Integer cepCob) {
        this.cepCob = cepCob;
    }


    /**
     * Gets the cepEnt value for this ClientesExportar2OutCliente.
     * 
     * @return cepEnt
     */
    public java.lang.Integer getCepEnt() {
        return cepEnt;
    }


    /**
     * Sets the cepEnt value for this ClientesExportar2OutCliente.
     * 
     * @param cepEnt
     */
    public void setCepEnt(java.lang.Integer cepEnt) {
        this.cepEnt = cepEnt;
    }


    /**
     * Gets the cepFre value for this ClientesExportar2OutCliente.
     * 
     * @return cepFre
     */
    public java.lang.Integer getCepFre() {
        return cepFre;
    }


    /**
     * Sets the cepFre value for this ClientesExportar2OutCliente.
     * 
     * @param cepFre
     */
    public void setCepFre(java.lang.Integer cepFre) {
        this.cepFre = cepFre;
    }


    /**
     * Gets the cepIni value for this ClientesExportar2OutCliente.
     * 
     * @return cepIni
     */
    public java.lang.Integer getCepIni() {
        return cepIni;
    }


    /**
     * Sets the cepIni value for this ClientesExportar2OutCliente.
     * 
     * @param cepIni
     */
    public void setCepIni(java.lang.Integer cepIni) {
        this.cepIni = cepIni;
    }


    /**
     * Gets the cgcCob value for this ClientesExportar2OutCliente.
     * 
     * @return cgcCob
     */
    public java.lang.Double getCgcCob() {
        return cgcCob;
    }


    /**
     * Sets the cgcCob value for this ClientesExportar2OutCliente.
     * 
     * @param cgcCob
     */
    public void setCgcCob(java.lang.Double cgcCob) {
        this.cgcCob = cgcCob;
    }


    /**
     * Gets the cgcCpf value for this ClientesExportar2OutCliente.
     * 
     * @return cgcCpf
     */
    public java.lang.String getCgcCpf() {
        return cgcCpf;
    }


    /**
     * Sets the cgcCpf value for this ClientesExportar2OutCliente.
     * 
     * @param cgcCpf
     */
    public void setCgcCpf(java.lang.String cgcCpf) {
        this.cgcCpf = cgcCpf;
    }


    /**
     * Gets the cgcEnt value for this ClientesExportar2OutCliente.
     * 
     * @return cgcEnt
     */
    public java.lang.Double getCgcEnt() {
        return cgcEnt;
    }


    /**
     * Sets the cgcEnt value for this ClientesExportar2OutCliente.
     * 
     * @param cgcEnt
     */
    public void setCgcEnt(java.lang.Double cgcEnt) {
        this.cgcEnt = cgcEnt;
    }


    /**
     * Gets the cidCli value for this ClientesExportar2OutCliente.
     * 
     * @return cidCli
     */
    public java.lang.String getCidCli() {
        return cidCli;
    }


    /**
     * Sets the cidCli value for this ClientesExportar2OutCliente.
     * 
     * @param cidCli
     */
    public void setCidCli(java.lang.String cidCli) {
        this.cidCli = cidCli;
    }


    /**
     * Gets the cidCob value for this ClientesExportar2OutCliente.
     * 
     * @return cidCob
     */
    public java.lang.String getCidCob() {
        return cidCob;
    }


    /**
     * Sets the cidCob value for this ClientesExportar2OutCliente.
     * 
     * @param cidCob
     */
    public void setCidCob(java.lang.String cidCob) {
        this.cidCob = cidCob;
    }


    /**
     * Gets the cidEnt value for this ClientesExportar2OutCliente.
     * 
     * @return cidEnt
     */
    public java.lang.String getCidEnt() {
        return cidEnt;
    }


    /**
     * Sets the cidEnt value for this ClientesExportar2OutCliente.
     * 
     * @param cidEnt
     */
    public void setCidEnt(java.lang.String cidEnt) {
        this.cidEnt = cidEnt;
    }


    /**
     * Gets the cliCon value for this ClientesExportar2OutCliente.
     * 
     * @return cliCon
     */
    public java.lang.String getCliCon() {
        return cliCon;
    }


    /**
     * Sets the cliCon value for this ClientesExportar2OutCliente.
     * 
     * @param cliCon
     */
    public void setCliCon(java.lang.String cliCon) {
        this.cliCon = cliCon;
    }


    /**
     * Gets the cliFor value for this ClientesExportar2OutCliente.
     * 
     * @return cliFor
     */
    public java.lang.String getCliFor() {
        return cliFor;
    }


    /**
     * Sets the cliFor value for this ClientesExportar2OutCliente.
     * 
     * @param cliFor
     */
    public void setCliFor(java.lang.String cliFor) {
        this.cliFor = cliFor;
    }


    /**
     * Gets the cliPrx value for this ClientesExportar2OutCliente.
     * 
     * @return cliPrx
     */
    public java.lang.String getCliPrx() {
        return cliPrx;
    }


    /**
     * Sets the cliPrx value for this ClientesExportar2OutCliente.
     * 
     * @param cliPrx
     */
    public void setCliPrx(java.lang.String cliPrx) {
        this.cliPrx = cliPrx;
    }


    /**
     * Gets the cliRep value for this ClientesExportar2OutCliente.
     * 
     * @return cliRep
     */
    public java.lang.Integer getCliRep() {
        return cliRep;
    }


    /**
     * Sets the cliRep value for this ClientesExportar2OutCliente.
     * 
     * @param cliRep
     */
    public void setCliRep(java.lang.Integer cliRep) {
        this.cliRep = cliRep;
    }


    /**
     * Gets the cliTra value for this ClientesExportar2OutCliente.
     * 
     * @return cliTra
     */
    public java.lang.Integer getCliTra() {
        return cliTra;
    }


    /**
     * Sets the cliTra value for this ClientesExportar2OutCliente.
     * 
     * @param cliTra
     */
    public void setCliTra(java.lang.Integer cliTra) {
        this.cliTra = cliTra;
    }


    /**
     * Gets the codAma value for this ClientesExportar2OutCliente.
     * 
     * @return codAma
     */
    public java.lang.String getCodAma() {
        return codAma;
    }


    /**
     * Sets the codAma value for this ClientesExportar2OutCliente.
     * 
     * @param codAma
     */
    public void setCodAma(java.lang.String codAma) {
        this.codAma = codAma;
    }


    /**
     * Gets the codCli value for this ClientesExportar2OutCliente.
     * 
     * @return codCli
     */
    public java.lang.Integer getCodCli() {
        return codCli;
    }


    /**
     * Sets the codCli value for this ClientesExportar2OutCliente.
     * 
     * @param codCli
     */
    public void setCodCli(java.lang.Integer codCli) {
        this.codCli = codCli;
    }


    /**
     * Gets the codCnv value for this ClientesExportar2OutCliente.
     * 
     * @return codCnv
     */
    public java.lang.Integer getCodCnv() {
        return codCnv;
    }


    /**
     * Sets the codCnv value for this ClientesExportar2OutCliente.
     * 
     * @param codCnv
     */
    public void setCodCnv(java.lang.Integer codCnv) {
        this.codCnv = codCnv;
    }


    /**
     * Gets the codFor value for this ClientesExportar2OutCliente.
     * 
     * @return codFor
     */
    public java.lang.Integer getCodFor() {
        return codFor;
    }


    /**
     * Sets the codFor value for this ClientesExportar2OutCliente.
     * 
     * @param codFor
     */
    public void setCodFor(java.lang.Integer codFor) {
        this.codFor = codFor;
    }


    /**
     * Gets the codGal value for this ClientesExportar2OutCliente.
     * 
     * @return codGal
     */
    public java.lang.String getCodGal() {
        return codGal;
    }


    /**
     * Sets the codGal value for this ClientesExportar2OutCliente.
     * 
     * @param codGal
     */
    public void setCodGal(java.lang.String codGal) {
        this.codGal = codGal;
    }


    /**
     * Gets the codGre value for this ClientesExportar2OutCliente.
     * 
     * @return codGre
     */
    public java.lang.Integer getCodGre() {
        return codGre;
    }


    /**
     * Sets the codGre value for this ClientesExportar2OutCliente.
     * 
     * @param codGre
     */
    public void setCodGre(java.lang.Integer codGre) {
        this.codGre = codGre;
    }


    /**
     * Gets the codMot value for this ClientesExportar2OutCliente.
     * 
     * @return codMot
     */
    public java.lang.Integer getCodMot() {
        return codMot;
    }


    /**
     * Sets the codMot value for this ClientesExportar2OutCliente.
     * 
     * @param codMot
     */
    public void setCodMot(java.lang.Integer codMot) {
        this.codMot = codMot;
    }


    /**
     * Gets the codMs2 value for this ClientesExportar2OutCliente.
     * 
     * @return codMs2
     */
    public java.lang.Integer getCodMs2() {
        return codMs2;
    }


    /**
     * Sets the codMs2 value for this ClientesExportar2OutCliente.
     * 
     * @param codMs2
     */
    public void setCodMs2(java.lang.Integer codMs2) {
        this.codMs2 = codMs2;
    }


    /**
     * Gets the codMs3 value for this ClientesExportar2OutCliente.
     * 
     * @return codMs3
     */
    public java.lang.Integer getCodMs3() {
        return codMs3;
    }


    /**
     * Sets the codMs3 value for this ClientesExportar2OutCliente.
     * 
     * @param codMs3
     */
    public void setCodMs3(java.lang.Integer codMs3) {
        this.codMs3 = codMs3;
    }


    /**
     * Gets the codMs4 value for this ClientesExportar2OutCliente.
     * 
     * @return codMs4
     */
    public java.lang.Integer getCodMs4() {
        return codMs4;
    }


    /**
     * Sets the codMs4 value for this ClientesExportar2OutCliente.
     * 
     * @param codMs4
     */
    public void setCodMs4(java.lang.Integer codMs4) {
        this.codMs4 = codMs4;
    }


    /**
     * Gets the codMsg value for this ClientesExportar2OutCliente.
     * 
     * @return codMsg
     */
    public java.lang.Integer getCodMsg() {
        return codMsg;
    }


    /**
     * Sets the codMsg value for this ClientesExportar2OutCliente.
     * 
     * @param codMsg
     */
    public void setCodMsg(java.lang.Integer codMsg) {
        this.codMsg = codMsg;
    }


    /**
     * Gets the codPai value for this ClientesExportar2OutCliente.
     * 
     * @return codPai
     */
    public java.lang.String getCodPai() {
        return codPai;
    }


    /**
     * Sets the codPai value for this ClientesExportar2OutCliente.
     * 
     * @param codPai
     */
    public void setCodPai(java.lang.String codPai) {
        this.codPai = codPai;
    }


    /**
     * Gets the codPdv value for this ClientesExportar2OutCliente.
     * 
     * @return codPdv
     */
    public java.lang.Integer getCodPdv() {
        return codPdv;
    }


    /**
     * Sets the codPdv value for this ClientesExportar2OutCliente.
     * 
     * @param codPdv
     */
    public void setCodPdv(java.lang.Integer codPdv) {
        this.codPdv = codPdv;
    }


    /**
     * Gets the codRam value for this ClientesExportar2OutCliente.
     * 
     * @return codRam
     */
    public java.lang.String getCodRam() {
        return codRam;
    }


    /**
     * Sets the codRam value for this ClientesExportar2OutCliente.
     * 
     * @param codRam
     */
    public void setCodRam(java.lang.String codRam) {
        this.codRam = codRam;
    }


    /**
     * Gets the codRoe value for this ClientesExportar2OutCliente.
     * 
     * @return codRoe
     */
    public java.lang.String getCodRoe() {
        return codRoe;
    }


    /**
     * Sets the codRoe value for this ClientesExportar2OutCliente.
     * 
     * @param codRoe
     */
    public void setCodRoe(java.lang.String codRoe) {
        this.codRoe = codRoe;
    }


    /**
     * Gets the codRtr value for this ClientesExportar2OutCliente.
     * 
     * @return codRtr
     */
    public java.lang.Integer getCodRtr() {
        return codRtr;
    }


    /**
     * Sets the codRtr value for this ClientesExportar2OutCliente.
     * 
     * @param codRtr
     */
    public void setCodRtr(java.lang.Integer codRtr) {
        this.codRtr = codRtr;
    }


    /**
     * Gets the codSab value for this ClientesExportar2OutCliente.
     * 
     * @return codSab
     */
    public java.lang.String getCodSab() {
        return codSab;
    }


    /**
     * Sets the codSab value for this ClientesExportar2OutCliente.
     * 
     * @param codSab
     */
    public void setCodSab(java.lang.String codSab) {
        this.codSab = codSab;
    }


    /**
     * Gets the codSro value for this ClientesExportar2OutCliente.
     * 
     * @return codSro
     */
    public java.lang.String getCodSro() {
        return codSro;
    }


    /**
     * Sets the codSro value for this ClientesExportar2OutCliente.
     * 
     * @param codSro
     */
    public void setCodSro(java.lang.String codSro) {
        this.codSro = codSro;
    }


    /**
     * Gets the codSuf value for this ClientesExportar2OutCliente.
     * 
     * @return codSuf
     */
    public java.lang.String getCodSuf() {
        return codSuf;
    }


    /**
     * Sets the codSuf value for this ClientesExportar2OutCliente.
     * 
     * @param codSuf
     */
    public void setCodSuf(java.lang.String codSuf) {
        this.codSuf = codSuf;
    }


    /**
     * Gets the conFin value for this ClientesExportar2OutCliente.
     * 
     * @return conFin
     */
    public java.lang.String getConFin() {
        return conFin;
    }


    /**
     * Sets the conFin value for this ClientesExportar2OutCliente.
     * 
     * @param conFin
     */
    public void setConFin(java.lang.String conFin) {
        this.conFin = conFin;
    }


    /**
     * Gets the contatos value for this ClientesExportar2OutCliente.
     * 
     * @return contatos
     */
    public br.com.senior.services.co.cad.clientes.ClientesExportar2OutClienteContatos[] getContatos() {
        return contatos;
    }


    /**
     * Sets the contatos value for this ClientesExportar2OutCliente.
     * 
     * @param contatos
     */
    public void setContatos(br.com.senior.services.co.cad.clientes.ClientesExportar2OutClienteContatos[] contatos) {
        this.contatos = contatos;
    }

    public br.com.senior.services.co.cad.clientes.ClientesExportar2OutClienteContatos getContatos(int i) {
        return this.contatos[i];
    }

    public void setContatos(int i, br.com.senior.services.co.cad.clientes.ClientesExportar2OutClienteContatos _value) {
        this.contatos[i] = _value;
    }


    /**
     * Gets the cplCob value for this ClientesExportar2OutCliente.
     * 
     * @return cplCob
     */
    public java.lang.String getCplCob() {
        return cplCob;
    }


    /**
     * Sets the cplCob value for this ClientesExportar2OutCliente.
     * 
     * @param cplCob
     */
    public void setCplCob(java.lang.String cplCob) {
        this.cplCob = cplCob;
    }


    /**
     * Gets the cplEnd value for this ClientesExportar2OutCliente.
     * 
     * @return cplEnd
     */
    public java.lang.String getCplEnd() {
        return cplEnd;
    }


    /**
     * Sets the cplEnd value for this ClientesExportar2OutCliente.
     * 
     * @param cplEnd
     */
    public void setCplEnd(java.lang.String cplEnd) {
        this.cplEnd = cplEnd;
    }


    /**
     * Gets the cplEnt value for this ClientesExportar2OutCliente.
     * 
     * @return cplEnt
     */
    public java.lang.String getCplEnt() {
        return cplEnt;
    }


    /**
     * Sets the cplEnt value for this ClientesExportar2OutCliente.
     * 
     * @param cplEnt
     */
    public void setCplEnt(java.lang.String cplEnt) {
        this.cplEnt = cplEnt;
    }


    /**
     * Gets the cxaPst value for this ClientesExportar2OutCliente.
     * 
     * @return cxaPst
     */
    public java.lang.Integer getCxaPst() {
        return cxaPst;
    }


    /**
     * Sets the cxaPst value for this ClientesExportar2OutCliente.
     * 
     * @param cxaPst
     */
    public void setCxaPst(java.lang.Integer cxaPst) {
        this.cxaPst = cxaPst;
    }


    /**
     * Gets the datAtu value for this ClientesExportar2OutCliente.
     * 
     * @return datAtu
     */
    public java.lang.String getDatAtu() {
        return datAtu;
    }


    /**
     * Sets the datAtu value for this ClientesExportar2OutCliente.
     * 
     * @param datAtu
     */
    public void setDatAtu(java.lang.String datAtu) {
        this.datAtu = datAtu;
    }


    /**
     * Gets the datCad value for this ClientesExportar2OutCliente.
     * 
     * @return datCad
     */
    public java.lang.String getDatCad() {
        return datCad;
    }


    /**
     * Sets the datCad value for this ClientesExportar2OutCliente.
     * 
     * @param datCad
     */
    public void setDatCad(java.lang.String datCad) {
        this.datCad = datCad;
    }


    /**
     * Gets the datFim value for this ClientesExportar2OutCliente.
     * 
     * @return datFim
     */
    public java.lang.String getDatFim() {
        return datFim;
    }


    /**
     * Sets the datFim value for this ClientesExportar2OutCliente.
     * 
     * @param datFim
     */
    public void setDatFim(java.lang.String datFim) {
        this.datFim = datFim;
    }


    /**
     * Gets the datIcv value for this ClientesExportar2OutCliente.
     * 
     * @return datIcv
     */
    public java.lang.String getDatIcv() {
        return datIcv;
    }


    /**
     * Sets the datIcv value for this ClientesExportar2OutCliente.
     * 
     * @param datIcv
     */
    public void setDatIcv(java.lang.String datIcv) {
        this.datIcv = datIcv;
    }


    /**
     * Gets the datMot value for this ClientesExportar2OutCliente.
     * 
     * @return datMot
     */
    public java.lang.String getDatMot() {
        return datMot;
    }


    /**
     * Sets the datMot value for this ClientesExportar2OutCliente.
     * 
     * @param datMot
     */
    public void setDatMot(java.lang.String datMot) {
        this.datMot = datMot;
    }


    /**
     * Gets the datPal value for this ClientesExportar2OutCliente.
     * 
     * @return datPal
     */
    public java.lang.String getDatPal() {
        return datPal;
    }


    /**
     * Sets the datPal value for this ClientesExportar2OutCliente.
     * 
     * @param datPal
     */
    public void setDatPal(java.lang.String datPal) {
        this.datPal = datPal;
    }


    /**
     * Gets the datPdv value for this ClientesExportar2OutCliente.
     * 
     * @return datPdv
     */
    public java.lang.String getDatPdv() {
        return datPdv;
    }


    /**
     * Sets the datPdv value for this ClientesExportar2OutCliente.
     * 
     * @param datPdv
     */
    public void setDatPdv(java.lang.String datPdv) {
        this.datPdv = datPdv;
    }


    /**
     * Gets the datSuf value for this ClientesExportar2OutCliente.
     * 
     * @return datSuf
     */
    public java.lang.String getDatSuf() {
        return datSuf;
    }


    /**
     * Sets the datSuf value for this ClientesExportar2OutCliente.
     * 
     * @param datSuf
     */
    public void setDatSuf(java.lang.String datSuf) {
        this.datSuf = datSuf;
    }


    /**
     * Gets the datVct value for this ClientesExportar2OutCliente.
     * 
     * @return datVct
     */
    public java.lang.String getDatVct() {
        return datVct;
    }


    /**
     * Sets the datVct value for this ClientesExportar2OutCliente.
     * 
     * @param datVct
     */
    public void setDatVct(java.lang.String datVct) {
        this.datVct = datVct;
    }


    /**
     * Gets the eenCli value for this ClientesExportar2OutCliente.
     * 
     * @return eenCli
     */
    public java.lang.String getEenCli() {
        return eenCli;
    }


    /**
     * Sets the eenCli value for this ClientesExportar2OutCliente.
     * 
     * @param eenCli
     */
    public void setEenCli(java.lang.String eenCli) {
        this.eenCli = eenCli;
    }


    /**
     * Gets the eenCob value for this ClientesExportar2OutCliente.
     * 
     * @return eenCob
     */
    public java.lang.String getEenCob() {
        return eenCob;
    }


    /**
     * Sets the eenCob value for this ClientesExportar2OutCliente.
     * 
     * @param eenCob
     */
    public void setEenCob(java.lang.String eenCob) {
        this.eenCob = eenCob;
    }


    /**
     * Gets the eenEnt value for this ClientesExportar2OutCliente.
     * 
     * @return eenEnt
     */
    public java.lang.String getEenEnt() {
        return eenEnt;
    }


    /**
     * Sets the eenEnt value for this ClientesExportar2OutCliente.
     * 
     * @param eenEnt
     */
    public void setEenEnt(java.lang.String eenEnt) {
        this.eenEnt = eenEnt;
    }


    /**
     * Gets the emaNfe value for this ClientesExportar2OutCliente.
     * 
     * @return emaNfe
     */
    public java.lang.String getEmaNfe() {
        return emaNfe;
    }


    /**
     * Sets the emaNfe value for this ClientesExportar2OutCliente.
     * 
     * @param emaNfe
     */
    public void setEmaNfe(java.lang.String emaNfe) {
        this.emaNfe = emaNfe;
    }


    /**
     * Gets the endCli value for this ClientesExportar2OutCliente.
     * 
     * @return endCli
     */
    public java.lang.String getEndCli() {
        return endCli;
    }


    /**
     * Sets the endCli value for this ClientesExportar2OutCliente.
     * 
     * @param endCli
     */
    public void setEndCli(java.lang.String endCli) {
        this.endCli = endCli;
    }


    /**
     * Gets the endCob value for this ClientesExportar2OutCliente.
     * 
     * @return endCob
     */
    public java.lang.String getEndCob() {
        return endCob;
    }


    /**
     * Sets the endCob value for this ClientesExportar2OutCliente.
     * 
     * @param endCob
     */
    public void setEndCob(java.lang.String endCob) {
        this.endCob = endCob;
    }


    /**
     * Gets the endEnt value for this ClientesExportar2OutCliente.
     * 
     * @return endEnt
     */
    public java.lang.String getEndEnt() {
        return endEnt;
    }


    /**
     * Sets the endEnt value for this ClientesExportar2OutCliente.
     * 
     * @param endEnt
     */
    public void setEndEnt(java.lang.String endEnt) {
        this.endEnt = endEnt;
    }


    /**
     * Gets the entCor value for this ClientesExportar2OutCliente.
     * 
     * @return entCor
     */
    public java.lang.String getEntCor() {
        return entCor;
    }


    /**
     * Sets the entCor value for this ClientesExportar2OutCliente.
     * 
     * @param entCor
     */
    public void setEntCor(java.lang.String entCor) {
        this.entCor = entCor;
    }


    /**
     * Gets the estCob value for this ClientesExportar2OutCliente.
     * 
     * @return estCob
     */
    public java.lang.String getEstCob() {
        return estCob;
    }


    /**
     * Sets the estCob value for this ClientesExportar2OutCliente.
     * 
     * @param estCob
     */
    public void setEstCob(java.lang.String estCob) {
        this.estCob = estCob;
    }


    /**
     * Gets the estEnt value for this ClientesExportar2OutCliente.
     * 
     * @return estEnt
     */
    public java.lang.String getEstEnt() {
        return estEnt;
    }


    /**
     * Sets the estEnt value for this ClientesExportar2OutCliente.
     * 
     * @param estEnt
     */
    public void setEstEnt(java.lang.String estEnt) {
        this.estEnt = estEnt;
    }


    /**
     * Gets the faxCli value for this ClientesExportar2OutCliente.
     * 
     * @return faxCli
     */
    public java.lang.String getFaxCli() {
        return faxCli;
    }


    /**
     * Sets the faxCli value for this ClientesExportar2OutCliente.
     * 
     * @param faxCli
     */
    public void setFaxCli(java.lang.String faxCli) {
        this.faxCli = faxCli;
    }


    /**
     * Gets the fonCl2 value for this ClientesExportar2OutCliente.
     * 
     * @return fonCl2
     */
    public java.lang.String getFonCl2() {
        return fonCl2;
    }


    /**
     * Sets the fonCl2 value for this ClientesExportar2OutCliente.
     * 
     * @param fonCl2
     */
    public void setFonCl2(java.lang.String fonCl2) {
        this.fonCl2 = fonCl2;
    }


    /**
     * Gets the fonCl3 value for this ClientesExportar2OutCliente.
     * 
     * @return fonCl3
     */
    public java.lang.String getFonCl3() {
        return fonCl3;
    }


    /**
     * Sets the fonCl3 value for this ClientesExportar2OutCliente.
     * 
     * @param fonCl3
     */
    public void setFonCl3(java.lang.String fonCl3) {
        this.fonCl3 = fonCl3;
    }


    /**
     * Gets the fonCl4 value for this ClientesExportar2OutCliente.
     * 
     * @return fonCl4
     */
    public java.lang.String getFonCl4() {
        return fonCl4;
    }


    /**
     * Sets the fonCl4 value for this ClientesExportar2OutCliente.
     * 
     * @param fonCl4
     */
    public void setFonCl4(java.lang.String fonCl4) {
        this.fonCl4 = fonCl4;
    }


    /**
     * Gets the fonCl5 value for this ClientesExportar2OutCliente.
     * 
     * @return fonCl5
     */
    public java.lang.String getFonCl5() {
        return fonCl5;
    }


    /**
     * Sets the fonCl5 value for this ClientesExportar2OutCliente.
     * 
     * @param fonCl5
     */
    public void setFonCl5(java.lang.String fonCl5) {
        this.fonCl5 = fonCl5;
    }


    /**
     * Gets the fonCli value for this ClientesExportar2OutCliente.
     * 
     * @return fonCli
     */
    public java.lang.String getFonCli() {
        return fonCli;
    }


    /**
     * Sets the fonCli value for this ClientesExportar2OutCliente.
     * 
     * @param fonCli
     */
    public void setFonCli(java.lang.String fonCli) {
        this.fonCli = fonCli;
    }


    /**
     * Gets the historico value for this ClientesExportar2OutCliente.
     * 
     * @return historico
     */
    public br.com.senior.services.co.cad.clientes.ClientesExportar2OutClienteHistorico[] getHistorico() {
        return historico;
    }


    /**
     * Sets the historico value for this ClientesExportar2OutCliente.
     * 
     * @param historico
     */
    public void setHistorico(br.com.senior.services.co.cad.clientes.ClientesExportar2OutClienteHistorico[] historico) {
        this.historico = historico;
    }

    public br.com.senior.services.co.cad.clientes.ClientesExportar2OutClienteHistorico getHistorico(int i) {
        return this.historico[i];
    }

    public void setHistorico(int i, br.com.senior.services.co.cad.clientes.ClientesExportar2OutClienteHistorico _value) {
        this.historico[i] = _value;
    }


    /**
     * Gets the horAtu value for this ClientesExportar2OutCliente.
     * 
     * @return horAtu
     */
    public java.lang.Integer getHorAtu() {
        return horAtu;
    }


    /**
     * Sets the horAtu value for this ClientesExportar2OutCliente.
     * 
     * @param horAtu
     */
    public void setHorAtu(java.lang.Integer horAtu) {
        this.horAtu = horAtu;
    }


    /**
     * Gets the horCad value for this ClientesExportar2OutCliente.
     * 
     * @return horCad
     */
    public java.lang.Integer getHorCad() {
        return horCad;
    }


    /**
     * Sets the horCad value for this ClientesExportar2OutCliente.
     * 
     * @param horCad
     */
    public void setHorCad(java.lang.Integer horCad) {
        this.horCad = horCad;
    }


    /**
     * Gets the horFim value for this ClientesExportar2OutCliente.
     * 
     * @return horFim
     */
    public java.lang.Integer getHorFim() {
        return horFim;
    }


    /**
     * Sets the horFim value for this ClientesExportar2OutCliente.
     * 
     * @param horFim
     */
    public void setHorFim(java.lang.Integer horFim) {
        this.horFim = horFim;
    }


    /**
     * Gets the horMot value for this ClientesExportar2OutCliente.
     * 
     * @return horMot
     */
    public java.lang.Integer getHorMot() {
        return horMot;
    }


    /**
     * Sets the horMot value for this ClientesExportar2OutCliente.
     * 
     * @param horMot
     */
    public void setHorMot(java.lang.Integer horMot) {
        this.horMot = horMot;
    }


    /**
     * Gets the horPal value for this ClientesExportar2OutCliente.
     * 
     * @return horPal
     */
    public java.lang.Integer getHorPal() {
        return horPal;
    }


    /**
     * Sets the horPal value for this ClientesExportar2OutCliente.
     * 
     * @param horPal
     */
    public void setHorPal(java.lang.Integer horPal) {
        this.horPal = horPal;
    }


    /**
     * Gets the horPdv value for this ClientesExportar2OutCliente.
     * 
     * @return horPdv
     */
    public java.lang.Integer getHorPdv() {
        return horPdv;
    }


    /**
     * Sets the horPdv value for this ClientesExportar2OutCliente.
     * 
     * @param horPdv
     */
    public void setHorPdv(java.lang.Integer horPdv) {
        this.horPdv = horPdv;
    }


    /**
     * Gets the ideCli value for this ClientesExportar2OutCliente.
     * 
     * @return ideCli
     */
    public java.lang.String getIdeCli() {
        return ideCli;
    }


    /**
     * Sets the ideCli value for this ClientesExportar2OutCliente.
     * 
     * @param ideCli
     */
    public void setIdeCli(java.lang.String ideCli) {
        this.ideCli = ideCli;
    }


    /**
     * Gets the indCoo value for this ClientesExportar2OutCliente.
     * 
     * @return indCoo
     */
    public java.lang.String getIndCoo() {
        return indCoo;
    }


    /**
     * Sets the indCoo value for this ClientesExportar2OutCliente.
     * 
     * @param indCoo
     */
    public void setIndCoo(java.lang.String indCoo) {
        this.indCoo = indCoo;
    }


    /**
     * Gets the indExp value for this ClientesExportar2OutCliente.
     * 
     * @return indExp
     */
    public java.lang.Integer getIndExp() {
        return indExp;
    }


    /**
     * Sets the indExp value for this ClientesExportar2OutCliente.
     * 
     * @param indExp
     */
    public void setIndExp(java.lang.Integer indExp) {
        this.indExp = indExp;
    }


    /**
     * Gets the iniCob value for this ClientesExportar2OutCliente.
     * 
     * @return iniCob
     */
    public java.lang.Integer getIniCob() {
        return iniCob;
    }


    /**
     * Sets the iniCob value for this ClientesExportar2OutCliente.
     * 
     * @param iniCob
     */
    public void setIniCob(java.lang.Integer iniCob) {
        this.iniCob = iniCob;
    }


    /**
     * Gets the iniEnt value for this ClientesExportar2OutCliente.
     * 
     * @return iniEnt
     */
    public java.lang.Integer getIniEnt() {
        return iniEnt;
    }


    /**
     * Sets the iniEnt value for this ClientesExportar2OutCliente.
     * 
     * @param iniEnt
     */
    public void setIniEnt(java.lang.Integer iniEnt) {
        this.iniEnt = iniEnt;
    }


    /**
     * Gets the insAnp value for this ClientesExportar2OutCliente.
     * 
     * @return insAnp
     */
    public java.lang.Integer getInsAnp() {
        return insAnp;
    }


    /**
     * Sets the insAnp value for this ClientesExportar2OutCliente.
     * 
     * @param insAnp
     */
    public void setInsAnp(java.lang.Integer insAnp) {
        this.insAnp = insAnp;
    }


    /**
     * Gets the insEnt value for this ClientesExportar2OutCliente.
     * 
     * @return insEnt
     */
    public java.lang.String getInsEnt() {
        return insEnt;
    }


    /**
     * Sets the insEnt value for this ClientesExportar2OutCliente.
     * 
     * @param insEnt
     */
    public void setInsEnt(java.lang.String insEnt) {
        this.insEnt = insEnt;
    }


    /**
     * Gets the insEst value for this ClientesExportar2OutCliente.
     * 
     * @return insEst
     */
    public java.lang.String getInsEst() {
        return insEst;
    }


    /**
     * Sets the insEst value for this ClientesExportar2OutCliente.
     * 
     * @param insEst
     */
    public void setInsEst(java.lang.String insEst) {
        this.insEst = insEst;
    }


    /**
     * Gets the insMun value for this ClientesExportar2OutCliente.
     * 
     * @return insMun
     */
    public java.lang.String getInsMun() {
        return insMun;
    }


    /**
     * Sets the insMun value for this ClientesExportar2OutCliente.
     * 
     * @param insMun
     */
    public void setInsMun(java.lang.String insMun) {
        this.insMun = insMun;
    }


    /**
     * Gets the intNet value for this ClientesExportar2OutCliente.
     * 
     * @return intNet
     */
    public java.lang.String getIntNet() {
        return intNet;
    }


    /**
     * Sets the intNet value for this ClientesExportar2OutCliente.
     * 
     * @param intNet
     */
    public void setIntNet(java.lang.String intNet) {
        this.intNet = intNet;
    }


    /**
     * Gets the limRet value for this ClientesExportar2OutCliente.
     * 
     * @return limRet
     */
    public java.lang.String getLimRet() {
        return limRet;
    }


    /**
     * Sets the limRet value for this ClientesExportar2OutCliente.
     * 
     * @param limRet
     */
    public void setLimRet(java.lang.String limRet) {
        this.limRet = limRet;
    }


    /**
     * Gets the marCli value for this ClientesExportar2OutCliente.
     * 
     * @return marCli
     */
    public java.lang.String getMarCli() {
        return marCli;
    }


    /**
     * Sets the marCli value for this ClientesExportar2OutCliente.
     * 
     * @param marCli
     */
    public void setMarCli(java.lang.String marCli) {
        this.marCli = marCli;
    }


    /**
     * Gets the natCof value for this ClientesExportar2OutCliente.
     * 
     * @return natCof
     */
    public java.lang.Integer getNatCof() {
        return natCof;
    }


    /**
     * Sets the natCof value for this ClientesExportar2OutCliente.
     * 
     * @param natCof
     */
    public void setNatCof(java.lang.Integer natCof) {
        this.natCof = natCof;
    }


    /**
     * Gets the natPis value for this ClientesExportar2OutCliente.
     * 
     * @return natPis
     */
    public java.lang.Integer getNatPis() {
        return natPis;
    }


    /**
     * Sets the natPis value for this ClientesExportar2OutCliente.
     * 
     * @param natPis
     */
    public void setNatPis(java.lang.Integer natPis) {
        this.natPis = natPis;
    }


    /**
     * Gets the natRet value for this ClientesExportar2OutCliente.
     * 
     * @return natRet
     */
    public java.lang.Integer getNatRet() {
        return natRet;
    }


    /**
     * Sets the natRet value for this ClientesExportar2OutCliente.
     * 
     * @param natRet
     */
    public void setNatRet(java.lang.Integer natRet) {
        this.natRet = natRet;
    }


    /**
     * Gets the nenCli value for this ClientesExportar2OutCliente.
     * 
     * @return nenCli
     */
    public java.lang.String getNenCli() {
        return nenCli;
    }


    /**
     * Sets the nenCli value for this ClientesExportar2OutCliente.
     * 
     * @param nenCli
     */
    public void setNenCli(java.lang.String nenCli) {
        this.nenCli = nenCli;
    }


    /**
     * Gets the nenCob value for this ClientesExportar2OutCliente.
     * 
     * @return nenCob
     */
    public java.lang.String getNenCob() {
        return nenCob;
    }


    /**
     * Sets the nenCob value for this ClientesExportar2OutCliente.
     * 
     * @param nenCob
     */
    public void setNenCob(java.lang.String nenCob) {
        this.nenCob = nenCob;
    }


    /**
     * Gets the nenEnt value for this ClientesExportar2OutCliente.
     * 
     * @return nenEnt
     */
    public java.lang.String getNenEnt() {
        return nenEnt;
    }


    /**
     * Sets the nenEnt value for this ClientesExportar2OutCliente.
     * 
     * @param nenEnt
     */
    public void setNenEnt(java.lang.String nenEnt) {
        this.nenEnt = nenEnt;
    }


    /**
     * Gets the nomCli value for this ClientesExportar2OutCliente.
     * 
     * @return nomCli
     */
    public java.lang.String getNomCli() {
        return nomCli;
    }


    /**
     * Sets the nomCli value for this ClientesExportar2OutCliente.
     * 
     * @param nomCli
     */
    public void setNomCli(java.lang.String nomCli) {
        this.nomCli = nomCli;
    }


    /**
     * Gets the numAnx value for this ClientesExportar2OutCliente.
     * 
     * @return numAnx
     */
    public java.lang.Double getNumAnx() {
        return numAnx;
    }


    /**
     * Sets the numAnx value for this ClientesExportar2OutCliente.
     * 
     * @param numAnx
     */
    public void setNumAnx(java.lang.Double numAnx) {
        this.numAnx = numAnx;
    }


    /**
     * Gets the numIdf value for this ClientesExportar2OutCliente.
     * 
     * @return numIdf
     */
    public java.lang.String getNumIdf() {
        return numIdf;
    }


    /**
     * Sets the numIdf value for this ClientesExportar2OutCliente.
     * 
     * @param numIdf
     */
    public void setNumIdf(java.lang.String numIdf) {
        this.numIdf = numIdf;
    }


    /**
     * Gets the obsMot value for this ClientesExportar2OutCliente.
     * 
     * @return obsMot
     */
    public java.lang.String getObsMot() {
        return obsMot;
    }


    /**
     * Sets the obsMot value for this ClientesExportar2OutCliente.
     * 
     * @param obsMot
     */
    public void setObsMot(java.lang.String obsMot) {
        this.obsMot = obsMot;
    }


    /**
     * Gets the perAin value for this ClientesExportar2OutCliente.
     * 
     * @return perAin
     */
    public java.lang.Double getPerAin() {
        return perAin;
    }


    /**
     * Sets the perAin value for this ClientesExportar2OutCliente.
     * 
     * @param perAin
     */
    public void setPerAin(java.lang.Double perAin) {
        this.perAin = perAin;
    }


    /**
     * Gets the qtdAtu value for this ClientesExportar2OutCliente.
     * 
     * @return qtdAtu
     */
    public java.lang.Integer getQtdAtu() {
        return qtdAtu;
    }


    /**
     * Sets the qtdAtu value for this ClientesExportar2OutCliente.
     * 
     * @param qtdAtu
     */
    public void setQtdAtu(java.lang.Integer qtdAtu) {
        this.qtdAtu = qtdAtu;
    }


    /**
     * Gets the regEst value for this ClientesExportar2OutCliente.
     * 
     * @return regEst
     */
    public java.lang.Integer getRegEst() {
        return regEst;
    }


    /**
     * Sets the regEst value for this ClientesExportar2OutCliente.
     * 
     * @param regEst
     */
    public void setRegEst(java.lang.Integer regEst) {
        this.regEst = regEst;
    }


    /**
     * Gets the retCof value for this ClientesExportar2OutCliente.
     * 
     * @return retCof
     */
    public java.lang.String getRetCof() {
        return retCof;
    }


    /**
     * Sets the retCof value for this ClientesExportar2OutCliente.
     * 
     * @param retCof
     */
    public void setRetCof(java.lang.String retCof) {
        this.retCof = retCof;
    }


    /**
     * Gets the retCsl value for this ClientesExportar2OutCliente.
     * 
     * @return retCsl
     */
    public java.lang.String getRetCsl() {
        return retCsl;
    }


    /**
     * Sets the retCsl value for this ClientesExportar2OutCliente.
     * 
     * @param retCsl
     */
    public void setRetCsl(java.lang.String retCsl) {
        this.retCsl = retCsl;
    }


    /**
     * Gets the retIrf value for this ClientesExportar2OutCliente.
     * 
     * @return retIrf
     */
    public java.lang.String getRetIrf() {
        return retIrf;
    }


    /**
     * Sets the retIrf value for this ClientesExportar2OutCliente.
     * 
     * @param retIrf
     */
    public void setRetIrf(java.lang.String retIrf) {
        this.retIrf = retIrf;
    }


    /**
     * Gets the retOur value for this ClientesExportar2OutCliente.
     * 
     * @return retOur
     */
    public java.lang.String getRetOur() {
        return retOur;
    }


    /**
     * Sets the retOur value for this ClientesExportar2OutCliente.
     * 
     * @param retOur
     */
    public void setRetOur(java.lang.String retOur) {
        this.retOur = retOur;
    }


    /**
     * Gets the retPis value for this ClientesExportar2OutCliente.
     * 
     * @return retPis
     */
    public java.lang.String getRetPis() {
        return retPis;
    }


    /**
     * Sets the retPis value for this ClientesExportar2OutCliente.
     * 
     * @param retPis
     */
    public void setRetPis(java.lang.String retPis) {
        this.retPis = retPis;
    }


    /**
     * Gets the retPro value for this ClientesExportar2OutCliente.
     * 
     * @return retPro
     */
    public java.lang.String getRetPro() {
        return retPro;
    }


    /**
     * Sets the retPro value for this ClientesExportar2OutCliente.
     * 
     * @param retPro
     */
    public void setRetPro(java.lang.String retPro) {
        this.retPro = retPro;
    }


    /**
     * Gets the rotAnx value for this ClientesExportar2OutCliente.
     * 
     * @return rotAnx
     */
    public java.lang.Integer getRotAnx() {
        return rotAnx;
    }


    /**
     * Sets the rotAnx value for this ClientesExportar2OutCliente.
     * 
     * @param rotAnx
     */
    public void setRotAnx(java.lang.Integer rotAnx) {
        this.rotAnx = rotAnx;
    }


    /**
     * Gets the senCli value for this ClientesExportar2OutCliente.
     * 
     * @return senCli
     */
    public java.lang.String getSenCli() {
        return senCli;
    }


    /**
     * Sets the senCli value for this ClientesExportar2OutCliente.
     * 
     * @param senCli
     */
    public void setSenCli(java.lang.String senCli) {
        this.senCli = senCli;
    }


    /**
     * Gets the seqInt value for this ClientesExportar2OutCliente.
     * 
     * @return seqInt
     */
    public java.lang.Integer getSeqInt() {
        return seqInt;
    }


    /**
     * Sets the seqInt value for this ClientesExportar2OutCliente.
     * 
     * @param seqInt
     */
    public void setSeqInt(java.lang.Integer seqInt) {
        this.seqInt = seqInt;
    }


    /**
     * Gets the seqRoe value for this ClientesExportar2OutCliente.
     * 
     * @return seqRoe
     */
    public java.lang.Integer getSeqRoe() {
        return seqRoe;
    }


    /**
     * Sets the seqRoe value for this ClientesExportar2OutCliente.
     * 
     * @param seqRoe
     */
    public void setSeqRoe(java.lang.Integer seqRoe) {
        this.seqRoe = seqRoe;
    }


    /**
     * Gets the sigUfs value for this ClientesExportar2OutCliente.
     * 
     * @return sigUfs
     */
    public java.lang.String getSigUfs() {
        return sigUfs;
    }


    /**
     * Sets the sigUfs value for this ClientesExportar2OutCliente.
     * 
     * @param sigUfs
     */
    public void setSigUfs(java.lang.String sigUfs) {
        this.sigUfs = sigUfs;
    }


    /**
     * Gets the sitCli value for this ClientesExportar2OutCliente.
     * 
     * @return sitCli
     */
    public java.lang.String getSitCli() {
        return sitCli;
    }


    /**
     * Sets the sitCli value for this ClientesExportar2OutCliente.
     * 
     * @param sitCli
     */
    public void setSitCli(java.lang.String sitCli) {
        this.sitCli = sitCli;
    }


    /**
     * Gets the temCob value for this ClientesExportar2OutCliente.
     * 
     * @return temCob
     */
    public java.lang.String getTemCob() {
        return temCob;
    }


    /**
     * Sets the temCob value for this ClientesExportar2OutCliente.
     * 
     * @param temCob
     */
    public void setTemCob(java.lang.String temCob) {
        this.temCob = temCob;
    }


    /**
     * Gets the temEnt value for this ClientesExportar2OutCliente.
     * 
     * @return temEnt
     */
    public java.lang.String getTemEnt() {
        return temEnt;
    }


    /**
     * Sets the temEnt value for this ClientesExportar2OutCliente.
     * 
     * @param temEnt
     */
    public void setTemEnt(java.lang.String temEnt) {
        this.temEnt = temEnt;
    }


    /**
     * Gets the tipAce value for this ClientesExportar2OutCliente.
     * 
     * @return tipAce
     */
    public java.lang.Integer getTipAce() {
        return tipAce;
    }


    /**
     * Sets the tipAce value for this ClientesExportar2OutCliente.
     * 
     * @param tipAce
     */
    public void setTipAce(java.lang.Integer tipAce) {
        this.tipAce = tipAce;
    }


    /**
     * Gets the tipCli value for this ClientesExportar2OutCliente.
     * 
     * @return tipCli
     */
    public java.lang.String getTipCli() {
        return tipCli;
    }


    /**
     * Sets the tipCli value for this ClientesExportar2OutCliente.
     * 
     * @param tipCli
     */
    public void setTipCli(java.lang.String tipCli) {
        this.tipCli = tipCli;
    }


    /**
     * Gets the tipEmc value for this ClientesExportar2OutCliente.
     * 
     * @return tipEmc
     */
    public java.lang.Integer getTipEmc() {
        return tipEmc;
    }


    /**
     * Sets the tipEmc value for this ClientesExportar2OutCliente.
     * 
     * @param tipEmc
     */
    public void setTipEmc(java.lang.Integer tipEmc) {
        this.tipEmc = tipEmc;
    }


    /**
     * Gets the tipEmp value for this ClientesExportar2OutCliente.
     * 
     * @return tipEmp
     */
    public java.lang.Integer getTipEmp() {
        return tipEmp;
    }


    /**
     * Sets the tipEmp value for this ClientesExportar2OutCliente.
     * 
     * @param tipEmp
     */
    public void setTipEmp(java.lang.Integer tipEmp) {
        this.tipEmp = tipEmp;
    }


    /**
     * Gets the tipMer value for this ClientesExportar2OutCliente.
     * 
     * @return tipMer
     */
    public java.lang.String getTipMer() {
        return tipMer;
    }


    /**
     * Sets the tipMer value for this ClientesExportar2OutCliente.
     * 
     * @param tipMer
     */
    public void setTipMer(java.lang.String tipMer) {
        this.tipMer = tipMer;
    }


    /**
     * Gets the triCof value for this ClientesExportar2OutCliente.
     * 
     * @return triCof
     */
    public java.lang.String getTriCof() {
        return triCof;
    }


    /**
     * Sets the triCof value for this ClientesExportar2OutCliente.
     * 
     * @param triCof
     */
    public void setTriCof(java.lang.String triCof) {
        this.triCof = triCof;
    }


    /**
     * Gets the triIcm value for this ClientesExportar2OutCliente.
     * 
     * @return triIcm
     */
    public java.lang.String getTriIcm() {
        return triIcm;
    }


    /**
     * Sets the triIcm value for this ClientesExportar2OutCliente.
     * 
     * @param triIcm
     */
    public void setTriIcm(java.lang.String triIcm) {
        this.triIcm = triIcm;
    }


    /**
     * Gets the triIpi value for this ClientesExportar2OutCliente.
     * 
     * @return triIpi
     */
    public java.lang.String getTriIpi() {
        return triIpi;
    }


    /**
     * Sets the triIpi value for this ClientesExportar2OutCliente.
     * 
     * @param triIpi
     */
    public void setTriIpi(java.lang.String triIpi) {
        this.triIpi = triIpi;
    }


    /**
     * Gets the triPis value for this ClientesExportar2OutCliente.
     * 
     * @return triPis
     */
    public java.lang.String getTriPis() {
        return triPis;
    }


    /**
     * Sets the triPis value for this ClientesExportar2OutCliente.
     * 
     * @param triPis
     */
    public void setTriPis(java.lang.String triPis) {
        this.triPis = triPis;
    }


    /**
     * Gets the usuAtu value for this ClientesExportar2OutCliente.
     * 
     * @return usuAtu
     */
    public java.lang.Double getUsuAtu() {
        return usuAtu;
    }


    /**
     * Sets the usuAtu value for this ClientesExportar2OutCliente.
     * 
     * @param usuAtu
     */
    public void setUsuAtu(java.lang.Double usuAtu) {
        this.usuAtu = usuAtu;
    }


    /**
     * Gets the usuCad value for this ClientesExportar2OutCliente.
     * 
     * @return usuCad
     */
    public java.lang.Double getUsuCad() {
        return usuCad;
    }


    /**
     * Sets the usuCad value for this ClientesExportar2OutCliente.
     * 
     * @param usuCad
     */
    public void setUsuCad(java.lang.Double usuCad) {
        this.usuCad = usuCad;
    }


    /**
     * Gets the usuMot value for this ClientesExportar2OutCliente.
     * 
     * @return usuMot
     */
    public java.lang.Double getUsuMot() {
        return usuMot;
    }


    /**
     * Sets the usuMot value for this ClientesExportar2OutCliente.
     * 
     * @param usuMot
     */
    public void setUsuMot(java.lang.Double usuMot) {
        this.usuMot = usuMot;
    }


    /**
     * Gets the usuOpe value for this ClientesExportar2OutCliente.
     * 
     * @return usuOpe
     */
    public java.lang.Double getUsuOpe() {
        return usuOpe;
    }


    /**
     * Sets the usuOpe value for this ClientesExportar2OutCliente.
     * 
     * @param usuOpe
     */
    public void setUsuOpe(java.lang.Double usuOpe) {
        this.usuOpe = usuOpe;
    }


    /**
     * Gets the zipCod value for this ClientesExportar2OutCliente.
     * 
     * @return zipCod
     */
    public java.lang.String getZipCod() {
        return zipCod;
    }


    /**
     * Sets the zipCod value for this ClientesExportar2OutCliente.
     * 
     * @param zipCod
     */
    public void setZipCod(java.lang.String zipCod) {
        this.zipCod = zipCod;
    }


    /**
     * Gets the zonFra value for this ClientesExportar2OutCliente.
     * 
     * @return zonFra
     */
    public java.lang.Integer getZonFra() {
        return zonFra;
    }


    /**
     * Sets the zonFra value for this ClientesExportar2OutCliente.
     * 
     * @param zonFra
     */
    public void setZonFra(java.lang.Integer zonFra) {
        this.zonFra = zonFra;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientesExportar2OutCliente)) return false;
        ClientesExportar2OutCliente other = (ClientesExportar2OutCliente) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.apeCli==null && other.getApeCli()==null) || 
             (this.apeCli!=null &&
              this.apeCli.equals(other.getApeCli()))) &&
            ((this.baiCli==null && other.getBaiCli()==null) || 
             (this.baiCli!=null &&
              this.baiCli.equals(other.getBaiCli()))) &&
            ((this.baiCob==null && other.getBaiCob()==null) || 
             (this.baiCob!=null &&
              this.baiCob.equals(other.getBaiCob()))) &&
            ((this.baiEnt==null && other.getBaiEnt()==null) || 
             (this.baiEnt!=null &&
              this.baiEnt.equals(other.getBaiEnt()))) &&
            ((this.bloCre==null && other.getBloCre()==null) || 
             (this.bloCre!=null &&
              this.bloCre.equals(other.getBloCre()))) &&
            ((this.calFun==null && other.getCalFun()==null) || 
             (this.calFun!=null &&
              this.calFun.equals(other.getCalFun()))) &&
            ((this.calSen==null && other.getCalSen()==null) || 
             (this.calSen!=null &&
              this.calSen.equals(other.getCalSen()))) &&
            ((this.campoUsuarioCliente==null && other.getCampoUsuarioCliente()==null) || 
             (this.campoUsuarioCliente!=null &&
              java.util.Arrays.equals(this.campoUsuarioCliente, other.getCampoUsuarioCliente()))) &&
            ((this.cepCli==null && other.getCepCli()==null) || 
             (this.cepCli!=null &&
              this.cepCli.equals(other.getCepCli()))) &&
            ((this.cepCob==null && other.getCepCob()==null) || 
             (this.cepCob!=null &&
              this.cepCob.equals(other.getCepCob()))) &&
            ((this.cepEnt==null && other.getCepEnt()==null) || 
             (this.cepEnt!=null &&
              this.cepEnt.equals(other.getCepEnt()))) &&
            ((this.cepFre==null && other.getCepFre()==null) || 
             (this.cepFre!=null &&
              this.cepFre.equals(other.getCepFre()))) &&
            ((this.cepIni==null && other.getCepIni()==null) || 
             (this.cepIni!=null &&
              this.cepIni.equals(other.getCepIni()))) &&
            ((this.cgcCob==null && other.getCgcCob()==null) || 
             (this.cgcCob!=null &&
              this.cgcCob.equals(other.getCgcCob()))) &&
            ((this.cgcCpf==null && other.getCgcCpf()==null) || 
             (this.cgcCpf!=null &&
              this.cgcCpf.equals(other.getCgcCpf()))) &&
            ((this.cgcEnt==null && other.getCgcEnt()==null) || 
             (this.cgcEnt!=null &&
              this.cgcEnt.equals(other.getCgcEnt()))) &&
            ((this.cidCli==null && other.getCidCli()==null) || 
             (this.cidCli!=null &&
              this.cidCli.equals(other.getCidCli()))) &&
            ((this.cidCob==null && other.getCidCob()==null) || 
             (this.cidCob!=null &&
              this.cidCob.equals(other.getCidCob()))) &&
            ((this.cidEnt==null && other.getCidEnt()==null) || 
             (this.cidEnt!=null &&
              this.cidEnt.equals(other.getCidEnt()))) &&
            ((this.cliCon==null && other.getCliCon()==null) || 
             (this.cliCon!=null &&
              this.cliCon.equals(other.getCliCon()))) &&
            ((this.cliFor==null && other.getCliFor()==null) || 
             (this.cliFor!=null &&
              this.cliFor.equals(other.getCliFor()))) &&
            ((this.cliPrx==null && other.getCliPrx()==null) || 
             (this.cliPrx!=null &&
              this.cliPrx.equals(other.getCliPrx()))) &&
            ((this.cliRep==null && other.getCliRep()==null) || 
             (this.cliRep!=null &&
              this.cliRep.equals(other.getCliRep()))) &&
            ((this.cliTra==null && other.getCliTra()==null) || 
             (this.cliTra!=null &&
              this.cliTra.equals(other.getCliTra()))) &&
            ((this.codAma==null && other.getCodAma()==null) || 
             (this.codAma!=null &&
              this.codAma.equals(other.getCodAma()))) &&
            ((this.codCli==null && other.getCodCli()==null) || 
             (this.codCli!=null &&
              this.codCli.equals(other.getCodCli()))) &&
            ((this.codCnv==null && other.getCodCnv()==null) || 
             (this.codCnv!=null &&
              this.codCnv.equals(other.getCodCnv()))) &&
            ((this.codFor==null && other.getCodFor()==null) || 
             (this.codFor!=null &&
              this.codFor.equals(other.getCodFor()))) &&
            ((this.codGal==null && other.getCodGal()==null) || 
             (this.codGal!=null &&
              this.codGal.equals(other.getCodGal()))) &&
            ((this.codGre==null && other.getCodGre()==null) || 
             (this.codGre!=null &&
              this.codGre.equals(other.getCodGre()))) &&
            ((this.codMot==null && other.getCodMot()==null) || 
             (this.codMot!=null &&
              this.codMot.equals(other.getCodMot()))) &&
            ((this.codMs2==null && other.getCodMs2()==null) || 
             (this.codMs2!=null &&
              this.codMs2.equals(other.getCodMs2()))) &&
            ((this.codMs3==null && other.getCodMs3()==null) || 
             (this.codMs3!=null &&
              this.codMs3.equals(other.getCodMs3()))) &&
            ((this.codMs4==null && other.getCodMs4()==null) || 
             (this.codMs4!=null &&
              this.codMs4.equals(other.getCodMs4()))) &&
            ((this.codMsg==null && other.getCodMsg()==null) || 
             (this.codMsg!=null &&
              this.codMsg.equals(other.getCodMsg()))) &&
            ((this.codPai==null && other.getCodPai()==null) || 
             (this.codPai!=null &&
              this.codPai.equals(other.getCodPai()))) &&
            ((this.codPdv==null && other.getCodPdv()==null) || 
             (this.codPdv!=null &&
              this.codPdv.equals(other.getCodPdv()))) &&
            ((this.codRam==null && other.getCodRam()==null) || 
             (this.codRam!=null &&
              this.codRam.equals(other.getCodRam()))) &&
            ((this.codRoe==null && other.getCodRoe()==null) || 
             (this.codRoe!=null &&
              this.codRoe.equals(other.getCodRoe()))) &&
            ((this.codRtr==null && other.getCodRtr()==null) || 
             (this.codRtr!=null &&
              this.codRtr.equals(other.getCodRtr()))) &&
            ((this.codSab==null && other.getCodSab()==null) || 
             (this.codSab!=null &&
              this.codSab.equals(other.getCodSab()))) &&
            ((this.codSro==null && other.getCodSro()==null) || 
             (this.codSro!=null &&
              this.codSro.equals(other.getCodSro()))) &&
            ((this.codSuf==null && other.getCodSuf()==null) || 
             (this.codSuf!=null &&
              this.codSuf.equals(other.getCodSuf()))) &&
            ((this.conFin==null && other.getConFin()==null) || 
             (this.conFin!=null &&
              this.conFin.equals(other.getConFin()))) &&
            ((this.contatos==null && other.getContatos()==null) || 
             (this.contatos!=null &&
              java.util.Arrays.equals(this.contatos, other.getContatos()))) &&
            ((this.cplCob==null && other.getCplCob()==null) || 
             (this.cplCob!=null &&
              this.cplCob.equals(other.getCplCob()))) &&
            ((this.cplEnd==null && other.getCplEnd()==null) || 
             (this.cplEnd!=null &&
              this.cplEnd.equals(other.getCplEnd()))) &&
            ((this.cplEnt==null && other.getCplEnt()==null) || 
             (this.cplEnt!=null &&
              this.cplEnt.equals(other.getCplEnt()))) &&
            ((this.cxaPst==null && other.getCxaPst()==null) || 
             (this.cxaPst!=null &&
              this.cxaPst.equals(other.getCxaPst()))) &&
            ((this.datAtu==null && other.getDatAtu()==null) || 
             (this.datAtu!=null &&
              this.datAtu.equals(other.getDatAtu()))) &&
            ((this.datCad==null && other.getDatCad()==null) || 
             (this.datCad!=null &&
              this.datCad.equals(other.getDatCad()))) &&
            ((this.datFim==null && other.getDatFim()==null) || 
             (this.datFim!=null &&
              this.datFim.equals(other.getDatFim()))) &&
            ((this.datIcv==null && other.getDatIcv()==null) || 
             (this.datIcv!=null &&
              this.datIcv.equals(other.getDatIcv()))) &&
            ((this.datMot==null && other.getDatMot()==null) || 
             (this.datMot!=null &&
              this.datMot.equals(other.getDatMot()))) &&
            ((this.datPal==null && other.getDatPal()==null) || 
             (this.datPal!=null &&
              this.datPal.equals(other.getDatPal()))) &&
            ((this.datPdv==null && other.getDatPdv()==null) || 
             (this.datPdv!=null &&
              this.datPdv.equals(other.getDatPdv()))) &&
            ((this.datSuf==null && other.getDatSuf()==null) || 
             (this.datSuf!=null &&
              this.datSuf.equals(other.getDatSuf()))) &&
            ((this.datVct==null && other.getDatVct()==null) || 
             (this.datVct!=null &&
              this.datVct.equals(other.getDatVct()))) &&
            ((this.eenCli==null && other.getEenCli()==null) || 
             (this.eenCli!=null &&
              this.eenCli.equals(other.getEenCli()))) &&
            ((this.eenCob==null && other.getEenCob()==null) || 
             (this.eenCob!=null &&
              this.eenCob.equals(other.getEenCob()))) &&
            ((this.eenEnt==null && other.getEenEnt()==null) || 
             (this.eenEnt!=null &&
              this.eenEnt.equals(other.getEenEnt()))) &&
            ((this.emaNfe==null && other.getEmaNfe()==null) || 
             (this.emaNfe!=null &&
              this.emaNfe.equals(other.getEmaNfe()))) &&
            ((this.endCli==null && other.getEndCli()==null) || 
             (this.endCli!=null &&
              this.endCli.equals(other.getEndCli()))) &&
            ((this.endCob==null && other.getEndCob()==null) || 
             (this.endCob!=null &&
              this.endCob.equals(other.getEndCob()))) &&
            ((this.endEnt==null && other.getEndEnt()==null) || 
             (this.endEnt!=null &&
              this.endEnt.equals(other.getEndEnt()))) &&
            ((this.entCor==null && other.getEntCor()==null) || 
             (this.entCor!=null &&
              this.entCor.equals(other.getEntCor()))) &&
            ((this.estCob==null && other.getEstCob()==null) || 
             (this.estCob!=null &&
              this.estCob.equals(other.getEstCob()))) &&
            ((this.estEnt==null && other.getEstEnt()==null) || 
             (this.estEnt!=null &&
              this.estEnt.equals(other.getEstEnt()))) &&
            ((this.faxCli==null && other.getFaxCli()==null) || 
             (this.faxCli!=null &&
              this.faxCli.equals(other.getFaxCli()))) &&
            ((this.fonCl2==null && other.getFonCl2()==null) || 
             (this.fonCl2!=null &&
              this.fonCl2.equals(other.getFonCl2()))) &&
            ((this.fonCl3==null && other.getFonCl3()==null) || 
             (this.fonCl3!=null &&
              this.fonCl3.equals(other.getFonCl3()))) &&
            ((this.fonCl4==null && other.getFonCl4()==null) || 
             (this.fonCl4!=null &&
              this.fonCl4.equals(other.getFonCl4()))) &&
            ((this.fonCl5==null && other.getFonCl5()==null) || 
             (this.fonCl5!=null &&
              this.fonCl5.equals(other.getFonCl5()))) &&
            ((this.fonCli==null && other.getFonCli()==null) || 
             (this.fonCli!=null &&
              this.fonCli.equals(other.getFonCli()))) &&
            ((this.historico==null && other.getHistorico()==null) || 
             (this.historico!=null &&
              java.util.Arrays.equals(this.historico, other.getHistorico()))) &&
            ((this.horAtu==null && other.getHorAtu()==null) || 
             (this.horAtu!=null &&
              this.horAtu.equals(other.getHorAtu()))) &&
            ((this.horCad==null && other.getHorCad()==null) || 
             (this.horCad!=null &&
              this.horCad.equals(other.getHorCad()))) &&
            ((this.horFim==null && other.getHorFim()==null) || 
             (this.horFim!=null &&
              this.horFim.equals(other.getHorFim()))) &&
            ((this.horMot==null && other.getHorMot()==null) || 
             (this.horMot!=null &&
              this.horMot.equals(other.getHorMot()))) &&
            ((this.horPal==null && other.getHorPal()==null) || 
             (this.horPal!=null &&
              this.horPal.equals(other.getHorPal()))) &&
            ((this.horPdv==null && other.getHorPdv()==null) || 
             (this.horPdv!=null &&
              this.horPdv.equals(other.getHorPdv()))) &&
            ((this.ideCli==null && other.getIdeCli()==null) || 
             (this.ideCli!=null &&
              this.ideCli.equals(other.getIdeCli()))) &&
            ((this.indCoo==null && other.getIndCoo()==null) || 
             (this.indCoo!=null &&
              this.indCoo.equals(other.getIndCoo()))) &&
            ((this.indExp==null && other.getIndExp()==null) || 
             (this.indExp!=null &&
              this.indExp.equals(other.getIndExp()))) &&
            ((this.iniCob==null && other.getIniCob()==null) || 
             (this.iniCob!=null &&
              this.iniCob.equals(other.getIniCob()))) &&
            ((this.iniEnt==null && other.getIniEnt()==null) || 
             (this.iniEnt!=null &&
              this.iniEnt.equals(other.getIniEnt()))) &&
            ((this.insAnp==null && other.getInsAnp()==null) || 
             (this.insAnp!=null &&
              this.insAnp.equals(other.getInsAnp()))) &&
            ((this.insEnt==null && other.getInsEnt()==null) || 
             (this.insEnt!=null &&
              this.insEnt.equals(other.getInsEnt()))) &&
            ((this.insEst==null && other.getInsEst()==null) || 
             (this.insEst!=null &&
              this.insEst.equals(other.getInsEst()))) &&
            ((this.insMun==null && other.getInsMun()==null) || 
             (this.insMun!=null &&
              this.insMun.equals(other.getInsMun()))) &&
            ((this.intNet==null && other.getIntNet()==null) || 
             (this.intNet!=null &&
              this.intNet.equals(other.getIntNet()))) &&
            ((this.limRet==null && other.getLimRet()==null) || 
             (this.limRet!=null &&
              this.limRet.equals(other.getLimRet()))) &&
            ((this.marCli==null && other.getMarCli()==null) || 
             (this.marCli!=null &&
              this.marCli.equals(other.getMarCli()))) &&
            ((this.natCof==null && other.getNatCof()==null) || 
             (this.natCof!=null &&
              this.natCof.equals(other.getNatCof()))) &&
            ((this.natPis==null && other.getNatPis()==null) || 
             (this.natPis!=null &&
              this.natPis.equals(other.getNatPis()))) &&
            ((this.natRet==null && other.getNatRet()==null) || 
             (this.natRet!=null &&
              this.natRet.equals(other.getNatRet()))) &&
            ((this.nenCli==null && other.getNenCli()==null) || 
             (this.nenCli!=null &&
              this.nenCli.equals(other.getNenCli()))) &&
            ((this.nenCob==null && other.getNenCob()==null) || 
             (this.nenCob!=null &&
              this.nenCob.equals(other.getNenCob()))) &&
            ((this.nenEnt==null && other.getNenEnt()==null) || 
             (this.nenEnt!=null &&
              this.nenEnt.equals(other.getNenEnt()))) &&
            ((this.nomCli==null && other.getNomCli()==null) || 
             (this.nomCli!=null &&
              this.nomCli.equals(other.getNomCli()))) &&
            ((this.numAnx==null && other.getNumAnx()==null) || 
             (this.numAnx!=null &&
              this.numAnx.equals(other.getNumAnx()))) &&
            ((this.numIdf==null && other.getNumIdf()==null) || 
             (this.numIdf!=null &&
              this.numIdf.equals(other.getNumIdf()))) &&
            ((this.obsMot==null && other.getObsMot()==null) || 
             (this.obsMot!=null &&
              this.obsMot.equals(other.getObsMot()))) &&
            ((this.perAin==null && other.getPerAin()==null) || 
             (this.perAin!=null &&
              this.perAin.equals(other.getPerAin()))) &&
            ((this.qtdAtu==null && other.getQtdAtu()==null) || 
             (this.qtdAtu!=null &&
              this.qtdAtu.equals(other.getQtdAtu()))) &&
            ((this.regEst==null && other.getRegEst()==null) || 
             (this.regEst!=null &&
              this.regEst.equals(other.getRegEst()))) &&
            ((this.retCof==null && other.getRetCof()==null) || 
             (this.retCof!=null &&
              this.retCof.equals(other.getRetCof()))) &&
            ((this.retCsl==null && other.getRetCsl()==null) || 
             (this.retCsl!=null &&
              this.retCsl.equals(other.getRetCsl()))) &&
            ((this.retIrf==null && other.getRetIrf()==null) || 
             (this.retIrf!=null &&
              this.retIrf.equals(other.getRetIrf()))) &&
            ((this.retOur==null && other.getRetOur()==null) || 
             (this.retOur!=null &&
              this.retOur.equals(other.getRetOur()))) &&
            ((this.retPis==null && other.getRetPis()==null) || 
             (this.retPis!=null &&
              this.retPis.equals(other.getRetPis()))) &&
            ((this.retPro==null && other.getRetPro()==null) || 
             (this.retPro!=null &&
              this.retPro.equals(other.getRetPro()))) &&
            ((this.rotAnx==null && other.getRotAnx()==null) || 
             (this.rotAnx!=null &&
              this.rotAnx.equals(other.getRotAnx()))) &&
            ((this.senCli==null && other.getSenCli()==null) || 
             (this.senCli!=null &&
              this.senCli.equals(other.getSenCli()))) &&
            ((this.seqInt==null && other.getSeqInt()==null) || 
             (this.seqInt!=null &&
              this.seqInt.equals(other.getSeqInt()))) &&
            ((this.seqRoe==null && other.getSeqRoe()==null) || 
             (this.seqRoe!=null &&
              this.seqRoe.equals(other.getSeqRoe()))) &&
            ((this.sigUfs==null && other.getSigUfs()==null) || 
             (this.sigUfs!=null &&
              this.sigUfs.equals(other.getSigUfs()))) &&
            ((this.sitCli==null && other.getSitCli()==null) || 
             (this.sitCli!=null &&
              this.sitCli.equals(other.getSitCli()))) &&
            ((this.temCob==null && other.getTemCob()==null) || 
             (this.temCob!=null &&
              this.temCob.equals(other.getTemCob()))) &&
            ((this.temEnt==null && other.getTemEnt()==null) || 
             (this.temEnt!=null &&
              this.temEnt.equals(other.getTemEnt()))) &&
            ((this.tipAce==null && other.getTipAce()==null) || 
             (this.tipAce!=null &&
              this.tipAce.equals(other.getTipAce()))) &&
            ((this.tipCli==null && other.getTipCli()==null) || 
             (this.tipCli!=null &&
              this.tipCli.equals(other.getTipCli()))) &&
            ((this.tipEmc==null && other.getTipEmc()==null) || 
             (this.tipEmc!=null &&
              this.tipEmc.equals(other.getTipEmc()))) &&
            ((this.tipEmp==null && other.getTipEmp()==null) || 
             (this.tipEmp!=null &&
              this.tipEmp.equals(other.getTipEmp()))) &&
            ((this.tipMer==null && other.getTipMer()==null) || 
             (this.tipMer!=null &&
              this.tipMer.equals(other.getTipMer()))) &&
            ((this.triCof==null && other.getTriCof()==null) || 
             (this.triCof!=null &&
              this.triCof.equals(other.getTriCof()))) &&
            ((this.triIcm==null && other.getTriIcm()==null) || 
             (this.triIcm!=null &&
              this.triIcm.equals(other.getTriIcm()))) &&
            ((this.triIpi==null && other.getTriIpi()==null) || 
             (this.triIpi!=null &&
              this.triIpi.equals(other.getTriIpi()))) &&
            ((this.triPis==null && other.getTriPis()==null) || 
             (this.triPis!=null &&
              this.triPis.equals(other.getTriPis()))) &&
            ((this.usuAtu==null && other.getUsuAtu()==null) || 
             (this.usuAtu!=null &&
              this.usuAtu.equals(other.getUsuAtu()))) &&
            ((this.usuCad==null && other.getUsuCad()==null) || 
             (this.usuCad!=null &&
              this.usuCad.equals(other.getUsuCad()))) &&
            ((this.usuMot==null && other.getUsuMot()==null) || 
             (this.usuMot!=null &&
              this.usuMot.equals(other.getUsuMot()))) &&
            ((this.usuOpe==null && other.getUsuOpe()==null) || 
             (this.usuOpe!=null &&
              this.usuOpe.equals(other.getUsuOpe()))) &&
            ((this.zipCod==null && other.getZipCod()==null) || 
             (this.zipCod!=null &&
              this.zipCod.equals(other.getZipCod()))) &&
            ((this.zonFra==null && other.getZonFra()==null) || 
             (this.zonFra!=null &&
              this.zonFra.equals(other.getZonFra())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getApeCli() != null) {
            _hashCode += getApeCli().hashCode();
        }
        if (getBaiCli() != null) {
            _hashCode += getBaiCli().hashCode();
        }
        if (getBaiCob() != null) {
            _hashCode += getBaiCob().hashCode();
        }
        if (getBaiEnt() != null) {
            _hashCode += getBaiEnt().hashCode();
        }
        if (getBloCre() != null) {
            _hashCode += getBloCre().hashCode();
        }
        if (getCalFun() != null) {
            _hashCode += getCalFun().hashCode();
        }
        if (getCalSen() != null) {
            _hashCode += getCalSen().hashCode();
        }
        if (getCampoUsuarioCliente() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCampoUsuarioCliente());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCampoUsuarioCliente(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCepCli() != null) {
            _hashCode += getCepCli().hashCode();
        }
        if (getCepCob() != null) {
            _hashCode += getCepCob().hashCode();
        }
        if (getCepEnt() != null) {
            _hashCode += getCepEnt().hashCode();
        }
        if (getCepFre() != null) {
            _hashCode += getCepFre().hashCode();
        }
        if (getCepIni() != null) {
            _hashCode += getCepIni().hashCode();
        }
        if (getCgcCob() != null) {
            _hashCode += getCgcCob().hashCode();
        }
        if (getCgcCpf() != null) {
            _hashCode += getCgcCpf().hashCode();
        }
        if (getCgcEnt() != null) {
            _hashCode += getCgcEnt().hashCode();
        }
        if (getCidCli() != null) {
            _hashCode += getCidCli().hashCode();
        }
        if (getCidCob() != null) {
            _hashCode += getCidCob().hashCode();
        }
        if (getCidEnt() != null) {
            _hashCode += getCidEnt().hashCode();
        }
        if (getCliCon() != null) {
            _hashCode += getCliCon().hashCode();
        }
        if (getCliFor() != null) {
            _hashCode += getCliFor().hashCode();
        }
        if (getCliPrx() != null) {
            _hashCode += getCliPrx().hashCode();
        }
        if (getCliRep() != null) {
            _hashCode += getCliRep().hashCode();
        }
        if (getCliTra() != null) {
            _hashCode += getCliTra().hashCode();
        }
        if (getCodAma() != null) {
            _hashCode += getCodAma().hashCode();
        }
        if (getCodCli() != null) {
            _hashCode += getCodCli().hashCode();
        }
        if (getCodCnv() != null) {
            _hashCode += getCodCnv().hashCode();
        }
        if (getCodFor() != null) {
            _hashCode += getCodFor().hashCode();
        }
        if (getCodGal() != null) {
            _hashCode += getCodGal().hashCode();
        }
        if (getCodGre() != null) {
            _hashCode += getCodGre().hashCode();
        }
        if (getCodMot() != null) {
            _hashCode += getCodMot().hashCode();
        }
        if (getCodMs2() != null) {
            _hashCode += getCodMs2().hashCode();
        }
        if (getCodMs3() != null) {
            _hashCode += getCodMs3().hashCode();
        }
        if (getCodMs4() != null) {
            _hashCode += getCodMs4().hashCode();
        }
        if (getCodMsg() != null) {
            _hashCode += getCodMsg().hashCode();
        }
        if (getCodPai() != null) {
            _hashCode += getCodPai().hashCode();
        }
        if (getCodPdv() != null) {
            _hashCode += getCodPdv().hashCode();
        }
        if (getCodRam() != null) {
            _hashCode += getCodRam().hashCode();
        }
        if (getCodRoe() != null) {
            _hashCode += getCodRoe().hashCode();
        }
        if (getCodRtr() != null) {
            _hashCode += getCodRtr().hashCode();
        }
        if (getCodSab() != null) {
            _hashCode += getCodSab().hashCode();
        }
        if (getCodSro() != null) {
            _hashCode += getCodSro().hashCode();
        }
        if (getCodSuf() != null) {
            _hashCode += getCodSuf().hashCode();
        }
        if (getConFin() != null) {
            _hashCode += getConFin().hashCode();
        }
        if (getContatos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getContatos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getContatos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCplCob() != null) {
            _hashCode += getCplCob().hashCode();
        }
        if (getCplEnd() != null) {
            _hashCode += getCplEnd().hashCode();
        }
        if (getCplEnt() != null) {
            _hashCode += getCplEnt().hashCode();
        }
        if (getCxaPst() != null) {
            _hashCode += getCxaPst().hashCode();
        }
        if (getDatAtu() != null) {
            _hashCode += getDatAtu().hashCode();
        }
        if (getDatCad() != null) {
            _hashCode += getDatCad().hashCode();
        }
        if (getDatFim() != null) {
            _hashCode += getDatFim().hashCode();
        }
        if (getDatIcv() != null) {
            _hashCode += getDatIcv().hashCode();
        }
        if (getDatMot() != null) {
            _hashCode += getDatMot().hashCode();
        }
        if (getDatPal() != null) {
            _hashCode += getDatPal().hashCode();
        }
        if (getDatPdv() != null) {
            _hashCode += getDatPdv().hashCode();
        }
        if (getDatSuf() != null) {
            _hashCode += getDatSuf().hashCode();
        }
        if (getDatVct() != null) {
            _hashCode += getDatVct().hashCode();
        }
        if (getEenCli() != null) {
            _hashCode += getEenCli().hashCode();
        }
        if (getEenCob() != null) {
            _hashCode += getEenCob().hashCode();
        }
        if (getEenEnt() != null) {
            _hashCode += getEenEnt().hashCode();
        }
        if (getEmaNfe() != null) {
            _hashCode += getEmaNfe().hashCode();
        }
        if (getEndCli() != null) {
            _hashCode += getEndCli().hashCode();
        }
        if (getEndCob() != null) {
            _hashCode += getEndCob().hashCode();
        }
        if (getEndEnt() != null) {
            _hashCode += getEndEnt().hashCode();
        }
        if (getEntCor() != null) {
            _hashCode += getEntCor().hashCode();
        }
        if (getEstCob() != null) {
            _hashCode += getEstCob().hashCode();
        }
        if (getEstEnt() != null) {
            _hashCode += getEstEnt().hashCode();
        }
        if (getFaxCli() != null) {
            _hashCode += getFaxCli().hashCode();
        }
        if (getFonCl2() != null) {
            _hashCode += getFonCl2().hashCode();
        }
        if (getFonCl3() != null) {
            _hashCode += getFonCl3().hashCode();
        }
        if (getFonCl4() != null) {
            _hashCode += getFonCl4().hashCode();
        }
        if (getFonCl5() != null) {
            _hashCode += getFonCl5().hashCode();
        }
        if (getFonCli() != null) {
            _hashCode += getFonCli().hashCode();
        }
        if (getHistorico() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getHistorico());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getHistorico(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getHorAtu() != null) {
            _hashCode += getHorAtu().hashCode();
        }
        if (getHorCad() != null) {
            _hashCode += getHorCad().hashCode();
        }
        if (getHorFim() != null) {
            _hashCode += getHorFim().hashCode();
        }
        if (getHorMot() != null) {
            _hashCode += getHorMot().hashCode();
        }
        if (getHorPal() != null) {
            _hashCode += getHorPal().hashCode();
        }
        if (getHorPdv() != null) {
            _hashCode += getHorPdv().hashCode();
        }
        if (getIdeCli() != null) {
            _hashCode += getIdeCli().hashCode();
        }
        if (getIndCoo() != null) {
            _hashCode += getIndCoo().hashCode();
        }
        if (getIndExp() != null) {
            _hashCode += getIndExp().hashCode();
        }
        if (getIniCob() != null) {
            _hashCode += getIniCob().hashCode();
        }
        if (getIniEnt() != null) {
            _hashCode += getIniEnt().hashCode();
        }
        if (getInsAnp() != null) {
            _hashCode += getInsAnp().hashCode();
        }
        if (getInsEnt() != null) {
            _hashCode += getInsEnt().hashCode();
        }
        if (getInsEst() != null) {
            _hashCode += getInsEst().hashCode();
        }
        if (getInsMun() != null) {
            _hashCode += getInsMun().hashCode();
        }
        if (getIntNet() != null) {
            _hashCode += getIntNet().hashCode();
        }
        if (getLimRet() != null) {
            _hashCode += getLimRet().hashCode();
        }
        if (getMarCli() != null) {
            _hashCode += getMarCli().hashCode();
        }
        if (getNatCof() != null) {
            _hashCode += getNatCof().hashCode();
        }
        if (getNatPis() != null) {
            _hashCode += getNatPis().hashCode();
        }
        if (getNatRet() != null) {
            _hashCode += getNatRet().hashCode();
        }
        if (getNenCli() != null) {
            _hashCode += getNenCli().hashCode();
        }
        if (getNenCob() != null) {
            _hashCode += getNenCob().hashCode();
        }
        if (getNenEnt() != null) {
            _hashCode += getNenEnt().hashCode();
        }
        if (getNomCli() != null) {
            _hashCode += getNomCli().hashCode();
        }
        if (getNumAnx() != null) {
            _hashCode += getNumAnx().hashCode();
        }
        if (getNumIdf() != null) {
            _hashCode += getNumIdf().hashCode();
        }
        if (getObsMot() != null) {
            _hashCode += getObsMot().hashCode();
        }
        if (getPerAin() != null) {
            _hashCode += getPerAin().hashCode();
        }
        if (getQtdAtu() != null) {
            _hashCode += getQtdAtu().hashCode();
        }
        if (getRegEst() != null) {
            _hashCode += getRegEst().hashCode();
        }
        if (getRetCof() != null) {
            _hashCode += getRetCof().hashCode();
        }
        if (getRetCsl() != null) {
            _hashCode += getRetCsl().hashCode();
        }
        if (getRetIrf() != null) {
            _hashCode += getRetIrf().hashCode();
        }
        if (getRetOur() != null) {
            _hashCode += getRetOur().hashCode();
        }
        if (getRetPis() != null) {
            _hashCode += getRetPis().hashCode();
        }
        if (getRetPro() != null) {
            _hashCode += getRetPro().hashCode();
        }
        if (getRotAnx() != null) {
            _hashCode += getRotAnx().hashCode();
        }
        if (getSenCli() != null) {
            _hashCode += getSenCli().hashCode();
        }
        if (getSeqInt() != null) {
            _hashCode += getSeqInt().hashCode();
        }
        if (getSeqRoe() != null) {
            _hashCode += getSeqRoe().hashCode();
        }
        if (getSigUfs() != null) {
            _hashCode += getSigUfs().hashCode();
        }
        if (getSitCli() != null) {
            _hashCode += getSitCli().hashCode();
        }
        if (getTemCob() != null) {
            _hashCode += getTemCob().hashCode();
        }
        if (getTemEnt() != null) {
            _hashCode += getTemEnt().hashCode();
        }
        if (getTipAce() != null) {
            _hashCode += getTipAce().hashCode();
        }
        if (getTipCli() != null) {
            _hashCode += getTipCli().hashCode();
        }
        if (getTipEmc() != null) {
            _hashCode += getTipEmc().hashCode();
        }
        if (getTipEmp() != null) {
            _hashCode += getTipEmp().hashCode();
        }
        if (getTipMer() != null) {
            _hashCode += getTipMer().hashCode();
        }
        if (getTriCof() != null) {
            _hashCode += getTriCof().hashCode();
        }
        if (getTriIcm() != null) {
            _hashCode += getTriIcm().hashCode();
        }
        if (getTriIpi() != null) {
            _hashCode += getTriIpi().hashCode();
        }
        if (getTriPis() != null) {
            _hashCode += getTriPis().hashCode();
        }
        if (getUsuAtu() != null) {
            _hashCode += getUsuAtu().hashCode();
        }
        if (getUsuCad() != null) {
            _hashCode += getUsuCad().hashCode();
        }
        if (getUsuMot() != null) {
            _hashCode += getUsuMot().hashCode();
        }
        if (getUsuOpe() != null) {
            _hashCode += getUsuOpe().hashCode();
        }
        if (getZipCod() != null) {
            _hashCode += getZipCod().hashCode();
        }
        if (getZonFra() != null) {
            _hashCode += getZonFra().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientesExportar2OutCliente.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesExportar2OutCliente"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("apeCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "apeCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baiCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "baiCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baiCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "baiCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baiEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "baiEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bloCre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "bloCre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("calFun");
        elemField.setXmlName(new javax.xml.namespace.QName("", "calFun"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("calSen");
        elemField.setXmlName(new javax.xml.namespace.QName("", "calSen"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("campoUsuarioCliente");
        elemField.setXmlName(new javax.xml.namespace.QName("", "campoUsuarioCliente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesExportar2OutClienteCampoUsuarioCliente"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cepCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cepCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cepEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepFre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cepFre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepIni");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cepIni"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cgcCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cgcCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cgcCpf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cgcCpf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cgcEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cgcEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cidCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cidCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cidCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cidCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cidEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cidEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cliCon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cliCon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cliFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cliFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cliPrx");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cliPrx"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cliRep");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cliRep"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cliTra");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cliTra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codAma");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codAma"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCnv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCnv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codGal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codGal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codGre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codGre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMs2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMs2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMs3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMs3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMs4");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMs4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMsg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMsg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPai");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPai"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPdv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPdv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRam");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRam"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRoe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRoe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRtr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRtr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codSab");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codSab"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codSro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codSro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codSuf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codSuf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conFin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "conFin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contatos");
        elemField.setXmlName(new javax.xml.namespace.QName("", "contatos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesExportar2OutClienteContatos"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cplCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cplCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cplEnd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cplEnd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cplEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cplEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cxaPst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cxaPst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAtu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAtu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datFim");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datFim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datIcv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datIcv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datMot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datMot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datPal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datPal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datPdv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datPdv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datSuf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datSuf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datVct");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datVct"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eenCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "eenCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eenCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "eenCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eenEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "eenEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emaNfe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "emaNfe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("entCor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "entCor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("faxCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "faxCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonCl2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonCl2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonCl3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonCl3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonCl4");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonCl4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonCl5");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonCl5"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("historico");
        elemField.setXmlName(new javax.xml.namespace.QName("", "historico"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "clientesExportar2OutClienteHistorico"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horAtu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horAtu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horFim");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horFim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horMot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horMot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horPal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horPal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horPdv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horPdv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ideCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ideCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indCoo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indCoo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indExp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indExp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("iniCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "iniCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("iniEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "iniEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insAnp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "insAnp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "insEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insEst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "insEst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insMun");
        elemField.setXmlName(new javax.xml.namespace.QName("", "insMun"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("intNet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "intNet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "limRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("marCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "marCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("natCof");
        elemField.setXmlName(new javax.xml.namespace.QName("", "natCof"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("natPis");
        elemField.setXmlName(new javax.xml.namespace.QName("", "natPis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("natRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "natRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nenCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nenCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nenCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nenCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nenEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nenEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numAnx");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numAnx"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numIdf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numIdf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsMot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsMot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perAin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perAin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qtdAtu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qtdAtu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("regEst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "regEst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retCof");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retCof"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retCsl");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retCsl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retIrf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retIrf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retOur");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retOur"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retPis");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retPis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retPro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retPro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rotAnx");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rotAnx"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("senCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "senCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqInt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqInt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqRoe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqRoe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sigUfs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sigUfs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("temCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "temCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("temEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "temEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipAce");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipAce"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipEmc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipEmc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipMer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipMer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triCof");
        elemField.setXmlName(new javax.xml.namespace.QName("", "triCof"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triIcm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "triIcm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triIpi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "triIpi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triPis");
        elemField.setXmlName(new javax.xml.namespace.QName("", "triPis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuAtu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuAtu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuMot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuMot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zipCod");
        elemField.setXmlName(new javax.xml.namespace.QName("", "zipCod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zonFra");
        elemField.setXmlName(new javax.xml.namespace.QName("", "zonFra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
