package br.com.senior.services.vetorh.constants;

import java.util.List;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.persist.QLRawFilter;
import com.neomind.fusion.workflow.adapter.AdapterUtils;

/**
 * Constantes para utilização dos webservices da Senior.
 * 
 * @author danilo.silva
 *
 */
public class WSConfigs {

    private String hostService = null;
    private String userService = null;
    private String passwordService = null;
    

    public WSConfigs(){
	List<NeoObject> wsList = null;
	try{
	    wsList = PersistEngine.getObjects(AdapterUtils.getEntityClass("rhWSConstants"));
	    
	    for (NeoObject itemWS : wsList) {
		
		EntityWrapper wItemWS = new EntityWrapper(itemWS);
		
		hostService = wItemWS.findGenericValue("host");
		
		userService = wItemWS.findGenericValue("user");
		
		passwordService = wItemWS.findGenericValue("psw");
		
	    }
	    if (wsList == null || (wsList != null && wsList.isEmpty())){
		System.out.println("Não foram encontradas parametrizações do webservice");
	    }
	}catch(Exception e){
	    System.out.println("Erro ao carregas WSConfigs");
	}
    }

    public String getHostService() {
        return hostService;
    }

    public void setHostService(String hostService) {
        this.hostService = hostService;
    }

    public String getUserService() {
        return userService;
    }

    public void setUserService(String userService) {
        this.userService = userService;
    }

    public String getPasswordService() {
        return passwordService;
    }

    public void setPasswordService(String passwordService) {
        this.passwordService = passwordService;
    }

    @Override
    public String toString() {
	return "WSConstants [hostService=" + hostService + ", userService=" + userService + ", passwordService=" + passwordService + "]";
    }
    
    
    
    

}
