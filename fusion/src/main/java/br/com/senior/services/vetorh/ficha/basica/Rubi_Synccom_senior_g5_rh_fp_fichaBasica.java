/**
 * Rubi_Synccom_senior_g5_rh_fp_fichaBasica.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.ficha.basica;

public interface Rubi_Synccom_senior_g5_rh_fp_fichaBasica extends java.rmi.Remote {
    public br.com.senior.services.vetorh.ficha.basica.FichaBasicaSindicatoOut sindicato(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.basica.FichaBasicaSindicatoIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.basica.FichaBasicaFichaBasica5Out fichaBasica_5(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.basica.FichaBasicaFichaBasica5In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.basica.FichaBasicaProjetosOut projetos(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.basica.FichaBasicaProjetosIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.basica.FichaBasicaSelecaoOut selecao(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.basica.FichaBasicaSelecaoIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.basica.FichaBasicaFilhosOut filhos(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.basica.FichaBasicaFilhosIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.basica.FichaBasicaDiretorOut diretor(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.basica.FichaBasicaDiretorIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.basica.FichaBasicaAnuidadesOut anuidades(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.basica.FichaBasicaAnuidadesIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.basica.FichaBasicaOutroContratoOut outroContrato(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.basica.FichaBasicaOutroContratoIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.basica.FichaBasicaPercentuaisOut percentuais(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.basica.FichaBasicaPercentuaisIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.basica.FichaBasicaContaBancariaOut contaBancaria(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.basica.FichaBasicaContaBancariaIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.basica.FichaBasicaSegurosOut seguros(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.basica.FichaBasicaSegurosIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.basica.FichaBasicaDeficienciasOut deficiencias(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.basica.FichaBasicaDeficienciasIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.basica.FichaBasicaEstrangeiroOut estrangeiro(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.basica.FichaBasicaEstrangeiroIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.basica.FichaBasicaFichaBasicaOut fichaBasica(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.basica.FichaBasicaFichaBasicaIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.basica.FichaBasicaFichaBasica4Out fichaBasica_4(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.basica.FichaBasicaFichaBasica4In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.basica.FichaBasicaPontoOut ponto(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.basica.FichaBasicaPontoIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.basica.FichaBasicaFichaBasica2Out fichaBasica_2(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.basica.FichaBasicaFichaBasica2In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.basica.FichaBasicaEstrangeiro2Out estrangeiro_2(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.basica.FichaBasicaEstrangeiro2In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.basica.FichaBasicaFichaBasica3Out fichaBasica_3(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.basica.FichaBasicaFichaBasica3In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.basica.FichaBasicaModalidadesPatOut modalidadesPat(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.basica.FichaBasicaModalidadesPatIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.vetorh.ficha.basica.FichaBasicaIndicacaoModulosOut indicacaoModulos(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.vetorh.ficha.basica.FichaBasicaIndicacaoModulosIn parameters) throws java.rmi.RemoteException;
}
