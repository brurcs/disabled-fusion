/**
 * FichaBasicaIndicacaoModulosIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.ficha.basica;

public class FichaBasicaIndicacaoModulosIn  implements java.io.Serializable {
    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private br.com.senior.services.vetorh.ficha.basica.FichaBasicaIndicacaoModulosInIndicacaoModulos[] indicacaoModulos;

    private java.lang.Integer numCad;

    private java.lang.Integer numEmp;

    public FichaBasicaIndicacaoModulosIn() {
    }

    public FichaBasicaIndicacaoModulosIn(
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           br.com.senior.services.vetorh.ficha.basica.FichaBasicaIndicacaoModulosInIndicacaoModulos[] indicacaoModulos,
           java.lang.Integer numCad,
           java.lang.Integer numEmp) {
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.indicacaoModulos = indicacaoModulos;
           this.numCad = numCad;
           this.numEmp = numEmp;
    }


    /**
     * Gets the flowInstanceID value for this FichaBasicaIndicacaoModulosIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this FichaBasicaIndicacaoModulosIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this FichaBasicaIndicacaoModulosIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this FichaBasicaIndicacaoModulosIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the indicacaoModulos value for this FichaBasicaIndicacaoModulosIn.
     * 
     * @return indicacaoModulos
     */
    public br.com.senior.services.vetorh.ficha.basica.FichaBasicaIndicacaoModulosInIndicacaoModulos[] getIndicacaoModulos() {
        return indicacaoModulos;
    }


    /**
     * Sets the indicacaoModulos value for this FichaBasicaIndicacaoModulosIn.
     * 
     * @param indicacaoModulos
     */
    public void setIndicacaoModulos(br.com.senior.services.vetorh.ficha.basica.FichaBasicaIndicacaoModulosInIndicacaoModulos[] indicacaoModulos) {
        this.indicacaoModulos = indicacaoModulos;
    }

    public br.com.senior.services.vetorh.ficha.basica.FichaBasicaIndicacaoModulosInIndicacaoModulos getIndicacaoModulos(int i) {
        return this.indicacaoModulos[i];
    }

    public void setIndicacaoModulos(int i, br.com.senior.services.vetorh.ficha.basica.FichaBasicaIndicacaoModulosInIndicacaoModulos _value) {
        this.indicacaoModulos[i] = _value;
    }


    /**
     * Gets the numCad value for this FichaBasicaIndicacaoModulosIn.
     * 
     * @return numCad
     */
    public java.lang.Integer getNumCad() {
        return numCad;
    }


    /**
     * Sets the numCad value for this FichaBasicaIndicacaoModulosIn.
     * 
     * @param numCad
     */
    public void setNumCad(java.lang.Integer numCad) {
        this.numCad = numCad;
    }


    /**
     * Gets the numEmp value for this FichaBasicaIndicacaoModulosIn.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this FichaBasicaIndicacaoModulosIn.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FichaBasicaIndicacaoModulosIn)) return false;
        FichaBasicaIndicacaoModulosIn other = (FichaBasicaIndicacaoModulosIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.indicacaoModulos==null && other.getIndicacaoModulos()==null) || 
             (this.indicacaoModulos!=null &&
              java.util.Arrays.equals(this.indicacaoModulos, other.getIndicacaoModulos()))) &&
            ((this.numCad==null && other.getNumCad()==null) || 
             (this.numCad!=null &&
              this.numCad.equals(other.getNumCad()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getIndicacaoModulos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getIndicacaoModulos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getIndicacaoModulos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getNumCad() != null) {
            _hashCode += getNumCad().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FichaBasicaIndicacaoModulosIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fichaBasicaIndicacaoModulosIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indicacaoModulos");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indicacaoModulos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fichaBasicaIndicacaoModulosInIndicacaoModulos"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
