/**
 * G5SeniorServices.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.sm.resultadosExames;

public interface G5SeniorServices extends javax.xml.rpc.Service {
    public java.lang.String getsm_Synccom_senior_g5_rh_sm_ResultadosExamesPortAddress();

    public br.com.senior.services.sm.resultadosExames.Sm_Synccom_senior_g5_rh_sm_ResultadosExames getsm_Synccom_senior_g5_rh_sm_ResultadosExamesPort() throws javax.xml.rpc.ServiceException;

    public br.com.senior.services.sm.resultadosExames.Sm_Synccom_senior_g5_rh_sm_ResultadosExames getsm_Synccom_senior_g5_rh_sm_ResultadosExamesPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
