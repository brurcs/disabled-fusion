/**
 * G5SeniorServices.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.sm.asoExterno;

public interface G5SeniorServices extends javax.xml.rpc.Service {
    public java.lang.String getsm_Synccom_senior_g5_rh_sm_asoexternoPortAddress();

    public br.com.senior.services.sm.asoExterno.Sm_Synccom_senior_g5_rh_sm_asoexterno getsm_Synccom_senior_g5_rh_sm_asoexternoPort() throws javax.xml.rpc.ServiceException;

    public br.com.senior.services.sm.asoExterno.Sm_Synccom_senior_g5_rh_sm_asoexterno getsm_Synccom_senior_g5_rh_sm_asoexternoPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
