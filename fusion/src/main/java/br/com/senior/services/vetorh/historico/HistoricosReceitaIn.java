/**
 * HistoricosReceitaIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.historico;

public class HistoricosReceitaIn  implements java.io.Serializable {
    private java.lang.Integer codRec;

    private java.lang.String datAlt;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.Integer forTri;

    private java.lang.Integer numCad;

    private java.lang.Integer numEmp;

    private java.lang.Double perAli;

    private java.lang.Integer tipCol;

    private java.lang.String tipIrf;

    private java.lang.String tipOpe;

    private java.lang.Integer tipRen;

    public HistoricosReceitaIn() {
    }

    public HistoricosReceitaIn(
           java.lang.Integer codRec,
           java.lang.String datAlt,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.Integer forTri,
           java.lang.Integer numCad,
           java.lang.Integer numEmp,
           java.lang.Double perAli,
           java.lang.Integer tipCol,
           java.lang.String tipIrf,
           java.lang.String tipOpe,
           java.lang.Integer tipRen) {
           this.codRec = codRec;
           this.datAlt = datAlt;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.forTri = forTri;
           this.numCad = numCad;
           this.numEmp = numEmp;
           this.perAli = perAli;
           this.tipCol = tipCol;
           this.tipIrf = tipIrf;
           this.tipOpe = tipOpe;
           this.tipRen = tipRen;
    }


    /**
     * Gets the codRec value for this HistoricosReceitaIn.
     * 
     * @return codRec
     */
    public java.lang.Integer getCodRec() {
        return codRec;
    }


    /**
     * Sets the codRec value for this HistoricosReceitaIn.
     * 
     * @param codRec
     */
    public void setCodRec(java.lang.Integer codRec) {
        this.codRec = codRec;
    }


    /**
     * Gets the datAlt value for this HistoricosReceitaIn.
     * 
     * @return datAlt
     */
    public java.lang.String getDatAlt() {
        return datAlt;
    }


    /**
     * Sets the datAlt value for this HistoricosReceitaIn.
     * 
     * @param datAlt
     */
    public void setDatAlt(java.lang.String datAlt) {
        this.datAlt = datAlt;
    }


    /**
     * Gets the flowInstanceID value for this HistoricosReceitaIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this HistoricosReceitaIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this HistoricosReceitaIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this HistoricosReceitaIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the forTri value for this HistoricosReceitaIn.
     * 
     * @return forTri
     */
    public java.lang.Integer getForTri() {
        return forTri;
    }


    /**
     * Sets the forTri value for this HistoricosReceitaIn.
     * 
     * @param forTri
     */
    public void setForTri(java.lang.Integer forTri) {
        this.forTri = forTri;
    }


    /**
     * Gets the numCad value for this HistoricosReceitaIn.
     * 
     * @return numCad
     */
    public java.lang.Integer getNumCad() {
        return numCad;
    }


    /**
     * Sets the numCad value for this HistoricosReceitaIn.
     * 
     * @param numCad
     */
    public void setNumCad(java.lang.Integer numCad) {
        this.numCad = numCad;
    }


    /**
     * Gets the numEmp value for this HistoricosReceitaIn.
     * 
     * @return numEmp
     */
    public java.lang.Integer getNumEmp() {
        return numEmp;
    }


    /**
     * Sets the numEmp value for this HistoricosReceitaIn.
     * 
     * @param numEmp
     */
    public void setNumEmp(java.lang.Integer numEmp) {
        this.numEmp = numEmp;
    }


    /**
     * Gets the perAli value for this HistoricosReceitaIn.
     * 
     * @return perAli
     */
    public java.lang.Double getPerAli() {
        return perAli;
    }


    /**
     * Sets the perAli value for this HistoricosReceitaIn.
     * 
     * @param perAli
     */
    public void setPerAli(java.lang.Double perAli) {
        this.perAli = perAli;
    }


    /**
     * Gets the tipCol value for this HistoricosReceitaIn.
     * 
     * @return tipCol
     */
    public java.lang.Integer getTipCol() {
        return tipCol;
    }


    /**
     * Sets the tipCol value for this HistoricosReceitaIn.
     * 
     * @param tipCol
     */
    public void setTipCol(java.lang.Integer tipCol) {
        this.tipCol = tipCol;
    }


    /**
     * Gets the tipIrf value for this HistoricosReceitaIn.
     * 
     * @return tipIrf
     */
    public java.lang.String getTipIrf() {
        return tipIrf;
    }


    /**
     * Sets the tipIrf value for this HistoricosReceitaIn.
     * 
     * @param tipIrf
     */
    public void setTipIrf(java.lang.String tipIrf) {
        this.tipIrf = tipIrf;
    }


    /**
     * Gets the tipOpe value for this HistoricosReceitaIn.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this HistoricosReceitaIn.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }


    /**
     * Gets the tipRen value for this HistoricosReceitaIn.
     * 
     * @return tipRen
     */
    public java.lang.Integer getTipRen() {
        return tipRen;
    }


    /**
     * Sets the tipRen value for this HistoricosReceitaIn.
     * 
     * @param tipRen
     */
    public void setTipRen(java.lang.Integer tipRen) {
        this.tipRen = tipRen;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HistoricosReceitaIn)) return false;
        HistoricosReceitaIn other = (HistoricosReceitaIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codRec==null && other.getCodRec()==null) || 
             (this.codRec!=null &&
              this.codRec.equals(other.getCodRec()))) &&
            ((this.datAlt==null && other.getDatAlt()==null) || 
             (this.datAlt!=null &&
              this.datAlt.equals(other.getDatAlt()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.forTri==null && other.getForTri()==null) || 
             (this.forTri!=null &&
              this.forTri.equals(other.getForTri()))) &&
            ((this.numCad==null && other.getNumCad()==null) || 
             (this.numCad!=null &&
              this.numCad.equals(other.getNumCad()))) &&
            ((this.numEmp==null && other.getNumEmp()==null) || 
             (this.numEmp!=null &&
              this.numEmp.equals(other.getNumEmp()))) &&
            ((this.perAli==null && other.getPerAli()==null) || 
             (this.perAli!=null &&
              this.perAli.equals(other.getPerAli()))) &&
            ((this.tipCol==null && other.getTipCol()==null) || 
             (this.tipCol!=null &&
              this.tipCol.equals(other.getTipCol()))) &&
            ((this.tipIrf==null && other.getTipIrf()==null) || 
             (this.tipIrf!=null &&
              this.tipIrf.equals(other.getTipIrf()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe()))) &&
            ((this.tipRen==null && other.getTipRen()==null) || 
             (this.tipRen!=null &&
              this.tipRen.equals(other.getTipRen())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodRec() != null) {
            _hashCode += getCodRec().hashCode();
        }
        if (getDatAlt() != null) {
            _hashCode += getDatAlt().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getForTri() != null) {
            _hashCode += getForTri().hashCode();
        }
        if (getNumCad() != null) {
            _hashCode += getNumCad().hashCode();
        }
        if (getNumEmp() != null) {
            _hashCode += getNumEmp().hashCode();
        }
        if (getPerAli() != null) {
            _hashCode += getPerAli().hashCode();
        }
        if (getTipCol() != null) {
            _hashCode += getTipCol().hashCode();
        }
        if (getTipIrf() != null) {
            _hashCode += getTipIrf().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        if (getTipRen() != null) {
            _hashCode += getTipRen().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HistoricosReceitaIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "historicosReceitaIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRec");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRec"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("forTri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "forTri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perAli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perAli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipIrf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipIrf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipRen");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipRen"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
