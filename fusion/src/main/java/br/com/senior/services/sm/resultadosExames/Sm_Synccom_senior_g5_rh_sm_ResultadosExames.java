/**
 * Sm_Synccom_senior_g5_rh_sm_ResultadosExames.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.sm.resultadosExames;

public interface Sm_Synccom_senior_g5_rh_sm_ResultadosExames extends java.rmi.Remote {
    public br.com.senior.services.sm.resultadosExames.ResultadosExamesResultadosOut resultados(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.sm.resultadosExames.ResultadosExamesResultadosIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.sm.resultadosExames.ResultadosExamesResultados3Out resultados_3(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.sm.resultadosExames.ResultadosExamesResultados3In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.sm.resultadosExames.ResultadosExamesResultados2Out resultados_2(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.sm.resultadosExames.ResultadosExamesResultados2In parameters) throws java.rmi.RemoteException;
}
