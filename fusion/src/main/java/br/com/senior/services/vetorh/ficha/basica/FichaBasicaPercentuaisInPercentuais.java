/**
 * FichaBasicaPercentuaisInPercentuais.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.vetorh.ficha.basica;

public class FichaBasicaPercentuaisInPercentuais  implements java.io.Serializable {
    private java.lang.Integer codPer;

    private java.lang.String desPer;

    private java.lang.Double perLiv;

    private java.lang.String tipOpe;

    public FichaBasicaPercentuaisInPercentuais() {
    }

    public FichaBasicaPercentuaisInPercentuais(
           java.lang.Integer codPer,
           java.lang.String desPer,
           java.lang.Double perLiv,
           java.lang.String tipOpe) {
           this.codPer = codPer;
           this.desPer = desPer;
           this.perLiv = perLiv;
           this.tipOpe = tipOpe;
    }


    /**
     * Gets the codPer value for this FichaBasicaPercentuaisInPercentuais.
     * 
     * @return codPer
     */
    public java.lang.Integer getCodPer() {
        return codPer;
    }


    /**
     * Sets the codPer value for this FichaBasicaPercentuaisInPercentuais.
     * 
     * @param codPer
     */
    public void setCodPer(java.lang.Integer codPer) {
        this.codPer = codPer;
    }


    /**
     * Gets the desPer value for this FichaBasicaPercentuaisInPercentuais.
     * 
     * @return desPer
     */
    public java.lang.String getDesPer() {
        return desPer;
    }


    /**
     * Sets the desPer value for this FichaBasicaPercentuaisInPercentuais.
     * 
     * @param desPer
     */
    public void setDesPer(java.lang.String desPer) {
        this.desPer = desPer;
    }


    /**
     * Gets the perLiv value for this FichaBasicaPercentuaisInPercentuais.
     * 
     * @return perLiv
     */
    public java.lang.Double getPerLiv() {
        return perLiv;
    }


    /**
     * Sets the perLiv value for this FichaBasicaPercentuaisInPercentuais.
     * 
     * @param perLiv
     */
    public void setPerLiv(java.lang.Double perLiv) {
        this.perLiv = perLiv;
    }


    /**
     * Gets the tipOpe value for this FichaBasicaPercentuaisInPercentuais.
     * 
     * @return tipOpe
     */
    public java.lang.String getTipOpe() {
        return tipOpe;
    }


    /**
     * Sets the tipOpe value for this FichaBasicaPercentuaisInPercentuais.
     * 
     * @param tipOpe
     */
    public void setTipOpe(java.lang.String tipOpe) {
        this.tipOpe = tipOpe;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FichaBasicaPercentuaisInPercentuais)) return false;
        FichaBasicaPercentuaisInPercentuais other = (FichaBasicaPercentuaisInPercentuais) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codPer==null && other.getCodPer()==null) || 
             (this.codPer!=null &&
              this.codPer.equals(other.getCodPer()))) &&
            ((this.desPer==null && other.getDesPer()==null) || 
             (this.desPer!=null &&
              this.desPer.equals(other.getDesPer()))) &&
            ((this.perLiv==null && other.getPerLiv()==null) || 
             (this.perLiv!=null &&
              this.perLiv.equals(other.getPerLiv()))) &&
            ((this.tipOpe==null && other.getTipOpe()==null) || 
             (this.tipOpe!=null &&
              this.tipOpe.equals(other.getTipOpe())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodPer() != null) {
            _hashCode += getCodPer().hashCode();
        }
        if (getDesPer() != null) {
            _hashCode += getDesPer().hashCode();
        }
        if (getPerLiv() != null) {
            _hashCode += getPerLiv().hashCode();
        }
        if (getTipOpe() != null) {
            _hashCode += getTipOpe().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FichaBasicaPercentuaisInPercentuais.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fichaBasicaPercentuaisInPercentuais"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("desPer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "desPer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perLiv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perLiv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
