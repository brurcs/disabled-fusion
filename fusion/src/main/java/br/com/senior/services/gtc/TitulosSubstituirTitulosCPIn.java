/**
 * TitulosSubstituirTitulosCPIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services.gtc;

public class TitulosSubstituirTitulosCPIn  implements java.io.Serializable {
    private java.lang.Integer codEmp;

    private java.lang.Integer codFil;

    private java.lang.String codTns;

    private java.lang.String datPgt;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String forCli;

    private TitulosSubstituirTitulosCPInTitulosBaixar[] titulosBaixar;

    private TitulosSubstituirTitulosCPInTitulosSubstitutos[] titulosSubstitutos;

    public TitulosSubstituirTitulosCPIn() {
    }

    public TitulosSubstituirTitulosCPIn(
           java.lang.Integer codEmp,
           java.lang.Integer codFil,
           java.lang.String codTns,
           java.lang.String datPgt,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String forCli,
           TitulosSubstituirTitulosCPInTitulosBaixar[] titulosBaixar,
           TitulosSubstituirTitulosCPInTitulosSubstitutos[] titulosSubstitutos) {
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.codTns = codTns;
           this.datPgt = datPgt;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.forCli = forCli;
           this.titulosBaixar = titulosBaixar;
           this.titulosSubstitutos = titulosSubstitutos;
    }


    /**
     * Gets the codEmp value for this TitulosSubstituirTitulosCPIn.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this TitulosSubstituirTitulosCPIn.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this TitulosSubstituirTitulosCPIn.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this TitulosSubstituirTitulosCPIn.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codTns value for this TitulosSubstituirTitulosCPIn.
     * 
     * @return codTns
     */
    public java.lang.String getCodTns() {
        return codTns;
    }


    /**
     * Sets the codTns value for this TitulosSubstituirTitulosCPIn.
     * 
     * @param codTns
     */
    public void setCodTns(java.lang.String codTns) {
        this.codTns = codTns;
    }


    /**
     * Gets the datPgt value for this TitulosSubstituirTitulosCPIn.
     * 
     * @return datPgt
     */
    public java.lang.String getDatPgt() {
        return datPgt;
    }


    /**
     * Sets the datPgt value for this TitulosSubstituirTitulosCPIn.
     * 
     * @param datPgt
     */
    public void setDatPgt(java.lang.String datPgt) {
        this.datPgt = datPgt;
    }


    /**
     * Gets the flowInstanceID value for this TitulosSubstituirTitulosCPIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this TitulosSubstituirTitulosCPIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this TitulosSubstituirTitulosCPIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this TitulosSubstituirTitulosCPIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the forCli value for this TitulosSubstituirTitulosCPIn.
     * 
     * @return forCli
     */
    public java.lang.String getForCli() {
        return forCli;
    }


    /**
     * Sets the forCli value for this TitulosSubstituirTitulosCPIn.
     * 
     * @param forCli
     */
    public void setForCli(java.lang.String forCli) {
        this.forCli = forCli;
    }


    /**
     * Gets the titulosBaixar value for this TitulosSubstituirTitulosCPIn.
     * 
     * @return titulosBaixar
     */
    public TitulosSubstituirTitulosCPInTitulosBaixar[] getTitulosBaixar() {
        return titulosBaixar;
    }


    /**
     * Sets the titulosBaixar value for this TitulosSubstituirTitulosCPIn.
     * 
     * @param titulosBaixar
     */
    public void setTitulosBaixar(TitulosSubstituirTitulosCPInTitulosBaixar[] titulosBaixar) {
        this.titulosBaixar = titulosBaixar;
    }

    public TitulosSubstituirTitulosCPInTitulosBaixar getTitulosBaixar(int i) {
        return this.titulosBaixar[i];
    }

    public void setTitulosBaixar(int i, TitulosSubstituirTitulosCPInTitulosBaixar _value) {
        this.titulosBaixar[i] = _value;
    }


    /**
     * Gets the titulosSubstitutos value for this TitulosSubstituirTitulosCPIn.
     * 
     * @return titulosSubstitutos
     */
    public TitulosSubstituirTitulosCPInTitulosSubstitutos[] getTitulosSubstitutos() {
        return titulosSubstitutos;
    }


    /**
     * Sets the titulosSubstitutos value for this TitulosSubstituirTitulosCPIn.
     * 
     * @param titulosSubstitutos
     */
    public void setTitulosSubstitutos(TitulosSubstituirTitulosCPInTitulosSubstitutos[] titulosSubstitutos) {
        this.titulosSubstitutos = titulosSubstitutos;
    }

    public TitulosSubstituirTitulosCPInTitulosSubstitutos getTitulosSubstitutos(int i) {
        return this.titulosSubstitutos[i];
    }

    public void setTitulosSubstitutos(int i, TitulosSubstituirTitulosCPInTitulosSubstitutos _value) {
        this.titulosSubstitutos[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosSubstituirTitulosCPIn)) return false;
        TitulosSubstituirTitulosCPIn other = (TitulosSubstituirTitulosCPIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codTns==null && other.getCodTns()==null) || 
             (this.codTns!=null &&
              this.codTns.equals(other.getCodTns()))) &&
            ((this.datPgt==null && other.getDatPgt()==null) || 
             (this.datPgt!=null &&
              this.datPgt.equals(other.getDatPgt()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.forCli==null && other.getForCli()==null) || 
             (this.forCli!=null &&
              this.forCli.equals(other.getForCli()))) &&
            ((this.titulosBaixar==null && other.getTitulosBaixar()==null) || 
             (this.titulosBaixar!=null &&
              java.util.Arrays.equals(this.titulosBaixar, other.getTitulosBaixar()))) &&
            ((this.titulosSubstitutos==null && other.getTitulosSubstitutos()==null) || 
             (this.titulosSubstitutos!=null &&
              java.util.Arrays.equals(this.titulosSubstitutos, other.getTitulosSubstitutos())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodTns() != null) {
            _hashCode += getCodTns().hashCode();
        }
        if (getDatPgt() != null) {
            _hashCode += getDatPgt().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getForCli() != null) {
            _hashCode += getForCli().hashCode();
        }
        if (getTitulosBaixar() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTitulosBaixar());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTitulosBaixar(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTitulosSubstitutos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTitulosSubstitutos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTitulosSubstitutos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosSubstituirTitulosCPIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosSubstituirTitulosCPIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTns");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTns"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datPgt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datPgt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("forCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "forCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titulosBaixar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "titulosBaixar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosSubstituirTitulosCPInTitulosBaixar"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titulosSubstitutos");
        elemField.setXmlName(new javax.xml.namespace.QName("", "titulosSubstitutos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosSubstituirTitulosCPInTitulosSubstitutos"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
