/**
 * ReceptorEventosWebService_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.segware.sigmaWebServices.webServices;

public interface ReceptorEventosWebService_PortType extends java.rmi.Remote {
    public java.lang.String receberEvento(br.com.segware.sigmaWebServices.webServices.EventoRecebido evento) throws java.rmi.RemoteException;
}
