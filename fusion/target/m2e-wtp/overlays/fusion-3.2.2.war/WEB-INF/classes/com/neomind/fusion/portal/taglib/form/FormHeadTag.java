package com.neomind.fusion.portal.taglib.form;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;

import com.neomind.fusion.persist.QLEqualsFilter;
import com.neomind.fusion.persist.QLFilter;
import com.neomind.fusion.security.NeoGroup;
import com.neomind.fusion.security.NeoPaper;
import com.neomind.fusion.security.NeoUser;
import com.neomind.fusion.security.SecurityEntity;
import com.thoughtworks.xstream.mapper.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.json.JSONObject;

import com.neomind.fusion.common.NeoObject;
import com.neomind.fusion.dms.DocumentStatus;
import com.neomind.fusion.doc.NeoDocument;
import com.neomind.fusion.doc.NeoFile;
import com.neomind.fusion.doc.NeoTemplate;
import com.neomind.fusion.eform.EForm;
import com.neomind.fusion.eform.EFormLayoutParser;
import com.neomind.fusion.eform.EFormManager;
import com.neomind.fusion.entity.EntityAction;
import com.neomind.fusion.entity.EntityInfo;
import com.neomind.fusion.entity.EntityRegister;
import com.neomind.fusion.entity.EntityWrapper;
import com.neomind.fusion.entity.FieldInfoFile;
import com.neomind.fusion.entity.ReflectionFieldWrapper;
import com.neomind.fusion.entity.SignaturePermission;
import com.neomind.fusion.entity.action.ActionBody;
import com.neomind.fusion.entity.action.ActionTrigger;
import com.neomind.fusion.entity.action.PostAction;
import com.neomind.fusion.entity.action.UrlAction;
import com.neomind.fusion.entity.action.UserEditTrigger;
import com.neomind.fusion.entity.action.UserTrigger;
import com.neomind.fusion.entity.action.UserViewTrigger;
import com.neomind.fusion.entity.dynamic.DocumentEntityInfo;
import com.neomind.fusion.entity.form.FormHeadProperties;
import com.neomind.fusion.entity.form.FormUtils;
import com.neomind.fusion.help.Help;
import com.neomind.fusion.i18n.I18nUtils;
import com.neomind.fusion.persist.PersistEngine;
import com.neomind.fusion.portal.NeoPortletConfig;
import com.neomind.fusion.portal.NeoPortletURL;
import com.neomind.fusion.portal.NeoPortletWrapper;
import com.neomind.fusion.portal.NeoRenderRequest;
import com.neomind.fusion.portal.NeoRenderResponse;
import com.neomind.fusion.portal.PortalKeys;
import com.neomind.fusion.portal.PortalUtil;
import com.neomind.fusion.portal.portlets.workflow.TaskPortlet;
import com.neomind.fusion.workflow.Activity;
import com.neomind.fusion.workflow.Task;
import com.neomind.fusion.workflow.TaskInstance;
import com.neomind.fusion.workflow.TaskInstanceHelper;
import com.neomind.fusion.workflow.TaskStatus;
import com.neomind.fusion.workflow.UserActivity;
import com.neomind.fusion.workflow.WFProcess;
import com.neomind.util.CustomFlags;
import com.neomind.util.NeoUtils;

@SuppressWarnings("serial")
public class FormHeadTag extends TagSupport
{
	private static final Log log = LogFactory.getLog(FormHeadTag.class);

	@Override
	public int doStartTag()
	{
		try
		{
			FormHeadProperties properties = new FormHeadProperties();

			properties.setRoot((Boolean) pageContext.getAttribute("root"));

			JspWriter out = pageContext.getOut();

			if (properties.isRoot())
			{
				boolean form = (Boolean) pageContext.getAttribute("form");
				EForm eForm = (EForm) pageContext.getAttribute("eForm");

				properties.setForm(form);
				properties.setEForm(eForm);

				if (form)
				{
					// Gets the portlet information
					NeoRenderRequest renderRequest = (NeoRenderRequest) pageContext.getRequest()
							.getAttribute(PortalKeys.JAVAX_PORTLET_REQUEST);
					NeoRenderResponse renderResponse = (NeoRenderResponse) pageContext.getRequest()
							.getAttribute(PortalKeys.JAVAX_PORTLET_RESPONSE);
					NeoPortletURL portletURL = (NeoPortletURL) renderResponse.createActionURL();

					properties.setPortletURL(portletURL);

					String target = eForm.getTarget();
					if (target == null || target.trim().length() == 0)
					{
						target = "";
					}
					else
					{
						target = " target='" + target + "' ";
					}

					properties.setTarget(target);

					String formCallback = renderRequest.getParameter("formCallback");
					String idContainer = renderRequest.getParameter("idContainer");
					String showAttachment = renderRequest.getParameter("showAttachment");
					String win = renderRequest.getParameter("win");
					String activeFunction = renderRequest.getParameter("activeFunction");
					String rootPath = (String) pageContext.getAttribute("rootPath");
					Long taskId = NeoUtils.safeLong(renderRequest.getParameter("task_id"));
					//Pega o nome da caixa para poder validar quando esta na caixa de gestor, para não mostrar tooltip de validação
					String boxname = renderRequest.getParameter("boxname");

					String applyCallback = renderRequest.getParameter("applyCallback");
					String urlCallback = renderRequest.getParameter("callBackURL");

					String parentField = renderRequest.getParameter("parentField");
					if (parentField != null)
					{
						properties.setParentField(parentField);
					}

					if (rootPath != null)
					{
						Long rootId = (Long) pageContext.getAttribute("rootId");
						properties.setRootId(rootId);

						EForm caller = EFormManager.getInstance().get(rootId.longValue());
						eForm.setCaller(caller);
						EFormManager.getInstance().save(eForm);
					}

					properties.setApplyCallback(applyCallback);
					properties.setUrlCallback(urlCallback);
					properties.setFormCallback(formCallback);
					properties.setIdContainer(idContainer);
					properties.setShowAttachment(showAttachment);
					properties.setWin(win);
					properties.setActiveFunction(activeFunction);
					properties.setRootPath(rootPath);
					properties.setTaskId(taskId);
					properties.setBoxname(boxname);
				}
			}
			NeoObject entity = (NeoObject) pageContext.getAttribute("entity");
			String path = (String) pageContext.getAttribute("path");

			properties.setEntity(entity);
			properties.setPath(path);

			out.print(FormUtils.renderFormHeadStart(properties));

			// Parâmetros
			String params = (String) pageContext.getRequest().getAttribute("params");
			if (!NeoUtils.safeIsNull(params))
			{
				try
				{
					JSONObject jsonObj = new JSONObject(params);
					@SuppressWarnings("unchecked")
					Iterator<String> it = jsonObj.keys();

					while (it.hasNext())
					{
						String key = it.next().toString();
						String value = jsonObj.get(key).toString();

						out.print("<input type='hidden' name='param_" + key + "' value='" + value
								+ "' />");
					}
				}
				catch (JSONException e)
				{
				}
			}

			// Lógica para e-form com layout customizado.
			EForm eForm = (EForm) pageContext.getAttribute("eForm");
			if (eForm.isCustomLayout())
			{
				// CSS customizado
				out.print(EFormLayoutParser.getCustomCss(eForm));

				// Header do layout
				if (properties.isRoot())
				{
					out.print(eForm.getHeader());
				}
			}

			out.flush();
		}
		catch (IOException e)
		{
			log.warn("Exception trying to render form header", e);
		}

		return EVAL_BODY_INCLUDE;
	}

	// TODO Transferir essa lógica para o FormFooterTag.
	@Override
	public int doEndTag()
	{
		try
		{
			EForm eForm = (EForm) pageContext.getAttribute("eForm");
			boolean root = (Boolean) pageContext.getAttribute("root");
			JspWriter out = pageContext.getOut();

			if (root && pageContext.getAttribute("entity") != null)
			{
				// Footer do layout
				if (eForm.isCustomLayout())
				{
					if (root)
					{
						out.print(eForm.getFooter());
					}
				}

				out.print("</div>");
				out.print("<br/>");

				// Renderiza os botões

				boolean edit = (Boolean) pageContext.getAttribute("edit");
				boolean customFooter = false;
				boolean showRetake = false;
				boolean showManager = false;
				boolean showSave = eForm.isEditable();
				String boxName = pageContext.getRequest().getParameter("boxname");
				if(boxName == null){
					// se a box name vir null seta "" para validar mensagem de gestor, pois compara as caixas.
					boxName = "";
				}

				Boolean readOnly = Boolean.valueOf(pageContext.getRequest().getParameter("readOnly"));

				// Valida se precisa exibir a barra de botões do formulário
				if (pageContext.getRequest().getParameter("disableFooter") == null)
				{
					final NeoPortletConfig npc = (NeoPortletConfig) pageContext
							.getAttribute("portletConfig");

					// Recupera o info
					EntityInfo entityInfo = getEntityInfo(pageContext);
					// Recupera as ações
					EntityAction[] actions = getActions(entityInfo, pageContext);

					// FIXME - Esta lógica não deve estar aqui...
					// Verificar se precisa do botão "Recusar", senão precisar retira o botão da tela
					String activityId = eForm.getParam("activity_id");
					if (activityId != null)
					{
						Activity act = PersistEngine.getNeoObject(Activity.class, new Long(activityId));
						//Verifica se usuario é gestor e atividade não esta em pool valida tooltip de Gestor
						QLFilter filter = new QLEqualsFilter("activity", act);
						List<TaskInstance> taskInst = PersistEngine.getObjects(TaskInstance.class, filter);
						for (TaskInstance i : taskInst)
						{
							Boolean managerUsers = false;
							Boolean potentialOwnerUsers = false;
							Set<SecurityEntity> setManager = i.getManagers();
							Set<SecurityEntity> setPotentialOwner = i.getPotentialOwners();
							//Verifica se o usuario logado é gestor
							for (SecurityEntity a : setManager)
							{
								if (a instanceof NeoGroup)
								{
									for (NeoUser t : ((NeoGroup) a).getUsers())
									{
										if (t.getNeoId() == PortalUtil.getCurrentUser().getNeoId())
											managerUsers = true;
									}
								}
								else if (a instanceof NeoPaper)
								{
									for (NeoUser t : ((NeoPaper) a).getUsers())
									{
										if (t.getNeoId() == PortalUtil.getCurrentUser().getNeoId())
											managerUsers = true;
									}
								}
								else if (PortalUtil.getCurrentUser().getNeoId() == a.getNeoId())
								{
									managerUsers = true;
								}
							}
							//Verifica se esta na raia
							for (SecurityEntity e:setPotentialOwner)
							{
								if (e instanceof NeoGroup)
								{
									for (NeoUser t : ((NeoGroup) e).getUsers())
									{
										if (t.getNeoId() == PortalUtil.getCurrentUser().getNeoId())
											potentialOwnerUsers = true;
									}
								}
								else if (e instanceof NeoPaper)
								{
									for (NeoUser t : ((NeoPaper) e).getUsers())
									{
										if (t.getNeoId() == PortalUtil.getCurrentUser().getNeoId())
											potentialOwnerUsers = true;
									}
								}
								else if (PortalUtil.getCurrentUser().getNeoId() == e.getNeoId())
								{
									potentialOwnerUsers = true;
								}
							}
							if (managerUsers && potentialOwnerUsers == false)
								showManager = true;
						}

						// Cabeçalho customizado para Task
						customFooter = true;

						WFProcess w = act.getProcess();
						Activity a = w.getCallBackTask();

						// legado! se não tem last origins não pode retornar
						// Não mais necessário.
						//						if (!act.getModel().isCanReturn() || !((UserActivity) act).isCanReturn()
						//								|| act.getLastOrigins().size() == 0)
						//						{
						//							if (a == null)
						//							{
						//								List<EntityAction> newActions = new ArrayList<EntityAction>();
						//								for (int i = 0; i < actions.length; i++)
						//								{
						//									if (!actions[i].getName().equalsIgnoreCase("Back"))
						//									{
						//										newActions.add(actions[i]);
						//									}
						//								}
						//								actions = newActions.toArray(new EntityAction[0]);
						//							}
						//						}

						
						// Verifica se deve desabilitar os botões de Avançar/Salvar, exibindo o botão de Retomar.
						for (int i = 0; i < actions.length; i++)
						{
							if (actions[i].getName().equals("retake"))
							{
							    	// ajuste orsegups para não mostrar botão Retomar
								showRetake = false;
								
								String taskId = eForm.getParam("task_id");
								TaskInstance ts = TaskInstanceHelper.findTaskInstance(
										Long.parseLong(taskId), true);
								//Se o status estiver como COMPLETED renderiza o botão retomar na caixa de saida.

								if (ts != null)
								{
									if (ts.getStatus() == TaskStatus.COMPLETED)
									{
										edit = true;
									}
								}
								if (ts != null)
								{
									if (ts.getStatus() != TaskStatus.COMPLETED)
									{
										showRetake = false;
										break;
									}
									else if (ts.getStatus() == TaskStatus.COMPLETED
											&& !((UserActivity) ts.getActivity()).isCanRetake())
									{ //caso de estar na caixa de saida e a tarefa for um subprocesso
										readOnly = true;
										break;
									}
								}
								else
								{
									showRetake = false;
									break;
								}
							}
						}
					}

					if (customFooter)
						out.print("<div id='footer_container' class='task_footer_container'>");
					else
						out.print("<div id='footer_container' class='footer_container'>");
					if (actions != null && actions.length > 0)
					{
						out.print("<input type=\"text\" id=\"enterKeyHack\" name=\"enterKeyHack\" style=\"display: none;\" />");

						if (customFooter)
							out.print("<fieldset class='task_edit_buttons' id='dibButtons' >");
						else
							out.print("<fieldset class='edit_buttons' id='dibButtons' >");

						NeoObject entity = (NeoObject) pageContext.getAttribute("entity");

						boolean showCancel = true;

						for (EntityAction action : actions)
						{
							//se for readOnly não renderiza nenhum botão
							if (readOnly || (action.getName().equalsIgnoreCase("save") && !showSave))
								continue;

							// Valida se a ação tem gatilho
							ActionTrigger trigger = action.getTrigger();
							if (trigger == null)
							{
								continue;
							}

							// FIXME - forca entity para tarefa original, para tratar os botoes de acoes
							if (npc != null && npc.getPortletName().equalsIgnoreCase("TaskBatch"))
							{
								entity = PersistEngine.getNeoObject(NeoUtils.safeLong(pageContext
										.getRequest().getParameter("id")));
							}

							if (FormHeadTag.log.isDebugEnabled())
							{
								FormHeadTag.log.debug(" " + action.getName() + " enabled to " + entity
										+ "?" + action.isEnabledTo(entity));
							}

							// Valida regras especificas para desabilitar a ação
							if (isDisabledTo(action, entity))
							{
								continue;
							}

							// Valida se a ação deve ser habilitado
							if (!action.isEnabledTo(entity))
							{
								continue;
							}

							if (action.getName().equalsIgnoreCase("retake"))
							{
								if (!showRetake)
									continue;
							}

							if ((action.getName().equalsIgnoreCase("save") || action.getName()
									.equalsIgnoreCase("send")) && showRetake)
								continue;

							if ((edit && trigger instanceof UserEditTrigger)
									|| (!edit && trigger instanceof UserViewTrigger))
							{
								UserTrigger userTrigger = (UserTrigger) trigger;
								String confirmQuestion = userTrigger.getConfirmQuestion();
								String onClickEvent = "";
								if (confirmQuestion == null || confirmQuestion.length() == 0)
								{
									// Se o form não for editável, não precisa validar o formulário
									if (edit && trigger instanceof UserEditTrigger)
									{
										onClickEvent = "onclick='return processAction("
												+ eForm.getFormId() + ", "
												+ action.isNeedsFormValidation() + ", event);'";
									}
								}
								else
								{
									// FIXME Falha de segurança -> Codificar o texto para HTML
									onClickEvent = "onclick='if(confirm(\""
											+ I18nUtils.getString(confirmQuestion)
											+ "\")) return processAction(" + eForm.getFormId() + ", "
											+ action.isNeedsFormValidation()
											+ ", event); else return false;'";
								}

								ActionBody body = action.getAction();

								// FIXME - Migrar essa lógica para o load do NeoObject... reiXX
								boolean sign = false;

								// TODO deve ter um jeito melhor de validar se o campo tem assinatura digital

								// Caso seja uma modal
								if (!entity.getClass().toString().equals("Task"))
								{
									for (Object o : entityInfo.getFieldSet())
									{
										NeoObject no = (NeoObject) o;
										if (no instanceof FieldInfoFile)
										{
											NeoFile file;
											if (((FieldInfoFile) no).getCanSign() == SignaturePermission.MANDATORY
													&& !((PostAction) body).getAction().equals("apply"))
											{
												if (!(eForm.getObjectId() > 0))
												{
													sign = true;
												}
												else
												{
													String field = ((FieldInfoFile) no).getName();
													file = PersistEngine.reload((NeoFile) eForm
															.getField(field).getValue());
													if (file != null && file.getSignatures().size() == 0)
													{
														showCancel = false;
													}
												}
											}
										}
									}
								}
								// caso seja workflow
								else
								{
									EntityWrapper wrapper = new EntityWrapper(((Task) entity)
											.getActivity().getProcess().getEntity());
									for (Object o : wrapper.getFieldSet())
									{
										NeoObject no = ((ReflectionFieldWrapper) o).getInfo();
										if (no instanceof FieldInfoFile && body instanceof PostAction)
										{
											NeoFile file;
											if (((FieldInfoFile) no).getCanSign() == SignaturePermission.MANDATORY
													&& ((PostAction) body).getAction().equals("send"))
											{
												String field = ((FieldInfoFile) no).getName();
												file = PersistEngine.reload((NeoFile) wrapper.getField(
														field).getValue());
												if (file == null)
												{
													sign = true;
												}
											}
										}
									}
								}

								// Maikon.Will - 16/08/2013: Custom para TCANGOLA - Chamado: 011424 - Mostrar apenas botões Salvar(e liberar) e Cancelar
								// Motivo: Necessidade de similaridade com os botões padrões de um eform
								if (CustomFlags.TCANGOLA && entityInfo instanceof DocumentEntityInfo)
								{
									// Pega o eform pai para verificar se este é um processo
									EForm parent = eForm;
									while (parent.getCaller() != null)
									{
										parent = parent.getCaller();
									}

									if (parent.getObject() instanceof Task)
									{
										if ("Save".equalsIgnoreCase(action.getName()))
										{
											continue;
										}
										else if ("saveAndClose".equalsIgnoreCase(action.getName()))
										{
											continue;
										}
										else if ("discardAndRelease".equalsIgnoreCase(action.getName()))
										{
											if (action.isEnabledTo(entity))
											{
												showCancel = true;
											}
										}
									}
								}

								String title = I18nUtils.getString(action.getName());

								// Maikon.Will - 09/09/2013: Complemento ao Custom acima
								// Salvar e Liberar - Aparecer como Salvar
								if (CustomFlags.TCANGOLA && "saveAndRelease".equals(action.getName()))
								{
									{
										// Pega o eform pai para verificar se este é um processo
										EForm parent = eForm;
										while (parent.getCaller() != null)
											parent = parent.getCaller();

										if (parent.getObject() instanceof Task)
										{
											title = I18nUtils.getString("Save");
										}
									}
								}

								if (sign)
								{
									continue;
								}

								if (body instanceof PostAction)
								{
									String buttonClass = "input_button";
									if (customFooter)
									{
										buttonClass = "task_input_button";
										if (action.isPrimaryAction())
											buttonClass = "task_input_button task_input_button_primary";
									}
									//input assumir customizado para gestor que não esta na raia.
									if (showManager && !boxName.equalsIgnoreCase("managed") && ((PostAction) body).getAction().equalsIgnoreCase("assume") || ((PostAction) body).getAction().equalsIgnoreCase("assumeTask"))
									{
										StringBuilder btn = new StringBuilder();
										btn.append("<input type='submit' class='").append(buttonClass).append("' ");
										btn.append("    id='gestorTooltipFormhead' ");
										btn.append("    name='action.").append(((PostAction) body).getAction()).append("' ");
										btn.append("    value='").append(title).append("' ");
										btn.append(" ").append(onClickEvent).append("/>");
										out.print(btn.toString());
									}
									else if (!(boxName.equalsIgnoreCase("outbox") && (action.getName().equalsIgnoreCase("send") || action.getName().equalsIgnoreCase("sendtaskaction"))))
									{
										out.print("<input  type='submit' class='" + buttonClass
												+ "' name='action." + ((PostAction) body).getAction()
												+ "' value='" + title + "' " + onClickEvent + "/>");
									}
								}
								else if (body instanceof UrlAction)
								{
									// Ação de Ajuda
									if (action.getName().equals("help"))
									{
										if (!Help.exists(entityInfo.getTypeName()))
										{
											continue;
										}
									}
									String bodyUrl = ((UrlAction) body).getUrl();
									if (action.getName().equals("help"))
										bodyUrl = bodyUrl
												.replace("#typeName#", entityInfo.getTypeName());
									else
										bodyUrl = bodyUrl.replace("#formId#", eForm.getFormId());
									String buttonClass = "input_button";


									if (customFooter)
									{
										buttonClass = "task_input_button";
										if (action.isPrimaryAction())
											buttonClass = "task_input_button task_input_button_primary";
									}

									if (action.getName().equals("cancel"))
									{
										buttonClass = "task_input_button_cancel";
									}
									out.print("<input type='button' class='" + buttonClass + "' title='"
											+ title + "' name='url." + action.getName() + "' value='"
											+ title + "' onclick='" + bodyUrl + "'/>");
								}
							}
						} // for

						// TODO - Esses botões devem ser criados no EntitAction!
						// FIXME PARA VALIDAR SE É UM PROCESSO OU NÃO, POSTERIORMENTE COLOCAR A VALIDAÇÃO POR TIPO (APLICAR NO NEOOBJECT)
						if (!(npc.getPortletName().equals("TaskContent") && edit)
								&& !PortalUtil.getCurrentUser().isReadOnly() && showCancel)
						{
							out.print("<script>");
							// closeFunction é a função que deverá ser executada caso seja feito um cancelamento deste form
							String closeFunction = pageContext.getRequest()
									.getParameter("closeFunction");

							String buttonClass = "input_button";
							if (customFooter)
								buttonClass = "task_input_button";

							if (closeFunction != null)
							{
								closeFunction = closeFunction.replace("'", "\\'");
								out.print("createCancelButton('" + closeFunction + "','" + buttonClass
										+ "');");
							}
							else
							{
								out.print("createCancelButton(null, '" + buttonClass + "');");
								if (readOnly)
								{
									// Se é somente leitura, nao renderiza botão, então remove.
									out.print("$('#dibButtons').remove();");
								}
							}
							out.print("</script>");
						}
						out.print("</fieldset>");

					}
					out.print("</div>");
				}
				// END DIV FOOTER

				boolean form = (Boolean) pageContext.getAttribute("form");
				if (form)
				{
					out.print("</form>");
				}

				out.print("<script>");
				// dando foco ao primeiro elemento da form
				out.print("var objToFocus = findFirstValidField('" + eForm.getFormId() + "');");
				out.print("try{ objToFocus.focus(); }catch (e){ }");

				out.print("if(cw.getCloseButton() != undefined){");
				// eliminando o tratamento default de fechamento da window --
				// especificamente no form o tratamento é diferente
				out.print("cw.getCloseButton().onclick = null;");
				// inserindo nova rotina de fechamento no botão X (fechar)
				out.print("NEO.neoUtils.addEvent(cw.getCloseButton(), 'click', onFormClose);");
				out.print("}");

				// inserindo evento na
				// out.print("window.attachEvent('onbeforeunload', onFormNavigation);");
				out.print("</script>");

				out.flush();
			}

			// Roda lógica para e-form com layout customizado
			if (eForm.isCustomLayout())
			{
				// Insere javascript customizado do layout no e-form
				out.print(EFormLayoutParser.getCustomJs(eForm));

				// Seta os limites do eform (altura, largura) e cor de fundo, se for root.
				out.print(EFormLayoutParser.setLayoutProperties(eForm, root));

				if (root)
				{
					out.print("<script>$('#form_container').attr('customLayout',true);</script>");
					// Alinha o layout ao centro e seta o background padrão + remove scroll do root
					out.print("<script>$('.form_content:first').css('overflow','hidden');</script>");
				}

			}

			if (root)
				out.print("<script>doScroll('window_default');</script>");

			// Limpa os erros após exibir o e-form
			if (eForm != null)
			{
				eForm.clearErrors();
			}
		}
		catch (IOException e)
		{
			log.warn("Exception trying to render form header", e);
		}
		return EVAL_PAGE;
	}

	private boolean isDisabledTo(final EntityAction action, final NeoObject entity)
	{
		// FIXME - Opções para desabilitar ações e criação de ponto de customização
		if (CustomFlags.NEOMIND
				&& (action.getName().equalsIgnoreCase("Back") || action.getName().equalsIgnoreCase(
						"Save"))
				&& ((entity.getAsString().contains("Registrar Solicitação")
						|| entity.getAsString().contains("Reformular Solicitação") || entity
						.getAsString().contains("Validar Solução"))))
		{
			return true;
		}

		if (CustomFlags.LUMINI && entity.getAsString().contains("Cadastrar Curriculo")
				&& action.getName().equalsIgnoreCase("save"))
		{
			return true;
		}

		if (!(entity instanceof NeoTemplate) && (entity instanceof NeoDocument)
				&& (action.getName().equals("XApply") || action.getName().equals("OK")))
		{
			return true;
		}

		if (entity instanceof NeoDocument && action.getName().equals("fastCheckIn"))
		{
			NeoDocument doc = (NeoDocument) entity;
			if (doc != null
					&& !doc.isVersioned()
					&& (doc.getVersionControl().getCurrent() == null || doc.getDocumentStatus() == DocumentStatus.EDITION))
				return false;
			return true;
		}

		return false;
	}

	@SuppressWarnings("deprecation")
	private EntityInfo getEntityInfo(final PageContext pageContext)
	{
		EntityInfo entityInfo = (EntityInfo) pageContext.getAttribute("entityInfo");

		// FIXME - Forçar que o portlet TaskContent exiba o conteúdo da task (content = true), mas com os botões da entidade Task (salva, recusar e salvar) de forma mais elegante.
		final NeoRenderRequest renderRequest = (NeoRenderRequest) pageContext.getRequest().getAttribute(
				PortalKeys.JAVAX_PORTLET_REQUEST);
		boolean content = NeoUtils.safeBoolean(renderRequest.getAttribute("content"));
		final NeoPortletConfig npc = (NeoPortletConfig) pageContext.getAttribute("portletConfig");
		if (npc != null
				&& (npc.getPortletName().equalsIgnoreCase("TaskContent") || npc.getPortletName()
						.equalsIgnoreCase("TaskBatch")))
		{
			content = false;

			if (npc.getPortletName().equalsIgnoreCase("TaskBatch"))
			{
				entityInfo = EntityRegister.getEntityInfo(Task.class);
			}
		}

		if (content && entityInfo != null && !(entityInfo instanceof DocumentEntityInfo))
		{
			//TODO!!
			entityInfo = EntityRegister.getInstance().getCache().getByClass(EntityInfo.class.getName());
		}

		return entityInfo;
	}

	private EntityAction[] getActions(final EntityInfo entityInfo, final PageContext pageContext)
	{
		EntityAction[] actions = new EntityAction[0];

		final NeoRenderRequest renderRequest = (NeoRenderRequest) pageContext.getRequest().getAttribute(
				PortalKeys.JAVAX_PORTLET_REQUEST);
		final NeoPortletConfig npc = (NeoPortletConfig) pageContext.getAttribute("portletConfig");

		if (npc.getPortletName().equalsIgnoreCase("TaskContent")
				|| npc.getPortletName().equalsIgnoreCase("ActivityContent"))
		{
			NeoPortletWrapper pw = (NeoPortletWrapper) pageContext.getRequest().getAttribute(
					PortalKeys.NEOMIND_PORTLET);
			TaskPortlet portlet = (TaskPortlet) pw.getPortlet();
			actions = portlet.getActions(renderRequest);
		}
		else if (!entityInfo.getAllActions().isEmpty())
		{
			actions = entityInfo.getAllActions().toArray(new EntityAction[0]);
		}

		return actions;
	}

}
