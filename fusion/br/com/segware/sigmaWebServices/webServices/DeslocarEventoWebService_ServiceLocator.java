/**
 * DeslocarEventoWebService_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.segware.sigmaWebServices.webServices;

public class DeslocarEventoWebService_ServiceLocator extends org.apache.axis.client.Service implements br.com.segware.sigmaWebServices.webServices.DeslocarEventoWebService_Service {

    public DeslocarEventoWebService_ServiceLocator() {
    }


    public DeslocarEventoWebService_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public DeslocarEventoWebService_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for DeslocarEventoWebServicePort
    private java.lang.String DeslocarEventoWebServicePort_address = "http://ssooap01:8080/SigmaWebServices/DeslocarEventoWebService";

    public java.lang.String getDeslocarEventoWebServicePortAddress() {
        return DeslocarEventoWebServicePort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String DeslocarEventoWebServicePortWSDDServiceName = "DeslocarEventoWebServicePort";

    public java.lang.String getDeslocarEventoWebServicePortWSDDServiceName() {
        return DeslocarEventoWebServicePortWSDDServiceName;
    }

    public void setDeslocarEventoWebServicePortWSDDServiceName(java.lang.String name) {
        DeslocarEventoWebServicePortWSDDServiceName = name;
    }

    public br.com.segware.sigmaWebServices.webServices.DeslocarEventoWebService_PortType getDeslocarEventoWebServicePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(DeslocarEventoWebServicePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getDeslocarEventoWebServicePort(endpoint);
    }

    public br.com.segware.sigmaWebServices.webServices.DeslocarEventoWebService_PortType getDeslocarEventoWebServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            br.com.segware.sigmaWebServices.webServices.DeslocarEventoWebServicePortBindingStub _stub = new br.com.segware.sigmaWebServices.webServices.DeslocarEventoWebServicePortBindingStub(portAddress, this);
            _stub.setPortName(getDeslocarEventoWebServicePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setDeslocarEventoWebServicePortEndpointAddress(java.lang.String address) {
        DeslocarEventoWebServicePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (br.com.segware.sigmaWebServices.webServices.DeslocarEventoWebService_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                br.com.segware.sigmaWebServices.webServices.DeslocarEventoWebServicePortBindingStub _stub = new br.com.segware.sigmaWebServices.webServices.DeslocarEventoWebServicePortBindingStub(new java.net.URL(DeslocarEventoWebServicePort_address), this);
                _stub.setPortName(getDeslocarEventoWebServicePortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("DeslocarEventoWebServicePort".equals(inputPortName)) {
            return getDeslocarEventoWebServicePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://webServices.sigmaWebServices.segware.com.br/", "DeslocarEventoWebService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://webServices.sigmaWebServices.segware.com.br/", "DeslocarEventoWebServicePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("DeslocarEventoWebServicePort".equals(portName)) {
            setDeslocarEventoWebServicePortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
