package br.com.segware.sigmaWebServices.webServices;

public class ReceptorEventosWebServiceProxy implements br.com.segware.sigmaWebServices.webServices.ReceptorEventosWebService_PortType {
  private String _endpoint = null;
  private br.com.segware.sigmaWebServices.webServices.ReceptorEventosWebService_PortType receptorEventosWebService_PortType = null;
  
  public ReceptorEventosWebServiceProxy() {
    _initReceptorEventosWebServiceProxy();
  }
  
  public ReceptorEventosWebServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initReceptorEventosWebServiceProxy();
  }
  
  private void _initReceptorEventosWebServiceProxy() {
    try {
      receptorEventosWebService_PortType = (new br.com.segware.sigmaWebServices.webServices.ReceptorEventosWebService_ServiceLocator()).getReceptorEventosWebServicePort();
      if (receptorEventosWebService_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)receptorEventosWebService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)receptorEventosWebService_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (receptorEventosWebService_PortType != null)
      ((javax.xml.rpc.Stub)receptorEventosWebService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public br.com.segware.sigmaWebServices.webServices.ReceptorEventosWebService_PortType getReceptorEventosWebService_PortType() {
    if (receptorEventosWebService_PortType == null)
      _initReceptorEventosWebServiceProxy();
    return receptorEventosWebService_PortType;
  }
  
  public java.lang.String receberEvento(br.com.segware.sigmaWebServices.webServices.EventoRecebido evento) throws java.rmi.RemoteException{
    if (receptorEventosWebService_PortType == null)
      _initReceptorEventosWebServiceProxy();
    return receptorEventosWebService_PortType.receberEvento(evento);
  }
  
  
}